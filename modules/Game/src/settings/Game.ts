/// <reference path="../views/MainSlot.ts" />
/// <reference path="BigWin.ts" />
/// <reference path="../engine/DummyEngine.ts" />

namespace MyGame.Settings
{
    export const Game: MyGame.Game.Settings =
    {
        landscape:
        {
            dimensions: { w: 1365, h: 1024 },
            stageDimensions: { w: 2048, h: 1024 }
        },
        portrait:
        {
            dimensions: { w: 1365, h: 2048 },
            stageDimensions: { w: 2048, h: 2048 },
            stageScale: 0.5 // We have twice as many pixels due to the stage height, so scale it to match
        },
        assetGroupPriorities:
        {
            [RS.Fonts.Groups.fonts_frutiger]:       RS.Asset.LoadPriority.Immediate,
            [RS.Fonts.Groups.fonts_frutigerWorld]:  RS.Asset.LoadPriority.Immediate,
            [RS.GDM.UI.Assets.Groups.gdmUI]:        RS.Asset.LoadPriority.Preload,
            [Assets.Groups.symbols]:                RS.Asset.LoadPriority.Preload,
            [Assets.Groups.mainSlot]:               RS.Asset.LoadPriority.Preload
        },

        userDefinedAllowedVariants: RS.Performance.getDevicePerformance() <= RS.Performance.Level.Low
            ? ["example"] // Use 'example' if low performance.
            : [],

        primaryView: MyGame.Views.MainSlot,
        loadingView: RS.Views.Loading,
        paytableView: RS.GDM.UI.Views.Paytable,
        replayDialogDisplayTime: 1000,
        bigWin:
        {
            controller: Red7.BigWin.Controller,
            settings: BigWin
        },
        stateMachine:
        {
            initialState: null,
            states:
            [
                RS.Slots.GameState.LoadingState,
                RS.Slots.GameState.ResumingState,
                RS.Slots.GameState.IdleState,
                RS.Slots.GameState.PreSpinState,
                RS.Slots.GameState.SpinningState,
                RS.Slots.GameState.WinRenderingState,
                RS.Slots.GameState.BigWinState,
                RS.Slots.GameState.MaxWinState,
                RS.Slots.GameState.PlayFinishedState
            ]
        },
        engine: Engine.DummyEngine,
        engineSettings: Engine.DummyEngine.defaultSettings,
        devTools:
        {
            ...RS.Slots.DevTools.defaultSettings,
            forceData:
            {
                "Multiple Winlines":
                {
                    stopIndices: [ 9, 8, 7, 1, 4 ]
                }
            }
        },
        loseForce: {stopIndices: [0, 0, 0, 0, 0]}
    };
}
