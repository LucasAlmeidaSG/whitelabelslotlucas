/// <reference path="../Reels.ts" />
/// <reference path="../../generated/Assets.ts" />

namespace MyGame.Settings.Views
{
    export const MainSlot: MyGame.Views.MainSlot.Settings =
    {
        landscape:
        {
            background:
            {
                dock: RS.Flow.Dock.Fill,
                kind: RS.Flow.Background.Kind.Image,
                asset: Assets.MainSlot.Background
            },
            reelsPos: { x: 0.5, y: 0.5 }
        },
        portrait:
        {
            background:
            {
                dock: RS.Flow.Dock.Fill,
                kind: RS.Flow.Background.Kind.Image,
                asset: Assets.MainSlot.Background.Portrait
            },
            reelsPos: { x: 0.5, y: 0.7 }
        },
        reels: ReelsComponentSettings,
        winlines:
        {
            colors: RS.Slots.Util.generateWinlineColors(5, 0.9),
            configModel: null
        }
    };
}
