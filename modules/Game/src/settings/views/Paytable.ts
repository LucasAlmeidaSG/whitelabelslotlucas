namespace MyGame.Settings.Views
{
    export const Paytable: RS.GDM.UI.Views.Paytable.Settings =
    {
        ...RS.GDM.UI.Views.Paytable.defaultSettings,
        body:
        {
            ...RS.GDM.UI.Views.Paytable.defaultSettings.body,
            size: {w: 660, h: 1000}
        },
        sections:
        [
            {
                title: "1",
                pages:
                [
                    {
                        title: "page1",
                        element: RS.Slots.Paytable.Pages.textPage.bind(
                            {
                                ...RS.Slots.Paytable.Pages.TextPage.defaultSettings,
                                elements:
                                [
                                    {
                                        kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                                        text: "page1"
                                    }
                                ]
                            }
                        )
                    },
                    {
                        title: "page2",
                        element: RS.Slots.Paytable.Pages.textPage.bind(
                            {
                                ...RS.Slots.Paytable.Pages.TextPage.defaultSettings,
                                elements:
                                [
                                    {
                                        kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                                        text: "page2"
                                    }
                                ]
                            }
                        )
                    }
                ]
            }
        ]
    }
}