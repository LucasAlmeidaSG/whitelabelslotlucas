/// <reference path="../generated/Assets.ts" />

namespace MyGame.Settings.Symbols
{
    const db = RS.Reels.Symbols.IDatabase.get();

    /** 0 - Jack */
    export const Jack: RS.Reels.Symbols.Definition =
    {
        name: "Jack",
        spriteAsset: Assets.Symbols.Jack,
        zIndex: 0
    };
    db.registerSymbol(Jack);

    /** 1 - Queen */
    export const Queen: RS.Reels.Symbols.Definition =
    {
        name: "Queen",
        spriteAsset: Assets.Symbols.Queen,
        zIndex: 0
    };
    db.registerSymbol(Queen);

    /** 2 - King */
    export const King: RS.Reels.Symbols.Definition =
    {
        name: "King",
        spriteAsset: Assets.Symbols.King,
        zIndex: 0
    };
    db.registerSymbol(King);

    /** 3 - Ace */
    export const Ace: RS.Reels.Symbols.Definition =
    {
        name: "Ace",
        spriteAsset: Assets.Symbols.Ace,
        zIndex: 0
    };
    db.registerSymbol(Ace);

    /** 4 - Wild */
    export const Wild: RS.Reels.Symbols.Definition =
    {
        name: "Wild",
        spriteAsset: Assets.Symbols.Wild,
        zIndex: 0
    };
    db.registerSymbol(Wild);

    /** 5 - Scatter */
    export const Scatter: RS.Reels.Symbols.Definition =
    {
        name: "Scatter",
        spriteAsset: Assets.Symbols.Scatter,
        isScatter: true,
        zIndex: 0
    };
    db.registerSymbol(Scatter);
}