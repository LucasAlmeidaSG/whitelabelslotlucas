/// <reference path="Symbols.ts" />

namespace MyGame.Settings
{
    const S12345: RS.Reels.Symbols.Binding[] =
    [
        { id: 0,    definition: Symbols.Jack,       selectionChance: 1 },
        { id: 1,    definition: Symbols.Queen,      selectionChance: 1 },
        { id: 2,    definition: Symbols.King,       selectionChance: 1 },
        { id: 3,    definition: Symbols.Ace,        selectionChance: 1 },
        { id: 4,    definition: Symbols.Wild,       selectionChance: 1 },
        { id: 5,    definition: Symbols.Scatter,    selectionChance: 1 }
    ];

    const GenericReelSettings: RS.Reels.Reel.Settings =
    {
        symbols: null,
        database: RS.Reels.Symbols.IDatabase.get(),
        stopSymbolCount: 3,
        windup: { time: 400, distance: -500, ease: RS.Ease.linear},
        topSpeed: { time: 500, speed: 3500, ease: RS.Ease.quadIn},
        deceleration: { time: 500, speed: 0, ease: RS.Ease.quadOut},
        overhang: {time: 100, distance: 50, ease: RS.Ease.linear},
        suspenseSpeed: { time: 750, speed: 1250, ease: RS.Ease.linear},
        suspenseSpinDownDelay: 500,
        symbolSpacing: 200,
        width: 200,
        symbolsPerStrip: 24,
        useMotionBlur: true,
        minimumBlurSpeed: 500,
        blurMultiplier: 2,
        shouldSuspense: false,
        suspenseTime: 3000,
        maskExpandTop: 0
    };

    const R12345: RS.Reels.Reel.Settings = { ...GenericReelSettings, symbols: S12345 };

    export const ReelsComponentSettings: RS.Reels.IComponent.Settings =
    {
        reelClass: RS.Reels.Reel,
        minimumStopTime: 0,
        reelStartDelta: 125,
        reelStopDelta: 125,
        scatterWinCount: 3,
        reels: [ R12345, R12345, R12345, R12345, R12345 ],

        dock: RS.Flow.Dock.Float,
        floatPosition: { x: 0.5, y: 0.4 },
        sizeToContents: true
    };
}