/// <reference path="../generated/Assets.ts" />

namespace MyGame.Views
{
    @RS.View.RequiresAssets([ Assets.Groups.symbols, Assets.Groups.mainSlot ])
    export class MainSlot extends RS.View.Base<MainSlot.Settings, Game.Context>
    {
        @RS.AutoDisposeOnSet protected _background: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _reels: RS.Reels.IGenericComponent;

        @RS.AutoDisposeOnSet protected _showButtonAreaHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _winRenderer: RS.OneOrMany<RS.Slots.WinRenderers.Base>;

        public onOpened()
        {
            super.onOpened();

            this.currencyFormatter = this.context.game.currencyFormatter;
            this.locale = this.context.game.locale;
            this.legacy = false;

            RS.Reels.Symbols.IDatabase.get().rebuild(this._controller);

            const models = this.context.game.models;
            const initialReelsState = models.config.defaultReels;
            this._reels = new RS.Reels.Component(this.settings.reels,
            {
                skipArbiter: this.context.game.arbiters.canSkip,
                initialConfiguration:
                {
                    reelSet: models.config.reelSets[initialReelsState.currentReelSetIndex],
                    symbolsInView: initialReelsState.symbolsInView,
                    stopIndices: initialReelsState.stopIndices
                }
            });
            this.addChild(this._reels);
            this.context.primaryReels = this._reels;

            this._winRenderer = new RS.Slots.WinRenderers.Composite([
                new RS.Slots.WinRenderers.SymbolAnimator(this._reels, this._reels),
                new RS.Slots.WinRenderers.Winline({
                    ...this.settings.winlines,
                    configModel: this.context.game.models.config
                }, this._reels, this._reels),
                new RS.Slots.WinRenderers.WinAmountUpdate({
                    winAmount: this.context.game.observables.win,
                    type: RS.Slots.WinRenderers.WinAmountUpdate.UpdateType.Immediate
                }),
                new RS.Slots.WinRenderers.StatusText({
                    configModel: this.context.game.models.config,
                    locale: this.context.game.locale,
                    currencyFormatter: this.context.game.currencyFormatter,
                    statusTextArbiter: this.context.game.arbiters.messageText
                }),
                new RS.Slots.WinRenderers.SymbolFade(this._reels)
            ]);
            this.context.winRenderer = this._winRenderer;

            this._showButtonAreaHandle = this.context.game.arbiters.showButtonArea.declare(true);

            if (RS.IPlatform.get().shouldDisplayDevTools && !this.context.game.models.state.isReplay)
            {
                // Dev tools
                this.context.game.devToolsController.reels = this._reels;
                this.context.game.devToolsController.tests = {
                    "Sample Test": () => alert("Sample Test")
                }
                this.context.game.devToolsController.create(this.context.game.gameVersion);
                this.context.game.devToolsController.attach(this.visibleRegionPanel,
                {
                    dock: RS.Flow.Dock.Float,
                    floatPosition: { x: 0.0, y: 0.45 },
                    dockAlignment: { x: 0.0, y: 0.5 }
                });
            }

            this.moveToTop(this.visibleRegionPanel);

            this.handleOrientationChanged(this.context.game.orientation.value);
        }

        public onClosed()
        {
            super.onClosed();

            if (this.context.primaryReels === this._reels) { this.context.primaryReels = null; }
            if (this.context.winRenderer === this._winRenderer) { this.context.winRenderer = null; }
        }

        protected setupOrientation(settings: MainSlot.OrientationSettings): void
        {
            this._background = RS.Flow.background.create(settings.background, this);
            this.moveToBottom(this._background);

            this._reels.floatPosition = settings.reelsPos;
        }

        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation): void
        {
            super.handleOrientationChanged(newOrientation);
            if (newOrientation === RS.DeviceOrientation.Landscape)
            {
                this.setupOrientation(this.settings.landscape);
            }
            else
            {
                this.setupOrientation(this.settings.portrait);
            }
            this.moveToTop(this.visibleRegionPanel);
        }
    }

    export namespace MainSlot
    {
        export interface OrientationSettings
        {
            background: RS.Flow.Background.Settings;
            reelsPos: RS.Math.Vector2D;
        }

        export interface Settings extends RS.View.Base.Settings
        {
            landscape: OrientationSettings;
            portrait: OrientationSettings;
            reels: RS.Reels.IComponent.Settings;
            winlines: RS.Slots.WinRenderers.Winline.Settings;
        }
    }
}
