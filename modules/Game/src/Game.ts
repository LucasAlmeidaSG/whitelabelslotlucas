/// <reference path="settings/Game.ts" />

namespace MyGame
{
    export class Game extends RS.Slots.Game
    {
        protected _devToolsController: RS.Slots.DevTools.IController;

        public get devToolsController() { return this._devToolsController; }

        protected _models: Models;

        public get models() { return this._models; }

        public constructor(public readonly settings: Game.Settings)
        {
            super(settings);
            RS.Storage.init("WLS", "{!GAME_VERSION!}");
        }

        @RS.Init() private static init()
        {
            const game = new Game(Settings.Game);
            game.run();
            (MyGame as any).game = game;
        }

        protected initialiseViews()
        {
            super.initialiseViews();

            this._viewController.bindSettings(Views.MainSlot, Settings.Views.MainSlot);
            this._viewController.bindSettings(RS.GDM.UI.Views.Paytable, Settings.Views.Paytable);
        }

        protected initialiseModels()
        {
            this._models = {} as Models;
            Models.clear(this._models);
        }

        protected createControllers()
        {
            super.createControllers();

            this._devToolsController = RS.Slots.DevTools.IController.get();
            this._devToolsController.initialise({
                ...this.settings.devTools,
                engine: this._engine,
                viewController: this.viewController,
                pauseArbiter: this.arbiters.pause,
                musicVolumeArbiter: this.arbiters.musicVolume,
                showButtonAreaArbiter: this.arbiters.showButtonArea,
                canSpinArbiter: this.arbiters.canSpin
            });
        }

    }

    export namespace Game
    {
        export interface Settings extends RS.Slots.Game.Settings
        {
            devTools: RS.Slots.DevTools.IController.Settings;
        }

        export interface Context extends RS.Slots.Game.Context
        {
            game: Game;
        }
    }
}
