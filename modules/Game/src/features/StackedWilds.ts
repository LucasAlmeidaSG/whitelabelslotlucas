namespace MyGame.Features
{
    export class StackedWilds implements RS.Slots.IFeature
    {
        public constructor(public readonly context: Game.Context)
        {
            RS.Log.debug("StackedWilds feature created");
        }

        /** Called before anything else when this feature is created from a restored game. Optionally returns state to move to. */
        public onResume(): PromiseLike<string>
        {
            return null;
        }

        /** Called when the resume dialog is closed after the main view has been opened. Optionally returns state to move to. */
        public onResumeDialogClosed(): PromiseLike<string>
        {
            return null;
        }

        /** Called to adjust the stop symbols of the reels before the reels have spun down. Useful for rigging certain results. */
        public adjustReels(reels: RS.Reels.IGenericComponent): void
        {
            return;
        }

        /** Called when the reels have reached maximum speed. Reels won't spin down until this is complete. Optionally returns state to move to. */
        public async onMidSpin()
        {
            const models = this.context.game.models;
            const featureData = models.feature.inReelFeature;
            if (featureData.type !== Models.Feature.InReelFeature.Type.StackedKings) { return null; }

            for (const reelIndex of featureData.reels)
            {
                await RS.ITicker.get().after(500);
                this.context.primaryReels.stopReel(reelIndex, RS.Reels.IReel.StopType.Immediate);
            }
            await RS.ITicker.get().after(500);

            return null;
        }

        /** Called when the reels have finished spinning down, but before big win or win rendering. Optionally returns state to move to. */
        public async onEndSpin()
        {
            return null;
        }

        /** Called just before win rendering has started. */
        public async onPreWinRendering(): Promise<void>
        {
            return;
        }

        /** Called after win rendering has completed. Optionally returns state to move to. */
        public async onEndWinRendering()
        {
            return null;
        }

        /** Called right at the end of the entire play, before moving to the next spin. Optionally returns state to move to. */
        public onEndPlay(): PromiseLike<string | null>
        {
            return null;
        }
    }
}
