[JIRA Board](https://nextgengaming.atlassian.net/secure/RapidBoard.jspa?projectKey=SCF&rapidView=403) | [High-Level Documentation](https://scientificgames.sharepoint.com/:f:/r/teams/BristolDocumentation/Shared%20Documents/03_Dev/Synergy%20Drive?csf=1&web=1&e=so5ofY) | [API Documentation](https://sglauncher.dev.red7sportstech.com/documentation/synergy/) | [Slots Template Game](https://bitbucket.org/red7mobile/whitelabelslot)

### How do I get set up? ###

1. Initialise your project
2. `git submodule add git@bitbucket.org:red7mobile/synergy-framework.git app/synergy`

### Something isn't working ###

Synergy aims to:

* minimise the amount of time games spend in development and testing
* ensure game quality and robustness in production

Therefore, if something does not behave as you expect, it's either a bug or incomplete documentation. If something requires a lot of effort to implement, it's either a missing feature or inflexible design in need of refactor.

Contributions, either directly in the form of pull requests, or indirectly, by reporting issues or making suggestions to the team, are always welcomed and encouraged.

### How can I contribute? ###

* Follow the [Code Style Guide](https://scientificgames.sharepoint.com/:w:/r/teams/BristolDocumentation/_layouts/15/Doc.aspx?sourcedoc=%7BB92E32BE-5576-485A-9508-69F2640C1D7B%7D&file=Synergy%20Code%20Style%20Guidelines.docx)
* Use the branch naming formats `1.1/Fix/MyFix` and `1.1/Feature/MyFeature`
* Branch from the appropriate Staging or Next branch
* Ensure the PR targets the appropriate Staging or Next branch

PRs are reviewed according to both the [Code Style Guide](https://scientificgames.sharepoint.com/:w:/r/teams/BristolDocumentation/_layouts/15/Doc.aspx?sourcedoc=%7BB92E32BE-5576-485A-9508-69F2640C1D7B%7D&file=Synergy%20Code%20Style%20Guidelines.docx) and the [PR Checklist](https://scientificgames.sharepoint.com/:x:/r/teams/BristolDocumentation/_layouts/15/Doc.aspx?sourcedoc=%7BF2AF8630-A34F-49D4-AB9E-CB928A393162%7D&file=Synergy%20Version%20Management.xlsm).

### Who do I talk to? ###

Developers using Synergy are invited to ask and talk about Synergy in the Slack channel `#synergy-framework`.

Also, feel free to message the project leads:

* Derek Choi
* Robin Graf