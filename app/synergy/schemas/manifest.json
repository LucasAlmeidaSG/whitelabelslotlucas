{
    "$schema": "https://json-schema.org/draft/2019-09/schema",
    "type": "object",
    "properties": {
        "$schema": {
            "type": "string",
            "description": "JSON Schema"
        }
    },
    "additionalProperties": {
        "anyOf": [
            {
                "$ref": "#/definitions/imageDefinition"
            },
            {
                "$ref": "#/definitions/texturePackDefinition"
            },
            {
                "$ref": "#/definitions/spineDefinition"
            },
            {
                "$ref": "#/definitions/spriteSheetDefinition"
            },
            {
                "$ref": "#/definitions/jsonDefinition"
            },
            {
                "$ref": "#/definitions/fontDefinition"
            },
            {
                "$ref": "#/definitions/soundDefinition"
            },
            {
                "$ref": "#/definitions/soundSpriteDefinition"
            },
            {
                "$ref": "#/definitions/videoDefinition"
            },
            {
                "$ref": "#/definitions/shaderDefinition"
            },
            {
                "$ref": "#/definitions/gradientDefinition"
            },
            {
                "$ref": "#/definitions/pathDefinition"
            }
        ]
    },
    "definitions": {
        "imageDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this image.",
                    "pattern": "\\.(png|jpg|jpeg|psd|tga)$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "image" ]
                },
                "usequant": {
                    "type": "boolean",
                    "description": "Whether to quantize the image (compress to 8-bit). Defaults to true."
                },
                "variants": {
                    "type": "object",
                    "description": "Any variants for this image.",
                    "properties": {
                        "desktop": {
                            "$ref": "#/definitions/imageDefinition/definitions/variant"
                        },
                        "mobile": {
                            "$ref": "#/definitions/imageDefinition/definitions/variant"
                        }
                    }
                },
                "formatSettings": {
                    "type": "object",
                    "description": "Settings to use when exporting to specific formats.",
                    "properties": {
                        "crn": {
                            "$ref": "#/definitions/imageDefinition/definitions/crnFormatSettings"
                        }
                    }
                },
                "formats": {
                    "type": "array",
                    "description": "The formats to export this asset to.",
                    "items": {
                        "type": "string",
                        "enum": [
                            "png",
                            "pvr",
                            "crn"
                        ]
                    },
                    "default": [ "png" ]
                }
            },
            "additionalProperties": false,
            "definitions": {
                "variant": {
                    "type": "object",
                    "description": "A variant for an image definition - e.g. an image may have desktop and mobile variants with different resolutions.",
                    "properties": {
                        "path": {
                            "type": "string",
                            "description": "The specific path for this image variant.",
                            "pattern": "\\.(png|jpg|jpeg|psd|tga)$"
                        },
                        "res": {
                            "type": "number",
                            "description": "The scale of this image variant in video memory. Only affects quality, not apparent size.",
                            "default": 1.0,
                            "exclusiveMinimum": 0
                        },
                        "scale": {
                            "type": "number",
                            "description": "The scale of this image variant. Affects apparent size.",
                            "default": 1.0,
                            "exclusiveMinimum": 0
                        },
                        "usequant": {
                            "type": "boolean",
                            "description": "Whether to quantize (compress to 8-bit) the image. Defaults to true.",
                            "default": true
                        },
                        "trimThreshold": {
                            "type": "integer",
                            "description": "Optional trim threshold (0-255). Defaults to 0 (no trimming).",
                            "default": 0,
                            "minimum": 0,
                            "maximum": 255
                        },
                        "qualityMin": {
                            "type": "integer",
                            "description": "Minimum image quality. Will use the original if quality is less than this value. Defaults to 0.",
                            "default": 0,
                            "minimum": 0,
                            "maximum": 100
                        },
                        "qualityMax": {
                            "type": "integer",
                            "description": "Maximum image quality. Will reduce colours if quality is higher than this value. Defaults to 100.",
                            "default": 100,
                            "minimum": 0,
                            "maximum": 100
                        },
                        "speed": {
                            "type": "integer",
                            "description": "Speed/quality trade-off. 1=slow, 3=default, 11=fast & rough.",
                            "default": 3,
                            "minimum": 1,
                            "maximum": 10
                        }
                    },
                    "additionalProperties": false
                },
                "crnFormatSettings": {
                    "type": "object",
                    "description": "Settings for exporting to crunch (crn) format.",
                    "properties": {
                        "pixelFormat": {
                            "type": "string",
                            "description": "Pixel format to compress the image to. Defaults to 'dxt5'.",
                            "enum": [
                                "dxt1",
                                "dxt1a",
                                "dxt3",
                                "dxt5",
                                "dxt5a",
                                "a8"
                            ]
                        }
                    }
                }
            }
        },
        "texturePackDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this sprite sheet."
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "texturepack" ]
                },
                "usequant": {
                    "type": "boolean",
                    "description": "Whether to quantize (compress to 8-bit) the sprite sheet's texture pages. Defaults to true."
                },
                "variants": {
                    "type": "object",
                    "description": "Any variants for this sprite sheet.",
                    "properties": {
                        "desktop": {
                            "$ref": "#/definitions/texturePackDefinition/definitions/variant"
                        },
                        "mobile": {
                            "$ref": "#/definitions/texturePackDefinition/definitions/variant"
                        }
                    }
                },
                "formatSettings": {
                    "type": "object",
                    "description": "Settings to use when exporting to specific formats.",
                    "properties": {
                        "crn": {
                            "$ref": "#/definitions/imageDefinition/definitions/crnFormatSettings"
                        }
                    }
                },
                "formats": {
                    "type": "array",
                    "description": "The formats to export this asset to.",
                    "items": {
                        "type": "string",
                        "enum": [
                            "png",
                            "pvr",
                            "crn"
                        ]
                    },
                    "default": [ "png" ]
                }
            },
            "additionalProperties": false,
            "definitions": {
                "variant": {
                    "type": "object",
                    "description": "A variant for an sprite sheet definition - e.g. a sprite sheet may have desktop and mobile variants with different resolutions.",
                    "properties": {
                        "path": {
                            "type": "string",
                            "description": "The specific path for this sprite sheet variant.",
                            "pattern": "\\.json$"
                        },
                        "res": {
                            "type": "number",
                            "description": "The scale of this variant in video memory. Only affects quality, not apparent size.\nNOTE: the scale must also be applied in the spritesheet itself; spritesheets are not automatically rescaled!",
                            "default": 1.0
                        },
                        "usequant": {
                            "type": "boolean",
                            "description": "Whether to quantize (compress to 8-bit) the sprite sheet's texture pages. Defaults to true.",
                            "default": true
                        }
                    },
                    "additionalProperties": false
                }
            }
        },
        "spineDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The path for this spine asset.",
                    "pattern": "\\.atlas$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "spine" ]
                },
                "variants": {
                    "type": "object",
                    "description": "Any variants for this spine.",
                    "properties": {
                        "desktop": {
                            "$ref": "#/definitions/spineDefinition/definitions/variant"
                        },
                        "mobile": {
                            "$ref": "#/definitions/spineDefinition/definitions/variant"
                        }
                    }
                },
                "exportArrays":
                {
                    "type": "boolean",
                    "description": "Export arrays of animations, skins, slots and bones in the asset definition. Defaults to false."
                }
            },
            "additionalProperties": false
        },
        "spriteSheetDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this sprite sheet.",
                    "pattern": "\\.json$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "spritesheet", "rsspritesheet" ]
                },
                "usequant": {
                    "type": "boolean",
                    "description": "Whether to quantize (compress to 8-bit) the sprite sheet's texture pages. Defaults to true."
                },
                "variants": {
                    "type": "object",
                    "description": "Any variants for this sprite sheet.",
                    "properties": {
                        "desktop": {
                            "$ref": "#/definitions/spriteSheetDefinition/definitions/variant"
                        },
                        "mobile": {
                            "$ref": "#/definitions/spriteSheetDefinition/definitions/variant"
                        }
                    }
                },
                "formatSettings": {
                    "type": "object",
                    "description": "Settings to use when exporting to specific formats.",
                    "properties": {
                        "crn": {
                            "$ref": "#/definitions/imageDefinition/definitions/crnFormatSettings"
                        }
                    }
                },
                "formats": {
                    "type": "array",
                    "description": "The formats to export this asset to.",
                    "items": {
                        "type": "string",
                        "enum": [
                            "png",
                            "pvr",
                            "crn"
                        ]
                    },
                    "default": [ "png" ]
                },
                "isAnimation": {
                    "type": "boolean",
                    "description": "If true, build process will convert the SpriteSheet json to RSSpriteSheet format for native playback support."
                }
            },
            "additionalProperties": false,
            "definitions": {
                "variant": {
                    "type": "object",
                    "description": "A variant for an sprite sheet definition - e.g. a sprite sheet may have desktop and mobile variants with different resolutions.",
                    "properties": {
                        "path": {
                            "type": "string",
                            "description": "The specific path for this sprite sheet variant.",
                            "pattern": "\\.json$"
                        },
                        "res": {
                            "type": "number",
                            "description": "The scale of this variant in video memory. Only affects quality, not apparent size.\nNOTE: the scale must also be applied in the spritesheet itself; spritesheets are not automatically rescaled!",
                            "default": 1.0
                        },
                        "usequant": {
                            "type": "boolean",
                            "description": "Whether to quantize (compress to 8-bit) the sprite sheet's texture pages. Defaults to true.",
                            "default": true
                        }
                    },
                    "additionalProperties": false
                }
            }
        },
        "jsonDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The path for this json asset.",
                    "pattern": "\\.json$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "json" ]
                }
            },
            "additionalProperties": false
        },
        "fontDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this font.",
                    "pattern": "\\.(woff|woff2|otf|ttf)$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "font" ]
                },
                "forceOS2": {
                    "type": "boolean",
                    "description": "If true, forces the use of the OS2 table instead of the standard OpenType HHEA table for metrics. Defaults to false.",
                    "default": false
                }
            },
            "additionalProperties": false
        },
        "soundDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset for this sound.",
                    "pattern": "\\.(mp3|mp2|aiff|aif|wav|flac|ogg|m4a)$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "sound" ]
                },
                "variants": {
                    "type": "object",
                    "description": "Any variants for this sound.",
                    "properties": {
                        "desktop": {
                            "$ref": "#/definitions/soundDefinition/definitions/variant"
                        },
                        "mobile": {
                            "$ref": "#/definitions/soundDefinition/definitions/variant"
                        }
                    }
                },
                "sampleRate": {
                    "type": "number",
                    "description": "The sample rate for this sound (hz). Recommended 44100.",
                    "default": 44100
                },
                "bitRate": {
                    "type": "number",
                    "description": "The bit rate for this sound (hz)."
                },
                "channels": {
                    "type": "number",
                    "description": "The number of channels for this sound. 2 for stereo, 1 for mono.",
                    "default": 2
                },
                "trim": {
                    "type": "number",
                    "description": "The amount of audio to clip from the front of the sound (s)."
                },
                "silencePadding": {
                    "type": "number",
                    "description": "The amount of audio to add to the front and back of the sound (s)."
                },
                "formats": {
                    "type": "array",
                    "description": "The formats to export this asset to.",
                    "items": {
                        "type": "string",
                        "enum": [
                            "mp3",
                            "mp2",
                            "aiff",
                            "aif",
                            "wav",
                            "flac",
                            "ogg",
                            "m4a"
                        ]
                    },
                    "default": [ "mp3", "m4a" ]
                }
            },
            "additionalProperties": false,
            "definitions": {
                "variant": {
                    "type": "object",
                    "description": "A variant for an sound definition - e.g. an sound may have desktop and mobile variants with different sample rates or channel counts.",
                    "properties": {
                        "path": {
                            "type": "string",
                            "description": "The specific path for this sound variant.",
                            "pattern": "\\.(mp3|mp2|aiff|aif|wav|flac|ogg|m4a)$"
                        },
                        "sampleRate": {
                            "type": "number",
                            "description": "The sample rate for this sound variant (hz). Recommended 44100.",
                            "default": 44100
                        },
                        "channels": {
                            "type": "number",
                            "description": "The number of channels for this sound variant. Recommended 2 for desktop and 1 for mobile.",
                            "default": 2
                        },
                        "trim": {
                            "type": "number",
                            "description": "The amount of audio to clip from the front of the sound (s)."
                        },
                        "silencePadding": {
                            "type": "number",
                            "description": "The amount of audio to add to the front and back of the sound (s)."
                        }
                    },
                    "additionalProperties": false
                }
            }
        },
        "soundSpriteDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "object",
                    "description": "The asset paths for this sound sprite. The keys of this map will be used as sub-asset IDs.",
                    "additionalProperties": {
                        "type": "string",
                        "pattern": "\\.(mp3|mp2|aiff|aif|wav|flac|ogg|m4a)$"
                    }
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "sound" ]
                },
                "variants": {
                    "type": "object",
                    "description": "Any variants for this sound sprite.",
                    "properties": {
                        "desktop": {
                            "$ref": "#/definitions/soundDefinition/definitions/variant"
                        },
                        "mobile": {
                            "$ref": "#/definitions/soundDefinition/definitions/variant"
                        }
                    }
                },
                "sampleRate": {
                    "type": "number",
                    "description": "The sample rate for this sound (hz). Recommended 44100.",
                    "default": 44100
                },
                "channels": {
                    "type": "number",
                    "description": "The number of channels for this sound. 2 for stereo, 1 for mono.",
                    "default": 2
                },
                "trim": {
                    "type": "number",
                    "description": "The amount of audio to clip from the front of each sound in the sound sprite (s)."
                },
                "silencePadding": {
                    "type": "number",
                    "description": "The amount of audio to add to the front and back of each sound in the the sound sprite (s)."
                },
                "formats": {
                    "type": "array",
                    "description": "The formats to export this asset to.",
                    "items": {
                        "type": "string",
                        "enum": [
                            "mp3",
                            "mp2",
                            "aiff",
                            "aif",
                            "wav",
                            "flac",
                            "ogg",
                            "m4a"
                        ]
                    },
                    "default": [ "mp3", "m4a" ]
                }
            },
            "additionalProperties": false
        },
        "videoDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this video.",
                    "pattern": "\\.mp4$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "video" ]
                },
                "usequant": {
                    "type": "boolean",
                    "description": "Whether to quantize the posters (compress to 8-bit). Defaults to true."
                },
                "usePosters": {
                    "type": "boolean",
                    "description": "Whether or not to load and use start/end posters for the video. Defaults to true."
                },
                "extractAudio": {
                    "type": "boolean",
                    "description": "Whether or not to extract, load and play audio for this video. Defaults to true."
                }
            },
            "formats": {
                "type": "array",
                "description": "The formats to export this asset's video channels to.",
                "items": {
                    "type": "string",
                    "enum": [
                        "mp4",
                        "webm",
                        "mov",
                        "avi",
                        "mpeg"
                    ]
                },
                "default": [ "mp4" ]
            },
            "sound": {
                "type": "object",
                "description": "Configuration for the video's sound channels",
                "properties": {
                    "formats": {
                        "type": "array",
                        "description": "The formats to export this asset's sound channels to.",
                        "items": {
                            "type": "string",
                            "enum": [
                                "mp3",
                                "mp2",
                                "aiff",
                                "aif",
                                "wav",
                                "flac",
                                "ogg",
                                "m4a"
                            ]
                        },
                        "default": [ "mp3", "m4a" ]
                    }
                }
            },
            "additionalProperties": false
        },
        "shaderDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this video.",
                    "pattern": "\\.(ps|vs|fs)$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "shader" ]
                }
            },
            "additionalProperties": false
        },
        "gradientDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this gradient.",
                    "pattern": "\\.grd$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "gradient" ]
                },
                "gradients": {
                    "type": "array",
                    "description": "The names of the gradients to extract from the grd file. If not present, ALL gradients will be exported.",
                    "items": {
                        "type": "string"
                    }
                },
                "foreground": {
                    "type": "string",
                    "description": "The foreground color for gradients which are based on foreground/background. Can be a named color or a hex code.",
                    "examples": [
                        "red",
                        "#ff0000"
                    ]
                },
                "background": {
                    "type": "string",
                    "description": "The background color for gradients which are based on foreground/background. Can be a named color or a hex code.",
                    "examples": [
                        "red",
                        "#ff0000"
                    ]
                }
            },
            "additionalProperties": false
        },
        "pathDefinition": {
            "properties": {
                "group": {
                    "type": "string",
                    "description": "The asset group to which this asset belongs."
                },
                "path": {
                    "type": "string",
                    "description": "The asset path for this bezier path.",
                    "pattern": "\\.ai$"
                },
                "type": {
                    "type": "string",
                    "description": "Explicit asset type.",
                    "enum": [ "path" ]
                }
            },
            "additionalProperties": false
        }
    }
}