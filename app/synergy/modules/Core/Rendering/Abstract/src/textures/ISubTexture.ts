namespace RS.Rendering
{
    /**
     * Encapsulates a frame within a texture.
     */
    export interface ISubTexture extends IDisposable
    {
        /** Gets the width of this sub-texture. */
        readonly width: number;

        /** Gets the height of this sub-texture. */
        readonly height: number;

        /** Gets or sets the base texture that this sub-texture references. */
        texture: ITexture;

        /** Gets the frame within the base texture that is referenced. */
        frame: ReadonlyRectangle;
    }

    /**
     * Constructor for ISubTexture.
     */
    export interface ISubTextureConstructor extends Function
    {
        prototype: ISubTexture;
        new(texture: ITexture): ISubTexture;
        new(texture: ITexture, frame: ReadonlyRectangle): ISubTexture;
    }

    export let SubTexture: ISubTextureConstructor;
}