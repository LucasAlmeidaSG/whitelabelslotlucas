namespace RS.Rendering
{
    /**
     * Encapsulates a pool of reusable render textures.
     */
    export interface IRenderTexturePool
    {
        /**
         * Approximate total memory usage of the pool, in bits.
         */
        readonly memoryUsage: number;

        /**
         * Acquires a render texture from the pool
         * @param width
         * @param height
         * @param exact If false, may return a render texture which is larger than the requested size
         */
        acquire(width: number, height: number, exact: boolean): IRenderTexture;

        /**
         * Acquires a render texture from the pool
         * @param width
         * @param height
         * @param resolution
         * @param exact If false, may return a render texture which is larger than the requested size or of a different resolution
         */
        acquire(width: number, height: number, resolution: number, exact: boolean): IRenderTexture;

        /**
         * Releases a render texture to the pool.
         * @param renderTexture
         */
        release(renderTexture: IRenderTexture): void;
    }

    export const IRenderTexturePool = Strategy.declare<IRenderTexturePool>(Strategy.Type.Singleton);
}