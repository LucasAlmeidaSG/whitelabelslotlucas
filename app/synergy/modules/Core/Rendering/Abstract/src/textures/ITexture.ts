namespace RS.Rendering
{
    /**
     * Encapsulates a texture.
     */
    export interface ITexture extends IDisposable
    {
        /** Observable that can be listened to to await for a texture that was just constructed to load. Will immediately complete if texture is already loaded.*/
        readonly isLoaded?: IReadonlyObservable<boolean>;

        /** Gets the apparent width of this texture. */
        readonly width: number;

        /** Gets the apparent height of this texture. */
        readonly height: number;

        /** The scale of this texture in video memory. Only affects quality, not apparent size. */
        readonly resolution: number;

        /** Gets the width of this texture in video memory. */
        readonly actualWidth: number;

        /** Gets the height of this texture in video memory. */
        readonly actualHeight: number;

        /** Gets the source object of this texture. */
        readonly source: object;

        /** Gets or sets the filter mode for the texture. */
        filterMode: ITexture.FilterMode;

        /**
         * Uploads the texture to the rendering context on the specified stage if it hasn't already been uploaded.
         * This eliminates stuttering due to just-in-time texture uploads, but takes time and uses video memory.
         */
        warmup(stage: IStage): void;
    }

    export namespace ITexture
    {
        export enum FilterMode
        {
            NearestNeighbour,
            Bilinear
        }
    }

    /**
     * Constructor for ITexture.
     */
    export interface ITextureConstructor extends Function
    {
        prototype: ITexture;
        new(source: any): ITexture;
    }

    export let Texture: ITextureConstructor;
}
