namespace RS.Rendering
{
    /**
     * Encapsulates a texture that can be rendered to.
     */
    export interface IRenderTexture extends ITexture
    {
        /**
         * Clears the render texture.
         * @param stage
         * @param clearColor colour to clear with; defaults to a transparent colour
         */
        clear(stage: IStage, clearColor?: Util.Color): void;

        /**
         * Renders the specified container to the render texture.
         * @param stage
         * @param container
         */
        render(stage: IStage, container: IContainer): void;

        /**
         * Resizes the render texture to a certain size.
         * @param newWidth The new apparent width of this texture.
         * @param newHeight The new apparent height of this texture.
         * @param resolution The new scale of this texture in video memory. Only affects quality, not apparent size.
         */
        resize(newWidth: number, newHeight: number, resolution?: number): void;
    }

    /**
     * Constructor for IRenderTexture.
     */
    export interface IRenderTextureConstructor
    {
        prototype: IRenderTexture;
        new(width: number, height: number, resolution?: number): IRenderTexture;
    }

    export let RenderTexture: IRenderTextureConstructor;

    export namespace IRenderTexture
    {
        export const defaultClearColor = new Util.Color(0, 0, 0, 0);
    }
}