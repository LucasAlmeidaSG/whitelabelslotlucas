/// <reference path="ISceneObject.ts" />

/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a mesh object with a geometry and a set of materials.
     */
    export interface IMeshObject extends ISceneObject
    {
        // #region Mesh

        /** Gets or sets the geometry of this object. */
        geometry: IReadonlyGeometry;

        /** Gets or sets the materials for this object (1 per index binding of the geometry). */
        materials: Material[];

        /** Gets or sets the color tint for this object. */
        tint: Util.Color | null;

        // #endregion
    }

    export interface MeshObjectConstructor extends Function
    {
        prototype: IMeshObject;
        new(): IMeshObject;
        new(geometry: IReadonlyGeometry, materials: Material[]): IMeshObject;
        new(modelReference: Asset.ModelReference, materials: Material[]): IMeshObject;
    }

    export let MeshObject: MeshObjectConstructor = null;
}