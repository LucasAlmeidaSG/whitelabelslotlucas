namespace RS.Rendering.Scene
{
    /** Encapsulates the type of a vertex binding. */
    export enum VertexBindingType
    {
        Position,
        TexCoord,
        Color,
        Normal
    }

    /** Encapsulates the type of a primitive. */
    export enum PrimitiveType
    {
        Triangle,
        Line
    }

    /**
     * Encapsulates data for a specific part of a vertex.
     */
    export interface VertexBinding
    {
        /** The type of this binding. */
        type: VertexBindingType;

        /** The ID of this binding relative to other bindings of the same type. */
        bindingID: number;

        /** The data for the binding. */
        vertexData: Float32Array | Uint8Array;

        /** The size of a vertex in elements. */
        vertexSize: number;

        /** Whether to normalise vertex elements (to 0-1). */
        normalise?: boolean;
    }

    export namespace VertexBinding
    {
        export function toString(vb: VertexBinding): string
        {
            return `${VertexBindingType[vb.type]}:${vb.bindingID}`;
        }
    }

    /**
     * Encapsulates a set of element indices.
     */
    export interface IndexBinding
    {
        /** The type of primitive. */
        type: PrimitiveType;

        /** The data for the binding. */
        indexData: Uint16Array;
    }

    /**
     * Encapsulates geometry data for a renderable mesh object.
     */
    export interface IReadonlyGeometry extends IDisposable
    {
        /** Gets the total number of vertices in the geometry. */
        readonly vertexCount: number;

        /** Gets a view of the vertex bindings available on this geometry. */
        readonly vertexBindings: ReadonlyArray<Readonly<VertexBinding>>;

        /** Gets a view of the index bindings available on this geometry. */
        readonly indexBindings: ReadonlyArray<Readonly<IndexBinding>>;

        /** Gets the bounds of this geometry. */
        readonly bounds: Math.ReadonlyAABB3D;

        /**
         * Validates the data of this geometry.
         * @returns a string containing information about the failed validation or null if validation succeeded.
         */
        validate(returnErr: true): string | null;

        /**
         * Validates the data of this geometry.
         * @returns true if succeeded.
         */
        validate(returnErr: false): boolean;
    }

    /**
     * Encapsulates modifiable geometry data for a renderable mesh object.
     */
    export interface IGeometry extends IReadonlyGeometry
    {
        /** Gets or sets the total number of vertices in the geometry. */
        vertexCount: number;

        /** Gets or sets a view of the vertex bindings available on this geometry. */
        readonly vertexBindings: VertexBinding[];

        /** Gets or sets a view of the index bindings available on this geometry. */
        readonly indexBindings: IndexBinding[];
    }

    export namespace Geometry
    {
        export let createGeometry: (vertexCount: number, posBuffer: Float32Array, normalBuffer: Float32Array, colorBuffer: Uint8Array, uvBuffer: Float32Array, bindingBuffer: Uint16Array, bindingType: Scene.PrimitiveType) => IReadonlyGeometry;
        export let createCube: (size?: Math.ReadonlyVector3D) => IReadonlyGeometry;
        export let createSphere: (radius?: number, divisions?: number) => IReadonlyGeometry;
        export let createCylinder: (radius?: number, divisions?: number, height?: number, uvRadius?: number) => IReadonlyGeometry;
        export let createPlane: (size?: Math.ReadonlyVector2D, uvRect?: Rendering.ReadonlyRectangle) => IReadonlyGeometry;
        export let createCircle: (radius?: number, divisions?: number, uvRadius?: number) => IReadonlyGeometry;

        export let createCubeOutline: (size?: Math.ReadonlyVector3D) => IReadonlyGeometry;
        export let createSphereOutline: (radius?: number, divisions?: number) => IReadonlyGeometry;
        export let createCylinderOutline: (radius?: number, divisions?: number, height?: number, uvRadius?: number) => IReadonlyGeometry;
        export let createPlaneOutline: (size?: Math.ReadonlyVector2D) => IReadonlyGeometry;
        export let createCircleOutline: (radius?: number, divisions?: number, uvRadius?: number) => IReadonlyGeometry;

        export let createFullScreenQuad: () => IReadonlyGeometry;

        export let createDebugNormals: (geo: IReadonlyGeometry, unitLength?: number) => IReadonlyGeometry;
        export let createDebugBezier: (path: Bezier.Path, divisionLength?: number) => IReadonlyGeometry;
    }
}