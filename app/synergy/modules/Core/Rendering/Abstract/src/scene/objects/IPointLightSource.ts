/// <reference path="ILightSource.ts" />

/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a point light source.
     */
    export interface IPointLightSource extends ILightSource
    {
        // #region Point Light

        /** Gets or sets the radius of this light in world space. */
        radius: number;

        // #endregion
    }

    export interface PointLightSourceConstructor extends Function
    {
        prototype: IPointLightSource;
        new(): IPointLightSource;
        new(radius: number): IPointLightSource;
        new(color: Util.Color, intensity: number, radius: number): IPointLightSource;
    }

    export let PointLightSource: PointLightSourceConstructor = null;
}