namespace RS.Rendering
{
    /**
     * Renders a 3D scene through a camera.
     */
    export interface ISceneView extends IDisplayObject
    {
        // #region Scene View

        /** Gets or sets the size of the viewport. */
        viewportSize: Math.Size2D;

        /** Gets or sets the camera for this view. */
        camera: Scene.ICamera;

        /**
         * Gets or sets whether to redraw the scene.
         * If false, the scene will still be displayed to the screen, just not updated.
         * This will help performance if nothing is changing in the scene.
         */
        frozen: boolean;

        // /**
        //  * Gets or sets the debug layer for this scene view.
        //  * The debug layer will be used to draw interaction raycasts.
        //  */
        // debugLayer: Scene.IDebugLayer | null;

        /** Transforms the specified point in world space to a 2D viewport coordinate. */
        projectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector2D): Math.Vector2D;

        /** Transforms the specified point in 2D viewport coordinate to world space. */
        unprojectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D;

        /** Ensures that the scene is redrawn next frame. This is only relevant if frozen. */
        renderOnce(): void;

        // #endregion
    }

    export interface ISceneViewConstructor extends Function
    {
        prototype: ISceneView;
        new(viewportSize: Math.Size2D, camera: Scene.ICamera): ISceneView;
    }

    export let SceneView: ISceneViewConstructor = null;
}
