/// <reference path="ILightSource.ts" />

/* tslint:disable:member-ordering */
/* tslint:disable:no-empty-interface */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates an ambient light source.
     */
    export interface IAmbientLightSource extends ILightSource
    {
        
    }

    export interface AmbientLightSourceConstructor extends Function
    {
        prototype: IAmbientLightSource;
        new(): IAmbientLightSource;
        new(color: Util.Color, intensity: number): IAmbientLightSource;
    }

    export let AmbientLightSource: AmbientLightSourceConstructor = null;
}