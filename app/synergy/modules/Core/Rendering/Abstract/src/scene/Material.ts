namespace RS.Rendering.Scene
{
    /**
     * Encapsulates data about how to render a particular surface.
     */
    export interface Material
    {
        /** Model to use. */
        model: Material.Model;

        /** The line width in pixels to use when rendering line primitives. Defaults to 1. */
        lineWidth?: number;

        /** Blend mode to use. Defaults to Opaque. */
        blend?: Material.Blend;

        /** Shader to use (only used if model is Shader). */
        shader?: IShader;

        /** Whether to draw both sides of the polygon. */
        twoSided?: boolean;

        /** Diffuse texture map. */
        diffuseMap?: ITexture | Asset.ImageReference;

        /** Value to modulate diffuse by. */
        diffuseTint?: Util.Color;

        /** Normal texture map (only used if model is LitPBR or LitSpec). */
        normalMap?: ITexture | Asset.ImageReference;

        /** Roughness texture map (only used if model is LitPBR). */
        roughnessMap?: ITexture | Asset.ImageReference;

        /** Value to modulate roughness by. */
        roughnessTint?: number;

        /** Metallic texture map (only used if model is LitPBR). */
        metallicMap?: ITexture | Asset.ImageReference;

        /** Value to modulate metallic by. */
        metallicTint?: number;

        /** Specular texture map (only used if model is LitSpec). */
        specularMap?: ITexture | Asset.ImageReference;

        /** Value to modulate specular color by. */
        specularColorTint?: Util.Color;

        /** Value to multiply specular power by. */
        specularPowerMult?: number;

        /** Ambient light texture. */
        ambientMap?: ITexture | Asset.ImageReference;

        /** Fallback color to use if ambient map is disabled. */
        ambientMapFallback?: Util.Color;

        /** Environment map texture (for reflections). */
        environmentMap?: ITexture | Asset.ImageReference;

        /** Value to modulate environment reflection color by. */
        environmentColorTint?: Util.Color;

        /** Whether this object should cast shadows or not. Defaults to true when model is lit and false when model is unlit. */
        castShadows?: boolean;

        /** Whether to use the vertex colors on the geometry. Defaults to true. */
        vertexColors?: boolean;
    }

    export namespace Material
    {
        /**
         * Surface rendering model.
         */
        export enum Model
        {
            /**
             * Surface is rendered using a custom shader.
             * Maps are consumed based on the shader in use.
             */
            Shader,

            /**
             * Surface is unlit.
             * Only diffuse map is consumed.
             */
            Unlit,

            /**
             * Surface is lit using physically based rendering.
             * Diffuse, normal, roughness and metallic maps are consumed.
             */
            LitPBR,

            /**
             * Surface is lit using specular based rendering.
             * Diffuse, normal and specular maps are consumed.
             */
            LitSpec
        }

        /**
         * Surface blend mode.
         */
        export enum Blend
        {
            /**
             * Surface is fully opaque.
             * Alpha is discarded.
             * Fastest option.
             */
            Opaque,

            /**
             * Surface is masked, e.g. binary alpha.
             * Alpha above 0.5 is assumed to be 1 and below 0.5 to be 0.
             * Slightly slower than opaque but still faster than transparent.
             */
            Masked,

            /**
             * Surface has full transparency via alpha.
             * No depth write, objects are sorted by z distance from camera.
             * Slowest option.
             */
            Transparent,

            /**
             * Like Transparent, but also additive.
             */
            TransparentAdd,

            /**
             * Like Transparent, but also multiplicative.
             */
            TransparentMult
        }
    }
}