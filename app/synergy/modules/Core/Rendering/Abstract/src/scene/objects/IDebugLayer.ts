/// <reference path="ISceneObject.ts" />

/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a target for drawing basic shapes for debugging purposes.
     */
    export interface IDebugLayer extends ISceneObject
    {
        // #region Debug Layer

        /**
         * Draws a wireframe circle.
         * @param origin 
         * @param normal 
         * @param radius 
         * @param color 
         * @param duration ms
         */
        drawCircle(origin: Math.ReadonlyVector3D, normal: Math.ReadonlyVector3D, radius: number, color?: Util.Color, duration?: number): void;

        /**
         * Draws a wireframe sphere.
         * @param origin 
         * @param radius 
         * @param color 
         * @param duration 
         */
        drawSphere(origin: Math.ReadonlyVector3D, radius: number, color?: Util.Color, duration?: number): void;

        /**
         * Draws a line.
         * @param a 
         * @param b 
         * @param color 
         * @param duration 
         */
        drawLine(a: Math.ReadonlyVector3D, b: Math.ReadonlyVector3D, color?: Util.Color, duration?: number): void;

        /**
         * Draws a bezier path.
         * @param a 
         * @param color 
         * @param duration 
         */
        drawPath(a: Bezier.Path, origin: Math.ReadonlyVector3D, normal: Math.ReadonlyVector3D, color?: Util.Color, duration?: number): void;

        // #endregion
    }

    export interface DebugLayerConstructor extends Function
    {
        prototype: IDebugLayer;
        new(): IDebugLayer;
    }

    export let DebugLayer: DebugLayerConstructor = null;
}