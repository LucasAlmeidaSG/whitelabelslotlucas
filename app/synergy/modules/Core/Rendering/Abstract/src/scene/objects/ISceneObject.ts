/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a renderable object in 3D space.
     */
    export interface ISceneObject extends IDisposable
    {
        // #region Transform

        /** Published when the transform for this object has been updated. */
        readonly onTransformUpdated: ISimplePrivateEvent;

        /** Gets the position of this object. */
        readonly position: Math.IObservableVector3D;

        /** Gets the rotation of this object. */
        readonly rotation: Math.IObservableQuaternion;

        /** Gets the scale of this object. */
        readonly scale: Math.IObservableVector3D;

        /** Whether or not to show debug bounds on this object. */
        debugBounds: boolean;

        /** Gets the local-to-parent transform of this object. */
        getLocalTransform(out?: Math.Matrix4): Math.Matrix4;

        /** Sets the local-to-parent transform of this object. */
        setLocalTransform(mtx: Math.Matrix4): void;

        /** Sets the local-to-parent transform of this object. */
        setLocalTransform(transform: Math.Transform3D): void;

        /** Gets the local-to-world transform of this object. */
        getWorldTransform(out?: Math.Matrix4): Math.Matrix4;

        /**
         * Sets the local-to-world transform of this object.
         * Note that this will only affect the local transform as needed to achieve the specified world transform, without modifying any parent transforms.
         */
        setWorldTransform(mtx: Math.Matrix4): void;

        /**
         * Sets the local-to-world transform of this object.
         * Note that this will only affect the local transform as needed to achieve the specified world transform, without modifying any parent transforms.
         */
        setWorldTransform(transform: Math.Transform3D): void;

        /**
         * Gets the local-to-local transform of this object relative to another object.
         */
        getRelativeTransform(relativeTo: ISceneObject, out?: Math.Matrix4): Math.Matrix4;

        /**
         * Sets the local-to-world transform of this object to be the specified transform relative to the specified object.
         * Note that this will only affect the local transform as needed, without modifying any parent transforms.
         */
        setRelativeTransform(mtx: Math.Matrix4, relativeTo: ISceneObject): void;

        /**
         * Sets the local-to-world transform of this object to be the specified transform relative to the specified object.
         * Note that this will only affect the local transform as needed, without modifying any parent transforms.
         */
        setRelativeTransform(transform: Math.Transform3D, relativeTo: ISceneObject): void;

        /** Transforms a point from world-space to local-space. */
        toLocal(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D;

        /** Transforms a point from another object's local-space to this object's local-space. */
        toLocal(point: Math.ReadonlyVector3D, from: ISceneObject, out?: Math.Vector3D): Math.Vector3D;

        /** Tranforms a point from local-space to world-space. */
        toWorld(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D;

        // #endregion

        // #region Hierarchy

        /** Published when the parent of this object has changed. */
        readonly onParentChanged: IEvent<ISceneObject | null>;

        /** Gets the root parent of this object. */
        readonly rootParent: ISceneObject | null;

        /** Gets or sets the parent of this object. */
        parent: ISceneObject | null;

        /** Gets a view of the children in this object. */
        children: ReadonlyArray<ISceneObject>;

        /**
         * Adds a child to this object, optionally at the specified index.
         * Use negative indices to indicate offset from the end.
         * @param child
         * @param index
         */
        addChild(child: ISceneObject, index?: number): this;

        /**
         * Adds a child to this object underneath the given child.
         * @param childToAdd
         * @param targetChild
         */
        addChildUnder(childToAdd: ISceneObject, targetChild: ISceneObject): this;

        /**
         * Adds a child to this object above the given child.
         * @param childToAdd
         * @param targetChild
         */
        addChildAbove(childToAdd: ISceneObject, targetChild: ISceneObject): this;

        /**
         * Adds a number of children to this object, optionally starting from the specified index.
         * Use negative indices to indicate offset from the end.
         * @param children
         * @param index
         */
        addChildren(children: ReadonlyArray<ISceneObject>, index?: number): this;

        /**
         * Adds a number of children to this object.
         * @param children
         */
        addChildren(...children: ISceneObject[]): this;

        /**
         * Sets the parent of the specified object to null if it's a child of this object
         * @param child
         */
        removeChild(child: ISceneObject): this;

        /**
         * Removes given children from this object
         * @param children
         */
        removeChildren(children: ReadonlyArray<ISceneObject>): this;

        /**
         * Removes given children from this object
         * @param children
         */
        removeChildren(...children: ISceneObject[]): this;

        /** Removes all children from this object */
        removeAllChildren(): void;

        /**
         * Removes the child at the specified index.
         * @param index
         * @returns child
         */
        removeChildAt(index: number): ISceneObject;

        /**
         * Removes all children within the specified range of indices, exclusive of endIndex.
         * @param startIndex
         * @param endIndex
         */
        removeChildRange(startIndex: number, endIndex: number): ISceneObject[];

        /**
         * Gets if this object contains the specified child.
         * @param child
         * @param deep If true, will look in any child objects also.
         */
        hasChild(child: ISceneObject, deep?: boolean): boolean;

        /**
         * Gets the index of the specified child, or -1 if it's not found.
         * @param child
         */
        getChildIndex(child: ISceneObject): number;

        /**
         * Sets the index of the specified child.
         * Use negative indices to indicate offset from the end.
         * @param child
         * @param index
         */
        setChildIndex(child: ISceneObject, index: number): this;

        /**
         * Gets the first child that satisfies the given filter function.
         * If more than one child satisfies the filter, the first one will be returned.
         * @param filter Filter function that takes a display object and returns true when it matches the desired condition.
         * @param deep If true, will look in any child containers also.
         */
        getChild(filter: (element: ISceneObject) => boolean, deep?: boolean): ISceneObject;

        /**
         * Gets the child at the specified index.
         * @param index
         */
        getChildAt(index: number): ISceneObject;

        /**
         * Gets the child with the specified name.
         * If more than one child exists with the name, the first one will be returned.
         * @param name
         * @param deep If true, will look in any child containers also.
         */
        getChildByName(name: string, deep?: boolean): ISceneObject;

        // #endregion

        // #region Bounds

        /** Gets the local bounds of this object. */
        readonly localBounds: Math.ReadonlyAABB3D;

        /** Gets the world bounds of this object. */
        readonly worldBounds: Math.ReadonlyAABB3D;

        /** Gets or sets if bounds should be displayed for debugging purposes. */
        displayBounds: boolean;

        /** Finds all children of this object that intersect the specified AABB in world space. */
        findIntersecting(aabb: Math.ReadonlyAABB3D, out?: ISceneObject[]): ISceneObject[];

        /** Finds the first interactive child of this object that intersects the specified line. */
        interactionRaycast(ctx: ISceneObject.InteractionRaycastContext): void;

        // #endregion

        // #region Display

        /** Gets or sets if this object is visible. */
        visible: boolean;

        /** Gets or sets this objects name. */
        name: string | null;

        // #endregion

        // #region Interaction

        /** Gets or sets whether this object should respond to interaction events.  */
        interactive: boolean;

        /** Gets or sets whether this object should pass interaction events down to children. */
        interactiveChildren: boolean;

        /** Gets or sets the cursor to show when this object is hovered. */
        cursor: Cursor;

        readonly onRightClicked:   IPrivateEvent<InteractionEventData>;
        readonly onRightDown:      IPrivateEvent<InteractionEventData>;
        readonly onRightUp:        IPrivateEvent<InteractionEventData>;
        readonly onRightUpOutside: IPrivateEvent<InteractionEventData>;

        readonly onClicked:        IPrivateEvent<InteractionEventData>;
        readonly onClickCancelled: IPrivateEvent<InteractionEventData>;
        readonly onDown:           IPrivateEvent<InteractionEventData>;
        readonly onMoved:          IPrivateEvent<InteractionEventData>;
        readonly onOut:            IPrivateEvent<InteractionEventData>;
        readonly onOver:           IPrivateEvent<InteractionEventData>;
        readonly onUp:             IPrivateEvent<InteractionEventData>;
        readonly onUpOutside:      IPrivateEvent<InteractionEventData>;

        // #endregion
    }

    export let SceneObjectTracker: RS.Util.IObjectTracker<ISceneObject>;

    export namespace ISceneObject
    {
        export interface InteractionRaycastContext
        {
            line: Math.ReadonlyLine3D;
            hitObject: ISceneObject;
            hitPoint: Math.Vector3D;
            hitDist: number;
        }

        export namespace InteractionRaycastContext
        {
            /**
             * Tests the specified hit point against the current closest hit and takes it if it's closer.
             * @param obj
             * @param hitPoint
             */
            export function setPoint(ctx: InteractionRaycastContext, obj: ISceneObject, hitPoint: Math.ReadonlyVector3D): boolean
            {
                const dist = Math.Vector3D.distanceBetween(ctx.line, hitPoint);
                if (dist < ctx.hitDist)
                {
                    ctx.hitDist = dist;
                    ctx.hitObject = obj;
                    Math.Vector3D.copy(hitPoint, ctx.hitPoint);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /**
             * Copies from one context to another.
             * @param from
             * @param to
             */
            export function copy(from: Readonly<InteractionRaycastContext>, to: InteractionRaycastContext): void
            {
                Math.Line3D.copy(from.line, to.line);
                to.hitObject = from.hitObject;
                Math.Vector3D.copy(from.hitPoint, to.hitPoint);
                to.hitDist = from.hitDist;
            }
        }

        const tmpArr: ISceneObject[] = [];

        /**
         * Finds the common grandparent among an array of scene objects.
         * May return null if the objects belong to different scene roots.
         * @param objects
         * @param allowObjects whether to allow the objects themselves as a common grandparent.
         */
        export function findCommonGrandparent(objects: ReadonlyArray<ISceneObject>, allowObjects: boolean = false): ISceneObject | null
        {
            if (objects.length === 0) { return null; }
            if (objects.length === 1) { return objects[0].parent; }
            tmpArr.length = 0;
            let p = allowObjects ? objects[0] : objects[0].parent;
            while (p)
            {
                tmpArr.unshift(p);
                p = p.parent;
            }
            outerLoop: for (let i = 1, l = objects.length; i < l; ++i)
            {
                const obj = objects[1];
                p = allowObjects ? obj : obj.parent;
                while (p)
                {
                    const idx = tmpArr.indexOf(p);
                    if (idx !== -1)
                    {
                        tmpArr.length = idx + 1;
                        continue outerLoop;
                    }
                }
                return null;
            }

            return tmpArr[tmpArr.length - 1];
        }

        /**
         * Draws a bounding box around the given scene object and returns it.
         */
        export function debugBounds(target: ISceneObject): ISceneObject
        {
            const worldBounds = target.worldBounds;
            const w = worldBounds.maxX - worldBounds.minX,
                  h = worldBounds.maxY - worldBounds.minY,
                  l = worldBounds.maxZ - worldBounds.minZ;
            const box = Geometry.createCubeOutline(Math.Vector3D(w, h, l));
            const material: Material =
            {
                model: Material.Model.Unlit,
                diffuseTint: Util.Colors.red
            };
            const mesh = new MeshObject(box, [material]);
            mesh.name = "BoundsDebug";
            target.addChild(mesh);
            return mesh;
        }
    }

    /**
     * Data for an interaction event with a scene object.
     */
    export interface InteractionEventData extends RS.Rendering.InteractionEventData
    {
        /** The scene object interacted with. */
        sceneTarget: ISceneObject;

        /** The position of the event in scene world space. */
        globalScenePosition: Math.Vector3D;

        /** The position of the event in scene local space. */
        localScenePosition: Math.Vector3D;

        /** The context of the raycast used to determine the interaction. */
        raycastData: ISceneObject.InteractionRaycastContext;
    }

    export interface SceneObjectConstructor extends Function
    {
        prototype: ISceneObject;
        new(name?: string): ISceneObject;
    }

    export let SceneObject: SceneObjectConstructor = null;
}
