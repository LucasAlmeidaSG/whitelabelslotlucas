/// <reference path="ISceneObject.ts" />

/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a camera which renders the scene it is parented to.
     */
    export interface ICamera extends ISceneObject
    {
        // #region Camera

        /** Gets or sets the background color of this camera. */
        backgroundColor: Util.Color;

        /** Gets or sets the projection matrix of this camera. */
        projection: Math.Matrix4;

        /** Sets this camera up with a perspective view. */
        setPerspective(fov: number, aspectRatio: number): void;

        /** Sets this camera up with an orthographic view. */
        setOrthographic(left: number, right: number, top: number, bottom: number): void;

        /** Transforms the specified point in world space to normalised viewport coordinates (0-1). */
        projectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector2D): Math.Vector2D;

        // #endregion
    }

    export interface CameraConstructor extends Function
    {
        prototype: ICamera;
        new(): ICamera;
        new(fov: number, aspectRatio: number): ICamera;
    }

    export let Camera: CameraConstructor = null;
}
