/// <reference path="ICamera.ts" />

/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a controllable camera which renders the scene it is parented to.
     */
    export interface IFlyCamera extends ICamera
    {
        // #region Fly Camera

        /** Gets or sets the left-right speed. */
        xSpeed: number;

        /** Gets or sets the up-down speed. */
        ySpeed: number;

        /** Gets or sets the forward-back speed. */
        zSpeed: number;

        /** Gets or sets the pitch rotation speed. */
        pitchSpeed: number;

        /** Gets or sets the yaw rotation speed. */
        yawSpeed: number;

        /** Gets the movement vector. */
        readonly moveVec: Math.Vector3D;

        /** Rotates this camera based on the x and y deltas. */
        rotate(x: number, y: number): void;

        // #endregion
    }

    export interface FlyCameraConstructor extends Function
    {
        prototype: IFlyCamera;
        new(): IFlyCamera;
        new(fov: number, aspectRatio: number): IFlyCamera;
    }

    export let FlyCamera: FlyCameraConstructor = null;
}