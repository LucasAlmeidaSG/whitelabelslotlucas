/// <reference path="ISceneObject.ts" />

/* tslint:disable:member-ordering */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a light source.
     */
    export interface ILightSource extends ISceneObject
    {
        // #region Light

        /** Gets or sets the color of this light. */
        color: Util.Color;

        /** Gets or sets the intensity of this light. */
        intensity: number;

        /** Gets or sets settings for the shadow that this light should cast. */
        shadow: ILightSource.Shadow | null;

        // #endregion
    }

    export namespace ILightSource
    {
        export enum ShadowType
        {
            /** Shadows are not casted. */
            None,

            /** Geometry is projected against a plane to create simple shadows. */
            Projected,

            /** Geometry is projected and drawn to the stencil buffer to create volume based shadows. */
            Volume,

            /** Geometry is rendered from the light's perspective into a buffer. */
            Mapped
        }

        export interface BaseShadow<TShadowType extends ShadowType>
        {
            type: TShadowType;
        }

        export type None = BaseShadow<ShadowType.None>;

        export interface Projected extends BaseShadow<ShadowType.Projected>
        {
            /** The plane onto which shadows will be projected. */
            plane: Math.ReadonlyPlane;

            /** The color of the shadows (will be multiplied so don't use alpha). */
            color: Util.Color;

            /** The resolution of the shadow RT (relative, e.g. 1.0 = 100% of viewport size). */
            resolution?: number;
        }

        export type Volume = BaseShadow<ShadowType.Volume>;

        export interface Mapped extends BaseShadow<ShadowType.Mapped>
        {
            /** The resolution of the shadow RT (absolute, e.g. 512 = 512 texels). */
            resolution: number;
        }

        export type Shadow = None | Projected | Volume | Mapped;
    }
}