/// <reference path="ILightSource.ts" />

/* tslint:disable:member-ordering */
/* tslint:disable:no-empty-interface */
namespace RS.Rendering.Scene
{
    /**
     * Encapsulates a directional light source, that points down the local Z axis.
     */
    export interface IDirectionalLightSource extends ILightSource
    {
        
    }

    export interface DirectionalLightSourceConstructor extends Function
    {
        prototype: IDirectionalLightSource;
        new(): IDirectionalLightSource;
        new(color: Util.Color, intensity: number): IDirectionalLightSource;
    }

    export let DirectionalLightSource: DirectionalLightSourceConstructor = null;
}