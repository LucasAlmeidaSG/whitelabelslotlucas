/// <reference path="../textures/ITexture.ts" />

namespace RS.Rendering
{
    export namespace ShaderUniform
    {
        /**
         * Encapsulates a uniform type.
         */
        export enum Type
        {
            /** 2D texture sampler */
            Sampler2D,

            /** 32-bit float */
            Float,

            /** 32-bit signed integer */
            Int,

            /** 32-bit unsigned integer */
            UInt,

            /** True or false */
            Bool,

            /** 2D vector of 32-bit floats */
            Vec2,

            /** 3D vector of 32-bit floats */
            Vec3,

            /** 4D vector of 32-bit floats */
            Vec4,

            /** 2D vector of 32-bit signed integers */
            IVec2,

            /** 3D vector of 32-bit signed integers */
            IVec3,

            /** 4D vector of 32-bit signed integers */
            IVec4,

            /** 2x2 matrix of 32-bit floats */
            Mat2x2,

            /** 2x3 matrix of 32-bit floats */
            Mat2x3,

            /** 3x3 matrix of 32-bit floats */
            Mat3x3,

            /** 3x4 matrix of 32-bit floats */
            Mat3x4,

            /** 4x4 matrix of 32-bit floats */
            Mat4x4,

            /** Array of primitive types */
            Array
        }

        /**
         * Encapsulates an operation, that is, how to combine a uniform with a piece of data.
         * This is useful for uniforms that might depend on, say:
         * - the size of the render texture or backbuffer being drawn to (shaders that use gl_FragCoord)
         * - the size of the main texture being sampled (shaders that sample neighbouring pixels of a texture)
         */
        export enum Op
        {
            /** Don't modify */
            None,

            /** Multiply components of uniform (X1 x X2, Y1 x Y2, Z1 x Z2, W1 x W2) */
            Multiply,

            /** Divide components of uniform (X1 / X2, Y1 / Y2, Z1 / Z2, W1 / W2) */
            Divide,

            /** Discard components of uniform and use provided (X2, Y2, Z2, W2) */
            Replace,

            /** Inverse X coordinate only (X2 - X1, Y1, Z1, W1) */
            InverseX,

            /** Inverse Y coordinate only (X1, Y2 - Y1, Z1, W1) */
            InverseY,

            /** Inverse X coordinate against all components (X2 - X1, X2 - Y1, X2 - Z1, X2 - W1) */
            InverseXAll,

            /** Inverse Y coordinate against all components (Y2 - X1, Y2 - Y1, Y2 - Z1, Y2 - W1) */
            InverseYAll
        }

        export interface Base<TType extends Type, TValue>
        {
            readonly type: TType;
            value: TValue;
            targetSizeOp: Op;
            targetSizeOpRT?: Op;
            inputSizeOp: Op;
            inputSizeOpRT?: Op;
        }

        export type Sampler2D = Base<Type.Sampler2D, ITexture | Asset.ImageReference | null>;
        export type Float = Base<Type.Float, number>;
        export type Int = Base<Type.Int, number>;
        export type UInt = Base<Type.UInt, number>;
        export type Bool = Base<Type.Bool, number>;
        export type Vec2 = Base<Type.Vec2, Math.Vector2D>;
        export type Vec3 = Base<Type.Vec3, Math.Vector3D>;
        export type Vec4 = Base<Type.Vec4, Math.Vector4D>;
        export type IVec2 = Base<Type.IVec2, Math.Vector2D>;
        export type IVec3 = Base<Type.IVec3, Math.Vector3D>;
        export type IVec4 = Base<Type.IVec4, Math.Vector4D>;
        export type Mat2x2 = Base<Type.Mat2x2, [number, number, number, number]>;
        export type Mat2x3 = Base<Type.Mat2x3, [number, number, number, number, number, number]>;
        export type Mat3x3 = Base<Type.Mat3x3, Math.Matrix3>;
        export type Mat3x4 = Base<Type.Mat3x4, [number, number, number, number, number, number, number, number, number, number, number, number]>;
        export type Mat4x4 = Base<Type.Mat4x4, Math.Matrix4>;

        export interface BaseArray<TInnerType extends Type, TValue> extends Base<Type.Array, TValue[]>
        {
            readonly innerType: TInnerType;
            readonly length: number;
        }

        export type Sampler2DArray = BaseArray<Type.Sampler2D, ITexture | null>;
        export type FloatArray = BaseArray<Type.Float, number>;
        export type IntArray = BaseArray<Type.Int, number>;
        export type UIntArray = BaseArray<Type.UInt, number>;
        export type BoolArray = BaseArray<Type.Bool, number>;
        export type Vec2Array = BaseArray<Type.Vec2, Math.Vector2D>;
        export type Vec3Array = BaseArray<Type.Vec3, Math.Vector3D>;
        export type Vec4Array = BaseArray<Type.Vec4, Math.Vector4D>;
        export type IVec2Array = BaseArray<Type.IVec2, Math.Vector2D>;
        export type IVec3Array = BaseArray<Type.IVec3, Math.Vector3D>;
        export type IVec4Array = BaseArray<Type.IVec4, Math.Vector4D>;
        export type Mat2x2Array = BaseArray<Type.Mat2x2, [number, number, number, number]>;
        export type Mat2x3Array = BaseArray<Type.Mat2x3, [number, number, number, number, number, number]>;
        export type Mat3x3Array = BaseArray<Type.Mat3x3, Math.Matrix3>;
        export type Mat3x4Array = BaseArray<Type.Mat3x4, [number, number, number, number, number, number, number, number, number, number, number, number]>;
        export type Mat4x4Array = BaseArray<Type.Mat4x4, Math.Matrix4>;

        /**
         * Encapsulates a description of a uniform.
         */
        export interface Descriptor
        {
            type: Type;
            array?:
            {
                innerType: Type;
                length: number;
            };
        }
    }

    export type ArrayShaderUniform =
        ShaderUniform.Sampler2DArray |
        ShaderUniform.FloatArray |
        ShaderUniform.IntArray |
        ShaderUniform.UIntArray |
        ShaderUniform.BoolArray |
        ShaderUniform.Vec2Array |
        ShaderUniform.Vec3Array |
        ShaderUniform.Vec4Array |
        ShaderUniform.IVec2Array |
        ShaderUniform.IVec3Array |
        ShaderUniform.IVec4Array |
        ShaderUniform.Mat2x2Array |
        ShaderUniform.Mat2x3Array |
        ShaderUniform.Mat3x3Array |
        ShaderUniform.Mat3x4Array |
        ShaderUniform.Mat4x4Array;

    /**
     * Encapsulates the type and state of a shader uniform.
     */
    export type ShaderUniform =
        ShaderUniform.Sampler2D |
        ShaderUniform.Float |
        ShaderUniform.Int |
        ShaderUniform.UInt |
        ShaderUniform.Bool |
        ShaderUniform.Vec2 |
        ShaderUniform.Vec3 |
        ShaderUniform.Vec4 |
        ShaderUniform.IVec2 |
        ShaderUniform.IVec3 |
        ShaderUniform.IVec4 |
        ShaderUniform.Mat2x2 |
        ShaderUniform.Mat2x3 |
        ShaderUniform.Mat3x3 |
        ShaderUniform.Mat3x4 |
        ShaderUniform.Mat4x4 |
        ArrayShaderUniform;
}