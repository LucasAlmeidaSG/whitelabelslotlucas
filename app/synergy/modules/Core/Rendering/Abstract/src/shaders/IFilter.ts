/// <reference path="../shaders/IShader.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a filter that can be applied to any display object.
     */
    export interface IFilter
    {
        /** Gets the underlying shader for this filter. */
        readonly shader: IShader;

        /** Gets or sets whether this filter should automatically match the bounds of the object. */
        autoFit: boolean;

        /** Gets or sets the amount of additional pixels to add to all bounds of the object. */
        padding: number;

        /** Gets or sets the resolution of the filter. */
        resolution: number;

        /** Gets or sets the blend mode of the filter */
        blendMode: BlendMode;
    }

    /**
     * Constructor for IFilter.
     */
    export interface IFilterConstructor extends Function
    {
        prototype: IFilter;
        new(shader: IShader): IFilter;
    }

    export let Filter: IFilterConstructor;
}