/// <reference path="IShaderProgram.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a shader program with a set of uniforms.
     */
    export interface IShader extends IDisposable
    {
        /** Gets or sets the shader program associated with this shader. */
        shaderProgram: IShaderProgram;

        /** Gets all uniforms for this shader. */
        readonly uniforms: Map<ShaderUniform>;
    }

    /**
     * Constructor for IShader.
     */
    export interface IShaderConstructor extends Function
    {
        prototype: IShader;
        new(shaderProgram: IShaderProgram): IShader;
    }

    export let Shader: IShaderConstructor;
}