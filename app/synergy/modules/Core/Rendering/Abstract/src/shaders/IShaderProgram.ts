/// <reference path="ShaderUniform.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a shader program, comprised of a single vertex and fragment shader.
     */
    export interface IShaderProgram
    {
        /** Gets the vertex source for this shader program. */
        readonly vertexSource: string;

        /** Gets the fragment source for this shader program. */
        readonly fragmentSource: string;

        /** Gets a view of all uniforms and their types supported by this shader program. */
        readonly uniforms: Readonly<Map<ShaderUniform.Descriptor>>;

        /** Creates a shader instance from this shader program. */
        createShader(): IShader;

        /** Compiles the shader program on the target stage if it hasn't already been compiled. */
        warmup(stage: IStage): void;
    }

    /**
     * Constructor for IShaderProgram.
     */
    export interface IShaderProgramConstructor extends Function
    {
        prototype: IShaderProgram;
        new(settings: IShaderProgram.Settings): IShaderProgram;
    }

    export let ShaderProgram: IShaderProgramConstructor;

    export namespace IShaderProgram
    {
        export interface Settings
        {
            vertexSource?: string | Asset.Reference<"shader">;
            fragmentSource: string | Asset.Reference<"shader">;
            defines?: Map<string>;
        }
    }
}