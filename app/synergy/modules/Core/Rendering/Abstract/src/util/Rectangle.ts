/// <reference path="ReadonlyRectangle.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a 2D rectangle in a cartesian coordinate space.
     */
    export class Rectangle extends ReadonlyRectangle
    {
        /** Gets or sets the X value of this rectangle. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the Y value of this rectangle. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets or sets the W value of this rectangle. */
        public get w() { return this._w; }
        public set w(value)
        {
            if (this._w === value) { return; }
            this._w = value;
            this.onChanged.publish();
        }

        /** Gets or sets the H value of this rectangle. */
        public get h() { return this._h; }
        public set h(value)
        {
            if (this._h === value) { return; }
            this._h = value;
            this.onChanged.publish();
        }

        /**
         * Gets if rectangles a and b are equal.
         * @param a 
         * @param b 
         */
        public static equals(a: ReadonlyRectangle | null, b: ReadonlyRectangle | null): boolean
        {
            if (a == null) { return b == null; }
            if (b == null) { return a == null; }
            return a.x === b.x && a.y === b.y && a.w === b.w && a.h === b.h;
        }

        /** Sets the one or more of the X, Y, W or H values of this rectangle. */
        public set(newX?: number, newY?: number, newW?: number, newH?: number): this
        {
            if (newX != null) { this._x = newX; }
            if (newY != null) { this._y = newY; }
            if (newW != null) { this._w = newW; }
            if (newH != null) { this._h = newH; }
            this.onChanged.publish();
            return this;
        }

        /** Sets the X, Y, W and H values of this rectangle from the specified rectangle. */
        public copy(from: ReadonlyRectangle): this
        {
            this._x = from.x;
            this._y = from.y;
            this._w = from.w;
            this._h = from.h;
            this.onChanged.publish();
            return this;
        }
    }
}