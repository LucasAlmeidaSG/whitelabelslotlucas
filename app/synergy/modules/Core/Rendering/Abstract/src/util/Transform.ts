namespace RS.Rendering
{
    /**
     * Encapsulates a complete transformation for an object.
     */
    export interface Transform
    {
        position: RS.Math.Vector2D;
        scale: RS.Math.Vector2D;
        pivot: RS.Math.Vector2D;
        anchor: RS.Math.Vector2D;
        skew: RS.Math.Vector2D;
        rotation: number;
    }

    export namespace Transform
    {
        //export function apply(transform: Partial<Transform>, target: IDisplayObject): void
        export type IApply = (transform: Partial<Transform>, target: IDisplayObject) => void;
        /*export interface IApply extends Function
        {
            (transform: Partial<Transform>, target: IDisplayObject): void;
        }*/

        export let apply: IApply;
    }
}