namespace RS.Rendering
{
    export type ReadonlyShape = ReadonlyRectangle | ReadonlyCircle | ReadonlyEllipse | ReadonlyRoundedRectangle | ReadonlyPolygon;

    export type Shape = Rectangle | Circle | Ellipse | RoundedRectangle | Polygon;
}