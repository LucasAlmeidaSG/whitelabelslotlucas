namespace RS.Rendering
{
    /**
     * Encapsulates an immutable 2D rectangle in a cartesian coordinate space.
     */
    export class ReadonlyRectangle
    {
        /** Published when the X, Y, W or H value of this rectangle has changed. */
        public readonly onChanged = createSimpleEvent();//this._onChanged.public;

        protected _x: number;
        protected _y: number;
        protected _w: number;
        protected _h: number;

        //protected readonly _onChanged = createSimpleEvent();

        /** Gets the X value of this rectangle. */
        public get x() { return this._x; }

        /** Gets the Y value of this rectangle. */
        public get y() { return this._y; }

        /** Gets the W value of this rectangle. */
        public get w() { return this._w; }

        /** Gets the H value of this rectangle. */
        public get h() { return this._h; }

        public constructor(x: number = 0, y: number = 0, w: number = 0, h: number = 0)
        {
            this._x = x;
            this._y = y;
            this._w = w;
            this._h = h;
        }

        public toString()
        {
            return `${RS.Math.Vector2D.toString(this)}, ${RS.Math.Size2D.toString(this)}`;
        }

        /** Copies the X and Y values of this point to the specified vector. */
        public copyTo(to: Rectangle): this
        {
            to.copy(this);
            return this;
        }

        /** Creates a writable copy of this rectangle. */
        public clone(): Rectangle
        {
            return new Rectangle(this._x, this._y, this._w, this._h);
        }

        /**
         * Checks whether the given coordinates are contained within this Rectangle
         *
         * @param position - The coordinates of the point to test
         * @return Whether the given coordinates are within this Rectangle
         */
        public contains(position: Math.ReadonlyVector2D): boolean;
        /**
         * Checks whether the x and y coordinates given are contained within this Rectangle
         *
         * @param x - The X coordinate of the point to test
         * @param y - The Y coordinate of the point to test
         * @return Whether the x/y coordinates are within this Rectangle
         */
        public contains(x: number, y: number): boolean;
        public contains(xp1: number | Readonly<Math.Vector2D>, y?: number): boolean
        {
            // resolve x as a number
            let x = xp1;

            // xp1 is a Math.Vector2D
            if (!Is.number(xp1))
            {
                x = xp1.x;
                y = xp1.y;
            }

            if (this.w <= 0 || this.h <= 0)
            {
                return false;
            }

            if (x >= this.x && x < this.x + this.w)
            {
                if (y >= this.y && y < this.y + this.h)
                {
                    return true;
                }
            }

            return false;
        }

        public intersects(rect: ReadonlyRectangle): boolean
        {
            const boundsL = this.x, boundsT = this.y, boundsR = boundsL + this.w, boundsB = boundsT + this.h;
            const rectBoundsL = rect.x, rectBoundsT = rect.y;

            if (rectBoundsL >= boundsR) { return false; }
            if (rectBoundsT >= boundsB) { return false; }

            const rectBoundsR = rectBoundsL + rect.w, rectBoundsB = rectBoundsT + rect.h;
            if (rectBoundsR <= boundsL) { return false; }
            if (rectBoundsB <= boundsT) { return false; }
            
            return true;
        }
    }
}