namespace RS.Rendering
{
    /**
     * Encapsulates an immutable 2D measurement in a cartesian coordinate space.
     */
    export class ReadonlyPoint2D implements Readonly<Math.Vector2D>
    {
        /** Published when the X or Y value of this point has changed. */
        public readonly onChanged = createSimpleEvent();

        protected _x: number;
        protected _y: number;

        //protected readonly _onChanged = createSimpleEvent();

        /** Gets the X value of this point. */
        public get x() { return this._x; }

        /** Gets the Y value of this point. */
        public get y() { return this._y; }

        public constructor(x: number = 0, y: number = 0)
        {
            this._x = x;
            this._y = y;
        }

        /** Copies the X and Y values of this point to the specified vector. */
        public copyTo(to: Math.Vector2D): this
        {
            to.x = this._x;
            to.y = this._y;
            return this;
        }

        /** Creates a copy of this point. */
        public clone(): Point2D
        {
            return new Point2D(this._x, this._y);
        }
    }

    /**
     * Encapsulates a 2D measurement in a cartesian coordinate space.
     */
    export class Point2D extends ReadonlyPoint2D implements Math.Vector2D
    {
        /** Gets or sets the X value of this point. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the Y value of this point. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /**
         * Gets if points a and b are equal.
         * @param a
         * @param b
         */
        public static equals(a: Point2D | null, b: Point2D | null): boolean
        {
            if (a == null) { return b == null; }
            if (b == null) { return a == null; }
            return a.x === b.x && a.y === b.y;
        }

        /** Sets the X and/or Y values of this point. */
        public set(newX?: number, newY?: number): this
        {
            let changed = false;
            if (newX != null && this._x !== newX) { this._x = newX; changed = true; }
            if (newY != null && this._y !== newY) { this._y = newY; changed = true; }
            if (changed) { this.onChanged.publish(); }
            return this;
        }

        /** Sets the X and/or Y values of this point from the specified vector. */
        public copy(from: Math.Vector2D): this
        {
            const changed = this._x !== from.x || this._y !== from.y;
            this._x = from.x;
            this._y = from.y;
            if (changed) { this.onChanged.publish(); }
            return this;
        }
    }
}