namespace RS.Rendering
{
    export interface VideoFrameEvent
    {
        src: Asset.IVideoAsset;
        frame: number;
    }
}