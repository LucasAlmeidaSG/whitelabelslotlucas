namespace RS.Rendering
{
    export import TextAlign = TextOptions.Align;
    export import TextBaseline = TextOptions.Baseline;
}