namespace RS.Rendering
{  
    /**
     * Static list of rendering styles that an IStage can use.
     */
    export enum StageRenderStyle
    {
        WEBGL  = "WEBGL",
        CANVAS = "CANVAS",
        DOM    = "DOM"
    }
}