namespace RS.Rendering
{
    /**
     * Encapsulates an immutable 2D ellipse in a cartesian coordinate space.
     */
    export class ReadonlyEllipse
    {
        /** Published when the X, Y, W or H value of this ellipse has changed. */
        public readonly onChanged = createSimpleEvent(); //this._onChanged.public;

        protected _x: number;
        protected _y: number;
        protected _w: number;
        protected _h: number;

        //protected readonly _onChanged = createSimpleEvent();

        /** Gets the X value of the centre of this ellipse. */
        public get x() { return this._x; }

        /** Gets the Y value of the centre of this ellipse. */
        public get y() { return this._y; }

        /** Gets the W value of this ellipse. */
        public get w() { return this._w; }

        /** Gets the H value of this ellipse. */
        public get h() { return this._h; }

        public constructor(x: number = 0, y: number = 0, w: number = 0, h: number = 0)
        {
            this._x = x;
            this._y = y;
            this._w = w;
            this._h = h;
        }

        /** Copies the X and Y values of this point to the specified vector. */
        public copyTo(to: Ellipse): this
        {
            to.copy(this);
            return this;
        }

        /** Creates a writable copy of this ellipse. */
        public clone(): Ellipse
        {
            return new Ellipse(this._x, this._y, this._w, this._h);
        }

        /**
         * Checks whether the given coordinates are contained within this shape.
         *
         * @param position - The coordinates of the point to test
         * @return Whether the given coordinates are within this Rectangle
         */
        public contains(position: Math.ReadonlyVector2D): boolean;
        /**
         * Checks whether the x and y coordinates given are contained within this shape
         *
         * @param x - The X coordinate of the point to test
         * @param y - The Y coordinate of the point to test
         * @return Whether the x/y coordinates are within this Rectangle
         */
        public contains(x: number, y: number): boolean;
        public contains(p1: number | Readonly<Math.Vector2D>, y?: number): boolean
        {
            let x: number;
            if (RS.Is.number(p1))
            {
                x = p1;
            }
            else
            {
                x = p1.x;
                y = p1.y;
            }

            const wSq = Math.pow(this._w / 2, 2);
            const hSq = Math.pow(this._h / 2, 2);
            const pt = Math.pow(x - this.x, 2) * hSq + Math.pow(y - this.y, 2) * wSq;
            return pt <= wSq * hSq;
        }
    }
}