namespace RS.Rendering
{
    /**
     * Standard cursor types.
     */
    export enum Cursor
    {
        Default = "default",
        None = "none",
        Pointer = "pointer",
        Help = "help",
        Wait = "wait",
        Progress = "progress",
        Crosshair = "crosshair",
        NotAllowed = "not-allowed",
        ZoomIn = "zoom-in",
        ZoomOut = "zoom-out",
        Cell = "cell",
        Text = "text",
        VerticalText = "vertical-text",
        Alias = "alias",
        Copy = "copy",
        Move = "move",
        NoDrop = "no-drop",
        AllScroll = "all-scroll",
        ResizeCol = "col-resize",
        ResizeRow = "row-resize",
        ResizeN = "n-resize",
        ResizeE = "e-resize",
        ResizeS = "s-resize",
        ResizeW = "w-resize",
        ResizeNE = "ne-resize",
        ResizeNW = "nw-resize",
        ResizeSE = "se-resize",
        ResizeSW = "sw-resize",
        ResizeEW = "ew-resize",
        ResizeNS = "ns-resize",
        ResizeNESW = "nesw-resize",
        ResizeNWSE = "nwse-resize",
        Grab = "grab",
        Grabbing = "grabbing"
    }
}