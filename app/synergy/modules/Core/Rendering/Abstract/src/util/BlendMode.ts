namespace RS.Rendering
{
    /** Encapsulates blend mode of an object. */
    export enum BlendMode
    {
        /** Blend source with destination colours based on alpha. Default. */
        Normal,

        /** 
         * Add source and destination colours.
         * If on iOS 14 this will default to Screen
         */
        Add,

        /** Multiply source and destination colours. */
        Multiply,

        /** Blend source with destination colours based on alpha, in a non-premultiplied-alpha context. */
        NormalNPM,

        /** Add source and destination colours, in a non-premultiplied-alpha context. */
        AddNPM,

        Screen
    }
}