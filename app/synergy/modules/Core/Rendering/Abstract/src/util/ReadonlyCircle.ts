namespace RS.Rendering
{
    /**
     * Encapsulates an immutable 2D circle in a cartesian coordinate space.
     */
    export class ReadonlyCircle
    {
        /** Published when the X, Y, or R value of this circle has changed. */
        public readonly onChanged = createSimpleEvent();

        protected _x: number;
        protected _y: number;
        protected _r: number;

        /** Gets the X value of the centre of this circle. */
        public get x() { return this._x; }

        /** Gets the Y value of the centre of this circle. */
        public get y() { return this._y; }

        /** Gets the R value of this circle. */
        public get r() { return this._r; }

        public constructor(x: number = 0, y: number = 0, r: number = 0)
        {
            this._x = x;
            this._y = y;
            this._r = r;
        }

        /** Copies the X and Y values of this point to the specified vector. */
        public copyTo(to: Circle): this
        {
            to.copy(this);
            return this;
        }

        /** Creates a writable copy of this rectangle. */
        public clone(): Circle
        {
            return new Circle(this._x, this._y, this._r);
        }

        /**
         * Checks whether the given coordinates are contained within this shape.
         *
         * @param position - The coordinates of the point to test
         * @return Whether the given coordinates are within this Rectangle
         */
        public contains(position: Math.ReadonlyVector2D): boolean;
        /**
         * Checks whether the x and y coordinates given are contained within this shape
         *
         * @param x - The X coordinate of the point to test
         * @param y - The Y coordinate of the point to test
         * @return Whether the x/y coordinates are within this Rectangle
         */
        public contains(x: number, y: number): boolean;
        public contains(p1: number | Readonly<Math.Vector2D>, y?: number): boolean
        {
            let x: number;
            if (RS.Is.number(p1))
            {
                x = p1;
            }
            else
            {
                x = p1.x;
                y = p1.y;
            }

            const dist = Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2);
            return dist <= Math.pow(this.r, 2);
        }
    }
}