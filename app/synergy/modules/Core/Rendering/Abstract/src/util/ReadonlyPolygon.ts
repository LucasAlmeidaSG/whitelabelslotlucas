namespace RS.Rendering
{
    const tmpLines = [Math.Line2D(), Math.Line2D()];
    const tmpVectors = [Math.Vector2D(), Math.Vector2D(), Math.Vector2D()];

    /**
     * Encapsulates an immutable 2D polygon in a cartesian coordinate space.
     */
    export class ReadonlyPolygon
    {
        /** Published when the Points value of this polygon has changed. */
        public readonly onChanged = createSimpleEvent();

        protected _points: RS.Rendering.Point2D[];

        /** Gets the Points value of this polygon. */
        public get points() { return this._points; }

        public constructor(points: RS.Rendering.Point2D[] = [])
        {
            this._points = points;
        }

        /** Copies the Points values of this point to the specified vector. */
        public copyTo(to: Polygon): this
        {
            to.copy(this);
            return this;
        }

        /** Creates a writable copy of this rectangle. */
        public clone(): Polygon
        {
            return new Polygon(this._points);
        }

        /**
         * Checks whether the given coordinates are contained within this Rectangle
         *
         * @param position - The coordinates of the point to test
         * @return Whether the given coordinates are within this Rectangle
         */
        public contains(position: Math.ReadonlyVector2D): boolean;
        /**
         * Checks whether the x and y coordinates given are contained within this Rectangle
         *
         * @param x - The X coordinate of the point to test
         * @param y - The Y coordinate of the point to test
         * @return Whether the x/y coordinates are within this Rectangle
         */
        public contains(x: number, y: number): boolean;
        public contains(xp1: number | Readonly<Math.Vector2D>, y?: number): boolean
        {
            // resolve x as a number
            let x: number;
            if (!Is.number(xp1))
            {
                // xp1 is a Math.Vector2D
                x = xp1.x;
                y = xp1.y;
            }
            else
            {
                x = xp1;
            }

            const [left, top, right, bottom] = this.calculateBounds();

            // Check within rectangular bounds.
            if (x < left || y < top) { return false; }
            if (x > right || y > bottom) { return false; }

            // Check using intersections.
            const point = tmpVectors[0];
            Math.Vector2D.set(point, x, y);
            for (let i = 0; i < this._points.length; i++)
            {
                // If the point is equal to this point, it's contained, and we can skip everything else.
                if (Math.Vector2D.equals(point, this._points[i])) { return true; }

                // Get line in polygon.
                const lastPoint = i === 0 ? this._points.length - 1 : i - 1;
                const polyLine = Math.Line2D.fromPoints(this._points[lastPoint], this._points[i], tmpLines[0]);

                // Calculate middle of line.
                const midpoint = tmpVectors[1];
                midpoint.x = polyLine.x + polyLine.dx * 0.5;
                midpoint.y = polyLine.y + polyLine.dy * 0.5;

                // Create ray from point to middle of line.
                const ray = Math.Line2D.fromPoints(point, midpoint, tmpLines[1]);

                // Skip parallel lines.
                if (Math.Line2D.isParallel(ray, polyLine)) { continue; }

                // Test intersection against every other line
                let intersections = 1;
                for (let j = 0; j < this._points.length; j++)
                {
                    // Skip line we're starting from.
                    if (j === i) { continue; }

                    const testLastPoint = j === 0 ? this._points.length - 1 : j - 1;
                    const testPolyLine = Math.Line2D.fromPoints(this._points[testLastPoint], this._points[j], tmpLines[0]);
                    
                    // Skip parallel lines.
                    if (Math.Line2D.isParallel(ray, testPolyLine)) { continue; }
                    
                    // Get intersection and skip if it is not on the finite poly line.
                    const intersection = Math.Line2D.intersection(ray, testPolyLine, tmpVectors[1]);

                    // Get lambda from ray to intersection.
                    const lambRayX = (intersection.x - ray.x) / ray.dx;
                    const lambRayY = (intersection.y - ray.y) / ray.dy;
                    // Check intersection is in front of ray.
                    if (lambRayX < 0 || lambRayY < 0) { continue; }

                    // Get lambda from line to intersection.
                    const lambLineX = (intersection.x - testPolyLine.x) / testPolyLine.dx;
                    const lambLineY = (intersection.y - testPolyLine.y) / testPolyLine.dy;
                    // Check intersection is within line length.
                    if (lambLineX < 0 || lambLineX < 0 || lambLineX > 1 || lambLineY > 1) { continue; }

                    intersections++;
                }

                // If it is within the polygon, we'll have an odd number of intersections, otherwise even.
                return (intersections & 1) === 1;
            }

            // We're invalid and may attack at any time.
            return false;
        }

        private calculateBounds()
        {
            let left: number, top: number, right: number, bottom: number;
            for (const point of this.points)
            {
                if (left == null || point.x < left) { left = point.x; }
                if (right == null || point.x > right) { right = point.x; }

                if (top == null || point.y < top) { top = point.y; }
                if (bottom == null || point.y > bottom) { bottom = point.y; }
            }
            return [left, top, right, bottom];
        }
    }
}