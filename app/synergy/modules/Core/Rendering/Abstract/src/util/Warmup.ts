namespace RS.Rendering
{
    /**
     * Uploads the texture for the given image to the rendering context on the specified stage if it hasn't already been uploaded.
     * This eliminates stuttering due to just-in-time texture uploads, but takes time and uses video memory.
     */
    export function warmup(asset: Asset.ImageReference, stage: IStage): void;
    /**
     * Uploads all textures for the given sprite-sheet to the rendering context on the specified stage if they haven't already been uploaded.
     * This eliminates stuttering due to just-in-time texture uploads, but takes time and uses video memory.
     */
    export function warmup(asset: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference, stage: IStage, animation?: string): void;
    /**
     * Uploads all textures for the given spine to the rendering context on the specified stage if they haven't already been uploaded.
     * This eliminates stuttering due to just-in-time texture uploads, but takes time and uses video memory.
     */
    export function warmup(asset: Asset.SpineReference, stage: IStage, animation?: string): void;
    export function warmup(asset: Asset.ImageReference | Asset.SpriteSheetReference | Asset.RSSpriteSheetReference | Asset.SpineReference, stage: IStage, animation?: string): void
    {
        if (asset == null) { Log.error(`[RS.Rendering.warmup] Cannot warmup null asset. Check compiler reference order.`); return; }

        try
        {
            warmupUnsafe(asset, stage, animation);
        }
        catch (err)
        {
            // warmup failure is not fatal so just log it
            Log.error(`[RS.Rendering.warmup] Failed to warmup asset '${asset.id}': ${err}`);
        }
    }

    function warmupUnsafe(asset: Asset.Reference, stage: IStage, animation?: string): void
    {
        const assetContainer = Asset.Store.getAsset(asset);
        if (!assetContainer) { throw new Error("asset not found"); }
        if (!assetContainer.asset) { throw new Error("asset not loaded"); }

        switch (asset.type)
        {
            case "image":
            {
                const container = assetContainer.as(Asset.ImageAsset);
                container.asset.warmup(stage);
                break;
            }

            case "spritesheet":
            {
                const container: Asset.ISpriteSheetAsset<ISpriteSheet> = assetContainer.as(Asset.SpriteSheetAsset);
                container.asset.warmup(stage, animation);
                break;
            }

            case "rsspritesheet":
            {
                const container: Asset.ISpriteSheetAsset<ISpriteSheet> = assetContainer.as(Asset.RSSpriteSheetAsset);
                container.asset.warmup(stage, animation);
                break;
            }

            case "spine":
            {
                const container = assetContainer.as(Asset.SpineAsset);
                container.asset.warmup(stage);
                break;
            }

            default: throw new Error(`asset type '${asset.type}' not supported`);
        }
    }
}