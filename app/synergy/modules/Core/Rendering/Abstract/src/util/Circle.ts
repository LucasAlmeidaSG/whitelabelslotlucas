/// <reference path="ReadonlyCircle.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a 2D circle in a cartesian coordinate space.
     */
    export class Circle extends ReadonlyCircle
    {
        /** Gets or sets the X value of this circle. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the Y value of this circle. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets or sets the R value of this circle. */
        public get r() { return this._r; }
        public set r(value)
        {
            if (this._r === value) { return; }
            this._r = value;
            this.onChanged.publish();
        }

        /**
         * Gets if circles a and b are equal.
         * @param a 
         * @param b 
         */
        public static equals(a: ReadonlyCircle | null, b: ReadonlyCircle | null): boolean
        {
            if (a == null) { return b == null; }
            if (b == null) { return a == null; }
            return a.x === b.x && a.y === b.y && a.r === b.r;
        }

        /** Sets the one or more of the X, Y or R values of this circle. */
        public set(newX?: number, newY?: number, newR?: number): this
        {
            if (newX != null) { this._x = newX; }
            if (newY != null) { this._y = newY; }
            if (newR != null) { this._r = newR; }
            this.onChanged.publish();
            return this;
        }

        /** Sets the X, Y and R values of this circle from the specified circle. */
        public copy(from: ReadonlyCircle): this
        {
            this._x = from.x;
            this._y = from.y;
            this._r = from.r;
            this.onChanged.publish();
            return this;
        }
    }
}