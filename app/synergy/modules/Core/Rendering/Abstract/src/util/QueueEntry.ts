namespace RS.Rendering
{
    export interface QueueEntry
    {
        src: Asset.Reference;
        loop?: boolean;
        callback?: (...args)=>any;
        endCallback?: (...args)=>any;
    }
}