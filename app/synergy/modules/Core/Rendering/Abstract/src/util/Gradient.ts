namespace RS.Rendering
{

    export namespace Gradient
    {

        export enum Type
        {
            Linear,
            Vertical,
            Horizontal,
            Radial
        }

        export interface Stop
        {
            offset: number;
            color: RS.Util.Color;
        }

        export type Stops = (() => Stop[]) | Stop[];

        export interface Base<T extends Type>
        {
            type: T;
            stops: Stops;
            reverse?: boolean;
        }

        export interface Linear extends Base<Type.Linear>
        {
            posA: Math.Vector2D;
            posB: Math.Vector2D;
        }

        export interface Vertical extends Base<Type.Vertical>
        {
        }

        export interface Horizontal extends Base<Type.Horizontal>
        {
        }

        export interface Radial extends Base<Type.Radial>
        {
            pos: Math.Vector2D;
            radius: number;
        }

        export function isStop(obj): obj is Stop
        {
            if (!Is.object(obj)) { return false; }
            if (!Is.number(obj.offset)) { return false; }
            if (!Util.isColorLike(obj.color)) { return false; }
            return true;
        }

        export function isStops(obj): obj is Stops
        {
            return Is.arrayOf(obj, isStop) || Is.func(obj)
        }

        export function isBaseGradient(obj): boolean
        {
            if (!Is.object(obj)) { return false; }
            if (!obj.type) { return false; }
            if (!isStops(obj.stops)) { return false; }
            if (obj.reverse != undefined && !Is.boolean(obj.reverse)) { return false; }
            return true;
        }

        /**
         * Creates a gradient texture from the given texture size and gradient definition.
         * @param width
         * @param height
         * @param gradient
         */
        export let createTexture: (width: number, height: number, gradient: RS.Rendering.Gradient) => ISubTexture;

        /**
         * Creates a simple linear horizontal gradient texture with a sensible size from the given color stops.
         * @param colorStops
         * @param vertical
         */
        export let createSimpleTexture: (colorStops: RS.Rendering.Gradient.Stops, vertical?: boolean) => ISubTexture;

        /**
         * Computes a hash of the specified gradient.
         * @param gradient
         */
        export function hash(gradient: RS.Rendering.Gradient): number
        {
            const arr: number[] = [];
            arr.push(gradient.type);
            switch (gradient.type)
            {
                case Type.Linear:
                    hashVec(gradient.posA, arr);
                    hashVec(gradient.posB, arr);
                    break;
                case Type.Radial:
                    hashVec(gradient.pos, arr);
                    hashFloat(gradient.radius, arr);
                    break;
            }

            let stops = Is.func(gradient.stops) ? gradient.stops() : gradient.stops;
            for (const stop of stops)
            {
                hashStop(stop, arr);
            }

            return Math.hash(arr);
        }

        /**
         * Returns a reversed copy of the given stops array.
         * @param stops stops array (not lazy func)
         */
        export function reverseStops(stops: ReadonlyArray<Stop>): Stop[]
        {
            const out: Stop[] = [];
            for (const stop of stops)
            {
                out.push({ ...stop, offset: 1 - stop.offset });
            }
            return out;
        }

        const maxInt = (1 << 53) - 1;
        function hashStop(stop: Stop, target: number[]): void
        {
            target.push((stop.offset * maxInt)|0);
            target.push((stop.color.r * 255)|0);
            target.push((stop.color.g * 255)|0);
            target.push((stop.color.b * 255)|0);
        }

        function hashFloat(val: number, target: number[]): void
        {
            target.push(val | 0, ((val - (val | 0)) * maxInt) | 0);
        }

        function hashVec(vec: Math.Vector2D, target: number[]): void
        {
            hashFloat(vec.x, target);
            hashFloat(vec.y, target);
        }
    }

    export type Gradient = Gradient.Linear | Gradient.Vertical | Gradient.Horizontal | Gradient.Radial;
}

namespace RS.Is
{
    export function gradient(obj): boolean
    {
        if (!Rendering.Gradient.isBaseGradient(obj)) { return false; }
        switch (obj.type)
        {
            case Rendering.Gradient.Type.Linear:
                return Is.vector2D(obj.posA) && Is.vector2D(obj.posB);
            case Rendering.Gradient.Type.Radial:
                return Is.vector2D(obj.pos) && Is.number(obj.radius);
            case Rendering.Gradient.Type.Vertical:
            case Rendering.Gradient.Type.Horizontal:
                return true;
            default:
                return false;
        }
    }
}
