/// <reference path="ReadonlyRoundedRectangle.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a 2D rounded rectangle in a cartesian coordinate space.
     */
    export class RoundedRectangle extends ReadonlyRoundedRectangle
    {
        /** Gets or sets the X value of this rounded rectangle. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the Y value of this rounded rectangle. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets or sets the W value of this rounded rectangle. */
        public get w() { return this._w; }
        public set w(value)
        {
            if (this._w === value) { return; }
            this._w = value;
            this.onChanged.publish();
        }

        /** Gets or sets the H value of this rounded rectangle. */
        public get h() { return this._h; }
        public set h(value)
        {
            if (this._h === value) { return; }
            this._h = value;
            this.onChanged.publish();
        }

        /** Gets or sets the R value of this rounded rectangle. */
        public get r() { return this._r; }
        public set r(value)
        {
            if (this._r === value) { return; }
            this._r = value;
            this.onChanged.publish();
        }

        /**
         * Gets if rounded rectangles a and b are equal.
         * @param a 
         * @param b 
         */
        public static equals(a: ReadonlyRoundedRectangle | null, b: ReadonlyRoundedRectangle | null): boolean
        {
            if (a == null) { return b == null; }
            if (b == null) { return a == null; }
            return a.x === b.x && a.y === b.y && a.w === b.w && a.h === b.h && a.r === b.r;
        }

        /** Sets the one or more of the X, Y, W, H or R values of this rounded rectangle. */
        public set(newX?: number, newY?: number, newW?: number, newH?: number, newR?: number): this
        {
            if (newX != null) { this._x = newX; }
            if (newY != null) { this._y = newY; }
            if (newW != null) { this._w = newW; }
            if (newH != null) { this._h = newH; }
            if (newR != null) { this._r = newR; }
            this.onChanged.publish();
            return this;
        }

        /** Sets the X, Y, W and H values of this rounded rectangle from the specified rounded rectangle. */
        public copy(from: ReadonlyRoundedRectangle): this
        {
            this._x = from.x;
            this._y = from.y;
            this._w = from.w;
            this._h = from.h;
            this._r = from.r;
            this.onChanged.publish();
            return this;
        }
    }
}