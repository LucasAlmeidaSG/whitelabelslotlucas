namespace RS.Rendering
{
    /**
     * Encapsulates an immutable 2D rounded rectangle in a cartesian coordinate space.
     */
    export class ReadonlyRoundedRectangle
    {
        /** Published when the X, Y, W or H value of this rounded rectangle has changed. */
        public readonly onChanged = createSimpleEvent();//this._onChanged.public;

        protected _x: number;
        protected _y: number;
        protected _w: number;
        protected _h: number;
        protected _r: number;

        //protected readonly _onChanged = createSimpleEvent();

        /** Gets the X value of this rounded rectangle. */
        public get x() { return this._x; }

        /** Gets the Y value of this rounded rectangle. */
        public get y() { return this._y; }

        /** Gets the W value of this rounded rectangle. */
        public get w() { return this._w; }

        /** Gets the H value of this rounded rectangle. */
        public get h() { return this._h; }

        /** Gets the R value of this rounded rectangle. */
        public get r() { return this._r; }

        public constructor(x: number = 0, y: number = 0, w: number = 0, h: number = 0, r: number = 0)
        {
            this._x = x;
            this._y = y;
            this._w = w;
            this._h = h;
            this._r = r;
        }

        /** Copies the X and Y values of this point to the specified vector. */
        public copyTo(to: RoundedRectangle): this
        {
            to.copy(this);
            return this;
        }

        /** Creates a writable copy of this rounded rectangle. */
        public clone(): RoundedRectangle
        {
            return new RoundedRectangle(this._x, this._y, this._w, this._h, this._r);
        }

        /**
         * Checks whether the given coordinates are contained within this Rectangle
         *
         * @param position - The coordinates of the point to test
         * @return Whether the given coordinates are within this Rectangle
         */
        public contains(position: Math.ReadonlyVector2D): boolean;
        /**
         * Checks whether the x and y coordinates given are contained within this Rectangle
         *
         * @param x - The X coordinate of the point to test
         * @param y - The Y coordinate of the point to test
         * @return Whether the x/y coordinates are within this Rectangle
         */
        public contains(x: number, y: number): boolean;
        public contains(xp1: number | Readonly<Math.Vector2D>, y?: number): boolean
        {
            // resolve x as a number
            let x: number;
            if (!Is.number(xp1))
            {
                // xp1 is a Math.Vector2D
                x = xp1.x;
                y = xp1.y;
            }
            else
            {
                x = xp1;
            }

            // rect is some sort of impossirect
            if (this.w <= 0 || this.h <= 0) { return false; }

            // outside rectangular bounds
            if (x < this.x || y < this.y) { return false; }
            if (x > this.x + this.w || y > this.y + this.h) { return false; }

            // Get point position relative to our top left corner.
            const relX = x - this.x;
            const relY = y - this.y;
            // Normalise point to top-left corner because we're symmetrical.
            // If we are ever not symmetrical, everything below will need to change.
            const normX = relX > this.w / 2 ? this.w - relX : relX;
            const normY = relY > this.h / 2 ? this.h - relY : relY;
            if (normX < this.r && normY < this.r)
            {
                // Point is in the corner, get radial distance.
                const dX = this.r - normX;
                const dY = this.r - normY;
                const dist = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
                if (dist > this.r) { return false; }
            }

            return true;
        }
    }
}