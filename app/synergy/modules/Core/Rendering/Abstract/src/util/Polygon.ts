/// <reference path="ReadonlyPolygon.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a 2D polygon in a cartesian coordinate space.
     */
    export class Polygon extends ReadonlyPolygon
    {
        /** Gets or sets the Points value of this polygon. */
        public get points() { return this._points; }
        public set points(value)
        {
            if (this._points === value) { return; }
            this._points = value;
            this.onChanged.publish();
        }

        /**
         * Gets if polygons a and b are equal.
         * @param a 
         * @param b 
         */
        public static equals(a: ReadonlyPolygon | null, b: ReadonlyPolygon | null): boolean
        {
            if (a == null) { return b == null; }
            if (b == null) { return a == null; }
            if (a.points.length !== b.points.length)
            {
                return false;
            }
            for (let p = 0; p < a.points.length; p++)
            {
                if (!RS.Rendering.Point2D.equals(a.points[p], b.points[p]))
                {
                    return false;
                }
            }
            return true;
        }

        /** Sets the one or more of the Points values of this polygon. */
        public set(newPoints?: RS.Rendering.Point2D[]): this
        {
            if (newPoints != null) { this._points = newPoints; }
            this.onChanged.publish();
            return this;
        }

        /** Sets the Points values of this polygon from the specified polygon. */
        public copy(from: ReadonlyPolygon): this
        {
            this._points = from.points;
            this.onChanged.publish();
            return this;
        }
    }
}