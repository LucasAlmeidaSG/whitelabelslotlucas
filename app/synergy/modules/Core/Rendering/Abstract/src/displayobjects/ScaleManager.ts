namespace RS.Rendering
{
    /**
     * @class
     * The ScaleManager stores a scale that is used by all display objects to translate their positions and dimensions on different resolutions
     * The scale value should be set before the inital Stage is created 
     * Each game should be designed at a specific resolution e.g. 1920x1080 and then have a different scale set for different resolutions
     */
    export class ScaleManager
    {
        /**
         * Scale value
         */
        protected static _positionScale: number = 1;
        /**
         * Set the scale for the selected Stage/Asset resolution
         */
        public static setPositionScale(value:number) 
        {
            this._positionScale = value;
        }
        /**
         * Get the scale that has been set
         */
        public static getPositionScale(): number 
        {
            return this._positionScale;
        }
    }
}