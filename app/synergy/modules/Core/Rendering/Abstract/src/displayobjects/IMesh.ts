namespace RS.Rendering
{
    /**
     * Encapsulates a polygonal mesh consisting of vertices that make up triangles.
     */
    export interface IMesh extends IDisplayObject
    {
        /** Gets or sets the tint for this object. */
        tint: Util.Color | null;

        /** Gets or sets the blend mode of this object. */
        blendMode: BlendMode;

        /** Gets or sets the texture for this object. */
        texture: ISubTexture;

        /** Gets or sets the shader for this object. */
        shader: IShader;

        /** Gets the indices array. */
        readonly indices: Int16Array;

        /** Gets the positions array. */
        readonly positions: Float32Array;

        /** Gets the texture coordinate array. */
        readonly texCoords: Float32Array;

        /** Flags the vertices to be updated. */
        markVerticesDirty(): void;
    }

    export namespace IMesh
    {
        export interface Settings
        {
            indices?: Int16Array;
            positions: Float32Array;
            texCoords?: Float32Array;
        }
    }

    /**
     * Constructor for IMesh.
     */
    export interface IMeshConstructor extends Function
    {
        prototype: IMesh;
        new(settings: IMesh.Settings, texture?: ISubTexture): IMesh;
    }

    export let Mesh: IMeshConstructor;
}