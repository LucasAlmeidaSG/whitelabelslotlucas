namespace RS.Rendering
{
    export interface IStage extends IContainer
    {
        readonly onResized: IPrivateEvent<StageEventData>;
        /**
         * Read Only. The width of the stage specified when first created.
         */
        readonly stageWidth: number;
        /**
         * Read Only. The height of the stage specified when first created.
         */
        readonly stageHeight: number;
        /**
         * Whether or not the render() should automatically be called every frame.
         */
        autoRender: boolean;
        /**
         * Canvas that the stage draws to
         */
        readonly canvas: HTMLCanvasElement;
        /**
         * Set the target fps of the game
         */
        fps: number;
        /**
         * whether the stage rendering is paused or not
         */
        paused: boolean;
        /** 
         * Whether input handling is blocked or not.
         * Events captured whilst input handling is blocked will be fired after the block has been lifted.
         * To disable event capture entirely, use interactiveChildren.
         */
        inputBlocked: RS.IArbiter<boolean>;
        /**
         * Gets the current render style used for the stage
         */
        readonly renderStyle: string;
        /**
         * Renders the current display list to the display. This is automatically called if autoRender is set to true.
         */
        render(): void;
        /**
         * Resizes the game canvas size, useful for switching between portrait and landscape
         *
         * @param width The new width of the canvas
         * @param height The new height of the canvas
         */
        resize(width: number, height: number): void;

        /**
         * Capture the canvas into a png image and download it.
         */
        capture(): void;
    }
    export interface IStageConstructor extends Function
    {
        prototype: IStage;
        new(element: HTMLElement, width: number, height: number, renderStyles?: string[], transparent?: boolean): IStage;
    }

    export let Stage: IStageConstructor;

    /**
     * Data for an stage event.
     */
    export interface StageEventData
    {
        stage: IStage;

        width?: number;

        height?: number;
    }
}
