namespace RS.Rendering
{
    const charBlacklist: RegExp = new RegExp('[έ|ύ|υ|ί|ό|ώ|ά|ή|μ|Ύ]');

    export interface IText<TFont = opentype.Font> extends IDisplayObject
    {
        /**
         * Gets the number of lines in this text
         */
        readonly lineCount?: number;

        /**
         * Gets or sets the content of this text.
         */
        text: string;

        /**
         * Gets or sets the font of this text.
         */
        font: TFont;

        /**
         * Gets or sets the font size of this text.
         */
        fontSize: number;

        /**
         * Gets or sets the wrap width of this text.
         */
        wrapWidth: number;

        /**
         * Gets or sets the horizontal alignment for this text.
         * Only applicable for multiline text.
         */
        align: TextOptions.Align;

        /**
         * Gets or sets the tint of this text.
         */
        tint: Util.Color;

        /** Optional string function applied to the input text string before it is rendered. */
        textFilter?: TextOptions.TextFilter;

        scaleToWidth?: number;
        outlineColour?: Util.Color;
        outlineSize?: number;
        anchorX: number;
        anchorY: number;

        /**
         * LEGACY: Gets the local bounds of the text object.
         * Use localBounds instead.
         */
        getLocalBounds(): ReadonlyRectangle;

        /**
         * LEGACY: Gets the complete area occupied by this text.
         * Use localBounds instead.
         */
        calculateTextBounds?(): ReadonlyRectangle;
    }

    export interface ITextConstructor extends Function
    {
        prototype: IText;
        new(settings: TextOptions.Settings, text?: string): IText;
    }

    export let Text: ITextConstructor;

    export namespace TextOptions
    {
        export interface LineData
        {
            text: string;
            width: number;
        }

        export enum Align
        {
            Left,
            Middle,
            Right
        }

        /** Relative position of the text baseline. */
        export enum Baseline
        {
            /** Position the baseline such that the top of the text is at y = 0. Default. */
            Top,
            /** Position the baseline such that the bottom of the text is at y = 0. */
            Bottom,
            /** Position the baseline such that the middle of the text is at y = 0. */
            Middle,
            /**
             * Position the baseline at y = 0.
             * The main part of the text will therefore be at y < 0, whilst hanging tails of letters such as g and y will be at y > 0.
             */
            Alphabetic,
            /** Position the baseline such that the bottom of the text is at y = 0 minus the font underhang size. */
            Hanging
        }

        /** Line-break strategy when wrapping. */
        export enum WrapType
        {
            /** Split words, if needed. Default. */
            Word,
            /** Only split on whitespace. */
            WhiteSpace
        }

        export enum LayerType { Fill, Border }
        export interface BaseLayer<T extends LayerType>
        {
            layerType: T;
            /** Layer offset from the default position in pixels. */
            offset?: Math.Vector2D;
        }
        export interface FillLayer extends BaseLayer<LayerType.Fill> { }
        export interface BorderLayer extends BaseLayer<LayerType.Border>
        {
            /** The radius of the border. */
            size: number;
        }
        export type Layer = FillLayer | BorderLayer;

        export enum FillType { SolidColor, Gradient, Texture }
        export interface Shader<TSettings extends RS.Rendering.IShaderProgram.Settings>
        {
            settings: TSettings;
            class: { new(settings: TSettings): RS.Rendering.IShaderProgram; };
        }
        export interface BaseFill<T extends FillType>
        {
            fillType: T;
            /** Layer shader. */
            shader?: Shader<RS.Rendering.IShaderProgram.Settings>;
        }
        export interface SolidColorFill extends BaseFill<FillType.SolidColor>
        {
            /** Layer color, including opacity as the alpha component. */
            color: RS.Util.Color;
        }
        export interface GradientFill extends BaseFill<FillType.Gradient>
        {
            gradient: Rendering.Gradient;
            /** Number of times to repeat the gradient. */
            repeat?: number;
            /** Layer opacity. */
            alpha?: number;
        }
        export interface TextureFill extends BaseFill<FillType.Texture>
        {
            /** Image to use for the texture. Supports 3 kinds: Bitmap, Spritesheet, and Texture. Choose the type needed by setting the kind variable. */
            image: ImageData;
            /** Transparency of the image. */
            alpha?: number;
        }

        /**
         *  Types of texture that can be passed in.
         *  Currently, bitmap assets, spritesheet assets, and subtextures are supported.
         */
        export enum ImageKind { Bitmap, SpriteFrame, Texture }

        /** Base settings that all image type settings share. */
        export interface BaseSettings<TKind extends ImageKind>
        {
            /** Type of texture resource to use. Currently, bitmap assets, spritesheet assets, and subtextures are supported. */
            kind: TKind;
            /** If true, will automatically scale to fit the entire text. If false, will use the image's normal dimensions and uvPosition and scale settings if set. */
            scaleToFit?: boolean;
            /** Only used if scaleToFit is false. Sets the UV position of the part of the texture being rendered.
             * Defaults to { 0, 0 } in the shader, and has a range of 0-1. */
            uvPosition?: RS.Math.Vector2D;
            /** Only used if scaleToFit is false. Sets the scale of the section of image being rendered.
             * Defaults to 1 in the shader. */
            scale?: number;
            /** If true, will change all alpha for each pixel in the image to be a white background. */
            transparentToWhite?: boolean;
        }

        export interface ImageSettings extends BaseSettings<ImageKind.Bitmap>
        {
            /** Image asset to be used for the texture. */
            asset: Asset.ImageReference;
        }

        export interface SpritesheetFrameSettings extends BaseSettings<ImageKind.SpriteFrame>
        {
            /** Spritesheet asset to get the texture from. */
            asset: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference;
            /** Name of the spritesheet frame, or group of frames, to get the texture from. */
            animationName: string;
            /** Can be used to specify a frame from the animation name to use. If unspecified, defaults to frame 0. */
            frameID?: number;
        }

        export interface TextureSettings extends BaseSettings<ImageKind.Texture>
        {
            /** Subtexture to use to render the text layer. */
            texture: Rendering.ISubTexture;
        }

        export type ImageData = ImageSettings | SpritesheetFrameSettings | TextureSettings;

        export type Fill = SolidColorFill | GradientFill | TextureFill;

        /** Technical strategy to use when rendering text. */
        export enum RenderStrategy
        {
            /**
             * Render the text as a graphics object which may be larger than needed and then scaled down for anti-aliasing.
             * Fast, but typically has high memory usage compared to Canvas.
             * Text size is limited to the maximum allowed texture size on the current device and will be scaled down to fit as needed, at the cost of quality.
             * If the device does not support WebGL, will fall back to Canvas.
             */
            SupersampledGraphics,
            /**
             * Render the text by writing to an HTML5 Canvas and capturing it as a texture.
             * Slow due to the need to manipulate and capture DOM elements, but low on memory usage.
             * Text size is limited to the maximum allowed texture size on the current device and will be scaled down to fit as needed, at the cost of quality.
             */
            Canvas
        }
        export interface BaseRenderSettings<T extends RenderStrategy>
        {
            /**
             * Technical strategy to use when rendering text.
             * See RenderStrategy.
             */
            strategy: T;
        }
        export interface SupersampledGraphicsRenderSettings extends BaseRenderSettings<RenderStrategy.SupersampledGraphics>
        {
            /**
             * Supersampling power, defined as the number of times to double the actual size.
             * Defaults to 1, meaning the size will be doubled once.
             * Higher values are slower and increase video memory usage, but up to a point may provide anti-aliasing benefits.
             */
            power: number;
            /** Texture padding. */
            padding: number;
            /**
             * Canvas fallback settings to use if WebGL is not supported.
             * Defaults to TextOptions.defaultCanvasRenderSettings.
             */
            canvasFallback?: CanvasRenderSettings;
        }
        export interface CanvasRenderSettings extends BaseRenderSettings<RenderStrategy.Canvas>
        {
            /**
             * Texture padding.
             * Used to resolve issues with underhanging text features etc. being cropped from the texture.
             */
            padding: number;
            /**
             * MiterLimit
             * Used to resolve issues with text being too spiky
             */
            miterLimit?: number;
        }
        export type RenderSettings = SupersampledGraphicsRenderSettings | CanvasRenderSettings;

        export type LayerInfo = Layer & Fill;

        /**
         * Settings for text.
         */
        export interface Settings
        {
            /** Defines optional additional styling. */
            layers?: LayerInfo[];
            /** Determines the text's appearance. */
            font: Asset.IFontAsset| Asset.Reference<"font"> | Asset.GenericReference;
            /** Font size, in pixels. */
            fontSize: number;
            /**
             * Width which the text will be broken into lines in order to fit within.
             * See Settings.wrapType.
             */
            wrapWidth?: number;
            /**
             * Defines line-break strategy when automatically wrapping to fit within a set width.
             * Defaults to WrapType.Word, which allows it to break in the middle of words.
             */
            wrapType?: WrapType;
            /**
             * Defines the horizontal alignment of the text.
             * Defaults to Align.Left.
             */
            align?: Align;
            /**
             * Defines baseline position relative to the text pivot.
             * Defaults to Baseline.Top.
             */
            baseline?: Baseline;
            /**
             * Defines the base tint.
             * If there are no layers, this is the text color.
             * Layers are tinted by this color.
             */
            color?: RS.Util.Color;
            /**
             * Technical settings for text rendering.
             * Can be used to vary quality and performance.
             */
            renderSettings?: RenderSettings;

            /** Optional string function applied to the input text string before it is rendered. */
            textFilter?: TextFilter;
            /**
             * Amount to multiply line height by.
             * Defaults to 1.
             */
            lineHeight?: number;

            scaleWidth?: number;
            scaleHeight?: number;
            maxWidth?: number;
        }

        export let defaultRenderSettings: RenderSettings =
        {
            strategy: RenderStrategy.SupersampledGraphics,
            power: 1,
            padding: 2
        };

        export let defaultCanvasRenderSettings: RenderSettings =
        {
            strategy: RenderStrategy.Canvas,
            padding: 2
        };

        /** A string function applied to input text strings before it is rendered. */
        export type TextFilter = (text: string) => string;

        export namespace TextFilter
        {
            export function combine(...filters: TextFilter[]): TextFilter
            {
                return function (value: string): string
                {
                    for (const filter of filters)
                    {
                        value = filter(value);
                    }
                    return value;
                };
            }
        }

        /** String functions that may be applied to input text strings before it is rendered. */
        export namespace TextFilters
        {
            /** Converts text to uppercase. el_GR chars blacklisted */
            export const Uppercase: TextFilter = (text: string) => (text.search(charBlacklist) > -1) ? text : text.toUpperCase();
            /** Converts text to lowercase. */
            export const Lowercase: TextFilter = (text: string) => text.toLowerCase();
            /** Converts text to a single character wide vertical tower. */
            export const Vertical: TextFilter = (text: string) =>
            {
                if (text.length === 0) { return ""; }
                let out = text[0];
                for (let i = 1; i < text.length; i++)
                {
                    const char = text[i];
                    out += `\n${char}`;
                }
                return out;
            };
        }
    }
}
