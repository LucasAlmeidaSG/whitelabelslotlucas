/// <reference path="../assets/ISpriteSheet.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates an animated sprite.
     */
    export interface IAnimatedSprite extends IDisplayObject
    {
        /** Gets the current absolute frame index. */
        readonly currentFrame: number;

        /** Gets the currently playing animation name. */
        readonly currentAnimation: string;

        /** Gets or sets if playback is paused. */
        paused: boolean;

        /** Gets or sets the spritesheet. */
        spriteSheet: ISpriteSheet;

        /** Gets or sets the current frame index relative to the current animation. */
        currentAnimationFrame: number;

        /** Gets or sets the playback rate. */
        framerate: number;

        shader: any;
        tint: Util.Color;

        /** Gets if the current animation looping or not. */
        readonly looping: boolean;

        anchorX: number;
        anchorY: number;

        /** Published when the currently playing animation has finished. */
        readonly onAnimationEnded: IPrivateEvent;

        /** Published when the current frame has changed. */
        readonly onChanged: IPrivateEvent;

        /** Published when a custom animation event has fired. */
        readonly onCustomEvent: IPrivateEvent<string>;

        /** Resumes playback. */
        play(): void;

        /** Suspends playback. */
        stop(): void;

        /**
         * Begins playback of the specified animation.
         * Returns a promise that resolves when the animation completes playback.
         * @param animation
         * @param settings
         */
        gotoAndPlay(animation: string, settings?: IAnimatedSprite.PlaySettings): PromiseLike<void>;

        /**
         * Begins playback from the specified absolute frame index.
         * Returns a promise that resolves when the animation completes playback.
         * @param animation
         * @param settings
         */
        gotoAndPlay(frame: number, settings?: IAnimatedSprite.PlaySettings): PromiseLike<void>;

        /**
         * Seeks to the beginning of the specified animation and suspends playback.
         * @param animation
         */
        gotoAndStop(animation: string): void;

        /**
         * Seeks to the specified absolute frame index and suspends playback.
         * @param animation
         */
        gotoAndStop(frame: number): void;

        /**
         * Advances the playhead by the specified amount.
         * @param time ms
         */
        advance(time: number): void;
    }

    /**
     * Constructor for IAnimatedSprite.
     */
    export interface IAnimatedSpriteConstructor extends Function
    {
        prototype: IAnimatedSprite;
        new(spriteSheet: ISpriteSheet): IAnimatedSprite;
        new(spriteSheet: ISpriteSheet, animation: string): IAnimatedSprite;
        new(spriteSheet: ISpriteSheet, frame: number): IAnimatedSprite;
    }

    export let AnimatedSprite: IAnimatedSpriteConstructor;

    export namespace IAnimatedSprite
    {
        export interface PlaySettings
        {
            reverse?: boolean;
            loop?: boolean;
            yoyo?: boolean;
            fps?: number;
            speed?: number;
            startFrameOffset?: number;
        }
    }
}