/// <reference path="./ISimpleMesh.ts" />
namespace RS.Rendering
{
    /**
     * Encapsulates an animated Rope
     */
    export interface IAnimatedSimpleRope extends ISimpleRope
    {
        /** Get the animated sprite */
        readonly animatedSprite: RS.Rendering.IAnimatedSprite;
    }

    export namespace IAnimatedSimpleRope
    {
        /** Settings used to create an animated rope */
        export interface Settings extends ISimpleRope.Settings
        {
            asset: RS.Asset.RSSpriteSheetReference;
            animation: string;
            playSettings?: Rendering.IAnimatedSprite.PlaySettings;
        }
    }

    /**
     * Constructor for IAnimatedSimpleRope.
     */
    export interface IAnimatedSimpleRopeConstructor extends Function
    {
        prototype: IAnimatedSimpleRope;
        new(settings: IAnimatedSimpleRope.Settings, texture?: ISubTexture): IAnimatedSimpleRope;
    }

    export let AnimatedSimpleRope: IAnimatedSimpleRopeConstructor;
}