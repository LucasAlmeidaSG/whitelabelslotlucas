namespace RS.Rendering
{
    /**
     * Encapsulates a vector-based renderable path.
     */
    export interface IGraphics extends IDisplayObject
    {
        /** Gets or sets the shader to render this graphics using. */
        shader: IShader | null;

        /** Gets or sets the tint for this sprite. */
        tint: Util.Color | null;

        /** Gets or sets the blend mode of this object. */
        blendMode: BlendMode;
        
        /** Gets or sets the current fill alpha. */
        fillAlpha: number;

        /** Sets the current line style. */
        lineStyle(lineWidth?: number, color?: Util.Color, alpha?: number, caps?: string, joints?: string, miterLimit?: number): this;

        /** Moves the cursor. */
        moveTo(x: number, y: number): this;

        /** Moves the cursor. */
        moveTo(pos: Math.Vector2D): this;

        /** Draws a straight line. */
        lineTo(x: number, y: number): this;

        /** Draws a straight line. */
        lineTo(pos: Math.Vector2D): this;

        /** Draws a quadratic curve. */
        quadraticCurveTo(cpX: number, cpY: number, toX: number, toY: number): this;

        /** Draws a quadratic curve. */
        quadraticCurveTo(cp: Math.Vector2D, to: Math.Vector2D): this;

        /** Draws a bezier curve. */
        bezierCurveTo(cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number): this;

        /** Draws a bezier curve. */
        bezierCurveTo(cp1: Math.Vector2D, cp2: Math.Vector2D, to: Math.Vector2D): this;

        /** Draws an arc. */
        arcTo(x1: number, y1: number, x2: number, y2: number, radius: number): this;

        /** Draws an arc. */
        arcTo(pos1: Math.Vector2D, pos2: Math.Vector2D, radius: number): this;

        /** Draws an arc. */
        arc(cx: number, cy: number, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean): this;

        /** Draws an arc. */
        arc(cpos: Math.Vector2D, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean): this;

        /** Begins a fill operation. */
        beginFill(color: Util.Color, alpha?: number): this;

        /** Ends a fill operation. */
        endFill(): this;

        /** Draws a rectangle. */
        drawRect(x: number, y: number, width: number, height: number): this;

        /** Draws a rectangle. */
        drawRect(pos: Math.Vector2D, size: Math.Size2D): this;

        /** Draws a rectangle. */
        drawRect(rect: Rectangle): this;

        /** Draws a rounded rectangle. */
        drawRoundedRect(x: number, y: number, width: number, height: number, radius: number): this;

        /** Draws a rounded rectangle. */
        drawRoundedRect(pos: Math.Vector2D, size: Math.Size2D, radius: number): this;

        /** Draws a rounded rectangle. */
        drawRoundedRect(rect: Rectangle, radius: number): this;

        /** Draws a circle. */
        drawCircle(x: number, y: number, radius: number): this;

        /** Draws a circle. */
        drawCircle(circle: Circle): this;

        /** Draws a circle. */
        drawCircle(pos: Math.Vector2D, radius: number): this;

        /** Draws an ellipse. */
        drawEllipse(x: number, y: number, width: number, height: number): this;

        /** Draws an ellipse. */
        drawEllipse(pos: Math.Vector2D, size: Math.Size2D): this;

        /** Draws an ellipse. */
        drawEllipse(rect: Rectangle): this;

        /** Draws an arbitrary polygon. */
        drawPolygon(path: Math.Vector2D[]): this;

        /** Draws an arbitrary polygon. */
        drawPolygon(path: Math.Vector2DList): this;

        /** Draws an arbitrary shape. */
        drawShape(shape: ReadonlyShape): this;

        /** Clears all draws. */
        clear(): this;

        /** Gets if the path in this graphics object contains the specified point. */
        containsPoint(x: number, y: number): boolean;

        /** Gets if the path in this graphics object contains the specified point. */
        containsPoint(pos: Math.Vector2D): boolean;

        /** Closes the current path and starts a new one. */
        closePath(): this;

        /** Specifies the last closed path as a "hole". */
        addHole(): this;
    }

    /**
     * Constructor for IGraphics.
     */
    export interface IGraphicsConstructor extends Function
    {
        prototype: IGraphics;
        new(): IGraphics;
    }

    export let Graphics: IGraphicsConstructor;
}