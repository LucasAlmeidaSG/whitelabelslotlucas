namespace RS.Rendering
{
    export interface IBitmapText extends IDisplayObject, IText<string>
    {

    }

    export interface IBitmapTextConstructor extends Function
    {
        prototype: IBitmapText;
        new(asset: Asset.IBitmapFontAsset, text?: string): IBitmapText;
    }

    export let BitmapText: IBitmapTextConstructor;
}
