/// <reference path="./ISimpleMesh.ts" />
namespace RS.Rendering
{
    /**
     * Encapsulates a polygonal Rope consisting of vertices that make up triangles.
     */
    export interface ISimpleRope extends ISimpleMesh
    {
        /** Gets or sets the positions array. */
        points: RS.Math.Vector2D[];

        // Re-calculate vertices by rope points each frame
        autoUpdate: boolean;

        /**
         * Refreshes uvs for generated meshes (rope, plane)
         * sometimes refreshes vertices too
         *
         * @param {boolean} [forceUpdate=false] if true, matrices will be updated any case
         */
        refresh(forceUpdate: boolean);

        /**
         * Update the rope points to match a path
         *
         * This can be used along with a BezierTween
         * to update the rope as it travels along a path
         * onChange: () => this.bendWhileMoving)
         *
         * @param path the patch to match
         * @param container container to local of the points coordinates system
         */
        recalculatePointsForPath(path: RS.Bezier.Path, container?: RS.Rendering.IDisplayObject): void;
    }

    export namespace ISimpleRope
    {
        // Settings used to create a SimpleRope
        export interface Settings
        {
            // Used to calculate the vertices
            points: RS.Math.Vector2D[];
        }
    }

    /**
     * Constructor for ISimpleRope.
     */
    export interface ISimpleRopeConstructor extends Function
    {
        prototype: ISimpleRope;
        new(settings: ISimpleRope.Settings, texture?: ISubTexture): ISimpleRope;
    }

    export let SimpleRope: ISimpleRopeConstructor;
}
