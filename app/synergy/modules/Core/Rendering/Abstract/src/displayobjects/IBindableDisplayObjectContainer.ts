/// <reference path="IContainer.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates a bindable display object container.
     */
    export type IBindableDisplayObjectContainer<TSettings = void, TRuntimeData = void> = IContainer

    /**
     * Constructor for IBindableDisplayObjectContainer.
     */
    export interface IBindableDisplayObjectContainerConstructor extends Function
    {
        prototype: IContainer;
        new <TSettings = void, TRuntimeData = void>(settings: TSettings, runtimeData: TRuntimeData): IBindableDisplayObjectContainer<TSettings, TRuntimeData>;
    }

    export let BindableDisplayObjectContainer: IBindableDisplayObjectContainerConstructor;
}
