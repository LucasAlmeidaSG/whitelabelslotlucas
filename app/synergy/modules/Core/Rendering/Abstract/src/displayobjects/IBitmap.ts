namespace RS.Rendering
{
    /**
     * The IBitmap interface defines properties and methods for using bitmap images.
     */
    export interface IBitmap extends IContainer
    {
        /** Gets or sets the sub-texture for this bitmap. */
        texture: ISubTexture;

        /** Gets or sets the shader to render this bitmap using. */
        shader: IShader | null;

        /** Gets or sets the tint for this bitmap. */
        tint: Util.Color;

        /** Gets or sets the X anchor for the bitmap (0-1). */
        anchorX: number;

        /** Gets or sets the Y anchor for the bitmap (0-1). */
        anchorY: number;
    }

    /**
     * Constructor for IBitmap.
     */
    export interface IBitmapConstructor extends Function
    {
        prototype: IBitmap;
        new(frame: Frame): IBitmap;
        new(asset: Asset.IImageAsset): IBitmap;
        new(texture: ISubTexture): IBitmap;
    }

    export let Bitmap: IBitmapConstructor;
}
