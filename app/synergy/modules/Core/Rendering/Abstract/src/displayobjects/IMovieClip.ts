namespace RS.Rendering
{
    /**
     * The IMovieClip<T> interface defines properties and methods for using multi frame animated images.
     */
    export interface IMovieClip extends IContainer
    {
        /** Published when the currently playing animation has finished. */
        readonly onAnimationEnded: IEvent;

        /** Published when the current frame has changed. */
        readonly onChanged: IEvent;
        /**
         * The currently displayed frame of the animation.
         */
        readonly currentFrame: number;
        /**
         * The total number of frames in the animation.
         */
        readonly totalFrames: number;

        readonly paused: boolean;
        
        shader: any;
        
        tint: Util.Color;
        /**
         * Frames to display per second
         */
        fps: number;
        /**
         * Play the animation from the current frame until the last frame is displayed.
         */
        play(): void;
        /**
         * Stop the animation at the currently displayed frame.
         */
        stop(): void;
        /**
         * Stop the animation at the specified frame.
         * @param frame Frame to display.
         */
        gotoAndStop(frame: number): void;
        /**
         * Play the animation from the specified frame until the last frame is displayed.
         * @param frame Frame to start playing from.
         */
        gotoAndPlay(frame: number): void;
        /**
         * Create a new instance of the MovieClip with the same assets
         */
        clone(): IMovieClip;
    }
    export interface IMovieClipConstructor extends Function
    {
        prototype: IMovieClip;
        new(frame: Frame[], fps: number): IMovieClip;
        new(asset: Asset.IImageAsset[], fps: number): IMovieClip;
    }

    export let MovieClip: IMovieClipConstructor;
}
