namespace RS.Rendering
{
    /**
     * Encapsulates a polygonal mesh consisting of vertices that make up triangles.
     */
    export interface ISimpleMesh extends IDisplayObject
    {
        /** Gets or sets the tint for this object. */
        tint: Util.Color | null;

        /** Gets or sets the blend mode of this object. */
        blendMode: BlendMode;

        /** Gets or sets the texture for this object. */
        texture: ISubTexture;

        /** Gets or sets the shader for this object. */
        shader: IShader;

        /** Gets or sets the indices array. */
        indices: Uint16Array;

        /** Gets or sets the positions array. */
        positions: Float32Array;

        /** Gets the texture coordinate array. */
        texCoords: Float32Array;
    }

    export namespace ISimpleMesh
    {
        // The way the Mesh should be drawn
        export enum DrawMode
        {
            // PIXI.mesh.Mesh.DRAW_MODES.TRIANGLES
            Triangles,
            // PIXI.mesh.Mesh.DRAW_MODES.TRIANGLE_MESH
            TriangleMesh
        }

        // Settings used to create a SimpleMesh
        export interface Settings
        {
            // Created Indices
            indices?: Int16Array;
            // Used to calculate the vertices
            positions: Float32Array;
            // Used to calculate the uv coordinates
            texCoords?: Float32Array;
            // The way the Mesh should be drawn, can be any of the DrawMode constants
            drawMode?: DrawMode;
        }
    }

    /**
     * Constructor for ISimpleMesh.
     */
    export interface ISimpleMeshConstructor extends Function
    {
        prototype: ISimpleMesh;
        new(settings: ISimpleMesh.Settings, texture?: ISubTexture): ISimpleMesh;
    }

    export let SimpleMesh: ISimpleMeshConstructor;
}