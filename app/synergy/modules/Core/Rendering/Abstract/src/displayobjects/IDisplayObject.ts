namespace RS.Rendering
{
    /**
     * Encapsulates an abstract display object.
     */
    export interface IDisplayObject extends IDisposable
    {
        /** Published when the parent of this display object has changed. */
        readonly onParentChanged:  IPrivateEvent<IDisplayObject>;

        /** Published when the stage of this display object has changed. */
        readonly onStageChanged:   IPrivateEvent<IStage>;

        /** Published when this display object has been updated. */
        readonly onUpdated:        IPrivateEvent<IDisplayObject>;

        readonly onRightClicked:   IPrivateEvent<InteractionEventData>;
        readonly onRightDown:      IPrivateEvent<InteractionEventData>;
        readonly onRightUp:        IPrivateEvent<InteractionEventData>;
        readonly onRightUpOutside: IPrivateEvent<InteractionEventData>;

        readonly onClicked:        IPrivateEvent<InteractionEventData>;
        readonly onClickCancelled: IPrivateEvent<InteractionEventData>;
        readonly onDown:           IPrivateEvent<InteractionEventData>;
        readonly onMoved:          IPrivateEvent<InteractionEventData>;
        readonly onOut:            IPrivateEvent<InteractionEventData>;
        readonly onOver:           IPrivateEvent<InteractionEventData>;
        readonly onUp:             IPrivateEvent<InteractionEventData>;
        readonly onUpOutside:      IPrivateEvent<InteractionEventData>;
        readonly onPreRender:      IPrivateEvent<RenderEventData>;
        readonly onPostRender:     IPrivateEvent<RenderEventData>;

        /** Gets or sets the name of this display object. Useful for debugging. */
        name: string | null;
        /** The x coordinate relative to the local coordinates of the parent IDisplayObject<T>. Equivalent to object.position.x. */
        x: number;
        /** The y coordinate relative to the local coordinates of the parent IDisplayObject<T>. Equivalent to object.position.y. */
        y: number;
        /** Location of this object in parent space. */
        readonly position: Point2D;
        /** Scale factor of this object's size. */
        readonly scale: Point2D;
        /**
         * Gets or sets the width of this object in global coordinate space.
         *
         * If set, will apply a scale along the X axis to achieve the desired width.
         *
         * NOTE: setting this property attempts to match using local width, not global width!
         */
        width: number;
        /**
         * @description Gets or sets the height of this object in global parent coordinate space.
         *
         * If set, will apply a scale along the Y axis to achieve the desired height.
         *
         * NOTE: setting this property attempts to match using local height, not global height!
         */
        height: number;
        /** The horizontal scale factor applied from the registration point. With a value of -1 being -100% and 1 being 100%. Equivalent to object.scale.x. */
        scaleX: number;
        /** The vertical scale factor applied from the registration point. With a value of -1 being -100% and 1 being 100%. Equivalent to object.scale.y. */
        scaleY: number;
        /**
         * The absolute x scale of this object in canvas space, including stage scale.
         *
         * Mostly for use by shaders and anything else in canvas space.
         *
         * Use toLocalScale() to get the scale of one object relative to another.
         */
        readonly worldScaleX: number;
        /**
         * The absolute y scale of this object in canvas space, including stage scale.
         * Mostly for use by shaders and anything else in canvas space.
         * Use toLocalScale() to get the scale of one object relative to another.
         */
        readonly worldScaleY: number;
        /** The opacity (transparency) multiplier, from 0.0 to 1.0. */
        alpha: number;
        /** Rotation offset in radians, from its original orientation. */
        rotation: number;
        /** Rotation offset in degrees, from its original orientation. */
        rotationDegrees: number;
        /** Gets or sets if this object is visible or not. */
        visible: boolean;
        /** Gets or sets if this object responds to interaction events. */
        interactive: boolean;
        /** Gets or sets the mask of this object. May be null (no mask). */
        mask: Shape | IGraphics | IMovieClip | IBitmap | null;
        /** Gets or sets the cursor style to use when this object is hovered over. */
        cursor: Cursor;
        /** Gets or sets the hit area for this object. */
        hitArea: ReadonlyShape | null;
        /** Gets or sets the filter area for this object. Defaults to object bounds. */
        filterArea: ReadonlyRectangle | null;
        /** Gets or sets the parent object of this display object, or null if it doesn't have one. */
        parent: IContainer | null;
        /** Gets or sets the stage of this display object, or null if it doesn't have one. */
        stage: IStage | null;
        /** If true, displays a mouse hand cursor on roll-over. Equivalent to object.cursor === Cursor.Pointer. */
        buttonMode: boolean;

        /** Skew transformation. X is the angle offset of a vertical line in radians, whilst Y is the angle offset of a horizontal line. Defaults to 0, 0. */
        readonly skew: Point2D;
        /** The origin of this object in local space i.e. relative to this object. */
        readonly pivot: Point2D;
        /** Gets the native object that this display object wraps. */
        readonly nativeObject: any;
        /** Gets the root parent of this display object i.e. the highest parent in the current hierarchy. */
        readonly rootParent: IDisplayObject | null;
        /** The filters to apply to this object when rendering. */
        filters: IFilter[];
        /** Object bounds in world space. */
        readonly bounds: ReadonlyRectangle;
        /** Object bounds in local space. */
        readonly localBounds: ReadonlyRectangle;

        blendMode: BlendMode;

        /** Sets the transform of this object. */
        setTransform(x?: number, y?: number, scaleX?: number, scaleY?: number, rotation?: number, skewX?: number, skewY?: number, pivotX?: number, pivotY?: number): this;
        /**
         * Converts a point from this object's local coordinate space into global coordinate space.
         * @param absolute If true, returns the point relative to the canvas, rather than the stage. Defaults to false.
         */
        toGlobal(localPt: Math.Vector2D, out?: Math.Vector2D, absolute?: boolean): Math.Vector2D;

        /** Converts a point from another object's local coordinate space into this object's local coordinate space. */
        toLocal(fromPt: Math.Vector2D, from: IDisplayObject, out?: Math.Vector2D): Math.Vector2D;
        /**
         * Converts a point from global coordinate space into this object's local coordinate space.
         * @param absolute If true, returns the point relative to the canvas, rather than the stage. Defaults to false.
         */
        toLocal(globalPt: Math.Vector2D, out?: Math.Vector2D, absolute?: boolean): Math.Vector2D;

        /** Returns the scale of the given display object relative to this display object. */
        toLocalScale(object: IDisplayObject, out?: Math.Vector2D): Math.Vector2D;

        onRemoved(): void;

        onAdded(): void;

        /** @deprecated Pre/post rendering is enabled automatically when onPreRender/onPostRender is listened to. */
        enablePrePostRendering(): void;

        /**
         * Renders this display object to the given render texture.
         * The resulting sub-texture will be at the position specified by its position property.
         * It will be aligned to its top-left corner, regardless of pivot or anchor.
         * Does not clear the texture.
         * @param texture     Texture to render to. Will not be cleared.
         * @param allowResize If the texture is too small, allows it to be resized. Defaults to false.
         * @param stage       Stage context in which to render this object. Only needed if object has no parent.
         */
        renderToTexture(texture: IRenderTexture, allowResize?: boolean, stage?: IStage): ISubTexture;
        /**
         * Renders this display object to a render texture.
         * @param stage      The stage context in which to render this object. Only needed if object has no parent.
         * @param resolution The scale of the texture in video memory. Only affects quality, not apparent size.
         */
        renderToTexture(stage?: IStage, resolution?: number): ISubTexture;
    }

    /** Tracks creation and disposal of display objects. */
    export let DisplayObjectTracker: Util.IObjectTracker<IDisplayObject>;

    export interface IDisplayObjectConstructor
    {
        prototype: IDisplayObject;
        new(): IDisplayObject;
    }

    export let DisplayObject: IDisplayObjectConstructor;

    /**
     * Data for an interaction event.
     */
    export interface InteractionEventData
    {
        /** The object interacted with. */
        target: IDisplayObject;

        /** The position of the event in global coordinate space. */
        globalPosition: Math.Vector2D;

        /** The position of the event in the object's local coordinate space. */
        localPosition: Math.Vector2D;

        /** A unique identifier for the pointer. */
        pointerID: number;
    }

    export interface RenderEventData
    {
        /** The object interacted with. */
        target: IDisplayObject;

        canvas: HTMLCanvasElement;

        customProperties?: any;
    }
}