namespace RS.Rendering
{
    export type BaseSettings = object;
    export type BaseRuntimeData = object;

    /**
     * Allows creation of a particular element with predetermined settings.
     */
    export interface IBoundElement<TElement extends IBindableDisplayObjectContainer<TSettings>, TSettings extends BaseSettings>
    {
        readonly settings: TSettings;
        create(): TElement;
        create(overrideSettings: Partial<TSettings>): TElement;
        create(parent: Rendering.IContainer): TElement;
        create(overrideSettings: Partial<TSettings>, parent: Rendering.IContainer): TElement;
    }

    /**
     * Allows creation of a particular element with predetermined settings.
     */
    export interface IBoundElementNeedsRuntimeData<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData>
    {
        readonly settings: TSettings;
        create(runtimeData: TRuntimeData): TElement;
        create(overrideSettings: Partial<TSettings>, runtimeData: TRuntimeData): TElement;
        create(runtimeData: TRuntimeData, parent: Rendering.IContainer): TElement;
        create(overrideSettings: Partial<TSettings>, runtimeData: TRuntimeData, parent: Rendering.IContainer): TElement;
    }

    export type ILooseBoundElement<TElement extends IBindableDisplayObjectContainer<any, void>> = IBoundElement<TElement, any>;
    export type ILooseBoundElementNeedsRuntimeData<TElement extends IBindableDisplayObjectContainer<any, any>> = IBoundElementNeedsRuntimeData<TElement, any, any>;

    export type IGenericBoundElement = IBoundElement<IBindableDisplayObjectContainer, any>;
    export type IGenericBoundElementNeedsRuntimeData = IBoundElementNeedsRuntimeData<IBindableDisplayObjectContainer, any, any>;

    /**
     * Allows creation of a particular element.
     */
    export interface IElementFactory<TElement extends IBindableDisplayObjectContainer<TSettings>, TSettings extends BaseSettings>
    {
        create(settings: TSettings): TElement;
        create(settings: TSettings, parent: Rendering.IContainer): TElement;
        bind(settings: TSettings): IBoundElement<TElement, TSettings>;
    }

    /**
     * Allows creation of a particular element.
     */
    export interface IElementFactoryNeedsRuntimeData<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData>
    {
        create(settings: TSettings, runtimeData: TRuntimeData): TElement;
        create(settings: TSettings, runtimeData: TRuntimeData, parent: Rendering.IContainer): TElement;
        bind(settings: TSettings): IBoundElementNeedsRuntimeData<TElement, TSettings, TRuntimeData>;
    }

    class BoundElement<TElement extends IBindableDisplayObjectContainer<TSettings>, TSettings extends BaseSettings> implements IBoundElement<TElement, TSettings>
    {
        public constructor(public readonly ctor: { new(settings: TSettings): TElement }, public readonly settings: TSettings) { }

        public create(p1?: Rendering.IContainer|Partial<TSettings>, p2?: Rendering.IContainer): TElement
        {
            const parent = p1 instanceof Rendering.DisplayObject ? p1 : p2;
            const overrideSettings = p1 instanceof Rendering.DisplayObject ? null : p1;

            const settings = { ...(this.settings as object), ...(overrideSettings as object || {}) } as TSettings;

            const el = new this.ctor(settings);
            if (parent != null) { (parent as Rendering.IContainer).addChild(el); }
            return el;
        }
    }

    class BoundElementNeedsRuntimeData<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData> implements IBoundElementNeedsRuntimeData<TElement, TSettings, TRuntimeData>
    {
        public constructor(public readonly ctor: { new(settings: TSettings, runtimeData: TRuntimeData): TElement }, public readonly settings: TSettings) { }

        public create(p1: Partial<TSettings>|TRuntimeData, p2?: Rendering.IContainer|TRuntimeData, p3?: Rendering.IContainer): TElement
        {
            const parent = p2 instanceof Rendering.DisplayObject ? p2 : p3;
            const overrideSettings = (p2 != null && p2 !== parent) ? (p1 as Partial<TSettings>) : null;
            const runtimeData = ((p2 == null || p2 === parent) ? p1 : p2) as TRuntimeData;

            const settings = { ...(this.settings as object), ...(overrideSettings as object || {}) } as TSettings;

            const el = new this.ctor(settings, runtimeData);
            if (parent != null) { (parent as Rendering.IContainer).addChild(el); }
            return el;
        }
    }

    class ElementFactory<TElement extends IBindableDisplayObjectContainer<TSettings>, TSettings extends BaseSettings> implements IElementFactory<TElement, TSettings>
    {
        public constructor(public readonly ctor: { new(settings: TSettings): TElement }, public readonly defaultSettings?: TSettings) { }

        public create(settings: TSettings, parent?: Rendering.IContainer): TElement
        {
            if (this.defaultSettings)
            {
                const newSettings = Object.create(this.defaultSettings) as TSettings;
                Util.assign(settings, newSettings, false);
                settings = newSettings;
            }
            const el = new this.ctor(settings);
            if (parent != null) { parent.addChild(el); }
            return el;
        }

        public bind(settings: TSettings): IBoundElement<TElement, TSettings>
        {
            return new BoundElement(this.ctor, settings);
        }
    }

    class ElementFactoryNeedsRuntimeData<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData> implements IElementFactoryNeedsRuntimeData<TElement, TSettings, TRuntimeData>
    {
        public constructor(public readonly ctor: { new(settings: TSettings, runtimeData: TRuntimeData): TElement }, public readonly defaultSettings?: TSettings) { }

        public create(settings: TSettings, runtimeData: TRuntimeData, parent?: Rendering.IContainer): TElement
        {
            if (this.defaultSettings)
            {
                const newSettings = Object.create(this.defaultSettings) as TSettings;
                Util.assign(settings, newSettings, false);
                settings = newSettings;
            }
            const el = new this.ctor(settings, runtimeData);
            if (parent != null) { parent.addChild(el); }
            return el;
        }

        public bind(settings: TSettings): IBoundElementNeedsRuntimeData<TElement, TSettings, TRuntimeData>
        {
            return new BoundElementNeedsRuntimeData(this.ctor, settings);
        }
    }

    /**
     * Declares a type of element with no default settings and no runtime data.
     * @param ctor
     */
    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends void>(ctor: { new(settings: TSettings): TElement }): IElementFactory<TElement, TSettings>;

    /**
     * Declares a type of element with default settings and no runtime data.
     * @param ctor
     */
    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends void>(ctor: { new(settings: TSettings): TElement }, defaultSettings: TSettings): IElementFactory<TElement, TSettings>;

    /**
     * Declares a type of element with no default settings and no runtime data.
     * @param ctor
     */
    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends void>(ctor: { new(settings: TSettings): TElement }, requiresRuntimeData: false): IElementFactory<TElement, TSettings>;

    /**
     * Declares a type of element with default settings and no runtime data.
     * @param ctor
     */
    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends void>(ctor: { new(settings: TSettings): TElement }, requiresRuntimeData: false, defaultSettings: TSettings): IElementFactory<TElement, TSettings>;

    /**
     * Declares a type of element with no default settings and runtime data.
     * @param ctor
     */
    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData>(ctor: { new(settings: TSettings, runtimeData: TRuntimeData): TElement }, requiresRuntimeData: true): IElementFactoryNeedsRuntimeData<TElement, TSettings, TRuntimeData>;

    /**
     * Declares a type of element with default settings and runtime data.
     * @param ctor
     */
    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData>(ctor: { new(settings: TSettings, runtimeData: TRuntimeData): TElement }, requiresRuntimeData: true, defaultSettings: TSettings): IElementFactoryNeedsRuntimeData<TElement, TSettings, TRuntimeData>;

    export function declareElement<TElement extends IBindableDisplayObjectContainer<TSettings, TRuntimeData>, TSettings extends BaseSettings, TRuntimeData extends BaseRuntimeData>(ctor: { new(settings: TSettings, runtimeData?: TRuntimeData): TElement }, p2?: TSettings | boolean, p3?: TSettings): IElementFactory<any, TSettings> | IElementFactoryNeedsRuntimeData<TElement, TSettings, TRuntimeData>
    {
        const requiresRuntimeData = Is.boolean(p2) ? p2 : false;
        const defaultSettings = Is.boolean(p2) ? p3 : p2;
        return (requiresRuntimeData
            ? new ElementFactoryNeedsRuntimeData<any, any, any>(ctor, defaultSettings)
            : new ElementFactory<any, any>(ctor as any, defaultSettings)) as any;
    }
}