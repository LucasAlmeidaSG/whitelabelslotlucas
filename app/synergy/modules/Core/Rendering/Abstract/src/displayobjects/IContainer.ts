/// <reference path="IDisplayObject.ts" />
/// <reference path="IBaseContainer.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates an abstract display object container.
     */
    export interface IContainer<T extends IDisplayObject | IFastBitmap = IDisplayObject> extends IBaseContainer<T>
    {
        simulateMouse?(type: MouseEventType, position: Math.Vector2D): void;
    }

    export enum MouseEventType
    {
        Up,
        Down,
        Moved,
        Click
    }

    /**
     * Constructor for IContainer.
     */
    export interface IContainerConstructor extends Function
    {
        prototype: IContainer;
        new <T extends IDisplayObject | IFastBitmap = IDisplayObject>(): IContainer<T>;
        new <T extends IDisplayObject | IFastBitmap = IDisplayObject>(name: string): IContainer<T>;
    }

    export let DisplayObjectContainer: IContainerConstructor;
    export let Container: IContainerConstructor;

    // Setup property for Container to just return DisplayObjectContainer
    Object.defineProperties(RS.Rendering,
    {
        Container:
        {
            get: function (this: typeof RS.Rendering)
            {
                return this.DisplayObjectContainer;
            },
            set: function (this: typeof RS.Rendering, value: IContainerConstructor)
            {
                this.DisplayObjectContainer = value;
            },
            enumerable: true,
            configurable: true
        }
    });
}
