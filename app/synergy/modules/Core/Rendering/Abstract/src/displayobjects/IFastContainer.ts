/// <reference path="IContainer.ts" />
/// <reference path="IFastBitmap.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates an abstract display object container that uses a faster rendering route but is less flexible.
     */
    export interface IFastContainer<T extends IBitmap | IAnimatedSprite | IFastBitmap = IFastBitmap> extends IBaseContainer<T>
    {

    }

    export namespace IFastContainer
    {
        export interface Settings
        {
            /** Whether or not children of the fast container should support tint AND alpha. */
            supportColor: boolean;

            /** Whether or not children of the fast container should support rotation. */
            supportRotation: boolean;

            /** Whether or not children of the fast container should support SubTextures. */
            supportUVs: boolean;

            /** Number of children to render per batch. */
            batchSize: number;
        }

        export const defaultSettings: Settings =
        {
            supportColor: true,
            supportRotation: true,
            supportUVs: true,
            batchSize: 4096
        };
    }

    /**
     * Constructor for IFastContainer.
     */
    export interface IFastContainerConstructor extends Function
    {
        prototype: IFastContainer;
        new(settings: Partial<IFastContainer.Settings>): IFastContainer;
        new(name: string): IFastContainer;
        new(settings: Partial<IFastContainer.Settings>, name: string): IFastContainer;
    }

    export let FastContainer: IFastContainerConstructor;
}