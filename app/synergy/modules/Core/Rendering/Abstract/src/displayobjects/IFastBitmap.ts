namespace RS.Rendering
{
    /**
     * Encapsulates a lightweight bitmap object, which can be used to display an arbitrary texture.
     * A simpler design with fewer composite objects provides significant performance benefits when instantiating and using large quantites of objects.
     * They are designed for cases where a container contains large quantities of homogenous objects, such as in a particle system.
     * They are intended to be an implementation detail, and so should ideally not be exposed to prevent misuse.
     * Use a RenderTexture to allow the object to take on the appearance of another class of object, or modify the texture property to add animation.
     */
    export interface IFastBitmap extends IDisposable
    {
        /** The name of this fast bitmap. Useful for debugging. */
        name: string;
        /** The stage to which this fast bitmap belongs. */
        stage: IStage;
        /** The FastContainer that contains this fast bitmap. */
        parent: IFastContainer;
        /** Gets the native object that this fast bitmap wraps. */
        readonly nativeObject: any;
        /** Gets the root parent of this display object, that is, the highest parent in the current hierarchy. */
        readonly rootParent: IDisplayObject | null;
        /** The X anchor for this fast bitmap (0-1). */
        anchorX: number;
        /** The Y anchor for this fast bitmap (0-1). */
        anchorY: number;
        /** The colour tint for this fast bitmap. */
        tint: Util.ColorLike;
        /** The alpha transparency value, from 0 to 1.0. */
        alpha: number;
        /** The subtexture used to render this fast bitmap. */
        texture: RS.Rendering.ISubTexture;
        /** Gets or sets the shader to render this bitmap using. */
        shader: IShader | null;

        /** Gets or sets if this object is visible or not. */
        visible: boolean;
        /** The x coordinate relative to the local coordinates of the parent IDisplayObject<T>. */
        x: number;
        /** The y coordinate relative to the local coordinates of the parent IDisplayObject<T>. */
        y: number;
        /** The horizontal scale applied from the registration point. With a value of -1 being -100% and 1 being 100%. Equivalent to object.scale.x. */
        scaleX: number;
        /** The vertical scale applied from the registration point. With a value of -1 being -100% and 1 being 100%. Equivalent to object.scale.y. */
        scaleY: number;
        /** The rotation in radians, from its original orientation. */
        rotation: number;
        /** The rotation in degrees, from its original orientation. */
        rotationDegrees: number;
        /** The x coordinate of this object's origin point, relative to the local coordinates of this object. */
        pivotX: number;
        /** The y coordinate of this object's origin point, relative to the local coordinates of this object. */
        pivotY: number;

        /** Sets the transform of this fast bitmap. */
        setTransform(x: number, y: number, scaleX: number, scaleY: number, rotation: number, pivotX: number, pivotY: number): this;
        /** Called when the fast bitmap has been added to a container. */
        onAdded(): void;
        /** Called when the fast bitmap has been removed from a container. */
        onRemoved(): void;
    }

    /**
     * Constructor for IFastBitmap.
     */
    export interface IFastBitmapConstructor extends Function
    {
        prototype: IFastBitmap;
        new(texture: ISubTexture): IFastBitmap;
    }

    export let FastBitmap: IFastBitmapConstructor;
}