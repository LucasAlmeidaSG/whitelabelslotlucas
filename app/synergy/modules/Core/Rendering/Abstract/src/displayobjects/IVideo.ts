namespace RS.Rendering
{
    export interface IVideo extends Rendering.IDisplayObject
    {

        readonly onFrameChanged:IPrivateEvent<VideoFrameEvent>;
        readonly onFinished:IEvent;

        readonly tag: HTMLVideoElement;
        readonly playing: boolean;

        play(params: QueueEntry): void;
        queue(params: QueueEntry): void;
        pause(): void;
        setFrame(frame: number): void;
        step(): void;
        resume(): void;
        end(): void;
    }
    
    export namespace Video
    {
        export interface Settings
        {
            /** CSS ID of the video tag. */
            cssID?: string;

            /** DOM element to add the video tag to. Should be the same container that holds the canvas. */
            cssContainer: DOM.Component|HTMLElement;

            /** The child index to insert the video tag at. Used to layer the video below or above the canvas (or other elements). */
            cssIndex?: number;

            /** Whether or not to use DOM-based video or PIXI-based video. If unset, will pick based on device type. */
            useDom?: boolean;

            /** Audio track to play alongside the video. */
            audio?: Asset.SoundReference;
        }
    }

    export interface IVideoConstructor extends Function
    {
        prototype: IVideo;
        new(settings: Video.Settings): IVideo;
    }

    export let Video: IVideoConstructor;
}
