/// <reference path="IDisplayObject.ts" />

namespace RS.Rendering
{
    /**
     * Encapsulates an abstract display object container.
     */
    export interface IBaseContainer<T extends IDisplayObject | IFastBitmap> extends IDisplayObject
    {
        /** Whether or not children of this container may be interactive. Defaults to true. */
        interactiveChildren: boolean;

        // #region Hierarchy

        /** Gets a view of all children of this container. */
        readonly children: ReadonlyArray<T>;

        /** Published when a child has been added to this container. */
        readonly onChildAdded: IPrivateEvent<T>;

        /** Published when a child has been removed from this container. */
        readonly onChildRemoved: IPrivateEvent<T>;

        /** The function to use for sorting children. */
        sortFunction: ((a: T, b: T) => number) | null;

        /**
         * Gets if this container contains the specified child.
         * @param child
         * @param deep If true, will look in any child containers also.
         */
        hasChild(child: T, deep?: boolean): boolean;

        /**
         * Gets the index of the specified child, or -1 if it's not found.
         * @param child
         */
        getChildIndex(child: T): number;

        /**
         * Sets the index of the specified child.
         * Use negative indices to indicate offset from the end.
         * @param child
         * @param index
         */
        setChildIndex(child: T, index: number): this;

        /**
         * Adds a child to this container, optionally at the specified index.
         * Use negative indices to indicate offset from the end.
         * @param child
         * @param index
         */
        addChild(child: T, index?: number): this;

        /**
         * Adds a child to this container underneath the given child.
         * @param childToAdd
         * @param targetChild
         */
        addChildUnder(childToAdd: T, targetChild: T): this;

        /**
         * Adds a child to this container above the given child.
         * @param childToAdd
         * @param targetChild
         */
        addChildAbove(childToAdd: T, targetChild: T): this;

        /**
         * Adds a number of children to this container, optionally starting from the specified index.
         * Use negative indices to indicate offset from the end.
         * @param children
         * @param index
         */
        addChildren(children: ReadonlyArray<T>, index?: number): this;

        /**
         * Adds a number of children to this container.
         * @param children
         * @param index
         */
        addChildren(...children: T[]): this;

        /**
         * Removes a child from this container.
         * @param child
         */
        removeChild(child: T): this;

        /**
         * Removes a number of children from this container.
         * @param children
         */
        removeChildren(children: ReadonlyArray<T>): this;

        /**
         * Removes a number of children from this container.
         * @param children
         */
        removeChildren(...children: T[]): this;

        /**
         * Removes all children from this container.
         */
        removeAllChildren(): this;

        /**
         * Removes the child at the specified index.
         * @param index
         */
        removeChildAt(index: number): T;

        /**
         * Removes all children within the specified range of indices, exclusive of endIndex.
         * @param startIndex
         * @param endIndex
         */
        removeChildRange(startIndex: number, endIndex: number): T[];

        /**
         * Gets the first child that satisfies the given filter function.
         * If more than one child satisfies the filter, the first one will be returned.
         * @param filter Filter function that takes a display object and returns true when it matches the desired condition.
         * @param deep If true, will look in any child containers also.
         */
        getChild(filter: (element: T) => boolean, deep?: boolean): T;

        /**
         * Gets the child at the specified index.
         * @param index
         */
        getChildAt(index: number): T;

        /**
         * Gets the child with the specified name.
         * If more than one child exists with the name, the first one will be returned.
         * @param name
         * @param deep If true, will look in any child containers also.
         */
        getChildByName(name: string, deep?: boolean): T;

        /**
         * Swaps the index of the two specified children.
         * @param a
         * @param b
         */
        swapChildren(a: T, b: T): this;

        /**
         * Replaces the given child with the other given child.
         * @param a
         * @param b
         */
        replaceChild(a: T, b: T): this;

        /**
         * Sorts all children using the specified standard sorting function.
         * @param sortFunc If not specified, the currently set sortFunction should be used.
         */
        sortChildren(sortFunc?: (a: T, b: T) => number): this;

        /**
         * Sets the index of the specified child to -1, that is, so it draws ontop of everything else.
         * @param child
         */
        moveToTop(child: T): this;

        /**
         * Sets the index of the specified child to 0, that is, so it draws underneath of everything else.
         * @param child
         */
        moveToBottom(child: T): this;

        // #endregion
    }
}