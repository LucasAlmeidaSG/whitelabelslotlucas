namespace RS.Rendering
{
    /**
     * @interface ISpine
     *
     * @description Interface for Spine objects that handle the loading and playing of a Spine animation
     */
    export interface ISpine extends IDisplayObject
    {

        readonly onSpineComplete: IPrivateEvent<ISpine>;
        readonly onSpineRepeat: IPrivateEvent<ISpine>;
        readonly onSpineUpdate: IPrivateEvent<SpineEventData>;
        /**
         * Set the animation for the current Spine
         *
         * @param animation The name of the animation
         * @deprecated You should use gotoAndPlay or gotoAndStop functions instead of setAnimation and play/playFrom
         */
        setAnimation(animation: string): void;
        /**
         * Play the animation for/to a specific point
         *
         * @param from The time in the animation to start from
         * @param to The time when to stop the animation
         * @param repeatCount The amount of times to repeat the animation
         * @param speed Playback speed. -1.0 is reverse.
         * @deprecated You should use gotoAndPlay or gotoAndStop functions instead of setAnimation and play/playFrom
         */
        playFrom(from: number, to?: number, repeatCount?: number, speed?: number): void;
        /**
         * Play the animation
         *
         * @param repeatCount The amount of times to repeat the animation
         * @param to The time when to stop the animation
         * @param speed Playback speed. -1.0 is reverse.
         * @deprecated You should use gotoAndPlay or gotoAndStop functions instead of setAnimation and play/playFrom
         */
        play(repeatCount?: number, to?: number, speed?: number): void;
        /**
         * Stops the current animation
         *
         * @param reset Whether or not the animation should start again next time it is played
         */
        stop(reset?: boolean): void;

        /**
         * Reports whether or not this spine is playing
         *
         * @returns {boolean} Value indicating whether or not this spine is playing
         */
        isPlaying(): boolean;

        /**
         * Sets the skin to use for the spine object.
         *
         * @param skin The skin to use for this object
         */
        setSkin(skin: string): void;

        /**
         * Sets the animation and starts it playing. 
         * Plays the current set animation if none specified.
         *
         * @param animation The animation to start playing
         * @param settings animation playback settings
         */
        gotoAndPlay(animation?: string, settings?: ISpine.PlaySettings): PromiseLike<void>;

        /**
         * Sets the animation and starts it playing. 
         * Plays the current set animation if none specified.
         *
         * @param animation The animation to start playing
         * @param settings animation playback settings
         */
        gotoAndPlay(animation?: string[], settings?: ISpine.PlaySettings): PromiseLike<void>;

        /**
         * Seeks to the beginning of the specified animation and suspends playback.
         * Uses the current set animation if none specified.
         * 
         * @param animation
         */
        gotoAndStop(animation?: string): void;

        /**
         * Seeks to the specified absolute frame index and suspends playback.
         * Uses the current set animation if none specified.
         * @param animation
         */
        gotoAndStop(animation?: string[]): void;

        /**
         * Get animation length. Gets the lengh of the current set animation if none specified.
         * @param animation
         */
        getAnimationLength(animation?: string): number;

        /**
         * Sets the pivot point based on a bone name
         * @param bone
         */
        setPivotToBone(bone: string): void;

        /**
         * Gets the position of the animation playing on the given track.
         * @param trackID Index of the track on which the animation is playing.
         */
        getAnimationPosition(trackID: number): number;
    }

    export interface ISpineConstructor
    {
        prototype: ISpine;
        new(scale: number, data: Asset.ISpineAsset, useSetupPose?: boolean): ISpine;
    }

    export namespace ISpine
    {
        export interface PlaySettings
        {
            loop?: boolean;
            speed?: number;
            startTime?: number;
        }
    }

    export let Spine: ISpineConstructor;

    export interface SpineEventData
    {
        target: ISpine;
        time: number;
    }
}
