namespace RS.Rendering {
    /**
     * Encapsulates data about a single frame within a SpriteSheet.
     */
    export interface Frame {
        /** The image object in which the frame is found. */
        imageData:     ISubTexture;

        /** The image object in which the frame is found, for various maps. */
        maps?:         { [mapName: string]: ISubTexture };

        /** Image frame ID. */
        imageID:       number;

        /** Defines the boundaries for the frame within the image. */
        imageRect:     ReadonlyRectangle;

        /** The reg value for the frame. */
        reg:           Math.Vector2D;

        /** The original untrimmed size of the frame. */
        untrimmedSize: Math.Size2D;

        /** Defines the trimmed content within the frame. */
        trimRect:      ReadonlyRectangle;

        event?:        string;
    }
}

namespace RS.Is
{
    export function frame(obj: any): obj is RS.Rendering.Frame
    {
        return Is.object(obj) &&
            Is.object(obj.imageData) &&
            Is.number(obj.imageID) &&
            obj.imageRect instanceof RS.Rendering.ReadonlyRectangle &&
            Is.vector2D(obj.reg) &&
            Is.size2D(obj.untrimmedSize) &&
            obj.trimRect instanceof RS.Rendering.ReadonlyRectangle;
    }
}
