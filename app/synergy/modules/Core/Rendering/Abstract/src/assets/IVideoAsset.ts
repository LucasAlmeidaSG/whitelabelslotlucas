namespace RS.Asset
{
    /**
     * Encapsulates a video asset type.
     */
    export interface IVideoAsset<T = any> extends Asset.IBaseAsset<string>
    {
        /** Gets the start poster texture for this video asset. */
        readonly startPoster: T;

        /** Gets the end poster texture for this video asset. */
        readonly endPoster: T;

        /** Gets the framerate of this video asset. */
        readonly framerate: number;

        /** Gets the duration of this video asset (ms). */
        readonly duration: number;

        /** Gets the sound source for this video. */
        readonly soundSource: RS.Asset.Base<RS.Audio.ISoundSource>;

    }
    export interface IVideoAssetAssetConstructor
    {
        prototype: IVideoAsset;
        new(definition: Asset.Definition): IVideoAsset;
    }
    export let VideoAsset: IVideoAssetAssetConstructor;
}
