
namespace RS.Asset {
    export interface IShaderAsset<T = any> extends Asset.IBaseAsset<T> {
        selectFormat(options: string[]): string | null;
    }
    export interface IShaderAssetConstructor
    {
        prototype: IShaderAsset;
        new(definition: Definition): IShaderAsset;
    }
    export let ShaderAsset: IShaderAssetConstructor;
}