
namespace RS.Asset {
    export interface ISpriteSheetAsset<T = any> extends Asset.IBaseAsset<T> {
        resolution:number;
        selectFormat(options: string[]): string | null;
    }
    export interface ISpriteSheetAssetConstructor
    {
        prototype: ISpriteSheetAsset;
        new(definition: Definition): ISpriteSheetAsset;
    }
    export let SpriteSheetAsset: ISpriteSheetAssetConstructor;
    export let RSSpriteSheetAsset: ISpriteSheetAssetConstructor;
}