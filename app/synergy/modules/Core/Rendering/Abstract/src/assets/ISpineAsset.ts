
namespace RS.Asset {
    export interface ISpineAsset<T = any> extends Asset.IBaseAsset<T> {
        resolution:number;
        selectFormat(options: string[]): string | null;
    }
    export interface ISpineAssetConstructor
    {
        prototype: ISpineAsset;
        new(definition: Definition): ISpineAsset;
    }
    export let SpineAsset: ISpineAssetConstructor;
}