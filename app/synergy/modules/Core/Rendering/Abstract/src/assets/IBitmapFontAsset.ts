
namespace RS.Asset {
    export interface IBitmapFontAsset<T = any> extends Asset.IBaseAsset<T> {
        selectFormat(options: string[]): string | null;
    }

    export interface IBitmapFontAssetConstructor
    {
        prototype: IBitmapFontAsset;
        new(definition: Definition): IBitmapFontAsset;
    }
    export let BitmapFontLoader: IBitmapFontAssetConstructor;
}