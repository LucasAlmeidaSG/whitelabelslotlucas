namespace RS.Rendering {
    export interface IFrameEvent
    {
        frame: number;
        event: string;
    }
}