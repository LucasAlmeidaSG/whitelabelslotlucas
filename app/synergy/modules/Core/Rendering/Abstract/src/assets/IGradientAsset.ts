
namespace RS.Asset
{
    export interface IGradientAsset<T = Asset.GradientStopMap> extends Asset.IBaseAsset<T>
    {

    }
    export interface IGradientAssetConstructor
    {
        prototype: IGradientAsset;
        new(definition: Definition): IGradientAsset;
    }
    export let GradientAsset: IGradientAssetConstructor;
}
