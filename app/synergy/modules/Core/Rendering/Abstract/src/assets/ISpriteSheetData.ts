namespace RS.Rendering {
    /**
     * Raw JSON data for a sprite sheet.
     */
    export interface ISpriteSheetData<T>
    {
        frames: ISpriteSheetFrame[];
        meta:
        { 
            app: string;
            version: string;
            image: T;
            format: string;
            size: {
                w:number,
                h:number
            };
            scale: number;
            smartupdate: string;
        };
    }

    export interface ISpriteSheetFrame
    {
        filename: string;
        frame: {
            x:number,   
            y:number,
            w:number,
            h:number
        };
        rotated: boolean;
        trimmed: boolean;
        spriteSourceSize: {
            x:number,   
            y:number,
            w:number,
            h:number
        };
        sourceSize: {
            w:number,
            h:number
        };
        pivot: {
            x:number,   
            y:number
        };
    }
}