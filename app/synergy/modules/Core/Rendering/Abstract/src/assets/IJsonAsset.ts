
namespace RS.Asset {
    export interface IJsonAsset<T = any> extends Asset.IBaseAsset<T> {
        
    }
    export interface IJsonAssetConstructor
    {
        prototype: IJsonAsset;
        new(definition: Definition): IJsonAsset;
    }
    export let JsonAsset: IJsonAssetConstructor;
}