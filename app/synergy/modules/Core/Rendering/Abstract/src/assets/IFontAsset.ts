namespace RS.Asset
{
    export interface IFontAsset<T = any> extends Asset.IBaseAsset<T>
    {
        
    }
    export interface IFontAssetConstructor
    {
        prototype: IFontAsset;
        new(definition: Definition): IFontAsset;
    }
    export let FontAsset: IFontAssetConstructor;
}