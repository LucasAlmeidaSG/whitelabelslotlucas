namespace RS.Asset
{
    export interface IModelAsset extends Asset.IBaseAsset<RS.Rendering.Scene.IReadonlyGeometry>
    {
        
    }
    export interface IModelAssetConstructor
    {
        prototype: IModelAsset;
        new(definition: Definition): IModelAsset;
    }
    export let ModelAsset: IModelAssetConstructor;
}