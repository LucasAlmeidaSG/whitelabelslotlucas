namespace RS.Rendering
{
    export interface ISpineData
    {
        json: object;
        atlas: string;
        images: ITexture[];

        /**
         * Uploads all textures to the rendering context on the specified stage if they haven't already been uploaded.
         * This eliminates stuttering due to just-in-time texture uploads, but takes time and uses video memory.
         */
        warmup(stage: IStage): void;
    }
}