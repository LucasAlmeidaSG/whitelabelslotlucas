namespace RS.Rendering
{
    export interface ICompressedImage
    {
        readonly onLoad: IEvent<void>;

        init(src: string, data: Uint8Array, type: string, width: number, height: number, levels: number, internalFormat: number, crunchCache?: [ crunch.Pointer, crunch.Pointer ]): this;

        generateWebGLTexture(gl: WebGLRenderingContext): void;
        
        loadFromArrayBuffer(arrayBuffer: ArrayBuffer, crnLoad?: boolean): this;

        arrayBufferCopy(src: ArrayBufferView, dst: ArrayBufferView, dstByteOffset: number, numBytes: number): void;
    }
    
    export interface ICompressedImageConstructor
    {
        prototype: ICompressedImage;
        new(src: string, data?: any, type?: any, width?: number, height?: number, levels?: number, internalFormat?: number): ICompressedImage;
    }
    export let CompressedImage: ICompressedImageConstructor;
}