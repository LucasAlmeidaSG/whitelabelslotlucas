namespace RS.Rendering {
    export interface IAnimation
    {
        /** An array of the frame ids in the animation. */
        frames: number[];

        /** The playback speed for this animation. */
        speed?: number;

        /** The name of the animation. */
        name:   string;

        /** the default animation to play next. If the animation loops, the name and next property will be the same. */
        next?:  string;
    }
}