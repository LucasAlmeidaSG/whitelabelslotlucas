namespace RS.Rendering
{
    /**
	 * Encapsulates an animated sprite sheet.
     * A sprite sheet is a series of images (usually animation frames) combined into a larger image (or images).
     * For example, an animation consisting of eight 100x100 images could be combined into a single 400x200 sprite sheet (4 frames across by 2 high).
     */
    export interface ISpriteSheet
    {

        /** Gets a rectangle encompassing every frame within this sprite sheet. */
        readonly maxFrame: ReadonlyRectangle;

        framerate: number;

        readonly animations: string[];

        getAnimation(name: string, reverse?: boolean ): Rendering.IAnimation;
        /**
         * Gets the total number of frames within this sprite sheet.
         */
        getNumFrames(): number;

        /**
         * Gets data for a specific absolute frame.
         * @param frameIndex
         */
        getFrames(): Frame[];

        /**
         * Gets data for a specific absolute frame.
         * @param frameIndex
         */
        getFrame(frameIndex: number): Frame;

        /**
         * Gets data for a specific frame within an animation.
         * @param animation
         * @param frameIndex
         */
        getFrame(name: string, frameIndex?: number): Frame;

        /**
         * Returns a Rectangle instance defining the bounds of the specified frame relative to the pivot.
         * For example, a 90 x 70 frame with a regX of 50 and a regY of 40 would return:
         * 	[x=-50, y=-40, width=90, height=70]
         * @param frameIndex
         * @param out
         */
        getFrameBounds(frameIndex: number, out?: Rectangle): ReadonlyRectangle;

        /**
         * Creates a deep copy of this sprite sheet.
         */
        clone(): ISpriteSheet;

        /**
         * Uploads all textures to the rendering context on the specified stage if they haven't already been uploaded.
         * This eliminates stuttering due to just-in-time texture uploads, but takes time and uses video memory.
         */
        warmup(stage: IStage, animation?: string): void;
    }

    /**
     * Constructor for ISpriteSheet.
     */
    export interface ISpriteSheetConstructor extends Function
    {
        prototype: ISpriteSheet;
        new(data: any, resolution?: number): ISpriteSheet;
    }
    export let SpriteSheet: ISpriteSheetConstructor;
    export let RSSpriteSheet: ISpriteSheetConstructor;

}
