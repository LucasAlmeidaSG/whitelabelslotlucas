
namespace RS.Asset {
    export interface IMsgPackAsset<T = any> extends Asset.IBaseAsset<T> {

    }
    export interface IMsgPackAssetConstructor
    {
        prototype: IMsgPackAsset;
        new(definition: Definition): IMsgPackAsset;
    }
    export let MsgPackAsset: IMsgPackAssetConstructor;
}