namespace RS.Asset
{
    export interface IImageAsset<T = Rendering.ITexture> extends Asset.IBaseAsset<T>
    {
        resolution:number;
    }
    export interface IImageAssetConstructor
    {
        prototype: IImageAsset;
        new(definition: Definition): IImageAsset;
    }
    export let ImageAsset: IImageAssetConstructor;
}
