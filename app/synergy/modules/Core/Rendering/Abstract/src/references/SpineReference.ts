namespace RS.Asset
{
    export type SpineReference = Asset.Reference<"spine">;
    export namespace SpineReference
    {
        /**
         * Gets the sprite sheet for this asset.
         * @param this 
         */
        export function get(this: Asset.GenericReference): RS.Asset.ISpineAsset | null
        {
            const asset = Asset.Store.getAsset(this) as object as RS.Asset.ISpineAsset;
            if (asset == null) { return null; }
            return asset;
        }
    }
}