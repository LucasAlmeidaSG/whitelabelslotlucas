namespace RS.Asset
{
    export type ShaderReference = Asset.Reference<"shader">;
    export namespace ShaderReference
    {
        /**
         * Gets the source for this shader.
         * @param this 
         */
        export function get(this: Asset.GenericReference): string | null
        {
            const asset = Asset.Store.getAsset(this).asset as IShaderAsset<string>;
            if (asset == null) { return null; }
            return asset.asset;
        }
    }

}