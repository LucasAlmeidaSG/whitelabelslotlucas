namespace RS.Asset
{
    export type BitmapFontReference = Asset.Reference<"bitmapfont">;
}