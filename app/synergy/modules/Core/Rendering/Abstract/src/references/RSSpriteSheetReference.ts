namespace RS.Asset
{
    export type RSSpriteSheetReference = Asset.Reference<"rsspritesheet">;
    export namespace RSSpriteSheetReference
    {
        /**
         * Gets the sprite sheet for this asset.
         * @param this
         */
        export function get(this: Asset.GenericReference): Rendering.ISpriteSheet | null
        {
            const asset = Asset.Store.getAsset(this).asset as Rendering.ISpriteSheet;
            if (asset == null) { return null; }
            return asset;
        }
    }
}
