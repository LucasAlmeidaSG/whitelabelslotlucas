namespace RS.Asset
{
    export type ModelReference = Asset.Reference<"model">;
    export namespace ModelReference
    {
        /**
         * Gets the geometry for this asset.
         * @param this 
         */
        export function get(this: Asset.ModelReference): IModelAsset | null
        {
            const asset = Asset.Store.getAsset(this) as object as IModelAsset;
            if (asset == null) { return null; }
            return asset;
        }
    }

}