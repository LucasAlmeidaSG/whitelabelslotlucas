namespace RS.Asset
{
    export type GradientReference = Asset.Reference<"gradient">;
    export type GradientStopMap = {[name: string] : Rendering.Gradient.Stop[]};

    export namespace GradientReference
    {
        /**
         * Gets the source for this asset.
         * @param this
         * @param gradient The gradient to get from the asset
         * @param info Partial Rendering Gradient information, this will be filled out with the correct stops information
         */
        export function get(this: Asset.GenericReference,gradient?: string): GradientStopMap | Rendering.Gradient.Stop[]
        {
            const asset = Asset.Store.getAsset(this).asset as GradientStopMap;

            if (asset == null) { return null; }
            if (gradient == null) { return asset; }

            return asset[gradient];
        }
    }

}
