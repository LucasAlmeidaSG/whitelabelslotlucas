namespace RS.Asset
{
    export type JsonReference = Asset.Reference<"json">;
    export namespace JsonReference
    {
        /**
         * Gets the source for this asset.
         * @param this 
         */
        export function get(this: Asset.GenericReference): string | null
        {
            const asset = Asset.Store.getAsset(this).asset as string;
            if (asset == null) { return null; }
            return asset;
        }
    }

}