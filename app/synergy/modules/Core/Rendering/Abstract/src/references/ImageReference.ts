namespace RS.Asset
{
    export type ImageReference = Asset.Reference<"image">;
    export namespace ImageReference
    {
        /**
         * Gets the texture for this asset.
         * @param this 
         */
        export function get(this: Asset.GenericReference): IImageAsset | null
        {
            const asset = Asset.Store.getAsset(this) as object as IImageAsset;
            if (asset == null) { return null; }
            return asset;
        }
    }

}