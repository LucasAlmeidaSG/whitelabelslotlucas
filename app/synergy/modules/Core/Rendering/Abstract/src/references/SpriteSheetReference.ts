namespace RS.Asset
{
    export type SpriteSheetReference = Asset.Reference<"spritesheet">;
    export namespace SpriteSheetReference
    {
        /**
         * Gets the sprite sheet for this asset.
         * @param this 
         */
        export function get(this: Asset.GenericReference): RS.Asset.ISpriteSheetAsset | null
        {
            const asset = Asset.Store.getAsset(this) as object as RS.Asset.ISpriteSheetAsset;
            if (asset == null) { return null; }
            return asset;
        }
    }
}