namespace RS.Asset
{
    export type MsgPackReference = Asset.Reference<"msgpack">;
    export namespace MsgPackReference
    {
        /**
         * Gets the source for this asset.
         * @param this 
         */
        export function get(this: Asset.GenericReference): IMsgPackAsset | null
        {
            const asset = Asset.Store.getAsset(this).asset as IMsgPackAsset;
            if (asset == null) { return null; }
            return asset;
        }
    }

}