namespace RS.Asset
{
    export type FontReference = Asset.Reference<"font">;
}