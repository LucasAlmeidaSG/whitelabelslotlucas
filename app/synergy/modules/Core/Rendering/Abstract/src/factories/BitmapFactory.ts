namespace RS.Factory
{
    import Vec2 = RS.Math.Vector2D;

    /** Creates a new static bitmap from the specified image reference. */
    export function bitmap(assetID: Asset.ImageReference, transform?: Partial<Rendering.Transform>): Rendering.IBitmap; // 2 params

    /** Creates a new static bitmap from the specified generic asset reference. */
    export function bitmap(assetID: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference, frameID: number, transform?: Partial<Rendering.Transform>): Rendering.IBitmap; // 3 params

    export function bitmap(assetID: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference, animName: string, transform?: Partial<Rendering.Transform>): Rendering.IBitmap; // 3 params

    export function bitmap(assetID: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference, animName: string, frameID: number, transform?: Partial<Rendering.Transform>): Rendering.IBitmap; // 4 params

    /** Creates a new static bitmap from the specified asset group and asset ID. */
    export function bitmap(group: string, assetID: string, transform?: Partial<Rendering.Transform>): Rendering.IBitmap; // 3 params

    export function bitmap(p1: string|Asset.GenericReference, p2?: string|Partial<Rendering.Transform>|number, p3?: number|Partial<Rendering.Transform>, p4?: Partial<Rendering.Transform>): Rendering.IBitmap
    {
        // Resolve overload
        const group = Is.string(p1) ? p1 : p1.group;
        const assetID = Is.string(p1) ? (Is.string(p2) ? p2 : null) : p1.id;

        const animName = Is.string(p1) ? null : Is.string(p2) ? p2 : null;
        const frameID = Is.number(p2) ? p2 : Is.number(p3) ? p3 : null;
        const transform = Is.primitive(p2) ? Is.number(p3) ? p4 : p3 : p2;

        let spr: Rendering.IBitmap;

        const asset = RS.Asset.Store.getAsset(group, assetID);
        if (asset == null)
        {
            Log.warn(`Failed to create bitmap with asset id '${assetID}' (asset not found)`);
            return null;
        }

        // Is it a spritesheet frame?
        if (frameID != null || animName != null)
        {
            // Get asset
            const spritesheetAsset = asset.asset as Rendering.ISpriteSheet;
            if (spritesheetAsset == null)
            {
                Log.warn(`Failed to create bitmap with spritesheet id '${assetID}' (wrong type)`);
                return null;
            }

            // Create sprite
            const frame = animName != null ? spritesheetAsset.getFrame(animName, frameID) : spritesheetAsset.getFrame(frameID);
            spr = new Rendering.Bitmap(frame);
            spr.pivot.set(frame.reg.x, frame.reg.y);
        }
        else
        {
            // Get asset
            const textureAsset = asset.as(Asset.ImageAsset);
            if (textureAsset == null)
            {
                Log.warn(`Failed to create bitmap with id '${assetID}' (wrong type)`);
                return null;
            }

            // Create sprite
            spr = new Rendering.Bitmap(textureAsset);
        }

        // Setup transform
        if (transform != null)
        {
            Rendering.Transform.apply(transform, spr);
        }

        // P8
        // const def = RS.Assets.Store.getAssetDefinition(assetID);
        // if (def && def.format === "P8")
        // {
        //     // Get asset
        //     const asset = RS.Assets.Store.getAsset<PIXI.BaseTexture>(manifestName, assetID);
        //     if (asset == null)
        //     {
        //         Log.warn(`Failed to create P8 bitmap with id '${assetID}' (asset not found)`);
        //         return null;
        //     }
        //     if (!(asset instanceof RS.Create.DataTexture))
        //     {
        //         Log.warn(`Failed to create P8 bitmap with id '${assetID}' (asset was not correct format)`);
        //         return null;
        //     }

        //     spr.shader = RS.Create.GlobalPalette.getPaletteShader(asset);
        // }

        // Apply modifiers
        applyModifiers(spr);

        // Return it
        return spr;
    }

    /** Factory responsible for creating static bitmaps. */
    export namespace bitmap
    {
        export const FactoryInfo: Info<Rendering.IBitmap> =
        {
            type: Rendering.Bitmap,
            modifiers: []
        };
        register(FactoryInfo);
    }
}