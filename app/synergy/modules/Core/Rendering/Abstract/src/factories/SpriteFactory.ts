namespace RS.Factory
{
    import Vec2 = RS.Math.Vector2D;

    /** Creates a new animated sprite from the specified asset reference. */
    export function sprite(assetID: Asset.RSSpriteSheetReference, anchor?: Vec2, position?: Vec2, scale?: Vec2): Rendering.IAnimatedSprite; // 4 params

    /** Creates a new static bitmap from the specified asset group and asset ID. */
    export function sprite(group: string, assetID: string, anchor?: Vec2, position?: Vec2, scale?: Vec2): Rendering.IAnimatedSprite; // 5 params

    export function sprite(p1: string|Asset.GenericReference, p2?: string|Vec2, p3?: Vec2, p4?: Vec2, p5?: Vec2): Rendering.IAnimatedSprite
    {
        // Resolve overload
        const group = Is.string(p2) ? (Is.string(p1) ? p1 : p1.group) : (Is.string(p1) ? Asset.globalManifest : p1.group); // 1st or NONE
        const assetID = Is.string(p2) ? p2 : (Is.string(p1) ? p1 : p1.id); // 2nd or 1st
        const anchor = Is.string(p2) ? p3 : p2; // 3rd or 2nd
        const position = Is.string(p2) ? p4 : p3; // 4th or 3rd
        const scale = Is.string(p2) ? p5 : p4; // 5th or 4th

        // Get asset
        const asset = Asset.Store.getAsset(group, assetID);
        if (asset == null)
        {
            RS.Log.error(`Failed to create sprite with id '${assetID}' (asset not found)`);
            return null;
        }
        const assetAsSpritesheet = asset.as(Rendering.RSSpriteSheet);
        if (assetAsSpritesheet == null)
        {
            RS.Log.error(`Failed to create sprite with id '${assetID}' (asset wrong type)`);
            return null;
        }

        // Create sprite
        const spr = new Rendering.AnimatedSprite(assetAsSpritesheet);

        // Setup transform
        if (anchor != null) { 
            spr.anchorX = anchor.x;
            spr.anchorY = anchor.y; 
        }
        if (position != null) { 
            spr.x = position.x;
            spr.y = position.y; 
        }
        if (scale != null) { 
            spr.scaleX = scale.x;
            spr.scaleY = scale.y; 
        }

        // Apply modifiers
        applyModifiers(spr);

        // Return it
        return spr;
    }

    /** Factory responsible for creating animated sprites. */
    export namespace sprite
    {
        export const FactoryInfo: Info<Rendering.IAnimatedSprite> =
        {
            type: Rendering.AnimatedSprite,
            modifiers: []
        };
        register(FactoryInfo);
    }
}