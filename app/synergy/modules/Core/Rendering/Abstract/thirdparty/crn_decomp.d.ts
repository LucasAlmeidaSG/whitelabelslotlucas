declare namespace crunch
{
    export const HEAPU8: Uint8Array;
    
    export type Pointer = number;

    export function _malloc(size: number): Pointer;
    export function _free(ptr: Pointer): void;

    export function _crn_get_width(src: Pointer, srcSize: number): number;
    export function _crn_get_height(src: Pointer, srcSize: number): number;
    export function _crn_get_levels(src: Pointer, srcSize: number): number;
    export function _crn_get_dxt_format(src: Pointer, srcSize: number): number;
    export function _crn_get_uncompressed_size(src: Pointer, srcSize: number, u0: number): number;

    export function _crn_decompress(src: Pointer, srcSize: number, dst: Pointer, dstSize: number, u0: number): number;
    
}