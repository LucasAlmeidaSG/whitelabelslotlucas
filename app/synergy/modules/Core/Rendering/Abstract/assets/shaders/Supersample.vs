attribute vec2 aVertexPosition;
attribute vec2 aTextureCoord;

uniform float uSrcPixelSize;
uniform vec4 uSrcFrame;
uniform vec4 uDstFrame;

varying vec2 vTextureCoords[4];

void main(void)
{
    gl_Position = vec4(uDstFrame.xy + aVertexPosition * uDstFrame.zw, 0.0, 1.0);
    vTextureCoords[0] = uSrcFrame.xy + aTextureCoord * uSrcFrame.zw;
    vTextureCoords[1] = vTextureCoords[0] + vec2(uSrcPixelSize, 0.0);
    vTextureCoords[2] = vTextureCoords[0] + vec2(uSrcPixelSize, uSrcPixelSize);
    vTextureCoords[3] = vTextureCoords[0] + vec2(0.0, uSrcPixelSize);
}