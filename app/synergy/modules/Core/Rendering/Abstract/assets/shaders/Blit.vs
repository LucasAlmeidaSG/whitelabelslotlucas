attribute vec2 aVertexPosition;
attribute vec2 aTextureCoord;

uniform vec4 uSrcFrame;
uniform vec4 uDstFrame;

varying vec2 vTextureCoord;

void main(void)
{
    gl_Position = vec4(uDstFrame.xy + aVertexPosition * uDstFrame.zw, 0.0, 1.0);
    vTextureCoord = uSrcFrame.xy + aTextureCoord * uSrcFrame.zw;
}