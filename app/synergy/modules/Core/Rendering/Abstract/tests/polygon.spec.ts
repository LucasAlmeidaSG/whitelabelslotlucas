namespace RS.Tests
{
    const { expect } = chai;

    describe("Polygon.ts", function ()
    {
        describe("Polygon", function ()
        {
            describe("contains", function ()
            {
                it("should return whether or not the point is contained within the polygon", function ()
                {
                    // Box
                    const box = new Rendering.Polygon([new Rendering.Point2D(0, 0), new Rendering.Point2D(10, 0), new Rendering.Point2D(10, 10), new Rendering.Point2D(0, 10)]);
                    expect(box.contains({ x: 5, y: 5 }), "box: inside").to.equal(true);
                    expect(box.contains({ x: 0, y: 0 }), "box: inside, top-left boundary").to.equal(true);
                    expect(box.contains({ x: 10, y: 10 }), "box: inside, bottom-right boundary").to.equal(true);

                    expect(box.contains({ x: 20, y: 5 }), "box: outside, x-axis, positive").to.equal(false);
                    expect(box.contains({ x: -10, y: 5 }), "box: outside, x-axis, negative").to.equal(false);
                    expect(box.contains({ x: 5, y: 20 }), "box: outside, y-axis, positive").to.equal(false);
                    expect(box.contains({ x: 5, y: -10 }), "box: outside, y-axis, negative").to.equal(false);

                    const triangle = new Rendering.Polygon([new Rendering.Point2D(0, 0), new Rendering.Point2D(5, 10), new Rendering.Point2D(10, 0)]);
                    expect(triangle.contains({ x: 5, y: 5 }), "triangle: inside").to.equal(true);
                    expect(triangle.contains({ x: 20, y: 1 }), "triangle: outside, not within bounding box").to.equal(false);
                    expect(triangle.contains({ x: 0, y: 10 }), "triangle: outside, within bounding box").to.equal(false);
                });
            });
        });
    });
}