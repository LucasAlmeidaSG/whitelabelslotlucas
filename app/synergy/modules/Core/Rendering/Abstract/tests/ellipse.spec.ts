namespace RS.Tests
{
    const { expect } = chai;

    describe("ReadonlyEllipse.ts", function ()
    {
        describe("ReadonlyEllipse", function ()
        {
            describe("contains", function ()
            {
                it("should return whether or not the point is contained within the ellipse", function ()
                {
                    const ellipse = new Rendering.ReadonlyEllipse(11, 13, 64, 39);
                    expect(ellipse.contains({ x: 11, y: 13 }), "inside").to.equal(true);
                    expect(ellipse.contains({ x: 11 - 32, y: 13 }), "inside, on bounding box").to.equal(true);
                    expect(ellipse.contains({ x: 11 - 64, y: 13 }), "outside, beyond bounding box").to.equal(false);
                    expect(ellipse.contains({ x: 11 - 31, y: 13 - 8 }), "outside, within bounding box").to.equal(false);
                });
            });
        });
    });
}