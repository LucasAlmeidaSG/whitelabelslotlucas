namespace RS.Rendering.Tests
{
    const { expect } = chai;
    
    describe("Rectangle.ts", function ()
    {
        describe("equals", function ()
        {
            it("should equal another rectangle with same numbers", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                const rectB = new RS.Rendering.Rectangle(10, 20, 100, 200);
                chai.expect(RS.Rendering.Rectangle.equals(rectA, rectB)).to.equal(true);
            });

            it("should not equal another rectangle with different numbers", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);

                const rectB = new RS.Rendering.Rectangle(10, 25, 100, 200);
                const rectC = new RS.Rendering.Rectangle(15, 20, 100, 200);
                const rectD = new RS.Rendering.Rectangle(10, 20, 105, 200);
                const rectE = new RS.Rendering.Rectangle(10, 20, 100, 205);

                chai.expect(RS.Rendering.Rectangle.equals(rectA, rectB)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(rectA, rectC)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(rectA, rectD)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(rectA, rectE)).to.equal(false);

                chai.expect(RS.Rendering.Rectangle.equals(rectB, rectA)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(rectC, rectA)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(rectD, rectA)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(rectE, rectA)).to.equal(false);
            });

            it("should not equal a rectangle with a null rectangle", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);

                chai.expect(RS.Rendering.Rectangle.equals(rectA, null)).to.equal(false);
                chai.expect(RS.Rendering.Rectangle.equals(null, rectA)).to.equal(false);
            });

            it("should equal two null rectangles", function ()
            {
                chai.expect(RS.Rendering.Rectangle.equals(null, null)).to.equal(true);
            });
        });

        describe("contains", function ()
        {
            it("should return true if the vec2 is contained in the rectangle", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                chai.expect(rectA.contains({ x: 50, y: 50 })).to.be.true;
                chai.expect(rectA.contains({ x: 15, y: 50 })).to.be.true;
                chai.expect(rectA.contains({ x: 50, y: 150 })).to.be.true;
            });

            it("should return false if the vec2 is not contained in the rectangle", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                chai.expect(rectA.contains({ x: 5, y: 50 })).to.be.false;
                chai.expect(rectA.contains({ x: 115, y: 50 })).to.be.false;
                chai.expect(rectA.contains({ x: 50, y: 15 })).to.be.false;
                chai.expect(rectA.contains({ x: 50, y: 225 })).to.be.false;
                chai.expect(rectA.contains({ x: 5, y: 5 })).to.be.false;
                chai.expect(rectA.contains({ x: 115, y: 225 })).to.be.false;
            });
        });

        describe("intersects", function ()
        {
            it("should return true for two identical rectangles", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                const rectB = new RS.Rendering.Rectangle(10, 20, 100, 200);
                expect(rectA.intersects(rectB)).to.be.true;
            });

            it("should return true for two overlapping rectangles", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                const rectB = new RS.Rendering.Rectangle(-40, -80, 100, 200);
                expect(rectA.intersects(rectB)).to.be.true;
            });

            it("should return false for two non-overlapping rectangles", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                const rectB = new RS.Rendering.Rectangle(-400, -800, 100, 200);
                expect(rectA.intersects(rectB)).to.be.false;
            });

            it("should return false for two rectangles which are touching but not overlapping", function ()
            {
                const rectA = new RS.Rendering.Rectangle(10, 20, 100, 200);
                const rectB = new RS.Rendering.Rectangle(-90, -180, 100, 200);
                expect(rectA.intersects(rectB)).to.be.false;
            });
        })
    });
}