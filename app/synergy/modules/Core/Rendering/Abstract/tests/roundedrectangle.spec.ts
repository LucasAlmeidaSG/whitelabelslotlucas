namespace RS.Tests
{
    const { expect } = chai;

    describe("ReadonlyRoundedRectangle.ts", function ()
    {
        describe("ReadonlyRoundedRectangle", function ()
        {
            describe("contains", function ()
            {
                it("should return whether or not the point is contained within the rounded rectangle", function ()
                {
                    const circle = new Rendering.ReadonlyRoundedRectangle(11, 13, 32, 32, 6);
                    expect(circle.contains({ x: 11 + 10, y: 13 + 10 }), "inside").to.equal(true);
                    expect(circle.contains({ x: 11 + 5, y: 13 + 5 }), "inside, in corner").to.equal(true);
                    expect(circle.contains({ x: 11 + 10, y: 13 + 10 }), "inside, on corner").to.equal(true);
                    expect(circle.contains({ x: 11 + 10, y: 13 }), "inside, on edge").to.equal(true);
                    expect(circle.contains({ x: 11 - 1, y: 13 - 1 }), "outside, beyond bounding box").to.equal(false);
                    expect(circle.contains({ x: 11 + 1, y: 13 + 1 }), "outside, in corner").to.equal(false);
                });
            });
        });
    });
}