describe('Point2D.ts', function ()
{
    describe('equals', function ()
    {
        it('should equal another point with same numbers', function ()
        {
            const pointA = new RS.Rendering.Point2D(10, 20);
            const pointB = new RS.Rendering.Point2D(10, 20);
            chai.expect(RS.Rendering.Point2D.equals(pointA, pointB)).to.equal(true);
        });

        it('should not equal another point with different numbers', function ()
        {
            const pointA = new RS.Rendering.Point2D(10, 20);

            const pointB = new RS.Rendering.Point2D(10, 25);
            const pointC = new RS.Rendering.Point2D(15, 20);

            chai.expect(RS.Rendering.Point2D.equals(pointA, pointB)).to.equal(false);
            chai.expect(RS.Rendering.Point2D.equals(pointA, pointC)).to.equal(false);

            chai.expect(RS.Rendering.Point2D.equals(pointB, pointA)).to.equal(false);
            chai.expect(RS.Rendering.Point2D.equals(pointC, pointA)).to.equal(false);
        });

        it('should not equal a point with a null point', function ()
        {
            const pointA = new RS.Rendering.Point2D(10, 20);
            
            chai.expect(RS.Rendering.Point2D.equals(pointA, null)).to.equal(false);
            chai.expect(RS.Rendering.Point2D.equals(null, pointA)).to.equal(false);
        });

        it('should equal two null points', function ()
        {
            chai.expect(RS.Rendering.Point2D.equals(null, null)).to.equal(true);
        });
    });    
});