namespace RS.Rendering.Tests
{
    const { expect } = chai;

    describe("Pack.ts", function ()
    {
        describe("pack", function ()
        {
            const sheet1: Math.ReadonlySize2D[] = [{"w":165,"h":187},{"w":137,"h":189},{"w":155,"h":191},{"w":163,"h":189},{"w":128,"h":187},{"w":125,"h":187},{"w":163,"h":190},{"w":153,"h":187},{"w":70,"h":187},{"w":112,"h":189},{"w":148,"h":187},{"w":127,"h":187},{"w":190,"h":187},{"w":154,"h":187},{"w":176,"h":192},{"w":135,"h":188},{"w":177,"h":209},{"w":140,"h":188},{"w":134,"h":191},{"w":152,"h":187},{"w":152,"h":189},{"w":164,"h":187},{"w":217,"h":187},{"w":157,"h":187},{"w":158,"h":187},{"w":151,"h":187},{"w":130,"h":154},{"w":145,"h":197},{"w":129,"h":153},{"w":144,"h":197},{"w":138,"h":153},{"w":116,"h":197},{"w":143,"h":193},{"w":135,"h":194},{"w":74,"h":187},{"w":98,"h":229},{"w":134,"h":194},{"w":70,"h":194},{"w":191,"h":151},{"w":136,"h":151},{"w":147,"h":154},{"w":145,"h":191},{"w":144,"h":191},{"w":101,"h":151},{"w":116,"h":153},{"w":110,"h":179},{"w":135,"h":151},{"w":144,"h":149},{"w":193,"h":149},{"w":141,"h":149},{"w":143,"h":193},{"w":131,"h":149},{"w":141,"h":187},{"w":96,"h":182},{"w":135,"h":185},{"w":134,"h":187},{"w":148,"h":182},{"w":133,"h":185},{"w":142,"h":187},{"w":134,"h":182},{"w":140,"h":187},{"w":140,"h":187},{"w":76,"h":78},{"w":76,"h":148}];

            it("should not produce packs with overlapping rects", function ()
            {
                const packs = Rendering.pack(sheet2, RS.Math.Size2D(2048, Number.MAX_VALUE));
                for (const pack of packs)
                {
                    for (let i = 0; i < pack.rects.length; i++)
                    {
                        const rect = pack.rects[i];
                        for (let j = i + 1; j < pack.rects.length; j++)
                        {
                            const otherRect = pack.rects[j];
                            expect(otherRect.intersects(rect), `${otherRect.toString()} @ ${j} does not intersect ${rect.toString()} @ ${i}`).to.be.false;
                        }
                    }
                }
            });

            it("should keep rects and sizeRefs in the same order", function ()
            {
                const sz = Math.Size2D();
                function test(w: number, h: number)
                {
                    sz.w = w; sz.h = h;
                    const packs = Rendering.pack(sheet1, sz);

                    for (const pack of packs)
                    {
                        for (let i = 0; i < pack.rects.length; i++)
                        {
                            const rect = pack.rects[i];
                            const size = pack.sizeRefs[i];
                            expect(rect.w, "rect width should match size width").to.equal(size.w);
                            expect(rect.h, "rect width should match size height").to.equal(size.h);
                        }
                    }
                }

                test(512, 512);
            });

            it("should pack all given sizes", function ()
            {
                const seen: Math.ReadonlySize2D[] = [];
                const sz = Math.Size2D();
                function test(w: number, h: number)
                {
                    sz.w = w; sz.h = h;
                    seen.length = 0;

                    const packs = Rendering.pack(sheet1, sz);
                    for (const pack of packs)
                    {
                        for (const sizeRef of pack.sizeRefs)
                        {
                            expect(seen, "should only appear once").not.to.contain(sizeRef);
                            expect(sheet1, "should be a valid size reference").to.contain(sizeRef);
                            seen.push(sizeRef);
                        }
                    }

                    for (const size of sheet1)
                    {
                        expect(seen, "should appear once").to.contain(size);
                    }
                }

                test(4096, 4096);
                test(2048, 2048);
                test(1024, 1024);
                test(512, 512);
                test(256, 256);
            });

            it("should not produce packs exceeding the maximum size", function ()
            {
                const sz = Math.Size2D();
                function test(w: number, h: number)
                {
                    sz.w = w; sz.h = h;
                    const packs = Rendering.pack(sheet1, sz);
                    for (const pack of packs)
                    {
                        expect(pack.size.w, `width should not exceed ${w}`).to.be.lte(w);
                        expect(pack.size.h, `height should not exceed ${h}`).to.be.lte(h);
                    }
                }

                test(4096, 4096);
                test(2048, 2048);
                test(1024, 1024);
                test(512, 512);
                test(256, 256);
                expect(() => test(128, 128), "should throw if maximum size is too small").to.throw;
            });

            it("should produce an optimum pack for shapes that fit exactly into a rect", function ()
            {
                const sz = Math.Size2D();
                function test(sizes: ReadonlyArray<Math.ReadonlySize2D>, w: number, h: number)
                {
                    sz.w = w; sz.h = h;
                    const packs = Rendering.pack(sizes, sz);
                    expect(packs.length, "should only produce a single pack").to.equal(1);

                    const pack = packs[0];
                    expect(pack.size.w, "width should match exactly").to.equal(w);
                    expect(pack.size.h, "height should match exactly").to.equal(h);

                }
                test([Math.Size2D(50, 100), Math.Size2D(100, 200), Math.Size2D(50, 100)], 150, 200);
            });

            const sheet2Data: [number, number][] = [[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,155],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,163],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,180],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,199],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,245],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375],[226,375]];
            const sheet2 = sheet2Data.map(([ x, y ]) => RS.Math.Size2D(x, y));
            const tpSize = Math.Size2D(4096, 4096);

            it(`should be able to pack the test data into a single ${tpSize.w} x ${tpSize.h} space`, function ()
            {
                const max = tpSize;

                const packs = Rendering.pack(sheet2, max);
                expect(packs.length, "produced just one pack").to.equal(1);

                const pack = packs[0];
                expect(pack.size.w, "width is not greater than max").to.be.lte(max.w);
                expect(pack.size.h, "height is not greater than max").to.be.lte(max.h);
            });
        });
    });
}