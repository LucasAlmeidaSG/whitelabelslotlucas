namespace RS.Rendering.Tests
{
    const { expect } = chai;

    describe("IText.ts", function ()
    {
        describe("TextFilters", function ()
        {
            describe("Uppercase", function ()
            {
                it("should produce an uppercase string", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Uppercase("abcdefghijklmnopqrstuvwxyz 1234567890"), "simple Latin").to.equal("ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890");
                    expect(RS.Rendering.TextOptions.TextFilters.Uppercase("àáâäæãåāèéêëēėęîïíīįìôöòóœøōõûüùúū"), "accented Latin").to.equal("ÀÁÂÄÆÃÅĀÈÉÊËĒĖĘÎÏÍĪĮÌÔÖÒÓŒØŌÕÛÜÙÚŪ");
                    expect(RS.Rendering.TextOptions.TextFilters.Uppercase("абвгдеёжзийклмнопрстуфхцчшщъыьэюяіѣѳѵ"), "Cyrillic").to.equal("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯІѢѲѴ");
                });

                it("should leave an empty string alone", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Uppercase("")).to.equal("");
                });
            });

            describe("Lowercase", function ()
            {
                it("should produce a lowercase string", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Lowercase("ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890"), "simple Latin").to.equal("abcdefghijklmnopqrstuvwxyz 1234567890");
                    expect(RS.Rendering.TextOptions.TextFilters.Lowercase("ÀÁÂÄÆÃÅĀÈÉÊËĒĖĘÎÏÍĪĮÌÔÖÒÓŒØŌÕÛÜÙÚŪ"), "accented Latin").to.equal("àáâäæãåāèéêëēėęîïíīįìôöòóœøōõûüùúū");
                    expect(RS.Rendering.TextOptions.TextFilters.Lowercase("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯІѢѲѴ"), "Cyrillic").to.equal("абвгдеёжзийклмнопрстуфхцчшщъыьэюяіѣѳѵ");
                });

                it("should leave an empty string alone", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Lowercase("")).to.equal("");
                });
            });

            describe("Vertical", function ()
            {
                it("insert an \n between each character of a string", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Vertical("Lorem ipsum\ndolor amet")).to.equal("L\no\nr\ne\nm\n \ni\np\ns\nu\nm\n\n\nd\no\nl\no\nr\n \na\nm\ne\nt");
                });

                it("should leave a single character alone", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Vertical("X")).to.equal("X");
                });

                it("should leave an empty string alone", function ()
                {
                    expect(RS.Rendering.TextOptions.TextFilters.Vertical("")).to.equal("");
                });
            });
        });
    });
}