namespace RS.Tests
{
    const { expect } = chai;

    describe("ReadonlyCircle.ts", function ()
    {
        describe("ReadonlyCircle", function ()
        {
            describe("contains", function ()
            {
                it("should return whether or not the point is contained within the circle", function ()
                {
                    const circle = new Rendering.ReadonlyCircle(11, 13, 32);
                    expect(circle.contains({ x: 11, y: 13 }), "inside").to.equal(true);
                    expect(circle.contains({ x: 11 - 32, y: 13 }), "inside, on bounding box").to.equal(true);
                    expect(circle.contains({ x: 11 - 40, y: 13 - 2 }), "outside, beyond bounding box").to.equal(false);
                    expect(circle.contains({ x: 11 - 24, y: 13 - 24 }), "outside, within bounding box").to.equal(false);
                });
            });
        });
    });
}