namespace RS.Rendering.Shaders
{
    /** Supported line counts in a MaskByLinesShader */
    export type SupportedLines = 1|2;

    const tmpVec = Math.Vector2D();

    export class MaskByLinesShaderProgram extends ShaderProgram
    {
        protected _numLines: SupportedLines;

        /** Gets the number of lines. */
        public get numLines() { return this._numLines; }

        public constructor(lineCount: SupportedLines)
        {
            const defines: Map<string> = {};
            if (lineCount === 2) { defines["TWOLINES"] = "1"; }

            const baseSettings: IShaderProgram.Settings =
            {
                defines,
                fragmentSource: Assets.Shaders.MaskByLine.Fragment
            };

            super(baseSettings);

            this._numLines = lineCount;
        }

        /** Creates a shader instance from this shader program. */
        public createShader(): MaskByLinesShader
        {
            return new MaskByLinesShader(this);
        }
    }

    export class MaskByLinesShader extends Shader
    {
        protected _uLine: ShaderUniform.Vec4[];
        protected _uFadeSize: ShaderUniform.Float[];

        public constructor(shaderProgram: MaskByLinesShaderProgram)
        {
            super(shaderProgram);

            this._uLine = [];
            this._uFadeSize = [];

            if (shaderProgram.numLines === 1)
            {
                this._uLine.push(this.uniforms["uLine"] as ShaderUniform.Vec4);
                this._uFadeSize.push(this.uniforms["uFadeSize"] as ShaderUniform.Float);
                this.setLineFadeSize(1, 20);
            }
            else
            {
                for (let i = 0; i < shaderProgram.numLines; i++)
                {
                    this._uLine.push(this.uniforms[`uLine${i}`] as ShaderUniform.Vec4);
                    this._uFadeSize.push(this.uniforms[`uFadeSize${i}`] as ShaderUniform.Float);
                    this.setLineFadeSize((i + 1) as SupportedLines, 20);
                }
            }
        }

        /**
         * Sets a new mask line from the specified points.
         */
        public setLineByPoints(lineIndex: SupportedLines, start: Math.Vector2D, end: Math.Vector2D): void
        {
            this.setLineByDir(lineIndex, start, { x: end.x - start.x, y: end.y - start.y });
        }

        /**
         * Sets a new mask line from the specified point and direction.
         */
        public setLineByDir(lineIndex: SupportedLines, start: Math.Vector2D, dir: Math.Vector2D): void
        {
            // const height = RS.ViewManager.height;
            const height = 768; // TODO
            const nDir = Math.Vector2D.normalised(dir, tmpVec);
            this._uLine[lineIndex - 1].value = { x: start.x, y: height - start.y, z: nDir.x, w: nDir.y };
        }

        /**
         * Sets a new line fade size.
         */
        public setLineFadeSize(lineIndex: SupportedLines, fadeSize: number): void
        {
            this._uFadeSize[lineIndex - 1].value = fadeSize;
        }
    }
}