namespace RS.Rendering.Shaders
{
    export namespace ColorMatrix
    {
        export type Mat54 = number[];

        /**
         * Multiplies two mat5's
         * @private
         * @param out - 5x4 matrix the receiving matrix
         * @param a - 5x4 matrix the first operand
         * @param b - 5x4 matrix the second operand
         * @returns 5x4 matrix
         */
        function multiply(a: Mat54, b: Mat54, out?: Mat54): Mat54
        {
            if (!out) { out = new Array<number>(20); }

            // Red Channel
            out[0] = (a[0] * b[0]) + (a[1] * b[5]) + (a[2] * b[10]) + (a[3] * b[15]);
            out[1] = (a[0] * b[1]) + (a[1] * b[6]) + (a[2] * b[11]) + (a[3] * b[16]);
            out[2] = (a[0] * b[2]) + (a[1] * b[7]) + (a[2] * b[12]) + (a[3] * b[17]);
            out[3] = (a[0] * b[3]) + (a[1] * b[8]) + (a[2] * b[13]) + (a[3] * b[18]);
            out[4] = (a[0] * b[4]) + (a[1] * b[9]) + (a[2] * b[14]) + (a[3] * b[19]) + a[4];

            // Green Channel
            out[5] = (a[5] * b[0]) + (a[6] * b[5]) + (a[7] * b[10]) + (a[8] * b[15]);
            out[6] = (a[5] * b[1]) + (a[6] * b[6]) + (a[7] * b[11]) + (a[8] * b[16]);
            out[7] = (a[5] * b[2]) + (a[6] * b[7]) + (a[7] * b[12]) + (a[8] * b[17]);
            out[8] = (a[5] * b[3]) + (a[6] * b[8]) + (a[7] * b[13]) + (a[8] * b[18]);
            out[9] = (a[5] * b[4]) + (a[6] * b[9]) + (a[7] * b[14]) + (a[8] * b[19]) + a[9];

            // Blue Channel
            out[10] = (a[10] * b[0]) + (a[11] * b[5]) + (a[12] * b[10]) + (a[13] * b[15]);
            out[11] = (a[10] * b[1]) + (a[11] * b[6]) + (a[12] * b[11]) + (a[13] * b[16]);
            out[12] = (a[10] * b[2]) + (a[11] * b[7]) + (a[12] * b[12]) + (a[13] * b[17]);
            out[13] = (a[10] * b[3]) + (a[11] * b[8]) + (a[12] * b[13]) + (a[13] * b[18]);
            out[14] = (a[10] * b[4]) + (a[11] * b[9]) + (a[12] * b[14]) + (a[13] * b[19]) + a[14];

            // Alpha Channel
            out[15] = (a[15] * b[0]) + (a[16] * b[5]) + (a[17] * b[10]) + (a[18] * b[15]);
            out[16] = (a[15] * b[1]) + (a[16] * b[6]) + (a[17] * b[11]) + (a[18] * b[16]);
            out[17] = (a[15] * b[2]) + (a[16] * b[7]) + (a[17] * b[12]) + (a[18] * b[17]);
            out[18] = (a[15] * b[3]) + (a[16] * b[8]) + (a[17] * b[13]) + (a[18] * b[18]);
            out[19] = (a[15] * b[4]) + (a[16] * b[9]) + (a[17] * b[14]) + (a[18] * b[19]) + a[19];

            return out;
        }

        /**
         * Combines a number of color matrices into one.
         * @param matrices 
         */
        export function combine(...matrices: Mat54[]): Mat54
        {
            if (matrices.length === 0) { return identity(); }
            if (matrices.length === 1) { return [...matrices[0]]; }
            let current = matrices[0];
            for (let i = 1, l = matrices.length; i < l; ++i)
            {
                current = multiply(current, matrices[i]);
            }
            return current;
        }

        /**
         * Does nothing.
         * @param b value of the brigthness (0-1, where 0 is black)
         */
        export function identity(): Mat54
        {
            return [
                1, 0, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Adjusts brightness.
         * @param b value of the brightness (0-1, where 0 is black)
         */
        export function brightness(b: number): Mat54
        {
            return [
                b, 0, 0, 0, 0,
                0, b, 0, 0, 0,
                0, 0, b, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Converts to greyscale.
         * @param scale value of the grey (0-1, where 0 is black)
         */
        export function greyscale(scale: number): Mat54
        {
            return [
                scale, scale, scale, 0, 0,
                scale, scale, scale, 0, 0,
                scale, scale, scale, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Converts to black and white.
         */
        export function blackAndWhite(): Mat54
        {
            return [
                0.3, 0.6, 0.1, 0, 0,
                0.3, 0.6, 0.1, 0, 0,
                0.3, 0.6, 0.1, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Applies a hue shift.
         * @param rotation in degrees
         */
        export function hue(rotation: number): Mat54
        {
            rotation = Math.degreesToRadians(rotation);

            const cosR = Math.cos(rotation);
            const sinR = Math.sin(rotation);
            const sqrt = Math.sqrt;

            /* a good approximation for hue rotation
            This matrix is far better than the versions with magic luminance constants
            formerly used here, but also used in the starling framework (flash) and known from this
            old part of the internet: quasimondo.com/archives/000565.php
            This new matrix is based on rgb cube rotation in space. Look here for a more descriptive
            implementation as a shader not a general matrix:
            https://github.com/evanw/glfx.js/blob/58841c23919bd59787effc0333a4897b43835412/src/filters/adjust/huesaturation.js
            This is the source for the code:
            see http://stackoverflow.com/questions/8507885/shift-hue-of-an-rgb-color/8510751#8510751
            */

            const w = 1 / 3;
            const sqrW = sqrt(w); // weight is

            const a00 = cosR + ((1.0 - cosR) * w);
            const a01 = (w * (1.0 - cosR)) - (sqrW * sinR);
            const a02 = (w * (1.0 - cosR)) + (sqrW * sinR);

            const a10 = (w * (1.0 - cosR)) + (sqrW * sinR);
            const a11 = cosR + (w * (1.0 - cosR));
            const a12 = (w * (1.0 - cosR)) - (sqrW * sinR);

            const a20 = (w * (1.0 - cosR)) - (sqrW * sinR);
            const a21 = (w * (1.0 - cosR)) + (sqrW * sinR);
            const a22 = cosR + (w * (1.0 - cosR));

            return [
                a00, a01, a02, 0, 0,
                a10, a11, a12, 0, 0,
                a20, a21, a22, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Increases the separation between dark and bright.
         * Increase contrast : shadows darker and highlights brighter
         * Decrease contrast : bring the shadows up and the highlights down
         * @param amount value of the contrast (0-1)
         */
        export function contrast(amount: number): Mat54
        {
            const v = (amount || 0) + 1;
            const o = -0.5 * (v - 1);

            return [
                v, 0, 0, 0, o,
                0, v, 0, 0, o,
                0, 0, v, 0, o,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Increases the separation between colors
         * Increase saturation : increase contrast, brightness, and sharpness
         * @param amount The saturation amount (0-1)
         */
        export function saturate(amount = 0): Mat54
        {
            const x = (amount * 2 / 3) + 1;
            const y = ((x - 1) * -0.5);

            return [
                x, y, y, 0, 0,
                y, x, y, 0, 0,
                y, y, x, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Desaturates the image (removes color)
         */
        export function desaturate(): Mat54
        {
            return saturate(-1);
        }

        /**
         * Inverts the image
         */
        export function negative(): Mat54
        {
            return [
                -1, 0, 0, 1, 0,
                0, -1, 0, 1, 0,
                0, 0, -1, 1, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Sepia effect
         */
        export function sepia(): Mat54
        {
            return [
                0.393, 0.7689999, 0.18899999, 0, 0,
                0.349, 0.6859999, 0.16799999, 0, 0,
                0.272, 0.5339999, 0.13099999, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Color motion picture process invented in 1916 (thanks Dominic Szablewski)
         */
        export function technicolor(): Mat54
        {
            return [
                1.9125277891456083, -0.8545344976951645, -0.09155508482755585, 0, 11.793603434377337,
                -0.3087833385928097, 1.7658908555458428, -0.10601743074722245, 0, -70.35205161461398,
                -0.231103377548616, -0.7501899197440212, 1.847597816108189, 0, 30.950940869491138,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Polaroid filter
         */
        export function polaroid(): Mat54
        {
            return [
                1.438, -0.062, -0.062, 0, 0,
                -0.122, 1.378, -0.122, 0, 0,
                -0.016, -0.016, 1.483, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Transforms red to blue and blue to red
         */
        export function toBGR(): Mat54
        {
            return [
                0, 0, 1, 0, 0,
                0, 1, 0, 0, 0,
                1, 0, 0, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Color reversal film introduced by Eastman Kodak in 1935 (thanks Dominic Szablewski)
         */
        export function kodachrome(): Mat54
        {
            return [
                1.1285582396593525, -0.3967382283601348, -0.03992559172921793, 0, 63.72958762196502,
                -0.16404339962244616, 1.0835251566291304, -0.05498805115633132, 0, 24.732407896706203,
                -0.16786010706155763, -0.5603416277695248, 1.6014850761964943, 0, 35.62982807460946,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Brown delicious browni filter (thanks Dominic Szablewski)
         */
        export function browni(): Mat54
        {
            return [
                0.5997023498159715, 0.34553243048391263, -0.2708298674538042, 0, 47.43192855600873,
                -0.037703249837783157, 0.8609577587992641, 0.15059552388459913, 0, -36.96841498319127,
                0.24113635128153335, -0.07441037908422492, 0.44972182064877153, 0, -7.562075277591283,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Vintage filter (thanks Dominic Szablewski)
         */
        export function vintage(): Mat54
        {
            return [
                0.6279345635605994, 0.3202183420819367, -0.03965408211312453, 0, 9.651285835294123,
                0.02578397704808868, 0.6441188644374771, 0.03259127616149294, 0, 7.462829176470591,
                0.0466055556782719, -0.0851232987247891, 0.5241648018700465, 0, 5.159190588235296,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * We don't know exactly what it does, kind of gradient map, but funny to play with!
         * @param desaturation - Tone values.
         * @param toned - Tone values.
         * @param lightColor - Tone values, example: `0xFFE580`
         * @param darkColor - Tone values, example: `0xFFE580`
         */
        export function colorTone(desaturation: number = 0.2, toned: number = 0.15, lightColor: Util.Color = new Util.Color(0xFFE580), darkColor: Util.Color = new Util.Color(0x338000)): Mat54
        {
            return [
                0.3, 0.59, 0.11, 0, 0,
                lightColor.r, lightColor.g, lightColor.b, desaturation, 0,
                darkColor.r, darkColor.g, darkColor.b, toned, 0,
                lightColor.r - darkColor.r, lightColor.g - darkColor.g, lightColor.b - darkColor.b, 0, 0,
            ];
        }

        /**
         * Night effect
         * @param intensity intensity of the night effect (0-1)
         */
        export function night(intensity: number = 0.1): Mat54
        {
            return [
                intensity * (-2.0), -intensity, 0, 0, 0,
                -intensity, 0, intensity, 0, 0,
                0, intensity, intensity * 2.0, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Predator effect
         * @param amount how much the predator feels his future victim
         */
        export function predator(amount: number): Mat54
        {
            return [
                // row 1
                11.224130630493164 * amount,
                -4.794486999511719 * amount,
                -2.8746118545532227 * amount,
                0 * amount,
                0.40342438220977783 * amount,
                // row 2
                -3.6330697536468506 * amount,
                9.193157196044922 * amount,
                -2.951810836791992 * amount,
                0 * amount,
                -1.316135048866272 * amount,
                // row 3
                -3.2184197902679443 * amount,
                -4.2375030517578125 * amount,
                7.476448059082031 * amount,
                0 * amount,
                0.8044459223747253 * amount,
                // row 4
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * LSD effect
         */
        export function lsd(): Mat54
        {
            return [
                2, -0.4, 0.5, 0, 0,
                -0.5, 2, -0.4, 0, 0,
                -0.4, -0.5, 3, 0, 0,
                0, 0, 0, 1, 0,
            ];
        }

        /**
         * Colorize replaces all colors. Similar to a tint effect except it applies greyscale before adding the new color.
         * @param scale Greyscale, controls brightness of colour applied (0-1, where 0 is black)
         * @param color
         */
        export function colorize(scale: number, color: RS.Util.Color): Mat54
        {
            const mat = greyscale(scale);
            const colorMatrix =
            [
                color.r, 0.0, 0.0, 0, 0,
                0.0, color.g, 0.0, 0, 0,
                0.0, 0.0, color.b, 0, 0,
                0, 0, 0, color.a, 0,
            ];
            return multiply(colorMatrix, mat);
        }
    }
}