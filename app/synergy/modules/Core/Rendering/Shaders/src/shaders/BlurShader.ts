namespace RS.Rendering.Shaders
{
    /**
     * An efficient Gaussian blur shader that blurs in a single direction.
     */
    export class BlurShaderProgram extends RS.Rendering.ShaderProgram
    {
         // 32 max floats - vColor = 28 available floats
         // 28 / 2 (vec2) = 14 free varying slots
         // 14 varying slots supports a radius of 6 (6 + 1 + 6 = 13)
        private static maxKernelSize = 14;

        // https://github.com/pixijs/pixi.js/blob/v3/src/core/renderers/webgl/shaders/PrimitiveShader.js
        private static _baseVertexShader =
        [
            'precision lowp float;',
            'attribute vec2 aVertexPosition;',
            'attribute vec2 aTextureCoord;',
            // 'attribute vec4 aColor;',

            'uniform mat3 projectionMatrix;',
            '{{UNIFORMS}}',

            // 'varying vec2 vTextureCoord;',
            // 'varying vec4 vColor;',
            '{{VARYINGS}}',

            'void main(void){',
            '   gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);',
            // '   vTextureCoord = aTextureCoord;',
            // '   vColor = vec4(aColor.rgb * aColor.a, aColor.a);',
            '   {{VERTEX_SHADER}}',
            '}'
        ].join("\n");

        private static _baseFragmentShader =
        [
            'precision mediump float;',

            'uniform sampler2D uSampler;',
            '{{UNIFORMS}}',

            // 'varying vec4 vColor;',
            '{{VARYINGS}}',

            'void main(void){',
            '   {{FRAGMENT_SHADER}}',
            '}'
        ].join('\n');

        public constructor(settings: BlurShaderProgram.Settings)
        {
            if (!BlurShaderProgram.verifySettings(settings)) { return; }
            const baseSettings: IShaderProgram.Settings =
            {
                vertexSource: BlurShaderProgram.constructVertexShader(settings),
                fragmentSource: BlurShaderProgram.constructFragmentShader(settings)
            };
            super(baseSettings);
        }

        /**
         * Constructs a vertex shader from the specified settings.
         */
        private static constructVertexShader(settings: BlurShaderProgram.Settings)
        {
            const kernelSize = settings.radius * 2 + 1;
            if (settings.dependentTextureReads)
            {
                return BlurShaderProgram._baseVertexShader
                    .replace("{{UNIFORMS}}", "")
                    .replace("{{VARYINGS}}", "varying vec2 vTextureCoord;")
                    .replace("{{VERTEX_SHADER}}", "vTextureCoord = aTextureCoord;");
            }
            else
            {
                const uniformsText = `uniform vec2 uPixelSize;`;
                const varyingsText = `varying vec2 vTexCoords[${kernelSize}];`;
                const vertexShader: string[] = [];
                for (let i = 0; i < kernelSize; i++)
                {
                    const pixelIndex = i - settings.radius;
                    vertexShader.push(`vTexCoords[${i}] = aTextureCoord + vec2(${(settings.direction.x * pixelIndex).toFixed(2)}, ${(settings.direction.y * pixelIndex).toFixed(2)}) * uPixelSize;`);
                }
                return BlurShaderProgram._baseVertexShader
                    .replace("{{UNIFORMS}}", uniformsText)
                    .replace("{{VARYINGS}}", varyingsText)
                    .replace("{{VERTEX_SHADER}}", vertexShader.join("\n"));
            }
        }

        /**
         * Constructs a fragment shader from the specified settings.
         */
        private static constructFragmentShader(settings: BlurShaderProgram.Settings)
        {
            const kernelSize = settings.radius * 2 + 1;
            if (settings.dependentTextureReads)
            {
                const uniformsText = `uniform vec2 uPixelSize;`;
                const varyingsText = `varying vec2 vTextureCoord;`;
                const fragmentShader: string[] = [];
                fragmentShader.push(`vec4 color = vec4(0.0);`);
                const kernel = Math.createGaussianKernel(settings.radius * 2 + 1);
                for (let i = 0; i < kernelSize; i++)
                {
                    const pixelIndex = i - settings.radius;
                    fragmentShader.push(`color += texture2D(uSampler, vTextureCoord + vec2(${(settings.direction.x * pixelIndex).toFixed(2)}, ${(settings.direction.y * pixelIndex).toFixed(2)}) * uPixelSize) * ${kernel[i]};`);
                }
                fragmentShader.push(`gl_FragColor = color;`);
                return BlurShaderProgram._baseFragmentShader
                    .replace("{{UNIFORMS}}", uniformsText)
                    .replace("{{VARYINGS}}", varyingsText)
                    .replace("{{FRAGMENT_SHADER}}", fragmentShader.join("\n"));
            }
            else
            {
                const varyingsText = `varying vec2 vTexCoords[${kernelSize}];`;
                const fragmentShader: string[] = [];
                fragmentShader.push(`vec4 color = vec4(0.0);`);
                const kernel = Math.createGaussianKernel(settings.radius * 2 + 1);
                for (let i = 0; i < kernelSize; i++)
                {
                    fragmentShader.push(`color += texture2D(uSampler, vTexCoords[${i}]) * ${kernel[i]};`);
                }
                fragmentShader.push(`gl_FragColor = color;`);
                return BlurShaderProgram._baseFragmentShader
                    .replace("{{UNIFORMS}}", "")
                    .replace("{{VARYINGS}}", varyingsText)
                    .replace("{{FRAGMENT_SHADER}}", fragmentShader.join("\n"));
            }
        }

        /**
         * Verifies the validity of the specified settings object.
         * If a test fails, false is returned and a warning log message printed.
         */
        private static verifySettings(settings: BlurShaderProgram.Settings): boolean
        {
            const maxRadius = ((BlurShaderProgram.maxKernelSize % 2) === 0 ? (BlurShaderProgram.maxKernelSize - 2) : (BlurShaderProgram.maxKernelSize - 1)) / 2;
            if (settings.radius > maxRadius)
            {
                Log.error(`Invalid radius size - ${settings.radius} is too high! (maximum is ${maxRadius})`);
                return false;
            }
            return true;
        }

        /** Creates a shader instance from this shader program. */
        public createShader(): BlurShader
        {
            return new BlurShader(this);
        }
    }

    export class BlurShader extends RS.Rendering.Shader
    {
        protected _shaderProgram: BlurShaderProgram;

        protected _uPixelSize: ShaderUniform.Vec2;

        public get shaderProgram() { return this._shaderProgram; }

        public constructor(shaderProgram: BlurShaderProgram)
        {
            super(shaderProgram);
            this._uPixelSize = this.uniforms["uPixelSize"] as ShaderUniform.Vec2;
            this._uPixelSize.value.x = 1.0;
            this._uPixelSize.value.y = 1.0;
            this._uPixelSize.inputSizeOp = ShaderUniform.Op.Divide;
        }
    }

    export namespace BlurShaderProgram
    {
        /**
         * Settings for the blur.
         */
        export interface Settings
        {
            /** Direction to blur in. Should be normalised. */
            direction: RS.Math.Vector2D;

            /** Radius of the blur. Acceptable values 1-6 */
            radius: number;

            /**
             * Whether to use dependent texture reads.
             * Reduces varying usage and lifts the radius limit, but costs in performance (ALOT).
             */
            dependentTextureReads?: boolean;
        }
    }
}