precision lowp float;

#pragma meta private
uniform sampler2D uSampler;

#pragma meta property=overlayTexture
uniform sampler2D uTex;

uniform vec2 uDimensions;
uniform vec2 uTextureDimensions;

#pragma meta default=1.0
uniform float uScale;

varying vec2 vTextureCoord;

void OverlayFragment()
{
    vec2 repeat = vec2(uDimensions.x / uTextureDimensions.x / uScale, uDimensions.y / uTextureDimensions.y / uScale);
    vec2 repeatTextureCoord = vec2(mod(vTextureCoord.x * repeat.x, 1.0), mod(vTextureCoord.y * repeat.y, 1.0));
    vec4 inColor = texture2D(uSampler, vTextureCoord);
    vec4 outputCol = texture2D(uTex, repeatTextureCoord);
    gl_FragColor = outputCol * inColor;
}

// A shader displaying a tiled texture on the original resolution and that can be scaled.
#pragma export Overlay Fragment=OverlayFragment