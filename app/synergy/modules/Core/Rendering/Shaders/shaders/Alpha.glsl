varying vec2 vTextureCoord;

#pragma meta private
uniform sampler2D uSampler;

// The alpha value to apply to the filtered display object
uniform float uAlpha;

void AlphaFragment()
{
   gl_FragColor = texture2D(uSampler, vTextureCoord) * uAlpha;
}

#pragma export Alpha filter Fragment=AlphaFragment