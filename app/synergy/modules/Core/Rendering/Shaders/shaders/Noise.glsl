precision highp float;

#include "NoiseUtil.glsl"

varying vec2 vTextureCoord;
varying vec4 vColor;

// How much noise to apply
#pragma meta default=1.0
uniform float uNoise;

// Random seed
#pragma meta default=1.0
uniform float uSeed;

#pragma meta private
uniform sampler2D uSampler;

void NoiseFragment()
{
    vec4 color = texture2D(uSampler, vTextureCoord);
    float randomValue = rand(gl_FragCoord.xy * uSeed);
    float diff = (randomValue - 0.5) * uNoise;

    // Un-premultiply alpha before applying the color matrix. See issue #3539.
    if (color.a > 0.0) {
        color.rgb /= color.a;
    }

    color.r += diff;
    color.g += diff;
    color.b += diff;

    // Premultiply alpha again.
    color.rgb *= color.a;

    gl_FragColor = color;
}

#pragma export Noise filter Fragment=NoiseFragment