varying vec2 vFilterCoord;
varying vec2 vTextureCoord;

// How much to scale the displacement effect by
uniform vec2 uScale;

#pragma meta private
uniform sampler2D uSampler;

// The displacement texture
#pragma meta property=displacementTexture
uniform sampler2D uMapSampler;

#pragma meta private
uniform vec4 filterArea;

#pragma meta private
uniform vec4 filterClamp;

void DisplacementFragment()
{
    vec4 map = texture2D(uMapSampler, vFilterCoord);

    map -= 0.5;
    map.xy *= uScale / filterArea.xy;

    gl_FragColor = texture2D(uSampler, clamp(vec2(vTextureCoord.x + map.x, vTextureCoord.y + map.y), filterClamp.xy, filterClamp.zw));
}

#pragma export Displacement filter Fragment=DisplacementFragment