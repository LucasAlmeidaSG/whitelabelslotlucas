namespace RS.Particles.Initialisers
{
    const repulsionVector = RS.Math.Vector2D();
    const directionVector = RS.Math.Vector2D();

    function doRepulse(speed: number | RS.Math.Range, particle: Behaviours.LinearMover.Particle)
    {
        if (repulsionVector.x === 0 && repulsionVector.y === 0)
        {
            // Pick a random direction if target and particle share position, or no target set.
            RS.Math.Vector2D.set(directionVector, 1, 0);
            RS.Math.Vector2D.rotate(directionVector, Math.random() * RS.Math.Angles.fullCircle, directionVector);
        }
        else
        {
            // Use unit repulsion vector.
            RS.Math.Vector2D.normalised(repulsionVector, directionVector);
        }

        const speedResult = RS.Is.number(speed) ? speed : Math.Range.pickNumber(speed);
        particle.xVel = (particle.xVel || 0) + directionVector.x * speedResult;
        particle.yVel = (particle.yVel || 0) + directionVector.y * speedResult;
    }

    function repulseRandom(speed: number | RS.Math.Range)
    {
        return function (particle: Behaviours.LinearMover.Particle)
        {
            RS.Math.Vector2D.set(repulsionVector, 0, 0);
            doRepulse(speed, particle);
        };
    }

    function repulseFrom(speed: number | RS.Math.Range, repulsingPosition: Math.Vector2D)
    {
        return function (particle: Behaviours.LinearMover.Particle)
        {
            RS.Math.Vector2D.subtract(particle, repulsingPosition, repulsionVector);
            doRepulse(speed, particle);
        };
    }

    export const Repulse = (speed: number | RS.Math.Range, repulsingPosition?: Math.Vector2D) =>
        repulsingPosition ? repulseFrom(speed, repulsingPosition) : repulseRandom(speed);
}