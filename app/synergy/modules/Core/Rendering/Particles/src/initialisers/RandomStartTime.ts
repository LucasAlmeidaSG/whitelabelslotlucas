namespace RS.Particles.Initialisers
{
    export function RandomStartTime(p: Behaviours.Animator.Particle): void
    {
        const asset = RS.Asset.Store.getAsset(p.asset);
        const spriteSheet = (asset.as(RS.Asset.RSSpriteSheetAsset) || asset.as(RS.Asset.SpriteSheetAsset)).asset as Rendering.ISpriteSheet;
        const animation = spriteSheet.getAnimation(p.animationName);
        const frame = Math.generateRandomInt(0, animation.frames.length);
        p.frame = frame;
    }
}