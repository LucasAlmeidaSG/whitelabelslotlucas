/// <reference path="Base.ts" />

namespace RS.Particles
{
    /**
     * Represents the action that should be taken on a particular particle.
     */
    export const enum Action
    {
        /** No action should be taken */
        None,

        /** The particle should be removed */
        Remove
    }

    const defaultBitmask = 0x7FFFFFFF;

    /**
     * A behaviour for a set of particles.
     */
    export abstract class Behaviour<TParticle extends Base = Base, TSettings extends Behaviour.Settings = Behaviour.Settings>
    {
        protected _bitmask: number;

        /**
         * Initialises a new instance of the ParticleBehaviour class.
         * @param bitmask    The bitmask to filter particles against.
         */
        public constructor(public readonly settings?: TSettings)
        {
            this._bitmask = settings ? settings.bitmask != null ? settings.bitmask : defaultBitmask : defaultBitmask;
        }

        /**
         * Gets if the specified particle should be affected by this behaviour.
         * @param p        The particle to initialise.
         * @returns        True if the particle should be affected by this behaviour.
         */
        public filter(p: TParticle): boolean
        {
            return (p.bitmask & this._bitmask) !== 0;
        }

        /**
         * Initialises the specified particle.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public abstract setupParticle(p: TParticle): Action;

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public abstract updateParticle(p: TParticle, deltaTime: number): Action;
    }

    export namespace Behaviour
    {
        /**
         * Settings for a generic behaviour.
         */
        export interface Settings
        {
            /** The bitmask to filter particles against. */
            bitmask?: number;
        }
    }
}
