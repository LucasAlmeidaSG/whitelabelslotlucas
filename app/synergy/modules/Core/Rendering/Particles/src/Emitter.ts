/// <reference path="Base.ts" />
/// <reference path="System.ts" />

namespace RS.Particles
{
    /**
     * Encapsulates a particle emitter for a particular system.
     */
    @HasCallbacks
    export abstract class Emitter<TParticle extends Base = Base, TSettings extends Emitter.Settings<TParticle> = Emitter.Settings<TParticle>> implements IDisposable
    {
        protected _disposed: boolean = false;
        protected _enabled: boolean = false;
        protected _rng: Math.MersenneTwister;

        protected _tickerHandle: IDisposable|null = null;
        protected _emitCount: number;
        protected _emitTime: number;
        protected _accum: number = 0;

        protected _weightFunction: (...params: any[]) => void = null;

        /** Gets if this emitter has been disposed. */
        public get isDisposed() { return this._disposed; }

        /** Gets or sets the emit mapping function. */
        @CheckDisposed
        public get weightFunction() { return this._weightFunction; }
        public set weightFunction(value) { this._weightFunction = value; }

        /** Gets or sets if this emitter is enabled. */
        @CheckDisposed
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            if (this._enabled === value) { return; }
            this._enabled = value;
            if (value)
            {
                this._tickerHandle = ITicker.get().add(this.tick, { kind: Ticker.Kind.RealTime });
            }
            else
            {
                if (this._tickerHandle)
                {
                    this._tickerHandle.dispose();
                    this._tickerHandle = null;
                }
            }
        }

        /** Gets or sets how many particles to emit each time. */
        @CheckDisposed
        public get emitCount() { return this._emitCount; }
        public set emitCount(value) { this._emitCount = value; }

        /** Gets or sets the time between each emit in ms. */
        @CheckDisposed
        public get emitTime() { return this._emitTime; }
        public set emitTime(value)
        {
            if (this.enabled)
            {
                this.enabled = false;
                this._emitTime = value;
                this.enabled = true;
            }
            else
            {
                this._emitTime = value;
            }
        }

        public constructor(public readonly system: System<TParticle>, public readonly settings: TSettings)
        {
            this._rng = settings.rng || new Math.MersenneTwister();
            this._emitCount = settings.emitCount != null ? settings.emitCount : 1;
            this._emitTime = settings.emitTime;
        }

        /**
         * Emits a single particle.
         */
        public emit(): void;

        /**
         * Emits multiple particles.
         */
        public emit(count: number): void;

        @CheckDisposed
        public emit(count: number = 1): void
        {
            if (this.system.isDisposed)
            {
                throw new Error("Tried to emit particle on a disposed particle system");
            }
            while (--count >= 0)
            {
                this.system.addParticle(this.generate());
            }
        }

        /**
         * Steps this emitter and the particle system forwards by the specified amount.
         * @param time Time period to step by (ms)
         */
        public step(time: number): void
        {
            if (!this._enabled) { return; }
            while (time > this._emitTime)
            {
                this.emit(this._emitCount);
                this.system.step(this._emitTime);
                time -= this._emitTime;
            }
            this._accum = 0.0;
        }

        /**
         * Disposes this emitter.
         */
        public dispose()
        {
            if (this._disposed) { return; }
            this.enabled = false;
            this._disposed = true;
        }

        @Callback
        protected tick(ev: Ticker.Event): void
        {
            const ticker = ITicker.get();
            this._accum += Math.min(ticker.maxDelta, ev.delta) * ticker.timeScale;
            while (this._accum > this._emitTime)
            {
                this.emit(this._emitCount);
                this._accum -= this._emitTime;
            }
        }

        /**
         * Generates a new particle object.
         */
        protected generate(): TParticle
        {
            return (this.settings.defaultParticle ? { ...(this.settings.defaultParticle as object) } : {}) as TParticle;
        }
    }

    export namespace Emitter
    {
        /**
         * Settings for a particle emitter.
         */
        export interface Settings<TParticle extends Base>
        {
            /** Properties to use for emitted particles. */
            defaultParticle?: Partial<TParticle>;

            /** Random number generator to use. */
            rng?: Math.MersenneTwister;

            /** Number of particles to emit each time. */
            emitCount?: number;

            /** Amount of time between each emit (in ms). */
            emitTime?: number;
        }
    }
}