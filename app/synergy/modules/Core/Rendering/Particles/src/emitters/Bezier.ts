namespace RS.Particles.Emitters
{
    /**
     * A particle emitter that emits particles along a bezier path.
     */
    export class Bezier<TParticle extends Base = Base> extends Emitter<TParticle, Bezier.Settings<TParticle>> implements IDisposable
    {
        protected _path: RS.Bezier.Path;
        protected _maxAlong: number = 1.0;
        protected _minAlong: number = 0.0;
        protected _pathLength: number;

        /**
         * Gets or sets the ratio along the path up to which we can spawn particles.
         * Scaled 0 - 1.
         * For example, 0.5 means we will only emit particles from the start of the path to half way along the path.
         */
        public get maxAlong() { return this._maxAlong; }
        public set maxAlong(value) { this._maxAlong = value; }

        /**
         * Gets or sets the ratio along the path from which we can spawn particles.
         * Scaled 0 - 1.
         * For example, 0.5 means we will only emit particles from half way along the path to the end of the path.
         */
        public get minAlong() { return this._minAlong; }
        public set minAlong(value) { this._minAlong = value; }

        public constructor(system: System<TParticle>, settings: Bezier.Settings<TParticle>)
        {
            super(system, settings);
            this._path = Is.assetReference(settings.path) ? RS.Asset.Store.getAsset(settings.path).asset as RS.Bezier.Path : settings.path;
            this._pathLength = this._path.length;
            if (settings.minAlong != null)
            {
                this.minAlong = settings.minAlong;
            }
            if (settings.maxAlong != null)
            {
                this.maxAlong = settings.maxAlong;
            }
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Bezier
        {
            return new Bezier(this.system, this.settings);
        }

        /**
         * Generates a new particle object.
         */
        protected generate(): TParticle
        {
            const p = super.generate();
            const len = (this._rng.random() * (this._maxAlong - this._minAlong) + this._minAlong) * this._pathLength;
            const pt = this._path.sample(len);
            p.x = pt.x;
            p.y = pt.y;
            if (this._weightFunction)
            {
                this._weightFunction(p, pt.weight);
            }
            return p;
        }
    }

    export namespace Bezier
    {
        /**
         * Settings for a bezier emitter.
         */
        export interface Settings<TParticle extends Base> extends Emitter.Settings<TParticle>
        {
            path: RS.Bezier.Path | RS.Asset.Reference<"path">;

            maxAlong?: number;
            minAlong?: number;
        }
    }
}