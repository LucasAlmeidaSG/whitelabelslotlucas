namespace RS.Particles.Emitters
{
    export class Point extends RS.Particles.Emitter<RS.Particles.Base, Point.Settings>
    {
        public readonly position = RS.Math.Vector2D.clone(this.settings.position);

        public constructor(system: RS.Particles.System<RS.Particles.Base>, settings: Point.Settings = Emitters.Point.defaultSettings)
        {
            super(system, settings);
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Point
        {
            return new Point(this.system, this.settings);
        }

        protected generate()
        {
            const p = super.generate();
            RS.Math.Vector2D.copy(this.position, p);
            return p;
        }
    }

    export namespace Point
    {
        export interface Settings extends RS.Particles.Emitter.Settings<RS.Particles.Base>
        {
            position?: RS.Math.Vector2D;
        }

        export const defaultSettings: Settings = { position: RS.Math.Vector2D.zero, emitCount: 1, emitTime: 1000 / 60 };
    }
}