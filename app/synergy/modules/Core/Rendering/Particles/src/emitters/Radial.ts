namespace RS.Particles.Emitters
{
    /**
     * A particle emitter that emits particles in a circle.
     */
    export class Radial<TParticle extends Base = Base> extends Emitter<TParticle, Radial.Settings<TParticle>> implements IDisposable
    {
        protected _origin: Math.Vector2D;
        protected _radius: number;

        protected _tmpPos: RS.Math.Vector2D = RS.Math.Vector2D();

        public constructor(public readonly system: System<TParticle>, public readonly settings: Radial.Settings<TParticle>)
        {
            super(system, settings);
            this._origin = RS.Math.Vector2D.clone(settings.origin);
            this._radius = settings.radius;
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Radial
        {
            return new Radial(this.system, this.settings);
        }

        /**
         * Gets or sets the origin of this emitter.
         */
        public get origin(): Readonly<Math.Vector2D> { return this._origin; }
        public set origin(value)
        {
            this._origin.x = value.x;
            this._origin.y = value.y;
        }

        /**
         * Gets or sets the radius of this emitter.
         */
        public get radius() { return this._radius; }
        public set radius(value) { this._radius = value; }

        /**
         * Generates a new particle object.
         */
        protected generate(): TParticle
        {
            const p = super.generate();
            this._tmpPos.x = this._rng.randomRange(0.0, Math.PI * 2.0);
            this._tmpPos.y = Math.sqrt(this._rng.random());
            if (this._weightFunction)
            {
                this._weightFunction(this._tmpPos);
            }
            p.x = this.origin.x + Math.cos(this._tmpPos.x) * this._tmpPos.y * this._radius;
            p.y = this.origin.y + Math.sin(this._tmpPos.x) * this._tmpPos.y * this._radius;
            return p;
        }

        // weight functions examples
        //
        // particles concentrated on the external circle
        // (pos) => {pos.y = RS.Ease.circOut(pos.y);}
        //
        // particles concentrated on the origin point
        // (pos) => {pos.y = RS.Ease.circIn(pos.y);}
        //
        // radar
        // (pos) => {pos.x = (RS.Ease.circOut(pos.x)+timer.elapsed/1000)%(2*Math.PI);}
    }

    export namespace Radial
    {
        /**
         * Settings for a box emitter.
         */
        export interface Settings<TParticle extends Base> extends Emitter.Settings<TParticle>
        {
            origin: Math.Vector2D;
            radius: number;
        }
    }
}