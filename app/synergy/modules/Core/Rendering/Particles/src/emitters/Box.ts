namespace RS.Particles.Emitters
{
    /**
     * A particle emitter that emits particles in a rectangle.
     */
    export class Box<TParticle extends Base = Base> extends Emitter<TParticle, Box.Settings<TParticle>> implements IDisposable
    {
        protected _rect: RS.Rendering.Rectangle;
        protected _tmpPos: RS.Math.Vector2D = RS.Math.Vector2D();

        public constructor(public readonly system: System<TParticle>, public readonly settings: Box.Settings<TParticle>)
        {
            super(system, settings);
            this.rect = settings.rect;
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Box
        {
            return new Box(this.system, this.settings);
        }

        /**
         * Gets or sets the rectangle of this emitter.
         */
        public get rect(): RS.Rendering.ReadonlyRectangle { return this._rect; }
        public set rect(value)
        {
            this._rect = value.clone();
        }

        /**
         * Generates a new particle object.
         */
        protected generate(): TParticle
        {
            const p = super.generate();
            this._tmpPos.x = this._rng.random();
            this._tmpPos.y = this._rng.random();
            if (this._weightFunction)
            {
                this._weightFunction(this._tmpPos);
            }
            p.x = this._tmpPos.x * this._rect.w + this._rect.x;
            p.y = this._tmpPos.y * this._rect.h + this._rect.y;
            return p;
        }

        // weight functions examples
        //
        // particles concentrated on the right side
        // (pos) => {pos.x = RS.Ease.circOut(pos.x);}
        //
        // particles following lines
        // (pos) => {
        //     pos.x = Math.floor(pox.x * 15) / 15;
        //     pos.y = Math.floor(pox.y * 10) / 10;
        // }
        //
        // particles on sides
        // (pos) => {if (pos.x > 0.1 && pos.x < 0.9) {pos.y = pos.y < 0.5 ? pos.y / 5 : 1.1 - pos.y / 5;}}
    }

    export namespace Box
    {
        /**
         * Settings for a box emitter.
         */
        export interface Settings<TParticle extends Base> extends Emitter.Settings<TParticle>
        {
            rect: RS.Rendering.ReadonlyRectangle;
        }
    }
}
