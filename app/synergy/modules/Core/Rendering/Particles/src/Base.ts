namespace RS.Particles
{
    /**
     * Represents the most basic form of particle that can exist.
     */
    export interface Base
    {
        // Bitmask
        bitmask: number;

        // Standard transform properties
        x: number;
        y: number;
        scaleX?: number;
        scaleY?: number;
        rotation?: number;
        alpha?: number;
        regX?: number;
        regY?: number;
        tint?: Util.Color;

        // Lifetime
        age?: number;
        maxAge?: number;

        // Particle asset reference.
        asset?: Asset.IImageAsset | Asset.ISpriteSheetAsset | Asset.SpriteSheetReference | Asset.RSSpriteSheetReference | Asset.ImageReference;
        animationName?: string;
        frame?: number;

        // Particle texture.
        texture?: Rendering.ISubTexture;

        /** Physical representation. */
        object: Rendering.IFastBitmap;
        shape?: Rendering.ReadonlyShape;
    }
}