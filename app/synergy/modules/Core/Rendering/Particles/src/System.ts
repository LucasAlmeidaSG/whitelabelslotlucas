/// <reference path="Base.ts" />
/// <reference path="Behaviour.ts" />

namespace RS.Particles
{
    let pool: ResourcePool<Rendering.IFastBitmap>;

    export function getPooledCount() { return pool ? pool.pooledCount : 0; }

    /** Clears the global particle pool. */
    export function clearPool()
    {
        if (pool) { pool.dispose(); }
        pool = null;
    }

    /**
     * A generic extensible particle system.
     */
    @HasCallbacks
    export class System<TParticle extends Base> extends RS.Rendering.FastContainer implements IDisposable
    {
        @AutoDispose
        public readonly onEmpty = createSimpleEvent();

        protected _particles: Collection<TParticle>;
        protected _lastParticleCount: number = 0;
        protected _behaviours: Behaviour<TParticle>[];
        protected _template: TParticle;
        protected _enabled: boolean;

        private get _pool() { return pool || (pool = new ResourcePool<Rendering.IFastBitmap>({ allocator: () => new Rendering.FastBitmap(null) })); }

        /**
         * Initialises a new instance of the ParticleSystem class.
         */
        constructor(public settings: System.Settings<TParticle>)
        {
            // Base class
            super({ batchSize: settings.maxParticleCount });

            // Initialise
            this._particles = new Collection<TParticle>();
            this._behaviours = [ ...settings.behaviours ];
            this._template = settings.initialParticle as TParticle;

            // Start enabled
            this.enabled = true;
        }

        /**
         * Gets or sets if this system is enabled or not.
         */
        @RS.CheckDisposed
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            if (this._enabled === value) { return; }
            if (value)
            {
                this._enabled = true;
                Ticker.registerTickers(this);
            }
            else
            {
                this._enabled = false;
                Ticker.unregisterTickers(this);
            }
        }

        /**
         * Steps this particle system forwards by the specified amount.
         * @param time Time period to step by (ms)
         */
        public step(time: number): void
        {
            if (!this._enabled) { return; }
            this.updateParticles(time);
        }

        /**
         * Adds a particle instance to this system.
         * @param p        The particle instance to add.
         */
        @RS.CheckDisposed
        public addParticle(p: TParticle): void
        {
            // Default bitmask
            if (p.bitmask == null)
            {
                p.bitmask = 1;
            }

            // Init via template
            if (this._template)
            {
                for (const key in this._template)
                {
                    if (p[key] == null)
                    {
                        p[key] = this._template[key];
                    }
                }
            }

            // Init via all initialisers
            for (const initialiser of this.settings.initialisers)
            {
                initialiser(p);
            }

            // Init via all behaviours
            const behaviours = this._behaviours;
            let action = Action.None;
            for (const b of this._behaviours)
            {
                if (b.filter(p))
                {
                    action = Math.max(action, b.setupParticle(p));
                }
            }
            if (action === Action.Remove) { return; }

            // Default properties
            if (p.alpha == null)    { p.alpha = 1.0; }
            if (p.x == null)        { p.x = 0.0; }
            if (p.y == null)        { p.y = 0.0; }
            if (p.scaleX == null)   { p.scaleX = 1.0; }
            if (p.scaleY == null)   { p.scaleY = 1.0; }
            if (p.rotation == null) { p.rotation = 0.0; }
            if (p.regX == null)     { p.regX = 0.5; }
            if (p.regY == null)     { p.regY = 0.5; }
            if (p.age == null)      { p.age = 0; }
            if (p.maxAge == null)   { p.maxAge = Number.MAX_VALUE; }
            if (p.tint == null)     { p.tint = Util.Colors.white; }

            // Create sprite

            // Get texture or sprite-sheet from asset.
            if (p.asset && !p.texture)
            {
                if (Is.assetReference(p.asset))
                {
                    const storeAsset = RS.Asset.Store.getAsset(p.asset);
                    const imageAsset = storeAsset.as(Asset.ImageAsset);
                    if (imageAsset)
                    {
                        const baseTexture = imageAsset.asset;
                        p.texture = new Rendering.SubTexture(baseTexture);
                    }
                    else
                    {
                        const spriteSheetAsset = storeAsset.as(Asset.SpriteSheetAsset) || storeAsset.as(Asset.RSSpriteSheetAsset);
                        if (spriteSheetAsset)
                        {
                            const spriteSheet = (spriteSheetAsset.asset as Rendering.ISpriteSheet);
                            const frameNo = p.animationName ? spriteSheet.getAnimation(p.animationName).frames[p.frame || 0] : (p.frame || 0);
                            const frame = spriteSheet.getFrame(frameNo);
                            p.texture = frame.imageData;
                        }
                    }
                }
                else
                {
                    // Blank for now
                }
            }

            if (!p.object && p.texture)
            {
                // Get bitmap from texture.
                p.object = this._pool.get();
                p.object.texture = p.texture;
            }

            // Add sprite to system
            if (p.object)
            {
                p.object.tint = p.tint;
                p.object.anchorX = p.object.anchorY = 0.5;
                p.object.setTransform(p.x, p.y, p.scaleX, p.scaleY, p.rotation, p.regX, p.regY);
                p.object.alpha = p.alpha;
                this.addChild(p.object);
            }

            // Insert
            this._particles.addItem(p);
        }

        /**
         * Removes a particle instance from this system.
         * @param p        The particle instance to remove.
         */
        @RS.CheckDisposed
        public removeParticle(p: TParticle): void
        {
            this._particles.removeItem(p);
            if (p.object)
            {
                this.releaseParticleObject(p.object);
            }
        }

        /**
         * Resets age of particles
         */
        @RS.CheckDisposed
        public resetParticleAge(): void
        {
            this._particles.iterate((p) => p.age = 0);
        }

        /**
         * Cleans up this particle system
         */
        public dispose(): void
        {
            if (this.isDisposed) { return; }
            this.enabled = false;
            this._particles.iterate((p) => p.object && this.releaseParticleObject(p.object));
            this._particles.clear();
            this._particles = null;
            this._behaviours.length = 0;
            this._behaviours = null;
            Ticker.unregisterTickers(this);
            super.dispose();
        }

        /**
         * Handles a tick.
         */
        @RS.Tick({ priority: Ticker.Priority.PreRender })
        protected tick(ev: Ticker.Event): void
        {
            //let curTime = (performance && performance.now) ? performance.now() : Date.now();
            //let deltaTime = curTime - this._lastTimestamp;
            //this._lastTimestamp = curTime;
            // if (ev.paused) { return; }
            this.updateParticles(ev.delta);
            if (this._lastParticleCount > 0 && this._particles.count === 0) { this.onEmpty.publish(); }
            this._lastParticleCount = this._particles.count;
        }

        private releaseParticleObject(object: RS.Rendering.IFastBitmap)
        {
            this.removeChild(object);
            this._pool.release(object);
        }

        /**
         * Updates all particles on this system over the specified timeframe.
         * @param deltaMilliseconds        The timeframe to update over.
         */
        private updateParticles(deltaMilliseconds: number): void
        {
            if (this._particles == null)
            {
                Ticker.unregisterTickers(this);
                return;
            }
            const deltaSeconds = deltaMilliseconds / 1000;
            this._particles.iterate((p) => this.updateParticle(deltaMilliseconds, deltaSeconds, p));
        }

        @Callback
        private updateParticle(deltaMilliseconds: number, deltaSeconds: number, p: TParticle): void
        {
            p.age += deltaMilliseconds;
            if (p.age >= p.maxAge)
            {
                this.removeParticle(p);
                return;
            }

            let action = Action.None;
            for (const b of this._behaviours)
            {
                if (b.filter(p))
                {
                    action = Math.max(action, b.updateParticle(p, deltaSeconds));
                }
            }
            if (action === Action.Remove)
            {
                this.removeParticle(p);
                return;
            }
            if (p.object)
            {
                p.object.tint = p.tint;
                p.object.setTransform(p.x, p.y, p.scaleX, p.scaleY, p.rotation, p.regX, p.regY);
                p.object.alpha = p.alpha;
            }
        }

    }

    export namespace System
    {
        /**
         * Settings for initialising a particle system.
         */
        export interface Settings<TParticle extends Base>
        {
            behaviours: Behaviour<TParticle>[];
            initialParticle: Partial<TParticle>;
            initialisers: ((p: TParticle) => void)[];
            maxParticleCount: number;
        }
    }
}
