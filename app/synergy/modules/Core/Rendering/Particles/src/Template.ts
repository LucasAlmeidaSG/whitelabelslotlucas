/// <reference path="Base.ts" />

namespace RS.Particles
{
    /**
     * A template used as an intermediary to assemble a particle system.
     */
    export interface ITemplate<TParticle extends Base>
    {
        /**
         * Adds a behaviour to this template.
         */
        addBehaviour<TBehaviourParticle extends Base>(behaviour: Behaviour<TBehaviourParticle>): ITemplate<TParticle & TBehaviourParticle>;

        /**
         * Sets the default particle object for this template.
         */
        setInitialParticle(initialParticle: Partial<TParticle>): ITemplate<TParticle>;

        /**
         * Sets the maximum particle count for this template.
         */
        setMaxParticleCount(max: number): ITemplate<TParticle>;

        /**
         * Adds a function to this template which is called whenever a new particle is created.
         * This can be used to initialise particle properties, e.g. from an rng.
         */
        addParticleInitialiser(initialiser: (p: TParticle) => void): ITemplate<TParticle>;

        /**
         * Creates a new particle system from this template.
         */
        create(): System<TParticle>;

        /**
         * Returns a copy of this template
         */
        clone(): ITemplate<TParticle>;
    }

    class Template<TParticle extends Base> implements ITemplate<TParticle>
    {
        private _behaviours: Behaviour[] = [];
        private _initialParticle: Partial<TParticle> | null = null;
        private _particleInitialisers: ((p: TParticle) => void)[] = [];
        private _maxParticleCount: number;

        public clone(): Template<TParticle>
        {
            const clone = new Template<TParticle>();
            for (const behaviour of this._behaviours)
            {
                clone.addBehaviour(behaviour);
            }
            clone.setInitialParticle(this._initialParticle);
            for (const initialiser of this._particleInitialisers)
            {
                clone.addParticleInitialiser(initialiser);
            }
            clone.setMaxParticleCount(this._maxParticleCount);
            return clone;
        }

        /**
         * Adds a behaviour to this template.
         */
        public addBehaviour<TBehaviourParticle extends Base>(behaviour: Behaviour<TBehaviourParticle>): ITemplate<TParticle & TBehaviourParticle>
        {
            this._behaviours.push(behaviour);
            return this as any;
        }

        /**
         * Sets the default particle object for this template.
         */
        public setInitialParticle(initialParticle: Partial<TParticle>): ITemplate<TParticle>
        {
            this._initialParticle = initialParticle;
            return this;
        }

        /**
         * Sets the maximum particle count for this template.
         */
        public setMaxParticleCount(max: number): ITemplate<TParticle>
        {
            this._maxParticleCount = max;
            return this;
        }

        /**
         * Adds a function to this template which is called whenever a new particle is created.
         * This can be used to initialise particle properties, e.g. from an rng.
         */
        public addParticleInitialiser(initialiser: (p: TParticle) => void): ITemplate<TParticle>
        {
            this._particleInitialisers.push(initialiser);
            return this;
        }

        /**
         * Creates a new particle system from this template.
         */
        public create(): System<TParticle>
        {
            return new System<TParticle>({
                behaviours: this._behaviours,
                initialParticle: this._initialParticle,
                initialisers: this._particleInitialisers,
                maxParticleCount: this._maxParticleCount
            });
        }
    }

    /**
     * Creates a new particle system template.
     */
    export function template(): ITemplate<Base> { return new Template(); }
}