/// <reference path="../Behaviour.ts" />

namespace RS.Particles.Behaviours
{
    /**
     * A behaviour that moves particles based on cartesian velocity properties.
     */
    export class LinearMover extends Behaviour<LinearMover.Particle>
    {
        /**
         * Returns a copy of this behaviour
         */
        public clone(): LinearMover
        {
            return new LinearMover(this.settings);
        }

        /**
         * Initialises the specified particle.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public setupParticle(p: LinearMover.Particle): Action
        {
            // Setup defaults
            if (p.xVel == null)
            {
                p.xVel = 0.0;
            }
            if (p.yVel == null)
            {
                p.yVel = 0.0;
            }
            if (p.rotVel == null)
            {
                p.rotVel = 0.0;
            }

            // No action
            return Action.None;
        }

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public updateParticle(p: LinearMover.Particle, deltaTime: number): Action
        {
            // Integrate speeds
            p.x += p.xVel * deltaTime;
            p.y += p.yVel * deltaTime;
            p.rotation += p.rotVel * deltaTime;

            // No action
            return Action.None;
        }
    }

    export namespace LinearMover
    {
        /**
         * Represents a particle with moving properties.
         */
        export interface Particle extends Base
        {
            /** Linear speed along the X axis. */
            xVel?: number;

            /** Linear speed along the Y axis. */
            yVel?: number;

            /** Rotational speed about the (theoretical) Z axis. */
            rotVel?: number;
        }
    }
}
