namespace RS.Particles.Behaviours
{
    export class Animator extends Behaviour<Animator.Particle, Animator.Settings>
    {
        private readonly _perMillisec = 1 / 1000;

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Animator
        {
            return new Animator(this.settings);
        }

        public setupParticle(p: Animator.Particle)
        {
            const asset = RS.Asset.Store.getAsset(p.asset);
            const spriteSheet = (asset.as(RS.Asset.RSSpriteSheetAsset) || asset.as(RS.Asset.SpriteSheetAsset)).asset as Rendering.ISpriteSheet;
            if (spriteSheet)
            {
                p.spriteSheet = spriteSheet;
                if (p.framerate == null)
                {
                    if (this.settings.framerate)
                    {
                        p.framerate = this.settings.framerate;
                    }
                    else
                    {
                        p.framerate = spriteSheet.framerate;
                    }
                }
            }
            return Action.None;
        }

        public updateParticle(p: Animator.Particle)
        {
            const spriteSheet = p.spriteSheet;

            const animation = spriteSheet.getAnimation(p.animationName, p.reverse);
            const fpms = p.framerate * this._perMillisec;

            const frameOffset = p.frame | 0;
            const animationFrameNo = frameOffset + Math.floor(p.age * fpms);
            const restrictedAnimFrameNo = this.restrict(animationFrameNo, animation.frames.length);

            const frameNo = animation.frames[restrictedAnimFrameNo];
            p.texture = spriteSheet.getFrame(frameNo).imageData;
            p.object.texture = p.texture;

            return Action.None;
        }

        private restrict(value: number, frameCount: number)
        {
            return this.settings.loop ? Math.wrap(value, 0, frameCount) : Math.clamp(value, 0, frameCount - 1);
        }
    }

    export namespace Animator
    {
        export interface Settings extends Behaviour.Settings
        {
            loop?: boolean;
            framerate?: number;
        }

        export interface Particle extends Base
        {
            /** Spritesheet used for animation. */
            spriteSheet?: Rendering.ISpriteSheet;
            /** Framerate of this particle. */
            framerate?: number;
            /** Reverse the playback of the animation */
            reverse?: boolean;
        }
    }
}
