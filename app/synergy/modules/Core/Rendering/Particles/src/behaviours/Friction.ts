/// <reference path="../Behaviour.ts" />
/// <reference path="LinearMover.ts" />

namespace RS.Particles.Behaviours
{
    /** Simulates friction by applying a damping force to each particle's velocity. */
    export class Friction extends RS.Particles.Behaviour<RS.Particles.Behaviours.LinearMover.Particle, Friction.Settings>
    {
        private _damping: number;

        public constructor(settings: Friction.Settings)
        {
            super(settings);
            this._damping = Math.clamp(1 - settings.coefficient, 0, 1);
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Friction
        {
            return new Friction(this.settings);
        }

        public setupParticle(p: RS.Particles.Behaviours.LinearMover.Particle)
        {
            return RS.Particles.Action.None;
        }

        public updateParticle(p: RS.Particles.Behaviours.LinearMover.Particle, deltaTime: number)
        {
            p.xVel *= this._damping;
            p.yVel *= this._damping;
            p.rotVel *= this._damping;
            return RS.Particles.Action.None;
        }
    }

    export namespace Friction
    {
        export interface Settings extends RS.Particles.Behaviour.Settings
        {
            coefficient: number;
        }
    }
}
