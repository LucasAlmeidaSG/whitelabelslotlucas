/// <reference path="../Behaviour.ts" />
/// <reference path="LinearMover.ts" />

namespace RS.Particles.Behaviours
{
    /**
     * A behaviour that applies an acceleration to particles.
     * Depends on a linear mover behaviour to actually move the particles.
     */
    export class Accelerator extends Behaviour<LinearMover.Particle, Accelerator.Settings>
    {
        /**
         * Returns a copy of this behaviour
         */
        public clone(): Accelerator
        {
            return new Accelerator(this.settings);
        }

        /**
         * Initialises the specified particle.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         **/
        public setupParticle(p: LinearMover.Particle): Action
        {
            // Setup defaults
            if (p.xVel == null)
            {
                p.xVel = 0.0;
            }
            if (p.yVel == null)
            {
                p.yVel = 0.0;
            }

            // No action
            return Action.None;
        }

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public updateParticle(p: LinearMover.Particle, deltaTime: number): Action
        {
            // Integrate acceleration
            p.xVel += this.settings.acceleration.x * deltaTime;
            p.yVel += this.settings.acceleration.y * deltaTime;

            // No action
            return Action.None;
        }
    }

    export namespace Accelerator
    {
        export interface Settings extends Behaviour.Settings
        {
            acceleration: Math.Vector2D;
        }
    }
}
