/// <reference path="../Behaviour.ts" />

namespace RS.Particles.Behaviours
{
    /**
     * A behaviour that moves particles on a quadratic curve.
     */
    export class QuadraticCurveMover extends Behaviour<QuadraticCurveMover.Particle>
    {
        /**
         * Returns a copy of this behaviour
         */
        public clone(): QuadraticCurveMover
        {
            return new QuadraticCurveMover(this.settings);
        }

        /**
         * Initialises the specified particle.
         * @param p		The particle to initialise.
         * @returns		The action to take.
         */
        public setupParticle(p: QuadraticCurveMover.Particle): Action
        {
            // Setup defaults
            if (p.w == null) { p.w = 0; }
            if (p.h == null) { p.h = 0; }
            if (p.controlCoef == null) { p.controlCoef = 0; }
            if (p.maxAge == null) { p.maxAge = 0; }

            if (p.startPt == null) { p.startPt = { x: p.x, y: p.y }; }
            if (p.endPt == null) { p.endPt = { x: p.x + p.w, y: p.y }; }
            if (p.controlPt == null) { p.controlPt = { x: p.x + p.w * p.controlCoef, y: p.y - p.h }; }

            // No action
            return Action.None;
        }

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p		The particle to initialise.
         * @returns		The action to take.
         */
        public updateParticle(p: QuadraticCurveMover.Particle, deltaTime: number): Action
        {
            // updates the particle position on the curve
            const percent = p.age / p.maxAge;
            const x = (1 - percent) * (1 - percent) * p.startPt.x + 2 * (1 - percent) * percent * p.controlPt.x + percent * percent * p.endPt.x;
            const y = (1 - percent) * (1 - percent) * p.startPt.y + 2 * (1 - percent) * percent * p.controlPt.y + percent * percent * p.endPt.y;

            p.x = x;
            p.y = y;

            // No action
            return Action.None;
        }
    }

    export namespace QuadraticCurveMover
    {
        /**
         * Represents a particle with moving properties.
         * startPt, endPt and  controlPt can be either calculated
         * using w, h and controlCoef properties, either directly passed
         */
        export interface Particle extends Base
        {
            /** The width of the curve, used to calculate the end point */
            w?: number;

            /** The height of the curve, used to calculate control point */
            h?: number;

            /** a coefficient represent the percent the control point is situated between start point and end point  */
            controlCoef?: number;

            /**  start point of the curve */
            startPt?: Math.Vector2D;

            /**  end point of the curve */
            endPt?: Math.Vector2D;

            /**  control point of the curve */
            controlPt?: Math.Vector2D;
        }
    }
}
