/// <reference path="../Behaviour.ts" />

namespace RS.Particles.Behaviours
{
    /**
     * A specific tweening instruction.
     */
    interface Instruction
    {
        key: string;
        minAge: number;
        maxAge: number;
        fromValue?: number;
        valueRange: number;
        toValue: number;
        fromAge: number;
        ageRange: number;
    }

    /**
     * A behaviour that fades certain properties at certain lifetimes.
     */
    export class Tweener extends Behaviour<Base, Tweener.Settings>
    {
        private _instructions: Instruction[];

        public constructor(settings: Tweener.Settings)
        {
            super(settings);

            // Collapse the map into a set of "instructions"
            const insts: Instruction[] = this._instructions = [];
            for (const key in settings.propertyMap)
            {
                if(settings.propertyMap[key]) {
                    const val = settings.propertyMap[key];
                    if (Is.array(val))
                    {
                        for (const subVal of val)
                        {
                            insts.push(Tweener.toInstruction(key, subVal));
                        }
                    }
                    else
                    {
                        insts.push(Tweener.toInstruction(key, val));
                    }
                }
            }

            // Sort the instructions and work out dead zones
            // insts.sort((a, b) => a.fromAge - b.fromAge);
            // for (let i = 0, l = insts.length; i < l; ++i)
            // {
            //     const inst = insts[i];

            //     // First item?
            //     if (i === 0)
            //     {
            //         inst.minAge = 0;
            //     }
            //     else
            //     {
            //         inst.minAge = insts[i - 1].maxAge;
            //     }

            //     // Last item?
            //     if (i === l - 1)
            //     {
            //         inst.maxAge = Infinity;
            //     }
            //     else
            //     {
            //         inst.maxAge = insts[i + 1].minAge;
            //     }
            // }
        }

        /**
         * Converts a tween property to an instruction.
         * @param prop    The property to convert
         * @returns        The instruction that applies the property.
         */
        private static toInstruction(key: string, prop: Tweener.TweenProperty): Instruction
        {
            return {
                key: key,
                fromAge: prop.fromAge,
                ageRange: prop.toAge - prop.fromAge,
                fromValue: prop.fromValue,
                valueRange: prop.toValue - prop.fromValue,
                toValue: prop.toValue,
                minAge: prop.fromAge,
                maxAge: prop.toAge
            };
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Tweener
        {
            return new Tweener(this.settings);
        }

        /**
         * Initialises the specified particle.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public setupParticle(p: Base): Action
        {
            return Action.None;
        }

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public updateParticle(p: Base, deltaTime: number): Action
        {
            // Apply all instructions
            for (const inst of this._instructions)
            {
                if (p.age >= inst.minAge && p.age <= inst.maxAge)
                {
                    let delta = (p.age - inst.fromAge) / inst.ageRange;
                    delta = delta < 0 ? 0 : delta > 1 ? 1 : delta;
                    if (inst.fromValue != null)
                    {
                        p[inst.key] = inst.valueRange * delta + inst.fromValue;
                    }
                    else
                    {
                        const key = `_from${inst.key}`;
                        const fromValue = (p[key] != null) ? p[key] : (p[key] = p[inst.key]);
                        p[inst.key] = (inst.toValue - fromValue) * delta + fromValue;
                    }
                }
            }
            return Action.None;
        }
    }

    export namespace Tweener
    {
        /**
         * Details of how to tween a numeric property.
         */
        export interface TweenProperty
        {
            fromValue?: number;
            toValue: number;
            fromAge: number;
            toAge: number;
        }

        /**
         * A set of properties to tween.
         */
        export interface Settings<TParticle extends Base = Base> extends Behaviour.Settings
        {
            propertyMap: { [K in keyof TParticle]?: TweenProperty | TweenProperty[]; };
        }
    }
}
