/// <reference path="../Behaviour.ts" />
/// <reference path="LinearMover.ts" />

namespace RS.Particles.Behaviours
{
    /**
     * A behaviour that applies an acceleration to particles based on individual particle properties.
     * Depends on a linear mover behaviour to actually move the particles.
     */
    export class Acceleration extends Behaviour<Acceleration.Particle>
    {
        /**
         * Returns a copy of this behaviour
         */
        public clone(): Acceleration
        {
            return new Acceleration(this.settings);
        }

        /**
         * Initialises the specified particle.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         **/
        public setupParticle(p: Acceleration.Particle): Action
        {
            // Initialise acceleration if needed.
            if (p.xAccel == null) { p.xAccel = 0.0; }
            if (p.yAccel == null) { p.yAccel = 0.0; }
            if (p.rotAccel == null) { p.rotAccel = 0.0; }

            return Action.None;
        }

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public updateParticle(p: Acceleration.Particle, deltaTime: number): Action
        {
            p.xVel += p.xAccel * deltaTime;
            p.yVel += p.yAccel * deltaTime;
            p.rotVel += p.rotAccel * deltaTime;

            return Action.None;
        }
    }

    export namespace Acceleration
    {
        export interface Particle extends LinearMover.Particle
        {
            /** Linear acceleration along the X axis. */
            xAccel: number;
            /** Linear acceleration along the Y axis. */
            yAccel: number;
            /** Angular acceleration about the (theoretical) Z axis. */
            rotAccel: number;
        }
    }
}
