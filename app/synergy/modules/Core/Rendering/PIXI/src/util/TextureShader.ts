/** @internal  */
namespace RS.Rendering.Pixi
{
    const shaderPrograms: { [key: number]: Shaders.ImageTextureGraphicsShaderProgram } = {};

    function generateHash(transparentToWhite: boolean, scaleToFit: boolean): number
    {
        const bTransparentToWhite = transparentToWhite ? 1 : 0;
        const bScaleToFit = scaleToFit ? 1 : 0;
        return bTransparentToWhite + (bScaleToFit << 1);
    }

    function getProgram(transparentToWhite: boolean, scaleToFit: boolean): Shaders.ImageTextureGraphicsShaderProgram
    {
        const hash = generateHash(transparentToWhite, scaleToFit);
        if (shaderPrograms[hash])
        {
            return shaderPrograms[hash];
        }
        return (shaderPrograms[hash] = new Shaders.ImageTextureGraphicsShaderProgram({transparentToWhite, scaleToFit}))
    }

    export function getShaderForTexture(tex: Rendering.ISubTexture, settings: TextOptions.ImageData): RS.Rendering.IShader
    {
        const shouldBeOpaque = settings.transparentToWhite != null ? settings.transparentToWhite : false;
        const shouldScaleToFit = settings.scaleToFit != null ? settings.scaleToFit : true;
        
        const shaderProgram = getProgram(shouldBeOpaque, shouldScaleToFit);
        const shader = shaderProgram.createShader();
        shader.relativeSize = getRelativeSize({w: tex.texture.width, h: tex.texture.height}, tex.frame);
        shader.imageTex = tex.texture;
        shader.imageRect = RS.Math.Vector4D(tex.frame.x, tex.frame.y, tex.frame.h, tex.frame.w);
        if (settings.uvPosition != null) { shader.texUVPosition = settings.uvPosition; }  
        if (settings.scale != null) { shader.scale = settings.scale; }

        return shader;
    }

    function getRelativeSize(tex: RS.Math.Size2D, rect: ReadonlyRectangle)
    {
        return RS.Math.Vector4D(rect.x / tex.w, rect.y / tex.h, rect.h / tex.h, rect.w / tex.w);
    }

    export function warmupTextureShader(stage: IStage)
    {
        getProgram(false, false).warmup(stage);
        getProgram(false, true).warmup(stage);
        getProgram(true, false).warmup(stage);
        getProgram(true, true).warmup(stage);
    }
}
