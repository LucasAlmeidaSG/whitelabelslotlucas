/// <reference path="Base.ts" />
/** @internal  */
namespace RS.Rendering.Pixi.TextLayer
{
    const tmpSize = Math.Size2D();

    export class Canvas extends Base
    {
        protected _canvas: HTMLCanvasElement;
        protected _ctx: CanvasRenderingContext2D;
        protected _baseTex: PIXI.BaseTexture;
        protected _tex: PIXI.Texture;
        protected _spr: Pixi.Sprite;
        protected _padding: number;

        public get tint() { return this._spr.tint; }
        public set tint(value) { this._spr.tint = value; }

        public get blendMode() { return this._spr.blendMode; }
        public set blendMode(value) { this._spr.blendMode = value; }

        public constructor(text: TextBase, container: PIXI.Container, layerInfo: TextOptions.LayerInfo, padding: number, miterLimit?: number)
        {
            super(text, container, layerInfo);

            this._padding = padding;

            this._canvas = document.createElement("canvas");
            this._canvas.width = 1;
            this._canvas.height = 1;
            this._ctx = this._canvas.getContext("2d");

            if (miterLimit != null)
            {
                this._ctx.miterLimit = miterLimit;
            }

            this._baseTex = new PIXI.BaseTexture(this._canvas);
            this._tex = new PIXI.Texture(this._baseTex);

            this._spr = new Pixi.Sprite(this._tex);
            container.addChild(this._spr);
        }

        public update(lineData: TextOptions.LineData[], width: number, height: number, measurements: Base.LineMeasurements, baseline: TextBaseline): void
        {
            width = Math.ceil(width);
            height = Math.ceil(height);

            const p = this._padding;
            const p2 = p * 2.0;

            const font = this._text.font;
            const ctx = this._ctx;

            if (width + p2 !== this._canvas.width || height + p2 !== this._canvas.height)
            {
                this._canvas.width = Math.ceil(width + p2);
                this._canvas.height = Math.ceil(height + p2);
                this._baseTex.realWidth = this._canvas.width;
                this._baseTex.realHeight = this._canvas.height;
                this._baseTex.width = this._canvas.width;
                this._baseTex.height = this._canvas.height;
                this._baseTex.update();
                this._tex.requiresUpdate = true;
            }
            else
            {
                ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
            }

            const yOffset = Math.ceil(measurements.alphabeticHeight);
            const baselineOffset = TextBase.calculateBaselineOffset(measurements, baseline) - yOffset;

            //apply color.alpha to sprite
            this._spr.alpha = this._layerInfo.fillType === Rendering.TextOptions.FillType.SolidColor ?
                this._layerInfo.color.a : 1;

            ctx.save();

            // Iterate each line
            for (let lineIndex = 0, lineCount = lineData.length; lineIndex < lineCount; ++lineIndex)
            {
                const data = lineData[lineIndex];

                // Calculate y coords
                const lineY = Math.round(lineIndex * measurements.totalHeight + yOffset);

                // Calculate x coord
                let lineX: number;
                switch (this._text.align)
                {
                    case RS.Rendering.TextAlign.Left:
                        lineX = 0;
                        break;
                    case RS.Rendering.TextAlign.Middle:
                        lineX = Math.round(width * 0.5 - data.width * 0.5);
                        break;
                    case RS.Rendering.TextAlign.Right:
                        lineX = Math.round(width - data.width);
                        break;
                }

                // Generate path
                const path = font.getPath(data.text, lineX + p, lineY + p, this._text.fontSize);

                // What layer type is it?
                switch (this._layerInfo.layerType)
                {
                    case RS.Rendering.TextOptions.LayerType.Fill:
                    {
                        path.fill = `#${(this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.SolidColor ? this._layerInfo.color.tint : 0xFFFFFF).toString(16)}`;
                        break;
                    }
                    case RS.Rendering.TextOptions.LayerType.Border:
                    {
                        path.stroke = `#${(this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.SolidColor ? this._layerInfo.color.tint : 0xFFFFFF).toString(16)}`;
                        path.strokeWidth = this._layerInfo.size;
                        break;
                    }
                }
                path.draw(ctx);
            }

            ctx.restore();

            const filtersList: PIXI.Filter<any>[] = [];

            // Gradient?
            if (this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.Gradient)
            {
                tmpSize.w = width;
                tmpSize.h = height;
                this._spr.shader = getShaderForGradient(this._layerInfo.gradient, false, tmpSize) as Shader;
                if (this._layerInfo.shader)
                {
                    Log.warn(`Text layers using both a gradient and a shader are not supported`);
                }
            }
            else if (this._layerInfo.shader)
            {
                const shaderInfo = this._layerInfo.shader;
                const shaderProgram = new shaderInfo.class(shaderInfo.settings);
                filtersList.push(new Filter(shaderProgram.createShader() as Shader));
            }

            this._spr.filters = filtersList;

            // offset
            if (this._layerInfo.offset != null)
            {
                this._spr.position.set(this._layerInfo.offset.x - p, this._layerInfo.offset.y - p + baselineOffset);
            }
            else
            {
                this._spr.position.set(-p, -p + baselineOffset);
            }

            this._tex.update();
            this._localBoundsRect.copy(this._spr.getLocalBounds());
            this._localBoundsRect.x += p;
            this._localBoundsRect.y += p + baselineOffset;
            this._localBoundsRect.width -= p2;
            this._localBoundsRect.height -= p2;
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._container.removeChild(this._spr);
            this._spr = null;
            this._tex.destroy(true);
            this._tex = null;
            this._baseTex = null;
            this._canvas = null;
            this._ctx = null;
            super.dispose();
        }
    }
}
