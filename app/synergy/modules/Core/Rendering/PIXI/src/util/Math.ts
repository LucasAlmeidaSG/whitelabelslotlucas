/** @internal  */
namespace RS.Math
{
    /**
     * Finds a new rectangle that fully encompasses both a and b.
     * @param a 
     * @param b 
     */
    export function expandRect(a: PIXI.Rectangle, b: PIXI.Rectangle): PIXI.Rectangle
    {
        const x1 = Math.min(a.x, b.x), y1 = Math.min(a.y, b.y);
        const x2 = Math.max(a.x + a.width, b.x + b.width), y2 = Math.max(a.y + a.height, b.y + b.height);
        return new PIXI.Rectangle(
            x1, y1,
            x2 - x1, y2 - y1
        );
    }
}
/** @internal  */
namespace RS.Math.Matrix3
{
    export function fromPixiMatrix(mtx: PIXI.Matrix, out?: Matrix3): Matrix3
    {
        out = out || Matrix3();

        out[0] = mtx.a;
        out[1] = mtx.b;
        out[2] = 0;

        out[3] = mtx.c;
        out[4] = mtx.d;
        out[5] = 0;

        out[6] = mtx.tx;
        out[7] = mtx.ty;
        out[8] = 1;

        return out;
    }
}
/** @internal  */
namespace RS.Math.Matrix4
{
    export function fromPixiMatrix(mtx: PIXI.Matrix, out?: Matrix4): Matrix4
    {
        out = out || Matrix4();

        out[0] = mtx.a;
        out[1] = mtx.b;
        out[2] = 0;
        out[3] = 0;

        out[4] = mtx.c;
        out[5] = mtx.d;
        out[6] = 0;
        out[7] = 0;

        out[8] = 0;
        out[9] = 0;
        out[10] = 1;
        out[11] = 0;

        out[12] = mtx.tx;
        out[13] = mtx.ty;
        out[14] = 0;
        out[15] = 1;

        return out;
    }
}