/** @internal  */
namespace RS.Rendering.Pixi
{
    const shaderPrograms =
    {
        forGraphics:
        {
            linear: new Shaders.GraphicsLinearGradientShaderProgram(),
            radial: new Shaders.GraphicsRadialGradientShaderProgram(),
            texture: new Shaders.GraphicsTextureGradientShaderProgram()
        },
        forSprite:
        {
            linear: new Shaders.LinearGradientShaderProgram(),
            radial: new Shaders.RadialGradientShaderProgram(),
            texture: new Shaders.TextureGradientShaderProgram()
        }
    };

    const unitY = Math.Vector2D(0.0, 1.0);
    const unitX = Math.Vector2D(1.0, 0.0);

    interface CacheData
    {
        cacheTexture: ISubTexture;
        cacheHash: number;
    }
    function hasCacheData(g: Gradient): g is Gradient & CacheData
    {
        return (g as (Gradient&CacheData)).cacheTexture != null;
    }

    function setCache(g: Gradient, hash: number, texture: ISubTexture): void
    {
        const cacheData = g as (Gradient&CacheData);
        cacheData.cacheHash = hash;
        cacheData.cacheTexture = texture;
    }

    export function getShaderForGradient(gradient: Gradient, forGraphics: boolean, size: Math.Size2D): RS.Rendering.IShader
    {
        const stopsIn = Is.func(gradient.stops) ? gradient.stops() : gradient.stops;
        const stops = gradient.reverse ? Gradient.reverseStops(stopsIn) : stopsIn;
        const isSimpleGradient = stops.length === 2 && stops[0].offset === 0 && stops[1].offset === 1.0;
        const hash = Gradient.hash(gradient);
        const cachedTexture = (hasCacheData(gradient) && gradient.cacheHash === hash && gradient.cacheTexture) || null;
        if (forGraphics)
        {
            switch (gradient.type)
            {
                case Gradient.Type.Horizontal:
                case Gradient.Type.Vertical:
                {
                    if (isSimpleGradient)
                    {
                        return createSimpleLinearGradient(shaderPrograms.forGraphics.linear, stops, gradient);
                    }
                    else
                    {
                        const shaderProgram = shaderPrograms.forGraphics.texture;
                        const shader = shaderProgram.createShader();
                        const tex = cachedTexture || Gradient.createSimpleTexture(stops, gradient.type === Gradient.Type.Vertical);
                        setCache(gradient, hash, tex);
                        shader.gradientTex = tex.texture;
                        return shader;
                    }
                }
                case Gradient.Type.Linear:
                {
                    return createGradient(shaderPrograms.forGraphics.texture, cachedTexture, size, gradient, hash);
                }
                case Gradient.Type.Radial:
                {
                    if (isSimpleGradient)
                    {
                        const shader = createSimpleRadialGradient(shaderPrograms.forGraphics.radial, stops, gradient);
                        return shader;
                    }
                    else
                    {
                        return createGradient(shaderPrograms.forGraphics.texture, cachedTexture, size, gradient, hash);
                    }
                }
            }
        }
        else
        {
            switch (gradient.type)
            {
                case Gradient.Type.Horizontal:
                case Gradient.Type.Vertical:
                {
                    if (isSimpleGradient)
                    {
                        return createSimpleLinearGradient(shaderPrograms.forSprite.linear, stops, gradient);
                    }
                    else
                    {
                        return createGradient(shaderPrograms.forSprite.texture, cachedTexture, size, gradient, hash);
                    }
                }
                case Gradient.Type.Linear:
                {
                    return createGradient(shaderPrograms.forSprite.texture, cachedTexture, size, gradient, hash);
                }
                case Gradient.Type.Radial:
                {
                    if (isSimpleGradient)
                    {
                        return createSimpleRadialGradient(shaderPrograms.forSprite.radial, stops, gradient);
                    }
                    else
                    {
                        return createGradient(shaderPrograms.forSprite.texture, cachedTexture, size, gradient, hash);
                    }
                }
            }
        }
    }

    function createGradient(shaderProgram: Shaders.GraphicsTextureGradientShaderProgram | Shaders.TextureGradientShaderProgram, cachedTexture: ISubTexture, size: Math.Size2D, gradient: Gradient, hash: number)
    {
        const shader = shaderProgram.createShader();
        const tex = cachedTexture || Gradient.createTexture(size.w, size.h, gradient);
        setCache(gradient, hash, tex);
        shader.gradientTex = tex.texture;
        return shader;
    }

    function createSimpleLinearGradient(shaderProgram: Shaders.LinearGradientShaderProgram | Shaders.GraphicsLinearGradientShaderProgram, stops: Gradient.Stop[], gradient: Gradient)
    {
        const shader = shaderProgram.createShader();
        shader.colorA = stops[0].color;
        shader.colorB = stops[1].color;
        shader.direction = gradient.type === Gradient.Type.Vertical ? unitY : unitX;
        return shader;
    }

    function createSimpleRadialGradient(shaderProgram: Shaders.RadialGradientShaderProgram | Shaders.GraphicsRadialGradientShaderProgram, stops: Gradient.Stop[], gradient: Gradient.Radial)
    {
        const shader = shaderProgram.createShader();
        shader.colorA = stops[0].color;
        shader.colorB = stops[1].color;
        shader.radius = gradient.radius;
        shader.centerPos = gradient.pos;
        return shader;
    }

    export function warmupGradientShaders(stage: IStage)
    {
        shaderPrograms.forGraphics.linear.warmup(stage);
        shaderPrograms.forGraphics.radial.warmup(stage);
        shaderPrograms.forGraphics.texture.warmup(stage);

        shaderPrograms.forSprite.linear.warmup(stage);
        shaderPrograms.forSprite.radial.warmup(stage);
        shaderPrograms.forSprite.texture.warmup(stage);
    }
}
