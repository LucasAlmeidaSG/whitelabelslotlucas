/** @internal  */
namespace RS.Rendering.Pixi
{
    export enum RenderMode
    {
        Unknown,
        Canvas,
        WebGL
    }

    export let primaryRenderMode: RenderMode = RenderMode.Unknown;
}