/** @internal  */
namespace RS.Rendering.Pixi
{
    const tmpPoint1 = new PIXI.Point(), tmpPoint2 = new PIXI.Point();

    @HasCallbacks
    export class TextBase extends PIXI.Container
    {
        protected _renderSettings: TextOptions.RenderSettings;
        protected _settings: TextOptions.Settings;

        protected _dirty: boolean;
        protected _renderDirty: boolean;
        protected _bbox: opentype.BoundingBox;
        protected _lineData: TextOptions.LineData[] = [];
        protected _previousLinesCount: number = 0;

        protected _text: string;
        protected _filteredText: string;
        protected _textFilter: TextOptions.TextFilter;
        protected _font: opentype.Font;
        protected _fontSize: number;
        protected _wrapWidth: number;
        protected _wrapType: TextOptions.WrapType;
        protected _padding: number;
        protected _align: TextAlign;
        protected _baseline: TextBaseline;
        protected _tint: Util.Color;
        protected _renderContainer: Pixi.RenderContainer | null;
        protected _debug: PIXI.Graphics;
        protected _scaleToWidth: number;

        protected _layers: TextLayer.Base[];

        protected _isDisposed: boolean = false;

        /** Gets if this text has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /**
         * Gets or sets the content of this text.
         */
        @RS.CheckDisposed
        public get text() { return this._text; }
        public set text(value)
        {
            if (this._text === value) { return; }
            this._text = value;
            this.updateFilteredText();
            this._dirty = true;
        }

        /**
         * Gets or sets the text filter.
         */
        @CheckDisposed
        public get textFilter() { return this._textFilter; }
        public set textFilter(value)
        {
            if (this._textFilter === value) { return; }
            this._textFilter = value;
            this.updateFilteredText();
            this._dirty = true;
        }

        /**
         * Gets or sets the font of this text.
         */
        @RS.CheckDisposed
        public get font() { return this._font; }
        public set font(value)
        {
            if (typeof value === "string")
            {
                Log.warn("string values are not a supported font type");
                return;
            }
            if (this._font === value) { return; }
            this._font = value;
            this._dirty = true;
        }

        /**
         * Gets or sets the font size of this text.
         */
        @RS.CheckDisposed
        public get fontSize() { return this._fontSize; }
        public set fontSize(value)
        {
            if (this._fontSize === value) { return; }
            this._fontSize = value;
            this._dirty = true;
        }

        /**
         * Gets or sets the wrap width of this text.
         */
        @RS.CheckDisposed
        public get wrapWidth() { return this._wrapWidth; }
        public set wrapWidth(value)
        {
            if (this._wrapWidth === value) { return; }
            this._wrapWidth = value;
            this._dirty = true;
        }

        @RS.CheckDisposed
        public get scaleToWidth() { return this._scaleToWidth; }
        public set scaleToWidth(value)
        {
            if (this._scaleToWidth === value) { return; }
            this._scaleToWidth = value;
            this._dirty = true;
        }

        /**
         * Gets or sets the wrap type of this text.
         */
        @RS.CheckDisposed
        public get wrapType() { return this._wrapType; }
        public set wrapType(value)
        {
            if (this._wrapType === value) { return; }
            this._wrapType = value;
            this._dirty = true;
        }

        /**
         * Gets or sets the horizontal alignment for this text.
         * Only applicable for multiline text.
         */
        @RS.CheckDisposed
        public get align() { return this._align; }
        public set align(value)
        {
            if (this._align === value) { return; }
            this._align = value;
            this._dirty = true;
        }

        /**
         * Gets or sets the baseline for this text.
         * See: https://www.w3schools.com/tags/canvas_textbaseline.asp
         */
        @CheckDisposed
        public get baseline() { return this._baseline; }
        public set baseline(value)
        {
            if (this._baseline === value) { return; }
            this._baseline = value;
            this._dirty = true;
        }

        /**
         * Gets or sets the tint of this text.
         */
        @RS.CheckDisposed
        public get tint() { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            if (this._renderSettings.strategy === TextOptions.RenderStrategy.SupersampledGraphics)
            {
                this._renderContainer.tint = value.tint;
            }
            else
            {
                for (const layer of this._layers)
                {
                    layer.tint = value.tint;
                }
            }
            this._dirty = true;
        }

        /**
         * Gets or sets the blend mode of this text.
         */
        @CheckDisposed
        public get blendMode()
        {
            if (this._renderSettings.strategy === TextOptions.RenderStrategy.SupersampledGraphics)
            {
                return this._renderContainer.blendMode;
            }
            return this._layers[0].blendMode;
        }
        public set blendMode(value)
        {
            if (this._renderSettings.strategy === TextOptions.RenderStrategy.SupersampledGraphics)
            {
                this._renderContainer.blendMode = value;
                this.update();
            }
            else
            {
                for (const layer of this._layers)
                {
                    layer.blendMode = value;
                }
            }
        }

        /**
         * Gets the current number of lines of this text.
         */
        @CheckDisposed
        public get linesCount() { return this._lineData.length; }

        /**
         * Gets line measurement data in px.
         */
        @CheckDisposed
        public get lineMeasurements(): TextLayer.Base.LineMeasurements
        {
            const measurements: TextLayer.Base.LineMeasurements = {
                totalHeight: this.toScaledSize(this._font.ascender - this._font.descender),
                alphabeticHeight: this.toScaledSize(this._font.ascender),
                underhangHeight: this.toScaledSize(-this._font.descender)
            };
            if (this._settings.lineHeight)
            {
                measurements.totalHeight *= this._settings.lineHeight;
                measurements.alphabeticHeight *= this._settings.lineHeight;
                measurements.underhangHeight *= this._settings.lineHeight;
            }
            return measurements;
        }

        public constructor(settings: TextOptions.Settings, text: string = "")
        {
            super();
            this._settings = settings;
            if (Is.assetReference(settings.font))
            {
                this._font = RS.Asset.Store.getAsset(settings.font).as(opentype.Font);
            }
            else
            {
                this._font = settings.font;
            }
            this._fontSize = settings.fontSize;
            this._wrapWidth = settings.wrapWidth;
            this._wrapType = settings.wrapType || TextOptions.WrapType.Word;
            this._baseline = settings.baseline || TextBaseline.Top;
            this._text = text;
            this._textFilter = settings.textFilter;
            this.updateFilteredText();
            this._dirty = true;

            this._align = settings.align || TextAlign.Left;
            this._padding = 4;

            // Disable interactive children, otherwise we get false positives due to the texture size of the text.
            this.interactiveChildren = false;

            const layerData = this.getLayerData();

            this._renderSettings = settings.renderSettings || TextOptions.defaultRenderSettings;
            if (primaryRenderMode === RenderMode.Canvas && this._renderSettings.strategy === TextOptions.RenderStrategy.SupersampledGraphics)
            {
                this._renderSettings = this._renderSettings.canvasFallback || TextOptions.defaultCanvasRenderSettings;
            }
            if (this._renderSettings.strategy === TextOptions.RenderStrategy.SupersampledGraphics)
            {
                this._renderContainer = new RenderContainer({ mode: RenderContainer.Mode.Supersample, power: this._renderSettings.power, padding: this._renderSettings.padding });
                this.addChild(this._renderContainer);

                this._layers = layerData.map((ld) => new TextLayer.Graphics(this, this._renderContainer, ld));
            }
            else
            {
                this._layers = layerData.map((ld) => new TextLayer.Canvas(this, this, ld, this._renderSettings.padding));
            }

            if (settings.color) { this.tint = settings.color; }

            this._localBoundsRect = new PIXI.Rectangle();

            this.update(true);
            this._dirty = false;
            this._renderDirty = false;
        }

        public static calculateBaselineOffset(measurements: TextLayer.Base.LineMeasurements, baseline: TextBaseline): number
        {
            let baselineY = 0;
            switch (baseline)
            {
                case TextBaseline.Top:
                    baselineY += measurements.alphabeticHeight;
                    break;
                case TextBaseline.Hanging:
                    baselineY += measurements.underhangHeight;
                    break;
                case TextBaseline.Middle:
                    baselineY += measurements.alphabeticHeight;
                    baselineY -= measurements.totalHeight * 0.5;
                    break;
                case TextBaseline.Bottom:
                    baselineY -= measurements.underhangHeight;
                    break;
            }
            return baselineY;
        }

        public containsPoint(point: PIXI.Point): boolean
        {
            this.worldTransform.applyInverse(point, tmpPoint1);
            return this._localBoundsRect
                .contains(tmpPoint1.x, tmpPoint1.y);
        }

        /**
         * Gets the local bounds of the text object.
         */
        public getLocalBounds(rect = new PIXI.Rectangle()): PIXI.Rectangle
        {
            if (this._dirty)
            {
                this.update();
                this._dirty = false;
            }
            rect.copy(this._localBoundsRect);
            return rect;
        }

        /**
         * Recalculates the bounds of the container.
         */
        public calculateBounds(): void
        {
            this._bounds.clear();
            this._calculateBounds();
            const localBounds = this._localBoundsRect;
            const worldTransform = this.worldTransform;
            const verts = new Array<number>(8);

            tmpPoint1.x = localBounds.x; tmpPoint1.y = localBounds.y;
            this.toGlobal(tmpPoint1, tmpPoint2, true);
            verts[0] = tmpPoint2.x; verts[1] = tmpPoint2.y;

            tmpPoint1.x = localBounds.right; tmpPoint1.y = localBounds.y;
            this.toGlobal(tmpPoint1, tmpPoint2, true);
            verts[2] = tmpPoint2.x; verts[3] = tmpPoint2.y;

            tmpPoint1.x = localBounds.right; tmpPoint1.y = localBounds.bottom;
            this.toGlobal(tmpPoint1, tmpPoint2, true);
            verts[4] = tmpPoint2.x; verts[5] = tmpPoint2.y;

            tmpPoint1.x = localBounds.x; tmpPoint1.y = localBounds.bottom;
            this.toGlobal(tmpPoint1, tmpPoint2, true);
            verts[6] = tmpPoint2.x; verts[7] = tmpPoint2.y;

            this._bounds.addQuad(verts);
            this._lastBoundsID = this._boundsID;
        }

        public clone(): TextBase
        {
            const newText = new TextBase(this._settings, this._text);
            newText.position.copy(this.position);
            newText.scale.copy(this.scale);
            newText.pivot.copy(this.pivot);
            newText.rotation = this.rotation;
            return newText;
        }

        public drawBounds(rect: PIXI.Rectangle): void
        {
            if (!this._debug)
            {
                this._debug = new PIXI.Graphics();
                this.addChild(this._debug);
            }
            this._debug.clear();
            this._debug.lineStyle(2, 0xff0000, .2);
            this._debug.drawRect(rect.x, rect.y, rect.width, rect.height);
        }

        /** Disposes this text object. */
        public dispose()
        {
            if (this._isDisposed) { return; }
            for (const layer of this._layers)
            {
                layer.dispose();
            }
            this._layers.length = 0;
            this._layers = null;
            if (this._renderContainer)
            {
                this.removeChild(this._renderContainer);
                this._renderContainer.dispose();
                this._renderContainer = null;
            }
            this.destroy(true);
            this._isDisposed = true;
        }

        public render(renderer: GenericRenderer)
        {
            const renderNeeded = this._renderDirty;
            this.updateRender();
            if (renderNeeded && this._renderContainer)
            {
                this._renderContainer.render(renderer, true);
            }
        }

        public renderWebGL(renderer: PIXI.WebGLRenderer)
        {
            this.updateRender();
            super.renderWebGL(renderer);
        }

        public renderCanvas(renderer: PIXI.CanvasRenderer)
        {
            this.updateRender();
            super.renderCanvas(renderer);
        }

        protected updateRender()
        {
            if (!this._dirty && !this._renderDirty) { return; }
            if (!Pixi.World.apparent(this) || !this.renderable) { return; }

            this.update(true);
            this._dirty = false;
            this._renderDirty = false;
        }

        protected updateFilteredText()
        {
            this._filteredText = this._textFilter ? this._textFilter(this._text) : this._text;
        }

        protected refreshLineData()
        {
            this._lineData.length = 0;
            const glyphs = this._font.stringToGlyphs(this._filteredText, { features: { liga: false, rlig: false } });

            let i = 0;
            /** X position of the current character. */
            let curX = 0;
            /** Length of the last line. */
            let lastLen = 0;
            /** Width of the current line up to the current character. */
            let lineWidth = 0;
            /** Index of the last whitespace character. */
            let spaceIndex = 0;
            /** Width of the last whitespace character. */
            let spaceWidth = 0;

            for (const l = this._filteredText.length; i < l; ++i)
            {
                const g = glyphs[i];
                const c = this._filteredText[i];

                const kerning = i < l - 1 ? this.toScaledSize(this._font.getKerningValue(g, glyphs[i + 1])) : 0;
                /** Character width. */
                const advanceX = this.toScaledSize(g.advanceWidth) + kerning;

                /** Width of the current line including the current character. */
                const curWidth = curX + advanceX;

                const isSpace = /[ \t\r\n\f]/g.test(c);
                if (isSpace)
                {
                    lineWidth = curWidth;

                    spaceIndex = i;
                    spaceWidth = advanceX;
                }

                if (this._wrapWidth != null && curWidth > this._wrapWidth && c !== "\n")
                {
                    // We've exceeded the wrap width, see if there's a whitespace character we can jump back to
                    if (spaceIndex > lastLen)
                    {
                        // There is, push up to that character as a line
                        this._lineData.push({
                            text: this._filteredText.substring(lastLen, spaceIndex),
                            width: lineWidth - spaceWidth
                        });
                        // Start a new line
                        lastLen = spaceIndex + 1;
                        curX = curWidth - lineWidth;
                        spaceWidth = 0;
                    }
                    else if (this._wrapType === TextOptions.WrapType.Word)
                    {
                        // There isn't, cut here as if \n. Revert kerning adjustment from last glyph.
                        const lastKerning = i > 0 ? this.toScaledSize(this._font.getKerningValue(glyphs[i - 1], g)) : 0;
                        this._lineData.push({
                            text: this._filteredText.substring(lastLen, i),
                            width: (isSpace ? curX - spaceWidth : curX) - lastKerning
                        });
                        lastLen = i;

                        // We break on the preceding glyph, so let's move our cursor to the end of this one
                        curX = advanceX;
                        spaceWidth = 0;
                    }
                    else
                    {
                        // no whitespace found and wrap on whitespace: continue as normal
                        curX += advanceX;
                    }
                    spaceIndex = 0;
                    lineWidth = 0;
                }
                else if (c === "\n")
                {
                    this._lineData.push({
                        text: this._filteredText.substring(lastLen, i),
                        width: curX
                    });
                    lastLen = i;
                    curX = 0;
                    spaceIndex = i;
                    lineWidth = 0;
                    ++lastLen;
                }
                else
                {
                    curX += advanceX;
                }
            }
            this._lineData.push({
                text: this._filteredText.substring(lastLen, i),
                width: curX
            });

            // If the number of lines changed, we need to update the layers so that gradients display correctly
            if (this._settings.layers && this.linesCount !== this._previousLinesCount)
            {
                this.updateLayers();
            }
            this._previousLinesCount = this.linesCount;
        }

        /** Returns the given size scaled by the current font-size. */
        protected toScaledSize(size: number): number
        {
            return this._fontSize * size / this._font.unitsPerEm;
        }

        protected update(render: boolean = false): void
        {
            const p = this._padding;
            const p2 = p * 2.0;

            // Update lines
            this.refreshLineData();

            // Calculate line height
            const lineMeasurements = this.lineMeasurements;

            // Calculate text bounds
            const width = this._lineData
                .map((l) => l.width)
                .reduce((a, b) => Math.max(a, b), 0);
            const height = Math.ceil(lineMeasurements.alphabeticHeight * this._lineData.length + lineMeasurements.underhangHeight * (this._lineData.length - 1));
            if (this._scaleToWidth != undefined && width > this._scaleToWidth && this._fontSize > 1)
            {
                this._fontSize--;
                this.update(render);
                return;
            }

            // Update bounds
            this._localBoundsRect.x = 0;
            this._localBoundsRect.y = TextBase.calculateBaselineOffset(lineMeasurements, this._baseline) - lineMeasurements.alphabeticHeight;
            this._localBoundsRect.width = width;
            this._localBoundsRect.height = height;

            if (render)
            {
                // Iterate each layer
                for (const layer of this._layers)
                {
                    layer.update(this._lineData, width, height, lineMeasurements, this._baseline);
                }

                if (this._renderContainer)
                {
                    // Make sure we redraw the text
                    this._renderContainer.refreshNeeded = true;
                }
            }
            else if (this._dirty)
            {
                this._renderDirty = true;
            }
            //this.drawBounds(this._localBoundsRect);
        }

        /**
         * Calculates the bounds of the Text as a rectangle. The bounds calculation takes the worldTransform into account.
         */
        protected _calculateBounds()
        {
            if (this._dirty)
            {
                this.update();
                this._dirty = false;
            }
            super._calculateBounds();
        }

        /**
         * Updates the layers so that vertical gradients are rendered correctly when text is wrapped on multiple lines
         */
        protected updateLayers()
        {
            let layerData: RS.Rendering.TextOptions.LayerInfo[] = [];
            const defaultLayerData = this.getLayerData();

            // Gradient stops only need to be updated when the text has more than one line
            if (this.linesCount > 1)
            {
                const measurements = this.lineMeasurements; // Cache result of line measurements
                const totalHeight = Math.ceil(measurements.alphabeticHeight * this.linesCount + measurements.underhangHeight * (this.linesCount - 1)); // Total height of the text

                // Store the ratios of alphabetic height and underhang height over total text height
                const alphabeticRatio = measurements.alphabeticHeight / totalHeight;
                const underhangRatio = measurements.underhangHeight / totalHeight;

                for (let i = 0; i < defaultLayerData.length; i++)
                {
                    const layer: TextOptions.LayerInfo = defaultLayerData[i];
                    // If current layer is a vertical gradient layer, recreate it
                    // TODO: Add support for other types of gradients?
                    if (layer.fillType === TextOptions.FillType.Gradient && layer.gradient.type === RS.Rendering.Gradient.Type.Vertical)
                    {
                        const newStops: RS.Rendering.Gradient.Stop[] = []; // Updated gradient stops
                        let oldStops: RS.Rendering.Gradient.Stops; // Old gradient stops
                        if (layer.gradient.stops instanceof Function)
                        {
                            oldStops = layer.gradient.stops();
                        }
                        else
                        {
                            oldStops = layer.gradient.stops;
                        }

                        // For each line of text, we will make a copy of the gradient stops at the correct position
                        for (let j = 0; j < this.linesCount; j++)
                        {
                            // Get the position of the current line of text relative to entire text (in range 0-1)
                            const topPos = j * (alphabeticRatio + underhangRatio); // Top of current line
                            const bottomPos = topPos + alphabeticRatio; // Bottom of current line
                            const posDiff = bottomPos - topPos; // Ratio of total text height taken by the current line

                            // Re-map the current gradient stops to the position of the current line of text
                            for (let k = 0; k < oldStops.length; k++)
                            {
                                const stop: RS.Rendering.Gradient.Stop = oldStops[k];
                                const newStop: RS.Rendering.Gradient.Stop = {
                                    color: new RS.Util.Color(stop.color.r, stop.color.g, stop.color.b, stop.color.a),
                                    offset: topPos + posDiff * stop.offset
                                };
                                newStops.push(newStop);
                            }
                        }

                        // Create a new layer with the updated stops
                        layerData.push({
                            ...layer,
                            fillType: RS.Rendering.TextOptions.FillType.Gradient,
                            gradient:
                            {
                                type: RS.Rendering.Gradient.Type.Vertical,
                                stops: newStops
                            }
                        });
                    }
                    else
                    {
                        // Otherwise use the current layer as there is no need to update it
                        layerData.push(layer);
                    }
                }
            }
            else
            {
                // Use the layers from the settings if text fits on one line
                layerData = defaultLayerData;
            }

            // Clean previous layers
            if (this._renderContainer != null)
            {
                this._renderContainer.removeAllChildren();
            }

            for (const layer of this._layers)
            {
                layer.dispose();
            }

            // Recreate layers
            if (this._renderSettings.strategy === TextOptions.RenderStrategy.SupersampledGraphics)
            {
                this._layers = layerData.map((ld) => new TextLayer.Graphics(this, this._renderContainer, ld));
            }
            else
            {
                const renderSettings = this._renderSettings as TextOptions.CanvasRenderSettings;
                this._layers = layerData.map((ld) => new TextLayer.Canvas(this, this, ld, renderSettings.padding, renderSettings.miterLimit));
            }
        }

        protected getLayerData(): TextOptions.LayerInfo[]
        {
            let layerData: TextOptions.LayerInfo[];
            const settings = this._settings;
            if (settings.layers && settings.layers.length > 0)
            {
                layerData = settings.layers;
            }
            else
            {
                layerData = [{ layerType: TextOptions.LayerType.Fill, fillType: TextOptions.FillType.SolidColor, color: RS.Util.Colors.white }];
            }
            return layerData;
        }
    }
}
