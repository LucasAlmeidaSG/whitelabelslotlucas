/// <reference path="Base.ts" />
/** @internal  */
namespace RS.Rendering.Pixi.TextLayer
{
    const vertices = new Math.Vector2DList();
    const tmpSize = Math.Size2D();

    export class Graphics extends Base
    {
        protected _graphics: RSGraphics;

        public get tint() { return this._graphics.tint; }
        public set tint(value) { this._graphics.tint = value; }

        public get blendMode() { return this._graphics.blendMode; }
        public set blendMode(value) { this._graphics.blendMode = value; }

        public constructor(text: TextBase, container: PIXI.Container, layerInfo: TextOptions.LayerInfo)
        {
            super(text, container, layerInfo);

            this._graphics = new RSGraphics();
            container.addChild(this._graphics);
        }

        public update(lineData: TextOptions.LineData[], width: number, height: number, measurements: Base.LineMeasurements, baseline: TextBaseline): void
        {
            const font = this._text.font;
            const g = this._graphics;

            g.clear();

            const baselineY = TextBase.calculateBaselineOffset(measurements, baseline);

            // Iterate each line
            for (let lineIndex = 0, lineCount = lineData.length; lineIndex < lineCount; ++lineIndex)
            {
                const data = lineData[lineIndex];

                // Calculate y coord
                const lineY = Math.round(lineIndex * measurements.totalHeight + baselineY);

                // Calculate x coord
                let lineX: number;
                switch (this._text.align)
                {
                    case RS.Rendering.TextAlign.Left:
                        lineX = 0;
                        break;
                    case RS.Rendering.TextAlign.Middle:
                        lineX = Math.round(width * 0.5 - data.width * 0.5);
                        break;
                    case RS.Rendering.TextAlign.Right:
                        lineX = Math.round(width - data.width);
                        break;
                }

                // Render to graphics
                font.forEachGlyph(data.text, lineX, lineY, this._text.fontSize, null, (glyph, gX, gY, gFontSize) =>
                {
                    const glyphPath = glyph.getPath(gX, gY, gFontSize, null, font);
                    const tint = this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.SolidColor ? this._layerInfo.color.tint : 0xFFFFFF;
                    const alpha = this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.SolidColor ? this._layerInfo.color.a : this._layerInfo.alpha;
                    switch (this._layerInfo.layerType)
                    {
                        case RS.Rendering.TextOptions.LayerType.Fill:
                            {
                                this.pathToFillGraphics(glyphPath, g, tint, alpha);
                                break;
                            }
                        case RS.Rendering.TextOptions.LayerType.Border:
                            {
                                this.pathToStrokeGraphics(glyphPath, g, tint, this._layerInfo.size, alpha);
                                break;
                            }
                    }

                });
            }

            const filtersList = [];

            // Gradient?
            if (this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.Gradient)
            {
                tmpSize.w = width;
                tmpSize.h = height;
                g.shader = getShaderForGradient(this._layerInfo.gradient, true, tmpSize) as Shader;
                if (this._layerInfo.shader)
                {
                    Log.warn(`Text layers using both a gradient and a shader are not supported`);
                }
            }
            //Image?
            else if (this._layerInfo.fillType === RS.Rendering.TextOptions.FillType.Texture)
            {
                let shaderTex: Rendering.ISubTexture = null;

                switch (this._layerInfo.image.kind)
                {
                    case RS.Rendering.TextOptions.ImageKind.Texture:
                    {
                        shaderTex = this._layerInfo.image.texture;
                        break;
                    }
                    case RS.Rendering.TextOptions.ImageKind.Bitmap:
                    {
                        const asset = RS.Asset.Store.getAsset(this._layerInfo.image.asset).as(RS.Asset.ImageAsset);
                        const texture = new RS.Rendering.SubTexture(asset.asset);
                        shaderTex = texture;
                        break;
                    }
                    case RS.Rendering.TextOptions.ImageKind.SpriteFrame:
                    {
                        let frameIndex: number;
                        const asset = Asset.Store.getAsset(this._layerInfo.image.asset);
                        const sheet = asset.asset as Rendering.ISpriteSheet;
                        if (Is.number(this._layerInfo.image.frameID))
                        {
                            frameIndex = this._layerInfo.image.frameID;
                        }
                        else
                        {
                            frameIndex = sheet.getAnimation(this._layerInfo.image.animationName).frames[0];
                        }
                        const frameData = sheet.getFrame(frameIndex);
                        shaderTex = frameData.imageData as SubTexture;
                        break;
                    }
                }

                g.shader = getShaderForTexture(shaderTex, this._layerInfo.image) as Shader;

                if (this._layerInfo.shader)
                {
                    Log.warn(`Text layers using both a texture and a shader are not supported`);
                }
            }
            else if (this._layerInfo.shader)
            {
                const shaderInfo = this._layerInfo.shader;
                const shaderProgram = new shaderInfo.class(shaderInfo.settings);
                filtersList.push(new Filter(shaderProgram.createShader() as Shader));
            }

            g.filters = filtersList;

            // offset
            if (this._layerInfo.offset != null)
            {
                g.position.set(this._layerInfo.offset.x, this._layerInfo.offset.y);
            }
            else
            {
                g.position.set(0, 0);
            }

            this._localBoundsRect.copy(g.getLocalBounds());
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._container.removeChild(this._graphics);
            this._graphics.destroy();
            this._graphics = null;
            super.dispose();
        }

        protected pathToFillGraphics(path: opentype.Path, graphics: PIXI.Graphics, fillColor: number, alpha: number)
        {
            const polys: Graphics.Poly[] = [];
            const pts = vertices;
            pts.clear();
            /** Bounds of the current polygon. */
            let bounds = { x0: null, y0: null, x1: null, y1: null };
            let startIndex = 0;
            for (let i = 0, l = path.commands.length; i < l; ++i)
            {
                const cmd = path.commands[i];
                switch (cmd.type)
                {
                    case "M":
                    case "L":
                    case "C":
                    case "Q":
                        // Build polygon.
                        pts.add(cmd.x, cmd.y);

                        // Update bounds.
                        if (bounds.x0 == null || cmd.x < bounds.x0) { bounds.x0 = cmd.x; }
                        else if (bounds.x1 == null || cmd.x > bounds.x1) { bounds.x1 = cmd.x; }

                        if (bounds.y0 == null || cmd.y < bounds.y0) { bounds.y0 = cmd.y; }
                        else if (bounds.y1 == null || cmd.y > bounds.y1) { bounds.y1 = cmd.y; }

                        break;
                    case "Z":
                        // Add closed polygon.
                        polys.push({ signedArea: pts.signedArea, bounds, startIndex, length: i - startIndex, endsOnClose: true });

                        startIndex = i + 1;
                        bounds = { x0: null, y0: null, x1: null, y1: null };
                        pts.clear();
                        break;
                }
            }

            if (pts.length > 0)
            {
                // Add final unclosed poly.
                polys.push({ signedArea: pts.signedArea, bounds, startIndex, length: (path.commands.length - 1) - startIndex, endsOnClose: false });
            }

            const sortedPolys = Graphics.Poly.sort(polys);

            graphics.beginFill(fillColor, alpha);
            for (let i = sortedPolys.length - 1; i >= 0; i--)
            {
                const poly = sortedPolys[i];
                this.commandsToGraphics(path.commands, poly.startIndex, poly.length, graphics);
                if (poly.isHole) { graphics.addHole(); }
                graphics.closePath();
            }
            graphics.endFill();
        }

        protected commandsToGraphics(commands: opentype.PathCommand[], startIndex: number, length: number, graphics: PIXI.Graphics): void
        {
            for (let i = startIndex, l = startIndex + length; i < l; ++i)
            {
                const cmd = commands[i];

                // Skip duplicate commands.
                if (cmd.type !== "Z" && startIndex < i)
                {
                    const lastCmd = commands[i - 1];
                    if (lastCmd.x === cmd.x && lastCmd.y === cmd.y)
                    {
                        continue;
                    }
                }

                switch (cmd.type)
                {
                    case "M":
                        graphics.moveTo(cmd.x, cmd.y);
                        break;
                    case "L":
                        graphics.lineTo(cmd.x, cmd.y);
                        break;
                    case "C":
                        graphics.bezierCurveTo(cmd.x1, cmd.y1, cmd.x2, cmd.y2, cmd.x, cmd.y);
                        break;
                    case "Q":
                        graphics.quadraticCurveTo(cmd.x1, cmd.y1, cmd.x, cmd.y);
                        break;
                    case "Z":
                        graphics.closePath();
                }
            }
        }

        protected pathToStrokeGraphics(path: opentype.Path, graphics: PIXI.Graphics, strokeColor: number, size: number, alpha: number)
        {
            graphics.lineStyle(size, strokeColor, alpha);
            this.commandsToGraphics(path.commands, 0, path.commands.length, graphics);
        }
    }

    export namespace Graphics
    {
        export interface Poly
        {
            /** The square bounds of this polygon (two coordinates rather than x/y/w/h). */
            bounds: { x0: number, y0: number, x1: number, y1: number };
            signedArea: number;
            startIndex: number;
            length: number;
            endsOnClose: boolean;

            /** Whether or not this polygon forms a hole in the last one. */
            isHole?: boolean;

            /** How many containers (enclosing polygons) this polygon has. */
            nestLevel?: number;
        }

        export namespace Poly
        {
            /**
             * Sorts a polygon array such that holes appear immediately before their
             * containing polygon. The polygons must therefore be drawn in reverse order.
             */
            export function sort(polys: Poly[]): Poly[]
            {
                const sorted: Poly[] = [];

                for (let i = 0; i < polys.length; i++)
                {
                    const poly = polys[i];

                    let traversing: boolean = false;
                    let maxNestLevel: number = 0;
                    RS.Util.insert(poly, sorted, (cand: Poly, item: Poly, index: number) =>
                    {
                        if (contains(cand, item))
                        {
                            // We're adding an enclosed polygon; add another level to hierarchy.
                            item.isHole = !cand.isHole;
                            item.nestLevel = (cand.nestLevel | 0) + 1;

                            // Insert left.
                            return -1;
                        }

                        if (contains(item, cand))
                        {
                            traversing = true;

                            // Track maximum nest level enclosed by this poly.
                            if (cand.nestLevel > maxNestLevel) { maxNestLevel = cand.nestLevel; }

                            // Push node down.
                            cand.nestLevel = (cand.nestLevel | 0) + 1;
                            cand.isHole = !cand.isHole;
                        }
                        else if (traversing)
                        {
                            cand.nestLevel = maxNestLevel;
                            cand.isHole = cand.nestLevel % 2 > 0;

                            // End of hierarchy reached, insert left.
                            return -1;
                        }

                        // No relation between item and candidate; continue.
                        return 0;
                    });
                }

                return sorted;
            }

            /**
             * Gets whether a contains b.
             */
            export function contains(a: Poly, b: Poly): boolean
            {
                if (a.bounds.x0 > b.bounds.x0) { return false; }
                if (a.bounds.x1 < b.bounds.x1) { return false; }
                if (a.bounds.y0 > b.bounds.y0) { return false; }
                if (a.bounds.y1 < b.bounds.y1) { return false; }

                return true;
            }
        }
    }
}
