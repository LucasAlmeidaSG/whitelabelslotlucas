namespace RS.Rendering.Pixi
{
    /** Encapsulates a bunch of lazily-instantiating watched interaction events, with an observable for the handlers. */
    @HasCallbacks
    export class InteractionEventBundle implements RS.IDisposable
    {
        public readonly hasInteractionEventHandlers = new Observable(false);

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onUpOutside: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onRightClicked: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onRightDown: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onRightUp: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onRightUpOutside: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onClicked: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onClickCancelled: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onDown: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onMoved: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onOut: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onOver: IPrivateWatchedEvent<InteractionEventData>;

        @Lazy(InteractionEventBundle.createInteractionEventOnBundle)
        public readonly onUp: IPrivateWatchedEvent<InteractionEventData>;

        @RS.AutoDispose private readonly _observables: RS.IObservable<number>[] = [];
        @RS.AutoDispose private readonly _disposables: RS.IDisposable[] = [];

        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }

        private static createInteractionEventOnBundle(target: InteractionEventBundle)
        {
            return target.createInteractionEvent();
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }

        private createInteractionEvent()
        {
            const ev = createEvent<InteractionEventData>(true);
            this._observables.push(ev.listenerCount);
            this._disposables.push(ev);
            ev.listenerCount.onChanged(this.onEventListenerCountChanged);
            return ev;
        }

        @Callback
        private onEventListenerCountChanged()
        {
            this.hasInteractionEventHandlers.value = this._observables.some((obs) => obs.value > 0);
        }
    }
}
