/** @internal  */
namespace RS.Rendering.Pixi
{
    export class RSGraphics extends PIXI.Graphics
    {
        protected _shader: RS.Rendering.Pixi.Shader|null = null;

        /** Gets or sets the shader to be used for this sprite. */
        public get shader() { return this._shader; }
        public set shader(value) { this._shader = value; }
    }
}