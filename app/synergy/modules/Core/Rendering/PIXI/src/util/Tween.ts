/** @internal  */
namespace RS.Rendering.Pixi
{
    @RS.TweenTypeHandler
    class PointTweenTypeHandler implements RS.ITweenTypeHandler<PIXI.Point | PIXI.ObservablePoint>
    {
        /** Gets if this handler can handle the specified object. */
        public canHandle(obj: any): obj is PIXI.Point
        {
            return RS.Is.object(obj) && (obj instanceof PIXI.Point || obj instanceof PIXI.ObservablePoint);
        }
        
        /** Clones the specified object. */
        public clone(original: PIXI.Point | PIXI.ObservablePoint): PIXI.Point
        {
            return original != null ? (original instanceof PIXI.Point ? original.clone() : new PIXI.Point(original.x, original.y)) : null;
        }

        /** Creates a new object, interpolated between a and b by mu. */
        public interpolate(a: PIXI.Point | PIXI.ObservablePoint, b: PIXI.Point | PIXI.ObservablePoint, mu: number): PIXI.Point
        {
            return new PIXI.Point(RS.Math.linearInterpolate(a.x, b.x, mu), RS.Math.linearInterpolate(a.y, b.y, mu));
        }

        /** Copies properties of src into dst. */
        public apply(src: PIXI.Point | PIXI.ObservablePoint, dst: object, dstKey: string): void
        {
            const dstObj = dst[dstKey];
            if (dstObj != null && (dstObj instanceof PIXI.Point || dstObj instanceof PIXI.ObservablePoint))
            {
                dstObj.copy(src);
            }
            else
            {
                dst[dstKey] = src;
            }
        }
    }

    @RS.TweenTypeHandler
    class CircleTweenTypeHandler implements RS.ITweenTypeHandler<PIXI.Circle>
    {
        /** Gets if this handler can handle the specified object. */
        public canHandle(obj: any): obj is PIXI.Circle
        {
            return RS.Is.object(obj) && (obj instanceof PIXI.Circle);
        }
        
        /** Clones the specified object. */
        public clone(original: PIXI.Circle): PIXI.Circle
        {
            return original != null ? original.clone() : null;
        }

        /** Creates a new object, interpolated between a and b by mu. */
        public interpolate(a: PIXI.Circle, b: PIXI.Circle, mu: number): PIXI.Circle
        {
            return new PIXI.Circle(RS.Math.linearInterpolate(a.x, b.x, mu), RS.Math.linearInterpolate(a.y, b.y, mu), RS.Math.linearInterpolate(a.radius, b.radius, mu));
        }

        /** Copies properties of src into dst. */
        public apply(src: PIXI.Circle, dst: object, dstKey: string): void
        {
            const dstObj = dst[dstKey];
            if (dstObj != null && (dstObj instanceof PIXI.Circle))
            {
                dstObj.x = src.x;
                dstObj.y = src.y;
                dstObj.radius = src.radius;
            }
            else
            {
                dst[dstKey] = src;
            }
        }
    }

    @RS.TweenTypeHandler
    class EllipseTweenTypeHandler implements RS.ITweenTypeHandler<PIXI.Ellipse>
    {
        /** Gets if this handler can handle the specified object. */
        public canHandle(obj: any): obj is PIXI.Ellipse
        {
            return RS.Is.object(obj) && (obj instanceof PIXI.Ellipse);
        }
        
        /** Clones the specified object. */
        public clone(original: PIXI.Ellipse): PIXI.Ellipse
        {
            return original != null ? original.clone() : null;
        }

        /** Creates a new object, interpolated between a and b by mu. */
        public interpolate(a: PIXI.Ellipse, b: PIXI.Ellipse, mu: number): PIXI.Ellipse
        {
            return new PIXI.Ellipse(
                RS.Math.linearInterpolate(a.x, b.x, mu), RS.Math.linearInterpolate(a.y, b.y, mu),
                RS.Math.linearInterpolate(a.width, b.width, mu), RS.Math.linearInterpolate(a.height, b.height, mu));
        }

        /** Copies properties of src into dst. */
        public apply(src: PIXI.Ellipse, dst: object, dstKey: string): void
        {
            const dstObj = dst[dstKey];
            if (dstObj != null && (dstObj instanceof PIXI.Ellipse))
            {
                dstObj.x = src.x;
                dstObj.y = src.y;
                dstObj.width = src.width;
                dstObj.height = src.height;
            }
            else
            {
                dst[dstKey] = src;
            }
        }
    }

    @RS.TweenTypeHandler
    class RectTweenTypeHandler implements RS.ITweenTypeHandler<PIXI.Rectangle>
    {
        /** Gets if this handler can handle the specified object. */
        public canHandle(obj: any): obj is PIXI.Rectangle
        {
            return RS.Is.object(obj) && (obj instanceof PIXI.Rectangle);
        }
        
        /** Clones the specified object. */
        public clone(original: PIXI.Rectangle): PIXI.Rectangle
        {
            return original != null ? original.clone() : null;
        }

        /** Creates a new object, interpolated between a and b by mu. */
        public interpolate(a: PIXI.Rectangle, b: PIXI.Rectangle, mu: number): PIXI.Rectangle
        {
            return new PIXI.Rectangle(
                RS.Math.linearInterpolate(a.x, b.x, mu), RS.Math.linearInterpolate(a.y, b.y, mu),
                RS.Math.linearInterpolate(a.width, b.width, mu), RS.Math.linearInterpolate(a.height, b.height, mu));
        }

        /** Copies properties of src into dst. */
        public apply(src: PIXI.Rectangle, dst: object, dstKey: string): void
        {
            const dstObj = dst[dstKey];
            if (dstObj != null && (dstObj instanceof PIXI.Rectangle))
            {
                dstObj.copy(src);
            }
            else
            {
                dst[dstKey] = src;
            }
        }
    }

    @RS.TweenTypeHandler
    class RoundRectTweenTypeHandler implements RS.ITweenTypeHandler<PIXI.RoundedRectangle>
    {
        /** Gets if this handler can handle the specified object. */
        public canHandle(obj: any): obj is PIXI.RoundedRectangle
        {
            return RS.Is.object(obj) && (obj instanceof PIXI.RoundedRectangle);
        }
        
        /** Clones the specified object. */
        public clone(original: PIXI.RoundedRectangle): PIXI.RoundedRectangle
        {
            return original != null ? original.clone() : null;
        }

        /** Creates a new object, interpolated between a and b by mu. */
        public interpolate(a: PIXI.RoundedRectangle, b: PIXI.RoundedRectangle, mu: number): PIXI.RoundedRectangle
        {
            return new PIXI.RoundedRectangle(
                RS.Math.linearInterpolate(a.x, b.x, mu), RS.Math.linearInterpolate(a.y, b.y, mu),
                RS.Math.linearInterpolate(a.width, b.width, mu), RS.Math.linearInterpolate(a.height, b.height, mu),
                RS.Math.linearInterpolate(a.radius, b.radius, mu));
        }

        /** Copies properties of src into dst. */
        public apply(src: PIXI.RoundedRectangle, dst: object, dstKey: string): void
        {
            const dstObj = dst[dstKey];
            if (dstObj != null && (dstObj instanceof PIXI.RoundedRectangle))
            {
                dstObj.x = src.x;
                dstObj.y = src.y;
                dstObj.width = src.width;
                dstObj.height = src.height;
                dstObj.radius = src.radius;
            }
            else
            {
                dst[dstKey] = src;
            }
        }
    }
}