/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * A container used for super or sub sampling it's contents.
     */
    export class RenderContainer extends PIXI.Container implements RS.IDisposable
    {
        protected _innerContainer: PIXI.Container;
        protected _sprite: Sprite;
        protected _resolution: number;
        protected _renderTexture: IRenderTexture;

        protected _alwaysRefresh: boolean = false;
        protected _refreshNeeded: boolean = false;
        protected _disposed: boolean = false;

        // protected _renderer: PIXI.WebGLRenderer;

        /** Gets or sets if this container should ALWAYS redraw children. */
        public get alwaysRefresh() { return this._alwaysRefresh; }
        public set alwaysRefresh(value) { this._alwaysRefresh = value; }

        /** Gets or sets if this container needs to redraw children on the next frame */
        public get refreshNeeded() { return this._refreshNeeded; }
        public set refreshNeeded(value)
        {
            this._refreshNeeded = value;
            this.cacheAsBitmap = false;
        }

        public get tint() { return this._sprite.tint; }
        public set tint(value) { this._sprite.tint = value; }

        public get blendMode() { return this._sprite.blendMode; }
        public set blendMode(value) { this._sprite.blendMode = value; }

        /** Gets if this container has been disposed. */
        public get isDisposed() { return this._disposed; }

        public constructor(public readonly settings: RenderContainer.Settings)
        {
            super();
            this._innerContainer = new PIXI.Container();
            this._sprite = new Sprite();
            this._sprite.visible = false;
            super.addChild(this._sprite);
            this.interactiveChildren = false;

            if (settings.alwaysRefresh != null) { this._alwaysRefresh = settings.alwaysRefresh; }
            this._refreshNeeded = true;

            this._resolution = this.settings.mode === RenderContainer.Mode.Subsample ? this.settings.resolution : (1 << this.settings.power);
        }

        public addChild<T extends PIXI.DisplayObject>(child: T, ...additionalChildren: PIXI.DisplayObject[]): T
        {
            this._innerContainer.addChild(child);
            for (const additionalChild of additionalChildren)
            {
                this._innerContainer.addChild(additionalChild);
            }
            return child;
        }

        public addChildAt<T extends PIXI.DisplayObject>(child: T, index: number): T
        {
            return this._innerContainer.addChildAt(child, index);
        }

        public removeChild(child: PIXI.DisplayObject): PIXI.DisplayObject
        {
            return this._innerContainer.removeChild(child);
        }

        public removeChildAt(index: number): PIXI.DisplayObject
        {
            return this._innerContainer.removeChildAt(index);
        }

        public removeChildren(beginIndex?: number, endIndex?: number): PIXI.DisplayObject[]
        {
            return this._innerContainer.removeChildren(beginIndex, endIndex);
        }

        public removeAllChildren(): PIXI.DisplayObject[]
        {
            return this._innerContainer.removeChildren();
        }

        public getChildAt(index: number): PIXI.DisplayObject
        {
            return this._innerContainer.getChildAt(index);
        }

        public getChildByName(name: string): PIXI.DisplayObject
        {
            return this._innerContainer.getChildByName(name);
        }

        public getChildIndex(child: PIXI.DisplayObject): number
        {
            return this._innerContainer.getChildIndex(child);
        }

        public setChildIndex(child: PIXI.DisplayObject, index: number): void
        {
            this._innerContainer.setChildIndex(child, index);
        }

        public swapChildren(child: PIXI.DisplayObject, child2: PIXI.DisplayObject): void
        {
            this._innerContainer.swapChildren(child, child2);
        }

        public destroy()
        {
            this.dispose();
        }

        /**
         * Disposes this container.
         */
        public dispose()
        {
            if (this._disposed) { return; }
            // this._renderer = null;
            super.destroy(true);
            this._sprite = null;
            this._innerContainer = null;
            this._disposed = true;
        }

        public render(renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer, force: boolean = false): void
        {
            if (this._refreshNeeded || force)
            {
                this._render(renderer);
                this._refreshNeeded = false;
            }
        }

        protected _renderWebGL(renderer: PIXI.WebGLRenderer): void
        {
            if (this._alwaysRefresh || this._refreshNeeded)
            {
                const oldRT = renderer._activeRenderTarget;
                renderer.currentRenderer.flush();

                this._render(renderer);
                this._refreshNeeded = false;

                renderer.bindRenderTarget(oldRT);
            }
        }

        protected _renderCanvas(renderer: PIXI.CanvasRenderer): void
        {
            if (this._alwaysRefresh || this._refreshNeeded)
            {
                this._render(renderer);
                this._refreshNeeded = false;
            }
        }

        protected _render(renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer): void
        {
            this.cacheAsBitmap = false;
            const padding = this.settings.padding || 0;
            const doublePadding = padding * 2.0;

            // innerContainer -> resRT -> (supersample/subsample) -> cacheRT -> screen (via sprite)

            let res = this._resolution;

            // Pull our size
            const bounds = this._innerContainer.filterArea || this._innerContainer.getLocalBounds();
            const width = Math.ceil(bounds.width * res + doublePadding), height = Math.ceil(bounds.height * res + doublePadding);

            // Update cache RT (POT = power of two)
            let widthPOT = RS.Math.nextPowerOf2(width), heightPOT = RS.Math.nextPowerOf2(height);

            // Scale down to maximum texture size in WebGL.
            if (renderer instanceof PIXI.WebGLRenderer)
            {
                const maxDimen = Capabilities.WebGL.get().maxTextureSize;
                if (widthPOT > maxDimen || heightPOT > maxDimen)
                {
                    const maxPOT = Math.prevPowerOf2(maxDimen);
                    const shouldFitW = widthPOT > heightPOT;
                    const scale = maxPOT / (shouldFitW ? widthPOT : heightPOT);
                    widthPOT *= scale;
                    heightPOT *= scale;
                    res *= scale;
                }
            }

            if (widthPOT === 0 || heightPOT === 0)
            {
                this._sprite.texture = null;
                this._sprite.visible = false;
                return;
            }

            if (this._renderTexture)
            {
                IRenderTexturePool.get().release(this._renderTexture);
            }

            const rt = this._renderTexture = IRenderTexturePool.get().acquire(widthPOT, heightPOT, true) as RenderTexture;
            this._sprite.texture = new PIXI.Texture(rt.baseTexture);
            this._sprite.visible = true;

            //this._cacheRT.frame = new PIXI.Rectangle(0, 0, w, h);
            const x = Math.floor(bounds.x * res),
                  y = Math.floor(bounds.y * res);

            this._sprite.position.set(Math.floor(bounds.x) - padding, Math.floor(bounds.y) - padding);
            this._sprite.scale.set(1.0 / res, 1.0 / res);

            this._innerContainer.position.set(padding - x, padding - y);
            this._innerContainer.scale.set(res, res);

            rt.clearInternal(renderer);
            rt.renderInternal(renderer, this._innerContainer);
            this.cacheAsBitmap = true;
        }
    }

    export namespace RenderContainer
    {
        export enum Mode
        {
            /**
             * Render contents larger, then downsample to display on screen.
             * Increases fillrate requirement, but produces higher quality results.
             */
            Supersample,

            /**
             * Render contents smaller, then upsample to display on screen.
             * Decreases fillrate requirement, but produces lower quality results.
             */
            Subsample
        }

        export interface BaseSettings<TMode extends Mode>
        {
            /** Subsample or supersample? */
            mode: TMode;
            alwaysRefresh?: boolean;
            padding?: number;
        }

        export interface SupersampleSettings extends BaseSettings<Mode.Supersample>
        {
            /**
             * How many degrees to supersample by.
             * e.g. 1 = 200%, 2 = 400%, 3 = 800% etc
             */
            power: number;
        }

        export interface SubsampleSettings extends BaseSettings<Mode.Subsample>
        {
            /**
             * Resolution to subsample, scale of 0-1.
             * e.g. 0.5 = 50%
             */
            resolution: number;
        }

        export type Settings = SupersampleSettings | SubsampleSettings;
    }
}
