/** @internal  */
namespace RS.Rendering.Pixi.TextLayer
{
    export abstract class Base implements RS.IDisposable
    {
        protected _text: TextBase;
        protected _container: PIXI.Container;
        protected _layerInfo: TextOptions.LayerInfo;
        protected _isDisposed: boolean;
        protected _localBoundsRect: PIXI.Rectangle;

        public abstract get tint(): number;
        public abstract set tint(value: number);

        public abstract get blendMode(): number;
        public abstract set blendMode(value: number);

        public get isDisposed() { return this._isDisposed; }

        public constructor(text: TextBase, container: PIXI.Container, layerInfo: TextOptions.LayerInfo)
        {
            this._text = text;
            this._container = container;
            this._layerInfo = layerInfo;
            this._localBoundsRect = new PIXI.Rectangle();
        }

        public getLocalBounds(): PIXI.Rectangle
        {
            return this._localBoundsRect;
        }

        public abstract update(lineData: TextOptions.LineData[], width: number, height: number, measurements: Base.LineMeasurements, baseline: TextBaseline): void;

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }
    }

    export namespace Base
    {
        /** Encapsulates various vertical measurements for a line of text, in px. */
        export interface LineMeasurements
        {
            /* Total height of the line */
            totalHeight: number;

            /** Distance from top of text to alphabetic baseline. */
            alphabeticHeight: number;

            /** Distance from alphabetic baseline to bottom of text. */
            underhangHeight: number;
        }
    }
}
