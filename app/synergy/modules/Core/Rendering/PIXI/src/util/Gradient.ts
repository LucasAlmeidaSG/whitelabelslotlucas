/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * Creates a gradient texture from the given texture size and gradient definition.
     * @param width
     * @param height
     * @param gradient
     */
    export function createGradientTexture(width: number, height: number, gradient: RS.Rendering.Gradient): ISubTexture
    {
        const canvas = document.createElement("canvas");
        canvas.width = RS.Math.nextPowerOf2(width);
        canvas.height = RS.Math.nextPowerOf2(height);
        const ctx = canvas.getContext("2d");
        let canvasGradient: CanvasGradient;
        switch (gradient.type)
        {
            case RS.Rendering.Gradient.Type.Vertical:
                canvasGradient = ctx.createLinearGradient(0, 0, 0, height);
                break;
            case RS.Rendering.Gradient.Type.Horizontal:
                canvasGradient = ctx.createLinearGradient(0, 0, width, 0);
                break;
            case RS.Rendering.Gradient.Type.Linear:
                canvasGradient = ctx.createLinearGradient(gradient.posA.x, gradient.posA.y, gradient.posB.x, gradient.posB.y);
                break;
            case RS.Rendering.Gradient.Type.Radial:
                canvasGradient = ctx.createRadialGradient(gradient.pos.x, gradient.pos.y, 0, gradient.pos.x, gradient.pos.y, gradient.radius);
                break;
            default:
                return null;
        }
        let stops : Gradient.Stop[];
        if (Is.array(gradient.stops))
        {
            stops = gradient.stops;
        }
        else
        {
            stops = gradient.stops();
        }
        for (const stop of stops)
        {
            canvasGradient.addColorStop(gradient.reverse ? 1 - stop.offset : stop.offset, stop.color.toRGBAString());
        }
        ctx.fillStyle = canvasGradient;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        const baseTex = new PIXI.BaseTexture(canvas, PIXI.SCALE_MODES.LINEAR, 1.0);
        // Log.debug(`Created gradient texture (${width}x${height})`);
        return new SubTexture(new Texture(baseTex), new Rendering.Rectangle(0, 0, width, height));
    }

    /**
     * Creates a simple linear horizontal gradient texture with a sensible size from the given color stops.
     * @param colorStops
     * @param vertical
     */
    export function createSimpleGradientTexture(colorStops: RS.Rendering.Gradient.Stops, vertical: boolean = false): ISubTexture
    {
        // Calculate a good resolution based on the minimum gap between two offsets in the color stops
        // for example:
        // - if there are 2 stops at 0.0 and 1.0, the gap is 1.0, therefore we only need 2 pixels to represent that gradient
        // - if there are 3 stops at 0.0, 0.5, 1.0, the gap is 0.5, therefore we need 3 pixels to represent the gradient
        // - if there are 3 stops at 0.0, 0.1, 1.0, the gap is 0.1, therefore we need 10 pixels to represent the gradient
        if (Is.func(colorStops))
        {
            colorStops = colorStops();
        }

        let smallestGap = 1.0;
        for (let i = 1; i < colorStops.length; i++)
        {
            const gap = colorStops[i].offset - colorStops[i - 1].offset;
            if (gap > 0)
            {
                smallestGap = Math.min(smallestGap, gap);
            }
        }
        const resolution = Math.max(Math.min(1.0 + Math.floor(1.0 / smallestGap), 512), 32);

        // Create gradient
        return createGradientTexture(vertical ? 1 : resolution, vertical ? resolution : 1, {
            type: RS.Rendering.Gradient.Type.Linear,
            stops: colorStops,
            posA: { x: 0, y: 0 },
            posB: { x: vertical ? 0 : resolution, y: vertical ? resolution : 0 }
        });
    }

    RS.Rendering.Gradient.createTexture = createGradientTexture;
    RS.Rendering.Gradient.createSimpleTexture = createSimpleGradientTexture;
}
