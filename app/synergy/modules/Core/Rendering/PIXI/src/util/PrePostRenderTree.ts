/**
 * Implements a flattened bottom-up tree of objects requiring pre/post-render calls.
 *
 * The tree is an array of children in descending order of depth. \
 * This orders pre/post-render event handling as if it were done by recursing over the scene graph like normal rendering.
 * It also allows only children which have pre/post-render event listeners to be considered, rather than recursing all objects.
 *
 * @internal
 */
namespace RS.Rendering.Pixi.PrePostRenderTree
{
    /** Removes child from the pre/post-render tree. */
    export function remove(tree: GenericObject[], child: GenericObject)
    {
        const idx = tree.indexOf(child);
        if (idx === -1) { return; }
        tree.splice(idx, 1);
    }

    /** Used to store invalid display objects found during an add() call. */
    const invalid: number[] = [];

    /**
     * Adds child to the pre/post-render tree.
     * @param ancestor Used to calculate child depth for array ordering.
     */
    export function add(tree: GenericObject[], child: GenericObject, ancestor: GenericContainer)
    {
        if (tree.indexOf(child) !== -1) { return; }

        const childDepth = depth(child, ancestor);
        RS.Util.insert(child, tree, function (candidate, _, index)
        {
            try
            {
                const candidateDepth = depth(candidate, ancestor);
                return childDepth >= candidateDepth ? -1 : 0;
            }
            catch (err)
            {
                Log.error(err);
                invalid.push(index);
                return 0;
            }
        });

        if (invalid.length > 0)
        {
            // Clean up disposed others found
            invalid.sort((a, b) => b - a);
            for (const childToRemove of invalid)
            {
                tree.splice(childToRemove, 1);
            }
            Log.warn(`PrePostRenderTree: cleaned up ${invalid.length} invalid display objects`);
            invalid.length = 0;
        }
    }

    /** Returns the depth of child in ancestor. */
    export function depth(child: GenericObject, ancestor: GenericContainer): number
    {
        let result = 1, thisParent = child.parent;
        while (thisParent !== ancestor)
        {
            if (thisParent == null) { throw new Error("Internal rendering error: child not in parent"); }
            result++;
            thisParent = thisParent.parent;
        }
        return result;
    }

    /** Stores the last error (this could happen 60 times a sec, we don't want to spam the console etc.) */
    export let lastError: any;

    /** Executes pre-render handlers for the given tree. */
    export function preRender(tree: ReadonlyArray<GenericObject>, renderer: GenericRenderer)
    {
        for (const child of tree)
        {
            try
            {
                child.onPreRender.publish(
                {
                    target: child,
                    canvas: child.stage && child.stage.canvas,
                    renderer
                });
            }
            catch (err)
            {
                if (!lastError)
                {
                    Log.warn("PrePostRenderTree: preRender() encountered an error; future errors will not be logged. Error saved to RS.Rendering.Pixi.PrePostRenderTree.lastError");
                }
                lastError = err;
            }
        }
    }

    /** Executes post-render handlers for the given tree. */
    export function postRender(tree: ReadonlyArray<GenericObject>, renderer: GenericRenderer)
    {
        for (const child of tree)
        {
            try
            {
                child.onPostRender.publish(
                {
                    target: child,
                    canvas: child.stage && child.stage.canvas,
                    renderer
                });
            }
            catch (err)
            {
                if (!lastError)
                {
                    Log.warn("PrePostRenderTree: postRender() encountered an error; future errors will not be logged. Error saved to RS.Rendering.Pixi.PrePostRenderTree.lastError");
                }
                lastError = err;
            }
        }
    }
}