/// <reference path="CreateIndicesForQuads.ts" />
/** @internal  */
namespace RS.Rendering.Pixi
{
    const tmpSize1: Math.Size2D = { w: 0, h: 0 };
    const tmpSize2: Math.Size2D = { w: 0, h: 0 };

    export namespace Meters
    {
        /*export const spriteCount = Profiling.Meter.create(Profiling.IMeter.Type.Counter, "Sprites", true);
        export const drawCallCount = Profiling.Meter.create(Profiling.IMeter.Type.Counter, "Draw Calls", true);
        export const batchMissToBlendMode = Profiling.Meter.create(Profiling.IMeter.Type.Counter, "Batch Misses /blendMode", true);
        export const batchMissToTexture = Profiling.Meter.create(Profiling.IMeter.Type.Counter, "Batch Misses /texture", true);
        export const batchMissToShader = Profiling.Meter.create(Profiling.IMeter.Type.Counter, "Batch Misses /shader", true);*/
    }
    @RS.HasCallbacks
    export class SpriteRenderer extends PIXI.ObjectRenderer
    {
        public renderer: PIXI.WebGLRenderer;

        protected _indices: Uint16Array;
        protected _indexBuffer: PIXI.glCore.GLBuffer;

        protected _batchBuffers: BatchBuffer[];

        protected _baseShader: PIXI.Shader;

        protected _maxTextures: number;

        protected _draws: SpriteRenderer.Draw[];
        protected _drawCount: number;
        protected _quadCount: number;

        public constructor(renderer)
        {
            super(renderer);
            this.renderer.on("prerender", this.onPrerender);

            this._draws = [];
        }

        /**
         * Sets up the renderer context and necessary buffers.
         */
        public onContextChange(): void
        {
            const renderer = this.renderer;
            const gl = renderer.gl;

            // Get the maximum number of textures we can support
            if (renderer["legacy"])
            {
                this._maxTextures = 1;
            }
            else
            {
                this._maxTextures = Math.min(gl.getParameter(gl.MAX_TEXTURE_IMAGE_UNITS), PIXI.settings.SPRITE_MAX_TEXTURES);
                this._maxTextures = checkMaxIfStatmentsInShader(this._maxTextures, gl);
            }

            // Allocate index buffer
            this._indices = createIndicesForQuads(SpriteRenderer.maxQuadsPerBatch);
            this._indexBuffer = PIXI.glCore.GLBuffer.createIndexBuffer(gl, this._indices, gl.STATIC_DRAW);

            // Allocate base shader
            this._baseShader = generateMultiTextureShader(gl, this._maxTextures);

            // Generate batch buffers
            const numBatchBuffers = Math.ceil(RS.Math.log2(SpriteRenderer.maxQuadsPerBatch)) + 1;
            this._batchBuffers = new Array<BatchBuffer>(numBatchBuffers);
            for (let i = 0; i < numBatchBuffers; ++i)
            {
                const quadCount = 1 << i;
                this._batchBuffers[i] = new BatchBuffer({
                    renderer: this.renderer,
                    indexBuffer: this._indexBuffer,
                    shader: this._baseShader,
                    quadCount
                });
            }
        }

        /**
         * Renders the sprite object.
         */
        public render(sprite: PIXI.Sprite): void
        {
            // Flush if we've hit the quad limit
            if (this._quadCount >= SpriteRenderer.maxQuadsPerBatch)
            {
                this.flush();
            }

            // Get the texture
            const tex = sprite.texture;
            if (tex == null) { return; }
            if (tex["_uvs"] == null) { return; }
            const baseTex = tex.baseTexture;
            if (baseTex == null) { return; }

            // Get the shader
            const shader = sprite["_wrapper"] && (sprite["_wrapper"] as (Sprite | AnimatedSprite | MovieClip)).shader as Shader;

            // Check if we need to start a new draw
            let draw = this._draws[this._drawCount - 1];
            let startNewDraw = draw == null;
            if (!startNewDraw && draw.shader !== shader)
            {
                //Meters.batchMissToShader.add(1);
                startNewDraw = true;
            }
            if (!startNewDraw && draw.blendMode !== sprite.blendMode)
            {
                //Meters.batchMissToBlendMode.add(1);
                startNewDraw = true;
            }
            if (!startNewDraw && (shader == null))
            {
                // Standard multi-texture shader
                if (draw.textures.indexOf(baseTex) === -1 && draw.textures.length >= this._maxTextures)
                {
                    // Not enough space for an additional texture in this draw
                    //Meters.batchMissToTexture.add(1);
                    startNewDraw = true;
                }
            }
            if (!startNewDraw && (shader != null))
            {
                // Custom single-texture shader
                if (draw.textures[0] !== baseTex)
                {
                    // We can't draw two different primary textures in a single draw
                    //Meters.batchMissToTexture.add(1);
                    startNewDraw = true;
                }
            }
            if (startNewDraw)
            {
                this.startNewDraw(shader, sprite.blendMode);
                draw = this._draws[this._drawCount - 1];
            }

            // Add to draw
            draw.sprites.push(sprite);
            if (draw.textures.indexOf(baseTex) === -1)
            {
                draw.textures.push(baseTex);
            }
            if (shader)
            {
                const uniforms = shader.uniforms;
                for (const uniformName in uniforms)
                {
                    const uniform: RS.Rendering.ShaderUniform = uniforms[uniformName];
                    if (uniform.type === RS.Rendering.ShaderUniform.Type.Sampler2D && uniform.value != null)
                    {
                        const texture = Is.assetReference(uniform.value) ? Asset.Store.getAsset(uniform.value).asset : uniform.value;
                        if (texture != null)
                        {
                            const baseTexture = (texture as Texture).baseTexture;
                            if (baseTexture != null && draw.textures.indexOf(baseTexture) === -1)
                            {
                                draw.textures.push(baseTexture);
                            }
                        }
                    }
                }
            }
            //Meters.spriteCount.add(1);
            this._quadCount++;
        }

        /**
         * Renders the content and empties the current batch.
         */
        public flush(): void
        {
            if (this._quadCount === 0) { return; }

            const renderer = this.renderer;
            const gl = renderer.gl;
            const maxTextures = this._maxTextures;

            // Select a batch buffer based on how many quads we have to draw
            const batchBufferIdx = Math.ceil(RS.Math.log2(this._quadCount));
            const batchBuffer = this._batchBuffers[batchBufferIdx];
            batchBuffer.bind();

            // Get current state
            const curBoundTextures = [...renderer.boundTextures];

            // Render all sprites to the vertex buffer
            let quadIndex = 0;
            for (let drawID = 0, drawCnt = this._drawCount; drawID < drawCnt; ++drawID)
            {
                const draw = this._draws[drawID];
                draw.startQuad = quadIndex;

                // Allocate textures
                const touch = this.renderer.textureGC.count;
                for (const tex of draw.textures)
                {
                    const slotID = this.allocateTextureSlot(tex, curBoundTextures, draw.textures);
                    const oldTex = curBoundTextures[slotID];
                    if (oldTex != null) { oldTex["_virtualBoundId"] = -1; }
                    draw.textureBindings[slotID] = tex;
                    curBoundTextures[slotID] = tex;
                    tex["touched"] = touch;
                    tex["_virtualBoundId"] = slotID;
                }

                // Iterate each sprite
                for (const sprite of draw.sprites)
                {
                    // Draw into buffer
                    this.drawSprite(sprite, quadIndex, batchBuffer);
                    quadIndex++;
                }

                // Set quad count
                draw.quadCount = quadIndex - draw.startQuad;
            }

            // Upload the buffer
            batchBuffer.upload();

            // Dispatch draw calls
            let curGLShader: PIXI.Shader = null;
            let curShader: RS.Rendering.IShader = null;
            let curBlendMode: number = null;
            for (let drawID = 0, drawCnt = this._drawCount; drawID < drawCnt; ++drawID)
            {
                const draw = this._draws[drawID];

                // Switch shader
                const glShader = draw.shader ? draw.shader.shaderProgram.getRawShaderFor(renderer) : this._baseShader;
                if (glShader !== curGLShader)
                {
                    curGLShader = glShader;
                    renderer.bindShader(curGLShader);
                }

                // Sync uniforms
                if (draw.shader !== curShader)
                {
                    curShader = draw.shader;
                    if (glShader !== this._baseShader)
                    {
                        // Custom shader
                        const rt = renderer._activeRenderTarget;
                        tmpSize1.w = rt.size.width;
                        tmpSize1.h = rt.size.height;

                        Math.Size2D.set(tmpSize2, 0, 0);
                        for (const binding of draw.textureBindings)
                        {
                            if (binding)
                            {
                                Math.Size2D.set(tmpSize2, binding.width, binding.height);
                                break;
                            }
                        }

                        draw.shader.syncUniforms(glShader, tmpSize1, tmpSize2, !rt.root);
                        const uniforms = draw.shader.uniforms;
                        for (const uniformName in uniforms)
                        {
                            const uniform: RS.Rendering.ShaderUniform = uniforms[uniformName];
                            if (uniform.type === RS.Rendering.ShaderUniform.Type.Sampler2D && uniform.value != null)
                            {
                                const texture = Is.assetReference(uniform.value) ? Asset.Store.getAsset(uniform.value).asset : uniform.value;
                                if (texture)
                                {
                                    const baseTexture = (texture as Texture).baseTexture;
                                    if (baseTexture != null)
                                    {
                                        curGLShader.uniforms[uniformName] = draw.textureBindings.indexOf(baseTexture);
                                    }
                                }
                            }
                        }
                        if ("uSampler" in curShader.uniforms)
                        {
                            curGLShader.uniforms["uSampler"] = draw.textureBindings.indexOf(draw.textures[0]);
                        }
                    }
                }

                // Switch blend mode
                //if (draw.blendMode !== curBlendMode)
                {
                    curBlendMode = draw.blendMode;
                    renderer.setBlendMode(curBlendMode);
                }

                // Bind textures
                for (let i = 0; i < maxTextures; ++i)
                {
                    const tex = draw.textureBindings[i];
                    if (tex != null)
                    {
                        this.renderer.bindTexture(tex, i, true);
                    }
                }

                // Draw
                batchBuffer.draw(draw.startQuad, draw.quadCount);
                // if (gl.getError() != 0) { debugger; }
                //Meters.drawCallCount.add(1);
            }

            // All draws processed
            this._quadCount = 0;
            this._drawCount = 0;
        }

        /**
         * Starts a new sprite batch.
         */
        public start(): void
        {
            // Clear draws
            this._quadCount = 0;
            this._drawCount = 0;
        }

        /**
         * Stops and flushes the current batch.
         */
        public stop(): void
        {
            this.flush();
        }

        /**
         * Destroys the SpriteRenderer.
         */
        public destroy(): void
        {
            this._draws.length = 0;
            this._draws = null;
            for (const batchBuffer of this._batchBuffers)
            {
                batchBuffer.dispose();
            }
            this._batchBuffers.length = 0;
            this._batchBuffers = null;
            this._indexBuffer.destroy();
            this._indexBuffer = null;
            this._baseShader.destroy();
            this._baseShader = null;
            this._indices = null;
        }

        @RS.Callback
        protected onPrerender(): void
        {
            return;
        }

        protected allocateTextureSlot(texture: PIXI.BaseTexture, boundTextures: PIXI.BaseTexture[], useTextures: PIXI.BaseTexture[]): number
        {
            const idx = boundTextures.indexOf(texture);
            if (idx === -1)
            {
                // Find a slot which isn't occupied by a used texture
                for (let i = 0, l = this._maxTextures; i < l; ++i)
                {
                    const oldTex = boundTextures[i];
                    if (oldTex == null || useTextures.indexOf(oldTex) === -1)
                    {
                        return i;
                    }
                }
                RS.Log.warn(`Could not allocate slot for texture`);
                debugger;
            }
            else
            {
                // Already bound, reuse that slot
                return idx;
            }
        }

        protected drawSprite(sprite: PIXI.Sprite, quadIndex: number, batchBuffer: BatchBuffer): void
        {
            const tex = sprite.texture;
            const baseTex = tex.baseTexture;

            const vF32 = batchBuffer.verticesF32;
            const vI32 = batchBuffer.verticesI32;

            const uvs = sprite.texture["_uvs"].uvsUint32;
            const vertexData = sprite.vertexData;

            const index = quadIndex * SpriteRenderer.vertexSize * SpriteRenderer.verticesPerQuad;

            if (this.renderer.roundPixels)
            {
                const resolution = this.renderer.resolution;

                // xy
                vF32[index] = ((vertexData[0] * resolution) | 0) / resolution;
                vF32[index + 1] = ((vertexData[1] * resolution) | 0) / resolution;

                // xy
                vF32[index + 5] = ((vertexData[2] * resolution) | 0) / resolution;
                vF32[index + 6] = ((vertexData[3] * resolution) | 0) / resolution;

                // xy
                vF32[index + 10] = ((vertexData[4] * resolution) | 0) / resolution;
                vF32[index + 11] = ((vertexData[5] * resolution) | 0) / resolution;

                // xy
                vF32[index + 15] = ((vertexData[6] * resolution) | 0) / resolution;
                vF32[index + 16] = ((vertexData[7] * resolution) | 0) / resolution;
            }
            else
            {
                // xy
                vF32[index] = vertexData[0];
                vF32[index + 1] = vertexData[1];

                // xy
                vF32[index + 5] = vertexData[2];
                vF32[index + 6] = vertexData[3];

                // xy
                vF32[index + 10] = vertexData[4];
                vF32[index + 11] = vertexData[5];

                // xy
                vF32[index + 15] = vertexData[6];
                vF32[index + 16] = vertexData[7];
            }

            vI32[index + 2] = uvs[0];
            vI32[index + 7] = uvs[1];
            vI32[index + 12] = uvs[2];
            vI32[index + 17] = uvs[3];

            /* eslint-disable max-len */
            const alpha = Math.min(sprite.worldAlpha, 1.0);
            // we dont call extra function if alpha is 1.0, that's faster
            const argb = alpha < 1.0 && baseTex.premultipliedAlpha ? PIXI.utils.premultiplyTint(sprite["_tintRGB"], alpha)
                : sprite["_tintRGB"] + (alpha * 255 << 24);

            vI32[index + 3] = vI32[index + 8] = vI32[index + 13] = vI32[index + 18] = argb;
            vF32[index + 4] = vF32[index + 9] = vF32[index + 14] = vF32[index + 19] = baseTex["_virtualBoundId"];
            /* eslint-enable max-len */
        }

        protected startNewDraw(shader: RS.Rendering.Pixi.Shader, blendMode: number): void
        {
            if (this._drawCount >= this._draws.length)
            {
                // Create a new draw struct
                const draw = {
                    shader,
                    blendMode,
                    startQuad: 0,
                    quadCount: 0,
                    textures: [],
                    sprites: [],
                    textureBindings: new Array<PIXI.BaseTexture>(this._maxTextures)
                };
                this._draws.push(draw);
            }
            else
            {
                // Clear an existing draw struct
                const draw = this._draws[this._drawCount];
                draw.shader = shader;
                draw.blendMode = blendMode;
                draw.textures.length = 0;
                draw.startQuad = 0;
                draw.quadCount = 0;
                draw.sprites.length = 0;
                draw.textureBindings.length = 0;
                draw.textureBindings.length = this._maxTextures;
            }
            this._drawCount++;
        }
    }

    export namespace SpriteRenderer
    {
        export interface Draw
        {
            shader: RS.Rendering.Pixi.Shader | null;
            blendMode: number;
            textures: PIXI.BaseTexture[];
            textureBindings: PIXI.BaseTexture[];
            sprites: PIXI.Sprite[];
            startQuad: number;
            quadCount: number;
        }

        export const verticesPerQuad = 4;
        export const indicesPerQuad = 6;

        export const maxQuadsPerBatch = 1024;
        export const maxVerticesPerBatch = maxQuadsPerBatch * verticesPerQuad;
        export const maxIndicesPerBatch = maxQuadsPerBatch * indicesPerQuad;

        /**
         * Number of words (32-bit) per vertex as stored in the vertex buffer.
         * aVertexPosition(2), aTextureCoord(1), aColor(1), aTextureId(1) = 5
         */
        export const vertexSize = 5;
        export const vertexByteSize = vertexSize * 4;

        export const indexByteSize = 2; // ushort
    }

    /**
     * Remember the old sprite rendering plugin before we replace it with our own sprite renderer
     */
    PIXI.WebGLRenderer.registerPlugin("oldsprite", PIXI.WebGLRenderer.__plugins["sprite"]);
    
    PIXI.WebGLRenderer.registerPlugin("sprite", SpriteRenderer);
}
