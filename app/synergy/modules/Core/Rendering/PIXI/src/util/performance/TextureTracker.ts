/** @internal */
namespace RS.Rendering.Pixi.Profiling
{
    import GLTexture = PIXI.glCore.GLTexture;

    function toFormattedSize(bits: number): string
    {
        const order = 1024;
        let quantity = bits, unit: string = "";

        if (bits < 8)
        {
            unit = "bits"
        }
        else
        {
            const bytes = bits / 8;
            // If it's less than 1 KB and non-integer bytes, display bits
            if (bytes % 1 && bytes < order)
            {
                quantity = bits;
                unit = "bits";
            }
            else
            {
                const kilobytes = bytes / order;
                if (kilobytes < 1)
                {
                    quantity = bytes;
                    unit = "bytes";
                }
                else
                {
                    const megabytes = kilobytes / order;
                    if (megabytes < 1)
                    {
                        quantity = Math.ceil(kilobytes);
                        unit = "kilobytes";
                    }
                    else
                    {
                        const gigabytes = megabytes / order;
                        if (gigabytes < 1)
                        {
                            quantity = Math.ceil(megabytes);
                            unit = "megabytes";
                        }
                        else
                        {
                            quantity = gigabytes;
                            unit = "gigabytes";
                        }
                    }
                }
            }
        }

        return unit ? `${quantity} ${unit}` : `${quantity}`;
    }

    export class TextureTracker
    {
        /** Whether or not texture tracking should be logged. */
        protected readonly _debugMode = RS.URL.getParameterAsBool("renderdebug", false);
        /** Whether or not textures should be tracked. */
        protected readonly _enabled = this._debugMode;

        private readonly __textures: GLTexture[] = [];
        private readonly __sources: string[] = [];

        public get uploadedSize()
        {
            if (!this.__textures) { return 0; }

            return Find.sum(this.__textures, (t) => this.estimateSize(t));
        }

        public get uploadData()
        {
            if (!this.__textures) { return []; }

            const data = new Array<TextureTracker.UploadData>(this.__textures.length);
            for (let i = 0; i < data.length; i++)
            {
                const texture = this.__textures[i], source = this.__sources[i];
                const size = this.estimateSize(texture);
                data[i] = { source, size };
            }
            data.sort((a, b) => b.size - a.size);
            return data;
        }

        public track(texture: GLTexture, name: string = "unknown")
        {
            if (!this._enabled) { return; }

            const idx = this.__textures.indexOf(texture);
            if (idx !== -1)
            {
                if (this._debugMode)
                {
                    Log.debug(`Re-uploaded ${name} (${texture.width} x ${texture.height}, ${toFormattedSize(this.estimateSize(texture))})`);
                }
                return;
            }

            this.__textures.push(texture);
            this.__sources.push(name);

            if (this._debugMode)
            {
                Log.debug(`Uploaded ${name} (${texture.width} x ${texture.height}, ${toFormattedSize(this.estimateSize(texture))})`);
            }
        }

        public untrack(texture: GLTexture)
        {
            if (!this._enabled) { return; }

            const idx = this.__textures.indexOf(texture);
            if (idx === -1) { return; }

            this.__textures.splice(idx, 1);
            this.__sources.splice(idx, 1);
        }

        protected estimateSize(texture: GLTexture)
        {
            const pixels = texture.width * texture.height;
            const bpp = this.bitsPerPixel(texture);
            return pixels * bpp;
        }

        protected bitsPerPixel(texture: GLTexture): number
        {
            const formatSrc = texture.gl;
            switch (texture.format)
            {
                default:
                case formatSrc.RGBA:
                    return 8 * 4;
            }
        }
    }

    export namespace TextureTracker
    {
        export interface UploadData
        {
            source: string;
            size: number;
        }
    }

    export const textureTracker = new TextureTracker();
}