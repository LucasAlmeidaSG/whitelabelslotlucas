/** @internal  */
namespace RS.Rendering.Pixi
{
    interface GLData
    {
        rawShader: PIXI.Shader;
        shader: RS.Rendering.Pixi.Shader;
        vertexBuffer: PIXI.glCore.GLBuffer;
        vertexVersion: number;
        uvBuffer: PIXI.glCore.GLBuffer;
        uvVersion: number;
        colorBuffer: PIXI.glCore.GLBuffer | null;
        colorVersion: number;
        normalBuffer: PIXI.glCore.GLBuffer | null;
        normalVersion: number;
        indexBuffer: PIXI.glCore.GLBuffer;
        indexVersion: number;
        vao: PIXI.glCore.VertexArrayObject;
    }

    const identityMatrix3x3 = RS.Math.Matrix3.toArray(RS.Math.Matrix3.identity);
    const tmpMatrix1 = RS.Math.Matrix4();
    const tmpMatrix2 = RS.Math.Matrix4();
    const tmpMatrix3 = RS.Math.Matrix4();

    const baseVertexShader = `precision highp float;

attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;

uniform mat4 uTransform;
uniform mat3 uTransformUV;
uniform vec4 uColor;

varying vec2 vTextureCoord;
varying vec4 vColor;
varying vec3 vNormal;

void main(void)
{
    gl_Position = uTransform * vec4(aVertexPosition, 1.0);
    vTextureCoord = (uTransformUV * vec3(aTextureCoord, 1.0)).xy;
    vColor = vec4(uColor.rgb, 1.0) * uColor.a;
}`;

    const baseFragmentShader = `precision lowp float;

varying vec2 vTextureCoord;
varying vec4 vColor;

uniform sampler2D uSampler;

void main(void)
{
    gl_FragColor = texture2D(uSampler, vTextureCoord) * vColor;
}`;

    class BaseMeshShaderProgram extends RS.Rendering.Pixi.ShaderProgram
    {
        public constructor()
        {
            super({
                vertexSource: baseVertexShader,
                fragmentSource: baseFragmentShader
            });
        }
    }

    /**
     * Renderer dedicated to drawing meshes.
     * @internal
     */
    export class MeshRenderer extends PIXI.ObjectRenderer
    {
        public readonly renderer: PIXI.WebGLRenderer;

        protected _baseShaderProgram: RS.Rendering.Pixi.ShaderProgram = null;
        protected _baseShader: RS.Rendering.Pixi.Shader = null;

        constructor(renderer: PIXI.WebGLRenderer)
        {
            super(renderer);
        }

        /**
         * Sets up the renderer context and necessary buffers.
         */
        /*public onContextChange()
        {
            
        }*/

        /**
         * Renders the specified mesh.
         */
        public render(mesh: Mesh)
        {
            if (this._baseShader == null)
            {
                this._baseShaderProgram = new BaseMeshShaderProgram();
                this._baseShader = this._baseShaderProgram.createShader() as RS.Rendering.Pixi.Shader;
            }

            const texture = mesh.texture;
            if (texture == null || !texture.valid) { return; }
            const shader = mesh.shader || this._baseShader;
            const buffers = mesh.buffers;

            const meshEx = mesh as (Mesh & { _glDatas?: GLData[]; });
            if (meshEx._glDatas == null) { meshEx._glDatas = []; }

            const renderer = this.renderer;
            const gl = renderer.gl;

            let glData = meshEx._glDatas[renderer.CONTEXT_UID];

            // If there is no cached data present, generate it
            if (!glData)
            {
                renderer.bindVao(null);

                glData = {
                    shader, rawShader: shader.shaderProgram.getRawShaderFor(renderer),

                    vertexBuffer: PIXI.glCore.GLBuffer.createVertexBuffer(gl, buffers.vertices.buffer, gl.STREAM_DRAW),
                    vertexVersion: buffers.vertices.version,

                    uvBuffer: PIXI.glCore.GLBuffer.createVertexBuffer(gl, buffers.uvs.buffer, gl.STREAM_DRAW),
                    uvVersion: buffers.uvs.version,

                    colorBuffer: buffers.colors.buffer ? PIXI.glCore.GLBuffer.createVertexBuffer(gl, buffers.colors.buffer, gl.STREAM_DRAW) : null,
                    colorVersion: buffers.colors.version,

                    normalBuffer: buffers.normals.buffer ? PIXI.glCore.GLBuffer.createVertexBuffer(gl, buffers.normals.buffer, gl.STREAM_DRAW) : null,
                    normalVersion: buffers.normals.version,

                    indexBuffer: PIXI.glCore.GLBuffer.createIndexBuffer(gl, buffers.indices.buffer, gl.STATIC_DRAW),
                    indexVersion: buffers.indices.version,

                    vao: null
                };

                const attrPos = glData.rawShader.attributes["aVertexPosition"];
                const attrUV = glData.rawShader.attributes["aTextureCoord"];

                if (attrPos.size !== mesh.vertexSize)
                {
                    RS.Log.warn(`MeshRenderer: Mesh vertex size (${mesh.vertexSize}) does not match the shader's attribute size (${attrPos.size})!`);
                }

                // build the vao object that will render..
                glData.vao = new PIXI.glCore.VertexArrayObject(gl, null)
                    .addIndex(glData.indexBuffer)
                    .addAttribute(glData.vertexBuffer, attrPos, gl.FLOAT, false, mesh.vertexSize * 4, 0)
                    .addAttribute(glData.uvBuffer, attrUV, gl.FLOAT, false, 2 * 4, 0);
                if (glData.colorBuffer && glData.rawShader.attributes["aColor"])
                {
                    glData.vao.addAttribute(glData.colorBuffer, glData.rawShader.attributes["aColor"], gl.UNSIGNED_BYTE, true, 4, 0);
                }
                if (glData.normalBuffer && glData.rawShader.attributes["aNormal"])
                {
                    glData.vao.addAttribute(glData.colorBuffer, glData.rawShader.attributes["aNormal"], gl.FLOAT, false, 3 * 4, 0);
                }

                meshEx._glDatas[renderer.CONTEXT_UID] = glData;
            }

            // Change for shader change
            if (shader !== glData.shader)
            {
                glData.shader = shader;
                glData.rawShader = shader.shaderProgram.getRawShaderFor(renderer);
            }

            // Check for any buffer changes
            if (buffers.vertices.version > glData.vertexVersion)
            {
                glData.vertexBuffer.upload(buffers.vertices.buffer);
                glData.vertexVersion = buffers.vertices.version;
            }
            if (buffers.uvs.version > glData.uvVersion)
            {
                glData.uvBuffer.upload(buffers.uvs.buffer);
                glData.uvVersion = buffers.uvs.version;
            }
            if (buffers.colors.version > glData.colorVersion)
            {
                if (glData.colorBuffer == null)
                {
                    if (buffers.colors.buffer != null) { glData.colorBuffer = PIXI.glCore.GLBuffer.createVertexBuffer(gl, buffers.colors.buffer, gl.STREAM_DRAW); }
                }
                else
                {
                    if (buffers.colors.buffer == null)
                    {
                        glData.colorBuffer.destroy();
                        glData.colorBuffer = null;
                    }
                    else
                    {
                        glData.colorBuffer.upload(buffers.colors.buffer);
                    }
                }
                
                glData.colorVersion = buffers.colors.version;
            }
            if (buffers.normals.version > glData.normalVersion)
            {
                if (glData.normalBuffer == null)
                {
                    if (buffers.normals.buffer != null) { glData.normalBuffer = PIXI.glCore.GLBuffer.createVertexBuffer(gl, buffers.normals.buffer, gl.STREAM_DRAW); }
                }
                else
                {
                    if (buffers.normals.buffer == null)
                    {
                        glData.normalBuffer.destroy();
                        glData.normalBuffer = null;
                    }
                    else
                    {
                        glData.normalBuffer.upload(buffers.normals.buffer);
                    }
                }
                
                glData.normalVersion = buffers.normals.version;
            }
            if (buffers.indices.version > glData.indexVersion)
            {
                glData.indexBuffer.upload(buffers.indices.buffer);
                glData.indexVersion = buffers.indices.version;
            }

            // Bind shader and VAO
            renderer.bindShader(glData.rawShader);
            renderer.bindVao(glData.vao);

            // Compute transform
            mesh.getLocalToWorldMatrix(tmpMatrix1);
            RS.Math.Matrix4.fromPixiMatrix(renderer._activeRenderTarget.projectionMatrix, tmpMatrix2);
            RS.Math.Matrix4.mul(tmpMatrix2, tmpMatrix1, tmpMatrix3);

            // Setup shader
            glData.rawShader.uniforms["uSampler"] = renderer.bindTexture(texture);
            if ("uTransformUV" in glData.rawShader.uniforms)
            {
                glData.rawShader.uniforms["uTransformUV"] = identityMatrix3x3;
            }
            glData.rawShader.uniforms["uTransform"] = RS.Math.Matrix4.toArray(tmpMatrix3, false);
            glData.rawShader.uniforms["uColor"] = [...PIXI.utils.hex2rgb(mesh.tint), mesh.worldAlpha];
            
            // Setup blend mode
            renderer.state.setBlendMode(PIXI.utils.correctBlendMode(mesh.blendMode, texture.baseTexture.premultipliedAlpha));

            const drawMode = mesh.drawMode === Mesh.DrawMode.TriangleStrip ? gl.TRIANGLE_STRIP : gl.TRIANGLES;

            glData.vao.draw(drawMode, buffers.indices.buffer.length, 0);
        }
    }

    PIXI.WebGLRenderer.registerPlugin("rsmesh", MeshRenderer);
}