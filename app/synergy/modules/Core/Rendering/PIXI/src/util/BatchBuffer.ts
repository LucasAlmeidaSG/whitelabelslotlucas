/** @internal  */
namespace RS.Rendering.Pixi
{
    export class BatchBuffer implements RS.IDisposable
    {
        protected _disposed: boolean = false;

        protected _vertices: ArrayBuffer;
        protected _verticesF32: Float32Array;
        protected _verticesI32: Uint32Array;

        protected _vertexBuffer: PIXI.glCore.GLBuffer;

        protected _vao: PIXI.glCore.VertexArrayObject;

        /** Gets if this buffer has been disposed. */
        public get isDisposed() { return this._disposed; }        

        /** Gets the float32 view for the vertex buffer. */
        @RS.CheckDisposed
        public get verticesF32() { return this._verticesF32; }
        
        /** Gets the int32 view for the vertex buffer. */
        @RS.CheckDisposed
        public get verticesI32() { return this._verticesI32; }

        public constructor(public readonly settings: BatchBuffer.Settings)
        {
            const renderer = settings.renderer;
            const gl = renderer.gl;

            // Allocate vertex arrays
            this._vertices = new ArrayBuffer(settings.quadCount * SpriteRenderer.verticesPerQuad * SpriteRenderer.vertexByteSize);
            this._verticesF32 = new Float32Array(this._vertices);
            this._verticesI32 = new Uint32Array(this._vertices);

            // Allocate vertex buffer
            this._vertexBuffer = PIXI.glCore.GLBuffer.createVertexBuffer(
                renderer.gl,
                this._vertices,
                renderer.gl.STREAM_DRAW
            );

            // Allocate VAO
            // renderer.bindVao(null);
            const attrs = settings.shader.attributes;
            this._vao = renderer.createVao()
                .addIndex(settings.indexBuffer);
            
            // Bind to the shader
            if (attrs["aVertexPosition"] != null) { this._vao.addAttribute(this._vertexBuffer, attrs["aVertexPosition"], gl.FLOAT, false, SpriteRenderer.vertexByteSize, 0); }
            if (attrs["aTextureCoord"] != null) { this._vao.addAttribute(this._vertexBuffer, attrs["aTextureCoord"], gl.UNSIGNED_SHORT, true, SpriteRenderer.vertexByteSize, 2 * 4); }
            if (attrs["aColor"] != null) { this._vao.addAttribute(this._vertexBuffer, attrs["aColor"], gl.UNSIGNED_BYTE, true, SpriteRenderer.vertexByteSize, 3 * 4); }
            if (attrs["aTextureId"] != null) { this._vao.addAttribute(this._vertexBuffer, attrs["aTextureId"], gl.FLOAT, false, SpriteRenderer.vertexByteSize, 4 * 4); }
        }

        /**
         * Uploads changes to the vertex buffer.
         */
        @RS.CheckDisposed
        public upload(): void
        {
            this._vertexBuffer.upload(this._vertices, 0);
        }

        /**
         * Binds this batch buffer ready for drawing.
         */
        @RS.CheckDisposed
        public bind(): void
        {
            this._vertexBuffer.bind();
            this.settings.renderer.bindVao(this._vao);
        }

        /**
         * Dispatches a draw call for the specified range of quads.
         * @param startQuad 
         * @param quadCount 
         */
        @RS.CheckDisposed
        public draw(startQuad: number, quadCount: number): void
        {
            this._vao.draw(this.settings.renderer.gl.TRIANGLES, quadCount * SpriteRenderer.indicesPerQuad, startQuad * SpriteRenderer.indicesPerQuad);
        }

        /**
         * Destroys the buffer.
         */
        public dispose()
        {
            if (this._disposed) { return; }
            this._vao.destroy();
            this._vao = null;
            this._vertexBuffer.destroy();
            this._vertexBuffer = null;
            this._verticesF32 = null;
            this._verticesI32 = null;
            this._vertices = null;
            this._disposed = true;
        }
    }

    export namespace BatchBuffer
    {
        export interface Settings
        {
            renderer: PIXI.WebGLRenderer;
            indexBuffer: PIXI.glCore.GLBuffer;
            shader: PIXI.Shader;
            quadCount: number;
        }
    }
}