/** @internal  */
namespace RS.Rendering.Pixi
{
    export class BlendConvert
    {
        protected static blends: RS.Rendering.BlendMode[] =
        [
            RS.Rendering.BlendMode.Add,
            RS.Rendering.BlendMode.AddNPM,
            RS.Rendering.BlendMode.Multiply,
            RS.Rendering.BlendMode.Normal,
            RS.Rendering.BlendMode.NormalNPM,
            RS.Rendering.BlendMode.Screen
        ];
        protected static pixiBlends: number[] =
        [
            /* PIXI.BLEND_MODES.ADD */ 1,
            /* PIXI.BLEND_MODES.ADD_NPM */ 18,
            /* PIXI.BLEND_MODES.MULTIPLY */ 2,
            /* PIXI.BLEND_MODES.NORMAL */ 0,
            /* PIXI.BLEND_MODES.NORMAL_NPM */ 17,
            /* PIXI.BLEND_MODES.SCREEN */ 3
        ];

        public static blendToPixi(blend: RS.Rendering.BlendMode): number
        {
            if (blend === RS.Rendering.BlendMode.Add && Device.isIOS14)
            {
                blend = RS.Rendering.BlendMode.Screen;
            }
            const index = this.blends.indexOf(blend);
            return this.pixiBlends[index];
        }

        public static pixiToBlend(blend: number): RS.Rendering.BlendMode
        {
            const index = this.pixiBlends.indexOf(blend);
            return this.blends[index];
        }

        public static withNPM(blend: RS.Rendering.BlendMode): RS.Rendering.BlendMode
        {
            switch (blend)
            {
                case RS.Rendering.BlendMode.Add: return RS.Rendering.BlendMode.AddNPM;
                case RS.Rendering.BlendMode.Normal: return RS.Rendering.BlendMode.NormalNPM;
                default: return blend;
            }
        }

        public static withoutNPM(blend: RS.Rendering.BlendMode): RS.Rendering.BlendMode
        {
            switch (blend)
            {
                case RS.Rendering.BlendMode.AddNPM: return RS.Rendering.BlendMode.Add;
                case RS.Rendering.BlendMode.NormalNPM: return RS.Rendering.BlendMode.Normal;
                default: return blend;
            }
        }
    }
}