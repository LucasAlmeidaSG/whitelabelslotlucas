namespace RS.Rendering.Pixi.World
{
    interface IHasAlpha
    {
        alpha: number;
        parent?: IHasAlpha;
    }

    interface IHasVisible
    {
        visible: boolean;
        parent?: IHasVisible;
    }

    export function alpha(object: IHasAlpha): number
    {
        if (object.alpha === 0) { return 0; }
        if (!object.parent) { return object.alpha; }
        return object.alpha * alpha(object.parent);
    }

    export function visible(object: IHasVisible): boolean
    {
        if (!object.visible) { return false; }
        if (!object.parent) { return true; }
        return visible(object.parent);
    }

    /** Returns whether or not the given object is at all apparent on stage i.e. visible with non-zero alpha, accounting for parents. */
    export function apparent(object: IHasVisible & IHasAlpha): boolean
    {
        if (object.alpha === 0) { return false; }
        if (object.visible === false) { return false; }
        if (!object.parent) { return true; }
        return apparent(object.parent);
    }
}