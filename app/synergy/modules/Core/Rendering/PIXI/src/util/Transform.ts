/** @internal  */
namespace RS.Rendering.Pixi
{
    export namespace Transform
    {
        interface WithAnchor
        {
            anchorX: number;
            anchorY: number;
        }
        function hasAnchor(target: any): target is WithAnchor
        {
            return RS.Is.object(target) && RS.Is.number(target.anchorX) && RS.Is.number(target.anchorY);
        }

        /**
         * Applies the specified transform to a display object.
         * @param transform
         * @param target 
         */
        export function apply(transform: Partial<Transform>, target: IDisplayObject): void
        {
            if (transform.position != null)
            {
                target.x = transform.position.x;
                target.y = transform.position.y;
            }
            if (transform.scale != null)
            {
                target.scaleX = transform.scale.x;
                target.scaleY = transform.scale.y;
            }
            if (transform.pivot != null) { target.pivot.set(transform.pivot.x, transform.pivot.y); }
            if (transform.anchor != null && hasAnchor(target))
            {
                target.anchorX = transform.anchor.x;
                target.anchorY = transform.anchor.y;
            }
            if (transform.skew != null) { target.skew.set(transform.skew.x, transform.skew.y); }
            if (transform.rotation != null) { target.rotation = transform.rotation; }
        }

        Rendering.Transform.apply = apply;
    }
}