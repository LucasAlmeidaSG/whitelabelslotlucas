/** @internal  */
namespace RS.Rendering.Pixi
{
    import hex2rgb = PIXI.utils.hex2rgb;

    const tmpSize1 = Math.Size2D(), tmpSize2 = Math.Size2D();
    const tmpRect = new PIXI.Rectangle();

    /**
     * Renders the graphics object.
     */
    export class GraphicsRenderer extends PIXI.GraphicsRenderer
    {
        /**
         * Renders a graphics object.
         * @param graphics the graphics object to render
         */
        public render(graphics: RSGraphics): void
        {
            const renderer = this.renderer;
            const gl = renderer.gl;
            const rt = renderer._activeRenderTarget;

            let webGLData: PIXI.WebGLGraphicsData;
            let webGL = graphics["_webGL"][this.CONTEXT_UID];

            if (!webGL || graphics.dirty !== webGL.dirty)
            {
                this.updateGraphics(graphics);

                webGL = graphics["_webGL"][this.CONTEXT_UID];
            }

            const n = webGL.data.length;
            if (n === 0) { return; }

            const lb = graphics.getBounds(true, tmpRect);

            let glShader: PIXI.glCore.GLShader = null;
            const shader = graphics.shader;
            if (shader)
            {
                const pixiShader = graphics.shader.shaderProgram.getRawShaderFor(this.renderer);
                tmpSize1.w = rt.size.width;
                tmpSize1.h = rt.size.height;
                tmpSize2.w = lb.width;
                tmpSize2.h = lb.height;
                renderer.bindShader(pixiShader);
                shader.syncUniforms(pixiShader, tmpSize1, tmpSize2, !rt.root);
                const uniforms = graphics.shader.uniforms;
                let loc = 0;
                const touch = this.renderer.textureGC.count;
                for (const uniformName in uniforms)
                {
                    const uniform: RS.Rendering.ShaderUniform = uniforms[uniformName];
                    if (uniform.type === RS.Rendering.ShaderUniform.Type.Sampler2D && uniform.value != null)
                    {
                        const texture = Is.assetReference(uniform.value) ? Asset.Store.getAsset(uniform.value).asset : uniform.value;
                        if (texture)
                        {
                            const baseTexture = (texture as Texture).baseTexture;
                            if (baseTexture)
                            {
                                renderer.bindTexture(baseTexture, loc);
                                if (renderer.boundTextures[loc]) { renderer.boundTextures[loc]["_virtualBoundId"] = -1; }
                                renderer.boundTextures[loc] = baseTexture;
                                pixiShader.uniforms[uniformName] = loc;
                                baseTexture["touched"] = touch;
                                baseTexture["_virtualBoundId"] = loc;
                                ++loc;
                            }
                        }
                    }
                }
                glShader = pixiShader;
            }

            renderer.state.setBlendMode(graphics.blendMode);

            for (let i = 0; i < n; i++)
            {
                webGLData = webGL.data[i];
                const shaderTemp = glShader || webGLData.shader;

                renderer.bindShader(shaderTemp);
                shaderTemp.uniforms["translationMatrix"] = graphics.transform.worldTransform.toArray(true);
                shaderTemp.uniforms["tint"] = hex2rgb(graphics.tint);
                shaderTemp.uniforms["alpha"] = graphics.worldAlpha;
                shaderTemp.uniforms["origin"] = [ lb.x, lb.y ];

                renderer.bindVao(webGLData.vao);

                if (graphics.nativeLines)
                {
                    gl.drawArrays(gl.LINES, 0, webGLData.points.length / 6);
                }
                else
                {
                    webGLData.vao.draw(gl.TRIANGLE_STRIP, webGLData.indices.length, 0);
                }
            }
        }
    }

    PIXI.WebGLRenderer.registerPlugin("graphics", GraphicsRenderer);
}