/** @internal  */
namespace RS.Rendering.Pixi
{
    export type PixiShape = PIXI.Polygon | PIXI.RoundedRectangle | PIXI.Rectangle | PIXI.Ellipse | PIXI.Circle;

    export function toPixiPoint(point: RS.Rendering.Point2D): PIXI.Point
    {
        return new PIXI.Point(point.x, point.y);
    }

    export function fromPixiPoint(point: PIXI.Point, out = new Point2D()): RS.Rendering.Point2D
    {
        return out.set(point.x, point.y);
    }

    export function toPixiRect(rect: RS.Rendering.ReadonlyRectangle): PIXI.Rectangle
    {
        return new PIXI.Rectangle(rect.x, rect.y, rect.w, rect.h);
    }

    export function fromPixiRect(rect: PIXI.Rectangle, out = new Rectangle()): RS.Rendering.Rectangle
    {
        return out.set(rect.x, rect.y, rect.width, rect.height);
    }

    export function toPixiCircle(circle: RS.Rendering.ReadonlyCircle): PIXI.Circle
    {
        return new PIXI.Circle(circle.x, circle.y, circle.r);
    }

    export function fromPixiCircle(circle: PIXI.Circle, out = new Circle()): RS.Rendering.Circle
    {
        return out.set(circle.x, circle.y, circle.radius);
    }

    export function toPixiEllipse(ellipse: RS.Rendering.ReadonlyEllipse): PIXI.Ellipse
    {
        return new PIXI.Ellipse(ellipse.x, ellipse.y, ellipse.w / 2, ellipse.h / 2);
    }

    export function fromPixiEllipse(ellipse: PIXI.Ellipse, out = new Ellipse()): RS.Rendering.Ellipse
    {
        return out.set(ellipse.x, ellipse.y, ellipse.width * 2, ellipse.height * 2);
    }

    export function toPixiRoundedRect(roundedRect: RS.Rendering.ReadonlyRoundedRectangle): PIXI.RoundedRectangle
    {
        return new PIXI.RoundedRectangle(roundedRect.x, roundedRect.y, roundedRect.w, roundedRect.h, roundedRect.r);
    }

    export function fromPixiRoundedRect(roundedRect: PIXI.RoundedRectangle, out = new RoundedRectangle()): RS.Rendering.RoundedRectangle
    {
        return out.set(roundedRect.x, roundedRect.y, roundedRect.width, roundedRect.height, roundedRect.radius);
    }

    export function toPixiPolygon(polygon: RS.Rendering.ReadonlyPolygon): PIXI.Polygon
    {
        const points: PIXI.Point[] = [];
        for (const point of polygon.points)
        {
            points.push(new PIXI.Point(point.x, point.y));
        }
        return new PIXI.Polygon(points);
    }

    export function fromPixiPolygon(polygon: PIXI.Polygon, out = new Polygon()): RS.Rendering.Polygon
    {
        const points: RS.Rendering.Point2D[] = [];
        for (let p = 0; p < polygon.points.length; p += 2)
        {
            points.push(new RS.Rendering.Point2D(polygon.points[p], polygon.points[p + 1]));
        }
        return out.set(points);
    }

    export function toPixiShape(shape: Shape | Point2D)
    {
        if (shape instanceof Rectangle)
        {
            return toPixiRect(shape);
        }
        else if (shape instanceof Circle)
        {
            return toPixiCircle(shape);
        }
        else if (shape instanceof Ellipse)
        {
            return toPixiEllipse(shape);
        }
        else if (shape instanceof RoundedRectangle)
        {
            return toPixiRoundedRect(shape);
        }
        else if (shape instanceof Polygon)
        {
            return toPixiPolygon(shape);
        }
        else if (shape instanceof Point2D)
        {
            return toPixiPoint(shape);
        }
    }

    export function fromPixiShape(shape: PixiShape | PIXI.Point, out?: Shape | Point2D)
    {
        if (shape instanceof PIXI.Rectangle)
        {
            return out instanceof Rectangle ? fromPixiRect(shape, out) : fromPixiShape(shape);
        }
        else if (shape instanceof PIXI.Circle)
        {
            return out instanceof Circle ? fromPixiCircle(shape, out) : fromPixiCircle(shape);
        }
        else if (shape instanceof PIXI.Ellipse)
        {
            return out instanceof Ellipse ? fromPixiEllipse(shape, out) : fromPixiEllipse(shape);
        }
        else if (shape instanceof PIXI.RoundedRectangle)
        {
            return out instanceof RoundedRectangle ? fromPixiRoundedRect(shape, out) : fromPixiRoundedRect(shape);
        }
        else if (shape instanceof PIXI.Polygon)
        {
            return out instanceof Polygon ? fromPixiPolygon(shape, out) : fromPixiPolygon(shape);
        }
        else if (shape instanceof PIXI.Point)
        {
            return out instanceof Point2D ? fromPixiPoint(shape, out) : fromPixiPoint(shape);
        }
    }
}