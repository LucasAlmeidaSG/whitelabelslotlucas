/** @internal  */
namespace RS.Rendering.Pixi
{
    const tmpMatrix1 = RS.Math.Matrix4();
    const tmpMatrix2 = RS.Math.Matrix4();
    const tmpVector1 = RS.Math.Vector3D();
    const tmpVector2 = RS.Math.Vector3D();

    /**
     * Extended mesh.
     */
    @RS.HasCallbacks
    export class Mesh extends PIXI.DisplayObject implements RS.IDisposable
    {
        protected _vertexSize: number = 2;
        protected _wireframe: boolean = false;
        protected _disposed: boolean = false;
        protected _drawMode: Mesh.DrawMode;
        protected _blendMode: number;        
        protected _shader: RS.Rendering.Pixi.Shader;
        protected _tint: number = 0xFFFFFF;
        protected _uvTransform: PIXI.TextureMatrix | null = null;
        protected _transform: RS.Math.Matrix4 = null;

        protected _texture: PIXI.Texture | null = null;

        protected _vertices: Float32Array | null = null;
        protected _uvs: Float32Array | null = null;
        protected _muvs: Float32Array | null = null;
        protected _colors: Uint8Array | null = null;
        protected _normals: Float32Array | null = null;
        protected _indices: Uint16Array | null = null;
        protected _wireframeIndices: Uint16Array | null = null;

        protected _buffers: Mesh.VersionedVertexArrays;

        /** Gets if this mesh has been disposed. */
        public get isDisposed() { return this._disposed; }

        /** Gets the number of floats per vertex that represent vertex position. */
        public get vertexSize() { return this._vertexSize; }

        /** Gets or sets if this mesh should render in wireframe mode. */
        public get wireframe() { return this._wireframe; }
        public set wireframe(value)
        {
            if (value === this._wireframe) { return; }
            this._wireframe = value;
            if (this._wireframe)
            {
                this._wireframeIndices = Mesh.generateWireframeIndices(this._indices);
            }
        }

        /** Gets or sets the texture to display for this mesh. */
        public get texture() { return this._texture; }
        public set texture(value)
        {
            if (this._texture === value) { return; }
            if (this._texture != null) { this._texture.off("update", this.handleTextureUpdate); }
            this._uvTransform = null;
            this._texture = value;
            if (this._texture != null)
            {
                if (this._texture.baseTexture.hasLoaded)
                {
                    this.handleTextureUpdate();
                }
                else
                {
                    this._texture.once("update", this.handleTextureUpdate);
                }
            }
        }

        /**
         * Gets or sets the vertices buffer.
         * This buffer is mandatory, and may be 2-4 elements per vertex. (XYZW)
         * The number of elements per vertex MUST match the size of the position attribute in the shader.
         */
        public get vertices() { return this._vertices; }
        public set vertices(value)
        {
            if (value === this._vertices) { return; }
            this._vertices = this._buffers.vertices.buffer = value;
            this._buffers.vertices.version++;
        }

        /**
         * Gets or sets the texture coordinates buffer.
         * This buffer is mandatory, and must be 2 elements per vertex. (UV)
         */
        public get uvs() { return this._uvs; }
        public set uvs(value)
        {
            if (value === this._uvs) { return; }
            this._uvs = value;
            this.markUVsDirty();
        }

        /**
         * Gets or sets the colors buffer.
         * This buffer is optional, and must be 4 elements per vertex if present. (RGBA)
         */
        public get colors() { return this._uvs; }
        public set colors(value)
        {
            if (value === this._colors) { return; }
            this._colors = this._buffers.colors.buffer = value;
            this._buffers.colors.version++;
        }

        /**
         * Gets or sets the normals buffer.
         * This buffer is optional, and must be 3 elements per vertex if present. (XYZ)
         */
        public get normals() { return this._normals; }
        public set normals(value)
        {
            if (value === this._normals) { return; }
            this._normals = this._buffers.normals.buffer = value;
            this._buffers.normals.version++;
        }

        /**
         * Gets or sets the indices buffer.
         * This buffer is mandatory, and must be 3 indices per polygon if draw mode is triangles, or 2 + 1 indices per polygon if draw mode is triangle strip.
         */
        public get indices() { return this._indices; }
        public set indices(value)
        {
            if (value === this._indices) { return; }
            this._indices = value;
            if (this._wireframe)
            {
                this._wireframeIndices = Mesh.generateWireframeIndices(this._indices);
                this._buffers.indices.buffer = this._wireframeIndices;
            }
            else
            {
                this._buffers.indices.buffer = this._indices;
            }
            this._buffers.indices.version++;
        }

        /** Gets the buffers to draw with. */
        public get buffers() { return this._buffers; }

        /** Gets or sets the shader to render this mesh with. */
        public get shader() { return this._shader; }
        public set shader(value) { this._shader = value; }

        /** Gets or sets the draw mode of the mesh. */
        public get drawMode() { return this._drawMode; }
        public set drawMode(value) { this._drawMode = value; }

        /** Gets or sets the 3D transform for this mesh. Applied on top of the standard PIXI transform. */
        public get meshTransform(): Readonly<RS.Math.Matrix4> { return this._transform; }
        public set meshTransform(value) { this._transform = value; }

        /** Gets or sets the blend mode of this mesh. */
        public get blendMode() { return this._blendMode; }
        public set blendMode(value) { this._blendMode = value; }

        /** Gets or sets the tint of this mesh. */
        public get tint() { return this._tint; }
        public set tint(value) { this._tint = value; }

        /** Gets a bounding box for this mesh in local space (before transform is applied). */
        public get localBoundingBox(): RS.Math.AABB3D
        {
            const bbox = RS.Math.AABB3D();
            const vtxCnt = this._vertices.length / this._vertexSize;
            for (let i = 0; i < vtxCnt; ++i)
            {
                this.readVertex(i, tmpVector2);
                if (i === 0)
                {
                    RS.Math.AABB3D.set(bbox, tmpVector2, tmpVector2);
                }
                else
                {
                    RS.Math.AABB3D.expand(bbox, tmpVector2);
                }
            }
            return bbox;
        }

        /** Gets a bounding box for this mesh in local space (after transform is applied). */
        public get worldBoundingBox(): RS.Math.AABB3D
        {
            const mtx = this.getLocalToWorldMatrix(tmpMatrix2);
            const bbox = RS.Math.AABB3D();
            const vtxCnt = this._vertices.length / this._vertexSize;
            for (let i = 0; i < vtxCnt; ++i)
            {
                this.readVertex(i, tmpVector1);
                RS.Math.Matrix4.transform(mtx, tmpVector1, tmpVector2);
                if (i === 0)
                {
                    RS.Math.AABB3D.set(bbox, tmpVector2, tmpVector2);
                }
                else
                {
                    RS.Math.AABB3D.expand(bbox, tmpVector2);
                }
            }
            return bbox;
        }

        constructor(texture?: PIXI.Texture, vertices?: Float32Array, uvs?: Float32Array, indices?: Uint16Array, drawMode?: Mesh.DrawMode, wireframe?: boolean)
        {
            super();

            this._blendMode = PIXI.BLEND_MODES.NORMAL;

            this._buffers =
            {
                vertices: { buffer: null, version: 0 },
                uvs: { buffer: null, version: 0 },
                colors: { buffer: null, version: 0 },
                normals: { buffer: null, version: 0 },
                indices: { buffer: null, version: 0 }
            };

            this.vertices = vertices || new Float32Array([
                0, 0, 0,
                100, 0, 0,
                100, 100, 0,
                0, 100, 0
            ]);
            this.uvs = uvs || new Float32Array([
                0, 0,
                1, 0,
                1, 1,
                0, 1
            ]);
            this.indices = indices || new Uint16Array([0, 1, 2, 2, 3, 0]);
            this.drawMode = drawMode != null ? drawMode : Mesh.DrawMode.Triangles;
            this.wireframe = wireframe || false;
            this._transform = RS.Math.Matrix4.clone(RS.Math.Matrix4.identity);

            this._vertexSize = Math.round(2 * this._vertices.length / this._uvs.length);

            this.texture = texture;
        }

        /** Generates indices for a wireframe mesh. */
        protected static generateWireframeIndices(indices: Uint16Array): Uint16Array
        {
            const result = new Uint16Array(indices.length * 2);
            let out = 0;

            for (let index = 0; index < indices.length; index += 3)
            {
                result[out++] = indices[index];
                result[out++] = indices[index + 1];
                result[out++] = indices[index + 1];
                result[out++] = indices[index + 2];
                result[out++] = indices[index + 2];
                result[out++] = indices[index];
            }

            if (out !== result.length)
            {
                RS.Log.error("generateWireframeIndices: not a valid triangle list.");
            }

            return result;
        }

        /** Mark vertices as dirty. Call this if the contents of the vertices buffer changes. */
        public markVerticesDirty() { this._buffers.vertices.version++; }

        /** Mark uvs as dirty. Call this if the contents of the uvs buffer changes. */
        public markUVsDirty()
        {
            if (!this._uvTransform) { return; }
            this._muvs = this._uvs.slice();
            this._uvTransform.multiplyUvs(this._muvs);
            this._buffers.uvs.buffer = this._muvs;
            this._buffers.uvs.version++;
        }

        /** Mark uvs as dirty. Call this if the contents of the uvs buffer changes. */
        public markColorsDirty() { this._buffers.colors.version++; }

        /** Mark normals as dirty. Call this if the contents of the normals buffer changes. */
        public markNormalsDirty() { this._buffers.normals.version++; }

        /** Mark indices as dirty. Call this if the contents of the indices buffer changes. */
        public markIndicesDirty() { this._buffers.indices.version++; }

        /**
         * Gets a matrix that transforms from this meshes local coordinate space into world space.
         * Considers both the standard PIXI transform and meshTransform.
         */
        public getLocalToWorldMatrix(out?: RS.Math.Matrix4): RS.Math.Matrix4
        {
            out = out || RS.Math.Matrix4();
            RS.Math.Matrix4.fromPixiMatrix(this.worldTransform, tmpMatrix1);
            RS.Math.Matrix4.mul(tmpMatrix1, this.meshTransform, out);
            return out;
        }

        /**
         * Updates the transform on all children of this container for rendering
         */
        public updateTransform()
        {
            this._boundsID++;

            this.transform.updateTransform(this.parent.transform);

            // TODO: check render flags, how to process stuff here
            this.worldAlpha = this.alpha * this.parent.worldAlpha;
        }

        /**
         * Renders the object using the WebGL renderer
         * @param renderer a reference to the WebGL renderer
         */
        public renderWebGL(renderer: PIXI.WebGLRenderer)
        {
            const plugin: MeshRenderer = renderer.plugins["rsmesh"];
            renderer.setObjectRenderer(plugin);
            plugin.render(this);
        }

        /**
         * Renders the object using the Canvas renderer
         * @param renderer a reference to the Canvas renderer
         */
        public renderCanvas(renderer: PIXI.CanvasRenderer)
        {
            // Not supported
        }

        /** Disposes this mesh. */
        public dispose()
        {
            if (this._disposed) { return; }
            this._vertices = null;
            this._indices = null;
            this._texture = null;
            this._uvs = null;
            this._normals = null;
            this._colors = null;
            this._wireframeIndices = null;
            this._uvTransform = null;
            RS.Math.Matrix4.free(this._transform);
            this._transform = null;
            this._disposed = true;
        }

        /**
         * Calculates the bounds of the mesh as a rectangle.
         * The bounds calculation takes the worldTransform into account.
         */
        protected calculateBounds()
        {
            this._bounds.clear();
            const bbox = this.worldBoundingBox;
            const pt = new PIXI.Point();
            pt.set(bbox.minX, bbox.minY);
            this._bounds.addPoint(pt);
            pt.set(bbox.maxX, bbox.maxY);
            this._bounds.addPoint(pt);
        }

        /** Called when the texture has updated. */
        @RS.Callback
        protected handleTextureUpdate(): void
        {
            this._uvTransform = new PIXI.TextureMatrix(this._texture);
            this._uvTransform.update();
            this.markUVsDirty();
        }

        /** Reads the vertex at the specified index into a vector. */
        protected readVertex(idx: number, out: RS.Math.Vector3D): void
        {
            RS.Math.Vector3D.set(out, 0, 0, 0);
            idx *= this._vertexSize;
            if (this._vertexSize >= 1) { out.x = this._vertices[idx++]; }
            if (this._vertexSize >= 2) { out.y = this._vertices[idx++]; }
            if (this._vertexSize >= 3) { out.z = this._vertices[idx++]; }
        }
    }

    export namespace Mesh
    {
        /**
         * Specifies how polygons are assembled from a mesh by the GPU.
         */
        export enum DrawMode
        {
            /** Triangle strip - e.g. [0, 1, 2] [1, 2, 3] [2, 3, 4]  */
            TriangleStrip = PIXI.mesh.Mesh.DRAW_MODES.TRIANGLE_MESH,

            /** Triangle list - e.g. [0, 1, 2] [3, 4, 5] [6, 7, 8]  */
            Triangles = PIXI.mesh.Mesh.DRAW_MODES.TRIANGLES
        }

        /**
         * Contains a buffer and a "version" for it.
         */
        export interface Versioned<T extends Float32Array|Uint16Array|Uint8Array>
        {
            buffer: T;
            version: number;
        }

        /**
         * Contains "versions" for each vertex buffer type.
         */
        export interface VersionedVertexArrays
        {
            vertices: Versioned<Float32Array>;
            uvs: Versioned<Float32Array>;
            colors: Versioned<Uint8Array>;
            normals: Versioned<Float32Array>;
            indices: Versioned<Uint16Array>;
        }
    }
}
