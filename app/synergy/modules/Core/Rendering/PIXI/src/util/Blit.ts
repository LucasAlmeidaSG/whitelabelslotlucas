/** @internal  */
namespace RS.Rendering.Pixi
{
    interface BlitData
    {
        quad: PIXI.Quad;
        inited: boolean;
    }

    const blitData: BlitData[] = [];
    let blitShader: RS.Rendering.Pixi.Shader | null = null;
    const tmpDims1: RS.Dimensions = { w: 0, h: 0 };
    const tmpDims2: RS.Dimensions = { w: 0, h: 0 };

    /**
     * Blits from one RT to another, optionally through a shader.
     * @param renderer 
     * @param srcRT 
     * @param dstRT 
     * @param shader 
     */
    export function blit(
        renderer: PIXI.WebGLRenderer,
        srcRT: PIXI.RenderTarget, srcRect: PIXI.Rectangle,
        dstRT: PIXI.RenderTarget, dstRect: PIXI.Rectangle,
        clear?: boolean, shader?: RS.Rendering.Pixi.Shader
    ): void
    {
        renderer.flush();

        if (blitShader == null)
        {
            const blitBaseShader = new RS.Rendering.ShaderProgram({
                vertexSource: RS.Rendering.Assets.Shaders.Blit.Vertex,
                fragmentSource: RS.Rendering.Assets.Shaders.Blit.Fragment
            });
            blitShader = blitBaseShader.createShader() as RS.Rendering.Pixi.Shader;
        }

        shader = shader || blitShader;

        const gl = renderer.gl;

        const glShader = shader.shaderProgram.getRawShaderFor(renderer);
        const bData = getBlitData(renderer);

        //if (!blit.inited)
        {
            renderer.bindVao(null);
            bData.quad.initVao(glShader);
            bData.inited = true;
        }
        renderer.bindVao(bData.quad.vao);

        renderer.bindRenderTarget(dstRT);
        if (clear)
        {
            gl.disable(gl.SCISSOR_TEST);
            renderer.clear();// [1, 1, 1, 1]);
            gl.enable(gl.SCISSOR_TEST);
        }

        tmpDims1.w = dstRT.size.width;
        tmpDims1.h = dstRT.size.height;
        tmpDims2.w = srcRT.size.width;
        tmpDims2.h = srcRT.size.height;

        renderer.bindShader(glShader);
        shader.syncUniforms(glShader, tmpDims1, tmpDims2, !dstRT.root);
        const srcFrameArr: Float32Array = glShader.uniforms["uSrcFrame"];
        srcFrameArr[0] = srcRect.x / srcRT.size.width;
        srcFrameArr[1] = srcRect.y / srcRT.size.height;
        srcFrameArr[2] = srcRect.width / srcRT.size.width;
        srcFrameArr[3] = srcRect.height / srcRT.size.height;
        glShader.uniforms["uSrcFrame"] = srcFrameArr;
        const dstFrameArr: Float32Array = glShader.uniforms["uDstFrame"];
        dstFrameArr[0] = dstRect.x / dstRT.size.width - 1.0;
        dstFrameArr[1] = dstRect.y / dstRT.size.height - 1.0;
        dstFrameArr[2] = (dstRect.width / dstRT.size.width) * 2.0;
        dstFrameArr[3] = (dstRect.height / dstRT.size.height) * 2.0;
        glShader.uniforms["uDstFrame"] = dstFrameArr;

        const tex = renderer.boundTextures[0] = renderer["emptyTextures"][0];

        renderer.state.setBlendMode(PIXI.BLEND_MODES.NORMAL);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, srcRT.texture["texture"]);
        
        bData.quad.vao.draw(gl.TRIANGLES, 6, 0);

        gl.bindTexture(gl.TEXTURE_2D, tex["_glTextures"][renderer.CONTEXT_UID].texture);
    }

    function getBlitData(renderer: PIXI.WebGLRenderer): BlitData
    {
        return blitData[renderer.CONTEXT_UID] || (blitData[renderer.CONTEXT_UID] = initBlitData(renderer));
    }

    function initBlitData(renderer: PIXI.WebGLRenderer): BlitData
    {
        const quad = new PIXI.Quad(renderer.gl);
        quad.vertices[0] = 0; quad.vertices[1] = 0;
        quad.vertices[2] = 1; quad.vertices[3] = 0;
        quad.vertices[4] = 1; quad.vertices[5] = 1;
        quad.vertices[6] = 0; quad.vertices[7] = 1;
        quad.upload();
        return { quad, inited: false };
    }
}