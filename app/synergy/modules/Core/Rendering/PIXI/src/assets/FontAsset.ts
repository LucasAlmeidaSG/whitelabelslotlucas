/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a font asset type.
     */
    @Asset.Type("font")
    export class FontAsset extends Asset.Base<opentype.Font> implements IFontAsset<opentype.Font>
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): FontAsset
        {
            return new FontAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            return await this.loadOpenTypeFont(url);
        }

        protected loadOpenTypeFont(url: string): PromiseLike<opentype.Font>
        {
            return new Promise((resolve, reject) =>
            {
                opentype.load(Asset.urlWithVersion(url), (err, font) =>
                {
                    if (err != null)
                    {
                        reject(err);
                    }
                    else
                    {
                        const forceOS2 = RS.as(this._def.raw["forceOS2"], RS.Is.boolean);
                        if (forceOS2)
                        {
                            const os2 = font.tables["os2"];
                            if (os2)
                            {
                                font.ascender = os2["sTypoAscender"];
                                font.descender = os2["sTypoDescender"];
                            }
                            else
                            {
                                Log.warn("FontAsset: forceOS2 true but no OS2 table found");
                            }
                        }

                        resolve(font);
                    }
                }, { lowMemory: true });
            });
        }
    }
    Asset.FontAsset = FontAsset;
}