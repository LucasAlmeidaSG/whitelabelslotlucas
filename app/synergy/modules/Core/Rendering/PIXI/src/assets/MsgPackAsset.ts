/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a MsgPack asset type.
     */
    @Asset.Type("msgpack")
    export class MsgPackAsset extends Asset.Base<object> implements IMsgPackAsset<object>
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): MsgPackAsset
        {
            return new MsgPackAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            const file = await Request.ajax({ url }, Request.AJAXResponseType.Binary);
            const msgpack = "msgpackmsgpack";
            const obj = window[msgpack].decode(file);
            return obj;
        }
    }
    Asset.MsgPackAsset = MsgPackAsset;
}