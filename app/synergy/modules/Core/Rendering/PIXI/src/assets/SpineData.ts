/** @internal */
namespace RS.Rendering.Pixi
{
    export class SpineData
    {
        public json: object;
        public atlas: string;
        public images: ITexture[] = [];

        public warmup(stage: IStage)
        {
            // Warm all frames in sheet. TODO: animation support.
            for (const image of this.images)
            {
                image.warmup(stage);
            }
        }
    }
}