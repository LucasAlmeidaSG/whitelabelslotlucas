/// <reference path="../compression/CompressedImage.ts" />

/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates an image asset type.
     */
    @Asset.Type("spine")
    export class SpineAsset extends Asset.Base<Rendering.Pixi.SpineData> implements ISpineAsset
    {
        protected _resolution: number;
        protected _format: string;
        protected _compressedImg: Compression.CompressedImage;

        /**
         * Gets or sets the resolution of this spritesheet asset.
         */
        public get resolution() { return this._resolution; }
        public set resolution(value) { this._resolution = value; }

        public constructor(definition: Asset.Definition)
        {
            super(definition);
            this._resolution = definition.raw["res"] || 1.0;
        }

        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            const caps = Capabilities.WebGL.get();
            // Check S3TC
            if (options.indexOf("crn") !== -1 && caps.supportsS3TC) { return "crn"; }
            // Check PVRTC
            if (options.indexOf("pvr") !== -1 && caps.supportsPVRTC) { return "pvr"; }
            // Check PNG
            if (options.indexOf("png") !== -1) { return "png"; }

            // Unsupported
            return null;
        }

        public clone(): SpineAsset
        {
            return new SpineAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            // Request the spritesheet file
            const jsonUrl = Path.replaceExtension(url, ".json");
            const atlasData = await Asset.ajax({ url }, Request.AJAXResponseType.Text);
            const jsonData = await Asset.ajax({ url: jsonUrl }, Request.AJAXResponseType.JSON);
            const formats: string[] = ["png", "jpg", "jpeg", "tga", "bmp", "svg"];
            const lines: string[] = atlasData.split(/(?:\r\n|\r|\n)/g);
            const asset: Rendering.Pixi.SpineData = new Rendering.Pixi.SpineData();

            const imagePaths: string[] = [];

            for (let i: number = 0; i < lines.length; i++)
            {
                const parts = lines[i].split(".");
                if (parts.length > 1)
                {
                    if (formats.indexOf(parts[parts.length - 1]) != -1)
                    {
                        const imagePath: string = lines[i];
                        const imageURL = `${Path.combine(Path.directoryName(url), Path.replaceExtension(imagePath, ""))}.${format}`;
                        imagePaths.push(imageURL);
                    }
                }
            }

            for (let i: number = 0; i < imagePaths.length; i++)
            {
                switch (format)
                {
                    case "png":
                        {
                            asset.images.push(new Rendering.Pixi.Texture(new PIXI.BaseTexture(
                                await Asset.tag({ url: imagePaths[i], tagName: "img" }),
                                PIXI.SCALE_MODES.LINEAR,
                                this._resolution
                            )));
                            break;
                        }
                    case "crn":
                        {
                            const buffer = await Asset.ajax({ url: imagePaths[i], }, Request.AJAXResponseType.Binary);

                            this._compressedImg = new Compression.CompressedImage(imagePaths[i]);
                            this._compressedImg.loadFromArrayBuffer(buffer, true);
                            // this._compressedImgs[i].levels = 1;
                            const baseTex = new PIXI.BaseTexture(
                                this._compressedImg as any,
                                PIXI.SCALE_MODES.LINEAR,
                                this._resolution
                            );
                            baseTex.premultipliedAlpha = false;
                            asset.images.push(new Rendering.Pixi.Texture(baseTex));
                            break;
                        }
                    case "pvr":
                        {
                            const buffer = await Asset.ajax({ url: imagePaths[i], }, Request.AJAXResponseType.Binary);
                            this._compressedImg = new Compression.CompressedImage(imagePaths[i]);
                            this._compressedImg.loadFromArrayBuffer(buffer, false);
                            // this._compressedImgs[i].levels = 1;
                            asset.images.push(new Rendering.Pixi.Texture(new PIXI.BaseTexture(
                                this._compressedImg as any,
                                PIXI.SCALE_MODES.LINEAR,
                                this._resolution
                            )));
                            break;
                        }
                    default:
                        throw new Error(`Unsupported format '${this._format}'`);
                }
            }

            asset.json = jsonData;
            asset.atlas = atlasData;
            // Create the spritesheet
            return asset;//new Rendering.SpriteSheet(spriteSheetData, this._resolution);
        }
    }
    Asset.SpineAsset = SpineAsset;
}