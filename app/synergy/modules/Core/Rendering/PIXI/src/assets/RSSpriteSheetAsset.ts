/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates an image asset type.
     */
    @Asset.Type("rsspritesheet")
    export class RSSpriteSheetAsset extends Asset.Base<Rendering.ISpriteSheet> implements ISpriteSheetAsset<Rendering.ISpriteSheet>
    {
        protected _resolution: number;
        protected _format: string;
        protected _mipmap: boolean;
        protected _compressedImgs: Rendering.ICompressedImage[];

        /**
         * Gets or sets the resolution of this spritesheet asset.
         */
        public get resolution() { return this._resolution; }
        public set resolution(value) { this._resolution = value; }

        public constructor(definition: Asset.Definition)
        {
            super(definition);

            this._resolution = definition.raw["res"] || 1.0;
            this._mipmap = definition.raw["mipmap"] !== false;
        }

        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            const caps = Capabilities.WebGL.get();

            // Check S3TC
            if (options.indexOf("crn") !== -1 && caps.supportsS3TC) { return "crn"; }

            // Check PVRTC
            if (options.indexOf("pvr") !== -1 && caps.supportsPVRTC) { return "pvr"; }

            // Check PNG
            if (options.indexOf("png") !== -1) { return "png"; }

            // Unsupported
            return null;
        }

        public clone(): RSSpriteSheetAsset
        {
            return new RSSpriteSheetAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            // Request the spritesheet file
            const spriteSheetData = await Asset.ajax({ url }, Request.AJAXResponseType.JSON);

            // Load images
            const images = spriteSheetData["images"] as string[];
            this._compressedImgs = new Array<Compression.CompressedImage>();
            for (let i = 0, l = images.length; i < l; ++i)
            {
                const imageURL = `${Path.combine(Path.directoryName(url), images[i])}.${format}`;

                switch (format)
                {
                    case "png":
                    {
                        const baseTex = spriteSheetData["images"][i] = new PIXI.BaseTexture(
                            await Asset.tag({ url: imageURL, tagName: "img" }),
                            PIXI.SCALE_MODES.LINEAR,
                            this._resolution
                        );
                        baseTex.mipmap = this._mipmap;
                        break;
                    }
                    case "crn":
                    {
                        const buffer = await Asset.ajax({ url: imageURL }, Request.AJAXResponseType.Binary);

                        this._compressedImgs[i] = new Compression.CompressedImage(imageURL);
                        this._compressedImgs[i].loadFromArrayBuffer(buffer, true);
                        // this._compressedImgs[i].levels = 1;
                        const baseTex = spriteSheetData["images"][i] = new PIXI.BaseTexture(
                            this._compressedImgs[i] as any,
                            PIXI.SCALE_MODES.LINEAR,
                            this._resolution
                        );
                        baseTex.mipmap = this._mipmap;
                        baseTex.premultipliedAlpha = false;
                        break;
                    }
                    case "pvr":
                    {
                        const buffer = await Asset.ajax({ url: imageURL }, Request.AJAXResponseType.Binary);
                        this._compressedImgs[i] = new Compression.CompressedImage(imageURL);
                        this._compressedImgs[i].loadFromArrayBuffer(buffer, false);
                        // this._compressedImgs[i].levels = 1;
                        const baseTex = spriteSheetData["images"][i] = new PIXI.BaseTexture(
                            this._compressedImgs[i] as any,
                            PIXI.SCALE_MODES.LINEAR,
                            this._resolution
                        );
                        baseTex.mipmap = this._mipmap;
                        break;
                    }
                    default:
                        throw new Error(`Unsupported format '${this._format}'`);
                }

            }

            // Create the spritesheet
            return new Rendering.RSSpriteSheet(spriteSheetData,  this._resolution);
        }
    }
    Asset.RSSpriteSheetAsset = RSSpriteSheetAsset;
}