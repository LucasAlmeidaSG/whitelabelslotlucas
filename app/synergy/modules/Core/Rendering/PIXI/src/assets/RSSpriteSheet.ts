/** @internal */
namespace RS.Rendering.Pixi
{
    
    /**
     * Raw JSON data for a sprite sheet.
     */
    export interface SpriteSheetData
    {
        "framerate"?: number;
        "images": PIXI.BaseTexture[];
        "imageMaps": Map<PIXI.BaseTexture[]>;
        "frames": number[][];
        "animations": Map<{
            "frames": number[];
            "next"?: boolean|string;
            "speed"?: number;
        }>;
        "events":
        {
            "frame": number;
            "event": string;
        }[];
    }

    /**
	 * Encapsulates the properties and methods associated with a sprite sheet.
     * A sprite sheet is a series of images (usually animation frames) combined into a larger image (or images).
     * For example, an animation consisting of eight 100x100 images could be combined into a single 400x200 sprite sheet (4 frames across by 2 high).
     *
     * Ported from EaselJS to PIXI - T Smith, June 2016
     */
    export class RSSpriteSheet implements ISpriteSheet
    {
        protected _framerate:   number                         = 0;
        protected _animations:  string[]                       = null;
        protected _frames:      Frame[]                        = null;
        protected _data:        Map<Rendering.IAnimation>|null = null;
        protected _dataReverse: Map<Rendering.IAnimation>|null = null;
        protected _resolution:  number;
        protected _maxFrame:    RS.Rendering.Rectangle         = null;

        private _images:        Pixi.Texture[]                 = null;
        private _imageMaps:     Map<Pixi.Texture[]>|null       = null;

        constructor(data: SpriteSheetData, resolution: number = 1)
        {
            // setup:
            this._resolution = resolution;
            this.parseData(data);
        }

        /**
         * Gets or sets the framerate to use by default for Sprite instances using the SpriteSheet.
         */
        public get framerate() { return this._framerate; }
        public set framerate(value) { this._framerate = value; }

        /**
         * Gets an array of all available animation names available on this sprite sheet as strings.
         */
        public get animations() { return this._animations.slice(); }

        /**
         * Gets a rectangle encompassing every frame within this spritesheet.
         */
        public get maxFrame() { return this._maxFrame; }

        /**
         * Returns the total number of frames in the specified animation, or in the whole sprite sheet if the animation param is omitted.
         * Returns 0 if the spritesheet relies on calculated frame counts, and the images have not been fully loaded.
         * @param animation     The name of the animation to get a frame count for.
         * @returns             The number of frames in the animation, or in the entire sprite sheet if the animation param is omitted.
         */
        public getNumFrames(animation?: string): number
        {
            if (animation == null)
            {
                return this._frames.length;
            }
            else
            {
                const data = this._data[animation];
                if (data == null)
                {
                    return 0;
                }
                else
                {
                    return data.frames.length;
                }
            }
        }

        /**
         * Returns an object defining the specified animation.
         * @param name      The name of the animation to get.
         * @returns         A generic object with frames, speed, name, and next properties.
         */
        public getAnimation(name: string, reverse: boolean = false): Rendering.IAnimation
        {
            return reverse ? this._dataReverse[name] : this._data[name];
        }

        /**
         * Returns an object specifying the image and source rect of the specified frame.
         * @param frameIndex    The index of the frame.
         * @returns             A generic object with image and rect properties. Returns null if the frame does not exist.
         */
        public getFrame(frameIndex: number): Frame;

        /**
         * Returns an object specifying the image and source rect of the specified frame.
         * @param frameIndex    The index of the frame.
         * @returns             A generic object with image and rect properties. Returns null if the frame does not exist.
         */
        public getFrame(animName: string, frameIndex?: number): Frame;

        public getFrame(p1: number|string, p2?: number): Frame
        {
            if (Is.number(p1))
            {
                return this._frames[p1];
            }
            else
            {
                return this.getFrame(this.getAnimation(p1).frames[p2 || 0]);
            }
        }

        public getFrames(): RS.Rendering.Frame[] {
            return this._frames;
        }

        /**
         * Returns a Rectangle instance defining the bounds of the specified frame relative to the pivot.
         * For example, a 90 x 70 frame with a regX of 50 and a regY of 40 would return:
         * 	[x=-50, y=-40, width=90, height=70]
         * @param frameIndex    The index of the frame.
         * @param rectangle     A Rectangle instance to copy the values into. By default a new instance is created.
         * @returns             A Rectangle instance. Returns null if the frame does not exist, or the image is not fully loaded.
         */
        public getFrameBounds(frameIndex: number, out?: Rendering.Rectangle): Rendering.Rectangle
        {
            const frame = this.getFrame(frameIndex);
            if (frame == null) { return null; }
            out = out || new Rendering.Rectangle();
            out.x = frame.trimRect.x - frame.reg.x;
            out.y = frame.trimRect.y - frame.reg.y;
            out.w = frame.trimRect.w;
            out.h = frame.trimRect.h;
            return out;
        }

        /**
         * Returns a string representation of this object.
         * @returns     A string representation of the instance.
         */
        public toString(): string
        {
            return "[RSSpriteSheet]";
        }

        /**
         * Creates a new copy of this sprite sheet.
         */
        public clone(): RSSpriteSheet
        {
            const newSS: RSSpriteSheet = Object.create(RSSpriteSheet.prototype);
            newSS._framerate = this._framerate;

            newSS._animations = [...this._animations];
            newSS._frames = this._frames.map((f) =>
            {
                return {
                    ...f,
                    maps: {...f.maps},
                    rect: f.imageRect.clone()
                };
            });
            newSS._images = [...this._images];
            newSS._imageMaps = {};
            for (const mapName in this._imageMaps) { newSS._imageMaps[mapName] = [...this._imageMaps[mapName]]; }
            newSS._data = {};
            for (const name in this._data) { newSS._data[name] = {...this._data[name], frames: [...this._data[name].frames] }; }
            newSS._dataReverse = {};
            for (const name in this._dataReverse) { newSS._dataReverse[name] = {...this._dataReverse[name], frames: [...this._dataReverse[name].frames] }; }
            
            newSS._resolution = this._resolution;
            newSS._maxFrame = this._maxFrame != null ? this._maxFrame.clone() : null;

            return newSS;
        }

        public warmup(stage: IStage, animationName?: string): void
        {
            if (animationName == null)
            {
                // Warm all frames in sheet.
                const frames = this.getFrames();
                for (const frame of frames)
                {
                    frame.imageData.texture.warmup(stage);
                }
            }
            else
            {
                // Warm each animation frame.
                const animation = this.getAnimation(animationName);
                for (const frameNo of animation.frames)
                {
                    const frame = this.getFrame(frameNo);
                    frame.imageData.texture.warmup(stage);
                }
            }
        }

        /**
         *
         * @param data      An object describing the SpriteSheet data.
         */
        protected parseData(data: SpriteSheetData)
        {
            this._framerate = data["framerate"] || 0;

            // parse images
            if (data["images"])
            {
                this._images = data["images"].map((img) => new RS.Rendering.Pixi.Texture(img));
            }

            if (data["imageMaps"])
            {
                this._imageMaps = {};
                for (const key in data["imageMaps"])
                {
                    const mapArr = data["imageMaps"][key];
                    this._imageMaps[key] = mapArr.map((img) => new RS.Rendering.Pixi.Texture(img));
                }
            }

            // parse frames
            this._frames = [];
            const arr = data["frames"];
            for (const frameData of arr)
            {
                const imageRect = new Rendering.Rectangle(frameData[0] / this._resolution, frameData[1] / this._resolution, frameData[2] / this._resolution, frameData[3] / this._resolution);
                const frame: Frame =
                {
                    imageData: null,
                    imageID: frameData[4]||0,
                    imageRect,
                    reg: { x: (frameData[5]/this._resolution)||0, y: (frameData[6]/this._resolution)||0 },
                    untrimmedSize: { w: (frameData[7] || frameData[2]) / this._resolution, h: (frameData[8] || frameData[3]) / this._resolution },
                    trimRect: new Rendering.Rectangle((frameData[9] || 0) / this._resolution, (frameData[10] || 0) / this._resolution, imageRect.w, imageRect.h),
                    event: null
                };
                this._frames.push(frame);
            }
            if (data["events"])
            {
                for (const event of data["events"])
                {
                    this._frames[event.frame].event = event.event;
                }
            }

            // parse animations
            this._animations = [];
            if (data["animations"])
            {
                this._data = {};
                this._dataReverse = {};
                let name;
                for (name in data["animations"])
                {
                    const anim: Rendering.IAnimation = {name, frames: null, speed: null, next: null};
                    const obj = data["animations"][name];
                    
                    // complex
                    anim.speed = obj["speed"] || 1.0;
                    const next = obj["next"];
                    anim.frames = obj["frames"];
                    if (Is.string(next))
                    {
                        anim.next = next;
                    }
                    else if (next === true && anim.frames.length > 1)
                    {
                        // loop
                        anim.next = name;
                    }
                    else if (next === false)
                    {
                        // stop
                        anim.next = null;
                    }
                    this._animations.push(name);
                    this._data[name] = anim;
                    this._dataReverse[name] = { ...anim, frames: anim.frames.slice().reverse() };
                }
            }

            this.resolveFrameImages();
        }

        protected expandMaxFrame(rect: RS.Rendering.ReadonlyRectangle): void {
            this._maxFrame.x = Math.min(this._maxFrame.x, rect.x);
            this._maxFrame.y = Math.min(this._maxFrame.y, rect.y);
            this._maxFrame.w = Math.max(this._maxFrame.w, rect.w);
            this._maxFrame.w = Math.max(this._maxFrame.h, rect.h);
        }

        protected resolveFrameImages(): void
        {
            this._maxFrame = new RS.Rendering.Rectangle();
            for (let i = 0; i < this._frames.length; i++)
            {
                const frame = this._frames[i];
                if (!frame.imageData && frame.imageID != null)
                {
                    const baseTexture = this._images[frame.imageID];
                    frame.imageData = new RS.Rendering.Pixi.SubTexture(baseTexture, frame.imageRect);
                    this.expandMaxFrame(frame.imageRect);
                }

                if (this._imageMaps && frame.maps == null)
                {
                    frame.maps = {};
                    for (const mapName in this._imageMaps)
                    {
                        const baseTexture = this._imageMaps[mapName][frame.imageID];
                        frame.maps[mapName] = new RS.Rendering.Pixi.SubTexture(baseTexture, frame.imageRect);
                    }
                }
            }
        }

        /**
         * Creates a copy of this sprite sheet with all frames unpacked into individual textures.
         * Useful to limit texture bandwidth usage of drawing a single frame, or for cases where UV wrapping of individual frames is desired.
         */
        // public extract(): SpriteSheet
        // {
        //     const newSS = this.clone();

        //     // We don't support image maps
        //     if (this._imageMaps && Object.keys(this._imageMaps).length > 0)
        //     {
        //         Log.warn(`SpriteSheet.extract: Image maps not supported yet`);
        //         newSS._imageMaps = {};
        //     }

        //     // Iterate all frames
        //     const tmpSpr = new PIXI.Sprite();
        //     const renderer = RS.ViewManager.stage.renderer;
        //     newSS._images.length = 0;
        //     for (let frameID = 0; frameID < this._frames.length; frameID++)
        //     {
        //         const frame = newSS._frames[frameID];

        //         // Pull out the frame and render to a new RT
        //         tmpSpr.texture = frame.image;
        //         const newTex = tmpSpr.generateTexture(renderer);
        //         newSS._images.push(newTex.baseTexture);

        //         // Update the frame
        //         frame.image = newTex;
        //         frame.imageID = frameID;
        //         if (frame.maps && Object.keys(frame).length > 0) { frame.maps = {}; }
        //         frame.rect = new PIXI.Rectangle(0, 0, newTex.width, newTex.height);
        //     }

        //     // Done
        //     return newSS;
        // }
    }
    RS.Rendering.RSSpriteSheet = RSSpriteSheet;
}
