/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a font asset type.
     */
    @Asset.Type("json")
    export class JsonAsset extends Asset.Base<string> implements IJsonAsset<string>
    {
        /**
         * Selects a format to use from the given options.
         * @param options 
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): JsonAsset
        {
            return new JsonAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url 
         */
        protected async performLoad(url: string, format: string)
        {
            const file = await Asset.ajax({ url }, Request.AJAXResponseType.Text);
            return file;
        }
    }
    Asset.JsonAsset = JsonAsset;
}