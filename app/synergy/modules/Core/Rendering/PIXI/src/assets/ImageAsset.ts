/// <reference path="../compression/CompressedImage.ts" />

/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates an image asset type.
     */
    @Asset.Type("image")
    export class ImageAsset extends Asset.Base<Rendering.Pixi.Texture> implements IImageAsset<Rendering.Pixi.Texture>
    {
        protected _resolution: number;
        protected _compressedImg: Compression.CompressedImage;
        protected _wrapMode: string;
        protected _mipmap: boolean;

        /**
         * Gets or sets the resolution of this image asset.
         */
        public get resolution() { return this._resolution; }
        public set resolution(value) { this._resolution = value; }

        /**
         * Gets or sets the wrap mode of this image asset.
         */
        public get wrapMode() { return this._wrapMode; }
        public set wrapMode(value) { this._wrapMode = value; }

        public constructor(definition: Asset.Definition)
        {
            super(definition);

            this._resolution = definition.raw["res"] || 1.0;
            this._wrapMode = definition.raw["wrapMode"] || "clamp";
            this._mipmap = definition.raw["mipmap"] !== false;
        }

        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            const caps = Capabilities.WebGL.get();

            // Check S3TC
            if (options.indexOf("crn") !== -1 && caps.supportsS3TC) { return "crn"; }

            // Check PVRTC
            if (options.indexOf("pvr") !== -1 && caps.supportsPVRTC) { return "pvr"; }

            // Check PNG
            if (options.indexOf("png") !== -1) { return "png"; }

            if (options.indexOf("jpg") !== -1) { return "jpg"; }

            if (options.indexOf("jpeg") !== -1) { return "jpeg"; }

            // Unsupported
            return null;
        }

        public clone(): ImageAsset
        {
            return new ImageAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            url = Path.replaceExtension(url, `.${format}`);
            switch (format)
            {
                case "jpg":
                case "jpeg":
                case "png":
                {
                    const baseTex = new PIXI.BaseTexture(
                        await Asset.tag({ url, tagName: "img" }),
                        PIXI.SCALE_MODES.LINEAR,
                        this._resolution
                    );
                    baseTex.mipmap = this._mipmap;
                    this.setWrapMode(baseTex);
                    return new Rendering.Pixi.Texture(baseTex);
                }
                case "crn":
                {
                    const buffer = await Asset.ajax({ url }, Request.AJAXResponseType.Binary);
                    this._compressedImg = new Compression.CompressedImage(url);
                    this._compressedImg.loadFromArrayBuffer(buffer, true);
                    // this._compressedImg.levels = 1;
                    const baseTex = new PIXI.BaseTexture(
                        this._compressedImg as any,
                        PIXI.SCALE_MODES.LINEAR,
                        this._resolution
                    );
                    baseTex.mipmap = this._mipmap;
                    this.setWrapMode(baseTex);
                    baseTex.premultipliedAlpha = false;
                    return new Rendering.Pixi.Texture(baseTex);
                }
                case "pvr":
                {
                    const buffer = await Asset.ajax({ url }, Request.AJAXResponseType.Binary);
                    this._compressedImg = new Compression.CompressedImage(url);
                    this._compressedImg.loadFromArrayBuffer(buffer, false);
                    const baseTex = new PIXI.BaseTexture(
                        this._compressedImg as any,
                        PIXI.SCALE_MODES.LINEAR,
                        this._resolution
                    );
                    baseTex.mipmap = this._mipmap;
                    this.setWrapMode(baseTex);
                    return new Rendering.Pixi.Texture(baseTex);
                }
                default:
                    throw new Error(`Unsupported format '${format}'`);
            }
        }

        protected setWrapMode(target: PIXI.BaseTexture)
        {
            switch (this._wrapMode)
            {
                case "repeat": target.wrapMode = PIXI.WRAP_MODES.REPEAT; break;
                case "clamp": target.wrapMode = PIXI.WRAP_MODES.CLAMP; break;
            }
        }
    }
    Asset.ImageAsset = ImageAsset;
}