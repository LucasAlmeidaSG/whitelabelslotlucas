/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a bitmap font asset type.
     */
    @Asset.Type("bitmapfont")
    export class BitmapFontLoader extends Asset.Base<string> implements IBitmapFontAsset<string>
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): BitmapFontLoader
        {
            return new BitmapFontLoader(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            // Request the bitmap font file
            const fontData = await Asset.ajax({ url }, Request.AJAXResponseType.JSON);

            // Iterate all of the pages
            const pages: string[] = fontData["pages"];
            for (let i = 0, l = pages.length; i < l; ++i)
            {
                fontData["pages"][i] = new PIXI.BaseTexture(
                    await Asset.tag({
                        url: Path.combine(Path.directoryName(url), pages[i]),
                        tagName: "img"
                    })
                );
            }

            // Adjust data and store in pixi
            this.adjustFontData(fontData);
            PIXI.extras.BitmapText.fonts[this.id] = fontData;

            // Return asset ID back
            return this.id;
        }

        private adjustFontData(fontData: object): object
        {
            const chars: object[] = fontData["chars"];
            const newChars: object[] = [];
            for (let i = 0; i < chars.length; i++)
            {
                const char = chars[i];
                if (char)
                {
                    const baseTex: PIXI.BaseTexture = fontData["pages"][char["pageIndex"]];
                    const textureRect = new PIXI.Rectangle(
                        char["x"], char["y"],
                        char["width"], char["height"]
                    );
                    try
                    {
                        char["texture"] = new PIXI.Texture(baseTex, textureRect);
                    }
                    catch (err)
                    {
                        Log.warn(`${char["pageIndex"]}: ${err}`);
                    }
                    newChars[char["id"]] = char;
                }
            }
            fontData["chars"] = newChars;
            return fontData;
        }
    }
    Asset.BitmapFontLoader = BitmapFontLoader;
}
