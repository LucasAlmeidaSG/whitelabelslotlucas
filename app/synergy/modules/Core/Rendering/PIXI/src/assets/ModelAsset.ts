/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a model asset type.
     */
    @Asset.Type("model")
    export class ModelAsset extends Asset.Base<RS.Rendering.Scene.IReadonlyGeometry> implements IModelAsset
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone()
        {
            return new ModelAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            // Download the file
            const data = await Request.ajax({ url }, Request.AJAXResponseType.Binary);

            // Read header and identify endianness
            const uint32View = new Uint32Array(data, 0, data.byteLength >> 2);
            const uint8View = new Uint8Array(data);
            let needSwapEndian: boolean;
            if (uint32View[0] === 541478227)
            {
                // We are little endian
                needSwapEndian = false;
            }
            else if (uint32View[0] === 1397573152)
            {
                // We are big endian
                needSwapEndian = true;
            }
            else
            {
                throw new Error(`SMF file is invalid (header not present)`);
            }
            const vertexDescriptorCount = uint8View[4];
            const elementDescriptorCount = uint8View[5];

            // Read vertex descriptors
            let ptr = 8;
            const vertexDescriptors = new Array<SMF.VertexDescriptor>(vertexDescriptorCount);
            const vertexBufferRanges = new Array<{ offset: number; size: number; }>(vertexDescriptorCount);
            for (let i = 0; i < vertexDescriptorCount; ++i)
            {
                vertexDescriptors[i] =
                {
                    dataType: uint8View[ptr],
                    usage: uint8View[ptr + 1],
                    elementsPerVertex: uint8View[ptr + 2],
                    normalised: uint8View[ptr + 3] === 1
                };
                ptr += 4;
                vertexBufferRanges[i] =
                {
                    offset: uint32View[ptr >> 2],
                    size: uint32View[(ptr >> 2) + 1]
                };
                ptr += 8;
                if (needSwapEndian)
                {
                    vertexBufferRanges[i].offset = Math.Endian.swapUInt32(vertexBufferRanges[i].offset);
                    vertexBufferRanges[i].size = Math.Endian.swapUInt32(vertexBufferRanges[i].size);
                }
            }

            // Read element descriptors
            const elementDescriptors = new Array<SMF.ElementDescriptor>(elementDescriptorCount);
            const elementBufferRanges = new Array<{ offset: number; size: number; }>(elementDescriptorCount);
            for (let i = 0; i < elementDescriptorCount; ++i)
            {
                elementDescriptors[i] =
                {
                    geometryType: uint8View[ptr]
                };
                ptr += 4;
                elementBufferRanges[i] =
                {
                    offset: uint32View[ptr >> 2],
                    size: uint32View[(ptr >> 2) + 1]
                };
                ptr += 8;
                if (needSwapEndian)
                {
                    elementBufferRanges[i].offset = Math.Endian.swapUInt32(elementBufferRanges[i].offset);
                    elementBufferRanges[i].size = Math.Endian.swapUInt32(elementBufferRanges[i].size);
                }
            }

            // Read vertex buffers
            const vertexBuffers = new Array<Float32Array | Uint8Array>(vertexDescriptorCount);
            for (let i = 0; i < vertexDescriptorCount; ++i)
            {
                const vDesc = vertexDescriptors[i];
                switch (vDesc.dataType)
                {
                    case SMF.DataType.Float:
                    {
                        const arr = new Float32Array(data, vertexBufferRanges[i].offset, vertexBufferRanges[i].size >> 2);
                        if (needSwapEndian)
                        {
                            vertexBuffers[i] = Math.Endian.swapFloat32Array(arr);
                        }
                        else
                        {
                            vertexBuffers[i] = arr;
                        }
                        break;
                    }
                    case SMF.DataType.Byte:
                    {
                        vertexBuffers[i] = new Uint8Array(data, vertexBufferRanges[i].offset, vertexBufferRanges[i].size);
                        break;
                    }
                }
            }

            // Read element buffers
            const elementBuffers = new Array<Float32Array | Uint8Array>(elementDescriptorCount);
            for (let i = 0; i < elementDescriptorCount; ++i)
            {
                const arr = new Uint16Array(data, elementBufferRanges[i].offset, elementBufferRanges[i].size >> 1);
                if (needSwapEndian)
                {
                    elementBuffers[i] = Math.Endian.swapUInt16Array(arr);
                }
                else
                {
                    elementBuffers[i] = arr;
                }
            }

            // Create geometry
            const geo = new RS.Rendering.Pixi.Scene.Geometry();
            geo.vertexCount = 0;
            let hasTangents = false, hasBinormals = false;
            for (let i = 0; i < vertexDescriptorCount; ++i)
            {
                const vDesc = vertexDescriptors[i];
                const vBuff = vertexBuffers[i];
                geo.vertexBindings.push({
                    type: SMF.VertexUsage.toSceneType(vDesc.usage),
                    bindingID: SMF.VertexUsage.toSceneIndex(vDesc.usage),
                    vertexData: vBuff,
                    vertexSize: vDesc.elementsPerVertex,
                    normalise: vDesc.normalised
                });
                if (geo.vertexCount === 0 && vBuff instanceof Float32Array)
                {
                    geo.vertexCount = vBuff.length / vDesc.elementsPerVertex;
                }
            }
            for (let i = 0; i < elementDescriptorCount; ++i)
            {
                const elDesc = elementDescriptors[i];
                const elBuff = elementBuffers[i];
                geo.indexBindings.push({
                    type: SMF.GeometryType.toScene(elDesc.geometryType),
                    indexData: elBuff
                });
            }
            const err = geo.validate(true);
            if (err) { throw new Error(err); }

            // Calculate tangents and binormals if needed
            if (!hasTangents || !hasBinormals)
            {
                geo.recalculateTangents();
            }

            // Done
            return geo;
        }
    }
    Asset.ModelAsset = ModelAsset;

    export namespace SMF
    {
        /**
         * Encapsulates a datatype for a vertex element.
         */
        export enum DataType
        {
            Float,
            Byte
        }

        /**
         * Encapsulates a geometry type.
         */
        export enum GeometryType
        {
            Lines,
            Triangles
        }

        export namespace GeometryType
        {
            export function toScene(type: GeometryType): RS.Rendering.Scene.PrimitiveType
            {
                switch (type)
                {
                    case GeometryType.Lines: return RS.Rendering.Scene.PrimitiveType.Line;
                    case GeometryType.Triangles: return RS.Rendering.Scene.PrimitiveType.Triangle;
                    default: throw new Error(`Invalid GeometryType '${type}'`);
                }
            }
        }

        /**
         * Encapsulates how vertex data should be consumed.
         */
        export enum VertexUsage
        {
            Position,
            Color,
            Normal,
            TexCoord
        }

        export namespace VertexUsage
        {
            export function toSceneType(usage: VertexUsage): RS.Rendering.Scene.VertexBindingType
            {
                switch (usage)
                {
                    case VertexUsage.Position: return RS.Rendering.Scene.VertexBindingType.Position;
                    case VertexUsage.Color: return RS.Rendering.Scene.VertexBindingType.Color;
                    case VertexUsage.Normal: return RS.Rendering.Scene.VertexBindingType.Normal;
                    case VertexUsage.TexCoord: return RS.Rendering.Scene.VertexBindingType.TexCoord;
                    default: throw new Error(`Invalid VertexUsage '${usage}'`);
                }
            }

            export function toSceneIndex(usage: VertexUsage): number
            {
                switch (usage)
                {
                    case VertexUsage.Position: return 0;
                    case VertexUsage.Color: return 0;
                    case VertexUsage.Normal: return 0;
                    case VertexUsage.TexCoord: return 0;
                    default: throw new Error(`Invalid VertexUsage '${usage}'`);
                }
            }
        }

        export interface VertexDescriptor<TDataType extends DataType = DataType>
        {
            /** The data type of each element. */
            dataType: TDataType;

            /** How data should be consumed. */
            usage: VertexUsage;

            /** The number of elements per vertex. */
            elementsPerVertex: number;

            /** Whether elements should be normalised. */
            normalised: boolean;
        }

        export interface ElementDescriptor
        {
            /** The type of geometry for the elements. */
            geometryType: GeometryType;
        }
    }
}