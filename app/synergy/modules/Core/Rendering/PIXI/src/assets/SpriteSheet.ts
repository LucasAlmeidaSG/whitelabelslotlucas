/** @internal */
namespace RS.Rendering.Pixi
{
    /**
     * Encapsulates data about a single animation within a SpriteSheet.
     */
    
    /**
	 * Encapsulates the properties and methods associated with a sprite sheet.
     * A sprite sheet is a series of images (usually animation frames) combined into a larger image (or images).
     * For example, an animation consisting of eight 100x100 images could be combined into a single 400x200 sprite sheet (4 frames across by 2 high).
     *
     * Ported from EaselJS to PIXI - T Smith, June 2016
     */
    export class SpriteSheet implements ISpriteSheet
    {
        protected _frames: Frame[] = null;
        protected _data: any = null;
        protected _resolution: number;
        protected _maxFrame: PIXI.Rectangle = null;
        protected _framerate: number = 30;

        constructor(data: any, resolution: number = 1)
        {
            this._data = data;
            this._resolution = resolution;
            this.parseData(data);
        }
        
        /**
         * Gets or sets the framerate to use by default for Sprite instances using the SpriteSheet.
         */
        public get framerate() { return this._framerate; }
        public set framerate(value) { this._framerate = value; }
        
        /**
         * Gets an array of all available animation names available on this sprite sheet as strings.
         */
        public get animations() { return []; }
        /**
         * Gets a rectangle encompassing every frame within this spritesheet.
         */
        public get maxFrame(): RS.Rendering.ReadonlyRectangle
        {
            return new RS.Rendering.ReadonlyRectangle(this._maxFrame.x, this._maxFrame.y, this._maxFrame.width, this._maxFrame.height);
        }

        /**
         * Returns the total number of frames in the specified animation, or in the whole sprite sheet if the animation param is omitted.
         * Returns 0 if the spritesheet relies on calculated frame counts, and the images have not been fully loaded.
         * @param animation     The name of the animation to get a frame count for.
         * @returns             The number of frames in the animation, or in the entire sprite sheet if the animation param is omitted.
         */
        public getNumFrames(): number
        {
            return this._frames.length;
        }

        public getFrames(): RS.Rendering.Frame[]
        {
            return this._frames;
        }
        /**
         * Returns an object defining the specified animation.
         * @param name      The name of the animation to get.
         * @returns         A generic object with frames, speed, name, and next properties.
         */
        public getAnimation(name: string, reverse: boolean = false): Rendering.IAnimation
        {
            return null;
        }

        /**
         * Returns an object specifying the image and source rect of the specified frame.
         * @param frameIndex    The index of the frame.
         * @returns             A generic object with image and rect properties. Returns null if the frame does not exist.
         */
        public getFrame(frameIndex: number): RS.Rendering.Frame;

        /**
         * Returns an object specifying the image and source rect of the specified frame.
         * @param frameIndex    The index of the frame.
         * @returns             A generic object with image and rect properties. Returns null if the frame does not exist.
         */
        public getFrame(name: string, frameIndex?: number): RS.Rendering.Frame;

        public getFrame(p1: number | string, p2: number = 0): RS.Rendering.Frame
        {
            if (Is.number(p1)) 
            {
                return this._frames[p1];
            }
            else 
            {
                for (let i: number = 0; i < this._data["frames"].length; i++) 
                {
                    if (this._data["frames"][i]["filename"] === p1 as string)
                    {
                        return this._frames[i];
                    }
                }
            }
        }
        /**
         * Returns a Rectangle instance defining the bounds of the specified frame relative to the pivot.
         * For example, a 90 x 70 frame with a regX of 50 and a regY of 40 would return:
         * 	[x=-50, y=-40, width=90, height=70]
         * @param frameIndex    The index of the frame.
         * @param rectangle     A Rectangle instance to copy the values into. By default a new instance is created.
         * @returns             A Rectangle instance. Returns null if the frame does not exist, or the image is not fully loaded.
         */
        public getFrameBounds(frameIndex: number, out?: Rendering.Rectangle): Rendering.Rectangle 
        {
            const frame = this.getFrame(frameIndex);
            if (frame == null) { return null; }
            out = out || new Rendering.Rectangle();
            out.x = frame.trimRect.x - frame.reg.x;
            out.y = frame.trimRect.y - frame.reg.y;
            out.w = frame.trimRect.w;
            out.h = frame.trimRect.h;
            return out;
        }

        /**
         * Returns a string representation of this object.
         * @returns     A string representation of the instance.
         */
        public toString(): string
        {
            return "[SpriteSheet]";
        }

        /**
         * Creates a new copy of this sprite sheet.
         */
        public clone(): SpriteSheet
        {
            const newSS: SpriteSheet = Object.create(SpriteSheet.prototype);
            newSS._frames = this._frames.map((f) => {
                return {
                    ...f,
                    //maps: {...f.maps},
                    rect: f.imageRect.clone()
                };
            });
            newSS._data = this._data;

            newSS._resolution = this._resolution;
            newSS._maxFrame = this._maxFrame != null ? this._maxFrame.clone() : null;

            return newSS;
        }

        public warmup(stage: IStage, animationName?: string): void
        {
            if (animationName == null)
            {
                // Warm all frames in sheet.
                const frames = this.getFrames();
                for (const frame of frames)
                {
                    frame.imageData.texture.warmup(stage);
                }
            }
            else
            {
                // Warm each animation frame.
                const animation = this.getAnimation(animationName);
                for (const frameNo of animation.frames)
                {
                    const frame = this.getFrame(frameNo);
                    frame.imageData.texture.warmup(stage);
                }
            }
        }

        /**
         *
         * @param data      An object describing the SpriteSheet data.
         */
        protected parseData(data: RS.Rendering.ISpriteSheetData<PIXI.BaseTexture>)
        {
            // parse frames
            this._frames = [];
            const arr = data.frames;
            for (const frameData of arr)
            {
                const imageRect = new Rendering.Rectangle(
                    frameData["frame"]["x"] / this._resolution, 
                    frameData["frame"]["y"] / this._resolution, 
                    frameData["frame"]["w"] / this._resolution, 
                    frameData["frame"]["h"] / this._resolution);

                const frame: Frame =
                {
                    imageData: null,
                    imageID: 0,
                    imageRect,
                    reg:
                    {
                        x:0,
                        y:0
                    },
                    untrimmedSize:
                    { 
                        w: (frameData["sourceSize"]["w"] / this._resolution), 
                        h: (frameData["sourceSize"]["h"] / this._resolution) 
                    },
                    trimRect: new Rendering.Rectangle(
                        0 / this._resolution, 
                        0 / this._resolution, 
                        imageRect.w,
                        imageRect.h
                    )
                };
                if (frameData["pivot"])
                {
                    frame.reg =
                    { 
                        x: ((imageRect.w * frameData["pivot"]["x"]) / this._resolution),
                        y: ((imageRect.h * frameData["pivot"]["y"]) / this._resolution)
                    };
                }
                this._frames.push(frame);
            }
            this.resolveFrameImages();
        }

        protected expandMaxFrame(rect: RS.Rendering.ReadonlyRectangle): void
        {
            this._maxFrame.x = Math.min(this._maxFrame.x, rect.x);
            this._maxFrame.y = Math.min(this._maxFrame.y, rect.y);
            this._maxFrame.width = Math.max(this._maxFrame.width, rect.w);
            this._maxFrame.height = Math.max(this._maxFrame.height, rect.h);
        }

        protected resolveFrameImages(): void
        {
            this._maxFrame = new PIXI.Rectangle();
            for (let i = 0; i < this._frames.length; i++)
            {
                const frame = this._frames[i];
                if (!frame.imageData && frame.imageID != null)
                {
                    const baseTexture: PIXI.BaseTexture = this._data["meta"]["image"];
                    frame.imageData = new Pixi.SubTexture(new Pixi.Texture(baseTexture), frame.imageRect);
                    this.expandMaxFrame(frame.imageRect);
                }
            }
        }
    }
    RS.Rendering.SpriteSheet = SpriteSheet;
}
