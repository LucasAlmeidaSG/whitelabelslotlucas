/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a video asset type.
     */
    @Asset.Type("video")
    export class VideoAsset extends Asset.Base<string> implements IVideoAsset<PIXI.BaseTexture>
    {
        protected _buffer: ArrayBuffer;
        protected _blob: Blob;
        protected _startPoster: PIXI.BaseTexture;
        protected _endPoster: PIXI.BaseTexture;
        protected _soundSource: Asset.Base<Audio.ISoundSource>;
        protected _framerate: number;
        protected _duration: number;

        /** Gets the start poster texture for this video asset. */
        public get startPoster() { return this._startPoster; }

        /** Gets the end poster texture for this video asset. */
        public get endPoster() { return this._endPoster; }

        /** Gets the sound source for this video asset. */
        public get soundSource() { return this._soundSource; }

        /** Gets the framerate of this video asset. */
        public get framerate() { return this._framerate; }

        /** Gets the duration of this video asset (ms). */
        public get duration() { return this._duration * 1000; }

        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): VideoAsset
        {
            return new VideoAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            url = Path.replaceExtension(url, `.${format}`);

            // Store metadata
            this._framerate = this._def.raw["framerate"] || 0;
            this._duration = this._def.raw["duration"] || 0;
            const usePosters: boolean = this._def.raw["usePosters"] != null ? this._def.raw["usePosters"] : true;

            // Request the posters
            if (usePosters)
            {
                const fileName = Path.fileName(url);
                const dirName = Path.directoryName(url);
                const fileNameNoExt = fileName.substr(0, fileName.indexOf("."));
                const startPosterUrl = Path.combine(dirName, `${fileNameNoExt}_poster_start.png`);
                const endPosterUrl = Path.combine(dirName, `${fileNameNoExt}_poster_end.png`);
                const startPoster = await Request.tag({ tagName: "img", url: startPosterUrl });
                const endPoster = await Request.tag({ tagName: "img", url: endPosterUrl });
                this._startPoster = new PIXI.BaseTexture(startPoster);
                this._endPoster = new PIXI.BaseTexture(endPoster);
            }

            // Request the video itself
            this._buffer = await Asset.ajax({ url }, Request.AJAXResponseType.Binary);
            this._blob = new Blob([this._buffer], { type: "video/mp4" });

            // Request the video sound
            this._soundSource = await this.loadSound(url);

            // Create the URL for it
            return window.URL.createObjectURL(this._blob);
        }

        protected async loadSound(url: string): Promise<Asset.Base<Audio.ISoundSource>>
        {
            const def = this._def.raw["sound"] as Definition;
            if (!def) { return null; }

            const assetCtor = Base.get("sound");
            if (!assetCtor)
            {
                Log.warn(`Could not find appropriate asset for type 'sound' (${this.id})`);
                return null;
            }

            const asset = new assetCtor(def) as Asset.Base<Audio.ISoundSource>;
            await asset.load(url, asset.selectFormat(def["formats"]));
            return asset;
        }
    }
    Asset.VideoAsset = VideoAsset;
}
