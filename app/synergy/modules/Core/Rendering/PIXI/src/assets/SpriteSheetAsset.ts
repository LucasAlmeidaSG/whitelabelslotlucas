/// <reference path="../compression/CompressedImage.ts" />

/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates an image asset type.
     */
    @Asset.Type("spritesheet")
    export class SpriteSheetAsset extends Asset.Base<Rendering.ISpriteSheet> implements ISpriteSheetAsset<Rendering.ISpriteSheet>
    {
        protected _resolution: number;
        protected _mipmap: boolean;
        protected _format: string;
        protected _compressedImg: Compression.CompressedImage;

        /**
         * Gets or sets the resolution of this spritesheet asset.
         */
        public get resolution() { return this._resolution; }
        public set resolution(value) { this._resolution = value; }

        public constructor(definition: Asset.Definition) {
            super(definition);

            this._resolution = definition.raw["res"] || 1.0;
            this._mipmap = definition.raw["mipmap"] !== false;
        }

        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null {
            const caps = Capabilities.WebGL.get();
            // Check S3TC
            if (options.indexOf("crn") !== -1 && caps.supportsS3TC) { return "crn"; }
            // Check PVRTC
            if (options.indexOf("pvr") !== -1 && caps.supportsPVRTC) { return "pvr"; }
            // Check PNG
            if (options.indexOf("png") !== -1) { return "png"; }

            // Unsupported
            return null;
        }

        public clone(): SpriteSheetAsset
        {
            return new SpriteSheetAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string) {
            // Request the spritesheet file
            const spriteSheetData = await Asset.ajax({ url }, Request.AJAXResponseType.JSON);

            // Load images
            const imagePath = spriteSheetData["meta"]["image"] as string;
            this._compressedImg = null;//new Array<Compression.CompressedImage>();
            const imageURL = `${Path.combine(Path.directoryName(url), imagePath)}.${format}`;

            switch (format) {
                case "png":
                    {
                        const baseTex = spriteSheetData["meta"]["image"] = new PIXI.BaseTexture(
                            await Asset.tag({ url: imageURL, tagName: "img" }),
                            PIXI.SCALE_MODES.LINEAR,
                            this._resolution
                        );
                        baseTex.mipmap = this._mipmap;
                        break;
                    }
                case "crn":
                    {
                        const buffer = await Asset.ajax({ url: imageURL }, Request.AJAXResponseType.Binary);

                        this._compressedImg = new Compression.CompressedImage(imageURL);
                        this._compressedImg.loadFromArrayBuffer(buffer, true);
                        // this._compressedImgs[i].levels = 1;
                        const baseTex = spriteSheetData["meta"]["image"] = new PIXI.BaseTexture(
                            this._compressedImg as any,
                            PIXI.SCALE_MODES.LINEAR,
                            this._resolution
                        );
                        baseTex.mipmap = this._mipmap;
                        baseTex.premultipliedAlpha = false;
                        break;
                    }
                case "pvr":
                    {
                        const buffer = await Asset.ajax({ url: imageURL }, Request.AJAXResponseType.Binary);
                        this._compressedImg = new Compression.CompressedImage(imageURL);
                        this._compressedImg.loadFromArrayBuffer(buffer, false);
                        // this._compressedImgs[i].levels = 1;
                        const baseTex = spriteSheetData["meta"]["image"] = new PIXI.BaseTexture(
                            this._compressedImg as any,
                            PIXI.SCALE_MODES.LINEAR,
                            this._resolution
                        );
                        baseTex.mipmap = this._mipmap;
                        break;
                    }
                default:
                    throw new Error(`Unsupported format '${this._format}'`);
            }

            // Create the spritesheet
            return new Rendering.SpriteSheet(spriteSheetData, this._resolution);
        }
    }
    Asset.SpriteSheetAsset = SpriteSheetAsset;
}