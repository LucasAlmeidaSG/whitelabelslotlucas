/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a shader asset type.
     */
    @Asset.Type("shader")
    export class ShaderAsset extends Asset.Base<string> implements IShaderAsset<string>
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): ShaderAsset
        {
            return new ShaderAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            return await Asset.ajax({ url }, Request.AJAXResponseType.Text);
        }
    }
    Asset.ShaderAsset = ShaderAsset;
}