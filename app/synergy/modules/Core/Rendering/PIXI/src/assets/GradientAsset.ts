/** @internal */
namespace RS.Asset.Pixi
{
    /**
     * Encapsulates a font asset type.
     */
    @Asset.Type("gradient")
    export class GradientAsset extends Asset.Base<Asset.GradientStopMap> implements IJsonAsset<Asset.GradientStopMap>
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): GradientAsset
        {
            return new GradientAsset(this.definition);
        }

        protected generateColorStops(stops: GradientAsset.ColourStopInfo[], foreground: RS.Util.Color, background: RS.Util.Color): Rendering.Gradient.Stop[]
        {
            let previous = null;
            let previousColor = null;
            let colorStops: Rendering.Gradient.Stop[] = [];

            for (let stop of stops)
            {
                // If the midpoint isn't at 50% then we'll create an extra stop with the half interpolated colour
                // at the actual midpoint position (this isn't 100% correct, but we can't do quadratic interpolation in the
                // shader (because it uses the normal texture filtering to lookup texture values), so this is as close as we can get)
                let color: RS.Util.Color;
                switch (stop["type"])
                {
                    case "FrgC":
                        color = foreground;
                        break;

                    case "BckC":
                        color = background;
                        break;

                    case "UsrS":
                        color = new RS.Util.Color(stop["color"].r, stop["color"].g, stop["color"].b);
                        break;
                }

                if (stop["midpoint"] !== 50 && previous)
                {
                    const r = RS.Math.linearInterpolate(previousColor.r, color.r, 0.5);
                    const g = RS.Math.linearInterpolate(previousColor.g, color.g, 0.5);
                    const b = RS.Math.linearInterpolate(previousColor.b, color.b, 0.5);
                    const t = RS.Math.linearInterpolate(previous["location"], stop["location"], stop["midpoint"] / 100);
                    colorStops.push({ offset: t / 4096, color: new RS.Util.Color(r, g, b) });
                }

                // If we've not added any stops and this one isn't at the start, then create an extra at the begining
                if (colorStops.length === 0)
                {
                    if (stop["location"] > 0)
                    {
                        colorStops.push({ offset: 0, color });
                    }
                }
                colorStops.push({ offset: stop["location"] / 4096, color });
                previous = stop;
                previousColor = color;
            }

            // Insert an extra stop at the end if we aren't at the end
            if (colorStops[colorStops.length - 1].offset < 1)
            {
                colorStops.push({ offset: 1, color: colorStops[colorStops.length - 1].color });
            }

            return colorStops;
        }

        protected generateTransStops(stops: GradientAsset.TransStopInfo[]): Rendering.Gradient.Stop[]
        {
            let previous = null;
            let transStops: Rendering.Gradient.Stop[] = [];

            for (let stop of stops)
            {
                if (stop["midpoint"] !== 50 && previous)
                {
                    const o = RS.Math.linearInterpolate(previous["opacity"], stop["opacity"], 0.5);
                    const t = RS.Math.linearInterpolate(previous["location"], stop["location"], stop["midpoint"] / 100);
                    transStops.push({ offset: t / 4096, color: new RS.Util.Color(0, 0, 0, o * 2.55) });
                }

                // If the first stop isn't at the beginning then create a stop at the start
                if (transStops.length === 0 && stop["location"] > 0)
                {
                    transStops.push({ offset: 0, color: new RS.Util.Color(0, 0, 0, stop["opacity"] * 2.55) });
                }

                transStops.push({ offset: stop["location"] / 4096, color: new RS.Util.Color(0, 0, 0, stop["opacity"] * 2.55) });
                previous = stop;
            }

            // Create an extra stop at the end if our stops don't reach the end
            if (transStops[transStops.length - 1].offset < 1)
            {
                transStops.push({ offset: 1, color: transStops[transStops.length - 1].color });
            }

            return transStops;
        }

        protected interleaveStops(color: Rendering.Gradient.Stop[], trans: Rendering.Gradient.Stop[])
        {
            let colorIndex = 0;
            let transIndex = 0;
            let stops: Rendering.Gradient.Stop[] = [];
            let prevColor: Rendering.Gradient.Stop = null;
            let prevTrans: Rendering.Gradient.Stop = null;
            let diff = 0;

            while (colorIndex < color.length && transIndex < trans.length)
            {
                // Find the difference between the stop offsets
                diff = color[colorIndex].offset - trans[transIndex].offset;

                if (diff === 0)
                {
                    // Add single stop with combined colour and opacity
                    let newColor = new RS.Util.Color(color[colorIndex].color.r, color[colorIndex].color.g, color[colorIndex].color.b, trans[transIndex].color.a);
                    stops.push({ offset: color[colorIndex].offset, color: newColor });
                    prevColor = color[colorIndex++];
                    prevTrans = trans[transIndex++];
                }
                else if (diff < 0)
                {
                    // Color is before transparancy - Interpolate opacity
                    let offset = color[colorIndex].offset - prevTrans.offset / (trans[transIndex].offset - prevTrans.offset);
                    let a = RS.Math.linearInterpolate(prevTrans.color.a, trans[transIndex].color.a, offset);
                    let newColor = new RS.Util.Color(color[colorIndex].color.r, color[colorIndex].color.g, color[colorIndex].color.b, a);
                    stops.push({ offset: color[colorIndex].offset, color: newColor });
                    prevColor = color[colorIndex++];
                }
                else
                {
                    // Trans is before Colour - Interpolate Color
                    let offset = trans[transIndex].offset - prevColor.offset / (color[colorIndex].offset - prevColor.offset);
                    let r = RS.Math.linearInterpolate(prevColor.color.r, color[colorIndex].color.r, offset);
                    let g = RS.Math.linearInterpolate(prevColor.color.g, color[colorIndex].color.g, offset);
                    let b = RS.Math.linearInterpolate(prevColor.color.b, color[colorIndex].color.b, offset);
                    let newColor = new RS.Util.Color(r, g, b, trans[transIndex].color.a);
                    stops.push({ offset: trans[transIndex].offset, color: newColor });
                    prevTrans = trans[transIndex++];
                }
            }

            return stops;
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string)
        {
            let map: Asset.GradientStopMap = {};

            const file = await Asset.ajax({ url }, Request.AJAXResponseType.Text);
            let json = JSON.parse(file);
            let definition = this.definition.raw as GradientAsset.Definition;
            let foreground = new RS.Util.Color(definition["foreground"] || "#ffffff");
            let background = new RS.Util.Color(definition["background"] || "#000000");

            let gradients = Object.getOwnPropertyNames(json);

            for (const gradient of gradients)
            {
                let colorStops: Rendering.Gradient.Stop[] = this.generateColorStops(json[gradient]["colourStops"], foreground, background);
                let transStops: Rendering.Gradient.Stop[] = this.generateTransStops(json[gradient]["transStops"]);

                let stops = this.interleaveStops(colorStops, transStops);

                map[gradient] = stops;
            }

            return map;
        }
    }
    Asset.GradientAsset = GradientAsset;

    namespace GradientAsset
    {
        export interface ColourStopColour
        {
            r: number;
            g: number;
            b: number;
        }

        export interface ColourStopInfo
        {
            "location": number;
            "midpoint": number;
            "type": "FrgC" | "BckC" | "UsrS";
            "color"?: ColourStopColour;
        }

        export interface TransStopInfo
        {
            "location": number;
            "midpoint": number;
            "opacity": number;
        }

        export interface GradientInfo
        {
            "name": string;
            "form": "CstS" | "ClNs";
            "interpolation"?: number;
            "colourStops"?: ColourStopInfo[];
            "transStops"?: TransStopInfo[];
            "showTrans"?: boolean;
            "vectorColor"?: boolean;
            "seed"?: number;
            "smooth"?: number;
            "colourSpace"?: "RGBC" | "HSBl" | "LbCl";
            "Mins"?: number[];
            "Maxs"?: number[];
        }

        export interface Definition
        {
            foreground: string;
            background: string;
        }
    }
}
