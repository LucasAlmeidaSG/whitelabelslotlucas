/** @internal  */
namespace RS.Rendering.Pixi
{
    export class Shader implements IDisposable, IShader
    {
        protected _isDisposed: boolean = false;

        protected _shaderProgram: ShaderProgram;
        protected _uniforms: Map<ShaderUniform>;

        /** Gets if this shader has been disposed or not. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets or sets the shader program associated with this shader. */
        public get shaderProgram() { return this._shaderProgram; }

        /** Gets a view of this shader's current uniforms. */
        public get uniforms(): Readonly<Map<ShaderUniform>> { return this._uniforms; }

        public constructor(shaderProgram: ShaderProgram)
        {
            this._shaderProgram = shaderProgram;
            this._uniforms = {};
            for (const name in shaderProgram.uniforms)
            {
                const descriptor = shaderProgram.uniforms[name];
                if (descriptor.array)
                {
                    this._uniforms[name] =
                    {
                        type: descriptor.type,
                        value: ShaderProgram.getDefaultValueFor(shaderProgram.uniforms[name].type),
                        innerType: descriptor.array.innerType,
                        length: descriptor.array.length
                    } as ArrayShaderUniform;
                }
                else
                {
                    this._uniforms[name] =
                    {
                        type: descriptor.type,
                        value: ShaderProgram.getDefaultValueFor(shaderProgram.uniforms[name].type)
                    } as ShaderUniform;
                }
            }
        }

        /**
         * Synchronises uniform data to the specified raw PIXI shader.
         * @param target 
         */
        public syncUniforms(target: PIXI.Shader, targetSize: RS.Math.Size2D, inputSize: RS.Math.Size2D, isRT: boolean)
        {
            ShaderProgram.applyUniforms(this._uniforms, target, targetSize, inputSize, isRT);
        }

        /**
         * Disposes this shader.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }
            
            this._isDisposed = true;
        }
    }

    RS.Rendering.Shader = Shader;
}
