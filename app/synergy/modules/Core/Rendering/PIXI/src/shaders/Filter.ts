/// <reference path="Shader.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    const tmpDims: RS.Dimensions = { w: 0, h: 0 };
    const tmpDims2: RS.Dimensions = { w: 0, h: 0 };

    const tmpPixiMatrix = new PIXI.Matrix();

    export class Filter extends PIXI.Filter<any> implements IFilter
    {
        protected _shader: Shader;

        public get shader() { return this._shader;  }

        public constructor(shader: Shader)
        {
            super();
            this._shader = shader;
        }

        /**
         * Applies this filter.
         * @param filterManager 
         * @param input 
         * @param output 
         * @param clear 
         * @param currentState 
         */
        public apply(filterManager: PIXI.FilterManager, input: PIXI.RenderTarget, output: PIXI.RenderTarget, clear?: boolean, currentState?: PIXI.FilterManagerStackItem): void
        {
            const renderer = filterManager.renderer as PIXI.WebGLRenderer;
            const shader = this._shader.shaderProgram.getRawShaderFor(renderer);
            renderer.bindShader(shader);
            tmpDims.w = output.size.width;
            tmpDims.h = output.size.height;
            tmpDims2.w = input.size.width;
            tmpDims2.h = input.size.height;
            if (currentState && currentState.target instanceof PIXI.DisplayObject)
            {
                if ("uTexCoordMatrix" in this._shader.shaderProgram.uniforms)
                {
                    const uTexCoordMatrix = this._shader.uniforms["uTexCoordMatrix"];
                    if (uTexCoordMatrix && uTexCoordMatrix.type === ShaderUniform.Type.Mat3x3)
                    {
                        filterManager.calculateNormalizedScreenSpaceMatrix(tmpPixiMatrix, currentState.sourceFrame, currentState.destinationFrame);
                        if (uTexCoordMatrix.value)
                        {
                            const m = uTexCoordMatrix.value;

                            m[0] = tmpPixiMatrix.a;
                            m[1] = tmpPixiMatrix.b;
                            m[2] = tmpPixiMatrix.tx;

                            m[3] = tmpPixiMatrix.c;
                            m[4] = tmpPixiMatrix.d;
                            m[5] = tmpPixiMatrix.ty;

                            m[6] = 0;
                            m[7] = 0;
                            m[8] = 1;
                        }
                        else
                        {
                            uTexCoordMatrix.value = Math.Matrix3([
                                tmpPixiMatrix.a, tmpPixiMatrix.b, tmpPixiMatrix.tx,
                                tmpPixiMatrix.c, tmpPixiMatrix.d, tmpPixiMatrix.ty,
                                0, 0, 1
                            ]);
                        }
                    }
                }
            }
            this._shader.syncUniforms(shader, tmpDims, tmpDims2, true);
            let textureCount = 0;
            for (const key in this._shader.uniforms)
            {
                const uniform = this._shader.uniforms[key];
                if (uniform.type === ShaderUniform.Type.Sampler2D && key !== "uSampler")
                {
                    const tex = uniform.value != null ? (uniform.value as Texture).baseTexture : null;
                    if (tex)
                    {
                        shader.uniforms[key] = renderer.bindTexture(tex, textureCount + 1);
                        ++textureCount;
                    }
                }
            }
            if (this.glShaders[renderer.CONTEXT_UID] == null)
            {
                filterManager.quad.initVao(shader);
                this.glShaders[renderer.CONTEXT_UID] = shader;
            }
            super.apply(filterManager, input, output, clear, currentState);
        }
    }

    RS.Rendering.Filter = Filter;
}
