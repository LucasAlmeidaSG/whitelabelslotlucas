/** @internal  */
namespace RS.Rendering.Pixi
{
    const excludeUniformsMap: Map<boolean> =
    {
        "projectionMatrix": true
    };

    export class ShaderProgram implements IShaderProgram
    {
        protected static _nextUID = 1;
        protected _uid: number = ShaderProgram._nextUID++;

        protected _isDisposed: boolean = false;

        protected _vertexSrc: string;
        protected _fragSrc: string;

        protected _uniforms: Map<ShaderUniform.Descriptor>;
        protected _defines: string;

        protected _rawShaders: PIXI.Shader[] = [];

        /** Gets if this shader has been disposed or not. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets the vertex source for this shader program. */
        public get vertexSource() { return this._vertexSrc; }

        /** Gets the fragment source for this shader program. */
        public get fragmentSource() { return this._fragSrc; }

        /** Gets a view of all uniforms and their types supported by this shader program. */
        public get uniforms() { return this._uniforms; }

        /** Gets a unique ID for this shader program. */
        public get uid() { return this._uid; }

        public constructor(settings: IShaderProgram.Settings)
        {
            // Acquire shader source
            this._vertexSrc = settings.vertexSource != null
                ? (Is.string(settings.vertexSource) ? settings.vertexSource : Asset.Store.getAsset(settings.vertexSource).asset as string)
                : Pixi.defaultVertexShader;
            this._fragSrc = Is.string(settings.fragmentSource) ? settings.fragmentSource : Asset.Store.getAsset(settings.fragmentSource).asset as string;

            // Assemble defines
            const definesArr: string[] = [];

            // Some device capabilities so we can compile appropriate code in the shader
            if (Device.isMobileDevice)
            {
                definesArr.push("#define MOBILE");
                if (RS.Device.isAppleDevice)
                {
                    if (IOS.isDeviceIOS)
                    {
                        definesArr.push("#define IOS");
                    }
                    if (IOS.isDeviceIPhone)
                    {
                        definesArr.push("#define IPHONE");
                    }
                    else
                    {
                        definesArr.push("#define IPAD");
                    }
                }
            }
            else
            {
                definesArr.push("#define DESKTOP");
            }
            definesArr.push(`#define BROWSER_${RS.Device.browser.toUpperCase()}`);
            if (settings.defines)
            {
                for (const key in settings.defines)
                {
                    const val = settings.defines[key];
                    if (val != null)
                    {
                        definesArr.push(`#define ${key} ${val}`);
                    }
                }
            }
            definesArr.push(`#line 1`);
            this._defines = definesArr.join("\n");

            if (this._fragSrc != null) { this._fragSrc = `${this._defines}\n${this._fragSrc}`; }
            if (this._vertexSrc != null) { this._vertexSrc = `${this._defines}\n${this._vertexSrc}`; }

            // Assemble uniforms
            this._uniforms =
            {
                ...ShaderProgram.parseUniforms(this._vertexSrc),
                ...ShaderProgram.parseUniforms(this._fragSrc)
            };

            // Check all varyings are used. Has to be done here because the "default vertex shader" exists at this level.
            const vertexVaryings = ShaderProgram.parseVaryings(this._vertexSrc);
            const fragVaryings = ShaderProgram.parseVaryings(this._fragSrc);
            for (const key in vertexVaryings)
            {
                if (!(key in fragVaryings))
                {
                    Log.warn(`Varying ${key} was present in vertex shader, but not in fragment shader. This may cause issues on Samsung Mali GPUs.`);
                }
            }
        }

        public static applyUniforms(uniforms: Map<ShaderUniform>, toShader: PIXI.Shader, targetSize: RS.Math.Size2D, inputSize: RS.Math.Size2D, isRT: boolean): void
        {
            for (const name in uniforms)
            {
                const value = uniforms[name];
                if (name in toShader.uniforms)
                {
                    toShader.uniforms[name] = this.processUniformData(value, targetSize, inputSize, isRT);
                }
            }
        }

        public static getDefaultValueFor(type: ShaderUniform.Type): any
        {
            switch (type)
            {
                case ShaderUniform.Type.Float:
                case ShaderUniform.Type.Int:
                    return 0.0;
                case ShaderUniform.Type.Bool:
                    return false;
                case ShaderUniform.Type.Vec2:
                case ShaderUniform.Type.IVec2:
                    return Math.Vector2D();
                case ShaderUniform.Type.Vec3:
                case ShaderUniform.Type.IVec3:
                    return Math.Vector3D();
                case ShaderUniform.Type.Vec4:
                case ShaderUniform.Type.IVec4:
                    return Math.Vector4D();
                case ShaderUniform.Type.Mat2x2:
                    return [1.0, 0.0, 0.0, 1.0];
                case ShaderUniform.Type.Mat3x3:
                    return Math.Matrix3.identity;
                case ShaderUniform.Type.Mat4x4:
                    return Math.Matrix4.identity;
                case ShaderUniform.Type.Array:
                    return [];
                default:
                    return null;
            }
        }

        private static resolveInputSizeOp(uniform: ShaderUniform, isRT: boolean): ShaderUniform.Op
        {
            if (isRT && uniform.inputSizeOpRT != null)
            {
                return uniform.inputSizeOpRT;
            }
            else if (uniform.inputSizeOp != null)
            {
                return uniform.inputSizeOp;
            }
            else
            {
                return ShaderUniform.Op.None;
            }
        }

        private static resolveTargetSizeOp(uniform: ShaderUniform, isRT: boolean): ShaderUniform.Op
        {
            if (isRT && uniform.targetSizeOpRT != null)
            {
                return uniform.targetSizeOpRT;
            }
            else if (uniform.targetSizeOp != null)
            {
                return uniform.targetSizeOp;
            }
            else
            {
                return ShaderUniform.Op.None;
            }
        }

        private static processUniformData(uniform: ShaderUniform, targetSize: RS.Math.Size2D, inputSize: RS.Math.Size2D, isRT: boolean): any | null
        {
            switch (uniform.type)
            {
                case ShaderUniform.Type.Bool:
                    return uniform.value;
                case ShaderUniform.Type.Sampler2D:
                    {
                        if (Is.assetReference(uniform.value))
                        {
                            const asset = RS.Asset.Store.getAsset(uniform.value);
                            if (asset instanceof RS.Asset.ImageAsset)
                            {
                                return asset.asset;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return uniform.value;
                        }
                    }
                case ShaderUniform.Type.Float:
                    {
                        let val = uniform.value;
                        val = this.applyOpScalar(val, this.resolveTargetSizeOp(uniform, isRT), targetSize);
                        val = this.applyOpScalar(val, this.resolveInputSizeOp(uniform, isRT), inputSize);
                        return val;
                    }
                case ShaderUniform.Type.Vec2:
                    {
                        const val = [uniform.value.x, uniform.value.y];
                        this.applyOpVector(val, 0, 2, this.resolveTargetSizeOp(uniform, isRT), targetSize);
                        this.applyOpVector(val, 0, 2, this.resolveInputSizeOp(uniform, isRT), inputSize);
                        return val;
                    }
                case ShaderUniform.Type.Vec3:
                    {
                        const val = [uniform.value.x, uniform.value.y, uniform.value.z];
                        this.applyOpVector(val, 0, 3, this.resolveTargetSizeOp(uniform, isRT), targetSize);
                        this.applyOpVector(val, 0, 3, this.resolveInputSizeOp(uniform, isRT), inputSize);
                        return val;
                    }
                case ShaderUniform.Type.Vec4:
                    {
                        const val = [uniform.value.x, uniform.value.y, uniform.value.z, uniform.value.w];
                        this.applyOpVector(val, 0, 4, this.resolveTargetSizeOp(uniform, isRT), targetSize);
                        this.applyOpVector(val, 0, 4, this.resolveInputSizeOp(uniform, isRT), inputSize);
                        return val;
                    }

                case ShaderUniform.Type.Mat2x2:
                case ShaderUniform.Type.Mat2x3:
                case ShaderUniform.Type.Mat3x4:
                    {
                        return uniform.value;
                    }
                case ShaderUniform.Type.Mat3x3:
                    {
                        const arr = new Array<number>(9);
                        Math.Matrix3.write(uniform.value, arr, 0, false);
                        return arr;
                    }
                case ShaderUniform.Type.Mat4x4:
                    {
                        const arr = new Array<number>(16);
                        Math.Matrix4.write(uniform.value, arr, 0, false);
                        return arr;
                    }

                case ShaderUniform.Type.Array:
                    {
                        return this.processArrayUniformData(uniform, targetSize, inputSize, isRT);
                    }

                default:
                    debugger;
                    break;
            }
        }

        private static processArrayUniformData(uniform: ArrayShaderUniform, targetSize: RS.Math.Size2D, inputSize: RS.Math.Size2D, isRT: boolean): any
        {
            const l = Is.number(uniform.length) ? uniform.length : uniform.value.length;
            let elSz: number;
            let isFloat: boolean;
            switch (uniform.innerType)
            {
                case ShaderUniform.Type.Sampler2D:
                    // Handled elsewhere
                    return null;
                case ShaderUniform.Type.Float: elSz = 1; isFloat = true; break;
                case ShaderUniform.Type.Vec2: elSz = 2; isFloat = true; break;
                case ShaderUniform.Type.Vec3: elSz = 3; isFloat = true; break;
                case ShaderUniform.Type.Vec4: elSz = 4; isFloat = true; break;
                case ShaderUniform.Type.Mat2x2: elSz = 4; isFloat = true; break;
                case ShaderUniform.Type.Mat3x3: elSz = 9; isFloat = true; break;
                case ShaderUniform.Type.Mat4x4: elSz = 16; isFloat = true; break;

                case ShaderUniform.Type.Int: elSz = 1; isFloat = false; break;
                case ShaderUniform.Type.IVec2: elSz = 2; isFloat = false; break;
                case ShaderUniform.Type.IVec3: elSz = 3; isFloat = false; break;
                case ShaderUniform.Type.IVec4: elSz = 4; isFloat = false; break;

                case ShaderUniform.Type.Bool: elSz = 1; isFloat = false; break;

                default:
                    debugger;
                    break;
            }

            const arr = isFloat ? new Float32Array(l * elSz) : new Int32Array(l * elSz);
            for (let i = 0; i < l; ++i)
            {
                // TypeScript appears to have gotten very confused with this type narrowing
                // We've had to typecast to any inside this switch to get around it
                switch (uniform.innerType)
                {
                    case ShaderUniform.Type.Float:
                    case ShaderUniform.Type.Int:
                    case ShaderUniform.Type.Bool:
                        {
                            const val = uniform.value as any as number[];
                            arr[i] = val[i];
                            break;
                        }
                    case ShaderUniform.Type.Vec2:
                    case ShaderUniform.Type.IVec2:
                        {
                            const val = uniform.value as any as Math.Vector2D[];
                            arr[i * elSz + 0] = val[i].x;
                            arr[i * elSz + 1] = val[i].y;
                            break;
                        }
                    case ShaderUniform.Type.Vec3:
                    case ShaderUniform.Type.IVec3:
                        {
                            const val = uniform.value as any as Math.Vector3D[];
                            arr[i * elSz + 0] = val[i].x;
                            arr[i * elSz + 1] = val[i].y;
                            arr[i * elSz + 2] = val[i].z;
                            break;
                        }
                    case ShaderUniform.Type.Vec4:
                    case ShaderUniform.Type.IVec4:
                        {
                            const val = uniform.value as any as Math.Vector4D[];
                            arr[i * elSz + 0] = val[i].x;
                            arr[i * elSz + 1] = val[i].y;
                            arr[i * elSz + 2] = val[i].z;
                            arr[i * elSz + 3] = val[i].w;
                            break;
                        }
                    case ShaderUniform.Type.Mat2x2:
                    case ShaderUniform.Type.Mat3x3:
                    case ShaderUniform.Type.Mat4x4:
                        for (let j = 0, l2 = uniform.value.length; j < l2; ++j)
                        {
                            arr[i * elSz + j] = uniform.value[i][j];
                        }
                        break;
                }
                this.applyOpVector(arr, i * elSz, elSz, this.resolveTargetSizeOp(uniform, isRT), targetSize);
                this.applyOpVector(arr, i * elSz, elSz, this.resolveInputSizeOp(uniform, isRT), inputSize);
            }
            return arr;
        }

        private static createRegExpForValueDeclaration(name: string)
        {
            /** Matches a value/type identifier */
            const identifier = "([a-zA-Z0-9_]+)";
            /** Matches an array length e.g. [5] */
            const arrayLength = "(\\[.+\\])";
            // Match a name followed by two identifiers and an optional array length
            return new RegExp(`${name}\\s+${identifier}\\s+${identifier}\\s*${arrayLength}?;`, "g");
        }

        private static parseValueDeclaration(shader: string, name: string): Map<ShaderUniform.Descriptor>
        {
            const map: Map<ShaderUniform.Descriptor> = {};
            const regex = this.createRegExpForValueDeclaration(name);
            let match: RegExpMatchArray = regex.exec(shader);
            while (match)
            {
                const type = this.parseType(match[1]);
                const identifier = match[2];
                if (type == null)
                {
                    Log.warn(`Unknown uniform type '${type}' (${identifier})`);
                }
                else if (!excludeUniformsMap[identifier])
                {
                    if (match[3])
                    {
                        // const arrayLen = parseInt(/[0-9]+/g.exec(match[3])[0]);
                        const arrayLenRaw = match[3].substr(1, match[3].length - 2);
                        const arrayLen = parseInt(arrayLenRaw);
                        map[identifier] =
                            {
                                type: ShaderUniform.Type.Array,
                                array:
                                {
                                    innerType: type,
                                    length: arrayLen
                                }
                            };
                    }
                    else
                    {
                        map[identifier] = { type };
                    }
                }
                match = regex.exec(shader);
            }
            return map;
        }

        private static parseVaryings(shader: string): Map<ShaderUniform.Descriptor>
        {
            return this.parseValueDeclaration(shader, "varying");
        }

        private static parseUniforms(shader: string): Map<ShaderUniform.Descriptor>
        {
            return this.parseValueDeclaration(shader, "uniform");
        }

        private static parseType(type: string): ShaderUniform.Type | null
        {
            switch (type)
            {
                case "float": return ShaderUniform.Type.Float;
                case "vec2": return ShaderUniform.Type.Vec2;
                case "vec3": return ShaderUniform.Type.Vec3;
                case "vec4": return ShaderUniform.Type.Vec4;

                case "int": return ShaderUniform.Type.Int;
                case "ivec2": return ShaderUniform.Type.IVec2;
                case "ivec3": return ShaderUniform.Type.IVec3;
                case "ivec4": return ShaderUniform.Type.IVec4;

                case "bool": return ShaderUniform.Type.Bool;
                // case "bvec2": return ShaderUniform.Type.IVec2;
                // case "bvec3": return ShaderUniform.Type.IVec3;
                // case "bvec4": return ShaderUniform.Type.IVec4;

                // case "sampler1D": return ShaderUniform.Type.Sampler2D;
                case "sampler2D": return ShaderUniform.Type.Sampler2D;
                // case "sampler3D": return ShaderUniform.Type.Sampler2D;

                case "mat2": return ShaderUniform.Type.Mat2x2;
                case "mat3": return ShaderUniform.Type.Mat3x3;
                case "mat4": return ShaderUniform.Type.Mat4x4;

                default: return null;
            }
        }

        private static applyOpVector(arr: number[] | Float32Array | Int32Array, offset: number, count: number, op: ShaderUniform.Op, size: Math.Size2D): void
        {
            switch (op)
            {
                case ShaderUniform.Op.Multiply:
                    arr[offset] *= size.w;
                    if (count > 1) { arr[offset + 1] *= size.h; }
                    break;
                case ShaderUniform.Op.Divide:
                    arr[offset] /= size.w;
                    if (count > 1) { arr[1] /= size.h; }
                    break;
                case ShaderUniform.Op.Replace:
                    arr[offset] = size.w;
                    if (arr.length > 1) { arr[1] = size.h; }
                    break;
                case ShaderUniform.Op.InverseX:
                    arr[offset] = size.w - arr[offset];
                    break;
                case ShaderUniform.Op.InverseY:
                    if (count > 1) { count[offset + 1] = size.h - count[offset + 1]; }
                    break;
                case ShaderUniform.Op.InverseXAll:
                    for (let i = offset, l = offset + count; i < l; ++i)
                    {
                        arr[i] = size.w - arr[i];
                    }
                    break;
                case ShaderUniform.Op.InverseYAll:
                    for (let i = offset, l = offset + count; i < l; ++i)
                    {
                        arr[i] = size.h - arr[i];
                    }
                    break;
            }
        }

        private static applyOpScalar(data: number, op: ShaderUniform.Op, size: Math.Size2D): number
        {
            switch (op)
            {
                case ShaderUniform.Op.Multiply:
                    data *= size.w;
                case ShaderUniform.Op.Divide:
                    data /= size.w;
                    break;
                case ShaderUniform.Op.Replace:
                    data = size.w;
                    break;
                case ShaderUniform.Op.InverseX:
                    data = size.w - data;
                    break;
                case ShaderUniform.Op.InverseY:
                    break;
                case ShaderUniform.Op.InverseXAll:
                    data = size.w - data;
                case ShaderUniform.Op.InverseYAll:
                    data = size.h - data;
            }
            return data;
        }

        private static getCompilationError(gl: WebGLRenderingContext, type: number, src: string): string | null
        {
            let result: string | null = null;

            const shader = gl.createShader(type);
        
            gl.shaderSource(shader, src);
            gl.compileShader(shader);
        
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            {
                result = gl.getShaderInfoLog(shader) || null;
            }

            gl.deleteShader(shader);

            return result;
        }

        /** Creates a shader instance from this shader program. */
        public createShader(): IShader
        {
            return new Shader(this);
        }

        /**
         * Gets a raw PIXI shader for the specified renderer.
         * @param renderer
         */
        public getRawShaderFor(renderer: PIXI.WebGLRenderer): PIXI.Shader
        {
            const glID = renderer.CONTEXT_UID;
            let shader: PIXI.Shader = this._rawShaders[glID];
            if (shader == null)
            {
                const vertexSrc = this._vertexSrc || Pixi.defaultVertexShader;
                try
                {
                    shader = new PIXI.Shader(renderer.gl, vertexSrc, this._fragSrc);
                }
                catch (err)
                {
                    const vertexError = ShaderProgram.getCompilationError(renderer.gl, renderer.gl.VERTEX_SHADER, vertexSrc);
                    if (vertexError != null) { throw new Error(`Vertex shader compilation failed\n${vertexError}`); }

                    const fragmentError = ShaderProgram.getCompilationError(renderer.gl, renderer.gl.FRAGMENT_SHADER, this._fragSrc);
                    if (fragmentError != null) { throw new Error(`Fragment shader compilation failed\n${fragmentError}`); }

                    throw err;
                }

                renderer.bindShader(shader);
                this._rawShaders[glID] = shader;
            }
            return shader;
        }

        /**
         * Compiles the shader program on the target stage if it hasn't already been compiled.
         * @param stage
         */
        public warmup(stage: IStage): void
        {
            const renderer = (stage as Stage).renderer;
            if (!(renderer instanceof PIXI.WebGLRenderer)) { return; }
            this.getRawShaderFor(renderer);
        }

        /**
         * Disposes this shader.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }

            this._isDisposed = true;
        }
    }

    RS.Rendering.ShaderProgram = ShaderProgram;
}
