/// <reference path="LightingUtils.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    /**
     * Encapsulates a forward lighting engine that serves a specific shader type.
     */
    export interface IForwardLightingEngine<TShaderType extends RS.Rendering.IShader>
    {
        /**
         * Prepares and returns a shader for the specified light data and switches.
         * The shader will have lighting data set, but will need material parameters and matrices set.
         * @param lightData 
         * @param switches 
         */
        prepare(lightData: LightData[], switches: IForwardLightingEngine.Switches): TShaderType;
    }

    export namespace IForwardLightingEngine
    {
        export interface Switches
        {
            tangentSpaceNormals: boolean;
            specular: boolean;
            reflections: boolean;            
            vertexBased: boolean;
            vertexColors: boolean;
            withDiffuseMap: boolean;
            withNormalMap: boolean;
            withSpecularMap: boolean;
            withAmbientMap: boolean;
            withEnvironmentMap: boolean;
            blend: Scene.Material.Blend;
        }

        export namespace Switches
        {
            export function hash(switches: Switches): number
            {
                let value = 0;
                value = (value << 1) | (switches.tangentSpaceNormals ? 1 : 0);
                value = (value << 1) | (switches.specular ? 1 : 0);
                value = (value << 1) | (switches.reflections ? 1 : 0);
                value = (value << 1) | (switches.vertexBased ? 1 : 0);
                value = (value << 1) | (switches.vertexColors ? 1 : 0);
                value = (value << 1) | (switches.withDiffuseMap ? 1 : 0);
                value = (value << 1) | (switches.withNormalMap ? 1 : 0);
                value = (value << 1) | (switches.withSpecularMap ? 1 : 0);
                value = (value << 1) | (switches.withAmbientMap ? 1 : 0);
                value = (value << 1) | (switches.withEnvironmentMap ? 1 : 0);
                value = (value << 3) | (switches.blend);
                return value;
            }

            export function toString(switches: Switches): string
            {
                return `tangentSpaceNormals=${switches.tangentSpaceNormals},specular=${switches.specular},reflections=${switches.reflections},vertexBased=${switches.vertexBased},vertexColors=${switches.vertexColors},diffuseMap=${switches.withDiffuseMap},normalMap=${switches.withNormalMap},specularMap=${switches.withSpecularMap},ambientMap=${switches.withAmbientMap},envMap=${switches.withEnvironmentMap},blend=${Scene.Material.Blend[switches.blend]}`;
            }
        }
    }
}