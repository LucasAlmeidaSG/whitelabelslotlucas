/// <reference path="SceneObject.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpMatrix = Math.Matrix4();

    @HasCallbacks
    export class MeshObject extends SceneObject implements Scene.IMeshObject
    {
        protected _geometry: Geometry | null;
        protected _materials: Scene.Material[] | null;
        protected _hasLitMaterial: boolean = false;
        protected _tint: RS.Util.Color | null = null;

        /** Gets or sets the geometry of this object. */
        public get geometry() { return this._geometry; }
        public set geometry(value)
        {
            this._geometry = value;
            if (value != null)
            {
                Math.AABB3D.copy(value.bounds, this._localBounds);
                this._worldTransformDirty = true;
            }
        }

        /** Gets or sets the materials for this object (1 per index binding of the geometry). */
        public get materials() { return this._materials; }
        public set materials(value)
        {
            this._materials = value;
            this._hasLitMaterial = false;
            if (!value) { return; }
            for (const mat of value)
            {
                if (mat.model === Scene.Material.Model.LitPBR || mat.model === Scene.Material.Model.LitSpec)
                {
                    this._hasLitMaterial = true;
                    break;
                }
            }
        }

        /** Gets or sets the color tint for this object. */
        public get tint() { return this._tint; }
        public set tint(value) { this._tint = value; }

        public constructor();

        public constructor(geometry: Geometry, materials: Scene.Material[]);

        public constructor(modelReference: RS.Asset.ModelReference, materials: Scene.Material[]);

        public constructor(p1?: Geometry | RS.Asset.ModelReference, materials?: Scene.Material[])
        {
            super();
            if (p1 instanceof Geometry)
            {
                this.geometry = p1;
            }
            else if (p1 != null)
            {
                this.geometry = RS.Asset.Store.getAsset(p1).as(Geometry);
            }
            this.materials = materials;
        }

        public render(ctx: RenderContext): void
        {
            super.render(ctx);

            // Check visible
            if (!this._visible) { return; }

            // Only render if we have geometry and materials
            if (!this._geometry || !this._materials) { return; }

            // Cache local data
            const worldMatrix = this.getWorldTransform(tmpMatrix);
            const worldBounds = this.worldBounds;

            // Iterate each material and draw
            const flip = (Math.sign(this.scale.x) * Math.sign(this.scale.y) * Math.sign(this.scale.z)) < 0;
            for (let i = 0, l = Math.min(this._materials.length, this._geometry.indexBindings.length); i < l; ++i)
            {
                ctx.addGeometry(worldMatrix, worldBounds, this._geometry, this._materials[i], this._tint || RS.Util.Colors.white, i, flip);
            }
        }
    }

    Scene.MeshObject = MeshObject;
}