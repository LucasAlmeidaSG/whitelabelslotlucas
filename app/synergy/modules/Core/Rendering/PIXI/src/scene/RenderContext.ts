/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpLights: LightSource[] = [];
    const tmpMatrix1 = Math.Matrix4();
    const tmpVec1 = Math.Vector3D();
    const projShadowMat: Scene.Material =
    {
        model: Scene.Material.Model.Unlit,
        diffuseTint: RS.Util.Colors.black,
        blend: Scene.Material.Blend.Opaque,
        vertexColors: false
    };

    function isTransparent(mat: Scene.Material): boolean
    {
        return mat.blend === Scene.Material.Blend.Transparent || mat.blend === Scene.Material.Blend.TransparentAdd || mat.blend === Scene.Material.Blend.TransparentMult;
    }

    /**
     * Responsible for gathering all objects to draw from a single viewpoint.
     */
    @RS.HasCallbacks
    export class RenderContext implements IDisposable
    {
        protected _projectionMatrix = Math.Matrix4();
        protected _viewMatrix = Math.Matrix4();
        protected _viewPosition = Math.Vector3D();

        protected _geometryQueueOpaque: RenderContext.GeometryItem[] = [];
        protected _geometryQueueTrans: RenderContext.GeometryItem[] = [];
        protected _geometryCountOpaque: number = 0;
        protected _geometryCountTrans: number = 0;
        protected _geometrySorted: boolean = false;

        protected _lightQueue: RenderContext.LightItem[] = [];
        protected _lightCount: number = 0;

        protected _isDisposed: boolean = false;

        /** Gets if this object has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets a view of the light sources currently within this context. */
        public get lightSources(): ReadonlyArray<LightSource>
        {
            tmpLights.length = this._lightCount;
            for (let i = 0, l = this._lightCount; i < l; ++i)
            {
                tmpLights[i] = this._lightQueue[i].lightSource;
            }
            return tmpLights;
        }

        /**
         * Starts a new draw.
         * @param projectionMatrix 
         * @param viewMatrix 
         * @param viewPosition 
         */
        public begin(projectionMatrix: Math.ReadonlyMatrix4, viewMatrix: Math.ReadonlyMatrix4, viewPosition: Math.ReadonlyVector3D): void
        {
            Math.Matrix4.copy(projectionMatrix, this._projectionMatrix);
            Math.Matrix4.copy(viewMatrix, this._viewMatrix);
            Math.Vector3D.copy(viewPosition, this._viewPosition);
            this.clear(true);
        }

        /**
         * Dispatches a lit-forward draw pass to the scene renderer.
         */
        public dispatchLitForward(renderer: PIXI.WebGLRenderer, opaques = true, transparents = true): void
        {
            this.sortGeometry();
            const sceneRenderer = ISceneRenderer.get();

            // Iterate all geometry to draw
            if (opaques)
            {
                for (let i = 0, l = this._geometryCountOpaque; i < l; ++i)
                {
                    const item = this._geometryQueueOpaque[i];
                    this.gatherLights(item, tmpLights);
                    sceneRenderer.render(renderer, ScenePass.LitForward, this._projectionMatrix, this._viewMatrix, item.worldMatrix, this._viewPosition, item.geometry, item.material, item.tint, item.indexBinding, tmpLights, item.flip);
                }
            }
            if (transparents)
            {
                for (let i = 0, l = this._geometryCountTrans; i < l; ++i)
                {
                    const item = this._geometryQueueTrans[i];
                    this.gatherLights(item, tmpLights);
                    sceneRenderer.render(renderer, ScenePass.LitForward, this._projectionMatrix, this._viewMatrix, item.worldMatrix, this._viewPosition, item.geometry, item.material, item.tint, item.indexBinding, tmpLights, item.flip);
                }
            }
        }

        /**
         * Dispatches a projected shadow draw pass for a specific light source to the scene renderer.
         */
        public dispatchProjectedShadow(renderer: PIXI.WebGLRenderer, lightSource: LightSource): void
        {
            this.sortGeometry();

            // Cache shadow matrix
            const shadow = lightSource.shadow;
            if (!shadow || shadow.type !== Scene.ILightSource.ShadowType.Projected) { return; }
            const shadowMatrix = lightSource.getShadowMatrix(tmpMatrix1);
            const sceneRenderer = ISceneRenderer.get();
            const wb = lightSource.worldBounds;
            const omnipresent = Math.AABB3D.isInfinite(wb);

            // Iterate all geometry to draw
            for (let i = 0, l = this._geometryCountOpaque; i < l; ++i)
            {
                const item = this._geometryQueueOpaque[i];
                if (item.material.castShadows ? item.material.castShadows : item.material.model !== Scene.Material.Model.Unlit)
                {
                    if (omnipresent || Math.AABB3D.intersects(item.worldBounds, wb))
                    {
                        const modifiedWorldMatrix = Math.Matrix4.mul(shadowMatrix, item.worldMatrix);
                        projShadowMat.diffuseTint = shadow.color;
                        sceneRenderer.render(renderer, ScenePass.SimpleProjectedShadow, this._projectionMatrix, this._viewMatrix, modifiedWorldMatrix, this._viewPosition, item.geometry, projShadowMat, item.tint, item.indexBinding, null, item.flip);
                    }
                }
            }
            for (let i = 0, l = this._geometryCountTrans; i < l; ++i)
            {
                const item = this._geometryQueueTrans[i];
                if (item.material.castShadows ? item.material.castShadows : item.material.model !== Scene.Material.Model.Unlit)
                {
                    if (omnipresent || Math.AABB3D.intersects(item.worldBounds, wb))
                    {
                        const modifiedWorldMatrix = Math.Matrix4.mul(shadowMatrix, item.worldMatrix);
                        projShadowMat.diffuseTint = shadow.color;
                        sceneRenderer.render(renderer, ScenePass.SimpleProjectedShadow, this._projectionMatrix, this._viewMatrix, modifiedWorldMatrix, this._viewPosition, item.geometry, projShadowMat, item.tint, item.indexBinding, null, item.flip);
                    }
                }
            }
        }

        /** Disposes this render context. */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this.clear(false);
            this._geometryQueueOpaque = null;
            this._geometryQueueTrans = null;
            this._lightQueue = null;
            this._projectionMatrix = null;
            this._viewMatrix = null;
            this._viewPosition = null;
            this._isDisposed = true;
        }

        /**
         * Adds geometry to render to this context.
         * @param worldMatrix 
         * @param worldBounds 
         * @param geometry 
         * @param material 
         * @param indexBinding 
         */
        public addGeometry(worldMatrix: Math.ReadonlyMatrix4, worldBounds: Math.ReadonlyAABB3D, geometry: Geometry, material: Scene.Material, tint: RS.Util.Color, indexBinding: number, flip?: boolean): void
        {
            if (!isTransparent(material))
            {
                ++this._geometryCountOpaque;
                if (this._geometryCountOpaque > this._geometryQueueOpaque.length)
                {
                    this._geometryQueueOpaque.push({
                        worldMatrix: Math.Matrix4.clone(worldMatrix),
                        worldBounds: Math.AABB3D.clone(worldBounds),
                        geometry, material, indexBinding, flip: flip || false,
                        tint: tint.clone(),
                        zDepth: 0
                    });
                }
                else
                {
                    const item = this._geometryQueueOpaque[this._geometryCountOpaque - 1];
                    Math.Matrix4.copy(worldMatrix, item.worldMatrix);
                    Math.AABB3D.copy(worldBounds, item.worldBounds);
                    item.geometry = geometry;
                    item.material = material;
                    item.tint.copy(tint);
                    item.indexBinding = indexBinding;
                    item.flip = flip || false;
                    item.zDepth = 0;
                }
            }
            else
            {
                ++this._geometryCountTrans;
                if (this._geometryCountTrans > this._geometryQueueTrans.length)
                {
                    this._geometryQueueTrans.push({
                        worldMatrix: Math.Matrix4.clone(worldMatrix),
                        worldBounds: Math.AABB3D.clone(worldBounds),
                        geometry, material, indexBinding, flip: flip || false,
                        tint: tint.clone(),
                        zDepth: this.getZDepth(worldMatrix)
                    });
                }
                else
                {
                    const item = this._geometryQueueTrans[this._geometryCountTrans - 1];
                    Math.Matrix4.copy(worldMatrix, item.worldMatrix);
                    Math.AABB3D.copy(worldBounds, item.worldBounds);
                    item.geometry = geometry;
                    item.material = material;
                    item.tint.copy(tint);
                    item.indexBinding = indexBinding;
                    item.flip = flip || false;
                    item.zDepth = this.getZDepth(worldMatrix);
                }
            }
            this._geometrySorted = false;
        }

        /**
         * Adds a light source to render to this context.
         * @param worldBounds 
         * @param lightSource 
         */
        public addLight(worldBounds: Math.ReadonlyAABB3D, lightSource: LightSource): void
        {
            ++this._lightCount;
            const omnipresent = Math.AABB3D.isInfinite(worldBounds);
            if (this._lightCount > this._lightQueue.length)
            {
                this._lightQueue.push({
                    worldBounds: Math.AABB3D.clone(worldBounds),
                    omnipresent, lightSource
                });
            }
            else
            {
                const item = this._lightQueue[this._lightCount - 1];
                Math.AABB3D.copy(worldBounds, item.worldBounds);
                item.omnipresent = omnipresent;
                item.lightSource = lightSource;
            }
        }

        /**
         * Gathers all lights that affect the specified geometry item.
         * @param geometryItem 
         * @param out 
         */
        protected gatherLights(geometryItem: RenderContext.GeometryItem, out: LightSource[]): void
        {
            // Search all lights
            let outCount = 0;
            for (let i = 0, l = this._lightCount; i < l; ++i)
            {
                const item = this._lightQueue[i];
                if (item.omnipresent || Math.AABB3D.intersects(geometryItem.worldBounds, item.worldBounds))
                {
                    out[outCount] = item.lightSource;
                    ++outCount;
                }
            }
            out.length = outCount;
        }

        /**
         * Clears the render context ready for a new draw.
         * @param fast 
         */
        protected clear(fast: boolean = true): void
        {
            this._geometryCountOpaque = 0;
            this._geometryCountTrans = 0;
            this._lightCount = 0;
            if (!fast)
            {
                this._geometryQueueOpaque.length = 0;
                this._geometryQueueTrans.length = 0;
                this._lightQueue.length = 0;
            }
            this._geometrySorted = false;
        }

        protected getZDepth(worldMatrix: Math.ReadonlyMatrix4): number
        {
            tmpVec1.x = worldMatrix[12] - this._viewPosition.x;
            tmpVec1.y = worldMatrix[13] - this._viewPosition.y;
            tmpVec1.z = worldMatrix[14] - this._viewPosition.z;
            return Math.Vector3D.sizeSq(tmpVec1);
        }

        protected sortGeometry(): void
        {
            if (this._geometrySorted) { return; }
            this._geometryQueueTrans.length = this._geometryCountTrans;
            this._geometryQueueTrans.sort(this.transparentSortFunc);
            this._geometrySorted = true;
        }

        @RS.Callback
        protected transparentSortFunc(a: RenderContext.GeometryItem, b: RenderContext.GeometryItem): number
        {
            return b.zDepth - a.zDepth;
        }
    }

    export namespace RenderContext
    {
        export interface GeometryItem
        {
            worldMatrix: Math.Matrix4;
            worldBounds: Math.AABB3D;
            geometry: Geometry;
            material: Scene.Material;
            tint: Util.Color;
            indexBinding: number;
            flip: boolean;
            zDepth: number;
        }

        export interface LightItem
        {
            worldBounds: Math.AABB3D;
            omnipresent: boolean;
            lightSource: LightSource;
        }
    }
}