/// <reference path="SceneObject.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpMatrix = Math.Matrix4();

    @HasCallbacks
    export abstract class LightSource extends SceneObject implements Scene.ILightSource
    {
        protected _color: Util.Color;
        protected _intensity: number;
        protected _shadow: Scene.ILightSource.Shadow | null = null;

        /** Gets or sets the color of this light. */
        public get color() { return this._color; }
        public set color(value) { this._color = value; }

        /** Gets or sets the intensity of this light. */
        public get intensity() { return this._intensity; }
        public set intensity(value) { this._intensity = value; }

        /** Gets or sets settings for the shadow that this light should cast. */
        public get shadow() { return this._shadow; }
        public set shadow(value) { this._shadow = value; }

        public constructor();

        public constructor(color: Util.Color, intensity: number);

        public constructor(p1?: Util.Color, p2?: number)
        {
            super();
            if (p1)
            {
                this.color = p1;
                this.intensity = p2;
            }
            else
            {
                this.color = Util.Colors.white;
                this.intensity = 1.0;
            }
        }

        public render(ctx: RenderContext): void
        {
            super.render(ctx);

            // Check visible
            if (!this._visible) { return; }

            // Add to draw
            ctx.addLight(this.worldBounds, this);
        }

        /**
         * Gets the shadow matrix for this light.
         * The actual meaning of the matrix will depend on the shadow type selected in the settings.
         * @param out 
         */
        public abstract getShadowMatrix(out?: Math.Matrix4): Math.Matrix4;
    }
}