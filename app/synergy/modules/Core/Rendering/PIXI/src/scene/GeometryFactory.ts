/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const defaultUVRect = new ReadonlyRectangle(0.0, 0.0, 1.0, 1.0);

    Scene.Geometry.createGeometry = function (vertexCount: number, posBuffer: Float32Array, normalBuffer: Float32Array, colorBuffer: Uint8Array, uvBuffer: Float32Array, bindingBuffer: Uint16Array, bindingType: Scene.PrimitiveType): Geometry
    {
        return createGeometry(vertexCount, posBuffer, normalBuffer, colorBuffer, uvBuffer, bindingBuffer, bindingType);
    }
    Scene.Geometry.createCube = function (size?: Math.ReadonlyVector3D): Geometry
    {
        return createCube(size || Math.Vector3D(1, 1, 1), false);
    };

    Scene.Geometry.createCubeOutline = function (size?: Math.ReadonlyVector3D): Geometry
    {
        return createCube(size || Math.Vector3D(1, 1, 1), true);
    };

    Scene.Geometry.createPlane = function (size?: Math.ReadonlyVector2D, uvRect?: ReadonlyRectangle): Geometry
    {
        return createPlane(size || Math.Vector2D(1, 1), false, uvRect || defaultUVRect);
    };

    Scene.Geometry.createCircle = function (radius?: number, divisions?: number, uvRadius?: number): Geometry
    {
        return createCircle(radius != null ? radius : 0.5, divisions != null ? divisions : 12, uvRadius != null ? uvRadius : 1, false);
    };

    Scene.Geometry.createCircleOutline = function (radius?: number, divisions?: number, uvRadius?: number): Geometry
    {
        return createCircle(radius != null ? radius : 0.5, divisions != null ? divisions : 12, uvRadius != null ? uvRadius : 1, true);
    };

    Scene.Geometry.createPlaneOutline = function (size?: Math.ReadonlyVector2D): Geometry
    {
        return createPlane(size || Math.Vector2D(1, 1), true, defaultUVRect);
    };

    Scene.Geometry.createSphere = function (radius?: number, divisions?: number): Geometry
    {
        return createSphere(radius != null ? radius : 0.5, divisions != null ? divisions : 9, false);
    };

    Scene.Geometry.createSphereOutline = function (radius?: number, divisions?: number): Geometry
    {
        return createSphere(radius != null ? radius : 0.5, divisions != null ? divisions : 9, true);
    };

    Scene.Geometry.createCylinder = function (radius?: number, divisions?: number, height?: number, uvRadius?: number): Geometry
    {
        return createCylinder(radius != null ? radius : 0.5, divisions != null ? divisions : 12, height != null ? height : 1, uvRadius != null ? uvRadius : 1, false);
    };

    Scene.Geometry.createCylinderOutline = function (radius?: number, divisions?: number, height?: number, uvRadius?: number): Geometry
    {
        return createCylinder(radius != null ? radius : 0.5, divisions != null ? divisions : 12, height != null ? height : 1, uvRadius != null ? uvRadius : 1, true);
    };

    Scene.Geometry.createFullScreenQuad = function (): Geometry
    {
        return createFSQuad();
    };

    function createGeometry(vertexCount: number, posBuffer: Float32Array, normalBuffer: Float32Array, colorBuffer: Uint8Array, uvBuffer: Float32Array, bindingBuffer: Uint16Array, bindingType: Scene.PrimitiveType): Geometry
    {
        const geo = new Geometry();
        geo.vertexCount = vertexCount;
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: posBuffer.length / vertexCount,
            vertexData: posBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4, //colorBuffer.length / vertexCount
            vertexData: colorBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2, //uvBuffer.length / vertexCount
            vertexData: uvBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Normal,
            bindingID: 0,
            vertexSize: 3, //normalBuffer.length / vertexCount
            vertexData: normalBuffer
        });
        geo.indexBindings.push({
            type: bindingType,
            indexData: bindingBuffer
        });
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }
    function createCube(size: Math.ReadonlyVector3D, outline: boolean): Geometry
    {
        const hs = Math.Vector3D.divide(size, 2);
        const geo = new Geometry();
        geo.vertexCount = 4 * 6;
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 3,
            vertexData: new Float32Array([
                // -z
                -hs.x, -hs.y, -hs.z,
                -hs.x, +hs.y, -hs.z,
                +hs.x, +hs.y, -hs.z,
                +hs.x, -hs.y, -hs.z,

                // +z
                +hs.x, -hs.y, +hs.z,
                +hs.x, +hs.y, +hs.z,
                -hs.x, +hs.y, +hs.z,
                -hs.x, -hs.y, +hs.z,

                // -x
                -hs.x, -hs.y, +hs.z,
                -hs.x, +hs.y, +hs.z,
                -hs.x, +hs.y, -hs.z,
                -hs.x, -hs.y, -hs.z,

                // +x
                +hs.x, -hs.y, -hs.z,
                +hs.x, +hs.y, -hs.z,
                +hs.x, +hs.y, +hs.z,
                +hs.x, -hs.y, +hs.z,

                // -y
                -hs.x, -hs.y, +hs.z,
                -hs.x, -hs.y, -hs.z,
                +hs.x, -hs.y, -hs.z,
                +hs.x, -hs.y, +hs.z,

                // +y
                +hs.x, +hs.y, +hs.z,
                +hs.x, +hs.y, -hs.z,
                -hs.x, +hs.y, -hs.z,
                -hs.x, +hs.y, +hs.z
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4,
            vertexData: new Uint8Array([
                // -z
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,

                // +z
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,

                // -x
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,

                // +x
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,

                // -y
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,

                // +y
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2,
            vertexData: new Float32Array([
                // -z
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,

                // +z
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,

                // -x
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,

                // +x
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,

                // -y
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,

                // +y
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0,
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Normal,
            bindingID: 0,
            vertexSize: 3,
            vertexData: new Float32Array([
                // -z
                +0.0, +0.0, -1.0,
                +0.0, +0.0, -1.0,
                +0.0, +0.0, -1.0,
                +0.0, +0.0, -1.0,

                // +z
                +0.0, +0.0, +1.0,
                +0.0, +0.0, +1.0,
                +0.0, +0.0, +1.0,
                +0.0, +0.0, +1.0,

                // -x
                -1.0, +0.0, +0.0,
                -1.0, +0.0, +0.0,
                -1.0, +0.0, +0.0,
                -1.0, +0.0, +0.0,

                // +x
                +1.0, +0.0, +0.0,
                +1.0, +0.0, +0.0,
                +1.0, +0.0, +0.0,
                +1.0, +0.0, +0.0,

                // -y
                +0.0, -1.0, +0.0,
                +0.0, -1.0, +0.0,
                +0.0, -1.0, +0.0,
                +0.0, -1.0, +0.0,

                // +y
                +0.0, +1.0, +0.0,
                +0.0, +1.0, +0.0,
                +0.0, +1.0, +0.0,
                +0.0, +1.0, +0.0
            ])
        });
        if (outline)
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: new Uint16Array([
                    0, 1, 1, 2, 2, 3, 3, 0,
                    4, 5, 5, 6, 6, 7, 7, 4,
                    20, 21, 22, 23,
                    16, 17, 18, 19
                ])
            });
        }
        else
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Triangle,
                indexData: new Uint16Array([
                    0, 1, 2, 2, 3, 0,
                    4, 5, 6, 6, 7, 4,
                    8, 9, 10, 10, 11, 8,
                    12, 13, 14, 14, 15, 12,
                    16, 17, 18, 18, 19, 16,
                    20, 21, 22, 22, 23, 20
                ])
            });
            geo.recalculateTangents();
        }
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }
    function createPlane(size: Math.ReadonlyVector2D, outline: boolean, uvRect: ReadonlyRectangle): Geometry
    {
        const hs = Math.Vector2D.divide(size, 2);
        const geo = new Geometry();
        geo.vertexCount = 4;
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 3,
            vertexData: new Float32Array([
                +hs.x, 0.0, +hs.y,
                +hs.x, 0.0, -hs.y,
                -hs.x, 0.0, -hs.y,
                -hs.x, 0.0, +hs.y
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4,
            vertexData: new Uint8Array([
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2,
            vertexData: new Float32Array([
                uvRect.x, uvRect.y,
                uvRect.x, uvRect.y + uvRect.h,
                uvRect.x + uvRect.w, uvRect.y + uvRect.h,
                uvRect.x + uvRect.w, uvRect.y
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Normal,
            bindingID: 0,
            vertexSize: 3,
            vertexData: new Float32Array([
                +0.0, +1.0, +0.0,
                +0.0, +1.0, +0.0,
                +0.0, +1.0, +0.0,
                +0.0, +1.0, +0.0
            ])
        });
        if (outline)
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: new Uint16Array([
                    0, 1, 1, 2, 2, 3, 3, 0
                ])
            });
        }
        else
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Triangle,
                indexData: new Uint16Array([
                    0, 1, 2, 2, 3, 0
                ])
            });
            geo.recalculateTangents();
        }
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }
    function createCircle(radius: number, divisions: number, uvRadius: number, outline: boolean): Geometry
    {
        if (divisions < 3) { throw new Error("Circle must have at least 3 divisions"); }

        const geo = new Geometry();
        geo.vertexCount = 3 * divisions;

        const posBuffer = new Float32Array(geo.vertexCount * 3);
        const normBuffer = new Float32Array(geo.vertexCount * 3);
        const colBuffer = new Uint8Array(geo.vertexCount * 4);
        const uvBuffer = new Float32Array(geo.vertexCount * 2);
        const bindingBuffer = new Uint16Array(outline ? 4 * divisions : geo.vertexCount);

        for (let i = 0; i < divisions; ++i)
        {
            const angle = RS.Math.Angles.circle(i / divisions);
            const nextAngle = RS.Math.Angles.circle((i + 1) / divisions);

            // vertices position
            posBuffer[i * 9 + 0] = 0.0;
            posBuffer[i * 9 + 1] = 0.0;
            posBuffer[i * 9 + 2] = 0.0;

            posBuffer[i * 9 + 3] = Math.cos(angle) * radius;
            posBuffer[i * 9 + 4] = 0.0;
            posBuffer[i * 9 + 5] = Math.sin(angle) * radius;

            posBuffer[i * 9 + 6] = Math.cos(nextAngle) * radius;
            posBuffer[i * 9 + 7] = 0.0;
            posBuffer[i * 9 + 8] = Math.sin(nextAngle) * radius;

            // normals
            normBuffer[i * 9 + 0] = 0.0; normBuffer[i * 9 + 1] = 1.0; normBuffer[i * 9 + 2] = 0.0;
            normBuffer[i * 9 + 3] = 0.0; normBuffer[i * 9 + 4] = 1.0; normBuffer[i * 9 + 5] = 0.0;
            normBuffer[i * 9 + 6] = 0.0; normBuffer[i * 9 + 7] = 1.0; normBuffer[i * 9 + 8] = 0.0;

            // colors
            colBuffer[i * 12 + 0] = 255; colBuffer[i * 12 + 1] = 255; colBuffer[i * 12 + 2] = 255; colBuffer[i * 12 + 3] = 255;
            colBuffer[i * 12 + 4] = 255; colBuffer[i * 12 + 5] = 255; colBuffer[i * 12 + 6] = 255; colBuffer[i * 12 + 7] = 255;
            colBuffer[i * 12 + 8] = 255; colBuffer[i * 12 + 9] = 255; colBuffer[i * 12 + 10] = 255; colBuffer[i * 12 + 11] = 255;

            // uv coordinates
            uvBuffer[i * 6 + 0] = 0.5;
            uvBuffer[i * 6 + 1] = 0.5;
            uvBuffer[i * 6 + 2] = 0.5 + 0.5 * Math.cos(angle) * uvRadius;
            uvBuffer[i * 6 + 3] = 0.5 + 0.5 * Math.sin(angle) * uvRadius;
            uvBuffer[i * 6 + 4] = 0.5 + 0.5 * Math.cos(nextAngle) * uvRadius;
            uvBuffer[i * 6 + 5] = 0.5 + 0.5 * Math.sin(nextAngle) * uvRadius;

            // vertex binding
            if (outline)
            {
                bindingBuffer[i * 4 + 0] = i * 3 + 0;
                bindingBuffer[i * 4 + 1] = i * 3 + 1;
                bindingBuffer[i * 4 + 2] = i * 3 + 1;
                bindingBuffer[i * 4 + 3] = i * 3 + 2;
            }
            else
            {
                bindingBuffer[i * 3 + 0] = i * 3 + 0;
                bindingBuffer[i * 3 + 1] = i * 3 + 2;
                bindingBuffer[i * 3 + 2] = i * 3 + 1;
            }
        }

        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 3,
            vertexData: posBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4,
            vertexData: colBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2,
            vertexData: uvBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Normal,
            bindingID: 0,
            vertexSize: 3,
            vertexData: normBuffer
        });

        if (outline)
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: bindingBuffer
            });
        }
        else
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Triangle,
                indexData: bindingBuffer
            });
            geo.recalculateTangents();
        }

        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }
    function createSphere(radius: number, divisions: number, outline: boolean): Geometry
    {
        if (divisions < 3) { throw new Error("Sphere must have at least 3 divisions"); }

        const longDivs = divisions; // vertical
        const latDivs = longDivs * 2; // horizontal

        const geo = new Geometry();
        geo.vertexCount = longDivs * (latDivs + 1);

        const posBuffer = new Float32Array(geo.vertexCount * 4);
        const normBuffer = new Float32Array(geo.vertexCount * 3);
        const colBuffer = new Uint8Array(geo.vertexCount * 4);
        const uvBuffer = new Float32Array(geo.vertexCount * 2);

        function getVertexIdx(longitude: number, latitude: number): number
        {
            return Math.wrap(longitude, 0, longDivs) * (latDivs + 1) + Math.wrap(latitude, 0, latDivs + 1);
        }

        for (let longitude = 0; longitude < longDivs; ++longitude)
        {
            const v = longitude / (longDivs - 1);
            const pitch = v * Math.PI;

            const y = Math.cos(pitch);
            const r = Math.sin(pitch);
            for (let latitude = 0; latitude <= latDivs; ++latitude)
            {
                const vertPtr = getVertexIdx(longitude, latitude);

                const u = latitude / latDivs;
                const yaw = u * Math.PI * 2.0;
                const x = Math.cos(yaw) * r;
                const z = Math.sin(yaw) * r;

                posBuffer[vertPtr * 4 + 0] = x * radius;
                posBuffer[vertPtr * 4 + 1] = y * radius;
                posBuffer[vertPtr * 4 + 2] = z * radius;
                posBuffer[vertPtr * 4 + 3] = 1.0;

                normBuffer[vertPtr * 3 + 0] = x;
                normBuffer[vertPtr * 3 + 1] = y;
                normBuffer[vertPtr * 3 + 2] = z;

                colBuffer[vertPtr * 4 + 0] = 255;
                colBuffer[vertPtr * 4 + 1] = 255;
                colBuffer[vertPtr * 4 + 2] = 255;
                colBuffer[vertPtr * 4 + 3] = 255;

                if (longitude === 0 || longitude === longDivs - 1)
                {
                    uvBuffer[vertPtr * 2 + 0] = u + 0.5 / latDivs;
                }
                else
                {
                    uvBuffer[vertPtr * 2 + 0] = u;
                }
                uvBuffer[vertPtr * 2 + 1] = v;
            }
        }

        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 4,
            vertexData: posBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4,
            vertexData: colBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2,
            vertexData: uvBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Normal,
            bindingID: 0,
            vertexSize: 3,
            vertexData: normBuffer
        });

        if (outline)
        {
            const idxCount = (longDivs - 3) * latDivs * 8 + latDivs * 4;
            const indices = new Uint16Array(idxCount);
            let linePtr = 0;
            for (let longitude = 1; longitude < longDivs; ++longitude)
            {
                if (longitude === 1)
                {
                    for (let latitude = 0; latitude < latDivs; ++latitude)
                    {
                        indices[linePtr * 2 + 0] = getVertexIdx(longitude - 1, latitude);
                        indices[linePtr * 2 + 1] = getVertexIdx(longitude, latitude);
                        ++linePtr;
                    }
                }
                else if (longitude === longDivs - 1)
                {
                    for (let latitude = 0; latitude < latDivs; ++latitude)
                    {
                        indices[linePtr * 2 + 0] = getVertexIdx(longitude - 1, latitude);
                        indices[linePtr * 2 + 1] = getVertexIdx(longitude, latitude);
                        ++linePtr;
                    }
                }
                else
                {
                    for (let latitude = 0; latitude < latDivs; ++latitude)
                    {
                        indices[linePtr * 2 + 0] = getVertexIdx(longitude - 1, latitude);
                        indices[linePtr * 2 + 1] = getVertexIdx(longitude - 1, latitude + 1);
                        ++linePtr;

                        indices[linePtr * 2 + 0] = getVertexIdx(longitude - 1, latitude + 1);
                        indices[linePtr * 2 + 1] = getVertexIdx(longitude, latitude + 1);
                        ++linePtr;

                        indices[linePtr * 2 + 0] = getVertexIdx(longitude, latitude + 1);
                        indices[linePtr * 2 + 1] = getVertexIdx(longitude, latitude);
                        ++linePtr;

                        indices[linePtr * 2 + 0] = getVertexIdx(longitude, latitude);
                        indices[linePtr * 2 + 1] = getVertexIdx(longitude - 1, latitude);
                        ++linePtr;
                    }
                }
            }

            geo.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: indices
            });
            geo.recalculateTangents();
        }
        else
        {
            const idxCount = (longDivs - 2) * latDivs * 6;
            const indices = new Uint16Array(idxCount);
            let triPtr = 0;
            for (let longitude = 1; longitude < longDivs; ++longitude)
            {
                if (longitude === 1)
                {
                    for (let latitude = 0; latitude < latDivs; ++latitude)
                    {
                        indices[triPtr * 3 + 2] = getVertexIdx(longitude, latitude);
                        indices[triPtr * 3 + 1] = getVertexIdx(longitude, latitude + 1);
                        indices[triPtr * 3 + 0] = getVertexIdx(longitude - 1, latitude);
                        ++triPtr;
                    }
                }
                else if (longitude === longDivs - 1)
                {
                    for (let latitude = 0; latitude < latDivs; ++latitude)
                    {
                        indices[triPtr * 3 + 0] = getVertexIdx(longitude - 1, latitude);
                        indices[triPtr * 3 + 1] = getVertexIdx(longitude - 1, latitude + 1);
                        indices[triPtr * 3 + 2] = getVertexIdx(longitude, latitude);
                        ++triPtr;
                    }
                }
                else
                {
                    for (let latitude = 0; latitude < latDivs; ++latitude)
                    {
                        indices[triPtr * 3 + 2] = getVertexIdx(longitude, latitude);
                        indices[triPtr * 3 + 1] = getVertexIdx(longitude, latitude + 1);
                        indices[triPtr * 3 + 0] = getVertexIdx(longitude - 1, latitude);
                        ++triPtr;

                        indices[triPtr * 3 + 0] = getVertexIdx(longitude - 1, latitude);
                        indices[triPtr * 3 + 1] = getVertexIdx(longitude - 1, latitude + 1);
                        indices[triPtr * 3 + 2] = getVertexIdx(longitude, latitude + 1);
                        ++triPtr;
                    }
                }
            }

            geo.indexBindings.push({
                type: Scene.PrimitiveType.Triangle,
                indexData: indices
            });
            geo.recalculateTangents();
        }
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }
    function createCylinder(radius: number, divisions: number, height: number, uvRadius: number, outline: boolean): Geometry
    {
        if (divisions < 3) { throw new Error("Cylinder must have at least 3 divisions"); }

        const geo = new Geometry();
        geo.vertexCount = 12 * divisions;

        const posBuffer = new Float32Array(geo.vertexCount * 3);
        const normBuffer = new Float32Array(geo.vertexCount * 3);
        const colBuffer = new Uint8Array(geo.vertexCount * 4);
        const uvBuffer = new Float32Array(geo.vertexCount * 2);
        const bindingBuffer = new Uint16Array(outline ? 10 * divisions : geo.vertexCount);

        for (let i = 0; i < divisions; ++i)
        {
            const angle = RS.Math.Angles.circle(i / divisions);
            const nextAngle = RS.Math.Angles.circle((i + 1) / divisions);

            // vertices position
            // TOP
            posBuffer[i * 36 + 0] = 0.0;
            posBuffer[i * 36 + 1] = height / 2;
            posBuffer[i * 36 + 2] = 0.0;
            posBuffer[i * 36 + 3] = Math.cos(angle) * radius;
            posBuffer[i * 36 + 4] = height / 2;
            posBuffer[i * 36 + 5] = Math.sin(angle) * radius;
            posBuffer[i * 36 + 6] = Math.cos(nextAngle) * radius;
            posBuffer[i * 36 + 7] = height / 2;
            posBuffer[i * 36 + 8] = Math.sin(nextAngle) * radius;
            // BOTTOM
            posBuffer[i * 36 + 9] = 0.0;
            posBuffer[i * 36 + 10] = -height / 2;
            posBuffer[i * 36 + 11] = 0.0;
            posBuffer[i * 36 + 12] = Math.cos(angle) * radius;
            posBuffer[i * 36 + 13] = -height / 2;
            posBuffer[i * 36 + 14] = Math.sin(angle) * radius;
            posBuffer[i * 36 + 15] = Math.cos(nextAngle) * radius;
            posBuffer[i * 36 + 16] = -height / 2;
            posBuffer[i * 36 + 17] = Math.sin(nextAngle) * radius;
            // SIDE 1
            posBuffer[i * 36 + 18] = Math.cos(angle) * radius;
            posBuffer[i * 36 + 19] = height / 2;
            posBuffer[i * 36 + 20] = Math.sin(angle) * radius;
            posBuffer[i * 36 + 21] = Math.cos(nextAngle) * radius;
            posBuffer[i * 36 + 22] = -height / 2;
            posBuffer[i * 36 + 23] = Math.sin(nextAngle) * radius;
            posBuffer[i * 36 + 24] = Math.cos(nextAngle) * radius;
            posBuffer[i * 36 + 25] = height / 2;
            posBuffer[i * 36 + 26] = Math.sin(nextAngle) * radius;
            // SIDE 2
            posBuffer[i * 36 + 27] = Math.cos(angle) * radius;
            posBuffer[i * 36 + 28] = height / 2;
            posBuffer[i * 36 + 29] = Math.sin(angle) * radius;
            posBuffer[i * 36 + 30] = Math.cos(angle) * radius;
            posBuffer[i * 36 + 31] = -height / 2;
            posBuffer[i * 36 + 32] = Math.sin(angle) * radius;
            posBuffer[i * 36 + 33] = Math.cos(nextAngle) * radius;
            posBuffer[i * 36 + 34] = -height / 2;
            posBuffer[i * 36 + 35] = Math.sin(nextAngle) * radius;

            // normals
            // TOP
            normBuffer[i * 36 + 0] = 0.0; normBuffer[i * 36 + 1] = 1.0; normBuffer[i * 36 + 2] = 0.0;
            normBuffer[i * 36 + 3] = 0.0; normBuffer[i * 36 + 4] = 1.0; normBuffer[i * 36 + 5] = 0.0;
            normBuffer[i * 36 + 6] = 0.0; normBuffer[i * 36 + 7] = 1.0; normBuffer[i * 36 + 8] = 0.0;
            // BOTTOM
            normBuffer[i * 36 + 9] = 0.0; normBuffer[i * 36 + 10] = -1.0; normBuffer[i * 36 + 11] = 0.0;
            normBuffer[i * 36 + 12] = 0.0; normBuffer[i * 36 + 13] = -1.0; normBuffer[i * 36 + 14] = 0.0;
            normBuffer[i * 36 + 15] = 0.0; normBuffer[i * 36 + 16] = -1.0; normBuffer[i * 36 + 17] = 0.0;
            // SIDE 1
            normBuffer[i * 36 + 18] = Math.cos(angle); normBuffer[i * 36 + 19] = 0.0; normBuffer[i * 36 + 20] = Math.sin(angle);
            normBuffer[i * 36 + 21] = Math.cos(nextAngle); normBuffer[i * 36 + 22] = 0.0; normBuffer[i * 36 + 23] = Math.sin(nextAngle);
            normBuffer[i * 36 + 24] = Math.cos(nextAngle); normBuffer[i * 36 + 25] = 0.0; normBuffer[i * 36 + 26] = Math.sin(nextAngle);
            // SIDE 2
            normBuffer[i * 36 + 27] = Math.cos(angle); normBuffer[i * 36 + 28] = 0.0; normBuffer[i * 36 + 29] = Math.sin(angle);
            normBuffer[i * 36 + 30] = Math.cos(angle); normBuffer[i * 36 + 31] = 0.0; normBuffer[i * 36 + 32] = Math.sin(angle);
            normBuffer[i * 36 + 33] = Math.cos(nextAngle); normBuffer[i * 36 + 34] = 0.0; normBuffer[i * 36 + 35] = Math.sin(nextAngle);

            // colors
            colBuffer[i * 48 + 0] = 255; colBuffer[i * 48 + 1] = 255; colBuffer[i * 48 + 2] = 255; colBuffer[i * 48 + 3] = 255;
            colBuffer[i * 48 + 4] = 255; colBuffer[i * 48 + 5] = 255; colBuffer[i * 48 + 6] = 255; colBuffer[i * 48 + 7] = 255;
            colBuffer[i * 48 + 8] = 255; colBuffer[i * 48 + 9] = 255; colBuffer[i * 48 + 10] = 255; colBuffer[i * 48 + 11] = 255;
            colBuffer[i * 48 + 12] = 255; colBuffer[i * 48 + 13] = 255; colBuffer[i * 48 + 14] = 255; colBuffer[i * 48 + 15] = 255;
            colBuffer[i * 48 + 16] = 255; colBuffer[i * 48 + 17] = 255; colBuffer[i * 48 + 18] = 255; colBuffer[i * 48 + 19] = 255;
            colBuffer[i * 48 + 20] = 255; colBuffer[i * 48 + 21] = 255; colBuffer[i * 48 + 22] = 255; colBuffer[i * 48 + 23] = 255;
            colBuffer[i * 48 + 24] = 255; colBuffer[i * 48 + 25] = 255; colBuffer[i * 48 + 26] = 255; colBuffer[i * 48 + 27] = 255;
            colBuffer[i * 48 + 28] = 255; colBuffer[i * 48 + 29] = 255; colBuffer[i * 48 + 30] = 255; colBuffer[i * 48 + 31] = 255;
            colBuffer[i * 48 + 32] = 255; colBuffer[i * 48 + 33] = 255; colBuffer[i * 48 + 34] = 255; colBuffer[i * 48 + 35] = 255;
            colBuffer[i * 48 + 36] = 255; colBuffer[i * 48 + 37] = 255; colBuffer[i * 48 + 38] = 255; colBuffer[i * 48 + 39] = 255;
            colBuffer[i * 48 + 40] = 255; colBuffer[i * 48 + 41] = 255; colBuffer[i * 48 + 42] = 255; colBuffer[i * 48 + 43] = 255;
            colBuffer[i * 48 + 44] = 255; colBuffer[i * 48 + 45] = 255; colBuffer[i * 48 + 46] = 255; colBuffer[i * 48 + 47] = 255;

            // uv coordinates
            // TOP
            uvBuffer[i * 24 + 0] = 0.5;
            uvBuffer[i * 24 + 1] = 0.5;
            uvBuffer[i * 24 + 2] = 0.5 + 0.5 * Math.cos(angle) * uvRadius;
            uvBuffer[i * 24 + 3] = 0.5 + 0.5 * Math.sin(angle) * uvRadius;
            uvBuffer[i * 24 + 4] = 0.5 + 0.5 * Math.cos(nextAngle) * uvRadius;
            uvBuffer[i * 24 + 5] = 0.5 + 0.5 * Math.sin(nextAngle) * uvRadius;
            // BOTTOM
            uvBuffer[i * 24 + 6] = 0.5;
            uvBuffer[i * 24 + 7] = 0.5;
            uvBuffer[i * 24 + 8] = 0.5 + 0.5 * Math.cos(angle) * uvRadius;
            uvBuffer[i * 24 + 9] = 0.5 + 0.5 * Math.sin(angle) * uvRadius;
            uvBuffer[i * 24 + 10] = 0.5 + 0.5 * Math.cos(nextAngle) * uvRadius;
            uvBuffer[i * 24 + 11] = 0.5 + 0.5 * Math.sin(nextAngle) * uvRadius;
            // SIDE 1
            uvBuffer[i * 24 + 12] = 0.5 + 0.5 * Math.cos(angle) * uvRadius;
            uvBuffer[i * 24 + 13] = 0.5 + 0.5 * Math.sin(angle) * uvRadius;
            uvBuffer[i * 24 + 14] = 0.5 + 0.5 * Math.cos(nextAngle) * uvRadius;
            uvBuffer[i * 24 + 15] = 0.5 + 0.5 * Math.sin(nextAngle) * uvRadius;
            uvBuffer[i * 24 + 16] = 0.5 + 0.5 * Math.cos(nextAngle) * uvRadius;
            uvBuffer[i * 24 + 17] = 0.5 + 0.5 * Math.sin(nextAngle) * uvRadius;
            // SIDE 1
            uvBuffer[i * 24 + 18] = 0.5 + 0.5 * Math.cos(angle) * uvRadius;
            uvBuffer[i * 24 + 19] = 0.5 + 0.5 * Math.sin(angle) * uvRadius;
            uvBuffer[i * 24 + 20] = 0.5 + 0.5 * Math.cos(angle) * uvRadius;
            uvBuffer[i * 24 + 21] = 0.5 + 0.5 * Math.sin(angle) * uvRadius;
            uvBuffer[i * 24 + 22] = 0.5 + 0.5 * Math.cos(nextAngle) * uvRadius;
            uvBuffer[i * 24 + 23] = 0.5 + 0.5 * Math.sin(nextAngle) * uvRadius;

            // vertex binding
            if (outline)
            {
                bindingBuffer[i * 10 + 0] = i * 12 + 0;
                bindingBuffer[i * 10 + 1] = i * 12 + 1;
                bindingBuffer[i * 10 + 2] = i * 12 + 1;
                bindingBuffer[i * 10 + 3] = i * 12 + 2;
                bindingBuffer[i * 10 + 4] = i * 12 + 3;
                bindingBuffer[i * 10 + 5] = i * 12 + 4;
                bindingBuffer[i * 10 + 6] = i * 12 + 4;
                bindingBuffer[i * 10 + 7] = i * 12 + 5;
                bindingBuffer[i * 10 + 8] = i * 12 + 1;
                bindingBuffer[i * 10 + 9] = i * 12 + 4;
            }
            else
            {
                bindingBuffer[i * 12 + 0] = i * 12 + 0;
                bindingBuffer[i * 12 + 1] = i * 12 + 2;
                bindingBuffer[i * 12 + 2] = i * 12 + 1;
                bindingBuffer[i * 12 + 3] = i * 12 + 6;
                bindingBuffer[i * 12 + 4] = i * 12 + 8;
                bindingBuffer[i * 12 + 5] = i * 12 + 7;
                bindingBuffer[i * 12 + 6] = i * 12 + 9;
                bindingBuffer[i * 12 + 7] = i * 12 + 11;
                bindingBuffer[i * 12 + 8] = i * 12 + 10;
                bindingBuffer[i * 12 + 9] = i * 12 + 3;
                bindingBuffer[i * 12 + 10] = i * 12 + 4;
                bindingBuffer[i * 12 + 11] = i * 12 + 5;
            }
        }

        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 3,
            vertexData: posBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4,
            vertexData: colBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2,
            vertexData: uvBuffer
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Normal,
            bindingID: 0,
            vertexSize: 3,
            vertexData: normBuffer
        });

        if (outline)
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: bindingBuffer
            });
        }
        else
        {
            geo.indexBindings.push({
                type: Scene.PrimitiveType.Triangle,
                indexData: bindingBuffer
            });
            geo.recalculateTangents();
        }

        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }
    function createFSQuad(): Geometry
    {
        const geo = new Geometry();
        geo.vertexCount = 4;
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 4,
            vertexData: new Float32Array([
                -1.0, -1.0, 0.0, 1.0,
                -1.0, 1.0, 0.0, 1.0,
                1.0, 1.0, 0.0, 1.0,
                1.0, -1.0, 0.0, 1.0
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            normalise: true,
            bindingID: 0,
            vertexSize: 4,
            vertexData: new Uint8Array([
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
                255, 255, 255, 255,
            ])
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.TexCoord,
            bindingID: 0,
            vertexSize: 2,
            vertexData: new Float32Array([
                0.0, 0.0,
                0.0, 1.0,
                1.0, 1.0,
                1.0, 0.0
            ])
        });
        geo.indexBindings.push({
            type: Scene.PrimitiveType.Triangle,
            indexData: new Uint16Array([
                0, 1, 2, 2, 3, 0
            ])
        });
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    }

    Scene.Geometry.createDebugNormals = function (inGeo: Geometry, unitLength: number = 1.0): Geometry
    {
        const inPosVB = inGeo.getVertexBinding(Scene.VertexBindingType.Position, 0);
        const inNormVB = inGeo.getVertexBinding(Scene.VertexBindingType.Normal, 0);
        const inTangentVB = inGeo.getVertexBinding(Scene.VertexBindingType.Normal, 1);
        const inBinormalVB = inGeo.getVertexBinding(Scene.VertexBindingType.Normal, 2);
        if (!inPosVB) { return null; }
        if (!inNormVB && !inTangentVB && !inBinormalVB) { return null; }
        const geo = new Geometry();
        geo.vertexCount = inGeo.vertexCount * ((inNormVB ? 1 : 0) + (inTangentVB ? 1 : 0) + (inBinormalVB ? 1 : 0)) * 2;
        const outPositions = new Float32Array(geo.vertexCount * 4);
        const outColours = new Uint8Array(geo.vertexCount * 4);
        const outIndices = new Uint16Array(geo.vertexCount);
        let outPtr = 0;
        const vertPos = Math.Vector3D(), vertNorm = Math.Vector3D(), vertTmp = Math.Vector3D();
        function emit(pos: Math.Vector3D, col: Util.Color)
        {
            outPositions[outPtr + 0] = pos.x;
            outPositions[outPtr + 1] = pos.y;
            outPositions[outPtr + 2] = pos.z;
            outPositions[outPtr + 3] = 1.0;
            outColours[outPtr + 0] = (col.r * 255) | 0;
            outColours[outPtr + 1] = (col.g * 255) | 0;
            outColours[outPtr + 2] = (col.b * 255) | 0;
            outColours[outPtr + 3] = 255;
            outPtr += 4;
        }
        let vertPtr = 0, indPtr = 0;
        for (let i = 0, l = inGeo.vertexCount; i < l; ++i)
        {
            let ptr = i * inPosVB.vertexSize;
            if (inPosVB.vertexSize >= 1) { vertPos.x = inPosVB.vertexData[ptr++]; }
            if (inPosVB.vertexSize >= 2) { vertPos.y = inPosVB.vertexData[ptr++]; }
            if (inPosVB.vertexSize >= 3) { vertPos.z = inPosVB.vertexData[ptr++]; }

            // Normal
            ptr = i * inNormVB.vertexSize;
            if (inNormVB.vertexSize >= 1) { vertNorm.x = inNormVB.vertexData[ptr++]; }
            if (inNormVB.vertexSize >= 2) { vertNorm.y = inNormVB.vertexData[ptr++]; }
            if (inNormVB.vertexSize >= 3) { vertNorm.z = inNormVB.vertexData[ptr++]; }
            vertTmp.x = vertPos.x + vertNorm.x * unitLength;
            vertTmp.y = vertPos.y + vertNorm.y * unitLength;
            vertTmp.z = vertPos.z + vertNorm.z * unitLength;
            emit(vertPos, Util.Colors.blue);
            emit(vertTmp, Util.Colors.blue);
            outIndices[indPtr++] = vertPtr++;
            outIndices[indPtr++] = vertPtr++;

            // Tangent
            ptr = i * inTangentVB.vertexSize;
            if (inTangentVB.vertexSize >= 1) { vertNorm.x = inTangentVB.vertexData[ptr++]; }
            if (inTangentVB.vertexSize >= 2) { vertNorm.y = inTangentVB.vertexData[ptr++]; }
            if (inTangentVB.vertexSize >= 3) { vertNorm.z = inTangentVB.vertexData[ptr++]; }
            vertTmp.x = vertPos.x + vertNorm.x * unitLength;
            vertTmp.y = vertPos.y + vertNorm.y * unitLength;
            vertTmp.z = vertPos.z + vertNorm.z * unitLength;
            emit(vertPos, Util.Colors.red);
            emit(vertTmp, Util.Colors.red);
            outIndices[indPtr++] = vertPtr++;
            outIndices[indPtr++] = vertPtr++;

            // Binormal
            ptr = i * inBinormalVB.vertexSize;
            if (inTangentVB.vertexSize >= 1) { vertNorm.x = inBinormalVB.vertexData[ptr++]; }
            if (inTangentVB.vertexSize >= 2) { vertNorm.y = inBinormalVB.vertexData[ptr++]; }
            if (inTangentVB.vertexSize >= 3) { vertNorm.z = inBinormalVB.vertexData[ptr++]; }
            vertTmp.x = vertPos.x + vertNorm.x * unitLength;
            vertTmp.y = vertPos.y + vertNorm.y * unitLength;
            vertTmp.z = vertPos.z + vertNorm.z * unitLength;
            emit(vertPos, Util.Colors.green);
            emit(vertTmp, Util.Colors.green);
            outIndices[indPtr++] = vertPtr++;
            outIndices[indPtr++] = vertPtr++;
        }
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 4,
            vertexData: outPositions
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            bindingID: 0,
            vertexSize: 4,
            vertexData: outColours
        });
        geo.indexBindings.push({
            type: Scene.PrimitiveType.Line,
            indexData: outIndices
        });
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    };

    Scene.Geometry.createDebugBezier = function (path: Bezier.Path, divisionLength: number = 1.0): Geometry
    {
        const segments = path.curves;
        const totalLength = path.length * 0.999;
        const pointCount = Math.ceil(totalLength / divisionLength) + 1;
        divisionLength = totalLength / (pointCount - 1);
        const points = new Array<Math.Vector2D>(pointCount);
        for (let i = 0; i < pointCount; ++i)
        {
            const t = (i / (pointCount - 1)) * segments.length;
            points[i] = path.compute(t);
        }
        const geo = new Geometry();
        geo.vertexCount = pointCount + 4 * segments.length;
        const outPositions = new Float32Array(geo.vertexCount * 4);
        const outColours = new Uint8Array(geo.vertexCount * 4);
        const outIndices = new Uint16Array(pointCount * 2 + segments.length * 4);
        let outPtr = 0;
        function emit(pos: Math.Vector3D, col: Util.Color)
        {
            outPositions[outPtr + 0] = pos.x;
            outPositions[outPtr + 1] = pos.y;
            outPositions[outPtr + 2] = pos.z;
            outPositions[outPtr + 3] = 1.0;
            outColours[outPtr + 0] = (col.r * 255) | 0;
            outColours[outPtr + 1] = (col.g * 255) | 0;
            outColours[outPtr + 2] = (col.b * 255) | 0;
            outColours[outPtr + 3] = 255;
            outPtr += 4;
        }
        let indPtr = 0;
        const vertPos = Math.Vector3D();
        for (let i = 0; i < pointCount; ++i)
        {
            Math.Vector2D.copy(points[i], vertPos);
            emit(vertPos, Util.Colors.red);
            if (i > 0)
            {
                outIndices[indPtr++] = i - 1;
                outIndices[indPtr++] = i;
            }
        }
        vertPos.z = 0;
        let vertPtr = pointCount;
        for (const seg of segments)
        {
            switch (seg.order)
            {
                case 3:
                    Math.Vector2D.copy(seg.points[0], vertPos);
                    emit(vertPos, Util.Colors.yellow);
                    Math.Vector2D.copy(seg.points[1], vertPos);
                    emit(vertPos, Util.Colors.yellow);
                    Math.Vector2D.copy(seg.points[2], vertPos);
                    emit(vertPos, Util.Colors.yellow);
                    Math.Vector2D.copy(seg.points[3], vertPos);
                    emit(vertPos, Util.Colors.yellow);

                    outIndices[indPtr++] = vertPtr;
                    outIndices[indPtr++] = vertPtr + 1;

                    outIndices[indPtr++] = vertPtr + 2;
                    outIndices[indPtr++] = vertPtr + 3;

                    vertPtr += 4;

                    break;
                case 2:
                    Math.Vector2D.copy(seg.points[0], vertPos);
                    emit(vertPos, Util.Colors.yellow);
                    Math.Vector2D.copy(seg.points[1], vertPos);
                    emit(vertPos, Util.Colors.yellow);
                    Math.Vector2D.copy(seg.points[2], vertPos);
                    emit(vertPos, Util.Colors.yellow);

                    outIndices[indPtr++] = vertPtr;
                    outIndices[indPtr++] = vertPtr + 1;

                    outIndices[indPtr++] = vertPtr + 1;
                    outIndices[indPtr++] = vertPtr + 2;

                    vertPtr += 3;
                    
                    break;
            }
        }
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Position,
            bindingID: 0,
            vertexSize: 4,
            vertexData: outPositions
        });
        geo.vertexBindings.push({
            type: Scene.VertexBindingType.Color,
            bindingID: 0,
            vertexSize: 4,
            vertexData: outColours
        });
        geo.indexBindings.push({
            type: Scene.PrimitiveType.Line,
            indexData: outIndices
        });
        const err = geo.validate(true);
        if (err) { throw new Error(err); }
        return geo;
    };
}