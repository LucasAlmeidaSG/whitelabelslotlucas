/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    export interface LightData
    {
        type: number;
        position: Math.Vector4D;
        direction: Math.Vector4D;
        color: Math.Vector4D;
    }

    const tmpMatrix = Math.Matrix4();

    export function mapLights(lights: LightSource[], out?: LightData[]): LightData[]
    {
        if (out)
        {
            out.length = lights.length;
        }
        else
        {
            out = new Array<LightData>(lights.length);
        }
        for (let i = 0, l = lights.length; i < l; ++i)
        {
            const light = lights[i];
            const lightData = out[i] || (out[i] = { type: 0, position: Math.Vector4D(), direction: Math.Vector4D(), color: Math.Vector4D() });
            lightData.type = getLightType(light);
            Math.Vector4D.set(lightData.color, light.color.r * light.intensity, light.color.g * light.intensity, light.color.b * light.intensity, light.color.a);
            light.getWorldTransform(tmpMatrix);
            Math.Matrix4.transform(tmpMatrix, light.position, lightData.position);
            lightData.position.w = 1.0;
            Math.Matrix4.transformNormal(tmpMatrix, Math.Vector3D.unitZ, lightData.direction);
            if (light instanceof PointLightSource)
            {
                lightData.direction.w = light.radius;
            }
            else
            {
                lightData.direction.w = 0.0;
            }
        }
        return out;
    }

    export function mapLightTypes(lights: LightData[], out?: number[]): number[]
    {
        if (out)
        {
            out.length = lights.length;
        }
        else
        {
            out = new Array<number>(lights.length);
        }
        for (let i = 0, l = lights.length; i < l; ++i)
        {
            out[i] = lights[i].type;
        }
        return out;
    }

    export function getLightType(instance: LightSource)
    {
        if (instance instanceof AmbientLightSource)
        {
            return 1;
        }
        else if (instance instanceof DirectionalLightSource)
        {
            return 2;
        }
        else if (instance instanceof PointLightSource)
        {
            return 3;
        }
        else
        {
            throw new Error("Instance was not a light");
        }
    }

    export function optimiseLights(lights: LightData[]): void
    {
        lights.sort((a, b) => a.type - b.type);
        // TODO: Potentially merge lights together...
        // - multiple ambient lights could be reduced to one with their summed color
        // - multiple point lights that have similar position and radii could be reduced to one which approximates both
        // - multiple directional lights that have similar direction could bre reduced to one also
    }

    export function mapLightColors(lights: LightData[], out?: Math.Vector4D[]): Math.Vector4D[]
    {
        if (out)
        {
            out.length = lights.length;
        }
        else
        {
            out = new Array<Math.Vector4D>(lights.length);
        }
        for (let i = 0, l = lights.length; i < l; ++i)
        {
            if (out[i])
            {
                Math.Vector4D.copy(lights[i].color, out[i]);
            }
            else
            {
                out[i] = Math.Vector4D.clone(lights[i].color);
            }
        }
        return out;
    }

    export function mapLightPositions(lights: LightData[], out?: Math.Vector4D[]): Math.Vector4D[]
    {
        if (out)
        {
            out.length = lights.length;
        }
        else
        {
            out = new Array<Math.Vector4D>(lights.length);
        }
        for (let i = 0, l = lights.length; i < l; ++i)
        {
            if (out[i])
            {
                Math.Vector4D.copy(lights[i].position, out[i]);
            }
            else
            {
                out[i] = Math.Vector4D.clone(lights[i].position);
            }
        }
        return out;
    }

    export function mapLightDirections(lights: LightData[], out?: Math.Vector4D[]): Math.Vector4D[]
    {
        if (out)
        {
            out.length = lights.length;
        }
        else
        {
            out = new Array<Math.Vector4D>(lights.length);
        }
        for (let i = 0, l = lights.length; i < l; ++i)
        {
            if (out[i])
            {
                Math.Vector4D.copy(lights[i].direction, out[i]);
            }
            else
            {
                out[i] = Math.Vector4D.clone(lights[i].direction);
            }
        }
        return out;
    }
}