/// <reference path="SceneObject.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpMatrix = Math.Matrix4(), tmpVec1 = Math.Vector4D(), tmpVec2 = Math.Vector3D(), tmpVec3 = Math.Vector4D();

    @HasCallbacks
    export class Camera extends SceneObject implements Scene.ICamera
    {
        protected _bgColor: Util.Color;
        protected _projection: Math.Matrix4;

        /** Gets or sets the background color of this camera. */
        public get backgroundColor() { return this._bgColor; }
        public set backgroundColor(value) { this._bgColor = value; }

        /** Gets or sets the projection matrix of this camera. */
        public get projection(): Math.Matrix4 { return this._projection; }
        public set projection(value) { this._projection = value; }

        public constructor();

        public constructor(fov: number, aspectRatio: number);

        public constructor(p1?: number, p2?: number)
        {
            super();
            if (Is.number(p1))
            {
                this.setPerspective(p1, p2);
            }
            this._bgColor = Util.Colors.cornflowerblue;
        }

        /** Sets this camera up with a perspective view. */
        public setPerspective(fov: number, aspectRatio: number): void
        {
            this.projection = Math.Matrix4.createPerspective(fov, 1 / 16, 1024, aspectRatio);
        }

        /** Sets this camera up with an orthographic view. */
        public setOrthographic(left: number, right: number, top: number, bottom: number): void
        {
            this.projection = Math.Matrix4.createOrthographic(left, right, top, bottom, 1024, 0);
        }

        /** Transforms the specified point in world space to normalised viewport coordinates (0-1). */
        public projectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector2D): Math.Vector2D
        {
            out = out || Math.Vector2D();

            const inverseWorld = Math.Matrix4.inverse(this.getWorldTransform(), tmpMatrix);
            const tmpPoint = Math.Matrix4.transform(inverseWorld, point, tmpVec1);
            tmpVec1.w = 1.0;

            const transformed: Math.Vector4D = Math.Matrix4.transform(this.projection, tmpPoint, tmpVec2) as Math.Vector4D;

            out.x = ((transformed.x / transformed.w) + 1) / 2;
            out.y = ((transformed.y / transformed.w) + 1) / 2;

            return out;
        }

        /** Transforms the specified point normalised viewport coordinates (0-1) to world space. */
        public unprojectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D
        {
            out = out || Math.Vector3D();

            tmpVec3.x = point.x * 2.0 - 1.0;
            tmpVec3.y = point.y * 2.0 - 1.0;
            tmpVec3.z = point.z;
            tmpVec3.w = 1.0;

            const inverseProjection = Math.Matrix4.inverse(this.projection, tmpMatrix);
            Math.Matrix4.transform(inverseProjection, tmpVec3, tmpVec1);
            tmpVec1.w = 1.0;
            Math.Matrix4.transform(this.getWorldTransform(), tmpVec1, out);

            return out;
        }
    }

    Scene.Camera = Camera;
}
