/// <reference path="ISceneRenderer.ts" />
/// <reference path="lighting/LightingUtils.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpLightData: LightData[] = [];
    const tmpSpecular = Math.Vector4D();
    const tmpSize1 = Math.Size2D(), tmpSize2 = Math.Size2D();
    const tmpSwitches = {} as IForwardLightingEngine.Switches;
    const tmpColor = new RS.Util.Color();

    class SceneRenderer implements ISceneRenderer
    {
        private _unlitForwardLightingEngine: IForwardLightingEngine<Shaders.SceneUnlitShader>;
        private _litSpecForwardLightingEngine: IForwardLightingEngine<Shaders.SceneLitSpecShader>;

        private _whiteTexture: ITexture | null = null;
        private _flatNormalTexture: ITexture | null = null;

        private _overrideSwitches: IForwardLightingEngine.Switches;

        public constructor()
        {
            this._unlitForwardLightingEngine = new UnlitForwardLightingEngine();
            this._litSpecForwardLightingEngine = new LitSpecForwardLightingEngine();
            const perf = RS.Performance.getDevicePerformance();
            this._overrideSwitches =
            {
                tangentSpaceNormals: perf >= RS.Performance.Level.High,
                specular: perf >= RS.Performance.Level.Medium,
                reflections: perf >= RS.Performance.Level.High,
                vertexColors: true,
                withDiffuseMap: true,
                withNormalMap: perf >= RS.Performance.Level.High,
                withSpecularMap: perf >= RS.Performance.Level.Medium,
                withAmbientMap: perf >= RS.Performance.Level.Medium,
                withEnvironmentMap: perf >= RS.Performance.Level.Medium,
                vertexBased: perf <= RS.Performance.Level.Low,
                blend: 7
            };
        }

        public render(
            renderer: PIXI.WebGLRenderer, pass: ScenePass,
            projectionMatrix: Math.Matrix4, viewMatrix: Math.Matrix4, worldMatrix: Math.Matrix4,
            viewPosition: Math.Vector3D,
            geometry: Geometry, material: Scene.Material, tint: RS.Util.Color, indexBinding: number,
            lightSources?: LightSource[], flip?: boolean
        ): void
        {
            // Cache local data
            const gl = renderer.gl;
            const ib = geometry.indexBindings[indexBinding];

            // Bind shader
            const shader = this.retrieveAndSetupShader(
                material, tint,
                projectionMatrix, viewMatrix, worldMatrix,
                viewPosition, lightSources
            ) as Shader;
            const glShader = shader.shaderProgram.getRawShaderFor(renderer);
            renderer.bindShader(glShader, false);
            shader.syncUniforms(glShader, tmpSize1, tmpSize2, !renderer._activeRenderTarget.root);
            let texLoc = 0;
            for (const uniformName in shader.uniforms)
            {
                const uniform = shader.uniforms[uniformName];
                if (uniform.type === ShaderUniform.Type.Sampler2D && uniform.value != null)
                {
                    const tex = (uniform.value as Texture).baseTexture;
                    glShader.uniforms[uniformName] = renderer.bindTexture(tex, texLoc);
                    ++texLoc;
                }
            }

            // Bind vao
            const vao = geometry.getVAO(renderer, shader.shaderProgram, indexBinding);
            vao.bind();

            // Setup blend mode
            let needPopState = false;
            switch (material.blend || Scene.Material.Blend.Opaque)
            {
                case Scene.Material.Blend.Opaque:
                case Scene.Material.Blend.Masked:
                    {
                        renderer.gl.depthMask(true);
                        break;
                    }
                case Scene.Material.Blend.Transparent:
                case Scene.Material.Blend.TransparentAdd:
                case Scene.Material.Blend.TransparentMult:
                    {
                        renderer.gl.depthMask(false);
                        renderer.state.push();
                        renderer.state.setBlend(1);
                        needPopState = true;
                        break;
                    }
            }
            if (material.twoSided)
            {
                if (!needPopState)
                {
                    renderer.state.push();
                    needPopState = true;
                }
                renderer.state.setCullFace(0);
            }
            else if (flip)
            {
                if (!needPopState)
                {
                    renderer.state.push();
                    needPopState = true;
                }
                renderer.state.setFrontFace(1);
            }
            switch (material.blend)
            {
                case Scene.Material.Blend.Transparent:
                    renderer.state.setBlendMode(PIXI.BLEND_MODES.NORMAL);
                    break;
                case Scene.Material.Blend.TransparentAdd:
                    renderer.state.setBlendMode(PIXI.BLEND_MODES.ADD);
                    break;
                case Scene.Material.Blend.TransparentMult:
                    renderer.state.setBlendMode(PIXI.BLEND_MODES.MULTIPLY);
                    break;
            }

            // Draw
            switch (ib.type)
            {
                case Scene.PrimitiveType.Triangle:
                    vao.draw(gl.TRIANGLES, 0, 0);
                    break;
                case Scene.PrimitiveType.Line:
                    gl.lineWidth(material.lineWidth || 1);
                    vao.draw(gl.LINES, 0, 0);
                    break;
            }

            // Clean up
            if (needPopState) { renderer.state.pop(); }
        }

        private getWhiteTexture()
        {
            if (this._whiteTexture == null)
            {
                const canvas = document.createElement("canvas");
                canvas.width = canvas.height = 4;
                const ctx = canvas.getContext("2d");
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, 4, 4);
                this._whiteTexture = new Texture(canvas);
            }
            return this._whiteTexture;
        }

        private getFlatNormalTexture()
        {
            if (this._flatNormalTexture == null)
            {
                const canvas = document.createElement("canvas");
                canvas.width = canvas.height = 4;
                const ctx = canvas.getContext("2d");
                ctx.fillStyle = "rgb(127,127,255)";
                ctx.fillRect(0, 0, 4, 4);
                this._flatNormalTexture = new Texture(canvas);
            }
            return this._flatNormalTexture;
        }

        private retrieveAndSetupShader(
            mat: Scene.Material, tint: RS.Util.Color,
            projectionMatrix: Math.Matrix4, viewMatrix: Math.Matrix4, worldMatrix: Math.Matrix4,
            viewPosition: Math.Vector3D,
            lightSources?: LightSource[]
        ): RS.Rendering.IShader | null
        {
            const blend = mat.blend || Scene.Material.Blend.Opaque;

            // Setup common switches
            tmpSwitches.tangentSpaceNormals = this._overrideSwitches.tangentSpaceNormals && mat.normalMap != null;
            tmpSwitches.specular = this._overrideSwitches.specular;
            tmpSwitches.reflections = this._overrideSwitches.reflections;
            tmpSwitches.vertexBased = this._overrideSwitches.vertexBased;
            tmpSwitches.vertexColors = mat.vertexColors != null ? mat.vertexColors : true;
            tmpSwitches.withDiffuseMap = this._overrideSwitches.withDiffuseMap && mat.diffuseMap != null;
            tmpSwitches.withNormalMap = this._overrideSwitches.withNormalMap && mat.normalMap != null;
            tmpSwitches.withSpecularMap = this._overrideSwitches.withSpecularMap && mat.specularMap != null;
            tmpSwitches.withAmbientMap = this._overrideSwitches.withAmbientMap && mat.ambientMap != null;
            tmpSwitches.withEnvironmentMap = this._overrideSwitches.withEnvironmentMap && mat.environmentMap != null;
            tmpSwitches.blend = blend;

            // Switch on lighting model
            switch (mat.model)
            {
                case Scene.Material.Model.Unlit:
                    {
                        // Grab shader
                        const shader = this._unlitForwardLightingEngine.prepare(null, tmpSwitches);

                        // Setup matrices
                        shader.projectionMatrix = projectionMatrix;
                        shader.viewMatrix = viewMatrix;
                        shader.worldMatrix = worldMatrix;

                        // Setup maps
                        let diffuseMap = mat.diffuseMap || this.getWhiteTexture();
                        if (Is.assetReference(diffuseMap)) { diffuseMap = Asset.Store.getAsset(diffuseMap).asset as ITexture; }
                        shader.diffuse = diffuseMap;
                        tmpColor.copy(mat.diffuseTint || Util.Colors.white);
                        tmpColor.multiply(tint);
                        shader.diffuseTint = tmpColor;

                        // Done
                        return shader;
                    }
                case Scene.Material.Model.LitSpec:
                    {
                        // Map lights
                        const lightData = mapLights(lightSources, tmpLightData);
                        optimiseLights(lightData);

                        // Grab shader
                        const shader = this._litSpecForwardLightingEngine.prepare(lightData, tmpSwitches);

                        // Setup matrices
                        shader.projectionMatrix = projectionMatrix;
                        shader.viewMatrix = viewMatrix;
                        shader.worldMatrix = worldMatrix;
                        shader.viewPos = viewPosition;

                        // Setup maps
                        let diffuseMap = mat.diffuseMap || this.getWhiteTexture();
                        if (Is.assetReference(diffuseMap)) { diffuseMap = Asset.Store.getAsset(diffuseMap).asset as ITexture; }
                        shader.diffuse = diffuseMap;
                        tmpColor.copy(mat.diffuseTint || Util.Colors.white);
                        tmpColor.multiply(tint);
                        shader.diffuseTint = tmpColor;
                        let specularMap = mat.specularMap || this.getWhiteTexture();
                        if (Is.assetReference(specularMap)) { specularMap = Asset.Store.getAsset(specularMap).asset as ITexture; }
                        shader.specular = specularMap;
                        tmpSpecular.x = (mat.specularColorTint || Util.Colors.white).r;
                        tmpSpecular.y = (mat.specularColorTint || Util.Colors.white).g;
                        tmpSpecular.z = (mat.specularColorTint || Util.Colors.white).b;
                        tmpSpecular.w = mat.specularPowerMult || 1;
                        shader.specularTint = tmpSpecular;
                        let normalMap = mat.normalMap || this.getFlatNormalTexture();
                        if (Is.assetReference(normalMap)) { normalMap = Asset.Store.getAsset(normalMap).asset as ITexture; }
                        shader.normal = normalMap;
                        let ambientMap = mat.ambientMap || this.getWhiteTexture();
                        if (Is.assetReference(ambientMap)) { ambientMap = Asset.Store.getAsset(ambientMap).asset as ITexture; }
                        shader.ambience = ambientMap;
                        if (!tmpSwitches.withAmbientMap && mat.ambientMapFallback)
                        {
                            shader.ambienceTint = mat.ambientMapFallback;
                        }
                        else
                        {
                            shader.ambienceTint = RS.Util.Colors.white;
                        }
                        let envMap = mat.environmentMap || this.getWhiteTexture();
                        if (Is.assetReference(envMap)) { envMap = Asset.Store.getAsset(envMap).asset as ITexture; }
                        shader.environment = envMap;
                        shader.environmentTint = mat.environmentColorTint || Util.Colors.white;

                        // Done
                        return shader;
                    }
                default:
                    return null;
            }
        }
    }

    ISceneRenderer.register(SceneRenderer);
}