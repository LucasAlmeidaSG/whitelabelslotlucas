/// <reference path="ILightingEngine.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    interface ShaderData
    {
        program: Shaders.SceneLitSpecShaderProgram;
        shader: Shaders.SceneLitSpecShader;
    }

    const tmpLightTypes: number[] = [];
    const tmpLightColors: Math.Vector4D[] = [];
    const tmpLightPositions: Math.Vector4D[] = [];
    const tmpLightDirections: Math.Vector4D[] = [];

    const maxLights = 8;

    export class LitSpecForwardLightingEngine implements IForwardLightingEngine<Shaders.SceneLitSpecShader>
    {
        private _shaderMap: { [hash: number]: ShaderData; } = {};
        private _hashMask: number;

        public constructor()
        {
            // Build a mask to obscure any unsupported switches
            // This will prevent those switches from generating extra permutations of shader programs which are unnecessary
            this._hashMask = IForwardLightingEngine.Switches.hash({
                tangentSpaceNormals: true,
                specular: true,
                reflections: false,
                vertexBased: false,
                vertexColors: true,
                withDiffuseMap: true,
                withNormalMap: true,
                withSpecularMap: true,
                withAmbientMap: true,
                withEnvironmentMap: true,
                blend: 7
            });
        }

        /**
         * Prepares and returns a shader for the specified light data and switches.
         * The shader will have lighting data set, but will need material parameters and matrices set.
         * @param lightData 
         * @param switches 
         */
        public prepare(lightData: LightData[], switches: IForwardLightingEngine.Switches): Shaders.SceneLitSpecShader
        {
            // Check light count
            if (lightData.length > maxLights)
            {
                throw new Error(`LitSpecForwardLightingEngine does not support more than ${maxLights} lights affecting a single object`);
            }

            // Map lights
            const lightTypes = mapLightTypes(lightData, tmpLightTypes);

            // Hash the switches and light types
            let hash = IForwardLightingEngine.Switches.hash(switches) & this._hashMask;
            for (let i = 0; i < maxLights; ++i)
            {
                hash = (hash << 3) | ((lightTypes.length - 1) >= i ? lightTypes[i] : 0);
            }

            // Lookup or create the shader data
            const { program, shader } = this._shaderMap[hash] || (this._shaderMap[hash] = this.createShader(lightTypes, switches));

            // Write lighting data to the shader
            shader.lightPositions = mapLightPositions(lightData, tmpLightPositions);
            shader.lightDirections = mapLightDirections(lightData, tmpLightDirections);
            shader.lightColors = mapLightColors(lightData, tmpLightColors);

            // Done
            return shader;
        }

        private createShader(lightTypes: number[], switches: IForwardLightingEngine.Switches): ShaderData
        {
            Log.debug(`Generating new lit-spec shader for light config [${lightTypes.map((lt) => `${lt}`)}] and switches {${IForwardLightingEngine.Switches.toString(switches)}}`);
            const fragFunc = (src: string) => src.replace("#pragma LIGHTING", this.compileLightTypes(lightTypes));
            let shaderProgram: Shaders.SceneLitSpecShaderProgram;
            switch (switches.blend)
            {
                case Scene.Material.Blend.Opaque:
                    shaderProgram = new Shaders.SceneLitSpecShaderProgram({
                        masked: false,
                        ignoreAlpha: true,
                        tangentSpaceNormals: switches.tangentSpaceNormals && switches.withNormalMap,
                        specular: switches.specular,
                        vertexColors: switches.vertexColors,
                        useDiffuseMap: switches.withDiffuseMap,
                        useNormalMap: switches.tangentSpaceNormals && switches.withNormalMap,
                        useSpecularMap: switches.specular && switches.withSpecularMap,
                        useAmbientMap: switches.withAmbientMap,
                        useEnvironmentMap: switches.withEnvironmentMap
                    }, undefined, fragFunc);
                    break;
                case Scene.Material.Blend.Masked:
                    shaderProgram = new Shaders.SceneLitSpecShaderProgram({
                        masked: true,
                        ignoreAlpha: false,
                        tangentSpaceNormals: switches.tangentSpaceNormals && switches.withNormalMap,
                        specular: switches.specular,
                        vertexColors: switches.vertexColors,
                        useDiffuseMap: switches.withDiffuseMap,
                        useNormalMap: switches.tangentSpaceNormals && switches.withNormalMap,
                        useSpecularMap: switches.specular && switches.withSpecularMap,
                        useAmbientMap: switches.withAmbientMap,
                        useEnvironmentMap: switches.withEnvironmentMap
                    }, undefined, fragFunc);
                case Scene.Material.Blend.Transparent:
                case Scene.Material.Blend.TransparentAdd:
                case Scene.Material.Blend.TransparentMult:
                    shaderProgram = new Shaders.SceneLitSpecShaderProgram({
                        masked: false,
                        ignoreAlpha: false,
                        tangentSpaceNormals: switches.tangentSpaceNormals && switches.withNormalMap,
                        specular: switches.specular,
                        vertexColors: switches.vertexColors,
                        useDiffuseMap: switches.withDiffuseMap,
                        useNormalMap: switches.tangentSpaceNormals && switches.withNormalMap,
                        useSpecularMap: switches.specular && switches.withSpecularMap,
                        useAmbientMap: switches.withAmbientMap,
                        useEnvironmentMap: switches.withEnvironmentMap
                    }, undefined, fragFunc);
                    break;
            }
            const shader = shaderProgram.createShader();
            return { program: shaderProgram, shader };
        }

        private compileLightTypes(lightTypes: number[]): string
        {
            const result: string[] = [];
            for (let i = 0, l = lightTypes.length; i < l; ++i)
            {
                switch (lightTypes[i])
                {
                    case 1: result.push(`    c += Light_Ambient(data, uLightPositions[${i}], uLightDirections[${i}], uLightColors[${i}]);`); break;
                    case 2: result.push(`    c += Light_Directional(data, uLightPositions[${i}], uLightDirections[${i}], uLightColors[${i}]);`); break;
                    case 3: result.push(`    c += Light_Point(data, uLightPositions[${i}], uLightDirections[${i}], uLightColors[${i}]);`); break;
                }
            }
            return result.join("\n");
        }
    }
}