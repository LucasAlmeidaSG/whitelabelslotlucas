/// <reference path="SceneObject.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    interface DebugObject
    {
        object: Scene.ISceneObject;
        killTime: number;
        uniqueGeometry: Geometry | null;
    }

    @HasCallbacks
    export class DebugLayer extends SceneObject implements Scene.IDebugLayer
    {
        @AutoDisposeOnSet protected _circleGeo: Scene.IReadonlyGeometry;
        @AutoDisposeOnSet protected _sphereGeo: Scene.IReadonlyGeometry;
        @AutoDisposeOnSet protected _lineGeo: Scene.IReadonlyGeometry;

        private _debugObjects: DebugObject[] = [];

        public constructor()
        {
            super("Debug Layer");

            this._circleGeo = DebugLayer.generateCircleGeometry();
            this._sphereGeo = Scene.Geometry.createSphereOutline(1.0, 8);
            this._lineGeo = DebugLayer.generateLineGeometry();

            Ticker.registerTickers(this);
        }

        protected static generateCircleGeometry(points: number = 36): Geometry
        {
            const g = new Geometry();
            g.vertexCount = points;
            const positions = new Float32Array(points * 4);
            const colors = new Uint8Array(points * 4);
            for (let i = 0; i < points; ++i)
            {
                const angle = Math.Angles.circle(i / points);
                const baseIdx = i << 2;
                positions[baseIdx + 0] = Math.sin(angle);
                positions[baseIdx + 1] = Math.cos(angle);
                positions[baseIdx + 2] = 0.0;
                positions[baseIdx + 3] = 1.0;
                colors[baseIdx + 0] = 255;
                colors[baseIdx + 1] = 255;
                colors[baseIdx + 2] = 255;
                colors[baseIdx + 3] = 255;
            }
            g.vertexBindings.push({
                type: Scene.VertexBindingType.Position,
                bindingID: 0,
                vertexSize: 4,
                vertexData: positions
            });
            g.vertexBindings.push({
                type: Scene.VertexBindingType.Color,
                bindingID: 0,
                vertexSize: 4,
                vertexData: colors
            });
            const indices = new Uint16Array(points * 2);
            for (let i = 0; i < points; ++i)
            {
                indices[i << 1 | 0] = i;
                indices[i << 1 | 1] = (i + 1) % points;
            }
            g.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: indices
            });
            g.recalculateBounds();
            g.validate(false);
            return g;
        }

        protected static generateLineGeometry(): Geometry
        {
            const g = new Geometry();
            g.vertexCount = 2;
            g.vertexBindings.push({
                type: Scene.VertexBindingType.Position,
                bindingID: 0,
                vertexSize: 4,
                vertexData: new Float32Array([ 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 ])
            });
            g.vertexBindings.push({
                type: Scene.VertexBindingType.Color,
                bindingID: 0,
                vertexSize: 4,
                vertexData: new Uint8Array([ 255, 255, 255, 255, 255, 255, 255, 255 ])
            });
            g.indexBindings.push({
                type: Scene.PrimitiveType.Line,
                indexData: new Uint16Array([ 0, 1 ])
            });
            g.recalculateBounds();
            g.validate(false);
            return g;
        }

        public drawCircle(origin: Math.ReadonlyVector3D, normal: Math.ReadonlyVector3D, radius: number, color: Util.Color = Util.Colors.red, duration: number = 0): void
        {
            const obj = new MeshObject(this._circleGeo as Geometry, [{ model: Scene.Material.Model.Unlit, diffuseTint: color, lineWidth: 2 }]);
            obj.position.set(origin);
            Math.Quaternion.fromTo(Math.Vector3D.unitZ, normal, obj.rotation);
            obj.scale.set(radius, radius, radius);
            this.addChild(obj);
            this._debugObjects.push({ object: obj, killTime: Timer.now + duration, uniqueGeometry: null });
        }

        public drawSphere(origin: Math.ReadonlyVector3D, radius: number, color: Util.Color = Util.Colors.red, duration: number = 0.0): void
        {
            const obj = new MeshObject(this._sphereGeo as Geometry, [{ model: Scene.Material.Model.Unlit, diffuseTint: color, lineWidth: 2 }]);
            obj.position.set(origin);
            obj.scale.set(radius, radius, radius);
            this.addChild(obj);
            this._debugObjects.push({ object: obj, killTime: Timer.now + duration, uniqueGeometry: null });
        }

        public drawLine(a: Math.ReadonlyVector3D, b: Math.ReadonlyVector3D, color: Util.Color = Util.Colors.red, duration: number = 0.0): void
        {
            const obj = new MeshObject(this._lineGeo as Geometry, [{ model: Scene.Material.Model.Unlit, diffuseTint: color, lineWidth: 2 }]);
            obj.position.set(a);
            const between = Math.Vector3D.subtract(b, a);
            const length = Math.Vector3D.size(between);
            obj.scale.set(1.0, length, 1.0);
            Math.Vector3D.divide(between, length, between);
            Math.Quaternion.fromTo(Math.Vector3D.unitY, between, obj.rotation);
            this.addChild(obj);
            this._debugObjects.push({ object: obj, killTime: Timer.now + duration, uniqueGeometry: null });
        }

        public drawPath(a: Bezier.Path, origin: Math.ReadonlyVector3D, normal: Math.ReadonlyVector3D, color?: Util.Color, duration?: number): void
        {
            const geo = Scene.Geometry.createDebugBezier(a, 0.1);
            const obj = new MeshObject(geo as Geometry, [{ model: Scene.Material.Model.Unlit, diffuseTint: color, lineWidth: 2 }]);
            obj.position.set(origin);
            Math.Quaternion.fromTo(Math.Vector3D.unitZ, normal, obj.rotation);
            this.addChild(obj);
            this._debugObjects.push({ object: obj, killTime: Timer.now + duration, uniqueGeometry: geo as Geometry });
        }

        /** @inheritdoc */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            for (const debugObj of this._debugObjects)
            {
                if (debugObj.uniqueGeometry) { debugObj.uniqueGeometry.dispose(); }
                debugObj.object.dispose();
            }
            Ticker.unregisterTickers(this);
            super.dispose();
        }

        @Tick({ kind: Ticker.Kind.Render, priority: Ticker.Priority.PostRender })
        protected tick(ev: Ticker.Event): void
        {
            const now = Timer.now;
            for (let i = this._debugObjects.length - 1; i >= 0; --i)
            {
                const data = this._debugObjects[i];
                if (now >= data.killTime)
                {
                    this.removeChild(data.object as SceneObject);
                    data.object.dispose();
                    if (data.uniqueGeometry) { data.uniqueGeometry.dispose(); }
                    this._debugObjects.splice(i, 1);
                }
            }
        }
    }

    Scene.DebugLayer = DebugLayer;
}