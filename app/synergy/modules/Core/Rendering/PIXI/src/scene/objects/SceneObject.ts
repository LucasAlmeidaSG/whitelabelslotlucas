/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const transformStack = [Math.Matrix4(), Math.Matrix4(), Math.Matrix4()];
    const tmpMatrix1 = Math.Matrix4(), tmpMatrix2 = Math.Matrix4(), tmpMatrix3 = Math.Matrix4(), tmpMatrix4 = Math.Matrix4(), tmpMatrix5 = Math.Matrix4(), tmpMatrix6 = Math.Matrix4();
    const tmpVec = Math.Vector3D();
    const tmpCorners = new Array<Math.Vector3D>(8);
    const tmpObjArr: (Scene.ISceneObject | null)[] = [null, null];

    export const SceneObjectTracker = new Util.ObjectTracker<SceneObject>();
    Scene.SceneObjectTracker = SceneObjectTracker;

    @HasCallbacks
    @Util.Tracked(SceneObjectTracker)
    export class SceneObject implements Scene.ISceneObject
    {
        protected static _boundsGeometry = Scene.Geometry.createCubeOutline(Math.Vector3D(1.0, 1.0, 1.0));
        protected static _boundsMaterial: Scene.Material = { model: Scene.Material.Model.Unlit, diffuseTint: Util.Colors.red };

        /** Published when the parent of this object has changed. */
        public readonly onParentChanged = createEvent<Scene.ISceneObject | null>();

        /** Published when the transform for this object has been updated. */
        public readonly onTransformUpdated = createSimpleEvent();

        /** Gets the position of this object. */
        public readonly position = new Math.ObservableVector3D(Math.Vector3D.zero);

        /** Gets the rotation of this object. */
        public readonly rotation = new Math.ObservableQuaternion(Math.Quaternion.identity);

        /** Gets the scale of this object. */
        public readonly scale = new Math.ObservableVector3D(Math.Vector3D(1.0, 1.0, 1.0));

        public get debugBounds() { return this._debugBounds != null; }
        public set debugBounds(value)
        {
            if (this.debugBounds === value) { return; }
            this._debugBounds = value ? Rendering.Scene.ISceneObject.debugBounds(this) : null;
        }

        public readonly onRightClicked = createEvent<Scene.InteractionEventData>();
        public readonly onRightDown = createEvent<Scene.InteractionEventData>();
        public readonly onRightUp = createEvent<Scene.InteractionEventData>();
        public readonly onRightUpOutside = createEvent<Scene.InteractionEventData>();

        public readonly onClicked = createEvent<Scene.InteractionEventData>();
        public readonly onClickCancelled = createEvent<Scene.InteractionEventData>();
        public readonly onDown = createEvent<Scene.InteractionEventData>();
        public readonly onMoved = createEvent<Scene.InteractionEventData>();
        public readonly onOut = createEvent<Scene.InteractionEventData>();
        public readonly onOver = createEvent<Scene.InteractionEventData>();
        public readonly onUp = createEvent<Scene.InteractionEventData>();
        public readonly onUpOutside = createEvent<Scene.InteractionEventData>();

        protected _parent: SceneObject | null;
        protected _children: SceneObject[] = [];
        protected _interactive: boolean = false;
        protected _interactiveChildren: boolean = true;
        protected _cursor: Cursor = Cursor.Default;
        protected _isDisposed: boolean = false;

        protected _localTransform = Math.Matrix4.clone(Math.Matrix4.identity);
        protected _localTransformDirty: boolean = false;

        protected _worldTransform = Math.Matrix4.clone(Math.Matrix4.identity);
        protected _worldTransformDirty: boolean = false;

        protected _localBounds = Math.AABB3D();
        protected _worldBounds = Math.AABB3D();
        protected _worldBoundsDirty: boolean = false;

        protected _displayBounds: boolean = false;

        protected _visible: boolean = true;
        protected _name: string | null;

        @AutoDisposeOnSet
        protected _debugBounds: Rendering.Scene.ISceneObject;

        /** Gets the root parent of this object. */
        public get rootParent(): SceneObject
        {
            const p = this.parent;
            if (p == null) { return null; }
            return p.rootParent || p;
        }

        /** Gets or sets the parent of this object. */
        public get parent(): SceneObject { return this._parent; }
        public set parent(value: SceneObject)
        {
            if (value === this._parent) { return; }
            const oldParent = this._parent;
            if (oldParent != null)
            {
                const idx = oldParent._children.indexOf(this);
                if (idx !== -1) { oldParent._children.splice(idx, 1); }
            }
            this._parent = value;
            if (value != null)
            {
                const idx = value._children.indexOf(this);
                if (idx === -1) { value._children.push(this); }
            }
            this._worldTransformDirty = true;
            this._worldBoundsDirty = true;
            this.onParentChanged.publish(value);
        }

        /** Gets a view of the children in this object. */
        public get children(): ReadonlyArray<SceneObject> { return this._children; }

        /** Gets the local bounds of this object. */
        public get localBounds() { return this._localBounds; }

        /** Gets the world bounds of this object. */
        public get worldBounds()
        {
            if (this._worldBoundsDirty) { this.updateWorldBounds(); }
            return this._worldBounds;
        }

        /** Gets or sets if bounds should be displayed for debugging purposes. */
        public get displayBounds() { return this._displayBounds; }
        public set displayBounds(value) { this._displayBounds = value; }

        /** Gets or sets if this object is visible. */
        public get visible() { return this._visible; }
        public set visible(value) { this._visible = value; }

        /** Gets or sets whether this object should respond to interaction events.  */
        public get interactive() { return this._interactive; }
        public set interactive(value) { this._interactive = value; }

        /** Gets or sets whether this object should pass interaction events down to children. */
        public get interactiveChildren() { return this._interactiveChildren; }
        public set interactiveChildren(value) { this._interactiveChildren = value; }

        /** Gets or sets the cursor to show when this object is hovered. */
        public get cursor() { return this._cursor; }
        public set cursor(value) { this._cursor = value; }

        /** Gets if this object has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        public get name(): string { return this._name; }
        public set name(value: string) { this._name = value; }

        public constructor(name?: string)
        {
            this.name = name;

            this.position.onChanged(this.handlePositionChanged);
            this.rotation.onChanged(this.handleRotationChanged);
            this.scale.onChanged(this.handleScaleChanged);
        }

        /** Gets the local-to-parent transform of this object. */
        public getLocalTransform(out?: Math.Matrix4): Math.Matrix4
        {
            if (this._localTransformDirty) { this.updateLocalTransform(); }
            out = out || Math.Matrix4();
            Math.Matrix4.copy(this._localTransform, out);
            return out;
        }

        /** Sets the local-to-parent transform of this object. */
        public setLocalTransform(mtx: Math.Matrix4): void;

        /** Sets the local-to-parent transform of this object. */
        public setLocalTransform(transform: Math.Transform3D): void;

        public setLocalTransform(p1: Math.Matrix4 | Math.Transform3D): void
        {
            if (Is.matrix4(p1))
            {
                Math.Matrix4.decompose(p1, this.position, this.rotation, this.scale);
            }
            else
            {
                Math.Transform3D.decompose(p1, this.position, this.rotation, this.scale);
            }
        }

        /** Gets the local-to-world transform of this object. */
        public getWorldTransform(out?: Math.Matrix4): Math.Matrix4
        {
            if (this._worldTransformDirty) { this.updateWorldTransform(); }
            out = out || Math.Matrix4();
            Math.Matrix4.copy(this._worldTransform, out);
            return out;
        }

        /**
         * Sets the local-to-world transform of this object.
         * Note that this will only affect the local transform as needed to achieve the world transform, without modifying any parent transforms.
         */
        public setWorldTransform(mtx: Math.Matrix4): void;

        /**
         * Sets the local-to-world transform of this object.
         * Note that this will only affect the local transform as needed to achieve the world transform, without modifying any parent transforms.
         */
        public setWorldTransform(transform: Math.Transform3D): void;

        public setWorldTransform(p1: Math.Matrix4 | Math.Transform3D): void
        {
            if (!Is.matrix4(p1))
            {
                p1 = Math.Transform3D.toMatrix(p1, tmpMatrix1);
            }
            if (this._parent)
            {
                // We need to convert p1 from local-to-world to local-to-parent
                // We can do this by multiplying p1 by the parent's world-to-local matrix
                const parentLocalToWorld = this._parent.getWorldTransform(tmpMatrix2);
                const parentWorldToLocal = Math.Matrix4.inverse(parentLocalToWorld, tmpMatrix3);
                const newLocalToParent = Math.Matrix4.mul(parentWorldToLocal, p1, tmpMatrix2);
                this.setLocalTransform(newLocalToParent);
            }
            else
            {
                this.setLocalTransform(p1);
            }
        }

        /**
         * Gets the local-to-local transform of this object relative to another object.
         */
        public getRelativeTransform(relativeTo: Scene.ISceneObject, out?: Math.Matrix4): Math.Matrix4
        {
            out = out || Math.Matrix4();
            const ourLocalToWorld = this.getWorldTransform(tmpMatrix1);
            const otherLocalToWorld = relativeTo.getWorldTransform(tmpMatrix2);
            const otherWorldToLocal = Math.Matrix4.inverse(otherLocalToWorld, tmpMatrix3);
            Math.Matrix4.mul(otherWorldToLocal, ourLocalToWorld, out);
            return out;
        }

        /**
         * Sets the local-to-world transform of this object to be the specified transform relative to the specified object.
         * Note that this will only affect the local transform as needed, without modifying any parent transforms.
         */
        public setRelativeTransform(mtx: Math.Matrix4, relativeTo: Scene.ISceneObject): void;

        /**
         * Sets the local-to-world transform of this object to be the specified transform relative to the specified object.
         * Note that this will only affect the local transform as needed, without modifying any parent transforms.
         */
        public setRelativeTransform(transform: Math.Transform3D, relativeTo: Scene.ISceneObject): void;

        public setRelativeTransform(p1: Math.Matrix4 | Math.Transform3D, relativeTo: Scene.ISceneObject): void
        {
            if (!Is.matrix4(p1))
            {
                p1 = Math.Transform3D.toMatrix(p1, tmpMatrix4);
            }
            const relativeObjLocalToWorld = relativeTo.getWorldTransform(tmpMatrix5);
            const newLocalToWorld = Math.Matrix4.mul(relativeObjLocalToWorld, p1, tmpMatrix6);
            this.setWorldTransform(newLocalToWorld);
        }

        /** Transforms a point from world-space to local-space. */
        public toLocal(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D;

        /** Transforms a point from another object's local-space to this object's local-space. */
        public toLocal(point: Math.ReadonlyVector3D, from: Scene.ISceneObject, out?: Math.Vector3D): Math.Vector3D;

        public toLocal(point: Math.ReadonlyVector3D, p2?: Math.Vector3D | Scene.ISceneObject, p3?: Math.Vector3D): Math.Vector3D
        {
            let out: Math.Vector3D;
            let from: Scene.ISceneObject;
            if (p2 instanceof SceneObject)
            {
                out = p3 || Math.Vector3D();
                from = p2;
            }
            else if (Is.vector3D(p2))
            {
                out = p2 || Math.Vector3D();
                from = this.rootParent;
            }
            else
            {
                out = Math.Vector3D();
                from = this.rootParent;
            }
            if (from)
            {
                tmpObjArr[0] = this;
                tmpObjArr[1] = from;
                const commonGrandparent = Scene.ISceneObject.findCommonGrandparent(tmpObjArr, true);
                if (commonGrandparent == null) { throw new Error("Objects do not share a common scene root"); }
                let p = from;
                Math.Vector3D.copy(point, out);
                while (p !== commonGrandparent)
                {
                    p.getLocalTransform(tmpMatrix1);
                    Math.Vector3D.copy(out, tmpVec);
                    Math.Matrix4.transform(tmpMatrix1, tmpVec, out);
                    p = p.parent;
                }
                // out is now in "commonGrandparent" space
                p = this;
                Math.Matrix4.copy(Math.Matrix4.identity, tmpMatrix3);
                while (p !== commonGrandparent)
                {
                    p.getLocalTransform(tmpMatrix1);
                    Math.Matrix4.copy(tmpMatrix3, tmpMatrix2);
                    Math.Matrix4.mul(tmpMatrix1, tmpMatrix2, tmpMatrix3);
                    p = p.parent;
                }
                Math.Matrix4.inverse(tmpMatrix3, tmpMatrix2);
                Math.Vector3D.copy(out, tmpVec);
                Math.Matrix4.transform(tmpMatrix2, tmpVec, out);
            }
            else
            {
                if (this._worldTransformDirty) { this.updateWorldTransform(); }
                Math.Matrix4.inverse(this._worldTransform, tmpMatrix1);
                Math.Matrix4.transform(tmpMatrix1, point, out);
            }
            return out;
        }

        /** Tranforms a point from local-space to world-space. */
        public toWorld(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D
        {
            out = out || Math.Vector3D();
            if (this._worldTransformDirty) { this.updateWorldTransform(); }
            Math.Matrix4.transform(this._worldTransform, point, out);
            return out;
        }

        public getChild(filter: (element: SceneObject) => boolean, deep: boolean = false): SceneObject
        {
            for (const child of this._children)
            {
                if (filter(child))
                {
                    return child;
                }
            }

            if (deep)
            {
                for (const child of this._children)
                {
                    if (child instanceof DisplayObjectContainerBase)
                    {
                        const subChild = child.getChild(filter, true);
                        if (subChild) { return subChild; }
                    }
                }
            }

            return null;
        }

        public getChildAt(index: number): SceneObject
        {
            return this.children[index];
        }

        public getChildByName(name: string, deep: boolean = false): SceneObject
        {
            return this.getChild((element) => element.name === name, deep);
        }

        /** Gets if this scene contains a specific child. */
        public hasChild(child: SceneObject, deep?: boolean): boolean
        {
            if (deep)
            {
                let parent = this.parent;
                while (parent)
                {
                    if ((parent as SceneObject)._children)
                    {
                        if ((parent as SceneObject)._children.indexOf(child) != -1)
                        {
                            return true;
                        }
                    }
                    parent = parent.parent;
                }

                return this.checkChildren(this as SceneObject, child);
            }
            return this._children.indexOf(child) !== -1;
        }

        /** Gets the index of the specified child, or -1 if it's not found. */
        public getChildIndex(child: SceneObject): number
        {
            return this._children.indexOf(child);
        }

        /**
         * Sets the index of the specified child.
         * Use negative indices to indicate offset from the end.
         */
        public setChildIndex(child: SceneObject, index: number): this
        {
            if (this.getChildIndex(child) !== -1)
            {
                if (index < 0)
                {
                    index = 0;
                }
                if (index > this._children.length - 1)
                {
                    index = this._children.length - 1;
                }
                const curIndex: number = this.getChildIndex(child);
                this._children.splice(curIndex, 1);
                this._children.splice(index, 0, child);
            }
            return this;
        }

        /** Sets the parent of the specified object to this object. */
        public addChild(child: SceneObject, index?: number): this
        {
            if (child === this as SceneObject)
            {
                RS.Log.error("[addChild] - Tried adding self to parent");
                return this;
            }

            if (this.getChildIndex(child) === -1)
            {
                if (child.parent)
                {
                    child.parent.removeChild(child);
                }

                if (index > -1)
                {
                    this._children.splice(index, 0, child);
                }
                else
                {
                    this._children.push(child);
                }

                child.parent = this;
            }
            this.dirtyWorldBounds();
            return this;
        }

        public addChildUnder(childToAdd: SceneObject, targetChild: SceneObject): this
        {
            const index = this.getChildIndex(targetChild);
            if (index > -1) { this.addChild(childToAdd, index); }
            return this;
        }

        public addChildAbove(childToAdd: SceneObject, targetChild: SceneObject): this
        {
            const index = this.getChildIndex(targetChild);
            if (index > -1) { this.addChild(childToAdd, index + 1); }
            return this;
        }

        public addChildren(children: ReadonlyArray<SceneObject>, index?: number): this;
        public addChildren(...children: SceneObject[]): this;
        public addChildren(...children: any[]): this
        {
            let index: number = -1;
            if (RS.Is.array(children[0]))
            {
                index = children[1] ? children[1] : -1;
                children = children[0];
            }
            for (let i: number = 0; i < children.length; i++)
            {
                if (index !== -1)
                {
                    this.addChild(children[i], index + i);
                }
                else
                {
                    this.addChild(children[i]);
                }
            }
            return this;
        }

        public removeChild(child: SceneObject): this
        {
            if (this._children.indexOf(child) !== -1)
            {
                child.parent = null;
            }
            this.dirtyWorldBounds();
            return this;
        }

        public removeChildren(children: ReadonlyArray<SceneObject>): this;
        public removeChildren(...children: SceneObject[]): this;
        public removeChildren(...children: any[]): this
        {
            if (children.length == 0) { Log.warn("[DisplayObjectContainer] removeChildren() called with no arguments - did you mean removeAllChildren()?"); }
            if (RS.Is.array(children[0])) { children = children[0]; }

            children = children.concat();
            for (let i: number = 0; i < children.length; i++)
            {
                this.removeChild(children[i]);
            }

            return this;
        }

        public removeAllChildren(): void
        {
            const tmp = [...this._children];
            for (const child of tmp)
            {
                child.parent = null;
            }
            this._children.length = 0;
        }

        public removeChildAt(index: number): SceneObject
        {
            const child = this.children[index];
            if (child)
            {
                this.removeChild(child);
            }
            return child;
        }

        public removeChildRange(startIndex: number, endIndex: number): SceneObject[]
        {
            const range = endIndex - startIndex;
            if (range > 0 && range <= endIndex)
            {
                const removed = this.children.concat().splice(startIndex, range);
                for (let i: number = 0; i < removed.length; i++)
                {
                    this.removeChild(removed[i]);
                }
                return removed;
            }
            return [];
        }

        /** Disposes this object. */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this.parent = null;
            while (this._children.length > 0)
            {
                const child = this._children[0];
                if (child._parent === this) { child.parent = null; }
            }
            this._children.length = 0;
            this._children = null;
            this._isDisposed = true;
        }

        public render(ctx: RenderContext): void
        {
            // Check visible
            if (!this._visible) { return; }

            // Render children
            for (const child of this._children)
            {
                child.render(ctx);
            }

            // Render bounds
            if (this._displayBounds)
            {
                const wb = this.worldBounds;
                Math.Matrix4.copy(Math.Matrix4.identity, transformStack[0]);
                Math.Vector3D.set(tmpVec, wb.maxX - wb.minX, wb.maxY - wb.minY, wb.maxZ - wb.minZ);
                Math.Matrix4.createScale(tmpVec, transformStack[1]);
                Math.Vector3D.set(tmpVec, (wb.maxX + wb.minX) * 0.5, (wb.maxY + wb.minY) * 0.5, (wb.maxZ + wb.minZ) * 0.5);
                Math.Matrix4.createTranslation(tmpVec, transformStack[2]);
                Math.Matrix4.combine(transformStack, tmpMatrix1);
                ctx.addGeometry(tmpMatrix1, wb, SceneObject._boundsGeometry as Geometry, SceneObject._boundsMaterial, Util.Colors.white, 0);
            }
        }

        public findIntersecting(aabb: Math.ReadonlyAABB3D, out?: Scene.ISceneObject[]): Scene.ISceneObject[]
        {
            out = out || [];
            for (const child of this._children)
            {
                const childBounds = child.worldBounds;
                if (Math.AABB3D.intersects(aabb, childBounds))
                {
                    out.push(child);
                    child.findIntersecting(aabb, out);
                }
            }
            return out;
        }

        /** Finds the first interactive child of this object that intersects the specified line. */
        public interactionRaycast(ctx: Scene.ISceneObject.InteractionRaycastContext): void
        {
            const rootParent = this.rootParent;
            const ourIntersectPt = Math.Vector3D();

            try
            {
                // Test against our AABB unless we're the scene root
                if (rootParent)
                {
                    const bounds = this.worldBounds;
                    if (!Math.Line3D.intersects(ctx.line, bounds, ourIntersectPt)) { return null; }
                }

                // Test against children
                if (this._interactiveChildren)
                {
                    for (const child of this._children)
                    {
                        child.interactionRaycast(ctx);
                    }
                }

                // Test against us
                if (this._interactive)
                {
                    Scene.ISceneObject.InteractionRaycastContext.setPoint(ctx, this, ourIntersectPt);
                }
            }
            finally
            {
                Math.Vector3D.free(ourIntersectPt);
            }
        }

        @Callback
        protected handlePositionChanged(): void
        {
            this.dirtyLocalTransform();
        }

        @Callback
        protected handleRotationChanged(): void
        {
            this.dirtyLocalTransform();
        }

        @Callback
        protected handleScaleChanged(): void
        {
            this.dirtyLocalTransform();
        }

        protected dirtyLocalTransform(): void
        {
            if (this._localTransformDirty) { return; }
            this._localTransformDirty = true;
            this.dirtyWorldTransform();
        }

        protected dirtyWorldTransform(): void
        {
            if (this._worldTransformDirty) { return; }
            this._worldTransformDirty = true;
            for (const child of this._children)
            {
                child.dirtyWorldTransform();
            }
            this.dirtyWorldBounds();
        }

        protected dirtyWorldBounds(): void
        {
            if (this._worldBoundsDirty) { return; }
            this._worldBoundsDirty = true;
            if (this._parent) { this._parent.dirtyWorldBounds(); }
        }

        protected updateLocalTransform(): void
        {
            Math.Matrix4.createScale(this.scale, transformStack[0]);
            Math.Matrix4.createRotation(this.rotation, transformStack[1]);
            Math.Matrix4.createTranslation(this.position, transformStack[2]);
            Math.Matrix4.combine(transformStack, this._localTransform);
            if (isNaN(this._localTransform[0]))
            {
                debugger;
                Math.Matrix4.combine(transformStack, this._localTransform);
            }
            this._localTransformDirty = false;
            this.dirtyWorldTransform();
        }

        protected updateWorldTransform(): void
        {
            if (this._localTransformDirty) { this.updateLocalTransform(); }
            const p = this._parent;
            if (p)
            {
                if (p._worldTransformDirty) { p.updateWorldTransform(); }
                Math.Matrix4.mul(p._worldTransform, this._localTransform, this._worldTransform);
            }
            else
            {
                Math.Matrix4.copy(this._localTransform, this._worldTransform);
            }
            this._worldTransformDirty = false;
            this.onTransformUpdated.publish();
        }

        protected updateWorldBounds(): void
        {
            if (this._worldTransformDirty) { this.updateWorldTransform(); }
            Math.AABB3D.transform(this._localBounds, this._worldTransform, this._worldBounds);
            for (const child of this._children)
            {
                const wb = child.worldBounds;
                const corners = Math.AABB3D.corners(wb, tmpCorners);
                for (const vec of corners)
                {
                    Math.AABB3D.expand(this._worldBounds, vec);
                }
            }
            this._worldBoundsDirty = false;
        }

        protected checkChildren(ob: SceneObject, child: SceneObject): boolean
        {
            let hasChild: boolean = false;
            if (ob instanceof SceneObject)
            {
                hasChild = ob.children.indexOf(child) !== -1;
                const children = ob.children;
                let i: number = 0;
                while (i < children.length && !hasChild)
                {
                    hasChild = this.checkChildren(children[i], child);
                    i++;
                }
            }
            return hasChild;
        }
    }

    Scene.SceneObject = SceneObject;
}
