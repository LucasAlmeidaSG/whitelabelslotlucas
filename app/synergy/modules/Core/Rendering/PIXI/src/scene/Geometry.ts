/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    interface GLCache
    {
        vaosByShader: PIXI.glCore.VertexArrayObject[];
        vertexBuffersByBinding: PIXI.glCore.GLBuffer[];
        indexBuffersByBinding: PIXI.glCore.GLBuffer[];
    }

    const attributePrefixes: string[][] = [];
    attributePrefixes[Scene.VertexBindingType.Position] = ["aPosition", "position", "aPos", "pos"];
    attributePrefixes[Scene.VertexBindingType.TexCoord] = ["aTexCoord", "texCoord", "aTextureCoord", "textureCoord", "aTexCoordinate", "texCoordinate", "aUV", "uv"];
    attributePrefixes[Scene.VertexBindingType.Color] = ["aColor", "color", "aCol", "col", "aColour", "colour"];
    attributePrefixes[Scene.VertexBindingType.Normal] = ["aNormal", "normal", "aNorm", "norm"];

    export class Geometry implements Scene.IGeometry
    {
        /** Gets or sets the total number of vertices in the geometry. */
        public vertexCount: number;

        /** Gets or sets a view of the vertex bindings available on this geometry. */
        public readonly vertexBindings: Scene.VertexBinding[] = [];

        /** Gets or sets a view of the index bindings available on this geometry. */
        public readonly indexBindings: Scene.IndexBinding[] = [];

        protected _isDisposed: boolean = false;
        protected _glCache: GLCache[] = [];
        protected _bounds: Math.AABB3D;

        public get isDisposed() { return this._isDisposed; }

        public get bounds()
        {
            if (this._bounds == null) { this.recalculateBounds(); }
            return this._bounds;
        }

        /**
         * Validates the data of this geometry.
         * @returns a string containing information about the failed validation or null if validation succeeded.
         */
        public validate(returnErr: true): string | null;

        /**
         * Validates the data of this geometry.
         * @returns true if succeeded.
         */
        public validate(returnErr: false): boolean;

        public validate(returnErr: boolean): boolean | string | null
        {
            if (!returnErr) { return this.validate(true) == null; }

            // There must be at least 1 position binding
            if (!this.vertexBindings.some((vb) => vb.type === Scene.VertexBindingType.Position && vb.bindingID === 0)) { return "no position vertex binding"; }

            // Each binding should match the total vertex count
            for (const vb of this.vertexBindings)
            {
                const numVerts = vb.vertexData.length / vb.vertexSize;
                if ((numVerts | 0) !== numVerts) { return `${Scene.VertexBinding.toString(vb)} - invalid vertex count (${vb.vertexData.length} does not divide into ${vb.vertexSize})`; }
                if (numVerts !== this.vertexCount) { return `${Scene.VertexBinding.toString(vb)} - invalid vertex count (binding provides ${numVerts} vertices but geometry has ${this.vertexCount} vertices)`; }
            }

            // There must be at least 1 index binding
            if (this.indexBindings.length === 0) { return "no index bindings"; }

            // The indices must have correct geometry and not refer to non-existant vertices
            for (let i = 0, l = this.indexBindings.length; i < l; ++i)
            {
                const ib = this.indexBindings[i];
                switch (ib.type)
                {
                    case Scene.PrimitiveType.Line:
                        {
                            const numPrims = ib.indexData.length / 2;
                            if ((numPrims | 0) !== numPrims) { return `index:${i} - invalid index count (${ib.indexData.length} does not divide into 2)`; }
                            break;
                        }
                    case Scene.PrimitiveType.Triangle:
                        {
                            const numPrims = ib.indexData.length / 3;
                            if ((numPrims | 0) !== numPrims) { return `index:${i} - invalid index count (${ib.indexData.length} does not divide into 3)`; }
                            break;
                        }
                }
                for (let j = 0, l2 = ib.indexData.length; j < l2; ++j)
                {
                    const idx = ib.indexData[j];
                    if (idx >= this.vertexCount) { return `index:${i} - index out of bounds (refers to higher element than ${this.vertexCount - 1})`; }
                }
            }
        }

        public getVAO(renderer: PIXI.WebGLRenderer, target: ShaderProgram, indexBinding: number): PIXI.glCore.VertexArrayObject
        {
            const glCache = this._glCache[renderer.CONTEXT_UID] || (this._glCache[renderer.CONTEXT_UID] = { vaosByShader: [], vertexBuffersByBinding: [], indexBuffersByBinding: [] });

            const gl = renderer.gl;

            let vao: PIXI.glCore.VertexArrayObject;
            if (glCache.vaosByShader[target.uid])
            {
                vao = glCache.vaosByShader[target.uid];
            }
            else
            {
                const glShader = target.getRawShaderFor(renderer);

                vao = renderer.createVao();
                vao.bind();

                for (let i = 0, l = this.vertexBindings.length; i < l; ++i)
                {
                    const vb = this.vertexBindings[i];
                    let glBuffer: PIXI.glCore.GLBuffer;
                    if (glCache.vertexBuffersByBinding[i] == null)
                    {
                        glBuffer = glCache.vertexBuffersByBinding[i] = PIXI.glCore.GLBuffer.createVertexBuffer(gl, vb.vertexData, gl.STATIC_DRAW);
                    }
                    else
                    {
                        glBuffer = glCache.vertexBuffersByBinding[i];
                    }
                    const attr = this.findAttribute(glShader, vb);
                    if (attr != null)
                    {
                        if (vb.vertexData instanceof Float32Array)
                        {
                            vao.addAttribute(glBuffer, glShader.attributes[attr], gl.FLOAT, vb.normalise, vb.vertexSize * 4, 0);
                        }
                        else if (vb.vertexData instanceof Uint8Array)
                        {
                            vao.addAttribute(glBuffer, glShader.attributes[attr], gl.UNSIGNED_BYTE, vb.normalise, vb.vertexSize * 1, 0);
                        }
                    }
                }

                glCache.vaosByShader[target.uid] = vao;
            }

            let indexBuffer = glCache.indexBuffersByBinding[indexBinding];
            if (indexBuffer == null)
            {
                indexBuffer = PIXI.glCore.GLBuffer.createIndexBuffer(gl, this.indexBindings[indexBinding].indexData, gl.STATIC_DRAW);
                glCache.indexBuffersByBinding[indexBinding] = indexBuffer;
            }
            vao.indexBuffer = indexBuffer;

            return vao;
        }

        public getVertexBinding(type: Scene.VertexBindingType, id: number): Scene.VertexBinding | null
        {
            for (const binding of this.vertexBindings)
            {
                if (binding.type === type && binding.bindingID === id)
                {
                    return binding;
                }
            }
            return null;
        }

        public recalculateBounds(): void
        {
            this._bounds = Math.AABB3D();
            const tmpVec = Math.Vector3D();
            const posBinding = this.getVertexBinding(Scene.VertexBindingType.Position, 0);
            if (posBinding == null) { return; }
            for (let i = 0; i < this.vertexCount; ++i)
            {
                let ptr = i * posBinding.vertexSize;
                if (posBinding.vertexSize >= 1) { tmpVec.x = posBinding.vertexData[ptr++]; }
                if (posBinding.vertexSize >= 2) { tmpVec.y = posBinding.vertexData[ptr++]; }
                if (posBinding.vertexSize >= 3) { tmpVec.z = posBinding.vertexData[ptr++]; }
                Math.AABB3D.expand(this._bounds, tmpVec);
            }
        }

        public recalculateTangents(): void
        {
            // Find existing bindings
            let positionBinding: Scene.VertexBinding | null = null;
            let texCoordBinding: Scene.VertexBinding | null = null;
            let normalBinding: Scene.VertexBinding | null = null;
            let tangentBinding: Scene.VertexBinding | null = null;
            let binormalBinding: Scene.VertexBinding | null = null;
            for (const binding of this.vertexBindings)
            {
                if (binding.type === Scene.VertexBindingType.Position && binding.bindingID === 0)
                {
                    positionBinding = binding;
                }
                else if (binding.type === Scene.VertexBindingType.TexCoord && binding.bindingID === 0)
                {
                    texCoordBinding = binding;
                }
                else if (binding.type === Scene.VertexBindingType.Normal)
                {
                    if (binding.bindingID === 0)
                    {
                        normalBinding = binding;
                    }
                    else if (binding.bindingID === 1)
                    {
                        tangentBinding = binding;
                    }
                    else if (binding.bindingID === 2)
                    {
                        binormalBinding = binding;
                    }
                }
            }
            if (!positionBinding) { throw new Error("Position binding not found"); }
            if (!texCoordBinding) { throw new Error("Tex coord binding not found"); }
            if (!normalBinding) { throw new Error("Normal binding not found"); }

            if (positionBinding.vertexSize < 3) { throw new Error("Position binding has invalid vertex size"); }
            if (texCoordBinding.vertexSize < 2) { throw new Error("Tex coord binding has invalid vertex size"); }
            if (normalBinding.vertexSize < 3) { throw new Error("Normal binding has invalid vertex size"); }

            let tangentBuffer: Float32Array;
            if (tangentBinding)
            {
                if (tangentBinding.vertexSize !== 3) { throw new Error("Existing tangent binding has invalid vertex size"); }
                tangentBuffer = tangentBinding.vertexData;
            }
            else
            {
                tangentBuffer = new Float32Array(this.vertexCount * 3);
                this.vertexBindings.push({
                    type: Scene.VertexBindingType.Normal,
                    bindingID: 1,
                    vertexSize: 3,
                    vertexData: tangentBuffer
                });
            }

            let binormalBuffer: Float32Array;
            if (binormalBinding)
            {
                if (binormalBinding.vertexSize !== 3) { throw new Error("Existing binormal binding has invalid vertex size"); }
                binormalBuffer = binormalBinding.vertexData;
            }
            else
            {
                binormalBuffer = new Float32Array(this.vertexCount * 3);
                this.vertexBindings.push({
                    type: Scene.VertexBindingType.Normal,
                    bindingID: 2,
                    vertexSize: 3,
                    vertexData: binormalBuffer
                });
            }

            // https://gamedev.stackexchange.com/questions/68612/how-to-compute-tangent-and-bitangent-vectors

            const tan1 = new Float32Array(this.vertexCount * 3);
            const tan2 = new Float32Array(this.vertexCount * 3);

            const v1 = Math.Vector3D(), v2 = Math.Vector3D(), v3 = Math.Vector3D();
            const w1 = Math.Vector2D(), w2 = Math.Vector2D(), w3 = Math.Vector2D();

            for (const indexBinding of this.indexBindings)
            {
                if (indexBinding.type === Scene.PrimitiveType.Triangle)
                {
                    const triCount = indexBinding.indexData.length / 3;
                    for (let i = 0; i < triCount; ++i)
                    {
                        const i1 = indexBinding.indexData[i * 3 + 0];
                        const i2 = indexBinding.indexData[i * 3 + 1];
                        const i3 = indexBinding.indexData[i * 3 + 2];

                        Math.Vector3D.set(v1, positionBinding.vertexData[i1 * positionBinding.vertexSize + 0], positionBinding.vertexData[i1 * positionBinding.vertexSize + 1], positionBinding.vertexData[i1 * positionBinding.vertexSize + 2]);
                        Math.Vector3D.set(v2, positionBinding.vertexData[i2 * positionBinding.vertexSize + 0], positionBinding.vertexData[i2 * positionBinding.vertexSize + 1], positionBinding.vertexData[i2 * positionBinding.vertexSize + 2]);
                        Math.Vector3D.set(v3, positionBinding.vertexData[i3 * positionBinding.vertexSize + 0], positionBinding.vertexData[i3 * positionBinding.vertexSize + 1], positionBinding.vertexData[i3 * positionBinding.vertexSize + 2]);

                        Math.Vector2D.set(w1, texCoordBinding.vertexData[i1 * texCoordBinding.vertexSize + 0], texCoordBinding.vertexData[i1 * texCoordBinding.vertexSize + 1]);
                        Math.Vector2D.set(w2, texCoordBinding.vertexData[i2 * texCoordBinding.vertexSize + 0], texCoordBinding.vertexData[i2 * texCoordBinding.vertexSize + 1]);
                        Math.Vector2D.set(w3, texCoordBinding.vertexData[i3 * texCoordBinding.vertexSize + 0], texCoordBinding.vertexData[i3 * texCoordBinding.vertexSize + 1]);

                        const x1 = v2.x - v1.x;
                        const x2 = v3.x - v1.x;
                        const y1 = v2.y - v1.y;
                        const y2 = v3.y - v1.y;
                        const z1 = v2.z - v1.z;
                        const z2 = v3.z - v1.z;

                        const s1 = w2.x - w1.x;
                        const s2 = w3.x - w1.x;
                        const t1 = w2.y - w1.y;
                        const t2 = w3.y - w1.y;

                        const r = 1.0 / (s1 * t2 - s2 * t1);

                        const sdirX = (t2 * x1 - t1 * x2) * r, sdirY = (t2 * y1 - t1 * y2) * r, sdirZ = (t2 * z1 - t1 * z2) * r;
                        const tdirX = (s1 * x2 - s2 * x1) * r, tdirY = (s1 * y2 - s2 * y1) * r, tdirZ = (s1 * z2 - s2 * z1) * r;

                        tan1[i1 * 3 + 0] += sdirX; tan1[i1 * 3 + 1] += sdirY; tan1[i1 * 3 + 2] += sdirZ;
                        tan1[i2 * 3 + 0] += sdirX; tan1[i2 * 3 + 1] += sdirY; tan1[i2 * 3 + 2] += sdirZ;
                        tan1[i3 * 3 + 0] += sdirX; tan1[i3 * 3 + 1] += sdirY; tan1[i3 * 3 + 2] += sdirZ;

                        tan2[i1 * 3 + 0] += tdirX; tan2[i1 * 3 + 1] += tdirY; tan2[i1 * 3 + 2] += tdirZ;
                        tan2[i2 * 3 + 0] += tdirX; tan2[i2 * 3 + 1] += tdirY; tan2[i2 * 3 + 2] += tdirZ;
                        tan2[i3 * 3 + 0] += tdirX; tan2[i3 * 3 + 1] += tdirY; tan2[i3 * 3 + 2] += tdirZ;
                    }
                }
            }

            const n = Math.Vector3D();
            const t = Math.Vector3D();

            const tmp1 = Math.Vector3D();
            const tmp2 = Math.Vector3D();

            for (let i = 0, l = this.vertexCount; i < l; ++i)
            {
                Math.Vector3D.set(n, normalBinding.vertexData[i * 3 + 0], normalBinding.vertexData[i * 3 + 1], normalBinding.vertexData[i * 3 + 2]);
                Math.Vector3D.set(t, tan1[i * 3 + 0], tan1[i * 3 + 1], tan1[i * 3 + 2]);

                // Gram-Schmidt orthogonalize
                Math.Vector3D.multiply(n, Math.Vector3D.dot(n, t), tmp1);
                Math.Vector3D.subtract(t, tmp1, tmp2);
                Math.Vector3D.normalise(tmp2);

                Math.Vector3D.write(tmp2, tangentBuffer, i * 3);

                // Binormal
                Math.Vector3D.cross(n, tmp2, tmp1);
                Math.Vector3D.normalise(tmp1);
                Math.Vector3D.write(tmp1, binormalBuffer, i * 3);

                // Calculate handedness
                // tangent[a].w = (Dot(Cross(n, t), tan2[a]) < 0.0F) ? -1.0F : 1.0F;
            }
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this.vertexBindings.length = 0;
            this.indexBindings.length = 0;
            this.clearGLCache();
            this._glCache = null;
            this._isDisposed = true;
        }

        protected findAttribute(glShader: PIXI.Shader, vb: Scene.VertexBinding): string | null
        {
            const prefixes = attributePrefixes[vb.type];
            if (prefixes == null || prefixes.length === 0) { return null; }
            for (const attr in glShader.attributes)
            {
                for (const prefix of prefixes)
                {
                    if (attr.length >= prefix.length && attr.substr(0, prefix.length).toLowerCase() === prefix.toLowerCase())
                    {
                        const postfix = attr.substr(prefix.length);
                        if (!postfix && vb.bindingID === 0) { return attr; }
                        const postfixID = parseInt(postfix);
                        if (postfixID === vb.bindingID) { return attr; }
                    }
                }
            }
            return null;
        }

        protected clearGLCache(): void
        {
            for (const glCache of this._glCache)
            {
                for (const vao of glCache.vaosByShader)
                {
                    if (vao) { vao.destroy(); }
                }
                glCache.vaosByShader.length = 0;
                for (const buff of glCache.vertexBuffersByBinding)
                {
                    if (buff) { buff.destroy(); }
                }
                glCache.vertexBuffersByBinding.length = 0;
                for (const buff of glCache.indexBuffersByBinding)
                {
                    if (buff) { buff.destroy(); }
                }
                glCache.indexBuffersByBinding.length = 0;
            }
            this._glCache.length = 0;
        }
    }
}