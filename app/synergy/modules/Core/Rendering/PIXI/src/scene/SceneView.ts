/** @internal  */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    interface GLCache
    {
        baseTexture: PIXI.BaseTexture;
        texture: PIXI.Texture;
        renderTarget: PIXI.RenderTarget;
    }

    const tmpMatrix1 = Math.Matrix4(), tmpMatrix2 = Math.Matrix4(), tmpMatrix3 = Math.Matrix4(), tmpMatrix4 = Math.Matrix4(), tmpMatrix5 = Math.Matrix4(), tmpMatrix6 = Math.Matrix4();
    const tmpTransformStack: Math.Matrix4[] = [ null, null, null, null, null, null ];
    const tmpVec1 = Math.Vector3D(), tmpVec2 = Math.Vector3D(), tmpVec3 = Math.Vector3D(), tmpVec4 = Math.Vector3D();
    const tmpColArr = [0, 0, 0, 0], whiteColArr = [1, 1, 1, 1];

    const projShadowBlendMat: Scene.Material =
    {
        model: Scene.Material.Model.Unlit,
        blend: Scene.Material.Blend.TransparentMult
    };

    const tmpInteractionRaycastContext1: Scene.ISceneObject.InteractionRaycastContext =
    {
        line: Math.Line3D(),
        hitPoint: Math.Vector3D(),
        hitDist: 0.0,
        hitObject: null
    };

    const tmpInteractionRaycastContext2: Scene.ISceneObject.InteractionRaycastContext =
    {
        line: Math.Line3D(),
        hitObject: null,
        hitPoint: Math.Vector3D(),
        hitDist: 0.0
    };

    export class SceneView extends Bitmap implements ISceneView
    {
        protected _viewportSize: Math.Size2D;
        protected _camera: Camera;
        protected _frozen: boolean = false;
        protected _renderOnce: boolean = false;
        @AutoDisposeOnSet protected _forwardRenderContext: RenderContext;
        @AutoDisposeOnSet protected _fsQuad: Geometry;

        protected _tmpRT: PIXI.RenderTexture;

        protected _mouseDown: boolean = false;
        protected _lastMousePos: Math.Vector2D | null;
        protected _hoverObject: Scene.ISceneObject | null = null;
        protected _hoverObjectIsOut: boolean = false;
        protected _hoverObjectDown: boolean = false;
        protected _hoverObjectRightDown: boolean = false;
        protected _hoverWorldPos = Math.Vector3D();
        protected _hoverLocalPos = Math.Vector3D();
        protected _lastMoveEv: InteractionEventData | null = null;
        protected _lastSceneMoveEv: Scene.InteractionEventData | null = null;
        protected _debugLayer: Scene.IDebugLayer | null = null;

        private _glCache: GLCache[] = [];

        /** Gets or sets the size of the viewport. */
        public get viewportSize() { return this._viewportSize; }
        public set viewportSize(value) { this._viewportSize = value; }

        /** Gets or sets the camera for this view. */
        public get camera() { return this._camera; }
        public set camera(value) { this._camera = value; }

        /**
         * Gets or sets whether to redraw the scene.
         * If false, the scene will still be displayed to the screen, just not updated.
         * This will help performance if nothing is changing in the scene.
         */
        public get frozen() { return this._frozen; }
        public set frozen(value) { this._frozen = value; }

        // /**
        //  * Gets or sets the debug layer for this scene view.
        //  * The debug layer will be used to draw interaction raycasts.
        //  */
        // public get debugLayer() { return this._debugLayer; }
        // public set debugLayer(value) { this._debugLayer = value; }

        public constructor(viewportSize: Math.Size2D, camera: Camera)
        {
            super(null);

            this._viewportSize = viewportSize;
            this._camera = camera;
            this._forwardRenderContext = new RenderContext();
            this._fsQuad = Scene.Geometry.createFullScreenQuad() as Geometry;

            Ticker.registerTickers(this);

            this.onDown(this.handleDown);
            this.onUp(this.handleUp);
            this.onOver(this.handleOver);
            this.onMoved(this.handleMoved);
            this.onOut(this.handleOut);
            this.onClicked(this.handleClicked);
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            for (const glCache of this._glCache)
            {
                glCache.renderTarget.destroy();
                glCache.renderTarget = null;
                glCache.texture = null;
            }
            this._glCache.length = 0;
            this._glCache = null;
            Ticker.unregisterTickers(this);
            super.dispose();
        }

        /** Transforms the specified point in world space to a 2D viewport coordinate. */
        public projectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector2D): Math.Vector2D
        {
            out = out || Math.Vector2D();

            this._camera.projectPoint(point, out);

            out.x *= this._viewportSize.w;
            out.y *= this._viewportSize.h;

            return out;
        }

        /** Transforms the specified point in 2D viewport coordinate to world space. */
        public unprojectPoint(point: Math.ReadonlyVector3D, out?: Math.Vector3D): Math.Vector3D
        {
            out = out || Math.Vector3D();

            tmpVec1.x = point.x / this._viewportSize.w;
            tmpVec1.y = point.y / this._viewportSize.h;
            tmpVec1.z = point.z;

            this._camera.unprojectPoint(tmpVec1, out);

            return out;
        }

        /** Ensures that the scene is redrawn next frame. This is only relevant if frozen. */
        public renderOnce(): void
        {
            this._renderOnce = true;
        }

        @Callback
        protected handleDown(ev: InteractionEventData)
        {
            this._mouseDown = true;
            this._lastMousePos = Math.Vector2D.clone(ev.localPosition);
            this.sceneHoverUpdate();
            if (this._hoverObject && !this._hoverObjectDown && !this._hoverObjectIsOut)
            {
                this._hoverObjectDown = true;
                this._hoverObject.onDown.publish({ ...this._lastSceneMoveEv, ...ev });
            }
            // this.debugDrawInteractionRaycast(5000);
        }

        @Callback
        protected handleUp(ev: InteractionEventData)
        {
            this._mouseDown = false;
            if (this._hoverObject && this._hoverObjectDown)
            {
                this._hoverObjectDown = false;
                if (this._hoverObjectIsOut)
                {
                    this._hoverObject.onUpOutside.publish({ ...this._lastSceneMoveEv, ...ev });
                    this._hoverObjectIsOut = false;
                    if (!this._hoverObjectRightDown) { this._hoverObject = null; }
                }
                else
                {
                    this._hoverObject.onUp.publish({ ...this._lastSceneMoveEv, ...ev });
                }
            }
            // this.debugDrawInteractionRaycast(5000);
        }

        @Callback
        protected handleOver(ev: InteractionEventData)
        {
            this._lastMousePos = Math.Vector2D.clone(ev.localPosition);
        }

        @Callback
        protected handleMoved(ev: InteractionEventData)
        {
            this._lastMoveEv = ev;
            this.sceneHoverUpdate();
            if (this._hoverObject) { this._hoverObject.onMoved.publish(this._lastSceneMoveEv); }
            if (!this._lastMousePos) { return; }
            const dx = ev.localPosition.x - this._lastMousePos.x;
            const dy = ev.localPosition.y - this._lastMousePos.y;
            Math.Vector2D.copy(ev.localPosition, this._lastMousePos);
            if (this._mouseDown && this._camera instanceof FlyCamera && !(this._hoverObjectDown || this._hoverObjectRightDown))
            {
                this._camera.rotate(dx, dy);
            }
        }

        @Callback
        protected handleOut(ev: InteractionEventData)
        {
            this._lastMousePos = null;
            if (this._hoverObject)
            {
                if (!this._hoverObjectIsOut)
                {
                    this._hoverObject.onOut.publish({ ...this._lastSceneMoveEv, ...ev });
                }
                if (this._hoverObjectDown)
                {

                    this._hoverObject.onUpOutside.publish({ ...this._lastSceneMoveEv, ...ev });
                    this._hoverObjectDown = false;
                }
                // TODO: Right down
                this._hoverObject = null;
                this._hoverObjectIsOut = false;
            }
        }

        @Callback
        protected handleClicked(ev: InteractionEventData)
        {
            if (this._hoverObject && !this._hoverObjectIsOut)
            {
                this._hoverObject.onClicked.publish({ ...this._lastSceneMoveEv, ...ev });
            }
        }

        protected interactionRaycast(out: Scene.ISceneObject.InteractionRaycastContext): void
        {
            const sceneRoot = this._camera.rootParent;
            const cameraTransform = this._camera.getWorldTransform(tmpMatrix1);
            const cameraWorldPos = Math.Matrix4.transform(cameraTransform, Math.Vector3D.zero, tmpVec4);
            tmpVec2.x = this._lastMousePos.x;
            tmpVec2.y = this._lastMousePos.y;
            tmpVec2.z = 1.0;
            this.unprojectPoint(tmpVec2, tmpVec3);
            Math.Vector3D.subtract(tmpVec3, cameraWorldPos, tmpVec2);
            Math.Line3D.set(out.line, cameraWorldPos, tmpVec2);
            out.hitDist = Infinity;
            out.hitObject = null;
            Math.Vector3D.copy(Math.Vector3D.zero, out.hitPoint);
            sceneRoot.interactionRaycast(out);
        }

        // protected debugDrawInteractionRaycast(duration: number): void
        // {
        //     if (!this._debugLayer) { return; }
        //     this.interactionRaycast(tmpInteractionRaycastContext1);
        //     Math.Vector3D.addMultiply(tmpInteractionRaycastContext1.line, Math.Line3D.direction(tmpInteractionRaycastContext1.line, tmpVec1), 128, tmpVec2);
        //     this._debugLayer.drawLine(tmpInteractionRaycastContext1.line, tmpVec2, RS.Util.Colors.red, duration);
        // }

        @RS.Tick({ kind: Ticker.Kind.Render })
        protected sceneHoverUpdate(): void
        {
            if (!this.interactive || !this._camera || !this._lastMousePos) { return; }
            const sceneRoot = this._camera.rootParent;
            if (!sceneRoot.interactive && !sceneRoot.interactiveChildren) { return; }
            this.interactionRaycast(tmpInteractionRaycastContext1);
            const obj = tmpInteractionRaycastContext1.hitObject;
            Scene.ISceneObject.InteractionRaycastContext.copy(tmpInteractionRaycastContext1, tmpInteractionRaycastContext2);
            Math.Vector3D.copy(tmpInteractionRaycastContext1.hitPoint, this._hoverWorldPos);
            if (obj)
            {
                obj.toLocal(this._hoverWorldPos, this._hoverLocalPos);
            }
            else
            {
                Math.Vector3D.copy(Math.Vector3D.zero, this._hoverLocalPos);
            }
            this._lastSceneMoveEv = {
                ...this._lastMoveEv,
                sceneTarget: obj,
                globalScenePosition: this._hoverWorldPos,
                localScenePosition: this._hoverLocalPos,
                raycastData: tmpInteractionRaycastContext2
            };
            if (this._hoverObject && this._hoverObject !== obj)
            {
                if (!this._hoverObjectIsOut) { this._hoverObject.onOut.publish(this._lastSceneMoveEv); }
                if (!this._hoverObjectDown)
                {
                    this._hoverObject = null;
                    this._hoverObjectIsOut = false;
                }
                else
                {
                    this._hoverObjectIsOut = true;
                }
            }
            if (obj && (this._hoverObjectIsOut || !this._hoverObject))
            {
                this._hoverObject = obj;
                this._hoverObjectIsOut = false;
                obj.onOver.publish(this._lastSceneMoveEv);
            }
            if (obj)
            {
                this.cursor = obj.cursor;
            }
            else
            {
                this.cursor = Cursor.Default;
            }
        }

        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): PIXI.Sprite; }, args: any[]): PIXI.Sprite
        {
            const spr = super.createClass(classType, args);
            spr.renderWebGL = (renderer) =>
            {
                renderer.currentRenderer.stop();
                const oldRT = renderer._activeRenderTarget;
                this.render(renderer);
                renderer.bindRenderTarget(oldRT);
                const glCache = this._glCache[renderer.CONTEXT_UID];
                if (glCache != null)
                {
                    this.nativeObject.texture = null;
                    this.nativeObject.texture = glCache.texture;
                    PIXI.Sprite.prototype.renderWebGL.call(this._nativeObject, renderer);
                }
            };
            return spr;
        }

        protected render(renderer: PIXI.WebGLRenderer): void
        {
            const gl = renderer.gl;
            const glCache = this._glCache[renderer.CONTEXT_UID] || (this._glCache[renderer.CONTEXT_UID] = { texture: null, renderTarget: null, baseTexture: null });
            let firstTime = false;
            if (glCache.renderTarget == null)
            {
                glCache.renderTarget = new PIXI.RenderTarget(gl, this._viewportSize.w, this._viewportSize.h, PIXI.SCALE_MODES.LINEAR, 1.0, false);
                renderer.bindRenderTarget(glCache.renderTarget);
                glCache.renderTarget.attachStencilBuffer();
                glCache.baseTexture = new PIXI.BaseTexture(null);
                glCache.baseTexture.realWidth = glCache.baseTexture.width = glCache.renderTarget.size.width;
                glCache.baseTexture.realHeight = glCache.baseTexture.height = glCache.renderTarget.size.height;
                glCache.baseTexture.hasLoaded = true;
                glCache.baseTexture["_glTextures"][renderer.CONTEXT_UID] = glCache.renderTarget.texture;
                glCache.texture = new PIXI.Texture(glCache.baseTexture);
                firstTime = true;
            }
            else if (glCache.renderTarget.size.width !== this._viewportSize.w || glCache.renderTarget.size.height !== this._viewportSize.h)
            {
                renderer.bindRenderTarget(glCache.renderTarget);
                glCache.renderTarget.resize(this._viewportSize.w, this._viewportSize.h);
                glCache.baseTexture.realWidth = glCache.baseTexture.width = glCache.renderTarget.size.width;
                glCache.baseTexture.realHeight = glCache.baseTexture.height = glCache.renderTarget.size.height;
                glCache.baseTexture.emit("update", glCache.baseTexture);
                // BUG: Pixi doesn't resize the viewport if the currently bound RT is resized, leaving the old viewport in play for any renders up until the next RT bind
                // So, we set a new viewport ourself
                gl.viewport(
                    glCache.renderTarget.destinationFrame.x | 0,
                    glCache.renderTarget.destinationFrame.y | 0,
                    (glCache.renderTarget.destinationFrame.width * glCache.renderTarget.resolution) | 0,
                    (glCache.renderTarget.destinationFrame.height * glCache.renderTarget.resolution) | 0
                );
                firstTime = true;
            }
            else
            {
                renderer.bindRenderTarget(glCache.renderTarget);
            }
            if (firstTime || !this._frozen || this._renderOnce)
            {
                this._renderOnce = false;

                // Clear RT
                const c = this._camera.backgroundColor;
                [tmpColArr[0], tmpColArr[1], tmpColArr[2], tmpColArr[3]] = [c.r, c.g, c.b, c.a];
                glCache.renderTarget.clear(tmpColArr);

                // Render scene
                this.renderForward(renderer);
            }
            renderer.reset();
        }

        /**
         * Renders the scene using forward lighting.
         * @param renderer
         */
        protected renderForward(renderer: PIXI.WebGLRenderer): void
        {
            const gl = renderer.gl;

            // Prepare renderer state
            const scene = this._camera.rootParent;
            renderer.state.push();
            renderer.state.setDepthTest(1);
            renderer.state.setCullFace(1);
            renderer.state.setFrontFace(0);
            renderer.state.setBlend(0);

            // Forward rendering pass
            const cameraWorldTransform = this.camera.getWorldTransform(tmpMatrix1);
            tmpVec1.x = cameraWorldTransform[12];
            tmpVec1.y = cameraWorldTransform[13];
            tmpVec1.z = cameraWorldTransform[14];
            const projMatrix = this.camera.projection;
            const viewMatrix = Math.Matrix4.inverse(cameraWorldTransform, tmpMatrix2);
            this._forwardRenderContext.begin(projMatrix, viewMatrix, tmpVec1);
            scene.render(this._forwardRenderContext);
            this._forwardRenderContext.dispatchLitForward(renderer, true, false);

            // Projected shadow pass
            const sceneRenderer = ISceneRenderer.get();
            const lightSources = this._forwardRenderContext.lightSources;
            const rtPool = IRenderTexturePool.get();
            const oldRT = renderer._activeRenderTarget;
            renderer.state.setCullFace(0);
            for (const lightSource of lightSources)
            {
                if (lightSource.shadow && lightSource.shadow.type === Scene.ILightSource.ShadowType.Projected)
                {
                    // Acquire and setup RT for shadows
                    const res = lightSource.shadow.resolution || 1;
                    const rt = rtPool.acquire(Math.round(this._viewportSize.w * res), Math.round(this._viewportSize.h * res), true) as RenderTexture;
                    if (this._tmpRT == null)
                    {
                        this._tmpRT = new PIXI.RenderTexture(rt.source);
                    }
                    else
                    {
                        this._tmpRT.baseTexture = rt.source;
                        this._tmpRT.resize(rt.width, rt.height, true);
                    }
                    renderer.bindRenderTexture(this._tmpRT, null);
                    renderer.clear(whiteColArr as any);

                    // Render shadows to RT
                    gl.depthMask(false);
                    this._forwardRenderContext.dispatchProjectedShadow(renderer, lightSource);

                    // Switch back to screen
                    renderer.bindRenderTarget(oldRT);

                    // Calculate projection matrix
                    tmpTransformStack[0] = Math.Matrix4.createTranslation(Math.Vector3D.unitZ, tmpMatrix3);
                    tmpTransformStack[1] = Math.Matrix4.inverse(projMatrix, tmpMatrix4);
                    tmpTransformStack[2] = cameraWorldTransform;
                    tmpTransformStack[3] = Math.Plane.createPointProjection(lightSource.shadow.plane, tmpVec1, tmpMatrix5);
                    tmpTransformStack[4] = viewMatrix;
                    tmpTransformStack[5] = projMatrix;
                    Math.Matrix4.combine(tmpTransformStack, tmpMatrix6);

                    // Draw shadow RT to screen
                    projShadowBlendMat.diffuseMap = rt;
                    gl.depthMask(true);
                    sceneRenderer.render(renderer, ScenePass.LitForward, tmpMatrix6, Math.Matrix4.identity, Math.Matrix4.identity, Math.Vector3D.zero, this._fsQuad, projShadowBlendMat, RS.Util.Colors.white, 0);

                    // Free RT for future use
                    this._tmpRT.baseTexture = null;
                    rtPool.release(rt);
                }
            }

            // Transparent pass
            this._forwardRenderContext.dispatchLitForward(renderer, false, true);

            // Clean up renderer state
            renderer.state.pop();
            renderer.gl.depthMask(true);
        }
    }

    RS.Rendering.SceneView = SceneView;
}
