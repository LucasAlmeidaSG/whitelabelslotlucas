/// <reference path="ILightingEngine.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    interface ShaderData
    {
        program: Shaders.SceneUnlitShaderProgram;
        shader: Shaders.SceneUnlitShader;
    }

    export class UnlitForwardLightingEngine implements IForwardLightingEngine<Shaders.SceneUnlitShader>
    {
        private _shaderMap: { [hash: number]: ShaderData; } = {};
        private _hashMask: number;

        public constructor()
        {
            // Build a mask to obscure any unsupported switches
            // This will prevent those switches from generating extra permutations of shader programs which are unnecessary
            this._hashMask = IForwardLightingEngine.Switches.hash({
                tangentSpaceNormals: false,
                specular: false,
                reflections: false,
                vertexBased: false,
                vertexColors: true,
                withDiffuseMap: true,
                withNormalMap: false,
                withSpecularMap: false,
                withAmbientMap: false,
                withEnvironmentMap: false,
                blend: 7
            });
        }

        /**
         * Prepares and returns a shader for the specified light data and switches.
         * The shader will have lighting data set, but will need material parameters and matrices set.
         * @param lightData 
         * @param switches 
         */
        public prepare(lightData: LightData[], switches: IForwardLightingEngine.Switches): Shaders.SceneUnlitShader
        {
            // Hash the switches and light types
            const hash = IForwardLightingEngine.Switches.hash(switches) & this._hashMask;

            // Lookup or create the shader data
            const { program, shader } = this._shaderMap[hash] || (this._shaderMap[hash] = this.createShader(switches));

            // Done
            return shader;
        }

        private createShader(switches: IForwardLightingEngine.Switches): ShaderData
        {
            Log.debug(`Generating new unlit shader for switches {${IForwardLightingEngine.Switches.toString(switches)}}`);
            let shaderProgram: Shaders.SceneUnlitShaderProgram;
            switch (switches.blend)
            {
                case Scene.Material.Blend.Opaque:
                    shaderProgram = new Shaders.SceneUnlitShaderProgram({
                        masked: false,
                        ignoreAlpha: true,
                        vertexColors: switches.vertexColors,
                        useDiffuseMap: switches.withDiffuseMap
                    });
                    break;
                case Scene.Material.Blend.Masked:
                    shaderProgram = new Shaders.SceneUnlitShaderProgram({
                        masked: true,
                        ignoreAlpha: false,
                        vertexColors: switches.vertexColors,
                        useDiffuseMap: switches.withDiffuseMap
                    });
                case Scene.Material.Blend.Transparent:
                case Scene.Material.Blend.TransparentAdd:
                case Scene.Material.Blend.TransparentMult:
                    shaderProgram = new Shaders.SceneUnlitShaderProgram({
                        masked: false,
                        ignoreAlpha: false,
                        vertexColors: switches.vertexColors,
                        useDiffuseMap: switches.withDiffuseMap
                    });
                    break;
            }
            const shader = shaderProgram.createShader();
            return { program: shaderProgram, shader };
        }
    }
}