/// <reference path="SceneObject.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpVecs = [ Math.Vector3D(), Math.Vector3D(), Math.Vector3D() ];

    @HasCallbacks
    export class FlyCamera extends Camera implements Scene.IFlyCamera
    {
        /** Gets or sets the left-right speed. */
        public xSpeed: number = 1.0;

        /** Gets or sets the up-down speed. */
        public ySpeed: number = 1.0;

        /** Gets or sets the forward-back speed. */
        public zSpeed: number = 1.0;

        /** Gets or sets the pitch rotation speed. */
        public pitchSpeed: number = Math.Angles.circle(1 / 3200);

        /** Gets or sets the yaw rotation speed. */
        public yawSpeed: number = Math.Angles.circle(1 / 1600);

        /** Gets the movement vector. */
        public readonly moveVec = Math.Vector3D();

        protected _posX = 0;
        protected _posY = 0;
        protected _posZ = 0;
        protected _negX = 0;
        protected _negY = 0;
        protected _negZ = 0;

        protected _pitch: number = 0;
        protected _yaw: number = 0;

        public constructor();

        public constructor(fov: number, aspectRatio: number);

        public constructor(p1?: number, p2?: number)
        {
            super(p1, p2);
            Ticker.registerTickers(this);
            document.addEventListener("keydown", (ev) =>
            {
                switch (ev.key)
                {
                    case "w":
                        this._posZ = 1.0;
                        ev.preventDefault();
                        break;
                    case "s":
                        this._negZ = 1.0;
                        ev.preventDefault();
                        break;
                    case "d":
                        this._posX = 1.0;
                        ev.preventDefault();
                        break;
                    case "a":
                        this._negX = 1.0;
                        ev.preventDefault();
                        break;
                }
                this.updateMove();
            });
            document.addEventListener("keyup", (ev) =>
            {
                switch (ev.key)
                {
                    case "w":
                        this._posZ = 0.0;
                        ev.preventDefault();
                        break;
                    case "s":
                        this._negZ = 0.0;
                        ev.preventDefault();
                        break;
                    case "d":
                        this._posX = 0.0;
                        ev.preventDefault();
                        break;
                    case "a":
                        this._negX = 0.0;
                        ev.preventDefault();
                        break;
                }
                this.updateMove();
            });
        }

        /** Rotates this camera based on the x and y deltas. */
        public rotate(x: number, y: number): void
        {
            this._pitch = Math.clamp(this._pitch + y * this.pitchSpeed, Math.Angles.circle(-0.2499), Math.Angles.circle(0.249));
            this._yaw = this._yaw + x * this.yawSpeed;
            Math.Quaternion.fromEuler(this._pitch, this._yaw, 0, this.rotation);
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            Ticker.unregisterTickers(this);
            super.dispose();
        }

        protected updateMove(): void
        {
            this.moveVec.x = this._posX - this._negX;
            this.moveVec.y = this._posY - this._negY;
            this.moveVec.z = this._posZ - this._negZ;
        }

        @Tick({ kind: Ticker.Kind.Always })
        protected tick(ev: Ticker.Event): void
        {
            const deltaTime = ev.realDelta / 1000.0;
            
            const forward = Math.Quaternion.transform(this.rotation, Math.Vector3D.unitZ, tmpVecs[0]);
            const right = Math.Quaternion.transform(this.rotation, Math.Vector3D.unitX, tmpVecs[1]);
            const up = Math.Vector3D.unitY;

            const dx = (forward.x * this.zSpeed * this.moveVec.z + right.x * this.xSpeed * this.moveVec.x + up.x * this.ySpeed * this.moveVec.y) * deltaTime;
            const dy = (forward.y * this.zSpeed * this.moveVec.z + right.y * this.xSpeed * this.moveVec.x + up.y * this.ySpeed * this.moveVec.y) * deltaTime;
            const dz = (forward.z * this.zSpeed * this.moveVec.z + right.z * this.xSpeed * this.moveVec.x + up.z * this.ySpeed * this.moveVec.y) * deltaTime;

            this.position.set(this.position.x + dx, this.position.y + dy, this.position.z + dz);
        }
    }

    Scene.FlyCamera = FlyCamera;
}