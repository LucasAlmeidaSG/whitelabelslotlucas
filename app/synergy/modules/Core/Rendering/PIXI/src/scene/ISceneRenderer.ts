/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    /**
     * Encapsulates a render pass of the scene.
     */
    export enum ScenePass
    {
        /** Standard forward-lit approach, output final color to buffer. */
        LitForward,

        /** Only output diffuse color to buffer. */
        DeferredDiffuse,

        /** Only output world-space normal to buffer. */
        DeferredNormal,

        /** Only output material or specular properties to buffer. */
        DeferredMaterial,

        /** Output multiple attributes to MRT GBuffer. */
        DeferredMulti,

        /** Only output distance and distance squared (from view point) to buffer. */
        VarianceShadowMap,

        /** Project geometry against a plane and output shadow color directly to buffer. */
        SimpleProjectedShadow
    }

    /**
     * Responsible for actually rendering 3D objects based on material settings.
     */
    export interface ISceneRenderer
    {
        /**
         * Immediately dispatches a single draw call for an element binding of a geometry with a material.
         * @param renderer the PIXI WebGL renderer
         * @param pass the rendering strategy to use
         * @param projectionMatrix the view->screen matrix
         * @param viewMatrix the world->view matrix
         * @param worldMatrix the object->world matrix
         * @param viewPosition the position of the camera in world space
         * @param geometry the geometry to render from
         * @param material the material to render with
         * @param tint a color to modulate diffuse color from the material with
         * @param indexBinding the index of the element binding within the geometry to use
         * @param lightSources array of light sources that affect the object
         * @param flip flip winding order
         */
        render(
            renderer: PIXI.WebGLRenderer, pass: ScenePass,
            projectionMatrix: Math.Matrix4, viewMatrix: Math.Matrix4, worldMatrix: Math.Matrix4,
            viewPosition: Math.Vector3D,
            geometry: Geometry, material: RS.Rendering.Scene.Material, tint: RS.Util.Color, indexBinding: number,
            lightSources?: LightSource[], flip?: boolean
        ): void;
    }

    export const ISceneRenderer = RS.Strategy.declare<ISceneRenderer>(Strategy.Type.Singleton);
}