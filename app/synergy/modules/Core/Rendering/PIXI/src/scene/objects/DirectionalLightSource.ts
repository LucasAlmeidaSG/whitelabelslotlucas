/// <reference path="LightSource.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpMatrix1 = Math.Matrix4(), tmpMatrix2 = Math.Matrix4();
    const tmpVec1 = Math.Vector3D();

    @HasCallbacks
    export class DirectionalLightSource extends LightSource implements Scene.IDirectionalLightSource
    {
        public constructor();

        public constructor(color: Util.Color, intensity: number);

        public constructor(p1?: Util.Color, p2?: number)
        {
            if (p1)
            {
                super(p1, p2);
            }
            else
            {
                super();
            }
            this.localBounds.minX = -Infinity;
            this.localBounds.minY = -Infinity;
            this.localBounds.minZ = -Infinity;
            this.localBounds.maxX = Infinity;
            this.localBounds.maxY = Infinity;
            this.localBounds.maxY = Infinity;
        }

        /**
         * Gets the shadow matrix for this light.
         * The actual meaning of the matrix will depend on the shadow type selected in the settings.
         * @param out 
         */
        public getShadowMatrix(out?: Math.Matrix4): Math.Matrix4
        {
            out = out || Math.Matrix4();
            if (!this._shadow)
            {
                Math.Matrix4.copy(Math.Matrix4.identity, out);
                return out;
            }
            switch (this._shadow.type)
            {
                case Scene.ILightSource.ShadowType.Mapped:
                    // Matrix is a projection-view matrix from perspective of light
                    Math.Matrix4.inverse(this.getWorldTransform(), tmpMatrix1);
                    Math.Matrix4.createOrthographic(-32, 2, -32, 32, 1024, 0, tmpMatrix2);
                    Math.Matrix4.mul(tmpMatrix1, tmpMatrix2, out);
                    return out;
                case Scene.ILightSource.ShadowType.Projected:
                    // Matrix is a world modifier matrix that projects onto a plane
                    const wt = this.getWorldTransform(tmpMatrix1);
                    Math.Matrix4.transformNormal(wt, Math.Vector3D.unitZ, tmpVec1);
                    Math.Plane.createDirectionalProjection(this._shadow.plane, tmpVec1, out);
                    return out;
                default:
                    Math.Matrix4.copy(Math.Matrix4.identity, out);
                    return out;
            }
        }
    }

    Scene.DirectionalLightSource = DirectionalLightSource;
}