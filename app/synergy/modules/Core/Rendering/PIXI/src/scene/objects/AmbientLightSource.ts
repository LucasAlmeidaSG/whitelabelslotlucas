/// <reference path="LightSource.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    @HasCallbacks
    export class AmbientLightSource extends LightSource implements Scene.IAmbientLightSource
    {
        public constructor();

        public constructor(color: Util.Color, intensity: number);

        public constructor(p1?: Util.Color, p2?: number)
        {
            if (p1)
            {
                super(p1, p2);
            }
            else
            {
                super();
            }
            this.localBounds.minX = -Infinity;
            this.localBounds.minY = -Infinity;
            this.localBounds.minZ = -Infinity;
            this.localBounds.maxX = Infinity;
            this.localBounds.maxY = Infinity;
            this.localBounds.maxY = Infinity;
        }

        /**
         * Gets the shadow matrix for this light.
         * The actual meaning of the matrix will depend on the shadow type selected in the settings.
         * @param out 
         */
        public getShadowMatrix(out?: Math.Matrix4): Math.Matrix4
        {
            out = out || Math.Matrix4();
            Math.Matrix4.copy(Math.Matrix4.identity, out);
            return out;
        }
    }

    Scene.AmbientLightSource = AmbientLightSource;
}