/// <reference path="LightSource.ts" />

/** @internal */
namespace RS.Rendering.Pixi.Scene
{
    import Scene = RS.Rendering.Scene;

    const tmpMatrix1 = Math.Matrix4(), tmpMatrix2 = Math.Matrix4();
    const tmpVec1 = Math.Vector3D();

    @HasCallbacks
    export class PointLightSource extends LightSource implements Scene.IPointLightSource
    {
        protected _radius: number;

        public get radius() { return this._radius; }
        public set radius(value)
        {
            this._radius = value;
            this.localBounds.minX = -value;
            this.localBounds.minY = -value;
            this.localBounds.minZ = -value;
            this.localBounds.maxX = value;
            this.localBounds.maxY = value;
            this.localBounds.maxZ = value;
        }

        public constructor();

        public constructor(color: Util.Color, intensity: number, radius: number);

        public constructor(p1?: Util.Color, p2?: number, p3?: number)
        {
            if (p1)
            {
                super(p1, p2);
                this.radius = p3;
            }
            else
            {
                super();
                this.radius = 1.0;
            }
        }

        /**
         * Gets the shadow matrix for this light.
         * The actual meaning of the matrix will depend on the shadow type selected in the settings.
         * @param out 
         */
        public getShadowMatrix(out?: Math.Matrix4): Math.Matrix4
        {
            out = out || Math.Matrix4();
            if (!this._shadow)
            {
                Math.Matrix4.copy(Math.Matrix4.identity, out);
                return out;
            }
            switch (this._shadow.type)
            {
                case Scene.ILightSource.ShadowType.Mapped:
                    // Matrix is a projection-view matrix from perspective of light
                    Math.Matrix4.inverse(this.getWorldTransform(), tmpMatrix1);
                    Math.Matrix4.createPerspective(Math.Angles.halfCircle, 0.125, 512.0, 1.0, tmpMatrix2);
                    Math.Matrix4.mul(tmpMatrix1, tmpMatrix2, out);
                    return out;
                case Scene.ILightSource.ShadowType.Projected:
                    // Matrix is a world modifier matrix that projects onto a plane
                    const wt = this.getWorldTransform(tmpMatrix1);
                    Math.Matrix4.transform(wt, this.position, tmpVec1);
                    Math.Plane.createPointProjection(this._shadow.plane, tmpVec1, out);
                    return out;
                default:
                    Math.Matrix4.copy(Math.Matrix4.identity, out);
                    return out;
            }
        }
    }

    Scene.PointLightSource = PointLightSource;
}