/// <reference path="CompressedImage.ts" />
/** @internal */
namespace RS.Asset.Pixi.Compression
{
    import GLTexture = PIXI.glCore.GLTexture;

    interface GLTextureMixin
    {
        isCompressed: boolean;
        enableMipmap: GLTexture["enableMipmap"];
        destroy: GLTexture["destroy"];
        destroyNoUntrack: GLTexture["destroy"];
        uploadNotCompressed: GLTexture["upload"];
        upload(source: HTMLVideoElement | HTMLImageElement | HTMLCanvasElement | ImageData | RS.Asset.Pixi.Compression.CompressedImage): void;
    }

    type CompressableGLTexture = GLTexture & GLTextureMixin;

    /**
     * @mixin
     */
    export const GLTextureMixin: GLTextureMixin =
    {
        uploadNotCompressed: GLTexture.prototype.upload,
        isCompressed: false,
        upload: function (this: CompressableGLTexture, source)
        {
            if (!(source instanceof RS.Asset.Pixi.Compression.CompressedImage))
            {
                this.uploadNotCompressed(source);
                if (source instanceof HTMLVideoElement || source instanceof HTMLImageElement)
                {
                    Rendering.Pixi.Profiling.textureTracker.track(this, source.src);
                }
                else
                {
                    Rendering.Pixi.Profiling.textureTracker.track(this);
                }
                return;
            }
            this.bind();

            const gl = this.gl;

            gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, this.premultiplyAlpha);

            this.isCompressed = true;

            source.generateWebGLTexture(gl);

            Rendering.Pixi.Profiling.textureTracker.track(this, source.src);
        },
        enableMipmap: function (this: CompressableGLTexture)
        {
            if (this.isCompressed)
            {
                return;
            }
            const gl = this.gl;

            this.bind();

            this.mipmap = true;

            gl.generateMipmap(gl.TEXTURE_2D);
        },
        destroy: function (this: CompressableGLTexture)
        {
            Rendering.Pixi.Profiling.textureTracker.untrack(this);
            return this.destroyNoUntrack();
        },
        destroyNoUntrack: GLTexture.prototype.destroy
    };

    RS.Util.assign(GLTextureMixin, GLTexture.prototype, false);
}