/// <reference path="Utils.ts" />
/** @internal */
namespace RS.Asset.Pixi.Compression
{
    export class CompressedImage implements IDisposable, Rendering.ICompressedImage
    {
        /** Published when this image has loaded. */
        @AutoDispose
        public readonly onLoad = createSimpleEvent();
        public get src() { return this.__src; }

        private __src: string;
        private width: number;
        private height: number;
        private data: Uint8Array;
        private type: string;
        private levels: number;
        private internalFormat: number;
        private isCompressedImage: boolean;
        private crunch: [crunch.Pointer, crunch.Pointer];
        private preserveSource: boolean;
        private complete: boolean;
        private extensionNames: string[];

        public get isDisposed() { return this.data == null; }

        public constructor(src: string, data?: any, type?: any, width?: number, height?: number, levels?: number, internalFormat?: number)
        {
            CompressedImage.prototype.init.apply(this, arguments);
        }

        /**
         * Load a compressed image from an array buffer
         * @param arrayBuffer the buffer contains the image
         * @return the loaded CompressedImage
         */
        public static loadFromArrayBuffer(arrayBuffer: ArrayBuffer, src: string): CompressedImage
        {
            return new CompressedImage(src).loadFromArrayBuffer(arrayBuffer);
        }

        public init(src: string, data: Uint8Array, type: string, width: number, height: number, levels: number, internalFormat: number, crunchCache?: [crunch.Pointer, crunch.Pointer]): this
        {
            this.__src = src;
            this.width = width;
            this.height = height;
            this.data = data;
            this.type = type;
            this.levels = levels;
            this.internalFormat = internalFormat;
            this.isCompressedImage = true;
            this.crunch = crunchCache;
            this.preserveSource = true;
            this.extensionNames = extensionNames[type];

            const oldComplete = this.complete;
            this.complete = !!data;
            if (!oldComplete && this.complete)
            {
                this.onLoad.publish();
            }
            return this;
        }

        public dispose(): void
        {
            this.data = null;
            if (this.crunch)
            {
                crunch._free(this.crunch[0]); // source
                crunch._free(this.crunch[1]); // destination
                this.crunch = null;
            }
        }

        public generateWebGLTexture(gl: WebGLRenderingContext): void
        {
            if (this.data === null)
            {
                throw new Error("Trying to create a second (or more) webgl texture from the same CompressedImage : " + this.__src);
            }

            // "Activate" context by getting the extension
            let ext: any | null = null;
            for (const extName of this.extensionNames)
            {
                ext = gl.getExtension(extName);
                if (ext != null) { break; }
            }
            if (ext == null)
            {
                if (this.extensionNames.length === 1)
                {
                    throw new Error(`Extension '${this.extensionNames[0]}' not supported`);
                }
                else
                {
                    throw new Error(`One of extensions ${this.extensionNames.map((extName) => `'${extName}'`).join(", ")} not supported`);
                }
            }

            let width = this.width;
            let height = this.height;
            const levels = this.levels;
            let offset = 0;
            // Loop through each mip level of compressed texture data provided and upload it to the given texture.
            for (let i = 0; i < this.levels; ++i)
            {
                // Determine how big this level of compressed texture data is in bytes.
                const levelSize = textureLevelSize(this.internalFormat, width, height);
                // Get a view of the bytes for this level of DXT data.
                const dxtLevel = new Uint8Array(this.data.buffer, this.data.byteOffset + offset, levelSize);
                // Upload!
                gl.compressedTexImage2D(gl.TEXTURE_2D, i, this.internalFormat, width, height, 0, dxtLevel);
                // The next mip level will be half the height and width of this one.
                width = width >> 1;
                if (width < 1)
                {
                    width = 1;
                }
                height = height >> 1;
                if (height < 1)
                {
                    height = 1;
                }
                // Advance the offset into the compressed texture data past the current mip level's data.
                offset += levelSize;
            }

            // We can't use gl.generateMipmaps with compressed textures, so only use
            // mipmapped filtering if the compressed texture data contained mip levels.
            if (levels > 1)
            {
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
            }
            else
            {
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            }

            // Cleaning the data to save memory. NOTE : BECAUSE OF THIS WE CANNOT CREATE TWO GL TEXTURE FROM THE SAME COMPRESSED IMAGE !
            if (!this.preserveSource)
            {
                this.data = null;
            }
        }

        public loadFromArrayBuffer(arrayBuffer: ArrayBuffer, crnLoad?: boolean): this
        {
            const head = new Uint8Array(arrayBuffer, 0, 3);

            //todo: implement onload

            if (head[0] == "DDS".charCodeAt(0) && head[1] == "DDS".charCodeAt(1) && head[2] == "DDS".charCodeAt(2))
            {
                this._loadDDS(arrayBuffer);
            }
            else if (head[0] == "PVR".charCodeAt(0) && head[1] == "PVR".charCodeAt(1) && head[2] == "PVR".charCodeAt(2))
            {
                this._loadPVR(arrayBuffer);
            }
            else if (head[0] == 0x13 && head[1] == 0xab && head[2] == 0xa1)
            {
                this._loadASTC(arrayBuffer);
            }
            else if (crnLoad)
            {
                this._loadCRN(arrayBuffer);
            }
            else
            {
                throw new Error("Compressed texture format is not recognized: " + this.__src);
            }
            return this;
        }

        public arrayBufferCopy(src: ArrayBufferView, dst: ArrayBufferView, dstByteOffset: number, numBytes: number): void
        {
            const dst32Offset = dstByteOffset / 4;
            const tail = (numBytes % 4);
            const src32 = new Uint32Array(src.buffer, 0, (numBytes - tail) / 4);
            const dst32 = new Uint32Array(dst.buffer);
            for (let ii = 0; ii < src32.length; ii++)
            {
                dst32[dst32Offset + ii] = src32[ii];
            }
            for (let i = numBytes - tail; i < numBytes; i++)
            {
                dst[dstByteOffset + i] = src[i];
            }
        }

        private _loadCRN(arrayBuffer)
        {
            // Taken from crnlib.h
            const DXT_FORMAT_MAP =
                [
                    COMPRESSED_RGB_S3TC_DXT1_EXT, 	// 0
                    COMPRESSED_RGBA_S3TC_DXT3_EXT,  // 1
                    COMPRESSED_RGBA_S3TC_DXT5_EXT 	// 2
                ];

            const srcSize = arrayBuffer.byteLength;
            const bytes = new Uint8Array(arrayBuffer);
            const src = crunch._malloc(srcSize);
            CompressedImage.prototype.arrayBufferCopy(bytes, crunch.HEAPU8, src, srcSize);

            const width = crunch._crn_get_width(src, srcSize);
            const height = crunch._crn_get_height(src, srcSize);
            const levels = crunch._crn_get_levels(src, srcSize);
            const format = crunch._crn_get_dxt_format(src, srcSize);

            const dstSize = crunch._crn_get_uncompressed_size(src, srcSize, 0);
            const dst = crunch._malloc(dstSize);
            crunch._crn_decompress(src, srcSize, dst, dstSize, 0);
            const dxtData = new Uint8Array(crunch.HEAPU8.buffer, dst, dstSize);

            return this.init(this.__src, dxtData, 'CRN', width, height, levels, DXT_FORMAT_MAP[format], [src, dst]);
        }

        /**
         * Load a DDS compressed image from an array buffer
         * @param arrayBuffer the buffer contains the image
         * @return the loaded CompressedImage
         */
        private _loadDDS(arrayBuffer)
        {
            // Get a view of the arrayBuffer that represents the DDS header.
            const header = new Int32Array(arrayBuffer, 0, DDS_HEADER_LENGTH);

            // Do some sanity checks to make sure this is a valid DDS file.
            if (header[DDS_HEADER_MAGIC] != DDS_MAGIC)
            {
                throw new Error("Invalid magic number in DDS header");
            }

            if (!(header[DDS_HEADER_PF_FLAGS] & DDPF_FOURCC))
            {
                throw new Error("Unsupported format, must contain a FourCC code");
            }

            // Determine what type of compressed data the file contains.
            const fourCC = header[DDS_HEADER_PF_FOURCC];
            let internalFormat;
            switch (fourCC)
            {
                case FOURCC_DXT1:
                    internalFormat = COMPRESSED_RGB_S3TC_DXT1_EXT;
                    break;
                case FOURCC_DXT3:
                    internalFormat = COMPRESSED_RGBA_S3TC_DXT3_EXT;
                    break;
                case FOURCC_DXT5:
                    internalFormat = COMPRESSED_RGBA_S3TC_DXT5_EXT;
                    break;
                case FOURCC_ATC:
                    internalFormat = COMPRESSED_RGB_ATC_WEBGL;
                    break;
                case FOURCC_ATCA:
                    internalFormat = COMPRESSED_RGBA_ATC_EXPLICIT_ALPHA_WEBGL;
                    break;
                case FOURCC_ATCI:
                    internalFormat = COMPRESSED_RGBA_ATC_INTERPOLATED_ALPHA_WEBGL;
                    break;
                default:
                    throw new Error("Unsupported FourCC code: " + int32ToFourCC(fourCC));
            }

            // Determine how many mipmap levels the file contains.
            let levels = 1;
            if (header[DDS_HEADER_FLAGS] & DDSD_MIPMAPCOUNT)
            {
                levels = Math.max(1, header[DDS_HEADER_MIPMAPCOUNT]);
            }

            // Gather other basic metrics and a view of the raw the DXT data.
            const width = header[DDS_HEADER_WIDTH];
            const height = header[DDS_HEADER_HEIGHT];
            const dataOffset = header[DDS_HEADER_SIZE] + 4;
            const dxtData = new Uint8Array(arrayBuffer, dataOffset);

            return this.init(this.__src, dxtData, 'DDS', width, height, levels, internalFormat);
        }

        /**
         * Load a ASTC compressed image from an array buffer
         * @param arrayBuffer the buffer contains the image
         * @return the loaded CompressedImage
         */
        private _loadASTC(arrayBuffer)
        {
            // Get a view of the arrayBuffer that represents the DDS header.

            const header = new Int8Array(arrayBuffer, 0, ASTC_HEADER_LENGTH);

            const magic = new Uint32Array(arrayBuffer.slice(0, 4));

            // Do some sanity checks to make sure this is a valid DDS file.
            if (magic[0] != ASTC_MAGIC)
            { //0x5ca1ab13
                throw new Error("Invalid magic number in ASTC header");
            }

            // Determine what type of compressed data the file contains.
            const detectFormats = [COMPRESSED_RGBA_ASTC_4x4_KHR,
                COMPRESSED_RGBA_ASTC_5x4_KHR,
                COMPRESSED_RGBA_ASTC_5x5_KHR,
                COMPRESSED_RGBA_ASTC_6x5_KHR,
                COMPRESSED_RGBA_ASTC_6x6_KHR,
                COMPRESSED_RGBA_ASTC_8x5_KHR,
                COMPRESSED_RGBA_ASTC_8x6_KHR,
                COMPRESSED_RGBA_ASTC_8x8_KHR,
                COMPRESSED_RGBA_ASTC_10x5_KHR,
                COMPRESSED_RGBA_ASTC_10x6_KHR,
                COMPRESSED_RGBA_ASTC_10x8_KHR,
                COMPRESSED_RGBA_ASTC_10x10_KHR,
                COMPRESSED_RGBA_ASTC_12x10_KHR,
                COMPRESSED_RGBA_ASTC_12x12_KHR];

            /*
            */

            //https://developer.mozilla.org/en-US/docs/Web/API/WEBGL_compressed_texture_astc
            const dataSize = arrayBuffer.byteLength - ASTC_HEADER_LENGTH; //loaded image data payload size in bytes

            //retieve width and height of texture from the astc file header
            const widthBytes = new Uint8Array([header[7], header[8], header[9], 0]);
            const heightBytes = new Uint8Array([header[10], header[11], header[12], 0]);
            const width = new Uint32Array(widthBytes.buffer)[0];
            const height = new Uint32Array(heightBytes.buffer)[0];

            //detect format from data size
            let internalFormat = 0;
            for (let i = 0; i < detectFormats.length; i++)
            {
                if (dataSize === textureLevelSize(detectFormats[i], width, height))
                {
                    internalFormat = detectFormats[i];
                    break;
                }
            }
            if (internalFormat == 0)
            {
                throw new Error("Unable to autodetect ASTC format; file size not right");
            }

            const dataOffset = ASTC_HEADER_LENGTH;
            const astcData = new Uint8Array(arrayBuffer, dataOffset, dataSize);

            const levels = 1;
            return this.init(this.__src, astcData, 'ASTC', width, height, levels, internalFormat);
        }

        /**
         * Load a PVR compressed image from an array buffer
         * @param arrayBuffer the buffer contains the image
         * @return the loaded CompressedImage
         */
        private _loadPVR(arrayBuffer)
        {
            // Get a view of the arrayBuffer that represents the DDS header.
            const header = new Int32Array(arrayBuffer, 0, PVR_HEADER_LENGTH);

            // Do some sanity checks to make sure this is a valid DDS file.
            if (header[PVR_HEADER_MAGIC] != PVR_MAGIC)
            {
                throw new Error("Invalid magic number in PVR header");
            }

            // Determine what type of compressed data the file contains.
            const format = header[PVR_HEADER_FORMAT];
            let internalFormat;
            switch (format)
            {
                case PVR_FORMAT_2BPP_RGB:
                    internalFormat = COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
                    break;
                case PVR_FORMAT_2BPP_RGBA:
                    internalFormat = COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
                    break;
                case PVR_FORMAT_4BPP_RGB:
                    internalFormat = COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
                    break;
                case PVR_FORMAT_4BPP_RGBA:
                    internalFormat = COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
                    break;
                case PVR_FORMAT_ETC1:
                    internalFormat = COMPRESSED_RGB_ETC1_WEBGL;
                    break;
                case PVR_FORMAT_DXT1:
                    internalFormat = COMPRESSED_RGB_S3TC_DXT1_EXT;
                    break;
                case PVR_FORMAT_DXT3:
                    internalFormat = COMPRESSED_RGBA_S3TC_DXT3_EXT;
                    break;
                case PVR_FORMAT_DXT5:
                    internalFormat = COMPRESSED_RGBA_S3TC_DXT5_EXT;
                    break;
                default:
                    throw new Error("Unsupported PVR format: " + format);
            }

            // Gather other basic metrics and a view of the raw the DXT data.
            const width = header[PVR_HEADER_WIDTH];
            const height = header[PVR_HEADER_HEIGHT];
            const levels = header[PVR_HEADER_MIPMAPCOUNT];
            const dataOffset = header[PVR_HEADER_METADATA] + 52;
            const pvrtcData = new Uint8Array(arrayBuffer, dataOffset);

            return this.init(this.__src, pvrtcData, 'PVR', width, height, levels, internalFormat);
        }
    }
    Rendering.CompressedImage = CompressedImage;
}