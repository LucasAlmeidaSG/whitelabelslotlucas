/** @internal */
namespace RS.Asset.Pixi.Compression
{
    export const extensionNames: Map<string[]> =
    {
        "CRN": ["WEBGL_compressed_texture_s3tc", "WEBKIT_WEBGL_compressed_texture_s3tc"],
        "DDS": ["WEBGL_compressed_texture_s3tc", "WEBKIT_WEBGL_compressed_texture_s3tc"],
        "ASTC": ["WEBGL_compressed_texture_astc", "WEBKIT_WEBGL_compressed_texture_astc"],
        "PVR": ["WEBGL_compressed_texture_pvrtc", "WEBKIT_WEBGL_compressed_texture_pvrtc"]
    };

    //============================//
    // DXT constants and utilites //
    //============================//

    // Utility functions
    // Builds a numeric code for a given fourCC string
    export function fourCCToInt32(value)
    {
        return value.charCodeAt(0) +
            (value.charCodeAt(1) << 8) +
            (value.charCodeAt(2) << 16) +
            (value.charCodeAt(3) << 24);
    }

    // Turns a fourCC numeric code into a string
    export function int32ToFourCC(value)
    {
        return String.fromCharCode(
            value & 0xff,
            (value >> 8) & 0xff,
            (value >> 16) & 0xff,
            (value >> 24) & 0xff
        );
    }

    // Calcualates the size of a compressed texture level in bytes
    export function textureLevelSize(format, width, height)
    {
        switch (format)
        {
            case COMPRESSED_RGB_S3TC_DXT1_EXT:
            case COMPRESSED_RGB_ATC_WEBGL:
            case COMPRESSED_RGB_ETC1_WEBGL:
                return ((width + 3) >> 2) * ((height + 3) >> 2) * 8;

            case COMPRESSED_RGBA_S3TC_DXT3_EXT:
            case COMPRESSED_RGBA_S3TC_DXT5_EXT:
            case COMPRESSED_RGBA_ATC_EXPLICIT_ALPHA_WEBGL:
            case COMPRESSED_RGBA_ATC_INTERPOLATED_ALPHA_WEBGL:
                return ((width + 3) >> 2) * ((height + 3) >> 2) * 16;

            case COMPRESSED_RGB_PVRTC_4BPPV1_IMG:
            case COMPRESSED_RGBA_PVRTC_4BPPV1_IMG:
                return Math.floor((Math.max(width, 8) * Math.max(height, 8) * 4 + 7) / 8);

            case COMPRESSED_RGB_PVRTC_2BPPV1_IMG:
            case COMPRESSED_RGBA_PVRTC_2BPPV1_IMG:
                return Math.floor((Math.max(width, 16) * Math.max(height, 8) * 2 + 7) / 8);

            //ASTC formats, https://www.khronos.org/registry/webgl/extensions/WEBGL_compressed_texture_astc/
            case COMPRESSED_RGBA_ASTC_4x4_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR:
                return Math.floor((width + 3) / 4) * Math.floor((height + 3) / 4) * 16;
            case COMPRESSED_RGBA_ASTC_5x4_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR:
                return Math.floor((width + 4) / 5) * Math.floor((height + 3) / 4) * 16;
            case COMPRESSED_RGBA_ASTC_5x5_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR:
                return Math.floor((width + 4) / 5) * Math.floor((height + 4) / 5) * 16;
            case COMPRESSED_RGBA_ASTC_6x5_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR:
                return Math.floor((width + 5) / 6) * Math.floor((height + 4) / 5) * 16;
            case COMPRESSED_RGBA_ASTC_6x6_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR:
                return Math.floor((width + 5) / 6) * Math.floor((height + 5) / 6) * 16;
            case COMPRESSED_RGBA_ASTC_8x5_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR:
                return Math.floor((width + 7) / 8) * Math.floor((height + 4) / 5) * 16;
            case COMPRESSED_RGBA_ASTC_8x6_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR:
                return Math.floor((width + 7) / 8) * Math.floor((height + 5) / 6) * 16;
            case COMPRESSED_RGBA_ASTC_8x8_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR:
                return Math.floor((width + 7) / 8) * Math.floor((height + 7) / 8) * 16;
            case COMPRESSED_RGBA_ASTC_10x5_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR:
                return Math.floor((width + 9) / 10) * Math.floor((height + 4) / 5) * 16;
            case COMPRESSED_RGBA_ASTC_10x6_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR:
                return Math.floor((width + 9) / 10) * Math.floor((height + 5) / 6) * 16;
            case COMPRESSED_RGBA_ASTC_10x8_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR:
                return Math.floor((width + 9) / 10) * Math.floor((height + 7) / 8) * 16;
            case COMPRESSED_RGBA_ASTC_10x10_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR:
                return Math.floor((width + 9) / 10) * Math.floor((height + 9) / 10) * 16;
            case COMPRESSED_RGBA_ASTC_12x10_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR:
                return Math.floor((width + 11) / 12) * Math.floor((height + 9) / 10) * 16;
            case COMPRESSED_RGBA_ASTC_12x12_KHR:
            case COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR:
                return Math.floor((width + 11) / 12) * Math.floor((height + 11) / 12) * 16;

            default:
                return 0;
        }
    }
    
    // DXT formats, from:
    // http://www.khronos.org/registry/webgl/extensions/WEBGL_compressed_texture_s3tc/
    export const COMPRESSED_RGB_S3TC_DXT1_EXT = 0x83F0;
    export const COMPRESSED_RGBA_S3TC_DXT1_EXT = 0x83F1;
    export const COMPRESSED_RGBA_S3TC_DXT3_EXT = 0x83F2;
    export const COMPRESSED_RGBA_S3TC_DXT5_EXT = 0x83F3;

    // ATC formats, from:
    // http://www.khronos.org/registry/webgl/extensions/WEBGL_compressed_texture_atc/
    export const COMPRESSED_RGB_ATC_WEBGL = 0x8C92;
    export const COMPRESSED_RGBA_ATC_EXPLICIT_ALPHA_WEBGL = 0x8C93;
    export const COMPRESSED_RGBA_ATC_INTERPOLATED_ALPHA_WEBGL = 0x87EE;

    //ASTC formats
    //https://www.khronos.org/registry/webgl/extensions/WEBGL_compressed_texture_astc/
    export const COMPRESSED_RGBA_ASTC_4x4_KHR = 0x93B0;
    export const COMPRESSED_RGBA_ASTC_5x4_KHR = 0x93B1;
    export const COMPRESSED_RGBA_ASTC_5x5_KHR = 0x93B2;
    export const COMPRESSED_RGBA_ASTC_6x5_KHR = 0x93B3;
    export const COMPRESSED_RGBA_ASTC_6x6_KHR = 0x93B4;
    export const COMPRESSED_RGBA_ASTC_8x5_KHR = 0x93B5;
    export const COMPRESSED_RGBA_ASTC_8x6_KHR = 0x93B6;
    export const COMPRESSED_RGBA_ASTC_8x8_KHR = 0x93B7;
    export const COMPRESSED_RGBA_ASTC_10x5_KHR = 0x93B8;
    export const COMPRESSED_RGBA_ASTC_10x6_KHR = 0x93B9;
    export const COMPRESSED_RGBA_ASTC_10x8_KHR = 0x93BA;
    export const COMPRESSED_RGBA_ASTC_10x10_KHR = 0x93BB;
    export const COMPRESSED_RGBA_ASTC_12x10_KHR = 0x93BC;
    export const COMPRESSED_RGBA_ASTC_12x12_KHR = 0x93BD;

    /*
     No support for SRGB formats 
     - no way how to determine RGB vs SRGB from ASTC file
     */
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR = 0x93D0;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR = 0x93D1;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR = 0x93D2;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR = 0x93D3;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR = 0x93D4;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR = 0x93D5;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR = 0x93D6;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR = 0x93D7;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR = 0x93D8;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR = 0x93D9;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR = 0x93DA;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR = 0x93DB;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR = 0x93DC;
    export const COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR = 0x93DD;

    // DXT values and structures referenced from:
    // http://msdn.microsoft.com/en-us/library/bb943991.aspx/
    export const DDS_MAGIC = 0x20534444;
    export const DDSD_MIPMAPCOUNT = 0x20000;
    export const DDPF_FOURCC = 0x4;

    export const DDS_HEADER_LENGTH = 31; // The header length in 32 bit ints.

    // Offsets into the header array.
    export const DDS_HEADER_MAGIC = 0;

    export const DDS_HEADER_SIZE = 1;
    export const DDS_HEADER_FLAGS = 2;
    export const DDS_HEADER_HEIGHT = 3;
    export const DDS_HEADER_WIDTH = 4;

    export const DDS_HEADER_MIPMAPCOUNT = 7;

    export const DDS_HEADER_PF_FLAGS = 20;
    export const DDS_HEADER_PF_FOURCC = 21;

    // FourCC format identifiers.
    export const FOURCC_DXT1 = fourCCToInt32("DXT1");
    export const FOURCC_DXT3 = fourCCToInt32("DXT3");
    export const FOURCC_DXT5 = fourCCToInt32("DXT5");

    export const FOURCC_ATC = fourCCToInt32("ATC ");
    export const FOURCC_ATCA = fourCCToInt32("ATCA");
    export const FOURCC_ATCI = fourCCToInt32("ATCI");

    //===============//
    // PVR constants //
    //===============//

    // PVR formats, from:
    // http://www.khronos.org/registry/webgl/extensions/WEBGL_compressed_texture_pvrtc/
    export const COMPRESSED_RGB_PVRTC_4BPPV1_IMG = 0x8C00;
    export const COMPRESSED_RGB_PVRTC_2BPPV1_IMG = 0x8C01;
    export const COMPRESSED_RGBA_PVRTC_4BPPV1_IMG = 0x8C02;
    export const COMPRESSED_RGBA_PVRTC_2BPPV1_IMG = 0x8C03;

    // ETC1 format, from:
    // http://www.khronos.org/registry/webgl/extensions/WEBGL_compressed_texture_etc1/
    export const COMPRESSED_RGB_ETC1_WEBGL = 0x8D64;

    export const PVR_FORMAT_2BPP_RGB = 0;
    export const PVR_FORMAT_2BPP_RGBA = 1;
    export const PVR_FORMAT_4BPP_RGB = 2;
    export const PVR_FORMAT_4BPP_RGBA = 3;
    export const PVR_FORMAT_ETC1 = 6;
    export const PVR_FORMAT_DXT1 = 7;
    export const PVR_FORMAT_DXT3 = 9;
    export const PVR_FORMAT_DXT5 = 5;

    export const PVR_HEADER_LENGTH = 13; // The header length in 32 bit ints.
    export const PVR_MAGIC = 0x03525650; //0x50565203;

    // Offsets into the header array.
    export const PVR_HEADER_MAGIC = 0;
    export const PVR_HEADER_FORMAT = 2;
    export const PVR_HEADER_HEIGHT = 6;
    export const PVR_HEADER_WIDTH = 7;
    export const PVR_HEADER_MIPMAPCOUNT = 11;
    export const PVR_HEADER_METADATA = 12;

    //===============//
    // ASTC constants //
    //===============//
    export const ASTC_HEADER_LENGTH = 16; // The header length in bytes.
    export const ASTC_MAGIC = 0x5ca1ab13;
}