/// <reference path="DisplayObject.ts" />
/// <reference path="DisplayObjectContainerBase.ts" />
/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The Bitmap class implements the IBitmap<T> interface.
     *
     * A Bitmap is a single image that can be manipulated on the stage. A Bitmap also inherits all functionality from DisplayObject.
     * @internal
     */
    export class MovieClip extends DisplayObjectContainerBase<PIXI.extras.AnimatedSprite> implements IMovieClip
    {
        public onAnimationEnded: IPrivateEvent = createSimpleEvent();
        public onChanged: IPrivateEvent = createSimpleEvent();

        protected _playing: boolean;
        protected _fps: number;
        protected _fpsPerSixty: number;
        protected _interval: number;
        protected _delta: number;
        protected _frameCount: number;
        protected _shader: any = null;
        protected _tint: Util.Color = null;

        protected _assetOrFrame: Frame[] | Asset.IImageAsset[];

        /** Gets or sets the shader to be used for this sprite. */
        public get shader(): IShader | null { return this._shader; }
        public set shader(value: IShader | null)
        {
            this._shader = value;
        }

        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        constructor(frames: Frame[], fps: number);
        constructor(asset: Asset.IImageAsset[], fps: number);
        constructor(framesOrAsset: Frame[] | Asset.IImageAsset[], fps: number)
        {
            super();
            this.initialise(PIXI.extras.AnimatedSprite, framesOrAsset);

            this.alwaysTick = true;
            this._fps = fps;
            this._fpsPerSixty = this._fps / 60;
            this._interval = 1000 / this._fps;
            this._frameCount = 0;
            this._assetOrFrame = framesOrAsset;
        }

        /**
         * @inheritdoc
         */
        public clone(): MovieClip
        {
            return new MovieClip(this._assetOrFrame as Frame[], this.fps);
        }
        /**
         * @inheritdoc
         */
        public get paused(): boolean
        {
            return !this._playing;
        }
        /**
         * @inheritdoc
         */
        public get currentFrame(): number
        {
            return this._nativeObject.currentFrame;
        }
        /**
         * @inheritdoc
         */
        public get totalFrames(): number
        {
            return this._nativeObject.totalFrames;
        }
        /**
         * @inheritdoc
         */
        public play(): void
        {
            if (!this._playing)
            {
                this._delta = 0.0;
                this._playing = true;
                this._frameCount = 0;
            }
        }
        /**
         * @inheritdoc
         */
        public stop(): void
        {
            this._nativeObject.stop();
            if (this._playing)
            {
                this._playing = false;
            }
        }
        /**
         * @inheritdoc
         * @param frame @inheritdoc
         */
        public gotoAndStop(frame: number): void
        {
            if (this._playing)
            {
                this._playing = false;
            }
            this._nativeObject.gotoAndStop(frame);
            this.onChanged.publish(null);
        }
        /**
         * @inheritdoc
         * @param frame @inheritdoc
         */
        public gotoAndPlay(frame: number): void
        {
            this.gotoAndStop(frame);
            this.play();
        }
        /**
         * @inheritdoc
         * @param value @inheritdoc
         */
        public set fps(value: number)
        {
            this._fps = value;
            this._fpsPerSixty = this._fps / 60;
            this._interval = 1000 / this._fps;
        }

        /**
         * @inheritdoc
         */
        public get fps(): number
        {
            return this._fps;
        }

        protected gotoNextFrame(): void
        {
            let f: number = this._nativeObject.currentFrame + 1;
            if (f == this._nativeObject.totalFrames - 1)
            {
                //this.dispatchEvent(new DisplayEvent(DisplayEvent.ANIMATION_COMPLETE, this));
                this.onAnimationEnded.publish(null);
            }
            if (f >= this._nativeObject.totalFrames)
            {
                f = 0;
            }
            this._nativeObject.gotoAndStop(f);
            this.onChanged.publish(null);
        }

        protected onUpdate(delta: number): void
        {
            super.onUpdate(delta);
            if (this._playing)
            {
                this._delta += delta;
                if (this._delta > this._interval)
                {
                    while (this._delta > this._interval && this._playing)
                    {
                        this._delta -= this._interval;
                        this.gotoNextFrame();
                    }
                }
            }
        }
        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): PIXI.extras.AnimatedSprite; }, args: any[]): PIXI.extras.AnimatedSprite
        {
            const textures: PIXI.Texture[] = [];
            for (let i: number = 0; i < args[0].length; i++)
            {
                const imageData = (args[0][i] as Frame).imageData as SubTexture;

                if (imageData)
                {
                    textures.push(imageData.nativeObject);
                }
                else
                {
                    const asset = (args[0][i] as Asset.IImageAsset<Texture>);
                    const textureAsset = asset.asset;
                    if (textureAsset)
                    {
                        textures.push(new SubTexture(textureAsset, new ReadonlyRectangle(0, 0, textureAsset.width, textureAsset.height)).nativeObject);
                    }
                    else
                    {
                        textures.push(PIXI.Texture.from(""));
                    }
                }
            }
            return new PIXI.extras.AnimatedSprite(textures);
        }
    }
    Rendering.MovieClip = MovieClip;
}
