/// <reference path="DisplayObject.ts" />
/// <reference path="DisplayObjectContainerBase.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    export class IBitmapFont
    {
        public name: string;
        public size: number;
    }
}
/**
 * @internal
 */
namespace RS.Rendering.Pixi
{
    /**
     * The Bitmap class implements the IBitmap<T> interface.
     *
     * A Bitmap is a single image that can be manipulated on the stage. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class BitmapText extends DisplayObjectContainerBase<PIXI.extras.BitmapText> implements IBitmapText, IDisposable
    {
        /** Gets if this text has been disposed. */
        public get isDisposed() { return this._isDisposed; }
        /**
         * Gets or sets the content of this text.
         */
        @CheckDisposed
        public get text() { return this._nativeObject.text; }
        public set text(value) { this._nativeObject.text = value; }

        /**
         * Gets or sets the font of this text.
         */
        @CheckDisposed
        public get font() { return this._fontObject.name; }
        public set font(value: string)
        {
            this._fontObject.name = value;
            this._nativeObject.font = this._fontObject;
        }

        /**
         * Gets or sets the font size of this text.
         */
        @CheckDisposed
        public get fontSize() { return this._fontObject.size; }
        public set fontSize(value: number)
        {
            this._fontObject.size = value;
            this._nativeObject.font = this._fontObject;
        }

        /**
         * Gets or sets the wrap width of this text.
        */
        @CheckDisposed
        public get wrapWidth() { return this._nativeObject.maxWidth; }
        public set wrapWidth(value:number) { this._nativeObject.maxWidth = value; }

        /**
         * Gets or sets the horizontal alignment for this text.
         * Only applicable for multiline text.
         */
        @CheckDisposed
        public get align(): TextAlign { return this.fromPixiAlign(this._nativeObject.align); }
        public set align(value: TextAlign) { this._nativeObject.align = this.toPixiAlign(value); }

        /**
         * Gets or sets the tint of this text.
         */
        @CheckDisposed
        public get tint(): Util.Color
        {
            const col: Util.Color = new Util.Color();
            col.tint = this._nativeObject.tint;
            return col;
        }
        public set tint(value: Util.Color)
        {
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        @CheckDisposed
        public get anchorX(): number { return this.anchor.x; }
        public set anchorX(value: number) { this.anchor.x = value; }

        @CheckDisposed
        public get anchorY(): number { return this.anchor.y; }
        public set anchorY(value: number) { this.anchor.y = value; }

        @CheckDisposed
        public get localBounds(): ReadonlyRectangle
        {
            // Must be overridden this way due to nativeObject.getLocalBounds not accepting a rect parameter for PIXI.extras.BitmapText
            const rect = this._nativeObject.getLocalBounds();
            this._localBounds.set(rect.x, rect.y, rect.width, rect.height);
            return this._localBounds;
        }

        protected get anchor(): PIXI.Point
        {
            if (!this._nativeObject.anchor)
            {
                this._nativeObject.anchor = new PIXI.Point(0, 0);
            }
            return this._nativeObject.anchor as PIXI.Point;
        }

        protected _fontObject: IBitmapFont = { name: "", size: 1 };

        constructor(asset: Asset.IBitmapFontAsset, text: string = "")
        {
            super();
            this.initialise(PIXI.extras.BitmapText, asset);
            this.text = text;
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            super.dispose();
            this._isDisposed = true;
        }

        public getLocalBounds(): ReadonlyRectangle
        {
            return this.localBounds;
        }

        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): PIXI.extras.BitmapText; }, args: any[]): PIXI.extras.BitmapText
        {
            const asset: Asset.IBitmapFontAsset<string> = args[0];
            return new PIXI.extras.BitmapText("", { font: asset.asset });
        }

        protected toPixiAlign(align: TextAlign): string
        {
            const aligns: TextAlign[] = [TextAlign.Left, TextAlign.Middle, TextAlign.Right];
            const pixiAligns: string[] = ["left", "center", "right"];
            return pixiAligns[aligns.indexOf(align)];
        }

        protected fromPixiAlign(align: string): TextAlign
        {
            const aligns: TextAlign[] = [TextAlign.Left, TextAlign.Middle, TextAlign.Right];
            const pixiAligns: string[] = ["left", "center", "right"];
            return aligns[pixiAligns.indexOf(align)];
        }
    }
    Rendering.BitmapText = BitmapText;
}
