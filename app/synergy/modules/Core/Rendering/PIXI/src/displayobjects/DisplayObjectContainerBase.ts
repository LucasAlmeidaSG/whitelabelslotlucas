/// <reference path="DisplayObject.ts" />

/**
 * @internal
 */
namespace RS.Rendering.Pixi
{
    export type Child<TChild extends IDisplayObject | IFastBitmap> = (GenericObject | FastBitmap) & TChild;

    @RS.HasCallbacks
    export class DisplayObjectContainerBase<T extends PIXI.Container = PIXI.Container, TChild extends IDisplayObject | IFastBitmap = IDisplayObject> extends RS.Rendering.Pixi.DisplayObject<T> implements RS.Rendering.IBaseContainer<TChild>
    {
        @AutoDispose
        public readonly onChildAdded: RS.IPrivateEvent<Child<TChild>> = RS.createEvent<Child<TChild>>();
        @AutoDispose
        public readonly onChildRemoved: RS.IPrivateEvent<Child<TChild>> = RS.createEvent<Child<TChild>>();

        @CheckDisposed
        public get visible(): boolean { return this._nativeObject.visible; }
        public set visible(value: boolean)
        {
            this._nativeObject.visible = value;
            if (!value)
            {
                this.clearPointerInteraction();
                this.clearChildPointerInteractions();
            }
        }

        protected _prePostRenderTree: GenericObject[];
        public get prePostRenderTree(): ReadonlyArray<GenericObject> { return this._prePostRenderTree; }

        protected _sortFunction: ((a: Child<TChild>, b: Child<TChild>) => number) | null;
        public get sortFunction() { return this._sortFunction; }
        public set sortFunction(value)
        {
            this._sortFunction = value;
            if (value) { this.sortChildren(); }
        }

        public get interactiveChildren() { return this.nativeObject.interactiveChildren; }
        public set interactiveChildren(value: boolean)
        {
            value = value == null ? true : value;
            this.nativeObject.interactiveChildren = value;
            // Account for parent child interactivity
            if (!this.checkChildrenInteractive())
            {
                this.clearChildPointerInteractions();
            }
        }

        protected _children: Child<TChild>[];
        public get children(): ReadonlyArray<Child<TChild>> { return this._children; }

        constructor(name?: string)
        {
            super();
            this._children = [];
        }

        public registerPreRenderChild(child: GenericObject)
        {
            if (!this._prePostRenderTree) { this._prePostRenderTree = []; }
            PrePostRenderTree.add(this._prePostRenderTree, child, this);
            if (this.parent)
            {
                (this.parent as GenericContainer).registerPreRenderChild(child);
            }
        }

        public unregisterPreRenderChild(child: GenericObject)
        {
            if (!this._prePostRenderTree) { return; }
            PrePostRenderTree.remove(this._prePostRenderTree, child);
            if (this.parent)
            {
                (this.parent as GenericContainer).unregisterPreRenderChild(child);
            }
        }

        public dispose()
        {
            if (this.isDisposed) { return; }
            this.removeAllChildren();
            super.dispose();
        }

        public hasChild(child: Child<TChild>, deep?: boolean): boolean
        {
            if (deep)
            {
                let parent = this.parent;
                while (parent)
                {
                    if ((parent as GenericContainer)._children)
                    {
                        if ((parent as GenericContainer)._children.indexOf(child) != -1)
                        {
                            return true;
                        }
                    }
                    parent = parent.parent;
                }

                return this.checkChildren(this, child);
            }
            return this._children.indexOf(child) !== -1;
        }

        public getChildIndex(child: Child<TChild>): number
        {
            return this._children.indexOf(child);
        }

        public setChildIndex(child: Child<TChild>, index: number): this
        {
            if (this._sortFunction) { return; }
            if (this.getChildIndex(child) !== -1)
            {
                if (index < 0)
                {
                    index = 0;
                }
                if (index > this._children.length - 1)
                {
                    index = this._children.length - 1;
                }
                const curIndex: number = this.getChildIndex(child);
                this._nativeObject.setChildIndex(child.nativeObject, index);
                this._children.splice(curIndex, 1);
                this._children.splice(index, 0, child);
            }
            return this;
        }

        public addChild(child: Child<TChild>, index?: number): this
        {
            if (this.getChildIndex(child) === -1)
            {
                if (child.parent)
                {
                    (child.parent as GenericContainer)._removeChild(child, false);
                }

                if (this._sortFunction)
                {
                    index = RS.Util.insert(child, this._children, (candidate, item) => this._sortFunction(candidate, item) > 0 ? -1 : 0);
                    this.nativeObject.addChildAt(child.nativeObject, index);
                }
                else
                {
                    if (index > -1)
                    {
                        this.nativeObject.addChildAt(child.nativeObject, index);
                        this._children.splice(index, 0, child);
                    }
                    else
                    {
                        this._children.push(child);
                        this.nativeObject.addChild(child.nativeObject);
                    }
                }

                child.stage = this instanceof RS.Rendering.Stage ? this : this.stage;
                child.parent = this as IContainer;
                child.onAdded();

                this.onChildAdded.publish(child);
            }
            return this;
        }

        public addChildUnder(childToAdd: Child<TChild>, targetChild: Child<TChild>): this
        {
            const index = this.getChildIndex(targetChild);
            if (index > -1) { this.addChild(childToAdd, index); }
            return this;
        }

        public addChildAbove(childToAdd: Child<TChild>, targetChild: Child<TChild>): this
        {
            const index = this.getChildIndex(targetChild);
            if (index > -1) { this.addChild(childToAdd, index + 1); }
            return this;
        }

        public addChildren(children: ReadonlyArray<Child<TChild>>, index?: number): this;
        public addChildren(...children: Child<TChild>[]): this;
        public addChildren(...children: any[]): this
        {
            let index: number = -1;
            if (RS.Is.array(children[0]))
            {
                index = children[1] ? children[1] : -1;
                children = children[0];
            }
            for (let i: number = 0; i < children.length; i++)
            {
                if (index !== -1)
                {
                    this.addChild(children[i], index + i);
                }
                else
                {
                    this.addChild(children[i]);
                }
            }
            return this;
        }

        public removeChild(child: Child<TChild>): this
        {
            this._removeChild(child);
            return this;
        }

        public removeChildren(children: ReadonlyArray<Child<TChild>>): this;
        public removeChildren(...children: Child<TChild>[]): this;
        public removeChildren(...children: any[]): this
        {
            if (children.length == 0) { Log.warn("[DisplayObjectContainer] removeChildren() called with no arguments - did you mean removeAllChildren()?"); }
            if (RS.Is.array(children[0])) { children = children[0]; }

            children = children.concat();
            for (let i: number = 0; i < children.length; i++)
            {
                this.removeChild(children[i]);
            }

            return this;
        }

        public removeAllChildren(): this
        {
            this.removeChildren(this._children);
            this._children = [];
            return this;
        }

        public removeChildAt(index: number): TChild
        {
            const child = this.children[index];
            if (child)
            {
                this.removeChild(child);
            }
            return child;
        }

        public removeChildRange(startIndex: number, endIndex: number): Child<TChild>[]
        {
            const range = endIndex - startIndex;
            if (range > 0 && range <= endIndex)
            {
                const removed = this.children.concat().splice(startIndex, range);
                for (let i: number = 0; i < removed.length; i++)
                {
                    this.removeChild(removed[i]);
                }
                return removed;
            }
            return [];
        }

        public getChild(filter: (element: Child<TChild>) => boolean, deep: boolean = false): Child<TChild>
        {
            for (const child of this._children)
            {
                if (filter(child))
                {
                    return child;
                }
            }

            if (deep)
            {
                for (const child of this._children)
                {
                    if (child instanceof DisplayObjectContainerBase)
                    {
                        const subChild = child.getChild(filter, true);
                        if (subChild) { return subChild; }
                    }
                }
            }

            return null;
        }

        public getChildAt(index: number): Child<TChild>
        {
            return this.children[index];
        }

        public getChildByName(name: string, deep: boolean = false): Child<TChild>
        {
            return this.getChild((element) => element.name === name, deep);
        }

        public swapChildren(a: Child<TChild>, b: Child<TChild>): this
        {
            if (this._sortFunction) { return; }
            const indexA: number = this._children.indexOf(a);
            const indexB: number = this._children.indexOf(b);
            this.setChildIndex(a, indexB);
            this.setChildIndex(b, indexA);
            return this;
        }

        public replaceChild(a: Child<TChild>, b: Child<TChild>): this
        {
            if (this._sortFunction) { return; }
            const index = this._children.indexOf(a);
            if (index === -1)
            {
                Log.error("[DisplayObjectContainer] replaceChild was called, but the child to be replaced was not found. Aborting.");
                return this;
            }
            this.removeChildAt(index);
            this.addChild(b, index);
            return this;
        }

        public sortChildren(sortFunc: (a: Child<TChild>, b: Child<TChild>) => number = this._sortFunction): this
        {
            if (!sortFunc)
            {
                Log.warn("[DisplayObjectContainer] sortChildren() called with no sort function defined!");
                return this;
            }

            this._children.sort(sortFunc);
            for (let i = 0, l = this._children.length; i < l; ++i)
            {
                this._nativeObject.children[i] = this._children[i].nativeObject;
            }
            return this;
        }

        public moveToTop(child: Child<TChild>): this
        {
            if (this._sortFunction) { return; }
            const index: number = this.getChildIndex(child);
            if (index > -1)
            {
                this.setChildIndex(child, 999999);
            }
            return this;
        }

        public moveToBottom(child: Child<TChild>): this
        {
            if (this._sortFunction) { return; }
            const index: number = this.getChildIndex(child);
            if (index > -1)
            {
                this.setChildIndex(child, -2);
            }
            return this;
        }

        public onAdded(): void
        {
            super.onAdded();
            for (const child of this.children)
            {
                child.stage = this.stage;
                child.onAdded();
            }
        }

        public onRemoved(): void
        {
            super.onRemoved();
            for (const child of this.children)
            {
                child.stage = null;
                child.onRemoved();
                this.clearPointerInteractionsOnChild(child);
            }
        }

        public onAddedInternal(container: GenericContainer): void
        {
            super.onAddedInternal(container);
            for (const child of this.children)
            {
                child.stage = this.stage;
                child.onAddedInternal(container);
            }
        }

        public onRemovedInternal(container: GenericContainer): void
        {
            super.onRemovedInternal(container);
            for (const child of this.children)
            {
                child.stage = this.stage;
                child.onRemovedInternal(container);
            }
        }

        /**
         * Returns whether or not a child could be interacted with, regardless of its interactive state.
         * @internal
         */
        public checkChildrenInteractive()
        {
            if (!this.interactiveChildren) { return false; }
            if (this.parent instanceof DisplayObjectContainerBase) { return this.parent.checkChildrenInteractive(); }
            return true;
        }

        /**
         * Clears any pointer interactions on all children.
         */
        public clearChildPointerInteractions()
        {
            for (const child of this._children)
            {
                this.clearPointerInteractionsOnChild(child);
            }
        }

        protected _removeChild(child: Child<TChild>, unsetParent: boolean = true): boolean
        {
            const index: number = this.getChildIndex(child);
            if (index > -1)
            {
                if (unsetParent)
                {
                    child.stage = null;
                    child.parent = null;
                }

                this._children.splice(index, 1);
                if (this.nativeObject != null)
                {
                    this.nativeObject.removeChild(child.nativeObject);
                }
                this.onChildRemoved.publish(child);
                child.onRemoved();

                return true;
            }

            return false;
        }

        protected checkChildren(ob: Child<TChild> | this, child: Child<TChild>): boolean
        {
            let hasChild: boolean = false;
            if (ob instanceof DisplayObjectContainerBase)
            {
                hasChild = ob.children.indexOf(child) !== -1;
                const children = ob.children;
                let i: number = 0;
                while (i < children.length && !hasChild)
                {
                    hasChild = this.checkChildren(children[i], child);
                    i++;
                }
            }
            return hasChild;
        }

        protected clearPointerInteractionsOnChild(child: IDisplayObject | IFastBitmap)
        {
            if (child instanceof DisplayObject)
            {
                child.clearPointerInteraction();
            }
            if (child instanceof DisplayObjectContainerBase)
            {
                child.clearChildPointerInteractions();
            }
        }
    }

    export type GenericContainer = DisplayObjectContainerBase<PIXI.Container, IDisplayObject | IFastBitmap>;
}