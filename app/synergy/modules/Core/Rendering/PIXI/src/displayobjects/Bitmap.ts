/// <reference path="DisplayObjectContainerBase.ts" />
/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The Bitmap class implements the IBitmap<T> interface.
     *
     * A Bitmap is a single image that can be manipulated on the stage. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class Bitmap extends DisplayObjectContainerBase<PIXI.Sprite> implements IBitmap
    {
        protected _shader: IShader = null;
        protected _tint: Util.Color = null;
        protected _subTexture: SubTexture = null;

        @CheckDisposed
        public get texture() { return this._subTexture; }
        public set texture(value)
        {
            this._subTexture = value;
            if (value)
            {
                this._nativeObject.texture = value.nativeObject;
                const baseTex = value.texture;
                if (baseTex && baseTex.baseTexture)
                {
                    if (baseTex.baseTexture.premultipliedAlpha)
                    {
                        this.blendMode = BlendConvert.withoutNPM(this.blendMode || BlendMode.Normal);
                    }
                    else
                    {
                        this.blendMode = BlendConvert.withNPM(this.blendMode || BlendMode.Normal);
                    }
                }
            }
            else
            {
                this._nativeObject.texture = null;
            }
        }

        @CheckDisposed
        public get anchorX(): number { return this._nativeObject.anchor.x; }
        public set anchorX(value: number) { this._nativeObject.anchor.x = value; }

        @CheckDisposed
        public get anchorY(): number { return this._nativeObject.anchor.y; }
        public set anchorY(value: number) { this._nativeObject.anchor.y = value; }

        /** Gets or sets the shader to be used for this sprite. */
        public get shader(): IShader | null { return this._shader; }
        public set shader(value: IShader | null)
        {
            this._shader = value;
        }

        @CheckDisposed
        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        constructor(frame: Frame);
        constructor(asset: Asset.IImageAsset<any>);
        constructor(texture: SubTexture);

        constructor(frameOrAssetOrTexture: Frame | Asset.IImageAsset<Texture> | SubTexture)
        {
            super();

            this.initialise(PIXI.Sprite, frameOrAssetOrTexture);
            if (this._subTexture != null && this._subTexture.texture != null && this._subTexture.texture.baseTexture != null)
            {
                if (this._subTexture.texture.baseTexture.premultipliedAlpha)
                {
                    this.blendMode = BlendConvert.withoutNPM(this.blendMode || BlendMode.Normal);
                }
                else
                {
                    this.blendMode = BlendConvert.withNPM(this.blendMode || BlendMode.Normal);
                }
            }
        }
        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): PIXI.Sprite; }, args: any[]): PIXI.Sprite
        {
            const frameOrAssetOrTexture: Frame | Asset.IImageAsset<Texture> | SubTexture = args[0];
            let subTexture: SubTexture;
            if (frameOrAssetOrTexture instanceof SubTexture)
            {
                subTexture = frameOrAssetOrTexture;
            }
            else if (Is.frame(frameOrAssetOrTexture))
            {
                subTexture = frameOrAssetOrTexture.imageData as SubTexture;
            }
            else if (frameOrAssetOrTexture != null)
            {
                const baseTexture = (frameOrAssetOrTexture as Asset.IImageAsset<Texture>).asset;
                if (baseTexture)
                {
                    subTexture = new SubTexture(
                        baseTexture,
                        new ReadonlyRectangle(0, 0, baseTexture ? baseTexture.width : 1, baseTexture ? baseTexture.height : 1)
                    );
                }
            }
            this._subTexture = subTexture;
            return new Sprite(subTexture && subTexture.nativeObject);
        }
    }
    Rendering.Bitmap = Bitmap;
}
