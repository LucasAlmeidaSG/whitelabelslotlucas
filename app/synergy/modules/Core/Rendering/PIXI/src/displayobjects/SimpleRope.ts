/// <reference path="DisplayObjectContainerBase.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The SimpleRope class implements the ISimpleRope interface.
     *
     * A SimpleRope encapsulates a polygonal SimpleRope consisting of vertices that make up triangles. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class SimpleRopeDisplayObject extends DisplayObject<PIXI.mesh.Rope> implements Rendering.ISimpleRope
    {
        protected _shader: IShader = null;
        protected _tint: Util.Color = null;
        protected _subTexture: ISubTexture = null;
        protected _positions: RS.Math.Vector2D[];

        /** Re-calculate vertices by rope points each frame */
        public get autoUpdate(): boolean { return this._nativeObject.autoUpdate; }
        public set autoUpdate(value: boolean)
        {
            this._nativeObject.autoUpdate = value;
        }

        /** Gets or sets the texture to display for this SimpleRope. */
        public get texture() { return this._subTexture; }
        public set texture(value)
        {
            this._subTexture = value;
            this._nativeObject.texture = value ? (value as SubTexture).nativeObject : null;
        }

        /** Gets or sets the shader to be used for this sprite. */
        public get shader(): IShader | null { return this._shader; }
        public set shader(value: IShader | null)
        {
            this._shader = value;
        }

        /** Gets or sets the tint of this SimpleRope. */
        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        /**
         * Gets or sets the vertices buffer.
         */
        public get positions(): Float32Array { return this._nativeObject.vertices; };
        public set positions(value: Float32Array)
        {
            this._nativeObject.vertices = value;
        }

        /**
         * Gets or sets the points.
         */
        public get points(): RS.Math.Vector2D[] { return this._nativeObject.points; };
        public set points(value: RS.Math.Vector2D[])
        {
            // Convert RS.Math.Vector2D to PIXI.Point
            this._nativeObject.points = value.map((position) => new PIXI.Point(position.x, position.y));;
        }

        /**
         * Gets or sets the indices buffer.
         * This buffer is mandatory, and must be 3 indices per polygon if draw mode is triangles, or 2 + 1 indices per polygon if draw mode is triangle strip.
         */
        public get indices(): Uint16Array { return this._nativeObject.indices; }
        public set indices(value: Uint16Array)
        {
            this._nativeObject.indices = value;
        }

        /**
         * Gets or sets the texture coordinates buffer.
         */
        public get texCoords(): Float32Array { return this._nativeObject.uvs; }
        public set texCoords(value: Float32Array)
        {
            this._nativeObject.uvs = value;

            // Should call after you change uvs manually
            this._nativeObject.multiplyUvs();
        }

        constructor(settings: ISimpleRope.Settings, texture?: ISubTexture)
        {
            super();

            this.initialise(PIXI.mesh.Rope, settings, texture);

            // Set the sub texture
            this.texture = texture;
        }

        /**
         * Update the rope points to match a path
         *
         * This can be used along with a BezierTween
         * to update the rope as it travels along a path
         * onChange: () => this.bendWhileMoving)
         *
         * @param path the patch to match
         * @param container container to local of the points coordinates system
         */
        public recalculatePointsForPath(path: RS.Bezier.Path, container?: RS.Rendering.IDisplayObject): void
        {
            // Where on the path
            let closestSeg = path.sample(0);
            let foundAt = 0;

            // To compare old with new and avoid glitches
            const oldPoints = this.points;

            // Desired width over the path
            const spriteWidth = this.texture.width;

            // Find where it is on the path
            for (let i = 0; i < path.length - 1; i++)
            {
                const segment = path.sample(i);

                if (RS.Math.Vector2D.distanceBetween(this.position, segment) < RS.Math.Vector2D.distanceBetween(this.position, closestSeg))
                {
                    // Start here on the path
                    closestSeg = segment;
                    foundAt = i;
                }
            }

            const newVerts: RS.Math.Vector2D[] = [];
            let totalDistance = 0;

            // Get points along the desired length along the path
            for (let t = 0; t < path.length; t++)
            {
                const segId = (foundAt - t);
                if (segId < 0)
                {
                    break;
                }

                const segment = path.sample(segId);

                // Have we got past how long the rope is
                totalDistance = RS.Math.Vector2D.distanceBetween(closestSeg, segment);
                if (totalDistance >= spriteWidth)
                {
                    break;
                }

                // New verts along path
                newVerts.push(segment);
            }

            // Generate new points using container as local if provided
            let newPoints = newVerts.map((v) => this.toLocal(v, container));

            // If our points is shorter than the current length, it
            // can cause rendering issues when displaying.
            // so update our new array to match the old.
            // NOTE: We can perhaps get round this be forcing the rope to refresh(true)
            const lengthDifference = oldPoints.length - newPoints.length;
            const threshold = 1;
            if (lengthDifference > threshold)
            {
                // Generate points matching point 1 of new points
                const generator = ((v: RS.Math.Vector2D, max: number) =>
                {
                    const extraPointsArray: RS.Math.Vector2D[] = [];
                    for (let index = 0; index < max; index++)
                    {
                        extraPointsArray.push(v);
                    }

                    return extraPointsArray;
                });

                const extraPoints = generator(newPoints[0], lengthDifference);

                // Regen the array, should match length
                // TODO: Assert points matching
                newPoints.unshift(...extraPoints)
            }

            // Set verts on rope
            this.points = newPoints;
        }

        /**
         * Refreshes uvs for generated meshes (rope, plane)
         * sometimes refreshes vertices too
         *
         * @param {boolean} [forceUpdate=false] if true, matrices will be updated any case
         */
        public refresh(forceUpdate: boolean)
        {
            this._nativeObject.refresh(forceUpdate);
        }

        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): PIXI.mesh.Rope; }, args: any[]): PIXI.mesh.Rope
        {
            const settings: ISimpleRope.Settings = args[0];
            const texture: SubTexture | null = args[1];

            // Build a PIXI rope as our native object converting Vector2D to points
            const points = settings.points.map((position) => new PIXI.Point(position.x, position.y));
            return new PIXI.mesh.Rope(texture && texture.nativeObject, points);
        }

    }

    Rendering.SimpleRope = SimpleRopeDisplayObject;
}
