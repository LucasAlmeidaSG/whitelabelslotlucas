/// <reference path="DisplayObjectContainerBase.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The SimpleMesh class implements the ISimpleMesh interface.
     *
     * A SimpleMesh encapsulates a polygonal SimpleMesh consisting of vertices that make up triangles. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class SimpleMeshDisplayObject extends DisplayObject<PIXI.mesh.Mesh> implements Rendering.ISimpleMesh
    {
        protected _shader: IShader = null;
        protected _tint: Util.Color = null;
        protected _subTexture: ISubTexture = null;

        /** Gets or sets the texture to display for this SimpleMesh. */
        public get texture() { return this._subTexture; }
        public set texture(value)
        {
            this._subTexture = value;
            this._nativeObject.texture = value ? (value as SubTexture).nativeObject : null;
        }

        /** Gets or sets the shader to be used for this sprite. */
        public get shader(): IShader | null { return this._shader; }
        public set shader(value: IShader | null)
        {
            this._shader = value;
        }

        /** Gets or sets the tint of this SimpleMesh. */
        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        /**
         * Gets or sets the vertices buffer.
         * This buffer is mandatory, and may be 2-4 elements per vertex. (XYZW)
         * The number of elements per vertex MUST match the size of the position attribute in the shader.
         */
        public get positions(): Float32Array { return this._nativeObject.vertices; };
        public set positions(value: Float32Array)
        {
            this._nativeObject.vertices = value;
        }

        /**
         * Gets or sets the indices buffer.
         * This buffer is mandatory, and must be 3 indices per polygon if draw mode is triangles, or 2 + 1 indices per polygon if draw mode is triangle strip.
         */
        public get indices(): Uint16Array { return this._nativeObject.indices; }
        public set indices(value: Uint16Array)
        {
            this._nativeObject.indices = value;
        }

        /**
         * Gets or sets the texture coordinates buffer.
         * This buffer is mandatory, and must be 2 elements per vertex. (UV)
         */
        public get texCoords(): Float32Array { return this._nativeObject.uvs; }
        public set texCoords(value: Float32Array)
        {
            this._nativeObject.uvs = value;
        }

        constructor(settings: ISimpleMesh.Settings, texture?: ISubTexture)
        {
            super();

            this.initialise(PIXI.mesh.Mesh, settings, texture);

            // Set the sub texture
            this.texture = texture;
        }

        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): PIXI.mesh.Mesh; }, args: any[]): PIXI.mesh.Mesh
        {
            const settings: ISimpleMesh.Settings = args[0];
            const texture: SubTexture | null = args[1];

            const drawMode = settings.drawMode != null ? settings.drawMode === Rendering.ISimpleMesh.DrawMode.Triangles ? PIXI.mesh.Mesh.DRAW_MODES.TRIANGLES : PIXI.mesh.Mesh.DRAW_MODES.TRIANGLE_MESH : null;
            return new PIXI.mesh.Mesh(texture && texture.nativeObject, settings.positions, settings.texCoords, settings.indices, drawMode);
        }
    }

    Rendering.SimpleMesh = SimpleMeshDisplayObject;
}
