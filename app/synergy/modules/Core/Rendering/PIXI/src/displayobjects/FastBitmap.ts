/// <reference path="DisplayObjectContainerBase.ts" />
/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The Bitmap class implements the IBitmap<T> interface.
     *
     * A Bitmap is a single image that can be manipulated on the stage. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class FastBitmap implements IFastBitmap
    {
        public stage: IStage = null;
        public parent: IFastContainer = null;
        public shader: IShader | null = null;

        public get name() { return this._nativeObject.name; }
        public set name(value) { this._nativeObject.name = value; }

        public get alpha() { return this._nativeObject.alpha; }
        public set alpha(value) { this._nativeObject.alpha = value; }

        private _nativeObject: PIXI.Sprite;
        public get nativeObject() { return this._nativeObject; }

        public get rootParent(): IDisplayObject
        {
            const p = this.parent;
            const p2 = p ? p.rootParent : null;
            return p2 ? p2 : (p ? p : null);
        }

        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }

        private _subTexture: SubTexture = null;
        public get texture() { return this._subTexture; }
        public set texture(value)
        {
            this._subTexture = value;
            this._nativeObject.texture = value ? value.nativeObject : null;
        }

        public get visible() { return this._nativeObject.visible; }
        public set visible(value) { this._nativeObject.visible = value; }

        public get x() { return this._nativeObject.x; }
        public set x(value: number) { this._nativeObject.x = value; }

        public get y() { return this._nativeObject.y; }
        public set y(value: number) { this._nativeObject.y = value; }

        public get scaleX() { return this._nativeObject.scale.x; }
        public set scaleX(value) { this._nativeObject.scale.x = value; }

        public get scaleY() { return this._nativeObject.scale.y; }
        public set scaleY(value) { this._nativeObject.scale.y = value; }

        public get pivotX() { return this._nativeObject.pivot.x; }
        public set pivotX(value) { this._nativeObject.pivot.x = value; }

        public get pivotY() { return this._nativeObject.pivot.y; }
        public set pivotY(value) { this._nativeObject.pivot.y = value; }

        public get anchorX(): number { return this._nativeObject.anchor.x; }
        public set anchorX(value: number) { this._nativeObject.anchor.x = value; }

        public get anchorY(): number { return this._nativeObject.anchor.y; }
        public set anchorY(value: number) { this._nativeObject.anchor.y = value; }

        public get rotation() { return this._nativeObject.rotation; }
        public set rotation(value) { this._nativeObject.rotation = value; }

        public get rotationDegrees(): number { return Math.radiansToDegrees(this._nativeObject.rotation); }
        public set rotationDegrees(value: number) { this._nativeObject.rotation = Math.degreesToRadians(value); }

        private _tint: Util.Color = null;
        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        constructor(texture: SubTexture)
        {
            this._subTexture = texture;
            this._nativeObject = new Sprite(this._subTexture && this._subTexture.nativeObject);
            this._nativeObject["_wrapper"] = this;
        }

        public setTransform(x: number, y: number, scaleX: number, scaleY: number, rotation: number, pivotX: number, pivotY: number): this
        {
            this._nativeObject.setTransform(x, y, scaleX, scaleY, rotation, 0, 0, pivotX, pivotY);
            return this;
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            if (this.parent) { this.parent.removeChild(this); }
            this._nativeObject.destroy();
            this._nativeObject = null;
            this._isDisposed = true;
        }

        public onAdded(): void { /* Do nothing. */ }
        public onRemoved(): void { /* Do nothing. */ }

        public onAddedInternal(container: GenericContainer): void { /* Do nothing. */ }
        public onRemovedInternal(container: GenericContainer): void { /* Do nothing. */ }
    }

    Rendering.FastBitmap = FastBitmap;
}
