/// <reference path="DisplayObjectContainerBase.ts" />
/// <reference path="./SimpleRope.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The AnimatedSimpleRope class implements the IAnimatedSimpleRope interface.
     *
     * A AnimatedSimpleRope encapsulates a polygonal AnimatedSimpleRope consisting of vertices that make up triangles. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class AnimatedSimpleRopeDisplayObject extends SimpleRope implements Rendering.IAnimatedSimpleRope
    {
        private _texture: RS.Rendering.ISubTexture;
        @RS.AutoDisposeOnSet private _animatedSprite: RS.Rendering.IAnimatedSprite;

        /**
         * Get the animated sprite
         */
        public get animatedSprite(): RS.Rendering.IAnimatedSprite
        {
            return this._animatedSprite;
        }

        constructor(public readonly settings: IAnimatedSimpleRope.Settings, texture?: ISubTexture)
        {
            super(settings, texture);

            // Setup a sprite which animates and updates the rope
            this.setupSprite();

            // Get current texture
            this.getFrameTexture();
        }

        /**
            Create a sprite which updates the rope on it's frame change
         */
        protected setupSprite()
        {
            const asset = this.settings.asset;
            const animation = this.settings.animation;
            const playSettings = this.settings.playSettings || {};
            this._animatedSprite = RS.Factory.sprite(asset);
            this._animatedSprite.gotoAndPlay(animation, playSettings);
            this._animatedSprite.onChanged(this.getFrameTexture);
        }

        /**
            Update the texture based on the sprite frame
         */
        @RS.Callback
        protected getFrameTexture()
        {
            const asset = this.settings.asset;

            const rawAsset = RS.Asset.Store.getAsset(asset);
            const rawSpritesheet = rawAsset.asset as RS.Rendering.ISpriteSheet;

            const frames = rawSpritesheet.getFrames();
            const frame = frames[this._animatedSprite.currentFrame];

            this.texture = frame.imageData;
        }
    }

    Rendering.AnimatedSimpleRope = AnimatedSimpleRopeDisplayObject;
}
