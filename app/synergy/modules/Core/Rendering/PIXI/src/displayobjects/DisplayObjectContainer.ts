/// <reference path="DisplayObject.ts" />
/// <reference path="DisplayObjectContainerBase.ts" />

/**
 * @internal
 */
namespace RS.Rendering.Pixi
{
    @HasCallbacks
    export class DisplayObjectContainer<TChild extends IDisplayObject | IFastBitmap = IDisplayObject> extends DisplayObjectContainerBase<PIXI.Container, TChild> implements IContainer<TChild>
    {
        constructor(name?: string)
        {
            super();
            this.initialise(PIXI.Container, null);
            if (name) { this.name = name; }
        }

        @CheckDisposed
        public simulateMouse(type: Rendering.MouseEventType, position: Math.Vector2D)
        {
            if (type == Rendering.MouseEventType.Click)
            {
                this.simulateMouse(Rendering.MouseEventType.Down, position);
                const release = Tween.wait(1).call(() => this.simulateMouse(Rendering.MouseEventType.Up, position));
                Disposable.bind(release, this);
                return;
            }

            const interactionManager = (this.stage as Pixi.Stage).renderer.plugins.interaction as PIXI.interaction.InteractionManager;

            const data = new PIXI.interaction.InteractionData();
            data.height = 1;
            data.width = 1;
            data.identifier = 1;
            data.global.x = position.x;
            data.global.y = position.y;
            data.pointerType = "mouse";
            data.isPrimary = true;
            data.pressure = 0.5;

            const ev = new PIXI.interaction.InteractionEvent();
            ev.data = data;

            switch (type)
            {
                case Rendering.MouseEventType.Up:
                    data.button = 0;
                    data.buttons = 0;
                    ev.type = "pointerup";
                    interactionManager.processInteractive(ev, this.nativeObject, interactionManager.processPointerUp, true, true);
                    break;
                case Rendering.MouseEventType.Down:
                    data.button = 0;
                    data.buttons = 1;
                    ev.type = "pointerdown";
                    interactionManager.processInteractive(ev, this.nativeObject, interactionManager.processPointerDown, true, true);
                    break;
                case Rendering.MouseEventType.Moved:
                    data.button = -1;
                    data.buttons = 0;
                    ev.type = "pointermove";
                    interactionManager.processInteractive(ev, this.nativeObject, interactionManager.processPointerMove, true, true);
                    break;
            }
        }
    }

    Rendering.DisplayObjectContainer = DisplayObjectContainer;
}
