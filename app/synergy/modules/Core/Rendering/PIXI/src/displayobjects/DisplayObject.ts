/** @internal */
namespace RS.Rendering.Pixi
{
    const tmpPoint1 = new PIXI.Point();
    const tmpPoint2 = new PIXI.Point();
    const tmpRect = new PIXI.Rectangle();
    const pointHitsVec = RS.Math.Vector2D();

    /** Tracks creation and disposal of display objects. */
    export const DisplayObjectTracker = new Util.ObjectTracker<IDisplayObject>();
    RS.Rendering.DisplayObjectTracker = DisplayObjectTracker;

    interface PointerInteraction
    {
        position: RS.Math.Vector2D;
        over: boolean;
        leftDown: boolean;
        rightDown: boolean;
        clickValid: boolean;
    }

    export interface RenderEventData extends RS.Rendering.RenderEventData
    {
        renderer: GenericRenderer;
    }

    function createWatchedEvent<T>(cb: (listenerCount: number) => void)
    {
        const ev = createEvent<T>(true);
        ev.listenerCount.onChanged(cb);
        return ev;
    }

    export type GenericRenderer = PIXI.WebGLRenderer | PIXI.CanvasRenderer;
    export type GenericObject = DisplayObject<PIXI.DisplayObject>;

    @HasCallbacks
    @Util.Tracked(DisplayObjectTracker)
    export class DisplayObject<T extends PIXI.DisplayObject> implements Rendering.IDisplayObject
    {
        // #region EVENTS

        public get onRightClicked() { return this._interactionBundle.onRightClicked; }
        public get onRightDown() { return this._interactionBundle.onRightDown; }
        public get onRightUp() { return this._interactionBundle.onRightUp; }
        public get onRightUpOutside() { return this._interactionBundle.onRightUpOutside; }
        public get onClicked() { return this._interactionBundle.onClicked; }
        public get onClickCancelled() { return this._interactionBundle.onClickCancelled; }
        public get onDown() { return this._interactionBundle.onDown; }
        public get onMoved() { return this._interactionBundle.onMoved; }
        public get onOut() { return this._interactionBundle.onOut; }
        public get onOver() { return this._interactionBundle.onOver; }
        public get onUp() { return this._interactionBundle.onUp; }
        public get onUpOutside() { return this._interactionBundle.onUpOutside; }

        @CheckDisposed public get onPreRender() { return this._onPreRender || (this._onPreRender = createWatchedEvent(() => this.evaluateNeedsPrePostRender())); }
        @AutoDisposeOnSet protected _onPreRender: IPrivateWatchedEvent<RenderEventData>;

        @CheckDisposed public get onPostRender() { return this._onPostRender || (this._onPostRender = createWatchedEvent(() => this.evaluateNeedsPrePostRender())); }
        @AutoDisposeOnSet protected _onPostRender: IPrivateWatchedEvent<RenderEventData>;

        public get onUpdated() { return this._onUpdated || (this._onUpdated = createWatchedEvent(() => this.updateTickerHandle())); }
        @AutoDisposeOnSet protected _onUpdated: IPrivateWatchedEvent<IDisplayObject>;

        public get onStageChanged() { return this._onStageChanged || (this._onStageChanged = createEvent<IStage>()); }
        @AutoDisposeOnSet protected _onStageChanged: IPrivateEvent<IStage>;

        public get onParentChanged() { return this._onParentChanged || (this._onParentChanged = createEvent<IDisplayObject>()); }
        @AutoDisposeOnSet protected _onParentChanged: IPrivateEvent<IDisplayObject>;

        // #endregion

        protected _isDisposed: boolean = false;
        public get isDisposed() { return this._isDisposed; }

        @RS.AutoDispose
        protected readonly _interactionBundle = new InteractionEventBundle();

        protected _nativeObject: T;
        @AutoDisposeOnSet
        protected _tickerHandle: IDisposable | null = null;
        protected _parent: IContainer | null = null;
        protected _stage: Rendering.IStage;
        protected _mask: Shape | Graphics | MovieClip | Bitmap | null;
        @AutoDispose
        protected _filters: IFilter[] = [];

        protected _blendMode: BlendMode;
        protected _userInteractiveSet: boolean = false;
        protected _localBounds: Rendering.Rectangle = new Rectangle(0, 0, 0, 0);
        protected _bounds: Rendering.Rectangle = new Rectangle(0, 0, 0, 0);
        protected readonly _skew: Point2D = new Point2D(0, 0);
        protected readonly _pivot: Point2D = new Point2D(0, 0);
        protected readonly _position: Point2D = new Point2D(0, 0);
        protected readonly _scale: Point2D = new Point2D(1, 1);

        @CheckDisposed
        public get name(): string { return this._nativeObject.name; }
        public set name(value: string) { this._nativeObject.name = value; }

        @CheckDisposed
        public get x(): number { return this._position.x; }
        public set x(value: number) { this._position.x = value; }

        @CheckDisposed
        public get y(): number { return this._position.y; }
        public set y(value: number) { this._position.y = value; }

        @CheckDisposed
        public get position() { return this._position; }

        @CheckDisposed
        public get scale() { return this._scale; }

        public get width(): number { return this.bounds.w; }
        public set width(value: number)
        {
            const w = this.localBounds.w;
            if (w <= 0) { return; }
            this.scaleX = value / w;
        }

        public get height(): number { return this.bounds.h; }
        public set height(value: number)
        {
            const h = this.localBounds.h;
            if (h <= 0) { return; }
            this.scaleY = value / h;
        }

        @CheckDisposed
        public get scaleX(): number { return this._scale.x; }
        public set scaleX(value: number) { this._scale.x = value; }

        @CheckDisposed
        public get scaleY(): number { return this._scale.y; }
        public set scaleY(value: number) { this._scale.y = value; }

        @CheckDisposed
        public get alpha(): number { return this._nativeObject.alpha; }
        public set alpha(value: number) { this._nativeObject.alpha = value; }

        @CheckDisposed
        public get rotation(): number { return this._nativeObject.rotation; }
        public set rotation(value: number) { this._nativeObject.rotation = value; }

        @CheckDisposed
        public get rotationDegrees(): number { return Math.radiansToDegrees(this._nativeObject.rotation); }
        public set rotationDegrees(value: number) { this._nativeObject.rotation = Math.degreesToRadians(value); }

        @CheckDisposed
        public get visible(): boolean { return this._nativeObject.visible; }
        public set visible(value: boolean)
        {
            this._nativeObject.visible = value;
            if (!value) { this.clearPointerInteraction(); }
        }

        @CheckDisposed
        public get interactive(): boolean { return this._nativeObject.interactive; }
        public set interactive(value: boolean)
        {
            if (value == null)
            {
                this._nativeObject.interactive = this._interactionBundle.hasInteractionEventHandlers.value;
                this._userInteractiveSet = false;
            }
            else
            {
                this._nativeObject.interactive = value;
                this._userInteractiveSet = true;
                if (!value) { this.clearPointerInteraction(); }
            }
        }

        @CheckDisposed
        public get buttonMode(): boolean { return this._nativeObject.buttonMode; }
        public set buttonMode(value: boolean) { this._nativeObject.buttonMode = value; }

        @CheckDisposed
        public get mask(): Shape | Graphics | MovieClip | Bitmap | null { return this._mask; }
        public set mask(value: Shape | Graphics | MovieClip | Bitmap | null)
        {
            this._mask = value;
            if (value == null)
            {
                this._nativeObject.mask = null;
            }
            else if (value instanceof Graphics || value instanceof Bitmap || value instanceof MovieClip)
            {
                this._nativeObject.mask = value.nativeObject;
            }
            else
            {
                let g: PIXI.Graphics;
                if (this._nativeObject.mask instanceof PIXI.Graphics)
                {
                    g = this._nativeObject.mask;
                }
                else
                {
                    g = new PIXI.Graphics();
                    this._nativeObject.mask = g;
                }
                g.clear();
                g.beginFill(0);
                if (value instanceof Rendering.Rectangle)
                {
                    g.drawRect(value.x, value.y, value.w, value.h);
                }
                else if (value instanceof Rendering.Circle)
                {
                    g.drawCircle(value.x, value.y, value.r);
                }
                else if (value instanceof Rendering.Ellipse)
                {
                    g.drawEllipse(value.x, value.y, value.w, value.h);
                }
                else if (value instanceof Rendering.RoundedRectangle)
                {
                    g.drawRoundedRect(value.x, value.y, value.w, value.h, value.r);
                }
                else if (value instanceof Rendering.Polygon)
                {
                    g.drawPolygon(value.points.map((point) => new PIXI.Point(point.x, point.y)));
                }
            }
        }

        @CheckDisposed
        public get cursor(): Cursor { return this._nativeObject.cursor as Cursor; }
        public set cursor(value: Cursor) { this._nativeObject.cursor = value; }

        @CheckDisposed
        public get hitArea(): Rendering.ReadonlyShape | null
        {
            const nativeHitArea = this._nativeObject.hitArea as PixiShape;
            this.__hitArea = nativeHitArea ? fromPixiShape(nativeHitArea, this.__hitArea) : null;
            return this.__hitArea;
        }
        public set hitArea(value: Rendering.ReadonlyShape | null)
        {
            if (value)
            {
                this.__hitArea = value.clone();
                this._nativeObject.hitArea = toPixiShape(this.__hitArea) as PixiShape;
            }
            else
            {
                this.__hitArea = null;
                this._nativeObject.hitArea = null;
            }
        }

        @CheckDisposed
        public get filterArea(): Rectangle | null
        {
            if (!this._nativeObject.filterArea) { return null; }
            return fromPixiRect(this._nativeObject.filterArea);
        }
        public set filterArea(value: Rectangle | null)
        {
            this._nativeObject.filterArea = value ? toPixiRect(value) : null;
        }

        public set parent(value: IContainer | null)
        {
            if (this._parent && value !== this._parent)
            {
                this.onRemovedInternal(this._parent as GenericContainer);
            }

            this._parent = value;

            if (value)
            {
                this.onAddedInternal(value as GenericContainer);
            }

            if (this._onParentChanged)
            {
                this._onParentChanged.publish(value);
            }
        }
        public get parent(): IContainer | null { return this._parent; }

        public get stage(): IStage { return this._stage; }
        public set stage(value: IStage)
        {
            this._stage = value;
            if (this._onStageChanged)
            {
                this._onStageChanged.publish(value);
            }
        }

        public get skew(): Point2D { return this._skew; }

        public get pivot(): Point2D { return this._pivot; }

        public get nativeObject(): T { return this._nativeObject; }

        public get rootParent(): IDisplayObject
        {
            const p = this.parent;
            const p2 = p ? p.rootParent : null;
            return p2 ? p2 : (p ? p : null);
        }

        @CheckDisposed
        public get filters(): IFilter[] { return this._filters; }
        public set filters(value: IFilter[])
        {
            this._filters = value;
            this._nativeObject.filters = value as any;
        }

        @CheckDisposed
        public get blendMode(): BlendMode { return this._blendMode; }
        public set blendMode(value: BlendMode)
        {
            if (this._nativeObject.hasOwnProperty("blendMode"))
            {
                this._blendMode = value;
                (this._nativeObject as any).blendMode = BlendConvert.blendToPixi(value);
            }
        }

        @CheckDisposed
        public get bounds(): ReadonlyRectangle
        {
            // Clear tmpRect as if nativeObject is a container and there are no children it will not be changed.
            tmpRect.x = tmpRect.y = tmpRect.width = tmpRect.height = 0;
            this._nativeObject.getBounds(false, tmpRect);
            this._bounds.set(tmpRect.x, tmpRect.y, tmpRect.width, tmpRect.height);
            return this._bounds;
        }

        @CheckDisposed
        public get localBounds(): ReadonlyRectangle
        {
            // Clear tmpRect as if nativeObject is a container and there are no children it will not be changed.
            tmpRect.x = tmpRect.y = tmpRect.width = tmpRect.height = 0;
            this._nativeObject.getLocalBounds(tmpRect);
            this._localBounds.set(tmpRect.x, tmpRect.y, tmpRect.width, tmpRect.height);
            if (this.parent)
            {
                // getLocalBounds wipes out child transforms, so we need to recalculate them.
                this._nativeObject.updateTransform();
            }
            return this._localBounds;
        }

        @CheckDisposed
        public get worldScaleX()
        {
            return this.parent ? this.scaleX * this.parent.worldScaleX : this.scaleX;
        }
        @CheckDisposed
        public get worldScaleY()
        {
            return this.parent ? this.scaleY * this.parent.worldScaleY : this.scaleY;
        }

        protected get alwaysTick() { return this._alwaysTick; }
        protected set alwaysTick(value)
        {
            this._alwaysTick = value;
            this.updateTickerHandle();
        }

        // Ticker stuff
        private _alwaysTick = false;
        private _shouldHaveTicker: boolean = false;

        // Interaction stuff
        private __hitArea: Shape;
        /** Point interaction state tracker object. Null if the pointer isn't interacting with this object. */
        private __pointerInteraction: PointerInteraction = null;
        /** Listener which waits for stage input block clear if input was blocked on prior pointer interaction publish attempt. */
        @AutoDisposeOnSet private __awaitStageUnblock: IDisposable;

        /** Prints the object name and class name to a string. */
        public toString()
        {
            const className = Object.getPrototypeOf(this).constructor.name;
            return this.name ? `[${className} ${this.name}]` : `[${className}]`;
        }

        @CheckDisposed
        public setTransform(x?: number, y?: number, scaleX?: number, scaleY?: number, rotation?: number, skewX?: number, skewY?: number, pivotX?: number, pivotY?: number): this
        {
            this._position.set(x, y);
            this._scale.set(scaleX, scaleY);
            this._skew.set(skewX, skewY);
            this._pivot.set(pivotX, pivotY);
            if (rotation != null) { this.rotation = rotation; }
            return this;
        }

        @CheckDisposed
        public toGlobal(localPt: Math.Vector2D, out?: Math.Vector2D, absolute: boolean = false): Math.Vector2D
        {
            if (!absolute && this.stage) { return this.stage.toLocal(localPt, this, out); }

            out = out || Math.Vector2D();
            tmpPoint1.set(localPt.x, localPt.y);
            this._nativeObject.toGlobal(tmpPoint1, tmpPoint2);
            Math.Vector2D.set(out, tmpPoint2.x, tmpPoint2.y);
            return out;
        }

        public toLocal(fromPt: Math.Vector2D, from: Rendering.IDisplayObject, out?: Math.Vector2D): Math.Vector2D;
        public toLocal(globalPt: Math.Vector2D, out?: Math.Vector2D, absolute?: boolean): Math.Vector2D;
        @CheckDisposed
        public toLocal(p1: Math.Vector2D, p2?: Math.Vector2D | Rendering.IDisplayObject, p3?: Math.Vector2D | boolean): Math.Vector2D
        {
            let out: Math.Vector2D;
            let from: IDisplayObject;
            if (p2 instanceof DisplayObject)
            {
                out = (p3 as Math.Vector2D) || Math.Vector2D();
                from = p2;
            }
            else
            {
                const absolute = Is.boolean(p3) ? p3 : false;
                out = p2 || Math.Vector2D();
                from = absolute ? null : this.stage;
            }

            tmpPoint1.set(p1.x, p1.y);
            this._nativeObject.toLocal(tmpPoint1, from && from.nativeObject, tmpPoint2);
            Math.Vector2D.set(out, tmpPoint2.x, tmpPoint2.y);
            return out;
        }

        public toLocalScale(object: DisplayObject<any>, out: RS.Math.Vector2D = RS.Math.Vector2D())
        {
            out.x = object.worldScaleX / this.worldScaleX;
            out.y = object.worldScaleY / this.worldScaleY;
            return out;
        }

        public onAdded(): void
        {
            this.updateTickerHandle();
        }

        public onRemoved(): void
        {
            this.updateTickerHandle();
            this.clearPointerInteraction();
        }

        public forEachParent(obj: DisplayObject<any>, call: (child: DisplayObjectContainerBase<any>) => void)
        {
            let parent = obj.parent;
            while (parent)
            {
                call(parent as any);
                parent = parent.parent;
            }
        }

        /* @internal */
        public onRemovedInternal(container: GenericContainer)
        {
            if ((this._onPreRender && this._onPreRender.listenerCount.value > 0) ||
                (this._onPostRender && this._onPostRender.listenerCount.value > 0))
            {
                container.unregisterPreRenderChild(this);
            }
        }

        /* @internal */
        public onAddedInternal(container: GenericContainer)
        {
            if ((this._onPreRender && this._onPreRender.listenerCount.value > 0) ||
                (this._onPostRender && this._onPostRender.listenerCount.value > 0))
            {
                container.registerPreRenderChild(this);
            }
        }

        @CheckDisposed
        public disablePrePostRendering(): void
        {
            if (this.parent)
            {
                (this.parent as GenericContainer).unregisterPreRenderChild(this);
            }
        }

        @CheckDisposed
        public enablePrePostRendering(): void
        {
            if (this.parent)
            {
                (this.parent as GenericContainer).registerPreRenderChild(this);
            }
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            // Dispose before calling removeChild
            this._onStageChanged = null;
            this._onParentChanged = null;
            if (this._parent) { this._parent.removeChild(this); }
            this._nativeObject.destroy();
            this._nativeObject = null;
            this._mask = null;
            this._isDisposed = true;

        }

        @CheckDisposed
        public renderToTexture(p1?: IRenderTexture | IStage, p2?: boolean | number, stage?: IStage): ISubTexture
        {
            /** Whether or not the render texture only exists for this object. */
            let texture: RenderTexture;
            let allowResize: boolean;
            let resolution: number;
            if (p1 instanceof RenderTexture)
            {
                texture = p1;
                allowResize = (p2 as boolean) || false;
                resolution = texture.resolution;
            }
            else
            {
                stage = p1 as IStage;
                resolution = p2 == null ? 1 : p2 as number;
            }

            // Get renderer.
            stage = stage || this._stage;
            if (!stage) { throw new Error("[DisplayObject] renderToTexture was called, but no stage was found. Add the object to a stage or pass the stage as the second argument."); }
            const renderer = (stage as Stage).renderer;

            // Work out our position and size in the texture.
            const pos = this._position;
            const myBounds = this.localBounds;
            const width = myBounds.w, height = myBounds.h;
            const offsX = -myBounds.x + pos.x, offsY = -myBounds.y + pos.y;

            // Get render texture of the correct size.
            let acquiredFromPool: boolean;
            if (texture)
            {
                const minW = pos.x + width, minH = pos.y + height;
                if (texture.width < minW || texture.height < minH)
                {
                    if (allowResize)
                    {
                        const newW = Math.nextPowerOf2(minW), newH = Math.nextPowerOf2(minH);
                        texture.resize(newW, newH, resolution);
                    }
                    else
                    {
                        throw new Error(
                        [
                            `[DisplayObject] renderToTexture was called, but there was not enough space on the texture.`,
                            `Texture size: ${texture.width} x ${texture.height}; required size: ${pos.x + width} x ${pos.y + height}.`,
                            `Consider setting allowResize to true. Check object position; objects are positioned by their top-left corner.`
                        ].join(""));
                    }
                }
                acquiredFromPool = false;
            }
            else
            {
                const widthPOT = Math.nextPowerOf2(width), heightPOT = Math.nextPowerOf2(height);
                texture = IRenderTexturePool.get().acquire(widthPOT, heightPOT, resolution, true) as RenderTexture;
                texture.clearInternal(renderer);
                acquiredFromPool = true;
            }

            // Do render.
            this.nativeObject.position.set(offsX, offsY);
            texture.renderInternal(renderer, this.nativeObject);
            this.nativeObject.position.copy(pos);

            const textureBounds = new Rectangle(pos.x, pos.y, width, height);
            return new SubTexture(texture, textureBounds, acquiredFromPool);
        }

        /**
         * Clears the current pointer interaction.
         * Publishes onClickCancelled if called during a valid click.
         * TODO: when we track multiple pointers, just remove all interactions, and probably pluralise the name.
         */
        public clearPointerInteraction(): void
        {
            const interaction = this.__pointerInteraction;
            if (!interaction) { return; }
            if (interaction.clickValid && (interaction.leftDown || interaction.rightDown))
            {
                this.onClickCancelled.publish(
                {
                    target: this,
                    globalPosition: this.toGlobal(interaction.position, null, true),
                    localPosition: interaction.position,
                    pointerID: null
                });
            }
            this.__pointerInteraction = null;
        }

        protected initialise(classType: { new(...args): T; }, ...args): void
        {
            this._nativeObject = this.createClass(classType, args);
            this._nativeObject["_wrapper"] = this;
            this._nativeObject.interactive = false;
            this.cursor = Cursor.Default;
            this._pivot.onChanged(this.handlePivotChanged);
            this._skew.onChanged(this.handleSkewChanged);
            this._position.onChanged(this.handlePositionChanged);
            this._scale.onChanged(this.handleScaleChanged);
            // this.nativeObject.interactiveChildren = true;
            this.createListeners();
            // this.nativeObject.globalPosition;
        }

        protected createClass(classType: { new(...args: any[]): T; }, args: any[]): T
        {
            return new classType(...args);
        }

        protected destroyNativeObject()
        {
            this._nativeObject.destroy();
        }

        /** Evaluate whether or not we need to hook pre/post render events. */
        protected evaluateNeedsPrePostRender(): void
        {
            if ((!this._onPreRender || this._onPreRender.listenerCount.value === 0) &&
                (!this._onPostRender || this._onPostRender.listenerCount.value === 0))
            {
                this.disablePrePostRendering();
            }
            else
            {
                this.enablePrePostRendering();
            }
        }

        protected createListeners()
        {
            this._interactionBundle.hasInteractionEventHandlers.onChanged((doesHave) =>
            {
                const updateListeners = doesHave ? this._nativeObject.on : this._nativeObject.off;
                // Right mouse button events.
                updateListeners.call(this._nativeObject, "rightdown", this.onRightDownHandler, this);
                updateListeners.call(this._nativeObject, "rightup", this.onRightUpHandler, this);
                updateListeners.call(this._nativeObject, "rightupoutside", this.onRightUpOutsideHandler, this);

                // Left mouse button events.
                updateListeners.call(this._nativeObject, "pointerdown", this.onPointerDownHandler, this);
                updateListeners.call(this._nativeObject, "pointerup", this.onPointerUpHandler, this);
                updateListeners.call(this._nativeObject, "pointerupoutside", this.onPointerUpOutsideHandler, this);
                updateListeners.call(this._nativeObject, "pointercancel", this.onPointerCancelHandler, this);

                // Pointer position events.
                updateListeners.call(this._nativeObject, "pointermove", this.onPointerMoveHandler, this);
                updateListeners.call(this._nativeObject, "pointerover", this.onPointerOverHandler, this);
                updateListeners.call(this._nativeObject, "pointerout", this.onPointerOutHandler, this);

                if (this._userInteractiveSet) { return; }
                this.nativeObject.interactive = doesHave;
            });
        }

        // Interaction events: we don't blindly trust events from below, instead we use them to inform our interaction state.
        // Right mouse button events.
        protected onRightDownHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, null, null, true, null); }
        protected onRightUpHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, null, null, false, null); }
        protected onRightUpOutsideHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, false, null, false, null); }

        // Left mouse button events.
        protected onPointerDownHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, true, true, null, null); }
        protected onPointerUpHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, null, false, null, null); }
        protected onPointerUpOutsideHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, false, false, null, null); }

        // Pointer position events.
        protected onPointerCancelHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, null, null, null, false); }
        protected onPointerOverHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, true, null, null, null); }
        protected onPointerOutHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, false, null, null, null); }
        protected onPointerMoveHandler(e: PIXI.interaction.InteractionEvent) { this.updatePointerInteraction(e, null, null, null, null); }

        protected getInteractionEventData(eventData: PIXI.interaction.InteractionEvent): InteractionEventData
        {
            return {
                target: this,
                globalPosition: { x: eventData.data.global.x, y: eventData.data.global.y },
                localPosition: eventData.data.getLocalPosition(this._nativeObject),
                pointerID: eventData == null ? null : eventData.data.pointerID
            };
        }

        @Callback
        @CheckDisposed
        protected handlePivotChanged(): void { this._nativeObject.pivot.set(this._pivot.x, this._pivot.y); }
        @Callback
        @CheckDisposed
        protected handleSkewChanged(): void { this._nativeObject.skew.set(this._skew.x, this._skew.y); }
        @Callback
        @CheckDisposed
        protected handlePositionChanged(): void { this._nativeObject.position.set(this._position.x, this._position.y); }
        @Callback
        @CheckDisposed
        protected handleScaleChanged(): void { this._nativeObject.scale.set(this._scale.x, this._scale.y); }

        /** Returns whether or not this display object is interactive, based on its interactive state and the interactiveChildren state of its parents. */
        protected checkInteractive()
        {
            if (!this.interactive) { return false; }
            if (this.parent instanceof DisplayObjectContainerBase) { return this.parent.checkChildrenInteractive(); }
            return true;
        }

        /**
         * Dispatches DisplayEvent.ENTER_FRAME each frame.
         */
        protected onUpdate(delta: number): void
        {
            this.onUpdated.publish(this);
        }

        /**
         * Updates the pointer interaction state with new information.
         * @param pixiEventData original pixi event data
         * @param over current mouse over state; null if not known
         * @param over current left mouse down state; null if not known
         */
        private updatePointerInteraction(pixiEventData: PIXI.interaction.InteractionEvent, over: boolean | null, leftDown: boolean | null, rightDown: boolean | null, clickValid: boolean | null): void
        {
            // TODO: right click context menu = get stuck in bad interaction state.

            // If we enter this function and we aren't interactive (multi-touch issues), clear any interaction and do nothing else.
            if (!this.checkInteractive()) { return; }
            const eventData = this.getInteractionEventData(pixiEventData);

            // Do we need to start a new interaction?
            // TODO: when we track multiple pointers, replace with lookup for interaction with matching pointer ID
            if (!this.__pointerInteraction) { this.__pointerInteraction = { position: null, over: false, leftDown: false, rightDown: false, clickValid: true }; }
            const interaction = this.__pointerInteraction;

            // Continue from prior pointer interaction state.
            if (over == null) { over = interaction.over; }
            if (leftDown == null) { leftDown = interaction.leftDown; }
            if (rightDown == null) { rightDown = interaction.rightDown; }
            if (clickValid == null) { clickValid = interaction.clickValid && over; }

            // Reset click state if we managed to capture a right-down, because we don't preventDefault
            if (rightDown) { leftDown = rightDown = clickValid = false; }

            // Check for pointer out, because this is otherwise broken on mobile.
            if (over && !this.pointHits(eventData.localPosition)) { over = false; }

            // What's changed in this update?
            const overChanged = over !== interaction.over;
            const leftDownChanged = leftDown !== interaction.leftDown;
            const rightDownChanged = rightDown !== interaction.rightDown;
            const positionChanged = !interaction.position || !RS.Math.Vector2D.equals(interaction.position, eventData.localPosition);
            const clickValidChanged = clickValid !== interaction.clickValid;

            // If nothing has changed except the position, skip this.
            const changed = overChanged || leftDownChanged || rightDownChanged;
            const interacting = over || leftDown || rightDown;
            if (!changed && !interacting) { return; }

            // Update pointer interaction data.
            interaction.position = eventData.localPosition;
            interaction.over = over;
            interaction.leftDown = leftDown;
            interaction.rightDown = rightDown;
            interaction.clickValid = clickValid;

            // Pick & dispatch events.
            const stage = this.stage || (this instanceof Stage ? this : null);
            if (stage.inputBlocked.value)
            {
                this.__awaitStageUnblock = stage.inputBlocked.onChanged.once((value) => this.dispatchPointerInteractionEvents(eventData, interaction, positionChanged, overChanged, leftDownChanged, rightDownChanged, clickValidChanged));
                return;
            }
            this.dispatchPointerInteractionEvents(eventData, interaction, positionChanged, overChanged, leftDownChanged, rightDownChanged, clickValidChanged);
        }

        /** Uses the current pointer interaction state and reported changes to decide which events to publish now, then publishes them. */
        private dispatchPointerInteractionEvents(eventData: InteractionEventData, interaction: PointerInteraction, positionChanged: boolean, overChanged: boolean, leftDownChanged: boolean, rightDownChanged: boolean, clickValidChanged: boolean): void
        {
            const over = interaction.over,
                  leftDown = interaction.leftDown,
                  rightDown = interaction.rightDown,
                  clickValid = interaction.clickValid;

            const shouldDispatchHoverStateEvents = !Device.isTouchDevice;

            const logger = Logging.ILogger.get().getContext("Interaction");
            logger.log(`${this}: Dispatch pointer interaction event:
                overChanged? ${overChanged} (${over})
                leftDownChanged? ${leftDownChanged} (${leftDown})
                rightDownChanged? ${rightDownChanged} (${rightDown})
                clickValidChanged? ${clickValidChanged} (${clickValid})
                positionChanged? ${positionChanged} (${eventData.localPosition.x}, ${eventData.localPosition.y})`, RS.Logging.LogLevel.Debug);

            if (positionChanged)
            {
                // Process position.
                this.onMoved.publish(eventData);
            }
            if (overChanged && shouldDispatchHoverStateEvents)
            {
                // Process hover state.
                if (over)
                {
                    this.onOver.publish(eventData);
                }
                else
                {
                    this.onOut.publish(eventData);
                }
            }
            if (leftDownChanged)
            {
                // Process left button state.
                if (leftDown)
                {
                    this.onDown.publish(eventData);
                }
                else
                {
                    this.onUp.publish(eventData);
                    if (!over) { this.onUpOutside.publish(eventData); }
                    if (clickValid) { this.onClicked.publish(eventData); }
                }
            }
            if (rightDownChanged)
            {
                // Process right button state.
                if (rightDown)
                {
                    this.onRightDown.publish(eventData);
                }
                else
                {
                    this.onRightUp.publish(eventData);
                    if (!over) { this.onRightUpOutside.publish(eventData); }
                    if (clickValid) { this.onRightClicked.publish(eventData); }
                }
            }
            // A click is cancelled if:
            // - the user initiated a click (left/right down)
            // - left the hit area without completing the click
            if ((leftDown || rightDown) && clickValidChanged && !clickValid)
            {
                this.onClickCancelled.publish(eventData);
            }
            // Reset pointer interaction, if it wasn't cleared by something in an event handler.
            // TODO: when we track multiple pointers, remove third condition
            if (!leftDown && !rightDown && this.__pointerInteraction)
            {
                if (over)
                {
                    interaction.clickValid = true;
                }
                else
                {
                    // TODO: when we track multiple pointers, remove from pointers array
                    this.__pointerInteraction = null;
                }
            }
        }

        /** Returns whether or not this display object contains the given point in local space. */
        private pointHits(localPos: Math.Vector2D): boolean
        {
            const globalPos = this.toGlobal(localPos, pointHitsVec, true);
            if (this.__hitArea) { return this.hitArea.contains(localPos); }
            if (this._mask)
            {
                if (this._mask instanceof DisplayObject)
                {
                    return (this._mask as DisplayObject<any>).pointHits(localPos);
                }
                else
                {
                    return this._mask.contains(localPos);
                }
            }
            if ((this.nativeObject as any).containsPoint) { return (this.nativeObject as any).containsPoint(globalPos); }
            return this.localBounds.contains(localPos);
        }

        private updateTickerHandle(): void
        {
            if (this.isDisposed)
            {
                this._tickerHandle = null;
                return;
            }

            const shouldHaveTicker = (this.onUpdated.listenerCount.value > 0 || this._alwaysTick) && this._parent != null;
            if (this._shouldHaveTicker === shouldHaveTicker) { return; }
            this._shouldHaveTicker = shouldHaveTicker;
            if (shouldHaveTicker)
            {
                if (!this._tickerHandle || this._tickerHandle.isDisposed)
                {
                    this._tickerHandle = ITicker.get().add((ev) => this.onUpdate(ev.delta));
                }
            }
            else
            {
                this._tickerHandle = null;
            }
        }
    }
    RS.Rendering.DisplayObject = DisplayObject;
}
