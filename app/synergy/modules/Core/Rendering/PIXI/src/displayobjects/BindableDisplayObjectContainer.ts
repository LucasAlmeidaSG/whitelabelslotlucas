
/// <reference path="DisplayObjectContainer.ts" />
namespace RS.Rendering.Pixi
{
    @HasCallbacks
    export class BindableDisplayObjectContainer<TSettings = void, TRuntimeData = void> extends DisplayObjectContainer
    {
        public constructor(protected readonly settings: TSettings, protected readonly runtimeData: TRuntimeData)
        {
            super();
        }
    }

    Rendering.BindableDisplayObjectContainer = BindableDisplayObjectContainer;
}