/// <reference path="DisplayObject.ts" />
/// <reference path="DisplayObjectContainerBase.ts" />

/**
 * @internal
 */
namespace RS.Rendering.Pixi
{
    @HasCallbacks
    export class FastContainer<T extends IBitmap | IAnimatedSprite | IFastBitmap = IFastBitmap> extends DisplayObjectContainerBase<PIXI.particles.ParticleContainer, T> implements IFastContainer<T>
    {
        public get blendMode() { return this._blendMode; }
        public set blendMode(value)
        {
            this._blendMode = value;
            this._nativeObject.blendMode = BlendConvert.blendToPixi(value);
        }

        constructor(p1?: Partial<IFastContainer.Settings> | string, p2?: string)
        {
            super();
            const settings: IFastContainer.Settings = { ...IFastContainer.defaultSettings, ...(Is.nonPrimitive(p1) ? p1 : null) };
            const name = Is.string(p1) ? p1 : p2;

            const props: PIXI.particles.ParticleContainerProperties =
            {
                scale: true,
                position: true,
                rotation: settings.supportRotation,
                uvs: settings.supportUVs,
                alpha: settings.supportColor
            };
            const batchSize = settings.batchSize == null ? IFastContainer.defaultSettings.batchSize : settings.batchSize;
            this.initialise(PIXI.particles.ParticleContainer, batchSize, props, batchSize);
            if (name) { this.name = name; }
        }
    }

    Rendering.FastContainer = FastContainer;
}