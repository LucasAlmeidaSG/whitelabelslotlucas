/// <reference path="Bitmap.ts" />
/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * A sprite implementation for PIXI that uses EaselJS spritesheets.
     *
     * Ported from EaselJS to PIXI - T Smith, June 2016
     */
    export class AnimatedSprite extends DisplayObjectContainerBase<PIXI.Sprite> implements IAnimatedSprite
    {
        @AutoDispose public onAnimationEnded: ISimplePrivateEvent = createSimpleEvent();
        @AutoDispose public onChanged: ISimplePrivateEvent = createSimpleEvent();
        @AutoDispose public onCustomEvent: IPrivateEvent<string> = createEvent<string>();

        protected _shader: IShader = null;
        protected _tint: Util.Color = null;

        private _currentFrame: number = 0; // the decimal one
        private _currentFrameIndex: number = 0; // the integer one
        private _currentAnimation: string = null;
        private _paused: boolean = true;
        private _spriteSheet: ISpriteSheet;
        private _currentAnimationFrame: number = 0;
        private _framerate: number = 0;
        private _animation: RS.Rendering.IAnimation = null;
        private _skipAdvance: boolean = false;
        private _loop: boolean | null = null;
        private _yoyo: boolean | null = null;

        private _lastFrame: number = 0;

        private _reg: PIXI.Point = new PIXI.Point(0, 0);

        /** Is the current animation looping or not */
        public get looping(): boolean { return this._loop == true; }
        public get yoyo(): boolean { return this._yoyo == true; }

        @CheckDisposed
        public get anchorX(): number { return this._nativeObject.anchor.x; }
        public set anchorX(value: number) { this._nativeObject.anchor.x = value; }

        @CheckDisposed
        public get anchorY(): number { return this._nativeObject.anchor.y; }
        public set anchorY(value: number) { this._nativeObject.anchor.y = value; }
        /**
         * Gets the reg (registration) point of this RSAnimatedSprite.
         * Functionally equivalent to the "anchor" property of PIXI.Sprite (do NOT use RSAnimatedSprite.anchor!)
         */
        public get reg() { return this._reg; }

        /**
		 * The frame index that will be drawn when draw is called. Note that with some {{#crossLink "SpriteSheet"}}{{/crossLink}}
		 * definitions, this will advance non-sequentially. This will always be an integer value.
		 */
        public get currentFrame() { return this._currentFrameIndex; }

        /**
		 * Returns the name of the currently playing animation.
		 */
        public get currentAnimation() { return this._currentAnimation; }

        /**
		 * Prevents the animation from advancing each tick automatically. For example, you could create a sprite
		 * sheet of icons, set paused to true, and display the appropriate icon by setting <code>currentFrame</code>.
		 */
        @CheckDisposed
        public get paused() { return this._paused; }
        public set paused(value)
        {
            if (this._paused === value) { return; }
            this._paused = value;
            if (!value)
            {
                Ticker.registerTickers(this);
            }
            else
            {
                Ticker.unregisterTickers(this);
            }
        }

        /** Gets or sets the shader to be used for this sprite. */
        public get shader(): IShader | null { return this._shader; }
        public set shader(value: IShader | null)
        {
            this._shader = value;
        }

        @CheckDisposed
        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        /**
		 * The SpriteSheet instance to play back. This includes the source image, frame dimensions, and frame
		 * data. See {{#crossLink "SpriteSheet"}}{{/crossLink}} for more information.
		 */
        public get spriteSheet() { return this._spriteSheet; }
        public set spriteSheet(value) { this._spriteSheet = value; }

        /**
		 * Specifies the current frame index within the currently playing animation. When playing normally, this will increase
		 * from 0 to n-1, where n is the number of frames in the current animation.
		 *
		 * This could be a non-integer value if
		 * using time-based playback (see {{#crossLink "Sprite/framerate"}}{{/crossLink}}, or if the animation's speed is
		 * not an integer.
		 */
        public get currentAnimationFrame() { return this._currentAnimationFrame; }
        public set currentAnimationFrame(value)
        {
            if (this._currentAnimationFrame === value) { return; }
            this._currentAnimationFrame = value;
            if (this._currentAnimation != null)
            {
                this._goto(this._currentAnimation, value);
            }
            else
            {
                this._goto(value);
            }
        }

        /**
		 * By default Sprite instances advance one frame per tick. Specifying a framerate for the Sprite (or its related
		 * SpriteSheet) will cause it to advance based on elapsed time between ticks as appropriate to maintain the target
		 * framerate.
		 *
		 * For example, if a Sprite with a framerate of 10 is placed on a Stage being updated at 40fps, then the Sprite will
		 * advance roughly one frame every 4 ticks. This will not be exact, because the time between each tick will
		 * vary slightly between frames.
		 *
		 * This feature is dependent on the tick event object (or an object with an appropriate "delta" property) being
		 * passed into {{#crossLink "Stage/update"}}{{/crossLink}}.
         */
        public get framerate() { return this._framerate; }
        public set framerate(value) { this._framerate = value; }

        public get isDisposed() { return this._isDisposed; }

        /**
         * Displays a frame or sequence of frames (ie. an animation) from a SpriteSheet instance. A sprite sheet is a series of
         * images (usually animation frames) combined into a single image. For example, an animation consisting of 8 100x100
         * images could be combined into a 400x200 sprite sheet (4 frames across by 2 high). You can display individual frames,
         * play frames as an animation, and even sequence animations together.
         *
         * Until {{#crossLink "Sprite/gotoAndStop"}}{{/crossLink}} or {{#crossLink "Sprite/gotoAndPlay"}}{{/crossLink}} is called,
         * only the first defined frame defined in the sprite sheet will be displayed.
         * @param spriteSheet The SpriteSheet instance to play back. This includes the source image(s), frame dimensions, and frame data. See {{#crossLink "SpriteSheet"}}{{/crossLink}} for more information.
         * @param frameOrAnimation The frame number or animation to play initially.
         */
        constructor(spriteSheet: RS.Rendering.ISpriteSheet);

        constructor(spriteSheet: RS.Rendering.ISpriteSheet, animation: string);

        constructor(spriteSheet: RS.Rendering.ISpriteSheet, frame: number);

        constructor(spriteSheet: RS.Rendering.ISpriteSheet, frameOrAnimation?: string | number)
        {
            super();
            this.initialise(PIXI.Sprite, spriteSheet, frameOrAnimation);
        }

        /**
         * Play (unpause) the current animation. The Sprite will be paused if either {{#crossLink "Sprite/stop"}}{{/crossLink}}
         * or {{#crossLink "Sprite/gotoAndStop"}}{{/crossLink}} is called. Single frame animations will remain
         * unchanged.
         */
        @CheckDisposed
        public play(): void
        {
            this.paused = false;
            this._lastFrame = Timer.now;
        }

        /**
         * Stop playing a running animation. The Sprite will be playing if {{#crossLink "Sprite/gotoAndPlay"}}{{/crossLink}}
         * is called. Note that calling {{#crossLink "Sprite/gotoAndPlay"}}{{/crossLink}} or {{#crossLink "Sprite/play"}}{{/crossLink}}
         * will resume playback.
         */
        @CheckDisposed
        public stop(): void
        {
            this.paused = true;
        }

        /**
         * Sets paused to false and plays the specified animation name or named frame.
         * @param animation     The animation name that the playhead should move to and begin playing.
         * @param settings
         */
        public gotoAndPlay(animation: string, settings?: RSAnimatedSprite.PlaySettings): PromiseLike<void>;

        /**
         * Sets paused to false and plays the specified frame number.
         * @param frame         The frame number the playhead should move to and begin playing.
         * @param settings
         */
        public gotoAndPlay(frame: number, settings?: RSAnimatedSprite.PlaySettings): PromiseLike<void>;

        @CheckDisposed
        public gotoAndPlay(frameOrAnimation: string | number, settings?: RSAnimatedSprite.PlaySettings): PromiseLike<void>
        {
            return new Promise<void>((resolve, reject) =>
            {
                this.paused = false;
                this._lastFrame = Date.now();
                this._skipAdvance = true;
                let animationFrameIndex = 0;

                if (settings != null)
                {
                    if (settings.fps != null || settings.speed != null)
                    {
                        this.framerate = (settings.fps || this._spriteSheet.framerate) * (settings.speed || 1.0);
                    }
                    if (settings.loop != null)
                    {
                        this._loop = settings.loop;
                    }
                    else
                    {
                        this._loop = null;
                    }
                    if (settings.yoyo != null)
                    {
                        this._yoyo = settings.yoyo;
                    }
                    else
                    {
                        this._yoyo = null;
                    }

                    // if we specified an offset, use the start as an offset
                    if (settings.startFrameOffset != null)
                    {
                        animationFrameIndex = settings.startFrameOffset;
                    }
                }
                else
                {
                    this._loop = null;
                    this._yoyo = null;
                }
                if (Is.number(frameOrAnimation))
                {
                    // add the offset if we provided one, else just use the frame number
                    animationFrameIndex += frameOrAnimation;
                    this._goto(animationFrameIndex, settings && settings.reverse);
                }
                else
                {
                    this._goto(frameOrAnimation, animationFrameIndex, settings && settings.reverse);
                }
                this.onAnimationEnded.once(resolve);
            });
        }

        /**
         * Sets paused to true and seeks to the specified animation name or named frame.
         * @param animation      The animation name that the playhead should move to and stop.
         */
        public gotoAndStop(animation: string): void;

        /**
         * Sets paused to true and seeks to the specified frame number.
         * @param frame      The frame number the playhead should move to and stop.
         */
        public gotoAndStop(frame: number): void;

        @CheckDisposed
        public gotoAndStop(frameOrAnimation: string | number): void
        {
            this.paused = true;
            if (Is.number(frameOrAnimation))
            {
                this._goto(frameOrAnimation);
            }
            else
            {
                this._goto(frameOrAnimation);
            }
        }

        /**
         * Advances the playhead. This occurs automatically each tick by default.
         * @param time      The amount of time in ms to advance by. Only applicable if framerate is set on the Sprite or its SpriteSheet.
         */
        @CheckDisposed
        public advance(time: number): void
        {
            const fps = this._framerate || this._spriteSheet.framerate;
            const t = (fps && time != null) ? time / (1000 / fps) : 1;
            this._normalizeFrame(t);
        }

        /**
         * Returns a string representation of this object.
         * @returns a string representation of the instance.
         */
        public toString()
        {
            return `[RSAnimatedSprite]`;
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this.paused = true;
            super.dispose();
            this._isDisposed = true;
        }

        protected createClass(classType: { new(...args: any[]): PIXI.Sprite; }, args: any[]): PIXI.Sprite
        {
            const spriteSheet: RS.Rendering.ISpriteSheet = args[0];
            const frameOrAnimation: string | number = args[1];
            const frameTexture: PIXI.Texture = (spriteSheet.getFrame(0).imageData as SubTexture).nativeObject;
            this._spriteSheet = spriteSheet;
            this._framerate = spriteSheet.framerate;
            let display: PIXI.Sprite;
            if (frameTexture)
            {
                if (frameTexture.baseTexture)
                {
                    display = new Sprite(frameTexture);
                }
                display = Sprite.fromImage("");
            }
            this._nativeObject = display;
            if (frameOrAnimation != null)
            {
                if (Is.number(frameOrAnimation))
                {
                    this.gotoAndPlay(frameOrAnimation);
                }
                else
                {
                    this.gotoAndPlay(frameOrAnimation);
                }
            }
            else
            {
                this.gotoAndStop(0);
            }
            return display;
        }

        /**
         * Handles a tick event.
         */
        @Tick({})
        protected tick(ev: Ticker.Event)
        {
            if (!this._paused)
            {
                if (!this._skipAdvance) { this.advance(ev.delta); }
                this._skipAdvance = false;
            }
        }

        /**
         * Normalizes the current frame, advancing animations and dispatching callbacks as appropriate.
         */
        protected _normalizeFrame(frameDelta: number = 0): void
        {
            frameDelta = frameDelta || 0;
            const animation = this._animation;
            const paused = this.paused;
            let frame = this._currentFrame;
            let l: number;

            if (animation)
            {
                const speed = animation.speed || 1;
                let animFrame = this._currentAnimationFrame;
                l = animation.frames.length;
                if (animFrame + frameDelta * speed >= l)
                {
                    const next = this._loop == null ? animation.next : (this._loop === true ? animation.name : null);
                    if (this._dispatchAnimationEnd(animation, frame, paused, next, l - 1))
                    {
                        // something changed in the event stack, so we shouldn't make any more changes here.
                        return;
                    }
                    else if (next)
                    {
                        // sequence. Automatically calls _normalizeFrame again with the remaining frames.
                        return this._goto(next, frameDelta - (l - animFrame) / speed);
                    }
                    else
                    {
                        // end.
                        this.paused = true;
                        animFrame = animation.frames.length - 1;
                    }
                }
                else
                {
                    animFrame += frameDelta * speed;
                }
                this._currentAnimationFrame = animFrame;
                this._currentFrame = animation.frames[animFrame | 0];
            }
            else
            {
                frame = (this._currentFrame += frameDelta);
                l = this._spriteSheet.getNumFrames();
                if (frame >= l && l > 0)
                {
                    // ended but has not changed
                    if (this._dispatchAnimationEnd(animation, frame, paused, null, l - 1))
                    {
                        // something changed in the event stack, so we shouldn't make any more changes here.
                        return;
                    }
                    else if (this._loop != false)
                    {
                        // looped.
                        this._currentFrame -= l;
                        if (this._currentFrame >= l)
                        {
                            this._normalizeFrame();
                            return;
                        }

                        // hit max frames and no change
                        return;
                    }
                    else
                    {
                        //end.
                        this.paused = true;
                        this._currentFrame = this._spriteSheet.getNumFrames() - 1;
                    }
                }
            }
            frame = this._currentFrame | 0;
            if (this._currentFrameIndex !== frame)
            {
                this._currentFrameIndex = frame;
                this.onChanged.publish();
                const currentFrame = this._spriteSheet.getFrame(frame);
                if (currentFrame.event)
                {
                    this.onCustomEvent.publish(currentFrame.event);
                }
            }
            this.updateVertices();
        }

        /**
         * Moves the playhead to the specified frame number or animation.
         * @param animation     The animation that the playhead should move to.
         * @param frame         The frame of the animation to go to. Defaults to 0.
         * @protected
         */
        protected _goto(animation: string, frame?: number, reverse?: boolean): void;

        /**
         * Moves the playhead to the specified frame number or animation.
         * @param frame The frame of the animation to go to.
         */
        protected _goto(frame: number, reverse?: boolean): void;

        protected _goto(frameOrAnimation: string | number, frame: number | boolean, reverse?: boolean)
        {
            this._currentAnimationFrame = 0;
            if (!Is.number(frameOrAnimation))
            {
                let actualFrame: number;
                if (Is.number(frame))
                {
                    actualFrame = frame;
                }
                else
                {
                    actualFrame = undefined;
                    reverse = frame;
                }
                const data = this._spriteSheet.getAnimation(frameOrAnimation, reverse);
                if (data)
                {
                    const dataClone: Rendering.IAnimation = { ...data, frames: data.frames.slice() };
                    if (this._yoyo)
                    {
                        const reverseData = this._spriteSheet.getAnimation(frameOrAnimation, !reverse);
                        const reverseFrames = reverseData.frames.slice();
                        reverseFrames.splice(0, 1);
                        reverseFrames.splice(reverseFrames.length - 1, 1);
                        dataClone.frames = dataClone.frames.concat(reverseFrames);
                    }
                    this._animation = dataClone;
                    this._currentAnimation = frameOrAnimation;
                    this._normalizeFrame(actualFrame);
                }
            }
            else
            {
                this._currentAnimation = this._animation = null;
                this._currentFrame = frameOrAnimation;
                this._normalizeFrame();
            }
        }

        private updateVertices(): void
        {
            const o = this.spriteSheet.getFrame(this._currentFrame | 0);
            if (!o) { return; }
            this._nativeObject.texture = (o.imageData as SubTexture).nativeObject;
            const baseTex = this._nativeObject.texture.baseTexture;
            if (baseTex)
            {
                if (baseTex.premultipliedAlpha)
                {
                    this.blendMode = BlendConvert.withoutNPM(this.blendMode || BlendMode.Normal);
                }
                else
                {
                    this.blendMode = BlendConvert.withNPM(this.blendMode || BlendMode.Normal);
                }
            }

            this._nativeObject.anchor.x = (o.reg.x + this._reg.x * o.untrimmedSize.w) / o.imageData.width;
            this._nativeObject.anchor.y = (o.reg.y + this._reg.y * o.untrimmedSize.h) / o.imageData.height;
        }

        /**
         * Dispatches the "animationend" event. Returns true if a handler changed the animation (ex. calling {{#crossLink "Sprite/stop"}}{{/crossLink}},
         * {{#crossLink "Sprite/gotoAndPlay"}}{{/crossLink}}, etc.)
         * @property _dispatchAnimationEnd
         */
        private _dispatchAnimationEnd(animation: RS.Rendering.IAnimation, frame: number, paused: boolean, next: string, end?: number): boolean
        {
            this.onAnimationEnded.publish();

            // did the animation get changed in the event stack?:
            let changed = (this._animation !== animation || this._currentFrame !== frame);
            // if the animation hasn't changed, but the sprite was paused, then we want to stick to the last frame:
            if (!changed && !paused && this._paused) { this._currentAnimationFrame = end; changed = true; }
            return changed;
        }
    }

    RS.Rendering.AnimatedSprite = AnimatedSprite;

    export namespace RSAnimatedSprite
    {
        export interface PlaySettings
        {
            reverse?: boolean;
            loop?: boolean;
            yoyo?: boolean;
            fps?: number;
            speed?: number;
            startFrameOffset?: number;
        }
    }
}
