/// <reference path="DisplayObject.ts" />
/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The Graphics class implements the IGraphics<T> interface.
     *
     * A Graphics instance can dynamically draw multiple graphics and be added to the display list.
     */
    export class Graphics extends DisplayObject<RSGraphics> implements IGraphics
    {
        @CheckDisposed
        public get shader() { return this._nativeObject.shader; }
        public set shader(value) { this._nativeObject.shader = value; }

        @CheckDisposed
        public get tint(): Util.Color
        {
            const col = new Util.Color();
            col.tint = this._nativeObject.tint;
            return col;
        }
        public set tint(value: Util.Color)
        {
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        @CheckDisposed
        public get blendMode(): BlendMode { return BlendConvert.pixiToBlend(this._nativeObject.blendMode); }
        public set blendMode(value: BlendMode) { this._nativeObject.blendMode = BlendConvert.blendToPixi(value); }

        @CheckDisposed
        public get fillAlpha(): number { return this._nativeObject.fillAlpha; }
        public set fillAlpha(value: number) { this._nativeObject.fillAlpha = value; }

        @CheckDisposed
        public get scaleX(): number { return this._nativeObject ? this.scale.x : 1; }
        public set scaleX(value: number) { if (this._nativeObject) { this.scale.x = value; } }

        @CheckDisposed
        public get scaleY(): number { return this._nativeObject ? this.scale.y : 1; }
        public set scaleY(value: number) { if (this._nativeObject) { this.scale.y = value; } }

        public constructor()
        {
            super();
            this.initialise(RSGraphics, null);
            this.scaleX = this.scaleY = 1;
        }

        /**
         * @inheritdoc
         * @param x @inheritdoc
         * @param y @inheritdoc
         */
        public moveTo(x: number, y: number): this;
        public moveTo(pos: Math.Vector2D): this;
        @CheckDisposed
        public moveTo(p1: Math.Vector2D | number, p2?: number): this
        {
            if (Is.number(p1))
            {
                this._nativeObject.moveTo(p1, p2);
            }
            else
            {
                this._nativeObject.moveTo(p1.x, p1.y);
            }
            return this;
        }
        /**
         * @inheritdoc
         * @param x @inheritdoc
         * @param y @inheritdoc
         */
        public lineTo(pos: Math.Vector2D): this;
        public lineTo(x: number, y: number): this;
        @CheckDisposed
        public lineTo(p1: Math.Vector2D | number, p2?: number): this
        {
            if (Is.number(p1))
            {
                this._nativeObject.lineTo(p1, p2);
            }
            else
            {
                this._nativeObject.lineTo(p1.x, p1.y);
            }
            return this;
        }
        /**
         * @inheritdoc
         * @param thickness @inheritdoc
         * @param color @inheritdoc
         * @param alpha @inheritdoc
         * @param caps @inheritdoc
         * @param joints @inheritdoc
         * @param miterLimit @inheritdoc
         */
        @CheckDisposed
        public lineStyle(thickness: number, color: Util.Color, alpha?: number, caps?: string, joints?: string, miterLimit?: number): this
        {
            if (caps || joints || miterLimit)
            {
                //console.log("Caps, Joints and Miter Styles unsupported by PixiJs due to webGL complexities =(");
            }

            this._nativeObject.lineStyle(thickness, color ? color.tint : null, alpha);

            return this;
        }
        /**
         * @inheritdoc
         */
        @CheckDisposed
        public beginFill(color: Util.Color, alpha: number = 1): this
        {
            this._nativeObject.beginFill(color.tint, alpha);

            return this;
        }
        /**
         * @inheritdoc
         */
        @CheckDisposed
        public endFill(): this
        {
            this._nativeObject.endFill();

            return this;
        }
        /**
         * @inheritdoc
         */
        @CheckDisposed
        public clear(): this
        {
            this._nativeObject.clear();

            return this;
        }
        /**
         * @inheritdoc
         * @param x @inheritdoc
         * @param y @inheritdoc
         * @param radius @inheritdoc
         */
        public drawCircle(x: number, y: number, radius: number): this;
        public drawCircle(pos: Math.Vector2D, radius: number): this;
        public drawCircle(circle: Circle): this;
        @CheckDisposed
        public drawCircle(p1: number | Math.Vector2D | Circle, p2?: number, p3?: number): this
        {
            if (Is.number(p1))
            {
                this._nativeObject.drawCircle(p1, p2, p3);
            }
            else if (p1 instanceof Circle)
            {
                this._nativeObject.drawCircle(p1.x, p1.y, p1.r);
            }
            else
            {
                const pos: Math.Vector2D = p1 as Math.Vector2D;
                this._nativeObject.drawCircle(pos.x, pos.y, p2);
            }

            return this;
        }
        /**
         * @inheritdoc
         * @param x @inheritdoc
         * @param y @inheritdoc
         * @param width @inheritdoc
         * @param height @inheritdoc
         */
        public drawEllipse(x: number, y: number, width: number, height: number): this;
        public drawEllipse(pos: Math.Vector2D, size: Math.Size2D): this;
        public drawEllipse(rect: Rectangle): this;
        @CheckDisposed
        public drawEllipse(p1: number | Math.Vector2D | Rectangle, p2?: number | Math.Size2D, p3?: number, p4?: number): this
        {
            if (Is.number(p1) && Is.number(p2))
            {
                this._nativeObject.drawEllipse(p1, p2, p3 / 2, p4 / 2);
            }
            else if (Is.vector2D(p1) && p2 != null)
            {
                const size: Math.Size2D = p2 as Math.Size2D;
                this._nativeObject.drawEllipse(p1.x, p1.y, size.w / 2, size.h / 2);
            }
            else
            {
                const rect: Rectangle = p1 as any as Rectangle;
                this._nativeObject.drawEllipse(rect.x, rect.y, rect.w / 2, rect.h / 2);
            }
            return this;
        }
        /**
         * @inheritdoc
         * @param x @inheritdoc
         * @param y @inheritdoc
         * @param width @inheritdoc
         * @param height @inheritdoc
         */
        public drawRect(x: number, y: number, width: number, height: number): this;
        public drawRect(pos: Math.Vector2D, size: Math.Size2D): this;
        public drawRect(rect: Rectangle): this;
        @CheckDisposed
        public drawRect(p1: number | Math.Vector2D | Rectangle, p2?: number | Math.Size2D, p3?: number, p4?: number): this
        {
            if (Is.number(p1) && Is.number(p2))
            {
                this._nativeObject.drawRect(p1, p2, p3, p4);
            }
            else if (Is.vector2D(p1) && p2 != null)
            {
                const size: Math.Size2D = p2 as Math.Size2D;
                this._nativeObject.drawRect(p1.x, p1.y, size.w, size.h);
            }
            else
            {
                const rect: Rectangle = p1 as any as Rectangle;
                this._nativeObject.drawRect(rect.x, rect.y, rect.w, rect.h);
            }

            return this;
        }
        /**
         * @inheritdoc
         * @param x @inheritdoc
         * @param y @inheritdoc
         * @param width @inheritdoc
         * @param height @inheritdoc
         * @param radius @inheritdoc
         */
        public drawRoundedRect(x: number, y: number, width: number, height: number, radius: number): this;
        public drawRoundedRect(pos: Math.Vector2D, size: Math.Size2D, radius: number): this;
        public drawRoundedRect(rect: Rectangle, radius: number): this;
        @CheckDisposed
        public drawRoundedRect(p1: number | Math.Vector2D | Rectangle, p2: number | Math.Size2D, p3?: number, p4?: number, p5?: number): this
        {
            if (Is.number(p1) && Is.number(p2))
            {
                this._nativeObject.drawRoundedRect(p1, p2, p3, p4, p5);
            }
            else if (Is.number(p2))
            {
                const rect: Rectangle = p1 as Rectangle;
                this._nativeObject.drawRoundedRect(rect.x, rect.y, rect.w, rect.h, p2);
            }
            else
            {
                const pos: Math.Vector2D = p1 as Math.Vector2D;
                const size: Math.Size2D = p2 as Math.Size2D;
                this._nativeObject.drawRoundedRect(pos.x, pos.y, size.w, size.h, p3);
            }
            return this;
        }
        /**
         * @inheritdoc
         *
         * @param {number} x @inheritdoc
         * @param {number} y @inheritdoc
         * @param {number} radius @inheritdoc
         * @param {number} startAngle @inheritdoc
         * @param {number} endAngle @inheritdoc
         * @param {boolean} anticlockwise @inheritdoc
         * @returns {this<T>} @inheritdoc
         */
        public arc(cx: number, cy: number, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean): this;
        public arc(cpos: Math.Vector2D, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean): this;
        @CheckDisposed
        public arc(p1: number | Math.Vector2D, p2: number, p3: number, p4: number, p5: number | boolean, p6?: boolean): this
        {
            if (Is.number(p1))
            {
                this._nativeObject.arc(p1, p2, p3, p4, p5 as number, p6);
            }
            else
            {
                const pos: Math.Vector2D = p1 as Math.Vector2D;
                this._nativeObject.arc(pos.x, pos.y, p2, p3, p4, p5 as boolean);
            }
            return this;
        }
        /**
         * @inheritdoc
         *
         * @param {number} x @inheritdoc
         * @param {number} y @inheritdoc
         * @param {number} controlX @inheritdoc
         * @param {number} controlY @inheritdoc
         */
        public quadraticCurveTo(cpX: number, cpY: number, toX: number, toY: number): this;
        public quadraticCurveTo(cp: Math.Vector2D, to: Math.Vector2D): this;
        @CheckDisposed
        public quadraticCurveTo(p1: number | Math.Vector2D, p2: number | Math.Vector2D, toX?: number, toY?: number): this
        {
            if (Is.number(p1) && Is.number(p2))
            {
                this._nativeObject.quadraticCurveTo(p1, p2, toX, toY);
            }
            else
            {
                const cp: Math.Vector2D = p1 as Math.Vector2D;
                const to: Math.Vector2D = p2 as Math.Vector2D;
                this._nativeObject.quadraticCurveTo(cp.x, cp.y, to.x, to.y);
            }
            return this;
        }

        /** Draws a bezier curve. */
        public bezierCurveTo(cp1: Math.Vector2D, cp2: Math.Vector2D, to: Math.Vector2D): this;
        public bezierCurveTo(cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number): this;
        @CheckDisposed
        public bezierCurveTo(p1: number | Math.Vector2D, p2: number | Math.Vector2D, p3: number | Math.Vector2D, cpY2?: number, toX?: number, toY?: number): this
        {
            if (Is.number(p1) && Is.number(p2) && Is.number(p3))
            {
                this._nativeObject.bezierCurveTo(p1, p2, p3, cpY2, toX, toY);
            }
            else
            {
                const cp1: Math.Vector2D = p1 as Math.Vector2D;
                const cp2: Math.Vector2D = p2 as Math.Vector2D;
                const to: Math.Vector2D = p3 as Math.Vector2D;
                this._nativeObject.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, to.x, to.y);
            }
            return this;
        }

        /** Draws an arc. */
        public arcTo(x1: number, y1: number, x2: number, y2: number, radius: number): this;
        public arcTo(pos1: Math.Vector2D, pos2: Math.Vector2D, radius: number): this;
        @CheckDisposed
        public arcTo(p1: number | Math.Vector2D, p2: number | Math.Vector2D, p3: number, p4?: number, p5?: number): this
        {
            if (Is.number(p1) && Is.number(p2))
            {
                this._nativeObject.arcTo(p1, p2, p3, p4, p5);
            }
            else
            {
                const pos1: Math.Vector2D = p1 as Math.Vector2D;
                const pos2: Math.Vector2D = p2 as Math.Vector2D;
                this._nativeObject.arcTo(pos1.x, pos1.y, pos2.x, pos2.y, p3);
            }
            return this;
        }
        /** Draws an arbitrary polygon. */
        public drawPolygon(path: Math.Vector2DList): this;
        public drawPolygon(path: Math.Vector2D[]): this;
        @CheckDisposed
        public drawPolygon(path: Math.Vector2D[] | Math.Vector2DList): this
        {
            const pathList: Math.Vector2DList = path as Math.Vector2DList;
            const finalPath: Math.Vector2D[] = pathList.toArray ? pathList.toArray().concat() : (path as Math.Vector2D[]).concat();
            const nativeList: number[] = [];
            for (let i: number = 0; i < finalPath.length; i++)
            {
                nativeList.push(finalPath[i].x);
                nativeList.push(finalPath[i].y);
            }
            this._nativeObject.drawPolygon(nativeList);
            //this._nativeObject.drawPolygon([0,0,90,90,-90,90,0,0]);

            return this;
        }

        /** Draws an arbitrary shape */
        public drawShape(shape: ReadonlyShape): this
        {
            if (shape instanceof ReadonlyRectangle)
            {
                return this.drawRect(shape.x, shape.y, shape.w, shape.h);
            }
            else if (shape instanceof ReadonlyRoundedRectangle)
            {
                return this.drawRoundedRect(shape.x, shape.y, shape.w, shape.h, shape.r)
            }
            else if (shape instanceof ReadonlyCircle)
            {
                return this.drawCircle(shape.x, shape.y, shape.r);
            }
            else if (shape instanceof ReadonlyEllipse)
            {
                return this.drawEllipse(shape.x, shape.y, shape.w, shape.h);
            }
            else if (shape instanceof ReadonlyPolygon)
            {
                return this.drawPolygon(shape.points);
            }
        }

        /** Gets if the path in this graphics object contains the specified point. */
        public containsPoint(x: number, y: number): boolean;
        public containsPoint(pos: Math.Vector2D): boolean;
        @CheckDisposed
        public containsPoint(p1: number | Math.Vector2D, p2?: number): boolean
        {
            if (Is.number(p1))
            {
                return this._nativeObject.containsPoint(new PIXI.Point(p1, p2));
            }
            const pos: Math.Vector2D = p1 as Math.Vector2D;
            return this._nativeObject.containsPoint(new PIXI.Point(pos.x, pos.y));
        }

        /** Specifies the last closed path as a "hole". */
        @CheckDisposed
        public addHole(): this
        {
            this._nativeObject.addHole();
            return this;
        }

        /** Closes the current path and starts a new one. */
        @CheckDisposed
        public closePath(): this
        {
            this._nativeObject.closePath();
            return this;
        }

        protected destroyNativeObject()
        {
            if (this._nativeObject["graphicsData"])
            {
                this._nativeObject.destroy();
            }
        }

        /**
         * @inheritdoc
         */
        protected createClass(classType: { new(...args: any[]): RSGraphics; }, args: any[]): RSGraphics
        {
            return new RSGraphics();
        }
    }
    Rendering.Graphics = Graphics;
}