/** @internal  */
namespace RS.Rendering.Pixi
{
    export class Text extends Pixi.DisplayObject<TextBase> implements Rendering.IText
    {
        @RS.AutoDisposeOnSet protected _nativeObject: TextBase;

        /**
         * Gets the number of lines in this text.
         */
        @CheckDisposed
        public get lineCount() { return this._nativeObject.linesCount; }

        public get textFilter() { return this._nativeObject.textFilter; }
        public set textFilter(filter: TextOptions.TextFilter) { this._nativeObject.textFilter = filter; }

        /**
         * Gets or sets the content of this text.
         */
        @CheckDisposed
        public get text() { return this._nativeObject.text; }
        public set text(value) { this._nativeObject.text = value; }

        /**
         * Gets or sets the font of this text.
         */
        @CheckDisposed
        public get font() { return this._nativeObject.font; }
        public set font(value) { this._nativeObject.font = value; }

        /**
         * Gets or sets the font size of this text.
         */
        @CheckDisposed
        public get fontSize() { return this._nativeObject.fontSize; }
        public set fontSize(value) { this._nativeObject.fontSize = value; }

        /**
         * Gets or sets the wrap width of this text.
         */
        @CheckDisposed
        public get wrapWidth() { return this._nativeObject.wrapWidth; }
        public set wrapWidth(value) { this._nativeObject.wrapWidth = value; }

        /**
         * Gets or sets the horizontal alignment for this text.
         * Only applicable for multiline text.
         */
        @CheckDisposed
        public get align() { return this._nativeObject.align; }
        public set align(value) { this._nativeObject.align = value; }

        /**
         * Gets or sets the tint of this text.
         */
        @CheckDisposed
        public get tint(): Util.Color { return this._nativeObject.tint; }
        public set tint(value: Util.Color)
        {
            this._nativeObject.tint = value != null ? value : new Util.Color(1, 1, 1);
        }

        @CheckDisposed
        public get scaleToWidth() { return this.nativeObject.scaleToWidth; }
        public set scaleToWidth(value) { this.nativeObject.scaleToWidth = value; }

        @CheckDisposed
        public get outlineColour() { return null; }
        public set outlineColour(value) { Log.warn("outlineColour not implemented yet"); }

        @CheckDisposed
        public get outlineSize() { return 0; }
        public set outlineSize(value) { Log.warn("outlineSize not implemented yet"); }

        public get anchorX(): number { return 0; }
        public set anchorX(value: number) { Log.warn("anchorX not implemented yet"); }

        public get anchorY(): number { return 0; }
        public set anchorY(value: number) { Log.warn("anchorY not implemented yet"); }

        public constructor(settings: TextOptions.Settings, text: string = "")
        {
            super();
            this.initialise(TextBase, settings, text);
            this.onPreRender((data: RenderEventData) =>
            {
                if (Pixi.World.apparent(this))
                {
                    this._nativeObject.render(data.renderer);
                }
            });
        }

        public getLocalBounds(): ReadonlyRectangle
        {
            return this.localBounds;
        }

        public calculateTextBounds(): ReadonlyRectangle
        {
            return this.localBounds;
        }

        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new (...args: any[]): TextBase; }, args: any[]): TextBase
        {
            return new TextBase(args[0], args[1]);
        }
    }

    Rendering.Text = Text;
}