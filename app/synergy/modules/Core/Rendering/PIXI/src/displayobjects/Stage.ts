/// <reference path="DisplayObject.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * @internal
     */
    @HasCallbacks
    export class Stage extends DisplayObjectContainer implements IStage
    {
        protected static _supportedStyles: string[] | null = null;

        public onResized: IPrivateEvent<StageEventData> = createEvent<StageEventData>();

        public paused: boolean;

        @AutoDispose
        public readonly inputBlocked = new Arbiter(RS.Arbiter.OrResolver, false);

        @AutoDispose
        protected _renderTickerHandle: IDisposable | null = null;
        protected _tickerHandle: IDisposable | null = null;
        protected _renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
        protected _fps: number;
        protected _interval: number;
        protected _autoRender: boolean;
        protected _stageWidth: number;
        protected _stageHeight: number;
        protected _renderStyle: string;

        @AutoDispose private readonly __dumpHandler: IDisposable;

        public get stageWidth(): number { return this._renderer.width / this.scaleX; }

        public get stageHeight(): number { return this._renderer.height / this.scaleY; }

        public get canvas(): HTMLCanvasElement { return this._renderer.view; }

        public get autoRender(): boolean { return this._autoRender; }
        public set autoRender(value: boolean)
        {
            this._autoRender = value;
            if (value && !this._renderTickerHandle)
            {
                this._renderTickerHandle = ITicker.get().add((ev) => this.onNextFrame(ev.delta), {
                    kind: Ticker.Kind.Render,
                    priority: Ticker.Priority.Render,
                    data: this._interval
                });
            }
            else if (this._renderTickerHandle && !value)
            {
                this._renderTickerHandle.dispose();
                this._renderTickerHandle = null;
            }
        }

        public get fps(): number { return this._fps; }
        public set fps(value: number)
        {
            this._fps = value;
            this._interval = 1000 / this._fps;

            // get autoRender property to reset the ticker with our new interval
            const oldAutoRender = this._autoRender;
            this.autoRender = false;
            this.autoRender = oldAutoRender;
        }

        /**
         * Gets the current render style used for the stage
         */
        public get renderStyle() { return this._renderStyle; }

        /** @internal  */
        public get renderer() { return this._renderer; }

        constructor(element: HTMLElement, width: number, height: number, renderStyles: string[] = [StageRenderStyle.WEBGL, StageRenderStyle.CANVAS], transparent: boolean = false)
        {
            super();

            if (Stage._supportedStyles == null)
            {
                Stage._supportedStyles = [StageRenderStyle.CANVAS];
                if (RS.Capabilities.WebGL.get().available)
                {
                    Stage._supportedStyles.push(StageRenderStyle.WEBGL);
                }
            }
            
            if (Device.isMobileDevice && Device.isAppleDevice && Capabilities.WebGL.get().supportsHighPrecisionFragmentShaders)
            {
                // Prevents sprite animations from "wobbling" on iOS
                PIXI.settings.PRECISION_FRAGMENT = "highp";
            }

            this.fps = 60;
            this.paused = false;

            const renderStyle = this.getSupportedRenderer(renderStyles);

            const options: PIXI.RendererOptions = { transparent: transparent };
            if (element instanceof HTMLCanvasElement)
            {
                options.view = element;
            }
            if (renderStyle === StageRenderStyle.WEBGL)
            {
                this._renderer = new PIXI.WebGLRenderer(width, height, options);
            }
            else if (renderStyle === StageRenderStyle.CANVAS)
            {
                this._renderer = new PIXI.CanvasRenderer(width, height, options);
            }
            if (this._renderer instanceof PIXI.WebGLRenderer)
            {
                if (primaryRenderMode === RenderMode.Unknown) { primaryRenderMode = RenderMode.WebGL; }
                this._renderStyle = StageRenderStyle.WEBGL;
            }
            else if (this._renderer instanceof PIXI.CanvasRenderer)
            {
                if (primaryRenderMode === RenderMode.Unknown) { primaryRenderMode = RenderMode.Canvas; }
                this._renderStyle = StageRenderStyle.CANVAS;
            }

            if (!options.view)
            {
                element.appendChild(this._renderer.view);
            }

            warmupGradientShaders(this);

            this.autoRender = true;
            this._nativeObject["_nativeRenderer"] = this._renderer;
            this.onNextFrame(0);

            const dump = RS.IDump.get();
            const stageDump = dump.getSubDump("Stage");
            stageDump.clearOnSave = true;
            this.__dumpHandler = dump.onPreSave(() => this.dump(stageDump));
        }

        public render(): void
        {
            if (this.paused)
            {
                return;
            }

            if (this._prePostRenderTree)
            {
                PrePostRenderTree.preRender(this._prePostRenderTree, this._renderer);
            }

            this._renderer.render(this._nativeObject);

            if (this._prePostRenderTree)
            {
                PrePostRenderTree.postRender(this._prePostRenderTree, this._renderer);
            }

            this.onUpdated.publish(this);
        }

        public resize(width: number, height: number): void
        {
            this._renderer.resize(width, height);
            this.onResized.publish({
                width: width,
                height: height,
                stage: this
            });
        }

        /**
         * Capture the canvas into a png image and download it.
         */
        public capture(): void
        {
            // save canvas in data
            const sourceCanvas = this._renderer.extract.canvas();
            const dataURL: string = sourceCanvas.toDataURL();
            const data = atob(dataURL.substring("data:image/png;base64,".length));
            // change to correct encoding
            const asArray = new Uint8Array(data.length);
            for (let i = 0, len = data.length; i < len; ++i)
            {
                asArray[i] = data.charCodeAt(i);
            }
            // create blob
            const curTime = Dump.getTimestamp();
            const blob = new Blob([asArray.buffer], { type: "image/png" });
            // create download picture link
            URL.download(`capture-${curTime}.png`, blob);
        }

        @Callback
        protected dump(target: RS.ISubDump): void
        {
            target.add(this, (key, value) => value instanceof DisplayObject);
        }

        protected onNextFrame(delta: number)
        {
            this.render();
        }

        protected getSupportedRenderer(renderStyles: string[]): string
        {
            for (let i: number = 0; i < renderStyles.length; i++)
            {
                if (Stage._supportedStyles.indexOf(renderStyles[i]) !== -1)
                {
                    return renderStyles[i];
                }
            }
            return "";
        }
    }
    Rendering.Stage = Stage;
}
