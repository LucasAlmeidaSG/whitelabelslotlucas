/// <reference path="DisplayObjectContainerBase.ts" />
/// <reference path="../util/Mesh.ts" />

/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * The Mesh class implements the IMesh interface.
     *
     * A Mesh encapsulates a polygonal mesh consisting of vertices that make up triangles. A Bitmap also inherits all functionality from DisplayObject.
     */
    export class MeshDisplayObject extends DisplayObject<Mesh> implements IMesh
    {
        protected _shader: IShader = null;
        protected _tint: Util.Color = null;
        protected _subTexture: SubTexture = null;

        public get texture() { return this._subTexture; }
        public set texture(value)
        {
            this._subTexture = value;
            this._nativeObject.texture = value ? value.nativeObject : null;
        }

        /** Gets or sets the shader to be used for this sprite. */
        public get shader(): IShader | null { return this._shader; }
        public set shader(value: IShader | null)
        {
            this._shader = value;
        }

        public get tint(): Util.Color { return this._tint; }
        public set tint(value: Util.Color)
        {
            this._tint = value;
            this._nativeObject.tint = value != null ? value.tint : new Util.Color(1, 1, 1).tint;
        }

        public get positions() { return this._nativeObject.vertices; }
        public get indices() { return this._nativeObject.indices; }
        public get texCoords() { return this._nativeObject.uvs; }

        constructor(settings: IMesh.Settings, texture?: ISubTexture)
        {
            super();

            this.initialise(Mesh, settings, texture);
        }

        /** Flags the mesh to update the vertices */
        public markVerticesDirty() { this._nativeObject.markVerticesDirty(); }

        /**
         * Creates the native class instance with the class type and arguments from the constructor
         */
        protected createClass(classType: { new(...args: any[]): Mesh; }, args: any[]): Mesh
        {
            const settings: IMesh.Settings = args[0];
            const texture: SubTexture | null = args[1];
            return new Mesh(texture && texture.nativeObject, settings.positions, settings.texCoords, settings.indices);
        }
    }
    Rendering.Mesh = MeshDisplayObject;
}
