/** @internal  */
namespace RS.Rendering.Pixi
{
    /**
     * Encapsulates a HTML 5 video element.
     * @internal
     */
    @HasCallbacks
    export class Video extends Rendering.Pixi.DisplayObjectContainer implements IVideo, IDisposable
    {
        /** Published when the current frame of this video has changed. */
        public readonly onFrameChanged = createEvent<VideoFrameEvent>();

        /** Published when this video has finished playing. */
        public readonly onFinished = createSimpleEvent();

        protected _tag: HTMLVideoElement;
        @AutoDispose protected _component: DOM.Component<HTMLVideoElement>;

        protected _videoTexture: PIXI.VideoBaseTexture;
        protected _videoSprite: Pixi.Bitmap;
        protected _poster: Pixi.Bitmap;

        protected _videoQueue: QueueEntry[];
        protected _play: boolean;
        protected _playCallback: (...args) => void;
        protected _startPoster: Pixi.SubTexture;
        protected _endPoster: Pixi.SubTexture;
        protected _time: number;
        protected _frame: number;
        protected _framerate: number;
        protected _src: Asset.IVideoAsset<PIXI.BaseTexture>;
        @AutoDispose protected _tickerHandle: IDisposable | null = null;
        protected _singleStep: boolean;
        protected _playing: boolean;
        protected _endCallback: () => void;
        @AutoDisposeOnSet protected _sound: RS.Audio.ISound | null;

        /** Gets the underlying video tag for this video. */
        @CheckDisposed
        public get tag() { return this._tag; }

        /** Gets if this video is playing or not. */
        @CheckDisposed
        public get playing() { return this._playing; }

        constructor(public readonly settings: Video.Settings)
        {
            super();

            this._poster = new Bitmap(null);
            this._poster.visible = false;
            this.addChild(this._poster);

            this._tag = document.createElement("video");
            this._component = new DOM.Component(this._tag);
            this._tag.addEventListener("click", (ev) => this.onClicked.publish({
                target: this,
                globalPosition: { x: ev.pageX, y: ev.pageY },
                localPosition: { x: ev.screenX, y: ev.screenY },
                pointerID: 0
            }));
            this._tag.addEventListener("touchend", (ev) => this.onClicked.publish({
                target: this,
                globalPosition: { x: 0, y: 0 },
                localPosition: { x: 0, y: 0 },
                pointerID: 0
            }));
            if (this.shouldUseDOM())
            {
                if (settings.cssContainer instanceof DOM.Component)
                {
                    settings.cssContainer.addChild(this._component, settings.cssIndex);
                }
                else
                {
                    settings.cssContainer.appendChild(this._component.element);
                }
            }
            else
            {
                this._videoTexture = PIXI.VideoBaseTexture.fromVideo(this._tag);
                this._videoTexture.autoPlay = false;
                this._videoTexture.autoUpdate = false;
                const baseTex = new Pixi.Texture(this._videoTexture);
                this._videoSprite = new Pixi.Bitmap(new Pixi.SubTexture(baseTex));
                this.addChild(this._videoSprite);
            }

            this._tag.autoplay = false;
            this._tag.loop = false;
            this._tag.addEventListener("ended", this.handleVideoEnded);
            this._tag.addEventListener("canplay", this.handleReadyToPlay);
            if (settings.cssID != null) { this._tag.id = settings.cssID; }

            this._tag.muted = true;
            this._tag.hidden = false;
            this._tag.setAttribute("playsinline", "");
            this._tag.load();

            this._play = false;
            this._endPoster = null;
            this._playing = false;
            this._singleStep = false;

            if (Device.isAppleDevice && Device.isMobileDevice)
            {
                enableInlineVideo(this._tag);
            }

            if (settings.audio)
            {
                this._sound = RS.Audio.play(settings.audio, { paused: true });
            }

            this._videoQueue = [];
        }

        /**
         * Plays a video immediately.
         * @param params
         */
        @CheckDisposed
        public play(params: QueueEntry)
        {
            if (this._playing)
            {
                if (this._videoTexture) { this._videoTexture.autoUpdate = false; }
                if (this._tickerHandle)
                {
                    this._tickerHandle.dispose();
                    this._tickerHandle = null;
                }
                this._playing = false;
            }

            this._src = Asset.Store.getAsset(params.src).as(Asset.VideoAsset);

            if (this._src.soundSource)
            {
                this._sound = RS.Audio.ISoundController.get().createSound(this._src.soundSource);
                this._sound.pause();
            }

            this._framerate = this._src.framerate;
            this._tag.src = this._src.asset;
            this._tag.loop = params.loop;
            this._tag.playbackRate = RS.ITicker.get().timeScale;
            this._play = true;
            this._playCallback = params.callback;
            if (this._src.startPoster != null)
            {
                this._startPoster = new SubTexture(new Texture(this._src.startPoster));
                this._poster.texture = this._startPoster;
            }
            if (this._src.endPoster != null)
            {
                this._endPoster = new SubTexture(new Texture(this._src.endPoster));
            }
            this._endCallback = params.endCallback;
            this._tag.play();
            if (this._startPoster != null)
            {
                this._poster.visible = true;
            }
            this._playing = true;
        }

        /**
         * Queues a video to be played after the current one has finished.
         * @param params
         */
        @CheckDisposed
        public queue(params: QueueEntry)
        {
            if (this._videoQueue.length === 0)
            {
                this.play(params);
            }
            else
            {
                this._videoQueue.push(params);
                this._tag.loop = false;
            }
        }

        /**
         * Pauses the currently playing video.
         */
        @CheckDisposed
        public pause()
        {
            this._singleStep = true;
            this._tag.playbackRate = 0;
            if (this._sound && !this._sound.isDisposed)
            {
                this._sound.pause();
            }
        }

        /**
         * Sets the current frame to be displayed.
         * @param frame
         */
        @CheckDisposed
        public setFrame(frame: number)
        {
            this._frame = frame;
            this._time = (frame + 0.5) / this._framerate;
            this._tag.currentTime = this._time;
            this._tag.playbackRate = 0;
            this.onFrameChanged.publish({ src: this._src, frame: this._frame });
        }

        /**
         * Steps forward a single frame.
         * Enables single-stepping.
         */
        @CheckDisposed
        public step()
        {
            this._singleStep = true;
            this._tag.playbackRate = RS.ITicker.get().timeScale;
        }

        /**
         * Resumes normal playback.
         * Disables single-stepping.
         */
        @CheckDisposed
        public resume()
        {
            this._singleStep = false;
            this._tag.playbackRate = RS.ITicker.get().timeScale;
            if (this._sound && !this._sound.isDisposed)
            {
                this._sound.play();
            }
        }

        /**
         * Force video to end and publish on finished
         */
        public end(): void
        {
            this.pause();
            this.handleVideoEnded();
        }

        /**
         * Disposes this video.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }
            if (this._tag)
            {
                this._tag.removeEventListener("ended", this.handleVideoEnded);
                this._tag.removeEventListener("canplay", this.handleReadyToPlay);
                if (this._tag.parentElement != null) { this._tag.parentElement.removeChild(this._tag); }
                this._tag = null;
            }
            if (this._videoSprite)
            {
                this._videoSprite.dispose();
                this._videoSprite = null;
            }
            if (this._videoTexture)
            {
                this._videoTexture.autoUpdate = false;
                this._videoTexture.destroy();
                this._videoTexture = null;
            }
            super.dispose();
        }

        protected shouldUseDOM(): boolean
        {
            if (this.settings.useDom != null) { return this.settings.useDom; }
            if (IOS.isDeviceIOS) { return true; }
            if (Device.browser === "IE") { return true; }
            return false;
        }

        @Callback
        @CheckDisposed
        protected handleVideoEnded()
        {
            if (this._videoTexture) { this._videoTexture.autoUpdate = false; }
            this.onFinished.publish();
            if (this._endCallback)
            {
                this._endCallback();
            }
            if (this._videoQueue.length > 0)
            {
                this.play(this._videoQueue.shift());
            }
            else
            {
                if (this._endPoster)
                {
                    this._poster.texture = this._endPoster;
                    this._poster.visible = true;
                }
                this._playing = false;
            }
            this._tickerHandle.dispose();
            this._tickerHandle = null;
        }

        @Callback
        @CheckDisposed
        protected handleReadyToPlay()
        {
            if (this._play)
            {
                const ticker = ITicker.get();
                ticker.after(() => { this._poster.visible = false; }, 0);
                this._play = false;
                if (this._playCallback)
                {
                    this._playCallback();
                    this._playCallback = null;
                }
                this._playing = true;
                this._time = 0;
                this._frame = -1;
                this._tickerHandle = ticker.add(this.tick);

                if (this._sound != null && !this._sound.isDisposed)
                {
                    this._sound.play();
                }

                if (this._videoSprite)
                {
                    this._videoTexture.update();
                    this._videoSprite.texture.frame = new ReadonlyRectangle(0, 0, this._tag.videoWidth, this._tag.videoHeight);
                }
            }
        }

        @Callback
        private tick(ev: RS.Ticker.Event)
        {
            this._time += ev.delta / 1000;
            const frame = Math.floor(this._time * this._framerate);
            if (frame !== this._frame)
            {
                this._frame = frame;
                this.onFrameChanged.publish({ src: this._src, frame: this._frame });
                this._tag.playbackRate = this._singleStep ? 0 : RS.ITicker.get().timeScale;
            }
            if (this._component.parent)
            {
                const stage = this.getStage();
                if (stage != null)
                {
                    const canvasX = stage.canvas.style.left;
                    const canvasY = stage.canvas.style.top;

                    const t = this.nativeObject.worldTransform;

                    this._tag.style.left = canvasX;
                    this._tag.style.top = canvasY;
                    //this._tag.style.width = `${canvasWidth}px`;
                    //this._tag.style.height = `${canvasHeight}px`;
                    this._tag.style.transform = `matrix(${t.a.toFixed(2)},${t.b.toFixed(2)},${t.c.toFixed(2)},${t.d.toFixed(2)},${t.tx.toFixed(2)},${t.ty.toFixed(2)})`;
                    this._tag.style.transformOrigin = `top left`;
                }
            }
            if (this._videoTexture) { this._videoTexture.update(); }
        }

        private getStage(): Rendering.IStage
        {
            let curObj: Rendering.IDisplayObject = this;
            while (curObj.parent != null)
            {
                curObj = curObj.parent;
            }
            return curObj instanceof Rendering.Stage ? curObj : null;
        }
    }

    Rendering.Video = Video;
}
