/** @internal */
namespace RS.Rendering.Pixi
{
    export class Spine extends DisplayObject<PIXI.Container> implements Rendering.ISpine
    {

        public readonly onSpineComplete: IPrivateEvent<ISpine> = createEvent<ISpine>();
        public readonly onSpineRepeat: IPrivateEvent<ISpine> = createEvent<ISpine>();
        public readonly onSpineUpdate: IPrivateEvent<SpineEventData> = createEvent<SpineEventData>();

        protected images: ITexture[];
        protected _spineScale: number;

        protected nativeSpine: PIXI.spine.Spine;
        protected _animation: PIXI.spine.core.Animation;

        @RS.AutoDisposeOnSet protected _tween: RS.Tween<any>;
        protected _lastTime: number;
        protected _playOffset: number;
        protected _playTo: number;
        protected _repeatCount: number;
        protected _playbackSpeed: number;
        protected _animationName: string;
        protected _animationTime: number;

        protected _playSettings: ISpine.PlaySettings;
        protected _playing: boolean;

        constructor(scale: number, asset: Asset.Pixi.SpineAsset, useSetupPose?: boolean)
        {
            super();

            super.initialise(PIXI.Container, null);

            this._lastTime = 0;
            this._spineScale = scale;
            this._playbackSpeed = 1.0;

            this.images = asset.asset.images;

            const spineAtlas: PIXI.spine.core.TextureAtlas = new PIXI.spine.core.TextureAtlas(asset.asset.atlas, this.textureLoader.bind(this));
            const spineAtlasLoader: PIXI.spine.core.AtlasAttachmentLoader = new PIXI.spine.core.AtlasAttachmentLoader(spineAtlas);
            const skeletonJsonParser: PIXI.spine.core.SkeletonJson = new PIXI.spine.core.SkeletonJson(spineAtlasLoader);
            const skeletonData: PIXI.spine.core.SkeletonData = skeletonJsonParser.readSkeletonData(JSON.stringify(asset.asset.json));

            this.nativeSpine = new PIXI.spine.Spine(skeletonData);
            this.nativeSpine.autoUpdate = false;
            this.nativeSpine.skeleton.setSlotsToSetupPose();
            this._nativeObject.addChild(this.nativeSpine);

            this.nativeSpine.state.addListener({
                end: () => this.handleSpineEnded(),
                complete: () => this.handleSpineComplete()
            });

            if (!useSetupPose)
            {
                this.gotoAndStop(skeletonData.animations[0].name);
            }
        }

        public setAnimation(animation: string): void
        {
            RS.Log.warn("DEPRECATED - This function is deprecated, please use gotoAndPlay instead of setAnimation and play.");
            this._animationName = animation;
            this._animation = this.nativeSpine.skeleton.data.findAnimation(animation);
        }

        public play(repeatCount?: number, to?: number, speed?: number): void
        {
            RS.Log.warn("DEPRECATED - This function is deprecated, please use gotoAndPlay instead of setAnimation and play.");
            this._animationTime = 0;
            this._lastTime = 0;
            this._playOffset = 0;
            this._playTo = to;
            this.doPlay(repeatCount, to, speed);
        }

        public playFrom(from: number, to?: number, repeatCount?: number, speed?: number): void
        {
            RS.Log.warn("DEPRECATED - This function is deprecated, please use gotoAndPlay instead of setAnimation and play.");
            this._lastTime = 0;
            this._animationTime = from * 1000;
            this._playOffset = from;
            this._playTo = to;
            this.doPlay(repeatCount, to, speed);
        }

        public stop(reset: boolean = false): void
        {
            if (this._tween)
            {
                this._tween.paused = true;
                if (reset)
                {
                    this._tween.position = 0;
                }
            }
            else
            {
                this.unregisterTickers();
            }
        }

        public isPlaying(): boolean
        {
            let playing: boolean = false;
            if (this._tween)
            {
                playing = !this._tween.isDisposed && !this._tween.paused && !this._tween.finished;
            }
            return playing;
        }

        public setSkin(skin: string): void
        {
            this.nativeSpine.skeleton.setSkinByName(skin);
        }

        public gotoAndPlay(animation?: string, settings?: ISpine.PlaySettings) : PromiseLike<void>;

        public gotoAndPlay(animation?: string[], settings?: ISpine.PlaySettings) : PromiseLike<void>;

        public async gotoAndPlay(animation: string | string[] = this._animationName, settings?: ISpine.PlaySettings)
        {
            this._playSettings = settings;
            const time = settings ? settings.startTime || 0 : 0;
            const timeScale = settings ? settings.speed || 1 : 1;
            const loop = settings ? settings.loop || false : false;

            this.setAnimations(animation, time, loop);
            this.nativeSpine.state.timeScale = timeScale;
            this.registerTickers();

            await Promise.race([this.onSpineComplete, this.onSpineRepeat]);
        }

        public gotoAndStop(animation?: string);

        public gotoAndStop(animation?: string[]);

        public gotoAndStop(animation: string|string[] = this._animationName)
        {
            this.setAnimations(animation, 0, false);
            this.unregisterTickers();
        }

        public getAnimationLength(animation: string = this._animationName): number
        {
            const anim = this.nativeSpine.skeleton.data.findAnimation(animation);

            return anim ? anim.duration * 1000 : 0;
        }

        public getAnimationPosition(trackID: number): number
        {
            if (trackID < this.nativeSpine.state.tracks.length)
            {
                return this.nativeSpine.state.tracks[trackID].trackTime * 1000;
            }
            else
            {
                RS.Log.warn(`No animation is active on track ${trackID}`);
                return 0;
            }
        }

        public setPivotToBone(bone: string): void
        {
            const boneData = this.nativeSpine.skeleton.data.findBone(bone);
            const pos = this.getBonePosition(boneData);
            this.pivot.set(pos.x, pos.y);
        }

        protected setAnimations(animation: string|string[], time: number, loop: boolean)
        {
            this.nativeSpine.state.clearTracks();
            this.nativeSpine.skeleton.setBonesToSetupPose();
            this.nativeSpine.skeleton.setSlotsToSetupPose();

            if (RS.Is.array(animation))
            {
                this.setMultiple(animation, time, loop);
            }
            else
            {
                this._animationName = animation;
                this.nativeSpine.state.setAnimation(0, animation, loop);
                this.nativeSpine.state.tracks[0].trackTime = time;
            }
            this.nativeSpine.update(0);
        }

        protected getBonePosition(bone: PIXI.spine.core.BoneData) : RS.Math.Vector2D
        {
            let pos = {x: 0, y: 0};
            if (bone.parent)
            {
                pos = this.getBonePosition(bone.parent);
            }
            return {x: bone.x + pos.x, y: bone.y + pos.y};
        }

        protected registerTickers()
        {
            if (!this._playing)
            {
                this._playing = true;
                RS.Ticker.registerTickers(this);
            }
        }

        protected unregisterTickers()
        {
            if (this._playing)
            {
                this._playing = false;
                RS.Ticker.unregisterTickers(this);
            }
        }

        protected setMultiple(animations: string[], time: number, loop: boolean)
        {
            for(let track = 0; track < animations.length; ++track)
            {
                if (animations[track])
                {
                    this.nativeSpine.state.setAnimation(track, animations[track], loop);
                    this.nativeSpine.state.tracks[track].trackTime = time;
                }
            }
        }

        protected handleSpineEnded()
        {
            this.unregisterTickers();
        }

        protected handleSpineComplete()
        {
            if (this._playSettings)
            {
                if (this._playSettings.loop)
                {
                    this.onSpineRepeat.publish(this);
                    return;
                }
            }
            this.onSpineComplete.publish(this);
            this.unregisterTickers();
        }

        @RS.Tick({kind: RS.Ticker.Kind.Default})
        protected spineTicker(ev: RS.Ticker.Event)
        {
            this.nativeSpine.update(ev.delta / 1000);
            this.onSpineUpdate.publish({
                target: this,
                time: this.nativeSpine.state.tracks[0].trackTime
            });
        }

        /**
         * @deprecated
         */
        protected doPlay(repeatCount: number = 0, to: number = 0, speed: number = 1.0): void
        {
            this._playbackSpeed = speed;
            let duration: number = this._animation.duration;
            if (to > 0)
            {
                duration = to;
            }
            this._animationTime = (this._playbackSpeed < 0.0) ? duration : 0;

            this._tween = RS.Tween.get(this as Spine);
            this._tween.onPositionChanged(this.handleTweenUpdate);

            for (let i = 0; i <= repeatCount; i++)
            {
                this._tween.to({}, (duration - this._playOffset) * 1000);
                this._tween.call(() =>
                {
                    this.handleTweenComplete();
                });

                if (i > 0)
                {
                    this.handleTweenRepeat();
                }
            }

            this.handleTweenUpdate();
        }

        /**
         * Update the skeleton, animation state and time
         */
        protected update(dt: number): void
        {

            dt *= this._playbackSpeed;

            this._animation.apply(this.nativeSpine.skeleton, this._animationTime, this._animationTime + dt, false, null, 1, PIXI.spine.core.MixPose.setup, PIXI.spine.core.MixDirection.in);
            this.nativeSpine.skeleton.updateWorldTransform();
            this._animationTime += dt;
            const duration: number = this._animation.duration;
            while (this._animationTime < 0)
            {
                this._animationTime += duration;
            }
            while (this._animationTime > duration)
            {
                this._animationTime -= duration;
            }
        }
        /**
         * Callback for the tween
         *
         * Dispatches SpineEvent.ON_UPDATE
         */
        protected handleTweenUpdate(): void
        {
            this.update(this._tween.position - this._lastTime);
            //this.draw();
            //this.dispatchEvent(new SpineEvent(SpineEvent.ON_UPDATE, this, this._tween.time() + this._playOffset));
            this.onSpineUpdate.publish({
                target: this,
                time: this._tween.position / 1000 + this._playOffset
            });

            this._lastTime = this._tween.position;
        }
        /**
         * Dispatches SpineEvent.ON_COMPLETE every time the animation completes
         */
        protected handleTweenComplete(): void
        {
            //this.dispatchEvent(new SpineEvent(SpineEvent.ON_COMPLETE, this));
            this.onSpineComplete.publish(this);
        }

        /**
         * Dispatches SpineEvent.ON_REPEAT every time the animation repeats
         *
         * @protected
         */
        protected handleTweenRepeat(): void
        {
            //this.dispatchEvent(new SpineEvent(SpineEvent.ON_REPEAT, this));
            this.onSpineRepeat.publish(this);
        }

        private textureLoader(path: string, loader: (tex: PIXI.BaseTexture) => any): void
        {
            for (let i: number = 0; i < this.images.length; i++)
            {
                let imagePath = (this.images[i].source as any).src;

                // Strip query string
                const queryIndex = imagePath.indexOf("?");
                if (queryIndex !== -1)
                {
                    imagePath = imagePath.substr(0, queryIndex);
                }

                imagePath = Path.fileName(imagePath);

                console.log(imagePath, path);
                if (imagePath == path)
                {
                    loader((this.images[i] as Rendering.Pixi.Texture).baseTexture);
                    return;
                }
            }

            throw new Error(`Failed to load texture for path ${path} (no texture source path matched)`);
        }
    }
    RS.Rendering.Spine = Spine;

    /**
     * replacement renderWebGL for the spine sprite objects, this will render them using the old sprite rendering plugin.
     */
    PIXI.spine.SpineSprite.prototype["_renderWebGL"] = function(renderer: PIXI.WebGLRenderer)
    {
        this.calculateVertices();

        renderer.setObjectRenderer(renderer.plugins["oldsprite"]);
        renderer.plugins["oldsprite"].render(this);
    };

}
