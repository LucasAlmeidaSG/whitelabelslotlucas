/// <reference path="TextureBase.ts"/>
/** @internal  */
namespace RS.Rendering.Pixi
{
    export class Texture extends TextureBase implements ITexture
    {
        @AutoDispose public readonly isLoaded: Observable<boolean> = new Observable(false);

        public get source() { return this._nativeObject.source; }

        public constructor(source: any)
        {
            super();

            if (source == null)
            {
                this._nativeObject = new PIXI.BaseTexture();
            }
            else if (source instanceof PIXI.BaseTexture)
            {
                this._nativeObject = source;
            }
            else if (Is.string(source) || source instanceof HTMLImageElement || source instanceof HTMLCanvasElement)
            {
                this._nativeObject = PIXI.BaseTexture.from(source);
            }
            else
            {
                throw new Error(`Invalid texture source (${source})`);
            }

            if (this._nativeObject != null)
            {
                if (!this._nativeObject.hasLoaded)
                {
                    this._nativeObject.on("loaded", () => { this.isLoaded.value = true });
                }
                else
                {
                    this.isLoaded.value = true;
                }
            }
        }
    }

    RS.Rendering.Texture = Texture;
}