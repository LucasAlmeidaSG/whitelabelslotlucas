/** @internal  */
namespace RS.Rendering.Pixi
{
    export class TextureBase implements IDisposable
    {
        protected _nativeObject: PIXI.BaseTexture;

        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }

        /** @internal */
        public get baseTexture() { return this._nativeObject; }

        public get width() { return this._nativeObject.width; }
        public get height() { return this._nativeObject.height; }
        public get actualWidth() { return this._nativeObject.realWidth; }
        public get actualHeight() { return this._nativeObject.realHeight; }
        public get resolution() { return this._nativeObject.resolution; }

        /** Gets or sets the filter mode for the texture. */
        public get filterMode(): RS.Rendering.ITexture.FilterMode
        {
            switch (this._nativeObject.scaleMode)
            {
                case PIXI.SCALE_MODES.LINEAR:
                    return RS.Rendering.ITexture.FilterMode.Bilinear;
                case PIXI.SCALE_MODES.NEAREST:
                    return RS.Rendering.ITexture.FilterMode.NearestNeighbour;
            }
            throw new Error(`Unsupported PIXI scale mode '${this._nativeObject.scaleMode}'`);
        }
        public set filterMode(value: RS.Rendering.ITexture.FilterMode)
        {
            switch (value)
            {
                case RS.Rendering.ITexture.FilterMode.Bilinear:
                    this._nativeObject.scaleMode = PIXI.SCALE_MODES.LINEAR;
                    break;
                case RS.Rendering.ITexture.FilterMode.NearestNeighbour:
                    this._nativeObject.scaleMode = PIXI.SCALE_MODES.NEAREST;
                    break;
            }
            throw new Error(`Unsupported filter mode '${value}'`);
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._nativeObject.destroy();
            this._nativeObject = null;
            this._isDisposed = true;
        }

        /**
         * Uploads the texture to the rendering context on the specified stage if it hasn't already been uploaded.
         * @param stage
         */
        public warmup(stage: IStage): void
        {
            const renderer = (stage as Stage).renderer;
            if (!(renderer instanceof PIXI.WebGLRenderer)) { return; }
            if (this.hasContextTexture(renderer.CONTEXT_UID)) { return; }
            renderer.textureManager.updateTexture(this._nativeObject);
        }

        private hasContextTexture(uid: number): boolean
        {
            // Less than ideal, but PIXI doesn't expose this any other way
            const glTextures: typeof PIXI.BaseTexture.prototype["_glTextures"] = (this._nativeObject as any)._glTextures;
            if (!glTextures) { return false; }
            return (uid in glTextures);
        }
    }
}