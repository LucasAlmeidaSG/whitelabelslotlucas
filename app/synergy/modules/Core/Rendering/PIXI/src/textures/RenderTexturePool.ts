namespace RS.Rendering.Pixi
{
    /** Maximum memory usage of RTs to keep in the pool before disposing old ones. */
    const maxPixelCount = RS.Device.isMobileDevice
        // Allow up to 4 1024 x 1024 RTs on mobile, up to 4 2048 x 2048 on desktop
        ? 1024 * 1024 * 4
        : 2048 * 2048 * 4;

    /** Maximum number of RTs to keep in the pool before disposing old ones. */
    const maxTextureCount = RS.Device.isMobileDevice ? 32 : 64;

    /**
     * Encapsulates a pool of reusable render textures.
     */
    class RenderTexturePool implements IRenderTexturePool
    {
        private readonly _pool: IRenderTexture[] = [];

        private _pixelCount = 0;

        public get memoryUsage()
        {
            const colours = 4;
            const colourDepth = 8;
            return this._pixelCount * colours * colourDepth;
        }

        /**
         * Acquires a render texture from the pool
         * @param width
         * @param height
         * @param exact If false, may return a render texture which is larger than the requested size
         */
        public acquire(width: number, height: number, resolution: number, exact: boolean): IRenderTexture;
        public acquire(width: number, height: number, exact: boolean): IRenderTexture;
        public acquire(width: number, height: number, p2: number | boolean, exact?: boolean): IRenderTexture
        {
            let resolution: number = 1;
            if (Is.number(p2))
            {
                resolution = p2;
            }
            else
            {
                exact = p2;
            }

            // Find exact match
            for (let i = 0, l = this._pool.length; i < l; ++i)
            {
                const rt = this._pool[i];
                if (rt.width === width && rt.height === height && rt.resolution === resolution)
                {
                    this.removeFromPool(i);
                    return rt;
                }
            }
            if (exact)
            {
                if (this._pool.length > 0)
                {
                    const rt = this._pool.pop();
                    this._pixelCount -= this.getSize(rt);
                    rt.resize(width, height, resolution);
                    return rt;
                }

                return this.createRT(width, height, resolution, exact);
            }

            // Find best match (smallest pixel count)
            const candidates = this._pool.filter((rt) => rt.width >= width && rt.height >= height && rt.resolution >= resolution);
            if (candidates.length === 0) { return this.createRT(width, height, resolution, exact); }
            const best = Find.lowest(candidates, (rt) => rt.width * rt.height * rt.resolution);
            this.removeFromPool(this._pool.indexOf(best));
            return best;
        }

        /**
         * Releases a render texture to the pool.
         * @param renderTexture
         */
        public release(renderTexture: IRenderTexture): void
        {
            if (renderTexture.isDisposed) { return Log.warn("Can't release a disposed render texture!"); }
            this._pool.push(renderTexture);
            this._pixelCount += this.getSize(renderTexture);
            this.cleanPool();
        }

        private removeFromPool(index: number): void
        {
            const rt = this._pool[index];
            if (index < this._pool.length - 1)
            {
                this._pool[index] = this._pool[this._pool.length - 1];
            }
            --this._pool.length;
            this._pixelCount -= this.getSize(rt);
        }

        private createRT(width: number, height: number, resolution: number, exact: boolean): IRenderTexture
        {
            if (exact)
            {
                return new RenderTexture(width, height, resolution);
            }
            else
            {
                return new RenderTexture(Math.nextPowerOf2(width), Math.nextPowerOf2(height), resolution);
            }
        }

        /** Currently just returns the pixel count of the given RT. */
        private getSize(rt: IRenderTexture): number
        {
            return rt.actualWidth * rt.actualHeight;
        }

        private cleanPool(): void
        {
            // Sort pool so that larger RTs fall to the bottom
            this._pool.sort((a, b) => this.getSize(a) - this.getSize(b));

            // Remove items until we fall under max memory usage (delete biggest ones first)
            while (this._pixelCount > maxPixelCount)
            {
                const rt = this._pool.pop();
                this._pixelCount -= this.getSize(rt);
                rt.dispose();
            }

            // Remove items until we fall below the max pool size (delete smallest ones first)
            while (this._pool.length > maxTextureCount)
            {
                const rt = this._pool.shift();
                this._pixelCount -= this.getSize(rt);
                rt.dispose();
            }
        }
    }

    IRenderTexturePool.register(RenderTexturePool);
}