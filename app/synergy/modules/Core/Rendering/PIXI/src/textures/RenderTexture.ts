/// <reference path="TextureBase.ts"/>
/** @internal  */
namespace RS.Rendering.Pixi
{
    const tmpColorArr = [ 0, 0, 0, 0 ];
    export class RenderTexture extends TextureBase implements IRenderTexture
    {
        public get source() { return this._nativeObject; }

        protected _nativeObject: PIXI.BaseRenderTexture;
        protected _rt: PIXI.RenderTexture;

        protected _clearBeforeRender: boolean = false;

        public constructor(width: number, height: number, resolution: number = 1.0)
        {
            super();

            this._nativeObject = new PIXI.BaseRenderTexture(width, height, PIXI.SCALE_MODES.LINEAR, resolution);
            this._rt = new PIXI.RenderTexture(this._nativeObject);
        }

        /**
         * Clears the render texture.
         * @param stage
         * @param clearColor colour to clear with; defaults to a transparent colour
         */
        public clear(stage: Stage, clearColor: Util.Color = IRenderTexture.defaultClearColor): void
        {
            this.clearInternal(stage.renderer, clearColor);
        }

        /**
         * Clears the render texture via internal PIXI APIs.
         * @param renderer
         * @param clearColor colour to clear with; defaults to a transparent colour
         */
        public clearInternal(renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer, clearColor: Util.Color = IRenderTexture.defaultClearColor): void
        {
            if (renderer instanceof PIXI.WebGLRenderer)
            {
                renderer.bindRenderTexture(this._rt, undefined);
                [ tmpColorArr[0], tmpColorArr[1], tmpColorArr[2], tmpColorArr[3] ] = [ clearColor.r, clearColor.g, clearColor.b, clearColor.a ];
                renderer.clearRenderTexture(this._rt, tmpColorArr as any); // note: PIXI claims it wants a number but actually it wants an array
                this._clearBeforeRender = false;
            }
            else
            {
                this._clearBeforeRender = true;
            }
        }

        /**
         * Renders the specified container to the render texture.
         * @param stage
         * @param container
         */
        public render(stage: Stage, container: DisplayObjectContainer): void
        {
            if (container.prePostRenderTree)
            {
                PrePostRenderTree.preRender(container.prePostRenderTree, stage.renderer);
            }

            this.renderInternal(stage.renderer, container.nativeObject);
            this._clearBeforeRender = false;

            if (container.prePostRenderTree)
            {
                PrePostRenderTree.postRender(container.prePostRenderTree, stage.renderer);
            }
        }

        /**
         * Renders the specified container to the render texture via internal PIXI APIs.
         * @param renderer
         * @param container
         * @param transform
         */
        public renderInternal(renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer, container: PIXI.DisplayObject, transform?: PIXI.Transform): void
        {
            renderer.render(container, this._rt, this._clearBeforeRender, transform);
        }

        /**
         * Resizes the render texture to a certain size.
         * @param newWidth
         * @param newHeight
         * @param resolution
         */
        public resize(newWidth: number, newHeight: number, resolution: number = this.resolution): void
        {
            this._nativeObject.resolution = resolution;
            this._rt.resize(newWidth, newHeight);
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            this._rt.destroy(true);
            this._rt = null;
            super.dispose();
        }
    }

    RS.Rendering.RenderTexture = RenderTexture;

    // HACK: Fix crash in IE11 when creating new RT
    if (RS.Device.browser === RS.Browser.IE)
    {
        const oldUploadData = PIXI.glCore.GLTexture.prototype.uploadData;
        /**
         * Use a data source and uploads this texture to the GPU
         * @param data {TypedArray} the data to upload to the texture
         * @param width {number} the new width of the texture
         * @param height {number} the new height of the texture
         */
        PIXI.glCore.GLTexture.prototype.uploadData = function(data, width, height)
        {
            if (data == null)
            {
                this.width = 0;
            }
            try
            {
                oldUploadData.call(this, data, width, height);
            }
            catch (err)
            {
                RS.Log.error(err);
                debugger;
            }
        };
    }
}