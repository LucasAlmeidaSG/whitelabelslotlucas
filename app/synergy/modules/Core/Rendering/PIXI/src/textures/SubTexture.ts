/** @internal  */
namespace RS.Rendering.Pixi
{
    export class SubTexture implements ISubTexture
    {
        protected _isDisposed: boolean = false;
        protected _texture: Texture | RenderTexture;
        protected _frame: ReadonlyRectangle;
        protected _nativeObject: PIXI.Texture;
        protected _disposeTexture: boolean;

        public get isDisposed() { return this._isDisposed; }

        public get width() { return this._frame.w; }
        public get height() { return this._frame.h; }
        public get texture() { return this._texture; }

        public get frame() { return this._frame; }
        public set frame(value)
        {
            this._frame = value;
            this._nativeObject.frame = new PIXI.Rectangle(value.x, value.y, value.w, value.h);
        }

        /** @internal  */
        public get nativeObject() { return this._nativeObject; }

        public constructor(texture: Texture | RenderTexture, frame?: ReadonlyRectangle, disposeTexture: boolean = false)
        {
            this._texture = texture;
            if (!frame) { frame = new ReadonlyRectangle(0, 0, texture.width, texture.height); }
            this._frame = frame;
            this._nativeObject = new PIXI.Texture(texture.baseTexture, new PIXI.Rectangle(frame.x, frame.y, frame.w, frame.h));
            this._disposeTexture = disposeTexture;
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            if (this._disposeTexture) { this._texture.dispose(); }
            this._texture = null;
            this._frame = null;
            this._nativeObject.destroy(false);
            this._nativeObject = null;
            this._isDisposed = true;
        }
    }

    RS.Rendering.SubTexture = SubTexture;
}