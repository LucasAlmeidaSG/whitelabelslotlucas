#pragma meta private
uniform mat3 projectionMatrix;

#pragma meta private
uniform sampler2D uSampler;

attribute vec2 aVertexPosition;
attribute vec4 aColor;
attribute vec2 aTextureCoord;
attribute float aTextureId;