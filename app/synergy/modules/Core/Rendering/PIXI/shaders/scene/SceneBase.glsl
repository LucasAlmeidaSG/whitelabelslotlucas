#pragma meta property=vertexColors
#pragma switch VERTEX_COLORS

// View to screen matrix
uniform mat4 uProjectionMatrix;

// World to view matrix
uniform mat4 uViewMatrix;

// Object to world matrix
uniform mat4 uWorldMatrix;

attribute vec3 aPosition;
#pragma meta condition=VERTEX_COLORS
attribute vec4 aColor;
attribute vec2 aTexCoord;

#pragma meta condition=VERTEX_COLORS
varying vec4 vColor;
varying vec2 vTexCoord;

void SceneVertex()
{
    vec4 localPos = vec4(aPosition, 1.0);
    vec4 worldPos = uWorldMatrix * localPos;
    // note this division wouldn't normally be necessary but in some cases we have world matrices that rely on homogenous coords (e.g. plane projection)
    worldPos /= worldPos.w;
    vec4 viewPos = uViewMatrix * worldPos;
    gl_Position = uProjectionMatrix * viewPos;
#ifdef VERTEX_COLORS
    vColor = aColor;
#endif
    vTexCoord = aTexCoord;
}