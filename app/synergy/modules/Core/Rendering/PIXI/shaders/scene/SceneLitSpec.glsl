#include "scene/SceneBase.glsl"
#include "scene/Lighting.glsl"

#pragma meta property=masked
#pragma switch ALPHA_CUTOFF

#pragma meta property=ignoreAlpha
#pragma switch ALPHA_IGNORE

#pragma meta property=tangentSpaceNormals
#pragma switch TANGENT_SPACE_NORMALS

#pragma meta property=useNormalMap
#pragma switch NORMAL_MAP

#pragma meta property=useDiffuseMap
#pragma switch DIFFUSE_MAP

#pragma meta property=useSpecularMap
#pragma switch SPECULAR_MAP

attribute vec3 aNormal0; // Normal
#pragma meta condition=TANGENT_SPACE_NORMALS
attribute vec3 aNormal1; // Tangent
#pragma meta condition=TANGENT_SPACE_NORMALS
attribute vec3 aNormal2; // Binormal

// Diffuse texture map
#pragma meta condition=DIFFUSE_MAP
uniform sampler2D uDiffuse;

// Diffuse tint
#pragma meta color
uniform vec4 uDiffuseTint;

// Normal texture map
#pragma meta condition=NORMAL_MAP
uniform sampler2D uNormal;

// Specular texture map
#pragma meta condition=SPECULAR_MAP
uniform sampler2D uSpecular;

// Specular tint
uniform vec4 uSpecularTint;

varying vec3 vPosition;
varying vec3 vNormal;

#pragma meta condition=TANGENT_SPACE_NORMALS
varying vec3 vTangent;
#pragma meta condition=TANGENT_SPACE_NORMALS
varying vec3 vBinormal;

void SceneLitSpecVertex()
{
    vec4 localPos = vec4(aPosition, 1.0);
    vNormal = normalize(mat3(uWorldMatrix) * aNormal0);
#ifdef TANGENT_SPACE_NORMALS
    vTangent = normalize(mat3(uWorldMatrix) * aNormal1);
    vBinormal = normalize(mat3(uWorldMatrix) * aNormal2);
#endif
    vec4 worldPos = uWorldMatrix * localPos;
    vPosition = worldPos.xyz;
    vec4 viewPos = uViewMatrix * worldPos;
    gl_Position = uProjectionMatrix * viewPos;
#ifdef VERTEX_COLORS
    vColor = aColor;
#endif
    vTexCoord = aTexCoord;
}

void SceneLitSpecFragment()
{
    vec4 diffuse = vec4(uDiffuseTint.rgb, 1.0) * uDiffuseTint.a;
#ifdef VERTEX_COLORS
    diffuse *= vec4(vColor.rgb, 1.0) * vColor.a;
#endif
#ifdef DIFFUSE_MAP
    diffuse *= texture2D(uDiffuse, vTexCoord);
#endif
#ifdef ALPHA_IGNORE
    diffuse.a = 1.0;
#endif
#ifdef ALPHA_CUTOFF
    if (diffuse.a < 0.5)
    {
        diffuse.a = 0.0;
        discard;
    }
    else
    {
        diffuse.a = 1.0;
    }
#endif

#ifdef SPECULAR_MAP
    vec4 specular = texture2D(uSpecular, vTexCoord) * uSpecularTint;
#else
    vec4 specular = uSpecularTint;
#endif
    
    touch(uLightPositions);
    touch(uLightDirections);
    touch(uLightColors);

    touch(Light_Ambient);
    touch(Light_Reflection);
    touch(Light_Directional);
    touch(Light_Point);

#if defined(TANGENT_SPACE_NORMALS) && defined(NORMAL_MAP)
    mat3 tangentBasis = mat3(vTangent, vBinormal, vNormal);
    vec3 tangentSpaceNormal = (texture2D(uNormal, vTexCoord).xyz * 2.0) - 1.0;
    vec3 worldNormal = tangentBasis * tangentSpaceNormal;
#else
    vec3 worldNormal = vNormal;
#endif

    LightingData data;
    data.worldPosition = vPosition;
    data.worldNormal = worldNormal;
    data.diffuseColor = diffuse.rgb;
    data.specularColor = specular.rgb;
    data.specularPower = specular.a;

    vec3 c = vec3(0.0);
    
    #pragma LIGHTING

    c += Light_Reflection(data);
    
    // c = (data.worldNormal + 1.0) * 0.5;

    gl_FragColor = vec4(c, diffuse.a);
}

#pragma export SceneLitSpec Vertex=SceneLitSpecVertex Fragment=SceneLitSpecFragment dynamic