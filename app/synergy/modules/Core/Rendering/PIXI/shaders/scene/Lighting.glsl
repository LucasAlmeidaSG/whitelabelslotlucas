#pragma meta property=useAmbientMap
#pragma switch AMBIENT_MAP

#pragma meta property=useEnvironmentMap
#pragma switch ENV_MAP

#pragma meta property=specular
#pragma switch SPECULAR

struct LightingData
{
    vec3 worldPosition;
    vec3 worldNormal;
    vec3 diffuseColor;
    vec3 specularColor;
    float specularPower;
};

const int MAX_LIGHTS = 8;
uniform vec4 uLightPositions[MAX_LIGHTS];
uniform vec4 uLightDirections[MAX_LIGHTS];
uniform vec4 uLightColors[MAX_LIGHTS];

// Ambience map
#pragma meta condition=AMBIENT_MAP
uniform sampler2D uAmbience;

// Ambience tint
#pragma meta color
uniform vec4 uAmbienceTint;

// Environment map
#pragma meta condition=ENV_MAP
uniform sampler2D uEnvironment;

// Environment tint
#pragma meta color
uniform vec4 uEnvironmentTint;

uniform vec3 uViewPos;

const float PI = 3.141592653;
const float PI2 = PI * 2.0;

vec2 CalculateAmbienceUV(in vec3 normal)
{
    return vec2(atan(normal.z, normal.x) / PI2 + 0.5, normal.y * 0.5 + 0.5);
}

vec3 Light_Ambient(in LightingData data, in vec4 lightPos, in vec4 lightDir, in vec4 lightCol)
{
#ifdef AMBIENT_MAP
    vec3 ambience = texture2D(uAmbience, CalculateAmbienceUV(-data.worldNormal), -7.0).rgb * uAmbienceTint.rgb * uAmbienceTint.a;
    return ambience * lightCol.rgb * lightCol.a * data.diffuseColor;
#else
    return uAmbienceTint.rgb * uAmbienceTint.a * lightCol.rgb * lightCol.a * data.diffuseColor;
#endif
}

vec3 Light_Reflection(in LightingData data)
{
#ifdef ENV_MAP
    vec3 viewDir = normalize(data.worldPosition.xyz - uViewPos);
    vec3 reflectDir = reflect(-viewDir, data.worldNormal);
    vec3 envColor = texture2D(uEnvironment, CalculateAmbienceUV(reflectDir), -7.0).rgb * uEnvironmentTint.rgb * uEnvironmentTint.a;
    return envColor;
#else
    return vec3(0.0);
#endif
}

float CalculateSpecularFactor(in LightingData data, vec3 lightDirection)
{
    vec3 viewDir = normalize(data.worldPosition.xyz - uViewPos);
    vec3 halfDir = normalize(lightDirection + viewDir);
    float specAngle = max(-dot(halfDir, data.worldNormal.xyz), 0.0);
    return pow(specAngle, data.specularPower);
}

vec3 Light_Directional(in LightingData data, in vec4 lightPos, in vec4 lightDir, in vec4 lightCol)
{
    float lambertFactor = clamp(-dot(lightDir.xyz, data.worldNormal), 0.0, 1.0);
#ifdef SPECULAR
    float specularFactor = CalculateSpecularFactor(data, lightDir.xyz);
    return lightCol.rgb * (lambertFactor * data.diffuseColor + specularFactor * data.specularColor);
#else
    return lightCol.rgb * (lambertFactor * data.diffuseColor);
#endif
}

vec3 Light_Point(in LightingData data, in vec4 lightPos, in vec4 lightDir, in vec4 lightCol)
{
    vec3 dP = lightPos.xyz - data.worldPosition;
    float dist = length(dP);
    dP /= dist;
    float distFactor = 1.0 - clamp(dist / lightDir.w, 0.0, 1.0);
    float lambertFactor = clamp(dot(dP, data.worldNormal), 0.0, 1.0);
#ifdef SPECULAR
    float specularFactor = CalculateSpecularFactor(data, -dP);
    return lightCol.rgb * distFactor * (lambertFactor * data.diffuseColor + specularFactor * data.specularColor);
#else
    return lightCol.rgb * distFactor * (lambertFactor * data.diffuseColor);
#endif
}