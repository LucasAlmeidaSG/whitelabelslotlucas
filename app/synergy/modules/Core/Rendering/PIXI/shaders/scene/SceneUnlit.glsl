#include "scene/SceneBase.glsl"

#pragma meta property=masked
#pragma switch ALPHA_CUTOFF

#pragma meta property=ignoreAlpha
#pragma switch ALPHA_IGNORE

#pragma meta property=useDiffuseMap
#pragma switch DIFFUSE_MAP

// Diffuse texture map
#pragma meta condition=DIFFUSE_MAP
uniform sampler2D uDiffuse;

// Diffuse tint
#pragma meta color
uniform vec4 uDiffuseTint;

void SceneUnlitFragment()
{
    vec4 c = vec4(uDiffuseTint.rgb, 1.0) * uDiffuseTint.a;
#ifdef VERTEX_COLORS
    c *= vec4(vColor.rgb, 1.0) * vColor.a;
#endif
#ifdef DIFFUSE_MAP
    c *= texture2D(uDiffuse, vTexCoord);
#endif
#ifdef ALPHA_IGNORE
    c.a = 1.0;
#endif
#ifdef ALPHA_CUTOFF
    if (c.a < 0.5)
    {
        c.a = 0.0;
        discard;
    }
    else
    {
        c.a = 1.0;
    }
#endif
    gl_FragColor = c;
}

#pragma export SceneUnlit Vertex=SceneVertex Fragment=SceneUnlitFragment