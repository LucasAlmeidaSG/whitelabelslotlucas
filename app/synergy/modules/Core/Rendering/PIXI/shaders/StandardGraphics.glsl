attribute vec2 aVertexPosition;
attribute vec4 aColor;

#pragma meta private
uniform mat3 translationMatrix;
#pragma meta private
uniform mat3 projectionMatrix;
#pragma meta private
uniform float alpha;
#pragma meta private
uniform vec3 tint;
#pragma meta private
uniform vec3 origin;
#pragma meta private inputsizeop=multiply default=1.0
uniform vec3 uSize;

varying vec4 vColor;
varying vec2 vTextureCoord;

void StandardGraphicsVertexShader()
{
    vec3 worldPos = translationMatrix * vec3(aVertexPosition, 1.0);
    vTextureCoord = ((worldPos - origin) / uSize).xy;

    gl_Position = vec4((projectionMatrix * worldPos).xy, 0.0, 1.0);
    vColor = aColor * vec4(tint * alpha, alpha);
}