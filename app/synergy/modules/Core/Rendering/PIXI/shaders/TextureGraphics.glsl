precision lowp float;

#include "StandardGraphics.glsl"

// Toggles transparency on the texture
#pragma meta default="1"
#pragma switch TRANSPARENT_TO_WHITE

// Toggles if the texture should automatically scale to fit the text bounds
#pragma meta default="1"
#pragma switch SCALE_TO_FIT

// The texture containing the image
uniform sampler2D uImageTex;

// The section of the image we are rendering
uniform vec4 uImageRect;

// The size of the frame of the image being rendered, in relative 0-1 co-ords
uniform vec4 uRelativeSize;

// The position of the area of texture being rendered
#pragma meta default=0.0
uniform vec2 texUVPosition;

// The scale of the area of texture being rendered
#pragma meta default=1
uniform float scale;

void ImageTextureGraphicsFragment()
{
    // final colour to be outputted to gl_FragColor
    vec4 outputCol = vec4(0.0, 0.0, 0.0, 0.0);
    // coordinates of the texture to render, relative to the section of texture being rendered
    vec2 cropCoord = vec2(0.0, 0.0);

#ifdef SCALE_TO_FIT
    // render the texture relative to the section of the texture we've chosen to render
    cropCoord = vec2(uRelativeSize.x + vTextureCoord.x * uRelativeSize.w, uRelativeSize.y + vTextureCoord.y * uRelativeSize.z);
    outputCol = texture2D(uImageTex, cropCoord);
#else
    //convert the pixel sizes of the image and text to a relative size ranging from 0-1
    vec2 relSize = uImageRect.wz / uSize.xy;
    //position the texture coordinates to be relative to the image size and not the text size using the above
    //this ensures the image keeps its aspect ratio and isnt stretched to match the text bounds
    //could be considered like we're sampling a section of the image, with the text width and height being the bounds we're sampling with
    //also applies any custom user scaling here
    vec2 relCoords = mod(vTextureCoord / scale, relSize) / relSize;
    //reposition the section of texture we're rendering using the texUVPosition
    relCoords = mod(relCoords + texUVPosition, 1.0);
    //transmute the relcoords into the section of the texture we're rendering
    cropCoord = vec2(uRelativeSize.x + relCoords.x * uRelativeSize.w, uRelativeSize.y + relCoords.y * uRelativeSize.z);
    outputCol = texture2D(uImageTex, cropCoord);
#endif

#ifdef TRANSPARENT_TO_WHITE
    if(outputCol.a != 1.0)
    {
        outputCol.rgb = mix(vec3(1.0, 1.0, 1.0), outputCol.rgb, outputCol.a);
        outputCol.a = 1.0;
    }
#endif

    gl_FragColor = outputCol * vColor;
}

#pragma export ImageTextureGraphics Fragment=ImageTextureGraphicsFragment Vertex=StandardGraphicsVertexShader