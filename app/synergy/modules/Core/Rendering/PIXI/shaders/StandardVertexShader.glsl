#include "StandardAttributes.glsl"

void StandardVertexShader()
{
    gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
}