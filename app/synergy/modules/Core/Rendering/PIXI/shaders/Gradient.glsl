precision lowp float;

#pragma meta private
uniform sampler2D uSampler;

// How many times to repeat the gradient over the object
#pragma meta default=1.0
uniform float uRepeat;
// The texture containing the gradient
uniform sampler2D uGradientTex;
// The first color of the linear gradient
#pragma meta color
uniform vec4 uColorA;
// The second color of the linear gradient
#pragma meta color
uniform vec4 uColorB;
// The origin of the radial gradient (in texture coord space)
uniform vec2 uCenterPos;
// The direction of the gradient (normalised)
uniform vec2 uDirection;
// The radius of the radial gradient (in texture coord space)
#pragma meta default=1.0
uniform float uRadius;

varying vec2 vTextureCoord;

// Need to use vColor and vTextureId somehow or they won't be included in the built shader
varying vec4 vColor;
varying float vTextureId;

void TextureGradientFragment()
{
    vec2 repeatTextureCoord = mod(vTextureCoord * uRepeat, 1.0);
    vec4 inColor = texture2D(uSampler, vTextureCoord);
    vec4 outputCol = texture2D(uGradientTex, repeatTextureCoord);
    gl_FragColor = outputCol * inColor.a;

    vec4 c = vColor;
    float i = vTextureId;
}

void LinearGradientFragment()
{
    vec2 repeatTextureCoord = mod(vTextureCoord * uRepeat, 1.0);
    vec4 inColor = texture2D(uSampler, vTextureCoord);
    vec4 outputCol = mix(uColorA, uColorB, dot(repeatTextureCoord, uDirection));
    gl_FragColor = outputCol * inColor.a;

    vec4 c = vColor;
    float i = vTextureId;
}

void RadialGradientFragment()
{
    vec2 repeatTextureCoord = vec2(mod(vTextureCoord.x * uRepeat, 1.0), mod(vTextureCoord.y * uRepeat, 1.0));
    vec4 inColor = texture2D(uSampler, vTextureCoord);
    float distanceFromCenter = length(repeatTextureCoord - uCenterPos);
    vec4 outputCol = mix(uColorA, uColorB, clamp(distanceFromCenter / uRadius, 0.0, 1.0));
    gl_FragColor = outputCol * inColor.a;

    vec4 c = vColor;
    float i = vTextureId;
}

#pragma export TextureGradient Fragment=TextureGradientFragment
#pragma export LinearGradient Fragment=LinearGradientFragment
#pragma export RadialGradient Fragment=RadialGradientFragment