namespace RS.Rendering.Pixi.Tests.Core
{
    export function dimensions<T extends IDisplayObject>(fn: () => T, minW: number, maxW: number, minH: number, maxH: number, x = 0, y = 0)
    {
        return describe("Dimensions:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("width", function ()
            {
                it("should initialise between " + minW + " & " + maxW, function ()
                {
                    chai.expect(disp.width).to.be.within(minW, maxW);
                });

                it("should set a value", function ()
                {
                    disp.width = 123;
                    chai.expect(disp.width).to.equal(123);
                });

                it("should be affected by scaleManager", function ()
                {
                    RS.Rendering.ScaleManager.setPositionScale(200);
                    disp.width = 123;
                    chai.expect(disp.width).to.equal(123);
                });
            });

            describe("height", function ()
            {
                it("should initialise between " + minH + " & " + maxH, function ()
                {
                    chai.expect(disp.height).to.be.within(minH, maxH);
                });

                it("should set a value", function ()
                {
                    disp.height = 123;
                    chai.expect(disp.height).to.equal(123);
                });

                it("should be affected by scaleManager", function ()
                {
                    RS.Rendering.ScaleManager.setPositionScale(200);
                    disp.height = 123;
                    chai.expect(disp.height).to.equal(123);
                });
            });

            describe("bounds", function ()
            {
                it("should initialise h between " + minH + " & " + maxH + " and w between " + minW + " & " + maxW, function ()
                {
                    chai.expect(disp.bounds.x, "x correct").to.equal(x);
                    chai.expect(disp.bounds.y, "y correct").to.equal(y);
                    chai.expect(disp.bounds.w).to.be.within(minW, maxW);
                    chai.expect(disp.bounds.h).to.be.within(minH, maxH);
                });

                it("should change when height does", function ()
                {
                    disp.height = 123;
                    chai.expect(disp.bounds.x).to.equal(x);
                    chai.expect(disp.bounds.y).to.equal(y * 123);
                    chai.expect(disp.bounds.h).to.equal(123);
                });

                it("should change when width does", function ()
                {
                    disp.width = 123;
                    chai.expect(disp.bounds.x).to.equal(x * 123);
                    chai.expect(disp.bounds.y).to.equal(y);
                    chai.expect(disp.bounds.w).to.equal(123);
                });

                it("should be affected by scaleManager", function ()
                {
                    RS.Rendering.ScaleManager.setPositionScale(200);
                    disp.height = 123;
                    chai.expect(disp.bounds.h).to.equal(123);
                    disp.width = 123;
                    chai.expect(disp.bounds.w).to.equal(123);
                });

                it("should move relative to parent", function ()
                {
                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.x = 100;
                    container.y = 100;

                    chai.expect(disp.bounds.x).to.equal(x + 100);
                    chai.expect(disp.bounds.y).to.equal(y + 100);
                    disp.height = 123;
                    chai.expect(disp.bounds.h).to.equal(123);
                    disp.width = 123;
                    chai.expect(disp.bounds.w).to.equal(123);
                });
            });

            describe("localBounds", function ()
            {
                it("should initialise between " + minH + " & " + maxH, function ()
                {
                    chai.expect(disp.localBounds.x, "x correct").to.equal(x);
                    chai.expect(disp.localBounds.y, "y correct").to.equal(y);
                    chai.expect(disp.localBounds.w).to.be.within(minW, maxW);
                    chai.expect(disp.localBounds.h).to.be.within(minH, maxH);
                });

                it("should not change when height does", function ()
                {
                    const oldLB = disp.localBounds.clone();
                    disp.height = 123;
                    chai.expect(disp.localBounds.x).to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y).to.equal(oldLB.y);
                    chai.expect(disp.localBounds.h).to.equal(oldLB.h);
                });

                it("should not change when width does", function ()
                {
                    const oldLB = disp.localBounds.clone();
                    disp.width = 123;
                    chai.expect(disp.localBounds.x).to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y).to.equal(oldLB.y);
                    chai.expect(disp.localBounds.w).to.equal(oldLB.w);
                });

                it("should not change when rotation does", function ()
                {
                    const oldLB = disp.localBounds.clone();
                    disp.rotation = RS.Math.Angles.eighthCircle;
                    chai.expect(disp.localBounds.x, "x unchanged").to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y, "y unchanged").to.equal(oldLB.y);
                    chai.expect(disp.localBounds.w, "w unchanged").to.equal(oldLB.w);
                    chai.expect(disp.localBounds.h, "h unchanged").to.equal(oldLB.h);
                });

                it("should not change when skew does", function ()
                {
                    const oldLB = disp.localBounds.clone();
                    disp.skew.set(2, 0.25);
                    chai.expect(disp.localBounds.x, "x unchanged").to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y, "y unchanged").to.equal(oldLB.y);
                    chai.expect(disp.localBounds.w, "w unchanged").to.equal(oldLB.w);
                    chai.expect(disp.localBounds.h, "h unchanged").to.equal(oldLB.h);
                });

                it("should not be affected by scaleManager", function ()
                {
                    const oldLB = disp.localBounds.clone();
                    RS.Rendering.ScaleManager.setPositionScale(200);
                    disp.height = 123;
                    chai.expect(disp.localBounds.h).to.equal(oldLB.h);
                    disp.width = 123;
                    chai.expect(disp.localBounds.w).to.equal(oldLB.w);
                });

                it("should not move relative to parent", function ()
                {
                    const oldLB = disp.localBounds.clone();

                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.x = 100;
                    container.y = 100;

                    chai.expect(disp.localBounds.x).to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y).to.equal(oldLB.y);
                    disp.height = 123;
                    disp.width = 123;
                    chai.expect(disp.localBounds.w).to.be.within(minW, maxW);
                    chai.expect(disp.localBounds.h).to.be.within(minH, maxH);
                });

                it("should not move when parent height changes", function ()
                {
                    const oldLB = disp.localBounds.clone();

                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.height = 123;

                    chai.expect(disp.localBounds.x, "x unchanged").to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y, "y unchanged").to.equal(oldLB.y);
                });

                it("should not move when parent width changes", function ()
                {
                    const oldLB = disp.localBounds.clone();

                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.width = 123;
                    
                    chai.expect(disp.localBounds.x, "x unchanged").to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y, "y unchanged").to.equal(oldLB.y);
                });

                it("should not change when parent rotation does", function ()
                {
                    const oldLB = disp.localBounds.clone();

                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.rotation = RS.Math.Angles.eighthCircle;

                    chai.expect(disp.localBounds.x, "x unchanged").to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y, "y unchanged").to.equal(oldLB.y);
                    chai.expect(disp.localBounds.w, "w unchanged").to.equal(oldLB.w);
                    chai.expect(disp.localBounds.h, "h unchanged").to.equal(oldLB.h);
                });

                it("should not change when parent skew does", function ()
                {
                    const oldLB = disp.localBounds.clone();

                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.skew.set(2, 0.25);

                    chai.expect(disp.localBounds.x, "x unchanged").to.equal(oldLB.x);
                    chai.expect(disp.localBounds.y, "y unchanged").to.equal(oldLB.y);
                    chai.expect(disp.localBounds.w, "w unchanged").to.equal(oldLB.w);
                    chai.expect(disp.localBounds.h, "h unchanged").to.equal(oldLB.h);
                });
            });
        });
    }
}