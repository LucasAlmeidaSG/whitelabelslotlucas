namespace RS.Rendering.Pixi.Tests.Core
{
    export function misc<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Misc:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("name", function ()
            {
                it("should initialise to null", function ()
                {
                    chai.expect(disp.name).to.equal(null)
                });

                it("should set a value", function ()
                {
                    disp.name = "test";
                    chai.expect(disp.name).to.equal("test");
                });
            });

            describe("hitArea", function ()
            {
                it("should initialise to null", function ()
                {
                    chai.expect(disp.hitArea).to.equal(null)
                });

                it("should set a value", function ()
                {
                    const rect = new RS.Rendering.Rectangle(0, 0, 100, 100)
                    disp.hitArea = rect;
                    chai.expect(RS.Rendering.Rectangle.equals(disp.hitArea, rect)).to.equal(true);

                    disp.hitArea = null;
                    chai.expect(disp.hitArea).to.equal(null);
                });
            });
        });
    }
}