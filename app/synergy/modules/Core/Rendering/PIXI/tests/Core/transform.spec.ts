namespace RS.Rendering.Pixi.Tests.Core
{
    export function transform<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Transform:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });
            
            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("setTransform", function ()
            {
                it("should set position", function ()
                {
                    disp.setTransform(100, 200);
                    chai.expect(disp.x).to.equal(100);
                    chai.expect(disp.y).to.equal(200);

                    chai.expect(disp.scaleX).to.equal(1);
                    chai.expect(disp.scaleY).to.equal(1);

                    chai.expect(disp.rotation).to.equal(0);

                    chai.expect(disp.skew.x).to.equal(0);
                    chai.expect(disp.skew.y).to.equal(0);

                    chai.expect(disp.pivot.x).to.equal(0);
                    chai.expect(disp.pivot.y).to.equal(0);
                });

                it("should set scale", function ()
                {
                    disp.setTransform(0, 0, 5, -5);
                    chai.expect(disp.x).to.equal(0);
                    chai.expect(disp.y).to.equal(0);

                    chai.expect(disp.scaleX).to.equal(5);
                    chai.expect(disp.scaleY).to.equal(-5);

                    chai.expect(disp.rotation).to.equal(0);

                    chai.expect(disp.skew.x).to.equal(0);
                    chai.expect(disp.skew.y).to.equal(0);

                    chai.expect(disp.pivot.x).to.equal(0);
                    chai.expect(disp.pivot.y).to.equal(0);
                });

                it("should set rotation", function ()
                {
                    disp.setTransform(0, 0, 1, 1, 90);
                    chai.expect(disp.x).to.equal(0);
                    chai.expect(disp.y).to.equal(0);

                    chai.expect(disp.scaleX).to.equal(1);
                    chai.expect(disp.scaleY).to.equal(1);

                    chai.expect(disp.rotation).to.equal(90);

                    chai.expect(disp.skew.x).to.equal(0);
                    chai.expect(disp.skew.y).to.equal(0);

                    chai.expect(disp.pivot.x).to.equal(0);
                    chai.expect(disp.pivot.y).to.equal(0);
                });

                it("should set skew", function ()
                {
                    disp.setTransform(0, 0, 1, 1, 0, 3, -3);
                    chai.expect(disp.x).to.equal(0);
                    chai.expect(disp.y).to.equal(0);

                    chai.expect(disp.scaleX).to.equal(1);
                    chai.expect(disp.scaleY).to.equal(1);

                    chai.expect(disp.rotation).to.equal(0);

                    chai.expect(disp.skew.x).to.equal(3);
                    chai.expect(disp.skew.y).to.equal(-3);

                    chai.expect(disp.pivot.x).to.equal(0);
                    chai.expect(disp.pivot.y).to.equal(0);
                });

                it("should set pivot", function ()
                {
                    disp.setTransform(0, 0, 1, 1, 0, 0, 0, 4, -4);
                    chai.expect(disp.x).to.equal(0);
                    chai.expect(disp.y).to.equal(0);

                    chai.expect(disp.scaleX).to.equal(1);
                    chai.expect(disp.scaleY).to.equal(1);

                    chai.expect(disp.rotation).to.equal(0);

                    chai.expect(disp.skew.x).to.equal(0);
                    chai.expect(disp.skew.y).to.equal(0);

                    chai.expect(disp.pivot.x).to.equal(4);
                    chai.expect(disp.pivot.y).to.equal(-4);
                });

                it("should set all", function ()
                {
                    disp.setTransform(100, 200, 2, -2, 90, 3, -3, 4, -4);
                    chai.expect(disp.x).to.equal(100);
                    chai.expect(disp.y).to.equal(200);

                    chai.expect(disp.scaleX).to.equal(2);
                    chai.expect(disp.scaleY).to.equal(-2);

                    chai.expect(disp.rotation).to.equal(90);

                    chai.expect(disp.skew.x).to.equal(3);
                    chai.expect(disp.skew.y).to.equal(-3);

                    chai.expect(disp.pivot.x).to.equal(4);
                    chai.expect(disp.pivot.y).to.equal(-4);
                });
            });
        });
    }
}