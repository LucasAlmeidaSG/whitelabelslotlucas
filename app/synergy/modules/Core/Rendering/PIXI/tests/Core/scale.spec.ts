namespace RS.Rendering.Pixi.Tests.Core
{
    export function scale<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Scale:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("scaleX", function ()
            {
                it("should initialise to 1", function ()
                {
                    chai.expect(disp.scaleX).to.equal(1);
                });

                it("should set a value", function ()
                {
                    disp.scaleX = 0.78;
                    chai.expect(disp.scaleX).to.equal(0.78);
                });
            });

            describe("scaleY", function ()
            {
                it("should initialise to 1", function ()
                {
                    chai.expect(disp.scaleY).to.equal(1);
                });

                it("should set a value", function ()
                {
                    disp.scaleY = 0.78;
                    chai.expect(disp.scaleY).to.equal(0.78);
                });
            });

            describe("worldScaleX", function ()
            {
                it("should return the scale for objects with no parent on the world x-axis", function ()
                {
                    disp.scaleX = 0.47;
                    chai.expect(disp.worldScaleX).to.equal(0.47);
                });

                it("should return the total scale for objects with a parent on the world x-axis", function ()
                {
                    disp.scaleX = 0.48;
                    
                    const container = new RS.Rendering.Container("container");
                    container.addChild(disp);
                    container.scaleX = 0.5;

                    chai.expect(disp.worldScaleX).to.equal(0.24);
                });

                it("should return the total scale for rotated objects with a parent on the world x-axis", function ()
                {
                    disp.scaleX = 0.48;
                    disp.rotation = RS.Math.Angles.quarterCircle;
                    
                    const container = new RS.Rendering.Container("container");
                    container.addChild(disp);
                    container.scaleY = 0.5;

                    chai.expect(disp.worldScaleX).to.equal(0.48);
                });
            });

            describe("worldScaleY", function ()
            {
                it("should return the scale for objects with no parent on the world y-axis", function ()
                {
                    disp.scaleY = 0.47;
                    chai.expect(disp.worldScaleY).to.equal(0.47);
                });

                it("should return the total scale for objects with a parent on the world y-axis", function ()
                {
                    disp.scaleY = 0.48;
                    
                    const container = new RS.Rendering.Container("container");
                    container.addChild(disp);
                    container.scaleY = 0.5;

                    chai.expect(disp.worldScaleY).to.equal(0.24);
                });

                it("should return the total scale for rotated objects with a parent on the world y-axis", function ()
                {
                    disp.scaleY = 0.48;
                    disp.rotation = RS.Math.Angles.quarterCircle;
                    
                    const container = new RS.Rendering.Container("container");
                    container.addChild(disp);
                    container.scaleX = 0.5;

                    chai.expect(disp.worldScaleY).to.equal(0.48);
                });
            });
        });
    }
}