namespace RS.Rendering.Pixi.Tests.Core
{
    export function children<T extends IDisplayObject>(fn: () => T, isContainer: boolean)
    {
        return describe("Children:", function ()
        {
            let stage: IStage;
            let disp: IDisplayObject;
            before(function ()
            {
                stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                stage.autoRender = false;
            });

            after(function ()
            {
                stage.dispose();
                stage = null;
            });

            beforeEach(function ()
            {
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("children", function ()
            {
                it("should exist if added to display", function ()
                {
                    stage.addChild(disp);
                    chai.expect(disp.parent).to.equal(stage);
                });

                it("should not exist if removed from display", function ()
                {
                    stage.addChild(disp);
                    stage.removeChild(disp);
                    chai.expect(disp.parent).to.equal(null);
                });

                it("should not exist if disposed of", function ()
                {
                    stage.addChild(disp);
                    disp.dispose();
                    chai.expect(disp.parent, "parent is null").to.equal(null);
                    chai.expect(stage.children.length, "parent has no children").to.equal(0);
                });
            });

            describe("parent", function ()
            {
                it("should start as null", function ()
                {
                    chai.expect(disp.parent).to.equal(null);
                });

                it("should exist if added to display", function ()
                {
                    stage.addChild(disp);
                    chai.expect(disp.parent).to.equal(stage);
                });

                it("should not exist if removed from display", function ()
                {
                    stage.addChild(disp);
                    stage.removeChild(disp);
                    chai.expect(disp.parent).to.equal(null);
                });
            });

            describe("rootParent", function ()
            {
                it("should start as null", function ()
                {
                    chai.expect(disp.rootParent).to.equal(null);
                });

                it("should exist if added to container", function ()
                {
                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    chai.expect(disp.rootParent).to.equal(container);
                });

                it("should not exist if removed from container", function ()
                {
                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    container.removeChild(disp);
                    chai.expect(disp.rootParent).to.equal(null);
                });

                it("should be stage if added to stage", function ()
                {
                    stage.addChild(disp);
                    chai.expect(disp.rootParent).to.equal(stage);
                });

                it("should not exist if removed from stage", function ()
                {
                    stage.addChild(disp);
                    stage.removeChild(disp);
                    chai.expect(disp.rootParent).to.equal(null);
                });

                it("should be stage if added to container on stage", function ()
                {
                    const container = new RS.Rendering.DisplayObjectContainer();
                    stage.addChild(container);
                    container.addChild(disp);
                    chai.expect(disp.rootParent).to.equal(stage);
                });

                it("should not exist if removed from container on stage", function ()
                {
                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    chai.expect(disp.rootParent).to.equal(container);
                    stage.addChild(container);
                    chai.expect(disp.rootParent).to.equal(stage);
                    container.removeChild(disp);
                    chai.expect(disp.rootParent).to.equal(null);
                });
            });

            describe("stage", function ()
            {
                it("should start as undefined", function ()
                {
                    chai.expect(disp.stage).to.equal(undefined);
                });

                it("should exist if added to display", function ()
                {
                    stage.addChild(disp);
                    chai.expect(disp.stage).to.equal(stage);
                });

                it("should not exist if removed from display", function ()
                {
                    stage.addChild(disp);
                    stage.removeChild(disp);
                    chai.expect(disp.stage).to.equal(null);
                });
            });
            if (isContainer)
            {
                let container: IContainer;
                beforeEach(function ()
                {
                    container = disp as any as IContainer;
                });

                afterEach(function ()
                {
                    container = null;
                });

                describe("children", function ()
                {
                    it("should start as an empty array", function ()
                    {
                        chai.expect(container.children.length).to.equal(0);
                    });

                    it("not be settable", function ()
                    {
                        (container as any).children = null;
                        chai.expect(container.children.length).to.equal(0);
                    });
                });

                describe("getChildByName", function ()
                {
                    it("should return a child of the given name", function ()
                    {
                        const childA = new RS.Rendering.DisplayObjectContainer("MockChildA");
                        const childB = new RS.Rendering.DisplayObjectContainer("MockChildB");
                        container.addChildren(childA, childB);
                        chai.expect(container.getChildByName("MockChildA")).to.equal(childA);
                        chai.expect(container.getChildByName("MockChildB")).to.equal(childB);
                    });

                    it("should not search child containers if deep is false or unspecified", function ()
                    {
                        const childA = new RS.Rendering.DisplayObjectContainer("MockChildA");
                        const childB = new RS.Rendering.DisplayObjectContainer("MockChildB");
                        const childAA = new RS.Rendering.DisplayObjectContainer("MockChildAA");
                        childA.addChild(childAA);
                        container.addChildren(childA, childB);
                        chai.expect(container.getChildByName("MockChildAA", false), "did not find child in child container").to.equal(null);
                    });

                    it("should search child containers if no child is found in the root container and deep is true", function ()
                    {
                        const childA = new RS.Rendering.DisplayObjectContainer("MockChildA");
                        const childB = new RS.Rendering.DisplayObjectContainer("MockChildB");
                        const childAA = new RS.Rendering.DisplayObjectContainer("MockChildAA");
                        childA.addChild(childAA);
                        container.addChildren(childA, childB);
                        chai.expect(container.getChildByName("MockChildAA", true), "found child in child container").to.equal(childAA);
                    });

                    it("should return a child at a higher level before one at a lower level if deep is true", function ()
                    {
                        const childA = new RS.Rendering.DisplayObjectContainer("MockChildA");
                        const childB = new RS.Rendering.DisplayObjectContainer("MockChildB");
                        const goodChild = new RS.Rendering.DisplayObjectContainer("MockChildTarget");
                        const badChild = new RS.Rendering.DisplayObjectContainer("MockChildTarget");
                        childA.addChild(badChild);
                        container.addChildren(childA, goodChild, childB);
                        chai.expect(container.getChildByName("MockChildTarget", true), "returned higher level child").to.equal(goodChild);
                    });

                    it("should return null if no child was found", function ()
                    {
                        const childA = new RS.Rendering.DisplayObjectContainer("MockChildA");
                        const childB = new RS.Rendering.DisplayObjectContainer("MockChildB");
                        container.addChildren(childA, childB);
                        chai.expect(container.getChildByName("MockChildZ")).to.equal(null);
                    });
                });
            }
        });
    }
}