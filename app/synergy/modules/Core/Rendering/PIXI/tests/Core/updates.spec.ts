namespace RS.Rendering.Pixi.Tests.Core
{
    export function updates<T extends IDisplayObject>(fn: () => T, defaultAlwaysTick = false)
    {
        return describe("Updates:", function ()
        {
            describe("onUpdate", function ()
            {
                let stage: IStage;
                let disp: T;
                before(function ()
                {
                    stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                    stage.autoRender = false;
                });

                after(function ()
                {
                    stage.dispose();
                    stage = null;
                });

                beforeEach(function ()
                {
                    disp = fn();
                    RS.Rendering.ScaleManager.setPositionScale(1);
                });

                afterEach(function ()
                {
                    disp.dispose();
                    disp = null;
                });

                it("should be called when rendered when alwaysTick is true", function ()
                {
                    const spy = sinon.spy(disp as any, "onUpdate");

                    (disp as any).alwaysTick = true;

                    stage.addChild(disp);
                    stage.render();

                    RS.Tests.simulateTime(100);

                    chai.expect(spy.called).to.equal(true);
                    spy.restore();
                });

                it("should not be called when rendered when alwaysTick is false", function ()
                {
                    const spy = sinon.spy(disp as any, "onUpdate");

                    (disp as any).alwaysTick = false;

                    stage.addChild(disp);
                    stage.render();

                    RS.Tests.simulateTime(100);

                    chai.expect(spy.called).to.equal(false);
                    spy.restore();
                });

                it("should publish onUpdated when alwaysTick is true", function ()
                {
                    const spy = sinon.spy(disp.onUpdated, "publish");

                    (disp as any).alwaysTick = true;

                    stage.addChild(disp);
                    stage.render();

                    disp.onUpdated(() => { return; });

                    RS.Tests.simulateTime(100);

                    chai.expect(spy.called).to.equal(true);
                    chai.expect(spy.getCall(0).args[0]).to.equal(disp);
                    spy.restore();
                });

                // If alwaysTick defaults to true on the object, onUpdated should be called irrespective of whether an event listener was attached or not
                if (defaultAlwaysTick)
                {
                    it("should publish onUpdated when no event listener is attached", function ()
                    {
                        const spy = sinon.spy(disp.onUpdated, "publish");

                        stage.addChild(disp);
                        stage.render();

                        RS.Tests.simulateTime(100);

                        chai.expect(spy.called).to.equal(true);
                        chai.expect(spy.getCall(0).args[0]).to.equal(disp);
                        spy.restore();
                    });
                }
                else
                {
                    it("should not publish onUpdated when no event listener is attached", function ()
                    {
                        const spy = sinon.spy(disp.onUpdated, "publish");

                        stage.addChild(disp);
                        stage.render();

                        RS.Tests.simulateTime(100);

                        chai.expect(spy.called).to.equal(false);
                        spy.restore();
                    });
                }
            });

            describe("onParentChanged", function ()
            {
                let disp: IDisplayObject;
                beforeEach(function ()
                {
                    disp = fn();
                });

                it("should be called when added to container", function ()
                {
                    const spy = sinon.spy(disp.onParentChanged, "publish");
                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);

                    RS.Tests.simulateTime(100);

                    chai.expect(spy.called).to.equal(true);
                    chai.expect(spy.getCall(0).args[0]).to.equal(container);
                    spy.restore();
                });

                it("should not be called when added to same container twice", function ()
                {
                    const container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);
                    const spy = sinon.spy(disp.onParentChanged, "publish");
                    container.addChild(disp);

                    RS.Tests.simulateTime(100);

                    chai.expect(spy.called).to.equal(false);
                    spy.restore();
                });

                it("should be called when added to second container", function ()
                {
                    let container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);

                    const spy = sinon.spy(disp.onParentChanged, "publish");

                    container = new RS.Rendering.DisplayObjectContainer();
                    container.addChild(disp);

                    RS.Tests.simulateTime(100);

                    chai.expect(spy.called).to.equal(true);
                    chai.expect(spy.getCall(0).args[0]).to.equal(container);
                    spy.restore();
                });
            });
        });
    }
}