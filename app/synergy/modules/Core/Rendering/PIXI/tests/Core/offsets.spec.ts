namespace RS.Rendering.Pixi.Tests.Core
{
    export function offsets<T extends IDisplayObject>(fn: () => T, pivotDisabled: boolean)
    {
        return describe("Offsets:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("skew", function ()
            {
                it("should initialise x to 0", function ()
                {
                    chai.expect(disp.skew.x).to.equal(0)
                });

                it("should initialise y to 0", function ()
                {
                    chai.expect(disp.skew.y).to.equal(0)
                });
            });

            describe("pivot", function ()
            {
                it("should initialise x to 0", function ()
                {
                    chai.expect(disp.pivot.x).to.equal(0)
                });

                it("should initialise y to 0", function ()
                {
                    chai.expect(disp.pivot.y).to.equal(0)
                });

                it("should not be affected by ScaleManager", function ()
                {
                    if (!pivotDisabled)
                    {
                        disp.pivot.set(20, 30);
                        chai.expect(Math.round(disp.pivot.x)).to.equal(20);
                        chai.expect(Math.round(disp.pivot.y)).to.equal(30);

                        RS.Rendering.ScaleManager.setPositionScale(0.7878);
                        disp.pivot.set(20, 30);
                        chai.expect(Math.round(disp.pivot.x)).to.equal(20);
                        chai.expect(Math.round(disp.pivot.y)).to.equal(30);

                        RS.Rendering.ScaleManager.setPositionScale(2);
                        disp.pivot.set(20, 30);
                        chai.expect(Math.round(disp.pivot.x)).to.equal(20);
                        chai.expect(Math.round(disp.pivot.y)).to.equal(30);
                    }
                    else
                    {
                        RS.Rendering.ScaleManager.setPositionScale(0.7878);
                        disp.pivot.set(20, 30);
                        chai.expect(Math.round(disp.pivot.x)).to.equal(0);
                        chai.expect(Math.round(disp.pivot.y)).to.equal(0);
                    }
                });
            })
        });
    }
}