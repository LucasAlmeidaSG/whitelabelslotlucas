namespace RS.Rendering.Pixi.Tests.Core
{
    export function interactivity<T extends IDisplayObject>(fn: () => T, defaultInteractive: boolean)
    {
        return describe("Interactivity:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("buttonMode", function ()
            {
                it("should initialise to false", function ()
                {
                    chai.expect(disp.buttonMode).to.equal(false);
                });

                it("should set a value", function ()
                {
                    disp.buttonMode = true;
                    chai.expect(disp.buttonMode).to.equal(true);
                });
            });

            describe("interactive", function ()
            {
                it("should initialise to " + defaultInteractive, function ()
                {
                    chai.expect(disp.interactive).to.equal(defaultInteractive);
                });

                it("should become true when an interaction event handler is added", function ()
                {
                    const hdl = disp.onClicked(() => { return; });
                    chai.expect(disp.interactive).to.equal(true);
                    hdl.dispose();
                    chai.expect(disp.interactive).to.equal(defaultInteractive);
                });

                it("should ignore interaction event handlers when explicitly set", function ()
                {
                    disp.interactive = false;
                    const hdl = disp.onClicked(() => { return; });
                    chai.expect(disp.interactive).to.equal(false);
                    hdl.dispose();
                    chai.expect(disp.interactive).to.equal(false);
                });

                it("should set a value", function ()
                {
                    disp.interactive = !defaultInteractive;
                    chai.expect(disp.interactive).to.equal(!defaultInteractive);
                    disp.interactive = defaultInteractive;
                    chai.expect(disp.interactive).to.equal(defaultInteractive);
                });
            });

            describe("cursor", function ()
            {
                it("should initialise to Cursor.Default", function ()
                {
                    chai.expect(disp.cursor).to.equal(RS.Rendering.Cursor.Default)
                });

                it("should set a value", function ()
                {
                    disp.cursor = RS.Rendering.Cursor.Pointer;
                    chai.expect(disp.cursor).to.equal(RS.Rendering.Cursor.Pointer);
                });
            });
        });
    }
}