namespace RS.Rendering.Pixi.Tests.Core
{
    export function displayList<T extends IDisplayObject>(fn: () => T, isContainer: boolean)
    {
        return describe("Display List:", function ()
        {
            let disp: T, stage: IStage;
            before(function ()
            {
                stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                stage.autoRender = false;
            });

            after(function ()
            {
                stage.dispose();
                stage = null;
            });
            
            beforeEach(function ()
            {
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            if (isContainer)
            {
                let container: IContainer;
                beforeEach(function ()
                {
                    container = disp as any as IContainer;
                });

                afterEach(function ()
                {
                    container = null;
                });

                describe("onChildAdded", function ()
                {
                    it("should be dispatched on addChild", function ()
                    {
                        const spy = sinon.spy(container.onChildAdded, "publish");

                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);

                        chai.expect(spy.calledOnce).to.equal(true);
                        chai.expect(spy.getCall(0).args[0]).to.equal(child);
                    });

                    it("should be dispatched on addChildren", function ()
                    {
                        const spy = sinon.spy(container.onChildAdded, "publish");

                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren([child]);

                        chai.expect(spy.calledOnce).to.equal(true);
                        chai.expect(spy.getCall(0).args[0]).to.equal(child);
                    });
                });

                describe("onChildRemoved", function ()
                {
                    it("should be dispatched on removeChild", function ()
                    {
                        const spy = sinon.spy(container.onChildRemoved, "publish");

                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.removeChild(child);

                        chai.expect(spy.calledOnce).to.equal(true);
                        chai.expect(spy.getCall(0).args[0]).to.equal(child);
                        spy.restore();
                    });

                    it("should be dispatched on removeChildren", function ()
                    {
                        const spy = sinon.spy(container.onChildRemoved, "publish");

                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.removeChildren([child]);

                        chai.expect(spy.calledOnce).to.equal(true);
                        chai.expect(spy.getCall(0).args[0]).to.equal(child);
                        spy.restore();
                    });

                    it("should be dispatched on removeAllChildren", function ()
                    {
                        const spy = sinon.spy(container.onChildRemoved, "publish");

                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.removeAllChildren();

                        chai.expect(spy.calledTwice).to.equal(true);
                        chai.expect(spy.getCall(0).args[0]).to.equal(child);
                        chai.expect(spy.getCall(1).args[0]).to.equal(child2);
                        spy.restore();
                    });
                });

                describe("addChild", function ()
                {
                    it("should contain a child when added", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        chai.expect(container.children.length).to.equal(1);
                        chai.expect(container.children.indexOf(child)).to.equal(0);
                    });
                });

                describe("hasChild", function ()
                {
                    it("should return false if not added", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.removeChild(child);
                        chai.expect(container.hasChild(child)).to.equal(false);
                    });

                    it("should return true if added", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        chai.expect(container.hasChild(child)).to.equal(true);
                    });

                    it("should return false if added to sub child and not deep", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        child2.addChild(child);
                        container.addChild(child2);
                        chai.expect(container.hasChild(child)).to.equal(false);
                    });

                    it("should return true if added to sub child and deep", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        child2.addChild(child);
                        container.addChild(child2);
                        chai.expect(container.hasChild(child, true)).to.equal(true);
                    });
                });

                describe("addChildAt", function ()
                {
                    it("should contain a child in the correct position when added", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.addChild(child3, 1);

                        chai.expect(container.children.indexOf(child)).to.equal(0);
                        chai.expect(container.children.indexOf(child2)).to.equal(2);
                        chai.expect(container.children.indexOf(child3)).to.equal(1);
                    });
                });

                describe("addChildren", function ()
                {
                    it("should add all children in the array order", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren([child, child2, child3]);

                        chai.expect(container.children.indexOf(child)).to.equal(0);
                        chai.expect(container.children.indexOf(child2)).to.equal(1);
                        chai.expect(container.children.indexOf(child3)).to.equal(2);
                    });

                    it("should add all children in the array order at index", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        const child4 = new RS.Rendering.DisplayObjectContainer();
                        const child5 = new RS.Rendering.DisplayObjectContainer();
                        const child6 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren([child, child2, child3]);
                        container.addChildren([child4, child5, child6], 1);

                        chai.expect(container.children.indexOf(child)).to.equal(0);
                        chai.expect(container.children.indexOf(child2)).to.equal(4);
                        chai.expect(container.children.indexOf(child3)).to.equal(5);
                        chai.expect(container.children.indexOf(child4)).to.equal(1);
                        chai.expect(container.children.indexOf(child5)).to.equal(2);
                        chai.expect(container.children.indexOf(child6)).to.equal(3);
                    });

                    it("should add all children in the listed order", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren(child, child2, child3);

                        chai.expect(container.children.indexOf(child)).to.equal(0);
                        chai.expect(container.children.indexOf(child2)).to.equal(1);
                        chai.expect(container.children.indexOf(child3)).to.equal(2);
                    });
                });

                describe("removeChild", function ()
                {
                    it("should not contain a child when removed", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();

                        container.addChild(child);
                        container.addChild(child2);
                        container.removeChild(child);
                        container.removeChild(child);

                        chai.expect(container.children.length).to.equal(1);
                        chai.expect(container.children.indexOf(child)).to.equal(-1);
                        chai.expect(container.children.indexOf(child2)).to.equal(0);
                    });
                    it("should not error when called on a disposed container", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();

                        container.addChild(child);

                        container.dispose();
                        container.removeChild(child);
                    });
                });

                describe("removeChildren", function ()
                {
                    it("should remove all children specified in array", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren(child, child2, child3);
                        container.removeChildren([child, child2]);

                        chai.expect(container.children.indexOf(child)).to.equal(-1);
                        chai.expect(container.children.indexOf(child2)).to.equal(-1);
                        chai.expect(container.children.indexOf(child3)).to.equal(0);
                    });

                    it("should remove all children specified", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren(child, child2, child3);
                        container.removeChildren(child, child2);

                        chai.expect(container.children.indexOf(child)).to.equal(-1);
                        chai.expect(container.children.indexOf(child2)).to.equal(-1);
                        chai.expect(container.children.indexOf(child3)).to.equal(0);
                    });
                });

                describe("removeAllChildren", function ()
                {
                    it("should remove all children", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren(child, child2, child3);
                        container.removeAllChildren();

                        chai.expect(container.children.indexOf(child)).to.equal(-1);
                        chai.expect(container.children.indexOf(child2)).to.equal(-1);
                        chai.expect(container.children.indexOf(child3)).to.equal(-1);
                    });
                });

                describe("moveToTop", function ()
                {
                    it("should move child to top of children", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren(child, child2, child3);
                        container.moveToTop(child);

                        chai.expect(container.children.indexOf(child)).to.equal(2);
                        chai.expect(container.children.indexOf(child2)).to.equal(0);
                        chai.expect(container.children.indexOf(child3)).to.equal(1);
                    });
                });

                describe("moveToBottom", function ()
                {
                    it("should move child to bottom of children", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChildren(child, child2, child3);
                        container.moveToBottom(child3);

                        chai.expect(container.children.indexOf(child)).to.equal(1);
                        chai.expect(container.children.indexOf(child2)).to.equal(2);
                        chai.expect(container.children.indexOf(child3)).to.equal(0);
                    });
                });

                describe("getChildIndex", function ()
                {
                    it("should be correct when using addChild", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.addChild(child3, 1);

                        chai.expect(container.getChildIndex(child)).to.equal(0);
                        chai.expect(container.getChildIndex(child2)).to.equal(2);
                        chai.expect(container.getChildIndex(child3)).to.equal(1);
                    });

                    it("should be correct when using removeChild", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.addChild(child3, 1);
                        container.removeChild(child3);

                        chai.expect(container.getChildIndex(child)).to.equal(0);
                        chai.expect(container.getChildIndex(child2)).to.equal(1);
                        chai.expect(container.getChildIndex(child3)).to.equal(-1);
                    });
                });

                describe("setChildIndex", function ()
                {
                    it("should change the index of the child", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.addChild(child3, 1);

                        container.setChildIndex(child, 2);

                        chai.expect(container.getChildIndex(child)).to.equal(2);
                        chai.expect(container.getChildIndex(child2)).to.equal(1);
                        chai.expect(container.getChildIndex(child3)).to.equal(0);

                        container.setChildIndex(child3, 1);

                        chai.expect(container.getChildIndex(child)).to.equal(2);
                        chai.expect(container.getChildIndex(child2)).to.equal(0);
                        chai.expect(container.getChildIndex(child3)).to.equal(1);
                    });

                    it("should cap high numbers", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.addChild(child3);
                        container.setChildIndex(child, 2000);

                        chai.expect(container.getChildIndex(child)).to.equal(2);
                        chai.expect(container.getChildIndex(child2)).to.equal(0);
                        chai.expect(container.getChildIndex(child3)).to.equal(1);
                    });

                    it("should go to 0 with negative number", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        const child2 = new RS.Rendering.DisplayObjectContainer();
                        const child3 = new RS.Rendering.DisplayObjectContainer();
                        container.addChild(child);
                        container.addChild(child2);
                        container.addChild(child3);
                        container.setChildIndex(child3, -2);

                        chai.expect(container.getChildIndex(child)).to.equal(1);
                        chai.expect(container.getChildIndex(child2)).to.equal(2);
                        chai.expect(container.getChildIndex(child3)).to.equal(0);
                    });
                });
            }
            describe("onAdded", function ()
            {
                it("should be called when added to display", function ()
                {
                    const spy = sinon.spy(disp, "onAdded");

                    stage.addChild(disp);
                    stage.addChild(disp);

                    chai.expect(spy.calledOnce).to.equal(true);
                    spy.restore();
                });
            });

            describe("onRemoved", function ()
            {
                it("should be called when removed from display", function ()
                {
                    const spy = sinon.spy(disp, "onRemoved");

                    stage.addChild(disp);
                    stage.removeChild(disp);
                    stage.removeChild(disp);

                    chai.expect(spy.calledOnce).to.equal(true);
                    spy.restore();
                });
            });
        });
    }
}