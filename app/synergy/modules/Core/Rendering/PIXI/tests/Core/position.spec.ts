namespace RS.Rendering.Pixi.Tests.Core
{
    export function position<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Position:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("x", function ()
            {
                it("should initialise to 0", function ()
                {
                    chai.expect(disp.x).to.equal(0);
                });

                it("should not be affected by ScaleManager", function ()
                {
                    disp.x = 87;
                    chai.expect(Math.round(disp.x)).to.equal(87);

                    RS.Rendering.ScaleManager.setPositionScale(0.7878);
                    disp.x = 87;
                    chai.expect(Math.round(disp.x)).to.equal(87);

                    RS.Rendering.ScaleManager.setPositionScale(2);
                    disp.x = 87;
                    chai.expect(Math.round(disp.x)).to.equal(87);
                });

                it("should not be affected by scaleX", function ()
                {
                    disp.x = 22;
                    disp.scaleX = 0.27827;
                    chai.expect(Math.round(disp.x)).to.equal(22);
                    disp.x = 13;
                    chai.expect(Math.round(disp.x)).to.equal(13);
                });
            });

            describe("y", function ()
            {
                it("should initialise to 0", function ()
                {
                    chai.expect(disp.x).to.equal(0);
                });

                it("should not be affected by ScaleManager", function ()
                {
                    disp.y = 87;
                    chai.expect(Math.round(disp.y)).to.equal(87);

                    RS.Rendering.ScaleManager.setPositionScale(0.7878);
                    disp.y = 87;
                    chai.expect(Math.round(disp.y)).to.equal(87);

                    RS.Rendering.ScaleManager.setPositionScale(2);
                    disp.y = 87;
                    chai.expect(Math.round(disp.y)).to.equal(87);

                });

                it("should not be affected by scaleY", function ()
                {
                    disp.y = 22;
                    disp.scaleY = 0.27827;
                    chai.expect(Math.round(disp.y)).to.equal(22);
                    disp.y = 13;
                    chai.expect(Math.round(disp.y)).to.equal(13);
                });
            });

            describe("toGlobal", function ()
            {
                it("should be local when no parent", function ()
                {
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).x).to.equal(0);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).y).to.equal(0);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).x).to.equal(10);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).y).to.equal(20);
                });

                it("should be global when on stage", function ()
                {
                    const stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                    stage.autoRender = false;

                    disp.x = 100;
                    disp.y = 200;
                    stage.addChild(disp);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).x).to.equal(100);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).y).to.equal(200);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).x).to.equal(110);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).y).to.equal(220);

                    stage.dispose();
                });

                it("should be stage-local when the stage is scaled and absolute is false or unspecified", function ()
                {
                    const stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                    stage.autoRender = false;

                    stage.scaleX = stage.scaleY = 0.5;

                    disp.x = 100;
                    disp.y = 200;
                    stage.addChild(disp);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).x).to.equal(100);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).y).to.equal(200);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).x).to.equal(110);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).y).to.equal(220);

                    stage.dispose();
                });

                it("should be canvas-local when the stage is scaled and absolute is true", function ()
                {
                    const stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                    stage.autoRender = false;

                    stage.scaleX = stage.scaleY = 0.5;

                    disp.x = 100;
                    disp.y = 200;
                    stage.addChild(disp);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0), null, true).x).to.equal(50);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0), null, true).y).to.equal(100);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20), null, true).x).to.equal(55);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20), null, true).y).to.equal(110);

                    stage.dispose();
                });

                it("should be global when on container", function ()
                {
                    const stage = new RS.Rendering.DisplayObjectContainer();
                    disp.x = 100;
                    disp.y = 200;
                    stage.addChild(disp);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).x).to.equal(100);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(0, 0)).y).to.equal(200);

                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).x).to.equal(110);
                    chai.expect(disp.toGlobal(RS.Math.Vector2D(10, 20)).y).to.equal(220);

                    stage.dispose();
                });
            });

            describe("toLocal", function ()
            {
                it("should be local when no parent", function ()
                {
                    chai.expect(disp.toLocal(RS.Math.Vector2D(0, 0)).x).to.equal(0);
                    chai.expect(disp.toLocal(RS.Math.Vector2D(0, 0)).y).to.equal(0);

                    chai.expect(disp.toLocal(RS.Math.Vector2D(10, 20)).x).to.equal(10);
                    chai.expect(disp.toLocal(RS.Math.Vector2D(10, 20)).y).to.equal(20);
                });

                it("should be global when on stage", function ()
                {
                    const stage = new RS.Rendering.Stage(document.createElement("canvas"), 512, 512, [RS.Rendering.StageRenderStyle.CANVAS]);
                    stage.autoRender = false;

                    disp.x = 100;
                    disp.y = 200;

                    stage.addChild(disp);

                    chai.expect(disp.toLocal(RS.Math.Vector2D(0, 0)).x).to.equal(-100);
                    chai.expect(disp.toLocal(RS.Math.Vector2D(0, 0)).y).to.equal(-200);

                    chai.expect(disp.toLocal(RS.Math.Vector2D(10, 20)).x).to.equal(-90);
                    chai.expect(disp.toLocal(RS.Math.Vector2D(10, 20)).y).to.equal(-180);

                    stage.dispose();
                });

                it("should be global when on container", function ()
                {
                    const stage = new RS.Rendering.DisplayObjectContainer();
                    disp.x = 100;
                    disp.y = 200;

                    stage.addChild(disp);

                    chai.expect(disp.toLocal(RS.Math.Vector2D(0, 0)).x).to.equal(-100);
                    chai.expect(disp.toLocal(RS.Math.Vector2D(0, 0)).y).to.equal(-200);

                    chai.expect(disp.toLocal(RS.Math.Vector2D(10, 20)).x).to.equal(-90);
                    chai.expect(disp.toLocal(RS.Math.Vector2D(10, 20)).y).to.equal(-180);
                    
                    stage.dispose();
                });
            });
        });
    }
}