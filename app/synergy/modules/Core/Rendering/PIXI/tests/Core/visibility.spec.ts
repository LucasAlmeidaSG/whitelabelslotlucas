namespace RS.Rendering.Pixi.Tests.Core
{
    export function visibility<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Visibility:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("alpha", function ()
            {
                it("should initialise to 1", function ()
                {
                    chai.expect(disp.alpha).to.equal(1)
                });

                it("should set a value", function ()
                {
                    disp.alpha = 0.42;
                    chai.expect(disp.alpha).to.equal(0.42)
                });
            });

            describe("visible", function ()
            {
                it("should initialise to true", function ()
                {
                    chai.expect(disp.visible).to.equal(true)
                });

                it("should set a value", function ()
                {
                    disp.visible = false;
                    chai.expect(disp.visible).to.equal(false)
                });
            });
        });
    }
}