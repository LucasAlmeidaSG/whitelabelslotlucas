namespace RS.Rendering.Pixi.Tests.Core
{
    export function dispose(factoryFn: () => RS.Rendering.IDisplayObject, isContainer: boolean)
    {
        return describe("Dispose:", function ()
        {
            let disp: RS.Rendering.IDisplayObject;
            beforeEach(function ()
            {
                disp = factoryFn();
            });

            afterEach(function ()
            {
                disp.dispose();
            });

            describe("dispose", function ()
            {
                if (isContainer)
                {
                    it("should remove all children", function ()
                    {
                        const child = new RS.Rendering.DisplayObjectContainer();
                        (disp as IContainer).addChild(child);

                        disp.dispose();

                        chai.expect(child.parent, "child parent is null").to.equal(null);
                        chai.expect(child.stage, "child stage is null").to.equal(null);
                    });
                }

                it("should remove the object from its parent", function ()
                {
                    const parent = new RS.Rendering.DisplayObjectContainer();
                    parent.addChild(disp);

                    disp.dispose();

                    chai.expect(parent.children.indexOf(disp), "object not in parent array").to.equal(-1);
                });

                it("should not publish onParentChanged", function ()
                {
                    const parent = new RS.Rendering.DisplayObjectContainer();
                    parent.addChild(disp);

                    let eventPublished = false;
                    disp.onParentChanged(() => eventPublished = true);

                    disp.dispose();

                    chai.expect(eventPublished, "event not published").to.equal(false);
                });

                it("should not publish onStageChanged", function ()
                {
                    const parent = new RS.Rendering.DisplayObjectContainer();
                    parent.addChild(disp);

                    let eventPublished = false;
                    disp.onStageChanged(() => eventPublished = true);

                    disp.dispose();

                    chai.expect(eventPublished, "event not published").to.equal(false);
                });
            });
        });
    }
}