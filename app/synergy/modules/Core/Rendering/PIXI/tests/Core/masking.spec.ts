namespace RS.Rendering.Pixi.Tests.Core
{
    export function masking<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Masking:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                RS.Rendering.ScaleManager.setPositionScale(1);
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("mask", function ()
            {
                it("should initialise to undefined", function ()
                {
                    chai.expect(disp.mask).to.equal(undefined)
                });

                it("should set a value as IGraphics object", function ()
                {
                    let mask = new RS.Rendering.Graphics();
                    disp.mask = mask;
                    chai.expect(disp.mask).to.equal(mask);
                });

                it("should set a value as Rectangle object", function ()
                {
                    let mask = new RS.Rendering.Rectangle(0, 0, 100, 100);
                    disp.mask = mask;
                    chai.expect(disp.mask).to.equal(mask);
                });

                it("should set a value as IBitmap object", function ()
                {
                    let asset = new RS.Asset.ImageAsset({
                        id: "",
                        group: "",
                        path: "",
                        raw: {},
                        type: "",
                        formats: [],
                        exclude: []
                    })
                    let mask = new RS.Rendering.Bitmap(asset);
                    disp.mask = mask;
                    chai.expect(disp.mask).to.equal(mask);
                });

                it("should set a value as IMovieClip object", function ()
                {
                    let asset = new RS.Asset.ImageAsset({
                        id: "",
                        group: "",
                        path: "",
                        raw: {},
                        type: "",
                        formats: [],
                        exclude: []
                    })
                    let mask = new RS.Rendering.MovieClip([asset], 60);
                    disp.mask = mask;
                    chai.expect(disp.mask).to.equal(mask);
                });
            });
        });
    }
}