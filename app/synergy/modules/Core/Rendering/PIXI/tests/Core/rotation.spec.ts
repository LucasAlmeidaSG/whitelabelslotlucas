namespace RS.Rendering.Pixi.Tests.Core
{
    export function rotation<T extends IDisplayObject>(fn: () => T)
    {
        return describe("Rotation:", function ()
        {
            let disp: T;
            beforeEach(function ()
            {
                disp = fn();
            });

            afterEach(function ()
            {
                disp.dispose();
                disp = null;
            });

            describe("rotation", function ()
            {
                it("should initialise to 0", function ()
                {
                    chai.expect(disp.rotation).to.equal(0)
                });

                it("should not be affected by ScaleManager", function ()
                {
                    disp.rotation = 87;
                    chai.expect(Math.round(disp.rotation)).to.equal(87);

                    RS.Rendering.ScaleManager.setPositionScale(0.7878);
                    disp.rotation = 87;
                    chai.expect(Math.round(disp.rotation)).to.equal(87);
                });

                it("should not wrap", function ()
                {
                    disp.rotation = 999;
                    chai.expect(Math.round(disp.rotation)).to.equal(999);

                    disp.rotation = -99;
                    chai.expect(Math.round(disp.rotation)).to.equal(-99);
                });
            });
        });
    }
}