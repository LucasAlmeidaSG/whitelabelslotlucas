namespace RS.Rendering.Pixi.Tests
{
    describe("SceneObject.ts", function ()
    {
        let sceneObject: RS.Rendering.Scene.ISceneObject;
        beforeEach(function ()
        {
            sceneObject = new RS.Rendering.Scene.SceneObject();
        });

        afterEach(function ()
        {
            sceneObject.dispose();
            sceneObject = null;
        });

        describe("addChild", function ()
        {
            it("should contain a child when added", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChild(child);
                chai.expect(sceneObject.children.length).to.equal(1);
                chai.expect(sceneObject.children.indexOf(child)).to.equal(0);
            });
        });

        describe("hasChild", function ()
        {
            it("should return false if not added", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChild(child);
                sceneObject.removeChild(child);
                chai.expect(sceneObject.hasChild(child)).to.equal(false);
            });

            it("should return true if added", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChild(child);
                chai.expect(sceneObject.hasChild(child)).to.equal(true);
            });

            it("should return false if added to sub child and not deep", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                child2.addChild(child);
                sceneObject.addChild(child2);
                chai.expect(sceneObject.hasChild(child)).to.equal(false);
            });

            it("should return true if added to sub child and deep", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                child2.addChild(child);
                sceneObject.addChild(child2);
                chai.expect(sceneObject.hasChild(child, true)).to.equal(true);
            });
        });

        describe("addChildAt", function ()
        {
            it("should contain a child in the correct position when added", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChild(child);
                sceneObject.addChild(child2);
                sceneObject.addChild(child3, 1);

                chai.expect(sceneObject.children.indexOf(child)).to.equal(0);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(2);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(1);
            });
        });

        describe("addChildren", function ()
        {
            it("should add all children in the array order", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChildren([child, child2, child3]);

                chai.expect(sceneObject.children.indexOf(child)).to.equal(0);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(1);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(2);
            });

            it("should add all children in the array order at index", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                const child4 = new RS.Rendering.Scene.SceneObject();
                const child5 = new RS.Rendering.Scene.SceneObject();
                const child6 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChildren([child, child2, child3]);
                sceneObject.addChildren([child4, child5, child6], 1);

                chai.expect(sceneObject.children.indexOf(child)).to.equal(0);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(4);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(5);
                chai.expect(sceneObject.children.indexOf(child4)).to.equal(1);
                chai.expect(sceneObject.children.indexOf(child5)).to.equal(2);
                chai.expect(sceneObject.children.indexOf(child6)).to.equal(3);
            });

            it("should add all children in the listed order", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChildren(child, child2, child3);

                chai.expect(sceneObject.children.indexOf(child)).to.equal(0);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(1);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(2);
            });
        });

        describe("removeChild", function ()
        {
            it("should not contain a child when removed", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();

                sceneObject.addChild(child);
                sceneObject.addChild(child2);
                sceneObject.removeChild(child);
                sceneObject.removeChild(child);

                chai.expect(sceneObject.children.length).to.equal(1);
                chai.expect(sceneObject.children.indexOf(child)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(0);
            });
        });

        describe("removeChildren", function ()
        {
            it("should remove all children specified in array", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChildren(child, child2, child3);
                sceneObject.removeChildren([child, child2]);

                chai.expect(sceneObject.children.indexOf(child)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(0);
            });

            it("should remove all children specified", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChildren(child, child2, child3);
                sceneObject.removeChildren(child, child2);

                chai.expect(sceneObject.children.indexOf(child)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(0);
            });
        });

        describe("removeAllChildren", function ()
        {
            it("should remove all children", function ()
            {
                const child = new RS.Rendering.Scene.SceneObject();
                const child2 = new RS.Rendering.Scene.SceneObject();
                const child3 = new RS.Rendering.Scene.SceneObject();
                sceneObject.addChildren(child, child2, child3);
                sceneObject.removeAllChildren();

                chai.expect(sceneObject.children.indexOf(child)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child2)).to.equal(-1);
                chai.expect(sceneObject.children.indexOf(child3)).to.equal(-1);
            });
        });
    });
}
