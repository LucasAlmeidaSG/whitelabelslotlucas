/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("Stage.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });

        Core.misc(createStage);
        Core.position(createStage);
        Core.rotation(createStage);
        Core.dimensions(createStageWithSize, 0, 1, 0, 1);
        Core.scale(createStage);
        Core.transform(createStage);
        Core.visibility(createStage);
        Core.interactivity(createStage, false);
        Core.masking(createStage);
        Core.offsets(createStage, false);
        Core.children(createStage, true);
        Core.displayList(createStage, true);
        Core.updates(createStage, true);
        Core.dispose(createStage, true);
        //corePrePost(createBitmap);
        beforeEach(function ()
        {
            this.stage = createStage();
        });

        afterEach(function ()
        {
            const stage = this.stage as RS.Rendering.IStage;
            stage.dispose();
            this.stage = null;
        });

        describe("stageWidth", function ()
        {
            it("should default to constructor size", function ()
            {
                chai.expect(this.stage.stageWidth).to.equal(200);
            });

            it("should not be settable", function ()
            {
                this.stage.stageWidth = 90;
                chai.expect(this.stage.stageWidth).to.equal(200);
            });
        });

        describe("stageHeight", function ()
        {
            it("should default to constructor size", function ()
            {
                chai.expect(this.stage.stageHeight).to.equal(100);
            });

            it("should not be settable", function ()
            {
                this.stage.stageHeight = 90;
                chai.expect(this.stage.stageHeight).to.equal(100);
            });
        });

        describe("autoRender", function ()
        {
            it("should default to true", function ()
            {
                chai.expect(this.stage.autoRender).to.equal(true);
            });

            it("should be settable", function ()
            {
                this.stage.autoRender = false;
                chai.expect(this.stage.autoRender).to.equal(false);
            });
        });

        describe("canvas", function ()
        {
            it("should exist", function ()
            {
                chai.expect(this.stage.canvas).to.not.equal(null);
                chai.expect(this.stage.canvas).to.not.equal(undefined);
            });

            it("should not be settable", function ()
            {
                this.stage.canvas = 7;
                chai.expect(this.stage.canvas).to.not.equal(7);
            });
        });

        describe("fps", function ()
        {
            it("should default to 60", function ()
            {
                chai.expect(this.stage.fps).to.equal(60);
            });

            it("should be settable", function ()
            {
                this.stage.fps = 10;
                chai.expect(this.stage.fps).to.equal(10);
            });
        });

        describe("paused", function ()
        {
            it("should default to false", function ()
            {
                chai.expect(this.stage.paused).to.equal(false);
            });

            it("should be settable", function ()
            {
                this.stage.paused = true;
                chai.expect(this.stage.paused).to.equal(true);
            });
        })

        describe("render", function ()
        {
            it("should not autocall when autoRender is false", function ()
            {
                this.stage.autoRender = false;
                let disp = this.stage;

                RS.Tests.simulateTime(100);

                sinon.spy(disp, "render");

                RS.Tests.simulateTime(100);

                chai.expect(disp.render.called).to.equal(false);
                disp.render.restore();
            });

            it("should autocall when autoRender is true", function ()
            {
                this.stage.autoRender = true;

                sinon.spy(this.stage, "render");
                let disp = this.stage;

                RS.Tests.simulateTime(100);

                chai.expect(disp.render.called).to.equal(true);
                disp.render.restore();
            });
        })

        describe("resize", function ()
        {
            it("should change stage width", function ()
            {
                this.stage.resize(100, 200);
                chai.expect(this.stage.stageWidth).to.equal(100);
            });

            it("should change stage height", function ()
            {
                this.stage.resize(100, 200);
                chai.expect(this.stage.stageHeight).to.equal(200);
            });
        })
    });

    function createStage()
    {
        return new RS.Rendering.Stage(document.createElement("canvas"), 200, 100, [RS.Rendering.StageRenderStyle.CANVAS]);
    }

    function createStageWithSize()
    {
        const d = new RS.Rendering.Stage(document.createElement("canvas"), 200, 100, [RS.Rendering.StageRenderStyle.CANVAS]);
        const g = new RS.Rendering.Graphics();
        g.drawRect(0, 0, 1, 1);
        d.addChild(g);
        return d;
    }
}