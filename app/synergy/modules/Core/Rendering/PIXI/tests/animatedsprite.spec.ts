/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("AnimatedSprite.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });

        Core.misc(createAnim);
        Core.position(createAnim);
        Core.rotation(createAnim);
        Core.dimensions(createAnim, 140, 140, 214, 214);
        Core.scale(createAnim);
        Core.transform(createAnim);
        Core.visibility(createAnim);
        Core.interactivity(createAnim, false);
        Core.masking(createAnim);
        Core.offsets(createAnim, false);
        Core.children(createAnim, true);
        Core.displayList(createAnim, true);
        Core.updates(createAnim);
        Core.dispose(createAnim, true);
        //corePrePost(createBitmap);

        beforeEach(function ()
        {
            const asset = new MockRSSpritesheet(dummyAnimData);
            this.ss = asset;
            this.anim = new RS.Rendering.AnimatedSprite(asset);
        });

        afterEach(function ()
        {
            const anim = this.anim as RS.Rendering.IDisplayObject;
            anim.dispose();
            this.anim = null;
        });

        describe("constructor", function ()
        {
            it("should fail with no paramater", function ()
            {
                let isFail = false;
                try
                {
                    const spr = new RS.Rendering.AnimatedSprite(undefined);
                }
                catch (error)
                {
                    isFail = true;
                }

                chai.expect(isFail).to.equal(true);
            });

            it("should accept valid ISpritesheet as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const spr = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData));
                }
                catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should accept valid ISpritesheet and frame number as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const spr = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), 3);
                }
                catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should accept valid ISpritesheet and animation name as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const spr = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), "Lamp_NN");
                }
                catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });
        });

        describe("currentFrame", function ()
        {
            it("should default to 0", function ()
            {
                chai.expect(this.anim.currentFrame).to.equal(0);
            });

            it("should default to constructor value if passed", function ()
            {
                let anim = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), 3);
                chai.expect(anim.currentFrame).to.equal(3);

                anim = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), "Lamp_NN");
                chai.expect(anim.currentFrame).to.equal(0);
            });

            it("should not be settable", function ()
            {
                this.anim.currentFrame = 5;
                chai.expect(this.anim.currentFrame).to.equal(0);
            });
        });

        describe("currentAnimation", function ()
        {
            it("should default to null", function ()
            {
                chai.expect(this.anim.currentAnimation).to.equal(null);
            });

            it("should default to constructor value if passed", function ()
            {
                let anim = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), "Lamp_NN");
                chai.expect(anim.currentAnimation).to.equal("Lamp_NN");

                anim = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), 3);
                chai.expect(anim.currentAnimation).to.equal(null);
            });

            it("should not be settable", function ()
            {
                this.anim.currentAnimation = "test";
                chai.expect(this.anim.currentAnimation).to.equal(null);
            });
        });

        describe("paused", function ()
        {
            it("should default to true", function ()
            {
                chai.expect(this.anim.paused).to.equal(true);
            });

            it("should be settable", function ()
            {
                this.anim.paused = false;
                chai.expect(this.anim.paused).to.equal(false);
            });

            it("should be false when played", function ()
            {
                chai.expect(this.anim.paused).to.equal(true);
                this.anim.play();
                chai.expect(this.anim.paused).to.equal(false);
            });

            it("should be false when gotoAndPlay used", function ()
            {
                chai.expect(this.anim.paused).to.equal(true);
                this.anim.gotoAndPlay(10);
                chai.expect(this.anim.paused).to.equal(false);
            });

            it("should be true when stopped", function ()
            {
                chai.expect(this.anim.paused).to.equal(true);
                this.anim.play();
                this.anim.stop();
                chai.expect(this.anim.paused).to.equal(true);
            });

            it("should be true when gotoAndStop used", function ()
            {
                chai.expect(this.anim.paused).to.equal(true);
                this.anim.play();
                this.anim.gotoAndStop(5);
                chai.expect(this.anim.paused).to.equal(true);
            });
        });

        describe("spriteSheet", function ()
        {
            it("should default to constructor value", function ()
            {
                chai.expect(this.anim.spriteSheet).to.equal(this.ss);
            });

            it("should be settable", function ()
            {
                const ss = new MockRSSpritesheet(dummyAnimData);
                this.anim.spriteSheet = ss;
                chai.expect(this.anim.spriteSheet).to.equal(ss);
                chai.expect(this.anim.spriteSheet).to.not.equal(this.ss);
            });
        });

        describe("currentAnimationFrame", function ()
        {
            it("should default to constructor value", function ()
            {
                chai.expect(this.anim.currentAnimationFrame).to.equal(0);
            });

            it("should be 0 when not specified in constructor", function ()
            {
                this.anim = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), 3);
                chai.expect(this.anim.currentAnimationFrame).to.equal(0);
            });

            it("should be 0 when specified in constructor", function ()
            {
                this.anim = new RS.Rendering.AnimatedSprite(new MockRSSpritesheet(dummyAnimData), "Lamp_NN");
                chai.expect(this.anim.currentAnimationFrame).to.equal(0);
            });

            it("should be settable", function ()
            {
                this.anim.currentAnimationFrame = 2;
                chai.expect(this.anim.currentAnimationFrame).to.equal(2);
            });
        });

        describe("looping", function ()
        {
            it("should default to false", function ()
            {
                chai.expect(this.anim.looping).to.equal(false);
            });

            it("should not be settable", function ()
            {
                this.anim.looping = true;
                chai.expect(this.anim.looping).to.equal(false);
            });

            it("should be true when playing the sprite with play settings loop: true", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay("test", { loop: true });
                chai.expect(this.anim.looping).to.equal(true);
            });

            it("should be false when playing the sprite with play settings loop: false", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay("test", { loop: false });
                chai.expect(this.anim.looping).to.equal(false);
            });
        });

        describe("framerate", function ()
        {
            it("should default to json data value", function ()
            {
                chai.expect(this.anim.framerate).to.equal(24);
            });

            it("should be settable", function ()
            {
                this.anim.framerate = 2;
                chai.expect(this.anim.framerate).to.equal(2);
            });
        });

        describe("onAnimationEnded", function ()
        {
            it("should be called when animation completes", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onAnimationEnded, "publish");
                anim.play();

                RS.Tests.simulateTime((15 / 24) * 1000 * 1.1);

                chai.expect(spy.called).to.equal(true);
                chai.expect(spy.calledOnce).to.equal(true);
                spy.restore();
            });

            it("should not be called before the animation completes", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onAnimationEnded, "publish");
                anim.play();

                RS.Tests.simulateTime((15 / 24) * 500);

                chai.expect(spy.called).to.equal(false);
                spy.restore();
                anim.stop();
            });
        });

        describe("onChanged", function ()
        {
            it("should be called when frame changes via play", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onChanged, "publish");
                anim.play();

                RS.Tests.simulateTime(100);

                chai.expect(spy.called).to.equal(true);
                spy.restore();
                anim.stop();
            });

            it("should be called when frame changes via gotoAndStop", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onChanged, "publish");
                anim.gotoAndStop(3);

                RS.Tests.simulateTime(100);

                chai.expect(spy.called).to.equal(true);
                chai.expect(spy.calledOnce).to.equal(true);
                spy.restore();
            });

            it("should not be called when idling", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onChanged, "publish");

                RS.Tests.simulateTime(10);

                chai.expect(spy.called).to.equal(false);
                spy.restore();
            });
        });

        describe("onCustomEvent", function ()
        {
            it("should be called when hits frame via play", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onCustomEvent, "publish");
                anim.play();

                RS.Tests.simulateTime((3 / 24) * 1000 * 1.1);

                chai.expect(spy.called).to.equal(true);
                chai.expect(spy.getCall(0).args[0]).to.equal("test");
                spy.restore();
                anim.stop();
            });

            it("should be called when frame changes via gotoAndStop", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onCustomEvent, "publish");
                anim.gotoAndStop(3);

                RS.Tests.simulateTime(100);

                chai.expect(spy.called).to.equal(true);
                chai.expect(spy.calledOnce).to.equal(true);
                chai.expect(spy.getCall(0).args[0]).to.equal("test");
                spy.restore();
            });

            it("should not be called when idling", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                const spy = sinon.spy(anim.onCustomEvent, "publish");

                RS.Tests.simulateTime(10);

                chai.expect(spy.called).to.equal(false);
                spy.restore();
            });
        });

        describe("play", function ()
        {
            it("should play the clip", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.play();
                chai.expect(anim.currentFrame).to.equal(0);

                RS.Tests.simulateTime(50);

                chai.expect(anim.currentFrame).to.not.equal(0);
            });
        });

        describe("stop", function ()
        {
            it("should stop the clip", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.play();
                let frame = 0;
                anim.play();
                chai.expect(anim.currentFrame).to.equal(0);

                RS.Tests.simulateTime(50);

                anim.stop();
                frame = anim.currentFrame;

                RS.Tests.simulateTime(50);

                chai.expect(anim.currentFrame).to.equal(frame);
            });
        });

        describe("gotoAndPlay", function ()
        {
            it("should change the frame", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay(5);
                chai.expect(anim.currentFrame).to.equal(5);
            });

            it("should change the frame with offset", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay(5, { startFrameOffset: 1 });
                chai.expect(anim.currentFrame).to.equal(6);
            });

            it("should play the clip", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay(5);
                chai.expect(anim.currentFrame).to.equal(5);

                RS.Tests.simulateTime(100);

                chai.expect(anim.currentFrame).to.not.equal(5);
            });

            it("should go to frame of animation", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay("test");
                chai.expect(anim.currentFrame).to.equal(3);
                chai.expect(anim.currentAnimation).to.equal("test");
                chai.expect(anim.currentAnimationFrame).to.equal(0);

                RS.Tests.simulateTime(100);

                chai.expect(anim.currentFrame).to.not.equal(3);
                chai.expect(anim.currentAnimationFrame).to.not.equal(0);
            });

            it("should accept settings", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndPlay("test", { fps: 1 });
                chai.expect(anim.currentFrame).to.equal(3);
                chai.expect(anim.currentAnimation).to.equal("test");
                chai.expect(anim.currentAnimationFrame).to.equal(0);
                chai.expect(anim.framerate).to.equal(1);
            });
        });

        describe("gotoAndStop", function ()
        {
            it("should change the frame", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.gotoAndStop(5);
                chai.expect(anim.currentFrame).to.equal(5);
            });

            it("should stop the clip", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                anim.play();
                anim.gotoAndStop(5);
                chai.expect(anim.currentFrame).to.equal(5);

                RS.Tests.simulateTime(50);

                chai.expect(anim.currentFrame).to.equal(5);
            });
        });

        describe("advance", function ()
        {
            it("should advance the animation frames", function ()
            {
                const anim: RS.Rendering.IAnimatedSprite = this.anim;
                chai.expect(anim.currentFrame).to.equal(0);
                anim.advance(100);
                chai.expect(anim.currentFrame).to.not.equal(0);
            });
        });
    });

    function createAnim()
    {
        const asset = new MockRSSpritesheet(dummyAnimData);
        return new RS.Rendering.AnimatedSprite(asset);
    }

    class MockRSSpritesheet extends RS.Rendering.RSSpriteSheet
    {
        protected resolveFrameImages(): void
        {
            const that = this as any;
            that._maxFrame = new RS.Rendering.Rectangle();
            for (let i = 0; i < that._frames.length; i++)
            {
                const frame = that._frames[i];
                if (!frame.imageData && frame.imageID != null)
                {
                    const tex = new RS.Rendering.Texture("");
                    frame.imageData = new RS.Rendering.SubTexture(tex, frame.imageRect);
                }
            }
        }
    }

    const dummyAnimData =
    {
        "images": [ "lamp_desktop.png" ],
        "framerate": 24,
        "frames":
        [
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300],
            [1904, 171, 140, 214, 0, 0, 0, 300, 300]
        ],
        "animations":
        {
            "Lamp_NN": { "frames": [0, 1, 2, 3, 4, 5], "next": null },
            "test": { "frames": [3, 4, 5], "next": null }
        },
        "events": [{ "frame": 3, "event": "test" }],
        "meta":
        {
            "version": "Red 7 Exporter ver. 2.0",
            "exported": "Tue Sep 12 2017",
            "options": { "compact": false, "sorted": true, "fps": 24 },
            "smartKey": "$TexturePacker:SmartUpdate:4544da9cd148b27fbcd0908c45737567:d225a27c634529322764b29521e3275f:1197c78aaf56a6e9d06bd6a1b0a6e9b0$"
        }
    };
}
