/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("Mesh.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });
        
        describe("constructor", function ()
        {
            it("should fail with no paramater", function ()
            {
                let isFail = false;
                try
                {
                    const mesh = new RS.Rendering.Mesh(undefined);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(true);
            });
        });

        Core.misc(createMesh);
        Core.position(createMesh);
        Core.rotation(createMesh);
        Core.dimensions(createMesh, 0, 1, 0, 1);
        Core.scale(createMesh);
        Core.transform(createMesh);
        Core.visibility(createMesh);
        Core.interactivity(createMesh, false);
        Core.masking(createMesh);
        Core.offsets(createMesh, false);
        Core.children(createMesh, false);
        Core.displayList(createMesh, false);
        Core.updates(createMesh);
        Core.dispose(createMesh, false);
        //corePrePost(createBitmap);
    });

    function createMesh()
    {
        const settings: RS.Rendering.IMesh.Settings = {
            positions: new Float32Array([
                0, 0, 0,
                1, 0, 0,
                1, 1, 0,
                0, 1, 0
            ])
        };
        return new RS.Rendering.Mesh(settings, null);
    }
}