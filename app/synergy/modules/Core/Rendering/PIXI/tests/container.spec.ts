/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("DisplayObjectContainer.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });

        Core.misc(createDisplayObContainer);
        Core.position(createDisplayObContainer);
        Core.rotation(createDisplayObContainer);
        Core.dimensions(createDisplayObContainerWithSize, 0, 1, 0, 1);
        Core.scale(createDisplayObContainer);
        Core.transform(createDisplayObContainer);
        Core.visibility(createDisplayObContainer);
        Core.interactivity(createDisplayObContainer, false);
        Core.masking(createDisplayObContainer);
        Core.offsets(createDisplayObContainer, false);
        Core.children(createDisplayObContainer, true);
        Core.displayList(createDisplayObContainer, true);
        Core.updates(createDisplayObContainer);
        Core.dispose(createDisplayObContainer, true);
        //corePrePost(createBitmap);

        let container: RS.Rendering.IContainer;
        beforeEach(function ()
        {
            container = createDisplayObContainer();
        });

        afterEach(function ()
        {
            container.dispose();
            container = null;
        });

        describe("sortFunction", function ()
        {
            it("should sort existing children when set", function ()
            {
                const children = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                children[0].y = 60;
                children[1].y = 494;
                container.addChildren(children);

                container.sortFunction = (childA, childB) => childA.y - childB.y;
                chai.expect(container.children.map((child) => child.y)).to.deep.equal([0, 60, 494]);
            });

            it("should add new children in a sorted manner", function ()
            {
                const children = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                children[0].y = 0;
                children[1].y = 60;
                children[2].y = 494;

                container.addChildren(children);
                container.sortFunction = (childA, childB) => childA.y - childB.y;

                // Child should be added in middle of list [0, 60, 82, 494]
                const newChild = new RS.Rendering.DisplayObjectContainer();
                newChild.y = 82;

                container.addChild(newChild);
                chai.expect(container.children.map((child) => child.y), "general case").to.deep.equal([0, 60, 82, 494]);

                // Child should be added at start of list [-10, 0, 60, 82, 494]
                const newHead = new RS.Rendering.DisplayObjectContainer();
                newHead.y = -10;

                container.addChild(newHead);
                chai.expect(container.children.map((child) => child.y), "minimal child").to.deep.equal([-10, 0, 60, 82, 494]);

                // Child should be added at end of list [-10, 0, 60, 82, 494, 138518401]
                const newTail = new RS.Rendering.DisplayObjectContainer();
                newTail.y = 138518401;

                container.addChild(newTail);
                chai.expect(container.children.map((child) => child.y), "maximal child").to.deep.equal([-10, 0, 60, 82, 494, 138518401]);

                // Multiple assorted children [-10, 0, 30, 60, 74, 82, 300, 494, 138518401]
                const multiple = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                multiple[0].y = 300;
                multiple[1].y = 30;
                multiple[2].y = 74;

                container.addChildren(multiple);
                chai.expect(container.children.map((child) => child.y), "multiple children").to.deep.equal([-10, 0, 30, 60, 74, 82, 300, 494, 138518401]);
            });
        });

        describe("addChildBelow", function ()
        {
            it("should not add the child if the target child is not found", function ()
            {
                const children = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                container.addChildren(children);

                const newChild = new RS.Rendering.DisplayObjectContainer();
                container.addChildUnder(newChild, new RS.Rendering.DisplayObjectContainer());

                chai.expect(container.children.indexOf(newChild)).to.equal(-1);
            });

            it("should allow adding a child below another", function ()
            {
                const children = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                container.addChildren(children);

                const newChild = new RS.Rendering.DisplayObjectContainer();
                container.addChildUnder(newChild, children[1]);

                chai.expect(container.children.indexOf(newChild)).to.equal(1);
            });
        });

        describe("addChildAbove", function ()
        {
            it("should not add the child if the target child is not found", function ()
            {
                const children = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                container.addChildren(children);

                const newChild = new RS.Rendering.DisplayObjectContainer();
                container.addChildAbove(newChild, new RS.Rendering.DisplayObjectContainer());

                chai.expect(container.children.indexOf(newChild)).to.equal(-1);
            });

            it("should allow adding a child above another", function ()
            {
                const children = [new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer(), new RS.Rendering.DisplayObjectContainer()];
                container.addChildren(children);

                const newChild = new RS.Rendering.DisplayObjectContainer();
                container.addChildAbove(newChild, children[1]);

                chai.expect(container.children.indexOf(newChild)).to.equal(2);
            });
        });
    });

    function createDisplayObContainer()
    {
        return new RS.Rendering.DisplayObjectContainer();
    }

    function createDisplayObContainerWithSize()
    {
        const d = new RS.Rendering.DisplayObjectContainer();
        const g = new RS.Rendering.Graphics();
        g.drawRect(0, 0, 1, 1);
        d.addChild(g);
        return d;
    }
}