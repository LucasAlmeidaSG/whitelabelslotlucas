/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("MovieClip.ts", function ()
    {
        before(function () {
            RS.Tests.initTicker();
        });

        Core.misc(createMc);
        Core.position(createMc);
        Core.rotation(createMc);
        Core.dimensions(createMc, 0, 1, 0, 1);
        Core.scale(createMc);
        Core.transform(createMc);
        Core.visibility(createMc);
        Core.interactivity(createMc, false);
        Core.masking(createMc);
        Core.offsets(createMc, false);
        Core.children(createMc, true);
        Core.displayList(createMc, true);
        Core.updates(createMc, true);
        Core.dispose(createMc, true);
        //corePrePost(createBitmap);

        beforeEach(function ()
        {
            this.mc = createLongMc();
        });

        afterEach(function ()
        {
            const mc = this.nc as RS.Rendering.IMovieClip;
            if (mc) { mc.dispose(); }
            this.mc = null;
        });

        describe("constructor", function ()
        {
            it("should fail with no paramater", function ()
            {
                let isFail = false;
                try
                {
                    let bitmap = new RS.Rendering.MovieClip(undefined, 0);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(true);
            });

            it("should accept Frames as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const frame: RS.Rendering.Frame = {
                        /** The image object in which the frame is found. */
                        imageData: new RS.Rendering.SubTexture(new RS.Rendering.Texture(""), new RS.Rendering.ReadonlyRectangle(0, 0, 0, 0)),
                        imageID: 1,
                        imageRect: null,
                        reg: null,
                        untrimmedSize: null,
                        trimRect: null
                    };
                    let ob = new RS.Rendering.MovieClip([frame], 60);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should accept IAssets as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const asset = new RS.Asset.ImageAsset({
                        id: "",
                        group: "",
                        path: "",
                        raw: {},
                        type: "",
                        formats: [],
                        exclude: []
                    });
                    const bitmap = new RS.Rendering.MovieClip([asset], 60);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });
        });

        describe("onAnimationEnded", function ()
        {
            it("should be called when animation completes", function ()
            {
                sinon.spy(this.mc.onAnimationEnded, "publish");
                let clip = this.mc;
                let parent = new RS.Rendering.DisplayObjectContainer();
                parent.addChild(clip);
                clip.fps = 60;
                clip.play();

                RS.Tests.simulateTime((10 / 60) * 1000 * 1.1);

                chai.expect(clip.onAnimationEnded.publish.called).to.equal(true);
                chai.expect(clip.onAnimationEnded.publish.calledOnce).to.equal(true);
                clip.onAnimationEnded.publish.restore();
            });

            it("should not be called before the animation completes", function ()
            {
                sinon.spy(this.mc.onAnimationEnded, "publish");
                let clip = this.mc;
                clip.fps = 60;
                clip.play();

                RS.Tests.simulateTime((10 / 60) * 500);

                chai.expect(clip.onAnimationEnded.publish.called).to.equal(false);
                clip.onAnimationEnded.publish.restore();
            });
        });

        describe("onChanged", function ()
        {
            it("should be called when frame changes via play", function ()
            {
                sinon.spy(this.mc.onChanged, "publish");
                let clip = this.mc;

                let parent = new RS.Rendering.DisplayObjectContainer();
                parent.addChild(clip);
                clip.fps = 60;
                clip.play();

                RS.Tests.simulateTime(100);

                chai.expect(clip.onChanged.publish.called).to.equal(true);
                clip.onChanged.publish.restore();
            });

            it("should be called when frame changes via gotoAndStop", function ()
            {
                sinon.spy(this.mc.onChanged, "publish");
                let clip = this.mc;
                clip.fps = 60;
                clip.gotoAndStop(3);

                RS.Tests.simulateTime(100);

                chai.expect(clip.onChanged.publish.called).to.equal(true);
                chai.expect(clip.onChanged.publish.calledOnce).to.equal(true);
                clip.onChanged.publish.restore();
            });

            it("should not be called when idling", function ()
            {
                sinon.spy(this.mc.onChanged, "publish");
                let clip = this.mc;
                clip.fps = 60;

                RS.Tests.simulateTime(10);

                chai.expect(clip.onChanged.publish.called).to.equal(false);
                clip.onChanged.publish.restore();
            });
        });

        describe("currentFrame", function ()
        {
            it("should default to 0", function ()
            {
                chai.expect(this.mc.currentFrame).to.equal(0);
            });

            it("not be settable", function ()
            {
                this.mc.currentFrame = 3;
                chai.expect(this.mc.currentFrame).to.equal(0);
            });

            it("should change when frame does", function ()
            {
                this.mc.gotoAndStop(5);
                chai.expect(this.mc.currentFrame).to.equal(5);
            });

            it("should wrap round", function ()
            {
                this.mc.gotoAndStop(11);
                chai.expect(this.mc.currentFrame).to.equal(1);
            });
        });

        describe("totalFrames", function ()
        {
            it("should be correct", function ()
            {
                chai.expect(this.mc.totalFrames).to.equal(10);
            });

            it("should not be settable", function ()
            {
                this.mc.totalFrames = 30;
                chai.expect(this.mc.totalFrames).to.equal(10);
            });
        });

        describe("paused", function ()
        {
            it("should default to true", function ()
            {
                chai.expect(this.mc.paused).to.equal(true);
            });

            it("should not be settable", function ()
            {
                this.mc.paused = false;
                chai.expect(this.mc.paused).to.equal(true);
            });

            it("should be false when played", function ()
            {
                chai.expect(this.mc.paused).to.equal(true);
                this.mc.play();
                chai.expect(this.mc.paused).to.equal(false);
            });

            it("should be false when gotoAndPlay used", function ()
            {
                chai.expect(this.mc.paused).to.equal(true);
                this.mc.gotoAndPlay(10);
                chai.expect(this.mc.paused).to.equal(false);
            });

            it("should be true when stopped", function ()
            {
                chai.expect(this.mc.paused).to.equal(true);
                this.mc.play();
                this.mc.stop();
                chai.expect(this.mc.paused).to.equal(true);
            });

            it("should be true when gotoAndStop used", function ()
            {
                chai.expect(this.mc.paused).to.equal(true);
                this.mc.play();
                this.mc.gotoAndStop(5);
                chai.expect(this.mc.paused).to.equal(true);
            });
        });

        describe("gotoAndStop", function ()
        {
            it("should change the frame", function ()
            {
                this.mc.gotoAndStop(5);
                chai.expect(this.mc.currentFrame).to.equal(5);
            });
        });

        describe("play", function ()
        {
            it("should play the clip", function ()
            {
                sinon.spy(this.mc.onAnimationEnded, "publish");
                let clip = this.mc;
                let parent = new RS.Rendering.DisplayObjectContainer();
                parent.addChild(clip);
                clip.play()
                chai.expect(this.mc.currentFrame).to.equal(0);

                RS.Tests.simulateTime(50);

                chai.expect(clip.currentFrame).to.not.equal(0);
            });
        });

        describe("stop", function ()
        {
            it("should stop the clip", function ()
            {
                sinon.spy(this.mc.onAnimationEnded, "publish");
                let clip = this.mc;
                let frame = 0;
                clip.play()
                chai.expect(this.mc.currentFrame).to.equal(0);

                RS.Tests.simulateTime(50);

                clip.stop();
                frame = clip.currentFrame;

                RS.Tests.simulateTime(50);

                chai.expect(clip.currentFrame).to.equal(frame);
            });
        });

        describe("gotoAndPlay", function ()
        {
            it("should change the frame", function ()
            {
                sinon.spy(this.mc.onAnimationEnded, "publish");
                let clip = this.mc;
                let parent = new RS.Rendering.DisplayObjectContainer();
                parent.addChild(clip);
                clip.gotoAndPlay(5);
                chai.expect(this.mc.currentFrame).to.equal(5);
            });

            it("should play the clip", function ()
            {
                sinon.spy(this.mc.onAnimationEnded, "publish");
                let clip = this.mc;
                let parent = new RS.Rendering.DisplayObjectContainer();
                parent.addChild(clip);
                clip.gotoAndPlay(5);
                chai.expect(this.mc.currentFrame).to.equal(5);

                RS.Tests.simulateTime(50);

                chai.expect(clip.currentFrame).to.not.equal(5);
            });
        });

        describe("clone", function ()
        {
            it("copy the Movieclip", function ()
            {
                const newMc = this.mc.clone();
                chai.expect(newMc.currentFrame).to.equal(0);
                chai.expect(newMc.totalFrames).to.equal(10);
            });
        });
    });

    function createMc()
    {
        let asset = new RS.Asset.ImageAsset({
            id: "",
            group: "",
            path: "",
            raw: {},
            type: "",
            formats: [],
            exclude: []
        });
        return new RS.Rendering.MovieClip([asset], 60);
    }

    function createLongMc()
    {
        let asset = new RS.Asset.ImageAsset({
            id: "",
            group: "",
            path: "",
            raw: {},
            type: "",
            formats: [],
            exclude: []
        });
        let assetList = [];
        for (let i = 0; i < 10; i++)
        {
            assetList.push(asset);
        }
        return new RS.Rendering.MovieClip(assetList, 60);
    }
}