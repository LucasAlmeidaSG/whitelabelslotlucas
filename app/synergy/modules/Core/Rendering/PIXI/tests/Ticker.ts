namespace RS.Tests
{
    export function initTicker(): void
    {
        const ticker = RS.ITicker.get();
        ticker.enabled = false;
    }

    export function simulateTime(time: number): void
    {
        const ticker = RS.ITicker.get();
        const frames = Math.max(1, Math.floor(time / ticker.targetTimestep));
        const frameTime = time / frames;
        for (let i = 0; i < frames; ++i)
        {
            ticker.advance(frameTime);
        }
    }
}