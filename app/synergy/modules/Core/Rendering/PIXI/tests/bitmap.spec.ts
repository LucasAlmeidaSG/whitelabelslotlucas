/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("Bitmap.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });

        describe("constructor", function ()
        {
            it("should not fail with no paramater", function ()
            {
                let isFail = false;
                try
                {
                    let bitmap = new RS.Rendering.Bitmap(undefined);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should accept a Frame as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const frame: RS.Rendering.Frame = {
                        /** The image object in which the frame is found. */
                        imageData: new RS.Rendering.SubTexture(new RS.Rendering.Texture(""), new RS.Rendering.ReadonlyRectangle(0, 0, 0, 0)),
                        imageID: 1,
                        imageRect: null,
                        reg: null,
                        untrimmedSize: null,
                        trimRect: null
                    };
                    let ob = new RS.Rendering.Bitmap(frame);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should accept an IAsset as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const asset = new RS.Asset.ImageAsset({
                        id: "",
                        group: "",
                        path: "",
                        raw: {},
                        type: "",
                        formats: [],
                        exclude: []
                    });
                    const bitmap = new RS.Rendering.Bitmap(asset);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should accept a SubTexture as a paramater", function ()
            {
                let isFail = false;
                try
                {
                    const subTexture = new RS.Rendering.SubTexture(new RS.Rendering.Texture(""), new RS.Rendering.ReadonlyRectangle(0, 0, 0, 0));
                    let ob = new RS.Rendering.Bitmap(subTexture);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });
        });

        Core.misc(createBitmap);
        Core.position(createBitmap);
        Core.rotation(createBitmap);
        Core.dimensions(createBitmap, 0, 1, 0, 1);
        Core.scale(createBitmap);
        Core.transform(createBitmap);
        Core.visibility(createBitmap);
        Core.interactivity(createBitmap, false);
        Core.masking(createBitmap);
        Core.offsets(createBitmap, false);
        Core.children(createBitmap, false);
        Core.displayList(createBitmap, false);
        Core.updates(createBitmap);
        Core.dispose(createBitmap, false);
        //corePrePost(createBitmap);
    });

    function createBitmap()
    {
        let asset = new RS.Asset.ImageAsset({
            id: "",
            group: "",
            path: "",
            raw: {},
            type: "",
            formats: [],
            exclude: []
        })
        return new RS.Rendering.Bitmap(asset);
    }
}