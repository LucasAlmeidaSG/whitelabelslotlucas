/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("Graphics.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });

        Core.misc(createGraphics);
        Core.position(createGraphics);
        Core.rotation(createGraphics);
        Core.dimensions(createGraphicsWithSize, 0, 1, 0, 1, 1, 1);
        Core.scale(createGraphics);
        Core.transform(createGraphics);
        Core.visibility(createGraphics);
        Core.interactivity(createGraphics, false);
        Core.masking(createGraphics);
        Core.offsets(createGraphics, false);
        Core.children(createGraphics, false);
        Core.displayList(createGraphics, false);
        Core.updates(createGraphics);
        Core.dispose(createGraphics, false);
        //corePrePost(createGraphics);
        beforeEach(function ()
        {
            this.graphics = createGraphics();
        });

        afterEach(function ()
        {
            const graphics = this.graphics as RS.Rendering.IGraphics;
            graphics.dispose();
            this.graphics = null;
        });

        describe("lineStyle", function ()
        {
            it("should work with no parameters", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle();
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should work with 1 param", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle(1);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should work with 2 params", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should work with 3 params", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0), .5);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should work with 4 params", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0), .5, "");
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should work with 5 params", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0), .5, "", "");
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });

            it("should work with 6 params", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0), .5, "", "", "");
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(false);
            });
        });

        describe("moveTo", function ()
        {
            it("should accept x/y", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);
                this.graphics.moveTo(100, 100);
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);
            });

            it("should accept Vector2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);
                this.graphics.moveTo(RS.Math.Vector2D(1, 1));
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);
            });
        });

        describe("lineTo", function ()
        {
            it("should fail without moveTo x/y", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineTo(1, 1);
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(true);
            });

            it("should fail without moveTo Vector2D", function ()
            {
                let isFail = false;
                try
                {
                    this.graphics.lineTo(RS.Math.Vector2D(0, 100));
                } catch (error)
                {
                    isFail = true;
                }
                chai.expect(isFail).to.equal(true);
            });

            it("should accept x/y", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);
                this.graphics.moveTo(0, 0);
                this.graphics.lineTo(1, 1);
                chai.expect(this.graphics.width).to.equal(1);
                chai.expect(this.graphics.height).to.equal(1);

                this.graphics.clear();

                this.graphics.lineStyle(5, new RS.Util.Color(0, 0, 0), 1);
                this.graphics.moveTo(0, 0);
                this.graphics.lineTo(0, 100);
                chai.expect(this.graphics.width).to.equal(5);
                chai.expect(this.graphics.height).to.equal(100);
            });

            it("should accept Vector2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);
                this.graphics.moveTo(RS.Math.Vector2D(0, 0));
                this.graphics.lineTo(RS.Math.Vector2D(1, 1));
                chai.expect(this.graphics.width).to.equal(1);
                chai.expect(this.graphics.height).to.equal(1);

                this.graphics.clear();

                this.graphics.lineStyle(3, new RS.Util.Color(0, 0, 0));
                this.graphics.moveTo(RS.Math.Vector2D(0, 100));
                this.graphics.lineTo(RS.Math.Vector2D(0, 200));
                chai.expect(this.graphics.width).to.equal(3);
                chai.expect(this.graphics.height).to.equal(100);
            });
        });

        describe("quadraticCurveTo", function ()
        {
            it("should accept x/y", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.quadraticCurveTo(1, 1, 2, 2);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.quadraticCurveTo(1, 1, 2, 2);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });

            it("should accept Vector2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.quadraticCurveTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2));

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.quadraticCurveTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2));

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });
        });

        describe("bezierCurveTo", function ()
        {
            it("should accept x/y", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.bezierCurveTo(1, 1, 2, 2, 3, 3);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.bezierCurveTo(1, 1, 2, 2, 3, 3);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });

            it("should accept Vector2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.bezierCurveTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2), RS.Math.Vector2D(3, 3));

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.bezierCurveTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2), RS.Math.Vector2D(3, 3));

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });
        });

        describe("arcTo", function ()
        {
            it("should accept x/y", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.moveTo(0, 0);
                this.graphics.arcTo(1, 1, 2, 2, 5);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.moveTo(0, 0);
                this.graphics.arcTo(1, 1, 2, 2, 5);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });

            it("should accept Vector2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.moveTo(0, 0);
                this.graphics.arcTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2), 5);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.moveTo(0, 0);
                this.graphics.arcTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2), 5);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });
        });

        describe("arc", function ()
        {
            it("should accept x/y", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.arc(1, 1, 5, 2, 7);
                this.graphics.arc(1, 1, 5, 2, 7, true);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.arc(1, 1, 5, 2, 7);
                this.graphics.arc(1, 1, 5, 2, 7, true);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });

            it("should accept Vector2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.arc(RS.Math.Vector2D(1, 1), 5, 2, 7);
                this.graphics.arc(RS.Math.Vector2D(1, 1), 5, 2, 7, true);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.arc(RS.Math.Vector2D(1, 1), 5, 2, 7);
                this.graphics.arc(RS.Math.Vector2D(1, 1), 5, 2, 7, true);

                chai.expect(this.graphics.width).to.not.equal(0);
                chai.expect(this.graphics.height).to.not.equal(0);
            });
        });

        describe("drawRect", function ()
        {
            it("should accept x/y/w/h", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawRect(0, 0, 100, 100);

                chai.expect(this.graphics.width).to.be.within(100, 102);
                chai.expect(this.graphics.height).to.be.within(100, 102);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawRect(100, 100, 200, 200);

                chai.expect(this.graphics.width).to.be.within(200, 202);
                chai.expect(this.graphics.height).to.be.within(200, 202);
            });

            it("should accept Vector2D/Size2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawRect(RS.Math.Vector2D(10, 10), RS.Math.Size2D(90, 90));

                chai.expect(this.graphics.width).to.be.within(90, 91);
                chai.expect(this.graphics.height).to.be.within(90, 91);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawRect(RS.Math.Vector2D(10, 10), RS.Math.Size2D(90, 90));

                chai.expect(this.graphics.width).to.be.within(90, 91);
                chai.expect(this.graphics.height).to.be.within(90, 91);
            });

            it("should accept Rectangle", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawRect(new RS.Rendering.Rectangle(0, 0, 100, 100));

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawRect(new RS.Rendering.Rectangle(0, 0, 100, 100));

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);
            });
        });

        describe("drawRoundedRect", function ()
        {
            it("should accept x/y/w/h", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawRoundedRect(0, 0, 100, 100, 5);

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawRoundedRect(0, 0, 100, 100, 5);

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);
            });

            it("should accept Vector2D/Size2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawRoundedRect(RS.Math.Vector2D(10, 10), RS.Math.Size2D(90, 90), 8);

                chai.expect(this.graphics.width).to.be.within(90, 91);
                chai.expect(this.graphics.height).to.be.within(90, 91);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawRoundedRect(RS.Math.Vector2D(10, 10), RS.Math.Size2D(90, 90), 8);

                chai.expect(this.graphics.width).to.be.within(90, 91);
                chai.expect(this.graphics.height).to.be.within(90, 91);
            });

            it("should accept Rectangle", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawRoundedRect(new RS.Rendering.Rectangle(0, 0, 100, 100), 10);

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawRoundedRect(new RS.Rendering.Rectangle(0, 0, 100, 100), 10);

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);
            });
        });

        describe("drawCircle", function ()
        {
            it("should accept x/y/r", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawCircle(0, 0, 100);

                chai.expect(this.graphics.width).to.be.within(200, 201);
                chai.expect(this.graphics.height).to.be.within(200, 201);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawCircle(0, 0, 100);

                chai.expect(this.graphics.width).to.be.within(200, 201);
                chai.expect(this.graphics.height).to.be.within(200, 201);
            });

            it("should accept Vector2D/r", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawCircle(0, 0, 100);

                chai.expect(this.graphics.width).to.be.within(200, 201);
                chai.expect(this.graphics.height).to.be.within(200, 201);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawCircle(RS.Math.Vector2D(200, 200), 100);

                chai.expect(this.graphics.width).to.be.within(200, 201);
                chai.expect(this.graphics.height).to.be.within(200, 201);
            });
        });

        describe("drawEllipse", function ()
        {
            it("should accept x/y/w/h", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawEllipse(0, 0, 100, 100);

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawEllipse(0, 0, 100, 100);

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);
            });

            it("should accept Vector2D/Size2D", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawEllipse(RS.Math.Vector2D(10, 10), RS.Math.Size2D(90, 90));

                chai.expect(this.graphics.width).to.be.within(90, 91);
                chai.expect(this.graphics.height).to.be.within(90, 91);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawEllipse(RS.Math.Vector2D(10, 10), RS.Math.Size2D(90, 90));

                chai.expect(this.graphics.width).to.be.within(90, 91);
                chai.expect(this.graphics.height).to.be.within(90, 91);
            });

            it("should accept Rectangle", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.drawEllipse(new RS.Rendering.Rectangle(0, 0, 100, 100));

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawEllipse(new RS.Rendering.Rectangle(0, 0, 100, 100));

                chai.expect(this.graphics.width).to.be.within(100, 101);
                chai.expect(this.graphics.height).to.be.within(100, 101);
            });
        });

        describe("drawPolygon", function ()
        {
            it("should accept Math.Vector2D[]", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                const list = [
                    RS.Math.Vector2D(0, 0),
                    RS.Math.Vector2D(90, 90),
                    RS.Math.Vector2D(-90, 90),
                    RS.Math.Vector2D(0, 0)
                ];
                this.graphics.drawPolygon(list);

                chai.expect(this.graphics.width).to.be.within(180, 181);
                chai.expect(this.graphics.height).to.be.within(90, 91);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawPolygon(list);

                chai.expect(this.graphics.width).to.be.within(180, 181);
                chai.expect(this.graphics.height).to.be.within(90, 91);
            });

            it("should accept Math.Vector2DList", function ()
            {
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                const list = [
                    RS.Math.Vector2D(0, 0),
                    RS.Math.Vector2D(90, 90),
                    RS.Math.Vector2D(-90, 90),
                    RS.Math.Vector2D(0, 0)
                ];
                this.graphics.drawPolygon(new RS.Math.Vector2DList(list));

                chai.expect(this.graphics.width).to.be.within(180, 181);
                chai.expect(this.graphics.height).to.be.within(90, 91);

                this.graphics.clear();
                chai.expect(this.graphics.width).to.equal(0);
                chai.expect(this.graphics.height).to.equal(0);

                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawPolygon(new RS.Math.Vector2DList(list));

                chai.expect(this.graphics.width).to.be.within(180, 181);
                chai.expect(this.graphics.height).to.be.within(90, 91);
            });
        });

        describe("clear", function ()
        {
            it("should work with no operations", function ()
            {
                this.graphics.clear();
            });

            it("should work after operations", function ()
            {
                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawEllipse(0, 0, 100, 100);
                this.graphics.clear();
            });
        });

        describe("containsPoint", function ()
        {
            it("should not with no operations", function ()
            {
                chai.expect(this.graphics.containsPoint(0, 0)).to.equal(false);
            });

            it("should work after operations", function ()
            {
                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.lineStyle(1, new RS.Util.Color(0, 0, 0));
                this.graphics.drawEllipse(0, 0, 100, 100);
                this.graphics.endFill();

                chai.expect(this.graphics.containsPoint(0, 0)).to.equal(true);
                chai.expect(this.graphics.containsPoint(110, 110)).to.equal(false);
            });
        });

        describe("beginFill / endFill", function ()
        {
            it("should work with 1 value", function ()
            {
                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.arc(1, 1, 5, 2, 7);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.arcTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2), 5);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.bezierCurveTo(1, 1, 2, 2, 3, 3);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.quadraticCurveTo(1, 1, 2, 2);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.moveTo(1, 1);
                this.graphics.lineTo(1, 1);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.drawRect(new RS.Rendering.Rectangle(0, 0, 100, 100));
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100));
                this.graphics.drawRoundedRect(new RS.Rendering.Rectangle(0, 0, 100, 100), 7);
                this.graphics.endFill();
                this.graphics.clear();

            });

            it("should work with 2 values", function ()
            {
                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.arc(1, 1, 5, 2, 7);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.arcTo(RS.Math.Vector2D(1, 1), RS.Math.Vector2D(2, 2), 5);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.bezierCurveTo(1, 1, 2, 2, 3, 3);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.quadraticCurveTo(1, 1, 2, 2);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.moveTo(1, 1);
                this.graphics.lineTo(1, 1);
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.drawRect(new RS.Rendering.Rectangle(0, 0, 100, 100));
                this.graphics.endFill();
                this.graphics.clear();

                this.graphics.beginFill(new RS.Util.Color(100, 100, 100), .5);
                this.graphics.drawRoundedRect(new RS.Rendering.Rectangle(0, 0, 100, 100), 7);
                this.graphics.endFill();
                this.graphics.clear();
            });
        });
    });

    function createGraphics()
    {
        return new RS.Rendering.Graphics();
    }

    function createGraphicsWithSize()
    {
        const g = new RS.Rendering.Graphics();
        g.drawRect(1, 1, 1, 1);
        return g;
    }
}