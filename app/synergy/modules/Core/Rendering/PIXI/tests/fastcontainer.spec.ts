/// <reference path="./Core/core.spec.ts"/>
namespace RS.Rendering.Pixi.Tests
{
    describe("FastContainer.ts", function ()
    {
        before(function ()
        {
            RS.Tests.initTicker();
        });

        Core.misc(createFastContainer);
        Core.position(createFastContainer);
        Core.rotation(createFastContainer);
        // Core.dimensions(createDisplayObContainerWithSize, 0, 1, 0, 1);
        Core.scale(createFastContainer);
        Core.transform(createFastContainer);
        Core.visibility(createFastContainer);
        Core.interactivity(createFastContainer, false);
        Core.masking(createFastContainer);
        Core.offsets(createFastContainer, false);
        Core.children(createFastContainer, true);
        Core.displayList(createFastContainer, true);
        Core.updates(createFastContainer);
        Core.dispose(createFastContainer, true);
        //corePrePost(createBitmap);
    });

    function createFastContainer()
    {
        return new RS.Rendering.FastContainer({});
    }
}