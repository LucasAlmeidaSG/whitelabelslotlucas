/// <reference path="../IFactory.ts" />
/// <reference path="../Factory.ts" />

namespace RS.Bootstrap
{
    /**
     * Wraps a Bootstrap DOM component.
     */
    export class Component<T extends HTMLElement = HTMLElement> extends DOM.Component<T> implements IFactory
    {
        /** Gets a jQuery selector for this component. */
        public get $() { return $(this._element); }

        /** Gets or sets the tooltip for this component. */
        public get tooltip() { return this.attr("title"); }
        public set tooltip(value)
        {
            if (value)
            {
                this.attr("title", value);
                this.attr("rel", "tooltip");
                this.$.tooltip({ delay: { show: 500, hide: 0 }, trigger: "hover" });
                this.$.on("click", (ev) =>
                {
                    this.$.tooltip("hide");
                });
            }
            else
            {
                this.attr("title", "");
                this.attr("rel", "");
            }
        }

        public constructor(elementName: string, classList?: string[], cssID?: string)
        {
            const el = document.createElement(elementName);
            if (cssID) { el.id = cssID; }
            if (classList)
            {
                for (const className of classList)
                {
                    el.classList.add(className);
                }
            }
            super(el as T);
        }

        // #region IFactory

        public row(...extraClasses: string[]): Component<HTMLDivElement>
        {
            const cmp = factory.row(...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public col(width: number, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const cmp = factory.col(width, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public button(kind?: ButtonKind, size?: ButtonSize, ...extraClasses: string[]): Component<HTMLButtonElement>
        {
            const cmp = factory.button(kind, size, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public buttonGroup(ariaLabel?: string, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const cmp = factory.buttonGroup(ariaLabel, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public buttonToolbar(ariaLabel?: string, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const cmp = factory.buttonToolbar(ariaLabel, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public paragraph(...extraClasses: string[]): Component<HTMLParagraphElement>
        {
            const cmp = factory.paragraph(...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public panel(settings?: Partial<IFactory.PanelSettings>, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const cmp = factory.panel(settings, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public icon(kind: Icon, tooltip?: string, ...extraClasses: string[]): Component<HTMLSpanElement>
        {
            const cmp = factory.icon(kind, tooltip, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public badge(value: string | number, ...extraClasses: string[]): Component<HTMLSpanElement>
        {
            const cmp = factory.badge(value, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }
        public alert(kind: AlertKind, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const cmp = factory.alert(kind, ...extraClasses);
            this.addChild(cmp);
            return cmp;
        }

        /** @inheritdoc */
        public dispose(): void
        {
            if (this.isDisposed) { return; }
            if (this.tooltip) { this.$.tooltip("dispose"); }
            super.dispose();
        }

        /**
         * Adds the default classes to this component.
         */
        protected addDefaultClasses(): void
        {
            // Do nothing as we don't want rs-cmp
            return;
        }

        // #endregion
    }
}