/// <reference path="IFactory.ts" />

namespace RS.Bootstrap
{
    class Factory implements IFactory
    {
        public row(...extraClasses: string[]): Component<HTMLDivElement>
        {
            return new Component<HTMLDivElement>("div", [ "row", ...extraClasses ]);
        }
        public col(width: number, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            return new Component<HTMLDivElement>("div", [ `col-${width}`, ...extraClasses ]);
        }
        public button(kind: ButtonKind = ButtonKind.Primary, size: ButtonSize = ButtonSize.Default, ...extraClasses: string[]): Component<HTMLButtonElement>
        {
            const btn = new Component<HTMLButtonElement>("button", [ "btn", ButtonKind.toClass(kind), ...extraClasses ]);
            if (size !== ButtonSize.Default) { btn.addClass(ButtonSize.toClass(size)); }
            btn.attr("type", "button");
            return btn;
        }
        public buttonGroup(ariaLabel?: string, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const grp = new Component<HTMLDivElement>("div", [ "btn-group", ...extraClasses ]);
            grp.attr("role", "group");
            if (ariaLabel != null) { grp.attr("aria-label", ariaLabel); }
            return grp;
        }
        public buttonToolbar(ariaLabel?: string, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const tb = new Component<HTMLDivElement>("div", [ "btn-toolbar", ...extraClasses ]);
            tb.attr("role", "toolbar");
            if (ariaLabel != null) { tb.attr("aria-label", ariaLabel); }
            return tb;
        }
        public paragraph(...extraClasses: string[]): Component<HTMLParagraphElement>
        {
            return new Component<HTMLParagraphElement>("p", extraClasses);
        }

        public panel(settings?: Partial<IFactory.PanelSettings>, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const panel = new Component<HTMLDivElement>("div", [ "card", ...extraClasses ]);
            if (settings && settings.headerContent != null)
            {
                const header = new Component<HTMLDivElement>("div", [ "card-header" ]);
                header.HTML = settings.headerContent;
                if (settings.headerBadge != null)
                {
                    header.addChild(this.badge(settings.headerBadge.kind, settings.headerBadge.value, "float-right"));
                }
                panel.addChild(header);
            }
            if (settings && settings.bodyContent != null)
            {
                const body = new Component<HTMLDivElement>("div", [ "card-body" ]);
                body.HTML = settings.bodyContent;
                panel.addChild(body);
            }
            if (settings && settings.footerContent != null)
            {
                const footer = new Component<HTMLDivElement>("div", [ "card-footer" ]);
                footer.HTML = settings.footerContent;
                panel.addChild(footer);
            }
            return panel;
        }

        public icon(kind: Icon, tooltip?: string, ...extraClasses: string[]): Component<HTMLSpanElement>
        {
            const icon = new Component<HTMLSpanElement>("span", [ "oi", `oi-${kind}`, ...extraClasses ]);
            if (tooltip != null)
            {
                icon.attr("data-toggle", "tooltip");
                icon.attr("title", tooltip);
            }
            return icon;
        }

        public badge(kind: BadgeKind, value: string | number, ...extraClasses: string[]): Component<HTMLSpanElement>
        {
            const badge = new Component<HTMLSpanElement>("span", [ "badge", BadgeKind.toClass(kind), ...extraClasses ]);
            badge.text = `${value}`;
            return badge;
        }

        public alert(kind: AlertKind, ...extraClasses: string[]): Component<HTMLDivElement>
        {
            const alert = new Component<HTMLDivElement>("div", [ "alert", AlertKind.toClass(kind), ...extraClasses ]);
            alert.attr("role", "alert");
            return alert;
        }

    }

    export const factory: IFactory = new Factory();
}