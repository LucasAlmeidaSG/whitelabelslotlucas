namespace RS.Bootstrap
{
    export interface IFactory
    {
        row(...extraClasses: string[]): Component<HTMLDivElement>;
        col(width: number, ...extraClasses: string[]): Component<HTMLDivElement>;
        button(kind?: ButtonKind, size?: ButtonSize, ...extraClasses: string[]): Component<HTMLButtonElement>;
        buttonGroup(ariaLabel?: string, ...extraClasses: string[]): Component<HTMLDivElement>;
        buttonToolbar(ariaLabel?: string, ...extraClasses: string[]): Component<HTMLDivElement>;
        paragraph(...extraClasses: string[]): Component<HTMLParagraphElement>;
        panel(settings?: Partial<IFactory.PanelSettings>, ...extraClasses: string[]): Component<HTMLDivElement>;
        icon(kind: Icon, tooltip?: string, ...extraClasses: string[]): Component<HTMLSpanElement>;
        badge(value: string | number, ...extraClasses: string[]): Component<HTMLSpanElement>;
        alert(kind: AlertKind, ...extraClasses: string[]): Component<HTMLDivElement>;
    }

    export namespace IFactory
    {
        export interface PanelSettings
        {
            headerContent: string;
            headerBadge:
            {
                kind: BadgeKind;
                value: string | number;
            };
            bodyContent: string;
            footerContent: string;
        }
    }

    export enum ButtonKind
    {
        Primary, Secondary, Success, Danger, Warning, Info, Light, Dark,
        OutlinePrimary, OutlineSecondary, OutlineSuccess, OutlineDanger, OutlineWarning, OutlineInfo, OutlineLight, OutlineDark
    }

    export namespace ButtonKind
    {
        export function toClass(kind: ButtonKind)
        {
            switch (kind)
            {
                case ButtonKind.Primary: return "btn-primary";
                case ButtonKind.Secondary: return "btn-secondary";
                case ButtonKind.Success: return "btn-success";
                case ButtonKind.Danger: return "btn-danger";
                case ButtonKind.Warning: return "btn-warning";
                case ButtonKind.Info: return "btn-info";
                case ButtonKind.Light: return "btn-light";
                case ButtonKind.Dark: return "btn-dark";
                case ButtonKind.OutlinePrimary: return "btn-outline-primary";
                case ButtonKind.OutlineSecondary: return "btn-outline-secondary";
                case ButtonKind.OutlineSuccess: return "btn-outline-success";
                case ButtonKind.OutlineDanger: return "btn-outline-danger";
                case ButtonKind.OutlineWarning: return "btn-outline-warning";
                case ButtonKind.OutlineInfo: return "btn-outline-info";
                case ButtonKind.OutlineLight: return "btn-outline-light";
                case ButtonKind.OutlineDark: return "btn-outline-dark";
                default: return "btn-light";
            }
        }
    }

    export enum BadgeKind { Primary, Secondary, Success, Danger, Warning, Info, Light, Dark }

    export namespace BadgeKind
    {
        export function toClass(kind: BadgeKind)
        {
            switch (kind)
            {
                case BadgeKind.Primary: return "badge-primary";
                case BadgeKind.Secondary: return "badge-secondary";
                case BadgeKind.Success: return "badge-success";
                case BadgeKind.Danger: return "badge-danger";
                case BadgeKind.Warning: return "badge-warning";
                case BadgeKind.Info: return "badge-info";
                case BadgeKind.Light: return "badge-light";
                case BadgeKind.Dark: return "badge-dark";
                default: return "badge-light";
            }
        }
    }

    export enum ButtonSize { Default, Large, Small, Block }

    export namespace ButtonSize
    {
        export function toClass(size: ButtonSize)
        {
            switch (size)
            {
                case ButtonSize.Default: return "";
                case ButtonSize.Large: return "btn-lg";
                case ButtonSize.Small: return "btn-sm";
                case ButtonSize.Block: return "btn-block";
                default: return "";
            }
        }
    }

    export enum AlertKind { Primary, Secondary, Success, Danger, Warning, Info, Light, Dark }

    export namespace AlertKind
    {
        export function toClass(kind: AlertKind)
        {
            switch (kind)
            {
                case AlertKind.Primary: return "alert-primary";
                case AlertKind.Secondary: return "alert-secondary";
                case AlertKind.Success: return "alert-success";
                case AlertKind.Danger: return "alert-danger";
                case AlertKind.Warning: return "alert-warning";
                case AlertKind.Info: return "alert-info";
                case AlertKind.Light: return "alert-light";
                case AlertKind.Dark: return "alert-dark";
                default: return "";
            }
        }
    }

    export enum Icon
    {
        AccountLogin = "account-login",
        AccountLogout = "account-logout",
        ActionRedo = "action-redo",
        ActionUndo = "action-undo",
        AlignCenter = "align-center",
        AlignLeft = "align-left",
        AlignRight = "align-right",
        Aperture = "aperture",
        ArrowBottom = "arrow-bottom",
        ArrowCircleBottom = "arrow-circle-bottom",
        ArrowCircleLeft = "arrow-circle-left",
        ArrowCircleRight = "arrow-circle-right",
        ArrowCircleTop = "arrow-circle-top",
        ArrowLeft = "arrow-left",
        ArrowRight = "arrow-right",
        ArrowThickBottom = "arrow-thick-bottom",
        ArrowThickLeft = "arrow-thick-left",
        ArrowThickRight = "arrow-thick-right",
        ArrowThickTop = "arrow-thick-top",
        ArrowTop = "arrow-top",
        AudioSpectrum = "audio-spectrum",
        Audio = "audio",
        Badge = "badge",
        Ban = "ban",
        BarChart = "bar-chart",
        Basket = "basket",
        BatteryEmpty = "battery-empty",
        BatteryFull = "battery-full",
        Beaker = "beaker",
        Bell = "bell",
        Bluetooth = "bluetooth",
        Bold = "bold",
        Bolt = "bolt",
        Book = "book",
        Bookmark = "bookmark",
        Box = "box",
        Briefcase = "briefcase",
        BritishPound = "british-pound",
        Browser = "browser",
        Brush = "brush",
        Bug = "bug",
        Bullhorn = "bullhorn",
        Calculator = "calculator",
        Calendar = "calendar",
        CameraSLR = "camera-slr",
        CaretBottom = "caret-bottom",
        CaretLeft = "caret-left",
        CaretRight = "caret-right",
        CaretTop = "caret-top",
        Cart = "cart",
        Chat = "chat",
        Check = "check",
        ChevronBottom = "chevron-bottom",
        ChevronLeft = "chevron-left",
        ChevronRight = "chevron-right",
        ChevronTop = "chevron-top",
        CircleCheck = "circle-check",
        CircleX = "circle-x",
        Clipboard = "clipboard",
        Clock = "clock",
        CloudDownload = "cloud-download",
        CloudUpload = "cloud-upload",
        Cloud = "cloud",
        Cloudy = "cloudy",
        Code = "code",
        Cog = "cog",
        CollapseDown = "collapse-down",
        CollapseLeft = "collapse-left",
        CollapseRight = "collapse-right",
        CollapseUp = "collapse-up",
        Command = "command",
        CommentSquare = "comment-square",
        Compass = "compass",
        Contrast = "contrast",
        Copywriting = "copywriting",
        CreditCard = "credit-card",
        Crop = "crop",
        Dashboard = "dashboard",
        DataTransferDownload = "data-transfer-download",
        DataTransferUpload = "data-transfer-upload",
        Delete = "delete",
        Dial = "dial",
        Document = "document",
        Dollar = "dollar",
        DoubleQuoteSansLeft = "double-quote-sans-left",
        DoubleQuoteSansRight = "double-quote-sans-right",
        DoubleQuoteSerifLeft = "double-quote-serif-left",
        DoubleQuoteSerifRight = "double-quote-serif-right",
        Droplet = "droplet",
        Eject = "eject",
        Elevator = "elevator",
        Ellipses = "ellipses",
        EnvelopeClosed = "envelope-closed",
        EnvelopeOpen = "envelope-open",
        Euro = "euro",
        Excerpt = "excerpt",
        ExpandDown = "expand-down",
        ExpandLeft = "expand-left",
        ExpandRight = "expand-right",
        ExpandUp = "expand-up",
        ExternalLink = "external-link",
        Eye = "eye",
        Eyedropper = "eyedropper",
        File = "file",
        Fire = "fire",
        Lag = "flag",
        Flash = "flash",
        Folder = "folder",
        Fork = "fork",
        FullscreenEnter = "fullscreen-enter",
        FullscreenExit = "fullscreen-exit",
        Globe = "globe",
        Graph = "graph",
        GridFourUp = "grid-four-up",
        GridThreeUp = "grid-three-up",
        GridTwoUp = "grid-two-up",
        HardDrive = "hard-drive",
        Header = "header",
        Headphones = "headphones",
        Heart = "heart",
        Home = "home",
        Image = "image",
        Inbox = "inbox",
        Infinity = "infinity",
        Info = "info",
        Italic = "italic",
        JustifyCenter = "justify-center",
        JustifyLeft = "justify-left",
        JustifyRight = "justify-right",
        Key = "key",
        Laptop = "laptop",
        Layers = "layers",
        Lightbulb = "lightbulb",
        LinkBroken = "link-broken",
        LinkIntact = "link-intact",
        ListRich = "list-rich",
        List = "list",
        Location = "location",
        LockLocked = "lock-locked",
        LockUnlocked = "lock-unlocked",
        LoopCircular = "loop-circular",
        LoopSquare = "loop-square",
        Loop = "loop",
        MagnifyingGlass = "magnifying-glass",
        MapMarker = "map-marker",
        Map = "map",
        MediaPause = "media-pause",
        MediaPlay = "media-play",
        MediaRecord = "media-record",
        MediaSkipBackward = "media-skip-backward",
        MediaSkipForward = "media-skip-forward",
        MediaStepBackward = "media-step-backward",
        MediaStepForward = "media-step-forward",
        MediaStop = "media-stop",
        MedicalCross = "medical-cross",
        Menu = "menu",
        Microphone = "microphone",
        Minus = "minus",
        Monitor = "monitor",
        Moon = "moon",
        Move = "move",
        MusicalNote = "musical-note",
        Paperclip = "paperclip",
        Pencil = "pencil",
        People = "people",
        Person = "person",
        Phone = "phone",
        PieChart = "pie-chart",
        Pin = "pin",
        PlayCircle = "play-circle",
        Plus = "plus",
        PowerStandby = "power-standby",
        Print = "print",
        Project = "project",
        Pulse = "pulse",
        PuzzlePiece = "puzzle-piece",
        QuestionMark = "question-mark",
        Rain = "rain",
        Random = "random",
        Reload = "reload",
        ResizeBoth = "resize-both",
        ResizeHeight = "resize-height",
        ResizeWidth = "resize-width",
        RssAlt = "rss-alt",
        Rss = "rss",
        Script = "script",
        ShareBoxed = "share-boxed",
        Share = "share",
        Shield = "shield",
        Signal = "signal",
        Signpost = "signpost",
        SortAscending = "sort-ascending",
        SortDescending = "sort-descending",
        Spreadsheet = "spreadsheet",
        Star = "star",
        Sun = "sun",
        Tablet = "tablet",
        Tag = "tag",
        Tags = "tags",
        Target = "target",
        Task = "task",
        Terminal = "terminal",
        Text = "text",
        ThumbDown = "thumb-down",
        ThumbUp = "thumb-up",
        Timer = "timer",
        Transfer = "transfer",
        Trash = "trash",
        Underline = "underline",
        VerticalAlignBottom = "vertical-align-bottom",
        VerticalAlignCenter = "vertical-align-center",
        VerticalAlignTop = "vertical-align-top",
        Video = "video",
        VolumeHigh = "volume-high",
        VolumeLow = "volume-low",
        VolumeOff = "volume-off",
        Warning = "warning",
        Wifi = "wifi",
        Wrench = "wrench",
        X = "x",
        Yen = "yen",
        ZoomIn = "zoom-in",
        ZoomOut = "zoom-out"
    }
}