namespace RS.BigWin
{
    /**
     * Encapsulates a big win controller.
     */
    export interface IController extends IDisposable
    {
        /**
         * Initialises the big win controller.
         * @param runtimeData 
         */
        initialise(runtimeData: IController.RuntimeData): void;

        /**
         * Gets if the specified stake and payout constitutes a big win.
         * @param totalStake 
         * @param totalPayout 
         */
        isBigWin(totalStake: number, totalPayout: number): boolean;

        /**
         * Starts animating big win.
         * @param totalStake 
         * @param totalPayout 
         */
        do(totalStake: number, totalPayout: number): PromiseLike<void>;

        /**
         * Skips any currently animating big win.
         */
        skip(): PromiseLike<void>;
    }

    export interface ControllerConstructor extends Function
    {
        new(settings: IController.Settings): IController;
    }

    export namespace IController
    {
        export interface Settings
        {
            /** How much to dim the music volume to during big win (0-1). */
            dimVolume: number;
        }

        export interface RuntimeData
        {
            musicVolumeArbiter: Arbiter<number>;
            bigWinVolumeArbiter: Arbiter<number>;
            assetController: Asset.IController;
            viewController: RS.View.IController;
            locale: Localisation.Locale;
            currencyFormatter: Localisation.CurrencyFormatter;
        }
    }
}