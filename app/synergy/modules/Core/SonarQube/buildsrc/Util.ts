namespace RS.SonarQube
{
    export interface Config
    {
        projectName: string;
        projectKey: string; //unique to project
        serverURL: string;
        authKey: string; // authentication, could be unique to user/project
        baseDirectory?: string; // base directory to scan files from, defaults to "modules" for scanning game code
    }

    export function validateConfig(config: any): config is Config
    {
        if (config == null)
        {
            throw new Error("Core.SonarQube moduleconfig is null");
        }

        const invalidKeys = [];
        if (!Is.string(config.projectName)) { invalidKeys.push("projectName"); }
        if (!Is.string(config.projectKey)) { invalidKeys.push("projectKey"); }
        if (!Is.string(config.serverURL)) { invalidKeys.push("serverURL"); }
        if (!Is.string(config.authKey)) { invalidKeys.push("authKey"); }

        if (invalidKeys.length > 0)
        {
            throw new Error(`SonarQube config missing/invalid properties: ${invalidKeys.join(", ")}`);
        }
        return true;
    }
}