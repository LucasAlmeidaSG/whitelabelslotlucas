namespace RS.SonarQube
{
    const isWindows = process.platform === "win32";
    const version = new EnvArg("VERSION", "dev");
    const debug = new BooleanEnvArg("DEBUG", false);

    const fs = require("fs");
    function chmod(path: string, mode: number): PromiseLike<void>
    {
        return new Promise((resolve, reject) =>
        {
            fs.chmod(path, mode, (err) =>
            {
                if (err)
                {
                    reject(err);
                }
                else
                {
                    resolve();
                }
            });
        });
    }

    export const sonarqube = Build.task({ name: "sonarqube", after: [ Tasks.clean ], description: "Runs SonarQube scanner to analyse and uploads results to a given. Optional environment argument to specify version e.g. VERSION=0.1.2 gulp sonarqube, defaults to dev" }, async () =>
    {
        Profile.reset();

        const config = (await Config.get()).moduleConfigs["Core.SonarQube"];

        if (validateConfig(config))
        {
            if (await Command.exists("java"))
            {
                const thisModule = Module.getModule("Core.SonarQube");
                const binPath = Path.combine(thisModule.path, "build_tools", "sonar-scanner", "bin", `sonar-scanner${isWindows ? ".bat" : ""}`);

                await chmod(binPath, 0x755);

                const args: string[] = [];

                if (debug.value)
                {
                    args.push("-X");
                }

                args.push(getArg("sonar.projectName", config.projectName));
                args.push(getArg("sonar.projectKey", config.projectKey));
                args.push(getArg("sonar.projectVersion", version.value));
                args.push(getArg("sonar.host.url", config.serverURL));
                args.push(getArg("sonar.login", config.authKey));

                args.push(getArg("sonar.exclusions", "**/thirdparty/**/*, **/externs/**/*"));

                const baseDir = Is.string(config.baseDirectory) ? config.baseDirectory : "modules";
                args.push(getArg("sonar.projectBaseDir", baseDir));

                await Command.run(binPath, args, null, debug.value);
            }
            else
            {
                throw new Error("Command \"java\" doesn't exist, exiting");
            }
        }

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    function getArg(arg: string, value: string)
    {
        /**
         * Need to put a quote around the property on windows but not anywhere else
         * Nevermind, outdated docs/intro screen or something weird is going on. Leaving it here for future reference
         */
        // const quote = isWindows ? '"' : '';
        // return `-D${quote}${arg}=${value}${quote}`;
        return `-D${arg}=${value}`;
    }
}