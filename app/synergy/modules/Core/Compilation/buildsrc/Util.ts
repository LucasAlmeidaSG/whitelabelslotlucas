namespace RS.Compilation
{
    export function consumeWhitespace(stream: CharacterStream)
    {
        return expectOptionalChars(stream, / /);
    }

    export function expectOptionalChars(stream: CharacterStream, pattern: RegExp): string
    {
        let result = "";
        while (stream.nextChar != null && stream.nextChar.match(pattern)) { result += stream.next(); }
        return result;
    }

    export function expectString(stream: CharacterStream, str: string): string | null
    {
        for (const char of str)
        {
            if (stream.next() != char) { return null; }
        }
        return str;
    }

    /** Expect a string that matches the given string. If found, advance the stream, otherwise stay put. */
    export function expectOptionalString(stream: CharacterStream, str: string): string | null
    {
        const fork = stream.fork();
        // check string in a fork so we don't modify original stream
        for (const char of str)
        {
            if (fork.next() != char) { return null; }
        }

        // advance original stream
        for (let i = 0; i < str.length; ++i)
        {
            stream.next();
        }

        return str;
    }

    export function expectChars(stream: CharacterStream, pattern: RegExp): string | null
    {
        const result = expectChar(stream, pattern);
        if (result == null) { return null; }
        return result + expectOptionalChars(stream, pattern);
    }

    /** Expect a token of the given type. Advance the stram regardless. */
    export function expectToken<T>(stream: TokenStream, type: new(...args: any[]) => T): T
    {
        const next = stream.next();
        if (next instanceof type) { return next; }
        return null;
    }

    /** Expect a char that matches the given pattern. Advance the stream regardless. */
    export function expectChar(stream: CharacterStream, pattern: RegExp): string | null
    {
        const char = stream.next();
        if (char != null && char.match(pattern) != null) { return char; }
        return null;
    }

    /** Expect a char that matches the given pattern. If found, advance the stream, otherwise stay put. */
    export function expectOptionalChar(stream: CharacterStream, pattern: RegExp): string | null
    {
        const char = stream.nextChar;
        if (char != null && char.match(pattern) != null) { return stream.next(); }
        return null;
    }
}