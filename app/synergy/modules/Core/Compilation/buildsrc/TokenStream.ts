namespace RS.Compilation
{
    export class TokenStream
    {
        private _index: number;
        private readonly _data: ReadonlyArray<Token>;

        public get nextToken()
        {
            return this._data[this._index + 1];
        }

        public constructor(data: ReadonlyArray<Token>)
        {
            this._index = -1;
            this._data = data;
        }

        public next()
        {
            return this._data[++this._index];
        }

        public fork()
        {
            const forked = new TokenStream(this._data);
            forked._index = this._index;
            return forked;
        }

        public join(stream: TokenStream)
        {
            this._index = stream._index;
        }
    }

    export type Instance<T extends object> = { prototype: object };
}