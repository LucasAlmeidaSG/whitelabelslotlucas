namespace RS.Compilation
{
    export type IExpector = (stream: CharacterStream) => Token[] | Token | null;

    export class Lexer
    {
        constructor(private readonly expectors: IExpector[]) {}

        public parse(data: string): Token[]
        {
            const stream = new CharacterStream(data);
            const tokens: Token[] = [];
            const plainTextBuffer: string[] = [];
            
            while (stream.nextChar != null)
            {
                let token: Token | Token[];
                for (const expector of this.expectors)
                {
                    const fork = stream.fork();
                    try
                    {
                        token = expector(fork);
                    }
                    catch (expectorError)
                    {
                        const position = fork.position;
                        const preData = data.slice(0, position);
                        const newLines = preData.match(/\n/g);
                        const lineNo = newLines == null ? 0 : newLines.length;
                        const charNo = position - preData.lastIndexOf("\n");

                        const lexerError = new LexerError();
                        lexerError.name = "LexerError";
                        lexerError.message = `${expectorError} at char ${charNo} on line ${lineNo + 1} (position ${position})`;
                        Util.copyErrorStack(expectorError, lexerError);
                        throw lexerError;
                    }

                    if (token)
                    {
                        if (plainTextBuffer.length > 0)
                        {
                            tokens.push(new PlainTextToken(plainTextBuffer.join("")));
                            plainTextBuffer.length = 0;
                        }

                        stream.join(fork);
                        if (Is.array(token)) { tokens.push(...token); } else { tokens.push(token); }
                        break;
                    }
                }

                if (!token)
                {
                    plainTextBuffer.push(stream.next());
                }
            }

            if (plainTextBuffer.length > 0)
            {
                tokens.push(new PlainTextToken(plainTextBuffer.join("")));
                plainTextBuffer.length = 0;
            }

            return tokens;
        }
    }

    export abstract class Token { constructor() { /* */ } }
    /** Any text which is not otherwise tokenised is added to a PlainTextToken. */
    export class PlainTextToken extends Token { constructor(public readonly text: string) { super(); } }

    export class LexerError extends Error {}
}