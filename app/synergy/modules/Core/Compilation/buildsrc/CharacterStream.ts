namespace RS.Compilation
{
    export class CharacterStream
    {
        private _index: number;
        private readonly _data: string;

        public get position()
        {
            return this._index;
        }

        public get nextChar()
        {
            return this._data[this._index + 1];
        }

        public constructor(data: string)
        {
            this._index = -1;
            this._data = data;
        }

        public next()
        {
            return this._data[++this._index];
        }

        public fork()
        {
            const forked = new CharacterStream(this._data);
            forked._index = this._index;
            return forked;
        }

        public join(stream: CharacterStream)
        {
            this._index = stream._index;
        }
    }
}