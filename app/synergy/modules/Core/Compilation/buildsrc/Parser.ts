namespace RS.Compilation
{
    export type IParser = (stream: TokenStream) => string | null;

    export class Parser
    {
        constructor(private readonly parsers: ReadonlyArray<IParser> = [plainTextParser]) {}

        public parse(tokens: ReadonlyArray<Token>): string
        {
            const stream = new TokenStream(tokens);
            const output: string[] = [];
            while (stream.nextToken != null)
            {
                let skip = true;
                for (const parser of this.parsers)
                {
                    const fork = stream.fork();
                    const text = parser(fork);
                    if (text != null)
                    {
                        stream.join(fork);
                        output.push(text);
                        skip = false;
                        break;
                    }
                }

                if (skip) { stream.next(); }
            }
            return output.join("");
        }
    }

    export const plainTextParser: IParser = (stream) =>
    {
        const token = expectToken(stream, PlainTextToken);
        if (!token) { return null; }
        return token.text;
    };
}