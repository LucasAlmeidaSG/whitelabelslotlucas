/// <reference path="Button.ts" />

namespace RS.DevTools.UI
{
    /**
     * Dev tools button list.
     */
    export class ButtonList extends Flow.BaseElement<ButtonList.Settings>
    {
        /** Published when one of the buttons has been clicked. */
        public readonly onButtonClicked = createEvent<number>();
        /** Published when the list is collapsed. */
        public readonly onCollapsed = createSimpleEvent();
        /** Published when the list is revealed. */
        public readonly onOpened = createSimpleEvent();

        protected _titleBar: Flow.Container;
        protected _openButton: Button;
        protected _closeButton: Flow.Button;
        protected _titleLabel: Flow.Label;
        protected _buttons: Button[];
        protected _background: Flow.Background;
        protected _list: Flow.List;
        protected _collapsed: boolean;

        /** Gets or sets if this button list is collapsed or not. */
        public get collapsed() { return this._collapsed; }
        public set collapsed(value)
        {
            if (value === this._collapsed) { return; }
            this._collapsed = value;
            this.suppressLayouting();
            if (value)
            {
                // Hide everything
                if (this._closeButton) { this._closeButton.visible = false; }
                if (this._background) { this._background.visible = false; }
                if (this._list) { this._list.visible = false; }
                this._titleBar.visible = false;

                // Show the dev button
                if (this._openButton) { this._openButton.visible = true; }
            }
            else
            {
                // Show everything
                if (this._closeButton) { this._closeButton.visible = true; }
                if (this._background) { this._background.visible = true; }
                if (this._list) { this._list.visible = true; }
                this._titleBar.visible = true;

                // Hide the dev button
                if (this._openButton) { this._openButton.visible = false; }
            }
            this.invalidateLayout();
            this.restoreLayouting();
        }

        public constructor(settings: ButtonList.Settings)
        {
            super(settings, undefined);

            if (settings.background != null)
            {
                this._background = Flow.background.create(settings.background, this);
            }

            this._titleBar = Flow.container.create({
                dock: Flow.Dock.Top,
                ignoreParentSpacing: true,
                sizeToContents: true,
                contentAlignment: { x: 0.5, y: 0.0 }
            }, this);

            if (settings.collapseButton != null)
            {
                this._closeButton = Flow.button.create(settings.collapseButton, this._titleBar);
                this._closeButton.onClicked(() =>
                {
                    this.collapsed = true;
                    this.onCollapsed.publish();
                });
            }
            if (settings.expandButton != null)
            {
                this._openButton = button.create(settings.expandButton, this);
                this._openButton.onButtonClicked(() =>
                {
                    this.collapsed = false;
                    this.onOpened.publish();
                });
            }
            if (settings.titleLabel != null)
            {
                this._titleLabel = Flow.label.create(settings.titleLabel, this._titleBar);
            }

            this._list = Flow.list.create(settings.innerList, this);
            this._buttons = settings.buttons.map((buttonSettings) => button.create(buttonSettings, this._list));
            for (let i = 0, l = this._buttons.length; i < l; ++i)
            {
                this._buttons[i].onButtonClicked(() => this.onButtonClicked.publish(i));
            }

            this.collapsed = true;
        }

        public getButton(index: number): Button | null
        {
            return this._buttons[index] || null;
        }
    }

    export namespace ButtonList
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            buttons: Button.Settings[];
            titleLabel?: Flow.Label.Settings;
            collapseButton?: Flow.Button.Settings;
            expandButton?: Button.Settings;
            background?: Flow.Background.Settings;
            innerList: Flow.List.Settings;
        }

        export let defaultSettings: Settings =
        {
            name: "Dev Tools Button List",
            buttons: [],
            titleLabel: null,
            collapseButton:
            {
                background:
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: RS.Util.Colors.black,
                    alpha: 0.5,
                    borderSize: 0,
                    cornerRadius: 0
                },
                hoverbackground:
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: RS.Util.Colors.black.brighter(0.15),
                    alpha: 0.5,
                    borderSize: 0,
                    cornerRadius: 0
                },
                pressbackground: null,
                sizeToContents: false,
                expand: Flow.Expand.Disallowed,
                size: { w: 25, h: 25 },
                dock: Flow.Dock.Right,
                spacing: Flow.Spacing.none,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    text: "-",
                    size: { w: 25, h: 25 }
                }
            },
            expandButton:
            {
                ...UI.Button.defaultSettings,
                color: new RS.Util.Color(213, 94, 0), // vermillion
                text: "DEV"
            },
            background:
            {
                kind: Flow.Background.Kind.SolidColor,
                color: RS.Util.Colors.black,
                alpha: 0.5,
                borderSize: 0,
                cornerRadius: 0
            },
            spacing: Flow.Spacing.none,
            sizeToContents: true,
            contentAlignment: { x: 1.0, y: 0.5 },
            innerList:
            {
                direction: Flow.List.Direction.TopToBottom,
                contentAlignment: { x: 0.5, y: 0.5 },
                spacing: Flow.Spacing.all(10),
                dock: Flow.Dock.Fill
            }
        };
    }

    /**
     * Dev tools button list.
     */
    export const buttonList = Flow.declareElement(ButtonList, ButtonList.defaultSettings);
}
