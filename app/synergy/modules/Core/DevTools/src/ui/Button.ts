namespace RS.DevTools.UI
{
    /**
     * Dev tools button.
     */
    export class Button extends Flow.BaseElement<Button.Settings>
    {
        /** Published when the button has been clicked. */
        public readonly onButtonClicked = createSimpleEvent();

        @RS.AutoDisposeOnSet protected _button: Flow.Button | null = null;
        @RS.AutoDisposeOnSet protected _bar: Flow.Background | null = null;

        /** Published when this button has been clicked. */
        // public get onClicked() { return this._button.onClicked; }

        /** Gets or sets the text of this button. */
        public get text() { return this._button.label.text; }
        public set text(value) { this._button.label.text = value; }

        public constructor(settings: Button.Settings)
        {
            super(settings, undefined);

            this._button = Flow.button.create({
                ...settings.innerButton,
                textLabel:
                {
                    ...settings.innerButton.textLabel,
                    text: this.settings.text
                },
                background:
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: this.settings.color,
                    borderSize: 0,
                    cornerRadius: 0,
                    ignoreParentSpacing: true
                },
                hoverbackground:
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: this.settings.color.brighter(0.15),
                    borderSize: 0,
                    cornerRadius: 0,
                    ignoreParentSpacing: true
                },
                pressbackground:
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: this.settings.color.darker(0.15),
                    borderSize: 0,
                    cornerRadius: 0,
                    ignoreParentSpacing: true
                },
                spacing: { left: 10, top: 10, right: 10, bottom: 18 }
            }, this);
            this._button.onClicked((ev) => this.onButtonClicked.publish());

            this._bar = Flow.background.create({
                kind: Flow.Background.Kind.SolidColor,
                color: settings.color.darker(this.settings.barDarkness),
                dock: Flow.Dock.Bottom,
                size: { w: 0, h: this.settings.barSize },
                ignoreParentSpacing: true,
                sizeToContents: false,
                borderSize: 0,
                cornerRadius: 0
            }, this._button);
            this._bar.interactive = false;
        }
    }

    /**
     * Dev tools button.
     */
    export const button = Flow.declareElement(Button);

    export namespace Button
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            color: RS.Util.Color;
            text: string;
            barSize: number;
            barDarkness: number;

            innerButton: Flow.Button.Settings;
        }

        export let defaultSettings: Settings =
        {
            name: "Dev Tools Button",
            color: RS.Util.Colors.gray,
            text: "DEFAULT",
            barSize: 8,
            barDarkness: 0.25,
            sizeToContents: true,

            innerButton:
            {
                ...Flow.Button.defaultSettings,
                expand: Flow.Expand.Disallowed,
                sizeToContents: true,
                dock: Flow.Dock.Fill,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    font: RS.Fonts.FrutigerWorld.Bold,
                    fontSize: 32,
                    layers:
                    [
                        {
                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                            color: RS.Util.Colors.black,
                            layerType: RS.Rendering.TextOptions.LayerType.Border,
                            size: 4
                        },
                        {
                            layerType: RS.Rendering.TextOptions.LayerType.Fill,
                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                            color: RS.Util.Colors.white
                        }
                    ]
                }
            }
        };
    }
}
