namespace RS.DevTools.UI
{
    @HasCallbacks
    export class ItemCategoryPanel extends Flow.Container
    {
        /** The index of the button to be highlighted (or -1 for none). */
        //public readonly selectedItem = new Observable(-1);

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _title: Flow.Label | null = null;
        @RS.AutoDisposeOnSet protected _body: Flow.List | null = null;
        @RS.AutoDisposeOnSet protected _optionGrid: Flow.Grid | null = null;
        @RS.AutoDisposeOnSet protected _buttons: Flow.ToggleButton[] | null = null;

        public constructor(public readonly settings: ItemCategoryPanel.Settings)
        {
            super(settings);

            this._bg = Flow.background.create(settings.background, this);
            this._title = Flow.label.create(settings.title, this);
            this._body = Flow.list.create({
                name: "Item Category Body",
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.TopToBottom,
                spacing: { left: 0, top: 0, right: 0, bottom: settings.listSpacing },
                expand: Flow.Expand.HorizontalOnly
            }, this);
            this._optionGrid = null;

            //this.selectedItem.onChanged(this.updateButtonToggleStates);
        }

        /**
         * Adds a button to this item panel.
         * @param text
         * @param clickCallback
         */
        public addButton(text: string, clickCallback?: () => void): Flow.ToggleButton
        {
            if (!this._optionGrid)
            {
                this._optionGrid = Flow.grid.create(this.settings.optionGrid, this._body);
                this._buttons = [];
            }
            const btn = Flow.toggleButton.create({
                ...this.settings.button,
                textLabel: { ...this.settings.button.textLabel, text },
                disabledTextLabel: this.settings.button.disabledTextLabel && { ...this.settings.button.disabledTextLabel, text },
                seleniumId: this.settings.seleniumPrefix && `${this.settings.seleniumPrefix}:Button-${this._buttons.length}`
            }, this._optionGrid);
            this._buttons.push(btn);
            if (clickCallback) { btn.onClicked(clickCallback); }
            return btn;
        }

        /**
         * Adds a sub category to this item panel.
         * @param title
         */
        public addSubCategory(title: string, fgColor: RS.Util.Color, bgColor: RS.Util.Color): ItemCategoryPanel
        {
            this._optionGrid = null;
            return this.internalAddSubCategory(this.settings, title, fgColor, bgColor);
        }

        /**
         * Adds a spinner to this item panel.
         * @param options
         */
        public addSpinner(options: string[], clickCallback: () => void): Flow.Spinner
        {
            if (!this._optionGrid)
            {
                this._optionGrid = Flow.grid.create(this.settings.optionGrid, this._body);
                this._buttons = [];
            }
            const spinner = Flow.spinner.create({
                ...this.settings.spinner,
                options:
                {
                    wrap: false,
                    type: Flow.Spinner.Type.Options,
                    options,
                    initialIndex: 0
                }
            }, this._optionGrid);
            spinner.optionIndex = 1;
            spinner.optionIndex = 0;
            spinner.onInnerButtonClicked(clickCallback);
            return spinner;
        }

        /**
         * Adds a label to this item panel.
         * @param text
         */
        public addLabel(text: string): Flow.Label
        {
            this._optionGrid = null;
            return Flow.label.create({
                ...this.settings.label,
                text
            }, this._body);
        }

        // @Callback
        // protected updateButtonToggleStates(): void
        // {
        //     this.suppressLayouting();
        //     for (let i = 0, l = this._buttons.length; i < l; ++i)
        //     {
        //         this._buttons[i].toggled.value = i === this.selectedItem.value;
        //     }
        //     this.restoreLayouting();
        // }

        protected internalAddSubCategory(baseSettings: ItemCategoryPanel.Settings, title: string, fgColor: RS.Util.Color, bgColor: RS.Util.Color): ItemCategoryPanel
        {
            return itemCategoryPanel.create({
                ...baseSettings,
                background: { ...baseSettings.background, color: bgColor } as Flow.Background.Settings,
                button: ItemCategoryPanel.getButtonSettings(fgColor, baseSettings.button),
                spinner:
                {
                    ...baseSettings.spinner,
                    incrementButton: ItemCategoryPanel.getButtonSettings(fgColor, baseSettings.spinner.incrementButton, Flow.Background.Corners.rightOnly),
                    decrementButton: ItemCategoryPanel.getButtonSettings(fgColor, baseSettings.spinner.decrementButton, Flow.Background.Corners.leftOnly),
                    button: ItemCategoryPanel.getButtonSettings(fgColor, (baseSettings.spinner as Flow.Spinner.ButtonSettings).button, Flow.Background.Corners.none)
                } as Flow.Spinner.Settings,
                title:
                {
                    ...baseSettings.title,
                    text: title
                }
            }, this._body);
        }
    }

    export const itemCategoryPanel = Flow.declareElement(ItemCategoryPanel);

    export namespace ItemCategoryPanel
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            background: Flow.Background.Settings;
            title: Flow.Label.Settings;
            optionGrid: Flow.Grid.Settings;
            button: Flow.ToggleButton.Settings;
            seleniumPrefix?: string;
            listSpacing: number;
            spinner: Flow.Spinner.Settings;
            label: Flow.Label.Settings;
        }

        const standardButtonBackground: Flow.Background.Settings =
        {
            dock: Flow.Dock.Fill,
            kind: Flow.Background.Kind.SolidColor,
            borderSize: 2,
            borderColor: RS.Util.Colors.lightgrey,
            cornerRadius: 4,
            color: RS.Util.Colors.black,
            ignoreParentSpacing: true
        };

        const standardButton: Flow.ToggleButton.Settings =
        {
            ...Flow.Button.defaultSettings,
            name: "Button",
            background:
            {
                ...standardButtonBackground,
                color: new RS.Util.Color(0x727272)
            },
            hoverbackground:
            {
                ...standardButtonBackground,
                color: new RS.Util.Color(0x969696)
            },
            togglebackground:
            {
                ...standardButtonBackground,
                color: new RS.Util.Color(0x929292)
            },
            pressbackground:
            {
                ...standardButtonBackground,
                color: new RS.Util.Color(0x595959)
            },
            disabledbackground:
            {
                ...standardButtonBackground,
                color: new RS.Util.Color(0x444444)
            },
            textLabel:
            {
                ...Flow.Button.defaultSettings.textLabel,
                font: RS.Fonts.FrutigerWorld.Bold,
                fontSize: 32,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 4
                    },
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.white
                    }
                ]
            },
            disabledTextLabel:
            {
                ...Flow.Button.defaultSettings.textLabel,
                font: RS.Fonts.FrutigerWorld.Bold,
                fontSize: 32,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 4
                    },
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.lightgrey
                    }
                ]
            },
            spacing: Flow.Spacing.axis(8, 10),
            sizeToContents: true,
            dock: Flow.Dock.Fill,
            autoToggle: false
        };

        export const defaultSettings: Settings =
        {
            name: "Item Category Panel",
            spacing: { left: 12, top: 12, right: 12, bottom: 8 },
            listSpacing: 4,
            background:
            {
                name: "Item Category Panel BG",
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.SolidColor,
                borderSize: 2,
                borderColor: RS.Util.Colors.lightgrey,
                cornerRadius: 8,
                color: RS.Util.Colors.black,
                ignoreParentSpacing: true
            },
            title:
            {
                ...Flow.Label.defaultSettings,
                name: "Item Category Title",
                font: RS.Fonts.FrutigerWorld.Bold,
                fontSize: 32,
                dock: Flow.Dock.Top,
                sizeToContents: true
            },
            label:
            {
                ...Flow.Label.defaultSettings,
                name: "Label",
                font: RS.Fonts.FrutigerWorld.Bold,
                fontSize: 28,
                sizeToContents: true
            },
            optionGrid:
            {
                ...Flow.Grid.defaultSettings,
                name: "Item Category Grid",
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                squared: true,
                spacing: Flow.Spacing.all(4),
                expand: Flow.Expand.HorizontalOnly
            },
            button: standardButton,
            spinner:
            {
                ...Flow.BaseSpinner.defaultSettings,
                name: "Spinner",
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                innerType: Flow.Spinner.InnerType.Button,
                button:
                {
                    ...(standardButton as Flow.Button.Settings),
                    dock: Flow.Dock.Fill
                },
                incrementButton:
                {
                    ...(standardButton as Flow.Button.Settings),
                    dock: Flow.Dock.Right,
                    textLabel:
                    {
                        ...standardButton.textLabel,
                        text: "+"
                    },
                    disabledTextLabel:
                    {
                        ...standardButton.disabledTextLabel,
                        text: "+"
                    },
                    size: { w: 35, h: 0 },
                    expand: Flow.Expand.VerticalOnly,
                    sizeToContents: false
                },
                decrementButton:
                {
                    ...(standardButton as Flow.Button.Settings),
                    dock: Flow.Dock.Left,
                    textLabel:
                    {
                        ...standardButton.textLabel,
                        text: "-"
                    },
                    disabledTextLabel:
                    {
                        ...standardButton.disabledTextLabel,
                        text: "-"
                    },
                    size: { w: 35, h: 0 },
                    expand: Flow.Expand.VerticalOnly,
                    sizeToContents: false
                },
                background: null
            },
            sizeToContents: true
        };

        export function getButtonSettings(color: RS.Util.Color, baseSettings: Flow.ToggleButton.Settings = defaultSettings.button, corners: Flow.Background.Corners = Flow.Background.Corners.all): Flow.ToggleButton.Settings
        {
            return {
                ...baseSettings,
                background: { ...baseSettings.background, corners, color } as Flow.Background.Settings,
                togglebackground: { ...baseSettings.background, corners, color: color.brighter(0.15) } as Flow.Background.Settings,
                hoverbackground: { ...baseSettings.background, corners, color: color.brighter(0.2) } as Flow.Background.Settings,
                pressbackground: { ...baseSettings.background, corners, color: color.darker(0.2) } as Flow.Background.Settings,
                disabledbackground: { ...baseSettings.background, corners, color: color.darker(0.5) } as Flow.Background.Settings
            };
        }
    }
}
