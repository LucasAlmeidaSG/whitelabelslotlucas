/// <reference path="ItemCategoryPanel.ts" />

namespace RS.DevTools.UI
{
    export class ItemPanel extends ItemCategoryPanel
    {
        public readonly settings: ItemPanel.Settings;

        public constructor(settings: ItemPanel.Settings)
        {
            super(settings);

            this.removeChild(this._title);
            this._body.addChild(this._title);
            this._title.dock = Flow.Dock.Fill;
            this._body.dock = Flow.Dock.Fill;
        }

        /**
         * Adds a sub category to this item panel.
         * @param title
         */
        public addSubCategory(title: string, fgColor: RS.Util.Color, bgColor: RS.Util.Color): ItemCategoryPanel
        {
            this._optionGrid = null;
            return this.internalAddSubCategory(this.settings.subCategory, title, fgColor, bgColor);
        }
    }

    export const itemPanel = Flow.declareElement(ItemPanel);

    export namespace ItemPanel
    {
        export interface Settings extends ItemCategoryPanel.Settings
        {
            listSpacing: number;
            subCategory: ItemCategoryPanel.Settings;
        }

        export const defaultSettings: Settings =
        {
            ...ItemCategoryPanel.defaultSettings,
            name: "Item Panel",
            background:
            {
                name: "Item Panel BG",
                kind: Flow.Background.Kind.SolidColor,
                borderSize: 2,
                borderColor: RS.Util.Colors.white,
                alpha: 0.8,
                cornerRadius: 16,
                color: RS.Util.Colors.black,
                ignoreParentSpacing: true
            },
            spacing: { left: 16, top: 16, right: 16, bottom: 8 },
            listSpacing: 8,
            subCategory: ItemCategoryPanel.defaultSettings,
            sizeToContents: true
        };
    }
}
