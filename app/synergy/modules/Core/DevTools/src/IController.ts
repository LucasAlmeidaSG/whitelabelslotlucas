/// <reference path="ui/Button.ts" />
/// <reference path="ui/ButtonList.ts" />
/// <reference path="ui/ItemPanel.ts" />

namespace RS.DevTools
{
    export interface IController<TSettings extends IController.Settings = IController.Settings> extends Controllers.IController<TSettings>
    {
        /** Gets or sets the tests map */
        tests: IController.TestData;

        /** Creates the dev tools UI. */
        create(gameVersion: Version): void;

        /** Attaches the dev tools to the specified container at the specified position. */
        attach(container: Rendering.IContainer, props?: Partial<Flow.ElementProperties>): void;

        /** Detaches the dev tools from the given container if necessary. */
        detach(container: RS.Rendering.IContainer): void;

        /** Detaches the dev tools from the current parent container if there is one. */
        detach(): void;
    }

    export const IController = Controllers.declare<IController<IController.Settings>, IController.Settings>(Strategy.Type.Instance);

    export namespace IController
    {
        export type ForceData = Map<Helpers.ItemSelector.Entry<OneOrMany<Engine.ForceResult>>>;
        export type TestData = Map<Helpers.ItemSelector.Entry<OneOrMany<(() => void)>>>;
        export type ReplayData<TReplayData extends object> = Map<Helpers.ItemSelector.Entry<TReplayData>>;

        export interface Settings<TModels extends Models = Models, TReplayData extends {} = any>
        {
            demo?: UI.Button.Settings;
            demoSelector?: Helpers.ItemSelector.Settings;
            demoList?: UI.ButtonList.Settings;
            demoListButton?: UI.Button.Settings;

            tests?: UI.Button.Settings;
            testSelector?: Helpers.ItemSelector.Settings;
            pause?: UI.Button.Settings;
            unpauseText?: string;
            replay?: UI.Button.Settings;
            replaySelector?: Helpers.ItemSelector.Settings;
            GREG?: UI.Button.Settings;
            GREGSelector: Helpers.ItemSelector.Settings;
            time?: UI.Button.Settings;
            timeSpinner?: Flow.Spinner.Settings;
            screen?: UI.Button.Settings;
            muteMusic?: UI.Button.Settings;
            dump?: UI.Button.Settings;
            toggleNTOn?: UI.Button.Settings;
            toggleNTOff?: UI.Button.Settings;
            buttonList?: UI.ButtonList.Settings;

            // hide the dev ui panel
            hide?: UI.Button.Settings;
            hideSpinner?: Flow.Spinner.Settings;
            hideApply?: UI.Button.Settings;

            testData?: TestData;
            replayData?: ReplayData<TReplayData>;
            engine?: Engine.IEngine<TModels, object>;
            forceData?: ForceData;
            viewController?: View.IController;
            pauseArbiter?: IArbiter<boolean>;
            showButtonAreaArbiter?: IArbiter<boolean>;
            showTurboButtonArbiter?: IArbiter<boolean>;
            musicVolumeArbiter?: IArbiter<number>;
            showMeterPanelArbiter?: IArbiter<boolean>;
        }
    }

    export const panelButtonColors: Util.Color[] =
    [
        new Util.Color(230, 159, 0), // orange
        new Util.Color(86, 180, 233), // sky blue
        new Util.Color(0, 158, 115), // bluish green
        new Util.Color(240, 228, 66), // yellow
        new Util.Color(0, 114, 178), // blue
        new Util.Color(213, 94, 0), // vermillion
        new Util.Color(204, 121, 167) // reddish purple
    ];

    export const panelBGColors: Util.Color[] =
    [
        Util.Colors.black,
        new Util.Color(0x606060)
    ];

    export const defaultSettings: IController.Settings =
    {
        demo:
        {
            ...UI.Button.defaultSettings,
            color: new RS.Util.Color(0, 158, 115), // bluish green
            text: "DEMO",
            innerButton: { ...UI.Button.defaultSettings.innerButton, seleniumId: "DemoButton" }
        },
        demoSelector:
        {
            itemPanelSettings:
            {
                ...UI.ItemPanel.defaultSettings,
                name: "Demo Item Panel",
                seleniumPrefix: "Demo",
                title:
                {
                    ...UI.ItemPanel.defaultSettings.title,
                    text: "Demo"
                },
                contentAlignment: { x: 0.5, y: 0.5 },
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.47 },
                expand: Flow.Expand.HorizontalOnly
            },
            buttonSettings: { ...UI.Button.defaultSettings, color: null },
            panelColors: panelButtonColors,
            bgColors: panelBGColors
        },
        tests:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(244, 188, 66),
            text: "TESTS"
        },
        testSelector:
        {
            itemPanelSettings:
            {
                ...UI.ItemPanel.defaultSettings,
                name: "Test Item Panel",
                title:
                {
                    ...UI.ItemPanel.defaultSettings.title,
                    text: "TESTS"
                },
                contentAlignment: { x: 0.5, y: 0.5 },
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.47 },
                expand: Flow.Expand.HorizontalOnly
            },
            buttonSettings: { ...UI.Button.defaultSettings, color: null },
            panelColors: panelButtonColors,
            bgColors: panelBGColors
        },
        pause:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(86, 180, 233), // sky blue
            text: "PAUSE"
        },
        unpauseText: "UNPAUSE",
        replay:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(204, 121, 167), // reddish purple
            text: "REPLAY"
        },
        replaySelector:
        {
            itemPanelSettings:
            {
                ...UI.ItemPanel.defaultSettings,
                name: "Replay Item Panel",
                title:
                {
                    ...UI.ItemPanel.defaultSettings.title,
                    text: "Replay"
                },
                contentAlignment: { x: 0.5, y: 0.5 },
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 }
            },
            buttonSettings: { ...UI.Button.defaultSettings, color: null },
            panelColors: panelButtonColors,
            bgColors: panelBGColors
        },
        GREG:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(8, 87, 214), // blue
            text: "GREG"
        },
        GREGSelector:
        {
            itemPanelSettings:
            {
                ...UI.ItemPanel.defaultSettings,
                name: "GREG Item Panel",
                title:
                {
                    ...UI.ItemPanel.defaultSettings.title,
                    text: "GREG"
                },
                contentAlignment: { x: 0.5, y: 0.5 },
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.47 },
                expand: Flow.Expand.HorizontalOnly
            },
            buttonSettings: { ...UI.Button.defaultSettings, color: null },
            panelColors: panelButtonColors,
            bgColors: panelBGColors
        },
        time:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(230, 159, 0), // orange
            text: "TIME"
        },
        timeSpinner:
        {
            label: { ...Flow.Spinner.defaultSettings.label },
            size: { w: 150, h: 40 },
            incrementButton: { ...Flow.Spinner.defaultSettings.incrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            decrementButton: { ...Flow.Spinner.defaultSettings.decrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            dock: Flow.Dock.None,
            sizeToContents: false,
            options:
            {
                type: Flow.Spinner.Type.Options,
                options: [0.01, 0.1, 0.25, 0.5, 1.0, 1.5, 2.0, 5.0, 10.0, 15.0],
                initialIndex: 2
            }
        },
        hide:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(240, 228, 66), // yellow
            text: "HIDE"
        },
        hideApply:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(0, 158, 115), // bluish green
            size: { w: 150, h: 40 },
            dock: Flow.Dock.None,
            sizeToContents: false,
            text: "APPLY"
        },
        hideSpinner:
        {
            label: { ...Flow.Spinner.defaultSettings.label },
            size: { w: 150, h: 40 },
            incrementButton: { ...Flow.Spinner.defaultSettings.incrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            decrementButton: { ...Flow.Spinner.defaultSettings.decrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            dock: Flow.Dock.None,
            sizeToContents: false,
            options:
            {
                type: Flow.Spinner.Type.Options,
                options: [2.5, 5.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0],
                initialIndex: 2
            }
        },
        screen:
        {
            ...UI.Button.defaultSettings,
            color: Util.Colors.gray,
            text: "SCREEN"
        },
        muteMusic:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(0, 128, 128),
            text: "MUSIC ON"
        },
        dump:
        {
            ...UI.Button.defaultSettings,
            color: new Util.Color(180, 0, 0),
            text: "DUMP"
        },
        toggleNTOn:
        {
            ...UI.Button.defaultSettings,
            color: RS.Util.Colors.purple,
            text: "ENABLE NT"
        },
        toggleNTOff:
        {
            ...UI.Button.defaultSettings,
            color: RS.Util.Colors.purple,
            text: "DISABLE NT"
        },
        buttonList:
        {
            ...UI.ButtonList.defaultSettings,
            expandButton:
            {
                ...UI.ButtonList.defaultSettings.expandButton,
                innerButton:
                {
                    ...UI.ButtonList.defaultSettings.expandButton.innerButton,
                    seleniumId: "DevButton"
                }
            },
            collapseButton:
            {
                ...UI.ButtonList.defaultSettings.collapseButton,
                seleniumId: "DevButton"
            }
        },
        pauseArbiter: null,
        replayData: {},
        engine: null
    };
}
