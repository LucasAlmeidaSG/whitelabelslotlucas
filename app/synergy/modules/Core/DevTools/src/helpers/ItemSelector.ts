/// <reference path="../ui/ItemPanel.ts" />

namespace RS.DevTools.Helpers
{
    export interface ItemSelection<T>
    {
        items: T[];
        final: boolean;
    }

    @HasCallbacks
    export class ItemSelector<T>
    {
        /** Published when a non-category selection of a single non-additive item and multiple additive items. */
        public readonly onSelected = createEvent<ItemSelection<T>>();

        @AutoDispose protected _panel: UI.ItemPanel;

        protected _currentEntries: Map<ItemSelector.Entry<T>>;
        protected _colorIndex: number;

        /** Gets whether or not the item selector is currently displaying. */
        public get isOpen(): boolean { return this._panel != null; }

        constructor(public readonly settings: ItemSelector.Settings, public readonly runtimeData: ItemSelector.RuntimeData<T>) {}

        public open(): void
        {
            if (this.isOpen) { this.close(); }
            if (!this.runtimeData.parent) { return; }

            this._colorIndex = 0;
            this.buildPanel(this.runtimeData.items);
        }

        public close(): void
        {
            if (this._panel)
            {
                this._panel.dispose();
                this._panel = null;
            }
        }

        /**
         * Gets the next color to use on a panel foreground (buttons).
         */
        protected getNextColor(): RS.Util.Color
        {
            const col = this.settings.panelColors[this._colorIndex];
            this._colorIndex = (this._colorIndex + 1) % this.settings.panelColors.length;
            return col;
        }

        /**
         * Gets the background panel color to use for the specified nest level.
         * @param nestLevel
         */
        protected getBGColor(nestLevel: number): RS.Util.Color
        {
            return this.settings.bgColors[nestLevel % this.settings.bgColors.length];
        }

        protected buildPanel(items: Map<ItemSelector.Entry<T>>): void
        {
            this._panel = UI.itemPanel.create({
                ...this.settings.itemPanelSettings,
                background: { ...this.settings.itemPanelSettings.background, color: this.getBGColor(0) } as Flow.Background.Settings,
                button: UI.ItemCategoryPanel.getButtonSettings(this.getNextColor(), this.settings.itemPanelSettings.button)
            }, this.runtimeData.parent);
            this._panel.suppressLayouting();
            this.buildCategory(this._panel, { isCategory: true, items }, new Observable(true), 1);
            this._panel.restoreLayouting(true);

            this._currentEntries = items;
        }

        protected buildCategory(categoryPanel: UI.ItemCategoryPanel, category: ItemSelector.Category<T>, constraint: IReadonlyObservable<boolean>, nestLevel: number = 0): void
        {
            let cnt = 0;
            for (const key in category.items)
            {
                ++cnt;
                const item = category.items[key];
                const subConstraint = item.constraint ? Observable.reduce([ constraint, item.constraint ], Arbiter.AndResolver) : constraint;
                // Note: the explicit === true and !== true are needed for TypeScript to correctly narrow the type of item
                if (item.isCategory === true)
                {
                    if (item.spinner)
                    {
                        if (item.additive)
                        {
                            Log.warn(`Additive spinner categories are not supported`);
                        }
                        const options = Object.keys(item.items).filter((innerKey) => !item.items[innerKey].isCategory);
                        for (const subKey of options)
                        {
                            const subItem = item.items[subKey];
                            if (subItem.isCategory !== true)
                            {
                                subItem.__state =
                                {
                                    button: null, categoryPanel, category,
                                    additive: false,
                                    activated: false
                                };
                            }
                        }
                        const spinner = categoryPanel.addSpinner(options, () =>
                        {
                            this.activateItem(item.items[options[spinner.optionIndex]] as ItemSelector.Item<T>);
                        });
                        spinner.bindToObservables({ enabled: subConstraint });
                    }
                    else
                    {
                        const subCategoryPanel = categoryPanel.addSubCategory(key, this.getNextColor(), this.getBGColor(nestLevel));
                        this.buildCategory(subCategoryPanel, item, subConstraint, nestLevel + 1);
                    }
                }
                else
                {
                    const button = categoryPanel.addButton(key, () =>
                    {
                        this.activateItem(item);
                    });
                    button.bindToObservables({ enabled: subConstraint });
                    item.__state =
                    {
                        button, categoryPanel, category,
                        additive: category.additive,
                        activated: false
                    };
                }
            }
            if (cnt === 0)
            {
                categoryPanel.addLabel("This category appears to be empty!");
            }
            if (category.additive)
            {
                Flow.label.create({
                    ...categoryPanel.settings.label,
                    text: "+",
                    dock: Flow.Dock.Float,
                    floatPosition: { x: 1.0, y: 0.0 },
                    dockAlignment: { x: 1.0, y: 0.0 },
                    sizeToContents: false,
                    size: { w: 25, h: 25 }
                }, categoryPanel);
            }
        }

        protected updateButtonToggleStates(items: ItemSelector.EntryMap<T> = this._currentEntries): void
        {
            // Note: the explicit === true and !== true are needed for TypeScript to correctly narrow the type of item
            this._panel.suppressLayouting();
            for (const key in items)
            {
                const item = items[key];
                if (item.isCategory === true)
                {
                    this.updateButtonToggleStates(item.items);
                }
                else if (item.__state && item.__state.button)
                {
                    item.__state.button.toggled.value = item.__state.activated;
                }
            }
            this._panel.restoreLayouting();
        }

        protected clearActiveStates(items: ItemSelector.EntryMap<T> = this._currentEntries, exclude: ItemSelector.Item<T>): void
        {
            // Note: the explicit === true and !== true are needed for TypeScript to correctly narrow the type of item
            for (const key in items)
            {
                const item = items[key];
                if (item.isCategory === true)
                {
                    this.clearActiveStates(item.items, exclude);
                }
                else if (item.__state && item !== exclude)
                {
                    item.__state.activated = false;
                }

            }
        }

        protected activateItem(item: ItemSelector.Item<T>): void
        {
            // Note: the explicit === true and !== true are needed for TypeScript to correctly narrow the type of item
            if (!item.__state) { return; }
            if (item.__state.activated && item.__state.additive)
            {
                this.publishSelection();
            }
            else
            {
                item.__state.activated = true;
                for (const key in item.__state.category.items)
                {
                    const otherItem = item.__state.category.items[key];
                    if (otherItem !== item && otherItem.isCategory !== true && otherItem.__state)
                    {
                        otherItem.__state.activated = false;
                    }
                }
                if (!item.__state.additive)
                {
                    this.publishSelection();
                }
                else
                {
                    this.updateButtonToggleStates();
                }
            }
        }

        protected publishSelection(): void
        {
            const gatheredItems: ItemSelector.Item<T>[] = [];
            this.gatherActiveItems(this._currentEntries, gatheredItems);
            this.onSelected.publish({
                items: gatheredItems,
                final: true
            });
        }

        protected gatherActiveItems(items: ItemSelector.EntryMap<T> = this._currentEntries, out: ItemSelector.Item<T>[])
        {
            // Note: the explicit === true and !== true are needed for TypeScript to correctly narrow the type of item
            for (const key in items)
            {
                const item = items[key];
                if (item.isCategory === true)
                {
                    this.gatherActiveItems(item.items, out);
                }
                else if (item.__state && item.__state.activated)
                {
                    out.push(item);
                }
            }
        }
    }

    export namespace ItemSelector
    {
        export interface ItemState<T>
        {
            button: Flow.ToggleButton | null;
            category: Category<T>;
            categoryPanel: UI.ItemCategoryPanel;
            activated: boolean;
            additive: boolean;
        }

        export type Item<T> = T &
        {
            isCategory?: false;
            constraint?: IReadonlyObservable<boolean>;
            buttonSettings?: Partial<UI.Button.Settings>;
            __state?: ItemState<T>;
        };

        export interface Category<T>
        {
            isCategory: true;
            constraint?: IReadonlyObservable<boolean>;
            additive?: boolean;
            spinner?: boolean;
            items: EntryMap<T>;
        }

        export type Entry<T> = Item<T> | Category<T>;

        export type EntryMap<T> = Map<Entry<T>>;

        export interface Settings
        {
            itemPanelSettings: UI.ItemPanel.Settings;
            buttonSettings: UI.Button.Settings;
            itemSettings?: Partial<UI.Button.Settings>;
            categorySettings?: Partial<UI.Button.Settings>;
            panelColors: RS.Util.Color[];
            bgColors: RS.Util.Color[];
        }

        export interface RuntimeData<T>
        {
            items: EntryMap<T>;
            parent?: RS.Rendering.IContainer;
        }
    }
}
