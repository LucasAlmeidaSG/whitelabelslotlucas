/// <reference path="IController.ts" />

namespace RS.DevTools
{
    const GREGEnabled = RS.URL.getParameterAsBool("greg");

    @HasCallbacks
    export class Controller<TSettings extends IController.Settings = IController.Settings> implements IController<TSettings>
    {
        public settings: TSettings;

        protected _disposed: boolean = false;

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this._disposed; }

        protected _gameVersion: Version;

        @AutoDisposeOnSet protected _ui: UI.ButtonList | null;
        @AutoDispose protected _demoPanel: Helpers.ItemSelector<OneOrMany<Engine.ForceResult>>;
        @AutoDispose protected _testsPanel: Helpers.ItemSelector<OneOrMany<(() => void)>>;
        @AutoDispose protected _replayPanel: Helpers.ItemSelector<any>;
        @AutoDispose protected _GREGPanel: Helpers.ItemSelector<any>;
        @AutoDisposeOnSet protected _timeSpinner: Flow.Spinner | null;
        @AutoDisposeOnSet protected _hideTimeSpinner: Flow.Spinner | null;
        @AutoDisposeOnSet protected _hideTimeGoBtn: UI.Button;
        @AutoDisposeOnSet protected _hideTween: RS.Tween<IController<TSettings>> | null = null;

        @AutoDisposeOnSet protected _pauseHandle: IArbiterHandle<boolean>;
        @AutoDisposeOnSet protected _showButtonAreaHandle: IArbiterHandle<boolean>;
        @AutoDisposeOnSet protected _showTurboButtonHandle: IArbiterHandle<boolean>;
        @AutoDisposeOnSet protected _showMeterPanelHandle: IArbiterHandle<boolean>;
        @AutoDisposeOnSet protected _musicVolumeHandle: IArbiterHandle<number>;

        protected _ntOverride: boolean = false;
        protected _isHidden: boolean = false;
        protected _testData: IController.TestData;
        protected _hideForTimeIndex: number;

        /** Gets or sets the tests map */
        public get tests() { return this._testData; }
        public set tests(value)
        {
            this._testData = value;
        }

        protected get showDemoButton()
        {
            return this.settings.engine != null && this.settings.forceData != null && Object.keys(this.settings.forceData).length > 0;
        }

        protected get showTestButton() { return this._testData != null && Object.keys(this._testData).length > 0; }
        protected get showPauseButton() { return this.settings.pauseArbiter != null; }
        protected get showTimeButton() { return true; }
        protected get showScreenButton() { return this.settings.viewController != null; }
        protected get showHideButton() { return true; }
        protected get showMusicMuteButton() { return this.settings.musicVolumeArbiter != null; }
        protected get showReplayButton() { return this.settings.engine != null; }
        protected get showDumpButton() { return true; }
        protected get showGREGButton() { return GREGEnabled && this.settings.engine != null; }
        protected get showToggleNTButton() { return true; }

        /** Whether to automatically mute. */
        protected get autoMute()
        {
            return URL.getParameterAsBool("mute", false);
        }

        public initialise(settings: TSettings)
        {
            this.settings = settings;
            this._testData = settings.testData;

            // initial time start
            if (this.settings.hideSpinner != null)
            {
                const options = this.settings.hideSpinner.options as RS.Flow.BaseSpinner.ExplicitOptionsSettings;
                this._hideForTimeIndex = options.initialIndex;
            }

            document.addEventListener("keypress", this.handleKeyPress);

            if (this.autoMute)
            {
                if (this.settings.musicVolumeArbiter)
                {
                    this._musicVolumeHandle = this.settings.musicVolumeArbiter.declare(0);
                }
                else
                {
                    Log.warn("[DevToolsController] No music volume arbiter set");
                }
            }
        }

        /** Initialises the dev tools UI. */
        public create(gameVersion: Version): void
        {
            this._demoPanel = new Helpers.ItemSelector(
            {
                ...this.settings.demoSelector,
                buttonSettings:
                {
                    ...this.settings.demoSelector.buttonSettings,
                    color: this.settings.demoSelector.buttonSettings.color || this.settings.demo.color
                }
            },
            {
                items: this.settings.forceData
            });
            this._demoPanel.onSelected(this.handleDemoButtonSelected);

            this._replayPanel = new Helpers.ItemSelector(
            {
                ...this.settings.replaySelector,
                buttonSettings:
                {
                    ...this.settings.replaySelector.buttonSettings,
                    color: this.settings.replaySelector.buttonSettings.color || this.settings.replay.color
                }
            },
            {
                items: this.settings.replayData
            });
            this._replayPanel.onSelected(this.handleReplayButtonSelected);

            this._GREGPanel = new Helpers.ItemSelector(
            {
                ...this.settings.GREGSelector,
                buttonSettings:
                {
                    ...this.settings.GREGSelector.buttonSettings,
                    color: this.settings.GREGSelector.buttonSettings.color || this.settings.GREG.color
                }
            },
            {
                items: {}
            });
            this._GREGPanel.onSelected(this.handleGREGButtonSelected);

            this._testsPanel = new Helpers.ItemSelector(
            {
                ...this.settings.testSelector,
                buttonSettings:
                {
                    ...this.settings.testSelector.buttonSettings,
                    color: this.settings.testSelector.buttonSettings.color || this.settings.tests.color
                }
            },
            {
                items: this.tests
            });
            this._testsPanel.onSelected(this.handleTestButtonSelected);

            this.createUI(gameVersion);

            const ticker = RS.ITicker.get();
            ticker.timeScale = RS.URL.getParameterAsNumber("timescale", ticker.timeScale);

            // Restore hidden state
            if (this._isHidden)
            {
                this._ui.alpha = 0.0;
                this._ui.visible = false;
                this._ui.collapsed = true;
            }
        }

        /** Attaches the dev tools to the specified container at the specified position. */
        public attach(container: RS.Rendering.IContainer, props?: Partial<Flow.ElementProperties>): void
        {
            const oldParent = this._ui.parent;
            if (oldParent)
            {
                oldParent.removeChild(this._ui);
            }
            if (oldParent instanceof Flow.BaseElement)
            {
                oldParent.invalidateLayout();
            }
            if (container != null)
            {
                container.addChild(this._ui);
                this._ui.invalidateLayout();
            }
            if (props != null) { this._ui.apply(props); }
            // Update debug layer visibility, it will now be shown only when the UI is shown
            this.settings.viewController.debugLayerVisibility = !this._ui.collapsed;
        }

        public detach(container?: RS.Rendering.IContainer)
        {
            if (this._ui == null)
            {
                Log.warn("Attempted to detach DevTools UI while it is null");
                return;
            }
            if (container && this._ui.parent != container)
            {
                Log.warn("Attempted to detach DevTools UI from container it is not a child of.");
                return;
            }
            if (this._ui.parent)
            {
                this._ui.parent.removeChild(this._ui);
            }
            else
            {
                Log.warn("Attempted to detach DevTools UI from null parent");
                return;
            }
        }

        /** Disposes this controller. */
        public dispose()
        {
            if (this._disposed) { return; }
            this._disposed = true;
        }

        protected createUI(gameVersion: Version): void
        {
            if (this._ui && !this._ui.isDisposed)
            {
                Log.warn("[DevToolsController] .create() called again when dev tools ui already exists.");
                return;
            }

            this._gameVersion = gameVersion;

            let titleText: string;
            if (this.settings.engine && this.settings.engine.version != null)
            {
                titleText = `C: ${Version.toString(gameVersion)}\nE: ${Version.toString(this.settings.engine.version)}`;
            }
            else
            {
                titleText = Version.toString(gameVersion);
            }

            let ntButton = this.settings.toggleNTOn;
            if (this.showToggleNTButton)
            {
                this._ntOverride = RS.Storage.load("isNT", false, true);
                if (this._ntOverride)
                {
                    ntButton = this.settings.toggleNTOff;
                }
            }

            this._ui = UI.buttonList.create({
                ...UI.ButtonList.defaultSettings,
                ...this.settings.buttonList,
                titleLabel:
                {
                    ...Flow.Label.defaultSettings,
                    text: titleText,
                    spacing: Flow.Spacing.all(3),
                    dock: Flow.Dock.Fill,
                    sizeToContents: true,
                    expand: Flow.Expand.Disallowed
                },
                buttons:
                [
                    this.showDemoButton ? this.settings.demo : null,
                    this.showTestButton ? this.settings.tests : null,
                    this.showPauseButton ? this.settings.pause : null,
                    this.showReplayButton ? this.settings.replay : null,
                    this.showGREGButton ? this.settings.GREG : null,
                    this.showTimeButton ? this.settings.time : null,
                    this.showHideButton ? this.settings.hide : null,
                    this.showScreenButton ? this.settings.screen : null,
                    this.showMusicMuteButton ? this.settings.muteMusic : null,
                    this.showDumpButton ? this.settings.dump : null,
                    this.showToggleNTButton ? ntButton : null
                ].filter((b) => b != null),
                dock: Flow.Dock.Float
            });
            this._ui.onButtonClicked(this.handleButtonClicked);
            this._ui.onCollapsed(this.handleUICollapsed);
            this._ui.onOpened(this.handleUIOpened);

            const ticker = RS.ITicker.get();
            ticker.timeScale = RS.URL.getParameterAsNumber("timescale", ticker.timeScale);

            // Restore hidden state
            if (this._isHidden)
            {
                this._ui.alpha = 0.0;
                this._ui.visible = false;
                this._ui.collapsed = true;
            }
        }

        @Callback
        protected async handleButtonClicked(index: number)
        {
            const settings = this._ui.settings.buttons[index];
            if (settings === this.settings.demo)
            {
                if (this._demoPanel.isOpen)
                {
                    this._demoPanel.close();
                    this._showButtonAreaHandle = null;
                    this._showTurboButtonHandle = null;
                    this._showMeterPanelHandle = null;
                }
                else
                {
                    this._demoPanel.runtimeData.parent = this._ui.parent;
                    this._demoPanel.open();
                    if (this.settings.showButtonAreaArbiter)
                    {
                        this._showButtonAreaHandle = this.settings.showButtonAreaArbiter.declare(false);
                    }
                    if (this.settings.showTurboButtonArbiter)
                    {
                        this._showTurboButtonHandle = this.settings.showTurboButtonArbiter.declare(false);
                    }
                    if (this.settings.showMeterPanelArbiter)
                    {
                        this._showMeterPanelHandle = this.settings.showMeterPanelArbiter.declare(false);
                    }
                }
            }
            else if (settings === this.settings.tests)
            {
                if (this._testsPanel.isOpen)
                {
                    this._testsPanel.close();
                    this._showButtonAreaHandle = null;
                    this._showTurboButtonHandle = null;
                    this._showMeterPanelHandle = null;
                }
                else
                {
                    this._testsPanel.runtimeData.parent = this._ui.parent;
                    this._testsPanel.open();
                    if (this.settings.showButtonAreaArbiter)
                    {
                        this._showButtonAreaHandle = this.settings.showButtonAreaArbiter.declare(false);
                    }
                    if (this.settings.showTurboButtonArbiter)
                    {
                        this._showTurboButtonHandle = this.settings.showTurboButtonArbiter.declare(false);
                    }
                    if (this.settings.showMeterPanelArbiter)
                    {
                        this._showMeterPanelHandle = this.settings.showMeterPanelArbiter.declare(false);
                    }
                }
            }
            else if (settings === this.settings.pause)
            {
                if (this._pauseHandle)
                {
                    this._pauseHandle = null;
                    this._ui.getButton(index).text = this.settings.pause.text;
                }
                else
                {
                    this._pauseHandle = this.settings.pauseArbiter.declare(true);
                    this._ui.getButton(index).text = this.settings.unpauseText;
                }
            }
            else if (settings === this.settings.time)
            {
                if (this._timeSpinner != null)
                {
                    this._timeSpinner = null;
                }
                else
                {
                    const timeBtn = this._ui.getButton(index);
                    this._timeSpinner = Flow.spinner.create(this.settings.timeSpinner, timeBtn);
                    this._timeSpinner.x = timeBtn.layoutSize.w;
                    this._timeSpinner.value = ITicker.get().timeScale;
                    this._timeSpinner.onValueChanged(this.handleTimeChange);
                }
            }
            else if (settings === this.settings.replay)
            {
                if (this._replayPanel.isOpen)
                {
                    this._replayPanel.close();
                }
                else
                {
                    this._replayPanel.runtimeData.parent = this._ui.parent;
                    if (this.settings.engine.supportsReplay)
                    {
                        this._replayPanel.runtimeData.items["Current Session"] =
                        {
                            isCategory: true,
                            items: await this.settings.engine.getReplayData()
                        };
                    }

                    this._replayPanel.open();
                }
            }
            else if (settings === this.settings.hide)
            {
                if (this._hideTimeSpinner != null)
                {
                    this._hideTimeSpinner = null;

                    // also remove button
                    this._hideTimeGoBtn = null;
                }
                else
                {
                    const hideBtn = this._ui.getButton(index);

                    hideBtn.suppressLayouting();

                    // add a hide button
                    this._hideTimeGoBtn = UI.button.create(this.settings.hideApply, hideBtn);
                    this._hideTimeGoBtn.x = hideBtn.layoutSize.w;
                    this._hideTimeGoBtn.y = hideBtn.layoutSize.h;
                    this._hideTimeGoBtn.onClicked(() => this.hideUIForPeriod());

                    // add a spinner for time period
                    this._hideTimeSpinner = Flow.spinner.create(this.settings.hideSpinner, hideBtn);
                    this._hideTimeSpinner.optionIndex = this._hideForTimeIndex;
                    this._hideTimeSpinner.x = hideBtn.layoutSize.w;
                    this._hideTimeSpinner.y = 0;
                    this._hideTimeSpinner.onValueChanged(this.handleHideTimeChange);

                    hideBtn.restoreLayouting(true);
                }
            }
            else if (settings === this.settings.screen)
            {
                await this.captureScreenShot();
            }
            else if (settings === this.settings.muteMusic)
            {
                // Toggle the music being at 0 volume
                if (this._musicVolumeHandle)
                {
                    this._musicVolumeHandle = null;
                    this._ui.getButton(index).text = "MUSIC ON";
                }
                else if (this.settings.musicVolumeArbiter)
                {
                    this._musicVolumeHandle = this.settings.musicVolumeArbiter.declare(0);
                    this._ui.getButton(index).text = "MUSIC OFF";
                }
            }
            else if (settings === this.settings.dump)
            {
                if (this.settings.viewController)
                {
                    await this.captureScreenShot();
                }
                else
                {
                    Log.warn("[DevTools] No view controller given, unable to capture screenshot.");
                }
                IDump.get().save();
            }
            else if (GREGEnabled && settings === this.settings.GREG)
            {
                if (this._GREGPanel.isOpen)
                {
                    this._GREGPanel.close();
                }
                else
                {
                    const GREGData = await this.settings.engine.getGREGData();
                    this._GREGPanel.runtimeData.parent = this._ui.parent;
                    this._GREGPanel.runtimeData.items = GREGData;

                    this._GREGPanel.open();
                }
            }
            else if (settings === this.settings.toggleNTOn || settings === this.settings.toggleNTOff)
            {
                if (this._ntOverride)
                {
                    Storage.delete("isNT", true)
                }
                else
                {
                    Storage.save("isNT", true, true);
                }

                //reload the page in order to force a refresh of everything that'd rely on NT
                location.reload();
            }
        }

        @Callback
        protected async handleUICollapsed()
        {
            if (this._demoPanel.isOpen)
            {
                this._demoPanel.close();
            }

            if (this._testsPanel.isOpen)
            {
                this._testsPanel.close();
            }

            if (this._replayPanel.isOpen)
            {
                this._replayPanel.close();
            }

            const opened = this._demoPanel.isOpen || this._testsPanel.isOpen;
            if (opened)
            {
                this._showButtonAreaHandle = null;
                this._showTurboButtonHandle = null;
                this._showMeterPanelHandle = null;
            }
            this.settings.viewController.debugLayerVisibility = false;
        }

        @Callback
        protected async handleUIOpened()
        {
            this.settings.viewController.debugLayerVisibility = true;
        }

        protected async hideUIForPeriod()
        {
            const timeToHideFor = 1000 * (Is.number(this._hideTimeSpinner.value) ? this._hideTimeSpinner.value : parseInt(this._hideTimeSpinner.value));

            this.hideUI();

            try
            {
                await (this._hideTween = RS.Tween.wait<IController>(timeToHideFor, this));
            }
            catch (err)
            {
                if (err instanceof RS.Tween.DisposedRejection)
                {
                    return;
                }
                else
                {
                    throw err;
                }
            }

            // If we're not hidden any more, something else showed us, so early out
            if (!this._isHidden) { return; }

            this.showUI();
        }

        protected hideUI(): void
        {
            if (this._isHidden) { return; }

            this.collapseMenus();

            // Hide UI
            this._ui.alpha = 0.0;
            this._ui.visible = false;
            this._isHidden = true;

            // Hide the screen resolution text
            if (this.settings.viewController) { this.settings.viewController.debugLayerVisibility = false; }
        }

        protected showUI(): void
        {
            if (!this._isHidden) { return; }

            // Show UI
            this._ui.visible = true;
            this._ui.alpha = 1.0;
            this._isHidden = false;

            // Show the screen resolution text
            if (this.settings.viewController) { this.settings.viewController.debugLayerVisibility = true; }
        }

        /**
         * Collapse all open menus, spinners etc but not the button list itself.
         */
        protected collapseMenus()
        {
            if (this._replayPanel.isOpen)
            {
                this._replayPanel.close();
            }

            if (this._testsPanel.isOpen)
            {
                this._testsPanel.close();
            }

            if (this._demoPanel.isOpen)
            {
                this._demoPanel.close();
            }

            this._timeSpinner = null;
            this._hideTimeSpinner = null;
            this._hideTimeGoBtn = null;
        }

        @Callback
        protected handleDemoButtonSelected(selection: Helpers.ItemSelection<OneOrMany<Engine.ForceResult>>)
        {
            let forceResult: OneOrMany<Engine.ForceResult> | null = null;
            if (selection.items.length === 1)
            {
                // Only one force to apply, just clone it
                const item = selection.items[0];
                if (Is.array(item))
                {
                    forceResult = [ ...item ];
                }
                else
                {
                    forceResult = { ...item };
                }
            }
            else if (selection.items.length > 1)
            {
                // Multiple forces to additively apply, merge into one
                let combinedItem: OneOrMany<Engine.ForceResult> | null = null;
                for (const item of selection.items)
                {
                    if (Is.array(item))
                    {
                        if (combinedItem == null)
                        {
                            combinedItem = [ ...item ];
                        }
                        else if (Is.array(combinedItem))
                        {
                            for (let i = 0, l = Math.min(item.length, combinedItem.length); i < l; ++i)
                            {
                                combinedItem[i] = { ...combinedItem[i], ...item[i] };
                            }
                        }
                        else
                        {
                            combinedItem = [ { ...combinedItem, ...item[0] }, ...item.slice(1) ];
                        }
                    }
                    else
                    {
                        if (combinedItem == null)
                        {
                            combinedItem = { ...item };
                        }
                        else if (Is.array(combinedItem))
                        {
                            combinedItem[0] = { ...combinedItem[0], ...item };
                        }
                        else
                        {
                            combinedItem = { ...combinedItem, ...item };
                        }
                    }
                }
                forceResult = combinedItem;
            }
            if (forceResult)
            {
                // This is a bit hacky but otherwise the engine tries to clone it, including __state and constraint, and it ends badly
                delete (forceResult as any).__state;
                delete (forceResult as any).constraint;
                if (Is.array(forceResult))
                {
                    for (const item of forceResult)
                    {
                        delete (item as any).__state;
                        delete (item as any).constraint;
                    }
                }
                // Apply the force
                this.settings.engine.force(forceResult);
            }
            if (this._demoPanel.isOpen && selection.final)
            {
                this._demoPanel.close();
                this._showButtonAreaHandle = null;
                this._showTurboButtonHandle = null;
                this._showMeterPanelHandle = null;
            }
        }

        @Callback
        protected handleReplayButtonSelected(replayData: {}): void
        {
            this.settings.engine.replay(replayData);
        }

        @Callback
        protected handleGREGButtonSelected(selection): void
        {
            // GREG replays are in the format stake¬win¬initResponse¬spinResponse1¬...¬spinResponseN
            const GREGData: Engine.GREGData = selection.items[0];
            let output = `${GREGData.stake}¬${GREGData.win}¬${GREGData.initResponse}¬${GREGData.networkResponses.join("¬")}`;
            const blob = new Blob([output], { type: "application/octet-stream" });
            URL.download(`GREG ${GREGData.id}.txt`, blob);
        }

        @Callback
        protected handleTestButtonSelected(entry: Helpers.ItemSelection<() => void>)
        {
            if (entry.items != null && entry.items.length > 0)
            {
                entry.items[0]();
            }

            if (this._testsPanel.isOpen)
            {
                this._testsPanel.close();
                this._showButtonAreaHandle = null;
                this._showTurboButtonHandle = null;
                this._showMeterPanelHandle = null;
            }
        }

        @Callback
        protected handleTimeChange(newValue: number)
        {
            ITicker.get().timeScale = newValue;
        }

        @Callback
        protected handleHideTimeChange(newValue: number)
        {
            this._hideForTimeIndex = this._hideTimeSpinner.optionIndex;
        }

        @Callback
        protected handleKeyPress(ev: KeyboardEvent)
        {
            // Only handle keyboard shortcuts if the UI exists
            if (!this._ui || this._ui.isDisposed) { return; }
            // Don't capture inputs if the user is focused on an input element or other.
            if (document.activeElement !== document.body) { return; }
            switch (ev.code)
            {
                case "KeyH":

                    if (this._isHidden)
                    {
                        this.showUI();
                    }
                    else
                    {
                        this.hideUI();
                    }
                    this._hideTween = null;
                    ev.preventDefault();

                    break;
            }
        }

        private async captureScreenShot()
        {
            this._ui.visible = false;
            this.settings.viewController.debugLayerVisibility = false;
            await RS.Tween.wait(1, null, { ignoreGlobalPause: true }); // get a black picture otherwise
            this.settings.viewController.stage.capture();
            this.settings.viewController.debugLayerVisibility = !this._ui.collapsed;
            this._ui.visible = true;
        }
    }

    IController.register(Controller);
}
