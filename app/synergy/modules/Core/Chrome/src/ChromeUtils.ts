namespace RS
{
    /**
     * Chrome browser utilities.
     */
    export class Chrome
    {
        private constructor() { }

        /**
         * Gets if the current browser is Chrome.
         */
        public static get isChrome()
        {
            return Device.browser.indexOf("Chrome") > -1;
        }

        /**
         * Gets if the current browser is mobile Chrome.
         */
        public static get isMobileChrome()
        {
            return this.isChrome && Device.isMobileDevice;
        }
    }
}
