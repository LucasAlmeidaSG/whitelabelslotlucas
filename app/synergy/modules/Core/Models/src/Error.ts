namespace RS.Models
{
    /**
     * Model for errors.
     */
    export interface Error
    {
        /** The type of the current error (if any). */
        type: Error.Type;

        /** Is this a fatal error?. */
        fatal?: boolean;

        /** The error title or header associated with the current error. */
        title?: Localisation.LocalisableString;

        /** The message associated with the current error. */
        message?: Localisation.LocalisableString;

        /** Options for the current error. */
        options?: Error.Option[];
    }

    export namespace Error
    {
        /**
         * Represents the type of an error.
         */
        export enum Type
        {
            /** No error. */
            None,

            /** Server sent an error back in a response. */
            Server,

            /** Error generated by the game rather than received remotely  */
            Client
        }

        /**
         * A possible option that may execute after an error.
         */
        export enum OptionType
        {
            /** Game should recover and continue. */
            Continue,

            /** Game should close. */
            Close,

            /** Game should restart. */
            Refresh
        }

        /**
         * An option the user may choose when presented with an error.
         */
        export interface Option
        {
            type: OptionType;
            text: Localisation.LocalisableString;
        }

        /** Populates the error model with default values. */
        export function clear(error: Error): void
        {
            error.type = Type.None;
            error.fatal = null;
            error.title = null;
            error.message = null;
            error.options = null;
        }
    }
}