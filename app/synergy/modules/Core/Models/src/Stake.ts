namespace RS.Models
{
    /**
     * Represents an amount of stake and it's type.
     */
    export interface StakeAmount
    {
        value: number;
    }

    export interface StakeOverride
    {
        totalBet?: number;
    }

    /**
     * Model for stake data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     */
    export interface Stake
    {
        /** Bet options for the primary bet kind. */
        betOptions: StakeAmount[];

         /** Index of the default bet option. */
        defaultBetIndex: number;

        /** Index of the current selected bet option. */
        currentBetIndex: number;

        /** Overridden stake data. */
        override?: StakeOverride
    }

    export namespace Stake
    {
        /**
         * Gets the currently selected bet.
         * @param model
         */
        export function getCurrentBet(model: Stake): StakeAmount
        {
            return model.override ? { value: model.override.totalBet } : model.betOptions[model.currentBetIndex];
        }

        /** Populates the stake model with default values. */
        export function clear(stake: Stake): void
        {
            stake.betOptions = [];
            stake.currentBetIndex = 0;
            stake.defaultBetIndex = 0;
        }
    }
}
