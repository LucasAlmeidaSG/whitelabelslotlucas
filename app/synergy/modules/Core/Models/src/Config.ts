namespace RS.Models
{
    /**
     * Model for general game config.
     */
    export interface Config
    {
        /** The maximum win amount per play. */
        maxWinAmount?: number;

        /** The smallest the RTP can be for the current game variant. */
        minRTP?: number;

        /** The highest the RTP can be for the current game variant. */
        maxRTP?: number;

        /** Whether we are in "Real Money" mode or not.
         * Real Money mode refers to spins having monetary value to the player,
         * meaning that the necessary currency data is sent to the client and all spins will be recorded in the platform for replay purposes. */
        isFreePlay?: boolean;

        /** Whether the turbo mode functionality is enabled. */
        turboModeEnabled?: boolean;
    }

    export namespace Config
    {
        /** Populates the config model with default values. */
        export function clear(config: Config): void
        {
            config.maxWinAmount = null;
            config.isFreePlay = false;
            config.turboModeEnabled = false;
        }
    }
}