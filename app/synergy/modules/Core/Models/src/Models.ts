/// <reference path="Customer.ts" />
/// <reference path="Error.ts" />

namespace RS
{
    /**
     * Encapsulate all models common to a game.
     */
    export interface Models
    {
        error: Models.Error;
        customer: Models.Customer;
        config: Models.Config;
        state: Models.State;
        stake: Models.Stake;
        /** Seed for random number generators in the context of the current wager. */
        seed: number;
        //gameResults: GameResults;
    }

    export namespace Models
    {
        /** Populates the models with default values. */
        export function clear(models: Models): void
        {
            models.error = models.error || {} as Models.Error;
            Models.Error.clear(models.error);

            models.customer = models.customer || {} as Models.Customer;
            Models.Customer.clear(models.customer);

            models.config = models.config || {} as Models.Config;
            Models.Config.clear(models.config);

            models.state = models.state || {} as Models.State;
            Models.State.clear(models.state);

            models.stake = models.stake || {} as Models.Stake;
            Models.Stake.clear(models.stake);

            models.seed = null;

            // models.gameResults = models.gameResults || [];
            // Models.GameResults.clear(models.gameResults);
        }
    }
}
