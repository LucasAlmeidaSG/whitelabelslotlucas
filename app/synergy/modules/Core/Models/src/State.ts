namespace RS.Models
{
    /**
     * Model for the current game state.
     */
    export interface State
    {
        /** The current state of the game, as far as the engine sees. */
        state: State.Type;
        /** Whether or not this is a replay of a previous wager. */
        isReplay: boolean;
        /** Whether or not replay is finished. */
        isReplayComplete: boolean;
        /** Whether or not max win has been hit in this wager. */
        isMaxWin: boolean;
    }

    export namespace State
    {
        /**
         * Possible states the game can be in.
         */
        export enum Type { Open, WaitingForUserInput, Closed }

        /** Populates the state model with default values. */
        export function clear(state: State): void
        {
            state.state = Type.Closed;
            state.isReplay = false;
            state.isReplayComplete = false;
            state.isMaxWin = false;
        }
    }
}
