/// <reference path="Balances.ts" />

namespace RS.Models
{
    /**
     * Model for customer data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     */
    export interface Customer
    {
        /** The balances for the customer at the end of the current play. */
        finalBalance: Balances;

        /** 3 digit currency code for the customer. */
        currencyCode: string;

        /** Multiplier for the customer's currency. */
        currencyMultiplier: number;

        /** Specific settings for the customer's currency. */
        currencySettings?: Localisation.CurrencySettings;
    }

    export namespace Customer
    {
        /** Populates the customer model with default values. */
        export function clear(customer: Customer): void
        {
            customer.finalBalance = customer.finalBalance || {} as Balances;
            Balances.clear(customer.finalBalance);
            customer.currencyCode = "GBP";
            customer.currencyMultiplier = 1.0;
            customer.currencySettings = null;
        }
    }
}