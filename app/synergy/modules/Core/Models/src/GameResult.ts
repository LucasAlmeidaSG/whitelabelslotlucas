namespace RS.Models
{
    export interface Win
    {
        /** Total win amount for this game's win. */
        totalWin: number;

        /** The win accumulated from the moment the current wager was made until this game. */
        accumulatedWin: number;
    }
    export namespace Win
    {
        export function clear(win: Win)
        {
            win.totalWin = 0;
            win.accumulatedWin = 0;
        }
    }

    export namespace GameResult
    {
        export function clear(gameResult: GameResult): void
        {
            gameResult.index = 0;
            gameResult.win = {} as Win;
            Win.clear(gameResult.win);
        }

        export function clone(gameResult: GameResult): GameResult
        {
            return {
                index: gameResult.index,
                win:
                {
                    accumulatedWin: gameResult.win.accumulatedWin,
                    totalWin: gameResult.win.totalWin
                },
                balanceBefore: RS.Models.Balances.clone(gameResult.balanceBefore),
                balanceAfter: RS.Models.Balances.clone(gameResult.balanceAfter)
            }
        }
    }

    /**
     * Model for the result of a game
     */
    export interface GameResult
    {
        /** Index of this game within the current batch. Relevant only for respin/streak features or cascade games. */
        index: number;

        /** Win data for this game. */
        win: Win;

        /** Customer's balance before this game. */
        balanceBefore: RS.Models.Balances;

        /** Customer's balance after this game. */
        balanceAfter: RS.Models.Balances;
    }

    /**
     * Multiple game results.
     */
    export type GameResults = GameResult[];

    export namespace GameResults
    {
        /** Populates the game results model with default values. */
        export function clear(gameResults: GameResults): void
        {
            gameResults.length = 0;
        }

        /** Creates a deep clone of the game results model. */
        export function clone(gameResult: GameResults): GameResults
        {
            return gameResult.map((sr) => GameResult.clone(sr));
        }
    }
}
