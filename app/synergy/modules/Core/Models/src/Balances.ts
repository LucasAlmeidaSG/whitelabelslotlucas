namespace RS.Models
{
    /**
     * Model for balance data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     */
    export interface Balances
    {
        /** Primary balance. */
        primary: number;

        /** Named balances. */
        named: Map<number>;
    }

    /** Creates a new Balances, optionally using a given primary balance. */
    export function Balances(primary: number = 0): Balances
    {
        return { primary, named: {} };
    }

    export namespace Balances
    {
        /** Populates the balances model with default values. */
        export function clear(balances: Balances): void
        {
            balances.primary = 0;
            balances.named = {};
        }

        /** Creates a deep clone of the balances model. */
        export function clone(balances: Balances): Balances
        {
            return {
                primary: balances.primary,
                named: Util.clone(balances.named, false)
            };
        }

        /** An amount of real money. */
        export const Cash = "cash";

        /** An amount of free promotional money. */
        export const Promotional = "freebets";
    }
}