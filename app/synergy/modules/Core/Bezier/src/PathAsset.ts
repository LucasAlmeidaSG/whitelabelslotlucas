/// <reference path="Path.ts" />

namespace RS.Bezier
{
    /**
     * A reference to a path asset.
     */
    export type PathReference = Asset.Reference<"path">;

    export interface PathData
    {
        "boundingBox": { "x": number, "y": number, "w": number, "h": number };
        "points": { "x": number, "y": number, "cp1x": number, "cp1y": number, "cp2x": number, "cp2y": number }[];
        "looping": boolean;
    }

    /**
     * Encapsulates a path asset type.
     */
    @Asset.Type("path")
    export class PathAsset extends Asset.Base<Path>
    {        
        protected _rawData: PathData;

        /**
         * Creates a new bezier path object for this asset.
         * @param looping 
         */
        public createBezierPath(): Path
        {
            const segments: Curve[] = [];
            const pts = this._rawData["points"];
            const looping  = this._rawData["looping"] || false;
            if (pts)
            {
                for (let i = (looping ? 0 : 1); i < pts.length; i++)
                {
                    const prevI = i === 0 ? pts.length - 1 : i - 1;
                    const pt = pts[i];
                    const prevPt = pts[prevI];

                    const hasCP1 = pt["cp1x"] != null && pt["cp1y"] != null;
                    const hasCP2 = pt["cp2x"] != null && pt["cp2y"] != null;

                    const curve = new Curve([
                        { x: prevPt["x"], y: prevPt["y"] },
                        {
                            x: hasCP1 ? pt["cp1x"] : hasCP2 ? pt["cp2x"] : Math.linearInterpolate(prevPt["x"], pt["x"], 0.5),
                            y: hasCP1 ? pt["cp1y"] : hasCP2 ? pt["cp2y"] : Math.linearInterpolate(prevPt["y"], pt["y"], 0.5)
                        },
                        {
                            x: hasCP2 ? pt["cp2x"] : hasCP1 ? pt["cp1x"] : Math.linearInterpolate(prevPt["x"], pt["x"], 0.5),
                            y: hasCP2 ? pt["cp2y"] : hasCP1 ? pt["cp1y"] : Math.linearInterpolate(prevPt["y"], pt["y"], 0.5)
                        },
                        { x: pt["x"], y: pt["y"] }
                    ]);
                    segments.push(curve);
                }
            }
            return new Path(segments, looping);
        }

        /**
         * Selects a format to use from the given options.
         * @param options 
         */
        public selectFormat(options: string[]): string | null
        {
            return options[0];
        }

        public clone(): PathAsset
        {
            return new PathAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url 
         */
        protected async performLoad(url: string, format: string)
        {
            this._rawData = await Request.ajax({ url }, Request.AJAXResponseType.JSON) as PathData;
            return this.createBezierPath();
        }
    }
}