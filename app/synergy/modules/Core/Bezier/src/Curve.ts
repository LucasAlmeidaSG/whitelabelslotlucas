/// <reference path="Utils.ts" />

namespace RS.Bezier
{
    import Math = RS.Math;
    const { abs, cos, sin, acos, atan2, sqrt, pow } = Math;

    const tmpLine = Math.Line2D();

    interface ABC
    {
        A: Math.Vector2D;
        B: Math.Vector2D;
        C: Math.Vector2D;
    }

    const tmpABC: ABC = { A: Math.Vector2D(), B: Math.Vector2D(), C: Math.Vector2D() };
    const tmpAABB1 = Math.AABB2D(), tmpAABB2 = Math.AABB2D();
    const tmpVec1 = Math.Vector2D(), tmpVec2 = Math.Vector2D();
    const tmpCCenter: Utils.CCenterResult = { x: 0, y: 0, e: 0, r: 0, s: 0 };
    const tmpExtrema: Curve.ExtremaResult = { x: [], y: [], values: [] };

    // TODO: any allocations that can be avoided (e.g. by using a tmp object in local class scope) are marked with // ALLOCATION
    // These should be optimised out in the future

    /**
     * Encapsulates an immutable single N-order bezier curve where N > 1.
     */
    @RS.HasCallbacks
    export class Curve
    {
        protected _order: number;
        protected _points: ReadonlyArray<Math.ReadonlyVector2D>;
        protected _linear: boolean;
        protected _t1: number;
        protected _t2: number;
        protected _clockwise: boolean;

        protected _derivativeFunc: Utils.DerivativeFunc;

        protected _lut: Math.Vector2D[] = [];
        protected _dpoints: Math.Vector2D[][] = [];

        protected _startWeight: number = 1
        protected _endWeight: number = 1;

        /**
         * Gets the derived points of this curve.
         */
        public get dpoints(): ReadonlyArray<ReadonlyArray<Math.ReadonlyVector2D>> { return this._dpoints; }

        /**
         * Finds the length of this curve.
         */
        public get length()
        {
            return Utils.length(this._derivativeFunc);
        }

        /**
         * Gets if this curve is considered "simple" (all control points on the same side of the baseline).
         */
        public get simple()
        {
            if (this._order === 3)
            {
                const a1 = Utils.angle(this._points[0], this._points[3], this._points[1]);
                const a2 = Utils.angle(this._points[0], this._points[3], this._points[2]);
                if ((a1 > 0 && a2 < 0) || (a1 < 0 && a2 > 0)) { return false; }
            }
            const n1 = this.normal(0, tmpVec1);
            const n2 = this.normal(1, tmpVec2);
            const s = Math.Vector2D.dot(n1, n2);
            const angle = abs(acos(s));
            return angle < Math.PI / 3;
        }

        /**
         * Gets the points of this curve.
         */
        public get points() { return this._points; }

        /**
         * Gets the order of this curve.
         */
        public get order() { return this._order; }

        public get startWeight() { return this._startWeight; }
        public set startWeight(value) { this._startWeight = value; }
        public get endWeight() { return this._endWeight; }
        public set endWeight(value) { this._endWeight = value; }

        public constructor(coords: ReadonlyArray<Math.ReadonlyVector2D>);

        public constructor(coords: Math.Vector2DList);

        public constructor(coords: ReadonlyArray<Math.ReadonlyVector2D> | Math.Vector2DList)
        {
            if (coords instanceof Math.Vector2DList)
            {
                this._points = coords.toArray();
            }
            else
            {
                this._points = coords.map((v) => Math.Vector2D.clone(v));
            }
            this._order = this._points.length - 1;

            Math.Line2D.fromPoints(this._points[0], this._points[this._order], tmpLine);
            const a = Utils.align(this._points, tmpLine);
            this._linear = true;
            for (let i = 0, l = a.length; i < l; ++i)
            {
                if (abs(a[i].y) > 0.0001)
                {
                    this._linear = false;
                    break;
                }
            }

            this._derivativeFunc = this.derivative.bind(this);

            this._t1 = 0;
            this._t2 = 1;
            this.update();
        }

        // #region Statics

        /**
         * Creates a new quadratic curve through p1 and p3, using p2 as a control point.
         * @param p1 
         * @param p2 
         * @param p3 
         * @param t 
         */
        public static quadraticFromPoints(p1: Math.ReadonlyVector2D, p2: Math.ReadonlyVector2D, p3: Math.ReadonlyVector2D, t: number = 0.5): Curve
        {
            // shortcuts, although they're really dumb
            if (t === 0) { return new Curve([p2, p2, p3]); }
            if (t === 1) { return new Curve([p1, p2, p2]); }
            // real fitting.
            const abc = this.getABC(2, p1, p2, p3, t, tmpABC);
            return new Curve([p1, abc.A, p3]);
        }

        /**
         * Creates a new cubic curve through S and E, using B as a control point.
         * @param S 
         * @param B 
         * @param E 
         * @param t 
         * @param d1 
         */
        public static cubicFromPoints(S: Math.ReadonlyVector3D, B: Math.ReadonlyVector3D, E: Math.ReadonlyVector3D, t: number = 0.5, d1?: number): Curve
        {
            const abc = this.getABC(3, S, B, E, t, tmpABC);
            if (d1 == null) { d1 = Math.Vector2D.distanceBetween(B, abc.C); }
            const d2 = d1 * (1 - t) / t;

            const selen = Math.Vector2D.distanceBetween(S, E),
                lx = (E.x - S.x) / selen,
                ly = (E.y - S.y) / selen,
                bx1 = d1 * lx,
                by1 = d1 * ly,
                bx2 = d2 * lx,
                by2 = d2 * ly;
            // derivation of new hull coordinates
            const e1 = { x: B.x - bx1, y: B.y - by1 }, // ALLOCATION
                e2 = { x: B.x + bx2, y: B.y + by2 }, // ALLOCATION
                A = abc.A,
                v1 = { x: A.x + (e1.x - A.x) / (1 - t), y: A.y + (e1.y - A.y) / (1 - t) }, // ALLOCATION
                v2 = { x: A.x + (e2.x - A.x) / t, y: A.y + (e2.y - A.y) / t }, // ALLOCATION
                nc1 = { x: S.x + (v1.x - S.x) / t, y: S.y + (v1.y - S.y) / t }, // ALLOCATION
                nc2 = {
                    x: E.x + (v2.x - E.x) / (1 - t),
                    y: E.y + (v2.y - E.y) / (1 - t)
                }; // ALLOCATION
            // ...done
            return new Curve([S, nc1, nc2, E]);
        }

        protected static curveIntersects(c1: ReadonlyArray<Curve>, c2: ReadonlyArray<Curve>, curveIntersectionThreshold: number, out?: [number, number][]): [number, number][]
        {
            out = out || [];
            const pairs: [ Curve, Curve ][] = []; // ALLOCATION

            // step 1: pair off any overlapping segments
            for (let i = c1.length - 1; i >= 0; --i)
            {
                const l = c1[i];
                for (let j = c2.length - 1; j >= 0; --j)
                {
                    const r = c2[j];
                    if (l.overlaps(r))
                    {
                        pairs.push([ l, r ]); // ALLOCATION
                    }
                }
            }
            
            // step 2: for each pairing, run through the convergence algorithm.
            for (const [ left, right ] of pairs)
            {
                const result = Curve.pairIteration(left, right, curveIntersectionThreshold); // ALLOCATION
                RS.ArrayUtils.copy(result, out);
            }
            return out;
        }

        protected static getABC(n: 2 | 3, S: Math.ReadonlyVector2D, B: Math.ReadonlyVector2D, E: Math.ReadonlyVector2D, t: number = 0.5, out?: ABC): ABC
        {
            out = out || { A: Math.Vector2D(), B: Math.Vector2D(), C: Math.Vector2D() };
            const u = Utils.projectionRatio(t, n),
                um = 1 - u,
                s = Utils.abcRatio(t, n);
            Math.Vector2D.set(out.C, u * S.x + um * E.x, u * S.y + um * E.y);
            Math.Vector2D.set(out.A, B.x + (B.x - out.C.x) / s, B.y + (B.y - out.C.y) / s);
            Math.Vector2D.copy(B, out.B);
            return out;
        }

        protected static pairIteration(c1: Curve, c2: Curve, curveIntersectionThreshold: number, out?: [number, number][]): [number, number][]
        {
            out = out || [];
            const c1b = c1.getBBox(tmpAABB1),
                c2b = c2.getBBox(tmpAABB2),
                r = 100000,
                threshold = curveIntersectionThreshold || 0.5;
            const c1xs = c1b.maxX - c1b.minX;
            const c1ys = c1b.maxY - c1b.minY;
            const c2xs = c2b.maxX - c2b.minX;
            const c2ys = c2b.maxY - c2b.minY;
            if (
                (c1xs + c1ys) < threshold &&
                (c2xs + c2ys) < threshold
            )
            {
                out.length = 1;
                out[0] = [ ((r * (c1._t1 + c1._t2) / 2) | 0) / r, ((r * (c2._t1 + c2._t2) / 2) | 0) / r ];
                return out;
            }
            const cc1 = c1.split(0.5),
                cc2 = c2.split(0.5),
                pairs = [
                    [ cc1.left, cc2.left ],
                    [ cc1.left, cc2.right ],
                    [ cc1.right, cc2.right ],
                    [ cc1.right, cc2.left ]
                ]; // ALLOCATION x5
            
            RS.ArrayUtils.filterInPlace(pairs, ([left, right]) =>
            {
                const leftbbox = left.getBBox(tmpAABB1);
                const rightbbox = right.getBBox(tmpAABB2);
                return Math.AABB2D.intersects(leftbbox, rightbbox);
            });
            
            if (pairs.length === 0) { return out; }

            for (const [left, right] of pairs)
            {
                const result = Curve.pairIteration(left, right, threshold); // ALLOCATION
                RS.ArrayUtils.copy(result, out);
            }
            RS.ArrayUtils.removeDuplicates(out, (a, b) => Math.abs(a[0] - b[0]) < Utils.epsilon && Math.abs(a[1] - b[1]) < Utils.epsilon);
            return out;
        }

        // #endregion

        /**
         * Generates a human-readable interpretation of this curve.
         */
        public toString(): string
        {
            return this.toSVG();
        }

        /**
         * Generates an SVG string from this curve.
         */
        public toSVG(): string
        {
            const arr: string[] = ["M", `${this._points[0].x}`, `${this._points[0].y}`, this._order === 2 ? "Q" : "C"];
            for (let i = 1, l = this._points.length; i < l; ++i)
            {
                const p = this._points[i];
                arr.push(`${p[i].x}`);
                arr.push(`${p[i].y}`);
            }
            return arr.join(" ");
        }

        /**
         * Creates a copy of this curve.
         */
        public clone(): Curve
        {
            return new Curve(this._points);
        }

        /**
         * Finds if the specified curve is on the point, and if so, the value of t along the curve.
         * @param point 
         * @param error 
         */
        public on(point: Math.ReadonlyVector2D, error: number = 5): boolean | number
        {
            const lut = this.getLUT();
            let hits = 0, t = 0;
            for (let i = 0, l = lut.length; i < l; ++i)
            {
                const c = lut[i];
                if (Math.Vector2D.distanceBetween(c, point) < error)
                {
                    ++hits;
                    t += i / l;
                }
            }
            if (hits === 0) { return false; }
            return t /= hits;
        }

        /**
         * Projects a point onto the curve, returning the projected point and value of t and d along the curve.
         * @param point 
         * @param out 
         */
        public project(point: Math.ReadonlyVector2D, out?: Math.Vector2D): Math.Vector2D & { t: number; d: number; }
        {
            out = out || Math.Vector2D();
            const outEx = out as Math.Vector2D & { t: number; d: number; };

            // If it's linear, let's do a line projection instead
            if (this._order === 1)
            {
                const line = Math.Line2D.fromPoints(this._points[0], this._points[this._points.length - 1], tmpLine);
                const lambda = Math.clamp(Math.Line2D.projectLambda(line, point), 0.0, 1.0);
                outEx.x = line.x + line.dx * lambda;
                outEx.y = line.y + line.dy * lambda;
                outEx.t = lambda;
                outEx.d = Math.Vector2D.distanceBetween(outEx, point);
                return outEx;
            }

            // step 1: coarse check
            const lut = this.getLUT(),
                l = lut.length - 1,
                closest = Utils.closest(lut, point),
                mpos = closest.mpos;
            let mdist = closest.mdist;
            if (mpos === 0 || mpos === l)
            {
                const t = mpos / l;
                this.compute(t, out);
                outEx.t = t;
                outEx.d = mdist;
                return outEx;
            }
            else
            {
                // step 2: fine check
                const t1 = (mpos - 1) / l, t2 = (mpos + 1) / l, step = 0.1 / l;
                mdist += 1;
                let t: number, ft: number;
                for (t = t1, ft = t; t < t2 + step; t += step)
                {
                    const p = this.compute(t, tmpVec1);
                    const d = Math.Vector2D.distanceBetween(point, p);
                    if (d < mdist)
                    {
                        mdist = d;
                        ft = t;
                    }
                }
                this.compute(ft, out);
                outEx.t = ft;
                outEx.d = mdist;
                return outEx;
            }
        }

        /**
         * Gets the point at the specified index.
         * @param idx 
         * @param out 
         */
        public getPoint(idx: number, out?: Math.Vector2D): Math.Vector2D
        {
            out = out || Math.Vector2D();
            Math.Vector2D.copy(this._points[idx], out);
            return out;
        }

        /**
         * Samples a point t along the curve.
         * @param t (0-1)
         * @param out 
         */
        public compute(t: number, out?: Math.Vector2D): Math.Vector2D
        {
            return Utils.compute(t, this._points, out);
        }

        /**
         * Creates a new curve from this one with a higher order.
         */
        public raise(): Curve
        {
            const p = this._points, k = p.length;
            const np = new Array<Math.Vector2D>(k + 1); // ALLOCATION
            np[0] = Math.Vector2D.clone(p[0]); // ALLOCATION
            for (let i = 1; i < k; i++)
            {
                const pi = p[i];
                const pim = p[i - 1];
                np[i] = Math.Vector2D(
                    (k - i) / k * pi.x + i / k * pim.x,
                    (k - i) / k * pi.y + i / k * pim.y
                ); // ALLOCATION
            }
            np[k] = p[k - 1];
            return new Curve(np);
        }

        /**
         * Finds the tangent at t along the curve.
         * @param t (0-1)
         * @param out 
         */
        public derivative(t: number, out?: Math.Vector2D): Math.Vector2D
        {
            out = out || Math.Vector2D();
            const mt = 1 - t, p = this._dpoints[0];
            if (this._order === 1)
            {
                out.x = p[0].x;
                out.y = p[0].y;
            }
            else if (this._order === 2)
            {
                const a = mt;
                const b = t;
                out.x = a * p[0].x + b * p[1].x;
                out.y = a * p[0].y + b * p[1].y;
            }
            else if (this._order === 3)
            {
                const a = mt * mt;
                const b = mt * t * 2;
                const c = t * t;
                out.x = a * p[0].x + b * p[1].x + c * p[2].x;
                out.y = a * p[0].y + b * p[1].y + c * p[2].y;
            }
            return out;
        }

        /**
         * Finds the curvature at t along the curve.
         * @param t (0-1)
         * @param out 
         */
        public curvature(t: number, out?: Utils.CurvatureResult): Utils.CurvatureResult
        {
            return Utils.curvature(t, this._points, out);
        }

        /**
         * Finds all inflections of the curve.
         * @param out 
         */
        public inflections(out?: number[]): number[]
        {
            return Utils.inflections(this._points, out);
        }

        /**
         * Finds the normal at t along the curve.
         * @param t (0-1)
         * @param out 
         */
        public normal(t: number, out?: Math.Vector2D): Math.Vector2D
        {
            out = out || Math.Vector2D();
            this.derivative(t, out);
            Math.Vector2D.normalise(out);
            const tmpX = out.x;
            out.x = -out.y;
            out.y = tmpX;
            return out;
        }

        /**
         * Finds the hull at t along the curve.
         * @param t (0-1)
         */
        public hull(t: number, out?: Math.Vector2D[]): Math.Vector2D[]
        {
            out = out || [];

            let p = this._points, idx = 0;

            out.length = 3;
            out[idx++] = Math.Vector2D.clone(p[0]); // ALLOCATION
            out[idx++] = Math.Vector2D.clone(p[1]); // ALLOCATION
            out[idx++] = Math.Vector2D.clone(p[2]); // ALLOCATION
            if (this._order === 3)
            {
                out.length = 4;
                out[idx++] = Math.Vector2D.clone(p[3]); // ALLOCATION
            }
            // we lerp between all points at each iteration, until we have 1 point left.
            while (p.length > 1)
            {
                const _p: Math.Vector2D[] = []; // ALLOCATION
                for (let i = 0, l = p.length - 1; i < l; i++)
                {
                    const pt = Math.Vector2D.linearInterpolate(p[i], p[i + 1], t); // ALLOCATION
                    out[idx++] = pt;
                    _p.push(pt);
                }
                p = _p;
            }
            return out;
        }

        /**
         * Splits this curve at t, returning the left and right sides of the split as new curves.
         * @param t (0-1)
         */
        public split(t: number): Curve.SplitResult;

        /**
         * Finds a sub-curve between t1 and t2.
         * @param t1 (0-1)
         * @param t2 (0-1)
         */
        public split(t1: number, t2: number): Curve;

        public split(t1: number, t2?: number): Curve.SplitResult | Curve
        {
            // shortcuts
            if (t1 === 0 && !!t2)
            {
                return this.split(t2).left;
            }
            if (t2 === 1)
            {
                return this.split(t1).right;
            }

            // no shortcut: use "de Casteljau" iteration.
            const q = this.hull(t1);
            const result: Curve.SplitResult =
            {
                left:
                    this._order === 2
                        ? new Curve([q[0], q[3], q[5]])
                        : new Curve([q[0], q[4], q[7], q[9]]),
                right:
                    this._order === 2
                        ? new Curve([q[5], q[4], q[2]])
                        : new Curve([q[9], q[8], q[6], q[3]]),
                span: q
            };

            // make sure we bind _t1/_t2 information!
            result.left._t1 = Utils.map(0, 0, 1, this._t1, this._t2);
            result.left._t2 = Utils.map(t1, 0, 1, this._t1, this._t2);
            result.right._t1 = Utils.map(t1, 0, 1, this._t1, this._t2);
            result.right._t2 = Utils.map(1, 0, 1, this._t1, this._t2);

            // if we have no t2, we're done
            if (!t2) { return result; }

            // if we have a t2, split again:
            t2 = Utils.map(t2, t1, 1, 0, 1);
            const subsplit = result.right.split(t2);
            return subsplit.left;
        }

        /**
         * Finds all extrema of the curve.
         */
        public extrema(out?: Curve.ExtremaResult): Curve.ExtremaResult
        {
            out = out || { x: [], y: [], values: [] };

            out.values.length = 0;

            if (this._order < 2)
            {
                out.x.length = 0;
                out.y.length = 0;
                return out;
            }

            const tmp: number[] = []; // ALLOCATION
            const tmp2: number[] = []; // ALLOCATION

            tmp.length = this._dpoints[0].length;
            for (let i = 0, l = this._dpoints[0].length; i < l; ++i) { tmp[i] = this._dpoints[0][i].x; }
            Utils.droots(tmp, out.x);
            if (this._order === 3)
            {
                tmp.length = this._dpoints[1].length;
                for (let i = 0, l = this._dpoints[1].length; i < l; ++i) { tmp[i] = this._dpoints[1][i].x; }
                Utils.droots(tmp, tmp2);
                RS.ArrayUtils.copy(tmp2, out.x);
            }
            out.x.sort();
            RS.ArrayUtils.filterInPlace(out.x, Utils.rootsReduce);
            RS.ArrayUtils.copy(out.x, out.values);

            tmp.length = this._dpoints[0].length;
            for (let i = 0, l = this._dpoints[0].length; i < l; ++i) { tmp[i] = this._dpoints[0][i].y; }
            Utils.droots(tmp, out.y);
            if (this._order === 3)
            {
                tmp.length = this._dpoints[1].length;
                for (let i = 0, l = this._dpoints[1].length; i < l; ++i) { tmp[i] = this._dpoints[1][i].y; }
                Utils.droots(tmp, tmp2);
                RS.ArrayUtils.copy(tmp2, out.y);
            }
            out.y.sort();
            RS.ArrayUtils.filterInPlace(out.y, Utils.rootsReduce);
            RS.ArrayUtils.copy(out.y, out.values);

            RS.ArrayUtils.removeDuplicates(out.values);
            out.values.sort();
            return out;
        }

        /**
         * Gets the bounding box of this curve.
         * @param out 
         */
        public getBBox(out?: Math.AABB2D): Math.AABB2D
        {
            out = out || Math.AABB2D();
            const extrema = this.extrema(tmpExtrema);
            if (extrema.values.indexOf(0) === -1) { extrema.values.push(0.0); }
            if (extrema.values.indexOf(1) === -1) { extrema.values.push(1.0); }
            const points = extrema.values.map((t) => this.compute(t)) // ALLOCATIONS
            return Math.AABB2D.fromPoints(points, out);
        }

        /**
         * Determines if this curve overlaps the other curve.
         * @param curve 
         */
        public overlaps(curve: Curve): boolean
        {
            const lbbox = this.getBBox(tmpAABB1);
            const tbbox = curve.getBBox(tmpAABB2);
            return Math.AABB2D.intersects(lbbox, tbbox);
        }

        /**
         * Finds the point that is offset by d along the normal from t along the curve.
         * @param t (0-1)
         * @param d 
         */
        public offset(t: number, d: number, out?: Math.Vector2D): Math.Vector2D & { c: Math.Vector2D; n: Math.Vector2D; };

        /**
         * Returns an array of new curves that together represent a curve offset from this curve by d.
         * @param t (0-1)
         * @param d 
         */
        public offset(d: number, out?: Curve[]): Curve[];

        public offset(p1: number, p2?: number | Curve[], p3?: Math.Vector2D): Curve[] | (Math.Vector2D & { c: Math.Vector2D; n: Math.Vector2D; })
        {
            if (Is.number(p2))
            {
                const t = p1, d = p2;
                const out = (p3 || Math.Vector2D()) as (Math.Vector2D & { c: Math.Vector2D; n: Math.Vector2D; });
                out.c = out.c || Math.Vector2D();
                this.compute(t, out.c);
                out.n = out.n || Math.Vector2D();
                this.normal(t, out.n);
                out.x = out.c.x + out.n.x * d;
                out.y = out.c.y + out.n.y * d;
                return out;
            }
            else
            {
                const out = p2 || [];
                const d = p1;
                if (this._linear)
                {
                    const nv = this.normal(0, tmpVec1);
                    const coords = this._points.map((p) => Math.Vector2D.addMultiply(p, nv, d)); // ALLOCATION
                    out.length = 1;
                    out[0] = new Curve(coords);
                    return out;
                }
                return this
                    .reduce()
                    .map((s) => s.scale(d));
            }
        }

        /**
         * Returns an array of "simple" curves that together make up this curve.
         * @param out 
         */
        public reduce(out?: Curve[]): Curve[]
        {
            const step = 0.01;
            
            // first pass: split on extrema
            const extrema = this.extrema().values; // ALLOCATION
            if (extrema.indexOf(0) === -1)
            {
                extrema.unshift(0);
            }
            if (extrema.indexOf(1) === -1)
            {
                extrema.push(1);
            }

            const pass1: Curve[] = []; // ALLOCATION
            for (let t1 = extrema[0], i = 1; i < extrema.length; i++)
            {
                const t2 = extrema[i];
                const segment = this.split(t1, t2);
                segment._t1 = t1;
                segment._t2 = t2;
                pass1.push(segment);
                t1 = t2;
            }

            // second pass: further reduce these segments to simple segments
            out.length = 0;
            for (const p1 of pass1)
            {
                let t1 = 0, t2 = 0;
                while (t2 <= 1)
                {
                    for (t2 = t1 + step; t2 <= 1 + step; t2 += step)
                    {
                        let segment = p1.split(t1, t2);
                        if (!segment.simple)
                        {
                            t2 -= step;
                            if (abs(t1 - t2) < step)
                            {
                                // we can never form a reduction
                                out.length = 0;
                                return out;
                            }
                            segment = p1.split(t1, t2);
                            segment._t1 = Utils.map(t1, 0, 1, p1._t1, p1._t2);
                            segment._t2 = Utils.map(t2, 0, 1, p1._t1, p1._t2);
                            out.push(segment);
                            t1 = t2;
                            break;
                        }
                    }
                }
                if (t1 < 1)
                {
                    const segment = p1.split(t1, 1);
                    segment._t1 = Utils.map(t1, 0, 1, p1._t1, p1._t2);
                    segment._t2 = p1._t2;
                    out.push(segment);
                }
            }
            return out;
        }

        /**
         * Returns a scaled version of this curve.
         * @param d 
         */
        public scale(d: number | ((t: number) => number)): Curve
        {
            const order = this._order;
            if (Is.func(d) && order === 2)
            {
                return this.raise().scale(d);
            }

            // TODO: add special handling for degenerate (=linear) curves.
            const clockwise = this._clockwise;
            const r0 = Is.func(d) ? d(0) : d;
            const r1 = Is.func(d) ? d(1) : d;
            const v0 = this.offset(0, 10, tmpVec1);
            const v1 = this.offset(1, 10, tmpVec2);
            const o = Utils.lli4(v0, v0.c, v1, v1.c);
            if (!o) { throw new Error("cannot scale this curve. Try reducing it first."); }

            // move all points by distance 'd' wrt the origin 'o'
            const points = this._points;
            const np = new Array<Math.Vector2D>(order + 1); // ALLOCATION

            // move end points by fixed distance along normal.
            const p0 = np[0] = Math.Vector2D.clone(points[0]); // ALLOCATION
            p0.x += r0 * v0.n.x;
            p0.y += r0 * v0.n.y;
            const p1 = np[order] = Math.Vector2D.clone(points[order]); // ALLOCATION
            p1.x += r1 * v1.n.x;
            p1.y += r1 * v1.n.y;

            if (!Is.func(d))
            {
                // move control points to lie on the intersection of the offset
                // derivative vector, and the origin-through-control vector
                const d0 = this.derivative(0); // ALLOCATION
                const p0d0 = Math.Vector2D.add(p0, d0); // ALLOCATION
                np[1] = Utils.lli4(p0, p0d0, o, points[1]);
                if (order > 2)
                {
                    const d1 = this.derivative(1); // ALLOCATION
                    const p1d1 = Math.Vector2D.add(p1, d1); // ALLOCATION
                    np[2] = Utils.lli4(p1, p1d1, o, points[2]);
                }
                return new Curve(np);
            }

            // move control points by "however much necessary to
            // ensure the correct tangent to endpoint".
            for (let t = 0; t < 2; ++t)
            {
                if (order === 2 && t > 0) { break; }
                const p = points[t + 1];
                const ov = Math.Vector2D.subtract(p, o); // ALLOCATION
                let rc = Is.func(d) ? d((t + 1) / order) : d;
                if (Is.func(d) && !clockwise) { rc = -rc; }
                Math.Vector2D.normalise(ov);
                np[t + 1] = Math.Vector2D.addMultiply(p, ov, rc); // ALLOCATION
            }
            return new Curve(np);
        }

        // public outline(d1, d2, d3, d4)
        // {
        //     d2 = typeof d2 === "undefined" ? d1 : d2;
        //     var reduced = this.reduce(),
        //         len = reduced.length,
        //         fcurves = [],
        //         bcurves = [],
        //         p,
        //         alen = 0,
        //         tlen = this.length();

        //     var graduated = typeof d3 !== "undefined" && typeof d4 !== "undefined";

        //     function linearDistanceFunction(s, e, tlen, alen, slen)
        //     {
        //         return function (v)
        //         {
        //             var f1 = alen / tlen,
        //                 f2 = (alen + slen) / tlen,
        //                 d = e - s;
        //             return utils.map(v, 0, 1, s + f1 * d, s + f2 * d);
        //         };
        //     }

        //     // form curve oulines
        //     reduced.forEach(function (segment)
        //     {
        //         slen = segment.length();
        //         if (graduated)
        //         {
        //             fcurves.push(
        //                 segment.scale(linearDistanceFunction(d1, d3, tlen, alen, slen))
        //             );
        //             bcurves.push(
        //                 segment.scale(linearDistanceFunction(-d2, -d4, tlen, alen, slen))
        //             );
        //         } else
        //         {
        //             fcurves.push(segment.scale(d1));
        //             bcurves.push(segment.scale(-d2));
        //         }
        //         alen += slen;
        //     });

        //     // reverse the "return" outline
        //     bcurves = bcurves
        //         .map(function (s)
        //         {
        //             p = s.points;
        //             if (p[3])
        //             {
        //                 s.points = [p[3], p[2], p[1], p[0]];
        //             } else
        //             {
        //                 s.points = [p[2], p[1], p[0]];
        //             }
        //             return s;
        //         })
        //         .reverse();

        //     // form the endcaps as lines
        //     var fs = fcurves[0].points[0],
        //         fe = fcurves[len - 1].points[fcurves[len - 1].points.length - 1],
        //         bs = bcurves[len - 1].points[bcurves[len - 1].points.length - 1],
        //         be = bcurves[0].points[0],
        //         ls = utils.makeline(bs, fs),
        //         le = utils.makeline(fe, be),
        //         segments = [ls]
        //             .concat(fcurves)
        //             .concat([le])
        //             .concat(bcurves),
        //         slen = segments.length;

        //     return new PolyBezier(segments);
        // }

        // public outlineshapes(d1, d2, curveIntersectionThreshold)
        // {
        //     d2 = d2 || d1;
        //     var outline = this.outline(d1, d2).curves;
        //     var shapes = [];
        //     for (var i = 1, len = outline.length; i < len / 2; i++)
        //     {
        //         var shape = utils.makeshape(
        //             outline[i],
        //             outline[len - i],
        //             curveIntersectionThreshold
        //         );
        //         shape.startcap.virtual = i > 1;
        //         shape.endcap.virtual = i < len / 2 - 1;
        //         shapes.push(shape);
        //     }
        //     return shapes;
        // }

        /**
         * Finds all values of t where this curve intersects the specified line.
         * @param line 
         * @param out 
         */
        public intersects(line: Math.ReadonlyLine2D, out?: number[]): number[];

        /**
         * Finds all pairs of t where this curve intersects the specified curve.
         * @param line 
         * @param out 
         */
        public intersects(curve: Curve, curveIntersectionThreshold?: number, out?: [number, number][]): [number, number][];

        /**
         * Finds all pairs of t where this curve intersects itself.
         * @param line 
         * @param out 
         */
        public intersects(curveIntersectionThreshold: number, out?: [number, number][]): [number, number][];

        public intersects(p1: Math.ReadonlyLine2D | Curve | number, p2?: number | number[] | [number, number][], p3?: [number, number][]): number[] | [number, number][]
        {
            if (Is.line2D(p1))
            {
                return this.lineIntersects(p1, p2 as number[]);
            }
            else if (p1 instanceof Curve)
            {
                return Curve.curveIntersects(this.reduce(), p1.reduce(), (p2 as number) || 0.5, p3);
            }
            else
            {
                return this.selfIntersects(p1, p2 as [number, number][]);
            }
        }

        /**
         * Approximates this curve as a sequence of circular arcs.
         * @param errorThreshold 
         * @param out 
         */
        public arcs(errorThreshold: number = 0.5, out?: Curve.Arc[]): Curve.Arc[]
        {
            out = out || [];
            out.length = 0;

            let t_s = 0,
                t_e = 1,
                safety: number;
            // we do a binary search to find the "good `t` closest to no-longer-good"
            do
            {
                safety = 0;

                // step 1: start with the maximum possible arc
                t_e = 1;

                // points:
                let np1 = this.compute(t_s), // ALLOCATION
                    np2: Math.Vector2D,
                    np3: Math.Vector2D,
                    arc: Curve.Arc,
                    prev_arc: Curve.Arc;

                // booleans:
                let curr_good = false,
                    prev_good = false,
                    done: boolean;

                // numbers:
                let t_m = t_e,
                    prev_e = 1,
                    step = 0;

                // step 2: find the best possible arc
                do
                {
                    prev_good = curr_good;
                    prev_arc = arc;
                    t_m = (t_s + t_e) / 2;
                    step++;

                    np2 = this.compute(t_m); // ALLOCATION
                    np3 = this.compute(t_e); // ALLOCATION

                    arc = Utils.getCCenter(np1, np2, np3) as Curve.Arc; // ALLOCATION
                    arc.interval = [ t_s, t_e ];

                    const error = this._error(arc, np1, t_s, t_e);
                    curr_good = error <= errorThreshold;

                    done = prev_good && !curr_good;
                    if (!done) { prev_e = t_e; }

                    // this arc is fine: we can move 'e' up to see if we can find a wider arc
                    if (curr_good)
                    {
                        // if e is already at max, then we're done for this arc.
                        if (t_e >= 1)
                        {
                            // make sure we cap at t=1
                            arc.interval[1] = prev_e = 1;
                            prev_arc = arc;
                            // if we capped the arc segment to t=1 we also need to make sure that
                            // the arc's end angle is correct with respect to the bezier end point.
                            if (t_e > 1)
                            {
                                const d = {
                                    x: arc.x + arc.r * cos(arc.e),
                                    y: arc.y + arc.r * sin(arc.e)
                                }; // ALLOCATION
                                arc.e += Utils.angle(arc, d, this.compute(1)); // ALLOCATION
                            }
                            break;
                        }
                        // if not, move it up by half the iteration distance
                        t_e = t_e + (t_e - t_s) / 2;
                    } else
                    {
                        // this is a bad arc: we need to move 'e' down to find a good arc
                        t_e = t_m;
                    }
                } while (!done && safety++ < 100);

                if (safety >= 100)
                {
                    break;
                }

                // console.log("L835: [F] arc found", t_s, prev_e, prev_arc.x, prev_arc.y, prev_arc.s, prev_arc.e);

                prev_arc = prev_arc ? prev_arc : arc;
                out.push(prev_arc);
                t_s = prev_e;
            } while (t_e < 1);
            return out;
        }

        protected lineIntersects(line: Math.ReadonlyLine2D, out?: number[]): number[]
        {
            const mx = Math.min(line.x, line.x + line.dx),
                my = Math.min(line.y, line.y + line.dy),
                MX = Math.max(line.x, line.x + line.dx),
                MY = Math.max(line.y, line.y + line.dy);
            
            out = out || [];
            Utils.roots(this._points, line, out);
            RS.ArrayUtils.filterInPlace(out, (t) =>
            {
                const p = this.compute(t);
                return Utils.between(p.x, mx, MX) && Utils.between(p.y, my, MY);
            });
            return out;
        }

        protected selfIntersects(curveIntersectionThreshold: number, out?: [number, number][]): [number, number][]
        {
            out = out || [];
            const reduced = this.reduce();
            // "simple" curves cannot intersect with their direct
            // neighbour, so for each segment X we check whether
            // it intersects [0:x-2][x+2:last].
            for (let i = 0, l = reduced.length - 2; i < l; ++i)
            {
                const left = reduced.slice(i, i + 1); // ALLOCATION
                const right = reduced.slice(i + 2); // ALLOCATION
                const result = Curve.curveIntersects(left, right, curveIntersectionThreshold); // ALLOCATION
                RS.ArrayUtils.copy(result, out);
            }
            return out;
        }

        protected update(): void
        {
            // invalidate any precomputed LUT
            this._lut.length = 0;
            Utils.derive(this._points, this._dpoints);
            this.computeDirection();
        }

        protected computeDirection(): void
        {
            this._clockwise = Utils.angle(this._points[0], this._points[this._order], this._points[1]) > 0;
        }

        protected getLUT(steps: number = 100): Math.Vector2D[]
        {
            if (this._lut.length === steps) { return this._lut; }
            this._lut.length = steps;
            // We want a range from 0 to 1 inclusive, so
            // we decrement and then use <= rather than <:
            steps--;
            for (let t = 0; t <= steps; ++t)
            {
                this._lut[t] = this.compute(t / steps);
            }
            return this._lut;
        }

        protected _error(pc: Math.ReadonlyVector2D, np1: Math.ReadonlyVector2D, s: number, e: number): number
        {
            const q = (e - s) / 4,
                c1 = this.compute(s + q),
                c2 = this.compute(e - q),
                ref = Math.Vector2D.distanceBetween(pc, np1),
                d1 = Math.Vector2D.distanceBetween(pc, c1),
                d2 = Math.Vector2D.distanceBetween(pc, c2);
            return abs(d1 - ref) + abs(d2 - ref);
        }
    }

    export namespace Curve
    {
        export interface SplitResult
        {
            left: Curve;
            right: Curve;
            span: Math.Vector2D[];
        }

        export interface ExtremaResult
        {
            x: number[];
            y: number[];
            values: number[];
        }

        export interface Arc extends Utils.CCenterResult
        {
            interval: [ number, number ];
        }
    }
}