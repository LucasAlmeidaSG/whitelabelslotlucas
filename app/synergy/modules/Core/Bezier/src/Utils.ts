/** Bezier utility functions. */
namespace RS.Bezier.Utils
{
    import Math = RS.Math;
    const { abs, cos, sin, acos, atan2, sqrt, pow } = Math;

    const pi = Math.PI;
    const tau = 2.0 * pi;
    const quart = 0.5 * pi;
    export const epsilon = 0.000001;
    const nMax = 9007199254740991;
    const nMin = -9007199254740991;

    /** cube root function yielding real roots */
    function crt(v: number): number
    {
        return v < 0 ? -pow(-v, 1 / 3) : pow(v, 1 / 3);
    }

    /** Legendre-Gauss abscissae with n=24 (x_i values, defined at i=n as the roots of the nth order Legendre polynomial Pn(x)) */
    export const Tvalues = [
        -0.0640568928626056260850430826247450385909,
        0.0640568928626056260850430826247450385909,
        -0.1911188674736163091586398207570696318404,
        0.1911188674736163091586398207570696318404,
        -0.3150426796961633743867932913198102407864,
        0.3150426796961633743867932913198102407864,
        -0.4337935076260451384870842319133497124524,
        0.4337935076260451384870842319133497124524,
        -0.5454214713888395356583756172183723700107,
        0.5454214713888395356583756172183723700107,
        -0.6480936519369755692524957869107476266696,
        0.6480936519369755692524957869107476266696,
        -0.7401241915785543642438281030999784255232,
        0.7401241915785543642438281030999784255232,
        -0.8200019859739029219539498726697452080761,
        0.8200019859739029219539498726697452080761,
        -0.8864155270044010342131543419821967550873,
        0.8864155270044010342131543419821967550873,
        -0.9382745520027327585236490017087214496548,
        0.9382745520027327585236490017087214496548,
        -0.9747285559713094981983919930081690617411,
        0.9747285559713094981983919930081690617411,
        -0.9951872199970213601799974097007368118745,
        0.9951872199970213601799974097007368118745
    ];

    /** Legendre-Gauss weights with n=24 (w_i values, defined by a function linked to in the Bezier primer article) */
    export const Cvalues = [
        0.1279381953467521569740561652246953718517,
        0.1279381953467521569740561652246953718517,
        0.1258374563468282961213753825111836887264,
        0.1258374563468282961213753825111836887264,
        0.121670472927803391204463153476262425607,
        0.121670472927803391204463153476262425607,
        0.1155056680537256013533444839067835598622,
        0.1155056680537256013533444839067835598622,
        0.1074442701159656347825773424466062227946,
        0.1074442701159656347825773424466062227946,
        0.0976186521041138882698806644642471544279,
        0.0976186521041138882698806644642471544279,
        0.086190161531953275917185202983742667185,
        0.086190161531953275917185202983742667185,
        0.0733464814110803057340336152531165181193,
        0.0733464814110803057340336152531165181193,
        0.0592985849154367807463677585001085845412,
        0.0592985849154367807463677585001085845412,
        0.0442774388174198061686027482113382288593,
        0.0442774388174198061686027482113382288593,
        0.0285313886289336631813078159518782864491,
        0.0285313886289336631813078159518782864491,
        0.0123412297999871995468056670700372915759,
        0.0123412297999871995468056670700372915759
    ];

    const tmpVec1 = Math.Vector2D();
    const tmpOrder3to4: Math.Vector2D[] = [undefined, undefined, undefined, Math.Vector2D.zero]
    const tmpVecArr1: Math.Vector2D[] = [], tmpVecArr2: Math.Vector2D[] = [], tmpVec2DArr1: Math.Vector2D[][] = [];
    const defaultLine: Math.ReadonlyLine2D = Math.Line2D(0, 0, 1, 0);
    const tmpLine = Math.Line2D();

    function copyVecArray(src: ReadonlyArray<Math.ReadonlyVector2D>, dst: Array<Math.Vector2D>): void
    {
        resizeVecArray(dst, src.length);
        for (let i = 0, l = src.length; i < l; ++i)
        {
            Math.Vector2D.copy(src[i], dst[i]);
        }
    }

    function resizeVecArray(arr: Array<Math.Vector2D>, newLength: number): void
    {
        const oldLength = arr.length;
        if (newLength > oldLength)
        {
            // Grow
            arr.length = newLength;
            for (let i = oldLength; i < newLength; ++i)
            {
                arr[i] = Math.Vector2D();
            }
        }
        else if (newLength < oldLength)
        {
            // Shrink
            for (let i = oldLength - 1; i >= newLength; --i)
            {
                Math.Vector2D.free(arr[i]);
            }
            arr.length = newLength;
        }
    }

    function setInVecArray(arr: Array<Math.Vector2D>, index: number, vec: Math.ReadonlyVector2D): void
    {
        if (index >= arr.length) { resizeVecArray(arr, index + 1); }
        Math.Vector2D.copy(vec, arr[index]);
    }

    export type DerivativeFunc = (t: number, out: Math.Vector2D) => void;

    /**
     * Finds the size of a vector based derivative.
     * @param t 
     * @param derivativeFn 
     */
    export function arcfn(t: number, derivativeFn: DerivativeFunc): number
    {
        derivativeFn(t, tmpVec1);
        return Math.Vector2D.size(tmpVec1);
    }

    /**
     * Samples a point on a n-order curve at t where n>=1 and t:(0-1)
     * @param t 
     * @param p 
     * @param out 
     */
    export function compute(t: number, p: ReadonlyArray<Math.Vector2D>, out?: Math.Vector2D): Math.Vector2D
    {
        out = out || Math.Vector2D();

        // shortcuts
        const order = p.length - 1;
        if (t === 0 || order === 0)
        {
            Math.Vector2D.copy(p[0], out);
            return out;
        }
        if (t === 1)
        {
            Math.Vector2D.copy(p[order], out);
            return out;
        }
        if (order === 1)
        {
            return Math.Vector2D.linearInterpolate(p[0], p[1], t, out);
        }

        // quadratic/cubic curve?
        if (order < 4)
        {
            const mt = 1 - t;
            const mt2 = mt * mt;
            const t2 = t * t;
            let a: number, b: number, c: number, d: number = 0;
            if (order === 2)
            {
                tmpOrder3to4[0] = p[0];
                tmpOrder3to4[1] = p[1];
                tmpOrder3to4[2] = p[2];
                p = tmpOrder3to4;
                a = mt2;
                b = mt * t * 2;
                c = t2;
            }
            else if (order === 3)
            {
                a = mt2 * mt;
                b = mt2 * t * 3;
                c = mt * t2 * 3;
                d = t * t2;
            }
            out.x = a * p[0].x + b * p[1].x + c * p[2].x + d * p[3].x;
            out.y = a * p[0].y + b * p[1].y + c * p[2].y + d * p[3].y;
            return out;
        }

        // higher order curves: use de Casteljau's computation
        const dCpts = tmpVecArr1;
        copyVecArray(p, dCpts);
        while (dCpts.length > 1)
        {
            for (let i = 0, l = dCpts.length - 1; i < l; ++i)
            {
                Math.Vector2D.linearInterpolate(dCpts[i], dCpts[i + 1], t, tmpVec1);
                setInVecArray(dCpts, i, tmpVec1);
            }
            resizeVecArray(dCpts, dCpts.length - 1);
        }
        Math.Vector2D.copy(dCpts[0], out);
        return out;
    }

    /**
     * Returns all orders of derivation of the specified n-order curve where n>=1
     * @param points 
     * @param out 
     */
    export function derive(points: ReadonlyArray<Math.ReadonlyVector2D>, out?: Math.Vector2D[][]): Math.Vector2D[][]
    {
        out = out || [];
        let ptr = 0;
        for (let p = points, d = p.length, c = d - 1; d > 1; d-- , c--)
        {
            const list = out[ptr] || (out[ptr] = []);
            resizeVecArray(list, c);
            for (let j = 0; j < c; ++j)
            {
                tmpVec1.x = c * (p[j + 1].x - p[j].x);
                tmpVec1.y = c * (p[j + 1].y - p[j].y);
                setInVecArray(list, j, tmpVec1);
            }
            p = list;
            ++ptr;
        }
        out.length = ptr;
        return out;
    }

    /**
     * Returns if v falls between m and M with a small margin of error.
     * @param v 
     * @param m 
     * @param M 
     */
    export function between(v: number, m: number, M: number): boolean
    {
        return (
            (m <= v && v <= M) ||
            approximately(v, m) ||
            approximately(v, M)
        );
    }

    /**
     * Returns if a and b are equal within a certain threshold.
     * @param a 
     * @param b 
     * @param precision 
     */
    export function approximately(a: number, b: number, precision: number = epsilon)
    {
        return abs(a - b) <= precision;
    }

    /**
     * Finds an approximate length of a derivative.
     * @param derivativeFn 
     */
    export function length(derivativeFn: DerivativeFunc): number
    {
        const z = 0.5, len = Tvalues.length;
        let sum = 0;
        for (let i = 0; i < len; ++i)
        {
            const t = z * Tvalues[i] + z;
            sum += Cvalues[i] * arcfn(t, derivativeFn);
        }
        return z * sum;
    }

    /**
     * Linearly maps v from the range (ds, de) onto (ts, te).
     * @param v 
     * @param ds 
     * @param de 
     * @param ts 
     * @param te 
     */
    export function map(v: number, ds: number, de: number, ts: number, te: number)
    {
        return ts + (te - ts) * (v - ds) / (de - ds);
    }

    /**
     * Finds the angle between two vectors based on an origin vector.
     * @param o 
     * @param v1 
     * @param v2 
     */
    export function angle(o: Math.ReadonlyVector2D, v1: Math.ReadonlyVector2D, v2: Math.ReadonlyVector2D): number
    {
        const dx1 = v1.x - o.x, dy1 = v1.y - o.y;
        const dx2 = v2.x - o.x, dy2 = v2.y - o.y;
        const cross = dx1 * dy2 - dy1 * dx2;
        const dot = dx1 * dx2 + dy1 * dy2;
        return atan2(cross, dot);
    }

    export interface ClosestLUTResult
    {
        /** Distance to the point. */
        mdist: number;
        /** Index of the point. */
        mpos: number | null;
    }

    /**
     * Finds the nearest point in a look-up table to the specified point.
     * @param LUT 
     * @param point 
     */
    export function closest(LUT: ReadonlyArray<Math.ReadonlyVector2D>, point: Math.ReadonlyVector2D, out?: ClosestLUTResult): ClosestLUTResult
    {
        out = out || { mdist: Infinity, mpos: null };
        for (let i = 0, l = LUT.length; i < l; ++i)
        {
            const pt = LUT[i];
            const d = Math.Vector2D.distanceBetween(pt, point);
            if (d < out.mdist)
            {
                out.mdist = d;
                out.mpos = i;
            }
        }
        return out;
    }

    /**
     * see ratio(t) note on http://pomax.github.io/bezierinfo/#abc
     * @param t 
     * @param n 
     */
    export function abcRatio(t: number, n: 2 | 3): number
    {
        if (t === 0 || t === 1) { return t; }
        const bottom = pow(t, n) + pow(1 - t, n);
        const top = bottom - 1;
        return abs(top / bottom);
    }

    /**
     * see u(t) note on http://pomax.github.io/bezierinfo/#abc
     * @param t 
     * @param n 
     */
    export function projectionRatio(t: number, n: 2 | 3): number
    {
        if (t === 0 || t === 1) { return t; }
        const top = pow(1 - t, n);
        const bottom = pow(t, n) + top;
        return top / bottom;
    }

    /**
     * 
     * @param x1 
     * @param y1 
     * @param x2 
     * @param y2 
     * @param x3 
     * @param y3 
     * @param x4 
     * @param y4 
     * @param out
     */
    export function lli8(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, x4: number, y4: number, out?: Math.Vector2D): Math.Vector2D | null
    {
        const nx =
            (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4),
            ny = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4),
            d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (d === 0) { return null; }
        out = out || Math.Vector2D();
        Math.Vector2D.set(out, nx / d, ny / d);
        return out;
    }

    /**
     * 
     * @param p1 
     * @param p2 
     * @param p3 
     * @param p4 
     * @param out 
     */
    export function lli4(p1: Math.ReadonlyVector2D, p2: Math.ReadonlyVector2D, p3: Math.ReadonlyVector2D, p4: Math.ReadonlyVector2D, out?: Math.Vector2D): Math.Vector2D | null
    {
        return lli8(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y, out);
    }

    /**
     * 
     * @param v1 
     * @param v2 
     * @param out
     */
    export function lli(v1: Math.ReadonlyVector2D & { c: Math.ReadonlyVector2D; }, v2: Math.ReadonlyVector2D & { c: Math.ReadonlyVector2D; }, out?: Math.Vector2D): Math.Vector2D | null
    {
        return lli4(v1, v1.c, v2, v2.c, out);
    }

    // export function makeline(p1, p2)
    // {
    //     var Bezier = require("./bezier");
    //     var x1 = p1.x,
    //         y1 = p1.y,
    //         x2 = p2.x,
    //         y2 = p2.y,
    //         dx = (x2 - x1) / 3,
    //         dy = (y2 - y1) / 3;
    //     return new Bezier(
    //         x1,
    //         y1,
    //         x1 + dx,
    //         y1 + dy,
    //         x1 + 2 * dx,
    //         y1 + 2 * dy,
    //         x2,
    //         y2
    //     );
    // }

    // export function findbbox(sections)
    // {
    //     var mx = nMax,
    //         my = nMax,
    //         MX = nMin,
    //         MY = nMin;
    //     sections.forEach(function (s)
    //     {
    //         var bbox = s.bbox();
    //         if (mx > bbox.x.min) mx = bbox.x.min;
    //         if (my > bbox.y.min) my = bbox.y.min;
    //         if (MX < bbox.x.max) MX = bbox.x.max;
    //         if (MY < bbox.y.max) MY = bbox.y.max;
    //     });
    //     return {
    //         x: { min: mx, mid: (mx + MX) / 2, max: MX, size: MX - mx },
    //         y: { min: my, mid: (my + MY) / 2, max: MY, size: MY - my }
    //     };
    // }

    // export function shapeintersections(
    //     s1,
    //     bbox1,
    //     s2,
    //     bbox2,
    //     curveIntersectionThreshold
    // )
    // {
    //     if (!bboxoverlap(bbox1, bbox2)) return [];
    //     var intersections = [];
    //     var a1 = [s1.startcap, s1.forward, s1.back, s1.endcap];
    //     var a2 = [s2.startcap, s2.forward, s2.back, s2.endcap];
    //     a1.forEach(function (l1)
    //     {
    //         if (l1.virtual) return;
    //         a2.forEach(function (l2)
    //         {
    //             if (l2.virtual) return;
    //             var iss = l1.intersects(l2, curveIntersectionThreshold);
    //             if (iss.length > 0)
    //             {
    //                 iss.c1 = l1;
    //                 iss.c2 = l2;
    //                 iss.s1 = s1;
    //                 iss.s2 = s2;
    //                 intersections.push(iss);
    //             }
    //         });
    //     });
    //     return intersections;
    // }

    // export function makeshape(forward, back, curveIntersectionThreshold)
    // {
    //     var bpl = back.points.length;
    //     var fpl = forward.points.length;
    //     var start = utils.makeline(back.points[bpl - 1], forward.points[0]);
    //     var end = utils.makeline(forward.points[fpl - 1], back.points[0]);
    //     var shape = {
    //         startcap: start,
    //         forward: forward,
    //         back: back,
    //         endcap: end,
    //         bbox: utils.findbbox([start, forward, back, end])
    //     };
    //     var self = utils;
    //     shape.intersections = function (s2)
    //     {
    //         return self.shapeintersections(
    //             shape,
    //             shape.bbox,
    //             s2,
    //             s2.bbox,
    //             curveIntersectionThreshold
    //         );
    //     };
    //     return shape;
    // }

    // export function getminmax(curve, d, list)
    // {
    //     if (!list) return { min: 0, max: 0 };
    //     var min = nMax,
    //         max = nMin,
    //         t,
    //         c;
    //     if (list.indexOf(0) === -1)
    //     {
    //         list = [0].concat(list);
    //     }
    //     if (list.indexOf(1) === -1)
    //     {
    //         list.push(1);
    //     }
    //     for (var i = 0, len = list.length; i < len; i++)
    //     {
    //         t = list[i];
    //         c = curve.get(t);
    //         if (c[d] < min)
    //         {
    //             min = c[d];
    //         }
    //         if (c[d] > max)
    //         {
    //             max = c[d];
    //         }
    //     }
    //     return { min: min, mid: (min + max) / 2, max: max, size: max - min };
    // }

    /**
     * 
     * @param points 
     * @param line 
     * @param out 
     */
    export function align(points: ReadonlyArray<Math.ReadonlyVector2D>, line: Math.ReadonlyLine2D, out?: Array<Math.Vector2D>): Array<Math.Vector2D>
    {
        out = out || [];
        resizeVecArray(out, points.length);
        const tx = line.x, ty = line.y;
        const a = -atan2(line.dy, line.dx);
        const c = cos(a), s = sin(a);
        for (let i = 0, l = points.length; i < l; ++i)
        {
            const v = points[i];
            tmpVec1.x = (v.x - tx) * c - (v.y - ty) * s;
            tmpVec1.y = (v.x - tx) * s + (v.y - ty) * c;
            setInVecArray(out, i, tmpVec1);
        }
        return out;
    }

    export function rootsReduce(t: number) { return 0 <= t && t <= 1; }

    /**
     * Finds the roots, if any, of the specified n-order curve where n:(2-3)
     * @param points 
     * @param line 
     * @param out 
     */
    export function roots(points: ReadonlyArray<Math.ReadonlyVector2D>, line: Math.ReadonlyLine2D = defaultLine, out?: Array<number>): Array<number>
    {
        const order = points.length - 1;
        const alignedPoints = align(points, line, tmpVecArr1);
        out = out || [];
        out.length = 0;

        if (order === 2)
        {
            const a = alignedPoints[0].y,
                b = alignedPoints[1].y,
                c = alignedPoints[2].y,
                d = a - 2 * b + c;
            if (d !== 0)
            {
                const m1 = -sqrt(b * b - a * c),
                    m2 = -a + b,
                    v1 = -(m1 + m2) / d,
                    v2 = -(-m1 + m2) / d;
                if (rootsReduce(v1)) { out.push(v1); }
                if (rootsReduce(v2)) { out.push(v1); }
                return out;
            }
            else if (b !== c && d === 0)
            {
                const v1 = (2 * b - c) / (2 * b - 2 * c);
                if (rootsReduce(v1)) { out.push(v1); }
                return out;
            }
            return out;
        }
        else
        {

            // see http://www.trans4mind.com/personal_development/mathematics/polynomials/cubicAlgebra.htm
            const pa = alignedPoints[0].y,
                pb = alignedPoints[1].y,
                pc = alignedPoints[2].y,
                pd = alignedPoints[3].y,
                d = -pa + 3 * pb - 3 * pc + pd;
            let a = 3 * pa - 6 * pb + 3 * pc,
                b = -3 * pa + 3 * pb,
                c = pa;

            if (approximately(d, 0))
            {
                // this is not a cubic curve.
                if (approximately(a, 0))
                {
                    // in fact, this is not a quadratic curve either.
                    if (approximately(b, 0))
                    {
                        // in fact in fact, there are no solutions.
                        return out;
                    }
                    // linear solution:
                    const t = -c / b;
                    if (rootsReduce(t)) { out.push(t); }
                    return out;
                }
                else
                {
                    // quadratic solution:
                    const q2 = sqrt(b * b - 4 * a * c),
                        a2 = 2 * a,
                        t1 = (q2 - b) / a2,
                        t2 = (-b - q2) / a2;
                    if (rootsReduce(t1)) { out.push(t1); }
                    if (rootsReduce(t2)) { out.push(t2); }
                    return out;
                }
            }
            else
            {

                // at this point, we know we need a cubic solution:

                a /= d;
                b /= d;
                c /= d;

                const p = (3 * b - a * a) / 3,
                    p3 = p / 3,
                    q = (2 * a * a * a - 9 * a * b + 27 * c) / 27,
                    q2 = q / 2,
                    discriminant = q2 * q2 + p3 * p3 * p3;
                let u1: number,
                    v1: number,
                    x1: number,
                    x2: number,
                    x3: number;
                if (discriminant < 0)
                {
                    const mp3 = -p / 3,
                        mp33 = mp3 * mp3 * mp3,
                        r = sqrt(mp33),
                        t = -q / (2 * r),
                        cosphi = t < -1 ? -1 : t > 1 ? 1 : t,
                        phi = acos(cosphi),
                        crtr = crt(r),
                        t1 = 2 * crtr;
                    x1 = t1 * cos(phi / 3) - a / 3;
                    x2 = t1 * cos((phi + tau) / 3) - a / 3;
                    x3 = t1 * cos((phi + 2 * tau) / 3) - a / 3;
                    if (rootsReduce(x1)) { out.push(x1); }
                    if (rootsReduce(x2)) { out.push(x2); }
                    if (rootsReduce(x3)) { out.push(x3); }
                    return out;
                }
                else if (discriminant === 0)
                {
                    u1 = q2 < 0 ? crt(-q2) : -crt(q2);
                    x1 = 2 * u1 - a / 3;
                    x2 = -u1 - a / 3;
                    if (rootsReduce(x1)) { out.push(x1); }
                    if (rootsReduce(x2)) { out.push(x2); }
                    return out;
                }
                else
                {
                    const sd = sqrt(discriminant);
                    u1 = crt(-q2 + sd);
                    v1 = crt(q2 + sd);
                    x1 = u1 - v1 - a / 3;
                    if (rootsReduce(x1)) { out.push(x1); }
                    return out;
                }
            }
        }
    }

    /**
     * 
     * @param p 
     * @param out
     */
    export function droots(points: ReadonlyArray<number>, out?: Array<number>): Array<number>
    {
        out = out || [];

        // quadratic roots are easy
        if (points.length === 3)
        {
            const a = points[0],
                b = points[1],
                c = points[2],
                d = a - 2 * b + c;
            if (d !== 0)
            {
                const m1 = -sqrt(b * b - a * c),
                    m2 = -a + b,
                    v1 = -(m1 + m2) / d,
                    v2 = -(-m1 + m2) / d;
                if (v1 === v2)
                {
                    out.length = 1;
                    out[0] = v1;
                }
                else
                {
                    out.length = 2;
                    out[0] = v1;
                    out[1] = v2;
                }
                return out;
            }
            else if (b !== c && d === 0)
            {
                out.length = 1;
                out[0] = (2 * b - c) / (2 * (b - c));
                return out;
            }
            out.length = 0;
            return out;
        }

        // linear roots are even easier
        if (points.length === 2)
        {
            const a = points[0],
                b = points[1];
            if (a !== b)
            {
                out.length = 1;
                out[0] = a / (a - b);
                return out;
            }
            out.length = 0;
            return out;
        }

        throw new Error("Invalid order");
    }

    export interface CurvatureResult
    {
        k: number;
        r: number;
    }

    /**
     * Finds the curvature of a n-order curve where n>=1
     * @param t 
     * @param points 
     * @param out 
     */
    export function curvature(t: number, points: ReadonlyArray<Math.ReadonlyVector2D>, out?: CurvatureResult): CurvatureResult
    {
        out = out || { k: 0, r: 0 };

        if (points.length < 3)
        {
            out.r = Infinity;
            out.k = 0.0;
            return out;
        }

        const dpoints = derive(points, tmpVec2DArr1);
        const d1 = dpoints[0];
        const d2 = dpoints[1];

        //
        // We're using the following formula for curvature:
        //
        //              x'y" - y'x"
        //   k(t) = ------------------
        //           (x'² + y'²)^(2/3)
        //
        // from https://en.wikipedia.org/wiki/Radius_of_curvature#Definition
        //

        const d = compute(t, d1);
        const dd = compute(t, d2);
        const num = d.x * dd.y - d.y * dd.x;
        const dnm = pow(d.x * d.x + d.y * d.y, 2 / 3);

        if (num === 0 || dnm === 0)
        {
            out.k = 0;
            out.r = 0;
            return out;
        }

        out.k = num / dnm;
        out.r = dnm / num;
        return out;
    }

    /**
     * 
     * @param points 
     * @param out 
     */
    export function inflections(points: ReadonlyArray<Math.ReadonlyVector2D>, out?: number[]): number[]
    {
        out = out || [];
        out.length = 0;

        if (points.length < 4) { return out; }

        // FIXME: TODO: add in inflection abstraction for quartic+ curves?
        // note: I have no idea what this means -Tom

        Math.Vector2D.copy(points[0], tmpLine);
        tmpLine.dx = points[points.length - 1].x;
        tmpLine.dy = points[points.length - 1].y;
        const p = align(points, tmpLine),
            a = p[2].x * p[1].y,
            b = p[3].x * p[1].y,
            c = p[1].x * p[2].y,
            d = p[3].x * p[2].y,
            v1 = 18 * (-3 * a + 2 * b + 3 * c - d),
            v2 = 18 * (3 * a - b - 3 * c),
            v3 = 18 * (c - a);

        if (approximately(v1, 0))
        {
            if (!approximately(v2, 0))
            {
                const t = -v3 / v2;
                if (rootsReduce(t)) { out.push(t); }
            }
            return out;
        }

        const trm = v2 * v2 - 4 * v1 * v3,
            sq = Math.sqrt(trm),
            d2 = 2 * v1;

        if (approximately(d2, 0)) { return out; }

        const t1 = (sq - v2) / d2;
        const t2 = -(v2 + sq) / d2;
        if (rootsReduce(t1)) { out.push(t1); }
        if (rootsReduce(t2)) { out.push(t2); }
        return out;
    }

    export interface CCenterResult extends Math.Vector2D
    {
        s: number;
        e: number;
        r: number;
    }

    /**
     * 
     * @param p1 
     * @param p2 
     * @param p3 
     * @param out 
     */
    export function getCCenter(p1: Math.ReadonlyVector2D, p2: Math.ReadonlyVector2D, p3: Math.ReadonlyVector2D, out?: CCenterResult): CCenterResult
    {
        out = out || { x: 0, y: 0, s: 0, e: 0, r: 0 };
        const dx1 = p2.x - p1.x,
            dy1 = p2.y - p1.y,
            dx2 = p3.x - p2.x,
            dy2 = p3.y - p2.y;
        const dx1p = dx1 * cos(quart) - dy1 * sin(quart),
            dy1p = dx1 * sin(quart) + dy1 * cos(quart),
            dx2p = dx2 * cos(quart) - dy2 * sin(quart),
            dy2p = dx2 * sin(quart) + dy2 * cos(quart);
        // chord midpoints
        const mx1 = (p1.x + p2.x) / 2,
            my1 = (p1.y + p2.y) / 2,
            mx2 = (p2.x + p3.x) / 2,
            my2 = (p2.y + p3.y) / 2;
        // midpoint offsets
        const mx1n = mx1 + dx1p,
            my1n = my1 + dy1p,
            mx2n = mx2 + dx2p,
            my2n = my2 + dy2p;
        // intersection of these lines:
        const arc = lli8(mx1, my1, mx1n, my1n, mx2, my2, mx2n, my2n, out),
            r = Math.Vector2D.distanceBetween(arc, p1);
        // arc start/end values, over mid point:
        let s = atan2(p1.y - arc.y, p1.x - arc.x),
            m = atan2(p2.y - arc.y, p2.x - arc.x),
            e = atan2(p3.y - arc.y, p3.x - arc.x);
        let _: number;
        // determine arc direction (cw/ccw correction)
        if (s < e)
        {
            // if s<m<e, arc(s, e)
            // if m<s<e, arc(e, s + tau)
            // if s<e<m, arc(e, s + tau)
            if (s > m || m > e)
            {
                s += tau;
            }
            if (s > e)
            {
                _ = e;
                e = s;
                s = _;
            }
        }
        else
        {
            // if e<m<s, arc(e, s)
            // if m<e<s, arc(s, e + tau)
            // if e<s<m, arc(s, e + tau)
            if (e < m && m < s)
            {
                _ = e;
                e = s;
                s = _;
            } else
            {
                e += tau;
            }
        }
        // assign and done.
        out.s = s;
        out.e = e;
        out.r = r;
        return out;
    }
}