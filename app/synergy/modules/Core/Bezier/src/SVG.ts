/** Bezier SVG utility functions. */
namespace RS.Bezier.SVGUtils
{
    /**
     * Normalise an SVG path to absolute coordinates
     * and full commands, rather than relative coordinates
     * and/or shortcut commands.
     */
    export function normalisePath(d: string): string
    {
        // preprocess "d" so that we have spaces between values
        d = d
            .replace(/,/g, " ") // replace commas with spaces
            .replace(/-/g, " - ") // add spacing around minus signs
            .replace(/-\s+/g, "-") // remove spacing to the right of minus signs.
            .replace(/([a-zA-Z])/g, " $1 ");

        // set up the variables used in this function
        const instructions = d.replace(/([a-zA-Z])\s?/g, "|$1").split("|"), instructionLength = instructions.length;

        let sx = 0,
            sy = 0,
            x = 0,
            y = 0,
            cx = 0,
            cy = 0,
            cx2 = 0,
            cy2 = 0,
            normalized: string[] = [];

        // we run through the instruction list starting at 1, not 0,
        // because we split up "|M x y ...." so the first element will
        // always be an empty string. By design.
        for (let i = 1; i < instructionLength; ++i)
        {
            // which instruction is this?
            const instruction = instructions[i];
            const op = instruction.substring(0, 1);
            const lop = op.toLowerCase();

            // what are the arguments? note that we need to convert
            // all strings into numbers, or + will do silly things.
            const args = instruction
                .replace(op, "")
                .trim()
                .split(" ")
                .filter((v) => v !== "")
                .map(parseFloat);
            const alen = args.length;

            switch (lop)
            {
                case "m":
                    // moveto command (plus possible lineto)
                    normalized.push("M ");
                    if (op === "m")
                    {
                        x += args[0];
                        y += args[1];
                    } else
                    {
                        x = args[0];
                        y = args[1];
                    }
                    // records start position, for dealing
                    // with the shape close operator ('Z')
                    sx = x;
                    sy = y;
                    normalized.push(`${x} ${y} `);
                    if (alen > 2)
                    {
                        for (let a = 0; a < alen; a += 2)
                        {
                            if (op === "m")
                            {
                                x += args[a];
                                y += args[a + 1];
                            }
                            else
                            {
                                x = args[a];
                                y = args[a + 1];
                            }
                            normalized.push(`L ${x} ${y} `);
                        }
                    }
                    break;
                case "l":
                    // lineto commands
                    for (let a = 0; a < alen; a += 2)
                    {
                        if (op === "l")
                        {
                            x += args[a];
                            y += args[a + 1];
                        }
                        else
                        {
                            x = args[a];
                            y = args[a + 1];
                        }
                        normalized.push(`L ${x} ${y} `);
                    }
                    break;
                case "h":
                    for (let a = 0; a < alen; a++)
                    {
                        if (op === "h")
                        {
                            x += args[a];
                        }
                        else
                        {
                            x = args[a];
                        }
                        normalized.push(`L ${x} ${y} `);
                    }
                    break;
                case "v":
                    for (let a = 0; a < alen; a++)
                    {
                        if (op === "v")
                        {
                            y += args[a];
                        }
                        else
                        {
                            y = args[a];
                        }
                        normalized.push(`L ${x} ${y} `);
                    }
                    break;
                case "q":
                    // quadratic curveto commands
                    for (let a = 0; a < alen; a += 4)
                    {
                        if (op === "q")
                        {
                            cx = x + args[a];
                            cy = y + args[a + 1];
                            x += args[a + 2];
                            y += args[a + 3];
                        }
                        else
                        {
                            cx = args[a];
                            cy = args[a + 1];
                            x = args[a + 2];
                            y = args[a + 3];
                        }
                        normalized.push(`Q ${cx} ${cy} ${x} ${y} `);
                    }
                    break;
                case "t":
                    for (let a = 0; a < alen; a += 2)
                    {
                        // reflect previous cx/cy over x/y
                        cx = x + (x - cx);
                        cy = y + (y - cy);
                        // then get real end point
                        if (op === "t")
                        {
                            x += args[a];
                            y += args[a + 1];
                        }
                        else
                        {
                            x = args[a];
                            y = args[a + 1];
                        }
                        normalized.push(`Q ${cx} ${cy} ${x} ${y} `);
                    }
                    break;
                case "c":
                    // cubic curveto commands
                    for (let a = 0; a < alen; a += 6)
                    {
                        if (op === "c")
                        {
                            cx = x + args[a];
                            cy = y + args[a + 1];
                            cx2 = x + args[a + 2];
                            cy2 = y + args[a + 3];
                            x += args[a + 4];
                            y += args[a + 5];
                        }
                        else
                        {
                            cx = args[a];
                            cy = args[a + 1];
                            cx2 = args[a + 2];
                            cy2 = args[a + 3];
                            x = args[a + 4];
                            y = args[a + 5];
                        }
                        normalized.push(`C ${cx} ${cy} ${cx2} ${cy2} ${x} ${y} `);
                    }
                    break;
                case "s":
                    for (let a = 0; a < alen; a += 4)
                    {
                        // reflect previous cx2/cy2 over x/y
                        cx = x + (x - cx2);
                        cy = y + (y - cy2);
                        // then get real control and end point
                        if (op === "s")
                        {
                            cx2 = x + args[a];
                            cy2 = y + args[a + 1];
                            x += args[a + 2];
                            y += args[a + 3];
                        }
                        else
                        {
                            cx2 = args[a];
                            cy2 = args[a + 1];
                            x = args[a + 2];
                            y = args[a + 3];
                        }
                        normalized.push(`C ${cx} ${cy} ${cx2} ${cy2} ${x} ${y} `);
                    }
                    break;
                case "z":
                    normalized.push("Z ");
                    // not unimportant: path closing changes the current x/y coordinate
                    x = sx;
                    y = sy;
                    break;
            }
        }
        return normalized.join("").trim();
    }
}