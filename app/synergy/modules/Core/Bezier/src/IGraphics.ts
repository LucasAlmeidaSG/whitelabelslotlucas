namespace RS.Bezier
{
    export interface IGraphics
    {
        lineStyle(width?: number, color?: Util.Color, alpha?: number): void;
        moveTo(x: number, y: number): void;
        lineTo(x: number, y: number): void;
    }
}
