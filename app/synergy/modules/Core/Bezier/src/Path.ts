/// <reference path="Curve.ts" />

namespace RS.Bezier
{
    const tmpAABB1 = Math.AABB2D();
    const tmpVec1 = Math.Vector2D(), tmpVec2 = Math.Vector2D();

    const svgMatcher = /[MLCQZ]/;
    const svgArgs = { "C": 6, "Q": 4, "L": 2, "M": 2 };

    /**
     * Encapsulates a sequence of contiguous bezier curves.
     * Note that contiguous-ness is assumed and not checked or enforced.
     */
    export class Path
    {
        protected _curves: Curve[] = [];
        protected _length: number;
        protected _bbox = Math.AABB2D();
        protected _looping = false;
        protected _transform = Math.Matrix3.clone(Math.Matrix3.identity);
        protected _transformInv = Math.Matrix3.clone(Math.Matrix3.identity);

        /**
         * Gets all curves in this path.
         */
        public get curves(): ReadonlyArray<Curve> { return this._curves; }

        /**
         * Gets the length of this path.
         */
        public get length() { return this._length; }

        /**
         * Gets or sets if this is a looping path (e.g. endpoint last curve matches startpoint of first curve).
         */
        public get looping() { return this._looping; }
        public set looping(value) { this._looping = value; }

        /**
         * Gets or sets the transform for this path.
         */
        public get transform(): Math.ReadonlyMatrix3 { return this._transform; }
        public set transform(value)
        {
            Math.Matrix3.copy(value, this._transform);
            Math.Matrix3.inverse(value, this._transformInv);
        }

        public constructor();

        public constructor(curves: Curve[], looping?: boolean);

        public constructor(curves?: Curve[], looping?: boolean)
        {
            if (curves) { RS.ArrayUtils.copy(curves, this._curves); }
            this._looping = looping || false;
            this.update();
        }

        // #region Statics

        /**
         * Creates a new path from the specified set of points using cubic interpolation.
         * @param points
         */
        public static fromPointsCubic(points: Math.ReadonlyVector2D[], t: number = 0.5, looping: boolean = false): Path
        {
            // Sanity check
            if (points.length === 0) { throw new Error("Can't create bezier path from 0 points"); }
            if (points.length === 1) { throw new Error("Can't create bezier path from 1 point"); }

            const segCount = looping ? points.length : points.length - 1;

            // Extrapolate extra points on the ends
            if (!looping)
            {
                points.unshift(Math.Vector2D.linearInterpolate(points[0], points[1], -1.0));
                points.push(Math.Vector2D.linearInterpolate(points[segCount], points[segCount + 1], 2.0));
            }

            const tmp1 = Math.Vector2D();
            const tmp2 = Math.Vector2D();

            // Iterate each segment
            const segs = new Array<Curve>(segCount);
            for (let i = 0; i < segCount; ++i)
            {
                const p0i = looping ? (i === 0 ? points.length - 1 : i - 1) : i;
                const p0 = points[p0i];
                const p1 = points[(p0i + 1) % points.length];
                const p2 = points[(p0i + 2) % points.length];
                const p3 = points[(p0i + 3) % points.length];

                const p3p2 = Math.Vector2D.subtract(p2, p3, tmp1);
                const p2p1 = Math.Vector2D.subtract(p1, p2, tmp2);

                const cp1 = Math.Vector2D.linearInterpolate(p3p2, p2p1, 0.5);
                cp1.x = p2.x + t * cp1.x;
                cp1.y = p2.y + t * cp1.y;

                const p0p1 = Math.Vector2D.subtract(p1, p0, tmp1);
                const p1p2 = Math.Vector2D.subtract(p2, p1, tmp2);

                const cp0 = Math.Vector2D.linearInterpolate(p0p1, p1p2, 0.5);
                cp0.x = p1.x + t * cp0.x;
                cp0.y = p1.y + t * cp0.y;

                segs[i] = new Curve([p1, cp0, cp1, p2]);
            }

            // Create path
            return new Path(segs);
        }

        /**
         * Creates a new path from the specified SVG string.
         * @param svg 
         */
        public static fromSVG(svg: string): Path
        {
            svg = SVGUtils.normalisePath(svg);
            const terms = svg.split(" ");
            const M = Math.Vector2D();
            const curves: Curve[] = [];
            for (let i = 0, l = terms.length; i < l; ++i)
            {
                const term = terms[i];
                if (svgMatcher.test(term))
                {
                    const values = terms.slice(i + 1, i + 1 + svgArgs[term]).map(parseFloat);

                    if (term === "Z") { continue; }
                    if (term === "M")
                    {
                        M.x = values[0];
                        M.y = values[1];
                        continue;
                    }
                    
                    const points = this.vecArrFromNumArr(values);
                    const curve = new Curve([ M, ...points ]);
                    Math.Vector2D.copy(points[points.length - 1], M);
                    curves.push(curve);
                }
            }
            return new Path(curves);
        }

        protected static vecArrFromNumArr(arr: ReadonlyArray<number>): Math.Vector2D[]
        {
            const out = new Array<Math.Vector2D>(arr.length >> 1);
            for (let i = 0, l = arr.length; i < l; i += 2)
            {
                out[i >> 1] = Math.Vector2D(arr[i], arr[i + 1]);
            }
            return out;
        }

        // #endregion

        /**
         * Adds a curve to this path.
         * @param curve 
         */
        public addCurve(curve: Curve): void
        {
            this._curves.push(curve);
            this._length += curve.length;
            if (this._curves.length === 1)
            {
                curve.getBBox(this._bbox);
            }
            else
            {
                const aabb = curve.getBBox(tmpAABB1);
                tmpVec1.x = aabb.minX;
                tmpVec1.y = aabb.minY;
                Math.AABB2D.expand(this._bbox, tmpVec1);
                tmpVec1.x = aabb.maxX;
                tmpVec1.y = aabb.maxY;
                Math.AABB2D.expand(this._bbox, tmpVec1);
            }
        }

        /**
         * Removes all curves from this path.
         */
        public removeAllCurves(): void
        {
            this._curves.length = 0;
            this._length = 0;
            this._bbox.minX = this._bbox.minY = this._bbox.maxX = this._bbox.maxY = 0.0;
        }

        /**
         * Samples a point t along the path.
         * @param t (0-n) where n is the number of curves in the path
         * @param out 
         */
        public compute(t: number, out?: Path.SamplePoint): Path.SamplePoint
        {
            out = out || { x: 0, y: 0, heading: 0, localT: 0, pathT: 0, curve: null };
            out.pathT = this._looping ? Math.wrap(t, 0, this._curves.length) : Math.clamp(t, 0, this._curves.length);
            // Special case: right at the end of a non-looping path
            if (out.pathT >= this._curves.length && !this._looping)
            {
                out.pathT = this._curves.length;
                out.curve = this._curves[out.pathT - 1];
                out.localT = 1.0;
            }
            else
            {
                out.curve = this._curves[out.pathT | 0];
                out.localT = t % 1;
            }
            out.curve.compute(out.localT, tmpVec1);
            Math.Matrix3.transform(this._transform, tmpVec1, out);
            out.curve.normal(out.localT, tmpVec1);
            Math.Matrix3.transformNormal(this._transform, tmpVec1, tmpVec2);
            out.heading = Math.atan2(-tmpVec2.y, tmpVec2.x) + Math.PI;
            return out;
        }

        /**
         * Samples a point at the given distance along this path.
         * @param distance
         * @param out
         */
        public sample(distance: number, out?: Path.SamplePoint): Path.SamplePoint
        {
            // Find the segment
            distance = Math.mod(distance, this._length + 0.0000001); // small number added to prevent wrapping when distance equals curve length
            let i: number, curDist = 0, curve: Curve;
            for (i = 0; i < this._curves.length; i++)
            {
                curve = this._curves[i];
                const nextDist = curDist + curve.length;
                if (nextDist >= distance) { break; }
                curDist = nextDist;
            }

            // Sample it
            const delta = (distance - curDist) / curve.length;
            const t = i + delta;
            out = this.compute(t, out);
            out.weight = RS.Math.linearInterpolate(curve.startWeight, curve.endWeight, delta);
            return out;
        }

        /**
         * Finds the curvature at t along the path.
         * @param t (0-n) where n is the number of curves in the path
         * @param out 
         */
        public curvature(t: number, out?: Utils.CurvatureResult): Utils.CurvatureResult
        {
            let pathT: number, localT: number;
            let curve: Curve;
            pathT = this._looping ? Math.wrap(t, 0, this._curves.length) : Math.clamp(t, 0, this._curves.length);
            // Special case: right at the end of a non-looping path
            if (pathT >= this._curves.length && !this._looping)
            {
                pathT = this._curves.length;
                curve = this._curves[pathT - 1];
                localT = 1.0;
            }
            else
            {
                curve = this._curves[pathT | 0];
                localT = t % 1;
            }
            return curve.curvature(localT, out);
        }

        /**
         * Projects a position onto the path.
         * Will return a value of t along the path.
         * This can be used to sample the path, to determine how far the projection was.
         * Returns null upon failure (no segments in path).
         * @param pos
         * @param out
         */
        public project(pos: Math.ReadonlyVector2D, out?: Path.SamplePoint): Path.SamplePoint | null
        {
            const ppos = Math.Matrix3.transform(this._transformInv, pos, tmpVec1);
            let bestDist2: number | null = null;
            let bestPos: Math.Vector2D | null = null;
            let bestCurve: Curve | null = null;
            let bestT: number | null = null;
            for (let i = 0, l = this._curves.length; i < l; ++i)
            {
                const curve = this._curves[i];
                const projPos = curve.project(ppos, tmpVec2);
                const dist2 = (projPos.x - ppos.x) * (projPos.x - ppos.x) + (projPos.y - ppos.y) * (projPos.y - ppos.y);
                if (bestDist2 == null || dist2 < bestDist2)
                {
                    bestDist2 = dist2;
                    bestPos = projPos;
                    bestCurve = curve;
                    bestT = i + projPos.t;
                }
                if (bestDist2 === 0) { break; }
            }
            if (bestPos == null || bestCurve == null) { return null; }
            return this.compute(bestT, out);
        }

        public getWeight(index: number): number
        {
            if (index == null || index > this._curves.length || index < 0)
            {
                return null;
            }
            if (index === this._curves.length)
            {
                return this._curves[index - 1].endWeight;
            }
            return this._curves[index].startWeight;
        }

        public setWeight(index: number, weight: number): void
        {
            if (index == null || index > this._curves.length || index < 0)
            {
                return;
            }
            if (index > 0)
            {
                this._curves[index - 1].endWeight = weight;
            }
            if (index < this._curves.length)
            {
                this._curves[index].startWeight = weight;
            }
        }

        /**
         * Draws this path into the specified graphics for debugging purposes.
         * @param graphics 
         * @param distPerPiece 
         */
        public debugDraw(graphics: IGraphics, distPerPiece: number = (this.length / this._curves.length) * 0.1): void
        {
            const initialPt = this.sample(0);
            graphics.lineStyle(2, Util.Colors.red);
            graphics.moveTo(initialPt.x, initialPt.y);
            const pieceCount = Math.round(this._length / distPerPiece);
            distPerPiece = this._length / pieceCount;
            for (let i = 1; i <= pieceCount; i++)
            {
                const pt = this.sample(i * distPerPiece * 0.99);
                graphics.lineTo(pt.x, pt.y);
            }

            graphics.lineStyle(1, Util.Colors.yellow);
            for (const curve of this._curves)
            {
                const p0 = curve.points[0];
                const cp0 = curve.points[1];
                if (cp0.x != null && cp0.y != null)
                {
                    graphics.moveTo(cp0.x, cp0.y);
                    graphics.lineTo(p0.x, p0.y);
                }

                const p1 = curve.points[3];
                const cp1 = curve.points[2];
                if (cp1.x != null && cp1.y != null)
                {
                    graphics.moveTo(cp1.x, cp1.y);
                    graphics.lineTo(p1.x, p1.y);
                }
            }

            graphics.lineStyle(2, Util.Colors.green);
            for (const curve of this._curves)
            {
                const cp0 = curve.points[1];
                if (cp0.x != null && cp0.y != null)
                {
                    graphics.moveTo(cp0.x - 2, cp0.y - 2);
                    graphics.lineTo(cp0.x + 2, cp0.y - 2);
                    graphics.lineTo(cp0.x + 2, cp0.y + 2);
                    graphics.lineTo(cp0.x - 2, cp0.y + 2);
                    graphics.lineTo(cp0.x - 2, cp0.y - 2);
                }

                const cp1 = curve.points[2];
                if (cp1.x != null && cp1.y != null)
                {
                    graphics.moveTo(cp1.x - 2, cp1.y - 2);
                    graphics.lineTo(cp1.x + 2, cp1.y - 2);
                    graphics.lineTo(cp1.x + 2, cp1.y + 2);
                    graphics.lineTo(cp1.x - 2, cp1.y + 2);
                    graphics.lineTo(cp1.x - 2, cp1.y - 2);
                }
            }
        }

        /**
         * Creates a clone of this path.
         */
        public clone(): Path
        {
            const newPath = new Path(this._curves, this._looping);
            newPath.transform = this._transform;
            return newPath;
        }

        protected update(): void
        {
            this._length = 0;
            let first = true;
            for (const curve of this._curves)
            {
                this._length += curve.length;
                const aabb = curve.getBBox(tmpAABB1);
                if (first)
                {
                    Math.AABB2D.copy(aabb, this._bbox);
                }
                else
                {
                    tmpVec1.x = aabb.minX;
                    tmpVec1.y = aabb.minY;
                    Math.AABB2D.expand(this._bbox, tmpVec1);
                    tmpVec1.x = aabb.maxX;
                    tmpVec1.y = aabb.maxY;
                    Math.AABB2D.expand(this._bbox, tmpVec1);
                }
            }
        }
    }

    export namespace Path
    {
        export interface SamplePoint extends Math.Vector2D
        {
            heading: number;
            curve: Curve;
            localT: number;
            pathT: number;
            weight?: number;
        }
    }
}