namespace RS.AssetPipeline.Bezier
{
    export interface PathDefinition extends AssetDefinition
    {
        looping?: boolean;
        pixelsPerInch?: number;
    }

    /**
     * Represents a bezier path asset, in the form of an Adobe Illustrator file.
     */
    @AssetType("path")
    @InferFromExtensions([".ai"])
    export class Path extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "json" ]; }

        public get classModule() { return "Core.Asset.SoundPipeline"; }
        
        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitSimpleAssetReference(this, "path", "RS.Asset.GenericReference");
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir 
         * @param assetPath 
         * @param variant 
         * @param dry 
         * @param format 
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: PathDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }
            if (format != "json")
            {
                Log.error(`Unsupported format '${format}'`);
                return;
            }
            if (!dry)
            {
                const ppi = variant.pixelsPerInch || (this._definition as PathDefinition).pixelsPerInch || 300;
                const ppp = ppi / 72;
                const looping = variant.looping != null ? variant.looping : (this._definition as PathDefinition).looping != null ? (this._definition as PathDefinition).looping : false;

                const outputPath = RS.Path.replaceExtension(RS.Path.combine(outputDir, RS.Path.sanitise(RS.Path.baseName(assetPath))), ".json");

                const rawData = await FileSystem.readFile(assetPath);
                const lines = rawData.split(/\r?\n/g);

                const boundingBox = { x: 0, y: 0, w: 0, h: 0 };

                // Locate bounding box
                for (let i = 0; i < lines.length; i++)
                {
                    if (lines[i].indexOf("%%HiResBoundingBox:") === 0)
                    {
                        const bbox = lines[i].slice(("%%HiResBoundingBox:").length + 1).split(" ");
                        if (bbox != null && bbox.length === 4)
                        {
                            boundingBox.x = parseFloat(bbox[0]) * ppp;
                            boundingBox.y = parseFloat(bbox[1]) * ppp;
                            boundingBox.w = parseFloat(bbox[2]) * ppp;
                            boundingBox.h = parseFloat(bbox[3]) * ppp;
                        }
                    }
                }

                const initialPoint = { x: 0, y: 0 };
                const points: { x: number, y: number, cp1x: number, cp1y: number, cp2x: number, cp2y: number }[] = [];

                // Find all lines ending in a single character
                const cmdLines = lines.filter((l) => (/\s[a-zA-Z]$/g).test(l));
                for (let i = 0; i < cmdLines.length; i++)
                {
                    const data = cmdLines[i].split(" ");
                    const cmdChar = data[data.length - 1];
                    switch (cmdChar)
                    {
                        // Move to
                        case "m":
                        {
                            initialPoint.x = parseFloat(data[0]) * ppp;
                            initialPoint.y = parseFloat(data[1]) * ppp;
                            break;
                        }

                        // Curve to
                        case "c":
                        {
                            points.push({
                                cp1x: parseFloat(data[0]) * ppp,
                                cp1y: parseFloat(data[1]) * ppp,
                                cp2x: parseFloat(data[2]) * ppp,
                                cp2y: parseFloat(data[3]) * ppp,
                                x: parseFloat(data[4]) * ppp,
                                y: parseFloat(data[5]) * ppp
                            });
                            break;
                        }

                        case "l":
                        {
                            points.push({
                                cp1x: null,
                                cp1y: null,
                                cp2x: null,
                                cp2y: null,
                                x: parseFloat(data[0]) * ppp,
                                y: parseFloat(data[1]) * ppp
                            });
                            break;
                        }

                        case "v":
                            points.push({
                                cp1x: null,
                                cp1y: null,
                                cp2x: parseFloat(data[0]) * ppp,
                                cp2y: parseFloat(data[1]) * ppp,
                                x: parseFloat(data[2]) * ppp,
                                y: parseFloat(data[3]) * ppp
                            });
                            break;

                        case "y":
                            points.push({
                                cp1x: parseFloat(data[0]) * ppp,
                                cp1y: parseFloat(data[1]) * ppp,
                                cp2x: null,
                                cp2y: null,
                                x: parseFloat(data[2]) * ppp,
                                y: parseFloat(data[3]) * ppp
                            });
                            break;
                    }
                }

                // Output format
                const result = {
                    boundingBox,
                    initialPoint,
                    points,
                    looping: variant.looping || false
                };

                // Write output
                await FileSystem.createPath(RS.Path.directoryName(outputPath));
                await FileSystem.writeFile(outputPath, JSON.stringify(result));

                // Commit checksum
                this.module.checksumDB.set(this.checksumID, this.checksum);
            }

            // Return variant data
            return {
                paths: [ RS.Path.sanitise(RS.Path.replaceExtension(assetPath, ".json")) ]
            };
        }
    }
}