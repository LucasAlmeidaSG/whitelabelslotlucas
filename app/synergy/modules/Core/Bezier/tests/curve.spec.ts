namespace RS.Tests
{
    const { expect } = chai;

    describe("Curve.ts", function ()
    {
        const epsilon = RS.Bezier.Utils.epsilon;

        function expectVec2ToEqual(observed: Math.ReadonlyVector2D, expected: Math.ReadonlyVector2D, message?: string, overrideEpsilon = epsilon): void
        {
            expect(observed.x).to.be.approximately(expected.x, overrideEpsilon, `x${message ? `: ${message}` : ""}`);
            expect(observed.y).to.be.approximately(expected.y, overrideEpsilon, `y${message ? `: ${message}` : ""}`);
        }

        function expectBBoxToEqual(observed: Math.ReadonlyAABB2D, expected: Math.ReadonlyAABB2D, message?: string): void
        {
            expect(observed.minX).to.be.approximately(expected.minX, epsilon, `minX${message ? `: ${message}` : ""}`);
            expect(observed.minY).to.be.approximately(expected.minY, epsilon, `minY${message ? `: ${message}` : ""}`);
            expect(observed.maxX).to.be.approximately(expected.maxX, epsilon, `maxX${message ? `: ${message}` : ""}`);
            expect(observed.maxY).to.be.approximately(expected.maxY, epsilon, `maxY${message ? `: ${message}` : ""}`);
        }

        describe("Curve", function ()
        {
            describe("compute", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expectVec2ToEqual(curve.compute(0.0), { x: 0.0, y: 0.0 });
                    expectVec2ToEqual(curve.compute(0.5), { x: 50.0, y: 50.0 });
                    expectVec2ToEqual(curve.compute(1.0), { x: 100.0, y: 100.0 });
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectVec2ToEqual(curve.compute(0.0), { x: 0.0, y: 0.0 });
                    expectVec2ToEqual(curve.compute(0.5), { x: 0.5, y: 0.5 });
                    expectVec2ToEqual(curve.compute(1.0), { x: 1.0, y: 0.0 });
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectVec2ToEqual(curve.compute(0.0), { x: 0.0, y: 0.0 });
                    expectVec2ToEqual(curve.compute(0.5), { x: 0.5, y: 0.75 });
                    expectVec2ToEqual(curve.compute(1.0), { x: 1.0, y: 0.0 });
                });

                it("should have correct behaviour for order 4 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 }
                    ]);
                    expectVec2ToEqual(curve.compute(0.0), { x: 0.0, y: 0.0 });
                    expectVec2ToEqual(curve.compute(0.25), { x: 0.265625, y: 0.734375 });
                    expectVec2ToEqual(curve.compute(0.5), { x: 0.75, y: 1.25 });
                    expectVec2ToEqual(curve.compute(0.75), { x: 1.265625, y: 1.734375 });
                    expectVec2ToEqual(curve.compute(1.0), { x: 2.0, y: 2.0 });
                });

                it("should have correct behaviour for order 5 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 },
                        { x: 2.0, y: 3.0 }
                    ]);
                    expectVec2ToEqual(curve.compute(0.0), { x: 0.0, y: 0.0 });
                    expectVec2ToEqual(curve.compute(0.25), { x: 0.3828125, y: 0.8671875 });
                    expectVec2ToEqual(curve.compute(0.5), { x: 1.0, y: 1.5 });
                    expectVec2ToEqual(curve.compute(0.75), { x: 1.6171875, y: 2.1328125 });
                    expectVec2ToEqual(curve.compute(1.0), { x: 2.0, y: 3.0 });
                });
            });

            describe("dpoints", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expect(curve.dpoints).to.deep.equal([
                        [{ x: 100, y: 100 }]
                    ]);
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expect(curve.dpoints).to.deep.equal([
                        [{ x: 1, y: 2 }, { x: 1, y: -2 }],
                        [{ x: 0, y: -4 }]
                    ]);
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expect(curve.dpoints).to.deep.equal([
                        [{ x: 0, y: 3 }, { x: 3, y: 0 }, { x: 0, y: -3 }],
                        [{ x: 6, y: -6 }, { x: -6, y: -6 }],
                        [{ x: -12, y: 0 }]
                    ]);
                });

                it("should have correct behaviour for order 4 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 }
                    ]);
                    expect(curve.dpoints).to.deep.equal([
                        [{ x: 0, y: 4 }, { x: 4, y: 0 }, { x: 0, y: 4 }, { x: 4, y: 0 }],
                        [{ x: 12, y: -12 }, { x: -12, y: 12 }, { x: 12, y: -12 }],
                        [{ x: -48, y: 48 }, { x: 48, y: -48 }],
                        [{ x: 96, y: -96 }]
                    ]);
                });

                it("should have correct behaviour for order 5 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 },
                        { x: 2.0, y: 3.0 }
                    ]);
                    expect(curve.dpoints).to.deep.equal([
                        [{ x: 0, y: 5 }, { x: 5, y: 0 }, { x: 0, y: 5 }, { x: 5, y: 0 }, { x: 0, y: 5 }],
                        [{ x: 20, y: -20 }, { x: -20, y: 20 }, { x: 20, y: -20 }, { x: -20, y: 20 }],
                        [{ x: -120, y: 120 }, { x: 120, y: -120 }, { x: -120, y: 120 }],
                        [{ x: 480, y: -480 }, { x: -480, y: 480 }],
                        [{ x: -960, y: 960 }]
                    ]);
                });
            });

            describe("derivative", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expectVec2ToEqual(curve.derivative(0.0), { x: 100.0, y: 100.0 });
                    expectVec2ToEqual(curve.derivative(0.5), { x: 100.0, y: 100.0 });
                    expectVec2ToEqual(curve.derivative(1.0), { x: 100.0, y: 100.0 });
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectVec2ToEqual(curve.derivative(0.0), { x: 1.0, y: 2.0 });
                    expectVec2ToEqual(curve.derivative(0.5), { x: 1.0, y: 0.0 });
                    expectVec2ToEqual(curve.derivative(1.0), { x: 1.0, y: -2.0 });
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectVec2ToEqual(curve.derivative(0.0), { x: 0.0, y: 3.0 });
                    expectVec2ToEqual(curve.derivative(0.5), { x: 1.5, y: 0.0 });
                    expectVec2ToEqual(curve.derivative(1.0), { x: 0.0, y: -3.0 });
                });
            });

            describe("inflections", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expect(curve.inflections()).to.deep.equal([]);
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expect(curve.inflections()).to.deep.equal([]);
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expect(curve.inflections()).to.deep.equal([]);

                    const curve2 = new RS.Bezier.Curve([
                        { x: 100, y: 25 },
                        { x: 10, y: 90 },
                        { x: 110, y: 100 },
                        { x: 150, y: 195 }
                    ]);
                    expect(curve2.inflections()).to.deep.equal([0.7422657651777699]);
                });

                it("should have correct behaviour for order 4 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 }
                    ]);
                    expect(curve.inflections()).to.deep.equal([2 / 3]);
                });

                it("should have correct behaviour for order 5 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 },
                        { x: 2.0, y: 3.0 }
                    ]);
                    expect(curve.inflections()).to.deep.equal([0.5519707931939497]);
                });
            });

            describe("length", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expect(curve.length).to.be.approximately(100 * Math.sqrt(2), epsilon);
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expect(curve.length).to.be.approximately(1.4789428575453212, epsilon);
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);

                    expect(curve.length).to.be.approximately(2.0, epsilon);
                });
            });

            describe("normal", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expectVec2ToEqual(curve.normal(0.0), { x: -0.7071067811865475, y: 0.7071067811865475 });
                    expectVec2ToEqual(curve.normal(0.5), { x: -0.7071067811865475, y: 0.7071067811865475 });
                    expectVec2ToEqual(curve.normal(1.0), { x: -0.7071067811865475, y: 0.7071067811865475 });
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectVec2ToEqual(curve.normal(0.0), { x: -0.8944271909999159, y: 0.4472135954999579 });
                    expectVec2ToEqual(curve.normal(0.5), { x: 0.0, y: 1.0 });
                    expectVec2ToEqual(curve.normal(1.0), { x: 0.8944271909999159, y: 0.4472135954999579 });
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectVec2ToEqual(curve.normal(0.0), { x: -1.0, y: 0.0 });
                    expectVec2ToEqual(curve.normal(0.5), { x: 0.0, y: 1.0 });
                    expectVec2ToEqual(curve.normal(1.0), { x: 1.0, y: 0.0 });
                });
            });

            describe("extrema", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    const extrema = curve.extrema();
                    expect(extrema.x).to.deep.equal([], "x");
                    expect(extrema.y).to.deep.equal([], "y");
                    expect(extrema.values).to.deep.equal([], "values");
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    const extrema = curve.extrema();
                    expect(extrema.x).to.deep.equal([], "x");
                    expect(extrema.y).to.deep.equal([0.5], "y");
                    expect(extrema.values).to.deep.equal([0.5], "values");
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    const extrema = curve.extrema();
                    expect(extrema.x).to.deep.equal([0.0, 0.5, 1.0], "x");
                    expect(extrema.y).to.deep.equal([0.5], "y");
                    expect(extrema.values).to.deep.equal([0.0, 0.5, 1.0], "values");
                });
            });

            describe("bbox", function ()
            {
                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    expectBBoxToEqual(curve.getBBox(), {
                        minX: 0.0, minY: 0.0,
                        maxX: 100.0, maxY: 100.0
                    });
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectBBoxToEqual(curve.getBBox(), {
                        minX: 0.0, minY: 0.0,
                        maxX: 1.0, maxY: 0.5
                    });
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    expectBBoxToEqual(curve.getBBox(), {
                        minX: 0.0, minY: 0.0,
                        maxX: 1.0, maxY: 0.75
                    });
                });
            });

            describe("project", function ()
            {
                // Project has lower numerical stability so allow more error
                const localEpsilon = 0.01;

                it("should have correct behaviour for lines", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]);
                    const proj = curve.project({ x: 80, y: 20 });
                    expectVec2ToEqual(proj, { x: 50, y: 50 }, undefined, localEpsilon);
                    expect(proj.t).to.be.approximately(0.5, localEpsilon, "t");
                    expect(proj.d).to.be.approximately(42.42640687119285, localEpsilon, "d");
                });

                it("should have correct behaviour for quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    const proj = curve.project({ x: 0.8, y: 0.2 });
                    expectVec2ToEqual(proj, { x: 0.86, y: 0.2408 }, undefined, localEpsilon);
                    expect(proj.t).to.be.approximately(0.86, localEpsilon, "t");
                    expect(proj.d).to.be.approximately(0.07255783899758854, localEpsilon, "d");
                });

                it("should have correct behaviour for cubic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]);
                    const proj = curve.project({ x: 0.8, y: 0.2 });
                    expectVec2ToEqual(proj, { x: 0.97955325, y: 0.233325 }, undefined, localEpsilon);
                    expect(proj.t).to.be.approximately(0.915, localEpsilon, "t");
                    expect(proj.d).to.be.approximately(0.18261961890925765, localEpsilon, "d");
                });

                it("should have correct behaviour for order 4 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 }
                    ]);
                    const proj = curve.project({ x: 0.8, y: 0.2 });
                    expectVec2ToEqual(proj, { x: 0.163903417344, y: 0.588096582656 }, undefined, localEpsilon);
                    expect(proj.t).to.be.approximately(0.188, localEpsilon, "t");
                    expect(proj.d).to.be.approximately(0.7451428184824082, localEpsilon, "d");
                });

                it("should have correct behaviour for order 5 curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 2.0 },
                        { x: 2.0, y: 2.0 },
                        { x: 2.0, y: 3.0 }
                    ]);
                    const proj = curve.project({ x: 0.8, y: 0.2 });
                    expectVec2ToEqual(proj, { x: 0.17083064182374405, y: 0.589169358176256 }, undefined, localEpsilon);
                    expect(proj.t).to.be.approximately(0.152, localEpsilon, "t");
                    expect(proj.d).to.be.approximately(0.7398019130897412, localEpsilon, "d");
                });
            });

            describe("intersects", function ()
            {
                it("should test for line intersection with quadratic curves", function ()
                {
                    const curve = new RS.Bezier.Curve([
                        { x: 76, y: 250 },
                        { x: 77, y: 150 },
                        { x: 220, y: 50 }
                    ]);
                    const line = Math.Line2D.fromPoints(Math.Vector2D(13, 140), Math.Vector2D(213, 140));
                    const result = curve.intersects(line);
                    expect(result).to.deep.equal([ 0.55 ]);
                });
            });
        });
    });
}