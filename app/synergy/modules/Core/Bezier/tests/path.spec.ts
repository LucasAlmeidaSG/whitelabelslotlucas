namespace RS.Tests
{
    const { expect } = chai;

    describe("Path.ts", function ()
    {
        const epsilon = RS.Bezier.Utils.epsilon;

        function expectVec2ToEqual(observed: Math.ReadonlyVector2D, expected: Math.ReadonlyVector2D, message?: string, overrideEpsilon = epsilon): void
        {
            expect(observed.x).to.be.approximately(expected.x, overrideEpsilon, `x${message ? `: ${message}` : ""}`);
            expect(observed.y).to.be.approximately(expected.y, overrideEpsilon, `y${message ? `: ${message}` : ""}`);
        }

        function expectBBoxToEqual(observed: Math.ReadonlyAABB2D, expected: Math.ReadonlyAABB2D, message?: string): void
        {
            expect(observed.minX).to.be.approximately(expected.minX, epsilon, `minX${message ? `: ${message}` : ""}`);
            expect(observed.minY).to.be.approximately(expected.minY, epsilon, `minY${message ? `: ${message}` : ""}`);
            expect(observed.maxX).to.be.approximately(expected.maxX, epsilon, `maxX${message ? `: ${message}` : ""}`);
            expect(observed.maxY).to.be.approximately(expected.maxY, epsilon, `maxY${message ? `: ${message}` : ""}`);
        }

        function expectCurveToEqual(observed: RS.Bezier.Curve, expected: RS.Bezier.Curve, message?: string): void
        {
            expect(observed.points.length).to.equal(expected.points.length, `order${message ? `: ${message}` : ""}`);
            for (let i = 0, l = observed.points.length; i < l; ++i)
            {
                expectVec2ToEqual(observed.points[i], expected.points[i], `p${i}${message ? `: ${message}` : ""}`)
            }
        }

        describe("Path", function ()
        {
            describe("fromSVG", function ()
            {
                it("should parse a simple line", function ()
                {
                    const path = RS.Bezier.Path.fromSVG("M 0 0 L 100 100");
                    expect(path.curves).to.have.length(1);
                    expectCurveToEqual(path.curves[0], new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 100.0, y: 100.0 }
                    ]));
                });

                it("should parse a simple quadratic curve", function ()
                {
                    const path = RS.Bezier.Path.fromSVG("M 0 0 Q 0.5 1 1 0");
                    expect(path.curves).to.have.length(1);
                    expectCurveToEqual(path.curves[0], new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.5, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]));
                });

                it("should parse a simple cubic curve", function ()
                {
                    const path = RS.Bezier.Path.fromSVG("M 0 0 C 0 1 1 1 1 0");
                    expect(path.curves).to.have.length(1);
                    expectCurveToEqual(path.curves[0], new RS.Bezier.Curve([
                        { x: 0.0, y: 0.0 },
                        { x: 0.0, y: 1.0 },
                        { x: 1.0, y: 1.0 },
                        { x: 1.0, y: 0.0 }
                    ]));
                });

                it("should parse a relative cubic curve", function ()
                {
                    const path = RS.Bezier.Path.fromSVG("m1-1c0,1 1,1 1,0");
                    expect(path.curves).to.have.length(1);
                    expectCurveToEqual(path.curves[0], new RS.Bezier.Curve([
                        { x: 1.0, y: -1.0 },
                        { x: 1.0, y: 0.0 },
                        { x: 2.0, y: 0.0 },
                        { x: 2.0, y: -1.0 }
                    ]));
                });

                it("should parse a complex multi-curve path", function ()
                {
                    const path1 = RS.Bezier.Path.fromSVG("M138.406787,51.9034859 C138.406787,51.9034859 74.8695308,-25.4868408 21.9358155,9.81888414 C-30.9978998,45.1246091 32.5393566,122.514936 32.5393566,122.514936");
                    expect(path1.curves).to.have.length(2);

                    const path2 = RS.Bezier.Path.fromSVG("M133.38,286.76 C59.7162601,286.76 0,227.04374 0,153.38 C0,79.7162601 59.7162601,20 133.38,20 C207.04374,20 266.76,79.7162601 266.76,153.38 C266.76,227.04374 207.04374,286.76 133.38,286.76 Z");
                    expect(path2.curves).to.have.length(4);
                });
            });
        });
    });
}