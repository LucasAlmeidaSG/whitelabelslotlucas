/// <reference path="Logger.ts" />

namespace RS.Logging
{
    export class ContextualLogger implements RS.Logging.IContextualLogger
    {
        public logLevel: LogLevel = Logging.LogLevel.DefaultMinimum;
        /** Whether or not to show stack trace on all messages. */
        public forceStackTrace: boolean = false;

        public verbose = false;
        public get queuedMessages(): ReadonlyArray<string> { return this._queue; }

        public context: string;
        public parent: Logger | ContextualLogger;

        private readonly _queue: string[] = [];
        private readonly _contexts: { [name: string]: ContextualLogger & ILogger.IExtensions } = {};

        public init(context: string, parent: Logger | ContextualLogger)
        {
            this.context = context;
            this.parent = parent;
        }

        public getContext(context: string)
        {
            if (!this._contexts[context])
            {
                const contextLogger = IContextualLogger.get() as ContextualLogger & ILogger.IExtensions;
                contextLogger.init(context, this);
                contextLogger.logLevel = this.logLevel;
                this._contexts[context] = contextLogger;
            }
            return this._contexts[context];
        }

        public queue(...messages: any[]): void
        {
            const message = messages.join(" ");
            if (this.verbose) { this.log(message, LogLevel.Debug, true); }
            this.queueInternal(message);
        }

        public drop(): void
        {
            this._queue.length = 0;
        }

        public dump(logLevel: LogLevel = LogLevel.Default): void
        {
            this.logMultiple(this._queue, logLevel);
            this._queue.length = 0;
        }

        public log(obj: any, logLevel?: LogLevel, excludeStackTrace?: boolean): void
        {
            if (logLevel < this.logLevel) { return; }
            const message = this.forceStackTrace ? `${this.tagMessage(obj)}\n${Util.getStackTrace()}` : this.tagMessage(obj);
            this.parent.log(message, logLevel, excludeStackTrace || this.forceStackTrace);
        }

        protected logMultiple(messages: any[], logLevel?: LogLevel, excludeStackTrace?: boolean)
        {
            messages.forEach((message) => this.log(message, logLevel, excludeStackTrace));
        }

        protected queueInternal(message: string): void
        {
            if (this.parent instanceof ContextualLogger) { this.parent.queueInternal(this.tagMessage(message)); }
            this._queue.push(message);
        }

        protected tagMessage(message: any): string
        {
            return `[${this.context}] ${message}`;
        }

    }

    IContextualLogger.register(ContextualLogger);
}