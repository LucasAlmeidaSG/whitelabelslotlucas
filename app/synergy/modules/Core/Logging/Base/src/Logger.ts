namespace RS.Logging
{
    /**
     * Custom logger with various logging levels. Can be turned off easily for production.
     */
    export class Logger implements ILogger
    {
        /**
         * Any log messages with a level below this value are ignored.
         */
        private _minLogLevel: LogLevel;

        /**
         * Whether each log level should show a stack trace.
        **/
        private shouldAppendStackTrace = {
            [LogLevel.Debug]: false,
            [LogLevel.Info]: false,
            [LogLevel.Warn]: true,
            [LogLevel.Error]: true,
            [LogLevel.Fatal]: true,
            [LogLevel.None]: false
        };

        private readonly _contexts: { [name: string]: ContextualLogger & Logging.ILogger.IExtensions } = {};

        public getContext(context: string)
        {
            if (!this._contexts[context])
            {
                const contextLogger = IContextualLogger.get() as ContextualLogger & ILogger.IExtensions;
                contextLogger.init(context, this);
                const debug = URL.getParameterAsBool("verbose", false) || URL.getParameterAsBool(context.toLowerCase() + "debug", false);
                if (debug) { contextLogger.logLevel = LogLevel.Debug; }
                this._contexts[context] = contextLogger;
            }
            return this._contexts[context];
        }

        /**
         * Set the minimum logging level. Levels below this will be ignored.
         * Set to LogLevel.NONE to ignore all logging.
         */
        public setLogLevel(logLevel: LogLevel): void
        {
            this._minLogLevel = logLevel;
        }

        /**
         * Logs the specified error.
         */
        public log(err: Error, logLevel?: LogLevel, excludeStackTrace?: boolean): void;

        /**
         * Logs the specified message.
         */
        public log(message: string, logLevel?: LogLevel, excludeStackTrace?: boolean): void;

        public log(obj: any, logLevel?: LogLevel, excludeStackTrace?: boolean): void;

        /**
         * Logs the specified message.
         */
        public log(obj: any, logLevel: LogLevel = LogLevel.Default, excludeStackTrace?: boolean): void
        {
            if (logLevel < this._minLogLevel)
            {
                return;
            }

            let message: string;

            // Is it an error?
            if (obj instanceof Error)
            {
                if (logLevel == null) { logLevel = LogLevel.Error; }
                if (logLevel < this._minLogLevel) { return; }
                if (this.shouldAppendStackTrace[logLevel] && !excludeStackTrace)
                {
                    message = `${obj.message}\n${obj.stack || "No stack trace available"}`;
                }
                else
                {
                    message = obj.message;
                }
            }
            else
            {
                if (logLevel == null) { logLevel = LogLevel.Info; }
                if (logLevel < this._minLogLevel) { return; }
                if (this.shouldAppendStackTrace[logLevel] && !excludeStackTrace)
                {
                    message = `${obj}\n${Util.getStackTrace()}`;
                }
                else
                {
                    message = obj;
                }
            }

            // Write to console
            switch (logLevel)
            {
                case LogLevel.Debug:
                    console.debug(message);
                    break;

                case LogLevel.Info:
                    console.log(message);
                    break;

                case LogLevel.Warn:
                    console.warn(message);
                    break;

                case LogLevel.Error:
                case LogLevel.Fatal:
                    console.error(message);
                    break;
            }

            // Write to analytics
            const analytics = Analytics.IAnalytics.get();
            if (analytics != null)
            {
                const tracker = analytics.getTracker();
                if (tracker != null)
                {
                    switch (logLevel)
                    {
                        case LogLevel.Error:
                            tracker.error({ message, fatal: false });
                            break;
                        case LogLevel.Fatal:
                            tracker.error({ message, fatal: true });
                            break;
                    }
                }
            }
        }
    }

    ILogger.register(Logger);
}
