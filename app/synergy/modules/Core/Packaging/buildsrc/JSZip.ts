declare interface JSZipStatic
{
    readonly support:
    {
        "arraybuffer": boolean;
        "uint8array": boolean;
        "blob": boolean;
        "nodebuffer": boolean;
        "nodestream": boolean;
    };

    readonly external:
    {
        Promise: object;
    };

    readonly version: string;

    new(): JSZip;

    loadAsync(data: string, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: number[], options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: ArrayBuffer, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: Uint8Array, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: Buffer, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: Blob, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;

    loadAsync(data: PromiseLike<string>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<number[]>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<ArrayBuffer>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<Uint8Array>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<Buffer>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<Blob>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
}

declare interface JSZip
{
    loadAsync(data: string, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: number[], options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: ArrayBuffer, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: Uint8Array, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: Buffer, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;
    loadAsync(data: Blob, options?: Partial<JSZip.LoadOptions>): PromiseLike<JSZip>;

    loadAsync(data: PromiseLike<string>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<number[]>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<ArrayBuffer>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<Uint8Array>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<Buffer>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;
    loadAsync(data: PromiseLike<Blob>, options?: JSZip.LoadOptions): PromiseLike<JSZip>;

    file(fileName: string, content: string, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: ArrayBuffer, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: Uint8Array, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: Buffer, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: Blob, options?: Partial<JSZip.FileOptions>): JSZip;

    file(fileName: string, content: PromiseLike<string>, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: PromiseLike<ArrayBuffer>, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: PromiseLike<Uint8Array>, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: PromiseLike<Buffer>, options?: Partial<JSZip.FileOptions>): JSZip;
    file(fileName: string, content: PromiseLike<Blob>, options?: Partial<JSZip.FileOptions>): JSZip;

    file(fileName: string): JSZip.ZipObject;
    file(regex: RegExp): JSZip.ZipObject[];

    folder(folderName: string): JSZip;
    folder(regex: RegExp): JSZip.ZipObject[];

    remove(fileName: string): JSZip;

    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.Base64>, onUpdate?: JSZip.UpdateFunction): PromiseLike<string>;
    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.BinaryString>, onUpdate?: JSZip.UpdateFunction): PromiseLike<string>;
    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.Array>, onUpdate?: JSZip.UpdateFunction): PromiseLike<number[]>;
    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.UInt8Array>, onUpdate?: JSZip.UpdateFunction): PromiseLike<Uint8Array>;
    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.ArrayBuffer>, onUpdate?: JSZip.UpdateFunction): PromiseLike<ArrayBuffer>;
    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.Blob>, onUpdate?: JSZip.UpdateFunction): PromiseLike<Blob>;
    generateAsync(options: JSZip.GenerateOptions<JSZip.ContentType.NodeBuffer>, onUpdate?: JSZip.UpdateFunction): PromiseLike<Buffer>;

    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.Base64>, onUpdate?: JSZip.UpdateFunction): any;
    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.BinaryString>, onUpdate?: JSZip.UpdateFunction): any;
    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.Array>, onUpdate?: JSZip.UpdateFunction): any;
    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.UInt8Array>, onUpdate?: JSZip.UpdateFunction): any;
    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.ArrayBuffer>, onUpdate?: JSZip.UpdateFunction): any;
    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.Blob>, onUpdate?: JSZip.UpdateFunction): any;
    generateNodeStream(options: JSZip.GenerateOptions<JSZip.ContentType.NodeBuffer>, onUpdate?: JSZip.UpdateFunction): any;

    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.Base64>): any;
    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.BinaryString>): any;
    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.Array>): any;
    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.UInt8Array>): any;
    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.ArrayBuffer>): any;
    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.Blob>): any;
    generateInternalStream(options: JSZip.GenerateOptions<JSZip.ContentType.NodeBuffer>): any;
}

declare namespace JSZip
{
    export const enum CompressionType
    {
        Store = "STORE",
        Deflate = "DEFLATE"
    }
    export const enum ContentType
    {
        Base64 = "base64",
        Text = "text",
        String = "string",
        BinaryString = "binarystring",
        Array = "array",
        UInt8Array = "uint8array",
        ArrayBuffer = "arraybuffer",
        Blob = "blob",
        NodeBuffer = "nodebuffer"
    }
    export interface LoadOptions
    {
        base64: boolean;
        checkCRC32: boolean;
        optimizedBinaryString: boolean;
        createFolders: boolean;
        decodeFileName: (bytes: Uint8Array|number[]) => string;
    }
    export interface FileOptions
    {
        base64: boolean;
        binary: boolean;
        date: Date;
        compression: string;
        compressionOptions: object;
        comment: string;
        optimizedBinaryString: boolean;
        createFolders: boolean;
        unixPermissions: number;
        dosPermissions: number;
        dir: boolean;
    }
    export interface GenerateOptions<TType extends string>
    {
        type: TType;
        compression?: CompressionType;
        compressionOptions?: object;
        comment?: string;
        mimeType?: string;
        platform?: string;
        encodeFileName?: (str: string) => Uint8Array|number[];
        streamFiles?: boolean;
    }
    export type UpdateFunction = (metadata: { percent: number }) => void;
    export interface ZipObject
    {
        readonly name: string;
        readonly dir: boolean;
        readonly date: Date;
        readonly comment: string;
        readonly unixPermissions: number;
        readonly dosPermissions: number;
        readonly options:
        {
            readonly compression: CompressionType;
            readonly compressionOptions: object;
        };

        async(type: ContentType.Base64, onUpdate?: UpdateFunction): PromiseLike<string>;
        async(type: ContentType.Text, onUpdate?: UpdateFunction): PromiseLike<string>;
        async(type: ContentType.String, onUpdate?: UpdateFunction): PromiseLike<string>;
        async(type: ContentType.BinaryString, onUpdate?: UpdateFunction): PromiseLike<string>;
        async(type: ContentType.Array, onUpdate?: UpdateFunction): PromiseLike<number[]>;
        async(type: ContentType.UInt8Array, onUpdate?: UpdateFunction): PromiseLike<Uint8Array>;
        async(type: ContentType.ArrayBuffer, onUpdate?: UpdateFunction): PromiseLike<ArrayBuffer>;
        async(type: ContentType.Blob, onUpdate?: UpdateFunction): PromiseLike<Blob>;
        async(type: ContentType.NodeBuffer, onUpdate?: UpdateFunction): PromiseLike<Buffer>;
        async(type: ContentType, onUpdate?: UpdateFunction): PromiseLike<any>;

        nodeStream(type?: ContentType.NodeBuffer, onUpdate?: UpdateFunction): any;

        internalStream(type: ContentType): any;
    }
}