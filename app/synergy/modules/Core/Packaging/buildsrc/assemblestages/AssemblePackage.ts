namespace RS.Packaging.AssembleStages
{
    const jszip: JSZipStatic = require("jszip");

    /**
     * Responsible for outputting a zipped package.
     */
    export abstract class AssemblePackage extends AssembleStage.Base
    {
        public includeSourceMaps: boolean = false;

        protected abstract readonly packageName: string;
        protected abstract readonly tag?: string;
        protected abstract readonly tagMessage?: string;

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            // Assemble
            Log.info(`Assembling package...`);
            const timer = new Timer();

            const zip = new jszip();
            try
            {
                await this.addPackageFilesToZip(zip, settings);
            }
            catch (err)
            {
                Log.error(`Failed to build package: ${err}`);
                return;
            }

            const outputZip = await zip.generateAsync({
                type: JSZip.ContentType.UInt8Array,
                compression: JSZip.CompressionType.Deflate,
                compressionOptions: { level: 9 }
            });

            let packageName: string;
            try
            {
                packageName = this.packageName;
            }
            catch (err)
            {
                Log.error(`Failed to get package name: ${err}`);
                return;
            }

            const packagePath = Path.combine(settings.outputDir, `${packageName}.zip`);
            await FileSystem.writeFile(packagePath, outputZip);
            Log.info(`Package '${packageName}' (${Util.formatSize(outputZip.byteLength)}) assembled in ${timer}`);

            const tag = this.tag, message = this.tagMessage;
            if (tag != null) { await this.tagCommit(tag, message || ""); }
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // No per-module work to do here
        }

        protected abstract addPackageFilesToZip(zip: JSZip, settings: AssembleStage.AssembleSettings): PromiseLike<void>;

        protected async addScriptFileToZip(zip: JSZip, srcPath: string, destPath: string = srcPath)
        {
            await this.addFileToZip(zip, srcPath, destPath);

            if (this.includeSourceMaps)
            {
                const mapFile = Path.replaceExtension(srcPath, ".js.map");
                if (await FileSystem.fileExists(mapFile))
                {
                    await this.addFileToZip(zip, mapFile, Path.replaceExtension(destPath, ".js.map"));
                }
            }
        }

        protected async addFileToZip(zip: JSZip, srcPath: string, destPath: string = srcPath)
        {
            const fileBuffer = await FileSystem.readFile(srcPath, true);
            const path = this.sanitiseZipPath(destPath);
            zip.file(path, fileBuffer, { createFolders: true, binary: true });
        }

        protected async addDirectoryToZip(zip: JSZip, srcPath: string, destPath: string = srcPath)
        {
            const files = await FileSystem.readFiles(srcPath);
            for (const file of files)
            {
                await this.addFileToZip(zip, file, Path.combine(destPath, Path.relative(srcPath, file)));
            }
        }

        protected async tagCommit(tag: string, message: string)
        {
            // Clear old tag
            try
            {
                await Git.untag(tag);
            }
            catch (err)
            {
                if (!Is.string(err) || err.indexOf("not found") === -1)
                {
                    Log.warn(`Failed to remove previous version tag: ${err}`);
                }
            }

            // Tag commit
            try
            {
                await Git.tag(tag, message);
            }
            catch (err)
            {
                Log.error(`Failed to tag commit: ${err}`);
                return;
            }

            // Update remote
            try
            {
                await Git.pushTag(tag);
            }
            catch (err)
            {
                Log.warn(`Failed to push tag: ${err}`);
                Log.warn("Use git push --tags to push manually");
            }

            // Done
            Log.info(`Tagged commit as '${tag}'`);
        }

        /** Sanitises a path for JSZip. */
        protected sanitiseZipPath(path: string): string
        {
            if (!Path.seperator)
            {
                Log.warn("No path separator found");
                return path;
            }
            if (Path.seperator === "/") { return path; }
            return Util.replaceAll(path, Path.seperator, "/");
        }
    }
}