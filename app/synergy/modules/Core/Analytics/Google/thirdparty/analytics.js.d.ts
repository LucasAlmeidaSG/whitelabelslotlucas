
declare interface GAFunction
{
    (command: string, ...args: any[]): void;
    (func: (tracker: object) => void): void;
    q: any[][];
}

declare const ga: GAFunction;