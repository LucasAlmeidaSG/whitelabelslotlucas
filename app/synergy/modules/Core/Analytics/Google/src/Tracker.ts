/** @internal */
namespace RS.Analytics.Google
{
    export class Tracker implements ITracker
    {
        protected _trackingID: string;
        protected _name: string | null;
        protected _tracker: object;
        protected _appInfo: Tracker.AppInfo;

        /** Gets the name of this tracker, or null if it's the default tracker. */
        public get name() { return this._name; }

        public constructor(trackingID: string, appInfo: Tracker.AppInfo, name?: string, cookieDomain: string = "auto")
        {
            this._trackingID = trackingID;
            this._appInfo = appInfo;
            this._name = name || null;
            ga("create", trackingID, cookieDomain, name);
            ga((tracker) => this._tracker = tracker);
        }

        /**
         * Dispatches a command via this tracker.
         * @param commandName 
         * @param data 
         */
        public command(commandName: string, data: object): void
        {
            if (this._name)
            {
                ga(`${this._name}.${commandName}`, data);
            }
            else
            {
                ga(`${commandName}`, data);
            }
        }

        /**
         * Dispatches a send command via this tracker.
         * @param arg 
         */
        public send(arg: Tracker.SendArg): void
        {
            this.command("send", { ...this._appInfo, ...arg });
        }

        /** Registers an event via this tracker. */
        public event(info: ITracker.EventInfo): void
        {
            this.send({ hitType: Tracker.HitType.Event, eventAction: info.action, eventCategory: info.category, eventLabel: info.label, eventValue: info.value });
        }
        
        /** Registers a timing via this tracker. */
        public timing(info: ITracker.TimingInfo): void
        {
            this.send({ hitType: Tracker.HitType.Timing, timingCategory: info.category, timingVar: info.var, timingLabel: info.label, timingValue: info.value });
        }

        /** Registers a screen change via this tracker. */
        public screen(info: ITracker.ScreenInfo): void
        {
            this.send({ hitType: Tracker.HitType.ScreenView, screenName: info.name });
        }

        /** Registers an error via this tracker. */
        public error(info: ITracker.ErrorInfo): void
        {
            this.send({ hitType: Tracker.HitType.Exception, exDescription: info.message, exFatal: info.fatal || false });
        }
    }

    export namespace Tracker
    {
        export interface AppInfo
        {
            appName: string;
            appId?: string;
            appVersion?: string;
            appInstallerId?: string;
        }

        export enum HitType
        {
            PageView = "pageview",
            ScreenView = "screenview",
            Event = "event",
            Transaction = "transaction",
            Item = "item",
            Social = "social",
            Exception = "exception",
            Timing = "timing"
        }
        export interface BaseSendArg<T extends HitType>
        {
            hitType: T;
        }

        export interface PageViewSendArg extends BaseSendArg<HitType.PageView>
        {
            title?: string;
            location?: string;
            page?: string;
        }

        export interface EventSendArg extends BaseSendArg<HitType.Event>
        {
            eventCategory: string;
            eventAction: string;
            eventLabel?: string;
            eventValue?: number;
            nonInteraction?: boolean;
        }

        export interface SocialSendArg extends BaseSendArg<HitType.Social>
        {
            socialNetwork: string;
            socialAction: string;
            socialTarget: string;
        }

        export interface ScreenViewSendArg extends BaseSendArg<HitType.ScreenView>
        {
            screenName: string;
        }

        export interface TimingSendArg extends BaseSendArg<HitType.Timing>
        {
            timingCategory: string;
            timingVar: string;
            timingValue: number;
            timingLabel?: string;
        }

        export interface ExceptionSendArg extends BaseSendArg<HitType.Exception>
        {
            exDescription?: string;
            exFatal?: boolean;
        }

        export type SendArg = PageViewSendArg | EventSendArg | SocialSendArg | ScreenViewSendArg | TimingSendArg | ExceptionSendArg;
    }
}