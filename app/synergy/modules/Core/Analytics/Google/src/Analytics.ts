/// <reference path="Tracker.ts" />
/** @internal */
namespace RS.Analytics.Google
{
    class Analytics implements IAnalytics
    {
        protected _inited: boolean = false;
        protected _defaultTracker: Tracker | null = null;
        
        protected _appInfo: IAnalytics.AppInfo;
        protected _trackingID: string;

        protected _trackers: Map<Tracker>;

        /**
         * Initialises analytics using the specified application info and API key.
         */
        public initialise(appInfo: IAnalytics.AppInfo, apiKey: string): void
        {
            if (this._inited) { return; }

            this._appInfo = appInfo;
            this._trackingID = apiKey;

            const script = document.createElement("script");
            script.src = "https://www.google-analytics.com/analytics.js";
            document.head.appendChild(script);
    
            if (window["ga"] == null)
            {
                window["ga"] = function()
                {
                    window["ga"]["q"].push(arguments);
                };
                window["ga"]["q"] = [];
            }

            this._trackers = {};
            this._defaultTracker = this._trackers.default = new Tracker(this._trackingID, { appName: this._appInfo.name, appVersion: Version.toString(this._appInfo.version) });
            this._defaultTracker.send({ hitType: Tracker.HitType.PageView });

            this._inited = true;
        }
        
        /**
         * Gets a tracker with the specified name.
         */
        public getTracker(name: string = "default"): ITracker
        {
            if (!this._inited) { return null; }
            const tracker = this._trackers[name];
            if (tracker != null) { return tracker; }
            return this._trackers[name] = new Tracker(this._trackingID, { appName: this._appInfo.name, appVersion: Version.toString(this._appInfo.version) });
        }
    }

    IAnalytics.register(Analytics);
}