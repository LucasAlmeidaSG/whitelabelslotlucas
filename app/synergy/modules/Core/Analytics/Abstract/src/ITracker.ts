namespace RS.Analytics
{
    /**
     * Represents a tracker associated with an analytics backend.
     */
    export interface ITracker
    {
        /** Gets the name of the tracker, or null for the default/root tracker. */
        readonly name: string | null;

        /** Registers an event via this tracker. */
        event(info: ITracker.EventInfo): void;

        /** Registers a timing via this tracker. */
        timing(info: ITracker.TimingInfo): void;

        /** Registers a screen change via this tracker. */
        screen(info: ITracker.ScreenInfo): void;

        /** Registers an error via this tracker. */
        error(info: ITracker.ErrorInfo): void;
    }

    export namespace ITracker
    {
        export interface EventInfo
        {
            category: string;
            action: string;
            value?: number;
            label?: string;
        }

        export interface TimingInfo
        {
            category: string;
            var: string;
            value: number;
            label?: string;
        }

        export interface ScreenInfo
        {
            name: string;
        }

        export interface ErrorInfo
        {
            message: string;
            fatal?: boolean;
        }
    }
}