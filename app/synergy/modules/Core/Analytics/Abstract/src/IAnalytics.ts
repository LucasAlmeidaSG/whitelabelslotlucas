namespace RS.Analytics
{
    /**
     * Represents an interface to an analytics backend.
     */
    export interface IAnalytics
    {
        /**
         * Initialises analytics using the specified application info and API key.
         */
        initialise(appInfo: IAnalytics.AppInfo, apiKey: string): void;

        /**
         * Gets a tracker with the specified name.
         */
        getTracker(name?: string): ITracker | null;
    }

    export const IAnalytics = Strategy.declare<IAnalytics>(Strategy.Type.Singleton, true);

    export namespace IAnalytics
    {
        export interface AppInfo
        {
            name: string;
            version: Version;
        }
    }
}