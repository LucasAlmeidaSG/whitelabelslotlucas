namespace RS.Analytics
{
    /**
     * Fires an analytics event whenever the method is called.
     * @param info 
     * @param trackerName 
     */
    export function Event(info: ITracker.EventInfo, trackerName?: string): MethodDecorator
    {
        return function(target: object, propertyKey: string, propertyDescriptor: PropertyDescriptor): void
        {
            if (propertyDescriptor != null)
            {
                if (propertyDescriptor.value != null && Is.func(propertyDescriptor.value))
                {
                    const oldFunc = propertyDescriptor.value as ()=>any;
                    propertyDescriptor.value = function()
                    {
                        const analytics = IAnalytics.get();
                        if (analytics != null)
                        {
                            analytics.getTracker(trackerName).event(info);
                        }
                        return oldFunc.apply(this, arguments);
                    };
                }
                Object.defineProperty(target, propertyKey, propertyDescriptor);
            }
        };
    }
}