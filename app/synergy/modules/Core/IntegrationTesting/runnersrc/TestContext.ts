namespace RS.Testing.Integration
{
    export interface TestContext
    {
        readonly driver: webdriver.WebDriver;
        readonly url: string;
        readonly params: { readonly [name: string]: string };
        readonly vars: { [name: string]: string };
    }
}