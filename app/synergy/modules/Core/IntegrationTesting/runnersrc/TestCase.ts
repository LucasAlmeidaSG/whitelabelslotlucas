namespace RS.Testing.Integration
{
    /**
     * Encapsulates an end-to-end test case.
     */ 
    export class TestCase implements IStepRunner
    {
        public get parameterNames() { return this._stepRunner.parameterNames; }

        private readonly _stepRunner: StepRunner;

        constructor(public readonly name: string, stepRegistry: Step.Registry)
        {
            this._stepRunner = new StepRunner(stepRegistry);
        }

        public addStep(name: string, ...args: ResolvableStepArgument[]): this
        {
            this._stepRunner.addStepReference(name, ...args);
            return this;
        }

        public run(context: TestContext)
        {
            return this._stepRunner.runAll(context);
        }
    }

    export namespace TestCase
    {
        export class Registry
        {
            private readonly _testCases: { [name: string]: TestCase } = {};
            public get names() { return Object.keys(this._testCases); }

            public register(name: string, testCase: TestCase)
            {
                if (name in this._testCases) { throw new Error(`Test case '${name}' already exists`); }
                this._testCases[name] = testCase;
            }

            public get(name: string)
            {
                const testCase = this._testCases[name];
                if (!testCase) { throw new Error(`Test case '${name}' not found`); }
                return testCase;
            }
        }
    }
}