/// <reference path="Parameter.ts"/>
const selenium: typeof webdriver = require("selenium-webdriver");

// Suite stuff
let defineCase: typeof RS.Testing.Integration.TestSuite.prototype.defineCase;
let defineStep: typeof RS.Testing.Integration.TestSuite.prototype.defineStep;
const Parameter = RS.Testing.Integration.Parameter;

namespace RS.Testing.Integration
{
    // TODO:
    // - Selenium wrapping/utils/etc.
    // - Run for each supported browser?

    (async function init()
    {
        Log.pushContext("TestRunner");
        await FileSystem.rebuildCache();

        IPC.id = "testrunner";
        IPC.timeout = 1500;
        Log.debug(`Starting IPC server ${IPC.id}`);

        const server = RS.IPC.Server.get();
        await server.start();
        Log.debug("IPC server started");

        // From here on out we need to make sure any errors or shutdown notifies clients (we kind of need to do that anyway but eh... can't do it before the server has actually started!)
        try
        {
            const suite = new TestSuite();
            defineCase = suite.defineCase.bind(suite);
            defineStep = suite.defineStep.bind(suite);

            let messagePromise = server.message();
            await server.broadcast("ready");
            Log.info("Test runner ready");

            while (true)
            {
                const message = await messagePromise;
                const messageArgv = message.split(" ");
                const command = messageArgv[0];
                switch (command)
                {
                    case "load":
                    {
                        const script = messageArgv[1];

                        const code = await FileSystem.readFile(script);
                        eval(code);

                        await server.broadcast("loaded");
                        break;
                    }
                    case "params":
                    {
                        const caseName = messageArgv[1];
                        const names = suite.getParameterNames(caseName);
                        await server.reply(names.join("\n"));
                        break;
                    }
                    case "run":
                    {
                        const browser = messageArgv[1];
                        const rawURL = messageArgv[2];
                        const caseName = messageArgv[3];

                        let url: URL;
                        try
                        {
                            url = new URL(rawURL);
                        }
                        catch (error)
                        {
                            await server.reply(TestResult.serialise({ state: TestState.Error, error: `${error}` }));
                            break;
                        }

                        url.params.set("selenium", "true");

                        Log.info("Acquiring Selenium driver");
                        const driver = await new selenium.Builder()
                            .forBrowser(browser)
                            .build();

                        // Prompt for parameters?!

                        let result: TestResult;
                        try
                        {
                            result = await suite.run(caseName, { driver, url: `${url}`, params: { }, vars: { } });
                        }
                        finally
                        {
                            await driver.close();
                            await driver.quit();
                        }

                        await server.reply(TestResult.serialise(result));
                        break;
                    }
                    case "list":
                    {
                        await server.reply(suite.testCases.join("\n"));
                        break;
                    }
                    case "liststeps":
                    {
                        await server.reply(suite.steps.join("\n"));
                        break;
                    }
                    case "hello":
                    {
                        Log.info("Hello!");
                        break;
                    }
                    case "exit":
                    {
                        process.exit();
                        break;
                    }
                }

                messagePromise = server.message();
            }
        }
        finally
        {
            // Notify all receivers of death
            await server.broadcast("shutdown");
        }
    })();
}
