namespace RS.Testing.Integration
{
    export class Parameter
    {
        public readonly mutator: Func<string>;
        public readonly defaultValue: string;

        constructor(name: string, defaultValue?: string);
        constructor(name: string, mutator?: Func<string>);
        constructor(name: string, defaultValue: string, mutator?: Func<string>);
        constructor(public readonly name: string, p1?: string | Func<string>, p2?: Func<string>)
        {
            if (Is.string(p1))
            {
                this.defaultValue = p1;
                this.mutator = p2;
            }
            else
            {
                this.mutator = p1;
            }
        }
    }
}