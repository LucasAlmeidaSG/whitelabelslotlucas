namespace Selenium
{
    export async function clickElementUsingSelector(driver: webdriver.WebDriver, selector: string): Promise<void>
    {
        const elem = await driver.findElements(selenium.By.css(selector));
        if (!elem.length) { throw new Error(`Failed to click '${selector}' because it was not found`); }
        await driver.executeScript(`document.querySelector('${selector}').onclick()`);
    }

    export async function clickElement(driver: webdriver.WebDriver, name: string): Promise<void>
    {
        const elem = await driver.findElements(selenium.By.name(name));
        if (!elem.length) { throw new Error(`Failed to click '${name}' because it was not found`); }
        await driver.executeScript(`document.getElementsByName("${name}")[0].onclick()`);
    }

    export async function executeRequirements(driver: webdriver.WebDriver, requirements: string[]): Promise<void>
    {
        if (requirements.length === 0) { return; }

        for (let i = requirements.length - 1; i >= 0; --i)
        {
            const requirementName = requirements[i];
            const requiredElements = await driver.findElements(selenium.By.name(requirementName));
            if (requiredElements.length > 0)
            {
                await driver.executeScript(`document.getElementsByName("${requirementName}")[0].onclick()`);
                for (let j = i + 1; j < requirements.length; ++j)
                {
                    const clickTargetID = requirements[j];
                    await driver.wait(selenium.until.elementLocated(selenium.By.name(clickTargetID)), 100);
                    await clickElement(driver, clickTargetID);
                }
                return;
            }
        }

        throw new Error(`Failed to meet requirements because '${requirements[0]}' was not found`);
    }

    export class ClickPanic
    {
        private readonly _finishPromise: Promise<void>;
        private _finishFn: RS.Func<void>;
        private _failFn: RS.Func<any>;

        /** A promise which resolves once we have clicked literally every pixel. */
        public get finish() { return this._finishPromise; }

        private constructor()
        {
            this._finishPromise = new Promise<void>((resolve, reject) =>
            {
                this._finishFn = resolve;
                this._failFn = reject;
            });
        }

        public static until<T>(driver: webdriver.WebDriver, condition: (() => void) | PromiseLike<T> | webdriver.Condition<T> | RS.Func<webdriver.WebDriver, T | PromiseLike<T>>): ClickPanic
        {
            throw new Error("Unimplemented");
        }
        
        public static forever(driver: webdriver.WebDriver): ClickPanic
        {
            throw new Error("Unimplemented");
        }

        public stop(): void
        {
            throw new Error("Unimplemented");
        }
    }
}