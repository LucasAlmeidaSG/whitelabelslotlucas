namespace RS.Testing.Integration
{
    export class StepRunner
    {
        private readonly _stepRefs: StepReference[] = [];

        public get parameterNames(): ReadonlyArray<string>
        {
            return this._stepRefs.map((s) => s.args.filter((arg) => arg instanceof Parameter).map((p: Parameter) => p.name)).reduce((a, b) => [...a, ...b], []);
        }

        constructor(protected readonly registry: Step.Registry) { }

        public addStepReference(name: string, ...args: ResolvableStepArgument[]): this
        {
            this._stepRefs.push({ name, args });
            return this;
        }

        /**
         * Runs all steps starting from the first.
         */
        public async runAll(context: TestContext): Promise<TestResult>
        {
            const steps = this.getAllContextSteps(context);

            for (const step of steps)
            {
                const stepResult = await this.runStep(context, step);
                if (stepResult.state !== TestState.Pass) { return stepResult; }
            }

            return { state: TestState.Pass };
        }

        protected getAllContextSteps(context: TestContext): Step[]
        {
            const result: Step[] = [];
            for (let i = 0; i < this._stepRefs.length; i++)
            {
                result.push(this.getContextStepByIndex(i, context));
            }
            return result;
        }

        protected getContextStepByIndex(index: number, context: TestContext): Step
        {
            const entry = this._stepRefs[index];
            const strArgs = entry.args.map((rawArg) => this.resolveStepArgument(rawArg, context));
            return this.registry.get(entry.name, strArgs);
        }

        protected resolveStepArgument(arg: ResolvableStepArgument, context: TestContext): string
        {
            if (Is.string(arg)) { return arg; }
            let value: string;
            if (!(arg.name in context.params))
            {
                if (arg.defaultValue == null) { throw new Error(`Parameter '${arg.name}' not specified`); }
                value = arg.defaultValue;
            }
            else
            {
                value = context.params[arg.name];
            }
            if (arg.mutator) { value = arg.mutator(value); }
            return value;
        }

        protected async runStep(context: TestContext, step: Step): Promise<TestResult>
        {
            try
            {
                const fail = await step.run(context);
                if (fail != null)
                {
                    return { state: TestState.Failure, reason: `${this.registry.nameOf(step)}: ${fail}` };
                }

                return { state: TestState.Pass };
            }
            catch (err)
            {
                return { state: TestState.Error, error: `${this.registry.nameOf(step)}: ${err}` };
            }
        }
    }

    export class StepError extends BaseError {}

    interface StepReference
    {
        name: string;
        args: ResolvableStepArgument[];
    }
}