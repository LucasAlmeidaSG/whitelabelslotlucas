namespace RS.Testing.Integration
{
    export interface Step
    {
        message: string;
        run: Step.RunFunc;
    }

    export namespace Step
    {
        export type Getter = (...args: string[]) => Step;

        interface BoundGetter extends Step
        {
            getter: Step.Getter;
        }

        function isBoundGetter(step: Step): step is BoundGetter
        {
            return Is.func((step as any).getter);
        }

        export type RunFunc = (context: TestContext) => PromiseLike<string | void>;

        export class Registry
        {
            private readonly _steps: { [name: string]: Step | Step.Getter } = {};
            public get names() { return Object.keys(this._steps); }

            public register(name: string, step: Step | Step.Getter): void
            {
                if (this._steps[name]) { throw new Error(`Test case '${name}' already exists`); }
                this._steps[name] = step;
            }

            public get(name: string, args: string[]): Step
            {
                const step = this._steps[name];
                if (!step) { throw new Error(`Step '${name}' not found`); }
                if (Is.func(step))
                {
                    const boundGetter: BoundGetter = { ...step(...args), getter: step };
                    return boundGetter;
                }
                if (args.length > 0) { throw new Error(`Step '${name}' does not accept arguments`); }
                return step;
            }

            public nameOf(step: Step): string
            {
                const target = isBoundGetter(step) ? step.getter : step;
                for (const name in this._steps)
                {
                    if (this._steps[name] === target) { return name; }
                }
                throw new Error(`Step not found: ${JSON.stringify(step)}`);
            }
        }
    }
}