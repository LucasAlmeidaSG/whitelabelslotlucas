namespace RS.Testing.Integration
{
    export class TestSuite
    {
        private readonly _steps = new Step.Registry();
        private readonly _testCases = new TestCase.Registry();

        public get testCases() { return this._testCases.names; }
        public get steps() { return this._steps.names; }

        public run(testName: string, context: TestContext): PromiseLike<TestResult>
        {
            const testCase = this._testCases.get(testName);
            return testCase.run(context);
        }

        public defineCase(name: string)
        {
            const testCase = new TestCase(name, this._steps);
            this._testCases.register(name, testCase);
            return testCase;
        }

        public getParameterNames(testName: string): ReadonlyArray<string>
        {
            const testCase = this._testCases.get(testName);
            return testCase.parameterNames;
        }

        public defineStep(name: string, getter: Step.Getter): void;
        public defineStep(name: string, message: string, fn: Step.RunFunc): void;
        public defineStep(name: string, p1: string | Step.Getter, fn?: Step.RunFunc): void
        {
            this._steps.register(name, Is.func(p1) ? p1 : { message: p1, run: fn });
        }
    }
}