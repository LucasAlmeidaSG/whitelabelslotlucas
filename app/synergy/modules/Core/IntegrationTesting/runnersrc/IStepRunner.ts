namespace RS.Testing.Integration
{
    export interface IStepRunner
    {
        /** Adds a step. */
        addStep(name: string, ...args: ResolvableStepArgument[]): this;
    }

    export type ResolvableStepArgument = string | Parameter;
}