namespace RS.Testing.Integration
{
    export interface ITestRunner
    {
        shutdownCallback: () => void;
        readonly tests: ReadonlyArray<string>;
        readonly isDisposed: boolean;
        run(test: string, browser: string, url: string): PromiseLike<TestResult>;
        load(file: string): PromiseLike<void>;
        dispose(): void;
        getParameterNames(test: string): ReadonlyArray<string>;
    }

    interface IProcessCommunicator
    {
        send(message: string): PromiseLike<void>;
        message(): Promise<string>;
    }

    interface Test
    {
        name: string;
        parameters: ReadonlyArray<string>
    }

    const logContext = "TestRunner.wrapper";

    export class RunnerLostError extends BaseError
    {
        constructor(message: string) { super(message); }
    }

    abstract class BaseTestRunner implements ITestRunner
    {
        public get tests(): ReadonlyArray<string> { return this._tests.map((x) => x.name); }

        public abstract readonly isDisposed: boolean;
        public shutdownCallback: Action;

        protected abstract communicator: IProcessCommunicator;

        /** Test names from the previous fetch. */
        private _tests: ReadonlyArray<Test>;

        /** Whether or not we are communicating currently. */
        private _isCommunicating = false;
        
        /** To be executed after tests updated. */
        private _onTestsUpdated: (err?: any) => void;

        public abstract dispose(): void;

        public async load(file: string)
        {
            this.throwIfCannotCommunicate();

            const update = new Promise((resolve, reject) =>
            {
                this._onTestsUpdated = (err: any) =>
                {
                    if (err) { reject(err); return; }
                    resolve();
                };
            });

            await this.communicator.send(`load ${file}`);

            await update;
            this._onTestsUpdated = null;
        }

        public async run(test: string, browser: string, url: string)
        {
            this.throwIfCannotCommunicate();
            this.throwIfTestInvalid(test);

            const resultData = await this.requestData(`run ${browser} ${url} ${test}`);
            return TestResult.deserialise(resultData);
        }

        public getParameterNames(testName: string)
        {
            const test = this.getTest(testName);
            return test.parameters;
        }

        protected async handleTestsUpdated()
        {
            try
            {
                Log.debug("Updating test list", logContext);
                const validTestNames = await this.requestDataArray("list");
                const tests = [];
                for (const test of validTestNames)
                {
                    const validParamNames = await this.requestDataArray(`params ${test}`);
                    const entry: Test = { name: test, parameters: validParamNames };
                    tests.push(entry);
                }
                this._tests = tests;
                if (this._onTestsUpdated) { this._onTestsUpdated(); }
            }
            catch (err)
            {
                if (this._onTestsUpdated) { this._onTestsUpdated(err); }
            }
        }

        protected async requestData(message: string): Promise<string>
        {
            this.communicationStart();
            const responseReceipt = this.communicator.message();
            this.communicator.send(message);
            const response = await responseReceipt;
            if (response === "shutdown") { throw new RunnerLostError(`Server shut down before data response`); }
            this.communicationEnd();
            return response;
        }

        protected async requestDataArray(message: string): Promise<string[]>
        {
            const responseText = await this.requestData(message);
            const responseData = responseText.split("\n").filter((x) => x != "");
            return responseData;
        }

        protected handleCommunicatorShutdown()
        {
            Log.debug("Test runner instance lost", logContext);
            if (this.shutdownCallback) { this.shutdownCallback(); }
        }

        private getTest(name: string): Test
        {
            for (const test of this._tests)
            {
                if (test.name === name) { return test; }
            }
            throw new Error(`Test '${name}' is not a valid test`);
        }

        // Communication state stuff

        private communicationStart()
        {
            this.throwIfCannotCommunicate();
            this._isCommunicating = true;
        }

        private communicationEnd()
        {
            this._isCommunicating = false;
        }

        // Throws

        private throwIfCannotCommunicate()
        {
            if (!this.communicator) { throw new Error("TestRunner not connected"); }
            if (this._isCommunicating) { throw new Error(`Already communicating with test runner`); }
        }

        private throwIfTestInvalid(name: string)
        {
            this.getTest(name);
        }
    }

    export class SandboxedTestRunner extends BaseTestRunner
    {
        protected communicator: Sandbox;

        public get isDisposed() { return this.communicator == null; }

        public dispose()
        {
            if (this.isDisposed) { return; }
            this.communicator.dispose();
            this.communicator = null;
        }

        public async start(path: string)
        {
            const communicator = this.communicator = new Sandbox(path);

            const ready = communicator.event("ready");
            communicator.start();
            await ready;
            await this.handleTestsUpdated();

            communicator.on("loaded", () => this.handleTestsUpdated());
            communicator.on("shutdown", () => this.handleCommunicatorShutdown());
        }
    }

    export class IPCTestRunner extends BaseTestRunner
    {
        protected communicator: IPC.Client;

        public get isDisposed() { return this.communicator == null; }

        public dispose()
        {
            if (this.isDisposed) { return; }
            this.communicator.dispose();
            this.communicator = null;
        }

        public async connect()
        {
            const communicator = this.communicator = new IPC.Client("testrunner");

            await communicator.connect();
            await this.handleTestsUpdated();

            communicator.on("loaded", () => this.handleTestsUpdated());
            communicator.on("shutdown", () => this.handleCommunicatorShutdown());
        }

        public async kill()
        {
            await this.communicator.send("exit");
        }
    }
}