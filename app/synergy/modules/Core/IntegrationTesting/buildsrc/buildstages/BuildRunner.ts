namespace RS.Testing.Integration.BuildStages
{
    export class BuildRunner extends RS.BuildStage.Base
    {
        public async execute()
        {
            const self = Module.getModule("Core.IntegrationTesting");
            const dependencies = self.gatherDependencySet();

            const existingUnit = self.getCompilationUnit("runner");
            if (existingUnit && !existingUnit.recompileNeeded) { return { workDone: false, errorCount: 0 }; }

            // Compile our runner.
            const unit = existingUnit || self.addCompilationUnit("runner", "runnersrc");
            Module.CompilationDependencies.addGulp(unit, self);
            Module.CompilationDependencies.addModule(unit, self, Module.CompilationUnits.BuildSource);
            Module.CompilationDependencies.addThirdParty(unit, self);
            const recompiled = await Build.CompilationUnit.executeUnit(unit);

            const dir = Path.combine(self.path, "build");
            const file = Path.combine(dir, "runner.js");
            if (!recompiled) { return { workDone: false, errorCount: 0 }; }
            await self.saveChecksums();
            return { workDone: true, errorCount: 0 };
        }

        public async executeModule(module: Module.Base): Promise<BuildStage.Result> { throw new Error("Unused"); }
    }

    export const buildRunner = new BuildRunner();
    BuildStage.register({}, buildRunner);
}