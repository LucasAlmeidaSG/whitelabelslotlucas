/// <reference path="BuildRunner.ts"/>

namespace RS.Testing.Integration.BuildStages
{
    const defConf: TSConfig =
    {
        compilerOptions:
        {
            target: "ES5",
            lib: [ "DOM", "ES2015.Promise", "ES5" ],
            rootDir: ".",
            outFile: "../build/gametestsrc.js",
            sourceMap: true,
            inlineSources: true,
            inlineSourceMap: false,
            experimentalDecorators: true,
            declaration: true,
            declarationMap: true,
            typeRoots: [],
            noEmitHelpers: true
        }
    };

    export class BuildTests extends RS.BuildStage.Base
    {
        public async executeModule(module: Module.Base)
        {
            const unit = module.getCompilationUnit("gametests") || module.addCompilationUnit("gametests", "gametests", defConf);

            const self = Module.getModule("Core.IntegrationTesting");
            Module.CompilationDependencies.addModule(unit, self, "runner");

            let recompiled: boolean;
            try
            {
                recompiled = await Build.CompilationUnit.executeUnit(unit);
            }
            catch (error)
            {
                return { workDone: false, errorCount: 1 };
            }

            return { workDone: recompiled, errorCount: 0 }
        }
    }

    export const buildTests = new BuildTests();
    BuildStage.register({ after: [buildRunner] }, buildTests);
}