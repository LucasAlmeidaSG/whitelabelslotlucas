namespace RS.Testing.Integration.AssembleStages
{
    export class AssembleTests extends AssembleStage.Base
    {
        private _codeSources: Assembler.CodeSource[];

        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            this._codeSources = [];
            await super.execute(settings, moduleList);
            if (this._codeSources.length === 0) { return; }

            await Assembler.assembleJS(settings.outputDir, "app.spec", this._codeSources,
            {
                withHelpers: false,
                withSourcemaps: false,
                minify: false
            });

            // Post-process assembled file
            const file = Path.combine(settings.outputDir, "app.spec.js");
            const data = await FileSystem.readFile(file);
            const newData = data.replace(/^var RS;/gm, "//var RS;");
            await FileSystem.writeFile(file, newData);
        }

        public async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            const unit = module.getCompilationUnit("gametests");
            if (!unit || !this.isUnitValid(unit)) { return; }
            await Assembler.CodeSources.addUnit(this._codeSources, unit);
        }

        private isUnitValid(unit: Build.CompilationUnit)
        {
            return unit.validityState === Build.CompilationUnit.ValidState.Compiled || (!unit.recompileNeeded && unit.validityState === Build.CompilationUnit.ValidState.Initialised);
        }
    }

    export const assembleTests = new AssembleTests();
    AssembleStage.register({}, assembleTests);
}