namespace RS.Testing.Integration.AssembleStages
{
    export class AssembleRunner extends AssembleStage.Base
    {
        private _codeSources: Assembler.CodeSource[];

        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            const self = Module.getModule("Core.IntegrationTesting");
            const dependencies = self.gatherDependencySet();

            const unit = self.getCompilationUnit("runner");
            const codeSources: Assembler.CodeSource[] = [];
            await Assembler.CodeSources.addGulp(codeSources);
            await Assembler.CodeSources.addModules(codeSources, dependencies.data, Module.CompilationUnits.BuildSource);
            await Assembler.CodeSources.addUnit(codeSources, unit);
            await Assembler.assembleJS(settings.outputDir, "runner", codeSources, { withHelpers: true, withSourcemaps: true, minify: false });

            // Post-process assembled file
            const file = Path.combine(settings.outputDir, "runner.js");
            const data = await FileSystem.readFile(file);
            const newData = data.replace(/^var RS = module/gm, "//var RS = module");
            await FileSystem.writeFile(file, newData);
        }

        public async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            const unit = module.getCompilationUnit("gametests");
            if (!unit || !this.isUnitValid(unit)) { return; }
            await Assembler.CodeSources.addUnit(this._codeSources, unit);
        }

        private isUnitValid(unit: Build.CompilationUnit)
        {
            return unit.validityState === Build.CompilationUnit.ValidState.Compiled || (!unit.recompileNeeded && unit.validityState === Build.CompilationUnit.ValidState.Initialised);
        }
    }

    export const assembleRunner = new AssembleRunner();
    AssembleStage.register({}, assembleRunner);
}