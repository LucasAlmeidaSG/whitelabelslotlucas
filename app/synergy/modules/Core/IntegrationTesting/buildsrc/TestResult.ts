namespace RS.Testing.Integration
{
    export enum TestState
    {
        Pass = "pass",
        Failure = "failure",
        Error = "error"
    }

    export type TestPass =
    {
        state: TestState.Pass;
    };

    export type TestFailure =
    {
        state: TestState.Failure;
        reason: string;
    };

    export type TestError =
    {
        state: TestState.Error;
        error: string;
    };

    export type TestResult = TestPass | TestFailure | TestError;

    export namespace TestResult
    {
        export function serialise(result: TestResult): string
        {
            switch (result.state)
            {
                case TestState.Pass: return "pass";
                case TestState.Failure: return `fail ${result.reason}`;
                case TestState.Error: return `error ${result.error}`;
                default: throw new Error(`Invalid test result: ${JSON.stringify(result)}`);
            }
        }

        export function deserialise(data: string): TestResult
        {
            const firstSpace = data.indexOf(" ");
            const state = firstSpace === -1 ? data : data.slice(0, firstSpace);
            switch (state)
            {
                case "pass": return { state: TestState.Pass };
                case "fail": return { state: TestState.Failure, reason: data.slice(firstSpace + 1) };
                case "error": return { state: TestState.Error, error: data.slice(firstSpace + 1) };
                default: throw new Error(`Invalid test result data: ${data}`);
            }
        }
    }
}