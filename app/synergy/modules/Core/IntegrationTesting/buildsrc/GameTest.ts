namespace RS.Tasks
{
    const maxInstances = 1;
    const envTests = new EnvArg("TESTS", "");
    const envBrowsers = new EnvArg("BROWSERS", "chrome");

    export const gameTest = Build.task({ name: "gametest", requires: [ Tasks.assemble ] }, async () =>
    {
        const testsToRun = envTests.value.split(",");
        const testScripts = (await FileSystem.readDirectory("tests", false))
                .filter((file) => Path.extension(file) === ".js")
                .map((file) => Path.combine("tests", file));

        const appFile = Path.combine("build", "app.spec.js");
        if (await FileSystem.fileExists(appFile))
        {
            testScripts.unshift(appFile);
        }

        await doGameTests(testScripts, testsToRun);
    });

    export async function doGameTests(files: string[], tests: string[])
    {
        const runCount = tests.length;

        const self = Module.getModule("Core.IntegrationTesting");
        const dir = Path.combine(self.path, "build");

        // Let's use the same port as karma
        const ip = "127.0.0.1";
        const port = 9876;

        const config = await Config.get();
        const rootPath = config.assemblePath;
        const runnerPath = Path.combine(config.assemblePath, "runner.js");

        let server: WebServer;
        try
        {
            // Server bit.
            Log.info(`Initialising server on port ${port}`);
            server = new WebServer(ip, port, rootPath);
            await server.open();
            Log.info("Server ready");

            const url = new URL(`http://${ip}`);
            url.port = port;
            url.params.set("selenium", "true");

            const instances = Math.min(maxInstances, runCount);
            const batchSize = Math.ceil(runCount / instances);

            Log.info(`Starting ${instances} test runner instance${instances === 1 ? "" : "s"}`);

            const promises: PromiseLike<boolean>[] = [];
            for (let i = 0; i < instances; i++)
            {
                const start = i * batchSize, end = start + batchSize;
                const testBatch = tests.slice(start, end);
                promises.push(runTests(runnerPath, files, `${url}`, testBatch));
            }
            const results = await Promise.all(promises);

            const failCount = results.reduce((currFailCount, pass) => pass ? currFailCount : currFailCount + 1, 0);
            if (failCount > 0)
            {
                throw new Error(`${failCount} tests failed`);
            }
        }
        finally
        {
            await server.close();
        }
    }

    async function runTests(runnerPath: string, scriptFiles: string[], url: string, tests: string[]): Promise<boolean>
    {
        const testRunner = new RS.Testing.Integration.SandboxedTestRunner();
        await testRunner.start(runnerPath);

        for (const testScript of scriptFiles)
        {
            await testRunner.load(testScript);
        }

        const browsers = envBrowsers.value.split(",");

        let pass = true;
        for (const test of tests)
        {
            for (const browser of browsers)
            {
                const result = await testRunner.run(test, browser, url);
                pass = pass && result.state === RS.Testing.Integration.TestState.Pass;
            }
        }

        testRunner.dispose();
        return pass;
    }
}