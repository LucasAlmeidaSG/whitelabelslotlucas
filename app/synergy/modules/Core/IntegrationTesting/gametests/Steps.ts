defineStep("Screenshot", function (destPath: string)
{
    if (!RS.Is.string(destPath)) { throw new TypeError(`${destPath} is not a string`); }
    return {
        message: `Take a screenshot and save it to ${destPath}`,
        run: async ({ driver }) =>
        {
            const myBeautifulPicture = await driver.takeScreenshot();
            const base64Data = Buffer.from(myBeautifulPicture.replace(/^data:image\/png;base64,/, ""), "base64");
            await RS.FileSystem.writeFile(destPath, base64Data);
        }
    };
});

defineStep("Resize", function (newW: string, newH: string)
{
    if (!RS.Is.string(newW)) { throw new TypeError(`${newW} is not a string`); }
    if (!RS.Is.string(newH)) { throw new TypeError(`${newH} is not a string`); }

    const w = RS.Read.integer(newW), h = RS.Read.integer(newH);

    return {
        message: `Resize the window to ${w} x ${h}`,
        run: async ({ driver }) =>
        {
            await driver.manage().window()
                .setSize(w, h);
        }
    };
});

defineStep("Wait", function (time:  string)
{
    const timeNum = RS.Read.number(time);
    if (timeNum == null) { throw new Error(`Invalid time ${time}`); }

    return {
        message: `Wait ${time} seconds`,
        run: ({ driver }) => new Promise<void>((resolve, reject) => setTimeout(resolve, timeNum * 1000))
    };
});

defineStep("Rotate", "Rotate the window", async function ({ driver })
{
    const window = driver.manage().window();
    const curRect = await window.getRect();
    await window.setRect({ x: curRect.x, y: curRect.y, height: curRect.width, width: curRect.height });
});

defineStep("Note", function (valueName: string, paramName: string)
{
    return {
        message: `Take a note of '${valueName}' and call it '${paramName}'`,
        run: async ({ driver, vars }) =>
        {
            const elem = await driver.findElement(selenium.By.name(valueName))

            const value = await elem.getAttribute("data-selenium-currency");
            vars[paramName] = value;
        }
    };
});

defineStep("Click", function (buttonName: string)
{
    return {
        message: `Click ${buttonName}`,
        run: async ({ driver }) =>
        {
            await Selenium.clickElement(driver, buttonName);
        }
    };
})

function normaliseOperator(opName: string): string
{
    switch (opName.toLowerCase())
    {
        case "*":
        case "multiplied by":
            return "*";
        case "/":
        case "divided by":
            return "/";
        case "+":
        case "plus":
            return "+";
        case "-":
        case "minus":
            return "-";
    }
    throw new Error(`Invalid operation '${opName}'`);
}

function getOperationFunc(operation: string): (a: number, b: number) => number
{
    switch (operation)
    {
        case "*":
            return (a, b) => a * b;
        case "/":
            return (a, b) => a / b;
        case "+":
            return (a, b) => a + b;
        case "-":
            return (a, b) => a - b;
    }
    throw new Error(`Invalid operation '${operation}'`);
}

function getComparisonFunc(compName: string): (a: number, b: number) => boolean
{
    switch (compName.toLowerCase())
    {
        case "equals":
            return (a, b) => a === b;
    }
}

async function getValue({ driver, vars, params }: RS.Testing.Integration.TestContext, value: string): Promise<number>
{
    const numericVal = RS.Read.number(value);
    if (numericVal != null) { return numericVal; }

    if (value in vars) { return RS.Read.number(vars[value]); }

    if (value in params) { return RS.Read.number(params[value]); }

    const elem = await driver.findElement(selenium.By.name(value));
    if (elem) { return RS.Read.number(await elem.getAttribute("data-selenium-currency")); }

    throw new Error(`Could not read value '${value}'`);
}

function normaliseExpression(expression: ReadonlyArray<string>): string[]
{
    return expression.map((seg, i) => (i % 2 === 1) ? normaliseOperator(seg) : seg);
}

function toReversePolishNotation(expression: string[]): (string | ((a: number, b: number) => number))[]
{
    const precedence = { "/": 1, "*": 1, "+": 0, "-": 0 };
    // Convert to RPN
    const operators: string[] = [];
    const output: (string | ((a: number, b: number) => number))[] = [];

    for (let i = 0; i < expression.length; i++)
    {
        const segment = expression[i];
        const isOperator = (i % 2 === 1);
        if (isOperator)
        {
            while (operators.length > 0 && precedence[operators[operators.length - 1]] >= precedence[segment])
            {
                const thisOp = operators.pop();
                const opFunc = getOperationFunc(thisOp);
                output.push(opFunc);
            }
            operators.push(segment);
        }
        else
        {
            output.push(segment);
        }
    }

    if (operators.length > 0)
    {
        output.push(getOperationFunc(operators[0]));
    }

    return output;
}

async function execute(context: RS.Testing.Integration.TestContext, expr: (string | ((a: number, b: number) => number))[]): Promise<number>
{
    const stack: number[] = [];
    for (let i = 0; i < expr.length; ++i)
    {
        const token = expr[i];
        if (RS.Is.string(token))
        {
            const numValue = await getValue(context, token);
            stack.push(numValue);
        }
        else
        {
            const b = stack.pop();
            const a = stack.pop();
            stack.push(token(a, b));
        }
    }
    return stack[0];
}

defineStep("Expect", function (valueName: string, comparison: string, ...expectedValue: string[])
{
    const expr = normaliseExpression(expectedValue);
    const rpn = toReversePolishNotation(expr);
    return {
        message: `Expect ${valueName} ${comparison.toLowerCase()} ${expr.join(" ")}`,
        run: async (context) =>
        {
            const value = await getValue(context, valueName);
            const result = await execute(context, rpn);

            const compFunc = getComparisonFunc(comparison);
            if (!compFunc(value, result))
            {
                throw new Error(`Expected ${value} ${comparison} ${result}`);
            }
        }
    };
});

// EXPECT BALANCE EQUALS INITIALBALANCE MINUS STAKE PLUS WIN