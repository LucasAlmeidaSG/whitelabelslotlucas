namespace SGI.Selenium
{
    /** Enum for the keys of a selenium description as returned by a describer's seleniumDescribe() method. */
    export enum DescriptionEntry
    {
        Key,
        Value
    }

    /**
     * @interface ISeleniumDescriber
     *
     * @description Interface that components can implement to add custom attributes to their div in the SeleniumDivTree
     *
     * E.g. [['myWidth', '100'], ['myHeight', this.height]] would generate two new attributes for the div
     */
    export interface ISeleniumDescriber extends ISeleniumComponent
    {
        /**
         * List of attibute/value pairs to add to the div e.g. ['width', '100']
         */
        seleniumDescribe(): ReadonlyArray<[string, string | number]>;
    }
}

namespace RS.Is
{
    /**
     * Gets if the value implements SGI.Selenium.ISeleniumDescriber.
     * @param value
     */
    export function seleniumDescriber(value: any): value is SGI.Selenium.ISeleniumDescriber
    {
        return Is.seleniumComponent(value) && Is.func((value as SGI.Selenium.ISeleniumDescriber).seleniumDescribe);
    }
}