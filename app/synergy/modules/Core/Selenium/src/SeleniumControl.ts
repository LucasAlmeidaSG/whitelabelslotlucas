namespace SGI.Selenium
{
    /**
     * @class SeleniumControl
     * 
     * @description Custom state control that can be used within selenium to read the current game state
     */
    export class SeleniumControl
    {
		/**
         * Html element to add the control to
         */
        protected static _root: HTMLElement;
        /**
         * Container element
         */
        protected static _textDiv: HTMLElement;
        /**
         * Text element
         */
        protected static _textElement: HTMLElement;
        /**
         * Creates the element and adds it to the DOM
         * 
         * @param element Html element to add the control to
         * @param width Width to draw the element
         * @param height Height to draw the element
         */
        public static createConsole(element: HTMLElement, width: number, height: number): HTMLElement
        {
            this._root = element;
            this._textDiv = document.createElement("div");
            //this._textDiv.style.display = 'none';
            this._textDiv.id = "selenium-control";
            this._textDiv.style.width = width.toString() + "px";
            this._textDiv.style.height = height.toString() + "px";
            this._textDiv.style.zIndex = "1";
            this._textDiv.style.position = "absolute";
            this._textDiv.style.bottom = "10pt";
            this._textDiv.style.left = "10pt";
            this._textDiv.style.visibility = "visible";

            this._textElement = document.createElement("TEXTAREA");
            this._textElement.setAttribute("value", "");
            this._textElement.setAttribute("id", "rs-selenium-console");
            this._textElement.setAttribute("name", "Test Name");
            this._textElement.setAttribute("style", "width:100%; height:100%");

            this._textDiv.appendChild(this._textElement);
            this._root.appendChild(this._textDiv);

            return this._textDiv;
        }
        /**
         * Set's the current state of the game
         */
        public static setText(txt: string): void
        {
            if (this._textElement)
            {
                this._textElement.textContent = txt;
            }
        }
    }
}