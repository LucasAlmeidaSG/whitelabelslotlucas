namespace SGI.Selenium
{
    /**
     * @interface ISeleniumComponent
     *
     * @description Interface that any class can implement to override the default div ID set by the SeleniumDivTree
     */
    export interface ISeleniumComponent
    {
        /**
         * A name for this object in the Selenium DOM tree.
         * If undefined, the object will not be represented.
         * TODO: rename to seleniumName
         */
        readonly seleniumId: string;

        /** @internal */
        seleniumElement?: HTMLElement;
    }
}

namespace RS.Is
{
    /**
     * Gets if the value implements SGI.Selenium.ISeleniumComponent.
     * @param value
     */
    export function seleniumComponent(value: any): value is SGI.Selenium.ISeleniumComponent
    {
        return value != null && Is.object(value) && Is.string(value.seleniumId);
    }
}