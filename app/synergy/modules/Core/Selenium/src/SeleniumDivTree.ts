/// <reference path="Module.ts"/>

namespace SGI.Selenium
{
    interface SeleniumElement
    {
        htmlElement: HTMLElement;
        displayObject: RS.Rendering.IDisplayObject;
        deleted: boolean;
        referenced: boolean;
    }

    /**
     * @class SeleniumDivTree
     *
     * @description Recursively creates a list of div elements that describe a display object and all its children
     *
     * This can be used in conjunction with selenium scripts or IDE to find canvas elements and their attributes to automate testing
     */
    export class SeleniumDivTree implements RS.IDisposable
    {
        /**
         * The html element to add the divs to, normally "game-canvas-div"
         */
        protected readonly _root: HTMLElement;

        /**
         * Root div that holds all the display object divs
         */
        protected readonly _container: HTMLElement;

        /**
         * Display object that we want to represent
         */
        protected readonly _rootDisplay: RS.Rendering.IContainer;

        private readonly _elements: SeleniumElement[] = [];
        private readonly _freeIDs: number[] = [];

        /** Current DOM element branch. Element 0 is always this._container. */
        private readonly _branch: HTMLElement[];
        private _branchHead: number;

        private _isDisposed: boolean;
        public get isDisposed() { return this._isDisposed; }

        /**
         * Creates a new SeleniumDivTree
         *
         * @param htmlParent The html element to add the divs to, normally "game-canvas-div"
         * @param display Display object that we want to represent
         */
        constructor(htmlParent: HTMLElement, display: RS.Rendering.IContainer)
        {
            this._root = htmlParent;
            this._rootDisplay = display;

            this._container = document.createElement("div");
            this._container.setAttribute("id", "selenium-div-tree");
            // Set numeric version for easy differentiation.
            this.setDataAttribute(this._container, "version", `${domVersion.major}`);
            // Set more advanced major.minor.revision version for more sophisticated differentation.
            this.setDataAttribute(this._container, "full-version", RS.Version.toString(domVersion));
            this._root.appendChild(this._container);

            this._branch = [this._container];
            this._branchHead = 0;
        }

        /**
         * Recreates the div tree
         *
         * @param xScale The current scale of the canvas container
         * @param yScale The current scale of the canvas container
         */
        public draw(xScale: number, yScale: number): void
        {
            this.markAllUnreferenced();
            this.describe(this._rootDisplay, this._container, xScale, yScale);
            this.cleanUpUnreferenced();
        }

        /**
         * Tears down the div tree
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._root.removeChild(this._container);
            this._elements.length = 0;
            this._isDisposed = true;
        }

        protected markAllUnreferenced()
        {
            for (const element of this._elements)
            {
                element.referenced = false;
            }
        }

        protected cleanUpUnreferenced()
        {
            for (let id = 0, elements = this._elements; id < elements.length; ++id)
            {
                const element = elements[id];
                if (!element.referenced) { this.removeElement(id); }
            }
        }

        /**
         * Creates a div for the given display object
         *
         * @param obj The display object to create a div for
         * @param htmlParent Parent div to add this one to
         * @param xScale The current scale of the canvas container
         * @param yScale The current scale of the canvas container
         */
        protected describe(obj: RS.Rendering.IDisplayObject, htmlParent: HTMLElement | null, xScale: number, yScale: number): void
        {
            if (!obj.visible || !obj.alpha) { return; }

            if (!RS.Is.seleniumComponent(obj) || obj.seleniumId == null)
            {
                if (obj instanceof RS.Rendering.Container)
                {
                    for (const child of obj.children)
                    {
                        this.describe(child, null, xScale, yScale);
                    }
                }
                return;
            }

            const elem = this.getSeleniumElement(obj, true);

            this.setDisplayObjectAttributes(elem, obj, xScale, yScale);
            if (RS.Is.seleniumDescriber(obj))
            {
                const dataPairs = obj.seleniumDescribe();
                dataPairs.forEach(([key, value]) => this.setDataAttribute(elem, key, `${value}`));
            }

            if (obj instanceof RS.Rendering.Container)
            {
                this._branch[++this._branchHead] = elem;

                for (const child of obj.children)
                {
                    this.describe(child, elem, xScale, yScale);
                }

                --this._branchHead;
            }
        }

        protected getSeleniumElement(obj: RS.Rendering.IDisplayObject & ISeleniumComponent, markReferenced = false): HTMLElement
        {
            const htmlParent = this._branch[this._branchHead];

            const existing = this.findElement((e) => e.displayObject === obj);
            if (existing)
            {
                if (existing.htmlElement.parentElement !== htmlParent) { htmlParent.appendChild(existing.htmlElement); }
                if (markReferenced) { existing.referenced = true; }
                return existing.htmlElement;
            }

            const id = this.getNextElementID();

            const htmlElement = document.createElement("div");
            htmlElement.setAttribute("name", obj.seleniumId);
            this.setDataAttribute(htmlElement, "id", `${id}`);

            htmlParent.appendChild(htmlElement);

            htmlElement.style.width = "1px";
            htmlElement.style.height = "1px";

            htmlElement.onclick = () =>
            {
                obj.onDown.publish({
                    target: obj,
                    globalPosition: obj.toGlobal({ x: obj.x, y: obj.y }),
                    localPosition: { x: obj.x, y: obj.y },
                    pointerID: 0
                });
                obj.onUp.publish({
                    target: obj,
                    globalPosition: obj.toGlobal({ x: obj.x, y: obj.y }),
                    localPosition: { x: obj.x, y: obj.y },
                    pointerID: 0
                });
                obj.onClicked.publish({
                    target: obj,
                    globalPosition: obj.toGlobal({ x: obj.x, y: obj.y }),
                    localPosition: { x: obj.x, y: obj.y },
                    pointerID: 0
                });
            };

            obj.seleniumElement = htmlElement;
            this._elements[id] = { htmlElement, displayObject: obj, deleted: false, referenced: markReferenced };
            return htmlElement;
        }

        protected findElement(predicate: (element: SeleniumElement) => boolean): SeleniumElement | null
        {
            return RS.Find.matching(this._elements, (elem) => !elem.deleted && predicate(elem));
        }

        protected getNextElementID(): number
        {
            if (this._freeIDs.length === 0) { return this._elements.length; }
            return this._freeIDs.pop();
        }

        protected removeElement(id: number): SeleniumElement | null
        {
            const elem = this._elements[id];
            if (!elem || elem.deleted) { return null; }
            elem.htmlElement.remove();
            this._freeIDs.push(id);
            elem.deleted = true;
            return elem;
        }

        protected setDisplayObjectAttributes(elem: Element, obj: RS.Rendering.IDisplayObject, xScale: number, yScale: number)
        {
            const posScale = RS.Rendering.ScaleManager.getPositionScale();
            const posScaleX = posScale / xScale;
            const posScaleY = posScale / yScale;

            const bounds = obj.bounds;
            const props =
            {
                // Get absolute positions
                "x": bounds.x * posScaleX,
                "y": bounds.y * posScaleY,

                "center-x": (bounds.x + bounds.w / 2) * posScaleX,
                "center-y": (bounds.y + bounds.h / 2) * posScaleY,

                "width": bounds.w / xScale,
                "height": bounds.h / yScale,

                // Rotation - todo: should be world rotation
                "rotation": obj.rotation,

                // Visible
                "alpha": obj.alpha
            };

            for (const key in props)
            {
                const value = props[key];
                this.setDataAttribute(elem, key, `${value}`);
            }
        }

        protected setDataAttribute(elem: Element, name: string, value: string)
        {
            elem.setAttribute(`data-selenium-${name}`, value);
        }
    }
}