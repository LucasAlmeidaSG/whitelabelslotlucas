/**
 * # Selenium Automated Testing
 *
 * This namespace contains classes and interfaces for creating html elements to describe the canvas's display list to use with Selenium.
 *
 * Selenium provides a way to automate browser testing for javascript applications. This tooling can be used by QA to save time and automate regression testing.
 *
 * Visit http://www.seleniumhq.org/ for more information on Selenium
 *
 * A SeleniumDivTree can be made for a display object and updated periodically to provide Selenium with a DOM structure for the canvas
 *
 * @example
 * ```typescript
 *
 * protected createSelenium(): void { *
 *     var div: HTMLElement = document.getElementById('game-canvas-div');
 *     this._seleniumDivTree = new selenium.SeleniumDivTree(div, this._stage);
 *     new TimelineMax({
 *         repeat: -1,
 *         repeatDelay: 1,
 *         delay: 1,
 *         onRepeat: this.onSeleniumUpdate,
 *         onRepeatScope: this
 *     });
 * }
 *
 * protected onSeleniumUpdate(): void {
 *     var scaleX:number = this._responsiveDiv.getResponsiveScaleX();
 *     var scaleY:number = this._responsiveDiv.getResponsiveScaleY();
 *     this._seleniumDivTree.draw(scaleX, scaleY);
 * }
 * ```
 *
 * Any display object can also implement the ISeleniumComponent to override the div's id attribute to aid QA when making scripts:
 *
 * @example
 * ```typescript
 *
 * namespace game {
 *     export class GameButton extends components.Button implements selenium.ISeleniumComponent {
 *         public seleniumId: string;
 *     }
 * }
 *
 * //Set the custom id in game
 * this._playBtn = new GameButton();
 * this._playBtn.seleniumId = "PlayButton";
 * ```
 *
 * Any display object can also implement the ISeleniumDescriber to add additional attributes to the div element to aid QA
 *
 * @example
 * ```typescript
 *
 * namespace game {
 *     export class GameButton extends components.Button implements selenium.ISeleniumComponent, selenium.ISeleniumDescriber {
 *         public seleniumId: string;
 *         public currentState: string;
 *
 *         constructor()
 *         {
 *             super();
 *             this.currentState = "UP";
 *         }
 *
 *         public seleniumDescribe(): string[][]
 *         {
 *             return [
 *                 ['state', this.currentState]
 *             ];
 *         }
 *     }
 * }
 * ```
 *
 * The SeleniumControl is a custom element that displays the current state of the game that QA can use with selenium
 *
 * @example
 * ```typescript
 *
 * //Create the control
 * selenium.SeleniumControl.createConsole(this._partnerAdapter.getGameDomElement(), 500, 25);
 * //Update the control at different stages of the game
 * selenium.SeleniumControl.setText("Game Loading");
 * selenium.SeleniumControl.setText("Game Loaded");
 * selenium.SeleniumControl.setText("Waiting for End Game");
 * selenium.SeleniumControl.setText("End Game Received");
 * ```
 */
namespace SGI.Selenium
{
    /**
     * Full version of the DOM structure for tools to differentiate against.
     * Breaking DOM changes should increment the first number.
     * New features should increment the second number.
     * Fixes should increment the third number.
     */
    export const domVersion = RS.Version(2, 0, 0);
}