namespace RS.Slots.Paytable.Plugins
{
    /**
     * Hide or show turbo text
     */
    export class TurboPlugin extends RS.ExternalHelp.VisitorPlugin
    {
        private readonly _showTurbo: boolean;

        public constructor(page: RS.ExternalHelp.IPage)
        {
            super(page);
            
            this._showTurbo = RS.URL.getParameterAsBool("turbo", false);
        }

        public onElementLoaded(element: HTMLElement)
        {
            const isTurbo = element.id === "turbo";
            if (isTurbo && !this._showTurbo)
            {
                RS.ExternalHelp.hideElement(element);
            }
        }
    }

    RS.ExternalHelp.pluginRegistry.register(TurboPlugin);
}