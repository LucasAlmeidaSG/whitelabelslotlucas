namespace RS.Paytable
{
    export interface IPageFactory<TSettings extends Pages.Base.Settings, TRuntimeData extends Pages.Base.RuntimeData>
    {
        create(overrideSettings: Partial<TSettings>, runtimeData: TRuntimeData): Pages.Base<TSettings, TRuntimeData> | Pages.Base<TSettings, TRuntimeData>[];
        create(runtimeData: TRuntimeData): Pages.Base<TSettings, TRuntimeData> | Pages.Base<TSettings, TRuntimeData>[];
    }
}
