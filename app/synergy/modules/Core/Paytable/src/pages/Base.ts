namespace RS.Paytable.Pages
{
    /**
     * Base paytable page.
     */
    export abstract class Base<TSettings extends Base.Settings, TRuntimeData extends Base.RuntimeData> extends Flow.BaseElement<TSettings, TRuntimeData>
    {
        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);
        }
    }

    export namespace Base
    {
        export interface DefaultSettings extends Partial<Flow.ElementProperties>
        {

        }

        // tslint:disable-next-line:no-empty-interface
        export interface Settings extends DefaultSettings
        {

        }

        export interface RuntimeData extends DefaultSettings
        {
            configModel: Models.Config;
            stakeModel: Models.Stake;
        }
    }
}
