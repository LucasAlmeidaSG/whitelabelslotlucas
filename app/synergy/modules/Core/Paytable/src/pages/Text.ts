namespace RS.Paytable.Pages
{
    // This string will be replaced by the build process at compile-time
    const gameVersion = "{!GAME_VERSION!}";

    export class TextPage<TSettings extends TextPage.Settings = TextPage.Settings, TRuntimeData extends Base.RuntimeData = Base.RuntimeData> extends Base<TSettings, TRuntimeData>
    {
        protected _contentList: Flow.List;
        protected _initialised: boolean = false;

        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);
        }

        protected onFlowParentChanged(): void
        {
            super.onFlowParentChanged();
            // Only create components on first time added
            if (this._initialised) { return; }
            this._initialised = true;
            this.createComponents();
        }

        protected createComponents()
        {
            // Create components
            this._contentList = Flow.list.create(
                {
                    legacy: false,
                    ...this.settings.pageSettings.listSettings
                }, this);
            this.addElements(this.settings.elements, this._contentList);
        }

        protected addElements(elements: TextPage.Element[], parent: Flow.GenericElement): void
        {
            for (let i = 0; i < elements.length; i++)
            {
                this.addElement(elements[i], parent);
            }
        }

        protected addElement(element: TextPage.Element, parent: Flow.GenericElement)
        {
            const config = this.runtimeData.configModel;
            const stake = this.runtimeData.stakeModel;

            if (!element) { return; }
            if (this.shouldExcludeElement(element)) { return; }

            switch (element.kind)
            {
                case TextPage.ElementKind.Image:
                    this.addImage(element.image, parent);
                    break;
                case TextPage.ElementKind.ImageList:
                    this.addImageList(element.images, element.list, parent);
                    break;
                case TextPage.ElementKind.SingleRTPText:
                    this.addSingleRTPText(element, parent, config);
                    break;
                case TextPage.ElementKind.RTPText:
                    this.addRTPText(element, parent, config);
                    break;
                case TextPage.ElementKind.MaxWinText:
                    this.addMaxWinText(element, parent, config);
                    break;
                case TextPage.ElementKind.MaxBetText:
                    this.addMaxBetText(element, parent, stake);
                    break;
                case TextPage.ElementKind.VersionText:
                    this.addVersionText(element, parent);
                    break;
                case TextPage.ElementKind.YearText:
                    this.addYearText(element, parent);
                    break;
                case TextPage.ElementKind.DefaultText:
                    this.addDefaultText(element, parent);
                    break;
                case TextPage.ElementKind.ComplexText:
                    this.addComplexText(element, parent);
                    break;
                case TextPage.ElementKind.ElementGroup:
                    this.addElementGroup(element, parent);
                    break;
                default:
                    throw new Error("Encountered an unhandled ElementKind");
            }
        }

        protected addElementGroup(element: TextPage.ElementGroup, parent: Flow.GenericElement): void
        {
            const container = Flow.container.create(element.container ? element.container : { ...TextPage.defaultElementGroupSettings.container }, parent);
            Flow.background.create(element.background, container);
            if (element.elementsList)
            {
                const elementsList: Flow.List = Flow.list.create(element.elementsList ? element.elementsList : { ...TextPage.defaultElementGroupSettings.elementsList }, container);
                // Elements will only be added to the list if list settings are provided for that page.
                if (element.elementsList) { this.addElements(element.elements, elementsList); }
            }
            else
            {
                this.addElements(element.elements, container);
            }
        }

        protected addComplexText(element: TextPage.ComplexTextSettings, parent: Flow.GenericElement): void
        {
            let complexText: Flow.List;
            if ((element as TextPage.ComplexTextInterface).settings)
            {
                complexText = Flow.ComplexText.generateComplexText((element as TextPage.ComplexTextInterface).settings);
            }
            else
            {
                const paramElement = element as TextPage.ComplexTextParams;
                complexText = Flow.generateComplexText(this.locale,
                    paramElement.translation,
                    paramElement.substitutions,
                    paramElement.textSettings,
                    paramElement.variableSettings,
                    paramElement.protectedSettings,
                    paramElement.listSettings,
                    paramElement.imageVariableSettings
                );
            }
            parent.addChild(complexText);
        }

        protected addDefaultText(element: TextPage.DefaultTextSettings, parent: Flow.GenericElement): void
        {
            if (element.languageCodeBlackList && element.languageCodeBlackList.indexOf(this.locale.languageCode) != -1) { return; }
            const resolvedText = Localisation.resolveLocalisableString(this.locale, element.text);
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addYearText(element: TextPage.YearTextSettings, parent: Flow.GenericElement): void
        {
            const year = this.settings.pageSettings.forceYear ? this.settings.pageSettings.forceYear : `${(new Date()).getFullYear()}`;
            const resolvedText = element.text.get(this.locale, { year });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addVersionText(element: TextPage.GameVersionTextSettings, parent: Flow.GenericElement): void
        {
            const resolvedText = element.text.get(this.locale, { gameVersion });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addMaxWinText(element: TextPage.MaxWinTextSettings, parent: Flow.GenericElement, config: Models.Config): void
        {
            if (IPlatform.get().maxWinMode == PlatformMaxWinMode.None) { return; }
            const resolvedText = element.text.get(this.locale, { maxWin: this.currencyFormatter.format(config.maxWinAmount, true) });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addMaxBetText(element: TextPage.MaxBetTextSettings, parent: Flow.GenericElement, stake: Models.Stake): void
        {
            const maxStake = Find.highest((stake.betOptions), (opt) => opt.value);
            const resolvedText = element.text.get(this.locale, { maxBet: this.currencyFormatter.format(maxStake.value, true) });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addRTPText(element: TextPage.RtpTextSettings, parent: Flow.GenericElement, config: Models.Config): void
        {
            const minRTP = config.minRTP == null ? -1 : config.minRTP;
            const maxRTP = config.maxRTP == null ? -1 : config.maxRTP;
            const resolvedText = element.text.get(this.locale, { minRTP: minRTP.toFixed(2), maxRTP: maxRTP.toFixed(2) });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addSingleRTPText(element: TextPage.SingleRtpTextSettings, parent: Flow.GenericElement, config: Models.Config): void
        {
            let singleRTP: number = -1;
            if (Is.number(config.minRTP) && Is.number(config.maxRTP) && config.maxRTP !== config.minRTP)
            {
                singleRTP = Math.min(config.maxRTP, config.minRTP);
                Log.warn("SingleRTPText was used, but maxRTP !== minRTP");
            }
            else
            {
                singleRTP = config.minRTP || config.maxRTP;
            }
            const resolvedText = element.text.get(this.locale, { RTP: singleRTP.toFixed(2) });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }

        protected addParagraph(settings: Flow.Label.Settings, text: string, parent: Flow.GenericElement): void
        {
            Flow.label.create(
            {
                ...this.settings.pageSettings.paragraphSettings,
                ...settings,
                text
            }, parent);
        }

        protected addImageList(imagesSettings: Flow.Image.Settings[], listSettings: Flow.List.Settings, parent: Flow.GenericElement): void
        {
            const list = Flow.list.create(listSettings, parent);
            for (let i = 0; i < imagesSettings.length; i++)
            {
                Flow.image.create(imagesSettings[i], list);
            }
        }

        protected addImage(settings: Flow.Image.Settings, parent: Flow.GenericElement): void
        {
            Flow.image.create(settings ? settings : TextPage.defaultImageSettings.image, parent);
        }

        protected shouldExcludeElement(element: TextPage.ElementSettings<any>): boolean
        {
            return false;
        }
    }

    export const textPage = Flow.declareElement(TextPage, true);

    export namespace TextPage
    {
        export enum ElementKind
        {
            DefaultText,
            RTPText,
            SingleRTPText,
            MaxWinText,
            MaxBetText,
            Image,
            ImageList,
            VersionText,
            LocaleText,
            YearText,
            ComplexText,
            ElementGroup
        }

        /** Settings for the page layout and elements to be added */
        export interface Settings extends Pages.Base.Settings
        {
            /** Page layout and base text settings */
            pageSettings: PageSettings;
            /** List of elements to be added to the page */
            elements: Element[];
        }

        /** Page layout and base text settings */
        export interface PageSettings
        {
            /** Settings for the list that all page elements are added to */
            listSettings: Flow.List.Settings;
            /** Base text settings for all paragraphs */
            paragraphSettings: Flow.Label.Settings;
            /** Year to enforce instead of using the current year */
            forceYear?: string;
        }

        export const defaultPageSettings: PageSettings =
        {
            listSettings:
            {
                spacing: Flow.Spacing.vertical(20),
                direction: Flow.List.Direction.TopToBottom,
                dock: Flow.Dock.Fill,
                expand: Flow.Expand.Allowed,
                expandItems: false,
                overflowMode: Flow.OverflowMode.Shrink
            },
            paragraphSettings:
            {
                expand: Flow.Expand.Allowed,
                legacy: false,
                text: "",
                font: Fonts.FrutigerWorld,
                fontSize: 30,
                dock: Flow.Dock.Top,
                align: Rendering.TextAlign.Middle,
                sizeToContents: true,
                canWrap: true,
                renderSettings: (Performance.getDevicePerformance() <= Performance.Level.Low) ? {
                    ...Rendering.TextOptions.defaultCanvasRenderSettings,
                    padding: 10
                } : null
            }
        };

        export const defaultSettings: Settings =
        {
            pageSettings: defaultPageSettings,
            elements: [],
            dock: Flow.Dock.Fill
        };

        /** Base type for an element to be added to the page */
        export interface ElementSettings<TKind extends ElementKind>
        {
            kind: TKind;
        }

        /** Type for grouping multiple elements onto one background */
        export interface ElementGroup extends ElementSettings<ElementKind.ElementGroup>
        {
            elements: Element[];
            background: Flow.Background.Settings;
            container?: Flow.Container.Settings;
            elementsList?: Flow.List.Settings;
        }

        export const defaultElementGroupSettings: ElementGroup =
        {
            background: Flow.Background.defaultSettings,
            container:
            {
                sizeToContents: true,
                dock: Flow.Dock.Fill,
                expand: Flow.Expand.Disallowed
            },
            elementsList:
            {
                ...Flow.List.defaultSettings,
                dock: Flow.Dock.Fill,
                evenlySpaceItems: true,
                name: "Elements list"
            },
            elements: [],
            kind: ElementKind.ElementGroup
        };

        /** Base text type for labels to be added to the page */
        export interface BaseTextSettings<TKind extends ElementKind, TText> extends ElementSettings<TKind>
        {
            kind: TKind;
            text: TText;
            paragraphSettings?: Flow.Label.Settings;
        }

        /** The base type for simple translations, with support for locale exclusion */
        export interface DefaultText<TKind extends ElementKind, TText> extends BaseTextSettings<TKind, TText>
        {
            /** Takes a list of locale codes and will not add this element if the locale matches any in the list */
            languageCodeBlackList?: string[];
        }

        /** Basic text settings, supports locale exclusion */
        export type DefaultTextSettings = DefaultText<ElementKind.DefaultText, Localisation.LocalisableString>;
        /** Settings for text containing {minRT} and {maxRTP}, which are substituted with the RTP range */
        export type RtpTextSettings = BaseTextSettings<ElementKind.RTPText, Localisation.ComplexTranslationReference< {minRTP: string, maxRTP: string} >>;
        /** Settings for text containing {RTP} which is substituted by the RTP */
        export type SingleRtpTextSettings = BaseTextSettings<ElementKind.SingleRTPText, Localisation.ComplexTranslationReference< {RTP: string} >>;
        /** Settings for text containing {maxWin} which is substituted by the maxWin */
        export type MaxWinTextSettings = BaseTextSettings<ElementKind.MaxWinText, Localisation.ComplexTranslationReference< {maxWin: string} >>;
        /** Settings for text containing {maxBet} which is substituted by the maximum bet amount */
        export type MaxBetTextSettings = BaseTextSettings<ElementKind.MaxBetText, Localisation.ComplexTranslationReference< {maxBet: string} >>;
        /** Settings for text containing {gameVersion} which is subsituted with the current game version */
        export type GameVersionTextSettings = BaseTextSettings<ElementKind.VersionText, Localisation.ComplexTranslationReference< {gameVersion: string} >>;
        /** Settings for text containing {year} which is subsituted with the current year */
        export type YearTextSettings = BaseTextSettings<ElementKind.YearText, Localisation.ComplexTranslationReference< {year: string} >>;
        /** Union type of supported text paragraph types */
        export type ParagraphSettings = DefaultTextSettings | RtpTextSettings | SingleRtpTextSettings | MaxWinTextSettings | MaxBetTextSettings | GameVersionTextSettings | YearTextSettings;
        /** Settings for adding a flow image to the page */

        export interface ImageSettings extends ElementSettings<ElementKind.Image>
        {
            image: Flow.Image.Settings;
        }

        export const defaultImageSettings: ImageSettings =
        {
            image:
            {
                kind: Flow.Image.Kind.Bitmap,
                asset: null,
                sizeToContents: true,
                dock: Flow.Dock.Float,
                scaleMode: Math.ScaleMode.Contain,
                expand: Flow.Expand.Allowed,
                floatPosition: Math.Vector2D(0.5, 0.5)
            },
            kind: ElementKind.Image
        };

        /** Settings for adding images in a list to the page */
        export interface ImageListSettings extends ElementSettings<ElementKind.ImageList>
        {
            images: Flow.Image.Settings[];
            list: Flow.List.Settings;
        }
        /** Settings for adding complex text to the page */
        export interface ComplexTextParams extends ElementSettings<ElementKind.ComplexText>
        {
            locale: Localisation.Locale;
            translation: Localisation.ComplexTranslationReference;
            substitutions: Localisation.Substitutions;
            textSettings: Flow.Label.Settings;
            variableSettings?: Flow.Label.Settings | ((variableName: string) => Flow.Label.Settings | null);
            protectedSettings?: Flow.Label.Settings | ((protectedName: string) => Flow.Label.Settings | null);
            listSettings?: Flow.List.Settings | Flow.List;
            imageVariableSettings?: ((variableName: string) => Flow.Image.Settings | null);
        }
        export interface ComplexTextInterface extends ElementSettings<ElementKind.ComplexText>
        {
            settings: Flow.ComplexText.ComplexTextSettings
        }
        export type ComplexTextSettings = ComplexTextParams | ComplexTextInterface;

        /** Union type of supported element types */
        export type Element = ParagraphSettings | ImageSettings | ImageListSettings | ComplexTextSettings | ElementGroup;
    }
}
