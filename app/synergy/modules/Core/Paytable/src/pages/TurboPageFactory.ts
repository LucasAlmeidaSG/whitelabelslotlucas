namespace RS.Paytable.Pages
{
    export class TurboPageFactory implements IPageFactory<Pages.TextPage.Settings, Pages.Base.RuntimeData>
    {
        constructor(protected readonly ctor: new (settings: Pages.TextPage.Settings, runtimeData: Pages.Base.RuntimeData) => Pages.TextPage = Pages.TextPage, protected readonly settings: TurboPageFactory.Settings = TurboPageFactory.defaultSettings)
        {

        }

        public create(overrideSettings: Partial<Pages.TextPage.Settings>, runtimeData: Pages.Base.RuntimeData): Pages.TextPage[];

        public create(runtimeData: Pages.Base.RuntimeData): Pages.TextPage[];

        public create(overrideSettingsOrRuntimeData: Partial<Pages.TextPage.Settings> | Pages.Base.RuntimeData, runtimeData?: Pages.Base.RuntimeData): Pages.TextPage[]
        {
            let overrideSettings: Partial<Pages.TextPage.Settings> = {};
            if (runtimeData)
            {
                overrideSettings = overrideSettingsOrRuntimeData;
            }
            else
            {
                runtimeData = overrideSettingsOrRuntimeData as Pages.Base.RuntimeData;
            }

            const platform: IPlatform = IPlatform.get();
            const isTurbo: boolean = platform.turboModeEnabled || false;
            const pageSettingsList: Pages.TextPage.Settings[] = [];
            if (isTurbo)
            {
                pageSettingsList.push(this.settings.turboPage);
            }

            const pages: Pages.TextPage[] = [];
            for (const pageSettings of pageSettingsList)
            {
                if (!pageSettings) { continue; }
                const page = new this.ctor(
                {
                    ...overrideSettings,
                    ...pageSettings,
                }, runtimeData);
                pages.push(page);
            }

            return pages;
        }
    }

    export namespace TurboPageFactory
    {
        export interface Settings extends Pages.Base.Settings
        {
            turboPage: Pages.TextPage.Settings;
        }

        export const defaultTurboPageSettings: Pages.TextPage.Settings =
        {
            ...Pages.TextPage.defaultSettings,
            elements:
            [
                {
                    kind: Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.TurboMode.Line1
                },
                {
                    kind: Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.TurboMode.Line2
                },
                {
                    kind: Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.TurboMode.Line3
                },
                {
                    kind: Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.TurboMode.Line4
                }
            ],
            pageSettings:
            {
                ...Pages.TextPage.defaultPageSettings,
            }
        }

        export const defaultSettings: Settings =
        {
            turboPage: defaultTurboPageSettings
        }
    }
}
