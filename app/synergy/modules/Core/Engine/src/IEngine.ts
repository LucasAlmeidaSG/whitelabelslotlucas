namespace RS.Engine
{
    /**
     * Data for a "force" or "rig".
     */
    export interface ForceResult
    {
        featureData?: any;
    }

    /**
     * Encapsulates a generic engine interface.
     */
    export interface IEngine<
        TModels extends Models,
        TSettings extends object,
        TReplayData extends object = {},
        TForceResult extends ForceResult = ForceResult
    >
    {
        /** Gets the settings for this engine. */
        readonly settings: TSettings;

        /** Gets the models for this engine. */
        readonly models: TModels;

        /** Gets the version for this engine if known. */
        readonly version?: Version | null;

        /** Published when an init response has been received and fully processed. */
        readonly onInitResponseReceived: IEvent<IResponse>;

        /** Published when a bet response has been received and fully processed. */
        readonly onBetResponseReceived: IEvent<IResponse>;

        /** Published when a close response has been received and fully processed. */
        readonly onCloseResponseReceived: IEvent<IResponse>;

        /** Gets if this engine supports replay. */
        readonly supportsReplay: boolean;

        /**
         * Initialises the engine and sends an init request.
         * Async.
         */
        init(): PromiseLike<IResponse>;

        /**
         * Sends a basic logic request.
         * Async.
         */
        bet(payload: ILogicRequest.Payload): PromiseLike<IResponse>

        /**
         * Sends a close request.
         * Async.
         */
        close(): PromiseLike<IResponse>;

        /**
         * Gets an array of all triggerable replays.
         */
        getReplayData(): PromiseLike<Map<TReplayData>>;

        /**
         * Starts a replay with the given data.
         */
        replay(data: TReplayData): void;

        /**
         * Rigs the next spin request.
         */
        force(forceResult: OneOrMany<TForceResult>): void;

        /**
         * Gets an array of all GREG replays.
         */
        getGREGData(): PromiseLike<Map<GREGData>>;
    }

    export type EngineType<
        TModels extends Models,
        TSettings extends object,
        TReplayData extends object = {},
        TForceResult extends ForceResult = ForceResult
    > = { new(settings: TSettings, models: TModels): IEngine<TModels, TSettings, TReplayData, TForceResult>; };

    /**
     * Contains the information necessary to reproduce a GREG (Game Report Generator Evaluator) input file.
     */
    export interface GREGData
    {
        readonly id: string | number; // Tag that identifies the replay. In real money, it will be the bet ID, in free play it will be a number.
        readonly networkResponses: string[]; // Contains the GLM network responses from all spins of the current wager.
        readonly initResponse: string; // Contains the GLM init response.
        readonly stake: number; // Contains the stake of the current wager.
        win: number; // Contains the amount won during the current wager. Some wagers last multiple spins, so win is updatable.
    }
}
