namespace RS.Engine
{
    /**
     * Encapsulates a request to initialise the game.
     */
    export interface IInitRequest<TPayload extends IInitRequest.Payload = IInitRequest.Payload, TResponse extends RS.Engine.IResponse = RS.Engine.IResponse> extends RS.Engine.IRequest<TPayload, TResponse> { }

    export namespace IInitRequest
    {
        export interface Payload
        {

        }
    }
}
