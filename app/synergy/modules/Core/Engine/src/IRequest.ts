/// <reference path="IResponse.ts" />

namespace RS.Engine
{
    /**
     * Encapsulates a request to be sent to a remote engine.
     * 
     * TODO remove this
     * @deprecated SG engine specific
     */
    export interface IRequest<TPayload, TResponse extends IResponse>
    {
        /**
         * Dispatches this request.
         * Async.
         */
        send(payload: TPayload): PromiseLike<TResponse>;
    }

    export type RequestType<TPayload, TResponse extends IResponse> = { new(): IRequest<TPayload, TResponse>; };
}
