namespace RS.Engine
{
    /**
     * Encapsulates a response received from a remote engine.
     * 
     * TODO replace with a success boolean
     * @deprecated Will eventually just be replaced with a success boolean
     */
    export interface IResponse
    {
        /** Gets if the response is considered an error. */
        readonly isError: boolean;

        /** Gets the request to which this response belongs. */
        readonly request: IRequest<any, any>;
    }
}