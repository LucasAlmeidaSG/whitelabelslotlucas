namespace RS.Engine
{
    /**
     * Encapsulates a request to close the game.
     */
    export interface ICloseRequest<TPayload extends ICloseRequest.Payload = ICloseRequest.Payload, TResponse extends RS.Engine.IResponse = RS.Engine.IResponse> extends RS.Engine.IRequest<TPayload, TResponse> { }

    export namespace ICloseRequest
    {
        export interface Payload
        {

        }
    }
}
