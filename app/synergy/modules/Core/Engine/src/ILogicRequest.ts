namespace RS.Engine
{
    /**
     * Encapsulates a request to spin the reels.
     */
    export interface ILogicRequest<TPayload extends ILogicRequest.Payload = ILogicRequest.Payload, TResponse extends RS.Engine.IResponse = RS.Engine.IResponse> extends RS.Engine.IRequest<TPayload, TResponse> { }

    export namespace ILogicRequest
    {
        export interface Payload
        {
            betAmount: Models.StakeAmount;
        }
    }
}
