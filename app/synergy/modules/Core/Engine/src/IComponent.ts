namespace RS.Engine
{
    export interface IComponent<
        TInitPayload extends object = {}, TInitResponse extends object = {},
        TLogicPayload extends object = {}, TLogicResponse extends object = {},
        TClosePayload extends object = {}, TCloseResponse extends object = {},
        TModels extends RS.Models = RS.Models> extends IDisposable
    {
        /** Populate init request payload */
        populateInitPayload(payload: TInitPayload): void;
        /** Populate game models with data from init response */
        populateInitModels(response: TInitResponse, models: TModels): void;

        /** Populate logic request payload */
        populateLogicPayload(payload: TLogicPayload): void;
        /** Populate game models with data from logic response */
        populateLogicModels(response: TLogicResponse, models: TModels): void;

        /** Populate close request payload */
        populateClosePayload(payload: TClosePayload): void;
        /** Populate game models with data from close response */
        populateCloseModels(response: TCloseResponse, models: TModels): void;
    }
}
