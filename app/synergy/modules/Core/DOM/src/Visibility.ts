namespace RS
{
    /**
     * Contains all page visibility related functions
     */
    export class Visibility
    {
        //private static readonly _onPageShown = createSimpleEvent();
        //private static readonly _onPageHidden = createSimpleEvent();

        /** Published when the game has gained focus. */
        public static readonly onPageShown = createSimpleEvent();//Visibility._onPageShown.public;

        /** Published when the game has lost focus. */
        public static readonly onPageHidden = createSimpleEvent();//Visibility._onPageHidden.public;

        private static _isPageHidden = false;
        private static _hiddenVar = "";

        private constructor() { }

        @Init()
        private static init(): void
        {
            if ((this._hiddenVar = "hidden") in document)
            {
                document.addEventListener("visibilitychange", () => this.onVisibilityChange());
                document.addEventListener("resume", () => this.onResume());
            }
            else if ((this._hiddenVar = "mozHidden") in document)
            {
                document.addEventListener("mozvisibilitychange", () => this.onVisibilityChange());
            }
            else if ((this._hiddenVar = "webkitHidden") in document)
            {
                document.addEventListener("webkitvisibilitychange", () => this.onVisibilityChange());
            }
            else if ((this._hiddenVar = "msHidden") in document)
            {
                document.addEventListener("msvisibilitychange", () => this.onVisibilityChange());
            }
            else if ("onfocusin" in (document as any))
            {
                document["onfocusin"] = document["onfocusout"] = () => this.onVisibilityChange();
            }
            else
            {
                window.onpageshow = window.onpagehide = () => this.onVisibilityChange();
                window.onfocus = window.onblur = () => this.onVisibilityChange();
            }

            window.addEventListener("pagehide", () => this.handlePageHidden());
            window.addEventListener("pageshow", () => this.handlePageShown());
        }

        private static onVisibilityChange(): void
        {
            if (document[this._hiddenVar])
            {
                this.handlePageHidden();
            }
            else
            {
                this.handlePageShown();
            }
        }

        private static handlePageShown(): void
        {
            if (!this._isPageHidden) { return; }
            this._isPageHidden = false;
            this.onPageShown.publish();
        }

        private static handlePageHidden(): void
        {
            if (this._isPageHidden) { return; }
            this._isPageHidden = true;
            this.onPageHidden.publish();
        }

        /**
         * Recheck visibility on resume for bug 
         * https://bugs.chromium.org/p/chromium/issues/detail?id=1018601&q=visibilitychange&can=2
         */
        private static onResume(): void
        {
            this.onVisibilityChange();
        }
    }
}
