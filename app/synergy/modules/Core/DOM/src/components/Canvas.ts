/// <reference path="Component.ts" />

namespace RS.DOM
{
    /**
     * A wrapper around a canvas element.
     */
    export class Canvas extends Component<HTMLCanvasElement>
    {
        /** The 2d context for this canvas */
        private _context: CanvasRenderingContext2D = null;

        /**
         * Creates a new canvas.
         * @param width     The viewport width of the canvas.
         * @param height    The viewport height of the canvas.
         * @param id        The optional ID to give this canvas.
         **/
        constructor(width: number, height: number, id?: string)
        {
            // Call base with a new canvas
            super(document.createElement("canvas"), id);

            // Setup width and height
            this._element.width = width;
            this._element.height = height;
        }

        /** Gets the 2D rendering context for this canvas. */
        public get context(): CanvasRenderingContext2D
        {
            return this._context || (this._context = this._element.getContext("2d"));
        }

        /** Gets the viewport width of this canvas. */
        public get width() { return this._element.width; }

        /** Gets the viewport height of this canvas. */
        public get height() { return this._element.height; }
    }
}
