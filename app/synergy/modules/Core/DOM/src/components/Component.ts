namespace RS.DOM
{
    export type CSSStyleKey = Extract<{ [K in keyof CSSStyleDeclaration]: CSSStyleDeclaration[K] extends string ? K : never }[keyof CSSStyleDeclaration], string>;

    /**
     * Wraps a DOM display element.
     * Any specialised components should extend this class.
     */
    @HasCallbacks
    export class Component<T extends HTMLElement = HTMLElement> implements IDisposable
    {
        /** The wrapper component. */
        private static _wrapper: Component = null;

        /**
         * Gets the wrapper component.
         */
        public static get wrapper()
        {
            return this._wrapper;
        }

        protected _element: T;
        protected _parent: Component;
        protected _children: Component[];
        protected _isDisposed: boolean = false;

        /**
         * Gets if this component has been disposed.
         */
        public get isDisposed() { return this._isDisposed; }

        /**
         * Gets the DOM element that this component encapsulates.
         */
        public get element(): T { return this._element; }

        /**
         * Gets or sets the CSS ID of this component.
         */
        public get id() { return this._element.id; }
        public set id(value) { this._element.id = value; }

        /**
         * Gets or sets the HTML of this component.
         */
        public get HTML() { return this._element.innerHTML; }
        public set HTML(value) { this._element.innerHTML = value; }

        /**
         * Gets or sets the text of this component.
         */
        public get text() { return this._element.innerText; }
        public set text(value) { this._element.innerText = value; }

        /**
         * Gets or sets the parent of this component.
         */
        public get parent() { return this._parent; }
        public set parent(value)
        {
            if (value === this._parent) { return; }

            if (this._parent)
            {
                const siblings = this._parent._children;
                const index = siblings.indexOf(this);
                if (index > -1)
                {
                    siblings.splice(index, 1);
                }
                this._element.parentElement.removeChild(this._element);
                this._parent = null;
            }

            if (value)
            {
                this._parent = value;
                this._parent._children.push(this);
                this._parent._element.appendChild(this._element);
            }
        }

        /**
         * Initialises a new instance of the BaseComponent class.
         * @param component     The DOM element to wrap.
         * @param id            The CSS ID to assign.
         */
        public constructor(element: T, id?: string);

        /**
         * Initialises a new instance of the BaseComponent class.
         * @param id            The CSS ID to assign.
         */
        public constructor(id?: string);

        public constructor(p1?: T | string, p2?: string)
        {
            // Resolve overload
            let element: T;
            let id: string;
            if (Is.string(p1))
            {
                element = null;
                id = p1;
            }
            else
            {
                element = p1;
                id = p2;
            }

            // Store the component or create a new one
            if (element != null)
            {
                this._element = element;
            }
            else
            {
                this._element = document.createElement("div") as HTMLElement as T; // this is naughty
            }

            // Set id
            if (id != null)
            {
                this.id = id;
            }

            this._children = [];
            this._parent = null;

            this.addDefaultClasses();
        }

        @Init() private static init()
        {
            Component._wrapper = new Component("rs-w");
            document.body.appendChild(Component._wrapper.element);
        }

        /**
         * Gets the children of this component.
         */
        public get children(): Component[]
        {
            return this._children;
        }

        /**
         * Adds a child to this component.
         * @param child     The child to add to this component.
         * @param index     An optional index to insert the child at.
         * @returns         Self - This for chaining.
         */
        public addChild(child: Component, index?: number): Component
        {
            child.parent = this;

            if (index !== undefined)
            {
                this.setChildIndex(child, index);
            }

            return this;
        }

        /**
         * Adds multiple children to this parent at once.
         * @param children
         * @returns {RS.Component} self for chaining
         */
        public addChildren(...children: Array<Component>): Component
        {
            for (let i = 0; i < children.length; i++)
            {
                children[i].parent = this;
            }

            return this;
        }

        /**
         * Gets the children of this component.
         * @param {Number} index - The index of the child component.
         * @returns {Component} - The child at the specified index.
         */
        public getChild(index: number): Component
        {
            return this._children[index];
        }

        /**
         * Gets the children of this component.
         * @param {Component} child - The child component to get an index for.
         * @returns {Number} - The index of the child.
         */
        public getChildIndex(child: Component): number
        {
            return this._children.indexOf(child);
        }

        /**
         * Sets the index of a child component.
         * @param {Component} child - The child to set the index of.
         * @param {Number} index - The new index of the child component.
         * @returns Self - This for chaining.
         */
        public setChildIndex(child: Component, index: number): this
        {
            if (child._parent !== this) { return this; }
            this._element.removeChild(child._element);

            this._element.insertBefore(child._element, this._element.children.item(index));

            this._children.splice(this.getChildIndex(child), 1);
            this._children.splice(index, 0, child);

            return this;
        }

        /**
         * Detaches a child from this component.
         * @param {Component} child - The child to detach from this component.
         * @returns Self - This for chaining.
         */
        public detachChild(child: Component): this
        {
            child.parent = null;
            return this;
        }

        /**
         * Detaches all children from this component.
         * @returns Self - This for chaining.
         */
        public detachAllChildren(): this
        {
            while (this._children.length > 0)
            {
                this.detachChild(this._children[0]);
            }
            return this;
        }

        /**
         * Adds this component to the RS wrapper DOM element.
         * @returns Self - This for chaining.
         */
        public addToWrapper(): this
        {
            this.parent = Component.wrapper;
            return this;
        }

        /**
         * Adds a CSS class to this component.
         * @param className The class to add.
         * @returns Self (for chaining).
         */
        public addClass(className: string): this
        {
            this._element.classList.add(className);
            return this;
        }

        /**
         * Removes a CSS class from this component.
         * @param className The class to remove.
         * @returns Self (for chaining).
         */
        public removeClass(className: string): this
        {
            this._element.classList.remove(className);
            return this;
        }

        /** Sets the value of an attribute on this component. */
        public attr(name: string, value: number): this;

        /** Sets the value of an attribute on this component. */
        public attr(name: string, value: string): this;

        /** Gets the value of an attribute on this component. */
        public attr(name: string): string|null;

        public attr(name: string, value?: string | number): any
        {
            if (value != null)
            {
                this._element.setAttribute(name, Is.number(value) ? value.toString() : value);
                return this;
            }
            else
            {
                return this._element.getAttribute(name);
            }
        }

        /** Sets the specified css property on this component. */
        public css<Type extends CSSStyleKey, U extends CSSStyleDeclaration[Type]>(name: Type, value: U): this;

        /** Gets the specified css property on this component. */
        public css(name: CSSStyleKey): string;

        public css(name: CSSStyleKey, value?: any): any
        {
            if (value != null)
            {
                this._element.style[name] = value;
                return this;
            }
            else
            {
                return this._element.style[name];
            }
        }

        /** Hides this element. */
        public hide(): this
        {
            this.css("display", "none");
            return this;
        }

        /** Shows this element. */
        public show(): this
        {
            this.css("display", "");
            return this;
        }

        /**
         * Removes this component from the DOM, destroying any children and then itself.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }

            for (const child of this._children)
            {
                child.dispose();
            }

            if (this._element.parentElement != null)
            {
                this._element.parentElement.removeChild(this._element);
            }

            this._element = null;
            this._parent = null;
            this._children = null;

            this._isDisposed = true;
        }

        /**
         * Adds the default classes to this component.
         */
        protected addDefaultClasses(): void
        {
            this._element.classList.add("rs-cmp");
        }
    }
}
