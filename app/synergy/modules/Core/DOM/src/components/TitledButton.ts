///<reference path="Button.ts"/>

namespace RS.DOM
{
    /**
     * Button with a title component.
     */
    export class TitledButton extends Button
    {
        private _title: Component;

        /** Gets the title component. */
        public get title(): Component { return this._title; }

        constructor()
        {
            super();

            this._title = new Component();
            this._title.addClass("btn-title");

            this.addChild(this._title);
        }

        public dispose(): void
        {
            super.dispose();
            this._title = null;
        }
    }
}
