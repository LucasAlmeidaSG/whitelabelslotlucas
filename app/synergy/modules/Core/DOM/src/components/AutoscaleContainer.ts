/// <reference path="Component.ts"/>

namespace RS.DOM
{
    /**
     * Indicates how content should be aligned within a space.
     */
    export enum Alignment
    {
        /** The left border of the content should align with the left border of the space. */
        Left,

        /** The origin the content should align with the origin of the space along an axis. */
        Center,

        /** The right border of the content should align with the right border of the space. */
        Right,

        /** The top border of the content should align with the top border of the space. */
        Top,

        /** The bottom border of the content should align with the bottom border of the space. */
        Bottom
    }

    /**
     * A function that converts the specified scales into a uniform scaling factor.
     */
    export type AspectFunction = (x: number, y: number) => number;

    /**
     * A container that scales all children via adjusting the scale transform.
     * Essentially a fixed coordinate-space can be mapped into parent coordinate-space using this container.
     * @param width           The width of the container's fixed coordinate-space
     * @param height          The height of the container's fixed coordinate-space
     * @param verticalAlign   How to align the container vertically
     * @param horizAlign      How to align the container horizontally
     */
    @HasCallbacks
    export class AutoscaleContainer extends Component
    {
        /** Published when this container has been autoscaled. */
        public readonly onAutoscaled = createSimpleEvent();

        /** Size of this container's fixed coordinate-space. */
        private _width: number;
        private _height: number;

        /** Alignment of this container within its parent's space. */
        private _vAlign: Alignment;
        private _hAlign: Alignment;

        /** Aspect ratio properties. */
        private _maintainAspectRatio: boolean = true;
        private _aspectFunction: AspectFunction = Math.min;

        /** Current scale. */
        private _xScale: number = 1.0;
        private _yScale: number = 1.0;

        private _parentSize: Math.Size2D;
        private _pollInterval: number;
        private _deferredAutoscale: number;

        /** A timeout used to force Safari to render. Refer to https://nextgengaming.atlassian.net/browse/SCF-441. */
        private _domUpdateTimeout: number;

        @AutoDispose private _viewportResizedHandler: IDisposable;
        @AutoDispose private _viewportScrolledHandler: IDisposable;

        /**
         * Gets or sets the width of this container's fixed coordinate-space.
         */
        public get width() { return this._width; }
        public set width(value: number)
        {
            this._width = value;
            this.css("width", `${this._width}px`);
        }

        /**
         * Gets or sets the height of this container's fixed coordinate-space.
         */
        public get height() { return this._height; }
        public set height(value: number)
        {
            this._height = value;
            this.css("height", `${this._height}px`);
        }

        /**
         * Gets or sets this container's vertical alignment.
         */
        public get verticalAlign() { return this._vAlign; }
        public set verticalAlign(value: Alignment)
        {
            this._vAlign = value;
        }

        /**
         * Gets or sets this container's horizontal alignment.
         */
        public get horizontalAlign() { return this._hAlign; }
        public set horizontalAlign(value: Alignment)
        {
            this._hAlign = value;
        }

        /**
         * Gets the current scale factor on the X axis.
         */
        public get currentXScale() { return this._xScale; }

        /**
         * Gets the current scale factor on the Y axis.
         */
        public get currentYScale() { return this._yScale; }

        /**
         * Gets or sets if the aspect ratio should be maintained.
         */
        public get maintainAspectRatio() { return this._maintainAspectRatio; }
        public set maintainAspectRatio(value: boolean) { this._maintainAspectRatio = value; }

        /**
         * Gets or sets the function used to clamp the aspect ratio
         */
        public get aspectFunction() { return this._aspectFunction; }
        public set aspectFunction(value: AspectFunction) { this._aspectFunction = value; }

        /**
         * Initialises a new instance of the AutoscaleContainer class.
         */
        public constructor(width: number, height: number, verticalAlign: Alignment = Alignment.Center, horizAlign: Alignment = Alignment.Center)
        {
            super();

            this._width = width;
            this._height = height;
            this._vAlign = verticalAlign;
            this._hAlign = horizAlign;

            this.css("width", `${this._width}px`)
                .css("height", `${this._height}px`)
                .css("transform", "matrix(1.0, 0.0, 0.0, 1.0, 0.0, 0.0)")
                .css("overflow", "visible");

            this.addClass("rs-autoscale");

            this._viewportResizedHandler = Viewport.onResized(this.handleViewportResized);
            this._viewportScrolledHandler = Viewport.onScrolled(this.handleViewportScrolled);

            this._parentSize = Math.Size2D(0, 0);

            this._pollInterval = window.setInterval(this.pollForSizeChange, 500);
        }

        /**
         * Finds the absolute size of the specified element.
         * @param {HTMLElement} element - The element to find the size of
         * @returns {Vector2D} The size
         */
        private static findElementSize(element: HTMLElement): Math.Size2D
        {
            let newWidth = element.clientWidth;
            let newHeight = element.clientHeight;

            // try
            // {
            //     const gdmPartner: any = window.parent;
            //     if (Is.func(gdmPartner.getSize))
            //     {
            //         const size = gdmPartner.getSize();
            //         newWidth = Number(size.w);
            //         newHeight = Number(size.h);
            //     }
            //     else if (gdmPartner.gameiframe != null)
            //     {
            //         newWidth = Number(gdmPartner.gameiframe.width);
            //         newHeight = Number(gdmPartner.gameiframe.height);
            //     }
            // }
            // catch (err)
            // {
            //     Log.warn(`Got an error trying to access window parent (${err})`);
            // }

            if (newWidth === 0 || isNaN(newWidth))
            {
                if (element.style.width.indexOf("%") !== -1)
                {
                    newWidth = Viewport.dimensions.w;
                }
                else
                {
                    newWidth = parseFloat(element.style.width.replace("px", ""));
                }
            }

            if (newHeight === 0 || isNaN(newHeight))
            {
                if (element.style.height.indexOf("%") !== -1)
                {
                    newHeight = Viewport.dimensions.h;
                }
                else
                {
                    newHeight = parseFloat(element.style.height.replace("px", ""));
                }
            }

            if (newWidth === 0 || isNaN(newWidth))
            {
                newWidth = Viewport.dimensions.w;
            }
            if (newHeight === 0 || isNaN(newHeight))
            {
                newHeight = Viewport.dimensions.h;
            }

            return { w: newWidth, h: newHeight };
        }

        /**
         * Recalculates new scale factors and applies the new transform.
         */
        public autoscale(onlyIfSizeChanged: boolean = false): void
        {
            const parentElement = this.getParentElement();
            if (parentElement == null) { return; }
            const parentSize = AutoscaleContainer.findElementSize(parentElement);
            if (onlyIfSizeChanged && Math.Size2D.equals(parentSize, this._parentSize)) { return; }
            this._parentSize = parentSize;
            this.doAutoscale();
        }

        @Callback
        protected pollForSizeChange(): void
        {
            this.autoscale(true);
        }

        @Callback
        protected handleViewportResized(size: Readonly<Math.Size2D>)
        {
            this.autoscale(false);
        }

        @Callback
        protected handleViewportScrolled()
        {
            this.autoscale(false);
        }

        protected getParentElement(): HTMLElement | null
        {
            let parentElement: HTMLElement = null;
            if (this.parent)
            {
                parentElement = this._parent.element;
            }
            else
            {
                parentElement = this._parent.element;
            }
            return parentElement;
        }

        protected doAutoscale(): void
        {
            const parentSize = this._parentSize;

            // Find the scale factors independant of aspect ratio
            this._xScale = parentSize.w / this._width;
            this._yScale = parentSize.h / this._height;

            // Maintain aspect ratio
            if (this._maintainAspectRatio)
            {
                this._xScale = this._yScale = this._aspectFunction(this._xScale, this._yScale);
            }

            // Find position
            let posX, posY;
            switch (this._hAlign)
            {
                case Alignment.Left:
                    posX = 0;
                    break;

                case Alignment.Center:
                    posX = parentSize.w / 2 - this._width * this._xScale / 2;
                    break;

                case Alignment.Right:
                    posX = parentSize.w - this._width * this._xScale;
                    break;
            }

            switch (this._vAlign)
            {
                case Alignment.Top:
                    posY = 0;
                    break;

                case Alignment.Center:
                    posY = parentSize.h / 2 - this._height * this._yScale / 2;
                    break;

                case Alignment.Bottom:
                    posY = parentSize.h - this._height * this._yScale;
                    break;
            }

            this.css("transform", `matrix(${this._xScale.toFixed(3)}, 0.0, 0.0, ${this._yScale.toFixed(3)}, ${posX | 0}, ${posY | 0})`);
            if (this._deferredAutoscale != null)
            {
                window.clearTimeout(this._deferredAutoscale);
                this._deferredAutoscale = null;
            }
            if (RS.Device.isAppleDevice)
            {
                this._deferredAutoscale = setTimeout(() => this.onAutoscaled.publish(), 500);
            }
            else
            {
                this.onAutoscaled.publish();
            }

            // Safari won't render again if maximised/minimised since 12.1, so 'update' the DOM.
            // Refer to https://nextgengaming.atlassian.net/browse/SCF-441
            if (Device.isDesktopDevice && Device.browser === Browser.Safari)
            {
                if (this._domUpdateTimeout != null)
                {
                    window.clearTimeout(this._domUpdateTimeout);
                }
                this._domUpdateTimeout = window.setTimeout(() =>
                {
                    const display = this.css("display");
                    this.css("display", "none");
                    this.css("display", display);
                }, 1);
            }
        }
    }
}
