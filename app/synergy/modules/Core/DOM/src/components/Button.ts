/// <reference path="Component.ts"/>

namespace RS.DOM
{
    /**
     * The RS.Button class creates a tappable / clickable component for use as a button.
     */
    export class Button extends Component
    {
        //protected readonly _onClicked = createSimpleEvent();

        /** Published when this button has been clicked. */
        public readonly onClicked = createSimpleEvent();//this._onClicked.public;

        protected _label: Component;
        protected _enabled: boolean = true;

        /**
         * Gets the label component.
         */
        public get label(): Component { return this._label; }

        /**
         * Gets or sets the CSS ID of this component.
         */
        public get id() { return this._element.id; }
        public set id(value)
        {
            this._element.id = value;
            this._label.id = `${value}-label`;
        }

        /**
         * Determines whether or not this button is enabled or not.
         */
        public get enabled(): boolean { return this._enabled; }

        public constructor()
        {
            super();
            this.addClass("rs-btn");
            this._label = new Component();
            this._label.addClass("rs-btn-label");
            this._label.HTML = "";
            this.addChild(this._label);

            this._element.addEventListener(Device.getSupportedClickEvent(TouchEventType.Click), (ev) =>
            {
                ev.preventDefault();
                this.onClicked.publish();
            });
        }

        /**
         * Enables this button to allow user interaction and removes any change to opacity.
         * @param opacity   The opacity to set
         * @returns         Self (for chaining)
         */
        public enable(opacity: number = 1.0): this
        {
            this.css("pointerEvents", "auto")
                .css("opacity", `${opacity}`);

            this._enabled = true;

            return this;
        }

        /**
         * Disables this button to disallow user interaction and adds an opacity change to show this.
         * @param opacity   The opacity to set
         * @returns         Self (for chaining)
         */
        public disable(opacity: number = 0.4): this
        {
            this.css("pointerEvents", "none")
                .css("opacity", `${opacity}`);

            this._enabled = false;

            return this;
        }

    }
}
