/// <reference path="Component.ts" />
/// <reference path="Button.ts" />

namespace RS.DOM
{
    /**
     * Shows an alert dialog on the screen, with a faded background. Takes up the whole screen.
     * By default, pressing the OK button on the alert dialog closes the dialog, though this can be supplemented with the user's own callback.
     * User should provide their own CSS classes, with id tags for alert, alert-text, alert-ok-button and alert-fade-bg.
     */
    export class Alert extends Component
    {
        protected _alertText: Component = null;
        protected _closeButton: TitledButton = null;
        protected _fadeBG: Component = null;

        constructor()
        {
            super();

            this.addClass("rs-alert");

            this._fadeBG = new Component();
            this._fadeBG.id = "alert-fade-bg";
            this.addChild(this._fadeBG);

            const background = new Component();
            background.id = "alert-bg";
            this.addChild(background);

            this._alertText = new Component();
            this._alertText.id = "alert-text";
            background.addChild(this._alertText);

            this._closeButton = new TitledButton();
            this._closeButton.id = "alert-ok-button";
            this._closeButton.label.id = "alert-ok-button-label";
            background.addChild(this._closeButton);
        }

        /**
         * Closes the alert. Called when the OK button is pressed.
         */
        public close(): void
        {
            this.parent = null;
            this.dispose();
        }

        /**
         * Shows an alert dialog with the given message and close callback.
         * @param {String} message - message to show in the alert box
         * @param {Function} closeCallback - function to call when box closed
         */
        public async showAlert(message: string, closeText: string)
        {
            this._alertText.HTML = message;
            this._closeButton.label.HTML = closeText;

            await this._closeButton.onClicked;

            this.close();
        }
    }
}
