/// <reference path="Component.ts" />

namespace RS.DOM
{
    /**
     * Renders a framerate counter to a DOM element.
     */
    export class FramerateCounter extends Component
    {
        /** Update counter at n FPS where updateTime = 1000 / n */
        private static updateTime: number = 1000 / 5;

        /** How much smoothing to apply to the counter. */
        private static smoothFactor: number = 0.75;

        private _accum: number;
        private _previousFPS: number;

        private _lastFrame: number;

        /**
         * Initialises a new instance of the FramerateCounter class.
         */
        public constructor()
        {
            super("fps-counter");
            this.text = "-";
            
            document.body.appendChild(this._element);

            Ticker.registerTickers(this);
            this._accum = 0;
            this._previousFPS = 0;
            this._lastFrame = Timer.now;
        }

        @Tick({ kind: Ticker.Kind.Always })
        private handleTick(ev: Ticker.Event): void
        {
            // Find frame delta
            const timeNow = Timer.now;
            const deltaTime = timeNow - this._lastFrame;
            this._lastFrame = timeNow;

            // Accumulate time
            this._accum += deltaTime;
            if (this._accum > FramerateCounter.updateTime) // Only update counter at limited FPS
            {
                // Update FPS
                const fps = 1000 / deltaTime;
                const ema = this._previousFPS = FramerateCounter.smoothFactor * this._previousFPS + (1 - FramerateCounter.smoothFactor) * fps;
                const newFPS = `${Math.round(ema)}`;
                if (this.text !== newFPS) { this.text = newFPS; }
                this._accum = 0;
            }
        }
    }
}