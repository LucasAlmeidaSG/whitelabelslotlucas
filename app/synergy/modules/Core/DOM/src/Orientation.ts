/// <reference path="Viewport.ts" />

namespace RS
{
    /**
     * Represents an orientation of the device.
     */
    export enum DeviceOrientation
    {
        Landscape,
        Portrait
    }

    /**
     * Responsible for tracking device orientation.
     */
    export class Orientation
    {
        /** Published when the orientation of the device has changed. */
        public static readonly onChanged = createEvent<DeviceOrientation>();//Orientation._onChanged.public;

        public static readonly Landscape: DeviceOrientation = DeviceOrientation.Landscape;
        public static readonly Portrait: DeviceOrientation = DeviceOrientation.Portrait;

        private static _currentOrientation: DeviceOrientation;
        //private static readonly _onChanged = createEvent<DeviceOrientation>();

        private constructor() { }

        /**
         * Initialises the Orientation class.
         */
        @RS.Init({ bindCallbacks: true }) private static init(): void
        {
            Viewport.onResized(this.checkOrientation);
        }

        /**
         * Checks device orientation.
         */
        @Callback
        private static checkOrientation(): void
        {
            const newOrientation = this.isLandscape ? Orientation.Landscape : Orientation.Portrait;
            if (newOrientation !== this._currentOrientation)
            {
                this._currentOrientation = newOrientation;
                this.onChanged.publish(newOrientation);
            }
        }

        /**
         * Gets whether or not the device is in landscape.
         */
        public static get isLandscape() { return Viewport.dimensions.h < Viewport.dimensions.w; }

        /**
         * Gets whether or not the device is in landscape.
         */
        public static get isPortrait() { return !this.isLandscape; }

        /**
         * Gets the current device orientation.
         */
        public static get currentOrientation()
        {
            this.checkOrientation();
            return this._currentOrientation;
        }
    }

    export namespace Orientation
    {
        /** Represents an observable for a DeviceOrientation. */
        export interface IReadonlyObservable extends RS.IReadonlyObservable<DeviceOrientation>
        {
            /** Whether or not this.value === RS.DeviceOrientation.Landscape. */
            readonly isLandscape: boolean;
            /** Whether or not this.value === RS.DeviceOrientation.Portrait. */
            readonly isPortrait: boolean;
        }

        export interface IObservable extends RS.IObservable<DeviceOrientation>
        {
            /** Whether or not value === RS.Orientation.Landscape. */
            isLandscape: boolean;
            /** Whether or not value === RS.Orientation.Portrait. */
            isPortrait: boolean;
        }

        export class Observable extends RS.Observable<DeviceOrientation | null>
        {
            public get isLandscape()
            {
                return this.value === DeviceOrientation.Landscape;
            }
            public set isLandscape(value)
            {
                this.value = value ? DeviceOrientation.Landscape : DeviceOrientation.Portrait;
            }

            public get isPortrait()
            {
                return this.value === DeviceOrientation.Portrait;
            }
            public set isPortrait(value)
            {
                this.value = value ? DeviceOrientation.Portrait : DeviceOrientation.Landscape;
            }

            constructor(value: DeviceOrientation = null)
            {
                super(value);
            }
        }
    }
}
