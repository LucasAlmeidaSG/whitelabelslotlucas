namespace RS
{
    /**
     * Centralises access to viewport size.
     */
    export class Viewport
    {
        /** Published when the viewport has been resized. */
        public static readonly onResized = createEvent<Dimensions>();

        /** Published when resize has been attempted but no dimension change detected (iOS App/iOS Chrome only, causes issues in browser)*/
        public static readonly onResizeAttempted = createSimpleEvent();

        /** Published when the viewport has been scrolled. */
        public static readonly onScrolled = createSimpleEvent();

        /** Viewport dimensions. Use this instead of innerWidth/innerHeight or similar properties. */
        public static get dimensions(): Math.ReadonlySize2D { return this._dimensions; }

        private static _dimensions: Math.Size2D;
        private static _timeoutHandle: number | null = null;
        private static _timeoutDelay: number = 200;

        private static _checkDuplicates = Device.isIOSApp || (Device.isAppleDevice && Device.browser === Browser.ChromeMobile)
        // Duplicate counters for iOS app/chrome
        private static _duplicateCount = 0;
        private static readonly _duplicateTarget = 3;

        private constructor() { }

        /**
         * Initialises the ViewportUtils class.
         */
        @RS.Init() private static init(): void
        {
            this._dimensions = { w: 0, h: 0 };
            this.updateWindowSize(this._dimensions);

            window.addEventListener("resize", this.handleWindowResize.bind(this));
            window.addEventListener("scroll", this.handleWindowScroll.bind(this));
        }

        private static handleWindowResize(ev: UIEvent): void
        {
            if (this.updateWindowSize(this._dimensions))
            {
                // Fix for iOS scrolling the screen when changing orientation
                if (Device.isAppleDevice)
                {
                    window.scrollTo(0, window.scrollY);
                }

                if (this._checkDuplicates)
                {
                    this._duplicateCount = 0;
                    this.checkResizeAgain(ev);
                }
                else
                {
                    this.onResized.publish(this._dimensions);
                    return;
                }
            }
            else
            {
                // Only publish if number of duplicate dimensions reaches target
                // iOS app changes dimensions a lot while changing orientations
                if (this._checkDuplicates &&++this._duplicateCount > this._duplicateTarget)
                {
                    this.onResized.publish(this._dimensions);
                }
                else
                {
                    this.checkResizeAgain(ev);
                }
            }
        }

        private static handleWindowScroll(ev: UIEvent): void
        {
            this.onScrolled.publish();
            this.handleWindowResize(ev);
        }

        private static checkResizeAgain(ev: UIEvent)
        {
            if (this._timeoutHandle != null)
            {
                clearTimeout(this._timeoutHandle);
            }
            this._timeoutHandle = setTimeout(this.handleWindowResize.bind(this, ev), this._timeoutDelay);

            if (this._checkDuplicates)
            {
                this.onResizeAttempted.publish();
            }
        }

        /**
         * Updates the given Size2D with the window size.
         * @returns true if a change resulted from this call.
         */
        private static updateWindowSize(out: Math.Size2D): boolean
        {
            const w = window.innerWidth, h = window.innerHeight;
            const changed = out.w !== w || out.h !== h;
            Math.Size2D.set(out, w, h);
            return changed;
        }
    }
}
