namespace RS.DOM
{
    export namespace Document
    {
        export const readyState = new Observable(document.readyState === "complete");
    }

    document.addEventListener("readystatechange", (ev) =>
    {
        Document.readyState.value = document.readyState === "complete";
    });
}