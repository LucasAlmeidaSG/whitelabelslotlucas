namespace RS.DOM
{
    /**
     * Contains strongly typed meta properties for the current page.
     * All properties could return null and can be set as null.
     */
    export class Metas
    {
        private constructor() { }

        /**
         * Gets or sets the theme color of the page.
         * Only works on Chrome/Android.
         */
        public static get themeColor()
        {
            const raw = Meta.get("theme-color");
            return raw ? new Util.Color(raw) : null;
        }
        public static set themeColor(value)
        {
            if (value)
            {
                Meta.set("theme-color", value.toString());
            }
            else
            {
                Meta.clear("theme-color");
            }
        }

        /**
         * Gets or sets the style of the status bar.
         * Only works on Safari/iOS.
         */
        public static get statusBarStyle(): Metas.StatusBarStyle
        {
            const raw = Meta.get("apple-mobile-web-app-status-bar-style");
            return raw ? (raw as Metas.StatusBarStyle) : null;
        }
        public static set statusBarStyle(value)
        {
            if (value)
            {
                Meta.set("apple-mobile-web-app-status-bar-style", value);
            }
            else
            {
                Meta.clear("apple-mobile-web-app-status-bar-style");
            }
        }

        /**
         * Gets or sets whether the page is "web app capable".
         * Only works on Safari/iOS.
         */
        public static get webAppCapable(): boolean
        {
            const raw = Meta.get("apple-mobile-web-app-capable");
            return raw ? raw.toLowerCase() === "yes" : null;
            
        }
        public static set webAppCapable(value)
        {
            if (value)
            {
                Meta.set("apple-mobile-web-app-capable", value ? "yes" : "no");
            }
            else
            {
                Meta.clear("apple-mobile-web-app-capable");
            }
        }

        /**
         * Gets or sets the viewport of the page.
         * Only works on Safari/iOS.
         */
        public static get viewport(): Viewport
        {
            const raw = Meta.get("viewport");
            return raw ? Metas.Viewport.deserialise(raw) : null;
            
        }
        public static set viewport(value)
        {
            if (value)
            {
                Meta.set("viewport", Metas.Viewport.serialise(value));
            }
            else
            {
                Meta.clear("viewport");
            }
        }
    }

    export namespace Metas
    {
        export enum StatusBarStyle
        {
            Black = "black",
            BlackTranslucent = "black-translucent"
        }
    
        export interface Viewport
        {
            initialScale?: number;
            maximumScale?: number;
            userScalable?: boolean;
            minimalUI?: boolean;
        }
    
        export namespace Viewport
        {
            export function serialise(viewport: Viewport): string
            {
                const segs: string[] = [];
                if (viewport.initialScale != null)
                {
                    segs.push(`initial-scale=${viewport.initialScale.toFixed(2)}`);
                }
                if (viewport.maximumScale != null)
                {
                    segs.push(`maximum-scale=${viewport.maximumScale.toFixed(2)}`);
                }
                if (viewport.userScalable != null)
                {
                    segs.push(`user-scalable=${viewport.userScalable ? "yes": "no"}`);
                }
                if (viewport.minimalUI)
                {
                    segs.push(`minimal-ui`);
                }
                return segs.join(", ");
            }
    
            export function deserialise(str: string): Viewport
            {
                const result: Viewport = {};
                const segs = str.split(/,\w*/g);
                for (const seg of segs)
                {
                    const kvPair = seg.split(/\w*=\w*/g);
                    switch (kvPair[0])
                    {
                        case "initial-scale":
                            result.initialScale = parseFloat(kvPair[1]);
                            break;
                        case "maximum-scale":
                            result.maximumScale = parseFloat(kvPair[1]);
                            break;
                        case "user-scalable":
                            result.userScalable = kvPair[1] == null || kvPair[1].toLowerCase() === "yes";
                            break;
                        case "minimal-ui":
                            result.minimalUI = kvPair[1] == null || kvPair[1].toLowerCase() === "yes";
                            break;
                    }
                }
                return result;
            }
        }
    }
}