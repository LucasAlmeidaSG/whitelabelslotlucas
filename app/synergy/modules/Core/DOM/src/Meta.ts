namespace RS.DOM.Meta
{
    let metas: Map<HTMLMetaElement>;

    function refresh()
    {
        metas = {};
        const metaTags = document.head.getElementsByTagName("meta");
        for (let i = 0, l = metaTags.length; i < l; ++i)
        {
            const metaTag = metaTags.item(i);
            metas[metaTag.name] = metaTag;
        }
    }

    RS.addInitCallback(refresh);

    /**
     * Gets if a meta tag with the specified name exists.
     * @param name 
     */
    export function exists(name: string): boolean
    {
        return metas[name] != null;
    }

    /**
     * Gets the value of the specified meta tag, or null if it doesn't exist.
     * @param name 
     */
    export function get(name: string): string | null
    {
        return metas[name] ? metas[name].content : null;
    }

    /**
     * Sets the value of the specified meta tag.
     * @param name 
     * @param value 
     */
    export function set(name: string, value: string): void
    {
        if (!metas[name])
        {
            const metaTag = document.createElement("meta");
            metaTag.name = name;
            metaTag.content = value;
            document.head.appendChild(metaTag);
            metas[name] = metaTag;
        }
        else
        {
            metas[name].content = value;
        }
    }

    /**
     * Removes the specified meta tag.
     * @param name 
     */
    export function clear(name: string): void
    {
        if (!metas[name]) { return; }
        metas[name].remove();
        delete metas[name];
    }

    /**
     * Removes all meta tags.
     */
    export function clearAll(): void
    {
        for (const key in metas)
        {
            if(metas[key]) {
                metas[key].remove();
            }
        }
        metas = {};
    }
}