namespace RS.Build.CodeGeneration.Reflection
{
    export type Action = (this: Lexer, lexeme: string, line: number, col: number) => Token[]|Token|void;

    interface Rule
    {
        pattern: RegExp;
        global: boolean;
        action: Action;
        start: number[];
    }

    interface Match
    {
        result: RegExpExecArray;
        action: Action;
        length: number;
    }

    export interface Token
    {
        lexeme: string;
        col: number;
        row: number;
    }

    export class Lexer
    {
        private _state: number = 0;
        private _index: number = 0;
        private _input: string = "";

        private _rules: Rule[] = [];
        private _remove: number = 0;
        private _tokens: Token[] = [];
        private _reject: boolean;

        private _lineData: { startIndex: number; length: number; }[] = [];

        public addRule(pattern: RegExp, action: Action, start: number[] = [0]): this
        {
            const global = pattern.global;

            if (!global)
            {
                let flags = "g";
                if (pattern.multiline) { flags += "m"; }
                if (pattern.ignoreCase) { flags += "i"; }
                pattern = new RegExp(pattern.source, flags);
            }

            this._rules.push({
                pattern: pattern,
                global: global,
                action: action,
                start: start
            });

            return this;
        }

        public setInput(input: string): this
        {
            this._remove = 0;
            this._state = 0;
            this._index = 0;
            this._tokens.length = 0;
            this._lineData.length = 0;
            this._input = input;

            let curIndex = 0;
            let curLine = 0;
            for (const l of this._input.split(/\n\r|\r\n|\n|\r/gm))
            {
                if (l.indexOf("\n") !== -1 || l.indexOf("\r") !== -1)
                {
                    curLine++;
                }
                else
                {
                    this._lineData.push({ startIndex: curIndex, length: l.length });
                }
                curIndex += l.length;
            }

            return this;
        }

        public lex(): Token
        {
            if (this._tokens.length) { return this._tokens.shift(); }

            this._reject = true;

            while (this._index <= this._input.length)
            {
                const matches = this.scan().splice(this._remove);
                const index = this._index;

                while (matches.length)
                {
                    if (this._reject)
                    {
                        const match = matches.shift();
                        const result = match.result;
                        const length = match.length;
                        const pos = this.indexToPos(this._index);
                        this._index += length;
                        this._reject = false;
                        this._remove++;

                        let token: Token|Token[] = match.action.call(this, result[0], pos.line, pos.col);
                        if (this._reject)
                        {
                            this._index = result.index;
                        }
                        else if (token != null)
                        {
                            if (Is.array(token))
                            {
                                this._tokens = token.slice(1);
                                token = token[0];
                            }
                            else
                            {
                                if (length) { this._remove = 0; }
                                return token;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                const input = this._input;

                if (index < input.length)
                {
                    if (this._reject)
                    {
                        this._remove = 0;
                        const token = this.defunct(input.charAt(this._index++));
                        if (token != null)
                        {
                            if (Is.array(token))
                            {
                                this._tokens = token.slice(1);
                                return token[0];
                            }
                            else
                            {
                                return token;
                            }
                        }
                    }
                    else
                    {
                        if (this._index !== index) { this._remove = 0; }
                        this._reject = true;
                    }
                }
                else if (matches.length)
                {
                    this._reject = true;
                }
                else
                {
                    break;
                }
            }
        }

        private defunct(chr: string): Token|Token[]
        {
            const pos = this.indexToPos(this._index - 1);
            throw new Error(`Unexpected character '${chr}' (line ${pos.line}, column ${pos.col})`);
        }

        private indexToPos(index: number): { col: number; line: number; }
        {
            for (let line = 0, l = this._lineData.length; line < l; ++line)
            {
                const data = this._lineData[line];
                const endIndex = data.startIndex + data.length;
                if (index >= data.startIndex && index < endIndex)
                {
                    return { line, col: index - data.startIndex };
                }
            }
            return { line: this._lineData.length - 1, col: this._lineData[this._lineData.length - 1].length };
        }

        private scan()
        {
            const matches: Match[] = [];
            let index = 0;

            const state = this._state;
            const lastIndex = this._index;
            const input = this._input;

            for (let i = 0, length = this._rules.length; i < length; i++)
            {
                const rule = this._rules[i];
                const start = rule.start;
                const states = start.length;

                if ((!states || start.indexOf(state) >= 0) ||
                    (state % 2 && states === 1 && !start[0]))
                {
                    const pattern = rule.pattern;
                    pattern.lastIndex = lastIndex;
                    const result = pattern.exec(input);

                    if (result && result.index === lastIndex)
                    {
                        let j = matches.push({
                            result: result,
                            action: rule.action,
                            length: result[0].length
                        });

                        if (rule.global) {
                            index = j;
                        }

                        while (--j > index)
                        {
                            const k = j - 1;

                            if (matches[j].length > matches[k].length)
                            {
                                const temple = matches[j];
                                matches[j] = matches[k];
                                matches[k] = temple;
                            }
                        }
                    }
                }
            }

            return matches;
        }
    }
}
