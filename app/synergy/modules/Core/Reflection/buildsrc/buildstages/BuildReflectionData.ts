namespace RS.Build.CodeGeneration.Reflection
{
    /**
     * Responsible for building reflection data.
     */
    export class BuildReflectionData extends BuildStage.Base
    {
        protected _builtReflectionData: number;

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            Log.pushContext("Reflection");
            this._builtReflectionData = 0;
            const result = await super.execute(moduleList);
            if (this._builtReflectionData > 0)
            {
                Log.info(`Built reflection data for ${this._builtReflectionData} modules`);
            }
            Log.popContext("Reflection");
            return result;
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const dtsPath = Path.combine(module.path, "build", "src.d.ts");
            if (!await FileSystem.fileExists(dtsPath)) { return { workDone: true, errorCount: 0 }; }

            // Load the build definitions file
            let dts: string;
            try
            {
                dts = await FileSystem.readFile(dtsPath);
            }
            catch (err)
            {
                Log.error(`Failed to read ${dtsPath}`);
                Log.error(`${err}`);
                return { workDone: true, errorCount: 1 };
            }

            // Checksum it
            const dtsChecksum = Checksum.getSimple(dts);
            if (!module.checksumDB.diff("dts_reflect", dtsChecksum))
            {
                // No work to do
                return { workDone: false, errorCount: 0 };
            }

            // Reflect
            const data: ReflectionData = {
                globalNamespace: {
                    kind: "namespace",
                    name: "global",
                    children: []
                }
            };
            try
            {
                reflect(dts, data);
            }
            catch (err)
            {
                Log.error(`Failed to build reflection data from ${dtsPath}`);
                Log.error(`${err}`);
                return { workDone: true, errorCount: 1 };
            }

            // Write
            await FileSystem.writeFile(Path.combine(module.path, "build", "reflectionData.json"), JSON.stringify(data));

            // Commit
            module.checksumDB.set("dts_reflect", dtsChecksum);
            await module.saveChecksums();
            this._builtReflectionData++;

            // Done
            return { workDone: true, errorCount: 0 };
        }
    }

    export const buildReflectionData = new BuildReflectionData();
    BuildStage.register({ after: [ RS.BuildStages.compile ] }, buildReflectionData);
}
