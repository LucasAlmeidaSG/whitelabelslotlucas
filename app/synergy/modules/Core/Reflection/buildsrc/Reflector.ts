namespace RS.Build.CodeGeneration.Reflection
{
    import TypeRef = CodeMeta.TypeRef;
    import AccessQualifier = CodeMeta.AccessQualifier;
    import StorageQualifier = CodeMeta.StorageQualifier;

    export interface ReflectionData
    {
        globalNamespace: Namespace;
    }

    interface Token
    {
        name: string;
        lexeme: string;
        line: number;
        col: number;
    }

    /** Parses the specified d.ts source and reflects into the target data. */
    export function reflect(dts: string, data: ReflectionData)
    {
        const lexer = new Lexer();

        const tokens: Token[] = [];

        function addRule(rule: RegExp, name: string, ignore: boolean = false)
        {
            lexer.addRule(rule, (lexeme, line, col) =>
            {
                if (ignore) { return; }
                tokens.push({ name, lexeme, line, col });
            });
        }

        addRule(/\/\/.*/, "singleLineComment", true);
        addRule(/\/\*(.|\n|\r)*?\*\//m, "multiLineComment", true);
        //addRule(/".*"/, "stringLiteral");
        addRule(/"([^"\\]|\\.)*"/, "stringLiteral");
        addRule(/'([^'\\]|\\.)*'/, "stringLiteral");
        addRule(/[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?/, "numberLiteral");
        addRule(/true|false/, "booleanLiteral");
        addRule(/(\s)+/, "whitespace", true);
        addRule(/(declare|namespace|typeof|type|get|set|interface|class|function|enum|const|var|let|abstract|private|protected|public|static|readonly|extends|implements|is|import|infer|in|keyof|new|module|export|as|default|from|global)/, "keyword");
        addRule(/[a-zA-Z_\$][a-zA-Z0-9_\$]*/, "identifier");
        addRule(/[\{\}\[\]\(\)]/, "bracket");
        addRule(/[<>]/, "typeBracket");
        addRule(/;/, "semiColon");
        addRule(/\./, "dot");
        addRule(/=\>/, "fatArrow");
        addRule(/=/, "assign");
        addRule(/,/, "comma");
        addRule(/:/, "colon");
        addRule(/\*/, "asterisk");
        addRule(/[\|&]/, "unionOrIntersect");
        addRule(/\?/, "questionMark");
        //addRule(/==|===|<=|<|>=|>|!=|!==|\+|-|\*|\/|\||\|\||&|&&/, "binaryOperator");

        lexer.setInput(dts);
        lexer.lex();

        const parser = new Parser(new TokenStream(tokens), data);
        parser.parse();
    }

    class TokenStream
    {
        private _tokens: Token[];
        private _position: number = 0;
        private _positionStack: number[] = [];

        public get eos() { return this._position >= this._tokens.length; }
        public get position() { return this._position; }
        public get stackSize() { return this._positionStack.length; }

        public constructor(tokens: Token[])
        {
            this._tokens = tokens;
        }

        public pushPosition(src?: string)
        {
            this._positionStack.push(this._position);
            // console.log(`== [${src || "unknown"}] push (${this._position}) :${this.stackSize}`);
        }

        public popPosition(src?: string)
        {
            this._position = this._positionStack.pop() || 0;
            // console.log(`== [${src || "unknown"}] pop (${this._position}) :${this.stackSize}`);
        }

        public takePosition(src?: string)
        {
            this._positionStack.pop();
            // console.log(`== [${src || "unknown"}] take (${this._position}) :${this.stackSize}`);
        }

        public next(): Token | null
        {
            return this._tokens[this._position++] || null;
        }
    }

    function expector(target: object, key: string, descriptor: PropertyDescriptor)
    {
        if (descriptor == null) { descriptor = Object.getOwnPropertyDescriptor(target, key); }

        const oldFunc: ()=>void = target[key];

        function newFunc()
        {
            const stackSize = this._tokenStream.stackSize;
            this._tokenStream.pushPosition(key);
            let result: any;
            try
            {
                result = oldFunc.apply(this, arguments);
            }
            catch (err)
            {
                throw new Error(`${err} (${key})`);
            }
            if (result != null && result !== false)
            {
                this._tokenStream.takePosition(key);
                if (this._tokenStream.stackSize !== stackSize)
                {
                    throw new Error(`Stack sizes didn't match before and after calling expector! (${key})`);
                }
                return result;
            }
            else
            {
                this._tokenStream.popPosition(key);
                if (this._tokenStream.stackSize !== stackSize)
                {
                    throw new Error(`Stack sizes didn't match before and after calling expector! (${key})`);
                }
                return null;
            }
        }

        descriptor.value = newFunc;
        Object.defineProperty(target, key, descriptor);
    }

    const allowKeywordAsIdentifier =
    {
        "type": true,
        "function": true,
        "declare": true,
        "public": true,
        "as": true,
        "var": true,
        "is": true,
        "namespace": true,
        "module": true,
        "export": true,
        "class": true,
        "in": true,
        "default": true,
        "from": true,
        "global": true,
        "static": true,
        "get": true,
        "set": true,
        "readonly": true
    };

    class Parser
    {
        private _tokenStream: TokenStream;
        private _data: ReflectionData;
        private _currentNamespace: string[] = [];
        private _currentAsNamespace: number = 0;
        private _currentPropertyHolder: { properties?: PropertyDeclaration[] } | null = null;
        private _currentMethodHolder: { methods?: MethodDeclaration[] } | null = null;

        private get namespace() { return this._currentNamespace.join("."); }

        public constructor(tokenStream: TokenStream, data: ReflectionData)
        {
            this._tokenStream = tokenStream;
            this._data = data;
        }

        public parse(): void
        {
            while (!this._tokenStream.eos)
            {
                if (!this.expectDeclaration(true)) { return this.fail(); }
            }
        }

        private fail(message?: string): void
        {
            const t = this._tokenStream.next();
            if (message)
            {
                throw new Error(`${message} near '${t ? t.lexeme : "eos"}' (line ${t.line + 1} column ${t.col})`);
            }
            else
            {
                throw new Error(`Unexpected '${t ? t.lexeme : "eos"}' (line ${t.line + 1} column ${t.col})`);
            }
        }

        private expectToken(name: string, lexeme?: string): Token | null
        {
            // console.log(`== push ${this._tokenStream.position} ==`);
            this._tokenStream.pushPosition("expectToken");
            const token = this._tokenStream.next();
            if (token.name === name && (lexeme == null || lexeme === token.lexeme))
            {
                // console.log(`Expecting ${name}${lexeme ? ` (${lexeme})` : ""}, got ${token.name} (${token.lexeme}). line ${token.line} col ${token.col}`);
                this._tokenStream.takePosition("expectToken");
                return token;
            }
            else
            {
                // console.log(`Expecting ${name}${lexeme ? ` (${lexeme})` : ""}, got ${token.name} (${token.lexeme}). line ${token.line} col ${token.col}`);
                this._tokenStream.popPosition("expectToken");
                return null;
            }
        }

        private expectOnlyToken(name: string, lexeme?: string): Token | null
        {
            const t = this.expectToken(name, lexeme);
            if (!t) { this.fail(`Expected ${name}, got ${t ? t.name : "null"}`); }
            return t;
        }

        @expector
        private expectTokens(name: string, lexemes: string[]): Token[] | null
        {
            const tokens: Token[] = [];
            if (lexemes.length <= 0) { return tokens; }

            const thisToken = this.expectToken(name);
            if (!thisToken) { return null; }

            const nextLexemes: string[] = [];
            let matchedToken: boolean = false;
            for (const lexeme of lexemes)
            {
                if (lexeme === thisToken.lexeme)
                {
                    tokens.push(thisToken);
                    matchedToken = true;
                }
                else
                {
                    nextLexemes.push(lexeme);
                }
            }

            if (!matchedToken) { return null; }
            const nextTokens = this.expectTokens(name, nextLexemes);

            return Array.isArray(nextTokens) ? tokens.concat(nextTokens) : tokens;
        }

        @expector
        private expectIdentifier(): Token | null
        {
            const identifier = this.expectToken("identifier");
            if (identifier != null) { return identifier; }

            const keyword = this.expectToken("keyword");
            if (keyword != null && allowKeywordAsIdentifier[keyword.lexeme]) { return keyword; }

            return null;
        }

        /** Expects a destructuring object assignment (object unpack). */
        @expector
        private expectDestructuringObjectAssignment(): boolean|null
        {
            // { identifier, key: identifier, key: { nestedKey: identifier, nestedKey: [arr1, arr2] } }: type

            if (!this.expectToken("bracket", "{")) { return false; }

            // Expect identifiers
            this.expectList(() =>
            {
                const key = this.expectIdentifier() || this.expectToken("stringLiteral") || this.expectToken("numberLiteral");
                if (!key) { return false; }

                // Property type
                if (this.expectToken("colon"))
                {
                    const identifier = this.expectIdentifier() || this.expectDestructuringObjectAssignment() || this.expectDestructuringArrayAssignment();
                    if (!identifier)
                    {
                        return false;
                    }
                }
                else if (key.name === "stringLiteral" || key.name === "numberLiteral")
                {
                    return false;
                }

                return true;
            });

            if (!this.expectToken("bracket", "}")) { return false; }

            return true;
        }

        /** Expects a destructuring array assignment (array unpack). */
        @expector
        private expectDestructuringArrayAssignment(): boolean|null
        {
            // [ identifier, { object }, [arr1, arr2], identifier, ...spread ]: type

            if (!this.expectToken("bracket", "[")) { return false; }

            // Expect identifiers
            this.expectList(() =>
            {
                const isSpread = this.expectSpread();
                if (isSpread)
                {
                    const identifier = this.expectIdentifier();
                    if (!identifier) { return false; }
                }
                else
                {
                    const identifier = this.expectIdentifier() || this.expectDestructuringObjectAssignment() || this.expectDestructuringArrayAssignment();
                    if (!identifier) { return false; }
                }

                return true;
            });

            if (!this.expectToken("bracket", "]")) { return false; }

            return true;
        }

        @expector
        private expectDeclare(): boolean
        {
            if (!this.expectToken("keyword", "declare")) { return false; }
            return true;
        }

        @expector
        private expectDeclaration(allowDeclare = false): boolean
        {
            const withExport = this.expectToken("keyword", "export");
            const withDeclare = !withExport && allowDeclare && this.expectDeclare();
            if (withExport && !withDeclare && this.expectToken("keyword", "default"))
            {
                const exportDefaultType = this.expectTypeRef();
                if (!exportDefaultType) { this.fail(); }
                if (!this.expectToken("semiColon")) { this.fail(); }
                return true;
            }
            if (this.expectNamespace(withDeclare)) { return true; }
            if (this.expectInterface()) { return true; }
            if (this.expectClass()) { return true; }
            if (this.expectEnum()) { return true; }
            if (this.expectType()) { return true; }
            if (this.expectImport()) { return true; }
            if (this.expectVariable()) { return true; }
            if (this.expectFunction()) { return true; }
            if (this.expectModule()) { return true; }
            if (withExport)
            {
                if (this.expectAsNamespace()) { return true; }
                // new TS sometimes generates "export {};" from the same code ¯\_(ツ)_/¯
                if (this.expectToken("bracket", "{") && this.expectToken("bracket", "}"))
                {
                    // Optional semicolon
                    this.expectToken("semiColon");
                    return true;
                }
                this.fail();
            }
            return false;
        }

        @expector
        private expectList(predicator: () => boolean, listSepName: string = "comma", listSepLexeme?: string, allowDangling: boolean = false): boolean
        {
            return this.expectListEx(predicator, () =>
            {
                if (listSepName != null)
                {
                    return this.expectToken(listSepName, listSepLexeme) != null;
                }
                else
                {
                    return true;
                }
            }, allowDangling);
        }

        @expector
        private expectListEx(itemPredicator: () => boolean, delimiterPredicator: () => boolean, allowDangling: boolean = false): boolean
        {
            do
            {
                this._tokenStream.pushPosition("expectListLoop");
                if (itemPredicator())
                {
                    this._tokenStream.takePosition("expectListLoop");
                }
                else
                {
                    this._tokenStream.popPosition("expectListLoop");
                    if (allowDangling)
                    {
                        delimiterPredicator();
                    }
                    return true;
                }
            } while (delimiterPredicator());
            return true;
        }

        @expector
        private expectNamespace(wasDeclared: boolean): boolean | null
        {
            const identifierList: string[] = [];

            if (wasDeclared && this.expectToken("keyword", "global"))
            {
                identifierList.push("global");
            }
            else
            {
                if (!this.expectToken("keyword", "namespace")) { return false; }
                if (!this.expectList(() =>
                {
                    const identifier = this.expectIdentifier();
                    if (!identifier) { return false; }
                    identifierList.push(identifier.lexeme);
                    return true;
                }, "dot")) { return false; }
            }
            if (!this.expectToken("bracket", "{")) { return false; }
            for (let i = 0; i < identifierList.length; i++) { this._currentNamespace.push(identifierList[i]); }
            let expectDec = this.expectDeclaration();
            while (expectDec) {
                expectDec = this.expectDeclaration();
            }
            for (let i = 0; i < identifierList.length; i++) { this._currentNamespace.pop(); }
            if (!this.expectToken("bracket", "}"))
            {
                this.fail();
                return false;
            }
            while (this._currentAsNamespace > 0)
            {
                this._currentNamespace.pop();
                this._currentAsNamespace--;
            }
            return true;
        }

        @expector
        private expectInterface(): boolean | null
        {
            if (!this.expectToken("keyword", "interface")) { return false; }
            const identifier = this.expectIdentifier();
            if (!identifier)
            {
                this.fail();
                return false;
            }

            const genericTypeArgs = this.expectGenericTypeArgs();

            const extendsList: TypeRef[] = [];
            if (this.expectToken("keyword", "extends"))
            {
                if (!this.expectList(() =>
                {
                    const type = this.expectTypeRef();
                    if (type == null) { return false; }
                    extendsList.push(type);
                    return true;
                }))
                {
                    this.fail();
                    return false;
                }
            }

            if (!this.expectToken("bracket", "{")) { this.fail(); return false; }

            const interfaceType = this.findOrCreateType("interface", identifier.lexeme);
            interfaceType.genericTypeArgs = genericTypeArgs;
            if (extendsList.length > 0) { interfaceType.extends = extendsList; }
            this._currentPropertyHolder = this._currentMethodHolder = interfaceType;

            if (!this.expectPropertySet(true))
            {
                this.fail();
                return false;
            }

            this._currentPropertyHolder = this._currentMethodHolder = null;

            if (!this.expectOnlyToken("bracket", "}")) { this.fail(); return false; }
            return true;
        }

        @expector
        private expectClass(): boolean | null
        {
            const isAbstract = this.expectToken("keyword", "abstract") != null;

            if (!this.expectToken("keyword", "class")) { return false; }
            const identifier = this.expectIdentifier();
            if (!identifier) { return false; }

            const genericTypeArgs = this.expectGenericTypeArgs();

            const extendsList: TypeRef[] = [];
            if (this.expectToken("keyword", "extends"))
            {
                if (!this.expectList(() =>
                {
                    const type = this.expectTypeRef();
                    if (type == null) { return false; }
                    extendsList.push(type);
                    return true;
                })) { return null; }
            }

            const implementsList: TypeRef[] = [];
            if (this.expectToken("keyword", "implements"))
            {
                if (!this.expectList(() =>
                {
                    const type = this.expectTypeRef();
                    if (type == null) { return false; }
                    implementsList.push(type);
                    return true;
                })) { return null; }
            }

            if (!this.expectToken("bracket", "{")) { return false; }

            const classType = this.findOrCreateType("class", identifier.lexeme);
            classType.genericTypeArgs = genericTypeArgs;
            classType.isAbstract = isAbstract;
            if (extendsList.length > 0) { classType.extends = extendsList; }
            if (implementsList.length > 0) { classType.implements = implementsList; }
            this._currentPropertyHolder = this._currentMethodHolder = classType;

            if (!this.expectPropertySet(false)) { return false; }

            this._currentPropertyHolder = this._currentMethodHolder = null;

            if (!this.expectOnlyToken("bracket", "}")) { return false; }
            return true;
        }

        @expector
        private expectEnum(): boolean | null
        {
            const isConst = this.expectToken("keyword", "const") != null;
            if (!this.expectToken("keyword", "enum")) { return false; }
            const enumName = this.expectIdentifier();
            if (enumName == null) { return false; }
            if (!this.expectToken("bracket", "{")) { return false; }
            const enumerators: { [name: string]: number|string } = {};
            let nextEnumValue = 0;
            if (!this.expectList(() =>
            {
                const enumeratorName = this.expectIdentifier();
                if (enumeratorName == null) { return false; }

                if (this.expectToken("assign"))
                {
                    const value = this.expectToken("numberLiteral") || this.expectToken("stringLiteral");
                    if (value == null) { return false; }
                    if (value.name === "stringLiteral")
                    {
                        enumerators[enumeratorName.lexeme] = value.lexeme.substr(1, value.lexeme.length - 2);
                    }
                    else
                    {
                        nextEnumValue = (enumerators[enumeratorName.lexeme] = parseFloat(value.lexeme)) + 1;
                    }
                }
                else
                {
                    enumerators[enumeratorName.lexeme] = nextEnumValue++;
                }

                return true;
            }, "comma", undefined, true)) { return false; }
            if (!this.expectToken("bracket", "}")) { return false; }

            const type = this.findOrCreateType("enum", enumName.lexeme);
            type.isConst = isConst;
            type.enumerators = enumerators;

            return true;
        }

        @expector
        private expectType(): boolean | null
        {
            if (!this.expectToken("keyword", "type")) { return false; }
            const typeName = this.expectIdentifier();
            if (typeName == null) { return false; }
            const genericTypeArgs = this.expectGenericTypeArgs();
            if (!this.expectToken("assign")) { return false; }
            const typeRef = this.expectTypeRef();
            if (!typeRef) { return false; }

            // Optional semicolon
            this.expectToken("semiColon");

            const type = this.findOrCreateType("type", typeName.lexeme);
            type.type = typeRef;
            type.genericTypeArgs = genericTypeArgs;

            return true;
        }

        @expector
        private expectImport(): boolean | null
        {
            if (!this.expectToken("keyword", "import")) { return false; }

            // import X = Y;
            // import * as X from "Y";

            if (this.expectToken("asterisk"))
            {
                if (!this.expectToken("keyword", "as")) { return false; }
                const asIdentifier = this.expectIdentifier();
                if (asIdentifier == null) { return false; }
                if (!this.expectToken("keyword", "from")) { return false; }
                const fromStr = this.expectToken("stringLiteral");
                if (fromStr == null) { return false; }
            }
            else
            {
                const identifier = this.expectIdentifier();
                if (identifier == null) { return false; }
                if (!this.expectToken("assign")) { return false; }
                const typeRef = this.expectTypeRef();
                if (!typeRef) { return false; }
            }
            this.expectToken("semiColon");

            // TODO: Store it

            return true;
        }

        @expector
        private expectSpread(): boolean | null
        {
            return (this.expectToken("dot") && this.expectToken("dot") && this.expectToken("dot")) ? true : false;
        }

        @expector
        private expectPropertySet(inInterface: boolean): boolean | null
        {
            while (this.expectPropertySetInner(inInterface))
            {
                const token = this.expectToken("comma") || this.expectToken("semiColon");
            }
            return true;
        }

        @expector
        private expectPropertySetInner(inInterface: boolean): boolean
        {
            if (this.expectMethod(inInterface)) { return true; }
            if (this.expectIndexSignature()) { return true; }
            if (this.expectCallSignature()) { return true; }
            if (this.expectProperty(inInterface)) { return true; }
            if (!inInterface && this.expectComputedProperty()) { return true; }
            return false;
        }

        @expector
        private expectAccessQualifier(): AccessQualifier | null
        {
            const qualifier = this.expectToken("keyword");
            if (qualifier == null) { return null; }
            switch (qualifier.lexeme)
            {
                case "private":
                    return AccessQualifier.Private;
                case "protected":
                    return AccessQualifier.Protected;
                case "public":
                    return AccessQualifier.Public;
                default:
                    return null;
            }
        }

        @expector
        private expectStorageQualifier(): StorageQualifier | null
        {
            const qualifier = this.expectToken("keyword");
            if (qualifier == null) { return null; }
            switch (qualifier.lexeme)
            {
                case "static":
                    return StorageQualifier.Static;
                default:
                    return null;
            }
        }

        @expector
        private expectAbstractQualifier(): boolean
        {
            const qualifier = this.expectToken("keyword");
            if (qualifier == null) { return null; }
            switch (qualifier.lexeme)
            {
                case "abstract":
                    return true;
                default:
                    return false;
            }
        }

        @expector
        private expectProperty(inInterface: boolean): boolean | null
        {
            const accessQualifier = inInterface ? null : this.expectAccessQualifier();
            const storageQualifier = inInterface ? null : this.expectStorageQualifier();

            const otherQualifiers = this.expectTokens("keyword", ["abstract", "readonly"]);

            let isAbstract = false, isReadonly = false;
            if (otherQualifiers)
            {
                for (const qual of otherQualifiers)
                {
                    switch (qual.lexeme)
                    {
                        case "abstract":
                            isAbstract = true;
                            break;

                        case "readonly":
                            isReadonly = true;
                            break;
                    }

                    // Nothing left to discover
                    if (isAbstract && isReadonly) { break; }
                }
            }

            // Property name
            const identifier = this.expectIdentifier() || this.expectToken("stringLiteral") || this.expectToken("numberLiteral");
            if (!identifier) { return false; }
            const optional = this.expectToken("questionMark") != null;

            // Property type
            let typeRef: TypeRef | null = null;
            if (this.expectToken("colon"))
            {
                typeRef = this.expectTypeRef();
                if (!typeRef) { return false; }
            }

            this.expectAssignment();

            // Insert
            if (!this._currentPropertyHolder.properties) { this._currentPropertyHolder.properties = []; }
            this._currentPropertyHolder.properties.push({
                kind: "propertydeclr",
                isAbstract, isReadonly,
                name: identifier.lexeme,
                accessQualifier, storageQualifier,
                optional,
                type: typeRef
            } as PropertyDeclaration);
            return true;
        }

        @expector
        private expectComputedProperty(): boolean | null
        {
            let getter = this.expectComputedPropertyGetter(), setter: CodeMeta.MethodDeclaration;
            if (getter)
            {
                setter = this.expectComputedPropertySetter(getter.name);
            }
            else
            {
                setter = this.expectComputedPropertySetter();
                if (setter)
                {
                    getter = this.expectComputedPropertyGetter(setter.name);
                }
            }

            /** Source of common info. */
            const info = getter || setter;
            if (!info) { return false; }

            if (!this._currentPropertyHolder.properties) { this._currentPropertyHolder.properties = []; }
            this._currentPropertyHolder.properties.push({
                kind: "propertydeclr",
                isAbstract: false,
                isReadonly: getter && !setter,
                name: info.name,
                accessQualifier: info.accessQualifier,
                storageQualifier: info.storageQualifier,
                optional: info.optional,
                type: info.signature.returnType

            } as PropertyDeclaration);

            return true;
        }

        @expector
        private expectComputedPropertyGetter(name?: string): CodeMeta.MethodDeclaration
        {
            const result: CodeMeta.MethodDeclaration =
            {
                name: null,
                isAbstract: false,
                signature:
                {
                    isConstructor: false,
                    genericTypeArgs: [],
                    parameters: null,
                    returnType: null
                }
            };

            result.accessQualifier = this.expectAccessQualifier();
            result.storageQualifier = this.expectStorageQualifier();

            const getterToken = this.expectToken("keyword", "get");
            if (!getterToken) { return null; }

            const identifier = this.expectIdentifier();
            if (!identifier) { return null; }
            if (name && identifier.lexeme !== name) { return null; }
            result.name = identifier.lexeme;

            result.optional = this.expectToken("questionMark") != null;

            const params = this.expectParameterList();
            if (!params || params.length !== 0) { return null; }
            result.signature.parameters = params;

            if (this.expectToken("colon"))
            {
                result.signature.returnType = this.expectTypeRef();
                if (!result.signature.returnType) { return null; }
            }

            return result;
        }

        @expector
        private expectComputedPropertySetter(name?: string): CodeMeta.MethodDeclaration
        {
            const result: CodeMeta.MethodDeclaration =
            {
                name: null,
                isAbstract: false,
                signature:
                {
                    isConstructor: false,
                    genericTypeArgs: [],
                    parameters: null,
                    returnType: null
                }
            };

            result.accessQualifier = this.expectAccessQualifier();
            result.storageQualifier = this.expectStorageQualifier();

            const setterToken = this.expectToken("keyword", "set");
            if (!setterToken) { return null; }

            const identifier = this.expectIdentifier();
            if (!identifier) { return null; }
            if (name && identifier.lexeme !== name) { return null; }
            result.name = identifier.lexeme;

            result.optional = this.expectToken("questionMark") != null;

            const params = this.expectParameterList();
            if (!params || params.length !== 1) { return null; }
            result.signature.parameters = params;

            if (this.expectToken("colon"))
            {
                result.signature.returnType = this.expectTypeRef();
                if (!result.signature.returnType) { return null; }
            }

            return result;
        }

        /** Expects an assignment e.g. `= 2` */
        @expector
        private expectAssignment(): boolean
        {
            const assignToken = this.expectToken("assign");
            if (!assignToken) { return false; }

            const value =
                this.expectToken("booleanLiteral") ||
                this.expectToken("numberLiteral") ||
                this.expectToken("stringLiteral") ||
                this.expectReference();
            if (!value) { return false; }

            return true;
        }

        /** Expects a reference to an object, enum, function, etc. e.g. `RS.PlatformGameState`. */
        @expector
        private expectReference(): boolean
        {
            return this.expectList(() =>
            {
                const identifier = this.expectIdentifier();
                if (!identifier) { return false; }
                return true;
            }, "dot");
        }

        @expector
        private expectGenericTypeArgs(): CodeMeta.GenericTypeArg[] | null
        {
            if (!this.expectToken("typeBracket", "<")) { return null; }
            const args: CodeMeta.GenericTypeArg[] = [];
            if (!this.expectList(() =>
            {
                const identifier = this.expectIdentifier();
                if (identifier == null) { return false; }
                const typeArg: CodeMeta.GenericTypeArg = { name: identifier.lexeme };
                if (this.expectToken("keyword", "extends"))
                {
                    const extendsType = this.expectTypeRef();
                    if (!extendsType) { return false; }
                    typeArg.extends = extendsType;
                }
                if (this.expectToken("assign"))
                {
                    const defType = this.expectTypeRef();
                    if (defType == null) { return false; }
                    typeArg.default = defType;
                }
                args.push(typeArg);
                return true;
            })) { return null; }
            if (!this.expectToken("typeBracket", ">")) { return null; }
            if (args.length === 0) { return null; }
            return args;
        }

        @expector
        private expectGenericTypeParams(): TypeRef[] | null
        {
            if (!this.expectToken("typeBracket", "<")) { return null; }
            const args: TypeRef[] = [];
            if (!this.expectList(() =>
            {
                // Optional infer
                this.expectToken("keyword", "infer");

                const typeRef = this.expectTypeRef();
                if (!typeRef) { return false; }
                args.push(typeRef);
                return true;
            })) { return null; }
            if (!this.expectToken("typeBracket", ">")) { return null; }
            if (args.length === 0) { return null; }
            return args;
        }

        @expector
        private expectFunction(): boolean | null
        {
            if (!this.expectToken("keyword", "function")) { return false; }
            const identifier = this.expectIdentifier();
            if (!identifier) { return false; }

            const typeArgs = this.expectGenericTypeArgs();

            // Parameter list
            const params = this.expectParameterList();
            if (!params) { return false; }

            let returnTypeRef: TypeRef | null;
            if (this.expectToken("colon"))
            {
                returnTypeRef = this.expectTypeRef();
                if (!returnTypeRef) { return false; }
            }

            // Optional semicolon
            this.expectToken("semiColon");

            const ns = this.getOrCreateNamespace();
            if (ns.children == null) { ns.children = []; }

            ns.children.push({
                kind: "func",
                name: identifier.lexeme,
                genericTypeArgs: typeArgs,
                signature: {
                    isConstructor: false,
                    parameters: params,
                    returnType: returnTypeRef
                }
            } as FunctionDeclaration);

            return true;
        }

        @expector
        private expectMethod(inInterface: boolean): boolean | null
        {
            const accessQualifier = inInterface ? null : this.expectAccessQualifier();
            const isAbstract = inInterface ? null : this.expectAbstractQualifier();
            const storageQualifier = inInterface ? null : this.expectStorageQualifier();

            // Read identifier
            const identifier = this.expectIdentifier();
            if (!identifier) { return false; }

            // Read any type args
            const typeArgs = this.expectGenericTypeArgs();

            // Could be an optional method
            let optional = false;
            if (this.expectToken("questionMark"))
            {
                optional = true;
            }

            // Parameter list
            const params = this.expectParameterList();
            if (!params) { return false; }

            let returnTypeRef: TypeRef | null;
            if (this.expectToken("colon"))
            {
                returnTypeRef = this.expectTypeRef();
                if (!returnTypeRef) { return false; }
            }

            if (!this._currentMethodHolder.methods) { this._currentMethodHolder.methods = []; }

            this._currentMethodHolder.methods.push({
                kind: "method",
                name: identifier.lexeme,
                isAbstract,
                accessQualifier, storageQualifier, optional,
                isConstructor: identifier.lexeme === "constructor",
                genericTypeArgs: typeArgs,
                signature: {
                    isConstructor: false,
                    parameters: params,
                    returnType: returnTypeRef
                }
            } as MethodDeclaration);

            return true;
        }

        @expector
        private expectParameterList(): CodeMeta.FunctionParameter[] | null
        {
            if (!this.expectToken("bracket", "(")) { return null; }

            const params: CodeMeta.FunctionParameter[] = [];
            if (!this.expectList(() =>
            {
                const isSpread = this.expectSpread();

                let name: string;
                const identifier = this.expectIdentifier();
                if (identifier)
                {
                    name = identifier.lexeme;
                }
                else
                {
                    const unpack = this.expectDestructuringObjectAssignment() || this.expectDestructuringArrayAssignment();
                    if (!unpack) { return false; }
                    name = this.makeTempVariableName(params.length);
                }

                const optional = this.expectToken("questionMark") != null;

                let typeRef: TypeRef | null = null;
                if (this.expectToken("colon"))
                {
                    typeRef = this.expectTypeRef();
                    if (typeRef == null) { return false; }
                }

                params.push({ name, optional, isSpread, type: typeRef });
                return true;
            })) { return null; }

            if (!this.expectToken("bracket", ")")) { return null; }

            return params;
        }

        /** Makes up a name given an index of the thing for e.g. a destructuring assignment. */
        private makeTempVariableName(index: number)
        {
            return index < 26
                ? `_${String.fromCharCode(97 /* a */ + index)}`
                : `_${index - 26}`;
        }

        @expector
        private expectCallSignature(): boolean | null
        {
            const isConstructor = this.expectToken("keyword", "new") != null;

            const genericTypeArgs = this.expectGenericTypeArgs();

            // Parameter list
            const params = this.expectParameterList();
            if (params == null) { return false; }

            let returnTypeRef: TypeRef | null;
            if (this.expectToken("colon"))
            {
                returnTypeRef = this.expectTypeRef();
                if (!returnTypeRef) { return false; }
            }

            // TODO: Store this call signature somewhere

            return true;
        }

        @expector
        private expectIndexSignature(): boolean | null
        {
            this.expectToken("keyword", "readonly");
            if (!this.expectToken("bracket", "[")) { return false; }
            const identifier = this.expectIdentifier();
            if (!identifier) { return false; }
            let keyTypeRef: TypeRef | null;
            if (this.expectToken("colon"))
            {
                keyTypeRef = this.expectTypeRef();
                if (!keyTypeRef) { return false; }
            }
            else if (this.expectToken("keyword", "in"))
            {
                const inType = this.expectTypeRef();
                if (inType == null) { return false; }
            }
            if (!this.expectToken("bracket", "]")) { return false; }
            const optional = this.expectToken("questionMark") != null;
            if (!this.expectToken("colon")) { return false; }

            const valueTypeRef = this.expectTypeRef();
            if (valueTypeRef == null) { return false; }

            // TODO: Store this somewhere

            return true;
        }

        @expector
        private expectVariable(): boolean | null
        {
            const qualifier = this.expectToken("keyword");
            if (qualifier == null) { return false; }
            if (qualifier.lexeme !== "var" && qualifier.lexeme !== "let" && qualifier.lexeme !== "const") { return false; }

            const vars: Variable[] = [];

            if (!this.expectList(() =>
            {
                const identifier = this.expectIdentifier();
                if (!identifier) { return false; }

                let typeRef: TypeRef | null = null;
                if (this.expectToken("colon"))
                {
                    typeRef = this.expectTypeRef();
                    if (!typeRef) { return false; }
                }

                let value: Value | null = null;
                if (this.expectToken("assign"))
                {
                    value = this.expectExpression();
                    if (!value) { return false; }
                }

                vars.push({
                    kind: "var",
                    name: identifier.lexeme,
                    type: typeRef,
                    isConst: qualifier.lexeme === "const",
                    value
                });

                return true;
            }))
            {
                this.fail();
                return false;
            }

            if (!this.expectToken("semiColon")) { return false; }

            const ns = this.getOrCreateNamespace();

            if (!ns.children) { ns.children = []; }
            for (const varObj of vars)
            {
                ns.children.push(varObj);
            }
            return true;
        }

        @expector
        private expectExpression(): Value | null
        {
            // Look for literals
            let token: Token | null;
            token = this.expectToken("stringLiteral") ||  this.expectToken("numberLiteral") || this.expectToken("booleanLiteral");
            if (token)
            {
                return token.lexeme as Value;
            }

            // TODO: Handle more complex expressions if needed?

            return null;
        }

        @expector
        private expectCallbackType(): TypeRef | null
        {
            const isConstructor = this.expectToken("keyword", "new") != null;

            const genericTypeArgs = this.expectGenericTypeArgs();

            // Parameter list
            const params = this.expectParameterList();
            if (params == null) { return null; }

            if (!this.expectToken("fatArrow")) { return null; }

            const returnType = this.expectTypeRef();
            if (returnType == null) { return null; }

            return {
                kind: TypeRef.Kind.Function,
                signature: {
                    isConstructor,
                    parameters: params,
                    returnType
                }
            };
        }

        @expector
        private expectTypeRefNoIntersectOrUnion(): TypeRef | null
        {
            let assembledType: TypeRef = null;

            // Optional readonly
            this.expectToken("keyword", "readonly");

            // Check for typeof type
            let token: Token;
            if (this.expectToken("keyword", "typeof"))
            {
                const inner = this.expectTypeRefNoIntersectOrUnion();
                if (inner == null) { return null; }
                assembledType = {
                    kind: TypeRef.Kind.TypeOf,
                    inner
                };
            }
            // Check for keyof type
            else if (this.expectToken("keyword", "keyof"))
            {
                const inner = this.expectTypeRefNoIntersectOrUnion();
                if (inner == null) { return null; }
                assembledType = {
                    kind: TypeRef.Kind.KeyOf,
                    inner
                };
            }
            // Check for inline type
            else if (this.expectToken("bracket", "{"))
            {
                const oldPropertyHolder = this._currentPropertyHolder;
                const oldMethodHolder = this._currentMethodHolder;

                this._currentPropertyHolder = { properties: [] };
                this._currentMethodHolder = { methods: [] };
                if (!this.expectPropertySet(true)) { return null; }

                if (!this.expectToken("bracket", "}")) { return null; }

                assembledType = {
                    kind: TypeRef.Kind.Inline,
                    properties: this._currentPropertyHolder.properties,
                    methods: this._currentMethodHolder.methods
                };

                this._currentPropertyHolder = oldPropertyHolder;
                this._currentMethodHolder = oldMethodHolder;
            }
            // Check for callback type
            else if (assembledType = this.expectCallbackType())
            {
                return assembledType;
            }
            // Check for bracketed type
            else if (this.expectToken("bracket", "("))
            {
                assembledType = this.expectTypeRef();
                if (!this.expectToken("bracket", ")")) { return null; }
            }
            // Check for fixed array type
            else if (this.expectToken("bracket", "["))
            {
                const inners: TypeRef[] = [];
                if (!this.expectList(() =>
                {
                    const typeRef = this.expectTypeRef();
                    if (!typeRef) { return false; }
                    inners.push(typeRef);
                    return true;
                })) { return null; }
                if (!this.expectToken("bracket", "]")) { return null; }
                assembledType = {
                    kind: TypeRef.Kind.FixedArray,
                    inners
                };
            }
            // Check for number literal
            else if (token = this.expectToken("numberLiteral"))
            {
                assembledType = {
                    kind: TypeRef.Kind.LiteralNumber,
                    value: parseFloat(token.lexeme)
                };
            }
            // Check for string literal
            else if (token = this.expectToken("stringLiteral"))
            {
                assembledType = {
                    kind: TypeRef.Kind.LiteralString,
                    value: token.lexeme
                };
            }
            // Check for bool literal
            else if (token = this.expectToken("booleanLiteral"))
            {
                assembledType = {
                    kind: TypeRef.Kind.LiteralBool,
                    value: token.lexeme === "true" ? true : false
                };
            }
            // Must be identifier, maybe namespaced (e.g. "RS.Slots.Reels")
            else
            {
                const identifierList: string[] = [];
                if (!this.expectList(() =>
                {
                    const identifier = this.expectIdentifier();
                    if (!identifier) { return false; }
                    identifierList.push(identifier.lexeme);
                    return true;
                }, "dot")) { return null; }

                // If there's only ONE identifier, check for a special "is" type
                if (identifierList.length === 1 && this.expectToken("keyword", "is"))
                {
                    const typeRef = this.expectTypeRef();
                    if (!typeRef) { return null; }
                    assembledType = {
                        kind: TypeRef.Kind.Is,
                        parameterName: identifierList[0],
                        inner: typeRef
                    };

                    // "is" types can't be arrayed since that would be weird, so return here
                    return assembledType;
                }
                else
                {
                    // Look for generic?
                    const genericTypes = this.expectGenericTypeParams();

                    // Assemble
                    if (genericTypes != null && genericTypes.length > 0)
                    {
                        assembledType = {
                            kind: TypeRef.Kind.WithTypeArgs,
                            inner: { kind: TypeRef.Kind.Raw, name: identifierList.join(".") },
                            genericTypeArgs: genericTypes
                        };
                    }
                    else
                    {
                        assembledType = {
                            kind: TypeRef.Kind.Raw,
                            name: identifierList.join(".")
                        };
                    }
                }
            }

            if (assembledType == null) { return null; }

            // Check for array
            if (!this.expectList(() =>
            {
                if (this.expectIndexWithoutType())
                {
                    assembledType = {
                        kind: TypeRef.Kind.Array,
                        inner: assembledType
                    };
                    return true;
                }
                else
                {
                    const indexType = this.expectIndexWithType();
                    if (indexType)
                    {
                        assembledType = {
                            kind: TypeRef.Kind.Indexed,
                            index: indexType,
                            inner: assembledType
                        };
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }, null, null)) { return null; }

            // Done
            return assembledType;
        }

        @expector
        private expectIndexWithoutType(): boolean
        {
            if (!this.expectToken("bracket", "[")) { return false; }
            if (!this.expectToken("bracket", "]")) { return false; }
            return true;
        }

        @expector
        private expectIndexWithType(): TypeRef | null
        {
            if (!this.expectToken("bracket", "[")) { return null; }

            const index = this.expectTypeRef();
            if (!index) { return null; }

            if (!this.expectToken("bracket", "]")) { return null; }

            return index;
        }

        @expector
        private expectTypeRefNoIntersect(): TypeRef | null
        {
            const assembledType = this.expectTypeRefNoIntersectOrUnion();
            if (assembledType == null) { return null; }
            if (this.expectToken("unionOrIntersect", "|"))
            {
                const intersectTypes: TypeRef[] = [];
                if (!this.expectList(() =>
                {
                    const nextType = this.expectTypeRefNoIntersectOrUnion();
                    if (nextType == null) { return null; }
                    intersectTypes.push(nextType);
                    return true;
                }, "unionOrIntersect", "|")) { return null; }

                return {
                    kind: TypeRef.Kind.Union,
                    inners: [assembledType, ...intersectTypes]
                };
            }
            return assembledType;
        }

        @expector
        private expectTypeRefNoConditional(): TypeRef | null
        {
            const assembledType = this.expectTypeRefNoIntersect();
            if (assembledType == null) { return null; }
            if (this.expectToken("unionOrIntersect", "&"))
            {
                const intersectTypes: TypeRef[] = [];
                if (!this.expectList(() =>
                {
                    const nextType = this.expectTypeRefNoIntersect();
                    if (nextType == null) { return null; }
                    intersectTypes.push(nextType);
                    return true;
                }, "unionOrIntersect", "&")) { return null; }

                return {
                    kind: TypeRef.Kind.Intersect,
                    inners: [assembledType, ...intersectTypes]
                };
            }
            return assembledType;
        }

        @expector
        private expectTypeRef(): TypeRef | null
        {
            const subject = this.expectTypeRefNoConditional();

            if (!this.expectToken("keyword", "extends")) { return subject; }

            const condition = this.expectTypeRef();
            if (!condition) { return null; }

            if (!this.expectToken("questionMark")) { return null; }

            const pass = this.expectTypeRef();
            if (!pass) { return null; }

            if (!this.expectToken("colon")) { return null; }

            const fail = this.expectTypeRef();
            if (!fail) { return null; }

            return {
                kind: TypeRef.Kind.Conditional,
                subject,
                condition,
                pass,
                fail
            };
        }

        @expector
        private expectModule(): boolean
        {
            const identifierList: string[] = [];

            if (!this.expectToken("keyword", "module")) { return false; }
            if (!this.expectList(() =>
            {
                const identifier = this.expectToken("stringLiteral") || this.expectIdentifier();
                if (!identifier) { return false; }
                if (identifier.name === "stringLiteral")
                {
                    identifierList.push(identifier.lexeme.substr(1, identifier.lexeme.length - 2));
                }
                else
                {
                    identifierList.push(identifier.lexeme);
                }
                return true;
            }, "dot")) { return false; }

            if (!this.expectToken("bracket", "{")) { return false; }
            for (let i = 0; i < identifierList.length; i++) { this._currentNamespace.push(identifierList[i]); }

            let expectVal = this.expectExportAssign() || this.expectDeclaration();
            while (expectVal) {
                expectVal = this.expectExportAssign() || this.expectDeclaration();
            }
            this._currentNamespace.pop();

            if (!this.expectToken("bracket", "}")) { this.fail(); return false; }

            return true;
        }

        @expector
        private expectExportAssign(): boolean
        {
            if (!this.expectToken("keyword", "export")) { return false; }
            if (!this.expectToken("assign")) { return false; }

            const exported = this.expectIdentifier();
            if (!exported) { this.fail(); return false; }

            this.expectToken("semiColon");

            return true;
        }

        @expector
        private expectAsNamespace(): boolean
        {
            if (!this.expectToken("keyword", "as")) { return false; }
            if (!this.expectToken("keyword", "namespace")) { this.fail(); return false; }

            const identifierList: string[] = [];
            if (!this.expectList(() =>
            {
                const identifier = this.expectIdentifier();
                if (!identifier) { return false; }
                identifierList.push(identifier.lexeme);
                return true;
            }, "dot"))
            {
                this.fail();
                return false;
            }

            // Remove any existing "export as namespace" directives
            while (this._currentAsNamespace > 0)
            {
                this._currentNamespace.pop();
                this._currentAsNamespace--;
            }

            for (const identifier of identifierList)
            {
                this._currentNamespace.push(identifier);
                this._currentAsNamespace++;
            }

            this.expectToken("semiColon");

            return true;
        }

        private getOrCreateNamespace(): Namespace
        {
            let ns = this._data.globalNamespace || (this._data.globalNamespace = { kind: "namespace", name: "global", children: [] });

            // Resolve/create namespaces
            for (let i = 0; i < this._currentNamespace.length; i++)
            {
                const nsName = this._currentNamespace[i];

                let found = false;
                if (ns.children)
                {
                    for (let j = 0; j < ns.children.length; j++)
                    {
                        const subNamed = ns.children[j];
                        if (subNamed.kind === "namespace" && subNamed.name === nsName)
                        {
                            ns = subNamed;
                            found = true;
                            break;
                        }
                    }
                }

                if (!found)
                {
                    const nsNew: Namespace = { kind: "namespace", name: nsName, children: [] };
                    ns.children.push(nsNew);
                    ns = nsNew;
                }
            }

            return ns;
        }

        private findOrCreateType(kind: "interface", name: string): Interface;
        private findOrCreateType(kind: "class", name: string): Class;
        private findOrCreateType(kind: "enum", name: string): Enum;
        private findOrCreateType(kind: "type", name: string): Type;

        private findOrCreateType(kind: string, name: string): Named
        {
            const ns = this.getOrCreateNamespace();

            // Locate type
            if (ns.children)
            {
                for (let i = 0; i < ns.children.length; i++)
                {
                    const child = ns.children[i];
                    if (child.kind === kind && child.name === name)
                    {
                        return child;
                    }
                }
            }

            // Create type
            const newType = { kind, name } as Named;
            if (!ns.children) { ns.children = []; }
            ns.children.push(newType);
            return newType;
        }
    }
}
