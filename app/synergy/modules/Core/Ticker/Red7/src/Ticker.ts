/** @internal */
namespace RS
{
    const requestAnimationFrame: typeof window.requestAnimationFrame =
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window["mozRequestAnimationFrame"] ||
        window["oRequestAnimationFrame"] ||
        window["msRequestAnimationFrame"];

    interface Handler extends Ticker.HandlerData
    {
        callback: Ticker.Callback;
        paused: boolean;
        remove: boolean;
        runTime: number;
        nextCall: number | null;
    }

    const tmpEvent: Ticker.Event =
    {
        delta: 0,
        realDelta: 0,
        paused: false,
        singleStep: false,
        runTime: 0
    };

    type Behaviour = (handler: Handler, timestamp: number) => Ticker.Result;

    const Behaviour = Decorators.Tag.create<RS.Ticker.Kind>(Decorators.TagKind.Method);

    class TickerHandle implements IDisposable
    {
        private _isDisposed: boolean = false;

        public get isDisposed() { return this._isDisposed; }

        public constructor(public readonly ticker: Ticker, public readonly handler: Handler) { }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this.ticker.removeHandler(this.handler);
            this._isDisposed = true;
        }
    }

    @HasCallbacks
    class Ticker implements ITicker
    {
        private _timer = new RS.Timer();
        private _timeScale: number = 1;
        private _singleStep: boolean = false;
        private _stepped: boolean = false;
        private _paused: boolean = false;
        private _tickers: Handler[] = [];
        private _lastTime: number = Timer.now;
        private _maxDelta: number = 50;
        private _defaultTimeStep: number = 1 / 60;
        private _inTick: boolean = false;
        private _animFrameRequested: boolean = false;
        private _enabled: boolean = false;
        private _manualAdvance: number = 0;

        private _behaviours: Behaviour[];

        /**
         * Gets or sets if the ticker is enabled.
         * This is different from paused - the ticker will not run at all when disabled.
         */
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            if (value === this._enabled) { return; }
            this._enabled = value;
            if (value && !this._animFrameRequested)
            {
                requestAnimationFrame(this.tick);
                this._animFrameRequested = true;
            }
            this._timer.paused = !value;
        }

        /**
         * Pauses any pausable tickers, non-pausable tickers will still be processed.
         */
        public get paused() { return this._paused; }
        public set paused(value) { this._paused = value; }

        /**
         * Controls the use of single step mode, when in single step normal tickers will
         * only step on when RS.Ticker.step is called.
         */
        public get singleStep() { return this._singleStep; }
        public set singleStep(value) { this._singleStep = value; }

        /**
         * timeStep is a multiple of the current time scale value, setting to less than 1 will
         * slow time down, setting to greater than 1 will speed time up, note that this may have
         * unpredicatible consequences.
         */
        public get timeScale() { return this._timeScale; }
        public set timeScale(value) { this._timeScale = value; }

        /**
         * The default fixed Time Step
         */
        public get targetTimestep() { return this._defaultTimeStep; }
        public set targetTimestep(value) { this._defaultTimeStep = value; }

        public get targetFramerate() { return 1000 / this._defaultTimeStep; }
        public set targetFramerate(value) { this._defaultTimeStep = 1000 / value; }

        /**
         * The maximum time between frames at 1x speed in milliseconds, default is 50.
         */
        public get maxDelta() { return this._maxDelta; }
        public set maxDelta(max: number) { this._maxDelta = max; }

        public constructor()
        {
            this._behaviours = [];
            const behaviourMap = Behaviour.get(this, Decorators.MemberType.Instance);
            for (const key in behaviourMap)
            {
                this._behaviours[behaviourMap[key]] = (this[key] as ()=>void).bind(this);
            }
            this._inTick = false;
            this.enabled = true;
        }

        /**
         * Adds a simple ticker handler with the default priority and kind.
         * @param callback
         * @returns a handle which can be disposed to remove the ticker handler.
         */
        public add(callback: Ticker.Callback): IDisposable;

        /**
         * Adds a ticker handler with the specified priority and default kind.
         * @param callback
         * @param settings
         * @returns a handle which can be disposed to remove the ticker handler.
         */
        public add(callback: Ticker.Callback, settings: Partial<Ticker.HandlerData>): IDisposable;

        public add(callback: Ticker.Callback, settings?: Partial<Ticker.HandlerData>): IDisposable
        {
            let handler: Handler;
            this.insertHandler(handler = {
                callback,
                nextCall: null,
                runTime: 0,
                remove: false,
                paused: false,
                priority: RS.Ticker.Priority.Update,
                kind: RS.Ticker.Kind.Default,
                data: 0,
                ...settings
            });
            return new TickerHandle(this, handler);
        }

        /**
         * Calls the specified function once on the next frame.
         * @param callback
         */
        public once(callback: Ticker.Callback): IDisposable
        {
            let handler: Handler;
            this.insertHandler(handler = {
                callback,
                nextCall: null,
                runTime: 0,
                remove: false,
                paused: false,
                priority: RS.Ticker.Priority.Update,
                kind: RS.Ticker.Kind.Once,
                data: 0
            });
            return new TickerHandle(this, handler);
        }

        /**
         * Adds a ticker that will be called once after a delay.
         * @param callback
         * @param delay
         */
        public after(callback: Ticker.Callback, delay: number): IDisposable;

        /**
         * Returns a promise that is resolved after a delay.
         * @param delay
         */
        public after(delay: number): PromiseLike<void>;

        public after(p1: Ticker.Callback | number, p2?: number): IDisposable | PromiseLike<void>
        {
            const callback = Is.func(p1) ? p1 : null;
            const delay = Is.number(p1) ? p1 : p2;

            if (callback == null)
            {
                return new Promise<void>((resolve, reject) =>
                {
                    this.after((ev) => resolve(), delay);
                });
            }

            let handler: Handler;
            this.insertHandler(handler = {
                callback,
                nextCall: null,
                runTime: 0,
                remove: false,
                paused: false,
                priority: RS.Ticker.Priority.Update,
                kind: RS.Ticker.Kind.After,
                data: delay
            });
            return new TickerHandle(this, handler);
        }

        /**
         * Removes the specified ticker callback.
         * @param callback
         */
        public remove(callback: Ticker.Callback): void
        {
            for (let i = 0, l = this._tickers.length; i < l; ++i)
            {
                const handler = this._tickers[i];
                if (handler.callback === callback)
                {
                    if (this._inTick)
                    {
                        handler.remove = true;
                    }
                    else
                    {
                        this._tickers.splice(i, 1);
                    }
                    break;
                }
            }
        }

        public removeHandler(handler: Handler): void
        {
            if (this._inTick)
            {
                // Mark for deletion after tick.
                handler.remove = true;
                return;
            }

            const idx = this._tickers.indexOf(handler);
            if (idx !== -1)
            {
                this._tickers.splice(idx, 1);
            }
        }

        /**
         * Steps forward a tick when in single-step mode.
         */
        public step(): void
        {
            this._stepped = true;
            this._singleStep = true;
        }

        /**
         * Manually advances the ticker by the specified delta.
         * @param delta ms
         */
        public advance(delta: number): void
        {
            this._manualAdvance += delta;
            this.advanceTo(this._timer.elapsed + this._manualAdvance);
        }

        @Callback
        private tick()
        {
            this._animFrameRequested = false;
            if (!this._enabled) { return; }
            this.advanceTo(this._timer.elapsed + this._manualAdvance);
            requestAnimationFrame(this.tick);
            this._animFrameRequested = true;
        }

        private advanceTo(time: number): void
        {
            this._inTick = true;

            for (const handler of this._tickers)
            {
                if (!handler.paused && !handler.remove)
                {
                    let result: Ticker.Result;
                    try
                    {
                        result = this._behaviours[handler.kind](handler, time);
                    }
                    catch (err)
                    {
                        // Report and isolate
                        Log.error(`TickError`, err);
                        handler.remove = true;
                    }
                    switch (result)
                    {
                        case undefined:
                        case null:
                        case RS.Ticker.Result.Default:
                            break;
                        case RS.Ticker.Result.Cancel:
                            handler.remove = true;
                            break;
                        case RS.Ticker.Result.Pause:
                            handler.paused = true;
                            break;
                    }
                }
            }

            // Remove any tickers marked for removal
            for (let i = this._tickers.length - 1; i >= 0; i--)
            {
                if (this._tickers[i].remove)
                {
                    this._tickers.splice(i, 1);
                }
            }

            this._lastTime = time;
            this._stepped = false;

            this._inTick = false;
        }

        /**
         * Gets the delta time from the last tick clamped and scaled appropriately
         */
        private getDelta(now: number = RS.Timer.now): number
        {
            let delta = now - this._lastTime;

            if (this._singleStep)
            {
                if (!this._stepped)
                {
                    delta = 0;
                }
                else
                {
                    delta = this._defaultTimeStep;
                }
            }

            return this._paused ? 0 : Math.min(this._maxDelta, delta) * (this._singleStep ? 1 : this._timeScale);
        }

        /**
         * Gets the real time delta from the last tick, this is completely unfiltered.
         */
        private getRealTimeDelta(now: number = RS.Timer.now): number
        {
            return now - this._lastTime;
        }

        private insertHandler(handler: Handler): void
        {
            const insertIndex = Util.binarySearch(this._tickers, handler, (a, b) => a.priority - b.priority, false);
            this._tickers.splice(insertIndex, 0, handler);
        }

        // #region Behaviours

        @Behaviour(RS.Ticker.Kind.Default)
        private tickerDefault(handler: Handler, now: number): Ticker.Result
        {
            let result = RS.Ticker.Result.Default;
            if (!this._paused)
            {
                const delta = this.getDelta(now);
                handler.runTime += delta;
                const realDelta = this.getRealTimeDelta(now);

                if (!this._singleStep || this._stepped)
                {
                    tmpEvent.delta = delta;
                    tmpEvent.realDelta = realDelta;
                    tmpEvent.paused = false;
                    tmpEvent.singleStep = this._singleStep;
                    tmpEvent.runTime = handler.runTime;
                    result = handler.callback(tmpEvent) || result;
                }
            }
            return result;
        }

        @Behaviour(RS.Ticker.Kind.Always)
        private tickerAlways(handler: Handler, now: number): Ticker.Result
        {
            const realDelta = this.getRealTimeDelta(now);
            handler.runTime += realDelta * this._timeScale;
            tmpEvent.delta = realDelta * this._timeScale;
            tmpEvent.realDelta = realDelta;
            tmpEvent.paused = this._paused;
            tmpEvent.singleStep = this._singleStep;
            tmpEvent.runTime = handler.runTime;
            return handler.callback(tmpEvent) || RS.Ticker.Result.Default;
        }

        @Behaviour(RS.Ticker.Kind.Render)
        private tickerRender(handler: Handler, now: number): Ticker.Result
        {
            const delta = this.getDelta(now);
            handler.runTime += delta;
            const realDelta = this.getRealTimeDelta(now);
            tmpEvent.delta = delta;
            tmpEvent.realDelta = realDelta;
            tmpEvent.paused = this._paused;
            tmpEvent.singleStep = this._singleStep;
            tmpEvent.runTime = handler.runTime;
            return handler.callback(tmpEvent) || RS.Ticker.Result.Default;
        }

        @Behaviour(RS.Ticker.Kind.RealTime)
        private tickerRealTime(handler: Handler, now: number): Ticker.Result
        {
            const realDelta = this.getRealTimeDelta(now);
            let result = RS.Ticker.Result.Default;
            const delta = this.getDelta(now);

            if (!this._paused && (!this._singleStep || this._stepped))
            {
                handler.runTime += realDelta;
                tmpEvent.delta = delta;
                tmpEvent.realDelta = realDelta;
                tmpEvent.paused = this._paused;
                tmpEvent.singleStep = this._singleStep;
                tmpEvent.runTime = handler.runTime;
                result = handler.callback(tmpEvent) || result;
            }

            return result;
        }

        @Behaviour(RS.Ticker.Kind.FixedStep)
        private tickerFixedStep(handler: Handler, now: number): Ticker.Result
        {
            const delta = handler.data;
            let result = RS.Ticker.Result.Default;

            if (!this._paused)
            {
                // Initialise nextCall?
                if (handler.nextCall == null)
                {
                    handler.nextCall = now;
                }

                // Execute missed ticks
                while ((result === RS.Ticker.Result.Default) && (handler.nextCall <= now))
                {
                    handler.runTime += delta;
                    tmpEvent.delta = delta;
                    tmpEvent.realDelta = delta;
                    tmpEvent.paused = this._paused;
                    tmpEvent.singleStep = this._singleStep;
                    tmpEvent.runTime = handler.runTime;
                    result = handler.callback(tmpEvent) || result;
                    handler.nextCall = this._singleStep ? now + delta : handler.nextCall + delta;
                }
            }
            else
            {
                handler.nextCall = now + delta;
            }

            return result;
        }

        @Behaviour(RS.Ticker.Kind.MultipleFixedStep)
        private tickerMultipleFixedStep(handler: Handler, now: number): Ticker.Result
        {
            let delta = handler.data;

            let result = RS.Ticker.Result.Default;

            if (!this._paused)
            {
                if (handler.nextCall == null) { handler.nextCall = now; }
                if (this._singleStep)
                {
                    // Initialise nextCall?
                    if (handler.nextCall == null)
                    {
                        handler.nextCall = now;
                    }

                    while (handler.nextCall + delta <= now)
                    {
                        delta += handler.data;
                    }
                    handler.nextCall += delta;
                    handler.runTime += delta;
                }
                else
                {
                    handler.nextCall = now + delta;
                }
                tmpEvent.delta = delta;
                tmpEvent.realDelta = delta;
                tmpEvent.paused = this._paused;
                tmpEvent.singleStep = this._singleStep;
                tmpEvent.runTime = handler.runTime;
                result = handler.callback(tmpEvent) || result;
            }
            else
            {
                handler.nextCall = now + delta;
            }

            return result;
        }

        @Behaviour(RS.Ticker.Kind.Once)
        private tickerOnce(handler: Handler, now: number): Ticker.Result
        {
            tmpEvent.delta = 0;
            tmpEvent.realDelta = 0;
            tmpEvent.paused = this._paused;
            tmpEvent.singleStep = this._singleStep;
            tmpEvent.runTime = handler.runTime;
            handler.callback(tmpEvent);
            return RS.Ticker.Result.Cancel;
        }

        @Behaviour(RS.Ticker.Kind.After)
        private tickerAfter(handler: Handler, now: number): Ticker.Result
        {
            handler.runTime += this.getDelta(now);

            if (handler.runTime >= handler.data)
            {
                tmpEvent.delta = handler.runTime;
                tmpEvent.realDelta = 0;
                tmpEvent.paused = this._paused;
                tmpEvent.singleStep = this._singleStep;
                tmpEvent.runTime = handler.runTime;
                handler.callback(tmpEvent);
                return RS.Ticker.Result.Cancel;
            }

            return RS.Ticker.Result.Default;
        }

        // #endregion
    }

    ITicker.register(Ticker);
}
