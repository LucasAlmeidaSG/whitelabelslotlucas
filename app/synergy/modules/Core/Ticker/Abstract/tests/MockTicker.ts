namespace RS.Ticker.Tests
{
    export class MockHandle implements RS.IDisposable
    {
        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }
        public dispose() { this._isDisposed = true; }
    }

    export class MockTicker implements ITicker
    {
        public enabled: boolean = true;
        public paused: boolean = false;
        public singleStep: boolean = false;
        public timeScale: number = 1;
        public targetFramerate: number = 60;
        public targetTimestep: number = 1000 / this.targetFramerate;
        public maxDelta: number = 50;

        public add(callback: Ticker.Callback): IDisposable;
        public add(callback: Ticker.Callback, settings: Partial<Ticker.HandlerData>): IDisposable;
        public add(callback: Ticker.Callback, settings?: Partial<Ticker.HandlerData>): IDisposable
        {
            return new MockHandle();
        }

        public remove(callback: Ticker.Callback): void
        {
            return;
        }

        public once(callback: Ticker.Callback): IDisposable
        {
            return new MockHandle();
        }

        public after(callback: Ticker.Callback, delay: number): IDisposable;
        public after(delay: number): PromiseLike<void>;
        public after(p1: Ticker.Callback | number, p2?: number): IDisposable | PromiseLike<void>
        {
            throw new Error("Method not implemented.");
        }

        public step(): void
        {
            throw new Error("Method not implemented.");
        }

        public advance(delta: number): void
        {
            throw new Error("Method not implemented.");
        }
    }

    ITicker.register(MockTicker);
}