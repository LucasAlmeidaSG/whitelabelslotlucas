namespace RS.Ticker.Tests
{
    const { expect } = chai;

    class MockDecorated
    {
        @Tick({})
        public tick()
        {
            return;
        }
    }

    describe("TickDecorator.ts", function ()
    {
        describe("registerTickers", function ()
        {
            it("should call ITicker.add() with the decorated methods", function ()
            {
                const ticker = ITicker.get();
                const addSpy = sinon.spy(ticker, "add");

                const inst = new MockDecorated();
                registerTickers(inst);

                expect(addSpy.callCount, "ITicker.add() should be called once").to.equal(1);

                unregisterTickers(inst);
                addSpy.restore();
            });

            it("should not register the same method on the same instance multiple times", function ()
            {
                const ticker = ITicker.get();
                const addSpy = sinon.spy(ticker, "add");

                const inst = new MockDecorated();
                registerTickers(inst);
                registerTickers(inst);

                expect(addSpy.callCount, "ITicker.add() should be called once").to.equal(1);

                unregisterTickers(inst);
                addSpy.restore();
            });

            it("should allow the same method to be registered for multiple instances", function ()
            {
                const ticker = ITicker.get();
                const addSpy = sinon.spy(ticker, "add");

                const instA = new MockDecorated();
                const instB = new MockDecorated();
                registerTickers(instA);
                registerTickers(instB);

                expect(addSpy.callCount, "ITicker.add() should be called twice").to.equal(2);

                unregisterTickers(instA);
                unregisterTickers(instB);
                addSpy.restore();
            });
        });

        describe("unregisterTickers", function ()
        {
            it("should call dispose() on any handles previously obtained", function ()
            {
                const ticker = ITicker.get();
                const addSpy = sinon.spy(ticker, "add");

                const inst = new MockDecorated();
                registerTickers(inst);

                const handle = addSpy.lastCall.returnValue;
                const disposeSpy = sinon.spy(handle, "dispose");

                unregisterTickers(inst);
                expect(disposeSpy.callCount, "IDisposable.dispose() should be called once").to.equal(1);

                disposeSpy.restore();
                addSpy.restore();
            });
        });
    });
}
