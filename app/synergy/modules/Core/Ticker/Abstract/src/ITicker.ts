namespace RS
{
    /**
     * Encapsulates an interface to subscribe to "ticks" which occur every frame.
     * The ticks drive game logic, from tweens through to rendering.
     */
    export interface ITicker
    {
        /** Gets or sets if the ticker is enabled. This is different from paused - the ticker will not run at all when disabled. */
        enabled: boolean;

        /** Gets or sets if the ticker is paused. Non-pausable ticker handlers will still be processed regardless. */
        paused: boolean;

        /** Gets or sets if the ticker is in single step mode. When in single step mode, normal tickers will only step on when RS.Ticker.step is called. */
        singleStep: boolean;

        /** Gets or sets a multiplier for ticker speed. Values less than one will slow down time, values greater than one will speed it up. */
        timeScale: number;

        /** Gets or sets the target timestep (ms). */
        targetTimestep: number;

        /** Gets or sets the target framerate (fps). */
        targetFramerate: number;

        /** Gets or sets the value at which delta should be clamped. */
        maxDelta: number;

        /**
         * Adds a simple ticker handler with the default priority and kind.
         * @param callback
         * @returns a handle which can be disposed to remove the ticker handler.
         */
        add(callback: Ticker.Callback): IDisposable;

        /**
         * Adds a ticker handler with the specified priority and default kind.
         * @param callback
         * @param settings
         * @returns a handle which can be disposed to remove the ticker handler.
         */
        add(callback: Ticker.Callback, settings: Partial<Ticker.HandlerData>): IDisposable;

        /**
         * Removes the specified ticker callback.
         * @param callback
         */
        remove(callback: Ticker.Callback): void;

        /**
         * Calls the specified function once on the next frame.
         * @param callback
         */
        once(callback: Ticker.Callback): IDisposable;

        /**
         * Calls the specified function once after a delay.
         * @param callback
         * @param delay ms
         */
        after(callback: Ticker.Callback, delay: number): IDisposable;

        /**
         * Gets a promise that resolves after a delay.
         * @param delay ms
         */
        after(delay: number): PromiseLike<void>;

        /**
         * Steps forward a tick when in single-step mode.
         */
        step(): void;

        /**
         * Manually advances the ticker by the specified delta.
         * @param delta ms
         */
        advance(delta: number): void;
    }

    export const ITicker = Strategy.declare<ITicker>(Strategy.Type.Singleton);

    export namespace Ticker
    {
        /**
         * Returned from a ticker handler function.
         * Used to adjust how a ticker callback is called in the future.
         */
        export enum Result
        {
            Default,
            Cancel,
            Pause
        }

        /**
         * Indicates when a ticker callback should be called.
         * Used to sort ticker callbacks.
         */
        export enum Priority
        {
            PreUpdate,
            Update,
            PostUpdate,
            PreRender,
            Render,
            PostRender
        }

        /**
         * Indicates how a ticker callback should be called.
         */
        export enum Kind
        {
            /** Default tickers will pause when the game pauses and can be scaled */
            Default,
            /** Always tickers will always be processed at realtime speed. */
            Always,
            /** Render tickers will always be processed, but at whatever speed the game is running at */
            Render,
            /** RealTime tickers can be paused, but will ignore any timestep value set */
            RealTime,
            /** FixedStep tickers will always be called with a fixed value timestep */
            FixedStep,
            /** MultipleFixedStep tickers will always be called with a multiple of the fixed timestep value */
            MultipleFixedStep,
            /** Once will call the function on the next tick */
            Once,
            /** After will wait for value milliseconds and then call the ticker function */
            After
        }

        /**
         * Encapsulates data for a ticker callback.
         */
        export interface Event
        {
            /** Delta time between ticks (ms) */
            delta: number;

            /** Actual delta time between ticks (not modified by pause or time multiplier) (ms) */
            realDelta: number;

            /** True while the game is paused (only received by non-pausable tickers) */
            paused: boolean;

            /** True when the game is single stepping */
            singleStep: boolean;

            /** Total time the ticker has been running */
            runTime: number;
        }

        /**
         * Encapsulates settings for a ticker handler.
         */
        export interface HandlerData
        {
            priority: Ticker.Priority;
            kind: Ticker.Kind;
            data: number;
        }

        export type Callback = (ev: Event) => Result | void;
    }
}