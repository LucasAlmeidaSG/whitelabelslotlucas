namespace RS.Ticker
{
    /**
     * A simple timer based on the game ticker.
     */
    @HasCallbacks
    export class Timer implements IDisposable
    {
        @RS.AutoDisposeOnSet private _tickerHandle;

        private _paused: boolean = true;
        private _elapsed: number = 0;
        private _isDisposed: boolean = false;

        public get paused() { return this._paused; }
        public set paused(value)
        {
            if (this._paused === value) { return; }
            this._paused = value;
            this.updateTickState();
        }

        /** Milliseconds since timer was last started, excluding pauses. */
        public get elapsed() { return this._elapsed; }
        /** Seconds since timer was last started, excluding pauses. */
        public get elapsedSeconds() { return this.elapsed / 1000; }
        public get isDisposed() { return this._isDisposed; }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._paused = true;
            this._isDisposed = true;
        }

        /**
         * Restarts timing from the beginning.
         */
        public start(): void
        {
            this._elapsed = 0;
            this.paused = false;
        }

        /**
         * Stops timing, returning the total time elapsed.
         */
        public stop(): number
        {
            const elapsed = this._elapsed;
            this._elapsed = 0;
            this.paused = true;
            return elapsed;
        }

        @Callback
        protected tick(event: Ticker.Event)
        {
            this._elapsed += event.delta;
        }

        private updateTickState()
        {
            if (this._paused)
            {
                this._tickerHandle = null;
            }
            else if (!this._paused && !this._tickerHandle)
            {
                this._tickerHandle = ITicker.get().add(this.tick, { priority: Ticker.Priority.PreUpdate });
            }
        }
    }
}