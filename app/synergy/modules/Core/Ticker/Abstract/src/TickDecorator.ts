namespace RS
{
    /**
     * Decorator that subscribes the decorated method to the ticker (when Ticker.registerTickers is called on the instance).
     */
    export const Tick = Decorators.Tag.create<Partial<Ticker.HandlerData>>(Decorators.TagKind.Method);

    export namespace Ticker
    {
        type TickerRegistrant =
        {
            __tickerHandles?: { [key: string]: RS.IDisposable; };
        };

        /**
         * Adds all tickers on the specified target.
         * @param target
         */
        export function registerTickers(target: object): void;
        export function registerTickers(target: TickerRegistrant): void
        {
            if (target.__tickerHandles == null) { target.__tickerHandles = {}; }

            const ticker = ITicker.get();
            const tags = Tick.get(target, Decorators.MemberType.Instance);
            for (const name in tags)
            {
                if (target.__tickerHandles[name]) { continue; }

                const val = target[name] as (...args) => any;
                const func = val.bind(target);
                const handle = ticker.add(func, tags[name]);
                target.__tickerHandles[name] = handle;
            }
        }

        /**
         * Removes all tickers from the specified target.
         * @param target
         */
        export function unregisterTickers(target: object): void;
        export function unregisterTickers(target: TickerRegistrant): void
        {
            if (target.__tickerHandles == null) { return; }
            for (const name in target.__tickerHandles)
            {
                const handle = target.__tickerHandles[name];
                if (handle)
                {
                    target.__tickerHandles[name].dispose();
                    target.__tickerHandles[name] = null;
                }
            }
        }
    }
}