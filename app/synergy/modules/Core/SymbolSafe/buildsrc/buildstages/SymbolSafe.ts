/// <reference path="../Decorators.ts" />
/// <reference path="../Enums.ts" />
/// <reference path="../Properties.ts" />

namespace RS.BuildStages
{
    /**
     * Prepares TypeScript-compiled code for closure compilation by destringifying accessor names, enum references and decorator property references.
     */
    export class SymbolSafe extends BuildStage.Base
    {
        protected _numProcessed: number;

        constructor(protected readonly _targetUnits: ReadonlyArray<string>, public readonly name?: string) { super(); }

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            const ctx = this.name == null ? "SymbolSafe" : `SymbolSafe.${this.name}`;
            Log.pushContext(ctx);
            this._numProcessed = 0;

            const time = new Timer();
            const result = await super.execute(moduleList);
            time.pause();

            if (this._numProcessed === 1)
            {
                Log.info(`Processed 1 module in ${time}`);
            }
            else if (this._numProcessed > 1)
            {
                Log.info(`Processed ${this._numProcessed} modules in ${time}`);
            }

            Log.popContext(ctx);

            return result;
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const units = await this.getTargetUnits(module);
            if (units.length === 0) { return { workDone: false, errorCount: 0 }; }

            let errorCount = 0, results: boolean[];
            try
            {
                // Todo: per-unit error count
                results = await Promise.all(units.map((unit) => this.do(unit)));
            }
            catch (err)
            {
                Log.error("SymbolSafe failed");
                Log.error(err.stack);
                ++errorCount;
            }

            const workDone = results.some((thisWorkDone) => thisWorkDone);
            if (workDone)
            {
                this._numProcessed++;
                await module.saveChecksums();
            }

            // Work done
            return { workDone, errorCount };
        }

        @Profile("SymbolSafe.getTargetUnits")
        private async getTargetUnits(module: Module.Base)
        {
            const units: Build.CompilationUnit[] = [];
            for (const unitName of this._targetUnits)
            {
                // Valid unit?
                const unit = module.getCompilationUnit(unitName);
                if (!unit) { continue; }
                units.push(unit);
            }
            return units;
        }

        @Profile("SymbolSafe.do")
        private async do(unit: Build.CompilationUnit): Promise<boolean>
        {
            const scriptFileIn = unit.outJSFile;
            const mapFileIn = unit.outMapFile;

            const scriptFileOut = this.getOutputPath(scriptFileIn);
            const mapFileOut = this.getOutputPath(mapFileIn);

            const checksum = unit.srcChecksum;
            const key = this.getChecksumKey(unit);

            // Work to do if diff, or output files missing
            if (unit.module.checksumDB.diff(key, checksum) || !(await FileSystem.fileExists(scriptFileOut)) || !(await FileSystem.fileExists(mapFileOut)))
            {
                const compiledFile = await Build.CompiledFile.load(scriptFileIn, mapFileIn);
                Build.SymbolSafe.fixDecorators(compiledFile);
                Build.SymbolSafe.fixProperties(compiledFile);
                Build.SymbolSafe.fixEnums(compiledFile);
                await compiledFile.write(scriptFileOut, mapFileOut);
                await compiledFile.write(scriptFileIn, mapFileIn);

                unit.module.checksumDB.set(key, checksum);
                return true;
            }

            // Just overwrite the files
            await FileSystem.copyFiles([scriptFileOut, mapFileOut], [scriptFileIn, mapFileIn]);
            return false;
        }

        private getOutputPath(inputPath: string): string
        {
            const suffix = "_symbolsafe";

            const baseName = Path.baseName(inputPath);
            const extIndex = baseName.indexOf(".", 1);

            if (extIndex === -1)
            {
                return `${inputPath}${suffix}`;
            }
            else
            {
                const fileName = baseName.slice(0, extIndex);
                const ext = baseName.slice(extIndex);
                return Path.combine(Path.directoryName(inputPath), `${fileName}${suffix}${ext}`);
            }
        }

        private getChecksumKey(unit: Build.CompilationUnit): string
        {
            return `symbolsafe_${unit.name}`;
        }
    }

    export const symbolSafe = new SymbolSafe(["src"]);
    BuildStage.register({ after: [ compile ] }, symbolSafe);

    export const symbolSafeHelp = new SymbolSafe(["help"], "help");
    BuildStage.register({ after: [ ExternalHelp.BuildStages.compileLibraries ] }, symbolSafeHelp);
}