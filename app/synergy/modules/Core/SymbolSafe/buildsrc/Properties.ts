namespace RS.Build.SymbolSafe
{
    interface DefinePropertyInfo
    {
        target: ESTree.Expression;
        name: string;
        desc: ESTree.Expression;
    }

    /**
     * Fixes all properties in the specified JS file.
     */
    export function fixProperties(code: Build.CompiledFile): void
    {
        code.transform((ast) =>
        {
            AST.visit(ast, (node) =>
            {
                if (node.type === "BlockStatement")
                {
                    let firstProperty: number;
                    let workDone = false;
                    const properties: DefinePropertyInfo[] = [];
                    for (let i = 0, l = node.body.length; i < l; ++i)
                    {
                        const statement = node.body[i];
                        if (statement.type === "ExpressionStatement" && statement.expression.type === "CallExpression")
                        {
                            const property = identifyDefinePropertyCall(statement.expression);
                            if (property)
                            {
                                if (firstProperty == null)
                                {
                                    firstProperty = i;
                                }
                                properties.push(property);
                                continue;
                            }
                        }
                        if (firstProperty != null)
                        {
                            const block = transformPropertyBlock(properties);
                            node.body.splice(firstProperty, i - firstProperty);
                            for (let j = 0, l2 = block.length; j < l2; ++j)
                            {
                                node.body.splice(firstProperty + j, 0, block[j]);
                            }
                            firstProperty = null;
                            properties.length = 0;
                            i = firstProperty + block.length;
                            l = node.body.length;
                            workDone = true;
                        }
                    }
                    if (workDone) { return true; }
                }
            });
        }, "src.js", "src.js.map");
    }

    function identifyDefinePropertyCall(node: ESTree.CallExpression): DefinePropertyInfo | null
    {
        if (node.callee.type !== "MemberExpression") { return null; }
        if (node.callee.object.type !== "Identifier") { return null; }
        if (node.callee.object.name !== "Object") { return null; }
        if (node.callee.property.type !== "Identifier") { return null; }
        if (node.callee.property.name !== "defineProperty") { return null; }

        const target = node.arguments[0];
        if (!target || target.type === "SpreadElement") { return null; }

        const propertyName = node.arguments[1];
        if (!propertyName || propertyName.type !== "Literal") { return null; }
        if (!Is.string(propertyName.value)) { return null; }

        const descriptor = node.arguments[2];
        if (!descriptor || descriptor.type === "SpreadElement") { return null; }

        return {
            target,
            name: propertyName.value,
            desc: descriptor
        };
    }

    function transformPropertyBlock(properties: DefinePropertyInfo[]): ESTree.Statement[]
    {
        const statements: ESTree.Statement[] = [];

        interface Batch
        {
            target: ESTree.Expression;
            properties: DefinePropertyInfo[];
        }

        const batches: Batch[] = [];
        for (const prop of properties)
        {
            let found = false;
            for (const batch of batches)
            {
                if (AST.expressionsEqual(batch.target, prop.target))
                {
                    batch.properties.push(prop);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                batches.push({ target: prop.target, properties: [ prop ] });
            }
        }
        for (const batch of batches)
        {
            for (const statement of transformPropertyBlockBatch(batch.properties, batch.target))
            {
                statements.push(statement);
            }
        }

        return statements;
    }

    function transformPropertyBlockBatch(properties: DefinePropertyInfo[], target: ESTree.Expression): ESTree.Statement[]
    {
        // Object.defineProperty(Game.prototype, "bigWinController", {
        //     get: function () { return this._bigWinController; },
        //     enumerable: true,
        //     configurable: true
        // });
        // Object.defineProperty(Game.prototype, "devToolsController", {
        //     get: function () { return this._devToolsController; },
        //     enumerable: true,
        //     configurable: true
        // });

        const result: ESTree.Statement[] = [];

        const tmpVarID: ESTree.Identifier = { type: "Identifier", name: "_propObj" };

        // Declare an object to hold everything
        result.push({
            type: "VariableDeclaration",
            kind: "var",
            declarations:
            [
                {
                    type: "VariableDeclarator",
                    id: tmpVarID,
                    init: { type: "ObjectExpression", properties: [] }
                }
            ]
        });

        // Fill in each property
        for (const info of properties)
        {
            result.push({
                type: "ExpressionStatement",
                expression:
                {
                    type: "AssignmentExpression",
                    operator: "=",
                    left:
                    {
                        type: "MemberExpression",
                        object: tmpVarID,
                        property: { type: "Identifier", name: info.name },
                        computed: false
                    },
                    right: info.desc
                }
            });
        }

        // Call Object.defineProperties
        result.push({
            type: "ExpressionStatement",
            expression:
            {
                type: "CallExpression",
                callee:
                {
                    type: "MemberExpression",
                    object: { type: "Identifier", name: "Object" },
                    property: { type: "Identifier", name: "defineProperties" },
                    computed: false
                },
                arguments: [ target, tmpVarID ]
            }
        });

        return result;
    }
}