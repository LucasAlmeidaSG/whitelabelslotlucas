namespace RS.Build.SymbolSafe
{
    interface DecorateInfo
    {
        decorators: ESTree.Expression[];
        target: ESTree.Expression;
        propertyName: string;
        descriptor: ESTree.Expression;
    }

    /**
     * Fixes all decorators in the specified JS file.
     */
    export function fixDecorators(code: Build.CompiledFile): void
    {
        code.transform((ast) =>
        {
            AST.visit(ast, (node) =>
            {
                if (node.type === "BlockStatement")
                {
                    let firstDecorator: number;
                    let workDone = false;
                    const decorators: DecorateInfo[] = [];
                    for (let i = 0, l = node.body.length; i < l; ++i)
                    {
                        const statement = node.body[i];
                        if (statement.type === "ExpressionStatement" && statement.expression.type === "CallExpression")
                        {
                            const decorator = identifyDecorateCall(statement.expression);
                            if (decorator)
                            {
                                if (firstDecorator == null)
                                {
                                    firstDecorator = i;
                                }
                                decorators.push(decorator);
                                continue;
                            }
                        }
                        if (firstDecorator != null)
                        {
                            const block = transformDecorateBlock(decorators);
                            node.body.splice(firstDecorator, i - firstDecorator);
                            for (let j = 0, l2 = block.length; j < l2; ++j)
                            {
                                node.body.splice(firstDecorator + j, 0, block[j]);
                            }
                            firstDecorator = null;
                            decorators.length = 0;
                            i = firstDecorator + block.length;
                            l = node.body.length;
                            workDone = true;
                        }
                    }
                    if (workDone) { return true; }
                }
            });
        }, "src.js", "src.js.map");   
    }

    function identifyDecorateCall(node: ESTree.CallExpression): DecorateInfo | null
    {
        if (node.callee.type !== "Identifier") { return null; }
        if (node.callee.name !== "__decorate") { return null; }

        const arr = node.arguments[0];
        if (!arr || arr.type !== "ArrayExpression") { return null; }
        const decorators = arr.elements as ESTree.Expression[];

        const target = node.arguments[1];
        if (!target || target.type === "SpreadElement") { return null; }

        const propertyName = node.arguments[2];
        if (!propertyName || propertyName.type !== "Literal") { return null; }
        if (!Is.string(propertyName.value)) { return null; }

        const descriptor = node.arguments[3];
        if (!descriptor || descriptor.type === "SpreadElement") { return null; }

        return {
            decorators,
            target,
            propertyName: propertyName.value,
            descriptor: descriptor
        };
    }

    function transformDecorateBlock(decorators: DecorateInfo[]): ESTree.Statement[]
    {
        // __decorate([
        //     RS.AutoDispose
        // ], Main.prototype, "_background", void 0);
        // __decorate([
        //     RS.AutoDispose
        // ], Main.prototype, "_reels", void 0);

        const result: ESTree.Statement[] = [];

        const tmpVarID: ESTree.Identifier = { type: "Identifier", name: "_decObj" };

        // Declare an object to hold everything
        result.push({
            type: "VariableDeclaration",
            kind: "var",
            declarations:
            [
                {
                    type: "VariableDeclarator",
                    id: tmpVarID,
                    init: { type: "ObjectExpression", properties: [] }
                }
            ]
        });

        // Fill in each property
        for (const info of decorators)
        {
            result.push({
                type: "ExpressionStatement",
                expression:
                {
                    type: "AssignmentExpression",
                    operator: "=",
                    left:
                    {
                        type: "MemberExpression",
                        object: tmpVarID,
                        property: { type: "Identifier", name: info.propertyName },
                        computed: false
                    },
                    right:
                    {
                        type: "ObjectExpression",
                        properties:
                        [
                            {
                                type: "Property",
                                key: { type: "Identifier", name: "decorators" },
                                value: { type: "ArrayExpression", elements: info.decorators },
                                kind: "init",
                                method: false, shorthand: false, computed: false
                            },
                            {
                                type: "Property",
                                key: { type: "Identifier", name: "target" },
                                value: info.target,
                                kind: "init",
                                method: false, shorthand: false, computed: false
                            },
                            {
                                type: "Property",
                                key: { type: "Identifier", name: "desc" },
                                value: info.descriptor || { type: "Literal", value: null, raw: "null" },
                                kind: "init",
                                method: false, shorthand: false, computed: false
                            }
                        ]
                    }
                }
            });
        }

        // Call __multiDecorate
        result.push({
            type: "ExpressionStatement",
            expression:
            {
                type: "CallExpression",
                callee: { type: "Identifier", name: "__multiDecorate" },
                arguments: [ tmpVarID ]
            }
        });

        return result;
    }
}