namespace RS.Build.SymbolSafe
{
    /**
     * Fixes all enums in the specified JS file.
     */
    export function fixEnums(code: Build.CompiledFile): void
    {
        code.transform((ast) =>
        {
            AST.visit(ast, (node) =>
            {
                if (node.type === "FunctionExpression")
                {
                    const body = node.body.body;
                    let workDone = false;
                    const p0 = node.params[0];
                    if (body.length > 0)
                    {
                        if (body.every((c) => tryFixStrenum(c, true)))
                        {
                            if (p0 != null && p0.type === "Identifier")
                            {
                                // Log.debug(`Fixing strenum '${p0.name}'`);
                            }
                            for (const child of body)
                            {
                                tryFixStrenum(child);
                            }
                            workDone = true;
                        }
                        else if (body.every((c) => tryFixEnum(c, true)))
                        {
                            if (p0 != null && p0.type === "Identifier")
                            {
                                // Log.debug(`Fixing enum '${p0.name}'`);
                            }
                            for (const child of body)
                            {
                                tryFixEnum(child);
                            }
                            workDone = true;
                        }
                    }
                    if (workDone) { return true; }
                }
            });
        }, "src.js", "src.js.map");
    }

    function tryFixEnum(statement: ESTree.Statement, dry = false): boolean
    {
        // var SegmentRotationType;
        // (function (SegmentRotationType) {
        //     SegmentRotationType[SegmentRotationType["DeadCenter"] = 0] = "DeadCenter";
        //     SegmentRotationType[SegmentRotationType["WellWithin"] = 1] = "WellWithin";
        //     SegmentRotationType[SegmentRotationType["JustWithinLeft"] = 2] = "JustWithinLeft";
        //     SegmentRotationType[SegmentRotationType["JustWithinRight"] = 3] = "JustWithinRight";
        //     SegmentRotationType[SegmentRotationType["OnLeft"] = 4] = "OnLeft";
        //     SegmentRotationType[SegmentRotationType["OnRight"] = 5] = "OnRight";
        // })(SegmentRotationType = GenieWheel.SegmentRotationType || (GenieWheel.SegmentRotationType = {}));

        if (statement.type !== "ExpressionStatement") { return false; }
        if (statement.expression.type !== "AssignmentExpression") { return false; }
        const expr = statement.expression;
        if (expr.operator !== "=") { return false; }
        if (expr.left.type !== "MemberExpression") { return false; }
        const left = expr.left;
        if (!left.computed) { return false; }
        if (left.object.type !== "Identifier") { return false; }
        const enumName = left.object.name;
        if (left.property.type !== "AssignmentExpression") { return false; }
        const innerAssign = left.property;
        if (innerAssign.operator !== "=") { return false; }
        if (innerAssign.left.type !== "MemberExpression") { return false; }
        const enumIndexer = innerAssign.left;
        if (!enumIndexer.computed) { return false; }
        if (enumIndexer.object.type !== "Identifier") { return false; }
        if (enumIndexer.object.name !== enumName) { return false; }
        if (enumIndexer.property.type !== "Literal") { return false; }
        if (!Is.string(enumIndexer.property.value)) { return false; }

        if (!dry)
        {
            enumIndexer.computed = false;
            enumIndexer.property = { type: "Identifier", name: enumIndexer.property.value };
        }

        return true;
    }

    function tryFixStrenum(statement: ESTree.Statement, dry = false): boolean
    {
        // (function (Type) {
        //     Type["Recover"] = "RECOVER";
        //     Type["Fatal"] = "FATAL";
        //     Type["RealityCheck"] = "REALITY_CHECK";
        // })(Type = Error.Type || (Error.Type = {}));

        if (statement.type !== "ExpressionStatement") { return false; }
        if (statement.expression.type !== "AssignmentExpression") { return false; }
        const expr = statement.expression;
        if (expr.operator !== "=") { return false; }
        if (expr.left.type !== "MemberExpression") { return false; }
        const enumIndexer = expr.left;
        if (!enumIndexer.computed) { return false; }
        if (enumIndexer.object.type !== "Identifier") { return false; }
        if (enumIndexer.property.type !== "Literal" || !Is.string(enumIndexer.property.value)) { return false; }

        if (!dry)
        {
            enumIndexer.computed = false;
            enumIndexer.property = { type: "Identifier", name: enumIndexer.property.value };
        }

        return true;
    }

}