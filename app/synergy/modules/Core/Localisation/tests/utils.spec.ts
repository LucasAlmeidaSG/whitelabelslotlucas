namespace RS.Tests
{
    const { expect } = chai;

    function mockSimpleReference(locale: Localisation.Locale, key: string, text: string): Localisation.SimpleTranslationReference
    {
        locale.addString(key, text);
        return {
            get: Localisation.generateSimpleGetter(key),
            getParts: null,
            clone: Localisation.clone
        }
    }

    function mockComplexReference<T extends Map<string>>(locale: Localisation.Locale, key: string, text: string, substitutions: T): Localisation.ComplexTranslationReference<T>
    {
        locale.addString(key, text);
        return {
            get: Localisation.generateComplexGetter(key, substitutions),
            getParts: null,
            clone: Localisation.clone
        };
    }

    describe("Utils.ts", function ()
    {
        describe("getParts", function ()
        {
            function expectPartsToBe(observed: RS.Localisation.TranslationPart[], expected: RS.Localisation.TranslationPart[])
            {
                expect(observed.length).to.be.equal(expected.length, "length");
                for (let i = 0, l = observed.length; i < l; ++i)
                {
                    expect(observed[i].kind).to.be.equal(expected[i].kind, `element ${i} kind`);
                    expect(observed[i].value).to.be.equal(expected[i].value, `element ${i} value`);
                }
            }

            it("should parse an empty string into an empty array", function ()
            {
                const result = RS.Localisation.getParts("");
                expect(result).to.be.empty;
            });

            it("should parse an simple string into a one part", function ()
            {
                const result = RS.Localisation.getParts("hello world");
                expectPartsToBe(result, [
                    { kind: RS.Localisation.TranslationPart.Kind.Text, value: "hello world" }
                ]);
            });

            it("should parse a complex string into multiple parts", function ()
            {
                const result = RS.Localisation.getParts("hello |||world|||, the time is {time}!");
                expectPartsToBe(result, [
                    { kind: RS.Localisation.TranslationPart.Kind.Text, value: "hello " },
                    { kind: RS.Localisation.TranslationPart.Kind.Protected, value: "world" },
                    { kind: RS.Localisation.TranslationPart.Kind.Text, value: ", the time is " },
                    { kind: RS.Localisation.TranslationPart.Kind.Variable, value: "time" },
                    { kind: RS.Localisation.TranslationPart.Kind.Text, value: "!" }
                ]);
            });

            it("should parse a complex string which ends in a variable into multiple parts", function ()
            {
                const result = RS.Localisation.getParts("FREE GAME {X} OF {Y}");
                expectPartsToBe(result, [
                    { kind: RS.Localisation.TranslationPart.Kind.Text, value: "FREE GAME " },
                    { kind: RS.Localisation.TranslationPart.Kind.Variable, value: "X" },
                    { kind: RS.Localisation.TranslationPart.Kind.Text, value: " OF " },
                    { kind: RS.Localisation.TranslationPart.Kind.Variable, value: "Y" }
                ]);
            });
        });

        describe("reduce", function ()
        {
            let locale: Localisation.Locale;
            beforeEach(function ()
            {
                locale = new Localisation.Locale("en", "GB");
            });

            it("should produce an empty string when passed an empty array", function ()
            {
                const strings = Localisation.reduce([], (a, b) => `${a}\n${b}`);
                const resolved = Localisation.resolveLocalisableString(locale, strings);
                expect(resolved).to.equal("");
            });

            it("should produce a localisable string from an array of simple localisables", function ()
            {
                const simpleText = mockSimpleReference(locale, "SimpleText", "eat pie");
                const strings = Localisation.reduce(["hi", "bye", simpleText], (a, b) => `${a}\n${b}`);
                const resolved = Localisation.resolveLocalisableString(locale, strings);
                expect(resolved).to.equal("hi\nbye\neat pie");
            });

            it("should produce a complex localisable string from an array of mixed simple and complex localisables", function ()
            {
                const complexText = mockComplexReference(locale, "ComplexText", "Here is a {thing} complex string.", { thing: "{thing}" });
                const strings = Localisation.reduce(["hi", complexText, "bye"], (a, b) => `${a}\n${b}`);
                const resolved = strings.get(locale, { thing: "yes" });
                expect(resolved).to.equal("hi\nHere is a yes complex string.\nbye");
            });
        });
    });
};