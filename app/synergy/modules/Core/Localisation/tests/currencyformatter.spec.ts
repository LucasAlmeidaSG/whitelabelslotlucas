/// <reference path="CurrenciesFromServer.ts" />

namespace RS.Tests
{
    const { expect } = chai;

    describe("CurrencyFormatter.ts", function ()
    {
        describe("CurrencyFormatter", function ()
        {
            const spaceCharacter = Localisation.SpecialChars.NonBreakingSpace;
            const space = (...strings: string[]) => strings.join(spaceCharacter);

            interface TestCase
            {
                in: number;
                out: string;
                outNoDec: string;
                outWithCode: string;
                outWithCodeNoDec: string;
            }
            let testCases: Map<TestCase>;

            before(function ()
            {
                testCases =
                {
                    "ARS": { in: 6280000, out: "$62.800,00", outNoDec: "$62.800", outWithCode: space("ARS", "62.800,00"), outWithCodeNoDec: space("ARS", "62.800") },
                    "AUD": { in: 1410000, out: "$14,100.00", outNoDec: "$14,100", outWithCode: space("AUD", "14,100.00"), outWithCodeNoDec: space("AUD", "14,100") },
                    "BGN": { in: 1960000, out: space("19", "600,00лв."), outNoDec: space("19", "600лв."), outWithCode: space("BGN", "19", "600,00"), outWithCodeNoDec: space("BGN", "19", "600") },
                    "BRL": { in: 3350000, out: space("R$", "33.500,00"), outNoDec: space("R$", "33.500"), outWithCode: space("BRL", "33.500,00"), outWithCodeNoDec: space("BRL", "33.500") },
                    "CAD": { in: 1370000, out: "$13,700.00", outNoDec: "$13,700", outWithCode: space("CAD", "13,700.00"), outWithCodeNoDec: space("CAD", "13,700") },
                    "CHF": { in: 1200000, out: space("SFr.", "12'000.00"), outNoDec: space("SFr.", "12'000"), outWithCode: space("CHF", "12'000.00"), outWithCodeNoDec: space("CHF", "12'000") },
                    "CNY": { in: 8100000, out: "¥81,000.00", outNoDec: "¥81,000", outWithCode: space("CNY", "81,000.00"), outWithCodeNoDec: space("CNY", "81,000") },
                    "CZK": { in: 25190000, out: space("251", "900,00", "Kč"), outNoDec: space("251", "900", "Kč"), outWithCode: space("CZK", "251", "900,00"), outWithCodeNoDec: space("CZK", "251", "900") },
                    "DKK": { in: 7460000, out: space("kr", "74.600,00"), outNoDec: space("kr", "74.600"), outWithCode: space("DKK", "74.600,00"), outWithCodeNoDec: space("DKK", "74.600") },
                    "EUR": { in: 1000000, out: space("10.000,00", "€"), outNoDec: space("10.000", "€"), outWithCode: space("EUR", "10.000,00"), outWithCodeNoDec: space("EUR", "10.000") },
                    "GBP": { in: 810000, out: "£8,100.00", outNoDec: "£8,100", outWithCode: space("GBP", "8,100.00"), outWithCodeNoDec: space("GBP", "8,100") },
                    "HKD": { in: 10140000, out: "HK$101,400.00", outNoDec: "HK$101,400", outWithCode: space("HKD", "101,400.00"), outWithCodeNoDec: space("HKD", "101,400") },
                    "HRK": { in: 7550000, out: space("Kn", "75.500,00"), outNoDec: space("Kn", "75.500"), outWithCode: space("HRK", "75.500,00"), outWithCodeNoDec: space("HRK", "75.500") },
                    "HUF": { in: 279860000, out: space("2", "798", "600,00", "Ft"), outNoDec: space("2", "798", "600", "Ft"), outWithCode: space("HUF", "2", "798", "600,00"), outWithCodeNoDec: space("HUF", "2", "798", "600") },
                    "INR": { in: 79130000, out: "Rs.791,300.00", outNoDec: "Rs.791,300", outWithCode: space("INR", "791,300.00"), outWithCodeNoDec: space("INR", "791,300") },
                    "ISK": { in: 161340000, out: space("1.613.400", "kr."), outNoDec: space("1.613.400", "kr."), outWithCode: space("ISK", "1.613.400"), outWithCodeNoDec: space("ISK", "1.613.400") },
                    "JPY": { in: 130110000, out: "¥1,301,100", outNoDec: "¥1,301,100", outWithCode: space("JPY", "1,301,100"), outWithCodeNoDec: space("JPY", "1,301,100") },
                    "MXN": { in: 17550000, out: "$175,500.00", outNoDec: "$175,500", outWithCode: space("MXN", "175,500.00"), outWithCodeNoDec: space("MXN", "175,500") },
                    "MYR": { in: 4240000, out: "RM42,400.00", outNoDec: "RM42,400", outWithCode: space("MYR", "42,400.00"), outWithCodeNoDec: space("MYR", "42,400") },
                    "NOK": { in: 7380000, out: space("kr", "73", "800,00"), outNoDec: space("kr", "73", "800"), outWithCode: space("NOK", "73", "800,00"), outWithCodeNoDec: space("NOK", "73", "800") },
                    "NZD": { in: 2100000, out: "$21,000.00", outNoDec: "$21,000", outWithCode: space("NZD", "21,000.00"), outWithCodeNoDec: space("NZD", "21,000") },
                    "PEN": { in: 3730000, out: "S/.37.300,00", outNoDec: "S/.37.300", outWithCode: space("PEN", "37.300,00"), outWithCodeNoDec: space("PEN", "37.300") },
                    "PLN": { in: 4100000, out: space("41", "000,00zł"), outNoDec: space("41", "000zł"), outWithCode: space("PLN", "41", "000,00"), outWithCodeNoDec: space("PLN", "41", "000") },
                    "RON": { in: 4510000, out: space("45.100,00", "LEI"), outNoDec: space("45.100", "LEI"), outWithCode: space("RON", "45.100,00"), outWithCodeNoDec: space("RON", "45.100") },
                    "RUB": { in: 40110000, out: space("401", "100,00", "руб."), outNoDec: space("401", "100", "руб."), outWithCode: space("RUB", "401", "100,00"), outWithCodeNoDec: space("RUB", "401", "100") },
                    "SEK": { in: 8270000, out: space("82", "700,00", "kr"), outNoDec: space("82", "700", "kr"), outWithCode: space("SEK", "82", "700,00"), outWithCodeNoDec: space("SEK", "82", "700") },
                    "SGD": { in: 1650000, out: "$16,500.00", outNoDec: "$16,500", outWithCode: space("SGD", "16,500.00"), outWithCodeNoDec: space("SGD", "16,500") },
                    "THB": { in: 41100000, out: "THB411,000.00", outNoDec: "THB411,000", outWithCode: space("THB", "411,000.00"), outWithCodeNoDec: space("THB", "411,000") },
                    "TRY": { in: 2320000, out: space("23.200,00", "TL"), outNoDec: space("23.200", "TL"), outWithCode: space("TRY", "23.200,00"), outWithCodeNoDec: space("TRY", "23.200") },
                    "TWD": { in: 39530000, out: "NT$395,300.00", outNoDec: "NT$395,300", outWithCode: space("TWD", "395,300.00"), outWithCodeNoDec: space("TWD", "395,300") },
                    "USD": { in: 1290000, out: "$12,900.00", outNoDec: "$12,900", outWithCode: space("USD", "12,900.00"), outWithCodeNoDec: space("USD", "12,900") },
                    "ZAR": { in: 13550000, out: space("R", "135,500.00"), outNoDec: space("R", "135,500"), outWithCode: space("ZAR", "135,500.00"), outWithCodeNoDec: space("ZAR", "135,500") }
                };
            });

            it("should instantiate with no errors", function ()
            {
                expect(new RS.Localisation.CurrencyFormatter(RS.Localisation.currencyMap["GBP"])).to.exist;
            });

            it("should format currencies without decimals correctly", function ()
            {
                for (const code in testCases)
                {
                    const settings = getCurrencySettings(code); // as if from the server
                    expect(settings).to.exist;

                    const formatter = new RS.Localisation.CurrencyFormatter(settings);

                    expect(formatter.format(testCases[code].in, undefined, true)).to.equal(testCases[code].outNoDec, code);
                }
            });

            it("should format currencies without decimals only if there aren't decimals", function ()
            {
                const settings = getCurrencySettings("GBP"); // as if from the server
                expect(settings).to.exist;

                const formatter = new RS.Localisation.CurrencyFormatter(settings);

                expect(formatter.format(100)).to.equal("£1.00");
                expect(formatter.format(100, undefined, true)).to.equal("£1");

                expect(formatter.format(105)).to.equal("£1.05");
                expect(formatter.format(105, undefined, true)).to.equal("£1.05");
            });

            it("should format numbers with the fractional seperator of that currency when using formatDecimal, even when there are no fractional digits", function ()
            {
                // try a currency with a . seperator
                const settingsGBP = getCurrencySettings("GBP"); // as if from the server
                expect(settingsGBP).to.exist;

                const gbpFormatter = new RS.Localisation.CurrencyFormatter(settingsGBP);

                expect(gbpFormatter.formatDecimal(100)).to.equal("£1.00");
                expect(gbpFormatter.formatDecimal(105)).to.equal("£1.05");
                expect(gbpFormatter.formatDecimal(100, false)).to.equal("1.00");
                expect(gbpFormatter.formatDecimal(105, false)).to.equal("1.05");

                //try a currency with a , seperator
                const settingsEUR = getCurrencySettings("EUR"); // as if from the server
                expect(settingsEUR).to.exist;

                const eurFormatter = new RS.Localisation.CurrencyFormatter(settingsEUR);

                expect(eurFormatter.formatDecimal(100)).to.equal("1,00\u00A0€");
                expect(eurFormatter.formatDecimal(105)).to.equal("1,05\u00A0€");
                expect(eurFormatter.formatDecimal(100, false)).to.equal("1,00");
                expect(eurFormatter.formatDecimal(105, false)).to.equal("1,05");

                //try a currency with a , seperator and 0 frac digits
                const settingsISK = getCurrencySettings("ISK"); // as if from the server
                expect(settingsISK).to.exist;

                const iskFormatter = new RS.Localisation.CurrencyFormatter(settingsISK);

                expect(iskFormatter.formatDecimal(100)).to.equal("1,00\u00A0kr.");
                expect(iskFormatter.formatDecimal(105)).to.equal("1,05\u00A0kr.");
                expect(iskFormatter.formatDecimal(100, false)).to.equal("1,00");
                expect(iskFormatter.formatDecimal(105, false)).to.equal("1,05");
            });

            it("should format currencies correctly", function ()
            {
                for (const code in testCases)
                {
                    const settings = getCurrencySettings(code); // as if from the server
                    expect(settings).to.exist;

                    const formatter = new RS.Localisation.CurrencyFormatter(settings);

                    expect(formatter.format(testCases[code].in)).to.equal(testCases[code].out, code);
                }
            });

            it("should format currencies with codes instead of symbols correctly", function ()
            {
                for (const code in testCases)
                {
                    const settings = getCurrencySettings(code); // as if from the server
                    expect(settings).to.exist;

                    const newSettings: RS.Localisation.CurrencySettings = {
                        ...settings,
                        symbol: "",
                        displaySymbol: false,
                        displayCode: true
                    };

                    const formatter = new RS.Localisation.CurrencyFormatter(newSettings);

                    expect(formatter.format(testCases[code].in)).to.equal(testCases[code].outWithCode, code);
                }
            });

            it("should format currencies with codes instead of symbols and without decimals correctly", function ()
            {
                for (const code in testCases)
                {
                    const settings = getCurrencySettings(code); // as if from the server
                    expect(settings).to.exist;

                    const newSettings: RS.Localisation.CurrencySettings = {
                        ...settings,
                        symbol: "",
                        displaySymbol: false,
                        displayCode: true
                    };

                    const formatter = new RS.Localisation.CurrencyFormatter(newSettings);

                    expect(formatter.format(testCases[code].in, undefined, true)).to.equal(testCases[code].outWithCodeNoDec, code);
                }
            });

            it("should change settings", function ()
            {
                const formatter = new RS.Localisation.CurrencyFormatter(getCurrencySettings("GBP"));

                expect(formatter.format(testCases["GBP"].in)).to.equal(testCases["GBP"].out, "original (GBP)");
                expect(formatter.format(testCases["GBP"].in)).to.not.equal(testCases["AUD"].out, "original (GBP)");

                formatter.settings = getCurrencySettings("AUD");

                expect(formatter.format(testCases["AUD"].in)).to.equal(testCases["AUD"].out, "new (AUD)");
                expect(formatter.format(testCases["AUD"].in)).to.not.equal(testCases["GBP"].out, "new (AUD)");
            });
        });
    });
}