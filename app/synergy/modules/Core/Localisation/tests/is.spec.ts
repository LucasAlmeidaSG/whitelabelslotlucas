namespace RS.Tests
{
    const { expect } = chai;

    describe("IsLocalisableString", function()
    {
        const simpleTranslationReference: Localisation.SimpleTranslationReference = { get: () => "", getParts: () => [], clone: Localisation.clone };
        const boundComplexGetter: Localisation.BoundComplexGetter<{}> = { get: () => "", getParts: () => [], substitutions: {}, clone: Localisation.clone }

        interface TestCase
        {
            name: string;
            value: any;
            expect: boolean;
        };
        const testCases: TestCase[] =
        [
            {name: "string", value: "test", expect: true},
            {name: "SimpleTranslationReference", value: simpleTranslationReference, expect: true},
            {name: "BoundComplexGetter", value: boundComplexGetter, expect: true},
            {name: "Object", value: {param1: ""}, expect: false},
            {name: "Array", value: [], expect: false}
        ];

        for (const testCase of testCases)
        {
            if (testCase.expect)
            {
                it(`should return true for '${testCase.name}'`, function ()
                {
                    expect(Is.localisableString(testCase.value)).to.be.true;
                });
            }
            else
            {
                it (`should return false for '${testCase.name}'`, function ()
                {
                    expect(Is.localisableString(testCase.value)).to.be.false;
                });
            }
        }
    })

    // Type narrowing "tests"
    // If the type narrowing is wrong, will cause a test build error
    function expectExplicitType<T>(val: T) { /* */ }
    type UnknownType = number | boolean | string | (() => any) | object;
    type Unknown = UnknownType | UnknownType[];
    const object: Unknown = null;
    if (Is.localisableString(object)) { expectExplicitType<Localisation.LocalisableString>(object)}
}
