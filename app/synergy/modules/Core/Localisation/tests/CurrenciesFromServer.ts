namespace RS.Tests
{
    const spaceCharacter = "\u00A0";
    const currenciesFromServer: RS.Localisation.CurrencySettings[] =
    [
        { name: "ARS", symbol: "$", exchangeRate: 25.0088532043, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 5, displaySymbol: true, displayCode: false },
        { name: "AUD", symbol: "$", exchangeRate: 1.5798748735, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "BGN", symbol: "лв.", exchangeRate: 1.9558299999, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: false, currencyMultiplier: 2, displaySymbol: true, displayCode: false },
        { name: "BRL", symbol: "R$", exchangeRate: 4.0435575995, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 2, displaySymbol: true, displayCode: false },
        { name: "CAD", symbol: "$", exchangeRate: 1.6064295951, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "CHF", symbol: "SFr.", exchangeRate: 1.1709514468, grouping: 3, groupingSeparator: "'", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "CNY", symbol: "¥", exchangeRate: 7.7799836522, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 10, displaySymbol: true, displayCode: false },
        { name: "CZK", symbol: "Kč", exchangeRate: 25.4050609526, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 20, displaySymbol: true, displayCode: false },
        { name: "DKK", symbol: "kr", exchangeRate: 7.4483422566, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 10, displaySymbol: true, displayCode: false },
        { name: "EEK", symbol: "€", exchangeRate: 15.64664, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 15, displaySymbol: true, displayCode: false },
        { name: "EUR", symbol: "€", exchangeRate: 1, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "GBP", symbol: "£", exchangeRate: 0.8830118641, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "HKD", symbol: "HK$", exchangeRate: 9.6502342802, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 10, displaySymbol: true, displayCode: false },
        { name: "HRK", symbol: "Kn", exchangeRate: 7.4427205914, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 10, displaySymbol: true, displayCode: false },
        { name: "HUF", symbol: "Ft", exchangeRate: 311.1416561323, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 300, displaySymbol: true, displayCode: false },
        { name: "INR", symbol: "Rs.", exchangeRate: 80.017336308, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 50, displaySymbol: true, displayCode: false },
        { name: "ISK", symbol: "kr.", exchangeRate: 122.9085445387, grouping: 3, groupingSeparator: ".", fractionalDigits: 0, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 200, displaySymbol: true, displayCode: false },
        { name: "JPY", symbol: "¥", exchangeRate: 130.8368092604, grouping: 3, groupingSeparator: ",", fractionalDigits: 0, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 100, displaySymbol: true, displayCode: false },
        { name: "LTL", symbol: "€", exchangeRate: 3.4528, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 5, displaySymbol: true, displayCode: false },
        { name: "LVL", symbol: "€", exchangeRate: 0.7028, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "MXN", symbol: "$", exchangeRate: 23.0179628425, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 20, displaySymbol: true, displayCode: false },
        { name: "MYR", symbol: "RM", exchangeRate: 4.8245985488, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 5, displaySymbol: true, displayCode: false },
        { name: "NOK", symbol: "kr", exchangeRate: 9.4953524624, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 10, displaySymbol: true, displayCode: false },
        { name: "NZD", symbol: "$", exchangeRate: 1.6927862669, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "PEN", symbol: "S/.", exchangeRate: 4.0156001161, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 4, displaySymbol: true, displayCode: false },
        { name: "PLN", symbol: "zł", exchangeRate: 4.2136601847, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: false, currencyMultiplier: 5, displaySymbol: true, displayCode: false },
        { name: "RON", symbol: "LEI", exchangeRate: 4.6679114941, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 5, displaySymbol: true, displayCode: false },
        { name: "RUB", symbol: "руб.", exchangeRate: 70.7149036634, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 40, displaySymbol: true, displayCode: false },
        { name: "SEK", symbol: "kr", exchangeRate: 10.0711051408, grouping: 3, groupingSeparator: spaceCharacter, fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 10, displaySymbol: true, displayCode: false },
        { name: "SGD", symbol: "$", exchangeRate: 1.617536548, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "THB", symbol: "THB", exchangeRate: 38.4278496466, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 40, displaySymbol: true, displayCode: false },
        { name: "TRY", symbol: "TL", exchangeRate: 4.7940161681, grouping: 3, groupingSeparator: ".", fractionalDigits: 2, fractionalSeparator: ",", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 2, displaySymbol: true, displayCode: false },
        { name: "TWD", symbol: "NT$", exchangeRate: 35.8958368289, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 40, displaySymbol: true, displayCode: false },
        { name: "USD", symbol: "$", exchangeRate: 1.2306464189, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 0, currencySymbolBeforeAmount: true, currencyMultiplier: 1, displaySymbol: true, displayCode: false },
        { name: "ZAR", symbol: "R", exchangeRate: 14.6465261928, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: true, currencyMultiplier: 10, displaySymbol: true, displayCode: false },

        { name: "FREEPLAY", symbol: "", exchangeRate: 1, grouping: 3, groupingSeparator: ",", fractionalDigits: 2, fractionalSeparator: ".", currencyCodeSpacing: 1, currencyCodeBeforeAmount: true, currencySymbolSpacing: 1, currencySymbolBeforeAmount: false, currencyMultiplier: 1, displaySymbol: false, displayCode: false }
    ];

    export function getValidCurrencyCodes(): string[]
    {
        return Object.keys(currenciesFromServer);
    }

    // export function getCurrencySettings(code: string): RS.Localisation.CurrencySettings
    // {
    //     if (!(code in currenciesFromServer)) { throw new Error("Invalid currency code"); }
    //     const xml = currenciesFromServer[code];

    //     const parser = new DOMParser();
    //     const doc = parser.parseFromString(xml, "text/xml");

    //     try
    //     {
    //         return {
    //             name: doc.getElementsByTagName("Currency")[0].textContent,
    //             exchangeRate: parseFloat(doc.getElementsByTagName("ExchangeRate")[0].textContent),
    //             symbol: doc.getElementsByTagName("CurrencySymbol")[0].textContent,
    //             displaySymbol: true,
    //             grouping: parseInt(doc.getElementsByTagName("Grouping")[0].textContent),
    //             groupingSeparator: doc.getElementsByTagName("GroupingSeparator")[0].textContent,
    //             fractionalDigits: parseInt(doc.getElementsByTagName("FracDigits")[0].textContent),
    //             fractionalSeparator: doc.getElementsByTagName("DecimalSeparator")[0].textContent,
    //             currencyCodeSpacing: parseInt(doc.getElementsByTagName("CurrencyCodeSpacing")[0].textContent),
    //             currencyCodeBeforeAmount: doc.getElementsByTagName("CodeBeforeAmount")[0].textContent === "true",
    //             currencySymbolSpacing: parseInt(doc.getElementsByTagName("CurrencySymbolSpacing")[0].textContent),
    //             currencySymbolBeforeAmount: doc.getElementsByTagName("SymbolBeforeAmount")[0].textContent === "true",
    //             currencyMultiplier: parseInt(doc.getElementsByTagName("CurrencyMultiplier")[0].textContent)
    //         };
    //     }
    //     catch (err)
    //     {
    //         Log.error(`Failed to parse XML settings for ${code}`);
    //         throw err;
    //     }
    // }

    export function getCurrencySettings(code: string): RS.Localisation.CurrencySettings
    {
        for (const item of currenciesFromServer)
        {
            if (item.name === code) { return item; }
        }
        throw new Error("Invalid currency code");
    }
}