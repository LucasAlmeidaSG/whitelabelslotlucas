namespace RS.Localisation.AssembleStages {
    /**
     * Responsible for copying the built translations of modules.
     */
    export class CopyLocales extends AssembleStage.Base {
        protected _locales: Util.Map<Util.Map<string>>;

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>) {
            this._locales = {};

            await super.execute(settings, moduleList);

            const translationsDir = Path.combine(settings.outputDir, Constants.outTranslationsFolder);
            await FileSystem.createPath(translationsDir);

            for (const localeCode in this._locales) {
                if (this._locales.hasOwnProperty(localeCode)) {
                    const fileName = Path.combine(translationsDir, `${localeCode}.json`);
                    await FileSystem.writeFile(fileName, JSON.stringify(this._locales[localeCode]));
                }
            }
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base) {
            let translationData: object;
            try {
                translationData = JSON.parse(await FileSystem.readFile(Path.combine(module.path, "build", "translations.json")));
            }
            catch (err) {
                // Module probably has no translations
                return;
            }

            // Iterate all locales, copy to our store
            for (const localeCode in translationData) {
                const inStrings = translationData[localeCode];
                const outStrings = this._locales[localeCode] || (this._locales[localeCode] = {});
                for (const key in inStrings) {
                    outStrings[key] = inStrings[key];
                }
            }
        }
    }

    export const copyLocales = new CopyLocales();
    AssembleStage.register({}, copyLocales);
}