namespace RS.Localisation
{
    import CodeGen = Build.CodeGeneration;

    /** Represents a node in a locale string hierarchy. */
    interface INode
    {
        name: string;
        type: "map"|"array";
        children: List<INode>;
        key?: string;
        vals?: ReadonlyArray<string>;
    }

    interface Substitution
    {
        name: string;
        type: string;
        orig: string;
    }

    function invertStringMap(localeToKeyMap: Util.Map<Util.Map<string>>): Util.Map<Util.Map<string>>
    {
        const out: Util.Map<Util.Map<string>> = {};
        for (const localeCode in localeToKeyMap)
        {
            const keyMap = localeToKeyMap[localeCode];
            for (const key in keyMap)
            {
                const str = keyMap[key];
                if (!(key in out))
                {
                    out[key] = {};
                }
                out[key][localeCode] = str;
            }
        }
        return out;
    }

    /**
     * Generates a node hierarchy for the specified map of locale strings.
     * @param strings Map of 'key' -> 'localeCode' -> 'string'
     */
    function generateLocaleStringHierarchy(strings: Util.Map<ReadonlyArray<string>>): INode
    {
        const g: INode = { name: "g", type: "map", children: new List<INode>() };
        const keys = Object.keys(strings);
        keys.sort();
        for (const key of keys)
        {
            const namePath = key.split(".");
            let curNode = g;
            for (let i = 0, l = namePath.length - 1; i < l; ++i)
            {
                const segment = namePath[i];
                let newNode: INode = curNode.children.find((n) => n.name.toLowerCase() === segment.toLowerCase());
                if (newNode)
                {
                    curNode = newNode;
                }
                else
                {
                    newNode = { name: segment, type: "map", children: new List<INode>() };
                    curNode.children.add(newNode);
                    curNode = newNode;
                }
            }
            const assetNodeName = namePath[namePath.length - 1];
            let assetNode: INode = curNode.children.find((n) => n.name.toLowerCase() === assetNodeName.toLowerCase());
            if (!assetNode)
            {
                assetNode = { name: assetNodeName, type: "map", children: new List<INode>() };
                curNode.children.add(assetNode);
            }
            if (assetNode.key == null)
            {
                assetNode.key = key;
                assetNode.vals = strings[key];
            }
            else
            {
                Log.warn(`Duplicate locale key found ('${key}') when assembling locale string hierarchy for code generation`);
            }
        }
        return g;
    }

    function logLocaleStringHierarchy(rootNode: INode, indent = 0)
    {
        const prefix = ("                ").substr(0, indent);
        Log.debug(`${prefix} - ${rootNode.name}`);
        for (const childNode of rootNode.children.data)
        {
            logLocaleStringHierarchy(childNode, indent + 1);
        }
    }

    function parseSubstitutions(str: string): Substitution[]
    {
        const regex = /\{([a-zA-Z0-9]+[a-zA-Z0-9_]*)(:[a-z]+)?\}/g;
        const results: { name: string; type: string; orig: string; }[] = [];
        let matches: RegExpMatchArray = regex.exec(str);
        while (matches !== null)
        {
            if (matches[1] != null)
            {
                if (matches[2] != null)
                {
                    results.push({ name: matches[1], type: matches[2], orig: matches[0] });
                }
                else
                {
                    results.push({ name: matches[1], type: "RS.Localisation.LocalisableString | number", orig: matches[0] });
                }
            }
            matches = regex.exec(str);
        }
        for (const result of results)
        {
            const isNumeric = !isNaN(parseInt(result.name));
            if (isNumeric)
            {
                if (results.length === 1)
                {
                    result.name = "value";
                }
                else
                {
                    result.name = `arg${result.name}`;
                }
            }
        }
        return results;
    }

    function parseNodeSubstitutions(node: INode): Substitution[]
    {
        const subs: Substitution[] = [];
        const mergedSubs: Util.Map<Substitution> = {};
        for (const val of node.vals)
        {
            const valSubs = parseSubstitutions(val);
            for (const valSub of valSubs)
            {
                const existingSub = mergedSubs[valSub.name];
                if (existingSub != null)
                {
                    if (existingSub.type !== valSub.type) { existingSub.type = `${existingSub.type} | ${valSub.type}`; }
                    continue;
                }

                mergedSubs[valSub.name] = {...valSub};
                subs.push(valSub);
            }
        }
        return subs;
    }

    function getArrayChildren(node: INode): List<INode>
    {
        const result = node.children.filter((c) => Is.number(parseInt(c.name)));
        result.sort((a, b) => parseInt(a.name) - parseInt(b.name));
        return result;
    }

    /**
     * Generates a value for the specified node.
     * @param node
     */
    function generateValue(node: INode): CodeGen.Value
    {
        const arrayChildren = getArrayChildren(node);
        const value = arrayChildren.length === node.children.length && node.key == null
            ? generateArrayValue(node, arrayChildren)
            : generateObjectValue(node);

        return value;
    }

    function generateArrayValue(node: INode, arrayChildren: List<INode>): CodeGen.ArrayValue
    {
        const value: CodeGen.ArrayValue =
        {
            kind: "arrval",
            name: node.name,
            values: []
        };

        // Iterate all subnodes
        for (const subnode of arrayChildren.data)
        {
            const valForSubnode = generateValue(subnode);
            if (valForSubnode != null)
            {
                value.values.push(valForSubnode);
            }
        }

        return value;
    }

    function generateObjectValue(node: INode): CodeGen.ObjectValue
    {
        // Generate object value for it
        const value: CodeGen.ObjectValue =
        {
            kind: "objval",
            name: node.name,
            values: []
        };

        if (node.key != null)
        {
            const subs = parseNodeSubstitutions(node);

            if (subs.length === 0)
            {
                value.values.push({
                    kind: "keyval",
                    name: "get",
                    value: `RS.Localisation.generateSimpleGetter("${node.key}")`
                });
            }
            else
            {
                const innerType = `{ ${subs.map((s) => `${s.name}: ${s.type};`).join(" ")} }`;
                const keyMap = `{ ${subs.map((s) => `${s.name}: "${s.orig}"`).join(", ")} }`;
                value.values.push({
                    kind: "keyval",
                    name: "get",
                    value: `RS.Localisation.generateComplexGetter<${innerType}>("${node.key}", ${keyMap})`
                });
            }
            value.values.push({
                kind: "keyval",
                name: "getParts",
                value: `function (locale: RS.Localisation.Locale) { return RS.Localisation.getParts(locale.getString("${node.key}")); }`
            });
            value.values.push({
                kind: "keyval",
                name: "clone",
                value: `RS.Localisation.clone`
            })
        }

        // Iterate all subnodes
        for (const subnode of node.children.data)
        {
            const valForSubnode = generateValue(subnode);
            if (valForSubnode != null)
            {
                const name = cleanName(subnode.name);
                if (name !== "")
                {
                    value.values.push({
                        kind: "keyval",
                        name: name,
                        value: valForSubnode
                    });
                }
                else
                {
                    Log.warn(`Skipped code generation for subnode '${subnode.name}', couldn't clean the name`);
                }
            }
        }

        return value;
    }

    /**
     * Performs code generation for the specified map of locale strings.
     * @param assets
     */
    export async function performCodeGeneration(module: IModule, path: string, strings: Util.Map<ReadonlyArray<string>>)
    {
        // Generate locale string hierarchy
        const g = generateLocaleStringHierarchy(strings);

        // Determine root asset namespace
        const assetNamespace = Build.Utils.getTranslationNamespace(module);

        // Root namespace
        const rootNamespace: CodeGen.Namespace =
        {
            name: assetNamespace,
            comment: `Contains translation accessors for the '${module.name}' module.`,
            kind: "namespace",
            children: []
        };

        // Iterate all nodes in the global node
        for (const node of g.children.data)
        {
            const value = generateValue(node);
            if (value != null)
            {
                const name = cleanName(node.name);
                if (name !== "")
                {
                    rootNamespace.children.push({
                        kind: "var",
                        isConst: true,
                        name: node.name,
                        value
                    });
                }
                else
                {
                    Log.warn(`Skipped code generation for subnode '${node.name}', couldn't clean the name`);
                }
            }
        }

        // Generate code
        const formatter = new CodeGen.Formatter();
        formatter.emitNamed(rootNamespace);
        const rawCode = formatter.toString();
        await FileSystem.writeFile(path, rawCode);
    }

    /** Generates a locale list file for the given module. */
    export async function generateLocaleList(module: IModule, path: string, locales: ReadonlyArray<string>)
    {
        const rootNamespace: CodeGen.Namespace =
        {
            name: "RS.Translations",
            kind: "namespace",
            children: []
        };

        // Generate list of locales with valid files
        const supportedLocales: CodeGen.Variable =
        {
            kind: "var",
            name: "supportedLocaleCodes",
            comment: `Contains the list of all supported locales.`,
            isConst: true,
            type:
            {
                kind: CodeMeta.TypeRef.Kind.LiteralString,
                value: "ReadonlyArray<string>"
            }
        };
        const array: CodeGen.ArrayValue = supportedLocales.value =
        {
            kind: "arrval",
            name: supportedLocales.name,
            comment: supportedLocales.comment,
            values: []
        };
        for (const locale of locales)
        {
            array.values.push(`"${locale}"`);
        }
        rootNamespace.children.push(supportedLocales);

        // Define fallback locale
        const defaultLocaleCode = locales.indexOf(Constants.referenceLocale) === -1 ? locales[0] : Constants.referenceLocale;
        const defaultLocale: CodeGen.Variable =
        {
            kind: "var",
            name: "defaultLocaleCode",
            // Let it theoretically be overridden
            isConst: false,
            comment: `The default locale that should be used in the event that the game fails to select an appropriate locale from ${supportedLocales.name}`,
            value: defaultLocaleCode && `"${defaultLocaleCode}"` || "null"
        };
        rootNamespace.children.push(defaultLocale);

        const formatter = new CodeGen.Formatter();
        formatter.emitNamed(rootNamespace);
        const rawCode = formatter.toString();
        await FileSystem.writeFile(path, rawCode);
    }

    /**
     * Cleans a name or id for suitable use as an identifier or key in TypeScript.
     * @param name
     */
    export function cleanName(name: string): string
    {
        // Trim
        name = name.trim();

        // Replace any unknown characters with underscores
        name = name.replace(/[^a-zA-Z0-9_]+/g, "_");

        // If it starts with any numbers, strip them
        name = name.replace(/^[0-9]+/g, "");

        return name;
    }
}