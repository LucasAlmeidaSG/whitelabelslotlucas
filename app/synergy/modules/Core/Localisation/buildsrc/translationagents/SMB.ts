/// <reference path="Base.ts" />
/// <reference path="../SMB.ts" />

namespace RS.Localisation.TranslationAgents
{
    const hostname = "red7serv5001";
    const shareName = "scientific-games";
    const domain = "DOMAIN";
    const tmxDir = "Games\\Translation Memory";

    /**
     * Pulls TMX files from the Red7 file server and uses them to translate.
     */
    @TranslationAgent("smb")
    export class SMB extends Base
    {
        protected credentialsFilename = "red7serv5000credentials.json";
        protected endPointName = "red7serv5000";

        public get locales() { return this._locales; }

        public async init(referenceLocale: string)
        {
            // Our setup doesn't support non-english reference locale
            if (referenceLocale !== "en_US" && referenceLocale !== "en_GB")
            {
                throw new Error("Only english (en_US or en_GB) reference locale is supported");
            }

            // If we're not on windows, we need to resolve the smb hostname
            let share: string;
            if (process.platform !== "win32")
            {
                const ip = await SMBClient.resolveHost(hostname);
                Log.debug(`Resolved '${hostname}' to ${ip}`);

                share = `\\\\${ip}\\${shareName}`;
            }
            else
            {
                share = `\\\\${hostname}\\${shareName}`;
            }

            // Load SMB credentials
            await this.requestCredentials();

            // Attempt connection
            const smb = new SMBClient({
                share,
                domain,
                username: this.username,
                password: this.password
            });
            let files: string[];
            try
            {
                files = await smb.readDir(tmxDir);
            }
            catch (err)
            {
                smb.close();
                Log.error(err);
                throw new Error("Connection to SMB server failed");
            }

            // Success, write credentials back if needed
            await this.saveCredentials();

            // Filter tmx files
            const tmxFiles = this.getFilesWithExtension(files, "tmx");

            Log.info(`Loading ${tmxFiles.length} tmx files from SMB server...`);
            const allTranslationUnits = await this.loadTranslationFiles(tmxDir, tmxFiles, (directory, filename) => smb.readFile(`${directory}\\${filename}`), parseTMX);

            Log.info(`Loaded ${allTranslationUnits.length} translation units`);

            this.addLocales(referenceLocale, allTranslationUnits);
        }
    }
}
