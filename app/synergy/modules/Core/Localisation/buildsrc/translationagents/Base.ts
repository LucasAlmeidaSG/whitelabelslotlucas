namespace RS.Localisation.TranslationAgents
{
    interface Constructor extends Function
    {
        prototype: Base;
        new(): Base;
    }

    interface SavedCredentials
    {
        /** username */
        username: string;
        /** password */
        password: string;
        /** Credentials encoding key */
        key: string;
        /** Credentials ??? */
        iv: string;
    }

    export interface TranslationUnit
    {
        fromLocale: string;
        fromText: string;
        toLocale: string;
        toText: string;
    }
    
    export interface ParsedTranslationUnit extends TranslationUnit
    {
        fromTextT: TranslationString;
        toTextT: TranslationString;
    }

    /**
     * Encapsulates a agent capable of translating text between languages.
     */
    export abstract class Base
    {
        /** Gets an array of all supported locale codes. */
        public readonly abstract locales: ReadonlyArray<string>;
        protected credentialsFilename: string = "UnknownEndpointCredentials.json";
        protected endPointName: string = "Unknown Endpoint";
        protected shouldSaveCredentials: boolean = false;
        protected credentialsRaw: string
        protected username: string
        protected _usernameHint: string = "";
        protected password: string;
        protected _units: ReadonlyArray<ParsedTranslationUnit>;
        protected _locales: string[];
        protected srcLocales: string[] = ["en_GB", "en_US"];

        /**
         * Initialises the translation agent and loads all required resources.
         * @param referenceLocale The reference locale to translate (usually en_GB)
         */
        public abstract init(referenceLocale: string): PromiseLike<void>;

        /**
         * Translates the specified string from the reference locale to the specified locale.
         * @param str 
         * @param locale 
         */
        public async translate(str: string, locale: string): Promise<string>
        {
            if (this._locales.indexOf(locale) == -1)
            {
                RS.Log.debug(`translate: ${locale} not found in ${this._locales}`);
                return str;
            }
            
            // Search for unit
            for (const unit of this._units)
            {
                if (this._locales.indexOf(locale) != -1 && unit.toLocale === locale && unit.fromText === str)
                {
                    return unit.toText;
                }
            }

            // No direct match found, fall back to fuzzy match
            const strT = TranslationString.fromString(str);
            for (const unit of this._units)
            {
                if (this._locales.indexOf(locale) != -1 && unit.toLocale === locale)
                {
                    const result = TranslationString.matchAndTranslate(strT, unit.fromTextT, unit.toTextT);
                    if (result != null)
                    {
                        return TranslationString.toString(result, false);
                    }
                }
            }
            return null;
        }

        protected async requestCredentials()
        {
            try
            {
                const smbCredentials: SavedCredentials = await JSON.parseFile(this.credentialsFilename);
                this.username = smbCredentials.username;
                this.password = Crypto.decrypt(smbCredentials.password, Buffer.from(smbCredentials.key, "base64"), Buffer.from(smbCredentials.iv, "base64"));
            }
            catch
            {
                // No credentials found, prompt user for them
                Log.info(`First time access detected, please enter your login details for ${this.endPointName}`);

                Log.info(`${this.endPointName} Username: ${this._usernameHint}`);
                this.username = await Input.readLine();

                Log.info(`${this.endPointName} Password:`);
                this.password = await Input.readLine(null, true);

                this.shouldSaveCredentials = true;
            }
        }

        protected async saveCredentials()
        {
            if (this.shouldSaveCredentials)
            {
                const iv = Crypto.generateIV();
                const key = Crypto.generateKey();
                let password;
                try
                {
                    password = Crypto.encrypt(this.password, key, iv);
                }
                catch (error)
                {
                    RS.Log.error(`Failed to encrypt password, credentials will not be saved.`);
                    return;
                }
                const credentials: SavedCredentials =
                {
                    username: this.username,
                    password,
                    key: key.toString("base64"),
                    iv: iv.toString("base64")
                };
                await FileSystem.writeFile(this.credentialsFilename, JSON.stringify(credentials));
            }
        }

        protected getFilesWithExtension(files: ReadonlyArray<string>, extension: string): string[]
        {
            //RS.Log.info(`Looking for ${extension} files in: ${files}`);
            const outputFiles = files
                .filter((file) => Path.extension(file) === `.${extension}`)
                .map((file) => file.indexOf("._") === 0 ? file.substr(2) : file);
            if (outputFiles.length === 0)
            {
                throw new Error(`No valid translation files found (expecting *.${extension})`);
            }
            Log.debug(`Found ${outputFiles.length} files with extension ${extension}`);
            return outputFiles;
        }

        protected async loadTranslationFiles(rootDirectory: string, files: ReadonlyArray<string>, readFileCallback: TranslationFileReader, parseFileCallback: TranslationFileParser): Promise<ParsedTranslationUnit[]>
        {
            const allTranslationUnits: ParsedTranslationUnit[] = [];
            for (const fileName of files)
            {
                let raw: string | Buffer;
                try
                {
                    raw = (await readFileCallback(rootDirectory, fileName));
                }
                catch (error)
                {
                    Log.error(`Failed to read translation file ${fileName} in ${rootDirectory} due to error:\n${error}`);
                }
                if (!Is.string(raw)) { raw = raw.toString("UTF8"); }
                let units: TranslationUnit[];
                try
                {
                    units = parseFileCallback(raw);
                }
                catch (error)
                {
                    Log.error(`Failed to parse translation file ${fileName} in ${rootDirectory} due to error:\n${error}`);
                }
                for (const unit of units)
                {
                    allTranslationUnits.push({
                        ...unit,
                        fromTextT: TranslationString.fromString(unit.fromText),
                        toTextT: TranslationString.fromString(unit.toText)
                    });
                }
            }
            return allTranslationUnits;
        }

        protected addLocales(referenceLocale: string, allTranslationUnits: ReadonlyArray<ParsedTranslationUnit>)
        {
            this._locales = [ referenceLocale ];
            this._units = allTranslationUnits;

            for (const unit of this._units)
            {
                if (this._locales.indexOf(unit.toLocale) === -1)
                {
                    this._locales.push(unit.toLocale);
                }
            }

            Log.debug(this._locales.join(", "));
        }
    }

    const translationAgents: Util.Map<Constructor> = {};
    export function TranslationAgent(name: string): ClassDecorator
    {
        return function(target: any)
        {
            translationAgents[name] = target as Constructor;
        };
    }

    /**
     * Creates a translation agent for the specified name.
     * May throw if agent implementation not found.
     * @param name 
     */
    export function create(name: string): Base
    {
        if (!(name in translationAgents)) { throw new Error(`Translation agent '${name}' not found`); }
        return new translationAgents[name]();
    }

    export type TranslationFileReader = (rootDirectory: string, filePath: string) => Promise<string | Buffer>;
    export type TranslationFileParser = (raw: string | Buffer) => TranslationUnit[];
}