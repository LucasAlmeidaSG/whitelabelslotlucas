namespace RS.Localisation.TranslationAgents
{
    @TranslationAgent("local")
    export class Local extends Base
    {
        public get locales() { return this._locales; }

        private _translations: List<TranslationLookupEntry>;

        public async init(referenceLocale: string)
        {
            // Get source path.
            Log.info(`Please enter the path to import from:`);
            const sourcePath = (await Input.readLine()).trim();

            // Read source files.
            const sourceFiles = await Localisation.getLangFiles(sourcePath);
            if (sourceFiles.length === 0) { return Log.error(`No locale files found at "${sourcePath}".`); }

            const translationFiles: string[] = [];
            const referenceFiles: string[] = [];
            for (const file of sourceFiles)
            {
                if (Localisation.extractLocaleCode(file) === referenceLocale)
                {
                    referenceFiles.push(file);
                }
                else
                {
                    translationFiles.push(file);
                }
            }

            if (referenceFiles.length === 0)
            {
                Log.warn("No reference file found in source folder. Searching modules...");
                const moduleReferenceFiles = await this.getModuleReferenceFiles(referenceLocale);
                if (moduleReferenceFiles.length === 0) { Log.error("No reference files found in modules."); }
                referenceFiles.push(...moduleReferenceFiles);
            }

            // Generate reference dictionary.
            this._locales = [];
            this._translations = new List();
            await this.importReferenceFiles(referenceFiles);
            await this.importSourceFiles(translationFiles);
        }

        public async translate(text: string, locale: string): Promise<string | null>
        {
            const lookup = this._translations.find((item) => matches(item.referenceText, text));
            if (!lookup) { return null; }

            let out = lookup.translations[locale];
            if (!Is.string(out)) { return null; }
            
            // Correct substitution keys.
            const subKeysIn = substitutions(lookup.referenceText);
            const subKeysOut = substitutions(text);
            for (let i = 0; i < subKeysIn.length; i++)
            {
                const keyIn = subKeysIn[i], keyOut = subKeysOut[i];
                out = out.replace(keyIn, keyOut);
            }

            return out;
        }

        private async getModuleReferenceFiles(referenceLocale: string): Promise<string[]>
        {
            const langFiles: string[] = [];
            const modules = Module.getAll(true);
            for (let i = modules.length - 1; i >= 0; i--)
            {
                const module = modules[i];
                const moduleLangFiles = await Localisation.getLangFiles(module);
                langFiles.push(...moduleLangFiles);
            }
            return langFiles.filter((file) => Localisation.extractLocaleCode(file) === referenceLocale);
        }

        private async importReferenceFiles(files: string[])
        {
            for (const file of files)
            {
                const translations = await this.readLocaleFile(file);
                for (const key of translations.keys)
                {
                    const text = translations.getString(key);
                    if (this._translations.find((item) => matches(item.referenceText, text))) { continue; }
                    this._translations.add(
                    {
                        referenceText: text,
                        referenceKey: key,
                        translations: {}
                    });
                }
            }
        }

        private async importSourceFiles(files: string[])
        {
            for (const file of files)
            {
                const localeCode = Localisation.extractLocaleCode(file);
                if (this._locales.indexOf(localeCode) === -1) { this._locales.push(localeCode); }

                const sourceTranslations = await this.readLocaleFile(file);
                for (const key of sourceTranslations.keys)
                {
                    const lookup = this._translations.find((item) => item.referenceKey === key);
                    if (!lookup) { continue; }
                    const translation = sourceTranslations.getString(key);
                    lookup.translations[localeCode] = translation;
                }
            }
        }

        private async readLocaleFile(path: string)
        {
            const file = Localisation.getTranslationFile(path);
            if (!file) { return null; }
            await file.load();
            return file;
        }
    }

    interface TranslationLookupEntry
    {
        referenceText: string;
        referenceKey: string;
        translations: { [localeCode: string]: string };
    }
}