/// <reference path="Base.ts" />

namespace RS.Localisation.TranslationAgents
{
    interface ParsedTranslationUnit extends TranslationUnit
    {
        fromTextT: TranslationString;
        toTextT: TranslationString;
    }

    /**
     * Pulls TMX files from the Red7 translation repo and uses them to translate.
     */
    @TranslationAgent("gittmx") // TODO: Work out why replacing "gittmx" with GitTMX.AgentName doesn't work.
    export class GitTMX extends Base
    {
        protected credentialsFilename: string = "red7gitcredentials.json";
        protected endPointName: string = "Bitbucket translation repo";

        public get locales() { return this._locales; }

        public async init(referenceLocale: string)
        {
            this._usernameHint = "(Bitbucket Profile > Account settings > Bitbucket profile settings > Username)";
            // Our setup doesn't support non-english reference locale
            if (referenceLocale !== "en_US" && referenceLocale !== "en_GB")
            {
                throw new Error("Only english (en_US or en_GB) reference locale is supported");
            }

            // Load Git credentials
            await this.requestCredentials();
            await RS.Git.clone(`https://${this.username}@bitbucket.org/red7mobile/translation-archive.git`, GitTMX.translationsRepoPath);
            await RS.Git.update(GitTMX.translationsRepoPath, GitTMX.translationsRepoBranch);
            // Success, write credentials back if needed
            await this.saveCredentials();
            
            let filePaths = await FileSystem.readFiles(GitTMX.translationsRepoPath);
            const tmxFiles = this.getFilesWithExtension(filePaths, "tmx");

            const allTranslationUnits: ParsedTranslationUnit[] = await this.loadTranslationFiles(GitTMX.translationsRepoPath, tmxFiles, (d, f) => FileSystem.readFile(`${d}/${f}`), parseTMX);

            this.addLocales(referenceLocale, allTranslationUnits);
        }
    }

    export namespace GitTMX
    {
        export const AgentName = "gittmx";
        export const translationsRepoPath = "../translations";
        export const translationsRepoBranch = "master";
    }
}
