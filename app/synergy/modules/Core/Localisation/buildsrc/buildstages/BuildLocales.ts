/// <reference path="../Constants.ts" />
/// <reference path="../translationfiles/Base.ts" />

namespace RS.Localisation.BuildStages
{
    const refLocale = Constants.referenceLocale;

    const __self = "Core.Localisation";

    export type LocaleStringMap = Util.Map<string>;
    export type ModuleLocales = Util.Map<LocaleStringMap>;

    function namesToString(array: ReadonlyArray<string>)
    {
        return array.map((x) => `'${x}'`).join(", ");
    }

    /**
     * Responsible for building all game translations.
     */
    export class BuildLocales extends BuildStage.Base
    {
        protected _translationData: Util.Map<ModuleLocales>;
        protected _allKeys: List<string>;
        protected _allLocales: List<string>;

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            this._translationData = {};
            this._allKeys = new List<string>();
            this._allLocales = new List<string>();

            const ourName: string = Object.getPrototypeOf(this).constructor.name;
            Log.pushContext(ourName);

            // Scan all modules and find all translation files
            const baseResult = await super.execute(moduleList);

            // Iterate each module
            let moduleCount = 0;
            const missingStrings: Util.Map<string> = {};
            for (const moduleName in this._translationData)
            {
                Log.pushContext(moduleName);

                moduleCount++;

                const data = this._translationData[moduleName];
                const module = Module.getModule(moduleName);

                // Check all locales are present
                if (Constants.checkMissingLocales)
                {
                    const missingLocales = this._allLocales.filter((localeCode) => data[localeCode] == null);
                    if (missingLocales.length === 1)
                    {
                        Log.warn(`Missing locale '${missingLocales.data[0]}'`);
                    }
                    else if (missingLocales.length > 1)
                    {
                        Log.warn(`Missing locales: ${namesToString(missingLocales.data)}`);
                    }

                    for (const missingLocale of missingLocales.data)
                    {
                        // Find a fallback locale by language
                        const missingLanguage = missingLocale.substr(0, missingLocale.indexOf("_"));
                        const substituteLocale = this._allLocales.find((localeCode) =>
                        {
                            if (data[localeCode] == null) { return false; }
                            const languageCode = localeCode.substr(0, localeCode.indexOf("_"));
                            return languageCode === missingLanguage;
                        });

                        if (substituteLocale != null)
                        {
                            Log.warn(`Using substitute locale ${substituteLocale} for missing locale ${missingLocale}`);
                            data[missingLocale] = data[substituteLocale];
                        }
                    }
                }

                const moduleKeys: string[] = [];
                for (const localeCode in data)
                {
                    for (const key in data[localeCode])
                    {
                        // Remove locale symbols from translations
                        data[localeCode][key] = data[localeCode][key].replace(/\|\|\|/gi, "").replace(/\|\|/gi, "");
                        if (moduleKeys.indexOf(key) === -1) { moduleKeys.push(key); }
                    }
                }

                // Check all keys are present
                if (Constants.checkMissingKeys)
                {
                    let substitutionCount = 0, substitutedLocales: string[] = [];
                    const missingModuleKeys: { [localeCode: string]: List<string> } = {};
                    for (const localeCode in data)
                    {
                        const languageCode = localeCode.substr(0, localeCode.indexOf("_"));
                        const strings = data[localeCode];
                        for (const key of moduleKeys)
                        {
                            if (strings[key] == null)
                            {
                                if (!missingModuleKeys[localeCode]) { missingModuleKeys[localeCode] = new List(); }
                                missingModuleKeys[localeCode].add(key);

                                const refString = data[refLocale][key];
                                missingStrings[key] = refString;
                                
                                for (const candidate in data)
                                {
                                    const candidateLanguage = candidate.substr(0, candidate.indexOf("_"));
                                    if (candidateLanguage !== languageCode) { continue; }
                                    const str = data[candidate][key];
                                    if (str == null) { continue; }

                                    data[localeCode][key] = str;

                                    // Update tracking data for logging
                                    if (substitutedLocales.indexOf(localeCode) === -1)
                                    {
                                        substitutedLocales.push(localeCode);
                                    }
                                    ++substitutionCount;

                                    break;
                                }
                            }
                        }
                    }

                    const allMissingKeys = new List<string>();
                    for (const localeCode in missingModuleKeys)
                    {
                        allMissingKeys.addRange(missingModuleKeys[localeCode], true);
                    }

                    const localeGroups: Util.Map<string[]> = {};
                    for (const key of allMissingKeys.data)
                    {
                        const localeCodes: string[] = [];
                        for (const localeCode in missingModuleKeys)
                        {
                            const missingLocaleKeys = missingModuleKeys[localeCode];
                            if (missingLocaleKeys.contains(key))
                            {
                                localeCodes.push(localeCode);
                            }
                        }

                        const hash = localeCodes.join(".");
                        if (localeGroups[hash]) { localeGroups[hash].push(key); }
                        else { localeGroups[hash] = [key]; }
                    }

                    for (const localeGroup in localeGroups)
                    {
                        const localeCodes = localeGroup.split(".");
                        const missingGroupKeys = localeGroups[localeGroup];
                        Log.warn(`Missing key${missingGroupKeys.length === 1 ? "" : "s"} ${namesToString(missingGroupKeys)} in locale${localeCodes.length === 1 ? "" : "s"} ${namesToString(localeCodes)}`);
                    }

                    if (substitutionCount > 0)
                    {
                        Log.warn(`Found substitutions for ${substitutionCount} missing key${substitutionCount === 1 ? "" : "s"} in ${namesToString(substitutedLocales)}`);
                    }
                }

                // Write output
                const buildDir = Path.combine(module.path, "build");
                await FileSystem.createPath(buildDir);
                await FileSystem.writeFile(Path.combine(buildDir, "translations.json"), JSON.stringify(data));

                // Gather a map of all strings
                const stringMap: Util.Map<string[]> = {};
                for (const localeCode of this._allLocales.data)
                {
                    const locale = data[localeCode];
                    for (const key in locale)
                    {
                        if (stringMap[key] == null)
                        {
                            stringMap[key] = [locale[key]];
                        }
                        else
                        {
                            stringMap[key].push(locale[key]);
                        }
                    }
                }

                // Code generation
                await FileSystem.createPath(Path.combine(module.path, "src", "generated"));
                await performCodeGeneration(module, Path.combine(module.path, "src", "generated", "Translations.ts"), stringMap);

                Log.popContext(moduleName);
            }

            // Stats
            Log.info(`Built ${this._allKeys.length} keys, ${this._allLocales.length} locales across ${moduleCount} modules`);

            // Output missing keys
            const missingKeys = Object.keys(missingStrings);
            if (missingKeys.length > 0)
            {
                let missingRefCount = 0;
                const missingKeysFile = new TranslationFiles.JSON(Constants.dumpFile);
                for (const key of missingKeys)
                {
                    const reference = missingStrings[key];
                    if (reference == null) { missingRefCount++; continue; }
                    missingKeysFile.setString(key, reference);
                }
                if (missingRefCount < missingKeys.length) { await missingKeysFile.save(); }

                const keyString = missingKeys.length === 1 ? "key was" : "keys were";
                const refString = missingRefCount === 0 ? "" : ` (${missingRefCount} were missing reference translations)`;
                const dumpCount = missingKeys.length - missingRefCount;
                Log.warn(`${missingKeys.length} ${keyString} missing; dumped ${dumpCount} to '${missingKeysFile.filename}'${refString}`);
            }

            // Localisation module code generation
            const thisModule = Module.getModule(__self);
            if (thisModule)
            {
                await FileSystem.createPath(Path.combine(thisModule.path, "src", "generated"));
                await generateLocaleList(thisModule, Path.combine(thisModule.path, "src", "generated", "Locales.ts"), this._allLocales.dataCopy.sort());
            }
            else
            {
                RS.Log.warn(`Failed to generate supported locale list: localisation module name changed‽`);
            }

            Log.popContext(ourName);

            return { workDone: true, errorCount: baseResult.errorCount };
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const langFiles = await getLangFiles(module);
            if (langFiles.length === 0)
            {
                await this.cleanTranslationFiles(module);
                return { workDone: false, errorCount: 0 };
            }

            const localeData: ModuleLocales = {};
            this._translationData[module.name] = localeData;

            let errorCount = 0;
            for (const file of langFiles)
            {
                const localeCode = extractLocaleCode(file);
                if (localeCode == null)
                {
                    Log.warn(`Translation file '${file}' missing valid locale code (expecting 'en_GB'-like code before extension)`);
                }
                else
                {
                    if (!this._allLocales.contains(localeCode)) { this._allLocales.add(localeCode); }
                    const strings = localeData[localeCode] || (localeData[localeCode] = {});
                    const ext = Path.extension(file);
                    try
                    {
                        let tFile: TranslationFiles.Base | null = null;
                        if (ext === ".json")
                        {
                            tFile = new TranslationFiles.JSON(file);
                        }
                        else if (ext === ".xml")
                        {
                            tFile = new TranslationFiles.WMSXML(file);
                        }
                        if (tFile != null)
                        {
                            await tFile.load();
                            this.parseTranslationFile(tFile, strings, localeCode);
                        }
                    }
                    catch (err)
                    {
                        Log.error(err);
                        ++errorCount;
                    }
                }
            }

            return { workDone: true, errorCount };
        }

        /** Deletes any existing translation files from a module. */
        protected async cleanTranslationFiles(module: Module.Base)
        {
            const codePath = Path.combine(module.path, "src", "generated", "Translations.ts");
            const buildPath = Path.combine(module.path, "build", "translations.json");
            await Promise.all(
            [
                this.cleanFile(codePath),
                this.cleanFile(buildPath)
            ]);
        }

        /** Deletes a file if it exists, logging a message in the process. */
        protected async cleanFile(path: string)
        {
            if (await FileSystem.fileExists(path))
            {
                Log.debug(`Cleaning up old file '${path}'`);
                await FileSystem.deletePath(path);
            }
        }

        protected parseTranslationFile(file: TranslationFiles.Base, strings: Util.Map<string>, localeCode: string): void
        {
            const keys = file.keys;
            for (const key of keys)
            {
                if (!this._allKeys.contains(key)) { this._allKeys.add(key); }
                if (strings[key] != null)
                {
                    Log.warn(`Duplicate key '${key}' in locale '${localeCode}' (${file.filename})`);
                }
                else
                {
                    strings[key] = file.getString(key);
                }
            }
        }
    }

    export const buildLocales = new BuildLocales();
    BuildStage.register({ before: [RS.BuildStages.compile] }, buildLocales);
}
