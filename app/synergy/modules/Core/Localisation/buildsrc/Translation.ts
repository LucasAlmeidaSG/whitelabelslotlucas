/// <reference path="translationagents/Base.ts" />
/// <reference path="translationfiles/Base.ts" />

namespace RS.Localisation
{
    /** Returns text with the substitution keys stripped. */
    function strip(a: string)
    {
        return a.replace(/{([^}]+)}/g, "{}");
    }

    /** Returns whether the two localisations match, ignoring . */
    export function matches(a: string, b: string)
    {
        return strip(a) === strip(b);
    }

    /** Returns an array of substitution keys in left-to-right order. */
    export function substitutions(a: string): string[]
    {
        const match = a.match(/{([^}]+)}/g);
        return match ? match.map((wrappedKey) => wrappedKey.substr(1, wrappedKey.length - 2)) : [];
    }

    /**
     * Gets all language files for a module.
     * @param module
     */
    export function getLangFiles(module: Module.Base): Promise<string[]>;
    /**
     * Gets all language files at a given path.
     * @param module
     */
    export function getLangFiles(path: string): Promise<string[]>;
    export async function getLangFiles(p1: Module.Base | string)
    {
        let translationsRoot: string;
        if (Is.string(p1))
        {
            translationsRoot = p1;
        }
        else
        {
            const module = p1;
            translationsRoot = Path.combine(module.path, Constants.inTranslationsFolder);
        }

        let allFiles: string[];
        try
        {
            allFiles = await FileSystem.readFiles(translationsRoot);
        }
        catch (err)
        {
            // Module probably doesn't have translations
            return [];
        }

        const acceptedExtensions = { ".json": true, ".xml": true };
        return allFiles.filter((f) => acceptedExtensions[Path.extension(f)]);
    }

    /**
     * Extracts a locale code from a filename.
     * e.g. "framework_en_GB.json" => "en_GB"
     * @param filename
     * @returns the locale code, or null
     */
    export function extractLocaleCode(filename: string): string | null
    {
        const result = /([a-z]{2}_[A-Z]{2})\./g.exec(filename);
        if (!result || result.length <= 1) { return null; }
        return result[1];
    }

    /**
     * Returns a translation file at the given path.
     * @param path
     * @returns a translation file object, or null
     */
    export function getTranslationFile(path: string): TranslationFiles.Base | null
    {
        switch (Path.extension(path))
        {
            case ".json":
                return new TranslationFiles.JSON(path);
            case ".xml":
                // TODO: Support xml files?
                // return new TranslationFiles.WMSXML(langFile);
            default:
                return null;
        }
    }

    /**
     * Performs translation logic for the specified list of modules.
     * @param modules
     * @param agentName
     */
    export async function doTranslation(modules: List<Module.Base>, agentName: string, overwrite: boolean)
    {
        const refLocale = Constants.referenceLocale;
        
        // Get an agent and initialise it
        const agent = TranslationAgents.create(agentName);
        await agent.init(refLocale);

        // Iterate all modules and discover translations for the reference locale
        interface ModuleData
        {
            refTranslations: TranslationFiles.Base[];
            existingTranslations: Util.Map<TranslationFiles.Base[]>;
        }
        const moduleMap: Util.Map<ModuleData> = {};
        for (const module of modules.data)
        {
            const moduleData = moduleMap[module.name] || (moduleMap[module.name] = { refTranslations: [], existingTranslations: {} });
            const langFiles = await getLangFiles(module);
            for (const langFile of langFiles)
            {
                const localeCode = extractLocaleCode(langFile);
                const tFile = getTranslationFile(langFile);
                if (tFile != null)
                {
                    await tFile.load();

                    if (localeCode === refLocale)
                    {
                        moduleData.refTranslations.push(tFile);
                    }
                    else
                    {
                        (moduleData.existingTranslations[localeCode] || (moduleData.existingTranslations[localeCode] = [])).push(tFile);
                    }
                }
            }
        }
        Log.info(`Performing translation for ${Object.keys(moduleMap).length} modules...`);

        // Iterate all modules
        const missingTranslationsMap: Util.Map<string> = {};
        const timer = new RS.Timer();
        for (const moduleName in moduleMap)
        {
            Log.pushContext(moduleName);
            timer.start();

            let hitCount = 0, missCount = 0, alreadyCount = 0, updatedCount = 0;
            
            const module = Module.getModule(moduleName);
            const moduleData = moduleMap[moduleName];

            // Iterate all reference translation files
            for (const srcFile of moduleData.refTranslations)
            {
                // Iterate all locales
                for (const localeCode of agent.locales)
                {
                    // Don't translate reference locale
                    if (localeCode !== refLocale)
                    {
                        // Generate a file for us
                        const fileName = srcFile.filename.replace(refLocale, localeCode);
                        const dstFile = Object.getPrototypeOf(srcFile).constructor(fileName) as TranslationFiles.Base;

                        // Lookup existing translations
                        const existingTranslations = moduleData.existingTranslations[localeCode];

                        // Iterate all translation keys
                        for (const key of srcFile.keys)
                        {
                            const srcString = srcFile.getString(key);

                            let translatedString: string | null = null;

                            // See if it's already translated
                            if (existingTranslations)
                            {
                                for (const existingFile of existingTranslations)
                                {
                                    translatedString = existingFile.getString(key);
                                    if (translatedString != null)
                                    {
                                        // Log.debug(`Translating ${key} to ${localeCode} - translation already exists`);
                                        ++alreadyCount;
                                        break;
                                    }
                                }
                            }

                            if (translatedString == null || overwrite)
                            {
                                // Translate it via the agent
                                let newString: string;
                                try
                                {
                                    newString = await agent.translate(srcString, localeCode);
                                }
                                catch (error)
                                {
                                    Log.popContext(moduleName);
                                    throw new Error(`${error} (whilst translating "${srcString}" into ${localeCode})`);
                                }

                                if (newString != null)
                                {
                                    // If we already had a string but we overwrote it, track that
                                    if (translatedString != null && translatedString != newString)
                                    {
                                        ++updatedCount;
                                    }
                                    // Apply update
                                    translatedString = newString;
                                }
                            }

                            if (translatedString != null)
                            {
                                dstFile.setString(key, translatedString);
                                ++hitCount;
                            }
                            else
                            {
                                missingTranslationsMap[key] = srcString;
                                ++missCount;
                            }
                        }

                        // Save it
                        await dstFile.save();
                    }
                }
                
            }

            if (timer.elapsed > 10)
            {
                if (missCount > 0)
                {
                    Log.info(`Translated ${hitCount + updatedCount - alreadyCount}/${hitCount} keys (${missCount} missing) in ${timer.toString()}`);
                }
                else if (hitCount > 0)
                {
                    Log.info(`Translated ${hitCount + updatedCount - alreadyCount}/${hitCount} keys in ${timer.toString()}`);
                }
            }
            else
            {
                if (missCount > 0)
                {
                    Log.info(`Translated ${hitCount + updatedCount - alreadyCount}/${hitCount} keys (${missCount} missing)`);
                }
                else if (hitCount > 0)
                {
                    Log.info(`Translated ${hitCount + updatedCount - alreadyCount}/${hitCount} keys`);
                }
            }
            timer.stop();
            Log.popContext(moduleName);
        }

        // Output missing keys
        const missingKeys = Object.keys(missingTranslationsMap);
        if (missingKeys.length > 0)
        {
            const missingKeysFile = new TranslationFiles.JSON(Constants.dumpFile);
            for (const key of missingKeys)
            {
                missingKeysFile.setString(key, missingTranslationsMap[key]);
            }
            await missingKeysFile.save();

            Log.info(`${missingKeys.length} keys could not be translated, dumped to '${missingKeysFile.filename}'`);
        }
    }
}
