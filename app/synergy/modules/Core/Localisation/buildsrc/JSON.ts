namespace RS.Localisation
{
    export interface TranslationMap
    {
        [key: string]: { source: string, target: string, output: object };
    }

    interface LocaleFile
    {
        [key: string]: string
    }

    export async function updateJSONFolder(sourceFolder: string, destinationFolder: string): Promise<TranslationMap>
    {
        Log.info(`Importing translations from ${sourceFolder} into ${destinationFolder}`);
        
        if (!FileSystem.fileExists(sourceFolder))
        {
            await FileSystem.createPath(sourceFolder);
        }
        if (!FileSystem.fileExists(destinationFolder))
        {
            await FileSystem.createPath(destinationFolder);
        }

        const sourceFiles = await Localisation.getLangFiles(sourceFolder);
        const targetFiles = await Localisation.getLangFiles(destinationFolder);
        Log.info(`${sourceFiles.length} source files found, ${targetFiles.length} target files found`);

        const translationMap: TranslationMap = {};
        for (const file of sourceFiles)
        {
            const locale = Localisation.extractLocaleCode(file.toLocaleLowerCase());
            translationMap[locale] = { source: file, target: null, output: null };
        }
        for (const file of targetFiles)
        {
            const locale = Localisation.extractLocaleCode(file.toLocaleLowerCase());
            if (translationMap[locale])
            {
                translationMap[locale].target = file;
            }
        }

        for (const locale in translationMap)
        {
            const fileJSON = await JSON.parseFile(translationMap[locale].source);
            let outFile: LocaleFile = {};
            if (translationMap[locale].target)
            {
                Log.info(`Trying to read file ${translationMap[locale].target}`);
                try
                {
                    outFile = await JSON.parseFile(translationMap[locale].target);
                }
                catch (error)
                {
                    // No target file to insert into
                    Log.error(`Failed to parse for file ${translationMap[locale].target}, creating new file, err: ${error}`);
                    translationMap[locale].target = `${destinationFolder}/${locale}.json`;
                }
            }
            else
            {
                Log.info(`No target file found for ${locale}, creating new path & file`);
                translationMap[locale].target = `${destinationFolder}/${locale}.json`;
                await FileSystem.createPath(destinationFolder);
            }
            const outObj = { ...outFile, ...fileJSON };
            await FileSystem.writeFile(translationMap[locale].target, JSON.stringify(outObj, null, 2));
            translationMap[locale].output = outObj;
        }
        Log.info(`Updated ${Object.keys(translationMap).length} translation files.`);
        return translationMap;
    }
}