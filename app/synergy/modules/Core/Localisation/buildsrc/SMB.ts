namespace RS
{
    const SMB2 = require("smb2");

    export class SMBClient
    {
        protected _settings: SMBClient.Settings;
        protected _client: any;

        public constructor(settings: SMBClient.Settings)
        {
            this._settings = settings;
            this._client = new SMB2(settings);
        }

        /**
         * Gets all files in the specified directory.
         * @param path 
         */
        public readDir(path: string): Promise<string[]>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.readdir(path, (err?: string, files?: string[]) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve(files);
                    }
                });
            });
        }

        /**
         * Reads the specified file in binary form.
         * @param filename 
         */
        public readFile(filename: string): Promise<Buffer>;

        /**
         * Reads the specified file in text form.
         * @param filename 
         */
        public readFile(filename: string, encoding: string): Promise<string>;

        public readFile(filename: string, encoding?: string): Promise<Buffer | string>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.readFile(filename, (err?: string, data?: Buffer | string) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve(data);
                    }
                });
            });
        }

        /**
         * Writes the specified file in binary form.
         * @param filename 
         * @param data
         */
        public writeFile(filename: string, data: Buffer): Promise<void>;

        /**
         * Writes the specified file in text form.
         * @param filename 
         * @param data 
         * @param encoding 
         */
        public writeFile(filename: string, data: string, encoding?: string): Promise<void>;

        public writeFile(filename: string, data: Buffer | string, encoding?: string): Promise<void>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.writeFile(filename, (err?: string) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve();
                    }
                });
            });
        }

        /**
         * Creates the specified directory.
         * @param path 
         */
        public createDir(path: string): Promise<void>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.mkdir(path, (err?: string) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve();
                    }
                });
            });
        }

        /**
         * Removes the specified directory.
         * @param path 
         */
        public deleteDir(path: string): Promise<void>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.rmdir(path, (err?: string) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve();
                    }
                });
            });
        }

        /**
         * Gets if the specified path exists.
         * @param path 
         */
        public exists(path: string): Promise<boolean>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.exists(path, (err?: string, exists?: boolean) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve(exists);
                    }
                });
            });
        }

        /**
         * Removes the specified file.
         * @param path 
         */
        public deleteFile(path: string): Promise<void>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.unlink(path, (err?: string) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve();
                    }
                });
            });
        }

        /**
         * Renames the specified file or directory.
         * @param oldPath 
         * @param newPath 
         */
        public rename(oldPath: string, newPath: string): Promise<void>
        {
            return new Promise((resolve, reject) =>
            {
                this._client.unlink(oldPath, newPath, (err?: string) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve();
                    }
                });
            });
        }

        /**
         * Closes the SMB client.
         */
        public close()
        {
            if (!this._client) { return; }
            this._client.close();
            this._client = null;
        }
    }

    export namespace SMBClient
    {
        export interface Settings
        {
            share: string;
            domain: string;
            username: string;
            password: string;
            port?: number;
            packetConcurrency?: number;
            autoCloseTimeout?: number;
        }

        async function resolveHostViaNmblookup(hostname: string)
        {
            let lookupOutput: string;
            try
            {
                lookupOutput = await Command.run("nmblookup", [ hostname ], undefined, false);
            }
            catch (err)
            {
                if (/Can't load/g.test(err))
                {
                    await FileSystem.writeFile("smb3.conf", "");
                    try
                    {
                        lookupOutput = await Command.run("nmblookup", [ "-s", "smb3.conf", hostname ], undefined, false);
                    }
                    catch (err)
                    {
                        throw err;
                    }
                    finally
                    {
                        await FileSystem.deletePath("smb3.conf");
                    }
                }
                else
                {
                    throw err;
                }
            }
            const matchRegex = new RegExp(`([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})\\s+${hostname}<[0-9]{2}>`, "g");
            const match = matchRegex.exec(lookupOutput);
            if (!match || match.length <= 1)
            {
                Log.debug(lookupOutput);
                throw new Error(`Failed to resolve SMB hostname '${hostname}'`);
            }

            const ip = match[1];
            return ip;
        }

        async function resolveHostViaSmbutil(hostname: string)
        {
            let lookupOutput: string;
            try
            {
                lookupOutput = await Command.run("smbutil", [ "lookup", hostname ], undefined, false);
            }
            catch (err)
            {
                throw err;
            }
            const matchRegex = new RegExp(`IP address of ${hostname}: ([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})`, "g");
            const match = matchRegex.exec(lookupOutput);
            if (!match || match.length <= 1)
            {
                Log.debug(lookupOutput);
                throw new Error(`Failed to resolve SMB hostname '${hostname}'`);
            }

            const ip = match[1];
            return ip;
        }

        /**
         * Resolves the specified SMB hostname into an IP address.
         * @param hostname 
         */
        export async function resolveHost(hostname: string)
        {
            let numCommandsFound = 0;
            if (await Command.exists("nmblookup"))
            {
                // Log.debug(`Trying nmblookup...`);
                ++numCommandsFound;
                try
                {
                    return resolveHostViaNmblookup(hostname);
                }
                catch (err)
                {
                    Log.warn(`Error executing nmblookup (${err})`);
                }
            }
            if (await Command.exists("smbutil"))
            {
                // Log.debug(`Trying smbutil...`);
                ++numCommandsFound;
                try
                {
                    return resolveHostViaSmbutil(hostname);
                }
                catch (err)
                {
                    Log.warn(`Error executing smbutil (${err})`);
                }
                
            }

            // If we got this far, either commands are missing or errored
            if (numCommandsFound === 0)
            {
                throw new Error(`Command 'nmblookup' or 'smbutil' not found, please install an SMB client`);
            }
            else
            {
                throw new Error(`One or more SMB commands failed`);
            }
        }
    }
}