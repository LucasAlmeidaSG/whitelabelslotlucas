namespace RS.Localisation
{
    export interface IModule extends RS.Module.Base
    {
        info: IModuleInfo;
    }

    export interface IModuleInfo extends RS.Module.Info
    {
        localisation?:
        {
            namespace?: string;
            reponamespace?: string;
        }
    }
}