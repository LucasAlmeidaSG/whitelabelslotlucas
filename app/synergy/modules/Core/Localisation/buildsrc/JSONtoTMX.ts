/**
 * Currently this builds all the TMX files and then writes them
 * In future this could potentially just generate and write them one at a time to save memory or even do them all async
 * Currently takes 20s to build 2000 json source files and push
 */
namespace RS.Localisation
{
    export interface TMXFileFormat
    {
        [translationKey: string]: ILocaleMap;
    };

    interface ILocaleMap
    {
        [locale: string]: string;
    }

    export async function generateTMXFiles(tmxOutputFolder: string, sourceFolder: string): Promise<void>
    {
        const srcLocale = Constants.referenceLocale;
        const outputLocales: {[locale: string]: boolean} = {};
        Log.info(`Generating TMX files, outputting to ${tmxOutputFolder}, using ${sourceFolder} as source.`);

        const filePaths = (await FileSystem.readFiles(sourceFolder)).filter((f) => Path.extension(f) == ".json");

        if (!filePaths || filePaths.length == 0)
        {
            Log.info(`No files found for export in source folder ${sourceFolder}`);
            return;
        }
        Log.info(`${filePaths.length} json files found in sourceFolder ${sourceFolder} for generating tmx files`);
        
        const tmxFiles: TMXFileFormat[] = [];
        for (const fileName of filePaths)
        {
            tmxFiles[srcLocale] = tmxFiles[srcLocale] || {}
            // TODO: Parse TMX files and only insert our changes, rather than re-building.

            // Build up JSON object from locale files
            let parsedFile: object;
            try
            {
                parsedFile = await JSON.parseFile(fileName);
            }
            catch (error)
            {
                Log.error(`Failed to parse file ${fileName}, error: ${error}`);
                continue;
            }
            const translationKeys = Object.keys(parsedFile);
            const locale = extractLegacyLocaleCode(fileName);
            if (!locale)
            {
                Log.error(`Failed to obtain locale from filename: ${fileName}`);
                continue;
            }
            const capitalisedLocale = `${locale.split("_")[0].toLowerCase()}_${locale.split("_")[1].toLocaleUpperCase()}`
            for (let key of translationKeys)
            {
                tmxFiles[srcLocale][key] = tmxFiles[srcLocale][key] || {};
                tmxFiles[srcLocale][key][capitalisedLocale] = parsedFile[key];
            }
            outputLocales[capitalisedLocale] = true;
        }
        
        try
        {
            await FileSystem.createPath(tmxOutputFolder);
        }
        catch (error)
        {
            Log.error(`Error while creating output path: ${tmxOutputFolder}, ${error}`);
            return;
        }

        for (const locale in tmxFiles)
        {
            Log.info(`Generating tmx files for ${Object.keys(outputLocales).length} locale(s): ${Object.keys(outputLocales)}`);
            for (const outLocale in outputLocales)
            {
                let output: string;
                // await FileSystem.writeFile(`${tmxOutputFolder}/${locale}-${outLocale}.json`, JSON.stringify(TMXFiles[locale], null, 2));
                try
                {
                    output = formatAsTMX(tmxFiles[locale], locale, outLocale);
                }
                catch (error)
                {
                    Log.error(`Failed to build TMX file for ${locale}->${outLocale} due to ${error}`);
                    continue;
                }
                try
                {
                    await FileSystem.writeFile(`${tmxOutputFolder}/${locale}-${outLocale}.tmx`, output);
                }
                catch (error)
                {
                    Log.error(`Failed to write TMX file ${tmxOutputFolder}/${locale}-${outLocale}.tmx due to ${error}`);
                }
            }
        }
    }
    
    function formatAsTMX(file: TMXFileFormat, srcLocale: string, targetLocale: string): string
    {
        const upperSrcLocale = srcLocale;
        const upperTargetLocale = targetLocale;
        // TODO: Write this to be a nicer XML formatter
        const filePieces: string[] = [];
        let depth = 0;
        addFilePiece(filePieces, `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>`, depth);
        addFilePiece(filePieces, `<tmx version="1.4">`, depth);
        depth++;
        addFilePiece(filePieces, `<header creationtool="SDLTM Converter" creationtoolversion="1.0" segtype="sentence" o-tmf="SDLTM" adminlang="${upperSrcLocale.split("-")[0]}" srclang="${upperSrcLocale}" datatype="unknown"/>`, depth);
        addFilePiece(filePieces, `<body>`, depth);
        depth++;
        // RS.Log.info(`Building tmx file with ${Object.keys(file).length} keys.`);
        for (const key in file)
        {
            const sourceTranslation = file[key][upperSrcLocale];
            const outTranslation = file[key][upperTargetLocale];
            if (sourceTranslation && outTranslation)
            {
                const entry = generateEntry(sourceTranslation, outTranslation, depth, upperSrcLocale, upperTargetLocale);
                addFilePiece(filePieces, entry, 0);
            }
        }
        depth--;
        addFilePiece(filePieces, `</body>`, depth);
        depth--;
        addFilePiece(filePieces, `</tmx>`, depth);
        // RS.Log.info(`Build tmx file with ${filePieces.length} pieces.`);
        return filePieces.join("\n");
    }

    function generateEntry(key: string, out: string, depth: number, srcLocale: string, targetLocale: string): string
    {
        const pieces: string[] = [];
        // Format: 20130510T112813Z
        const date = new Date(Date.now()).toISOString().replace(/[-:.]/g, "");
        const name = require("os").userInfo().username;
        const usageCount = 0; // No idea what this is

        // Open
        addFilePiece(pieces, `<tu>`, depth);// creationdate="${date}" creationid="${name}", changedate="${date}", changeid="${name}", usagecount="${usageCount}">`, depth);
        depth++;
        // Input line
        addFilePiece(pieces, `<tuv xml:lang="${srcLocale}">`, depth);
        depth++;
        addFilePiece(pieces, `<seg>${cleanStringForXML(key)}</seg>`, depth);
        depth--;
        addFilePiece(pieces, `</tuv>`, depth);
        // Output line
        addFilePiece(pieces, `<tuv xml:lang="${targetLocale}">`, depth);
        depth++;
        addFilePiece(pieces, `<seg>${cleanStringForXML(out)}</seg>`, depth);
        depth--;
        addFilePiece(pieces, `</tuv>`, depth);
        // Close
        depth--;
        addFilePiece(pieces, `</tu>`, depth);

        return pieces.join("\n");
    }

    function cleanStringForXML(str: string): string
    {
        str = str.replace(/[<]/g, "&lt;")
        str = str.replace(/[>]/g, "&gt;");
        str = str.replace(/[&]/g, "&amp;");
        return str;
    }

    function addFilePiece(pieces: string[], str: string, depth: number): string[]
    {
        pieces.push(`${Array(depth+1).join("\t")}${str}`);
        return pieces;
    }

    /**
     * Extracts a locale code from a filename.
     * e.g. "framework_en_GB.json" => "en_GB"
     * Legacy version of this function accepts all lower case
     * @param filename
     * @returns the locale code, or null
     */
    function extractLegacyLocaleCode(filename: string): string | null
    {
        const result = /([a-z]{2}_[a-zA-Z]{2})\./g.exec(filename);
        if (!result || result.length <= 1) { return null; }
        return result[1];
    }
}