namespace RS.Localisation
{
    export namespace TranslationSegment
    {
        export enum Kind
        {
            Text,
            Variable,
            ProtectedTerm
        }

        export interface Base<TKind extends Kind>
        {
            kind: TKind;
            raw: string;
        }

        export interface Text extends Base<Kind.Text> { }
        export interface Variable extends Base<Kind.Variable>
        {
            leftExtraneous: string;
            name: string;
            rightExtraneous: string;
        }
        export interface ProtectedTerm extends Base<Kind.ProtectedTerm>
        {
            leftExtraneous: string;
            term: string;
            rightExtraneous: string;
        }
    }
    export type TranslationSegment = Readonly<
        TranslationSegment.Text |
        TranslationSegment.Variable |
        TranslationSegment.ProtectedTerm
    >;

    export interface TranslationString
    {
        readonly leftExtraneous: string;
        readonly segments: ReadonlyArray<TranslationSegment>;
        readonly rightExtraneous: string;
    }

    export namespace TranslationString
    {
        export function toString(tStr: TranslationString, breakdown: boolean = false): string
        {
            const parts: string[] = [];
            parts.push(tStr.leftExtraneous);
            if (breakdown)
            {
                for (const seg of tStr.segments)
                {
                    parts.push("[");
                    switch (seg.kind)
                    {
                        case TranslationSegment.Kind.Text:
                            parts.push("\"");
                            parts.push(seg.raw);
                            parts.push("\"");
                            break;
                        case TranslationSegment.Kind.Variable:
                            if (seg.leftExtraneous)
                            {
                                parts.push("\"");
                                parts.push(seg.leftExtraneous);
                                parts.push("\" ");
                            }
                            parts.push("Variable '");
                            parts.push(seg.name);
                            parts.push("'");
                            if (seg.rightExtraneous)
                            {
                                parts.push(" \"");
                                parts.push(seg.rightExtraneous);
                                parts.push("\"");
                            }
                            break;
                        case TranslationSegment.Kind.ProtectedTerm:
                            if (seg.leftExtraneous)
                            {
                                parts.push("\"");
                                parts.push(seg.leftExtraneous);
                                parts.push("\" ");
                            }
                            parts.push("ProtectedTerm '");
                            parts.push(seg.term);
                            parts.push("'");
                            if (seg.rightExtraneous)
                            {
                                parts.push(" \"");
                                parts.push(seg.rightExtraneous);
                                parts.push("\"");
                            }
                            break;
                    }
                    parts.push("]");
                }
            }
            else
            {
                for (const seg of tStr.segments)
                {
                    parts.push(seg.raw);
                }
            }
            parts.push(tStr.rightExtraneous);
            return parts.join("");
        }

        const rgVariable = /([\s\.,_\-&%\*"'\;\:#!\=\+]*)\{\{?([a-zA-Z0-9_\-]+)\}\}?([\s\.,_\-&%\*"'\;\:#!\=\+]*)/g;
        const rgProtectedTerm = /([\s\.,_\-&%\*"'\;\:#!\=\+]*)|||([^|]+)|||([\s\.,_\-&%\*"'\;\:#!\=\+]*)/g;
        const rgPreExtraneous = /^[\s\.,_\-&%\*"'\;\:#!\=\+]+/;
        const rgPostExtraneous = /[\s\.,_\-&%\*"'\;\:#!\=\+]+$/;

        export function fromString(str: string): TranslationString
        {
            // Strip pre and post extraneous characters from str
            const preExtraneousMatch = rgPreExtraneous.exec(str);
            const preExtraneous = preExtraneousMatch != null ? preExtraneousMatch[0] : "";
            str = str.substr(preExtraneous.length);
            const postExtraneousMatch = rgPostExtraneous.exec(str);
            const postExtraneous = postExtraneousMatch != null ? postExtraneousMatch[0] : "";
            str = str.substr(0, str.length - postExtraneous.length);

            interface MappedSegment
            {
                kind: TranslationSegment.Kind;
                start: number;
                end: number;
                captureGroups: string[];
            }
            const mappedSegs: MappedSegment[] = [];

            function identify(regex: RegExp, kind: TranslationSegment.Kind): void
            {
                let match: RegExpMatchArray = regex.exec(str);
                while (match && match[0].length > 0)
                {
                    const matchStart = match.index;
                    const matchEnd = matchStart + match[0].length - 1;

                    // check that it doesn't intersect an existing segment
                    let doesIntersect = false;
                    for (const existingSeg of mappedSegs)
                    {
                        if (!(existingSeg.end < matchStart || existingSeg.start > matchEnd))
                        {
                            doesIntersect = true;
                            break;
                        }
                    }
                    if (!doesIntersect)
                    {
                        mappedSegs.push({ kind, start: matchStart, end: matchEnd, captureGroups: match.slice(1) });
                    }

                    // next match
                    match = regex.exec(str);
                }
            }

            // Identify variables and protected terms
            identify(rgVariable, TranslationSegment.Kind.Variable);
            identify(rgProtectedTerm, TranslationSegment.Kind.ProtectedTerm);

            // Convert mapped segs into actual sorted segs
            const result: TranslationSegment[] = [];
            mappedSegs.sort((a, b) => a.start - b.start);
            let curIdx = 0;
            for (const mappedSeg of mappedSegs)
            {
                // Is there a bit of text in between the previous segment and this one?
                if (mappedSeg.start > curIdx)
                {
                    // Insert text
                    result.push({ kind: TranslationSegment.Kind.Text, raw: str.substring(curIdx, mappedSeg.start) });
                }

                // Insert this segment
                const raw = str.substring(mappedSeg.start, mappedSeg.end + 1);
                switch (mappedSeg.kind)
                {
                    case TranslationSegment.Kind.Text:
                        result.push({ kind: mappedSeg.kind, raw });
                        break;
                    case TranslationSegment.Kind.Variable:
                        result.push({
                            kind: TranslationSegment.Kind.Variable,
                            raw,
                            leftExtraneous: mappedSeg.captureGroups[0],
                            name: mappedSeg.captureGroups[1],
                            rightExtraneous: mappedSeg.captureGroups[2]
                        });
                        break;
                    case TranslationSegment.Kind.ProtectedTerm:
                        result.push({
                            kind: TranslationSegment.Kind.ProtectedTerm,
                            raw,
                            leftExtraneous: mappedSeg.captureGroups[0],
                            term: mappedSeg.captureGroups[1],
                            rightExtraneous: mappedSeg.captureGroups[2]
                        });
                        break;
                }

                // Update index
                curIdx = mappedSeg.end + 1;
            }

            // Is there a bit of text at the end?
            if (curIdx < str.length)
            {
                // Insert text
                result.push({ kind: TranslationSegment.Kind.Text, raw: str.substring(curIdx) });
            }

            return { leftExtraneous: preExtraneous, segments: result, rightExtraneous: postExtraneous };
        }

        /**
         * Attempts to translate src using the reference src and dst translations.
         * Performs a fuzzy match that allows some variation between src and refSrc (e.g. extraneous characters and whitespace, different variable names etc).
         * Returns null if no match.
         * @param src 
         * @param refSrc 
         * @param refDst 
         */
        export function matchAndTranslate(src: TranslationString, refSrc: TranslationString, refDst: TranslationString): TranslationString | null
        {
            // Our objective here is to:
            // - map refSrc onto src in a way where we can verify that the mapping is valid and makes sense (e.g. "Loading..." can map to "Loading" but not "Unloading")
            // - then take the mapping and apply it to refDst to action the translation

            // == EXAMPLE #1 ==
            // src = "Loading"
            // refSrc = "Loading..."
            // refDst = "Chargement..." (french)
            // (mapping: remove "..." from end)
            // return = "Chargement"

            // refSrc maps to src by removing extraneous characters "...", this is valid
            // applying this mapping to refDst results in "Chargement", which is the return value

            // This is quite a bit more complex when the translations involve variables or protected terms.

            // == EXAMPLE #2 ==
            // src = "{lineCount} LINES x {betPerLine} PER LINE = {totalBet} TOTAL BET"
            // refSrc = "{0} LINES x {1} PER LINE = {2} TOTAL BET"
            // refDst = "{0} LIGNES x {1} PAR LIGNE = {2} TOTAL PAR"
            // (mapping: rename variable 0 to lineCount, 1 to betPerLine, 2 to totalBet)
            // return = "{lineCount} LIGNES x {betPerLine} PAR LIGNE = {totalBet} TOTAL PAR"

            /** Map of variable names from refSrc to src */
            const variableNameMap: Util.Map<TranslationSegment.Variable> = {};

            enum CasingMode { None, ToUpper, ToLower }

            /** Casing transformation applied from refSrc to src (which we'll later apply to refDst to our final result) */
            let casingMode: CasingMode | null = null;
            
            // Check that the segments of src match the segments of refSrc and build a variable name map
            // We are tolerant of extraneous text here
            if (src.segments.length !== refSrc.segments.length) { return null; }
            const srcSegCnt = src.segments.length;
            for (let i = 0; i < srcSegCnt; ++i)
            {
                const srcSeg = src.segments[i];
                const refSrcSeg = refSrc.segments[i];

                if (srcSeg.kind !== refSrcSeg.kind) { return null; }

                let isMatch = false;
                switch (srcSeg.kind)
                {
                    case TranslationSegment.Kind.Text:

                        // What casing mode are we in?
                        switch (casingMode)
                        {
                            case CasingMode.None:
                                // Only allow perfect match
                                isMatch = srcSeg.raw === refSrcSeg.raw;
                                break;
                            case CasingMode.ToLower:
                                // Only allow match if refSrc toLower's to src
                                isMatch = srcSeg.raw === refSrcSeg.raw.toLowerCase();
                                break;
                            case CasingMode.ToUpper:
                                // Only allow match if refSrc toUpper's to src
                                isMatch = srcSeg.raw === refSrcSeg.raw.toUpperCase();
                                break;
                            case null:
                                // We haven't decided on a casing mode yet, let's see...
                                if (srcSeg.raw === refSrcSeg.raw.toLowerCase())
                                {
                                    // Select lower casing mode
                                    casingMode = CasingMode.ToLower;
                                    isMatch = true;
                                }
                                else if (srcSeg.raw === refSrcSeg.raw.toUpperCase())
                                {
                                    // Select upper casing mode
                                    casingMode = CasingMode.ToUpper;
                                    isMatch = true;
                                }
                                else if (srcSeg.raw === refSrcSeg.raw)
                                {
                                    // Select no casing mode
                                    casingMode = CasingMode.None;
                                    isMatch = true;
                                }
                                else
                                {
                                    isMatch = false;
                                }

                                // TODO: Handle when toLowerCase == toUpperCase == raw (for example, numbers)
                                // In this case, we probably want to leave casing as null for now

                                break;
                        }
                        break;
                    case TranslationSegment.Kind.Variable:
                        const refSrcVar = refSrcSeg as TranslationSegment.Variable;
                        isMatch = true;
                        variableNameMap[refSrcVar.name] = srcSeg;
                        break;
                    case TranslationSegment.Kind.ProtectedTerm:
                        const refSrcTerm = refSrcSeg as TranslationSegment.ProtectedTerm;
                        isMatch = srcSeg.term === refSrcTerm.term;
                        break;
                }
                if (!isMatch) { return null; }
            }

            // So at this point, we've confirmed a match with src against refSrc
            // Now we need to remap it to refDst

            const newSegments: TranslationSegment[] = [];
            for (const refDstSeg of refDst.segments)
            {
                switch (refDstSeg.kind)
                {
                    case TranslationSegment.Kind.Text:
                        switch (casingMode)
                        {
                            case CasingMode.ToUpper:
                                newSegments.push({ kind: TranslationSegment.Kind.Text, raw: refDstSeg.raw.toUpperCase() });
                                break;
                            case CasingMode.ToLower:
                                newSegments.push({ kind: TranslationSegment.Kind.Text, raw: refDstSeg.raw.toLowerCase() });
                                break;
                            case CasingMode.None:
                                newSegments.push(refDstSeg);
                                break;
                        }
                        
                        break;
                    case TranslationSegment.Kind.Variable:
                        const seg = variableNameMap[refDstSeg.name];
                        if (seg)
                        {
                            newSegments.push(seg);
                        }
                        else
                        {
                            Log.warn(`refDst contains a variable name not present in refSrc`);
                            Log.debug(`refSrc: ${TranslationString.toString(refSrc, true)}`);
                            Log.debug(`refDst: ${TranslationString.toString(refDst, true)}`);
                        }
                        break;
                    case TranslationSegment.Kind.ProtectedTerm:
                        // We should be ok to push refDstSeg here as it shouldn't be translated
                        // If this presents an issue in the future, we could keep a map of segments like we do for variables and push the seg from src instead
                        newSegments.push(refDstSeg);
                        break;
                }
            }

            // Assemble final string, using extraneous parts from src
            return { leftExtraneous: src.leftExtraneous, segments: newSegments, rightExtraneous: src.rightExtraneous };
        }

        /**
         * Conducts a unit-like test translation.
         * Throws if the test failed.
         * @param src 
         * @param refSrc 
         * @param refDst 
         * @param expected 
         */
        export function testTranslate(src: string, refSrc: string, refDst: string, expected: string | null): string | null
        {
            const srcT = TranslationString.fromString(src);
            const refSrcT = TranslationString.fromString(refSrc);
            const refDstT = TranslationString.fromString(refDst);
            const resultT = TranslationString.matchAndTranslate(srcT, refSrcT, refDstT);
            if (expected == null && resultT == null)
            {
                // Failed to translate and this is expected, test passed
                return null;
            }
            if (expected == null && resultT != null)
            {
                throw new Error(`Translation succeeded where it should have failed (${src} via [${refSrc}, ${refDst}])`);
            }
            if (expected != null && resultT == null)
            {
                throw new Error(`Translation failed where it should have succeeded (${src} via [${refSrc}, ${refDst}])`);
            }
            const result = TranslationString.toString(resultT);
            if (expected !== result)
            {
                throw new Error(`Translation returned unexpected result '${result}' (${src} via [${refSrc}, ${refDst}])`);
            }
            // Test passed
            return result;
        }
    }
}
