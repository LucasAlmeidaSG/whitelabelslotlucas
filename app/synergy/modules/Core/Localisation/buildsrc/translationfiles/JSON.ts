/// <reference path="Base.ts" />

namespace RS.Localisation.TranslationFiles
{
    export class JSON extends Base
    {
        private characterMapRegex: RegExp = new RegExp(Object.keys(specialCharacterMap).join('|'), "g");

        public async loadRaw(raw: string | Buffer)
        {
            const rawStr = raw.toString().trim();
            const data: object = RS.JSON.parse(rawStr);
            this._strings = {};
            this.parseObject(data);
        }

        public async save()
        {
            RS.Log.debug(`Saving file ${this._filename}`);
            const raw = RS.JSON.stringify(this._strings, null, 2);
            await FileSystem.writeFile(this._filename, raw);
        }

        protected parseObject(obj: object)
        {
            for (const key in obj)
            {
                let value = obj[key];
                if (Is.string(value))
                {
                    value = value.replace(this.characterMapRegex, (char) => specialCharacterMap[char] );
                    this._strings[this.cleanKey(key)] = value;
                }
                // TODO: Handle objects
            }
        }
    }

    const specialCharacterMap =
    {
        "É": "É", // \u0045\u0301 -> \u00c9
        "Â": "Â", // \u0041\u0302 -> \u00c2
        "é": "é"  // \u0065\u0301 -> \u00e9
    }
}
