/// <reference path="Base.ts" />

namespace RS.Localisation.TranslationFiles
{
    const { parseStringSync } = require("xml2js-parser");

    export class WMSXML extends Base
    {
        public async loadRaw(raw: string | Buffer)
        {
            const rawStr = raw.toString();
            this._strings = {};
            await this.parse(rawStr);
        }

        public async save()
        {
            throw new Error("Method not implemented.");
        }

        protected async parse(raw: string)
        {
            const root = parseStringSync(raw);

            const localeConfiguration = root.LOCALECONFIGURATION;
            if (!localeConfiguration) { return; }

            const resources = localeConfiguration.RESOURCE;
            if (!resources) { return; }

            for (const resource of resources)
            {
                const key: string | null = resource.$.key;
                const value: string | null = resource.$.value;
                if (Is.string(key) && Is.string(value))
                {
                    this._strings[this.cleanKey(key)] = value;
                }
            }
        }
    }
}
