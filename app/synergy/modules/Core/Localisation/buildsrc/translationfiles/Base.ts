namespace RS.Localisation.TranslationFiles
{
    /**
     * Encapsulates a single file containing a map of arbitrary IDs to translated strings for a single locale.
     */
    export abstract class Base
    {
        protected _filename: string;
        protected _strings: Util.Map<string>;

        /** Gets the filename of this translation file. */
        public get filename() { return this._filename; }

        /** Gets an array of all keys within this file. */
        public get keys() { return Object.keys(this._strings); }

        public constructor(filename: string)
        {
            this._filename = filename;
            this._strings = {};
        }

        public hasKey(key: string): boolean
        {
            return key in this._strings;
        }

        public getString(key: string): string | null
        {
            return key in this._strings ? this._strings[key] : null;
        }

        public setString(key: string, value: string): void
        {
            this._strings[key] = value;
        }

        public removeKey(key: string): void
        {
            delete this._strings[key];
        }

        public async load()
        {
            const raw = await FileSystem.readFile(this._filename);
            try
            {
                await this.loadRaw(raw);
            }
            catch (e)
            {
                throw new Error(`${e} (in ${this.filename})`);
            }
        }

        public abstract save(): PromiseLike<void>;

        public abstract loadRaw(raw: string | Buffer): PromiseLike<void>;

        /**
         * Cleans up a translation string key.
         * @param key
         */
        protected cleanKey(key: string): string
        {
            // Identify java-type namespacing
            if (key.length >= 3 && key.substr(0, 3) === "com")
            {
                const spl = key.split(".");
                let i: number, l: number;
                for (i = 0, l = spl.length; i < l; ++i)
                {
                    const seg = spl[i];
                    if (seg != seg.toLowerCase())
                    {
                        break;
                    }
                }

                if (i < l)
                {
                    return spl.slice(i).join(".");
                }
            }

            // Unknown
            return key;
        }
    }
}