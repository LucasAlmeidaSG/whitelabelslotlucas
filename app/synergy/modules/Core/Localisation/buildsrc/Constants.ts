namespace RS.Localisation.Constants
{
    export const inTranslationsFolder = "translations";
    export const outTranslationsFolder = "translations";
    export const checkMissingKeys = true;
    export const checkMissingLocales = true;
    export const generatedNamespace = "Translations";
    export const referenceLocale = "en_GB";
    export const dumpFile = "missing_translations.json";
    export const translationsRepoBranch = "master";
}