namespace RS.Build
{
    const translationAgentEnvArg = new RS.EnvArg("TRANSLATION_AGENT", "gittmx");
    const overwrite = new RS.BooleanEnvArg("OVERWRITE", false);

    export const getAgents = Build.task({ name: "list-translation-agents", after: [ Tasks.clean ], description: "Translates the root module" }, async () =>
    {
        Log.info(`Agents: gittmx, local, smb`);
    })

    /**
     * Executes translation agent for the root module only.
     */
    export const translatePush = Build.task({ name: "translate-push", after: [ Tasks.clean ], description: "Pushes your translations to the repo" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();
        const rootModule = Module.getModule(config.rootModule);
        const translationsRepoPath = RS.Localisation.TranslationAgents.GitTMX.translationsRepoPath;

        // Pull latest from repo
        await Git.update("../translations", "temp", true);
        // Get our translations
        const repoNamespace = Build.Utils.getRepoNamespace(rootModule);
        if (repoNamespace)
        {
            await Localisation.updateJSONFolder(Path.combine(rootModule.path, RS.Localisation.Constants.inTranslationsFolder), `${translationsRepoPath}/${repoNamespace}`);
            // Build the repo TMX files
            await Localisation.generateTMXFiles(`${translationsRepoPath}/tmx`, translationsRepoPath);
            // Push to repo
            Log.info(`Committing changes to repo`);
            await Git.commitAll(translationsRepoPath, `[Automated] updating translations for ${repoNamespace}`);
            Log.info(`Pushing changes to repo`);
            await Git.push(translationsRepoPath);
        }
        else
        {
            Log.error(`Failed to find 'reponamespace' in 'translations' in file "${rootModule.path}/config.ts"`);
        }

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Executes translation agent for the root module only.
     */
    export const translate = Build.task({ name: "translate", after: [ Tasks.clean ], description: "Translates the root module" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Gather all dependent modules
        const allModules = new List([ rootModule ]);

        // Do translations
        await Localisation.doTranslation(allModules, translationAgentEnvArg.value, overwrite.value);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Executes translation agent for a specific set of modules only.
     */
    export const translateOnly = Build.task({ name: "translate-only", after: [ Tasks.clean ], description: "Translates the specified modules" }, async () =>
    {
        Profile.reset();

        Log.info(`Please enter the names of the modules to translated, separated by comma:`);

        const inputModulesRaw = await Input.readLine();
        const inputModules = inputModulesRaw.split(",").map((name) => name.trim());
        if (inputModules.length < 0 || inputModules[0].length === 0) { return; }

        const allModules = new List<Module.Base>();
        for (const moduleName of inputModules)
        {
            const module = Module.getModule(moduleName);
            if (module == null)
            {
                Log.warn(`Module '${moduleName}' not found`);
            }
            else
            {
                allModules.add(module);
            }
        }
        if (allModules.length === 0) { return; }

        // Do translations
        await Localisation.doTranslation(allModules, translationAgentEnvArg.value, overwrite.value);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Executes translaton agent for all loaded modules.
     */
    export const translateAll = Build.task({ name: "translate-all", after: [ Tasks.clean ], description: "Translates all modules" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Get all modules
        const allModules = RS.Module.getDependencySet(RS.Module.getAll().map((m) => m.name));

        // Do translations
        await Localisation.doTranslation(allModules, translationAgentEnvArg.value, overwrite.value);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Executes translation agent for the root module only.
     */
    export const rebuildArchive = Build.task({ name: "rebuild-archive", after: [ Tasks.clean ], description: "Rebuilds the repo tmx files" }, async () =>
    {
        Profile.reset();

        const repoPath = Localisation.TranslationAgents.GitTMX.translationsRepoPath;
        await Localisation.generateTMXFiles(`${repoPath}/tmx`, repoPath);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Generate xx_XX locale with the longest strings for each key
     */
    export const generateLongLocale = Build.task({ name: "generate-long-locale", after: [ Tasks.clean ], description: "Generates the xx_XX locale containing the longest value for each key" }, async () =>
    {
        const translationData: Util.Map<Localisation.BuildStages.ModuleLocales> = await Utils.getTranslationData();
        let longestTranslations: Util.Map<string> = {};

        for (const moduleName in translationData)
        {
            const data = translationData[moduleName];
            let localeCount = 0;
            for (const localeCode in data)
            {
                for (const key in data[localeCode])
                {
                    //remove locale symbols
                    data[localeCode][key] = data[localeCode][key].replace(/\|\|\|/gi, "").replace(/\|\|/gi, "");

                    if (localeCount == 0 || (data[localeCode][key] != null && longestTranslations[key] != null && data[localeCode][key].length > longestTranslations[key].length))
                    {
                        longestTranslations[key] = data[localeCode][key];
                    }
                }
                localeCount++;
            }
        }

        // Write file
        const assemblePath = (await Config.get()).assemblePath;
        const buildDir = Path.combine(assemblePath, Localisation.Constants.outTranslationsFolder);
        await FileSystem.createPath(buildDir);
        await FileSystem.writeFile(Path.combine(buildDir, "xx_XX.json"), JSON.stringify(longestTranslations));
    });

    export const getAllCharacters = Build.task({ name: "get-all-characters", after: [Tasks.clean], description: "Gets all unicode characters used within translations plus defaults" }, async () =>
    {
        const translationData: Util.Map<Localisation.BuildStages.ModuleLocales> = await Utils.getTranslationData();
        const defaultCharacters = "$лвRSFr¥Kčk€£HntsM/złLEIрубTBNAUDGCYZPJXOW1234567890., ";
        const chars: string[] = defaultCharacters.split("");
        for (const moduleName in translationData)
        {
            const data = translationData[moduleName];
            for (const localeCode in data)
            {
                for (const key in data[localeCode])
                {
                    const str = data[localeCode][key];
                    for (const char of str)
                    {
                        if (chars.indexOf(char) === -1)
                        {
                            chars.push(char);
                        }
                    }
                }
            }
        }

        console.log(chars.join(""));
    });
}
