namespace RS.Localisation
{
    const { parseStringSync } = require("xml2js-parser")// as XML2JSParser;
    // import XMLNode = XML2JSParser.XMLNode;
    interface BaseXMLNode
    {
        [key: string]: undefined | XMLNode | string | (XMLNode | string)[];
    }

    type XMLNode = { $?: Util.Map<string>; _?: string; } & BaseXMLNode;
    /**
     *
     * @param raw
     */
    export function parseTMX(raw: string | Buffer): TranslationAgents.TranslationUnit[]
    {
        if (raw instanceof Buffer) { raw = raw.toString(); }
        const root = parseStringSync(raw);

        const tmxRoot = root.tmx;
        if (!tmxRoot) { throw new Error("TMX document is missing 'tmx' tag"); }
        if (tmxRoot.$.version !== "1.4")
        {
            Log.warn(`Unexpected TMX version ${tmxRoot.$.version} (expected 1.4)`);
        }

        const body = tmxRoot.body[0] as XMLNode;
        if (!body) { throw new Error("TMX document is missing 'body' tag"); }

        const units: TranslationAgents.TranslationUnit[] = [];
        const tus = body.tu as (XMLNode | XMLNode[]);
        if (!tus) { return units; }

        if (Is.array(tus))
        {
            for (const tu of tus)
            {
                const unit = parseTU(tu);
                if (unit) { units.push(unit); }
            }
        }
        else
        {
            const unit = parseTU(tus);
            if (unit) { units.push(unit); }
        }

        return units;
    }

    function parseTU(tu: XMLNode): TranslationAgents.TranslationUnit | null
    {
        const tuvs = tu.tuv as XMLNode[];
        if (!tuvs) { return null; }

        const langMap: Util.Map<string> = {};
        for (const tuv of tuvs)
        {
            const localeCode = tuv.$["xml:lang"].replace("-", "_");
            const segs = tuv.seg as (XMLNode | string)[];
            langMap[localeCode] = segs.map(parseSeg).join(" ").trim();
        }

        const keys = Object.keys(langMap);
        if (keys.length === 2)
        {
            return {
                fromLocale: keys[0],
                fromText: langMap[keys[0]],
                toLocale: keys[1],
                toText: langMap[keys[1]]
            };
        }
        else
        {
            return null;
        }
    }

    function parseSeg(seg: XMLNode | string): string
    {
        if (Is.string(seg))
        {
            return seg;
        }
        else
        {
            if (Is.string(seg._))
            {
                return seg._;
            }
            else
            {
                return "";
            }
        }
    }
}
