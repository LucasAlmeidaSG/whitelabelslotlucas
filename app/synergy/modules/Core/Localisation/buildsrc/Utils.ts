namespace RS.Build.Utils
{
    export async function getTranslationData(): Promise<Util.Map<Localisation.BuildStages.ModuleLocales>>
    {
        const translationData: Util.Map<Localisation.BuildStages.ModuleLocales> = {};

        const dependencies = RS.Module.getDependencySet([ RS.Config.activeConfig.rootModule, ...RS.Config.activeConfig.extraModules ]);
        for (const module of dependencies.data)
        {
            const localeData: Util.Map<Util.Map<string>> = {};
            translationData[module.name] = localeData;

            const langfiles = await Localisation.getLangFiles(module);
            for (const file of langfiles)
            {
                const localeCode = Localisation.extractLocaleCode(file);
                if (localeCode == null)
                {
                    Log.warn(`Translation file '${file}' missing valid locale code (expecting 'en_GB'-like code before extension)`);
                }
                else
                {
                    const strings = localeData[localeCode] || (localeData[localeCode] = {});
                    const ext = Path.extension(file);
                    try
                    {
                        let tFile: Localisation.TranslationFiles.Base | null = null;
                        if (ext === ".json")
                        {
                            tFile = new Localisation.TranslationFiles.JSON(file);
                        }
                        else if (ext === ".xml")
                        {
                            tFile = new Localisation.TranslationFiles.WMSXML(file);
                        }
                        if (tFile != null)
                        {
                            await tFile.load();
                            parseTranslationFile(tFile, strings, localeCode);
                        }
                    }
                    catch (err)
                    {
                        Log.error(err);
                    }
                }
            }
        }
        return translationData;
    }

    export function getTranslationNamespace(module: Localisation.IModule)
    {
        return module.info.localisation && module.info.localisation.namespace || `${module.name}.${Localisation.Constants.generatedNamespace}`;
    }

    export function getRepoNamespace(module: Localisation.IModule)
    {
        return module.info.localisation && module.info.localisation.reponamespace || null;
    }

    export function parseTranslationFile(file: Localisation.TranslationFiles.Base, strings: Util.Map<string>, localeCode: string): void
    {
        const keys = file.keys;
        for (const key of keys)
        {
            if (strings[key] != null)
            {
                Log.warn(`Duplicate key '${key}' in locale '${localeCode}' (${file.filename})`);
            }
            else
            {
                strings[key] = file.getString(key);
            }
        }
    }
}
