/// <reference path="Utils.ts" />
namespace RS.Localisation
{
    /**
     * Settings for a currency.
     */
    export interface CurrencySettings
    {
        /** Name of the currency. */
        name: string;

        /** The currency symbol. */
        symbol: string;

        /** Exchange rate. */
        exchangeRate?: number;

        /** Number of digits to group together. */
        grouping: number;

        /** Separator symbol used between groups of digits. */
        groupingSeparator: string;

        /** Number of digits used to represent fractional currency. */
        fractionalDigits: number;

        /** Seperator symbol used betweeen whole and fractional currency. */
        fractionalSeparator: string;

        /** Space between currency code and numerical amount. */
        currencyCodeSpacing: number;

        /** Should the currency code should be before amount? */
        currencyCodeBeforeAmount: boolean;

        /** Space between currency symbol and numerical amount. */
        currencySymbolSpacing: number;

        /** Should the currency symbol should be before the amount? */
        currencySymbolBeforeAmount: boolean;

        /** The multiplier for currency values. */
        currencyMultiplier: number;

        /** Should a currency symbol be added? */
        displaySymbol: boolean;

        /** Should a currency code be added? */
        displayCode: boolean;
    }

    const standardCurrencySettings =
    {
        exchangeRate: 1,
        grouping: 3,
        groupingSeparator: ",",
        fractionalDigits: 2,
        fractionalSeparator: ".",
        currencyCodeSpacing: 1,
        currencyCodeBeforeAmount: true,
        currencySymbolSpacing: 0,
        currencySymbolBeforeAmount: true,
        currencyMultiplier: 1,
        displaySymbol: true,
        displayCode: false
    };

    const spaceCharacter = RS.Localisation.SpecialChars.NonBreakingSpace;

    export const currencyMap: { [code: string]: CurrencySettings } =
    {
        "GBP": { ...standardCurrencySettings, name: "GBP", symbol: "£", groupingSeparator: ",", fractionalSeparator: "." },
        "HKD": { ...standardCurrencySettings, name: "HKD", symbol: "$", groupingSeparator: ",", fractionalSeparator: "." },
        "EUR": { ...standardCurrencySettings, name: "EUR", symbol: "€", groupingSeparator: ".", fractionalSeparator: "," },
        "USD": { ...standardCurrencySettings, name: "USD", symbol: "$", groupingSeparator: ",", fractionalSeparator: "." },
        "JPY":
        {
            ...standardCurrencySettings,
            name: "JPY",
            symbol: "¥",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            fractionalDigits: 0,
            currencySymbolBeforeAmount: false
        },
        "SGD": { ...standardCurrencySettings, name: "SGD", symbol: "$", groupingSeparator: ",", fractionalSeparator: "." },
        "AUD": { ...standardCurrencySettings, name: "AUD", symbol: "$", groupingSeparator: " ", fractionalSeparator: "." },
        "CAD": { ...standardCurrencySettings, name: "CAD", symbol: "$", groupingSeparator: ",", fractionalSeparator: "." },
        "CHF":
        {
            ...standardCurrencySettings,
            name: "CHF",
            symbol: "SFr.",
            groupingSeparator: "'",
            fractionalSeparator: ".",
            currencySymbolSpacing: 0
        },
        "DKK":
        {
            ...standardCurrencySettings,
            name: "DKK",
            symbol: "kr",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencySymbolSpacing: 1
        },
        "SEK":
        {
            ...standardCurrencySettings,
            name: "SEK",
            symbol: "kr",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencySymbolSpacing: 1
        },
        "ARS":
        {
            ...standardCurrencySettings,
            name: "ARS",
            symbol: "$",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencyMultiplier: 5
        },

        "BGN":
        {
            ...standardCurrencySettings,
            name: "BGN",
            symbol: "лв.",
            groupingSeparator: spaceCharacter,
            fractionalSeparator: ",",
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 2,
        },
        "BRL":
        {
            ...standardCurrencySettings,
            name: "BRL",
            symbol: "R$",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencyMultiplier: 2,
        },

        "CNY":
        {
            ...standardCurrencySettings,
            name: "CNY",
            symbol: "¥",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 10,
        },
        "CZK":
        {
            ...standardCurrencySettings,
            name: "CZK",
            symbol: "Kč",
            groupingSeparator: spaceCharacter, 
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 20,
        },
        "EEK":
        {
            ...standardCurrencySettings,
            name: "EEK",
            symbol: "€",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencySymbolSpacing: 1,
            currencyMultiplier: 15,
        },
        "HRK":
        {
            ...standardCurrencySettings,
            name: "HRK",
            symbol: "Kn",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencyMultiplier: 10
        },
        "HUF": 
        {
            ...standardCurrencySettings,
            name: "HUF",
            symbol: "Ft",
            groupingSeparator: spaceCharacter,
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 300
        },
        "INR":
        {
            ...standardCurrencySettings,
            name: "INR",
            symbol: "Rs.",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 50
        },
        "ISK":
        {
            ...standardCurrencySettings,
            name: "ISK",
            symbol: "kr.",
            groupingSeparator: ".",
            fractionalDigits: 0,
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 200
        },
        "LIL":
        {
            ...standardCurrencySettings,
            name: "LTL",
            symbol: "€",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencySymbolSpacing: 1,
            currencyMultiplier: 5
        },
        "LVL":
        {
            ...standardCurrencySettings,
            name: "LVL",
            symbol: "€",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencySymbolSpacing: 1
        },
        "MXN": 
        { 
            ...standardCurrencySettings,
            name: "MXN",
            symbol: "$",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 20
        },
        "MYR":
        {
            ...standardCurrencySettings,
            name: "MYR",
            symbol: "RM",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 5
        },
        "NOK": 
        {
            ...standardCurrencySettings,
            name: "NOK",
            symbol: "kr",
            groupingSeparator: spaceCharacter,
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencyMultiplier: 10
        },
        "NZD": 
        {
            ...standardCurrencySettings,
            name: "NZD",
            symbol: "$", 
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 1
        },
        "PEN":
        {
            ...standardCurrencySettings,
            name: "PEN",
            symbol: "S/.",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencyMultiplier: 4
        },
        "PLN":
        {
            ...standardCurrencySettings,
            name: "PLN",
            symbol: "zł",
            groupingSeparator: spaceCharacter,
            fractionalSeparator: ",",
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 5
        },
        "RON":
        {
            ...standardCurrencySettings,
            name: "RON",
            symbol: "LEI",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 5
        },
        "RUB":
        {
            ...standardCurrencySettings,
            name: "RUB",
            symbol: "руб.",
            groupingSeparator: spaceCharacter,
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 40
        },
        "THB":
        {
            ...standardCurrencySettings,
            name: "THB",
            symbol: "THB",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 40
        },
        "TRY":
        {
            ...standardCurrencySettings,
            name: "TRY",
            symbol: "TL",
            groupingSeparator: ".",
            fractionalSeparator: ",",
            currencySymbolSpacing: 1,
            currencySymbolBeforeAmount: false,
            currencyMultiplier: 2
        },
        "TWD":
        {
            ...standardCurrencySettings,
            name: "TWD",
            symbol: "NT$",
            groupingSeparator: ",",
            fractionalSeparator: ".",
            currencyMultiplier: 40
        },
        "ZAR":
        {
            ...standardCurrencySettings,
            name: "ZAR",
            symbol: "R",
            groupingSeparator: ",",
            fractionalDigits: 2,
            fractionalSeparator: ".",
            currencySymbolSpacing: 1,
            currencyMultiplier: 10
        }
    };

    export const defaultCurrencySettings = currencyMap["GBP"];
}