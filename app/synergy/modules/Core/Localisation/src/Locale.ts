namespace RS.Localisation
{
    /**
     * Encapsulates a single locale.
     */
    export class Locale
    {
        protected _langCode: string;
        protected _countryCode: string;
        protected _strings: { [key: string]: string; } = {};

        /**
         * Gets the ISO 639 language code for this locale.
         * e.g. "en"
         */
        public get languageCode() { return this._langCode; }

        /**
         * Gets the ISO 3166-1 country code for this locale.
         * e.g. "US"
         */
        public get countryCode() { return this._countryCode; }

        constructor(langCode: string, countryCode: string)
        {
            this._langCode = langCode;
            this._countryCode = countryCode;
        }

        /**
         * Adds a localised string to this locale.
         * @param key
         * @param value 
         */
        public addString(key: string, value: string): void
        {
            this._strings[key] = value;
        }

        /**
         * Gets a localised string from this locale.
         * @param key 
         */
        public getString(key: string, def: string = key): string
        {
            return this._strings[key] || def;
        }
    }
}
