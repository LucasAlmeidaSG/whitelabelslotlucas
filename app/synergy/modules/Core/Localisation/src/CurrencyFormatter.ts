/// <reference path="CurrencySettings.ts" />

namespace RS.Localisation
{
    /**
     * Responsible for formatting numbers into a particular currency.
     */
    export class CurrencyFormatter
    {
        protected _settings: CurrencySettings;

        /** Gets or sets the currency settings for this formatter. */
        public get settings(): CurrencySettings { return this._settings; }
        public set settings(value) { this._settings = value; }

        public constructor(settings: CurrencySettings = defaultCurrencySettings)
        {
            this._settings = settings;
        }

        /**
         * Formats a number value.
         * @param value The value to format.
         * @param displaySymbol Whether to display the symbol/code or not.
         */
        public format(value: number, displaySymbol: boolean = true, removeUnusedFractionalDigits?: boolean): string
        {
            return this.performFormat(value, displaySymbol, this._settings.fractionalDigits, removeUnusedFractionalDigits);
        }

        /**
         * Formats a number to a specified amount of fractional digits, irregardless of whether or not the current currency has fractional digits in it.
         * If an amount of fractional digits is not provided, it will format it to 2dp.
         * @param value The value to format.
         * @param displaySymbol Whether to display the symbol/code or not.
         */
        public formatDecimal(value: number, displaySymbol: boolean = true, fractionalDigits: number = 2): string
        {
            return this.performFormat(value, displaySymbol, fractionalDigits);
        }

        protected performFormat(value: number, displaySymbol: boolean = true, fractionalDigits: number, removeUnusedFractionalDigits?: boolean): string
        {
            // Convert to string without decimals
            value = Math.round(value) / 100;
            if (removeUnusedFractionalDigits && (value-Math.floor(value)) === 0)
            {
                fractionalDigits = 0;
            }
            const digits = value.toFixed(fractionalDigits);
            const parts: string[] = digits.split("");
            const positionOfDecimal = parts.indexOf(".");

            // Insert fractional separator
            if (positionOfDecimal !== -1) { parts[positionOfDecimal] = this._settings.fractionalSeparator; }

            let groupFrom: number;
            if (positionOfDecimal !== -1)
            {
                groupFrom = positionOfDecimal - 1;
            }
            else
            {
                groupFrom = parts.length - 1;
            }

            // Insert grouping seperators
            let curGroup = 0;
            for (let i = groupFrom; i >= 0; --i)
            {
                ++curGroup;
                if (curGroup >= this._settings.grouping && i > 0)
                {
                    parts.splice(i, 0, this._settings.groupingSeparator);
                    curGroup = 0;
                }
            }

            // Insert currency symbol
            if (this._settings.displaySymbol && displaySymbol)
            {
                this.attach(this._settings.symbol, parts, this._settings.currencySymbolSpacing, this._settings.currencySymbolBeforeAmount);
            }

            // Insert currency code
            if (this._settings.displayCode && displaySymbol)
            {
                this.attach(this._settings.name, parts, this._settings.currencyCodeSpacing, this._settings.currencyCodeBeforeAmount);
            }

            // Join everything together
            return parts.join("");
        }

        protected attach(part: string, parts: string[], spacingCount: number, before: boolean): void
        {
            // Use non-breaking space.
            const spaceCharacter = SpecialChars.NonBreakingSpace;
            if (before)
            {
                parts.splice(0, 0, part);
                for (let i = 0; i < spacingCount; ++i)
                {
                    parts.splice(1, 0, spaceCharacter);
                }
            }
            else
            {
                for (let i = 0; i < spacingCount; ++i)
                {
                    parts.push(spaceCharacter);
                }
                parts.push(part);
            }
        }
    }
}