namespace RS.Localisation
{
    export type Substitutions<T extends object = object> = Map<LocalisableString> | T;
    export type SimpleGetter = (locale: Locale) => string;
    export type PartsGetter = (locale: Locale) => TranslationPart[];
    export type ComplexGetter<T extends object = object> = (locale: Locale, substitutions: Substitutions<T>) => string;
    export type BoundComplexGetter<T extends object = object> = { get: ComplexGetter<T>; getParts: PartsGetter; substitutions: Substitutions<T>; clone: () => LocalisableString; };

    export type SimpleTranslationReference = { get: SimpleGetter; getParts: PartsGetter; clone: () => LocalisableString; };
    export type ComplexTranslationReference<T extends object = object> = { get: ComplexGetter<T>; getParts: PartsGetter; clone: () => ComplexTranslationReference<T>; };

    export type LocalisableString = string | SimpleTranslationReference | BoundComplexGetter | Error;

    /**
     * Generates a simple getter function for the specified key.
     * Used by generated code.
     * @param key
     */
    export function generateSimpleGetter(key: string): SimpleGetter
    {
        return function (locale: Locale)
        {
            if (locale == null) { return "NO LOCALE"; }
            return locale.getString(key, key);
        };
    }

    /**
     * Generates a complex getter function for the specified key.
     * Used by generated code.
     * @param key
     */
    export function generateComplexGetter<T extends object = object>(key: string, keyMap: Map<string>): ComplexGetter<T>
    {
        return function (locale: Locale, substitutions: Substitutions<T>)
        {
            if (locale == null) { return "NO LOCALE"; }
            let str = locale.getString(key);
            if (str == null) { return key; }
            for (const subKey in substitutions)
            {
                const value = substitutions[subKey];
                const strValue = Is.localisableString(value)
                    ? Localisation.resolveLocalisableString(locale, value)
                    : `${value}`;
                str = str.replace(keyMap[subKey], strValue);
            }
            return str;
        };
    }

    export function clone<T extends object = object>(this: ComplexTranslationReference<T>): ComplexTranslationReference<T>;
    export function clone(this: LocalisableString): LocalisableString;
    export function clone<T extends object = object>(this: LocalisableString | ComplexTranslationReference<T>): LocalisableString | ComplexTranslationReference<T>
    {
        return RS.Util.clone(this, false);
    }

    /**
     * Binds the specified static substitutions to the specified complex translation key.
     * @param base
     * @param substitutions
     */
    export function bind<T extends object = object>(base: ComplexTranslationReference<T>, substitutions: Substitutions<T>): BoundComplexGetter
    {
        const bound: BoundComplexGetter = { get: base.get, getParts: base.getParts, substitutions: {}, clone: clone };
        for (const key in substitutions)
        {
            bound.substitutions[key] = substitutions[key];
        }
        return bound;
    }

    /**
     * Resolves the specified LocalisableString into a string.
     * @param locale
     * @param str
     */
    export function resolveLocalisableString(locale: Locale, str: LocalisableString): string
    {
        if (Is.string(str))
        {
            return str;
        }
        else if (Is.boundComplexGetter(str))
        {
            return str.get(locale, str.substitutions);
        }
        else if (str instanceof Error)
        {
            return str.stack || str.message;
        }
        else if (str && Is.func(str.get))
        {
            return str.get(locale);
        }
        else
        {
            return `${str}`;
        }
    }

    export interface TranslationPart
    {
        kind: TranslationPart.Kind;
        value: string;
    }

    export namespace TranslationPart
    {
        export enum Kind
        {
            Text,
            Variable,
            Protected
        }
    }

    interface TokenRule
    {
        regex: RegExp;
        group: number;
        kind: TranslationPart.Kind;
    }
    const tokenRules: TokenRule[] =
    [
        { regex: /\{([a-zA-Z0-9_]+)\}/, group: 1, kind: TranslationPart.Kind.Variable },
        { regex: /(\|\|?\|?)([^\|]+)\1/, group: 2, kind: TranslationPart.Kind.Protected }
    ];

    /**
     * Breaks down a translated string into parts.
     * @param str
     */
    export function getParts(str: string): TranslationPart[]
    {
        const result: TranslationPart[] = [];
        let remainingStr = str;
        while (remainingStr.length > 0)
        {
            let lowestMatch = remainingStr.length;
            let matchRule: TokenRule | null = null;
            let matchContent: string | null = null;
            let matchLength: number | null = null;
            for (const rule of tokenRules)
            {
                const match = rule.regex.exec(remainingStr);
                if (match)
                {
                    if (match.index < lowestMatch)
                    {
                        lowestMatch = match.index;
                        matchRule = rule;
                        matchContent = match[rule.group];
                        matchLength = match[0].length;
                    }
                }
            }
            if (lowestMatch > 0)
            {
                result.push({
                    kind: TranslationPart.Kind.Text,
                    value: remainingStr.substr(0, lowestMatch)
                });
                remainingStr = remainingStr.substr(lowestMatch);
            }
            if (matchRule)
            {
                result.push({
                    kind: matchRule.kind,
                    value: matchContent
                });
                remainingStr = remainingStr.substr(matchLength);
            }
        }
        return result;
    }

    export namespace SpecialChars
    {
        export const NonBreakingSpace = "\u00A0";
    }

    const defaultReduceFunc: (a: string, b: string) => string = (a, b) => `${a}\n${b}`;

    /**
     * Combines an array of localisable strings into a single LocalisableString.
     */
    export function reduce(localisations: LocalisableString[], reduceFunc?: (a: string, b: string) => string): LocalisableString;
    /**
     * Combines an array of localisable strings and complex translation references into a single complex translation reference.
     */
    export function reduce<T extends object = object>(localisations: (LocalisableString | ComplexTranslationReference<T>)[], reduceFunc?: (a: string, b: string) => string): ComplexTranslationReference<T>;
    export function reduce<T extends object = object>(localisations: (LocalisableString | ComplexTranslationReference<T>)[], reduceFunc: (a: string, b: string) => string = defaultReduceFunc): LocalisableString | ComplexTranslationReference<T>
    {
        if (localisations.length === 0) { return ""; }

        function resolve(locale: Locale, localisation: LocalisableString | ComplexTranslationReference<T>, substitutions: Substitutions<T>)
        {
            if ((localisation as ComplexTranslationReference<T>).get)
            {
                const complexLocalisation = localisation as ComplexTranslationReference<T>;
                return complexLocalisation.get(locale, substitutions);
            }

            return resolveLocalisableString(locale, localisation as LocalisableString);
        }

        const composite: Localisation.ComplexTranslationReference<T> =
        {
            get: (locale: Localisation.Locale, substitutions?: Substitutions<T>) => localisations
                .map((localisation) => resolve(locale, localisation, substitutions))
                .reduce((a: string, b: string) => reduceFunc(a, b)),
            getParts: (locale: Locale) => getParts(composite.get(locale, {})),
            clone: clone
        };

        return composite;
    }
}
