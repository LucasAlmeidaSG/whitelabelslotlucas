namespace RS.Localisation
{
    /** A localisation containing an arbitrarily-formatted date. */
    export type DateString = ComplexTranslationReference<Date.Substitutions>;

    /** Allows localisations to include an arbitrarily-formatted date. */
    export namespace Date
    {
        export interface Substitutions
        {
            /** Unpadded day. */
            D: string;
            /** Padded day. */
            DD: string;

            /** Unpadded month. */
            M: string;
            /** Padded month. */
            MM: string;

            /** Padded year. */
            YYYY: string;

            /** Unpadded hours on the 24-hour clock. */
            H: string;
            /** Padded hours on the 24-hour clock. */
            HH: string;

            /** Unpadded hours on the 12-hour clock. */
            h: string;
            /** Padded hours on the 12-hour clock. */
            hh: string;
            /** 12-hour period (am/pm) */
            pm: string;

            /** Unpadded minutes. */
            m: string;
            /** Padded minutes. */
            mm: string;

        }

        /** Binds a compatible localisation to a given Date. */
        export function bind(string: DateString, date: Date)
        {
            // Date
            const D = `${date.getDate()}`;
            const DD = Util.padLeft(D, "0", 2);

            const M = `${date.getMonth() + 1}`;
            const MM = Util.padLeft(M, "0", 2);

            const YYYY = `${date.getFullYear()}`;

            // Standard time
            const hours = date.getHours();
            const H = `${hours}`;
            const HH = Util.padLeft(H, "0", 2);

            const m = `${date.getMinutes()}`;
            const mm = RS.Util.padLeft(m, "0", 2);

            // 12-hour time
            let pm: string;
            let h: string;
            let hh: string;
            if (hours > 12)
            {
                pm = "pm";
                h = `${date.getHours() - 12}`;
                hh = Util.padLeft(h, "0", 2);
            }
            else
            {
                pm = "am";
                h = H;
                hh = HH;
            }

            return RS.Localisation.bind(string, { D, DD, M, MM, YYYY, H, HH, m, mm, h, hh, pm });
        }
    }
}