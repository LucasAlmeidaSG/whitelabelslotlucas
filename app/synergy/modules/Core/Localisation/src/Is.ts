namespace RS.Is
{
    /**
     * Gets if the specified value is a Localisation.LocalisableString
     * @param value
     */
    export function localisableString(value: any): value is Localisation.LocalisableString
    {
        return Is.string(value) ||
            (Is.object(value) && Is.func(value.get) && Is.func(value.getParts)) || // SimpleTranslationReference
            (Is.boundComplexGetter(value)) ||
            (value instanceof Error)
    }

    /** @internal */
    export function boundComplexGetter(value: any): value is Localisation.BoundComplexGetter
    {
        return Is.object(value) && Is.func(value.get) && Is.object(value.substitutions);
    }
}
