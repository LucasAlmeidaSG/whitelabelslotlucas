namespace RS.UI
{
    /**
     * Responsible for interfacing the UI with the game logic.
     */
    export interface IUIController<TSettings extends IUIController.Settings = IUIController.Settings> extends Controllers.IController<TSettings>
    {
        /** Published when the paytable has been requested by the user. */
        readonly onPaytableRequested: IEvent;

        /** Published when the help page has been requested by the user. */
        readonly onHelpRequested: IEvent;

        /** Published when a spin has been requested by the user. */
        readonly onSpinRequested: IEvent;

        /** Published when a skip has been requested by the user. */
        readonly onSkipRequested: IEvent;

        /** Published when the settings control has been requested by the user. */
        readonly onSettingsRequested: IEvent;

        /** Gets a rectangle representing how much of the attached container is fully occluded by the UI on all sides. */
        readonly enclosedSpace: Flow.Spacing;

        /** Initialises this controller with the specified settings. */
        initialise(settings: TSettings): void;

        /** Creates the UI with the specified settings. */
        create(settings: IUIController.InitSettings): void;

        /** Updates the balance to be displayed on the UI. */
        setBalance(value: number): void;

        /** Updates the win amount to be displayed on the UI. */
        setWin(value: number, final: boolean): void;

        /** Updates the total bet to be displayed on the UI. */
        setTotalBet(value: number): void;

        /** Sets replay mode. */
        setReplayMode(value: boolean);

        /** Attaches the UI to the specified container. */
        attach(view: RS.Rendering.IContainer): void;

        /** Detaches the UI from the specified container, if it is attached to that. */
        detach(view: RS.Rendering.IContainer): void;
    }

    export const IUIController = Controllers.declare<IUIController, IUIController.Settings>(Strategy.Type.Instance);

    export namespace IUIController
    {

        /**
         * Settings for the UI controller.
         */
        export interface Settings
        {
            allowSpinArbiter: IArbiter<boolean>;
            allowSkipArbiter: IArbiter<boolean>;
            allowChangeBetArbiter: IArbiter<boolean>;
            spinOrSkipArbiter: IArbiter<"spin"|"skip">;
            enableInteractionArbiter: IArbiter<boolean>;
            messageTextArbiter: IArbiter<RS.Localisation.LocalisableString>;
            showButtonAreaArbiter: IArbiter<boolean>;
            orientationObservable?: IReadonlyObservable<DeviceOrientation>;
            showTurboArbiter: IArbiter<boolean>;
            turboModeArbiter: IArbiter<boolean>;
        }

        /**
         * Settings for initialising the UI controller.
         * Usually this is stuff not known ahead-of-time (e.g. available bet options).
         */
        export interface InitSettings
        {
            locale: Localisation.Locale;
            currencyFormatter: Localisation.CurrencyFormatter;
        }
    }
}
