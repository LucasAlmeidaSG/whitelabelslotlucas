/// <reference path="ISoundSource.ts" />
/// <reference path="ISound.ts" />

namespace RS.Audio
{
    /**
     * Encapsulates sound control functionality.
     */
    export interface ISoundController extends Controllers.IController
    {
        /** Gets or sets the master volume for all audio. */
        masterVolume: number;

        /** Gets or sets if all audio should be muted. */
        muted: boolean;

        /** Gets or sets if all audio should be paused. */
        paused: boolean;

        /**
         * Creates a sound instance for the specified asset.
         */
        createSound(src: Asset.Base<ISoundSource> | null): ISound | null;
    }
    export const ISoundController = Controllers.declare<ISoundController, void>(Strategy.Type.Singleton);

    /**
     * Parameters for playing a sound.
     */
    export interface PlaySettings
    {
        paused: boolean;
        loop: boolean;
        volume: number;
        panning: number;
        pos: [number, number, number];
        startTime: number;
    }

    /**
     * Creates and starts playing a new sound by the specified asset group and asset ID.
     * @param id
     */
    export function play(group: string, assetID: string): ISound;

    /**
     * Creates and starts playing a new sound by the specified asset group and asset ID.
     * @param id
     */
    export function play(group: string, assetID: string, settings: Partial<PlaySettings>): ISound;

    /**
     * Creates and starts playing a new sound by the specified asset reference.
     * @param id
     */
    export function play(asset: Asset.SoundReference): ISound;

    /**
     * Creates and starts playing a new sound by the specified asset reference.
     * @param id
     */
    export function play(asset: Asset.SoundReference, settings: Partial<PlaySettings>): ISound;

    export function play(p1: Asset.Reference<string>|string, p2?: string | Partial<PlaySettings>, p3?: Partial<PlaySettings>): ISound
    {
        const controller = ISoundController.get();
        if (controller == null)
        {
            throw new Error("Attempted to play sound but no sound controller instance is present - did you forget to reference a concrete audio module?");
        }
        const assetID = Is.string(p1) ? (Is.string(p2) ? p2 : p1) : p1.id;
        const group = Is.string(p1) ? (Is.string(p2) ? p1 : Asset.globalManifest) : p1.group;
        const settings = Is.object(p3) ? p3 : Is.string(p2) ? null : p2;
        const asset = Asset.Store.getAsset(group, assetID);
        const snd = controller.createSound(asset as Asset.Base<ISoundSource>);
        if (settings)
        {
            if (settings.volume != null) { snd.volume = settings.volume; }
            if (settings.panning != null) { snd.panning = settings.panning; }
            if (settings.pos != null) { snd.pos = settings.pos; }
            if (settings.loop != null) { snd.loop = settings.loop; }
            if (settings.startTime != null) { snd.position = settings.startTime; }
            if (settings.paused === true) { snd.pause(); }
        }
        return snd;
    }
}
