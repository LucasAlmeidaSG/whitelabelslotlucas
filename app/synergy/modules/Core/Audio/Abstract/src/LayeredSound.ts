namespace RS.Audio
{
    /**
     * A layered sound made up of a number of synchronised sounds.
     * The sound layers should be of the same or smaller length as the first layer, as this one is used to "direct" the others.
     */
    export class LayeredSound implements IDisposable
    {
        protected _isDisposed: boolean = false;
        protected _layers: ISound[];
        protected _volume: number = 1;
        protected _fadeTimeline: RS.Timeline;

        /** Gets if this sound has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /**
         * Gets or sets the position of this layered sound.
        **/
        public get position(): number
        {
            if (this._layers.length === 0) { return 0; }
            for (let i = 0; i < this._layers.length; i++)
            {
                if (this._layers[i].volume > 0) { return this._layers[i].position; }
            }
            return this._layers[0].position;
        }
        public set position(value)
        {
            if (this._layers.length === 0) { return; }
            for (let i = 0; i < this._layers.length; i++)
            {
                this._layers[i].position = value;
            }
        }

        /**
         * Gets or sets if this layered sound is paused.
         */
        public get paused(): boolean
        {
            if (this._layers.length === 0) { return true; }
            return !this._layers[0].playing;
        }
        public set paused(value)
        {
            if (this._layers.length === 0) { return; }
            for (let i = 0; i < this._layers.length; i++)
            {
                if (value)
                {
                    this._layers[i].pause();
                }
                else
                {
                    this._layers[i].play();
                }
            }
            this.position = this.position; // this syncs up all the sounds
        }

        /**
         * Gets or sets if this layered sound should loop.
         */
        public get loop(): boolean
        {
            if (this._layers.length === 0) { return true; }
            return this._layers[0].loop;
        }
        public set loop(value)
        {
            if (this._layers.length === 0) { return; }
            for (let i = 0; i < this._layers.length; i++)
            {
                this._layers[i].loop = value;
            }
        }

        /**
         * Gets or sets the master volume of this sound.
         * Will not work correctly if set during an active fade.
         */
        public get masterVolume(): number
        {
            return this._volume;
        }
        public set masterVolume(value)
        {
            const oldVolume = this._volume;
            this._volume = value;
            for (let i = 0; i < this._layers.length; i++)
            {
                this._layers[i].volume = (this._layers[i].volume / oldVolume) * value;
            }
        }

        /**
         * Initialises a new instance of the LayeredSound class.
         * @param sounds    Sound layers to use.
         */
        constructor(...sounds: ISound[])
        {
            this._layers = sounds || [];

            // Normalise all the sounds
            this.position = this.position;
            this.paused = this.paused;
            this.loop = this.loop;
        }

        /**
         * Adds a sound layer to this layered sound.
         * @param sound     The sound instance.
         */
        public addSoundLayer(sound: ISound): this;

        /**
         * Adds a sound layer to this layered sound.
         * @param asset   The sound asset.
         */
        public addSoundLayer(asset: Asset.SoundReference): this;

        public addSoundLayer(sound: ISound|Asset.SoundReference): this
        {
            if (Is.assetReference(sound))
            {
                const snd = play(sound);
                snd.position = this.position;
                snd.loop = this.loop;
                snd.disposeOnEnd = false;
                this._layers.push(snd);
                if (this.paused)
                {
                    snd.pause();
                }
                else
                {
                    snd.play();
                }
            }
            else
            {
                this._layers.push(sound);
                sound.position = this.position;
                if (this.paused)
                {
                    sound.pause();
                }
                else
                {
                    sound.play();
                }
                sound.loop = this.loop;
                sound.volume = this.masterVolume;
            }
            return this;
        }

        /**
         * Sets the volume of the specified sound layer.
         * Fails silently if the layer index is invalid.
         * @param layerIndex    Index of the layer to change.
         * @param volume        The volume to set.
        **/
        public setLayerVolume(layerIndex: number, volume: number): this
        {
            if (layerIndex >= 0 && layerIndex < this._layers.length)
            {
                this._layers[layerIndex].volume = volume * this._volume;
            }
            return this;
        }

        /**
         * Gets the volume of the specified sound layer.
         * @param layerIndex    Index of the layer to change.
         * @param volume        The volume to set.
        **/
        public getLayerVolume(layerIndex: number): number
        {
            return this._layers[layerIndex].volume / this._volume;
        }

        /**
         * Fades the specified sound layer to the specified volume.
         * Fails silently if the layer index is invalid.
         * @param layerIndex    Index of the layer to change.
         * @param volume        The volume to fade to.
         * @param setPause      If passed, the "paused" property of this sound will be set to this after the fade.
        **/
        public fadeLayerVolume(layerIndex: number, volume: number, duration: number, doneFunc?: () => void): this
        {
            if (layerIndex >= 0 && layerIndex < this._layers.length)
            {
                const snd = this._layers[layerIndex];
                RS.Tween.removeTweens(snd);
                if (snd.volume === 0) { snd.position = this.position; }
                const endVol = volume * this._volume;
                if (snd.volume === endVol)
                {
                    if (doneFunc) { doneFunc(); }
                    return;
                }
                const tween = RS.Tween.get(snd)
                    .to({ volume: endVol }, duration);
                if (doneFunc) { tween.call(doneFunc); }
            }
            else if (doneFunc)
            {
                doneFunc();
            }
            return this;
        }

        /**
         * Fades ALL sound layers to the specified volume.
         * Does not affect the master volume.
         * @param layerIndex    Index of the layer to change.
         * @param volume        The volume to fade to.
         * @param setPause      If passed, the "paused" property of this sound will be set to this after the fade.
        **/
        public fadeVolume(volume: number, duration: number, doneFunc?: () => void): this
        {
            if (this._fadeTimeline)
            {
                this._fadeTimeline.finish();
                this._fadeTimeline = null;
            }
            const tweenArr: RS.Tween<LayeredSound | ISound>[] = [];
            for (let i = 0; i < this._layers.length; i++)
            {
                const snd = this._layers[i];
                if (snd.volume === 0) { snd.position = this.position; }
                tweenArr.push(
                    RS.Tween.get(snd, {override: true})
                        .to({volume: volume * this._volume}, duration)
                );
            }
            if (doneFunc != null)
            {
                tweenArr.push(
                    RS.Tween.get<LayeredSound>(this, {override: true})
                        .wait(duration)
                        .call(doneFunc)
                );
            }
            this._fadeTimeline = RS.Timeline.get(tweenArr);
            return this;
        }

        /**
         * Destroys this layered sound.
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            if (this._fadeTimeline)
            {
                this._fadeTimeline.finish();
                this._fadeTimeline = null;
            }
            for (let i = 0; i < this._layers.length; i++)
            {
                this._layers[i].dispose();
                RS.Tween.removeTweens(this._layers[i]);
            }
            RS.Tween.removeTweens<LayeredSound>(this);
            this._layers = null;
            this._isDisposed = true;
        }
    }
}
