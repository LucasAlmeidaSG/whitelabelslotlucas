/// <reference path="ISoundSource.ts" />

namespace RS.Audio
{
    /**
     * Represents an instance of a sound, backed by a sound source.
     */
    export interface ISound extends IDisposable
    {
        /** Gets or sets if this sound should be automatically disposed when it ends. */
        disposeOnEnd: boolean;

        /** Gets or sets the current position of the playhead of this sound (ms). */
        position: number;

        /** Gets or sets the current playback rate of this sound. */
        playbackRate: number;

        /** Gets the duration of this sound, in ms. */
        readonly duration: number;

        /** Gets if this sound is currently playing or not. */
        readonly playing: boolean;

        /** Gets the source of this sound. */
        readonly source: ISoundSource;

        /** Gets or sets if this sound should loop or not. */
        loop: boolean;

        /** Gets or sets the volume of this sound (0-1). */
        volume: number;

        /** Gets or sets the panning of this sound (-1 to 1). */
        panning: number;

        /** Gets or sets the spacial position of this sound (x, y, z). */
        pos: [number, number, number];

        /** Published when the sound has finished playing. Will occur if the sound is looped also. */
        readonly onFinished: IEvent;

        /** Begins or resumes playing this sound. */
        play(): void;

        /** Stops playing this sound, and resets the playhead to the start. */
        stop(): void;

        /** Stops playing this sound, leaving the position of the playhead intact. */
        pause(): void;

        /** Creates a tween on this sound with the specified properties. */
        tween(settings?: TweenSettings): Tween<ISound>;

        /** Fades this sound to the specified volume over a duration. */
        fade(volume: number, duration: number, ease?: EaseFunction): Tween<ISound>;
    }
}