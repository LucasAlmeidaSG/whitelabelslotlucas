namespace RS.Audio
{
    /**
     * Represents a sound clip.
     * The sound data could be stored in a standalone buffer, or be part of a larger buffer in the form of a sound sprite.
     */
    export interface ISoundSource extends IDisposable
    {
        /** Gets the total duration of this sound source (ms). */
        readonly length: number;
    }
}