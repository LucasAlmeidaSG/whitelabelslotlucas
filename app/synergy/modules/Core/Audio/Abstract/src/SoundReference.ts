namespace RS.Asset
{
    export type SoundReference = Asset.Reference<"sound">;
    export namespace SoundReference
    {
        /**
         * Gets the sound for this asset.
         * @param this 
         */
        export function get(this: SoundReference): Audio.ISoundSource | null
        {
            const asset = Asset.Store.getAsset(this);
            if (asset == null) { return null; }
            return asset.asset as Audio.ISoundSource;
        }

        /**
         * Plays this sound asset.
         * @param this 
         */
        export function play(this: SoundReference): Audio.ISound | null
        {
            return Audio.play(this);
        }
    }

}