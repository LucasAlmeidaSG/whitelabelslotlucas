/// <reference path="SoundSource.ts" />
/** @internal */
namespace RS.Howler
{
    /**
     * Represents an instance of a sound, backed by a sound source.
     */
    @HasCallbacks
    export class Sound implements Audio.ISound
    {
        /** Published when the sound has finished playing. Will occur if the sound is looped also. */
        @RS.AutoDispose
        public readonly onFinished = createSimpleEvent();

        protected _controller: SoundController;
        protected _source: SoundSource;
        protected _soundID: number;
        protected _loop: boolean = false;
        protected _disposeOnEnd: boolean = true;

        /** An inaudible 'sound', used to prevent timing issues due to autoplay policy etc. */
        @AutoDisposeOnSet
        private __virtualSound: VirtualSound;

        /** Gets or sets the sound controller. */
        public get controller() { return this._controller; }
        public set controller(value) { this._controller = value; }

        /** Gets or sets if this sound should be automatically disposed when it ends. */
        public get disposeOnEnd() { return this._disposeOnEnd; }
        public set disposeOnEnd(value) { this._disposeOnEnd = value; }

        /** Gets if this sound has been disposed. */
        public get isDisposed() { return this._source == null; }

        /** Gets or sets the current position of the playhead of this sound (ms). */
        @CheckDisposed
        public get position()
        {
            if (this.__virtualSound) { return this.__virtualSound.position; }
            const pos = (this._source.howl.seek(this._soundID));
            if (!Is.number(pos)) { return 0; }
            return pos * 1000;
        }
        public set position(value)
        {
            if (this.position === value) { return; }
            if (!Is.number(value)) { throw new Error(`Tried to seek sound to invalid value '${value}'`); }
            this.log(`seek to ${value}`);
            if (this.__virtualSound) { this.__virtualSound.position = value; return; }
            const offset = this._source.spriteInfo ? this._source.spriteInfo.startTime : 0;
            this._source.howl.seek(offset + value / 1000, this._soundID);
        }

        /** Gets or sets the current playback rate of this sound. */
        @CheckDisposed
        public get playbackRate()
        {
            if (this.__virtualSound) { return this.__virtualSound.playbackRate; }
            return this._source.howl.rate(this._soundID);
        }
        public set playbackRate(value)
        {
            this.log(`playback rate to ${value}`);
            if (this.__virtualSound) { this.__virtualSound.playbackRate = value; return; }
            this._source.howl.rate(value, this._soundID);
        }

        /** Gets the duration of this sound, in ms. */
        @CheckDisposed
        public get duration()
        {
            return this._source.howl.duration(this._soundID) * 1000;
        }

        /** Gets if this sound is currently playing or not. */
        @CheckDisposed
        public get playing()
        {
            if (this.__virtualSound) { return this.__virtualSound.playing; }
            return this._source.howl.playing(this._soundID);
        }

        /** Gets the source of this sound. */
        @CheckDisposed
        public get source() { return this._source; }

        /** Gets or sets if this sound should loop or not. */
        @CheckDisposed
        public get loop(): boolean { return this._loop; }
        public set loop(value)
        {
            this._loop = value;
            this.log(`loop ${value ? "enabled" : "disabled"}`);
            if (this.__virtualSound) { this.__virtualSound.loop = value; return; }
            this._source.howl.loop(value, this._soundID);
        }

        /** Gets or sets the volume of this sound (0-1). */
        @CheckDisposed
        public get volume()
        {
            if (this.__virtualSound) { return this.__virtualSound.volume; }
            return this._source.howl.volume(this._soundID);
        }
        public set volume(value)
        {
            this.log(`volume to ${value}`);
            if (this.__virtualSound) { this.__virtualSound.volume = value; return; }
            this._source.howl.volume(value, this._soundID);
        }

        /** Gets or sets the panning of this sound (-1 to 1). */
        @CheckDisposed
        public get panning()
        {
            if (this.__virtualSound) { return this.__virtualSound.stereoPan; }
            return this._source.howl.stereo(this._soundID);
        }
        public set panning(value)
        {
            this.log(`pan to ${value}`);
            if (this.__virtualSound) { this.__virtualSound.stereoPan = value; return; }
            this._source.howl.stereo(value, this._soundID);
        }

        /** Gets or sets the spacial position of this sound (x, y, z). */
        @CheckDisposed
        public get pos()
        {
            if (this.__virtualSound) { return this.__virtualSound.spatialPosition; }
            return this._source.howl.pos(null, null, null, this._soundID);
        }
        public set pos(value)
        {
            this.log(`move to (${value.join(", ")})`);
            if (this.__virtualSound) { this.__virtualSound.spatialPosition = value; return; }
            this._source.howl.pos(value[0], value[1], value[2], this._soundID);
        }

        public constructor(source: SoundSource, controller?: SoundController)
        {
            this._controller = controller;
            this._source = source;

            if (source.spriteInfo)
            {
                this._soundID = source.howl.play(source.spriteInfo.id);
                this.log(`created (${source.spriteInfo.startTime}s - ${source.spriteInfo.endTime}s)`);
            }
            else
            {
                this._soundID = source.howl.play();
                this.log("created");
            }

            source.howl.on("end", this.onEnd);

            if (!this.controller.canPlay)
            {
                this.emulate();
                source.howl.on("resume", this.onCanPlay);
                source.howl.on("unlock", this.onCanPlay);
            }
        }

        /** Begins or resumes playing this sound. */
        @CheckDisposed
        public play(): void
        {
            this.log("play");
            if (this.__virtualSound) { this.__virtualSound.paused = false; return; }
            this._source.howl.play(this._soundID);
        }

        /** Stops playing this sound, and resets the playhead to the start. */
        @CheckDisposed
        public stop(): void
        {
            this.log("stop");
            if (this._disposeOnEnd)
            {
                this.dispose();
            }
            else
            {
                this.pauseInternal();
                this.position = 0;
            }
        }

        /** Stops playing this sound, leaving the position of the playhead intact. */
        @CheckDisposed
        public pause(): void
        {
            this.log("pause");
            this.pauseInternal();
        }

        /** Creates a tween on this sound with the specified properties. */
        @CheckDisposed
        public tween(settings?: TweenSettings): Tween<Audio.ISound>
        {
            return Tween.get<Sound>(this, settings);
        }

        /** Fades this sound to the specified volume over a duration. */
        @CheckDisposed
        public fade(volume: number, duration: number, ease?: EaseFunction): Tween<Audio.ISound>
        {
            return this.tween().to({ volume }, duration);
        }

        /** Gets a string representation of this sound instance. */
        public toString(): string
        {
            if (this.isDisposed) { return "[Disposed Sound]"; }
            return this._source.spriteInfo ? `${this._source.id}.${this._source.spriteInfo.id}:${this._soundID}` : `${this._source.id}:${this._soundID}`;
        }

        /** Disposes this sound instance. */
        public dispose(): void
        {
            if (this.isDisposed) { return; }
            Tween.removeTweens<Sound>(this);
            this.log("dispose");
            if (this._controller)
            {
                this._controller.handleSoundDisposed(this);
            }
            if (this.__virtualSound)
            {
                this._source.howl.off("resume", this.onCanPlay);
                this._source.howl.off("unlock", this.onCanPlay);
            }
            this._source.howl.off("end", this.onEnd);
            this._source.howl.stop(this._soundID);
            this._source = null;
        }

        protected log(message: string): void
        {
            Logging.ILogger.get()
                .getContext("Audio")
                .log(`${this} - ${message}`, Logging.LogLevel.Debug);
        }

        @Callback
        protected async onCanPlay()
        {
            // Howler defers event listener callbacks using a setTimeout with a delay of 0 ms, which is not cleared by calling 'off'
            // This means if we become playable and then get disposed in the same frame, we will enter here with a null _source
            if (this.isDisposed) { return; }
            this._source.howl.off("resume", this.onCanPlay);
            this._source.howl.off("unlock", this.onCanPlay);
            this._source.howl.volume(0, this._soundID);
            await RS.Tween.wait<Sound>(1, this);
            if (this.__virtualSound) { this.realise(); }
        }

        @Callback
        protected onEnd(soundID: number = this._soundID)
        {
            if (soundID === this._soundID)
            {
                this.onFinished.publish();
                if (this._disposeOnEnd && !this._loop)
                {
                    this.dispose();
                }
            }
        }

        /** Hands over from Howler to a Tween-backed virtual sound, for timing reasons. */
        private emulate(): void
        {
            this.log("emulate");
            const duration = this._source.howl.duration(this._soundID) * 1000;
            this.__virtualSound = new VirtualSound(duration, this.onEnd);
        }

        /** Hands off from virtual sound to actual Howler sound instance. */
        private realise()
        {
            this.log("realise");
            if (this.__virtualSound.paused) { this._source.howl.pause(this._soundID); }
            const positionOffset = this._source.spriteInfo ? this._source.spriteInfo.startTime : 0;
            this._source.howl.seek(positionOffset + this.__virtualSound.position / 1000, this._soundID);
            this._source.howl.volume(this.__virtualSound.volume, this._soundID);
            this._source.howl.rate(this.__virtualSound.playbackRate, this._soundID);
            this._source.howl.pos(this.__virtualSound.spatialPosition[0], this.__virtualSound.spatialPosition[1], this.__virtualSound.spatialPosition[2], this._soundID);
            this._source.howl.stereo(this.__virtualSound.stereoPan, this._soundID);
            this._source.howl.loop(this.__virtualSound.loop, this._soundID);
            this.__virtualSound = null;
        }

        private pauseInternal()
        {
            if (this.__virtualSound) { this.__virtualSound.paused = true; return; }
            this._source.howl.pause(this._soundID);
        }
    }
}
