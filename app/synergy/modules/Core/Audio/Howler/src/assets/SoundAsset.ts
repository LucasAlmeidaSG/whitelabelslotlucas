/// <reference path="../SoundSource.ts" />

/** @internal */
namespace RS.Howler
{
    /**
     * Encapsulates a sound asset type.
     */
    @Asset.Type("sound")
    export class SoundAsset extends Asset.Base<Audio.ISoundSource>
    {
        /**
         * Selects a format to use from the given options.
         * @param options
         */
        public selectFormat(options: string[]): string | null
        {
            // Use m4a on chrome IOS
            if (options.indexOf("m4a") !== -1 && (Device.isAppleDevice && Device.isMobileDevice)) { return "m4a"; }

            // Use ogg on chrome
            if (options.indexOf("ogg") !== -1 && (Device.browser === Browser.Chrome || Device.browser === Browser.ChromeMobile)) { return "ogg"; }

            // Use m4a on safari
            if (options.indexOf("m4a") !== -1 && Device.browser === Browser.Safari) { return "m4a"; }

            // Fall back to mp3
            if (options.indexOf("mp3") !== -1) { return "mp3"; }

            // Unsupported
            return null;
        }

        public clone(): SoundAsset
        {
            return new SoundAsset(this.definition);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected async performLoad(url: string, format: string): Promise<Audio.ISoundSource>
        {
            url = Path.replaceExtension(url, `.${format}`);
            const objectURL = await Asset.ajax({ url }, Request.AJAXResponseType.ObjectURL);
            interface RawData
            {
                "samplerate": number;
                "spriteEntries": Map<{ "id": number, "startTime": number, "endTime": number }>;
            }
            const raw = this._def.raw as RawData;
            let sprite: IHowlSoundSpriteDefinition | null = null;
            if (raw && raw["spriteEntries"])
            {
                sprite = {};
                for (const key in raw["spriteEntries"])
                {
                    const entry = raw["spriteEntries"][key];
                    sprite[key] = [ Math.floor(entry.startTime * 1000), Math.ceil((entry.endTime - entry.startTime) * 1000) ];
                }
            }
            let soundSrc: SoundSource;
            try
            {
                soundSrc = await this.loadHowl(objectURL, format, sprite);
            }
            catch (err)
            {
                throw new Error(`Failed to load sound '${this.id}' (${err})`);
            }
            if (raw && raw["spriteEntries"])
            {
                for (const key in raw["spriteEntries"])
                {
                    const entry = raw["spriteEntries"][key];
                    const subSoundSrc = new SoundSource(this.id, soundSrc.howl, {
                        id: key,
                        startTime: entry["startTime"],
                        endTime: entry["endTime"]
                    });
                    const subSoundAsset = new DummySoundAsset({
                        ...this._def,
                        id: `${this._def.id}.${key}`
                    }, subSoundSrc);
                    Asset.Store.addAsset(subSoundAsset);
                }
            }
            return soundSrc;
        }

        protected loadHowl(src: string, format: string, sprite?: IHowlSoundSpriteDefinition): PromiseLike<SoundSource>
        {
            return new Promise((resolve, reject) =>
            {
                const howl = new Howl({
                    src, format, sprite,
                    onload: () =>
                    {
                        resolve(new SoundSource(this.id, howl));
                    },
                    onloaderror: (soundID, err) =>
                    {
                        reject(err);
                    }
                });
            });
        }
    }
}
