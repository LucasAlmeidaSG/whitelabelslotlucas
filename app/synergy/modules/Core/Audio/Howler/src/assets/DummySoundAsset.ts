/// <reference path="../SoundSource.ts" />

/** @internal */
namespace RS.Howler
{
    /**
     * Encapsulates a sound asset that wraps.
     */
    export class DummySoundAsset extends Asset.Base<Audio.ISoundSource>
    {
        public constructor(definition: Asset.Definition, src: SoundSource)
        {
            super(definition);
            this._asset = src;
            this._state.value = Asset.State.Loaded;
        }

        /**
         * Selects a format to use from the given options.
         * @param options 
         */
        public selectFormat(options: string[]): string | null
        {
            // Unsupported
            return null;
        }

        public clone(): DummySoundAsset
        {
            return new DummySoundAsset(this.definition, this._asset as SoundSource);
        }

        /**
         * Performs the actual load process for this asset.
         * @param url 
         */
        protected async performLoad(url: string, format: string): Promise<Audio.ISoundSource>
        {
            return this._asset;
        }
    }
}