namespace RS.Howler
{
    /** An inaudible 'sound', used to prevent timing issues due to autoplay policy etc. */
    export class VirtualSound implements IDisposable
    {
        public get position() { return this._tween.position; }
        public set position(value) { this._tween.position = value; }

        public get loop() { return this._tween.loop; }
        public set loop(value) { this._tween.loop = value; }

        public get playbackRate() { return this._tween.timeScale; }
        public set playbackRate(value) { this._tween.timeScale = value; }

        public get playing() { return !this._tween.paused && !this._tween.finished; }

        public get paused() { return this._tween.paused; }
        public set paused(value) { this._tween.paused = value; }

        // Sound-specific properties, don't actually do anything
        public spatialPosition: [number, number, number] = [0, 0, 0]
        public stereoPan = 0;
        public volume = 1;

        @AutoDispose
        private readonly _tween: RS.Tween<VirtualSound> = Tween.get(this as VirtualSound, { realTime: true });

        private _isDisposed: boolean = false;
        public get isDisposed() { return this._isDisposed; }

        constructor(duration: number, onEnd: () => void)
        {
            this._tween
                .wait(duration)
                .call(() => { onEnd(); });
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }
    }
}
