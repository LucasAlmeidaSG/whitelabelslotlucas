/** @internal */
namespace RS.Howler
{
    const HowlerLib: HowlerGlobal = window["Howler"];

    /**
     * Sound controller for howler.
     */
    export class SoundController implements Audio.ISoundController
    {
        public readonly settings: void;

        protected _sounds: Audio.ISound[] = [];
        protected _isDisposed: boolean = false;
        protected _masterVolume: number = 1.0;
        protected _muted: boolean = false;
        protected _paused: boolean = false;

        /** Gets if this controller is disposed. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets if we can play sound without being blocked etc. by interaction policy. */
        public get canPlay() { return HowlerLib.state === "running"; }

        /** Gets or sets the master volume for all audio. */
        public get masterVolume() { return this._masterVolume; }
        public set masterVolume(value)
        {
            if (this._masterVolume === value) { return; }
            this._masterVolume = value;
            HowlerLib.volume(value);
        }

        /** Gets or sets if all audio should be muted. */
        public get muted() { return this._muted; }
        public set muted(value)
        {
            if (this._muted === value) { return; }
            this._muted = value;
            HowlerLib.mute(value);
        }

        /** Gets or sets if all audio should be paused. */
        public get paused() { return this._paused; }
        public set paused(value)
        {
            if (this._paused !== value)
            {
                this.setSoundPaused(value);
            }
        }

        public initialise(): void { return; }

        /**
         * Called by a sound when it is disposed.
         */
        public handleSoundDisposed(sound: Audio.ISound): void
        {
            const index = this._sounds.indexOf(sound);
            if (index > -1)
            {
                this._sounds.splice(index, 1);
            }
        }

        /**
         * Creates a sound instance for the specified asset.
         */
        public createSound(src: Asset.Base<Audio.ISoundSource> | null): Audio.ISound | null
        {
            if (src == null)
            {
                RS.Log.debug("Sound asset not played as it was null or undefined.");
                return null;
            }
            if (src.asset instanceof SoundSource)
            {
                const sound = new Sound(src.asset, this);
                this._sounds.push(sound);
                return sound;
            }
            else
            {
                throw new Error(`Tried to createSound using a non-sound asset (${src.id})`);
            }
        }

        /**
         * Disposes this controller.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }

            this._isDisposed = true;
        }

        protected async setSoundPaused(value)
        {
            // When unpausing on iOS/safari, wait a moment to prevent iOS/safari sound bug
            if (this.paused && !value)
            {
                if (RS.Device.browser === RS.Browser.Safari)
                {
                    await ITicker.get().after(RS.Device.isAppleDevice ? 1000 : 100);
                }
            }

            if (HowlerLib.usingWebAudio && HowlerLib.ctx)
            {
                this._paused = value;
                if (value)
                {
                    HowlerLib.ctx.suspend();
                }
                else
                {
                    HowlerLib.ctx.resume();
                }
            }
            else
            {
                this._paused = value;
                for (const sound of this._sounds)
                {
                    if (this._paused)
                    {
                        sound.pause();
                    }
                    else
                    {
                        sound.play();
                    }
                }
            }
        }
    }

    Audio.ISoundController.register(SoundController);
}
