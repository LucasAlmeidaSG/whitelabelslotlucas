/** @internal */
namespace RS.Howler
{
    export interface SpriteInfo
    {
        id: string;
        startTime: number;
        endTime: number;
    }

    export class SoundSource implements Audio.ISoundSource
    {
        protected _id: string;
        protected _howl: Howl;
        protected _spriteInfo: SpriteInfo | null;

        public get id() { return this._id; }

        /** Gets if this sound source has been disposed. */
        public get isDisposed() { return this._howl == null; }

        /** Gets the underlying howl for this sound source. */
        public get howl() { return this._howl; }

        /** Gets the total duration of this sound source (ms). */
        public get length() { return this._howl.duration(); }

        /** Gets the sprite information for this sound source, or null if it's not a sound sprite. */
        public get spriteInfo() { return this._spriteInfo; }

        public constructor(id: string, howl: Howl, spriteInfo: SpriteInfo | null = null)
        {
            this._id = id;
            this._howl = howl;
            this._spriteInfo = spriteInfo;
            // Log.debug(`[Audio] Sound source created for ${id}`);
        }

        /** Disposes this sound source. */
        public dispose()
        {
            if (this.isDisposed) { return; }
            this._howl.unload();
            this._howl = null;
        }
    }
}