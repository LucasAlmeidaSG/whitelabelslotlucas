/// <reference path="helpers/Error.ts"/>
namespace RS
{
    export function assert(condition: boolean, message: string): void
    {
        if (!condition) { throw new AssertionError(message); }
    }

    export function hope(condition: boolean, message: string): void
    {
        if (!condition) { Log.warn(`AssertionWarning: ${message}`); }
    }

    export class AssertionError extends BaseError
    {
        constructor(message: string)
        {
            super(`AssertionError: ${message}`);
        }
    }
}