/// <reference path="URL.ts" />

namespace RS.Capabilities
{
    export interface WebGL
    {
        /** Whether or not WebGL is available. */
        available: boolean;

        /** Whether or not S3TC is supported. */
        supportsS3TC: boolean;

        /** Whether or not PVRTC is supported. */
        supportsPVRTC: boolean;

        /** Whether or not high precision fragment shaders are supported. */
        supportsHighPrecisionFragmentShaders: boolean;

        /** Maximum texture size. */
        maxTextureSize: number;

        /** Whether or not SRGB is supported. */
        supportsSRGB: boolean;
    }

    export namespace WebGL
    {
        let cachedInfo: WebGL | null = null;
        let cachedRenderer: string | null = null;

        /**
         * Queries WebGL capabilities on the current device.
         */
        export function get(): WebGL
        {
            return cachedInfo || (cachedInfo = getInternal());
        }

        /**
         * Returns the device's GPU, or if using the canvas renderer, a string specifying so.
         */
        export function getRenderer(): string
        {
            if (cachedRenderer != null) { return cachedRenderer; }

            const canvas = document.createElement("canvas");
            canvas.width = 1;
            canvas.height = 1;

            const context = canvas.getContext("webgl") || canvas.getContext("experimental-webgl") as WebGLRenderingContext;
            if (!context) { return Performance.DeviceRenderer.Canvas; }

            const info = context.getExtension("WEBGL_debug_renderer_info");

            cachedRenderer = info
                ? context.getParameter(info.UNMASKED_RENDERER_WEBGL)
                : Performance.DeviceRenderer.Canvas;

            const loseContext = context.getExtension("WEBGL_lose_context");
            if (loseContext) { loseContext.loseContext(); }

            return cachedRenderer;
        }

        /**
         * Tries to read the given string-or-undefined using a given string-reader.
         * Returns null if the string is null or invalid.
         */
        function tryTo<T>(read: (str: string, defaultVal: T | null) => T, str: string | null): T | null
        {
            if (str == null) { return null; }
            return read(str, null);
        }

        /**
         * Applies capability overrides from the URL to the given info object.
         * Example of syntax: forcecaps=gl:1,s3tc:0
         */
        function applyOverrides(info: WebGL): void
        {
            const forceCaps = URL.getParameter("forcecaps");
            if (forceCaps)
            {
                const map: Map<string> = {};
                for (const lrRaw of forceCaps.split(","))
                {
                    const [key, value] = lrRaw.split(":");
                    map[key] = value;
                }

                const { gl, s3tc, pvrtc, highp, texd, srgb } = map;

                info.available = tryTo(Read.boolean, gl),
                info.supportsS3TC = tryTo(Read.boolean, s3tc);
                info.supportsPVRTC = tryTo(Read.boolean, pvrtc);
                info.supportsHighPrecisionFragmentShaders = tryTo(Read.boolean, highp);
                info.maxTextureSize = tryTo(Read.integer, texd);
                info.supportsSRGB = tryTo(Read.boolean, srgb);
            }
        }

        /** Queries WebGL capabilities, ignoring the cache. */
        function getInternal(): WebGL
        {
            const caps: WebGL = { available: null, supportsPVRTC: null, supportsS3TC: null, supportsHighPrecisionFragmentShaders: null, maxTextureSize: null, supportsSRGB: null };

            applyOverrides(caps);

            const canvas = document.createElement("canvas");
            canvas.width = 1;
            canvas.height = 1;

            const context = canvas.getContext("webgl");
            if (!context)
            {
                // No WebGL so incapable of everything; set non-overridden properties to false.
                if (caps.available == null) { caps.available = false; }
                if (caps.supportsS3TC == null) { caps.supportsS3TC = false; }
                if (caps.supportsPVRTC == null) { caps.supportsPVRTC = false; }
                if (caps.supportsHighPrecisionFragmentShaders == null) { caps.supportsHighPrecisionFragmentShaders = false; }
                if (caps.supportsSRGB == null) { caps.supportsSRGB = false; }
                return caps;
            }

            // Set non-overridden properties accordingly.
            const extensions = context.getSupportedExtensions();

            if (caps.available == null)
            {
                caps.available = true;
            }

            if (caps.supportsPVRTC == null)
            {
                caps.supportsPVRTC = extensions.indexOf("WEBGL_compressed_texture_pvrtc") !== -1 || extensions.indexOf("WEBKIT_WEBGL_compressed_texture_pvrtc") !== -1;
            }

            if (caps.supportsS3TC == null)
            {
                caps.supportsS3TC = extensions.indexOf("WEBGL_compressed_texture_s3tc") !== -1 || extensions.indexOf("WEBKIT_WEBGL_compressed_texture_s3tc") !== -1;
            }

            if (caps.supportsHighPrecisionFragmentShaders == null)
            {
                caps.supportsHighPrecisionFragmentShaders = context.getShaderPrecisionFormat(context.FRAGMENT_SHADER, context.HIGH_FLOAT) != null && context.getShaderPrecisionFormat(context.FRAGMENT_SHADER, context.HIGH_INT) != null;
            }

            if (caps.maxTextureSize == null)
            {
                caps.maxTextureSize = context.getParameter(context.MAX_TEXTURE_SIZE);
            }

            if (caps.supportsSRGB == null)
            {
                caps.supportsSRGB = extensions.indexOf("EXT_sRGB") !== -1;
            }

            const loseContext = context.getExtension("WEBGL_lose_context");
            if (loseContext) { loseContext.loseContext(); }

            return caps;
        }
    }
}
