/// <reference path="../Strategy.ts" />

namespace RS.Controllers
{
    /**
     * Encapsulates a controller.
     */
    export interface IController<TSettings = void> extends IDisposable
    {
        readonly settings: TSettings;

        /** Initialises this controller. */
        initialise(settings: TSettings): void;
    }

    /**
     * Declares a controller of the specified type.
     * @param type
     */
    export function declare<TController extends IController<TSettings>, TSettings>(type = Strategy.Type.Instance, allowOverrides = Strategy.Overridable.Never): Strategy.IProvider<TController>
    {
        return Strategy.declare(type, false, allowOverrides);
    }
}