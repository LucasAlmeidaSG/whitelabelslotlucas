/// <reference path="Is.ts" />

namespace RS
{
    /** Provides utility functions for manipulating cookies. */
    export class Cookie
    {
        // https://github.com/ScottHamper/Cookies

        // Used to ensure cookie keys do not collide with
        // built-in `Object` properties

        private static readonly __cacheKeyPrefix = "cookey."; // Hurr hurr, :)

        private static readonly __maxExpireDate = new Date("Fri, 31 Dec 9999 23:59:59 UTC");
        private static readonly __autoExpireDate = new Date(0);

        private static readonly __defaultOptions: Cookie.Options =
        {
            path: "/",
            secure: false
        };

        private static __cachedDocumentCookie?: string;
        private static __cache: Map<string> = {};

        private static __enabledCache?: boolean;
        /**
         * Whether or not cookies are supported and enabled. \
         * NOTE: simply accessing this may attempt to write to document.cookie.
         */
        public static get enabled()
        {
            if (this.__enabledCache == null)
            {
                this.__enabledCache = this.areEnabled();
            }
            return this.__enabledCache;
        }

        private constructor() { throw new TypeError("Illegal constructor"); }

        /**
         * Gets the value of the specified cookie, or null if it doesn"t exist.
         * @param key
         */
        public static get(key: string): string | null
        {
            if (this.__cachedDocumentCookie !== document.cookie)
            {
                this.renewCache();
            }

            const value = this.__cache[`${this.__cacheKeyPrefix}${key}`];
            return value == null ? null : decodeURIComponent(value);
        }

        /**
         * Sets the value of the specified cookie.
         * @param key
         * @param value
         * @param options
         */
        public static set(key: string, value?: string | number, options?: Partial<Cookie.Options>): void
        {
            if (options != null)
            {
                options = { ...this.__defaultOptions, ...options };
            }
            else
            {
                options = { ...this.__defaultOptions };
            }
            document.cookie = this.generateCookieString(key, value, options);
        }

        /**
         * Expires the specified cookie.
         * @param key
         * @param options
         */
        public static expire(key: string, options?: Partial<Cookie.Options>): void
        {
            this.set(key, "", options ? { expires: this.__autoExpireDate, ...options } : { expires: this.__autoExpireDate });
        }

        protected static isValidDate(date: any): date is Date
        {
            return date != null && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date.getTime());
        }

        protected static getExpiresDate(expires: number | string | Date, now?: Date): Date
        {
            now = now || new Date();

            if (Is.number(expires))
            {
                expires = expires === Infinity ? this.__maxExpireDate : new Date(now.getTime() + expires * 1000);
            }
            else if (Is.string(expires))
            {
                expires = new Date(expires);
            }
            else if (!this.isValidDate(expires))
            {
                throw new Error("'expires' parameter cannot be converted to a valid Date instance");
            }
            return expires;
        }

        protected static generateCookieString(key: string, value: string | number, options?: Partial<Cookie.Options>): string
        {
            key = key.replace(/[^#$&+\^`|]/g, encodeURIComponent);
            key = key.replace(/\(/g, "%28").replace(/\)/g, "%29");
            value = (value + "").replace(/[^!#$&-+\--:<-\[\]-~]/g, encodeURIComponent);
            let cookieString = key + "=" + value;

            if (options)
            {
                cookieString += options.path ? ";path=" + options.path : "";
                cookieString += options.domain ? ";domain=" + options.domain : "";
                cookieString += options.expires ? ";expires=" + this.getExpiresDate(options.expires).toUTCString() : "";
                cookieString += options.secure ? ";secure" : "";
            }

            return cookieString;
        }

        protected static getCacheFromString(documentCookie?: string): Map<string>
        {
            const cookieCache = {};
            const cookiesArray = documentCookie ? documentCookie.split("; ") : [];

            for (let i = 0; i < cookiesArray.length; i++)
            {
                const cookieKvp = this.getKeyValuePairFromCookieString(cookiesArray[i]);

                if (cookieCache[`${this.__cacheKeyPrefix}${cookieKvp.key}`] == null)
                {
                    cookieCache[`${this.__cacheKeyPrefix}${cookieKvp.key}`] = cookieKvp.value;
                }
            }

            return cookieCache;
        }

        protected static getKeyValuePairFromCookieString(cookieString: string): { key: string; value: string; }
        {
            // "=" is a valid character in a cookie value according to RFC6265, so cannot `split("=")`
            let separatorIndex = cookieString.indexOf("=");

            // IE omits the "=" when the cookie value is an empty string
            separatorIndex = separatorIndex < 0 ? cookieString.length : separatorIndex;

            const key = cookieString.substr(0, separatorIndex);
            let decodedKey: string;
            try
            {
                decodedKey = decodeURIComponent(key);
            }
            catch (e)
            {
                Log.error(`Could not decode cookie with key "${key}"`);
                Log.error(e);
            }

            return {
                key: decodedKey,
                value: cookieString.substr(separatorIndex + 1) // Defer decoding value until accessed
            };
        }

        protected static renewCache(): void
        {
            this.__cache = this.getCacheFromString(document.cookie);
            this.__cachedDocumentCookie = document.cookie;
        }

        protected static areEnabled(): boolean
        {
            const testKey = "Synergy";
            this.set(testKey, 1);
            const isEnabled = this.get(testKey) === "1";
            this.expire(testKey);
            return isEnabled;
        }
    }

    export namespace Cookie
    {
        /**
         * Cookie options.
         */
        export interface Options
        {
            path: string;
            domain?: string;
            expires?: Date | number | string;
            secure: boolean;
        }
    }
}