namespace RS
{
    const shouldExposeCommand = true;
    const commandName = "rsdump";

    export interface IDump
    {
        /** Whether or not dumping is enabled. If false, new messages will not be stored. */
        enabled: boolean;

        /** Published before the dump is saved. */
        readonly onPreSave: IEvent<IDump>;

        /** Gets a named sub-dump. */
        getSubDump(name: string): ISubDump;

        /** Saves the dump. */
        save(): void | Promise<void>;

        /** Clears the dump. */
        clear(): void;
    }

    export interface ISubDump extends IDisposable
    {
        /** Whether or not this sub-dump should clear as soon as it is saved. Defaults to false. */
        clearOnSave: boolean;

        /** Adds an object to this sub dump. */
        add(object: object, deep: boolean): void;
        /** Adds an object to this sub dump. */
        add(object: object, whitelistFn?: (this: object, key: string, value: object) => boolean): void;
        /** Adds something to this sub dump. */
        add(data: any): void;

        /** Clears this sub dump. */
        clear(): void;
    }

    export const IDump = Strategy.declare<IDump>(Strategy.Type.Singleton, false, true);

    if (shouldExposeCommand)
    {
        window[commandName] = async function ()
        {
            const dump = IDump.get();
            await dump.save();
        };
    }

    window.addEventListener("error", function (ev: ErrorEvent)
    {
        const dump = IDump.get();
        if (!dump.enabled) { return; }
        const subDump = dump.getSubDump("UncaughtErrors");

        const date = new Date();
        const datetime = Dump.getTimestamp(date);
        const timestamp = date.getTime();

        subDump.add({ "error": ev.error, "datetime": datetime, "timestamp": timestamp, "promise": false });
    });

    window.addEventListener("unhandledrejection", function (ev: PromiseRejectionEvent)
    {
        const dump = IDump.get();
        if (!dump.enabled) { return; }
        const subDump = dump.getSubDump("UncaughtErrors");

        const date = new Date();
        const datetime = Dump.getTimestamp(date);
        const timestamp = date.getTime();

        subDump.add({ "error": ev.reason, "datetime": datetime, "timestamp": timestamp, "promise": true });
    });
}