namespace RS.Dump
{
    /** Gets a human-readable timestamp from the given date. */
    export function getTimestamp(date: number | Date = new Date())
    {
        if (Is.number(date)) { date = new Date(date); }

        const year = `${date.getFullYear()}`;
        // Month is 0 based so we need to add 1
        const month = Util.padLeft(`${date.getMonth() + 1}`, "0", 2);
        // getDate actually returns the day of the month
        const day = Util.padLeft(`${date.getDate()}`, "0", 2);
        const hours = Util.padLeft(`${date.getHours()}`, "0", 2);
        const minutes = Util.padLeft(`${date.getMinutes()}`, "0", 2);

        return `${year}_${month}_${day}-${hours}_${minutes}`;
    }

    /** Stringifies an object using the given whitelist function. */
    export function stringify(target: string | number | boolean | object, whitelistFn?: (this: object, key: string, value: object) => boolean)
    {
        return Is.primitive(target) ? JSON.stringify(target) : JSON.stringify(target, getObjectReplacer(target, whitelistFn));
    }

    /** Prepares an object for dumping using the given whitelist function. */
    export function prepare(object: object, whitelistFn?: (this: object, key: string, value: object) => boolean)
    {
        const replacer = getObjectReplacer(object, whitelistFn);
        return prepareInternal(object, replacer);
    }

    /** Returns true if the given object has members which are also objects. */
    function isDeepObject(value: object): boolean
    {
        for (const key in value)
        {
            const propValue = accessObjectPropertySafe(value, key);
            if (Is.object(propValue)) { return true; }
        }
        return false;
    }

    /** Tag added to prepared objects to ensure they are not toString()'d. */
    const preserveTag = "___dumpPreserve";
    /** Constructor name tag. */
    const ctorTag = "__ctor";
    /** toString tag. */
    const strTag = "__str";
    const defaultStr = Object.prototype.toString();

    function prepareInternal(object: object, replacer: (this: object, key: string, value: any) => any)
    {
        const out = Is.array(object) ? [] : {};

        // Don't stringify this object
        out[preserveTag] = true;

        // Add ctor name to the dump.
        if (object.constructor !== Object && object.constructor !== Array) { out[ctorTag] = Util.getClassName(object); }

        // Add toString() value to the dump.
        const strVal = stringifyValueSafe(object);
        if (strVal != defaultStr) { out[strTag] = strVal; }

        // Add the object key-value pairs to the dump
        const ownPropertyNames = Object.getOwnPropertyNames(object);
        for (const key of ownPropertyNames)
        {
            const oldValue = accessObjectPropertySafe(object, key);
            if (Is.func(oldValue)) { continue; }

            const replacedValue = replacer.call(object, key, oldValue);
            const newValue = (Is.object(replacedValue) || Is.array(replacedValue))
                ? prepareInternal(replacedValue, replacer)
                : replacedValue;
            out[key] = newValue;
        }

        return out;
    }

    /** Safely accesses an object property. If the object has an accessor that throws an error, we don't lose the dump. */
    function accessObjectPropertySafe(object: object, key: string): any
    {
        try
        {
            return object[key];
        }
        catch (e)
        {
            // The error itself could be a rogue custom class and throw an error on toString(), so let's be careful with that too
            return stringifyValueSafe(e);
        }
    }

    /** Safely stringifies a value. If an object has a custom toString() that throws an error, we don't lose the dump. */
    function stringifyValueSafe(value: any): string
    {
        try
        {
            return `${value}`;
        }
        catch (e)
        {
            // The error itself could be a rogue custom class and throw an error on toString(), so let's be careful with that too
            return stringifyValueSafe(e);
        }
    }

    /** Gets a single-use replacer function. */
    function getObjectReplacer(target: object, whitelistFn: (this: object, key: string, value: object) => boolean = () => false)
    {
        const visited = [];
        return function replacer(this: object, key: string, value: any): any
        {
            // Discard preserve tags.
            if (key === preserveTag) { return; }
            // Keep all non-object values.
            if (!Is.object(value)) { return value; }
            // Each object only appears once to prevent infinite looping or circular structur errors as well as keep the dump size down.
            if (visited.indexOf(value) !== -1) { return stringifyValueSafe(value); }
            // Flatten objects which contain other objects, unless either the target, tagged as preserve or whitelisted.
            if (isDeepObject(value) && value !== target && !value[preserveTag] && !whitelistFn.call(this, key, value))
            {
                let whitelisted: boolean = false;
                try
                {
                    whitelisted = whitelistFn.call(this, key, value);
                }
                catch (e)
                {
                    // Whitelist function threw an error, let's just treat it as not whitelisted and carry on.
                    Log.warn(`Error in whitelist function: ${e}`);
                }

                if (!whitelisted) { return stringifyValueSafe(value); }
            }
            // Track object.
            visited.push(value);
            return value;
        }
    }
}
