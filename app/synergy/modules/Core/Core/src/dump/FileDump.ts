/// <reference path="IDump.ts"/>

namespace RS
{
    class FileDump implements IDump, IDisposable
    {
        public enabled: boolean = false;

        @AutoDispose
        public readonly onPreSave = createEvent<IDump>();

        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }

        private readonly _subDumps: { [name: string]: SubDump; } = {};

        public getSubDump(name: string): ISubDump
        {
            return this._subDumps[name] || (this._subDumps[name] = new SubDump(this));
        }

        public clear()
        {
            for (const subDumpName in this._subDumps)
            {
                const subDump = this._subDumps[subDumpName];
                subDump.clear();
            }
        }

        public save()
        {
            const map: { [name: string]: any } = {};
            try
            {
                this.onPreSave.publish(this);
            }
            catch (e)
            {
                // We don't want a bad dump handler to lose us the rest of the dump.
                // Save error to the dump and continue.
                const errArr = map["DumpError"] || (map["DumpError"] = []);
                errArr.push(`Dump.onPreSave encountered an error during publish: ${e instanceof Error ? e.stack : e}`);
            }

            const subDumpNames = Object.keys(this._subDumps).sort();
            for (const subDumpName of subDumpNames)
            {
                const subDump = this._subDumps[subDumpName];
                map[subDumpName] = subDump.data;
            }

            const text = Dump.stringify(map);
            const curTime = Dump.getTimestamp();
            const blob = new Blob([text], { type: "application/octet-stream" });
            URL.download(`dump-${curTime}.json`, blob);

            for (const subDumpName in this._subDumps)
            {
                const subDump = this._subDumps[subDumpName];
                if (subDump.clearOnSave)
                {
                    subDump.clear();
                }
            }
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this.clear();
            this._isDisposed = true;
        }

        public undeclare(subDump: SubDump)
        {
            for (const key in this._subDumps)
            {
                const thisSubDump = this._subDumps[key];
                if (thisSubDump === subDump) { delete this._subDumps[key]; }
            }
        }
    }

    class SubDump implements ISubDump
    {
        public clearOnSave = false;

        private readonly _data: any[] = [];
        public get data(): ReadonlyArray<any> { return this._data; }

        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }

        constructor(private readonly _dump: FileDump) {}

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._dump.undeclare(this);
            this._isDisposed = true;
        }

        public add(object: object, deep: boolean): void;
        public add(object: object, whitelistFn?: (this: object, key: string, value: object) => boolean): void;
        public add(data: string | number | boolean): void;
        public add(data: string | number | boolean | object | (() => string | number | boolean | object), p2?: boolean | ((this: object, key: string, value: object) => boolean)): void
        {
            const whitelistFn = Is.func(p2) ? p2 : () => p2;
            if (!this._dump.enabled) { return; }
            try
            {
                const prepared = Is.primitive(data) ? data : Dump.prepare(data, whitelistFn);
                this._data.push(prepared);
            }
            catch (err)
            {
                this._data.push(`SubDump.add() failed: ${err instanceof Error ? err.stack : err}`);
                Log.error("SubDump.add() failed", err);
            }
        }

        public clear()
        {
            this._data.length = 0;
        }
    }

    IDump.register(FileDump);
}