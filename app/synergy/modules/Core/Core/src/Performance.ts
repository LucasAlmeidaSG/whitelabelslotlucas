namespace RS.Performance
{
    export enum DeviceRenderer
    {
        Canvas = "Canvas",
        WebGL = "WebGL"
    };

    export enum Level
    {
        Poor,
        Low,
        Medium,
        High,
        Uncertain
    };

    /**
     * List of Apple GPUs as of 24th October 2018
     */
    export enum AppleGPUs
    {
        /**
         * iPhone 6
         */
        Generic = "Apple GPU",
        /**
         * Used in -
         * iPad 1st Gen,
         * iPhone 4,
         * iPod Touch 4th Gen,
         * Apple TV 2nd Gen
         */
        A4 = "Apple A4 GPU",
        /**
         * Used in -
         * iPad 2,
         * iPhone 4S,
         * Apple TV 3rd Gen,
         * iPod Touch 5th Gen,
         * iPad Mini 1st Gen
         */
        A5 = "Apple A5 GPU",
        /**
         * Used in -
         * iPad 3rd Gen
         */
        A5X = "Apple A5X GPU",
        /**
         * Used in -
         * iPhone 5,
         * iPhone 5C
         */
        A6 = "Apple A6 GPU",
        /**
         * Used in -
         * iPad 4th Gen
         */
        A6X = "Apple A6X GPU",
        /**
         * Used in -
         * iPhone 5S,
         * iPad Mini 2,
         * iPad Mini 3,
         * iPad Air 1st Gen
         */
        A7 = "Apple A7 GPU",
        /**
         * Used in -
         * iPhone 6,
         * iPhone 6 Plus,
         * iPod Touch 6th Gen,
         * iPad Mini 4,
         * Apple TV 4th Gen,
         * HomePod
         */
        A8 = "Apple A8 GPU",
        /**
         * Used in -
         * iPad Air 2
         */
        A8X = "Apple A8X GPU",
        /**
         * Used in -
         * iPhone 6S,
         * iPhone 6S Plus,
         * iPhone SE,
         * iPad 2017
         */
        A9 = "Apple A9 GPU",
        /**
         * Used in -
         * iPad Pro 1st Gen
         */
        A9X = "Apple A9X GPU",
        /**
         * Used in -
         * iPhone 7,
         * iPhone 7 Plus,
         * iPad 2018
         */
        A10 = "Apple A10 GPU",
        /**
         * Used in -
         * iPad Pro 2nd Gen,
         * Apple TV 4K
         */
        A10X = "Apple A10X GPU",
        /**
         * Used in -
         * iPhone 8,
         * iPhone 8 Plus,
         * iPhone X
         */
        A11 = "Apple A11 GPU",
        /**
         * Used in -
         * iPhone XS,
         * iPhone XS Max,
         * iPhone XR
         */
        A12 = "Apple A12 GPU",
        /**
         * Alternative name for older devices
         */
        PowerVR535 = "PowerVR SGX 535",
        /**
         * Alternative name for older devices
         */
        PowerVR543 = "PowerVR SGX 543",
        /**
         * Alternative name for older devices
         */
        PowerVR554 = "PowerVR SGX 554"
    };

    /**
     * List of ARM GPUs based off the supported devices list as of 24th October 2018
     */
    export enum ARMGPUs
    {
        /**
         * Used in -
         * Samsung Galaxy S6
         */
        MaliT760 = "Mali-T760",
        /**
         * Used in -
         * Samsung Galaxy S7 Edge (G935FD, G935F, G935W8)
         */
        MaliT880 = "Mali-T880",
        /**
         * Used in -
         * Samsung Galaxy S8 (EMEA)
         */
        MaliG71 = "Mali-G71",
        /**
         * Used in -
         * Samsung Galaxy S9 (EMEA)
         */
        MaliG72 = "Mali-G72",
        /**
         * Used in -
         * Samsung Galaxy S10
         */
        MaliG76 = "Mali-G76"
    };

    /**
     * List of Qualcomm GPUs based off the supported devices list as of 24th October 2018
     */
    export enum QualcommGPUs
    {
        /**
         * Used in -
         * GALAXY TAB A
         */
        Adreno306 = "Adreno (TM) 306",
        /**
         * Used in -
         * Nexus 7
         */
        Adreno320 = "Adreno (TM) 320",
        /**
         * Used in -
         * HTC One M8,
         * Sony Xperia Z3,
         * Samsung Galaxy S5
         */
        Adreno330 = "Adreno (TM) 330",
        /**
         * Used in -
         * Galaxy C5
         */
        Adreno405 = "Adreno (TM) 405",
        /**
         * Used in -
         * Samsung Galaxy S7 Edge (G9350)
         */
        Adreno530 = "Adreno (TM) 530",
        /**
         * Used in -
         * Samsung Galaxy S8 (USA & China)
         */
        Adreno540 = "Adreno (TM) 540",
        /**
         * Used in -
         * Samsung Galaxy S9 (USA/LATAM & China)
         */
        Adreno630 = "Adreno (TM) 630",
        /**
         * Used in -
         * Samsung Galaxy S10
         */
        Adreno640 = "Adreno (TM) 640"
    };

    /** Curated list of really low performance GPUs */
    export const superLowPerformanceDevices: string[] =
    [
        QualcommGPUs.Adreno306,
        QualcommGPUs.Adreno320,
        QualcommGPUs.Adreno330,
        AppleGPUs.Generic,
        AppleGPUs.A4,
        AppleGPUs.A5,
        AppleGPUs.A5X,
        AppleGPUs.A6,
        AppleGPUs.A6X,
        AppleGPUs.A7,
        AppleGPUs.PowerVR535,
        AppleGPUs.PowerVR543,
        AppleGPUs.PowerVR554,
    ];

    /**
     * Curated list of low performance GPUs
     */
    export const defaultLowPerformanceGPUs: string[] =
    [
        AppleGPUs.A8,
        AppleGPUs.A8X,
        QualcommGPUs.Adreno405,
    ];

    /**
     * Curated list of medium performance GPUs
     */
    export const defaultMediumPerformanceGPUs: string[] =
    [
        AppleGPUs.A9,
        AppleGPUs.A9X,
        AppleGPUs.A10,
        AppleGPUs.A10X,
        ARMGPUs.MaliT760,
        ARMGPUs.MaliT880,
        QualcommGPUs.Adreno530
    ];

    /**
     * Curated list of high performance GPUs
     */
    export const defaultHighPerformanceGPUs: string[] =
    [
        AppleGPUs.A11,
        AppleGPUs.A12,
        ARMGPUs.MaliG71,
        ARMGPUs.MaliG72,
        ARMGPUs.MaliG76,
        QualcommGPUs.Adreno540,
        QualcommGPUs.Adreno630,
        QualcommGPUs.Adreno640
    ];

    /** Current Performance Level */
    let performanceLevel: Level = null;

    /**
     * The name given to all GPUs on iOS12.2+
     */
    const defaultIOS12_2GPUName: string = AppleGPUs.Generic;

    /**
     * Default performance levels of different groups of iPhones for when we're uncertain of the exact model.
     */

    /** Devices that run iOS 13+ should be good enough to give good performance. */
    const uncertainAppleDeviceIOS13_plus: Level = Level.High;
    /** Devices that are from the 10 range and above should be good enough to give good performance. */
    const uncertainAppleDeviceModel10_and_above: Level = Level.High;
    /** Devices that are anywhere from and between the 5 to 8 range can have very varying performance
    they could have 1gb Ram and crash on load, or 2gb and run okay.
    therefore set to uncertain and allow devs to decide. */
    const uncertainAppleDeviceModel5_to_8: Level = Level.Uncertain;
    /** Devices that are from the 4 range and below are not supported and have poor performance. */
    const uncertainAppleDeviceModel4S_and_below: Level = Level.Poor;

    /**
    * Returns the performance of the device. Covers all Apple devices and all other
    * devices specified on the supported devices list as of 2nd September 2019.
    * If the device is using a canvas renderer then we will assume it has bad
    * performance since IE and older devices such as the iPhone 5s mainly use it.
    * Otherwise it will return a value based off its location in a curated list of
    * device GPUs, or if it is not present in one of those lists then it will default
    * to high performance for desktop, and medium for mobile devices.  This is a
    * generalisation, but for the most part will give a good representation of how
    * the device can perform, particularly for Apple and officially supported devices.
    *
    * @returns Performance level of the device.
    */
    export function getDevicePerformance(): Level
    {
        // Set Performance Level
        if (performanceLevel === null)
        {
            const forcedLevel = URL.getParameterAsEnumVal(Level, "forceperflevel");
            if (forcedLevel != null)
            {
                performanceLevel = forcedLevel;
            }
            else
            {
                const deviceRenderer = RS.Capabilities.WebGL.getRenderer();

                // Find performance value based upon which group the GPU is in
                const level = [superLowPerformanceDevices, defaultLowPerformanceGPUs, defaultMediumPerformanceGPUs, defaultHighPerformanceGPUs]
                    .map((value, index, array) => getLevel(value, index, array, deviceRenderer));
                if (Math.max(...level) > -1)
                {
                    performanceLevel = Math.max(...level)
                }
                else if (deviceRenderer === DeviceRenderer.Canvas)
                {
                    performanceLevel = Device.isAppleDevice ? Level.Poor : Level.Low;
                }
                else
                {
                    performanceLevel = Device.isDesktopDevice ? Level.High : Level.Medium;
                }

                // Check if the gpu is the Apple Generic GPU that is always reported in iOS 12.2+
                // If it's generic GPU, it's either really old, or post iOS 12.2
                if (deviceRenderer === defaultIOS12_2GPUName)
                {
                    const operatingSystemVersion: string[] = RS.Device.operatingSystem.version.split(".");
                    // If running iOS 13 it's a decent device and should manage high settings
                    if (parseInt(operatingSystemVersion[0]) >= 13)
                    {
                        performanceLevel = uncertainAppleDeviceIOS13_plus;
                    }
                    // If running iOS 12.2 -> 13 it's potentially anything between
                    else if ((operatingSystemVersion[0] == "12") && (parseInt(operatingSystemVersion[1]) >= 2))
                    {
                        // Check if the screen resolution gives any clues
                        performanceLevel = getIphoneGroupFromResolution();
                    }

                    // If we get to here without triggering either of the above if statements, device is running < iOS 12.2
                    // and has Apple Generic GPU it's ancient and probably haunted. Therefore the performance value calculated
                    // earlier based upon which group the GPU is in is accurate.
                }

                // Check if the gpu is the Apple Generic GPU that is always reported in iOS 12.2+
                // If it's generic GPU, it's either really old, or post iOS 12.2
                if (deviceRenderer === defaultIOS12_2GPUName)
                {
                    const operatingSystemVersion: string[] = RS.Device.operatingSystem.version.split(".");
                    // If running iOS 13 it's a decent device and should manage high settings
                    if (parseInt(operatingSystemVersion[0]) >= 13)
                    {
                        performanceLevel = uncertainAppleDeviceIOS13_plus;
                    }
                    // If running iOS 12.2 -> 13 it's potentially anything between
                    else if ((operatingSystemVersion[0] == "12") && (parseInt(operatingSystemVersion[1]) >= 2))
                    {
                        // Check if the screen resolution gives any clues
                        performanceLevel = getIphoneGroupFromResolution();
                    }

                    // If we reach this point, the device is running < iOS 12.2 and has Apple Generic GPU so it's ancient and probably haunted
                    // therefore, don't take any special action, exit this IF and proceed to return performance value based upon which group the GPU is in, like normal
                }
            }

            RS.Log.debug("Performance - Renderer: " + RS.Capabilities.WebGL.getRenderer());
            RS.Log.debug("Performance - Device Level: " + Level[performanceLevel]);
        }

        // Else return set value
        return performanceLevel;
    }

    function getLevel(listOfDevices: string[], index: number, array: string[][], device: string): number
    {
        if (listOfDevices.indexOf(device) > -1)
        {
            return index;
        }
        else
        {
            return -1;
        }
    }

    export function isPoor(): boolean
    {
        return getDevicePerformance() === Level.Poor;
    }

    export function isLow(): boolean
    {
        return getDevicePerformance() === Level.Low;
    }

    export function isMedium(): boolean
    {
        return getDevicePerformance() === Level.Medium;
    }

    export function isHigh(): boolean
    {
        return getDevicePerformance() === Level.High;
    }

    /**
     * A function to decide the device performance based upon the aspect ratio and devicePixelRatio.
     * Although we can't narrow it down to specific devices, we can calculate a rough range.
     */
    function getIphoneGroupFromResolution(): number
    {
        const aspectRatio: number = (Math.max(window.screen.height, window.screen.width) / Math.min(window.screen.height, window.screen.width));
        const devicePixelRatio: number = window.devicePixelRatio;

        // Calculate rough phone range from the devicePixelRatio
        if (((aspectRatio == 736 / 414 || aspectRatio == 667 / 375) && devicePixelRatio == 3) ||
            ((aspectRatio == 1.775 || aspectRatio == 667 / 375) && devicePixelRatio == 2))
        {
            // Device is anything from iPhone 5 line to iPhone 8 line, all screen sizes included.
            // Return uncertain to allow the client dev to decide what to do.
            return uncertainAppleDeviceModel5_to_8;
        }
        else if ((aspectRatio == 1.5 && devicePixelRatio == 2) || devicePixelRatio == 1)
        {
            // Device is anything from iPhone 1 line to iPhone 4S line, all screen sizes included. Not supported
            return uncertainAppleDeviceModel4S_and_below;
        }
        else
        {
            // Device is a higher tier (>8) iPhone
            return uncertainAppleDeviceModel10_and_above;
        }
    }

    /**
     * Benchmarks the given function, returning the average time the function took to run.
     * @param func
     * @param size Sample size; number of times func is called.
     */
    export function benchmark(func: () => void, size = 1000000): number
    {
        const start = Date.now();
        for (let i = 0; i < size; i++)
        {
            func();
        }
        const time = Date.now() - start;
        return time / size;
    }
}