namespace RS.Logging
{
    /**
     * Defines the available logging levels.
     */
    export enum LogLevel
    {
        /** Used to show debug information. */
        Debug,

        /** Used to show general information. */
        Info,

        /** Used to show a warning. */
        Warn,

        /** Used to show an error. */
        Error,

        /** Used to show a fatal error. */
        Fatal,

        /** Use with setLogLevel to turn logging off altogether. */
        None
    }

    export namespace LogLevel
    {
        export let Default: LogLevel = LogLevel.Info;
        export let DefaultMinimum: LogLevel = LogLevel.Info;
    }
}