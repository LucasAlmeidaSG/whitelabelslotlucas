/// <reference path="LogLevel.ts" />
/// <reference path="../Strategy.ts" />

namespace RS.Logging
{
    export interface ILogger
    {
        /**
         * Logs a message or error with the specified log level.
         *
         * Automatically appends the stack trace unless excludeStackTrace is true.
         */
        log(message: string | Error, logLevel?: LogLevel.Error | LogLevel.Warn, excludeStackTrace?: boolean): void;
        /** Logs a message or error with the specified log level. */
        log(message: string | Error, logLevel?: LogLevel): void;

        /** Gets a contextual logger. */
        getContext(context: string): IContextualLogger & ILogger.IExtensions;
    }

    export interface IContextualLogger extends ILogger
    {
        /** Whether or not to show stack trace on all messages. Defaults to false. */
        forceStackTrace: boolean;
        /** Minimum log level for this logger. */
        logLevel: LogLevel;
        /** All currently queued messages. */
        readonly queuedMessages: ReadonlyArray<string>;
        /** If true, will print all queued messages as they are queued. Defaults to false. */
        verbose: boolean;
        /** Adds a message to the queue. Use to stay silent whilst maintaining a log record in case something goes wrong. */
        queue(...messages: any[]): void;
        /** Drops all queued messages. */
        drop(): void;
        /** Logs all queued messages to the console. */
        dump(logLevel?: LogLevel): void;
    }

    export namespace ILogger
    {
        export interface IExtensions
        {
            warn(this: ILogger, message: string): void;
            info(this: ILogger, message: string): void;
            debug(this: ILogger, message: string): void;
            error(this: ILogger, error: Error): void;
            error(this: ILogger, message: string, nestedError?: any): void;
        }
    }

    const extensions: ILogger.IExtensions =
    {
        /** Short-hand for log(message, LogLevel.Warn). */
        warn: function (this: ILogger, message: string)
        {
            this.log(message, LogLevel.Warn);
        },

        /** Short-hand for log(message, LogLevel.Info). */
        info: function (this: ILogger, message: string)
        {
            this.log(message, LogLevel.Info);
        },

        /** Short-hand for log(message, LogLevel.Debug). */
        debug: function (this: ILogger, message: string)
        {
            this.log(message, LogLevel.Debug);
        },

        /** Short-hand for log(message, LogLevel.Error). */
        error: function (this: ILogger, p2: string | Error, nestedError?: any)
        {
            if (nestedError != null)
            {
                // Get stack trace
                const stack = nestedError && Is.string(nestedError.stack) ? nestedError.stack.replace(/(.*)\n/, "") : Util.getStackTrace(1);
                const stackLines = stack ? `\n${stack}` : stack;

                // Get nested message
                const nestedMessage = Util.getErrorMessage(nestedError);
                const message = nestedMessage ? `${p2}: ${nestedMessage}${stackLines}` : `${p2}${stackLines}`;

                this.log(message, LogLevel.Error, true);
            }
            else
            {
                // Get stack trace (excluding this call)
                const stack = Util.getStackTrace(1);
                const stackLines = stack ? `\n${stack}` : stack;

                this.log(`${p2}${stackLines}`, LogLevel.Error, true);
            }
        }
    }

    export const ILogger = Strategy.declare<ILogger, ILogger.IExtensions>(Strategy.Type.Singleton, undefined, true, extensions);
    export const IContextualLogger = Strategy.declare<IContextualLogger, ILogger.IExtensions>(Strategy.Type.Instance, undefined, true, extensions);

    export function getContext(name: string, ...names: string[]): IContextualLogger & ILogger.IExtensions;
    export function getContext(...names: string[]): IContextualLogger & ILogger.IExtensions
    {
        let context: ILogger | IContextualLogger = ILogger.get();
        for (const name of names)
        {
            context = context.getContext(name);
            if (!context) { return null; }
        }
        return context as IContextualLogger & ILogger.IExtensions;
    }
}

namespace RS
{
    export const ILogger = Logging.ILogger;
}