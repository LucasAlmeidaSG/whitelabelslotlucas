/// <reference path="ILogger.ts" />

/**
 * Easy-access methods for logging.
 */
namespace RS.Log
{
    /**
     * Logs a debug message.
     * @param message
     */
    export function debug(message: string)
    {
        Logging.ILogger.get().debug(message);
    }

    /**
     * Logs an informative message.
     * @param message
     */
    export function info(message: string)
    {
        Logging.ILogger.get().info(message);
    }

    /**
     * Logs an warning message.
     * @param message
     */
    export function warn(message: string)
    {
        Logging.ILogger.get().warn(message);
    }

    /**
     * Logs an error.
     * @param error
     */
    export function error(error: Error): void;

    /**
     * Logs an error message.
     * @param message
     */
    export function error(message: string, error?: any): void;

    export function error(p1: string | Error, p2?: any)
    {
        if (Is.string(p1))
        {
            Logging.ILogger.get().error(p1, p2);
        }
        else
        {
            Logging.ILogger.get().error(p1);
        }
    }
}