namespace RS.Logging
{
    /**
     * Whether each log level should show a stack trace.
     */
    const shouldAppendStackTrace =
    {
        [LogLevel.Debug]: false,
        [LogLevel.Info]: false,
        [LogLevel.Warn]: true,
        [LogLevel.Error]: true,
        [LogLevel.Fatal]: true,
        [LogLevel.None]: false
    };

    class DefaultLogger implements ILogger, IContextualLogger
    {
        /** WARNING: queueing unimplemented in DefaultLogger. Use Core.Logging or another ILogging implementation. */
        public readonly queuedMessages: ReadonlyArray<string> = [];
        /** WARNING: queueing unimplemented in DefaultLogger. Use Core.Logging or another ILogging implementation. */
        public verbose: boolean = true;

        public logLevel: LogLevel = LogLevel.DefaultMinimum;
        public forceStackTrace: boolean = false;

        /** WARNING: contexts not supported in DefaultLogger. Use Core.Logging or another ILogging implementation. */
        public getContext(context: string)
        {
            return ILogger.get() as DefaultLogger & ILogger.IExtensions;
        }

        /** WARNING: queueing unimplemented in DefaultLogger. Use Core.Logging or another ILogging implementation. */
        public queue(...messages: any[])
        {
            const message = messages.join(" ");
            if (this.verbose) { this.log(message, LogLevel.Debug, true); }
            return;
        }

        /** WARNING: queueing unimplemented in DefaultLogger. Use Core.Logging or another ILogging implementation. */
        public drop()
        {
            return;
        }

        /** WARNING: queueing unimplemented in DefaultLogger. Use Core.Logging or another ILogging implementation. */
        public dump(logLevel?: LogLevel)
        {
            return;
        }

        /**
         * Logs the specified error.
         */
        public log(err: Error, logLevel?: LogLevel, excludeStackTrace?: boolean): void;

        /**
         * Logs the specified message.
         */
        public log(message: string, logLevel?: LogLevel, excludeStackTrace?: boolean): void;

        /**
         * Logs the specified message.
         */
        public log(obj: Error | string, logLevel?: LogLevel, excludeStackTrace?: boolean): void
        {
            let message: string;

            // Is it an error?
            if (obj instanceof Error)
            {
                if (logLevel == null) { logLevel = LogLevel.Error; }
                if (this.forceStackTrace || (shouldAppendStackTrace[logLevel] && !excludeStackTrace))
                {
                    message = `${obj.message}\n${obj.stack || "No stack trace available"}`;
                }
                else
                {
                    message = obj.message;
                }
            }
            else
            {
                if (logLevel == null) { logLevel = LogLevel.Info; }
                if (this.forceStackTrace || (shouldAppendStackTrace[logLevel] && !excludeStackTrace))
                {
                    message = `${obj}\n${Util.getStackTrace()}`;
                }
                else
                {
                    message = obj;
                }
            }

            if (logLevel < this.logLevel) { return; }

            // Write to console
            switch (logLevel)
            {
                case LogLevel.Debug:
                    console.debug(message);
                    break;

                case LogLevel.Info:
                    console.log(message);
                    break;

                case LogLevel.Warn:
                    console.warn(message);
                    break;

                case LogLevel.Error:
                case LogLevel.Fatal:
                    console.error(message);
                    break;
            }
        }
    }

    ILogger.register(DefaultLogger, true);
}