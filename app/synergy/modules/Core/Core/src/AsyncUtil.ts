/// <reference path="URL.ts"/>
namespace RS.Async
{
    /**
     * Returns a promise that resolves when ALL sub-promises have resolved.
     * If one promise rejects, all will reject.
     * @param promises
     */
    export function multiple<T>(promises: PromiseLike<T>[]): PromiseLike<T[]>
    {
        return new Promise((resolve, reject) =>
        {
            if (promises.length === 0)
            {
                resolve([]);
                return;
            }
            let resolved = false;
            let pendingCount = promises.length;
            const results = new Array<T>(pendingCount);
            for (let i = 0, l = promises.length; i < l; ++i)
            {
                promises[i].then((result) =>
                {
                    if (resolved) { return; }
                    results[i] = result;
                    pendingCount--;
                    if (pendingCount === 0)
                    {
                        resolved = true;
                        resolve(results);
                    }
                }, (reason) =>
                {
                    if (resolved) { return; }
                    resolved = true;
                    reject(reason);
                });
            }
        });
    }

    /**
     * Returns a promise that never resolves.
     */
    export function never(): PromiseLike<void>
    {
        return new Promise((resolve, reject) => { return; });
    }

    // Promise debugging.
    const oldPromise = window["Promise"];
    export class DebugPromise<T> implements PromiseLike<T>
    {
        private static _logged = false;

        private readonly _promise: Promise<T>;
        private _done = false;

        private static readonly _activeInstances: DebugPromise<any>[] = [];
        public static get activeInstances(): ReadonlyArray<DebugPromise<any>> { return this._activeInstances; }

        public readonly stack: string;

        public get source()
        {
            const awaiter = Object.keys({ __awaiter: true })[0];

            const stack = this.stack.split("\n");
            for (let i = 0; i < stack.length; i++)
            {
                if (stack[i].indexOf(awaiter) === -1) { return stack[i]; }
            }
            return stack[0];
        }

        constructor(private readonly _executor: (resolve: (v?: any) => void, reject: (r?: any) => void) => void)
        {
            DebugPromise._activeInstances.push(this);
            this.stack = Util.getStackTrace();

            this._promise = new oldPromise(this.execute.bind(this));

            this.then = oldPromise.prototype.then.bind(this._promise);
            this.catch = oldPromise.prototype.catch.bind(this._promise);

            if (!DebugPromise._logged)
            {
                Log.info("[Promise] Debugging active. To see all open Promises and their stacks, call the global function rspromises().");
                DebugPromise._logged = true;
            }
        }

        public readonly then: typeof Promise.prototype.then;
        public readonly catch: typeof Promise.prototype.catch;

        private execute(resolve: (v?: any) => void, reject: (r?: any) => void)
        {
            const newResolve = (v?: any) =>
            {
                if (this._done) { return; }
                this.cleanup();
                resolve(v);
                this._done = true;
            };

            const newReject = (r?: any) =>
            {
                if (this._done) { return; }
                this.cleanup();
                reject(r);
                this._done = true;
            };

            this._executor(newResolve, newReject);
        }

        private cleanup()
        {
            Find.remove(DebugPromise._activeInstances, (p) => p === this);
        }
    }

    export namespace DebugPromise
    {
        export const { all, race, reject, resolve } = Promise;
    }

    if (URL.getParameterAsBool("promisedebug"))
    {
        const commandName = "rspromises";
        Promise = DebugPromise;
        window[commandName] = function ()
        {
            DebugPromise.activeInstances.forEach((inst) => console.log(inst.source));
            return DebugPromise.activeInstances;
        };
    }
}
