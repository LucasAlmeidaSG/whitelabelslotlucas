/// <reference path="logging/Log.ts" />

namespace RS
{
    /** Settings for the Init decorator. */
    export interface InitSettings
    {
        after?: object[];
        bindCallbacks?: boolean;
    }

    const initQueue:
    {
        func: () => void;
        target: object;
        // after: Object[];
    }[] = [];

    /**
     * Specifies that the decorated method should be called once the document is ready.
     */
    export function Init(settings?: InitSettings)
    {
        return function(target: object, key: string, descriptor: PropertyDescriptor)
        {
            if (descriptor == null)
            {
                descriptor = Object.getOwnPropertyDescriptor(target, key);
            }
            const holder = target.constructor === Function ? target : target.constructor.prototype;
            // initQueue.push({ func: <()=>void>holder[key], target: holder, after: (settings && settings.after) || [] });
            initQueue.push({
                func: () =>
                {
                    if (settings && settings.bindCallbacks !== false)
                    {
                        bindCallbacks(holder, Decorators.MemberType.Static);
                    }
                    holder[key]();
                },
                target: holder
            });
        };
    }

    /**
     * Adds a callback to be called when inits are triggered.
     * @param func 
     * @param settings 
     */
    export function addInitCallback(func: () => void, settings?: InitSettings)
    {
        initQueue.push({
            func,
            target: null
        });
    }

    /**
     * Calls all pending inits (methods marked with the @Init decorator).
     */
    export function triggerInits()
    {
        while (initQueue.length > 0)
        {
            try
            {
                const item = initQueue.shift();
                item.func.call(item.target || window);
            }
            catch (err)
            {
                Log.error(err);
            }
        }
    }

    /**
     * Gets the number of pending inits (methods marked with the @Init decorator).
     */
    export function getPendingInitCount()
    {
        return initQueue.length;
    }
}