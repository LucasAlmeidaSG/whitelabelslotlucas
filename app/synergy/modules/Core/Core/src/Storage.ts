namespace RS
{
    /**
     * Implements a local storage class, this allows the game to load and save information from the localStorage object.
     */
    export class Storage
    {
        private static _gameName: string;
        private static _version: string;

        private static _initialised = false;
        public static get initialised() { return this._initialised; }

        private static readonly _onInitialised = RS.createSimpleEvent();
        public static get onInitialised() { return this._onInitialised.public; }

        /** Default game name to init with if not initialised before trying to access a key */
        public static DefaultGameName: string;
        /** Default version to init with if not initialised before trying to access a key */
        public static DefaultVersion: string = "{!GAME_VERSION!}";
        /** Indicates if the default init was used so we don't log a warning unless we have to */
        private static _defaultInit = false;

        /**
         * Initialise the localStorage object, specify a game name to ensure we write to storage for our game.
         */
        public static init(gameName: string, version: string): void
        {
            if (!Storage._defaultInit && Storage._initialised)
            {
                Log.warn("RS.Storage initialised twice");
            }
            Storage._gameName = gameName;
            Storage._version = version;
            Storage._defaultInit = false;
            Storage._initialised = true;
            Storage._onInitialised.publish();
        }

        /**
         * Saves a value to local storage. Objects will be converted to a JSON string before being stored.
         */
        public static save<T>(key: string, value: T, core: boolean = false): void
        {
            const saveValue = JSON.stringify(value);
            Storage.try(() => localStorage.setItem(Storage.getKey(key, core), saveValue));
        }

        /**
         * Deletes a value from local storage.
         */
        public static delete(key: string, core: boolean = false): void
        {
            Storage.try(() => localStorage.removeItem(Storage.getKey(key, core)));
        }

        /**
         * Loads a value from local storage.
         */
        public static load<T>(key: string, defaultValue: T, core: boolean = false): T
        {
            const value = Storage.try(() => localStorage.getItem(Storage.getKey(key, core)));
            if (value == null)
            {
                return defaultValue;
            }
            try
            {
                return JSON.parse(value);
            }
            catch (err)
            {
                return defaultValue;
            }
        }

        private static getKey(key: string, core: boolean = false): string
        {
            if (core)
            {
                return `sgd/synergy/${key}`;
            }

            Storage.checkInit();

            return `sgd/${Storage._gameName}/${Storage._version}/${key}`;
        }

        /**
         * Checks if initialised and initialises with defaults if not
         * Throws error if no defaults
         */
        private static checkInit(): void
        {
            if (!Storage._initialised)
            {
                if (Storage.DefaultGameName == null || Storage.DefaultVersion == null)
                {
                    throw new Error("RS.Storage.getKey: Accessing Local Storage without initialising RS.Storage and without any default values set");
                }
                Storage.init(Storage.DefaultGameName, Storage.DefaultVersion);
                Storage._defaultInit = true;
            }
        }

        /**
         * Tries to perform given localStorage function, returns null if it errors out
         */
        private static try<T>(fn: () => T): T | null
        {
            try
            {
                return fn();
            }
            catch
            {
                Log.warn("Unable to access LocalStorage due to browser settings");
                return null;
            }
        }
    }
}
