namespace RS.Util
{
    export type WideString = string | string;

    /** Reduces a string object to a native string. */
    export function narrowString(str: WideString): string { return str.toString(); }

    /**
     * Returns a number that can be used to sort two strings.
     * Returns a number greater than 0 if a > b.
     * Returns a number less than 0 if a < b.
     * Returns 0 if a == b.
     */
    export function stringCompare(a: string, b: string): number
    {
        // We currently return 1, 0 or -1, but this shouldn't be relied upon in general.
        if (a === b) { return 0; }
        if (a > b) { return 1; }
        return -1;
    }

    /**
     * Gets the file name from a path.
     * @deprecated use Path.fileName instead
     * @param path     The path to parse
     * @returns        Filename found in path, or null of none was found
     */
    export function getFilenameFromPath(path: string): string
    {
        Log.warn("getFilenameFromPath is deprecated, use Path.fileName instead");
        const regex = /([\w\d_-]*)\.?[^\\\/]*$/i;
        const matches = path.match(regex);
        return matches.length > 0 ? matches[1] : null;
    }

    /** Sets the prototype of target to prototype. */
    export function setPrototypeOf(target: object, prototype: object | null)
    {
        if ((Object as any).setPrototypeOf)
        {
            (Object as any).setPrototypeOf(target, prototype)
        }
        else
        {
            (target as any).__proto__ = prototype;
        }
    }

    /**
     * Gets a file's extension from a path
     * @deprecated use Path.extension instead
     * @param path     The path to parse
     * @returns        Filename found in path, or an empty string of none was found
     */
    export function getFileExtensionFromPath(path: string): string
    {
        Log.warn("getFileExtensionFromPath is deprecated, use Path.extension instead");
        const a = path.split(".");
        if (a.length === 1 || (a[0] === "" && a.length === 2))
        {
            return "";
        }
        return a.pop();
    }

    /** Binds the given function to the given target whilst maintaining type safety. */
    export function bind<T extends Function>(fn: T, target: object): T
    {
        return fn.bind(target);
    }

    /** Invokes the given function in the given context whilst maintaining type safety. */
    export function call<TReturn>(fn: () => TReturn, ctx: object): TReturn;
    /** Invokes the given function in the given context whilst maintaining type safety. */
    export function call<TArg0, TReturn>(fn: (arg0: TArg0) => TReturn, ctx: object, arg0: TArg0): TReturn;
    /** Invokes the given function in the given context whilst maintaining type safety. */
    export function call<TArg0, TArg1, TReturn>(fn: (arg1: TArg0, arg2: TArg1) => TReturn, ctx: object, arg0: TArg0, arg1: TArg1): TReturn;
    /** Invokes the given function in the given context whilst maintaining type safety. */
    export function call<TArg0, TArg1, TArg2, TReturn>(fn: (arg1: TArg0, arg2: TArg1) => TReturn, ctx: object, arg0: TArg0, arg1: TArg1, arg2: TArg2): TReturn;
    /** Invokes the given function in the given context whilst maintaining type safety. */
    export function call<TArg0, TArg1, TArg2, TArg3, TReturn>(fn: (arg1: TArg0, arg2: TArg1) => TReturn, ctx: object, arg0: TArg0, arg1: TArg1, arg2: TArg2, arg3: TArg3): TReturn;
    /** Invokes the given function in the given context whilst maintaining type safety. */
    export function call<TArg0, TArg1, TArg2, TArg3, TArg4, TReturn>(fn: (arg1: TArg0, arg2: TArg1) => TReturn, ctx: object, arg0: TArg0, arg1: TArg1, arg2: TArg2, arg3: TArg3, arg4: TArg4): TReturn;
    export function call(fn: Function, ctx: object, ...args: any[])
    {
        return fn.apply(ctx, args);
    }

    /** Invokes the given function in the given context using an array of arguments whilst maintaining type safety to the extent possible. */
    export function apply<TArgs extends any[], TReturn>(fn: (...args: TArgs[keyof TArgs][]) => TReturn, ctx: object, args: TArgs): TReturn
    {
        return fn.apply(ctx, args);
    }

    /**
     * Tests if the code has been closure compiled.
     * @returns  True if the code has been closure compiled.
     */
    export function isClosureCompiled(): boolean
    {
        return RS.Util["isClosureCompiled"] == null;
    }

    /**
     * Gets a stack trace of the caller of this function. \
     * NOTE: cannot be relied upon across browsers - use only for debug output.
     *
     * @param startDepth optional depth offset into the stack trace
     * @returns     The stack trace
     */
    export function getStackTrace(startDepth?: number): string
    {
        const error = new Error();
        // Limit startDepth to 0, ensure valid, and offset by 3
        startDepth = Math.max((startDepth || 0), 0) + 3;

        // Get stack trace
        const str = error.stack;

        // Remove top 3 items + startDepth
        const arr = str.split("\n");
        startDepth = Math.min(arr.length, startDepth);
        for (let i = 0; i < startDepth; i++)
        {
            arr.shift();
        }

        // Return it
        return arr.join("\n");
    }

    /**
     * Converts a thrown value to a string.
     * @param err A thrown value. May be any type, including null or undefined.
     */
    export function getErrorMessage(err: any)
    {
        if (!err) { return ""; }

        // Can we just get message?
        if (Is.string(err.message))
        {
            return err.message;
        }

        // Try to strip any 'error:' prefix.
        const str = `${err}`;
        const match = str.match(/(error:)(.+)/i);
        if (match)
        {
            return match[1];
        }

        // Give up and stringify it.
        return str === "[object Object]" ? Dump.stringify(err) : str;
    }

    /** Copies the given constructor, applying a hook function. */
    export function hookClass<T extends Function>(oldCtor: T, newCtor: T): T
    {
        // Create hooked ctor clone with matching name
        const namedCtor = renameFunction<T>(newCtor, oldCtor["name"] || "Object");
        namedCtor.prototype = oldCtor.prototype;

        // Copy static properties/methods
        const oldProps = Object.getOwnPropertyNames(oldCtor);
        for (const propName of oldProps)
        {
            if (!(propName in namedCtor))
            {
                const descriptor = Object.getOwnPropertyDescriptor(oldCtor, propName);
                if (descriptor)
                {
                    Object.defineProperty(namedCtor, propName, descriptor);
                }
                else
                {
                    namedCtor[propName] = oldCtor[propName];
                }
            }
        }

        return namedCtor;
    }

    /**
     * Gets the class name of the specified instance. \
     * May be obfuscated if closure compiled. \
     */
    export function getClassName(obj: Object): string
    {
        const ctor = obj.constructor;
        return getFunctionName(ctor);
    }

    /**
     * Gets the name of the specified function. \
     * May be obfuscated if closure compiled. \
     * Returns "" for anonymous functions.
     */
    export function getFunctionName(fn: Function): string
    {
        const str = fn.toString();
        const match = str.match(/function\s+([^\(\s]*)\(/);
        return match ? match[1] : "";
    }

    /**
     * Gets the chain of classes that the specified class derives.
     */
    export function getInheritanceChain(obj: Function, includeSelf = false): Function[]
    {
        const result: Function[] = [];
        let proto = obj.prototype;
        if (includeSelf) { result.push(obj); }
        while ((proto = Object.getPrototypeOf(proto)) != null && proto !== Function.prototype)
        {
            result.push(proto.constructor);
        }
        return result;
    }

    /**
     * Gathers all properties for the specified class.
     * Considers derived properties from the prototype chain.
     * Will NOT include or traverse further than ignoreBase when considering prototype chain.
     */
    export function gatherProperties(obj: Function, ignoreBase?: Function): PropertyDescriptorMap
    {
        const map: PropertyDescriptorMap = {};
        const inheritanceChain = this.getInheritanceChain(obj);
        inheritanceChain.unshift(obj);
        for (const classObj of inheritanceChain)
        {
            if (classObj == ignoreBase) { break; }
            const propNames = Object.getOwnPropertyNames(classObj.prototype);
            for (const key of propNames)
            {
                if (map[key] == null)
                {
                    const prop = Object.getOwnPropertyDescriptor(classObj.prototype, key);
                    if (prop != null)
                    {
                        map[key] = prop;
                    }
                }
            }
        }
        return map;
    }

    /**
     * Creates a copy of function fn with the specified name.
     * @param fn
     * @param name
     */
    export function renameFunction<T extends Function>(fn: T, name: string): T
    {
        return (new Function(`return function (call) { return function ${name}() { return call(this, arguments) }; };`)())(Function.apply.bind(fn));
    }

    /**
     * Assign all properties of "from" to "to".
     * Performs deep assignment.
     * Returns back "to".
     * @param from
     * @param to
     * @param deep
     */
    export function assign<T extends object>(from: PartialRecursive<T>, to: T, deep: true): T;

    /**
     * Assign all properties of "from" to "to".
     * Performs shallow assignment.
     * Returns back "to".
     * @param from
     * @param to
     * @param deep
     */
    export function assign<T extends object>(from: Partial<T>, to: T, deep: false): T;

    export function assign(from: object, to: object, deep: boolean): object
    {
        for (const key in from)
        {
            const val = from[key];
            if (deep && Is.nonPrimitive(val))
            {
                if (Is.cloneable(val))
                {
                    // clone should handle recursive assigning of that
                    to[key] = val.clone();
                }
                else if (!Is.func(val))
                {
                    if (to[key] == null)
                    {
                        const ctor = val.constructor as new() => any;
                        to[key] = new ctor();
                    }

                    assign(val, to[key], true);
                }
                else
                {
                    // Uncloneable function
                    throw new Error(`Unclonable function. Implement a clone() function for key: ${key}.`);
                }
            }
            else
            {
                to[key] = val;
            }
        }
        return to;
    }

    /**
     * Returns a clone of "obj".
     * @param obj
     * @param deep
     */
    export function clone<T>(obj: T, deep: boolean): T
    {
        if (obj == null || Is.primitive(obj) || Is.func(obj)) { return obj; }
        let newObj;
        if (Is.cloneable(obj))
        {
            newObj = obj.clone();
            return newObj;
        }
        else if (!Is.func(obj))
        {
            const ctor = obj.constructor as new() => any;
            newObj = new ctor();
        }
        else
        {
            newObj = Object.create(Object.getPrototypeOf(obj)) as Object;
        }
        return deep ? assign(obj as any, newObj, true) : assign(obj as any, newObj as any, false);
    }

    /**
     * Returns all matches of the specified regex within a string.
     * @param regex
     * @param text
     */
    export function matchAll(regex: RegExp, text: string): RegExpMatchArray[]
    {
        const result: RegExpMatchArray[] = [];
        let match: RegExpMatchArray | null;
        if (regex.global)
        {
            while (match = regex.exec(text))
            {
                result.push(match);
            }
        }
        else
        {
            if (match = regex.exec(text))
            {
                result.push(match);
            }
        }
        return result;
    }

    interface NumberFormatSettings
    {
        prepend: string;
        append: string;
        leftDigits: number;
        rightDigits: number;
        radix: number;
        decimal: string;
    }

    const formatCache: Map<NumberFormatSettings> = {};

    function compileFormat(format: string): NumberFormatSettings
    {
        const [left, right] = format.split(".");
        let leftDigits = 0;
        for (let i = left.length - 1; i >= 0; --i)
        {
            if (left[i] === "0")
            {
                ++leftDigits;
            }
            else
            {
                break;
            }
        }
        let rightDigits = 0;
        if (right != null)
        {
            for (let i = 0; i < right.length; ++i)
            {
                if (right[i] === "0")
                {
                    ++rightDigits;
                }
                else
                {
                    break;
                }
            }
        }
        let prepend = left.substr(0, left.length - leftDigits);
        const append = right && right.substr(rightDigits) || "";
        let radix = 10;
        if (prepend.length >= 2 && prepend.substr(prepend.length - 2) === "0x")
        {
            radix = 16;
            prepend = prepend.substr(0, prepend.length - 2);
        }
        return {
            prepend, append,
            leftDigits, rightDigits,
            radix,
            decimal: "."
        };
    }

    const tmpStrArr: string[] = [];

    /**
     * Repeats a string the specified number of times.
     * @param str
     * @param reps
     */
    export function repeat(str: string, reps: number): string
    {
        if (reps <= 0) { return ""; }
        tmpStrArr.length = reps;
        for (let i = reps - 1; i >= 0; --i)
        {
            tmpStrArr[i] = str;
        }
        const result = tmpStrArr.join("");
        return result;
    }

    /**
     * Pads a string by prepending characters until the length of the whole string matches the desired length.
     * @param str
     * @param char
     * @param length
     */
    export function padLeft(str: string, char: string, length: number): string
    {
        return `${repeat(char, (length - str.length) / char.length)}${str}`;
    }

    /**
     * Pads a string by appending characters until the length of the whole string matches the desired length.
     * @param str
     * @param char
     * @param length
     */
    export function padRight(str: string, char: string, length: number): string
    {
        return `${str}${repeat(char, (length - str.length) / char.length)}`;
    }

    /**
     * Formats a numeric value according to the specified format string.
     * A format string should use '0' to indicate digit and '.' to indicate decimal place.
     *
     * For example, the number 12.34 would format as:
     * - "00.0" => "12.3"
     * - "000.000" => "012.340"
     * - "0" => "12"
     *
     * Special formats are as follows:
     * - "N" => natural format
     * - "0x..." => base 16
     *
     * @param value
     * @param format
     */
    export function formatNumber(value: number, format: string): string
    {
        if (format === "N") { return Number.prototype.toString.call(value); }
        const settings = formatCache[format] || (formatCache[format] = compileFormat(format));
        const whole = value | 0;
        let wholeStr: string;
        if (whole < 0)
        {
            wholeStr = `-${padLeft(Math.abs(whole).toString(settings.radix), "0", settings.leftDigits)}`;
        }
        else
        {
            wholeStr = padLeft(whole.toString(settings.radix), "0", settings.leftDigits);
        }

        if (settings.rightDigits === 0)
        {
            tmpStrArr.length = 3;
            tmpStrArr[0] = settings.prepend;
            tmpStrArr[1] = wholeStr;
            tmpStrArr[2] = settings.append;
        }
        else
        {
            const decimal = Math.abs(value - whole);
            let decimalStr: string;
            if (settings.radix === 10)
            {
                decimalStr = Math.round(decimal * 10 ** settings.rightDigits).toString();
            }
            else
            {
                decimalStr = decimal.toString(settings.radix).substr(2);
            }
            decimalStr = padRight(decimalStr, "0", settings.rightDigits);

            tmpStrArr.length = 5;
            tmpStrArr[0] = settings.prepend;
            tmpStrArr[1] = wholeStr;
            tmpStrArr[2] = settings.decimal;
            if (decimalStr.length > settings.rightDigits)
            {
                tmpStrArr[3] = decimalStr.substr(0, settings.rightDigits);
            }
            else
            {
                tmpStrArr[3] = decimalStr;
            }
            tmpStrArr[4] = settings.append;
        }

        return tmpStrArr.join("");
    }

    /**
     * Gets a transposed version of the specified 2D array.
     * The array may be jagged but this will result in some undefined entries.
     * Does not modify the array - instead returns a new one.
     * @param array
     */
    export function transpose2DArray<T>(array: ReadonlyArray<ReadonlyArray<T>>): T[][]
    {
        const newArray: T[][] = [];
        for (let i = 0, iL = array.length; i < iL; ++i)
        {
            const inner = array[i];
            for (let j = 0, jL = inner.length; j < jL; ++j)
            {
                const value = inner[j];
                if (newArray[j] == null) { newArray[j] = []; }
                newArray[j][i] = value;
            }
        }
        return newArray;
    }

    /**
     * Performs a binary search through an array for an item, returning the index of it if found, or -1 if not.
     * The array MUST be sorted.
     * @param array the array to search
     * @param item the item to search for
     * @param comparer a comparer function that returns 1 if a > b, -1 if a < b, and 0 if a == b
     * @param exactMatch instead of returning -1 if not found, returns the closest index to a matching item
     */
    export function binarySearch<T>(array: ArrayLike<T>, item: T, comparer: (a: T, b: T) => number, exactMatch: boolean = true): number
    {
        if (array.length === 0) { return exactMatch ? -1 : 0; }

        let mid: number;
        let first = 0, last = array.length - 1;

        while (first <= last)
        {
            mid = (first + last) >> 1;
            const compare = comparer(array[mid], item);

            if (compare === 0)
            {
                return mid;
            }
            else if (compare > 0)
            {
                last = mid - 1;
            }
            else
            {
                first = mid + 1;
            }
        }

        if (exactMatch) { return -1; }

        if (first >= array.length || comparer(array[first], item) > 0)
        {
            return first;
        }
        else
        {
            return first + 1;
        }
    }

    /**
     * Searches a given array for a given object using a comparator function and returns
     * the index of the first match, or -1 if not found.
     *
     * @param search object to search for
     * @param array array to search
     * @param comparator function to test candidates
     */
    export function indexOf<T, U = T>(search: U, array: ArrayLike<T>, comparator: (candidate: T, search: U) => boolean): number;
    export function indexOf<T>(search: T, array: ArrayLike<T>, comparator?: (candidate: T, search: T) => boolean): number;
    export function indexOf<T>(search: any, array: ArrayLike<T>, comparator: (candidate: T, search: any) => boolean = (x, y) => x === y): number
    {
        for (let i = 0; i < array.length; i++)
        {
            if (comparator(array[i], search)) { return i; }
        }
        return -1;
    }

    /**
     * Searches a given array for a given object using a comparator function and returns
     * the index of the last match, or -1 if not found.
     *
     * @param search object to search for
     * @param array array to search
     * @param comparator function to test candidates
     */
    export function lastIndexOf<T, U = T>(search: U, array: ArrayLike<T>, comparator: (candidate: T, search: U) => boolean): number
    export function lastIndexOf<T>(search: T, array: ArrayLike<T>, comparator?: (candidate: T, search: T) => boolean): number
    export function lastIndexOf<T>(search: any, array: ArrayLike<T>, comparator: (candidate: T, search: any) => boolean = (x, y) => x === y): number
    {
        for (let i = array.length - 1; i >= 0; i--)
        {
            if (comparator(array[i], search)) { return i; }
        }

        return -1;
    }

    function defaultComparatorFunc<T>(candidate: T, item: T): number
    {
        return item < candidate ? -1 : 0;
    }

    /**
     * Performs a sorted insertion into an array using a comparator function and returns
     * the index at which the item was placed. If the comparator never returns true, the
     * item will be inserted at the end of the array.
     *
     * @param item object to insert
     * @param array array to insert into
     * @param comparator function to test candidates, returns 0 not to insert or a number to insert x places to the left/right
     */
    export function insert<T>(item: T, array: T[], comparator: (candidate: T, item: T, index: number) => number = defaultComparatorFunc): number
    {
        for (let i = 0; i < array.length; i++)
        {
            const comp = comparator(array[i], item, i);
            if (comp)
            {
                const insertAt = comp > 0 ? i + comp : i + comp + 1;
                array.splice(insertAt, 0, item);
                return insertAt;
            }
        }

        return array.push(item) - 1;
    }

    /**
     * Compares two arrays for equality.
     * @param arr1 the first array for comparison
     * @param arr2 the second array for comparison
     * @param comparer function to compare individual array items
     */
    export function arraysEqual<T>(arr1: ArrayLike<T>, arr2: ArrayLike<T>, comparer: (a: T, b: T) => boolean = (a, b) => a === b): boolean
    {
        if (arr1.length !== arr2.length) { return false; }
        for (let i = 0, l = arr1.length; i < l; ++i)
        {
            if (!comparer(arr1[i], arr2[i])) { return false; }
        }
        return true;
    }

    /**
     * Populates the specified keyMap with true or false if each key within it is found in obj.
     * @param obj
     * @param keyMap
     */
    export function hasOwnKeys<TKeyMap extends { [key: string]: any; }>(obj: object, keyMap: { [P in keyof TKeyMap]: boolean }): obj is TKeyMap;

    export function hasOwnKeys(obj: object, keyMap: { [key: string]: boolean | null; }): boolean
    {
        const ownKeys = Object.getOwnPropertyNames(obj);
        let fullMatch = true;
        for (const key in keyMap)
        {
            const match = keyMap[key] = ownKeys.indexOf(key) !== -1;
            fullMatch = fullMatch && match;
        }
        return fullMatch;
    }

    /** Mirrors one or more properties of one object to another. */
    export function mirror<T extends object, TProps extends Partial<{ [P in keyof T]: boolean }>>(master: T, slave: Partial<T>, props?: TProps): void;
    export function mirror<T extends object, TProp extends keyof T>(master: T, slave: Partial<T>, key: TProp): void;
    export function mirror<T extends object, TProps extends Partial<{ [P in keyof T]: boolean }>, TProp extends keyof T>(master: T, slave: Partial<T>, p3?: TProp | TProps): void
    {
        if (RS.Is.string(p3))
        {
            const key = p3 as string;
            const oldDescriptor = Object.getOwnPropertyDescriptor(master, key);

            /** Encloses the old value. */
            let outerValue: T[TProp] = master[key];
            Object.defineProperty(master, key,
            {
                // Use the old getter. If there was no getter but a setter, don't create getter. Otherwise create custom getter.
                get: (!oldDescriptor || (!oldDescriptor.get && !oldDescriptor.set)) ? function (this) { return outerValue; } : oldDescriptor.get,
                set: function (this, value)
                {
                    if (oldDescriptor && oldDescriptor.set)
                    {
                        oldDescriptor.set.call(this, value);
                    }
                    else if (!oldDescriptor || !oldDescriptor.get)
                    {
                        // Only set outerValue if we created a custom getter.
                        outerValue = value;
                    }

                    // Finally, update slave.
                    slave[key] = master[key];
                },
                configurable: true,
                enumerable: true
            });
        }
        else if (RS.Is.object(p3))
        {
            const props = p3;
            const keys = Object.keys(props) as (keyof T)[];
            for (const key of keys)
            {
                mirror(master, slave, key);
            }
        }
        else
        {
            // Use the keys common to both master and slave.
            for (const slaveKey in slave)
            {
                for (const masterKey in master)
                {
                    if (masterKey === slaveKey) { mirror(master, slave, masterKey); }
                }
            }
        }
    }

    /** Returns the property descriptor of the given key. */
    export function getPropertyDescriptor
        <T extends object,
        TProp extends Partial<{ [P in keyof T]: any }>,
        TKey extends keyof T & keyof TProp,
        TVal extends T[TKey]>
        (object: T, property: TProp)
    {
        const key = Object.keys(property)[0];
        do
        {
            const descriptor = Object.getOwnPropertyDescriptor(object, key);
            if (descriptor) { return descriptor; }
        }
        while (object = Object.getPrototypeOf(object));
    }

    /** Invokes a given callback when a given property is changed. NOTE: not reversible. */
    export function observe
        <T extends object,
        TProp extends Partial<{ [P in keyof T]: any }>,
        TKey extends keyof T & keyof TProp,
        TVal extends T[TKey]>
        (object: T, property: TProp, callback: (value: TVal) => void)
    {
        const key = Object.keys(property)[0];
        const oldDescriptor = getPropertyDescriptor(object, property);

        let lastValue: TVal;
        let newDescriptor: PropertyDescriptor;
        if (oldDescriptor && Object.keys(oldDescriptor).indexOf("value") === -1)
        {
            newDescriptor =
            {
                ...oldDescriptor,
                set: function (this: T, value: TVal)
                {
                    oldDescriptor.set.call(this, value);

                    if (lastValue !== value) { callback(value); }
                    lastValue = value;
                }
            }
        }
        else
        {
            let innerValue: TVal = object[key];
            newDescriptor =
            {
                get: function (this) { return innerValue; },
                set: function (this, value: TVal)
                {
                    innerValue = value;

                    if (lastValue !== value) { callback(value); }
                    lastValue = value;
                },
                configurable: true,
                enumerable: true
            }
        }

        Object.defineProperty(object, key, newDescriptor);
    }

    /** Encodes a character code as a UTF-8 character sequence. */
    export function encodeUTF8(code: number): string
    {
        const first = (code >> 16);
        const second = (code >> 12) & 0x3f;
        const third = (code >> 6) & 0x3f;
        const forth = (code & 0x3f) + 0x80;

        // Check for 4 character codes these are formatted 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        if (code >= 0x10000)
        {
            return String.fromCharCode(first + 0xf0, second + 0x80, third + 0x80, forth);
        }
        // Otherwise over 0x07ff are formatted 1110xxxx 10xxxxxx 10xxxxxx
        else if (code >= 0x0800)
        {
            return String.fromCharCode(second + 0xe0, third + 0x80, forth);
        }
        // Otherwise over 0x80 are formatted 110xxxxx 10xxxxxx
        else if (code >= 0x80)
        {
            return String.fromCharCode(third + 0xc0, forth);
        }
        // Otherwise they're just normal ASCII characters
        else
        {
            return String.fromCharCode(code);
        }
    }

    /** Converts a unicode encoded string into a UTF-8 encoded string */
    export function encodeUTF8String(value: string): string
    {
        return value.split("").map((ch) => encodeUTF8(ch.charCodeAt(0))).join("");
    }
}
