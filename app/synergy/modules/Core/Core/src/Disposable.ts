/// <reference path="Destroy.ts" />
/// <reference path="decorators/Tag.ts" />
/// <reference path="Is.ts" />

namespace RS
{
    /**
     * Encapsulates an object that can be disposed, releasing any sway it has over state external to itself.
     */
    export interface IDisposable
    {
        /** @internal */
        __boundDisposables?: IDisposable[];

        readonly isDisposed: boolean;
        dispose(): void;
    }

    export namespace Is
    {
        /**
         * Gets if the specified value implements IDisposable.
         * @param value
         */
        export function disposable(value: any): value is IDisposable
        {
            return (Is.object(value) || Is.func(value)) && Is.func(value.dispose) && Is.boolean(value.isDisposed);
        }
    }

    function disposer(obj: any): void
    {
        if (!Is.disposable(obj)) { return; }
        if (!obj.isDisposed) { obj.dispose(); }
    }
    addDestroyHandler(disposer);

    /**
     * Specifies that the specified property or method should cause an error if set/get/called when the instance is disposed.
     * @param target
     * @param propertyKey
     */
    export function CheckDisposed(target: any, propertyKey: string, propertyDescriptor?: PropertyDescriptor)
    {
        if (target.dispose == null)
        {
            Log.warn("CheckDisposed applied to a property or method within a class that doesn't implement IDisposable");
            return;
        }
        if (propertyDescriptor != null)
        {
            if (propertyDescriptor.get != null)
            {
                const oldGet = propertyDescriptor.get;
                propertyDescriptor.get = function (this: IDisposable)
                {
                    if (this.isDisposed) { throw new Error(`Attempt to get property '${propertyKey}' when instance was disposed`); }
                    return oldGet.call(this);
                };
            }
            if (propertyDescriptor.set != null)
            {
                const oldSet = propertyDescriptor.set;
                propertyDescriptor.set = function (this: IDisposable, value: any)
                {
                    if (this.isDisposed) { throw new Error(`Attempt to set property '${propertyKey}' when instance was disposed`); }
                    oldSet.call(this, value);
                };
            }
            if (propertyDescriptor.value != null && Is.func(propertyDescriptor.value))
            {
                const oldFunc = propertyDescriptor.value as Function;
                propertyDescriptor.value = function (this: IDisposable)
                {
                    if (this.isDisposed) { throw new Error(`Attempt to call method '${propertyKey}' when instance was disposed`); }
                    return oldFunc.apply(this, arguments);
                };
            }
            Object.defineProperty(target, propertyKey, propertyDescriptor);
        }
        else
        {
            Log.warn("CheckDisposed applied to an invalid property");
        }
    }

    export namespace Disposable
    {
        const key = Object.keys({ __boundDisposables: true })[0];

        /** Binds 'child' to 'parent', such that when parent is disposed, child is also disposed. */
        export function bind(child: IDisposable, parent: IDisposable): void
        {
            if (child == null)
            {
                Log.warn("Cannot dispose null");
                return;
            }

            if (parent.isDisposed)
            {
                // Immediately dispose child.
                child.dispose();
                return;
            }

            if (parent.__boundDisposables)
            {
                if (parent.__boundDisposables.indexOf(child) !== -1) { return; }
                parent.__boundDisposables.push(child);
            }
            else
            {
                parent.__boundDisposables = [child];

                if (Is.func(parent))
                {
                    // Events etc. We need to capture its dispose and apply it directly to the instance
                    const oldDispose = parent.dispose;
                    parent.dispose = function (this: IDisposable)
                    {
                        if (this.isDisposed) { return; }
                        oldDispose.call(this);

                        const val = this.__boundDisposables;
                        for (let i = 0, l = val.length; i < l; ++i)
                        {
                            const item = val[i];
                            item.dispose();
                            val[i] = null;
                        }
                        val.length = 0;
                        this[key] = null;
                    };
                }
                else
                {
                    if (parent == null) { throw new Error("Cannot bind disposable to null"); }

                    // Apply AutoDispose
                    const decoratedProps = AutoDispose.get(parent, Decorators.MemberType.Instance);
                    if (decoratedProps.indexOf(key) === -1)
                    {
                        const target = parent.constructor.prototype;
                        AutoDispose(target, key);
                    }
                }
            }
        }
    }

    function doAutoDispose(cl: Function): void
    {
        const oldDispose: Function = cl.prototype.dispose;
        cl.prototype.dispose = function (this: IDisposable)
        {
            if (!this.isDisposed)
            {
                oldDispose.call(this);
                for (const key of AutoDispose.get(this, Decorators.MemberType.Instance))
                {
                    const val = this[key];
                    if (Is.disposable(val))
                    {
                        val.dispose();
                        this[key] = null;
                    }
                    if (Is.array(val))
                    {
                        for (let i = 0, l = val.length; i < l; ++i)
                        {
                            const item = val[i];
                            if (Is.disposable(item))
                            {
                                item.dispose();
                                val[i] = null;
                            }
                        }
                        val.length = 0;
                        this[key] = null;
                    }
                }
            }
        };
    }

    /**
     * Specifies that the value of the property should be automatically disposed when the owner instance is disposed.
     * For flow elements, will call dispose on all children when disposed.
     * For Arrays, will call dispose on all array elements.
     */
    export const AutoDispose = Decorators.Tag.createDataless(Decorators.TagKind.Property);
    AutoDispose.onAppliedInitial(doAutoDispose);

    const getAutoDispose = AutoDispose.get;
    AutoDispose.get = function (obj: object, type: Decorators.MemberType)
    {
        const autoDispose = getAutoDispose(obj, type);
        const autoDisposeOnSet = AutoDisposeOnSet.get(obj, type);
        return autoDispose.concat(autoDisposeOnSet);
    };

    /**
     * Specifies that the value of the property should be automatically disposed when the owner instance is disposed or object reference is changed.
     * For flow elements, will call dispose on all children when disposed.
     * For Arrays, will call dispose on all array elements not shared by the new array.
     */
    export const AutoDisposeOnSet = Decorators.Tag.createDataless(Decorators.TagKind.Property);
    AutoDisposeOnSet.onAppliedInitial(doAutoDispose);
    AutoDisposeOnSet.onApplied((data) =>
    {
        const propertyDescriptor = Object.getOwnPropertyDescriptor(data.target, data.key);
        if (propertyDescriptor && !("value" in propertyDescriptor))
        {
            // Don't do disposal-on-set for getter-only properties.
            if (!propertyDescriptor.set) { return; }
            // Getter/setter property.
            if (!propertyDescriptor.get)
            {
                Log.warn(`Attempted to apply AutoDispose to a property without a getter.`);
                return;
            }

            const oldSet = propertyDescriptor.set;
            propertyDescriptor.set = function (this: Object, value: OneOrMany<IDisposable>)
            {
                const current = this[data.key];
                if (current === value) { return; }
                if (Is.disposable(current) && !current.isDisposed) { current.dispose(); }

                oldSet.call(this, value);
            };

            Object.defineProperty(data.target, data.key, propertyDescriptor);
        }
        else
        {
            // Normal property, use a closure getter.
            const newPropertyDescriptor: PropertyDescriptor = { enumerable: true, configurable: true };
            newPropertyDescriptor.set = function (this: Object, newValue: OneOrMany<IDisposable>)
            {
                const currentValue = this[data.key];
                if (newValue === currentValue) { return; }

                if (Is.array(newValue))
                {
                    if (Is.disposable(currentValue) && newValue.indexOf(currentValue) === -1) { currentValue.dispose(); }
                    if (Is.array(currentValue))
                    {
                        for (const member of currentValue)
                        {
                            if (newValue.indexOf(member) !== -1) { continue; }
                            if (Is.disposable(member)) { member.dispose(); }
                        }
                    }
                }
                else
                {
                    if (Is.disposable(currentValue)) { currentValue.dispose(); }
                    if (Is.array(currentValue))
                    {
                        for (const member of currentValue)
                        {
                            if (newValue === member) { continue; }
                            if (Is.disposable(member)) { member.dispose(); }
                        }
                    }
                }

                const thisDescriptor = Object.getOwnPropertyDescriptor(data.target, data.key);
                thisDescriptor.get = function (this: Object) { return newValue; };
                Object.defineProperty(this, data.key, thisDescriptor);
            };

            Object.defineProperty(data.target, data.key, newPropertyDescriptor);
        }
    });
}