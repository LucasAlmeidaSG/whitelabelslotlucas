/// <reference path="../Event.ts" />

namespace RS.Util
{
    /**
     * Notifies when objects of a particular kind are created or disposed.
     */
    export interface IObjectTracker<T>
    {
        /**
         * Published when a new object of type T is created.
         * This will be AFTER the constructor has finished executing.
         */
        readonly onCreated: IEvent<T>;

        /**
         * Published when an object of type T is disposed.
         * This will be BEFORE the dispose method has started executing.
         */
        readonly onDisposed: IEvent<T>;
    }

    /**
     * @inheritdoc
     * This instance should only be exposed to the object class itself, the interface should be exposed publicly.
     */
    export class ObjectTracker<T>
    {
        /** @inheritdoc */
        public readonly onCreated = createEvent<T>();

        /** @inheritdoc */
        public readonly onDisposed = createEvent<T>();

        public notifyCreate(obj: T): void
        {
            this.onCreated.publish(obj);
        }

        public notifyDispose(obj: T): void
        {
            this.onDisposed.publish(obj);
        }
    }

    /**
     * Specifies that creation and disposal of any instances of the class should be tracked via the specified object tracker.
     * @param tracker
     */
    export const Tracked = Decorators.Tag.create<ObjectTracker<any>>(Decorators.TagKind.Class, (oldCtor) =>
    {
        const tracker = Tracked.get(oldCtor);
        // Create hooked ctor clone
        const newCtor = hookClass(oldCtor, function ()
        {
            bindCallbacks(this, Decorators.MemberType.Instance);
            oldCtor.apply(this, arguments);
            tracker.notifyCreate(this);
        } as any);
        // Track disposal
        const oldDispose = newCtor.prototype.dispose as IDisposable["dispose"];
        if (oldDispose)
        {
            newCtor.prototype.dispose = function dispose(this: IDisposable): void
            {
                if (this.isDisposed) { return; }
                tracker.notifyDispose(this);
                oldDispose.call(this);
            };
        }
        return newCtor;
    });
}