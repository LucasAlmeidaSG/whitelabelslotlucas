namespace RS
{
    enum Type
    {
        Enter,
        Exit,
        Transition,
    }

    interface BaseHandler<T>
    {
        handler: ObservableStateMachine.TransitionHandler<T>;
    }

    interface ExitHandler<T> extends BaseHandler<T>
    {
        type: Type.Exit;
        value: T;
    }

    interface EnterHandler<T> extends BaseHandler<T>
    {
        type: Type.Enter;
        value: T;
    }

    interface TransitionHandler<T> extends BaseHandler<T>
    {
        type: Type.Transition;
        from: T;
        to: T;
    }

    type Handler<T> = TransitionHandler<T> | ExitHandler<T> | EnterHandler<T>;

    /** Allows handling of a finite set of observable state transitions. */
    @HasCallbacks
    export class ObservableStateMachine<T>
    {
        private readonly __handlers: Handler<T>[] = [];
        private __lastValue: T;

        constructor(public readonly observable: IReadonlyObservable<T>)
        {
            this.__lastValue = observable.value;
            observable.onChanged(this.onObservableChanged);
        }

        public setStateEnterHandler(value: T, handler: ObservableStateMachine.TransitionHandler<T>)
        {
            for (let i = 0; i < this.__handlers.length; i++)
            {
                const transition = this.__handlers[i];
                if (transition.type === Type.Enter && transition.value === value)
                {
                    this.__handlers[i] = { type: Type.Enter, value, handler };
                    return;
                }
            }

            this.__handlers.push({ type: Type.Enter, value, handler });
        }

        public setStateExitHandler(value: T, handler: ObservableStateMachine.TransitionHandler<T>)
        {
            for (let i = 0; i < this.__handlers.length; i++)
            {
                const transition = this.__handlers[i];
                if (transition.type === Type.Exit && transition.value === value)
                {
                    this.__handlers[i] = { type: Type.Exit, value, handler };
                    return;
                }
            }

            this.__handlers.push({ type: Type.Exit, value, handler });
        }

        public setTransitionHandler(from: T, to: T, handler: ObservableStateMachine.TransitionHandler<T>)
        {
            if (from === to) { throw new Error(`Invalid transition: '${from}' → '${to}'`); }

            for (let i = 0; i < this.__handlers.length; i++)
            {
                const transition = this.__handlers[i];
                if (transition.type === Type.Transition && transition.from === from && transition.to === to)
                {
                    this.__handlers[i] = { type: Type.Transition, from, to, handler };
                    return;
                }
            }

            this.__handlers.push({ type: Type.Transition, from, to, handler });
        }

        @Callback
        private onObservableChanged(to: T)
        {
            const from = this.__lastValue;
            this.__lastValue = to;

            for (const transition of this.__handlers)
            {
                switch (transition.type)
                {
                    case Type.Transition:
                        if (transition.from === from && transition.to === to && transition.handler) { transition.handler(from, to); }
                        break;

                    case Type.Enter:
                        if (transition.value === to && transition.handler) { transition.handler(from, to); }
                        break;

                    case Type.Exit:
                        if (transition.value === from && transition.handler) { transition.handler(from, to); }
                        break;
                }
            }
        }
    }
    
    export namespace ObservableStateMachine
    {
        /* tslint:disable-next-line:no-shadowed-variable */
        export type TransitionHandler<T> = (from: T, to: T) => void;
    }
}