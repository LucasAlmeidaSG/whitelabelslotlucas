namespace RS.Util
{
    const tmpKeyMap = { __typeID: null };

    /**
     * Encapsulates a map of types (TypeScript classes) to values.
     */
    export class TypeMap<TKey extends Function, TValue>
    {
        /** Gets an array of all keys belonging to this map. */
        public get keys(): ReadonlyArray<TKey> { return Object.keys(this._map).map((k) => TypeMap._allTypes[k] as TKey); }

        /** Gets an array of all values belonging to this map. */
        public get values(): ReadonlyArray<TValue> { return Object.keys(this._map).map((k) => this._map[k]); }

        protected static _allTypes: Function[] = [];

        protected _map: { [index: number]: TValue } = {};

        /** Gets the value associated with the given key. */
        public get(type: TKey): TValue|null
        {
            return this._map[this.indexFromClass(type)] || null;
        }

        /** Sets the value associated with the given key. */
        public set(type: TKey, value: TValue): void
        {
            this._map[this.indexFromClass(type)] = value;
        }

        /** Removes the given key from this map. Returns true if successful. */
        public remove(type: TKey): boolean
        {
            const idx = this.indexFromClass(type);
            if (idx in this._map)
            {
                delete this._map[idx];
                return true;
            }
            else
            {
                return false;
            }
        }

        /** Gets if the specified key can be found in this map. */
        public containsKey(type: TKey): boolean
        {
            return this.indexFromClass(type) in this._map;
        }

        /** Gets if the specified value can be found in this map. */
        public containsValue(value: TValue): boolean
        {
            for (const key in this._map)
            {
                if (this._map[key] === value)
                {
                    return true;
                }
            }
            return false;
        }
        
        /** Gets the first key that maps to the given value within this map. */
        public keyOf(value: TValue): TKey|null
        {
            for (const key in this._map)
            {
                if (this._map[key] === value)
                {
                    return TypeMap._allTypes[key] as TKey;
                }
            }
            return null;
        }
        
        protected indexFromClass(obj: TKey): number
        {
            const proto = obj.prototype as { __typeID?: number; };
            Util.hasOwnKeys(proto, tmpKeyMap);
            if (!tmpKeyMap.__typeID)
            {
                TypeMap._allTypes.push(obj);
                proto.__typeID = TypeMap._allTypes.length - 1;
            }
            return proto.__typeID;
        }
    }
}