/// <reference path="IObservable.ts" />
/// <reference path="../Disposable.ts" />
/// <reference path="../decorators/Callbacks.ts" />

namespace RS
{
    /**
     * Encapsulates a single mutable value.
     */
    export class Observable<T> implements IObservable<T>
    {
        protected _value: T;
        protected _isDisposed: boolean = false;
        protected _resolves: { value: T|T[]; resolve: () => void; reject: (reason: any) => void; }[] = [];
        protected _globalResolves: { resolve: (value: T) => void; reject: (reason: any) => void; }[] = [];

        protected readonly _onChanged = createEvent<T>();
        /** Published when the state has changed. */
        public readonly onChanged = this._onChanged.public;

        public valueToString: (value?: T) => string = (value: T) => `${value}`;

        /** Gets if this observable has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /**
         * Gets or sets the value of this observable.
         */
        public get value() { return this._value; }
        public set value(newValue)
        {
            if (newValue === this._value) { return; }
            this._value = newValue;
            this._onChanged.publish(newValue);
            for (let i = 0; i < this._resolves.length; i++)
            {
                const resolveData = this._resolves[i];
                if (Is.array(resolveData.value) ? resolveData.value.indexOf(newValue) !== -1 : resolveData.value === newValue)
                {
                    resolveData.resolve();
                    this._resolves.splice(i, 1);
                    i--;
                }
            }
            for (const globalResolve of this._globalResolves)
            {
                globalResolve.resolve(newValue);
            }
            this._globalResolves.length = 0;
        }

        public constructor(initialValue: T)
        {
            this._value = initialValue;
        }

        public toString(): string
        {
            return `[Observable: ${this.valueToString(this._value)}]`;
        }

        /**
         * Attaches callbacks for the resolution and/or rejection of the Promise.
         * @param onfulfilled The callback to execute when the Promise is resolved.
         * @param onrejected The callback to execute when the Promise is rejected.
         * @returns A Promise for the completion of which ever callback is executed.
         */
        public then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): PromiseLike<TResult1 | TResult2>
        {
            return new Promise((resolve, reject) =>
            {
                this._globalResolves.push({ resolve: () =>
                    {
                        onfulfilled(this._value);
                        resolve();
                    },
                    reject: (reason) =>
                    {
                        onrejected(reason);
                        reject(reason);
                    }
                });
            });
        }

        /**
         * Returns a promise that resolves when this observable's value has changed to the specified value.
         * @param value
         */
        public for(value: T): PromiseLike<void>;

        /**
         * Returns a promise that resolves when this observable's value has changed to one of the specified values.
         * @param value
         */
        public for(value: T[]): PromiseLike<void>;

        /**
         * Returns a promise that resolves when this observable's value has changed to the specified value.
         * @param value
         */
        public for(value: T|T[]): PromiseLike<void>
        {
            return new Promise<void>((resolve, reject) =>
            {
                if (this._value === value)
                {
                    resolve();
                }
                else
                {
                    this._resolves.push({ value, resolve, reject });
                }
            });
        }

        /**
         * Disposes this observable.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }
            this._onChanged.dispose();
            for (const resolve of this._resolves)
            {
                resolve.reject("disposed");
            }
            for (const globalResolve of this._globalResolves)
            {
                globalResolve.reject("disposed");
            }
            this._isDisposed = true;
        }

        /**
         * Creates a new observable that maps to this one through a transform function.
         * @param func
         */
        public map<S>(mapFunc: (t: T) => S): IReadonlyObservable<S>
        {
            return new MappedObservable(this, mapFunc);
        }
    }

    class MappedObservable<T, S> extends Observable<S>
    {
        private _baseObservableChangeHandler: IDisposable;

        public constructor(
            public readonly baseObservable: IReadonlyObservable<T>,
            public readonly mapFunc: (t: T) => S
        )
        {
            super(mapFunc(baseObservable.value));
            this._baseObservableChangeHandler = baseObservable.onChanged(this.handleBaseObservableValueChange.bind(this));
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            if (this._baseObservableChangeHandler)
            {
                this._baseObservableChangeHandler.dispose();
                this._baseObservableChangeHandler = null;
            }
            super.dispose();
        }

        private handleBaseObservableValueChange(): void
        {
            this.value = this.mapFunc(this.baseObservable.value);
        }
    }

    class ReducedObservable<T> extends Observable<T>
    {
        private _baseObservableChangeHandlers: IDisposable[];

        public constructor(
            public readonly baseObservables: IReadonlyObservable<T>[],
            public readonly reduceFunc: (a: T, b: T) => T
        )
        {
            super(baseObservables.map((o) => o.value).reduce(reduceFunc));
            this._baseObservableChangeHandlers = [];
            for (const o of baseObservables)
            {
                this._baseObservableChangeHandlers.push(o.onChanged(this.handleBaseObservableValueChange.bind(this)));
            }
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            if (this._baseObservableChangeHandlers)
            {
                for (const handler of this._baseObservableChangeHandlers)
                {
                    handler.dispose();
                }
                this._baseObservableChangeHandlers.length = 0;
                this._baseObservableChangeHandlers = null;
            }
            super.dispose();
        }

        private handleBaseObservableValueChange(): void
        {
            this.value = this.baseObservables
                .map((o) => o.value)
                .reduce(this.reduceFunc);
        }
    }

    @HasCallbacks
    class SimplexLink<T> implements IDisposable
    {
        public get isDisposed() { return this._left == null || this._left.isDisposed || this._right == null || this._right.isDisposed; }

        @AutoDisposeOnSet private _leftChangeHandle: IDisposable | null = null;

        private _left: IReadonlyObservable<T> | null = null;
        private _right: IObservable<T> | null = null;

        private _changeInProgress: boolean = false;

        public constructor(left: IReadonlyObservable<T>, right: IObservable<T>)
        {
            this._left = left;
            this._right = right;
            this._leftChangeHandle = left.onChanged(this.handleLeftChanged);
            this._changeInProgress = true;
            try
            {
                right.value = left.value;
            }
            finally
            {
                this._changeInProgress = false;
            }
        }

        public dispose()
        {
            this._left = null;
            this._right = null;
        }

        @Callback
        private handleLeftChanged(newValue: T): void
        {
            if (this.isDisposed || this._changeInProgress) { return; }
            this._changeInProgress = true;
            try
            {
                this._right.value = newValue;
            }
            finally
            {
                this._changeInProgress = false;
            }
        }
    }

    @HasCallbacks
    class DuplexLink<T> implements IDisposable
    {
        public get isDisposed() { return this._left == null || this._left.isDisposed || this._right == null || this._right.isDisposed; }

        @AutoDisposeOnSet private _leftChangeHandle: IDisposable | null = null;
        @AutoDisposeOnSet private _rightChangeHandle: IDisposable | null = null;

        private _left: IObservable<T> | null = null;
        private _right: IObservable<T> | null = null;

        private _changeInProgress: boolean = false;

        public constructor(left: IObservable<T>, right: IObservable<T>)
        {
            this._left = left;
            this._right = right;
            this._leftChangeHandle = left.onChanged(this.handleLeftChanged);
            this._rightChangeHandle = right.onChanged(this.handleRightChanged);
            this._changeInProgress = true;
            try
            {
                right.value = left.value;
            }
            finally
            {
                this._changeInProgress = false;
            }
        }

        public dispose()
        {
            this._left = null;
            this._right = null;
        }

        @Callback
        private handleLeftChanged(newValue: T): void
        {
            if (this.isDisposed || this._changeInProgress) { return; }
            this._changeInProgress = true;
            try
            {
                this._right.value = newValue;
            }
            finally
            {
                this._changeInProgress = false;
            }
        }

        @Callback
        private handleRightChanged(newValue: T): void
        {
            if (this.isDisposed || this._changeInProgress) { return; }
            this._changeInProgress = true;
            try
            {
                this._left.value = newValue;
            }
            finally
            {
                this._changeInProgress = false;
            }
        }
    }

    export namespace Observable
    {
        /**
         * Creates a new observable that encapsulates the reduced value of an array of observables.
         * @param observables
         * @param reductionFunc
         */
        export function reduce<T>(observables: IReadonlyObservable<T>[], reductionFunc: (a: T, b: T) => T): IReadonlyObservable<T>
        {
            return new ReducedObservable(observables, reductionFunc);
        }

        /**
         * Specifies the duplicity of an observable link.
         */
        export enum LinkDirection
        {
            /** Updating the left observable will push the change to the right observable but not vice versa. */
            LeftToRight,

            /** Updating the right observable will push the change to the left observable but not vice versa. */
            RightToLeft,

            /**
             * Updating one observable will push the change to the other.
             * The left will take priority if the values are out of sync when the link is created.
             */
            Duplex
        }

        /**
         * Links two observables such that updating the left one will push the change to the right one but not the other way.
         * @param left
         * @param right
         * @param direction
         */
        export function link<T>(left: IReadonlyObservable<T>, right: IObservable<T>, direction: LinkDirection.LeftToRight): IDisposable;

        /**
         * Links two observables such that updating the right one will push the change to the left one but not the other way.
         * @param left
         * @param right
         * @param direction
         */
        export function link<T>(left: IObservable<T>, right: IReadonlyObservable<T>, direction: LinkDirection.RightToLeft): IDisposable;

        /**
         * Links two observables such that updating either one will push the change to the other.
         * @param left
         * @param right
         * @param direction
         */
        export function link<T>(left: IObservable<T>, right: IObservable<T>, direction: LinkDirection.Duplex): IDisposable;

        /**
         * Links two observables such that their values will be automatically synchronised, subject to the direction rule.
         * @param left
         * @param right
         * @param direction
         */
        export function link<T>(left: IObservable<T>, right: IObservable<T>, direction: LinkDirection): IDisposable;

        export function link<T>(left: IObservable<T>, right: IObservable<T>, direction: LinkDirection): IDisposable
        {
            switch (direction)
            {
                case LinkDirection.LeftToRight: return new SimplexLink(left, right);
                case LinkDirection.RightToLeft: return new SimplexLink(right, left);
                case LinkDirection.Duplex: return new DuplexLink(left, right);
            }
        }
    }
}
