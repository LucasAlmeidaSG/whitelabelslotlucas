namespace RS
{
    /**
     * Encapsulates a single value that is immutable via this interface but may be changed externally.
     */
    export interface IReadonlyObservable<T> extends PromiseLike<T>, IDisposable
    {
        /** Published when the value has changed. */
        readonly onChanged: IEvent<T>;

        /** Gets the current value. */
        readonly value: T;

        /** Gets or sets a function to display value as a string */
        readonly valueToString: (value: T) => string;

        /**
         * Returns a promise that resolves when this observable's value has changed to the specified value.
         * @param value
         */
        for(value: T): PromiseLike<void>;

        /**
         * Returns a promise that resolves when this observable's value has changed to one of the specified values.
         * @param value
         */
        for(value: T[]): PromiseLike<void>;

        /**
         * Creates a new observable that maps to this one through a transform function.
         * @param func
         */
        map<S>(mapFunc: (t: T) => S): IReadonlyObservable<S>;
    }

    /**
     * Encapsulates a single value that is mutable via this interface.
     */
    export interface IObservable<T> extends IReadonlyObservable<T>
    {
        /** Gets or sets the current value. */
        value: T;

        /** Gets or sets a function to display value as a string */
        valueToString: (value: T) => string;
    }
}
