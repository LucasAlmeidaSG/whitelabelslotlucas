/// <reference path="../Disposable.ts" />
/// <reference path="IObservable.ts" />

namespace RS.Is
{
    /**
     * Determines if the given arbiter resolver is a simple arbiter resolver or not.
     * @param value The arbiter resolver.
     */
    export function simpleArbiterResolver<T>(value: ArbiterResolver<T>): value is RS.SimpleArbiterResolver<T>
    {
        // Simple resolvers take 2 parameters, complex resolvers take 1 parameter (array of values)
        return value != null && value.length === 2;
    }
}

namespace RS
{
    /**
     * A function that reduces multiple possible values for an arbiter into one.
     * @param values The candidate arbiter values that need to be resolved
     */
    export type ComplexArbiterResolver<T> = (a: T[]) => T;
    /**
     * A function that reduces two possible values for an arbiter into one.
     */
    export type SimpleArbiterResolver<T> = (a: T, b: T) => T;
    export type ArbiterResolver<T> = SimpleArbiterResolver<T> | ComplexArbiterResolver<T>;

    /**
     * Represents a potentially active declaration on a Arbiter.
     */
    export interface IArbiterHandle<T> extends IDisposable
    {
        /** The arbiter to which this handle is associated with. */
        readonly arbiter: IArbiter<T>;

        /** The value that this handle is trying to enforce. */
        readonly value: T;
    }

    /**
     * An object encapsulating a single unit of state.
     * - Subscribers can request to be notified when the state of the Arbiter has updated.
     * - User code can declare a modification to the Arbiter's value which is resolved alongside other declarations.
     * - User code can release their declaration at any time.
     */
    export interface IArbiter<T> extends IReadonlyObservable<T>
    {
        /**
         * Makes a declaration to modify this Arbiter's value.
         */
        declare(value: T): IArbiterHandle<T>;
    }
}