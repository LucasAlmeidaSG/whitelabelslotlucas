namespace RS
{
    /**
     * Encapsulates the reduced value of the last n states of something.
     * This could be useful for, for example, rolling average.
     */
    export interface IBufferObservable<T> extends IReadonlyObservable<T>
    {
        /**
         * Pushes a new state to the buffer.
         * The oldest state will be removed from the buffer.
         * @param value 
         */
        push(value: T): void;
    }

    class BufferObservable<T> extends Observable<T> implements IBufferObservable<T>
    {
        private _resolveFunc: IBufferObservable.ResolverFunction<T>;
        private _buffer: T[];

        public constructor(bufferSize: number, initialState: T, resolveFunc: IBufferObservable.ResolverFunction<T>)
        {
            const buffer = new Array<T>(bufferSize);
            for (let i = 0; i < bufferSize; ++i)
            {
                buffer[i] = initialState;
            }
            super(resolveFunc(buffer));
            this._resolveFunc = resolveFunc;
            this._buffer = buffer;
        }

        public push(value: T): void
        {
            for (let i = 0, l = this._buffer.length - 1; i < l; ++i)
            {
                this._buffer[i] = this._buffer[i + 1];
            }
            this._buffer[this._buffer.length - 1] = value;
            this.value = this._resolveFunc(this._buffer);
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._resolveFunc = null;
            this._buffer.length = 0;
            this._buffer = null;
            super.dispose();
        }
    }

    export namespace IBufferObservable
    {
        export type ResolverFunction<T> = (buffer: ReadonlyArray<T>) => T;

        /**
         * Creates a new buffer observable.
         * @param bufferSize 
         * @param initialState 
         * @param resolverFunc 
         */
        export function create<T>(bufferSize: number, initialState: T, resolverFunc: ResolverFunction<T>): IBufferObservable<T>
        {
            return new BufferObservable(bufferSize, initialState, resolverFunc);
        }
    }
}