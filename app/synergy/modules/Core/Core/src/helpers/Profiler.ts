namespace RS
{
    /**
     * A generic profiler that can be used to track time elapsed.
     * Does not subscribe to any events and so is cheap to allocate and "throw away".
     */
    export class Profiler
    {
        private readonly __count: RS.Map<number> = {};
        private readonly __running: RS.Map<number> = {};
        private readonly __times: RS.Map<number> = {};

        public get times(): Readonly<RS.Map<number>> { return this.__times; }

        public start(name: string): void
        {
            const count = this.__count[name];
            if (!count)
            {
                this.__running[name] = Timer.now;
            }
            this.__count[name] = (count || 0) + 1;
        }

        public stop(name: string): void
        {
            if (!this.__count[name])
            {
                RS.Log.warn(`Profiler: stop() called before start()`);
                return;
            }
            if (--this.__count[name])
            {
                // Still running
                return;
            }
            const thisTime = Timer.now - this.__running[name];
            const time = this.__times[name];
            if (time)
            {
                this.__times[name] += thisTime;
            }
            else
            {
                this.__times[name] = thisTime;
            }
        }

        public get(name: string): number
        {
            return this.__times[name] || 0;
        }

        public getSeconds(name: string): number
        {
            return this.get(name) / 1000;
        }

        public format(name: string): string
        {
            return Timer.format(this.get(name));
        }

        public formatAll(): string
        {
            let out: string = "";
            for (const name in this.__times)
            {
                out += `${name}: ${this.format(name)}\n`;
            }
            return out;
        }

        public clear(): void
        {
            for (const key in this.__times)
            {
                delete this.__times[key];
            }
        }
    }
}