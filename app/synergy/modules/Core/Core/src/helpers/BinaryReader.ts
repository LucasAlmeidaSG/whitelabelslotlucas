namespace RS
{
    /**
     * A generic binary reader.
    **/
    export class BinaryReader
    {
        private _dataView: DataView;
        private _pointer: number;

        /** Gets or sets the current read position of the reader. */
        public get pointer() { return this._pointer; }
        public set pointer(value) { this._pointer = value; }

        /** Gets the size of the buffer in bytes. */
        public get length() { return this._dataView.byteLength; }

        /** Gets if we've reached the end of the buffer. */
        public get eos() { return this._pointer >= this._dataView.byteLength; }

        /**
         * Initialises a new instance of the BinaryReader class.
         * @param data      Raw data buffer.
        **/
        constructor(data: ArrayBuffer, initialPointer: number = 0)
        {
            this._dataView = new DataView(data);
            this._pointer = initialPointer;
        }

        /**
         * Reads an 8-bit unsigned integer.
        **/
        public readByte(): number
        {
            return this._dataView.getUint8(this.advancePointer(1));
        }

        /**
         * Reads an 8-bit signed integer.
        **/
        public readSByte(): number
        {
            return this._dataView.getInt8(this.advancePointer(1));
        }

        /**
         * Reads an 16-bit signed integer.
        **/
        public readShort(): number
        {
            return this._dataView.getInt16(this.advancePointer(2));
        }

        /**
         * Reads an 16-bit unsigned integer.
        **/
        public readUShort(): number
        {
            return this._dataView.getUint16(this.advancePointer(2));
        }

        /**
         * Reads an 32-bit signed integer.
        **/
        public readInt(): number
        {
            return this._dataView.getInt32(this.advancePointer(4));
        }

        /**
         * Reads an 32-bit unsigned integer.
        **/
        public readUInt(): number
        {
            return this._dataView.getUint32(this.advancePointer(4));
        }

        /**
         * Reads an 32-bit floating point number.
        **/
        public readFloat(): number
        {
            return this._dataView.getFloat32(this.advancePointer(4));
        }

        /**
         * Reads an 64-bit floating point number.
        **/
        public readDouble(): number
        {
            return this._dataView.getFloat64(this.advancePointer(8));
        }

        /**
         * Reads a fixed-length string.
         * @param length    String length in bytes.
        **/
        public readFixedString(length: number): string
        {
            let str = "";
            while (length-- > 0)
            {
                str += String.fromCharCode(this.readByte());
            }
            return str;
        }

        /**
         * Skips the specified amount of bytes.
        **/
        public skip(amount: number): void
        {
            this._pointer += amount;
        }

        /**
         * Advances the pointer by the specified amount and returns the old pointer.
        **/
        private advancePointer(amount: number): number
        {
            return (this._pointer += amount) - amount;
        }
    }
}