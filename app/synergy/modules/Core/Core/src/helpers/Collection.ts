namespace RS
{
	/**
	 * An iterator for a collection.
	 */
	export type CollectionIterator<T> = (item: T) => void;
	
	/**
	 * A generic collection of items with an undefined order.
	 * Optimised for speed of addition, removal and iteration of items.
	 * Items can safely be added and removed while iterating.
	 */
	export class Collection<T>
	{
		// The backing array
		private _items: T[];
		
		// Items pending add/remove
		private _toAdd: T[];
		private _toRemove: T[];
		private _clearFlag: boolean;
		
		// Are we currently iterating?
		private _iterating: boolean;
		
		/**
		 * Initialises a new instance of the Collection class.
		 * @param items		Existing backing array.
		 */
		public constructor(items?: T[])
		{
			this._items = items || [];
			this._toAdd = [];
			this._toRemove = [];
			this._clearFlag = false;
		}

		/** Gets the number of items in this collection. */
		public get count() { return this._items.length; }
		
		/**
		 * Adds an item to this collection.
		 * @param item		The item to add
		 */
		public addItem(item: T): void
		{
			if (this._iterating)
			{
				this._toAdd.push(item);
			}
			else
			{
				this._items.push(item);
			}
		}
		
		/**
		 * Removes an item from this collection.
		 * @param item		The item to remove
		 */
		public removeItem(item: T): void
		{
			if (this._iterating)
			{
				this._toRemove.push(item);
			}
			else
			{
				const items = this._items;
				const index = items.indexOf(item);
				if (index != -1)
				{
					// Is it NOT the last item?
					if (index < items.length - 1)
					{
						// Overwrite it with the last item
						items[index] = items[items.length - 1];
					}
					
					// Remove last item
					items.length--;
				}
			}
		}

		/**
		 * Gets if this collection contains the specified item.
		 * @param item		The item to search for
		 */
		public contains(item: T): boolean
		{
			return this._items.indexOf(item) !== -1;
		}

		/**
		 * Clears all items in this collection.
		 * Safe to use within iterate, but will not take effect until after ALL iteration has completed.
		 * In addition to above, any attempts to add items AFTER clearing within an iterate will fail silently. 
		 */
		public clear(): void
		{
			if (this._iterating)
			{
				this._clearFlag = true;
			}
			else
			{
				this._items.length = 0;
			}
		}
		
		/**
		 * Iterates all items in this collection and calls the specified iterator for each.
		 * @returns		The total number of items iterated.
		 */
		public iterate(iterator: CollectionIterator<T>): number
		{
			if (this._iterating) { throw new Error("Nested iteration is not supported!"); }
			this._iterating = true;
			const items = this._items;
			const len = items.length;
			let item: T;
			for (let i = 0; i < len; i++)
			{
				item = items[i];
				if (item != null)
				{
					iterator(item);
				}
			}
			this._iterating = false;
			if (this._toAdd.length > 0)
			{
				for (let i = this._toAdd.length - 1; i >= 0; i--)
				{
					this.addItem(this._toAdd[i]);
					this._toAdd.length--;
				}
			}
			if (this._toRemove.length > 0)
			{
				for (let i = this._toRemove.length - 1; i >= 0; i--)
				{
					this.removeItem(this._toRemove[i]);
					this._toRemove.length--;
				}
			}
			if (this._clearFlag)
			{
				this._items.length = 0;
				this._clearFlag = false;
			}
			return len;
		}
	}
}