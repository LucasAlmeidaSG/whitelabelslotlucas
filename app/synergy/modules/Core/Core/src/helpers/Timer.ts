namespace RS
{
    /**
     * A generic timer that can be used to track time elapsed.
     * Does not subscribe to any events and so is cheap to allocate and "throw away".
     */
    export class Timer
    {
        /** Gets the current timestamp in ms. */
        public static get now() { return (performance || Date).now(); }

        /** Gets the current timestamp in s. */
        public static get nowSeconds() { return this.now / 1000.0; }

        /** Formats a time in milliseconds. */
        public static format(millis: number): string { return `${millis.toFixed(2)}s`; }

        protected _startTime: number = Timer.now;
        protected _paused: boolean = false;
        protected _pauseTime: number = 0;

        /** Gets the amount of elapsed time in ms. */
        public get elapsed() { return (this._paused ? this._pauseTime : Timer.now) - this._startTime; }

        /** Gets the amount of elapsed time in s. */
        public get elapsedSeconds() { return this.elapsed / 1000.0; }

        /** Gets this timer's formatted time. */
        public get formatted() { return Timer.format(this.elapsed); }

        /** Gets or sets if this timer is paused. */
        public get paused() { return this._paused; }
        public set paused(value)
        {
            if (this._paused === value) { return; }
            if (value)
            {
                this._paused = true;
                this._pauseTime = Timer.now;
            }
            else
            {
                this._startTime = Timer.now - this.elapsed;
                this._paused = false;
            }
        }

        /** Resets this timer. */
        public reset(): void
        {
            this._startTime = Timer.now;
        }
    }
}