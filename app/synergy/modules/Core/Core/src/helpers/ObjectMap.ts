namespace RS
{
    /**
     * Encapsulates a map of keys to values.
     * The keys and values can be of any type, unlike standard JS maps where keys can only be numbers or strings.
     */
    export class ObjectMap<TKey, TValue>
    {
        protected _hashFunc: ObjectMap.HashFunc<TKey>;
        protected _equalFunc: ObjectMap.EqualFunc<TKey>;
        protected _data: { [hash: number]: ObjectMap.Pair<TKey, TValue>[] };

        /**
         * Gets all keys in this map.
         */
        public get keys(): TKey[]
        {
            const result: TKey[] = [];
            for (const hash in this._data)
            {
                const arr = this._data[hash];
                for (const pair of arr)
                {
                    result.push(pair[0]);
                }
            }
            return result;
        }

        /**
         * Gets all values in this map.
         */
        public get values(): TValue[]
        {
            const result: TValue[] = [];
            for (const hash in this._data)
            {
                const arr = this._data[hash];
                for (const pair of arr)
                {
                    result.push(pair[1]);
                }
            }
            return result;
        }

        public constructor(hashFunc: ObjectMap.HashFunc<TKey>, equalFunc: ObjectMap.EqualFunc<TKey>)
        {
            this._hashFunc = hashFunc;
            this._equalFunc = equalFunc;
            this._data = {};
        }

        /**
         * Gets if this map contains the specified key or not.
         * @param key 
         */
        public containsKey(key: TKey): boolean
        {
            const hash = this._hashFunc(key);
            const pairs = this._data[hash];
            if (!pairs) { return false; }
            for (const pair of pairs)
            {
                if (this._equalFunc(pair[0], key))
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * Gets if this map contains the specified value or not.
         * @param value 
         */
        public containsValue(value: TValue): boolean
        {
            for (const hash in this._data)
            {
                const arr = this._data[hash];
                for (const pair of arr)
                {
                    if (pair[1] === value)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Gets if this map contains the specified key-value pair or not.
         * @param value 
         */
        public contains(key: TKey, value: TValue): boolean
        {
            const hash = this._hashFunc(key);
            const pairs = this._data[hash];
            if (!pairs) { return false; }
            for (const pair of pairs)
            {
                if (this._equalFunc(pair[0], key) && pair[1] === value)
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * Looks up an item from this map, returning the item or null if not found.
         * @param key 
         */
        public get(key: TKey): TValue | null
        {
            const hash = this._hashFunc(key);
            const pairs = this._data[hash];
            if (!pairs) { return null; }
            for (const pair of pairs)
            {
                if (this._equalFunc(pair[0], key))
                {
                    return pair[1];
                }
            }
            return null;
        }

        /**
         * Sets an item in this map.
         * @param key 
         */
        public set(key: TKey, value: TValue): void
        {
            const hash = this._hashFunc(key);
            const pairs = this._data[hash];
            if (!pairs)
            {
                this._data[hash] = [ [ key, value ] ];
                return;
            }
            let found = false;
            for (const pair of pairs)
            {
                if (this._equalFunc(pair[0], key))
                {
                    pair[1] = value;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                pairs.push([ key, value ]);
            }
        }

        /**
         * Removes an item from this map.
         * @param key 
         */
        public remove(key: TKey): boolean
        {
            const hash = this._hashFunc(key);
            const pairs = this._data[hash];
            if (!pairs) { return null; }
            for (let i = 0, l = pairs.length; i < l; ++i)
            {
                const pair = pairs[i];
                if (this._equalFunc(pair[0], key))
                {
                    if (pairs.length === 1)
                    {
                        delete this._data[hash];
                    }
                    else if (i === l - 1)
                    {
                        --pairs.length;
                    }
                    else
                    {
                        pairs[i] = pairs[l - 1];
                        --pairs.length;
                    }
                    return true;
                }
            }
            return false;
        }
    }

    export namespace ObjectMap
    {
        export type HashFunc<TKey> = (key: TKey) => number;
        export type EqualFunc<TKey> = (a: TKey, b: TKey) => boolean;
        export type Pair<TKey, TValue> = [ TKey, TValue ];
    }
}