namespace RS
{
    export type TaskCallback<T> = (task: Task<T>, resolve: (value?: T) => void, reject: (reason?: string) => void) => void;

    /**
     * Encapsulates an asynchronous task.
    **/
    export class Task<T> implements PromiseLike<T>
    {
        protected _baseCallback: TaskCallback<T>|null = null;
        protected _pendingPromises: {
            onfulfilled?: (value: T) => void;
            onrejected?: (reason: any) => void;
        }[] = [];
        protected _completed: boolean = false;
        protected _failed: boolean = false;
        protected _failReason: string|null = null;
        protected _value: T;
        protected _cancelled: boolean = false;
        protected _started: boolean = false;
        protected _allowEarlyTermination: boolean = false;
        protected _cancelledCallbacks: (() => void)[] = [];
        protected _name: string = "Untitled Task";

        /** Gets if this task has started. */
        public get started() { return this._started; }

        /** Gets if this task has completed. */
        public get completed() { return this._completed; }

        /** Gets if this task has failed. */
        public get failed() { return this._failReason; }

        /** Gets the reason why this task has failed. */
        public get failReason() { return this._completed; }

        /** Gets the result of this task. */
        public get result() { return this._value; }

        /** Gets if this task has been cancelled. */
        public get cancelled() { return this._cancelled; }

        /** Gets or sets the name of this task for debugging purposes. */
        public get name() { return this._name; }
        public set name(value) { this._name = value; }

        /** Gets or sets if this task is allowed to terminate early if the owner doesn't want it to hold up. */
        public get allowEarlyTermination() { return this._allowEarlyTermination; }
        public set allowEarlyTermination(value) { this._allowEarlyTermination = value; }

        public constructor(callback?: TaskCallback<T>, startNow: boolean = true)
        {
            this._baseCallback = callback;
            if (startNow) { this.start(); }
        }
        /**
         * Creates a new task from the specified promise.
        **/
       public static fromPromise<T>(promise: PromiseLike<T>, startNow?: boolean): Task<T>
       {
           const task = new Task<T>((taskArg, resolve, reject) =>
           {
               promise.then(resolve, reject);
           }, startNow);
           if (promise instanceof Task)
           {
               task.name = `-> ${promise.name}`;
               task.onCancelled(() => promise.cancel());
           }
           else
           {
               task.name = `-> Promise`;
           }
           return task;
       }

       /**
        * Creates a new task from the specified provider.
        * The provider function will only be evaluated when the task needs to be started.
       **/
       public static fromPromiseProvider<T>(promiseProvider: () => PromiseLike<T>): Task<T>
       {
           const task = new Task<T>((takeArg, resolve, reject) =>
           {
               const promise = promiseProvider();
               if (promise == null)
               {
                   resolve();
               }
               else
               {
                   promise.then(resolve, reject);
               }
           }, false);
           task.name = `-> PromiseProvider`;
           return task;
       }

        /** Starts this task if it hasn't already been started. */
        public start()
        {
            if (this._started) { return; }
            this._started = true;
            this.log(`has started`);
            if (this._baseCallback == null)
            {
                this._baseCallback = (task, resolve, reject) => task.doTask(resolve, reject);
            }
            this._baseCallback(this, (v) => this.onSucceed(v), (r) => this.onFail(r));
        }

        /**
         * Cancels this task.
         * @param reject    If true, rejects all pending promises.
        **/
        public cancel(reject: boolean = false)
        {
            if (this._completed || !this._started) { return; }
            this.log(`was cancelled`);
            this._cancelled = true;
            for (let i = 0; i < this._cancelledCallbacks.length; i++)
            {
                this._cancelledCallbacks[i]();
            }
            this._cancelledCallbacks = null;
            if (this._completed) { return; }
            if (reject)
            {
                this.onFail("Task cancelled");
            }
            else
            {
                this.onSucceed(undefined);
            }
        }

        /**
         * Attaches callbacks for the resolution and/or rejection of the Promise.
         * @param onfulfilled The callback to execute when the Promise is resolved.
         * @param onrejected The callback to execute when the Promise is rejected.
         * @returns A Promise for the completion of which ever callback is executed.
         */
        public then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): PromiseLike<TResult1 | TResult2>
        {
            return new Promise((resolve, reject) =>
            {
                if (this._completed)
                {
                    if (this._failed)
                    {
                        if(onrejected) {
                            onrejected(this._failReason);
                        }
                        reject(this._failReason);
                    }
                    else
                    {
                        if(onfulfilled) {
                            onfulfilled(this._value);
                        }
                        resolve(this._value as any);
                    }
                }
                else
                {
                    this._pendingPromises.push({
                        onfulfilled: (value) =>
                        {
                            if(onfulfilled) {
                                onfulfilled(value);
                            }
                            resolve(value as any);
                        },
                        onrejected: (reason) =>
                        {
                            if(onrejected) {
                                onrejected(reason);
                            }
                            reject(reason);
                        }
                    });
                }
            });
        }

        /**
         * Specify a function to call when this task is cancelled.
         * If the task is ALREADY cancelled, calls immediately.
        **/
        public onCancelled(callback: () => void)
        {
            if (this._completed && this._cancelled)
            {
                callback();
            }
            else
            {
                this._cancelledCallbacks.push(callback);
            }
        }

        /**
         * Gets a string representation of this task.
        **/
        public toString()
        {
            return this._name;
        }

        /** Logs a debug message. */
        protected log(message: string)
        {
            Logging.ILogger.get().getContext("Task").log(`'${this}' ${message}`, Logging.LogLevel.Debug);
        }

        /** Performs the async task. */
        protected doTask(resolve: (value: T) => void, reject: (reason?: string) => void) { return; }

        /** Called when the base promise has succeeded. */
        protected onSucceed(value: T)
        {
            if (this._completed)
            {
                if (this._cancelled) { return; }
                Log.warn(`A task tried to complete itself more than once!`);
                debugger;
            }
            this.log(`has succeeded`);
            this._completed = true;
            this._failed = false;
            this._value = value;
            for (let i = 0; i < this._pendingPromises.length; i++)
            {
                if(this._pendingPromises[i].onfulfilled) {
                    this._pendingPromises[i].onfulfilled(value);
                }
            }
            this._pendingPromises.length = 0;
        }

        /** Called when the base promise has failed. */
        protected onFail(reason?: any)
        {
            if (this._completed)
            {
                if (this._cancelled) { return; }
                Log.warn(`A task tried to complete itself more than once!`);
                debugger;
            }
            this.log(`has failed (${reason})`);
            this._completed = true;
            this._failed = true;
            this._failReason = reason;
            for (let i = 0; i < this._pendingPromises.length; i++)
            {
                if(this._pendingPromises[i].onrejected) {
                    this._pendingPromises[i].onrejected(reason);
                }
            }
            this._pendingPromises.length = 0;
        }
    }

    /**
     * Encapsulates an asynchronous task comprised of a number of sub-tasks.
     * Sub-tasks can be executed in parallel or sequentially.
     * All sub-tasks must be complete before this task is considered complete.
    **/
    export class MultiTask<T> extends Task<T>
    {
        protected _subTasks: Task<T>[];
        protected _pendingSubTasks: Task<T>[];
        protected _failIfOneFails: boolean;
        protected _reducer: (a: T, b: T) => T;
        protected _parallel: boolean;
        protected _shouldAttemptEarlyTermination: boolean;

        protected _curVal: T;
        protected _numSucceed: number;
        protected _numFail: number;

        protected _resolve: (value: T) => void;
        protected _reject: (reason?: string) => void;

        /** Gets all sub-tasks that this task is responsible for. */
        public get subTasks(): ReadonlyArray<Task<T>> { return this._subTasks; }

        public constructor(subTasks: Task<T>[], parallel: boolean = true, failIfOneFails: boolean = true, reducer?: (a: T, b: T) => T)
        {
            super(null, false);
            if (subTasks.length === 0) { throw new Error("Multi-task created with no sub-tasks"); }
            if (subTasks.some((task) => task == null)) { throw new Error("Multi-task created with null sub-tasks"); }
            this._parallel = parallel;
            this._subTasks = subTasks;
            this._failIfOneFails = failIfOneFails;
            this._reducer = reducer;
        }

        /**
         * Cancels this task.
         * @param reject    If true, rejects all pending promises.
        **/
        public cancel(reject: boolean = false)
        {
            if (this._completed || !this._started) { return; }
            this.log(`was cancelled`);
            this._cancelled = true;
            for (let i = 0; i < this._subTasks.length; i++)
            {
                const subTask = this._subTasks[i];
                if (!subTask.completed && subTask.started)
                {
                    subTask.cancel(false);
                }
            }
            for (let i = 0; i < this._cancelledCallbacks.length; i++)
            {
                this._cancelledCallbacks[i]();
            }
            this._cancelledCallbacks = null;
            if (this._completed) { return; }
            if (reject)
            {
                this.onFail("Task cancelled");
            }
            else
            {
                this.onSucceed(undefined);
            }
        }

        /** Performs the async task. */
        protected doTask(resolve: (value: T) => void, reject: (reason?: string) => void)
        {
            if (this._subTasks.length === 0)
            {
                resolve(undefined);
                return;
            }

            this._numSucceed = 0;
            this._numFail = 0;
            this._pendingSubTasks = this._subTasks.slice();
            this._resolve = resolve;
            this._reject = reject;
            if (this._parallel)
            {
                this._shouldAttemptEarlyTermination = true;
                for (let i = 0; i < this._subTasks.length; i++)
                {
                    const subTask = this._subTasks[i];
                    subTask.then((v) =>
                        {
                            this.log(`sub task succeeded`);
                            this.onSubTaskSucceed(subTask);
                            return undefined;
                        }, (r) =>
                        {
                            this.log(`sub task failed`);
                            this.onSubTaskFail(subTask);
                            return undefined;
                        }
                    );
                    subTask.start();
                }
            }
            else
            {
                this._shouldAttemptEarlyTermination = false;
                let taskPtr = -1;
                const doNextTask = () =>
                {
                    taskPtr++;
                    if (taskPtr >= this._subTasks.length) { return; }
                    if (this._cancelled)
                    {
                        this._resolve(this._curVal);
                        return;
                    }
                    const subTask = this._subTasks[taskPtr];
                    subTask.then((v) =>
                        {
                            this.onSubTaskSucceed(subTask);
                            doNextTask();
                            return v;
                        }, (r) =>
                        {
                            this.onSubTaskFail(subTask);
                            if (!this._failIfOneFails) { doNextTask(); }
                            return r;
                        }
                    );
                    subTask.start();
                };
                doNextTask();
            }
        }

        protected onSubTaskSucceed(subTask: Task<T>)
        {
            if (this._completed) { return; }
            this._pendingSubTasks.splice(this._pendingSubTasks.indexOf(subTask), 1);
            this._numSucceed++;
            if (this._curVal === undefined)
            {
                this._curVal = subTask.result;
            }
            else
            {
                this._curVal = this._reducer != null ? this._reducer(this._curVal, subTask.result) : subTask.result;
            }
            if (this._numSucceed + this._numFail === this._subTasks.length)
            {
                this._resolve(this._curVal);
            }
            if (this._shouldAttemptEarlyTermination) { this.attemptEarlyTermination(); }
        }

        protected onSubTaskFail(subTask: Task<T>)
        {
            if (this._completed) { return; }
            this._pendingSubTasks.splice(this._pendingSubTasks.indexOf(subTask), 1);
            this._numFail++;
            if (this._failIfOneFails)
            {
                this._reject();
            }
            else
            {
                if (this._numSucceed + this._numFail === this._subTasks.length)
                {
                    this._resolve(this._curVal);
                }
            }
            if (this._shouldAttemptEarlyTermination) { this.attemptEarlyTermination(); }
        }

        protected attemptEarlyTermination()
        {
            // Check if all pending sub tasks allow early termination
            if (this._pendingSubTasks.some((p) => !p.allowEarlyTermination)) { return; }

            // Infinite loop protection
            this._shouldAttemptEarlyTermination = false;

            // Cancel remaining sub tasks
            const toCancel = [...this._pendingSubTasks];
            for (let i = 0; i < toCancel.length; i++)
            {
                toCancel[i].cancel(false);
            }
        }
    }
}