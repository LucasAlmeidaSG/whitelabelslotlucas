namespace RS
{
    /** A base error class which can be extended for custom instance-checkable errors and which supports error nesting. */
    export abstract class BaseError extends Error
    {
        /** The cause of this error or null/undefined if the cause is nonexistent or unknown. */
        public readonly cause: any;

        constructor(message?: string, innerError?: any)
        {
            super(BaseError.getMessage(message, innerError));
            this.cause = innerError;
            Util.setPrototypeOf(this, new.target.prototype);
        }

        private static getMessage(message?: string, innerError?: any): string | null
        {
            if (innerError)
            {
                const innerMessage = Util.getErrorMessage(innerError);
                if (innerMessage)
                {
                    if (message)
                    {
                        return `${message}: ${innerMessage}`;
                    }

                    return innerMessage;
                }
            }

            return message;
        }
    }

    /** A generic nested error. */
    export class NestedError extends BaseError
    {
        constructor(message: string, innerError: any) { super(message, innerError); }
    }
}