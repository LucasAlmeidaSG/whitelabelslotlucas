namespace RS
{
    export type IAwaitable<T> = PromiseLike<T>;

    export type AwaitableValue<T> = IAwaitable<T> | T;
    
    type ExecutionCallback<TArg, TResult> = ((value: TArg) => TResult | PromiseLike<TResult>) | undefined | null;
    function argless<TResult>(cb: ExecutionCallback<void, TResult>): ExecutionCallback<any, TResult>
    {
        return function () { return cb(undefined); };
    }

    /**
     * Encapsulates an object that can be awaited on.
     */
    export abstract class Awaitable<TResult = void, TRejectionReason = any> implements IAwaitable<TResult>
    {
        private __awaitableCallbacks: RS.PendingPromise[] | null = null;
        private __awaitableResult: Result<TResult, any> | null = null;

        /**
         * Attaches callbacks for the resolution and/or rejection of the Promise.
         * @param onFulfilled The callback to execute when the Promise is resolved.
         * @param onRejected The callback to execute when the Promise is rejected.
         * @returns A Promise for the completion of which ever callback is executed.
         */
        public then<TResult1 = TResult, TResult2 = never>(onFulfilled?: ExecutionCallback<TResult, TResult1>, onRejected?: ExecutionCallback<TRejectionReason, TResult2>): Promise<TResult1 | TResult2>
        {
            /*
                From promisejs.org:

                this.then = function (onFulfilled, onRejected) {
                var self = this;
                return new Promise(function (resolve, reject) {
                    return self.done(function (result) {
                    if (typeof onFulfilled === 'function') {
                        try {
                        return resolve(onFulfilled(result));
                        } catch (ex) {
                        return reject(ex);
                        }
                    } else {
                        return resolve(result);
                    }
                    }, function (error) {
                    if (typeof onRejected === 'function') {
                        try {
                        return resolve(onRejected(error));
                        } catch (ex) {
                        return reject(ex);
                        }
                    } else {
                        return reject(error);
                    }
                    });
                });
                }
            */

            return new Promise((resolve, reject) =>
            {
                this.thenInternal((data) =>
                {
                    if (Is.func(onFulfilled))
                    {
                        try
                        {
                            resolve(onFulfilled(data));
                        }
                        catch (err)
                        {
                            reject(err);
                        }
                    }
                    else
                    {
                        resolve();
                    }
                }, (reason) =>
                {
                    if (Is.func(onRejected))
                    {
                        try
                        {
                            resolve(onRejected(reason));
                        }
                        catch (err)
                        {
                            reject(err);
                        }
                    }
                    else
                    {
                        reject(reason);
                    }
                });
            });
        }

        /**
         * The finally() method returns a Promise. When the promise is settled, i.e either fulfilled or rejected, the specified callback function is executed. This provides a way for code to be run whether the promise was fulfilled successfully or rejected once the Promise has been dealt with.
         *
         * This helps to avoid duplicating code in both the promise's then() and catch() handlers.
         * @param onFinally The callback to execute when the Promise is resolved or rejected.
         */
        public finally<TResult1 = never>(onFinally?: ExecutionCallback<void, TResult1>)
        {
            const cb = Is.func(onFinally) ? argless(onFinally) : undefined;
            return this.then(cb, cb);
        }

        /**
         * Attaches a callback for only the rejection of the Promise.
         * @param onrejected The callback to execute when the Promise is rejected.
         * @returns A Promise for the completion of the callback.
         */
        public catch<TResult1 = never>(onRejected?: ExecutionCallback<TRejectionReason, TResult1>)
        {
            return this.then(undefined, onRejected);
        }

        protected resolve(result?: TResult): void
        {
            if (this.__awaitableResult) { return; }
            if (this.__awaitableCallbacks)
            {
                for (const promiseCallback of this.__awaitableCallbacks)
                {
                    if (promiseCallback.resolve)
                    {
                        promiseCallback.resolve(result);
                    }
                }
                this.__awaitableCallbacks = null;
            }

            this.__awaitableResult = { type: "resolution", result };
        }

        protected reject(reason?: TRejectionReason): void
        {
            if (this.__awaitableResult) { return; }
            if (this.__awaitableCallbacks)
            {
                for (const promiseCallback of this.__awaitableCallbacks)
                {
                    if (promiseCallback.reject)
                    {
                        promiseCallback.reject(reason);
                    }
                }
                this.__awaitableCallbacks = null;
            }

            this.__awaitableResult = { type: "rejection", reason };
        }

        protected clearAwaitableResult()
        {
            this.__awaitableResult = null;
        }

        private thenInternal(resolve: (data?: TResult) => void, reject: (reason?: TRejectionReason) => void): void
        {
            if (this.__awaitableResult)
            {
                switch (this.__awaitableResult.type)
                {
                    case "rejection":
                    {
                        reject(this.__awaitableResult.reason);
                        return;
                    }
                    case "resolution":
                    {
                        resolve(this.__awaitableResult.result);
                        return;
                    }
                }
            }
            if (!this.__awaitableCallbacks) { this.__awaitableCallbacks = []; }
            this.__awaitableCallbacks.push({ resolve, reject });
        }
    }

    interface Rejection<T> { type: "rejection"; reason?: T; }
    interface Resolution<T> { type: "resolution"; result?: T; }
    type Result<TResolvedResult, TRejectionReason> = Resolution<TResolvedResult> | Rejection<TRejectionReason>;
}