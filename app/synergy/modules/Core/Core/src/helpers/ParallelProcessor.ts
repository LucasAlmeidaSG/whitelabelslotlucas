namespace RS
{
    /**
     * Runs several async processing chains in parallel.
    **/
    export class ParallelProcessor<T>
    {
        protected _items: T[];
        protected _threadCount: number;
        protected _processor: ParallelProcessor.ItemProcessor<T>;

        protected _result: ParallelProcessor.ProcessResult<T>;
        protected _curItemIndex: number;

        public constructor(items: T[], threadCount: number, processor: ParallelProcessor.ItemProcessor<T>)
        {
            this._items = items;
            this._threadCount = threadCount;
            this._processor = processor;
        }

        /** Launches all threads and waits until completion, returning the results. */
        public async process()
        {
            const result: ParallelProcessor.ProcessResult<T> = this._result = {
                succeedItems: [],
                failedItems: []
            };
            this._curItemIndex = 0;

            const threads: Promise<void>[] = [];
            for (let i = 0; i < this._threadCount; i++)
            {
                threads.push(this.thread(i));
            }
            for (let i = 0; i < this._threadCount; i++)
            {
                await threads[i];
            }

            // Done
            this._result = null;
            return result;
        }

        /** Runs a single thread that will process all items until exhaustion. */
        protected async thread(id: number)
        {
            while (this._curItemIndex < this._items.length)
            {
                let success: boolean = true;
                const item = this._items[this._curItemIndex++];
                try
                {
                    await this._processor(item, id);
                }
                catch (err)
                {
                    success = false;
                }
                if(success) {
                    this._result.succeedItems.push(item);
                }
                else {
                    this._result.failedItems.push(item);
                }
                //success && this._result.succeedItems.push(item) || this._result.failedItems.push(item);
            }
        }
    }

    export namespace ParallelProcessor
    {
        export type ItemProcessor<T> = (item: T, threadID: number) => PromiseLike<void>;
        export interface ProcessResult<T>
        {
            succeedItems: T[];
            failedItems: T[];
        }
    }
}