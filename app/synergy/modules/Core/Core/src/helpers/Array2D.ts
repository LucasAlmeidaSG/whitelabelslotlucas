namespace RS
{
    /**
     * A generic fixed-size 2D array.
     */
    export class Array2D<T>
    {
        protected _cols: number;
        protected _rows: number;
        protected _data: T[];

        /** Gets the number of columns in this array. */
        public get columns() { return this._cols; }

        /** Gets the number of rows in this array. */
        public get rows() { return this._rows; }

        public constructor(columns: number, rows: number, initialValue?: T)
        {
            this._cols = columns;
            this._rows = rows;
            this._data = new Array<T>(columns * rows);
            if (initialValue !== undefined) { this.set(initialValue); }
        }

        /**
         * Sets ALL values of this array to the specified value.
         */
        public set(value: T): void;

        /**
         * Sets the value of the specified cell to the specified value.
         */
        public set(colIndex: number, rowIndex: number, value: T): void;

        public set(colIndexOrValue: number | T, rowIndex?: number, value?: T): void
        {
            if (rowIndex != null)
            {
                const colIndex = colIndexOrValue as number;
                if (colIndex < 0 || colIndex >= this._cols)
                {
                    throw new Error("colIndex out of bounds");
                }
                if (rowIndex < 0 || rowIndex >= this._rows)
                {
                    throw new Error("rowIndex out of bounds");
                }
                this._data[rowIndex * this._cols + colIndex] = value;
            }
            else
            {
                for (let i = 0; i < this._data.length; i++)
                {
                    this._data[i] = colIndexOrValue as T;
                }
            }
        }

        /**
         * Gets the value of the specified cell.
         */
        public get(colIndex: number, rowIndex: number): T
        {
            if (colIndex < 0 || colIndex >= this._cols)
            {
                throw new Error("colIndex out of bounds");
            }
            if (rowIndex < 0 || rowIndex >= this._rows)
            {
                throw new Error("rowIndex out of bounds");
            }
            return this._data[rowIndex * this._cols + colIndex];
        }

        /**
         * Creates a copy of this array.
         */
        public clone(): Array2D<T>
        {
            const obj: Array2D<T> = Object.create(Array2D.prototype);
            obj._cols = this._cols;
            obj._rows = this._rows;
            obj._data = [...this._data];
            return obj;
        }

        /**
         * Gets if the content of this array is equal to the other.
         */
        public equals(other: Array2D<T>): boolean
        {
            if (other.columns !== this.columns || other.rows !== this.rows) { return false; }
            for (let i = 0; i < other._data.length; i++)
            {
                if (other._data[i] !== this._data[i])
                {
                    return false;
                }
            }
            return true;
        }

        /**
         * Returns a copy of the specified column.
         */
        public getColumn(colIndex: number): T[]
        {
            if (colIndex < 0 || colIndex >= this._cols)
            {
                throw new Error("colIndex out of bounds");
            }
            const arr = new Array<T>(this._rows);
            for (let rowIndex = 0; rowIndex < arr.length; rowIndex++)
            {
                arr[rowIndex] = this.get(colIndex, rowIndex);
            }
            return arr;
        }

        /**
         * Sets the specified column of this array.
         */
        public setColumn(colIndex: number, arr: T[]): void
        {
            if (colIndex < 0 || colIndex >= this._cols)
            {
                throw new Error("colIndex out of bounds");
            }
            for (let rowIndex = 0; rowIndex < Math.min(arr.length, this._rows); rowIndex++)
            {
                this.set(colIndex, rowIndex, arr[rowIndex]);
            }
        }

        /**
         * Returns a copy of the specified row.
         */
        public getRow(rowIndex: number): T[]
        {
            if (rowIndex < 0 || rowIndex >= this._rows)
            {
                throw new Error("rowIndex out of bounds");
            }
            return this._data.slice(rowIndex * this._cols, (rowIndex + 1) * this._cols);
        }

        /**
         * Sets the specified row of this array.
         */
        public setRow(rowIndex: number, arr: T[]): void
        {
            if (rowIndex < 0 || rowIndex >= this._rows)
            {
                throw new Error("rowIndex out of bounds");
            }
            for (let colIndex = 0; colIndex < Math.min(arr.length, this._cols); colIndex++)
            {
                this.set(colIndex, rowIndex, arr[colIndex]);
            }
        }

        /**
         * Converts this array into a native 2D array.
         */
        public toNative(): T[][]
        {
            const cols: T[][] = new Array<T[]>(this._cols);
            for (let colIndex = 0; colIndex < this._cols; colIndex++)
            {
                cols[colIndex] = this.getColumn(colIndex);
            }
            return cols;
        }

        /**
         * Creates a new boolean array.
         * For each cell in the new array:
         * - true: the value of the cell is equivalent
         * - false: the value of the cell is NOT equivalent
         * Returns null if the dimensions do not match.
         */
        public diff(other: Array2D<T>): Array2D<boolean> | null
        {
            if (this.rows !== other.rows || this.columns !== other.columns) { return null; }
            const tmp = new Array2D<boolean>(this._cols, this._rows);
            for (let rowIndex = 0; rowIndex < this._rows; rowIndex++)
            {
                for (let colIndex = 0; colIndex < this._cols; colIndex++)
                {
                    tmp.set(colIndex, rowIndex, this.get(colIndex, rowIndex) === other.get(colIndex, rowIndex));
                }
            }
            return tmp;
        }

        /**
         * Gets an array of all positions with the specified value.
         */
        public getPositions(value: T): { colIndex: number, rowIndex: number }[]
        {
            const positions: { colIndex: number, rowIndex: number }[] = [];
            for (let colIndex = 0; colIndex < this._cols; colIndex++)
            {
                for (let rowIndex = 0; rowIndex < this._rows; rowIndex++)
                {
                    if (this.get(colIndex, rowIndex) === value)
                    {
                        positions.push({ colIndex, rowIndex });
                    }
                }
            }
            return positions;
        }
    }
}