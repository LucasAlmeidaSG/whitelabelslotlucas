/// <reference path="../Disposable.ts" />

namespace RS
{
    /**
     * A resource pool used for caching frequently created short-lived object instances, to reduce pressure on memory and GC.
     */
    export class ResourcePool<T extends object> implements IDisposable
    {
        protected _pool: T[];
        protected _disposed: boolean = false;

        /** The number of objects currently pooled. */
        @CheckDisposed
        public get pooledCount() { return this._pool.length; }

        /** Gets if this pool has been disposed. */
        public get isDisposed() { return this._disposed; }

        public constructor(public readonly settings: ResourcePool.Settings<T>, initialCount = 0)
        {
            this._pool = new Array<T>(initialCount);
            for (let i = 0; i < initialCount; ++i)
            {
                this._pool[i] = settings.allocator();
            }
        }

        /** Gets an instance from this resource pool, or allocates one if needed. */
        @CheckDisposed
        public get(): T
        {
            if (this._pool.length === 0)
            {
                return this.settings.allocator();
            }
            else
            {
                return this._pool.pop();
            }
        }

        /** Releases an instance to this resource pool. */
        @CheckDisposed
        public release(instance: T): void
        {
            if (this.settings.releaser) { this.settings.releaser(instance); }
            this._pool.push(instance);
        }

        /** Disposes this resource pool and any resources within it. */
        public dispose(): void
        {
            if (this._disposed) { return; }
            for (const instance of this._pool)
            {
                this.disposeInstance(instance);
            }
            this._pool.length = 0;
            this._pool = null;
            this._disposed = true;
        }

        /** Disposes of the specified instance. */
        protected disposeInstance(instance: T): void
        {
            if (this.settings.disposer)
            {
                this.settings.disposer(instance);
            }
            else if (Is.disposable(instance))
            {
                instance.dispose();
            }
        }
    }

    export namespace ResourcePool
    {
        /**
         * Settings for a resource pool.
         */
        export interface Settings<T extends object>
        {
            /** Creates new instances when the pool is empty and an instance is requested. */
            allocator: () => T;

            /** Cleans instances when they are released to the pool. */
            releaser?: (obj: T) => void;

            /** Disposes instances when the pool itself is disposed. */
            disposer?: (obj: T) => void;
        }
    }
}