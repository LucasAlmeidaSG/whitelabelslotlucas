namespace RS
{
    /**
     * Encapsulates a mutable map of string keys to values.
     */
    export class ObservableMap<T> implements IDisposable
    {
        protected _map: Map<T>;
        protected _isDisposed: boolean = false;
        protected _resolves: { key: string; value: T|T[]; resolve: () => void; reject: (reason: any) => void; }[] = [];

        protected readonly _onChanged = createEvent<ObservableMap.OnChangedData<T>>();
        
        /** Published when the state has changed. */
        public readonly onChanged = this._onChanged.public;

        /** Gets if this observable has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets or sets the content of the map. */
        public get value()
        {
            const clone: Map<T> = {};
            for (const key in this._map)
            {
                if(this._map[key] != null) {
                    clone[key] = this._map[key];
                }
            }
            return clone;
        }
        public set value(value)
        {
            for (const key in this._map)
            {
                if (!(key in value))
                {
                    this.remove(key);
                }
            }
            for (const key in value)
            {
                if(value[key] != null) {
                    this.set(key, value[key]);
                }
            }
        }

        public constructor(initialValue: Map<T> = {})
        {
            this._map = initialValue;
        }

        /**
         * Gets if this map contains the specified key.
         * @param key 
         */
        public containsKey(key: string): boolean
        {
            return key in this._map;
        }

        /**
         * Gets the key for the specified value.
         * Returns null if the value is not found in the map.
         * Returns the first key found if the value is found multiple times in the map.
         * @param value 
         */
        public keyOf(value: T): string | null
        {
            for (const key in this._map)
            {
                if (this._map[key] === value)
                {
                    return key;
                }
            }
            return null;
        }

        /**
         * Gets if the specified value is found in this map.
         * @param value 
         */
        public containsValue(value: T): boolean
        {
            return this.keyOf(value) != null;
        }

        /**
         * Gets an item from the map.
         * Returns null if the key is not found.
         * @param key 
         */
        public get(key: string): T|null
        {
            const v = this._map[key];
            return v == null ? null : v;
        }

        /**
         * Sets an item in the map.
         * @param key 
         * @param value 
         */
        public set(key: string, newValue: T): void
        {
            const oldValue = this._map[key];
            if (oldValue === newValue) { return; }
            this._map[key] = newValue;
            this._onChanged.publish({ key, value: newValue });
            for (let i = this._resolves.length - 1; i >= 0; --i)
            {
                const resolveData = this._resolves[i];
                if (resolveData.key === key && Is.array(resolveData.value) ? resolveData.value.indexOf(newValue) !== -1 : resolveData.value === newValue)
                {
                    if(resolveData.resolve) {
                        resolveData.resolve();
                    }
                    this._resolves.splice(i, 1);
                }
            }
        }

        /**
         * Removes an item from the map.
         * @param key 
         */
        public remove(key: string): void
        {
            if (!this.containsKey(key)) { return; }
            this.set(key, null);
            delete this._map[key];
        }

        /**
         * Returns a promise that resolves when this observable's value has changed to the specified value.
         * @param value 
         */
        public for(key: string, value: T): PromiseLike<void>;

        /**
         * Returns a promise that resolves when this observable's value has changed to one of the specified values.
         * @param value 
         */
        public for(key: string, value: T[]): PromiseLike<void>;

        /**
         * Returns a promise that resolves when this observable's value has changed to the specified value.
         * @param value 
         */
        public for(key: string, value: T|T[]): PromiseLike<void>
        {
            return new Promise<void>((resolve, reject) =>
            {
                const curVal = this.get(key);
                if (Is.array(value) && value.indexOf(curVal) !== -1)
                {
                    resolve();
                }
                else if (!Is.array(value) && curVal === value)
                {
                    resolve();
                }
                else
                {
                    this._resolves.push({ key, value, resolve, reject });
                }
            });
        }

        /**
         * Disposes this observable.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }
            for (const resolve of this._resolves)
            {
                //resolve.reject && resolve.reject("disposed");
                if(resolve.reject) {
                    resolve.reject("disposed");
                }
            }
            this._resolves.length = 0;
            this._resolves = null;
            this._map = null;
            this._isDisposed = true;
        }
    }

    export namespace ObservableMap
    {
        export interface OnChangedData<T>
        {
            key: string;
            value: T;
        }
    }
}