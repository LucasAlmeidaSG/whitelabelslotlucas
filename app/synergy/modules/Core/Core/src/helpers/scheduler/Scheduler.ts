/// <reference path="../../decorators/Callbacks.ts" />
/// <reference path="../Observable.ts" />

namespace RS
{
    const verboseLogging = false;

    /**
     * A scheduler that works with arbitrary async tasks.
     * Tasks won't be completed in parallel to each other.
     * Tasks may be suspended if more important tasks are added, or the priorities of existing tasks change.
     * Operations track a set of tasks and their progress, and hold their own priority.
     */
    @HasCallbacks
    export class Scheduler
    {
        protected _allTasks: Scheduler.ITask[] = [];
        protected _allOps: Scheduler.Operation[] = [];

        protected _currentTask: Scheduler.ITask|null = null;

        protected _suspended: boolean = false;
        protected _asyncPending: boolean = false;
        protected _minPri: number|null = null;
        protected _autoRefresh: boolean = true;

        /** Gets or sets if this scheduler should be working on tasks or not. */
        public get suspended() { return this._suspended; }
        public set suspended(value)
        {
            if (value === this._suspended) { return; }
            this.log(value ? "suspended" : "unsuspended");
            this._suspended = value;
            if (this._autoRefresh) { this.refresh(); }
        }

        /** Gets or sets the minimum priority a task should be to work on it, or null for unbound. */
        public get minPriority() { return this._minPri; }
        public set minPriority(value)
        {
            if (value === this._minPri) { return; }
            this.log(`minPri = ${value}`);
            this._minPri = value;
            if (this._autoRefresh) { this.refresh(); }
        }

        /** Gets or sets if this scheduler should refresh whenever something happens. */
        public get autoRefresh() { return this._autoRefresh; }
        public set autoRefresh(value) { this._autoRefresh = value; }

        /**
         * Adds a new task to the scheduler.
         * @param task 
         */
        public addTask(task: Scheduler.ITask): void
        {
            task.onCompleted(this.refresh);
            task.onProgressChanged(this.refresh);
            this._allTasks.push(task);
            this._allTasks.sort((a, b) => b.priority - a.priority);
            this.log(task, "added");
            if (this._autoRefresh) { this.refresh(); }
        }

        /**
         * Adds a new operation to the scheduler.
         * @param task 
         */
        public addOperation(tasks: Scheduler.ITask[], priority: number = 0, debugName?: string): Scheduler.IOperation
        {
            const errEvent = createEvent<string>();
            for (const task of tasks)
            {
                if (this._allTasks.indexOf(task) === -1 && !task.isComplete)
                {
                    task.onCompleted(this.refresh);
                    task.onProgressChanged(this.refresh);
                    this._allTasks.push(task);
                }
                task.onErrored((err) => errEvent.publish(err));
            }
            this._allTasks.sort((a, b) => b.priority - a.priority);
            const exOp: Scheduler.IOperation =
            {
                progress: new Observable(0),
                complete: new Observable(false),
                onErrored: errEvent.public
            };
            const op: Scheduler.Operation =
            {
                tasks: tasks.slice(),
                priority,
                public: exOp,
                debugName: `op:${debugName}`,
                totalWeight: tasks.map((t) => t.progressWeight).reduce((a, b) => a + b, 0),
                completeWeight: 0
            };
            op.tasks.sort((a, b) => b.priority - a.priority);
            this._allOps.push(op);
            this._allOps.sort((a, b) => b.priority - a.priority);
            this.log(op, "added");
            if (this._autoRefresh) { this.refresh(); }
            return exOp;
        }        

        /**
         * Checks all state and reorganises current workload if required.
         */
        @Callback
        public refresh(): void
        {
            for (const op of this._allOps)
            {
                op.public.progress.value = this.computeOperationProgress(op);
            }
            if (this._asyncPending) { return; }
            const workingTask = this.identifyWorkingTask();
            if (workingTask !== this._currentTask)
            {
                if (this._currentTask != null)
                {
                    this._asyncPending = true;
                    this.log(this._currentTask, "suspending");
                    this._currentTask.suspend()
                        .then(() =>
                        {
                            this._asyncPending = false;
                            this._currentTask = null;
                            this.refresh();
                        });
                    return;
                }
                if (workingTask != null)
                {
                    this._asyncPending = true;
                    this._currentTask = workingTask;
                    this.log(this._currentTask, "resuming");
                    this._currentTask.resume()
                        .then(() =>
                        {
                            this._asyncPending = false;
                            this._currentTask = workingTask;
                            this.refresh();
                        });
                }
                else
                {
                    this.log("idle");
                }
            }
        }

        protected computeOperationProgress(op: Scheduler.Operation): number
        {
            const progress = op.completeWeight + op.tasks.map((t) => t.progress * t.progressWeight).reduce((a, b) => a + b, 0);
            return progress / op.totalWeight;
        }

        protected identifyWorkingTask(): Scheduler.ITask | null
        {
            // Are there operations to work on?
            while (this._allOps.length > 0)
            {
                const op = this._allOps[0];
                if (this._minPri != null && op.priority > this._minPri) { break; }

                // Find the first task in the list that is incomplete
                while (op.tasks.length > 0)
                {
                    const task = op.tasks[0];
                    if (task.isComplete)
                    {
                        if (this._currentTask === task) { this._currentTask = null; }
                        op.completeWeight += task.progress * task.progressWeight;
                        op.tasks.shift();
                        const idx = this._allTasks.indexOf(task);
                        if (idx !== -1) { this._allTasks.splice(idx, 1); }
                        task.onCompleted.off(this.refresh);
                        task.onProgressChanged.off(this.refresh);
                        this.log(task, "complete");
                    }
                    else
                    {
                        return task;
                    }
                }

                // They're all complete
                op.public.progress.value = 1.0;
                op.public.complete.value = true;
                this.log(op, "complete");
                this._allOps.shift();
            }

            // Clean out all complete tasks
            for (let i = this._allTasks.length - 1; i >= 0; --i)
            {
                const task = this._allTasks[i];
                if (task.isComplete)
                {
                    this.log(task, "complete");
                    if (this._currentTask === task) { this._currentTask = null; }
                    task.onCompleted.off(this.refresh);
                    task.onProgressChanged.off(this.refresh);
                    this._allTasks.splice(i, 1);
                }
            }

            // Find the first task in the list that is incomplete and falls above minPri
            for (const task of this._allTasks)
            {
                if (this._minPri == null || task.priority <= this._minPri)
                {
                    return task;
                }
            }

            // No task found
            return null;
        }

        protected log(task: Scheduler.ITask, message: string): void;

        protected log(op: Scheduler.Operation, message: string): void;

        protected log(message: string): void;

        protected log(p1: Scheduler.ITask | Scheduler.Operation | string, p2?: string): void
        {
            if (!verboseLogging) { return; }
            if (Is.string(p1))
            {
                Log.debug(`[Scheduler] ${p1}`);
            }
            else
            {
                Log.debug(`[Scheduler] [${p1.debugName || "noname"}] ${p2}`);
            }
        }        
    }

    export namespace Scheduler
    {
        export interface Operation
        {
            /** What priority is this operation? (lower value = more important). */
            priority: number;

            totalWeight: number;

            completeWeight: number;
    
            /** All tasks within this operation. */
            tasks: ITask[];

            /** Outwards facing operation data. */
            public: IOperation;

            debugName?: string;
        }

        export interface IOperation
        {
            /** Gets the current progress of the task (0-1). */
            readonly progress: Observable<number>;

            /** Gets whether the operation is complete. */
            readonly complete: Observable<boolean>;

            /** Published when an error occured. */
            readonly onErrored: IEvent<string>;
        }
    }
}
