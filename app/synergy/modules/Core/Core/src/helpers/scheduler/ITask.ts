namespace RS.Scheduler
{
    export interface ITask
    {
        /** What priority is this task? (lower value = more important). */
        readonly priority: number;

        /** Published when the task was running and has now been completed. */
        readonly onCompleted: IEvent;

        /** Published when the progress of the task has changed. */
        readonly onProgressChanged: IEvent<number>;

        /** Published when the task was cancelled due to an error. */
        readonly onErrored: IEvent<string>;

        /** Gets if the task is currently running. */
        readonly isRunning: boolean;

        /** Gets if the task is currently complete. */
        readonly isComplete: boolean;

        /** Gets the current progress of the task (0-1). */
        readonly progress: number;

        /** Gets the progress weight of this task relative to other tasks. */
        readonly progressWeight: number;

        /** Gets a name for this task (used for debugging e.g. log messages). */
        readonly debugName: string|null;

        /** Suspends this tasks. */
        suspend(): PromiseLike<void>;

        /** Resumes/starts this task. */
        resume(): PromiseLike<void>;
    }
}