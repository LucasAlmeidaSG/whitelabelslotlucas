/// <reference path="IArbiter.ts" />

namespace RS
{
    /**
     * Represents an active declaration on a Arbiter.
     */
    export class ArbiterHandle<T> implements IArbiterHandle<T>
    {
        protected _arbiter: Arbiter<T>;
        protected _value: T;

        /** The arbiter to which this handle is associated with. */
        public get arbiter() { return this._arbiter; }

        /** The state that this handle is trying to enforce. */
        public get value() { return this._value; }

        /** Gets if this handle has been disposed or not. */
        public get isDisposed() { return this._arbiter == null; }

        public constructor(arbiter: Arbiter<T>, value: T)
        {
            this._arbiter = arbiter;
            this._value = value;
        }

        /**
         * Removes this handle from the Arbiter.
         */
        public dispose()
        {
            if (this._arbiter == null) { return; }
            this._arbiter.undeclare(this);
            this._arbiter = null;
            this._value = null;
        }
    }

    /**
     * An object encapsulating a single unit of state.
     * - Subscribers can request to be notified when the state of the Arbiter has updated.
     * - Users can declare a modification to the Arbiter's state which is resolved alongside other requests.
     * - Users can release their declaration at any time.
     */
    export class Arbiter<T> extends Observable<T> implements IArbiter<T>
    {
        protected _defaultValue: T;
        protected _handles: ArbiterHandle<T>[] = [];
        protected _resolver: ArbiterResolver<T>;

        public constructor(resolver: SimpleArbiterResolver<T>, defaultState?: T);
        public constructor(resolver: ComplexArbiterResolver<T>, defaultState?: T);
        public constructor(resolver: ArbiterResolver<T>, defaultState?: T)
        {
            super(defaultState);
            this._resolver = resolver;
            this._defaultValue = defaultState;
            this.resolveState();
        }

        /**
         * Makes a declaration to modify this Arbiter's state.
         */
        public declare(state: T): ArbiterHandle<T>
        {
            const arbiterHandle = new ArbiterHandle<T>(this, state);
            this._handles.push(arbiterHandle);
            this.resolveState();
            return arbiterHandle;
        }

        /**
         * Removes a previously declared handle.
         * Don't use this - call handle.dispose() instead.
         */
        public undeclare(handle: ArbiterHandle<T>): void
        {
            const idx = this._handles.indexOf(handle);
            if (idx !== -1)
            {
                this._handles.splice(idx, 1);
                this.resolveState();
            }
        }

        public toString()
        {
            return `[Arbiter: ${this.valueToString(this._value)}]`;
        }

        protected resolveState(): void
        {
            const oldState = this._value;
            let newState = this._handles.length ? this._handles[0].value : this._defaultValue;
            if (RS.Is.simpleArbiterResolver(this._resolver))
            {
                for (let i = 1; i < this._handles.length; i++)
                {
                    newState = this._resolver(newState, this._handles[i].value);
                }
            }
            else if (this._handles.length > 0)
            {
                const values = this._handles.map((e) => e.value);
                newState = this._resolver(values);
            }
            if (newState !== oldState)
            {
                this.value = newState;
            }
        }
    }

    export namespace Arbiter
    {
        export const OrResolver: SimpleArbiterResolver<boolean> = (a, b) => a || b;
        export const AndResolver: SimpleArbiterResolver<boolean> = (a, b) => a && b;
        export const MaxResolver: SimpleArbiterResolver<number> = (a, b) => Math.max(a, b);
        export const MinResolver: SimpleArbiterResolver<number> = (a, b) => Math.min(a, b);

        /** Resolver that returns the most recent value, causing the arbiter to behave like a stack. */
        export const StackResolver: SimpleArbiterResolver<any> = (a, b) => b;

        /**
         * Resolver that returns true if there are more true values than false values.
         * In case of ties, returns true.
         */
        export const ConsensusResolver: ComplexArbiterResolver<boolean> = (values: boolean[]) =>
        {
            let trueCount = 0;
            let falseCount = 0;
            for (const val of values)
            {
                val ? ++trueCount : ++falseCount;
            }
            return trueCount >= falseCount;
        }

        /**
         * Resolver that returns the average value of all declared values.
         */
        export const AverageResolver: ComplexArbiterResolver<number> = (values: number[]) =>
        {
            const sum = values.reduce((curr, prev) => curr + prev, 0);
            if (values.length > 0)
            {
                return sum / values.length;
            }
            else
            {
                Log.warn("Attempt to divide by 0, returning 0 instead.");
                return 0;
            }
        }

        /**
         * Resolver that returns the sum of all declared values.
         */
        export const SumResolver: SimpleArbiterResolver<number> = (a, b) => a + b;
    }
}
