/// <reference path="Read.ts"/>
namespace RS.URL
{
    /**
     * Asynchronous.
     * Returns whether or not the given URL exists.
     * @param url The address to check.
     * @param retry Whether or not to retry on network failures. If false, network errors will be thrown. Defaults to false.
     * @param useHead Whether or not to use a HEAD request. Defaults to true.
     */
    export async function exists(url: string, retry?: boolean, useHead?: boolean): Promise<boolean>;
    /**
     * Asynchronous.
     * Returns whether or not the given URL exists.
     * @param settings AjaxSettings
     * @param retry Whether or not to retry on network failures. If false, network errors will be thrown. Defaults to false.
     */
    export async function exists(settings: Request.AJAXSettings, retry?: boolean): Promise<boolean>;
    export async function exists(p1: Request.AJAXSettings | string, retry: boolean = false, useHead: boolean = true): Promise<boolean>
    {
        //resolve params
        let settings: Request.AJAXSettings;
        if (Is.string(p1))
        {
            settings = {url: p1};
            settings.method = useHead ? "HEAD" : "GET";
        }
        else
        {
            settings = p1;
            if (!settings.method)
            {
                settings.method = "HEAD";
            }
        }

        try
        {
            await Request.ajax(settings, Request.AJAXResponseType.Text);
            return true;
        }
        catch (error)
        {
            if (error instanceof Request.ResponseError)
            {
                if (error.code === 404) { return false; }

                if (error.code === 405)
                {
                    if (settings.method === "HEAD") { return await exists({...settings, method: "GET"}, retry); }
                    // Apparently even GET is not allowed to that URL
                    // Rather than error out, we can say it actually doesn't exist
                    // Since it kinda doesn't if it's not GET-accessible
                    Log.warn(`[URL] GET was not allowed to ${settings.url}`);
                    return false;
                }
            }
            else
            {
                // DNS not resolved or other unknown error; return false
                Log.warn(`[URL] Failed to access ${settings.url}`);
                return false;
            }

            if (retry)
            {
                return await exists(settings);
            }

            throw error;
        }
    }

    export namespace Decoders
    {
        export const URI = decodeURI;
        export const Unicode = (value: string) => value.split("%").map((ch) => ch.charCodeAt(0)).join("");
    }

    export namespace Encoders
    {
        export const URI = encodeURI;
        export const Unicode = (value: string) => value.split("").map((ch) => `%${ch.charCodeAt(0)}`).join("");
    }

    /**
     * Gets a URL parameter from the current window URL.
     * @param name       Name of the URL parameter to extract
     * @param defaultVal Default value to return if parameter not found. Defaults to null.
     * @param decoder    Decoder to use to parse the string
     * @returns          The decoded URL parameter
     */
    export function getParameter(name: string, defaultVal: string | null = null, decoder: MapFunc<string> = Decoders.URI): string | null
    {
        const match = RegExp(`${name}=(.+?)(&|$)`).exec(location.search);
        return match ? decoder(match[1]) : defaultVal;
    }

    /**
     * Gets a boolean URL parameter from the current window URL.
     * @param name       Name of the URL parameter to extract
     * @param defaultVal Default value to return if parameter not found. Defaults to false.
     * @param decoder    Decoder to use to parse the string
     * @returns          The decoded URL parameter
     */
    export function getParameterAsBool(name: string, defaultVal: boolean = false, decoder?: MapFunc<string>): boolean
    {
        const val = getParameter(name, "", decoder);
        return Read.boolean(val, defaultVal);
    }

    /**
     * Gets a numeric URL parameter from the current window URL.
     * @param name       Name of the URL parameter to extract
     * @param defaultVal Default value to return if parameter not found. Defaults to 0.
     * @param decoder    Decoder to use to parse the string
     * @returns          The decoded URL parameter
     */
    export function getParameterAsNumber(name: string, defaultVal: number = 0, decoder?: MapFunc<string>): number
    {
        const val = getParameter(name, "", decoder);
        return Read.number(val, defaultVal);
    }

    /**
     * Gets a URL object parameter from the current window URL.
     * @param name       Name of the URL parameter to extract
     * @param decoder    Decoder to use to parse the string
     * @returns          The decoded URL parameter
     */
    export function getParameterAsObject(name: string, decoder?: MapFunc<string>): object | null
    {
        const val = getParameter(name, "", decoder);
        return readObject(val);
    }

    /**
     * Gets an enum URL parameter from the current window URL.
     * @param enumObj    Enum to read the parameter as
     * @param name       Name of the URL parameter to extract
     * @param defaultVal Default value to return if parameter not found. Defaults to null.
     * @param decoder    Decoder to use to parse the string
     * @returns          The decoded URL parameter
     */
    export function getParameterAsEnumVal<TEnum extends Enum<TEnum>>(enumObj: TEnum, name: string, defaultVal: TEnum[keyof TEnum] = null, decoder?: MapFunc<string>): TEnum[keyof TEnum] | null
    {
        const val = getParameter(name, "", decoder);
        const enumVal = Read.enumVal(enumObj, val, null);
        if (val && enumVal == null) { Log.warn(`Invalid enum value '${val}'; valid values are ${Object.keys(enumObj).join(", ")}`); }
        return enumVal == null ? defaultVal as any : enumVal;
    }

    /**
     * Removes the given parameters from a URL.
     *
     * @param params    The names of parameters to be removed
     * @param url       The optional URL to modify (defaults to the current window URL)
     * @returns         The modified URL
     */
    export function removeParameters(params: string[], url: string = location.href): string
    {
        for (const param of params)
        {
            const regExp = `((\\?|\\&)${param}=(?:[^\\&]*))`;
            const results = new RegExp(regExp).exec(url);
            if (results)
            {
                const removeFrom = results.index;
                const removeLen = results[0].length;

                const leftSide = url.slice(0, removeFrom);
                let rightSide = url.slice(removeFrom + removeLen);
                if (results[2] == "?" && rightSide.length > 0)
                {
                    rightSide = "?" + rightSide.slice(1);
                }

                url = leftSide + rightSide;
            }
        }

        return url;
    }

    /**
     * Sets the given URL parameters in a URL.
     *
     * @param params    An object containing parameter names and values to be set
     * @param url       The optional URL to modify (defaults to the current window URL)
     * @returns         The modified URL
     */
    export function setParameters(params: { [name: string]: any }, url: string = location.href, encoder: MapFunc<string> = Encoders.URI): string
    {
        url = removeParameters(Object.keys(params), url);

        let delimiter = url.indexOf("?") > -1 ? "&" : "?";
        for (const name in params)
        {
            const value = params[name];
            const stringValue = Is.string(value) ? value : stringify(value);
            const encodedValue = encoder(stringValue);
            url += `${delimiter}${name}=${encodedValue}`;

            if (delimiter === "?")
            {
                delimiter = "&";
            }
        }

        return url;
    }

    const objectMarker = "$";
    const objectDelimiter = "!";

    function read(str: string): any
    {
        if (str[0] === "'" && str[str.length - 1] === "'") { return str.substr(1, str.length - 2); }
        if (str === "true") { return true; }
        if (str === "false") { return false; }
        if (str === "null") { return null; }
        if (str === "undefined") { return undefined; }
        const number = Read.number(str);
        if (number) { return number; }
        throw new Error(`Could not read value ${str}`);
    }

    function readObject(str: string): { [key: string]: any } | null
    {
        if (str[0] !== objectMarker) { return null; }
        str = str.substr(1);

        const delimiter = objectDelimiter;
        const parts = str.split(delimiter);
        const obj: { [key: string]: any } = {};
        for (let i = 0; i < parts.length; i += 2)
        {
            const key = parts[i];
            const value = parts[i + 1];
            obj[key] = read(value);
        }

        return obj;
    }

    function stringify(val: any): string
    {
        if (Is.number(val) || Is.boolean(val) || val == null) { return `${val}`; }
        if (Is.string(val)) { return `'${val}'`; }
        if (Is.object(val)) { return stringifyObject(val); }
        throw new Error(`Could not URI-serialise value ${val}`);
    }

    function stringifyObject(obj: object): string
    {
        const delimiter = objectDelimiter;

        let addDelimiter = false;
        const parts: string[] = ["$"];
        for (const key in obj)
        {
            const value = obj[key];
            const valueString = stringify(value);
            if (addDelimiter) { parts.push(delimiter); }
            parts.push(key, delimiter, valueString);
            addDelimiter = true;

        }

        return parts.join("");
    }

    /** Downloads the given file. */
    export function download(blob: Blob): void;
    /** Downloads the given file with the given name (on most browsers). Include the file extension. */
    export function download(name: string, blob: Blob): void;
    export function download(p1: Blob | string, blob?: Blob): void
    {
        if (!Is.string(p1)) { blob = p1; }
        // create download picture link
        const a = document.createElement("a");
        document.body.appendChild(a);
        a.style.cssText = "display: none";
        const url = window.URL.createObjectURL(blob); // createObjectURL() will leak memory.
        a.href = url;
        if (Is.string(p1)) { a.download = p1; }
        // click link to download picture
        a.click();
        window.URL.revokeObjectURL(url);
        a.parentNode.removeChild(a);
    }
}
