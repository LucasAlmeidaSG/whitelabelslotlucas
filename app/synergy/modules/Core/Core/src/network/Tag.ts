namespace RS.Request
{
    export type TagName = "img"|"audio"|"video";

    /**
     * Settings for an tag-based request.
     */
    export interface TagSettings<K extends TagName>
    {
        tagName: K;
        url: string;
        method?: string;
        auth?: { username: string; password: string; };
        postData?: string|Document;
    }

    /**
     * Sends an asynchronous tag-based request.
     * @param settings 
     * @param responseType 
     */
    export function tag<K extends TagName>(settings: TagSettings<K>): PromiseLike<HTMLElementTagNameMap[K]>
    {
        return new Promise((resolve, reject) =>
        {
            const el = document.createElement(settings.tagName);
            el.crossOrigin = "anonymous";
            el.addEventListener("load", (ev) =>
            {
                document.body.removeChild(el);
                resolve(el);
            });
            el.addEventListener("error", (ev) =>
            {
                document.body.removeChild(el);
                reject("Tag based load failed");
            });
            el.style.position = "fixed";
            el.style.width = "0px";
            el.style.height = "0px";
            el.src = settings.url;
            document.body.appendChild(el);
        });
    }
}