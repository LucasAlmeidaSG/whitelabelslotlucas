namespace RS.Request
{
    /**
     * Encapsulates a type of response.
     */
    export enum AJAXResponseType
    {
        Text,
        XML,
        JSON,
        Binary,
        Blob,
        ObjectURL
    }

    /**
     * Settings for an ajax request.
     */
    export interface AJAXSettings
    {
        url: string;
        method?: string;
        headers?: Readonly<Map<string>>;
        auth?: { username: string; password: string; };
        postData?: string|Document|FormData|Blob|ArrayBufferView|ArrayBuffer|URLSearchParams;
        withCredentials?: boolean;
    }

    export class ResponseError extends BaseError
    {
        constructor(public readonly code: number, statusText: string) { super(`${code} ${statusText}`); }
    }

    /**
     * Sends an asynchronous ajax request, formatting the response as plain-text.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType.Text): PromiseLike<string>;

    /**
     * Sends an asynchronous ajax request, formatting the response as an XML document.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType.XML): PromiseLike<Document>;

    /**
     * Sends an asynchronous ajax request, parsing the response as JSON.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType.JSON): PromiseLike<object>;

    /**
     * Sends an asynchronous ajax request, formatting the response as an ArrayBuffer.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType.Binary): PromiseLike<ArrayBuffer>;

    /**
     * Sends an asynchronous ajax request, formatting the response as a blob.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType.Blob): PromiseLike<Blob>;

    /**
     * Sends an asynchronous ajax request, formatting the response as an object URL pointing to a blob.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType.ObjectURL): PromiseLike<string>;

    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType): PromiseLike<string | Blob | ArrayBuffer | object | Document>;

    export function ajax(settings: AJAXSettings, responseType: AJAXResponseType): PromiseLike<any>
    {
        return new Promise((resolve, reject) =>
        {
            const req = new XMLHttpRequest();
            if (settings.withCredentials != null) { req.withCredentials = settings.withCredentials; }
            req.addEventListener("load", (ev) =>
            {
                if (req.status !== 200)
                {
                    reject(new ResponseError(req.status, req.statusText));
                    return;
                }
                switch (responseType)
                {
                    case AJAXResponseType.Text:
                        resolve(req.responseText);
                        break;
                    case AJAXResponseType.XML:
                        resolve(req.responseXML);
                        break;
                    case AJAXResponseType.JSON:
                        if (Is.string(req.response))
                        {
                            let newResponse: any;
                            try
                            {
                                newResponse = JSON.parse(req.response);
                            }
                            catch (err)
                            {
                                reject(err);
                                break;
                            }
                            resolve(newResponse);
                        }
                        else
                        {
                            resolve(req.response);
                        }
                        break;
                    case AJAXResponseType.Binary:
                    case AJAXResponseType.Blob:
                        resolve(req.response);
                        break;
                    case AJAXResponseType.ObjectURL:
                        resolve(window.URL.createObjectURL(req.response));
                        break;
                }
            });
            req.addEventListener("error", (ev) =>
            {
                reject(ev);
            });
            req.addEventListener("abort", (ev) =>
            {
                reject("aborted");
            });

            req.open(settings.method || "GET", settings.url, true);
            if (settings.auth)
            {
                req.setRequestHeader("Authorization", `Basic ${btoa(`${settings.auth.username}:${settings.auth.password}`)}`);
            }

            switch (responseType)
            {
                case AJAXResponseType.Text:
                    req.responseType = "text";
                    break;
                case AJAXResponseType.XML:
                    req.responseType = "document";
                    break;
                case AJAXResponseType.JSON:
                    req.responseType = "json";
                    break;
                case AJAXResponseType.Binary:
                    req.responseType = "arraybuffer";
                    break;
                case AJAXResponseType.Blob:
                case AJAXResponseType.ObjectURL:
                    req.responseType = "blob";
                    break;
            }

            if (settings.headers)
            {
                for (const header in settings.headers)
                {
                    req.setRequestHeader(header, settings.headers[header]);
                }
            }

            req.send(settings.postData);
        });
    }
}
