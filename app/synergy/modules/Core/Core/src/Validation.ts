namespace RS
{
    const isArray = Array.isArray;

    function stringifyArg(arg: number | boolean | string | object | ((...args) => any) | undefined): string
    {
        if (arg == null) { return `${arg}`; }
        if (Is.func(arg)) { return `${arg}`; }
        if (Is.object(arg))
        {
            const stringVal = arg.toString();
            if (stringVal !== "[object Object]") { return stringVal; }
        }
        // Deep stringify arrays and simple object args using JSON.stringify
        return Dump.stringify(arg, () => true);
    }

    /**
     * Applies validation to an argument using the given validator.
     *
     * Equivalent to Validate.Argument({ validator, name, error }).
     *
     * The method must also be decorated with Validate.Method.
     */
    export function ArgIs<T>(validator: Validate.IValidator<T>, name?: string, error?: string): ParameterDecorator;

    /**
     * Applies validation to an argument using the given validators. \
     * The value must pass all validators.
     *
     * Equivalent to Validate.Argument({ validator: AllOf(...validators), name, error }).
     *
     * The method must be decorated with Validate.Method.
     */
    export function ArgIs<T>(validators: Validate.IValidator<T>[], name?: string, error?: string): ParameterDecorator;

    export function ArgIs<T>(validators: Validate.IValidator<T>[] | Validate.IValidator<T>, name?: string, error?: string): ParameterDecorator
    {
        if (validators == null) { throw new Error("ArgIs requires 1 or more validators"); }
        if (Is.array(validators))
        {
            if (validators.length === 0) { throw new Error("ArgIs requires 1 or more validators"); }
            const validator = validators.length === 1 ? validators[0] : Validate.AllOf(validators[0], validators[1], ...validators.slice(2));
            return Validate.Argument({ validator, name, error });
        }
        else
        {
            const validator = validators;
            return Validate.Argument({ validator, name, error });
        }
    }

    /** Contains validation decorators. See RS.Validation */
    export namespace Validate
    {
        /** Applies validation to an argument according to certain settings. The method must also be decorated with Validate.Method. */
        export const Argument = Decorators.Tag.create<ArgumentSettings<any>>(Decorators.TagKind.Parameter);

        /** Applies validation for a method according to certain settings. One or more parameters should also be decorated with Validate.Argument. */
        export function Method(settings: MethodSettings = {}): MethodDecorator
        {
            return function (target: object, key: string, descriptor: PropertyDescriptor): PropertyDescriptor
            {
                return {
                    ...descriptor,
                    value: function (this, ...args)
                    {
                        const settingsMap = Argument.get(this, Is.func(target) ? Decorators.MemberType.Static : Decorators.MemberType.Instance, key);
                        for (let i = 0; i < args.length; i++)
                        {
                            const validationSettings = settingsMap[i];
                            if (!validationSettings) { continue; }

                            const arg = args[i];

                            const method = settings.name || key;
                            const param = validationSettings.name || `parameter ${i}`;

                            let err: string;
                            try
                            {
                                err = validationSettings.validator.validate(arg);
                            }
                            catch (err)
                            {
                                throw new Error(`${method}: ${param} validator threw an error for argument ${stringifyArg(arg)}:\n${err}`);
                            }

                            if (!err) { continue; }

                            const failedBecause = validationSettings.error || err;
                            throw new Error(`${method}: ${param} ${failedBecause} (got ${stringifyArg(arg)})`);
                        }

                        return descriptor.value.apply(this, args);
                    }
                };
            };
        }

        export interface MethodSettings
        {
            logContext?: RS.Logging.ILogger & RS.Logging.ILogger.IExtensions;
            /** Method name override. */
            name?: string;
        }

        export interface ArgumentSettings<T>
        {
            validator: IValidator<T>;
            /** Parameter name override. */
            name?: string;
            /** Error message override e.g. "must be really cool". */
            error?: string;
        }

        export interface IValidator<T>
        {
            /** Returns whether or not value meets this validator's requirements. */
            validate(value: T | null | undefined): string | null;
        }

        /** Validates that a value is valid according to all given validators. */
        export function AllOf<T>(first: IValidator<T>, second: IValidator<T>, ...rest: IValidator<T>[]): IValidator<T>;

        export function AllOf<T>(...validators: IValidator<T>[]): IValidator<T>
        {
            if (validators.some((validator) => validator == null)) { throw new Error("AllOf validators must not be null"); }
            if (validators.length === 0) { throw new Error("AllOf requires 1 or more validators"); }
            return {
                validate: function (value)
                {
                    for (const validator of validators)
                    {
                        const err = validator.validate(value);
                        if (err)
                        {
                            return err;
                        }
                    }
                    return null;
                }
            };
        }

        /** Validates that a value is valid according to any one of the given validators. */
        export function AnyOf<T>(first: IValidator<T>, second: IValidator<T>, ...rest: IValidator<T>[]): IValidator<T>;

        export function AnyOf<T>(...validators: IValidator<T>[]): IValidator<T>
        {
            if (validators.some((validator) => validator == null)) { throw new Error("AnyOf validators must not be null"); }
            if (validators.length === 0) { throw new Error("AnyOf requires 1 or more validators"); }
            return {
                validate: function (value)
                {
                    let errs: string;
                    for (const validator of validators)
                    {
                        const err = validator.validate(value);
                        if (!err)
                        {
                            return null;
                        }

                        if (errs)
                        {
                            errs += ` or ${err}`;
                        }
                        else
                        {
                            errs = err;
                        }
                    }
                    return errs;
                }
            };
        }

        /** Validates that a value is one of the given values. */
        export function OneOf<T>(first: T, second: T, ...rest: T[]): IValidator<T>;

        export function OneOf<T>(...values: T[]): IValidator<T>
        {
            if (values.length === 0) { throw new Error("OneOf requires 1 or more validators"); }
            return {
                validate: function (value)
                {
                    if (value == null) { return null; }
                    if (values.indexOf(value) === -1) { return `must be ${values.join(" or ")}`; }
                    return null;
                }
            };
        }

        /** Validates that a value is not null or undefined. */
        export const NotNull: IValidator<any> =
        {
            validate: function (value)
            {
                if (value == null) { return "must not be null"; }
                return null;
            }
        };

        /** Validates that a value is a non-NaN number. Contains additional numeric validators. Does not require that the value not be null. */
        export const Number =
        {
            validate: function (value)
            {
                if (value == null) { return null; }
                if (!Is.number(value)) { return "must be a number"; }
                return null;
            },

            /** Validates that a number is greater than or equal to min and less than or equal to max. */
            InRange: function (min: number, max: number): IValidator<number>
            {
                return {
                    validate: function (value)
                    {
                        if (value < min) { return `must be at least ${min}`; }
                        if (max < value) { return `must not be greater than ${max}`; }
                        return null;
                    }
                };
            }
        };

        /** Validates that a value is a non-NaN whole number. Does not require that the value not be null. */
        export const Integer: IValidator<number> =
        {
            validate: function (value)
            {
                if (value == null) { return null; }
                if (!Is.integer(value)) { return "must be an integer"; }
                return null;
            }
        };

        /** Validates that a value is a string. Contains additional text validators. Does not require that the value not be null. */
        export const String =
        {
            validate: function (value: string)
            {
                if (value == null) { return null; }
                if (typeof value !== "string") { return "must be a string"; }
                return null;
            },

            /** Validates that a string is not empty. Does not validate that the value is a string. */
            NotEmpty:
            {
                validate: function (value: string)
                {
                    if (value == null) { return null; }
                    if (value.length === 0) { return "must not be empty"; }
                    return null;
                }
            } as IValidator<string>,

            /** Validates that a string matches the given regular expression. Does not validate that the value is a string. */
            Match: function (regExp: RegExp): IValidator<string>
            {
                return {
                    validate: function (value)
                    {
                        if (value == null) { return null; }
                        if (!regExp.test(value)) { return `must match ${regExp}`; }
                        return null;
                    }
                };
            }
        };

        /** Validates that a value is a boolean. Does not require that the value not be null. */
        export const Boolean: IValidator<boolean> =
        {
            validate: function (value)
            {
                if (value == null) { return null; }
                if (value !== true && value !== false) { return "must be a boolean"; }
                return null;
            }
        };

        /** Validates that a value is a function. Does not require that the value not be null. */
        export const Func: IValidator<(...args: any[]) => any> =
        {
            validate: function (value)
            {
                if (value == null) { return null; }
                if (typeof value !== "function") { return "must be a function"; }
                return null;
            }
        };

        /** Validates that a value is an array. Does not require that the value not be null. */
        export const Array =
        {
            validate: function (value: ReadonlyArray<any>)
            {
                if (value == null) { return null; }
                if (!isArray(value)) { return "must be an array"; }
                return null;
            },

            /** Validates that an array is not empty. Does not validate that the value is an array. */
            NotEmpty:
            {
                validate: function (value: ReadonlyArray<any>)
                {
                    if (value == null) { return null; }
                    if (value.length === 0) { return "must not be empty"; }
                    return null;
                }
            }
        };

        /** Validates that a value is an array, whose elements all pass a given inner validator. Does not require that the value not be null. */
        export function ArrayOf<T>(validator: IValidator<T>): IValidator<T[]>
        {
            return {
                validate: function (value)
                {
                    if (value == null) { return null; }
                    if (!isArray(value)) { return "must be an array"; }
                    for (const member of value)
                    {
                        const err = validator.validate(member);
                        if (err)
                        {
                            return `members ${err}`;
                        }
                    }
                    return null;
                }
            };
        };

        /** Validates that a value is an object. Does not match arrays. Does not require that the value not be null. */
        export const Object =
        {
            validate: function (value: object)
            {
                if (value == null) { return null; }
                if (typeof value !== "object" || isArray(value)) { return "must be an object"; }
                return null;
            }
        };
    }
}