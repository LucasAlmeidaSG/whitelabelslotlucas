namespace RS.Reflect
{
    // TODO es6/2015 deprecate these functions or at least re-evaluate all calls; they only really exist because super.get and super.set aren't transpiled to es5
    // TS AST transformer could also be used to implement super.get/set?

    export function getValueUsingDescriptor<T>(target: object, descriptor: PropertyDescriptor): T
    {
        if (descriptor.get) { return descriptor.get.call(target); }
        if (descriptor.value) { return descriptor.value; }
        return undefined;
    }

    export function setValueUsingDescriptor<T>(target: Object, descriptor: PropertyDescriptor, value: T): void
    {
        if (descriptor.writable === false)
        {
            Log.warn("Tried to set an unwritable property");
            return;
        }

        if (descriptor.set) { return descriptor.set.call(target, value); }
        if (descriptor.value) { descriptor.value = value; return; }
    }
}