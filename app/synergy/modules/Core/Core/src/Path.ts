/**
 * Path manipulation utilities.
 */
namespace RS.Path
{
    const pathSeperator = "/";
    const urlProtocolRegex = /([a-z]+):\/\//i;

    interface IntermediatePath
    {
        protocol?: string;
        segments: string[];
    }

    namespace IntermediatePath
    {
        export function parse(str: string): IntermediatePath
        {
            const protocol = urlProtocolRegex.exec(str);
            if (protocol)
            {
                return {
                    protocol: protocol[1],
                    segments: str.substr(protocol[0].length).split(pathSeperator)
                };
            }
            else
            {
                return { segments: str.split(pathSeperator) };
            }
        }

        export function clean(path: IntermediatePath): void
        {
            path.segments = path.segments
                .map((seg) => seg.trim())
                .filter((seg) => seg !== "");
        }

        export function stringify(path: IntermediatePath): string
        {
            if (path.protocol)
            {
                return `${path.protocol}://${path.segments.join("/")}`;
            }
            else
            {
                return path.segments.join("/");
            }
        }
    }

    /**
     * Combines multiple path segments into one.
     * @param segments
     */
    export function combine(...segments: string[]): string
    {
        const ip = IntermediatePath.parse(segments.join(pathSeperator));
        IntermediatePath.clean(ip);
        return IntermediatePath.stringify(ip);
    }

    /**
     * Gets the file name of the specified path (e.g. "my/test/file.txt" => "file.txt").
     * @param path 
     */
    export function fileName(path: string): string
    {
        const ip = IntermediatePath.parse(path);
        return ip.segments[ip.segments.length - 1];
    }

    /**
     * Gets the directory name of the specified path (e.g. "my/test/file.txt" => "my/test").
     * @param path 
     */
    export function directoryName(path: string): string
    {
        const ip = IntermediatePath.parse(path);
        if (ip.segments.length > 0) { ip.segments.pop(); }
        return IntermediatePath.stringify(ip);
    }

    /**
     * Resolves any "." and ".." segments of the specified path.
     * @param path 
     */
    export function normalise(path: string): string
    {
        const ip = IntermediatePath.parse(path);
        const curPath: string[] = [];
        for (const seg of ip.segments)
        {
            if (seg === "..")
            {
                if (curPath.length > 0)
                {
                    curPath.pop();
                }
                else
                {
                    curPath.push(seg);
                }
            }
            else if (seg !== ".")
            {
                curPath.push(seg);
            }
        }
        return IntermediatePath.stringify({ protocol: ip.protocol, segments: curPath });
    }

    /**
     * Gets the path from "from" to "to".
     * @param from 
     * @param to 
     */
    export function relative(from: string, to: string): string
    {
        const fromIP = IntermediatePath.parse(from);
        const toIP = IntermediatePath.parse(to);

        if (fromIP.protocol !== toIP.protocol)
        {
            throw new Error("Tried to get relative path with mismatching protocols");
        }
        
        let equalUntil = 0;
        for (let i = 0, l = Math.min(fromIP.segments.length, toIP.segments.length); i < l; ++i)
        {
            const fromSeg = fromIP.segments[i];
            const toSeg = toIP.segments[i];
            if (fromSeg !== toSeg) { break; }
            equalUntil++;
        }

        if (equalUntil === fromIP.segments.length)
        {
            // e.g. "foo/bar" -> "foo/bar/other/thing.txt"
            // equalUntil = 2
            return IntermediatePath.stringify({
                protocol: toIP.protocol,
                segments: toIP.segments
                    .slice(equalUntil)
            });
        }
        else
        {
            // e.g. "foo/lol" -> foo/bar/other/thing.txt"
            // equalUntil = 1
            const arr: string[] = [];
            for (let i = 0, l = fromIP.segments.length - equalUntil; i < l; ++i)
            {
                arr.push("..");
            }
            return IntermediatePath.stringify({
                protocol: toIP.protocol,
                segments: arr
                    .concat(toIP.segments.slice(equalUntil))
            });
        }
    }

    /**
     * Finds the extension of the given file path (e.g. "my/test/file.txt" => ".txt").
     * @param path 
     */
    export function extension(path: string): string
    {
        const matchResult = /(\.[a-zA-Z0-9]+)$/g.exec(path);
        if (!matchResult || matchResult.length <= 1) { return ""; }
        return matchResult[1];
    }

    /**
     * Replaces the extension of the given file path (e.g. "my/test/file.txt" => "my/test/file.json" with newExt = ".json").
     * @param path 
     * @param newExt 
     */
    export function replaceExtension(path: string, newExt: string): string
    {
        return path.replace(/(\.[a-zA-Z0-9]+)$/g, newExt);
    }
}