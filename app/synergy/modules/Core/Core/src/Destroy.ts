/// <reference path="logging/Log.ts" />

namespace RS
{
    export type DestroyHandler = (obj: any) => void;
    const destroyHandlers: DestroyHandler[] = [];

    /** Marks the specified property for cleanup when the owning class is destroyed. */
    export function destroy(target: object, propertyKey: string);

    /** Destroys all properties on the target object that are marked for cleanup. */
    export function destroy<T extends object>(obj: T): void;

    export function destroy(obj?: object, propertyKey?: string): void
    {
        Log.warn("destroy has been deprecated. Use AutoDispose and the IDisposable pattern instead.");
        if (obj == null) { return; }
        interface TargetClassEx
        {
            _destroyables: string[];
        }
        if (propertyKey != null)
        {
            const tClass = obj.constructor as (Function & TargetClassEx);
            if (tClass._destroyables == null) { tClass._destroyables = []; }
            tClass._destroyables.push(propertyKey);
            return;
        }
        const targetClass = obj.constructor as (Function & TargetClassEx);
        if (targetClass._destroyables == null) { return; }
        for (let i = 0; i < targetClass._destroyables.length; i++)
        {
            const key = targetClass._destroyables[i];
            if (obj.hasOwnProperty(key))
            {
                const value = obj[key];
                if (value != null && Is.object(value))
                {
                    for (let j = 0; j < destroyHandlers.length; j++)
                    {
                        try
                        {
                            destroyHandlers[j](value);
                        }
                        catch (err)
                        {
                            Log.error(err);
                        }
                    }
                    obj[key] = null;
                }
            }
        }
    }

    /**
     * Registers a destroy handler.
     * All destroy handlers will be called on the value of a property when it is cleaned up.
    **/
    export function addDestroyHandler(handler: DestroyHandler)
    {
        destroyHandlers.push(handler);
    }

    /**
     * Default destroy handlers.
    **/
    namespace DefaultDestroyHandlers
    {
        function disabler(obj: any): void
        {
            if (obj.enabled === undefined) { return; }
            obj.enabled = false;
        }
        addDestroyHandler(disabler);

        function destroyer(obj: any): void
        {
            if (!Is.func(obj.destroy)) { return; }
            obj.destroy.call(obj);
        }
        addDestroyHandler(destroyer);

        // function cancelEmitter(obj: any): void
        // {
        //     if (!(obj instanceof RS.Particles.ParticleEmitter)) { return; }
        //     obj.cancelEmit();
        // }
        // addDestroyHandler(cancelEmitter);

        // function soundStopper(obj: any): void
        // {
        //     if (!(obj instanceof RS.Sound)) { return; }
        //     obj.stop();
        //     RS.SoundManager.removeSound(obj);
        // }
        // addDestroyHandler(soundStopper);
    }
}