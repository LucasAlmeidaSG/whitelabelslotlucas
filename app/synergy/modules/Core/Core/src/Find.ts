/** Contains functions for reducing arrays to single items. */
namespace RS.Find
{
    /** Compares two array item scores. */
    export type ScoreComparer = (a: number, b: number) => boolean;
    export type ArrayLikeWithSplice<T> = ArrayLike<T> & { splice: ((pos: number, quantity?: number) => ArrayLike<T>) };

    export namespace ScoreComparers
    {
        export const Higher: ScoreComparer = (a, b) => a > b;
        export const Lower: ScoreComparer = (a, b) => a < b;
        export const CloserToZero: ScoreComparer = (a, b) => Lower(Math.abs(a), Math.abs(b));
        export const FartherFromZero: ScoreComparer = (a, b) => Higher(Math.abs(a), Math.abs(b));
    }

    /** Returns a score for a given array item. */
    export type Scorer<T> = (a: T) => number;

    const defaultScorer: Scorer<number> = (a) => a;

    /**
     * Find the item from the specified array with the best score.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     * @param scoreIsBetter Is a better than b?
     */
    export function best<T>(from: ArrayLike<T>, scorer: Scorer<T>, scoreIsBetter: ScoreComparer): T | null
    {
        if (from.length === 0) { return null; }
        let bestItem = from[0];
        let bestScore = scorer(bestItem);
        for (let i = 1, l = from.length; i < l; ++i)
        {
            const item = from[i];
            const score = scorer(item);
            if (scoreIsBetter(score, bestScore))
            {
                bestItem = item;
                bestScore = score;
            }
        }
        return bestItem;
    }

    /**
     * Find the item from the specified array with the worst score.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     * @param scoreIsBetter Is a better than b?
     */
    export function worst<T>(from: ArrayLike<T>, scorer: Scorer<T>, scoreIsBetter: ScoreComparer): T | null
    {
        return best(from, scorer, (a, b) => scoreIsBetter(b, a));
    }

    /**
     * Find the item from the specified array with the highest score.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     */
    export function highest<T>(from: ArrayLike<T>, scorer: Scorer<T>): T | null;
    /**
     * Find the highest number from the specified array.
     * @param from Array to pick from.
     */
    export function highest(from: ArrayLike<number>): number | null;
    
    export function highest<T>(from: ArrayLike<T> | ArrayLike<number>, scorer: Scorer<T | number> = defaultScorer): T | number | null
    {
        return best<T | number>(from, scorer, ScoreComparers.Higher);
    }

    /**
     * Find the item from the specified array with the lowest score.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     */
    export function lowest<T>(from: ArrayLike<T>, scorer: Scorer<T>): T | null
    /**
     * Find the lowest number from the specified array.
     * @param from Array to pick from.
     */
    export function lowest(from: ArrayLike<number>): number | null
    
    export function lowest<T>(from: ArrayLike<T> | ArrayLike<number>, scorer: Scorer<T | number> = defaultScorer): T | number | null
    {
        return best<T | number>(from, scorer, ScoreComparers.Lower);
    }

    /**
     * Find the item from the specified array with the score closest to zero.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     */
    export function closestToZero<T>(from: ArrayLike<T>, scorer: Scorer<T>): T | null;
    /**
     * Find the number from the specified array closest to zero.
     * @param from Array to pick from.
     */
    export function closestToZero(from: ArrayLike<number>): number | null;
    
    export function closestToZero<T>(from: ArrayLike<T> | ArrayLike<number>, scorer: Scorer<T | number> = defaultScorer): T | number | null
    {
        return best<T | number>(from, scorer, ScoreComparers.CloserToZero);
    }

    /**
     * Find the item from the specified array with the score farthest from zero.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     */
    export function farthestFromZero<T>(from: ArrayLike<T>, scorer: Scorer<T>): T | null;
    /**
     * Find the number from the specified array farthest from zero.
     * @param from Array to pick from.
     */
    export function farthestFromZero(from: ArrayLike<number>): number | null;
    
    export function farthestFromZero<T>(from: ArrayLike<T> | ArrayLike<number>, scorer: Scorer<T | number> = defaultScorer): T | number | null
    {
        return best<T | number>(from, scorer, ScoreComparers.FartherFromZero);
    }

    /**
     * Finds the first item from the specified array whose cumulative value is greater than the given value.
     * Useful for dealing with arrays where each item represents a delta from a previous item, such as a weighted random.
     * @param from Array to pick from.
     * @param scorer Function to convert items to numeric values.
     * @param value Value to find the ceiling for in the given array.
     */
    export function ceiling<T>(from: ArrayLike<T>, scorer: Scorer<T>, value: number): T | null;
    /**
     * Finds the first number from the specified array whose cumulative value is greater than the given value.
     * Useful for dealing with arrays where each number represents a delta from a previous item, such as a weighted random.
     * @param from Array to pick from.
     * @param value Value to find the ceiling for in the given array.
     */
    export function ceiling(from: ArrayLike<number>, value: number): number | null;

    export function ceiling<T>(from: ArrayLike<T> | ArrayLike<number>, p2: Scorer<T> | number, p3?: number): T | number | null
    {
        let value: number;
        let scorer: Scorer<T | number>;
        if (RS.Is.number(p2))
        {
            value = p2;
            scorer = defaultScorer;
        }
        else
        {
            scorer = p2;
            value = p3;
        }

        let total = 0;
        for (let i = 0, l = from.length; i < l; i++)
        {
            const item = from[i];
            total += scorer(item);
            if (total > value) { return from[i]; }
        }
        return null;
    }

    /**
     * Find the sum of all items from the specified array using the given scoring function.
     * @param array Array to sum the scores of.
     * @param scorer Function to convert items to numeric values.
     */
    export function sum<T>(array: ArrayLike<T>, scorer: Scorer<T>): number;
    /**
     * Find the sum of all numbers from the specified array.
     * @param array Array to sum.
     */
    export function sum(array: ArrayLike<number>): number;
    export function sum<T>(array: ArrayLike<T> | ArrayLike<number>, scorer: Scorer<T | number> = defaultScorer): number
    {
        let total = 0;
        for (let i = 0, l = array.length; i < l; ++i)
        {
            const item = array[i];
            total += scorer(item);
        }
        return total;
    }

    /**
     * Finds an index in a sorted array using a given director function.
     * The resulting index will either point to an exact match, or to where an exact match would be if there was one.
     * Can therefore be used both for searching and inserting. Useful for preventing duplicates when adding to an array.
     * Returns the leftmost match, so when removing, you can keep removing until there are no more matches at the returned index.
     * Current implementation involves a binary search.
     * NOTE: if the input array is not sorted, then the resulting index will not be either.
     * @param sortedArray Sorted array to find index into.
     * @param directFn Comparable to compareFn in Array.sort(). If negative or zero, moves left; if positive, moves right.
     */
    export function sortedIndex<T>(sortedArray: ArrayLike<T>, directFn: (item: T, index: number, array: ArrayLike<T>) => number): number
    {
        if (sortedArray.length === 0) { return 0; }

        let left = 0, 
            right = sortedArray.length,
            index = Math.floor((right + left) * 0.5);

        while ((right - left) > 1)
        {
            const direction = directFn(sortedArray[index], index, sortedArray);

            if (direction > 0)
            {
                // Move to right half
                left = index;
            }
            else
            {
                // Move to left half
                right = index;
            }
            
            index = Math.floor((right + left) * 0.5);
        }

        const finalDirection = directFn(sortedArray[index], index, sortedArray);
        return finalDirection > 0 ? index + 1 : index;
    }

    /**
     * Find the item from the specified array matching the given predicate.
     * @param from Array to find a matching item from.
     * @param predicate Function returning whether the given item is a match.
     */
    export function matching<T>(from: ArrayLike<T>, predicate: (item: T) => boolean): T | null
    {
        for (let i = 0, l = from.length; i < l; ++i)
        {
            const item = from[i];
            if (predicate(item)) { return item; }
        }
        return null;
    }

    /**
     * Removes any instance of a specific item from the specified array 
     * @param from the specified array
     * @param predicate 
     */
    export function remove<T>(from: ArrayLikeWithSplice<T>, predicate: (item: T) => boolean): ArrayLike<T> | null
    {
        for (let i = 0; i < from.length; ++i)
        {
            const item = from[i];
            if (predicate(item))
            {
                from.splice(i, 1);
                --i;
            }
        }
        return from;
    }
}