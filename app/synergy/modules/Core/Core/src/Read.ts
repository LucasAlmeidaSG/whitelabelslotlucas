/// <reference path="Is.ts"/>
/** Contains functions for reading strings as various types of value. */
namespace RS.Read
{
    const truthy = ["true", "1", "y", "yes"];
    const falsy = ["false", "0", "n", "no"];

    /**
     * Parses the given string as an boolean.
     * @return a boolean (or null if defaultValue = null and value is not a boolean)
     */
    export function boolean(value: string, defaultVal: boolean | null = null): boolean | null
    {
        const lower = value.toLowerCase();
        if (truthy.indexOf(lower) !== -1) { return true; }
        if (falsy.indexOf(lower) !== -1) { return false; }
        return defaultVal;
    }

    /**
     * Parses the given string as an integer.
     * @return a number (or null if defaultValue = null and value is not an integer)
     */
    export function integer(value: string, defaultVal: number | null = null): number | null
    {
        if (value == "") { return defaultVal; }
        const parsed = Number(value);
        return as(parsed, Is.integer, defaultVal);
    }

    /**
     * Parses the given string as a number.
     * @return a number (or null if defaultValue = null and value is not a number)
     */
    export function number(value: string, defaultVal: number | null = null): number | null
    {
        if (value == "") { return defaultVal; }
        const parsed = Number(value);
        return as(parsed, Is.number, defaultVal);
    }

    /**
     * Parses the given string as an object.
     * @return an object or null
     */
    export function object(value: string): object | null
    {
        let parsed: any = null;
        try { parsed = JSON.parse(value); } catch { /* Do nothing, we'll return null. */ }
        return as(parsed, Is.object);
    }

    /**
     * Parses the given string as an array of a specific type.
     * @return an array of T and null values
     */
    export function arrayOf<T>(value: string, innerReader: (value: string) => (T | null), delimiter: string = ","): T[]
    {
        const strings = value.split(delimiter);
        const values = strings.map((str) => innerReader(str));
        return values;
    }

    /**
     * Parses the given string as a value of the given enum.
     * @param enumObj The enum to read the string as
     * @param caseSensitive If true, the given string must match the case of an enum key. Defaults to false.
     */
    export function enumVal<TEnum extends Enum<TEnum>>(enumObj: TEnum, value: string, defaultVal: TEnum[keyof TEnum] = null, caseSensitive = false): TEnum[keyof TEnum] | null
    {
        const int = integer(value);
        if (int != null) { return (int in enumObj) ? int : defaultVal as any; }

        for (const key in enumObj)
        {
            // Numeric enum, break
            if (Is.number(Number(key))) { break; }
            const enumValue = enumObj[key];
            if (enumValue === value) { return enumValue as any; }
        }

        const mappedVal = enumObj[value];
        if (mappedVal != null) { return mappedVal; }

        if (!caseSensitive)
        {
            const valueLower = value.toLowerCase();
            const keys = Object.keys(enumObj);
            for (const key in enumObj)
            {
                if (key.toLowerCase() === valueLower) { return enumObj[key] as any; }
            }
        }

        return defaultVal as any;
    }
}