namespace RS
{
    export interface ExtensionProperty<TClass, TValue>
    {
        get?: (this: TClass) => TValue;
        set?: (this: TClass) => void;
        value?: TValue;
    }

    export type ExtensionMethod<TClass> = (this: TClass, ...args: any[]) => any;

    type ExtensionMap<TClass, TExtensions> = { [P in keyof TExtensions]: ExtensionMethod<TClass> | ExtensionProperty<TClass, any> };

    export namespace Strategy
    {
        export type Class<T extends object> = { new(): T; };

        /**
         * Encapsulates an object which provides instances of a strategy.
         */
        export interface IProvider<T extends object, TExtensions extends (ExtensionMap<T, TExtensions> | {}) = {}>
        {
            /** Gets an instance of this strategy to use. */
            get(): T & TExtensions;

            /** Registers the specified class as a concrete class of this factory. */
            register(cl: Class<T>, asDefault?: boolean): void;
        }

        /**
         * Declares a strategy of the specified type.
         * @param type
         * @param suppressErrors If true, calls to get() will return null upon failure instead of throwing.
         * @param allowOverrides If true, multiple concrete classes can be registered, and the latest one is the one that will be used when instantiating.
         */
        export function declare<T extends object>(type?: Type, suppressErrors?: boolean, allowOverrides?: boolean | Overridable): IProvider<T>

        /**
         * Declares a strategy of the specified type.
         * @param type
         * @param suppressErrors If true, calls to get() will return null upon failure instead of throwing.
         * @param allowOverrides If true, multiple concrete classes can be registered, and the latest one is the one that will be used when instantiating.
         * @param extensions An object containing methods and properties to be applied to registered implementations.
         */
        export function declare<T extends object, TExtensions extends ExtensionMap<T, TExtensions>>(type?: Type, suppressErrors?: boolean, allowOverrides?: boolean | Overridable, extensions?: TExtensions): IProvider<T, TExtensions>

        export function declare<T extends object, TExtensions extends ExtensionMap<T, TExtensions>>(type = Type.Instance, suppressErrors: boolean = false, allowOverrides: boolean | Overridable = false, extensions?: TExtensions): IProvider<T, TExtensions>
        {
            if (Is.boolean(allowOverrides))
            {
                allowOverrides = allowOverrides ? Overridable.Always : Overridable.Never;
            }

            switch (type)
            {
                case Type.Instance:
                    return new FactoryProvider(!suppressErrors, allowOverrides, extensions);
                case Type.Singleton:
                    return new SingletonProvider(!suppressErrors, allowOverrides, extensions);
                default:
                    return null;
            }
        }

        /**
         * Defines the type of a strategy.
         */
        export enum Type
        {
            /** Strategies are independent instances. */
            Instance,

            /** Strategies are a single shared instance. */
            Singleton
        }

        export enum Overridable
        {
            /** Disallow overriding entirely. */
            Never,
            /** Allow overriding only if get() has not yet been called. */
            BeforeUse,
            /** Allow overriding at any time. */
            Always
        }

        // Private implementation //

        abstract class BaseProvider<T extends object, TExtensions extends ExtensionMap<T, TExtensions>> implements IProvider<T, TExtensions>
        {
            protected _class: Class<T & TExtensions> | null = null;

            private readonly _overridable: Overridable;
            private readonly _errorIfNull: boolean;
            private readonly _extensions: TExtensions;

            private _baseClass: Class<T>;
            private _hasBeenUsed: boolean;
            private _isClassDefault: boolean;

            public constructor(errorIfNull: boolean, overridable: Overridable, extensions: TExtensions)
            {
                this._errorIfNull = errorIfNull;
                this._overridable = overridable;
                this._extensions = extensions;

                this._hasBeenUsed = false;
                this._isClassDefault = false;
            }

            public register(cl: Class<T>, asDefault = false): void
            {
                if (cl == null) { throw new Error("Tried to register null concrete implementation for strategy"); }
                if (cl === this._baseClass) { return; }

                if (asDefault)
                {
                    if (this._class)
                    {
                        if (this._isClassDefault)
                        {
                            throw new Error("Tried to register default concrete implementation for strategy when one was already registered");
                        }
                        else
                        {
                            throw new Error("Tried to register default concrete implementation for strategy after a non-default implementation was registered");
                        }
                    }
                }
                else
                {
                    if (this._class != null)
                    {
                        switch (this._overridable)
                        {
                            case Overridable.Never:
                            {
                                if (!this._isClassDefault)
                                {
                                    throw new Error("Tried to register concrete implementation for strategy when one was already registered");
                                }

                                // Fall through to Overridable.BeforeUse behaviour if set to default
                            }

                            case Overridable.BeforeUse:
                            {
                                if (this._hasBeenUsed)
                                {
                                    throw new Error("Tried to register concrete implementation for strategy which had already been accessed");
                                }
                            }
                        }
                    }
                }

                this._isClassDefault = asDefault;
                this._baseClass = cl;
                this._class = this._extensions ? this.applyExtensions(cl) : cl as Class<T & TExtensions>;
            }

            public get(): T & TExtensions
            {
                if (this._class == null)
                {
                    if (!this._errorIfNull) { return null; }
                    throw new Error("Tried to instantiate strategy which is not backed by a concrete implementation");
                }

                const inst = this.getInstance();
                this._hasBeenUsed = true;
                return inst;
            }

            protected abstract getInstance(): T & TExtensions;

            private applyExtensions(target: Class<T>): Class<T & TExtensions>
            {
                const BaseClass = target as Class<object>;
                class ExtendedClass extends BaseClass {}

                const extensions = this._extensions;
                for (const key in extensions)
                {
                    const extension: ExtensionMethod<T> | ExtensionProperty<T, any> = extensions[key];
                    if (Is.func(extension))
                    {
                        (ExtendedClass as Class<object>).prototype[key] = extension;
                    }
                    else
                    {
                        const desc: PropertyDescriptor & ThisType<ExtendedClass> = {};
                        if (extension.get) { desc.get = extension.get; }
                        if (extension.set) { desc.set = extension.set; }
                        if (extension.value) { desc.set = extension.value; }
                        Object.defineProperty(ExtendedClass.prototype, key, desc);
                    }
                }

                return ExtendedClass as Class<T & TExtensions>;
            }
        }

        class FactoryProvider<T extends object, TExtensions extends ExtensionMap<T, TExtensions>> extends BaseProvider<T, TExtensions>
        {
            public getInstance()
            {
                return new this._class();
            }
        }

        class SingletonProvider<T extends object, TExtensions extends ExtensionMap<T, TExtensions>> extends BaseProvider<T, TExtensions>
        {
            private _instance: (T & TExtensions) | null = null;

            public getInstance()
            {
                return this._instance || (this._instance = new this._class());
            }

            /** Registers the specified class as a concrete class of this factory. */
            public register(cl: Class<T>, asDefault?: boolean): void
            {
                super.register(cl, asDefault);

                if (this._instance)
                {
                    if (Is.disposable(this._instance)) { this._instance.dispose(); }
                    this._instance = null;
                }
            }
        }
    }
}