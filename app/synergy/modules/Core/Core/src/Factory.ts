namespace RS.Factory
{
    /** Represents a factory modifier function. */
    export type ModifierFunction<T> = (obj: T) => void;

    /**
     * Encapsulates information about a single factory.
     * @param T     The type of object created by this factory.
     */
    export interface Info<T extends object = object>
    {
        /**
         * The class name of the object being created.
         */
        type: { new(...params: any[]): T };

        /**
         * Modifiers that are applied after an object has been created.
         */
        modifiers: ModifierFunction<T>[];
    }

    /** Array of all registered factories. */
    const factories: Info[] = [];

    /**
     * Registers a factory.
     */
    export function register<T extends object>(info: Info<T>): void
    {
        factories.push(info);
    }

    /**
     * Finds all factories that create the specified object type.
     */
    export function find<T extends object>(type: { new(...params: any[]): T }, searchInheritanceChain: boolean = true): Info<T>[]
    {
        return factories.filter((i) => i.type === type || (searchInheritanceChain && type.prototype instanceof i.type)) as Info<T>[];
    }

    /**
     * Applies all factory modifies to the specified object.
     */
    export function applyModifiers<T extends object>(obj: T): number
    {
        if (obj == null) { return 0; }
        const facts = find(obj.constructor as new(...params: any[]) => T, false);
        let cnt = 0;
        for (let i = 0; i < facts.length; i++)
        {
            const info = facts[i];
            for (let j = 0; j < info.modifiers.length; j++)
            {
                info.modifiers[j](obj);
                cnt++;
            }
        }
        return cnt;
    }

    /**
     * Registers a factory modifier for the specified object type.
     */
    export function registerModifier<T extends object>(type: { new(...params: any[]): T }, modifier: ModifierFunction<T>): void
    {
        const facts = find(type, false);
        for (let i = 0; i < facts.length; i++)
        {
            facts[i].modifiers.push(modifier);
        }
    }
}