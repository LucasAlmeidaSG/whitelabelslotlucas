namespace RS.Clipboard
{
    // https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript

    function fallbackCopyTextToClipboard(text)
    {
        const textArea = document.createElement("textarea");
        textArea.value = text;
        document.body.appendChild(textArea);
        
        textArea.focus();
        textArea.select();

        try
        {
            const successful = document.execCommand('copy');
            if (!successful) { throw new Error("Copy command failed"); }
        }
        finally
        {
            document.body.removeChild(textArea);
        }
    }

    export async function writeText(text: string)
    {
        if (!navigator["clipboard"])
        {
            fallbackCopyTextToClipboard(text);
            return;
        }
        await navigator["clipboard"]["writeText"](text);
    }
}