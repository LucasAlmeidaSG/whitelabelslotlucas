namespace RS
{
    function stringifyArg(arg: number | boolean | string | object | ((...args) => any) | undefined): string
    {
        if (arg == null) { return `${arg}`; }
        if (Is.func(arg)) { return `${arg}`; }
        if (Is.object(arg))
        {
            const stringVal = arg.toString();
            if (stringVal !== "[object Object]") { return stringVal; }
        }
        // Deep stringify arrays and simple object args using JSON.stringify
        return Dump.stringify(arg, () => true);
    }

    /** Logs to console when this method is invoked. */
    export function DebugLog(context: string | Logging.IContextualLogger, messageIn?: string | RS.Func<string>, includeArgs?: boolean): MethodDecorator;

    /** Logs to console when this method is invoked. */
    export function DebugLog(context: string | Logging.IContextualLogger, messageIn?: string | RS.Func<string>, messageOut?: string, includeArgs?: boolean): MethodDecorator;

    export function DebugLog(context: string | Logging.IContextualLogger, messageIn?: string | RS.Func<string>, p2?: string | boolean, p3?: boolean): MethodDecorator
    {
        const logger = Is.string(context) ? Logging.ILogger.get().getContext(context) : context;

        let messageOut: string, includeArgs: boolean;
        if (Is.boolean(p2))
        {
            includeArgs = p2;
        }
        else
        {
            messageOut = p2;
            includeArgs = p3 == null ? messageIn == null : p3;
        }

        return function (target: object, propertyKey: string, propertyDescriptor: PropertyDescriptor): void
        {
            if (!propertyDescriptor)
            {
                Log.warn("DebugLog decorator applied to an invalid method");
                return;
            }
            
            const propMessageIn = messageIn || propertyKey;
            const oldFunc = propertyDescriptor.value;
            propertyDescriptor.value = function (this: object, ...args: any[])
            {
                const prefixMessage = includeArgs
                    ? `${propMessageIn}(${args.map((a) => stringifyArg(a)).join(", ")})`
                    : `${propMessageIn}`;

                logger.log(prefixMessage, Logging.LogLevel.Debug);
                const retVal = oldFunc.apply(this, args);
                if (messageOut) { logger.log(`${messageOut}`, Logging.LogLevel.Debug); }
                return retVal;
            };
            Object.defineProperty(target, propertyKey, propertyDescriptor);
        };
    }
}