/// <reference path="Tag.ts" />
/// <reference path="../Util.ts" />

namespace RS
{
    export interface CallbackMethod extends Function
    {
        owner: object;
    }

    interface CtorWithCallbacks extends Function
    {
        _hasCallbacks?: boolean;
        _callbacks?: (string | symbol)[];
    }

    /**
     * Marks the specified class as as having callbacks.
     * Note that this only works for non-static members of classes.
     */
    export const HasCallbacks = Decorators.Tag.createDataless(Decorators.TagKind.Class, function (oldCtor)
    {
        return Util.hookClass(oldCtor, function ()
        {
            bindCallbacks(this, Decorators.MemberType.Instance);
            oldCtor.apply(this, arguments);
        });
    });

    /**
     * Marks the specified method as a callback, allowing it to be passed directly into Event.on.
     * The owner class must be marked with HasCallbacks, if this is applied to an instance member.
     */
    export const Callback = Decorators.Tag.createDataless(Decorators.TagKind.Method);

    /**
     * Binds all @Callback marked methods to this on the specified object.
     * Should not be required to call this manually unless working with static classes.
     */
    export function bindCallbacks(target: object, type: Decorators.MemberType)
    {
        const callbacks = Callback.get(target, type);
        if (callbacks != null)
        {
            for (const methodName of callbacks)
            {
                target[methodName] = target[methodName].bind(target);
                // Log.debug(`@Callback: bound ${methodName} for ${target}`);
            }
        }
    }
}