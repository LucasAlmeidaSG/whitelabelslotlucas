namespace RS
{
    /** Used to avoid creating a big object unless it's actually going to be used. */
    export function Lazy<TClass, T>(generator: (obj: TClass) => T): PropertyDecorator
    {
        return function (target: TClass, key: string)
        {
            // Normal property, use a closure getter.
            const newPropertyDescriptor: PropertyDescriptor =
            {
                get: function (this: TClass)
                {
                    const val = generator(this);
                    const replacementDescriptor: PropertyDescriptor =
                    {
                        get: () => val,
                        enumerable: true,
                        configurable: true
                    };
                    Object.defineProperty(this, key, replacementDescriptor);
                    return val;
                },
                enumerable: true,
                configurable: true
            };

            return newPropertyDescriptor;
        };
    }
}