namespace RS.Decorators
{
    /**
     * Metadata for a class.
     */
    export interface Metadata
    {
        tags: any[];
        methods: Map<{ tags: any[], parameters: any[][] }>;
        properties: Map<{ tags: any[] }>;
        staticMethods: Map<{ tags: any[], parameters: any[][] }>;
        staticProperties: Map<{ tags: any[] }>;
    }

    /**
     * Creates new empty metadata.
     */
    function createMetadata(): Metadata
    {
        return {
            tags: [],
            methods: {},
            properties: {},
            staticMethods: {},
            staticProperties: {}
        };
    }

    /**
     * Gets, or creates if not found, blank metadata for the specified class or class instance.
     * Does NOT consider inheritance.
     * @param target
     */
    export function getMetadata(target: Function|object, createIfNotFound = true): Metadata
    {
        interface WithMetadata extends Function
        {
            _metadata?: Metadata;
        }
        if (Is.func(target))
        {
            const baseClass = Object.getPrototypeOf(target) as WithMetadata;
            const targetEx = target as WithMetadata;
            if (targetEx._metadata == null || targetEx._metadata === baseClass._metadata)
            {
                if (!createIfNotFound) { return null; }
                targetEx._metadata = createMetadata();
            }
            return targetEx._metadata;
        }
        else
        {
            const ctor = Object.getPrototypeOf(target).constructor;
            if (Is.func(ctor))
            {
                return getMetadata(ctor, createIfNotFound);
            }
            else
            {
                return null;
            }
        }
    }

    /**
     * Reads metadata for the given class or class instance.
     * Considers inheritance and flattens inherited metadata.
     * @param target
     */
    export function readMetadata(target: Function|object): Readonly<Metadata>
    {
        if (Is.func(target))
        {
            const newMetadata = createMetadata();
            const chain = Util.getInheritanceChain(target, true);
            for (const ctor of chain)
            {
                const metadata = getMetadata(ctor, false);
                if (metadata != null)
                {
                    mergeTags(metadata.tags, newMetadata.tags);
                    for (const methodName in metadata.methods)
                    {
                        if (newMetadata.methods[methodName] == null)
                        {
                            newMetadata.methods[methodName] = { tags: [], parameters: [] };
                        }
                        mergeTags(metadata.methods[methodName].tags, newMetadata.methods[methodName].tags);
                        mergeParams(metadata.methods[methodName].parameters, newMetadata.methods[methodName].parameters);
                    }
                    for (const propertyName in metadata.properties)
                    {
                        if (newMetadata.properties[propertyName] == null)
                        {
                            newMetadata.properties[propertyName] = { tags: [] };
                        }
                        mergeTags(metadata.properties[propertyName].tags, newMetadata.properties[propertyName].tags);
                    }
                    for (const methodName in metadata.staticMethods)
                    {
                        if (newMetadata.staticMethods[methodName] == null)
                        {
                            newMetadata.staticMethods[methodName] = { tags: [], parameters: [] };
                        }
                        mergeTags(metadata.staticMethods[methodName].tags, newMetadata.staticMethods[methodName].tags);
                        mergeParams(metadata.staticMethods[methodName].parameters, newMetadata.staticMethods[methodName].parameters);
                    }
                    for (const propertyName in metadata.staticProperties)
                    {
                        if (newMetadata.staticProperties[propertyName] == null)
                        {
                            newMetadata.staticProperties[propertyName] = { tags: [] };
                        }
                        mergeTags(metadata.staticProperties[propertyName].tags, newMetadata.staticProperties[propertyName].tags);
                    }
                }
            }
            return newMetadata;
        }
        else
        {
            const ctor = Object.getPrototypeOf(target).constructor;
            if (Is.func(ctor))
            {
                return readMetadata(ctor);
            }
            else
            {
                return null;
            }
        }
    }

    /**
     * Merges src params into dst params, taking care not to override any.
     * @param src
     * @param dst
     */
    function mergeParams(src: ReadonlyArray<ReadonlyArray<any>>, dst: any[][])
    {
        for (const paramIndex in src)
        {
            if (!src[paramIndex]) { continue; }
            if (!dst[paramIndex]) { dst[paramIndex] = []; }
            mergeTags(src[paramIndex], dst[paramIndex]);
        }
    }

    /**
     * Merges src tags into dst tags, taking care not to override any.
     * @param src
     * @param dst
     */
    function mergeTags(src: ReadonlyArray<any>, dst: any[])
    {
        for (let i = 0, l = src.length; i < l; ++i)
        {
            if (dst[i] == null)
            {
                dst[i] = src[i];
            }
        }
    }
}