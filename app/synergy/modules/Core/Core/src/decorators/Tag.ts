/// <reference path="Metadata.ts" />

namespace RS.Decorators
{
    export enum TagKind { Class, Method, Property, Parameter }

    export enum MemberType { Static, Instance }

    export interface BaseTag<TKind extends TagKind, TWithData extends boolean, TDataType>
    {
        kind: TKind;
        withData: TWithData;

        /**
         * Gets all classes tagged with ANY value for this tag.
         */
        classes: ({ new(): object; } & Function)[];

        /**
         * Published when this tag has been applied to a class for the first time.
         */
        onAppliedInitial: IEvent<Function>;
    }

    export interface ClassTag<TDataType> extends BaseTag<TagKind.Class, true, TDataType>
    {
        (value: TDataType): ClassDecorator;

        /**
         * Gets the tagged value for this tag on the specified class or class instance.
         * Returns null if no tag present.
         * Considers inheritance.
         */
        get(obj: object): TDataType;

        /**
         * Gets all classes tagged with the specified value for this tag.
         */
        getClassesWithTag(value: TDataType): ({ new(): object; } & Function)[];

        /**
         * Published when this tag has been applied to a class.
         */
        onApplied: IEvent<ClassTagAppliedEventData<TDataType>>;
    }

    export type ClassTagAppliedEventData<TDataType> = { target: Object, cl: Function, val: TDataType };

    export interface MethodTag<TDataType> extends BaseTag<TagKind.Method, true, TDataType>
    {
        (value: TDataType): MethodDecorator;

        /**
         * Gets a map of all methods to their tagged value for the specified class or class instance.
         * Considers inheritance.
         */
        get(obj: object, type: MemberType): Map<TDataType>;

        /**
         * Published when this tag has been applied to a method.
         */
        onApplied: IEvent<MethodTagAppliedEventData<TDataType>>;
    }

    export type MethodTagAppliedEventData<TDataType> = { target: Object, cl: Function, func: Function, key: string, val: TDataType };

    export interface PropertyTag<TDataType> extends BaseTag<TagKind.Property, true, TDataType>
    {
        (value: TDataType): PropertyDecorator;

        /**
         * Gets a map of all properties to their tagged value for the specified class or class instance.
         * Considers inheritance.
         */
        get(obj: object, type: MemberType): Map<TDataType>;

        /**
         * Published when this tag has been applied to a property.
         */
        onApplied: IEvent<PropertyTagAppliedEventData<TDataType>>;
    }

    export type PropertyTagAppliedEventData<TDataType> = { target: Object, cl: Function, prop: PropertyDescriptor, key: string, val: TDataType };

    export interface ParameterTag<TDataType> extends BaseTag<TagKind.Parameter, true, TDataType>
    {
        (value: TDataType): ParameterDecorator;

        /**
         * Gets a map of all parameters to their tagged value for the specified static or instance method.
         * Considers inheritance.
         */
        get(obj: object, type: MemberType, method: string): Map<TDataType>;

        /**
         * Published when this tag has been applied to a method.
         */
        onApplied: IEvent<ParameterTagAppliedEventData<TDataType>>;
    }

    export type ParameterTagAppliedEventData<TDataType> = { target: Object, cl: Function, func: Function, key: string, index: number, val: TDataType };

    export interface DatalessClassTag extends BaseTag<TagKind.Class, false, boolean>, ClassDecorator
    {
        /**
         * Gets if this tag is present on the specified class or class instance.
         * Considers inheritance.
         */
        get(obj: object): boolean;

        /**
         * Published when this tag has been applied to a class.
         */
        onApplied: IEvent<DatalessClassTagAppliedEventData>;
    }

    export type DatalessClassTagAppliedEventData = { target: Object, cl: Function };

    export interface DatalessMethodTag extends BaseTag<TagKind.Method, false, boolean>, MethodDecorator
    {
        /**
         * Gets an array of all tagged methods for the specified class or class instance.
         * Considers inheritance.
         */
        get(obj: object, type: MemberType): string[];

        /**
         * Published when this tag has been applied to a method.
         */
        onApplied: IEvent<DatalessMethodTagAppliedEventData>;
    }

    export type DatalessMethodTagAppliedEventData = { target: Object, cl: Function, func: Function, key: string };

    export interface DatalessPropertyTag extends BaseTag<TagKind.Property, false, boolean>, PropertyDecorator
    {
        /**
         * Gets an array of all tagged properties for the specified class or class instance.
         * Considers inheritance.
         */
        get(obj: object, type: MemberType): string[];

        /**
         * Published when this tag has been applied to a property.
         */
        onApplied: IEvent<DatalessPropertyTagTagAppliedEventData>;
    }

    export type DatalessPropertyTagTagAppliedEventData = { target: Object, cl: Function, prop: PropertyDescriptor, key: string };

    export interface DatalessParameterTag extends BaseTag<TagKind.Parameter, false, boolean>, ParameterDecorator
    {
        /**
         * Gets an array of all tagged parameters for the specified static or instance method.
         * Considers inheritance.
         */
        get(obj: object, type: MemberType, method: string): number[];

        /**
         * Published when this tag has been applied to a method.
         */
        onApplied: IEvent<DatalessParameterTagTagAppliedEventData>;
    }

    export type DatalessParameterTagTagAppliedEventData = { target: Object, cl: Function, func: Function, key: string, index: number };

    export type Tag<TData> = ClassTag<TData> | MethodTag<TData> | PropertyTag<TData> | ParameterTag<TData>;
    export type DatalessTag = DatalessClassTag | DatalessMethodTag | DatalessPropertyTag | DatalessParameterTag;

    export namespace Tag
    {
        type IDMap = { [P in TagKind]: number };
        const nextTagID: IDMap =
        {
            [TagKind.Class]: 0,
            [TagKind.Method]: 0,
            [TagKind.Property]: 0,
            [TagKind.Parameter]: 0
        };

        /**
         * Creates a new class tag.
         * @param kind
         */
        export function create<T>(kind: TagKind.Class, ctorModifier?: (oldCtor: Function) => Function): ClassTag<T>;

        /**
         * Creates a new method tag.
         * @param kind
         */
        export function create<T>(kind: TagKind.Method): MethodTag<T>;

        /**
         * Creates a new property tag.
         * @param kind
         */
        export function create<T>(kind: TagKind.Property): PropertyTag<T>;

        /**
         * Creates a new method parameter tag.
         * @param kind
         */
        export function create<T>(kind: TagKind.Parameter): ParameterTag<T>;

        /**
         * Creates a new tag.
         * @param kind
         */
        export function create<T>(kind: TagKind): Tag<T>;

        /**
         * Creates a new tag.
         */
        export function create<T>(kind: TagKind, ctorModifier?: (oldCtor: Function) => Function): Tag<T>
        {
            const tagID = nextTagID[kind]++;
            switch (kind)
            {
                case TagKind.Class:
                {
                    const appliedInitial = createEvent<Function>();
                    const applied = createEvent<ClassTagAppliedEventData<T>>();

                    const decorator = function (value: T): ClassDecorator
                    {
                        return function<TT extends Function>(target: TT): void | TT
                        {
                            // target is the constructor of the class
                            const metadata = getMetadata(target, true);
                            metadata.tags[tagID] = value;

                            if (ctorModifier)
                            {
                                target = (ctorModifier(target) as TT) || target;
                            }

                            if (decorator.classes.indexOf(target as any) === -1)
                            {
                                decorator.classes.push(target as any);
                                appliedInitial.publish(target);
                            }

                            applied.publish({ target, cl: target, val: value });
                            return target;
                        };
                    } as ClassTag<T>;
                    decorator.kind = kind;

                    decorator.get = function (obj: object): T | null
                    {
                        const metadata = readMetadata(obj);
                        return metadata.tags[tagID];
                    };

                    decorator.classes = [];
                    decorator.getClassesWithTag = function(this: ClassTag<T>, value: T)
                    {
                        return this.classes.filter((c) => decorator.get(c) === value);
                    };

                    decorator.onAppliedInitial = appliedInitial.public;
                    decorator.onApplied = applied.public;

                    decorator.withData = true;

                    return decorator;
                }

                case TagKind.Method:
                {
                    const appliedInitial = createEvent<Function>();
                    const applied = createEvent<MethodTagAppliedEventData<T>>();

                    const decorator = function (value: T): MethodDecorator
                    {
                        return function (target: object, propertyKey: string, descriptor: PropertyDescriptor)
                        {
                            if (Is.func(target))
                            {
                                // STATIC METHOD: target is the constructor of the class

                                // Update metadata
                                const metadata = getMetadata(target, true);
                                if (metadata.staticMethods[propertyKey] == null)
                                {
                                    metadata.staticMethods[propertyKey] = { tags: [], parameters: [] };
                                }
                                metadata.staticMethods[propertyKey].tags[tagID] = value;

                                // Update decorator
                                if (decorator.classes.indexOf(target as any) === -1)
                                {
                                    decorator.classes.push(target as any);
                                    appliedInitial.publish(target);
                                }
                                applied.publish({ target, cl: target, func: target[propertyKey], key: propertyKey, val: value });
                            }
                            else
                            {
                                // INSTANCE METHOD: target is the prototype of the class

                                // Update metadata
                                const metadata = getMetadata(target.constructor, true);
                                if (metadata.methods[propertyKey] == null)
                                {
                                    metadata.methods[propertyKey] = { tags: [], parameters: [] };
                                }
                                metadata.methods[propertyKey].tags[tagID] = value;

                                // Update decorator
                                if (decorator.classes.indexOf(target.constructor as any) === -1)
                                {
                                    decorator.classes.push(target.constructor as any);
                                    appliedInitial.publish(target.constructor);
                                }
                                applied.publish({ target, cl: target.constructor, func: target[propertyKey], key: propertyKey, val: value });
                            }

                            // In case the property descriptor was updated.
                            return Object.getOwnPropertyDescriptor(target, propertyKey);
                        };
                    } as MethodTag<T>;
                    decorator.kind = kind;

                    // Metadata getter
                    decorator.get = function (obj: object, type: MemberType): Map<T>
                    {
                        const metadata = readMetadata(obj);
                        const result: Map<T> = {};
                        const src = type === MemberType.Instance ? metadata.methods : metadata.staticMethods;

                        for (const methodName in src)
                        {
                            if (src[methodName] != null)
                            {
                                const tagValue = src[methodName].tags[tagID] as T;
                                if (tagValue != null)
                                {
                                    result[methodName] = tagValue;
                                }
                            }
                        }

                        return result;
                    };

                    decorator.classes = [];
                    decorator.onAppliedInitial = appliedInitial.public;
                    decorator.onApplied = applied.public;

                    decorator.withData = true;

                    return decorator;
                }

                case TagKind.Property:
                {
                    const appliedInitial = createEvent<Function>();
                    const applied = createEvent<PropertyTagAppliedEventData<T>>();

                    const decorator = function (value: T): PropertyDecorator
                    {
                        return function (target: object, propertyKey: string)
                        {
                            if (Is.func(target))
                            {
                                // STATIC METHOD: target is the constructor of the class

                                // Update metadata
                                const metadata = getMetadata(target, true);
                                if (metadata.staticProperties[propertyKey] == null)
                                {
                                    metadata.staticProperties[propertyKey] = { tags: [] };
                                }
                                metadata.staticProperties[propertyKey].tags[tagID] = value;

                                // Update decorator
                                if (decorator.classes.indexOf(target as any) === -1)
                                {
                                    decorator.classes.push(target as any);
                                    appliedInitial.publish(target);
                                }
                                applied.publish({ target, cl: target, prop: Object.getOwnPropertyDescriptor(target, propertyKey), key: propertyKey, val: value });
                            }
                            else
                            {
                                // INSTANCE METHOD: target is the prototype of the class

                                // Update metadata
                                const metadata = getMetadata(target.constructor, true);
                                if (metadata.properties[propertyKey] == null)
                                {
                                    metadata.properties[propertyKey] = { tags: [] };
                                }
                                metadata.properties[propertyKey].tags[tagID] = value;

                                // Update decorator
                                if (decorator.classes.indexOf(target.constructor as any) === -1)
                                {
                                    decorator.classes.push(target.constructor as any);
                                    appliedInitial.publish(target.constructor);
                                }
                                applied.publish({ target, cl: target.constructor, prop: Object.getOwnPropertyDescriptor(target.constructor, propertyKey), key: propertyKey, val: value });
                            }

                            // In case the property descriptor was updated.
                            return Object.getOwnPropertyDescriptor(target, propertyKey);
                        };
                    } as PropertyTag<T>;
                    decorator.kind = kind;

                    // Metadata getter
                    decorator.get = function (obj: object, type: MemberType): Map<T>
                    {
                        const metadata = readMetadata(obj);
                        const result: Map<T> = {};
                        const src = type === MemberType.Instance ? metadata.properties : metadata.staticProperties;

                        for (const propertyName in src)
                        {
                            if (src[propertyName] != null)
                            {
                                const tagValue = src[propertyName].tags[tagID] as T;
                                if (tagValue != null)
                                {
                                    result[propertyName] = tagValue;
                                }
                            }
                        }

                        return result;
                    };

                    decorator.classes = [];
                    decorator.onAppliedInitial = appliedInitial.public;
                    decorator.onApplied = applied.public;

                    decorator.withData = true;

                    return decorator;
                }

                case TagKind.Parameter:
                {
                    const appliedInitial = createEvent<Function>();
                    const applied = createEvent<ParameterTagAppliedEventData<T>>();

                    const decorator = function (value: T): ParameterDecorator
                    {
                        return function (target: object, propertyKey: string, parameterIndex: number)
                        {
                            if (Is.func(target))
                            {
                                // STATIC METHOD: target is the constructor of the class

                                // Update metadata
                                const metadata = getMetadata(target, true);
                                if (metadata.staticMethods[propertyKey] == null)
                                {
                                    metadata.staticMethods[propertyKey] = { tags: [], parameters: [] };
                                }
                                if (metadata.staticMethods[propertyKey].parameters[parameterIndex] == null)
                                {
                                    metadata.staticMethods[propertyKey].parameters[parameterIndex] = [];
                                }
                                metadata.staticMethods[propertyKey].parameters[parameterIndex][tagID] = value;

                                // Update decorator
                                if (decorator.classes.indexOf(target as any) === -1)
                                {
                                    decorator.classes.push(target as any);
                                    appliedInitial.publish(target);
                                }
                                applied.publish({ target, cl: target, func: target[propertyKey], key: propertyKey, index: parameterIndex, val: value });
                            }
                            else
                            {
                                // INSTANCE METHOD: target is the prototype of the class

                                // Update metadata
                                const metadata = getMetadata(target.constructor, true);
                                if (metadata.methods[propertyKey] == null)
                                {
                                    metadata.methods[propertyKey] = { tags: [], parameters: [] };
                                }
                                if (metadata.methods[propertyKey].parameters[parameterIndex] == null)
                                {
                                    metadata.methods[propertyKey].parameters[parameterIndex] = [];
                                }
                                metadata.methods[propertyKey].parameters[parameterIndex][tagID] = value;

                                // Update decorator
                                if (decorator.classes.indexOf(target.constructor as any) === -1)
                                {
                                    decorator.classes.push(target.constructor as any);
                                    appliedInitial.publish(target.constructor);
                                }
                                applied.publish({ target, cl: target.constructor, func: target[propertyKey], key: propertyKey, index: parameterIndex, val: value });
                            }

                            // In case the property descriptor was updated.
                            return Object.getOwnPropertyDescriptor(target, propertyKey);
                        };
                    } as ParameterTag<T>;
                    decorator.kind = kind;

                    // Metadata getter
                    decorator.get = function (obj: object, type: MemberType, methodName: string): Map<T>
                    {
                        const metadata = readMetadata(obj);
                        const result: Map<T> = {};
                        const src = type === MemberType.Instance ? metadata.methods : metadata.staticMethods;

                        if (src[methodName] != null)
                        {
                            for (const parameterIndex in src[methodName].parameters)
                            {
                                if (src[methodName].parameters[parameterIndex] != null)
                                {
                                    const tagValue = src[methodName].parameters[parameterIndex][tagID] as T;
                                    if (tagValue != null)
                                    {
                                        result[parameterIndex] = tagValue;
                                    }
                                }
                            }
                        }

                        return result;
                    };

                    decorator.classes = [];
                    decorator.onAppliedInitial = appliedInitial.public;
                    decorator.onApplied = applied.public;

                    decorator.withData = true;

                    return decorator;
                }

                default:
                    throw new Error(`Invalid or unhandled tag kind '${kind}'`);
            }

        }

        /**
         * Creates a new dataless class tag.
         * @param kind
         */
        export function createDataless(kind: TagKind.Class, ctorModifier?: (oldCtor: Function) => Function): DatalessClassTag;

        /**
         * Creates a new dataless method tag.
         * @param kind
         */
        export function createDataless(kind: TagKind.Method): DatalessMethodTag;

        /**
         * Creates a new dataless property tag.
         * @param kind
         */
        export function createDataless(kind: TagKind.Property): DatalessPropertyTag;

        /**
         * Creates a new dataless method parameter tag.
         * @param kind
         */
        export function createDataless(kind: TagKind.Parameter): DatalessParameterTag;

        /**
         * Creates a new dataless tag.
         * @param kind
         */
        export function createDataless(kind: TagKind): DatalessTag;

        /**
         * Creates a new dataless tag.
         */
        export function createDataless(kind: TagKind, ctorModifier?: (oldCtor: Function) => Function): DatalessTag
        {
            const baseTag = kind === TagKind.Class ? create<boolean>(kind, ctorModifier) : create<boolean>(kind);
            switch (baseTag.kind)
            {
                case TagKind.Class:
                {
                    const decorator = function <T extends Function>(target: T): T | void
                    {
                        return baseTag(true)(target);
                    } as DatalessClassTag;
                    decorator.kind = baseTag.kind;

                    decorator.get = function (obj: object): boolean
                    {
                        return baseTag.get(obj) || false;
                    };

                    decorator.classes = baseTag.classes;
                    decorator.onAppliedInitial = baseTag.onAppliedInitial;
                    decorator.onApplied = baseTag.onApplied;

                    decorator.withData = false;

                    return decorator;
                }

                case TagKind.Method:
                {
                    const decorator = function (target: object, propertyKey: string, descriptor: PropertyDescriptor)
                    {
                        return baseTag(true)(target, propertyKey, descriptor);
                    } as DatalessMethodTag;
                    decorator.kind = baseTag.kind;

                    decorator.get = function (obj: object, type: MemberType): string[]
                    {
                        const dataMap = baseTag.get(obj, type);
                        return Object.keys(dataMap);
                    };

                    decorator.classes = baseTag.classes;
                    decorator.onAppliedInitial = baseTag.onAppliedInitial;
                    decorator.onApplied = baseTag.onApplied;

                    decorator.withData = false;

                    return decorator;
                }

                case TagKind.Property:
                {
                    const decorator = function (target: object, propertyKey: string)
                    {
                        return baseTag(true)(target, propertyKey);
                    } as DatalessPropertyTag;
                    decorator.kind = baseTag.kind;

                    decorator.get = function (obj: object, type: MemberType): string[]
                    {
                        const dataMap = baseTag.get(obj, type);
                        return Object.keys(dataMap);
                    };

                    decorator.classes = baseTag.classes;
                    decorator.onAppliedInitial = baseTag.onAppliedInitial;
                    decorator.onApplied = baseTag.onApplied;

                    decorator.withData = false;

                    return decorator;
                }

                case TagKind.Parameter:
                {
                    const decorator = function (target: object, propertyKey: string, parameterIndex: number)
                    {
                        return baseTag(true)(target, propertyKey, parameterIndex);
                    } as DatalessParameterTag;
                    decorator.kind = baseTag.kind;

                    decorator.get = function (obj: object, type: MemberType, method: string): number[]
                    {
                        const dataMap = baseTag.get(obj, type, method);
                        return Object.keys(dataMap)
                                     .map((key) => parseInt(key));
                    };

                    decorator.classes = baseTag.classes;
                    decorator.onAppliedInitial = baseTag.onAppliedInitial;
                    decorator.onApplied = baseTag.onApplied;

                    decorator.withData = false;

                    return decorator;
                }

                default: return null;
            }

        }
    }
}
