namespace RS.ArrayUtils
{
    export type EqualsComparator<T> = (a: T, b: T) => boolean;
    export type FilterFunc<T> = (a: T) => boolean;

    /**
     * Copies all items from "from" and appends them to "to".
     * @param from 
     * @param to 
     */
    export function copy<T>(from: ArrayLike<T>, to: Array<T>)
    {
        const start = to.length;
        to.length += from.length;
        for (let i = 0, l = from.length; i < l; ++i)
        {
            to[start + i] = from[i];
        }
    }

    /**
     * Removes all duplicates from the specified array.
     * Preserves order.
     * @param arr 
     */
    export function removeDuplicates<T>(arr: Array<T>, equals?: EqualsComparator<T>): void
    {
        for (let i = arr.length - 1; i > 0; --i)
        {
            for (let j = i - 1; j >= 0; --j)
            {
                if (equals ? equals(arr[i], arr[j]) : arr[i] === arr[j])
                {
                    arr.splice(i, 1);
                    break;
                }
            }
        }
    }

    /**
     * Removes an item from the specified array by shifting the last item onto it.
     * Faster than splice as following indices do not need to be shifted, but does not preserve order.
     * @param arr 
     * @param index 
     */
    export function fastRemove<T>(arr: Array<T>, index: number): void
    {
        if (index < 0 || index >= arr.length) { return; }
        if (index < arr.length - 1)
        {
            arr[index] = arr[arr.length - 1];
        }
        --arr.length;
    }

    /**
     * Removes all items from an array that don't pass the predicate.
     * @param arr 
     * @param predicate 
     */
    export function filterInPlace<T>(arr: Array<T>, predicate: FilterFunc<T>, preserveOrder?: boolean): void
    {
        for (let i = arr.length - 1; i >= 0; --i)
        {
            if (!predicate(arr[i]))
            {
                if (preserveOrder)
                {
                    arr.splice(i, 1);
                }
                else
                {
                    fastRemove(arr, i);
                }
            }
        }
    }

    /**
     * Gets if the array contains at least one of the specified item.
     * @param arr 
     * @param item 
     * @param equals 
     */
    export function contains<T>(arr: ReadonlyArray<T>, item: T, equals?: EqualsComparator<T>): boolean
    {
        for (let i = 0, l = arr.length; i < l; ++i)
        {
            if (equals ? equals(arr[i], item) : arr[i] === item)
            {
                return true;
            }
        }
        return false;
    }
}