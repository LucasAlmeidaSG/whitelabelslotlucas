namespace RS
{
    /**
     * Encapsulates device properties.
     */
    interface DeviceProperties
    {
        isMobileDevice: boolean;
        isTouchDevice: boolean;
        isAppleDevice: boolean;
        isHandsetDevice: boolean;
        isTabletDevice: boolean;
        isSamsungGalaxyS8: boolean;
    }

    /**
     * Encapsulates device operating system.
     */
    export interface DeviceOS
    {
        os: string;
        version: string;
    }

    /**
     * A type of touch event.
     */
    export enum TouchEventType { Click, Tap, TouchStart, MouseDown, TouchEnd, MouseUp }

    /**
     * A type of browser.
     */
    export enum Browser
    {
        Chrome = "Chrome",
        ChromeMobile = "Chrome Mobile",
        IE = "IE",
        Edge = "Microsoft Edge",
        Safari = "Safari",
        Firefox = "Firefox"
    }

    /**
     * Convenience methods for accessing device specific modes and events.
     */
    export class Device
    {
        // TODO: Proper ipad mobile detection
        // Hacky fix for ipads being shown as desktop devices on iOS14+. This could potentially catch some desktop tablets running Safari
        private static get isiOS14Safari() { return platform.name === Browser.Safari && navigator.maxTouchPoints === 5; }

        /**
         * Precomputed device properties for the current platform.
         */
        private static deviceProperties : DeviceProperties =
        {
            isMobileDevice : /Android|HTC|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) || Device.isiOS14Safari,
            isTouchDevice : ("ontouchstart" in document.documentElement) || ("ontouchstart" in document),
            isAppleDevice : /iPhone|iPad|iPod/i.test(navigator.userAgent) || Device.isiOS14Safari,
            isHandsetDevice : window.matchMedia("only screen and (max-width: 760px)").matches,
            isTabletDevice : window.matchMedia("only screen and (max-width: 760px)").matches,
            isSamsungGalaxyS8 : /SM-G(950|955|55|892)[A-Z]/i.test(navigator.userAgent)
        };

        private constructor() { }

        /**
         * Convenience function to return the supported event for click / tap across any device
         * Example usage: $(button).bind(RS.Device.getSupportedEvent(TouchEventType.Click), () => {  });
         * @param eventType
         * @returns                Event name to use in jQuery bind or javascript addEventListener
         */
        public static getSupportedClickEvent(eventType: TouchEventType): string
        {
            switch (eventType)
            {
                case TouchEventType.Click:
                case TouchEventType.Tap:
                    return this.isTouchDevice ? "touchend" : "click";
                case TouchEventType.TouchStart:
                case TouchEventType.MouseDown:
                    return this.isTouchDevice ? "touchstart" : "mousedown";
                case TouchEventType.TouchEnd:
                case TouchEventType.MouseUp:
                    return this.isTouchDevice ? "touchend" : "mouseup";
                default:
                    return null;
            }
        }

        /**
         * Gets if the current device is an iOS Device.
         */
        public static get isAppleDevice() { return this.deviceProperties.isAppleDevice; }

        /**
         * Gets if the current os version is iOS 14
         */
        public static get isIOS14()
        {
            return this.isAppleDevice &&
            (this.operatingSystem.version.split(".")[0] === "14");
        }

        /**
         * Gets if the current device is a mobile device.
         */
        public static get isMobileDevice() { return this.deviceProperties.isMobileDevice; }

        /**
         * Gets if the current device supports touch (could be a laptop with a touch screen).
         */
        public static get isTouchDevice() { return this.deviceProperties.isTouchDevice; }

        /**
         * Gets if the current device is NOT a mobile device.
         */
        public static get isDesktopDevice() { return !this.deviceProperties.isMobileDevice; }

        /**
         * Gets if the current device is a handset device (mobile but not tablet).
         */
        public static get isHandsetDevice() { return this.deviceProperties.isHandsetDevice; }

        /**
         * Gets if the current device is a tablet device.
         */
        public static get isTabletDevice() { return this.deviceProperties.isTabletDevice; }

        /**
         * Gets if the current device is a Samsung Galaxy S8 or S8+.
         * NOTE: If you're using this because shaders aren't working, check that your shader uses ALL varyings, including vTextureId.
         */
        public static get isSamsungGalaxyS8() { return this.deviceProperties.isSamsungGalaxyS8; }

        /**
         * Gets if the current "device" is the iOS app
         */
        public static get isIOSApp()
        {
            // Safari version 11.x seems specific to the iOS app
            // Apart from UTH for some reason which reports 8+. So check for either .x or +
            // platfor.js adds .x or + onto the end of a version its approximating

            return this.isAppleDevice &&
                this.browser === Browser.Safari &&
                platform.version.match(/(\.x|\+)/) != null;
                //platform.version === "11.x";
        }

        /**
         * Gets the current device type.
         */
        public static get deviceDetails() { return platform.product; }

        /**
         * Gets the current device Operating System.
         */
        public static get operatingSystem(): DeviceOS
        {
            return {
                os : platform.os.family,
                version : platform.os.version
            };
        }

        /**
         * Gets the current the browser type.
         */
        public static get browser() { return platform.name; }

        /**
         * Gets a summary of the device.
         */
        public static get summary() { return `${platform} (renderer: ${Capabilities.WebGL.getRenderer()})`; }
    }
}
