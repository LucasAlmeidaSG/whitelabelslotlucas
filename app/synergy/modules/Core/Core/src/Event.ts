namespace RS
{
    /** @internal */
    const __eventObjectHash: number = Math.random();
    export namespace Is
    {
        export function event<T>(value: any): value is IEvent<T>
        {
            return value != null &&
                Is.func(value.once) &&
                Is.func(value.off) &&
                Is.func(value.has) &&
                value.__isObjectFunction &&
                value.__eventObjectHash === __eventObjectHash;
        }
    }

    export namespace Event
    {
        export interface CompositeEventData<T>
        {
            index: number;
            data: T;
        }

        /** Creates an event that is published when any of the given events is published. */
        export function any<T>(events: ReadonlyArray<RS.IEvent<T>>): IEvent<CompositeEventData<T>>
        {
            const result = createEvent<CompositeEventData<T>>();
            const listeners = events.map((ev, index) =>
                ev((data) => result.publish({ index, data })));
            const oldDispose = result.dispose;
            result.dispose = function (this: IPrivateEvent<T>)
            {
                if (this.isDisposed) { return; }
                listeners.forEach((l) => l.dispose());
                oldDispose();
            };
            return result;
        }
    }

    export type EventHandlerCallback<T> = (data: T) => void;

    interface EventHandlerData<T>
    {
        handler: EventHandlerCallback<T>;
        once: boolean;
    }

    export type IBaseEvent<T> = (callback: EventHandlerCallback<T>) => IDisposable;
    /*export interface IBaseEvent<T> extends Function
    {
        (callback: EventHandlerCallback<T>): IDisposable;
    }*/

    /**
     * Encapsulates an event that is explicitly owned by a single instance.
     */
    export interface IEvent<T = void> extends PromiseLike<T>, IBaseEvent<T>, IDisposable
    {
        /**
         * Adds a listener to this event that only fires once.
         * Returns an object that can be disposed to remove the listener.
         * @param handler
         */
        once(callback: EventHandlerCallback<T>): IDisposable;

        /**
         * Removes a listener to this event.
         */
        off(handler: EventHandlerCallback<T>): void;

        /**
         * Gets if the specified callback is a listener of this event.
         */
        has(handler: EventHandlerCallback<T>): boolean;

        /**
         * Returns a clone of this event
         */
        clone(): IEvent<T>;
    }

    /**
     * Encapsulates an event that is explicitly owned by a single instance.
     */
    export interface IPrivateEvent<T = void> extends IEvent<T>
    {
        /**
         * Gets an exposable reference to this event.
         */
        readonly public: IEvent<T>;

        /**
         * Triggers this event.
         * @param data
         */
        publish(data: T): void;

        /**
         * Returns a clone of this event
         */
        clone(): IPrivateEvent<T>;
    }

    export interface IPrivateWatchedEvent<T = void> extends IPrivateEvent<T>
    {
        /**
         * Gets the number of listeners attached to this event.
         */
        readonly listenerCount: IReadonlyObservable<number>;

        /**
         * Returns a clone of this event
         */
        clone(): IPrivateWatchedEvent<T>;
    }

    /**
     * Encapsulates an event that is explicitly owned by a single instance.
     */
    export interface ISimplePrivateEvent extends IEvent<void>
    {
        /**
         * Gets an exposable reference to this event.
         */
        readonly public: IEvent<void>;

        /**
         * Triggers this event.
         * @param data
         */
        publish(): void;

        /**
         * Returns a clone of this event
         */
        clone(): ISimplePrivateEvent;
    }

    export interface ISimplePrivateWatchedEvent extends ISimplePrivateEvent
    {
        /**
         * Gets the number of listeners attached to this event.
         */
        readonly listenerCount: IReadonlyObservable<number>;

        /**
         * Returns a clone of this event
         */
        clone(): ISimplePrivateWatchedEvent;
    }

    /**
     * Creates a new event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createEvent<T>(): IPrivateEvent<T>;

    /**
     * Creates a new event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createEvent<T>(watched: boolean, async?: boolean): IPrivateEvent<T> | IPrivateWatchedEvent<T>;

    /**
     * Creates a new event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createEvent<T>(watched: false, async?: boolean): IPrivateEvent<T>;

    /**
     * Creates a new watched event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createEvent<T>(watched: true, async?: boolean): IPrivateWatchedEvent<T>;

    export function createEvent<T>(watched?: boolean, async?: boolean): IPrivateEvent<T> | IPrivateWatchedEvent<T>
    {
        return createEventInternal<T>(watched, async);
    }

    /**
     * Creates a new parameterless event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createSimpleEvent(): ISimplePrivateEvent;

    /**
     * Creates a new parameterless event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createSimpleEvent(watched: boolean, async?: boolean): ISimplePrivateEvent | ISimplePrivateWatchedEvent;

    /**
     * Creates a new parameterless event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     */
    export function createSimpleEvent(watched: false, async?: boolean): ISimplePrivateEvent;

    /**
     * Creates a new parameterless watched event instance.
     * @param async If true, event publishes will be processed on the next tick, rather than immediately.
     *              Useful for dummy backends which otherwise
     */
    export function createSimpleEvent(watched: true, async?: boolean): ISimplePrivateWatchedEvent;

    export function createSimpleEvent(watched?: boolean, async?: boolean): ISimplePrivateEvent | ISimplePrivateWatchedEvent
    {
        return createEventInternal(watched, async);
    }

    function createEventInternal(watched?: boolean, async?: boolean): ISimplePrivateEvent;
    function createEventInternal<T>(watched?: boolean, async?: boolean): IPrivateEvent<T>;
    function createEventInternal<T>(watched?: boolean, async?: boolean): ISimplePrivateEvent | IPrivateEvent<T>
    {
        let baseEvent: InternalEvent<T>;
        if (watched)
        {
            baseEvent = new WatchedEvent();
        }
        else
        {
            baseEvent = new InternalEvent();
        }
        const func = baseEvent.on.bind(baseEvent) as IInternalEvent<T>;
        func._event = baseEvent;
        func.once = baseEvent.once.bind(baseEvent);
        func.off = baseEvent.off.bind(baseEvent);
        func.has = baseEvent.has.bind(baseEvent);
        func.publish = async ? baseEvent.publishAsync.bind(baseEvent) : baseEvent.publish.bind(baseEvent);
        func.then = baseEvent.then.bind(baseEvent);
        func.dispose = baseEvent.dispose.bind(baseEvent);
        func.clone = baseEvent.clone.bind(baseEvent);
        Object.defineProperties(func, {
            public: { value: func },
            isDisposed: { get: () => baseEvent.isDisposed },
            __isObjectFunction: { value: true },
            __eventObjectHash: { value: __eventObjectHash }
        });
        if (watched)
        {
            Object.defineProperties(func, { listenerCount: { value: (baseEvent as WatchedEvent<T>).listenerCount } });
        }
        return func as any;
    }

    interface IInternalEvent<T> extends IPrivateEvent<T>
    {
        _event: WatchedEvent<T> | InternalEvent<T>;
    }

    class EventListener<T> implements IDisposable
    {
        public get isDisposed() { return this._event.isDisposed || !this._event.has(this._callback); }
        public constructor(private _event: InternalEvent<T>, private _callback: EventHandlerCallback<T>) { }
        public dispose() { if (this._event.isDisposed) { return; } this._event.off(this._callback); }
    }

    class InternalEvent<T> implements IDisposable
    {
        protected _inPublish: number = 0;
        protected _handlers: (EventHandlerData<T> | null)[] = [];

        protected _isDisposed: boolean = false;
        public get isDisposed() { return this._isDisposed };

        public clone(): IPrivateEvent<T>
        {
            return createEvent<T>();
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._handlers.length = 0;
            this._handlers = null;
            this._isDisposed = true;
        }

        /**
         * Adds a listener to this event that only fires once.
         * @param handler
         */
        public once(handler: EventHandlerCallback<T>): IDisposable
        {
            return this.on(handler, true);
        }

        /**
         * Adds a listener to this event.
         * @param handler
         */
        public on(handler: EventHandlerCallback<T>, once: boolean = false): IDisposable
        {
            if (this._isDisposed) { throw new Error(`Attempt to add handler when event was disposed`); }
            this._handlers.push({ handler, once });
            return new EventListener(this, handler);
        }

        /**
         * Removes a listener from this event.
         * @param handler
         */
        public off(handler: EventHandlerCallback<T>): void
        {
            for (let i = 0, l = this._handlers.length; i < l; ++i)
            {
                if (this._handlers[i] != null && this._handlers[i].handler === handler)
                {
                    if (this._inPublish > 0)
                    {
                        this._handlers[i] = null;
                    }
                    else
                    {
                        this._handlers.splice(i, 1);
                    }
                    break;
                }
            }
        }

        /**
         * Gets if the specified callback is a listener of this event.
         */
        public has(handler: EventHandlerCallback<T>): boolean
        {
            for (let i = 0, l = this._handlers.length; i < l; ++i)
            {
                if (this._handlers[i] != null && this._handlers[i].handler === handler)
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * Schedules this event to be published.
         * @param data
         */
        public publishAsync(data: T): void
        {
            setTimeout(this.publish.bind(this, data), 0);
        }

        /**
         * Triggers this event.
         * @param data
         */
        public publish(data?: T): void
        {
            if (this._isDisposed) { throw new Error(`Attempt to publish event when event was disposed`); }
            // Early out; avoid loop overheads etc.
            if (this._handlers.length === 0) { return; }

            ++this._inPublish;
            for (let i = 0, l = this._handlers.length; i < l; ++i)
            {
                const handlerData = this._handlers[i];
                if (handlerData != null)
                {
                    handlerData.handler(data);
                    // Return if handler dispos
                    if (this._isDisposed) { return; }
                    if (handlerData.once)
                    {
                        this.off(handlerData.handler);
                    }
                }
            }
            --this._inPublish;
            if (this._inPublish === 0)
            {
                for (let i = this._handlers.length - 1; i >= 0; --i)
                {
                    if (this._handlers[i] == null)
                    {
                        this._handlers.splice(i, 1);
                    }
                }
            }
        }

        /**
         * Attaches callbacks for the resolution and/or rejection of the Promise.
         * @param onfulfilled The callback to execute when the Promise is resolved.
         * @param onrejected The callback to execute when the Promise is rejected.
         * @returns A Promise for the completion of which ever callback is executed.
         */
        public then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): PromiseLike<TResult1 | TResult2>
        {
            return new Promise((resolve, reject) =>
            {
                if (this.isDisposed) { resolve(); return; }

                this.once((data) =>
                {
                    onfulfilled(data);
                    resolve();
                });
            });
        }
    }

    class WatchedEvent<T> extends InternalEvent<T>
    {
        public readonly listenerCount = new Observable(0);

        public clone(): IPrivateWatchedEvent<T>
        {
            return createEvent<T>(true);
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this.listenerCount.dispose();
            super.dispose();
        }

        /**
         * Adds a listener to this event.
         * @param handler
         */
        public on(handler: EventHandlerCallback<T>, once: boolean = false): IDisposable
        {
            const listener = super.on(handler, once);
            this.updateListenerCount();
            return listener;
        }

        /**
         * Removes a listener from this event.
         * @param handler
         */
        public off(handler: EventHandlerCallback<T>): void
        {
            super.off(handler);
            this.updateListenerCount();
        }

        private updateListenerCount()
        {
            this.listenerCount.value = this._handlers.reduce((a, b) => b != null ? ++a : a, 0);
        }
    }

}
