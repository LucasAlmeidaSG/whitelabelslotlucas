/// <reference path="Event.ts" />
/// <reference path="Init.ts" />
/// <reference path="helpers/Observable.ts" />

namespace RS
{
    /**
     * Connection utilities.
     */
    export class Connection
    {
        private static _checkInterval: number = -1;

        /** Set to true or false to force a connection status. */
        public static connectedOverride: boolean|null = null;

        /** Whether or not we are connected to the internet. */
        public static readonly connected = new Observable(navigator.onLine);

        private constructor() { }

        /**
         * Initialises the ConnectionUtils class.
         */
        @Init()
        private static init(): void
        {
            this._checkInterval = window.setInterval(() =>
            {
                this.checkConnectivity();
            }, 100);
        }

        /**
         * Checks the current connectivity of the app and publishes events if the connection state has changed.
         */
        private static checkConnectivity(): void
        {
            this.connected.value = this.connectedOverride != null ? this.connectedOverride : navigator.onLine;
        }
    }
}
