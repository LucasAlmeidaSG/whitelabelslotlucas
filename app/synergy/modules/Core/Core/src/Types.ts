namespace RS
{
    /**
     * Encapsulates a constructor that takes no arguments and creates a new T.
     */
    export interface Constructor<T> extends Function { new (): T; }

    /**
     * Encapsulates a constructor that takes 1 argument and creates a new T.
     */
    export interface Constructor1A<T, TA1> extends Function { new (a: TA1): T; }

    /**
     * Encapsulates a constructor that takes 2 arguments and creates a new T.
     */
    export interface Constructor2A<T, TA1, TA2> extends Function { new (a: TA1, b: TA2): T; }

    /**
     * Encapsulates a constructor that takes 3 arguments and creates a new T.
     */
    export interface Constructor3A<T, TA1, TA2, TA3> extends Function { new (a: TA1, b: TA2, c: TA3): T; }

    /**
     * Encapsulates a constructor that takes 4 arguments and creates a new T.
     */
    export interface Constructor4A<T, TA1, TA2, TA3, TA4> extends Function { new (a: TA1, b: TA2, c: TA3, d: TA4): T; }

    /**
     * Encapsulates a constructor that takes 5 arguments and creates a new T.
     */
    export interface Constructor5A<T, TA1, TA2, TA3, TA4, TA5> extends Function { new (a: TA1, b: TA2, c: TA3, d: TA4, e: TA5): T; }

    // then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): PromiseLike<TResult1 | TResult2>;

    export type PendingPromise = { resolve: (value?: any) => void, reject: (reason?: any) => void };

    export type Class<T> = Function &
    {
        new(): T;
        prototype: T;
    };

    export type PartialRecursive<T> =
    {
        [P in keyof T]?: PartialRecursive<T[P]>;
    };

    /**
     * Represents a map of string based keys to generic type T.
     */
    export type Map<T> = { [key: string]: T; };

    export type MapFunc<T, U = T> = (value: T) => U;

    /**
     * Makes all properties of T nullable.
     */
    export type Nullable<T> =
    {
        [P in keyof T]: T[P] | null;
    }

    /**
     * Represents a type that may be an array or a single item of type T.
     */
    export type OneOrMany<T> = T[] | T;

    /** Effectively disables some properties of an interface. */
    export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

    /** Represents the a general enum. */
    export type Enum<E> = Record<keyof E, string | number> & { [k: number]: string | number };

    /** Represents a function which simply handles its invocation, possibly accepting a single argument. */
    export type Action<T = void> = T extends void ? () => void : (arg: T) => void;

    /** Represents a function which takes one argument and returns one result. */
    export type Func<T, TResult = T> = (arg: T) => TResult;
    
    /**
     * Represents an unknown value.
     * TODO Synergy 1.1 remove this
     * @deprecated Replaced with native unknown type in Synergy 1.1.
     */
    export type Unknown = number | boolean | string | object | ((...args: Unknown[]) => Unknown) | undefined;
}
