namespace RS
{
    /**
     * Encapsulates an object that can be cloned
     */
    export interface ICloneable<T extends object>
    {
        clone(): T;
    }

    export namespace Is
    {
        /**
         * Gets if the specified value implements ICloneable
         * @param value
         */
        export function cloneable(value: any): value is ICloneable<object>
        {
            return Is.func(value.clone) && Is.nonPrimitive(value);
        }
    }
}
