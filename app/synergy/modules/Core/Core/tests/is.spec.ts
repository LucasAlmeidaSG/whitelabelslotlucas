namespace RS.Tests
{
    const { expect } = chai;

    describe("Is.ts", function ()
    {
        const isArrayOfNumber = (v) => Is.arrayOf(v, Is.number);

        interface TestCase
        {
            name: string;
            value: any;
            acceptTrue: ((value: any) => boolean)[];
        }

        class MockClass {}

        const testCases: TestCase[] =
        [
            { name: "NaN", value: NaN, acceptTrue: [] },
            { name: "infinity", value: Infinity, acceptTrue: [ Is.number, Is.primitive ] },
            { name: "-infinity", value: -Infinity, acceptTrue: [ Is.number, Is.primitive ] },
            { name: "integer", value: 1, acceptTrue: [ Is.number, Is.integer, Is.primitive ] },
            { name: "number", value: 1.3, acceptTrue: [ Is.number, Is.primitive ] },
            { name: "true", value: true, acceptTrue: [ Is.boolean, Is.primitive ] },
            { name: "false", value: false, acceptTrue: [ Is.boolean, Is.primitive ] },
            { name: "string", value: "hello", acceptTrue: [ Is.string, Is.primitive ] },
            { name: "function", value: function () { return; }, acceptTrue: [ Is.func, Is.nonPrimitive ] },
            { name: "array (empty)", value: [], acceptTrue: [ Is.array, Is.nonPrimitive, isArrayOfNumber ] },
            { name: "array (numbers)", value: [1.3], acceptTrue: [ Is.array, Is.nonPrimitive, isArrayOfNumber ] },
            { name: "object", value: {}, acceptTrue: [ Is.object, Is.nonPrimitive ] },
            { name: "instance", value: new MockClass(), acceptTrue: [ Is.object, Is.nonPrimitive ] },
            { name: "class", value: MockClass, acceptTrue: [ Is.func, Is.nonPrimitive ] }
        ];

        const tests: Map<(value: any) => boolean> =
        {
            "number": Is.number,
            "integer": Is.integer,
            "string": Is.string,
            "boolean": Is.boolean,
            "primitive": Is.primitive,
            "function": Is.func,
            "object": Is.object,
            "array": Is.array,
            "arrayOf": isArrayOfNumber,
            "disposable": Is.disposable
        };

        for (const testName in tests)
        {
            const testTarget = tests[testName];
            describe(testName, function ()
            {
                for (const testCase of testCases)
                {
                    if (testCase.acceptTrue.indexOf(testTarget) !== -1)
                    {
                        it(`should return true for '${testCase.name}'`, function ()
                        {
                            expect(testTarget(testCase.value)).to.be.true;
                        });
                    }
                    else
                    {
                        it(`should return false for '${testCase.name}'`, function ()
                        {
                            expect(testTarget(testCase.value)).to.be.false;
                        });
                    }
                }
            });
        }
    });

    // Type narrowing "tests"
    // If the type narrowing is wrong, will cause a test build error
    function expectExplicitType<T>(val: T) { /* */ }
    type UnknownType = number | boolean | string | Function | object;
    type Unknown = UnknownType | UnknownType[];
    const object: Unknown = null;
    if (Is.number(object)) { expectExplicitType<number>(object); }
    if (Is.boolean(object)) { expectExplicitType<boolean>(object); }
    if (Is.string(object)) { expectExplicitType<string>(object); }
    if (Is.func(object)) { expectExplicitType<Function>(object); }
    if (Is.array(object)) { expectExplicitType<any[]>(object); }
    if (Is.object(object)) { expectExplicitType<object>(object); }
    if (Is.primitive(object)) { expectExplicitType<number | boolean | string>(object); }
    if (Is.arrayOf(object, Is.number)) { expectExplicitType<number[]>(object); }
}