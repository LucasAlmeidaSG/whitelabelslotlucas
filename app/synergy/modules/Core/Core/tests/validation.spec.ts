namespace RS.Tests
{
    const { expect } = chai;
    const { Number, Integer, NotNull, String, Boolean, Array, ArrayOf, Func, Object, AllOf, AnyOf, OneOf } = Validate;

    describe("Validation.ts", function ()
    {
        const MockValidatorAlwaysPasses: Validate.IValidator<any> =
        {
            validate: function () { return null; }
        };

        const failMessage = "should be correct";
        const MockValidatorAlwaysFails: Validate.IValidator<any> =
        {
            validate: function () { return failMessage; }
        };

        const MockValidatorAlwaysThrows: Validate.IValidator<any> =
        {
            validate: function () { throw new Error("Something went wrong"); }
        };

        describe("Decorators", function ()
        {
            it("should invoke the inner method if the IValidator does not return a string", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public method(@Validate.Argument({ validator: MockValidatorAlwaysPasses }) arg: boolean)
                    {
                        return arg;
                    }
                }
                const obj = new MyClass();

                expect(obj.method(true)).to.equal(true);
                expect(obj.method(false)).to.equal(false);
            });

            it("should invoke the inner static method if the IValidator does not return a string", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public static method(@Validate.Argument({ validator: MockValidatorAlwaysPasses }) arg: boolean)
                    {
                        return arg;
                    }
                }

                expect(MyClass.method(true)).to.equal(true);
                expect(MyClass.method(false)).to.equal(false);
            });

            it("should throw an error if the IValidator returns a string", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public method(@Validate.Argument({ validator: MockValidatorAlwaysFails }) arg: boolean)
                    {
                        return arg;
                    }
                }
                const obj = new MyClass();

                expect(() => obj.method(true)).to.throw(`method: parameter 0 ${failMessage} (got true)`);
            });

            it("should throw an error using the parameter name if one is set", function ()
            {
                const name = "arg";
                class MyClass
                {
                    @Validate.Method({})
                    public method(@Validate.Argument({ validator: MockValidatorAlwaysFails, name }) arg: boolean)
                    {
                        return arg;
                    }
                }
                const obj = new MyClass();

                expect(() => obj.method(true)).to.throw(`method: ${name} ${failMessage} (got true)`);
            });

            it("should throw an error using the error message if one is set", function ()
            {
                const error = "should be incorrect";
                class MyClass
                {
                    @Validate.Method({})
                    public method(@Validate.Argument({ validator: MockValidatorAlwaysFails, error }) arg: boolean)
                    {
                        return arg;
                    }
                }
                const obj = new MyClass();

                expect(() => obj.method(true)).to.throw(`method: parameter 0 ${error} (got true)`);
            });

            it("should throw an error using the method name if one is set", function ()
            {
                const name = "meth";
                class MyClass
                {
                    @Validate.Method({ name })
                    public method(@Validate.Argument({ validator: MockValidatorAlwaysFails }) arg: boolean)
                    {
                        return arg;
                    }
                }
                const obj = new MyClass();

                expect(() => obj.method(true)).to.throw(`${name}: parameter 0 ${failMessage} (got true)`);
            });

            it("should mention 'validation' or 'validator' in the error if the IValidator throws an error", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public static method(@Validate.Argument({ validator: MockValidatorAlwaysThrows }) arg: boolean)
                    {
                        return arg;
                    }
                }
                expect(() => MyClass.method(true)).to.throw(/validation|validator/i);
            });

            it("should mention the method name in the error if the IValidator throws an error", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public static coolMethod(@Validate.Argument({ validator: MockValidatorAlwaysThrows }) arg: boolean)
                    {
                        return arg;
                    }
                }
                expect(() => MyClass.coolMethod(true)).to.throw(/coolMethod/);
            });

            it("should mention the parameter name in the error if the IValidator throws an error", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public static coolMethod(@Validate.Argument({ validator: MockValidatorAlwaysThrows }) arg: boolean)
                    {
                        return arg;
                    }
                }
                expect(() => MyClass.coolMethod(true)).to.throw(/parameter 0/);
            });

            it("should include the deep string representation of the arg in the error if the IValidator throws an error", function ()
            {
                class MyClass
                {
                    @Validate.Method({})
                    public static coolMethod(@Validate.Argument({ validator: MockValidatorAlwaysThrows }) arg: any)
                    {
                        return arg;
                    }
                }

                expect(() => MyClass.coolMethod(undefined)).to.throw(/undefined/);
                expect(() => MyClass.coolMethod(null)).to.throw(/null/);
                expect(() => MyClass.coolMethod(true)).to.throw(/true/);
                expect(() => MyClass.coolMethod(3.5)).to.throw(/3\.5/);
                expect(() => MyClass.coolMethod("string")).to.throw(/"string"/);
                expect(() => MyClass.coolMethod({})).to.throw(/\{\}/);
                expect(() => MyClass.coolMethod([])).to.throw(/\[\]/);
            });
        });

        describe("Validators", function ()
        {
            function testAccepts<T>(validator: Validate.IValidator<T>, value: T)
            {
                it(`should return null when given ${value}`, function ()
                {
                    expect(validator.validate(value)).to.equal(null);
                });
            }

            function testRejects<T>(validator: Validate.IValidator<T>, value: any)
            {
                it(`should return a string when given ${value}`, function ()
                {
                    const str = validator.validate(value);
                    expect(str, "should be a string").to.be.a("string");
                    expect(str.length, "should not be empty").to.be.greaterThan(0);
                });
            }

            interface TestCase
            {
                value: any;
                acceptTrue: Validate.IValidator<any>[];
            }

            class MockClass {}

            const arrayOfNumber = ArrayOf(Number);

            const testCases: TestCase[] =
            [
                { value: NaN, acceptTrue: [] },
                { value: Infinity, acceptTrue: [Number] },
                { value: -Infinity, acceptTrue: [Number] },
                { value: 1, acceptTrue: [Number, Integer] },
                { value: 1.3, acceptTrue: [Number] },
                { value: true, acceptTrue: [Boolean] },
                { value: false, acceptTrue: [Boolean] },
                { value: "hello", acceptTrue: [String] },
                { value: function () { return; }, acceptTrue: [Func] },
                { value: [], acceptTrue: [Array, arrayOfNumber] },
                { value: [1.3], acceptTrue: [Array, arrayOfNumber] },
                { value: {}, acceptTrue: [Object] },
                { value: new MockClass(), acceptTrue: [Object] },
                { value: MockClass, acceptTrue: [Func] }
            ];

            const validators: { [name: string]: Validate.IValidator<any> } =
            {
                "number": Number,
                "integer": Integer,
                "string": String,
                "boolean": Boolean,
                "function": Func,
                "array": Array,
                "arrayOf": arrayOfNumber,
                "object": Object
            };

            for (const name in validators)
            {
                const validator = validators[name];
                testAccepts(validator, null);
                testAccepts(validator, undefined);
                describe(name, function ()
                {
                    for (const testCase of testCases)
                    {
                        if (testCase.acceptTrue && testCase.acceptTrue.indexOf(validator) !== -1)
                        {
                            testAccepts(validator, testCase.value);
                        }
                        else
                        {
                            testRejects(validator, testCase.value);
                        }
                    }
                });
            }

            describe("NotNull", function ()
            {
                testRejects(NotNull, null);
                for (const testCase of testCases)
                {
                    testAccepts(NotNull, testCase.value);
                }
            });

            describe("AnyOf", function ()
            {
                it("should throw if passed an empty array", function ()
                {
                    expect(() => AnyOf.apply(null, [])).to.throw();
                });

                it("should throw if passed an array with null members", function ()
                {
                    expect(() => AnyOf(Number, null)).to.throw();
                });

                it("should return null if any validator returns null", function ()
                {
                    const validator = AnyOf(MockValidatorAlwaysFails, MockValidatorAlwaysPasses);
                    expect(validator.validate(true)).to.equal(null);
                });

                it("should return a string if all validators return a string", function ()
                {
                    const validator = AnyOf.apply(null, [MockValidatorAlwaysFails]);
                    expect(validator.validate(true)).to.equal(`${failMessage}`);
                });

                it("should join failure messages with 'or'", function ()
                {
                    const validator = AnyOf(MockValidatorAlwaysFails, MockValidatorAlwaysFails);
                    expect(validator.validate(true)).to.equal(`${failMessage} or ${failMessage}`);
                });
            });

            describe("AllOf", function ()
            {
                it("should throw if passed an empty array", function ()
                {
                    expect(() => AllOf.apply(null, [])).to.throw();
                });

                it("should throw if passed an array with null members", function ()
                {
                    expect(() => AllOf(Number, null)).to.throw();
                });

                it("should return null if all validators return null", function ()
                {
                    const validator = AllOf(MockValidatorAlwaysPasses, MockValidatorAlwaysPasses);
                    expect(validator.validate(true)).to.equal(null);
                });

                it("should return a string if any validator returns a string", function ()
                {
                    const validator = AllOf(MockValidatorAlwaysPasses, MockValidatorAlwaysFails);
                    expect(validator.validate(true)).to.equal(`${failMessage}`);
                });
            });

            describe("OneOf", function ()
            {
                it("should throw if passed an empty array", function ()
                {
                    expect(() => OneOf.apply(null, [])).to.throw();
                });

                it("should return null if the array contains the passed value", function ()
                {
                    const validator = OneOf("valueA", "valueB");
                    expect(validator.validate("valueA")).to.equal(null);
                    expect(validator.validate("valueB")).to.equal(null);
                });

                it("should return a string if the array does not contain the passed value", function ()
                {
                    const validator = OneOf("valueA", "valueB");
                    expect(validator.validate("valueC")).to.be.a("string");
                });

                it("should return null if passed null or undefined", function ()
                {
                    const validator = OneOf("valueA", "valueB");
                    expect(validator.validate(null), "should return null if passed null").to.equal(null);
                    expect(validator.validate(undefined), "should return null if passed undefined").to.equal(null);
                });
            });

            describe("Array.NotEmpty", function ()
            {
                it("should return a string if the array length is 0", function ()
                {
                    expect(Array.NotEmpty.validate([])).to.equal("must not be empty");
                });

                it("should return null if the array length is not 0", function ()
                {
                    expect(Array.NotEmpty.validate([1])).to.equal(null);
                });
            });

            describe("Number.InRange", function ()
            {
                it("should return a string if passed a number less than the minimum value", function ()
                {
                    const validator = Number.InRange(0, 5);
                    expect(validator.validate(-0.1)).to.equal(`must be at least ${0}`);
                });

                it("should return a string if passed a number greater than the maximum value", function ()
                {
                    const validator = Number.InRange(0, 5);
                    expect(validator.validate(5.1)).to.equal(`must not be greater than ${5}`);
                });

                it("should return null if passed a number within the range", function ()
                {
                    const validator = Number.InRange(0, 5);
                    expect(validator.validate(3)).to.equal(null);
                });

                it("should return null if passed a number equal to the minimum value", function ()
                {
                    const validator = Number.InRange(0, 5);
                    expect(validator.validate(0)).to.equal(null);
                });

                it("should return null if passed a number equal to the maximum value", function ()
                {
                    const validator = Number.InRange(0, 5);
                    expect(validator.validate(5)).to.equal(null);
                });
            });

            describe("String.NotEmpty", function ()
            {
                it("should return a string if the string length is 0", function ()
                {
                    expect(String.NotEmpty.validate("")).to.equal("must not be empty");
                });

                it("should return null if the string length is not 0", function ()
                {
                    expect(String.NotEmpty.validate("1")).to.equal(null);
                });
            });

            describe("String.Match", function ()
            {
                it("should return a string if the string does not match the provided regular expression", function ()
                {
                    const validator = String.Match(/meow/);
                    expect(validator.validate("dogs go woof!")).to.equal("must match /meow/");
                });

                it("should return null if the string matches the provided regular expression", function ()
                {
                    const validator = String.Match(/meow/);
                    expect(validator.validate("cats go meow!")).to.equal(null);
                });
            });
        });
    });
}