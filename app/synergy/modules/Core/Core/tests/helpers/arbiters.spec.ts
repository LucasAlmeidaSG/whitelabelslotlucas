namespace RS.Tests
{
    const { expect } = chai;

    describe("Arbiter.ts", function ()
    {
        describe("Arbiter", function ()
        {
            describe("ConsensusResolver", function ()
            {
                it("should resolve to the majority of declared values", function ()
                {
                    const arbiter = new Arbiter<boolean>(Arbiter.ConsensusResolver);

                    arbiter.declare(true);
                    arbiter.declare(true);
                    arbiter.declare(true);
                    arbiter.declare(false);
                    arbiter.declare(false);
                    expect(arbiter.value).to.equal(true);

                    arbiter.declare(false);
                    arbiter.declare(false);
                    expect(arbiter.value).to.equal(false);
                });
            });

            describe("AverageResolver", function ()
            {
                it("should resolve to the average of all declared values", function ()
                {
                    const arbiter = new Arbiter<number>(Arbiter.AverageResolver);

                    arbiter.declare(3);
                    arbiter.declare(5);
                    arbiter.declare(10);
                    expect(arbiter.value).to.equal(6);
                });
            });

            describe("SumResolver", function ()
            {
                it("should resolve to the sum of all declared values", function ()
                {
                    const arbiter = new Arbiter<number>(Arbiter.SumResolver);

                    arbiter.declare(5);
                    arbiter.declare(6);
                    arbiter.declare(9);
                    expect(arbiter.value).to.equal(20);
                });
            });
        });
    });
}