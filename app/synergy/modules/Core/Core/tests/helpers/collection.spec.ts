namespace RS.Tests
{
    const { expect } = chai;

    describe("Collection.ts", function ()
    {
        describe("Collection", function ()
        {
            describe("constructor", function ()
            {
                it("should initialise an empty collection", function ()
                {
                    const collection = new Collection<number>();
                    expect(collection.count).to.equal(0);
                });

                it("should initialise a collection with default items", function ()
                {
                    const collection = new Collection<number>([ 1, 2, 3 ]);
                    expect(collection.count).to.equal(3);
                });
            });

            describe("addItem", function ()
            {
                it("should add an item to the collection", function ()
                {
                    const collection = new Collection<number>();
                    expect(collection.count).to.equal(0);

                    collection.addItem(1);
                    expect(collection.count).to.equal(1);
                });

                it("should add an item to the collection after iteration is complete", function ()
                {
                    const collection = new Collection<number>([ 1 ]);
                    
                    collection.iterate((i) =>
                    {
                        collection.addItem(2);
                        expect(collection.count).to.equal(1);
                    });

                    expect(collection.count).to.equal(2);
                });
            });

            describe("contains", function ()
            {
                it("should return true if the item was found", function ()
                {
                    const collection = new Collection<number>([ 5 ]);
                    expect(collection.contains(5)).to.equal(true);
                });

                it("should return false if the item was not found", function ()
                {
                    const collection = new Collection<number>([ 5 ]);
                    expect(collection.contains(6)).to.equal(false);
                });
            });

            describe("clear", function ()
            {
                it("should remove all items from the collection", function ()
                {
                    const collection = new Collection<number>([ 1, 2, 3 ]);
                    collection.clear();
                    expect(collection.count).to.equal(0);
                });

                it("should remove all items from the collection after iteration is complete", function ()
                {
                    const collection = new Collection<number>([ 1, 2, 3 ]);
                    collection.iterate((i) =>
                    {
                        collection.clear();
                        expect(collection.count).to.equal(3);
                    });
                    expect(collection.count).to.equal(0);
                });
            });

            describe("iterate", function ()
            {
                it("should visit all items in the collection", function ()
                {
                    const collection = new Collection<number>([ 1, 2, 3 ]);
                    const visited: { [index: number]: boolean } = {};
                    collection.iterate((i) =>
                    {
                        visited[i] = true;
                    });
                    expect(visited[1]).to.be.true;
                    expect(visited[2]).to.be.true;
                    expect(visited[3]).to.be.true;
                });
            });
        });
    });
}