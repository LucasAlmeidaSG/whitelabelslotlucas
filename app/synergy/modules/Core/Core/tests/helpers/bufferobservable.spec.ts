namespace RS.Tests
{
    const { expect } = chai;
    const promiseRetries = 2;

    describe("BufferObservable.ts", function ()
    {
        describe("BufferObservable", function ()
        {
            const testResolveFunc: RS.IBufferObservable.ResolverFunction<number> = function(buffer: ReadonlyArray<number>)
            {
                let value = 0;
                for (let i = 0; i < buffer.length; ++i)
                {
                    value += (i + 1) * buffer[i];
                }
                return value;
            };

            describe("constructor", function ()
            {
                it("should initialise to initial value", function ()
                {
                    const bufferObservable = RS.IBufferObservable.create(5, 1, testResolveFunc);
                    expect(bufferObservable.value).to.equal(1*1 + 1*2 + 1*3 + 1*4 + 1*5);
                });
            });

            describe("push", function ()
            {
                it("should add a new state to the observable", function ()
                {
                    const bufferObservable = RS.IBufferObservable.create(5, 0, testResolveFunc);
                    expect(bufferObservable.value).to.equal(0*1 + 0*2 + 0*3 + 0*4 + 0*5, "0,0,0,0,0");
                    bufferObservable.push(3);
                    expect(bufferObservable.value).to.equal(0*1 + 0*2 + 0*3 + 0*4 + 3*5, "0,0,0,0,3");
                    bufferObservable.push(7);
                    expect(bufferObservable.value).to.equal(0*1 + 0*2 + 0*3 + 3*4 + 7*5, "0,0,0,3,7");
                    bufferObservable.push(10);
                    expect(bufferObservable.value).to.equal(0*1 + 0*2 + 3*3 + 7*4 + 10*5, "0,0,3,7,10");
                    bufferObservable.push(1);
                    expect(bufferObservable.value).to.equal(0*1 + 3*2 + 7*3 + 10*4 + 1*5, "0,3,7,10,1");
                    bufferObservable.push(2);
                    expect(bufferObservable.value).to.equal(3*1 + 7*2 + 10*3 + 1*4 + 2*5, "3,7,10,1,2");
                    bufferObservable.push(5);
                    expect(bufferObservable.value).to.equal(7*1 + 10*2 + 1*3 + 2*4 + 5*5, "7,10,1,2,5");
                });
            });
        });
    });
}