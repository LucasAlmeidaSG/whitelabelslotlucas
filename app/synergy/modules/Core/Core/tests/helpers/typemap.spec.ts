namespace RS.Tests
{
    const { expect } = chai;

    describe("TypeMap.ts", function ()
    {
        describe("TypeMap", function ()
        {
            class TestTypeA { }
            class TestTypeB { }
            class TestTypeC { }

            class TestBaseType { }
            class TestDerivedType extends TestBaseType { }

            describe("get", function ()
            {
                it("should return the value of a previously set key", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.get(TestTypeA)).to.be.equal(1);
                    expect(testMap.get(TestTypeB)).to.be.equal(2);
                });
                
                it("should return null for a non previously set key", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.get(TestTypeC)).to.be.null;
                });
            });

            describe("set", function ()
            {
                it("should set a value", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    expect(testMap.get(TestTypeA)).to.be.null;
                    testMap.set(TestTypeA, 1);
                    expect(testMap.get(TestTypeA)).to.be.equal(1);
                });

                it("should set unique values for two keys", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    expect(testMap.get(TestTypeA)).to.be.null;
                    expect(testMap.get(TestTypeB)).to.be.null;
                    testMap.set(TestTypeA, 1);
                    expect(testMap.get(TestTypeA)).to.be.equal(1);
                    expect(testMap.get(TestTypeB)).to.be.null;
                    testMap.set(TestTypeB, 2);
                    expect(testMap.get(TestTypeA)).to.be.equal(1);
                    expect(testMap.get(TestTypeB)).to.be.equal(2);
                });

                it("should set unique values when one key derives another", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestBaseType, 1);
                    testMap.set(TestDerivedType, 2);
                    expect(testMap.get(TestBaseType)).to.be.equal(1);
                    expect(testMap.get(TestDerivedType)).to.be.equal(2);
                });
            });

            describe("remove", function ()
            {
                it("should remove a previously set key", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    testMap.remove(TestTypeA);
                    expect(testMap.get(TestTypeA)).to.be.null;
                    expect(testMap.get(TestTypeB)).to.be.equal(2);
                });

                it("should not remove a not previously set key", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    testMap.remove(TestTypeC);
                    expect(testMap.get(TestTypeA)).to.be.equal(1);
                    expect(testMap.get(TestTypeB)).to.be.equal(2);
                    expect(testMap.get(TestTypeC)).to.be.null;
                });
            });

            describe("containsKey", function ()
            {
                it("should return true for previously set keys", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.containsKey(TestTypeA)).to.be.true;
                    expect(testMap.containsKey(TestTypeB)).to.be.true;
                });

                it("should return false for non previously set keys", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.containsKey(TestTypeC)).to.be.false;
                });
            });

            describe("containsValue", function ()
            {
                it("should return true for previously set values", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.containsValue(1)).to.be.true;
                    expect(testMap.containsValue(2)).to.be.true;
                });

                it("should return false for non previously set values", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.containsValue(3)).to.be.false;
                });
            });

            describe("keys", function ()
            {
                it("should return all previously set keys", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    const keys = testMap.keys;
                    expect(keys).have.lengthOf(2);
                    expect(keys).to.contain(TestTypeA);
                    expect(keys).to.contain(TestTypeB);
                });
            });

            describe("values", function ()
            {
                it("should return all previously set values", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    const values = testMap.values;
                    expect(values).have.lengthOf(2);
                    expect(values).to.contain(1);
                    expect(values).to.contain(2);
                });
            });

            describe("keyOf", function ()
            {
                it("should return the keys of previously set values", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.keyOf(1)).to.equal(TestTypeA);
                    expect(testMap.keyOf(2)).to.equal(TestTypeB);
                });

                it("should return null for non previously set values", function ()
                {
                    const testMap = new RS.Util.TypeMap<Function, number>();
                    testMap.set(TestTypeA, 1);
                    testMap.set(TestTypeB, 2);
                    expect(testMap.keyOf(3)).to.be.null;
                });
            });
        });
    });    
}