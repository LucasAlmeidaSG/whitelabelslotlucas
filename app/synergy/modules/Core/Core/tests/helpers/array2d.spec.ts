namespace RS.Tests
{
    const { expect } = chai;

    describe("Array2D.ts", function ()
    {
        describe("Array2D", function ()
        {
            describe("constructor", function ()
            {
                it("should initialise the passed column and row count", function ()
                {
                    const arr = new Array2D<number>(2, 3);
                    expect(arr.columns).to.equal(2);
                    expect(arr.rows).to.equal(3);
                });

                it("should initialise the data to a specific value", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    for (let col = 0; col < 2; ++col)
                    {
                        for (let row = 0; row < 3; ++row)
                        {
                            expect(arr.get(col, row)).to.equal(10);
                        }
                    }
                });
            });

            describe("set", function ()
            {
                it("should set the value at a specific position", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    arr.set(1, 1, 20);
                    expect(arr.get(1, 1)).to.equal(20);
                });

                it("should throw when position is out of bounds", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    let didError = false;
                    try
                    {
                        arr.set(3, 3, 20);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.set(2, 4, 20);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.set(-1, 0, 20);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.set(0, -1, 20);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;
                });

                it("should set ALL values", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    arr.set(20);
                    for (let col = 0; col < 2; ++col)
                    {
                        for (let row = 0; row < 3; ++row)
                        {
                            expect(arr.get(col, row)).to.equal(20);
                        }
                    }
                });
            });

            describe("get", function ()
            {
                it("should throw when position is out of bounds", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    let didError = false;
                    try
                    {
                        arr.get(3, 3);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.get(2, 4);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.get(-1, 0);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.get(0, -1);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;
                });
            });

            describe("clone", function ()
            {
                it("should create a copy of the array", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    arr.set(1, 1, 20);

                    const copy = arr.clone();
                    expect(copy).to.not.equal(arr);
                    expect(copy).to.deep.equal(arr);
                });

                it("should create a deep copy of the array", function ()
                {
                    const arr = new Array2D<number>(2, 3, 10);
                    arr.set(1, 1, 20);

                    const copy = arr.clone();
                    const copy2 = arr.clone();

                    arr.set(1, 2, 30);

                    expect(copy).to.not.equal(arr);
                    expect(copy).to.not.equal(copy2);
                    expect(copy).to.not.deep.equal(arr);
                    expect(copy).to.deep.equal(copy2);
                });
            });

            describe("equals", function ()
            {
                it("should return false if the column or row count differ", function ()
                {
                    const arr1 = new Array2D<number>(2, 3, 10);
                    const arr2 = new Array2D<number>(3, 3, 10);

                    expect(arr1.equals(arr2)).to.be.false;
                    expect(arr2.equals(arr1)).to.be.false;
                });

                it("should return false if one of the values differ", function ()
                {
                    const arr1 = new Array2D<number>(2, 3, 10);
                    const arr2 = new Array2D<number>(2, 3, 10);

                    arr2.set(1, 1, 15);

                    expect(arr1.equals(arr2)).to.be.false;
                    expect(arr2.equals(arr1)).to.be.false;
                });

                it("should return true if they are identical", function ()
                {
                    const arr1 = new Array2D<number>(2, 3, 10);
                    arr1.set(1, 1, 15);

                    const arr2 = new Array2D<number>(2, 3, 10);
                    arr2.set(1, 1, 15);

                    expect(arr1.equals(arr2)).to.be.true;
                    expect(arr2.equals(arr1)).to.be.true;
                });
            });

            describe("getColumn", function ()
            {
                it("should return the specified column", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    arr.set(0, 0, 1);
                    arr.set(1, 0, 2);
                    arr.set(0, 1, 3);
                    arr.set(1, 1, 4);
                    arr.set(0, 2, 5);
                    arr.set(1, 2, 6);

                    expect(arr.getColumn(0)).to.deep.equal([ 1, 3, 5 ]);
                    expect(arr.getColumn(1)).to.deep.equal([ 2, 4, 6 ]);
                });

                it("should return a deep copy", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    const col0 = arr.getColumn(0);
                    col0[0] = 10;
                    expect(arr.get(0, 0)).to.not.equal(10);
                });

                it("should throw when colIndex is out of bounds", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    let didError = false;
                    try
                    {
                        arr.getColumn(-1);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.getColumn(2);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;
                });
            });

            describe("setColumn", function ()
            {
                it("should set the specified column", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    
                    arr.setColumn(0, [ 1, 3, 5 ]);

                    expect(arr.get(0, 0)).to.equal(1);
                    expect(arr.get(0, 1)).to.equal(3);
                    expect(arr.get(0, 2)).to.equal(5);
                });

                it("should throw when colIndex is out of bounds", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    let didError = false;
                    try
                    {
                        arr.setColumn(-1, []);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.setColumn(2, []);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;
                });
            });

            describe("getRow", function ()
            {
                it("should return the specified row", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    arr.set(0, 0, 1);
                    arr.set(1, 0, 2);
                    arr.set(0, 1, 3);
                    arr.set(1, 1, 4);
                    arr.set(0, 2, 5);
                    arr.set(1, 2, 6);

                    expect(arr.getRow(0)).to.deep.equal([ 1, 2 ]);
                    expect(arr.getRow(1)).to.deep.equal([ 3, 4 ]);
                    expect(arr.getRow(2)).to.deep.equal([ 5, 6 ]);
                });

                it("should return a deep copy", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    const row0 = arr.getRow(0);
                    row0[0] = 10;
                    expect(arr.get(0, 0)).to.not.equal(10);
                });

                it("should throw when rowIndex is out of bounds", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    let didError = false;
                    try
                    {
                        arr.getRow(-1);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.getRow(3);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;
                });
            });

            describe("setRow", function ()
            {
                it("should set the specified row", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    
                    arr.setRow(0, [ 1, 2 ]);

                    expect(arr.get(0, 0)).to.equal(1);
                    expect(arr.get(1, 0)).to.equal(2);
                });

                it("should throw when rowIndex is out of bounds", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    let didError = false;
                    try
                    {
                        arr.setRow(-1, []);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;

                    didError = false;
                    try
                    {
                        arr.setRow(3, []);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.true;
                });
            });

            describe("toNative", function ()
            {
                it("should return a 2D jagged array", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);
                    arr.set(0, 0, 1);
                    arr.set(1, 0, 2);
                    arr.set(0, 1, 3);
                    arr.set(1, 1, 4);
                    arr.set(0, 2, 5);
                    arr.set(1, 2, 6);

                    const nativeArr = arr.toNative();

                    expect(nativeArr).to.deep.equal([
                        [ 1, 3, 5 ],
                        [ 2, 4, 6 ]
                    ]);
                });
            });

            describe("diff", function ()
            {
                it("should identify the different cells between two arrays", function ()
                {
                    const arr1 = new Array2D<number>(2, 3, 0);
                    const arr2 = arr1.clone();

                    arr1.set(1, 1, 10);
                    arr1.set(1, 2, 10);

                    const diff1 = arr1.diff(arr2);
                    expect(diff1.toNative()).to.deep.equal([
                        [ true, true, true ],
                        [ true, false, false ]
                    ]);

                    const diff2 = arr2.diff(arr1);
                    expect(diff2.toNative()).to.deep.equal([
                        [ true, true, true ],
                        [ true, false, false ]
                    ]);
                });

                it("should return null if the dimensions don't match", function ()
                {
                    const arr1 = new Array2D<number>(2, 3, 0);
                    const arr2 = new Array2D<number>(3, 3, 0);
                    const arr3 = new Array2D<number>(2, 2, 0);

                    expect(arr1.diff(arr2)).to.be.null;
                    expect(arr1.diff(arr3)).to.be.null;
                });
            });

            describe("getPositions", function ()
            {
                it("should return all positions that match the value", function ()
                {
                    const arr = new Array2D<number>(2, 3, 0);

                    arr.set(1, 1, 10);
                    arr.set(1, 2, 10);

                    const positions = arr.getPositions(10);
                    expect(positions).to.deep.equal([
                        { colIndex: 1, rowIndex: 1 },
                        { colIndex: 1, rowIndex: 2 }
                    ]);
                });
            });
        });
    });
}