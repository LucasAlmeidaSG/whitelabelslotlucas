namespace RS.Tests
{
    const { expect } = chai;

    describe("Observable.ts", function ()
    {
        describe("Observable", function ()
        {
            function onPromiseResolution(callback: () => void)
            {
                Promise.resolve().then(callback);
            }

            describe("constructor", function ()
            {
                it("should initialise the value", function ()
                {
                    const observable = new Observable(10);
                    expect(observable.value).to.equal(10);
                });
            });

            describe("onChanged", function ()
            {
                it("should be published when the value has been changed", function (done)
                {
                    const observable = new Observable(10);
                    observable.onChanged((newValue) =>
                    {
                        expect(newValue).to.equal(20);
                        expect(observable.value).to.equal(20);
                        done();
                    });
                    observable.value = 20;
                });
            });

            describe("for", function ()
            {
                it("should return a promise that resolves when the value is reached", function (done)
                {
                    const observable = new Observable(10);
                    const promise = observable.for(20);

                    let didResolve = false, didReject = false;
                    promise.then(() =>
                    {
                        didResolve = true;
                    }, () =>
                        {
                            didReject = false;
                        });

                    observable.value = 20;

                    onPromiseResolution(() =>
                    {
                        expect(didResolve).to.be.true;
                        expect(didReject).to.be.false;
                        done();
                    });
                });

                it("should return a promise that does not resolve when the value is not reached", function (done)
                {
                    const observable = new Observable(10);
                    const promise = observable.for(25);

                    let didResolve = false, didReject = false;
                    promise.then(() =>
                    {
                        didResolve = true;
                    }, () =>
                        {
                            didReject = true;
                        });

                    observable.value = 20;

                    onPromiseResolution(() =>
                    {
                        expect(didResolve).to.be.false;
                        expect(didReject).to.be.false;
                        done();
                    });
                });
            });

            describe("dispose", function ()
            {
                it("should reject any pending promises", function (done)
                {
                    const observable = new Observable(10);
                    const promise = observable.for(25);

                    let didResolve = false, didReject = false;
                    promise.then(() =>
                    {
                        didResolve = true;
                    }, () =>
                        {
                            didReject = true;
                        });

                    observable.dispose();

                    onPromiseResolution(() =>
                    {
                        expect(didResolve).to.be.false;
                        expect(didReject).to.be.true;
                        done();
                    });
                });
            });

            describe("map", function ()
            {
                it("should return an observable with a value mapped through a transform function", function ()
                {
                    const observable = new Observable(10);
                    const mappedObservable = observable.map((i) => i * 2);

                    expect(mappedObservable.value).to.equal(20);

                    observable.value = 15;

                    expect(mappedObservable.value).to.equal(30);
                });

                it("should return an observable that updates when the parent observable updates", function (done)
                {
                    const observable = new Observable(10);
                    const mappedObservable = observable.map((i) => i * 2);

                    mappedObservable.onChanged((newValue) =>
                    {
                        expect(newValue).to.equal(30);
                        expect(mappedObservable.value).to.equal(30);
                        done();
                    });

                    observable.value = 15;
                });
            });

            describe("reduce", function ()
            {
                const sumReduceFunc = (a: number, b: number) => a + b;

                it("should throw when reducing an empty array", function ()
                {
                    expect(function ()
                    {
                        Observable.reduce([], sumReduceFunc);
                    }).to.throw();
                });

                it("should create a mirror observable from one observable", function ()
                {
                    const a = new Observable(1);
                    const mirror = Observable.reduce([a], sumReduceFunc);

                    expect(mirror.value).to.equal(1);

                    a.value = 3;

                    expect(mirror.value).to.equal(3);
                });

                it("should reduce two observables through a reduction function", function ()
                {
                    const a = new Observable(1);
                    const b = new Observable(2);
                    const sum = Observable.reduce([a, b], sumReduceFunc);

                    expect(sum.value).to.equal(3);

                    a.value = 5;

                    expect(sum.value).to.equal(7);

                    b.value = 0;

                    expect(sum.value).to.equal(5);
                });

                it("should reduce many observables through a reduction function", function ()
                {
                    const observables: IObservable<number>[] = [];
                    let expectedSum = 0;
                    for (let i = 0; i < 20; ++i)
                    {
                        const value = (Math.random() * 20) | 0;
                        expectedSum += value;
                        observables.push(new Observable(value));
                    }
                    const sum = Observable.reduce(observables, sumReduceFunc);

                    expect(sum.value).to.equal(expectedSum);

                    ++observables[5].value;

                    expect(sum.value).to.equal(expectedSum + 1);
                });

                it("should clean up all event listeners when disposed", function ()
                {
                    const a = new Observable(1);

                    expect((a.onChanged as any)._event._handlers.length).to.equal(0);

                    const mirror = Observable.reduce([a], sumReduceFunc);

                    expect((a.onChanged as any)._event._handlers.length).to.equal(1);

                    mirror.dispose();

                    expect((a.onChanged as any)._event._handlers.length).to.equal(0);
                });
            });

            describe("link", function ()
            {
                it("should simplex link left to right", function ()
                {
                    const a = new Observable(5);
                    const b = new Observable(10);

                    const link = Observable.link(a, b, Observable.LinkDirection.LeftToRight);

                    // Initial sync
                    expect(a.value).to.equal(5);
                    expect(b.value).to.equal(5);

                    // Left -> Right
                    a.value = 7;
                    expect(b.value).to.equal(7);

                    // Left |- right
                    b.value = 8;
                    expect(a.value).to.equal(7);

                    // Disposal
                    link.dispose();
                    a.value = 0;
                    expect(b.value).to.equal(8);
                });

                it("should simplex link right to left", function ()
                {
                    const a = new Observable(5);
                    const b = new Observable(10);

                    const link = Observable.link(a, b, Observable.LinkDirection.RightToLeft);

                    // Initial sync
                    expect(a.value).to.equal(10);
                    expect(b.value).to.equal(10);

                    // Left <- Right
                    b.value = 7;
                    expect(a.value).to.equal(7);

                    // Left -| right
                    a.value = 8;
                    expect(b.value).to.equal(7);

                    // Disposal
                    link.dispose();
                    b.value = 0;
                    expect(a.value).to.equal(8);
                });

                it("should duplex link both ways", function ()
                {
                    const a = new Observable(5);
                    const b = new Observable(10);

                    const link = Observable.link(a, b, Observable.LinkDirection.Duplex);

                    // Initial sync
                    expect(a.value).to.equal(5);
                    expect(b.value).to.equal(5);

                    // Left -> Right
                    a.value = 7;
                    expect(b.value).to.equal(7);

                    // Left <- Right
                    b.value = 8;
                    expect(a.value).to.equal(8);

                    // Disposal
                    link.dispose();
                    b.value = 0;
                    expect(a.value).to.equal(8);
                    a.value = 3;
                    expect(b.value).to.equal(0);
                });
            });
        });
    });

    describe("ObservableStateMachine.ts", function ()
    {
        describe("setTransitionHandler", function ()
        {
            // Simple test
            it("should invoke an arbitrary transition handler from one distinct value to another", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);
                const spy = sinon.spy();
                stateMachine.setTransitionHandler(0, 1, spy);

                // Run
                observable.value = 1;

                // Assert
                expect(spy.callCount, "it should invoke the handler once").to.equal(1);
                expect(spy.lastCall.args[0], "it should pass 'from' as arg 0").to.equal(0);
                expect(spy.lastCall.args[1], "it should pass 'to' as arg 1").to.equal(1);

                // Tear down
                observable.dispose();
            });

            // Enforce identity equals
            it("should invoke an arbitrary transition handler from null to undefined", function ()
            {
                // Set up
                const observable = new Observable(null);
                const stateMachine = new ObservableStateMachine(observable);
                const spy = sinon.spy();
                stateMachine.setTransitionHandler(null, undefined, spy);

                // Run
                observable.value = undefined;

                // Assert
                expect(spy.callCount, "it should invoke the handler once").to.equal(1);
                expect(spy.lastCall.args[0], "it should pass 'from' as arg 0").to.equal(null);
                expect(spy.lastCall.args[1], "it should pass 'to' as arg 1").to.equal(undefined);

                // Tear down
                observable.dispose();
            });

            // Enforce singular transitions
            it("should override an existing transition handler with the same 'from' and 'to'", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                const uncalled = sinon.spy();
                const called = sinon.spy();
                stateMachine.setTransitionHandler(0, 1, uncalled);
                stateMachine.setTransitionHandler(0, 1, called);

                // Run
                observable.value = 1;

                // Assert
                expect(uncalled.notCalled, "it should not invoke the first handler").to.equal(true);
                expect(called.called, "it should invoke the second handler").to.equal(true);

                // Tear down
                observable.dispose();
            });

            // Allow unsetting of transitions
            it("should not invoke a handler if it was set to null afterwards", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                const uncalled = sinon.spy();
                stateMachine.setTransitionHandler(0, 1, uncalled);
                stateMachine.setTransitionHandler(0, 1, null);

                // Run
                observable.value = 1;

                // Assert
                expect(uncalled.notCalled, "it should not invoke the first handler").to.equal(true);

                // Tear down
                observable.dispose();
            });

            // Handle invalid transitions
            it("should throw if from === to", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                // Run + Assert
                const spy = sinon.spy();
                expect(() => stateMachine.setTransitionHandler(0, 0, spy)).to.throw();

                // Tear down
                observable.dispose();
            });
        });

        describe("setStateEnterHandler", function ()
        {
            // Simple test
            it("should invoke an arbitrary enter handler into one distinct value", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);
                const spy = sinon.spy();
                stateMachine.setStateEnterHandler(1, spy);

                // Run
                observable.value = 1;

                // Assert
                expect(spy.callCount, "it should invoke the handler once").to.equal(1);
                expect(spy.lastCall.args[0], "it should pass 'from' as arg 0").to.equal(0);
                expect(spy.lastCall.args[1], "it should pass 'to' as arg 1").to.equal(1);

                // Tear down
                observable.dispose();
            });

            // Enforce identity equals
            it("should invoke an arbitrary handler into undefined", function ()
            {
                // Set up
                const observable = new Observable(null);
                const stateMachine = new ObservableStateMachine(observable);
                const spy = sinon.spy();
                stateMachine.setStateEnterHandler(undefined, spy);

                // Run
                observable.value = undefined;

                // Assert
                expect(spy.callCount, "it should invoke the handler once").to.equal(1);
                expect(spy.lastCall.args[0], "it should pass 'from' as arg 0").to.equal(null);
                expect(spy.lastCall.args[1], "it should pass 'to' as arg 1").to.equal(undefined);

                // Tear down
                observable.dispose();
            });

            // Enforce singular transitions
            it("should override an existing handler with the same 'value'", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                const uncalled = sinon.spy();
                const called = sinon.spy();
                stateMachine.setStateEnterHandler(1, uncalled);
                stateMachine.setStateEnterHandler(1, called);

                // Run
                observable.value = 1;

                // Assert
                expect(uncalled.notCalled, "it should not invoke the first handler").to.equal(true);
                expect(called.called, "it should invoke the second handler").to.equal(true);

                // Tear down
                observable.dispose();
            });

            // Allow unsetting of transitions
            it("should not invoke a handler if it was set to null afterwards", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                const uncalled = sinon.spy();
                stateMachine.setStateEnterHandler(1, uncalled);
                stateMachine.setStateEnterHandler(1, null);

                // Run
                observable.value = 1;

                // Assert
                expect(uncalled.notCalled, "it should not invoke the first handler").to.equal(true);

                // Tear down
                observable.dispose();
            });
        });
        
        describe("setStateExitHandler", function ()
        {
            // Simple test
            it("should invoke an arbitrary enter handler from one distinct value", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);
                const spy = sinon.spy();
                stateMachine.setStateExitHandler(0, spy);

                // Run
                observable.value = 1;

                // Assert
                expect(spy.callCount, "it should invoke the handler once").to.equal(1);
                expect(spy.lastCall.args[0], "it should pass 'from' as arg 0").to.equal(0);
                expect(spy.lastCall.args[1], "it should pass 'to' as arg 1").to.equal(1);

                // Tear down
                observable.dispose();
            });

            // Enforce identity equals
            it("should invoke an arbitrary handler from null", function ()
            {
                // Set up
                const observable = new Observable(null);
                const stateMachine = new ObservableStateMachine(observable);
                const spy = sinon.spy();
                stateMachine.setStateExitHandler(null, spy);

                // Run
                observable.value = undefined;

                // Assert
                expect(spy.callCount, "it should invoke the handler once").to.equal(1);
                expect(spy.lastCall.args[0], "it should pass 'from' as arg 0").to.equal(null);
                expect(spy.lastCall.args[1], "it should pass 'to' as arg 1").to.equal(undefined);

                // Tear down
                observable.dispose();
            });

            // Enforce singular transitions
            it("should override an existing handler with the same 'value'", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                const uncalled = sinon.spy();
                const called = sinon.spy();
                stateMachine.setStateExitHandler(0, uncalled);
                stateMachine.setStateExitHandler(0, called);

                // Run
                observable.value = 1;

                // Assert
                expect(uncalled.notCalled, "it should not invoke the first handler").to.equal(true);
                expect(called.called, "it should invoke the second handler").to.equal(true);

                // Tear down
                observable.dispose();
            });

            // Allow unsetting of transitions
            it("should not invoke a handler if it was set to null afterwards", function ()
            {
                // Set up
                const observable = new Observable(0);
                const stateMachine = new ObservableStateMachine(observable);

                const uncalled = sinon.spy();
                stateMachine.setStateExitHandler(0, uncalled);
                stateMachine.setStateExitHandler(0, null);

                // Run
                observable.value = 1;

                // Assert
                expect(uncalled.notCalled, "it should not invoke the first handler").to.equal(true);

                // Tear down
                observable.dispose();
            });
        });
    });
}