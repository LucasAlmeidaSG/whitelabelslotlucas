namespace RS.Tests
{
    const { expect } = chai;

    describe("ObjectMap.ts", function ()
    {
        describe("ObjectMap", function ()
        {
            interface MockKey
            {
                id: number;
            }

            function hashFunc(key: MockKey)
            {
                return key.id | 0;
            }

            function equalsFunc(a: MockKey, b: MockKey)
            {
                return a.id === b.id;
            }

            beforeEach(function()
            {
                this.testMap = new ObjectMap<MockKey, number>(hashFunc, equalsFunc);
                this.testMap.set({ id: 4 }, 4);
                this.testMap.set({ id: 5 }, 5);
                this.testMap.set({ id: 2 }, 2);
                this.testMap.set({ id: -20 }, -20);
                this.testMap.set({ id: 5.2 }, 5.2);
            });

            describe("keys", function ()
            {
                it("should return all keys of a map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    const keys = map.keys;
                    expect(keys).to.deep.equal([
                        { id: 2 },
                        { id: 4 },
                        { id: 5 },
                        { id: 5.2 },
                        { id: -20 }
                    ]);
                });
            });

            describe("values", function ()
            {
                it("should return all values of a map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    const values = map.values;
                    expect(values).to.deep.equal([ 2, 4, 5, 5.2, -20 ]);
                });
            });

            describe("containsKey", function ()
            {
                it("should return true for all keys in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    const keys = map.keys;
                    for (const key of keys)
                    {
                        expect(map.containsKey(key)).to.be.true;
                    }
                });

                it("should return true for keys in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.containsKey({ id: 2 })).to.be.true;
                    expect(map.containsKey({ id: 4 })).to.be.true;
                    expect(map.containsKey({ id: 5 })).to.be.true;
                    expect(map.containsKey({ id: 5.2 })).to.be.true;
                    expect(map.containsKey({ id: -20 })).to.be.true;
                });

                it("should return false for keys not in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.containsKey({ id: 0 })).to.be.false;
                    expect(map.containsKey({ id: 3 })).to.be.false;
                    expect(map.containsKey({ id: 4.2 })).to.be.false;
                    expect(map.containsKey({ id: 5.3 })).to.be.false;
                    expect(map.containsKey({ id: -21 })).to.be.false;
                    expect(map.containsKey({ id: -20.5 })).to.be.false;
                });
            });

            describe("containsValue", function ()
            {
                it("should return true for all values in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    const values = map.values;
                    for (const value of values)
                    {
                        expect(map.containsValue(value)).to.be.true;
                    }
                });

                it("should return true for values in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.containsValue(2)).to.be.true;
                    expect(map.containsValue(4)).to.be.true;
                    expect(map.containsValue(5)).to.be.true;
                    expect(map.containsValue(5.2)).to.be.true;
                    expect(map.containsValue(-20)).to.be.true;
                });

                it("should return false for values not in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.containsValue(0)).to.be.false;
                    expect(map.containsValue(3)).to.be.false;
                    expect(map.containsValue(4.2)).to.be.false;
                    expect(map.containsValue(5.3)).to.be.false;
                    expect(map.containsValue(-21)).to.be.false;
                    expect(map.containsValue(-20.5)).to.be.false;
                });
            });

            describe("contains", function ()
            {
                it("should return true for key-value pairs in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.contains({ id: 2 }, 2)).to.be.true;
                    expect(map.contains({ id: 4 }, 4)).to.be.true;
                    expect(map.contains({ id: 5 }, 5)).to.be.true;
                    expect(map.contains({ id: 5.2 }, 5.2)).to.be.true;
                    expect(map.contains({ id: -20 }, -20)).to.be.true;
                });

                it("should return false for key-value pairs not in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.contains({ id: 2 }, 0)).to.be.false;
                    expect(map.contains({ id: 0 }, 2)).to.be.false;
                    expect(map.contains({ id: 2 }, 4)).to.be.false;
                    expect(map.contains({ id: 5 }, 5.2)).to.be.false;
                    expect(map.contains({ id: 5.2 }, 5)).to.be.false;
                });
            });

            describe("get", function ()
            {
                it("should retrieve the value for each key in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.get({ id: 2 })).to.equal(2);
                    expect(map.get({ id: 4 })).to.equal(4);
                    expect(map.get({ id: 5 })).to.equal(5);
                    expect(map.get({ id: 5.2 })).to.equal(5.2);
                    expect(map.get({ id: -20 })).to.equal(-20);
                });

                it("should return null for keys not in the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    expect(map.get({ id: 3 })).to.equal(null);
                    expect(map.get({ id: 5.3 })).to.equal(null);
                });
            });

            describe("set", function ()
            {
                it("should add a new key to the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    map.set({ id: 25 }, 25);
                    expect(map.keys).to.have.lengthOf(6);
                    expect(map.values).to.have.lengthOf(6);
                    expect(map.containsKey({ id: 25 })).to.be.true;
                    expect(map.containsValue(25)).to.be.true;
                    expect(map.contains({ id: 25 }, 25)).to.be.true;
                    expect(map.get({ id: 25 })).to.equal(25);
                });

                it("should overwrite an existing key to the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    map.set({ id: 2 }, 3);
                    expect(map.keys).to.have.lengthOf(5);
                    expect(map.values).to.have.lengthOf(5);
                    expect(map.containsKey({ id: 2 })).to.be.true;
                    expect(map.containsValue(3)).to.be.true;
                    expect(map.contains({ id: 2 }, 2)).to.be.false;
                    expect(map.contains({ id: 2 }, 3)).to.be.true;
                    expect(map.get({ id: 2 })).to.equal(3);
                });
            });

            describe("remove", function ()
            {
                it("should remove an existing key from the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    map.remove({ id: 5.2 });
                    expect(map.keys).to.have.lengthOf(4);
                    expect(map.values).to.have.lengthOf(4);
                    expect(map.containsKey({ id: 5.2 })).to.be.false;
                    expect(map.containsValue(5.2)).to.be.false;
                    expect(map.contains({ id: 5.2 }, 5.2)).to.be.false;
                    expect(map.get({ id: 5.2 })).to.equal(null);
                });

                it("should not remove an non-existing key from the map", function ()
                {
                    const map = this.testMap as ObjectMap<MockKey, number>;
                    map.remove({ id: 6 });
                    expect(map.keys).to.have.lengthOf(5);
                    expect(map.values).to.have.lengthOf(5);
                });
            });
        });
    });
}