namespace RS.Tests
{
    const { expect } = chai;

    describe("Read.ts", function ()
    {
        interface TestCase<T>
        {
            input: string;
            expect: T | null
        }

        interface Test<T>
        {
            func: (value: string) => (T | null);
            cases: TestCase<T>[];
        }

        enum MockStringEnum
        {
            Zero = "nort",
            One = "wun",
            Two = "too",
            Three = "Three",
            Four = "four"
        }

        enum MockEnum
        {
            Zero, 
            One, 
            Two, 
            Three
        }

        const tests: { [name: string]: Test<any> } =
        {
            "boolean":
            {
                func: Read.boolean,
                cases:
                [
                    { input: "true", expect: true },
                    { input: "True", expect: true },
                    { input: "1", expect: true },
                    { input: "y", expect: true },
                    { input: "Y", expect: true },

                    { input: "false", expect: false },
                    { input: "False", expect: false },
                    { input: "0", expect: false },
                    { input: "n", expect: false },
                    { input: "N", expect: false },

                    { input: "string", expect: null },
                    { input: "aSdFgH", expect: null },
                    
                    { input: "", expect: null }
                ]
            },
            "integer":
            {
                func: Read.integer,
                cases:
                [
                    { input: "1", expect: 1 },
                    { input: "9472947", expect: 9472947 },
                    { input: "-0", expect: 0 },
                    { input: "-329", expect: -329 },

                    { input: "393.25", expect: null },
                    { input: "-393.25", expect: null },

                    { input: "string", expect: null },
                    { input: "integer", expect: null },
                    { input: "true", expect: null },
                    
                    { input: "", expect: null }
                ]
            },
            "number":
            {
                func: Read.number,
                cases:
                [
                    { input: "1", expect: 1 },
                    { input: "9472947", expect: 9472947 },
                    { input: "-0", expect: 0 },
                    { input: "-329", expect: -329 },
                    { input: "393.25", expect: 393.25 },
                    { input: "-393.25", expect: -393.25 },

                    { input: "string", expect: null },
                    { input: "integer", expect: null },
                    { input: "true", expect: null },
                    
                    { input: "", expect: null }
                ]
            },
            "object":
            {
                func: Read.object,
                cases:
                [
                    { input: "1", expect: null },
                    { input: "string", expect: null },
                    { input: "{}", expect: {} },
                    { input: `{"bool": true, "string": "true", "number": 2}`, expect: { "bool": true, "string": "true", "number": 2 } },
                    
                    { input: "", expect: null }
                ]
            },
            "enumVal (number enum)":
            {
                func: (v) => Read.enumVal(MockEnum, v),
                cases:
                [
                    { input: "0", expect: MockEnum.Zero },
                    { input: "string", expect: null },
                    { input: "One", expect: MockEnum.One },
                    { input: `one`, expect: MockEnum.One },
                    
                    { input: "", expect: null }
                ]
            },
            "enumVal (string enum)":
            {
                func: (v) => Read.enumVal(MockStringEnum, v),
                cases:
                [
                    { input: "0", expect: null },
                    { input: "1", expect: null },
                    { input: "nort", expect: MockStringEnum.Zero },
                    { input: "string", expect: null },
                    { input: "One", expect: MockStringEnum.One },
                    { input: `one`, expect: MockStringEnum.One },
                    { input: `wun`, expect: MockStringEnum.One },

                    { input: `three`, expect: MockStringEnum.Three },
                    { input: `Three`, expect: MockStringEnum.Three },
                    { input: `four`, expect: MockStringEnum.Four },
                    { input: `Four`, expect: MockStringEnum.Four },
                    
                    { input: "", expect: null }
                ]
            }
        };

        for (const testName in tests)
        {
            describe(testName, function ()
            {
                const test = tests[testName];
                for (const testCase of test.cases)
                {
                    it(`should return ${testCase.expect} for "${testCase.input}"`, function ()
                    {
                        if (Is.object(testCase.expect))
                        {
                            expect(test.func(testCase.input)).to.deep.equal(testCase.expect);
                        }
                        else
                        {
                            expect(test.func(testCase.input)).to.equal(testCase.expect);
                        }
                    });
                }
            });
        }
    });
}