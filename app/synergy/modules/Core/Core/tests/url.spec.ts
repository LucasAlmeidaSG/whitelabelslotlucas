namespace RS.Tests
{
    const { expect } = chai;

    const testURL = "http://localhost/index.html?a=1&b=two&cde=fgh12";

    describe("URL.ts", function ()
    {
        describe("getParameter", function ()
        {
            it("should return null as the default value", function ()
            {
                expect(URL.getParameter("notaparam")).to.equal(null);
            });

            it("should return a given string as the default value", function ()
            {
                expect(URL.getParameter("notaparam", "mydefaultvalue")).to.equal("mydefaultvalue");
            });
        });

        describe("removeParameters", function ()
        {
            it("should not change string when passed nothing", function ()
            {
                let output = RS.URL.removeParameters([], testURL);

                expect(output).to.equal(testURL);
            });

            it("should not change string when no parameters found", function ()
            {
                let output = RS.URL.removeParameters([ "test", "ab", "cdef", "c", "d", "two" ], testURL);

                expect(output).to.equal(testURL);
            });

            it("should remove a single parameter", function ()
            {
                let output = RS.URL.removeParameters([ "a" ], testURL);
                expect(output).to.equal("http://localhost/index.html?b=two&cde=fgh12");

                output = RS.URL.removeParameters([ "b" ], testURL);
                expect(output).to.equal("http://localhost/index.html?a=1&cde=fgh12");

                output = RS.URL.removeParameters([ "cde" ], testURL);
                expect(output).to.equal("http://localhost/index.html?a=1&b=two");
            });

            it("should remove multiple parameters", function ()
            {
                let output = RS.URL.removeParameters([ "a", "b" ], testURL);
                expect(output).to.equal("http://localhost/index.html?cde=fgh12");

                output = RS.URL.removeParameters([ "b", "cde" ], testURL);
                expect(output).to.equal("http://localhost/index.html?a=1");

                output = RS.URL.removeParameters([ "cde", "a" ], testURL);
                expect(output).to.equal("http://localhost/index.html?b=two");
            });

            it("should remove all parameters", function ()
            {
                let output = RS.URL.removeParameters([ "a", "b", "cde" ], testURL);
                expect(output).to.equal("http://localhost/index.html");
            });
        });

        describe("setParameters", function ()
        {
            it("should not change string when passed nothing", function ()
            {
                let output = RS.URL.setParameters({}, testURL);

                expect(output).to.equal(testURL);
            });

            it("should add new parameters to a URL", function ()
            {
                let output = RS.URL.setParameters({ "z": "xyz", "p": "1" }, testURL);

                expect(output).to.equal("http://localhost/index.html?a=1&b=two&cde=fgh12&z=xyz&p=1");
            });

            it("should edit existing parameters in a URL", function ()
            {
                let output = RS.URL.setParameters({ "a": "2" }, testURL);

                expect(output).to.equal("http://localhost/index.html?b=two&cde=fgh12&a=2");
            });
        });

    });
}