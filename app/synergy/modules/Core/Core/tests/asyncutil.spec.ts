namespace RS.Tests
{
    const { expect } = chai;

    describe("AsyncUtil.ts", function ()
    {
        interface IMockPromise<T> extends PromiseLike<T>
        {
            resolve(value?: T | PromiseLike<T>): void;
            reject(reason?: any): void;
        }

        function createMockPromise<T>(): IMockPromise<T>
        {
            let resolveFunc: (value?: T | PromiseLike<T>) => void, rejectFunc: (reason?: any) => void;
            const promise = new Promise<T>((resolve, reject) =>
            {
                resolveFunc = resolve;
                rejectFunc = reject;
            });
            const mockPromise = promise as any as IMockPromise<T>;
            mockPromise.resolve = resolveFunc;
            mockPromise.reject = rejectFunc;
            return mockPromise;
        }

        describe("multiple", function ()
        {
            it("should resolve when all sub-promises resolve", function (done)
            {
                const mockA = createMockPromise<number>();
                const mockB = createMockPromise<number>();
                const mockC = createMockPromise<number>();

                const multi = Async.multiple([ mockA, mockB, mockC ]);
                multi.then((result) =>
                {
                    expect(result[0]).to.equal(5);
                    expect(result[1]).to.equal(10);
                    expect(result[2]).to.equal(20);

                    done();
                }, (err) =>
                {
                    done(new Error("Promise was rejected"));
                });

                mockA.resolve(5);
                mockB.resolve(10);
                mockC.resolve(20);
            });

            it("should reject when one sub-promise rejected", function (done)
            {
                const mockA = createMockPromise<void>();
                const mockB = createMockPromise<void>();
                const mockC = createMockPromise<void>();

                const multi = Async.multiple([ mockA, mockB, mockC ]);
                multi.then((result) =>
                {
                    done(new Error("Promise was resolved"));
                }, (err) =>
                {
                    done();
                });

                mockA.resolve();
                mockB.resolve();
                mockC.reject();
            });
        });
    });
}