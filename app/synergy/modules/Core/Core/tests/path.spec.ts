namespace RS.Tests
{
    const { expect } = chai;

    describe("Path.ts", function ()
    {
        describe("combine", function ()
        {
            it("should not alter a single segment", function ()
            {
                expect(Path.combine("a")).to.equal("a");
            });

            it("should combine two path segments", function ()
            {
                expect(Path.combine("a", "b")).to.equal("a/b");
            });

            it("should combine three path segments", function ()
            {
                expect(Path.combine("a", "b", "c")).to.equal("a/b/c");
            });

            it("should cope with trailing slashes", function ()
            {
                expect(Path.combine("/a", "b")).to.equal("a/b");
                expect(Path.combine("a/", "b")).to.equal("a/b");
                expect(Path.combine("a", "/b")).to.equal("a/b");
                expect(Path.combine("a", "b/")).to.equal("a/b");
            });
        });

        describe("fileName", function ()
        {
            it("should not alter a file name", function ()
            {
                expect(Path.fileName("a.txt")).to.equal("a.txt");
            });

            it("should return the file name of a short path", function ()
            {
                expect(Path.fileName("a/b.txt")).to.equal("b.txt");
            });

            it("should return the file name of a long path", function ()
            {
                expect(Path.fileName("a/b/c/d.txt")).to.equal("d.txt");
            });

            it("should return the file name of a url", function ()
            {
                expect(Path.fileName("http://www.google.com/index.html")).to.equal("index.html");
            });
        });

        describe("directoryName", function ()
        {
            it("should return an empty directory for just a file name", function ()
            {
                expect(Path.directoryName("a.txt")).to.equal("");
            });

            it("should return the directory name of a short path", function ()
            {
                expect(Path.directoryName("a/b.txt")).to.equal("a");
            });

            it("should return the directory name of a long path", function ()
            {
                expect(Path.directoryName("a/b/c/d.txt")).to.equal("a/b/c");
            });

            it("should return the directory name of a url", function ()
            {
                expect(Path.directoryName("http://www.google.com/index.html")).to.equal("http://www.google.com");
            });
        });

        describe("normalise", function ()
        {
            it("should remove '..' from a path", function ()
            {
                expect(Path.normalise("a/b/../c")).to.equal("a/c");
            });

            it("should not remove '..' from the front of a path", function ()
            {
                expect(Path.normalise("../a/b/c")).to.equal("../a/b/c");
            });

            it("should not remove multiple '..'s from a path", function ()
            {
                expect(Path.normalise("a/../../b/c")).to.equal("../b/c");
            });

            it("should remove '.' from a path", function ()
            {
                expect(Path.normalise("a/b/./c")).to.equal("a/b/c");
            });

            it("should remove '.' from the front of a path", function ()
            {
                expect(Path.normalise("./a/b/c")).to.equal("a/b/c");
            });

            it("should remove '.' from the end of a path", function ()
            {
                expect(Path.normalise("a/b/c/.")).to.equal("a/b/c");
            });
        });

        describe("relative", function ()
        {
            it("should resolve a simple 'down' path hierarchy", function ()
            {
                expect(Path.relative("a/b", "a/b/c/d")).to.equal("c/d");
            });

            it("should resolve a simple 'up' path hierarchy", function ()
            {
                expect(Path.relative("a/b/c/d", "a/b")).to.equal("../..");
            });

            it("should resolve an 'up'/'down' path hierarchy", function ()
            {
                expect(Path.relative("a/b/c/d", "a/b/e/f")).to.equal("../../e/f");
            });
        });

        describe("extension", function ()
        {
            it("should return the extension of a file name", function ()
            {
                expect(Path.extension("file.txt")).to.equal(".txt");
            });

            it("should return the extension of a path", function ()
            {
                expect(Path.extension("folder/file.txt")).to.equal(".txt");
            });

            it("should return an empty string for an extension-less file", function ()
            {
                expect(Path.extension("folder/file")).to.equal("");
            });

            it("should return only the last extension segment for a file with multiple extensions", function ()
            {
                expect(Path.extension("file.d.ts")).to.equal(".ts");
            });
        });

        describe("replaceExtension", function ()
        {
            it("should replace the extension of a file name", function ()
            {
                expect(Path.replaceExtension("file.txt", ".png")).to.equal("file.png");
            });

            it("should replace the extension of a path", function ()
            {
                expect(Path.replaceExtension("folder/file.txt", ".png")).to.equal("folder/file.png");
            });

            it("should not alter an extension-less file", function ()
            {
                expect(Path.replaceExtension("folder/file", ".png")).to.equal("folder/file");
            });

            it("should alter only the last extension segment for a file with multiple extensions", function ()
            {
                expect(Path.replaceExtension("file.d.ts", ".png")).to.equal("file.d.png");
            });
        });
    });
}