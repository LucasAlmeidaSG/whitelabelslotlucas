namespace RS.Tests
{
    const { expect } = chai;

    function doBasicEventTests(watched: boolean)
    {
        describe("SimpleEvent", function ()
        {
            it("should support no event handlers", function ()
            {
                const ev = createSimpleEvent(watched);
                ev.publish();
            });

            it("should support a single event handler", function ()
            {
                const ev = createSimpleEvent(watched);
                let num = 0;
                const func = () => ++num;
                expect(ev.has(func)).to.equal(false);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish();
                ev.publish();
                expect(num).to.equal(2);
            });

            it("should support multiple event handlers", function ()
            {
                const ev = createSimpleEvent(watched);
                let numA = 0, numB = 0;
                const funcA = () => ++numA;
                const funcB = () => ++numB;
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                ev(funcA);
                ev(funcB);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(true);
                ev.publish();
                ev.publish();
                expect(numA).to.equal(2);
                expect(numB).to.equal(2);
            });

            it("should support a single once event handler", function ()
            {
                const ev = createSimpleEvent(watched);
                let num = 0;
                const func = () => ++num;
                expect(ev.has(func)).to.equal(false);
                ev.once(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish();
                expect(ev.has(func)).to.equal(false);
                ev.publish();
                expect(ev.has(func)).to.equal(false);
                expect(num).to.equal(1);
            });

            it("should support multiple once event handlers", function ()
            {
                const ev = createSimpleEvent(watched);
                let numA = 0, numB = 0;
                const funcA = () => ++numA;
                const funcB = () => ++numB;
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                ev.once(funcA);
                ev.once(funcB);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(true);
                ev.publish();
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                ev.publish();
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                expect(numA).to.equal(1);
                expect(numB).to.equal(1);
            });

            it("should support multiple different types of event handlers", function ()
            {
                const ev = createSimpleEvent(watched);
                let numA = 0, numB = 0, numC = 0, numD = 0;
                const funcA = () => ++numA;
                const funcB = () => ++numB;
                const funcC = () => ++numC;
                const funcD = () => ++numD;
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                expect(ev.has(funcC)).to.equal(false);
                expect(ev.has(funcD)).to.equal(false);
                ev(funcA);
                ev.once(funcB);
                ev.once(funcC);
                ev(funcD);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(true);
                expect(ev.has(funcC)).to.equal(true);
                expect(ev.has(funcD)).to.equal(true);
                ev.publish();
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(false);
                expect(ev.has(funcC)).to.equal(false);
                expect(ev.has(funcD)).to.equal(true);
                ev.publish();
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(false);
                expect(ev.has(funcC)).to.equal(false);
                expect(ev.has(funcD)).to.equal(true);
                expect(numA).to.equal(2);
                expect(numB).to.equal(1);
                expect(numC).to.equal(1);
                expect(numD).to.equal(2);
            });

            it("should support removing an event handler via dispose", function ()
            {
                const ev = createSimpleEvent(watched);
                let num = 0;
                const func = () => ++num;
                expect(ev.has(func)).to.equal(false);
                const hdl = ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish();
                expect(ev.has(func)).to.equal(true);
                hdl.dispose();
                expect(ev.has(func)).to.equal(false);
                ev.publish();
                expect(ev.has(func)).to.equal(false);
                expect(num).to.equal(1);
            });

            it("should support removing an event handler via off", function ()
            {
                const ev = createSimpleEvent(watched);
                let num = 0;
                const func = () => ++num;
                expect(ev.has(func)).to.equal(false);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish();
                expect(ev.has(func)).to.equal(true);
                ev.off(func);
                expect(ev.has(func)).to.equal(false);
                ev.publish();
                expect(ev.has(func)).to.equal(false);
                expect(num).to.equal(1);
            });

            it("should support removing an event handler during publish", function ()
            {
                const ev = createSimpleEvent(watched);
                let num = 0;
                const func = () => ++num;
                const func2 = () =>
                {
                    ev.off(func);
                };
                expect(ev.has(func)).to.equal(false);
                expect(ev.has(func2)).to.equal(false);
                ev(func2);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                expect(ev.has(func2)).to.equal(true);
                ev.publish();
                expect(ev.has(func)).to.equal(false);
                expect(ev.has(func2)).to.equal(true);
                expect(num).to.equal(0);
            });

            it("should support removing the current event handler during publish", function ()
            {
                const ev = createSimpleEvent(watched);
                const func = () =>
                {
                    ev.off(func);
                };
                expect(ev.has(func)).to.equal(false);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish();
                expect(ev.has(func)).to.equal(false);
            });

            it("should throw when attempting to publish after disposal", function ()
            {
                const ev = createSimpleEvent(watched);
                ev.dispose();
                expect(() => ev.publish()).to.throw();
            });

            it("should throw when attempting to attach a handler after disposal", function ()
            {
                const ev = createSimpleEvent(watched);
                ev.dispose();
                expect(() => ev(() => Log.debug("Do stuff"))).to.throw();
            });

            it("should throw when attempting to attach a one-time handler after disposal", function ()
            {
                const ev = createSimpleEvent(watched);
                ev.dispose();
                expect(() => ev.once(() => Log.debug("Do stuff"))).to.throw();
            });

            it("should not invoke subsequent handlers if disposed during publish", function ()
            {
                const ev = createSimpleEvent(watched);
                let invoked = false;
                ev(() => ev.dispose());
                ev(() => invoked = true);
                ev.publish();
                expect(invoked, "handler should not be invoked").to.equal(false);
            });

            describe("isDisposed", function ()
            {
                it("should return false for a new event", function ()
                {
                    const ev = createSimpleEvent(watched);
                    expect(ev.isDisposed).to.equal(false);
                    ev.dispose();
                });

                it("should return true after dispose has been called", function ()
                {
                    const ev = createSimpleEvent(watched);
                    ev.dispose();
                    expect(ev.isDisposed).to.equal(true);
                });
            });

            describe("clone", function()
            {
                it("should create a new event and not clone handlers", function()
                {
                    const ev = createSimpleEvent(watched);
                    let numA = 0;
                    const func = () => ++numA;
                    ev(func);

                    const newEV = ev.clone();
                    expect(newEV.has(func)).to.equal(false, "don't copy handlers");
                    ev.publish();
                    expect(numA).to.equal(1, "callbacks not cloned, so only called once from original event");
                });

                it("should not clone isDisposed", function()
                {
                    const ev = createSimpleEvent(watched);
                    ev.dispose();
                    const disposed = ev.clone();
                    expect(disposed.isDisposed).to.equal(false, "don't copy isDisposed");
                });
            });
        });

        describe("Event", function ()
        {
            it("should support no event handlers", function ()
            {
                const ev = createEvent<number>(watched);
                ev.publish(1);
            });

            it("should support a single event handler", function ()
            {
                const ev = createEvent<number>(watched);
                let num = 0;
                const func = (i) => num += i;
                expect(ev.has(func)).to.equal(false);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish(1);
                ev.publish(2);
                expect(num).to.equal(3);
            });

            it("should support multiple event handlers", function ()
            {
                const ev = createEvent<number>(watched);
                let numA = 0, numB = 0;
                const funcA = (i) => numA += i;
                const funcB = (i) => numB += i;
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                ev(funcA);
                ev(funcB);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(true);
                ev.publish(1);
                ev.publish(2);
                expect(numA).to.equal(3);
                expect(numB).to.equal(3);
            });

            it("should support a single once event handler", function ()
            {
                const ev = createEvent<number>(watched);
                let num = 0;
                const func = (i) => num += i;
                expect(ev.has(func)).to.equal(false);
                ev.once(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish(1);
                expect(ev.has(func)).to.equal(false);
                ev.publish(2);
                expect(ev.has(func)).to.equal(false);
                expect(num).to.equal(1);
            });

            it("should support multiple once event handlers", function ()
            {
                const ev = createEvent<number>(watched);
                let numA = 0, numB = 0;
                const funcA = (i) => numA += i;
                const funcB = (i) => numB += i;
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                ev.once(funcA);
                ev.once(funcB);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(true);
                ev.publish(1);
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                ev.publish(2);
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                expect(numA).to.equal(1);
                expect(numB).to.equal(1);
            });

            it("should support multiple different types of event handlers", function ()
            {
                const ev = createEvent<number>(watched);
                let numA = 0, numB = 0, numC = 0, numD = 0;
                const funcA = (i) => numA += i;
                const funcB = (i) => numB += i;
                const funcC = (i) => numC += i;
                const funcD = (i) => numD += i;
                expect(ev.has(funcA)).to.equal(false);
                expect(ev.has(funcB)).to.equal(false);
                expect(ev.has(funcC)).to.equal(false);
                expect(ev.has(funcD)).to.equal(false);
                ev(funcA);
                ev.once(funcB);
                ev.once(funcC);
                ev(funcD);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(true);
                expect(ev.has(funcC)).to.equal(true);
                expect(ev.has(funcD)).to.equal(true);
                ev.publish(1);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(false);
                expect(ev.has(funcC)).to.equal(false);
                expect(ev.has(funcD)).to.equal(true);
                ev.publish(2);
                expect(ev.has(funcA)).to.equal(true);
                expect(ev.has(funcB)).to.equal(false);
                expect(ev.has(funcC)).to.equal(false);
                expect(ev.has(funcD)).to.equal(true);
                expect(numA).to.equal(3);
                expect(numB).to.equal(1);
                expect(numC).to.equal(1);
                expect(numD).to.equal(3);
            });

            it("should support removing an event handler via dispose", function ()
            {
                const ev = createEvent<number>(watched);
                let num = 0;
                const func = (i) => num += i;
                expect(ev.has(func)).to.equal(false);
                const hdl = ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish(1);
                expect(ev.has(func)).to.equal(true);
                hdl.dispose();
                expect(ev.has(func)).to.equal(false);
                ev.publish(2);
                expect(ev.has(func)).to.equal(false);
                expect(num).to.equal(1);
            });

            it("should support removing an event handler via off", function ()
            {
                const ev = createEvent<number>(watched);
                let num = 0;
                const func = (i) => num += i;
                expect(ev.has(func)).to.equal(false);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish(1);
                expect(ev.has(func)).to.equal(true);
                ev.off(func);
                expect(ev.has(func)).to.equal(false);
                ev.publish(2);
                expect(ev.has(func)).to.equal(false);
                expect(num).to.equal(1);
            });

            it("should support removing an event handler during publish", function ()
            {
                const ev = createEvent<number>(watched);
                let num = 0;
                const func = (i) => num += i;
                const func2 = (i) =>
                {
                    ev.off(func);
                };
                expect(ev.has(func)).to.equal(false);
                expect(ev.has(func2)).to.equal(false);
                ev(func2);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                expect(ev.has(func2)).to.equal(true);
                ev.publish(1);
                expect(ev.has(func)).to.equal(false);
                expect(ev.has(func2)).to.equal(true);
                expect(num).to.equal(0);
            });

            it("should support removing the current event handler during publish", function ()
            {
                const ev = createEvent<number>(watched);
                const func = () =>
                {
                    ev.off(func);
                };
                expect(ev.has(func)).to.equal(false);
                ev(func);
                expect(ev.has(func)).to.equal(true);
                ev.publish(1);
                expect(ev.has(func)).to.equal(false);
            });

            it("should not invoke event handlers added during publish", function ()
            {
                const ev = createEvent<number>(watched);

                let invoked = false;
                ev.once(() =>
                {
                    ev.once(() => invoked = true);
                });

                ev.publish(1);
                expect(invoked, "invoked false after first publish").to.equal(false);

                ev.publish(1);
                expect(invoked, "invoked true after second publish").to.equal(true);
            });

            it("should throw when attempting to publish after disposal", function ()
            {
                const ev = createEvent<number>(watched);
                ev.dispose();
                expect(() => ev.publish(1)).to.throw();
            });

            it("should throw when attempting to attach a handler after disposal", function ()
            {
                const ev = createEvent<number>(watched);
                ev.dispose();
                expect(() => ev(() => Log.debug("Do stuff"))).to.throw();
            });

            it("should throw when attempting to attach a one-time handler after disposal", function ()
            {
                const ev = createEvent<number>(watched);
                ev.dispose();
                expect(() => ev.once(() => Log.debug("Do stuff"))).to.throw();
            });

            it("should not invoke subsequent handlers if disposed during publish", function ()
            {
                const ev = createEvent<number>(watched);
                let invoked = false;
                ev(() => ev.dispose());
                ev(() => invoked = true);
                ev.publish(1);
                expect(invoked, "handler should not be invoked").to.equal(false);
            });

            describe("isDisposed", function ()
            {
                it("should return false for a new event", function ()
                {
                    const ev = createEvent<number>(watched);
                    expect(ev.isDisposed).to.equal(false);
                    ev.dispose();
                });

                it("should return true after dispose has been called", function ()
                {
                    const ev = createEvent<number>(watched);
                    ev.dispose();
                    expect(ev.isDisposed).to.equal(true);
                });
            });

            describe("clone", function()
            {
                it("should create a new event and not clone handlers", function()
                {
                    const ev = createEvent<number>(watched);
                    let numA = 0;
                    const func = (val) => numA += val;
                    ev(func);

                    const newEV = ev.clone();
                    expect(newEV.has(func)).to.equal(false, "don't copy handlers");
                    ev.publish(1);
                    expect(numA).to.equal(1, "callbacks not cloned, so only called once from original event");
                });

                it("should not clone isDisposed", function()
                {
                    const ev = createEvent<number>(watched);
                    ev.dispose();
                    const disposed = ev.clone();
                    expect(disposed.isDisposed).to.equal(false, "don't copy isDisposed");
                });
            });
        });

        describe("EventListenerHandle", function ()
        {
            describe("isDisposed", function ()
            {
                function noop() { return; }

                it("should return false for a new listener", function ()
                {
                    const ev = createEvent<number>(watched);
                    const handle = ev(noop);
                    expect(handle.isDisposed).to.equal(false);
                });

                it("should return true after dispose has been called", function ()
                {
                    const ev = createEvent<number>(watched);
                    const handle = ev(noop);
                    handle.dispose();
                    expect(handle.isDisposed).to.equal(true);
                });

                it("should return true if the event has been disposed", function ()
                {
                    const ev = createEvent<number>(watched);
                    const handle = ev(noop);
                    ev.dispose();
                    expect(handle.isDisposed).to.equal(true);
                });
            });
        });
    }

    describe("Event.ts", function ()
    {
        describe("Events", function ()
        {
            doBasicEventTests(false);
        });

        describe("Watched Events", function ()
        {
            doBasicEventTests(true);

            describe("SimpleEvent", function ()
            {
                it("should support listener count", function ()
                {
                    const ev = createSimpleEvent(true);
                    expect(ev.listenerCount.value).to.equal(0);
                    const hdl = ev(() => { return; });
                    expect(ev.listenerCount.value).to.equal(1);
                    hdl.dispose();
                    expect(ev.listenerCount.value).to.equal(0);
                });

            });
        });

        describe("Event.any(events)", function()
        {
            it("should return an event if passed an empty array", function ()
            {
                expect(Event.any([])).not.to.be.null;
            });

            it("should return an event which is published when simple events passed in are published", function ()
            {
                const mockEvA = createSimpleEvent();
                const mockEvB = createSimpleEvent();
                const ev = Event.any([mockEvA, mockEvB]);

                let called = false;

                ev.once(() => called = true);
                expect(called, "should not be called by .once(cb)").to.equal(false);

                mockEvA.publish();
                expect(called, "should be called after .publish()").to.equal(true);
            });

            it("should return an event which is published with event data when events passed in are published", function ()
            {
                const mockEvA = createEvent<boolean>();
                const mockEvB = createEvent<number>();
                const ev = Event.any<number | boolean>([mockEvA, mockEvB]);

                let data: Event.CompositeEventData<number | boolean>;
                ev((d) => data = d);

                mockEvA.publish(true);
                expect(data).to.deep.equal({ index: 0, data: true });

                mockEvB.publish(42);
                expect(data).to.deep.equal({ index: 1, data: 42 });
            });

            it("should return an event which disposes of its listeners when it is disposed of", function ()
            {
                const mockEvA = createSimpleEvent(true);
                const mockEvB = createSimpleEvent(true);

                expect(mockEvA.listenerCount.value, "listenerCount should equal 0 before Event.any(evs)").to.equal(0);
                expect(mockEvB.listenerCount.value, "listenerCount should equal 0 before Event.any(evs)").to.equal(0);

                const ev = Event.any([mockEvA, mockEvB]);

                expect(mockEvA.listenerCount.value, "listenerCount should equal 1 after Event.any(evs)").to.equal(1);
                expect(mockEvB.listenerCount.value, "listenerCount should equal 1 after Event.any(evs)").to.equal(1);

                ev.dispose();

                expect(mockEvA.listenerCount.value, "listenerCount should equal 0 after dispose").to.equal(0);
                expect(mockEvB.listenerCount.value, "listenerCount should equal 0 after dispose").to.equal(0);
            });
        })

        describe("Is.event", function()
        {
            const notEvents =
            [
                true, false,
                "", "string",
                0, 10,
                null, undefined,
                {}, {n: 0, b: true}, {a: () => true },
                [], [0, 1, 2],
            ];

            const events =
            [
                createSimpleEvent(),
                createSimpleEvent(true),
                createEvent<boolean>(),
                createEvent<boolean>(true),
                createEvent<{n: number, b: boolean}>(),
                createEvent<{n: number, b: boolean}>(true)
            ];

            it("should return true for events", function()
            {
                for (const event of events)
                {
                    expect(Is.event(event)).to.be.true;
                }
            });

            it("should return false for everything that isn't an event", function()
            {
                for (const notEvent of notEvents)
                {
                    expect(Is.event(notEvent)).to.be.false;
                }
            });
        });
    });
}
