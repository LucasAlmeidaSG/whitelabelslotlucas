namespace RS.Decorators.Tests
{
    const { expect } = chai;

    describe("Tag.ts", function ()
    {
        interface MockTagData
        {
            id: number;
        }

        function testBaseTag(decorator: Tag<MockTagData> | DatalessTag, kind: TagKind, withData: boolean, applyFn: () => Function)
        {
            describe("kind", function ()
            {
                it(`should equal TagKind.${TagKind[kind]}`, function ()
                {
                    expect(decorator.kind).to.equal(kind);
                });
            });

            describe("withData", function ()
            {
                it("should be true", function ()
                {
                    expect(decorator.withData).to.equal(withData);
                });
            });

            describe("onAppliedInitial", function ()
            {
                it("should be published when the tag is applied to a new class", function ()
                {
                    const spy = sinon.spy();
                    const handle = decorator.onAppliedInitial(spy);

                    const cl = applyFn();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as Function;
                    expect(ev, "event data should be correct").to.equal(cl);

                    handle.dispose();
                });
            });

            describe("classes", function ()
            {
                it("should return an array of classes decorated with this tag", function ()
                {
                    const cl0 = applyFn();
                    const cl1 = applyFn();

                    expect(decorator.classes).to.contain(cl0);
                    expect(decorator.classes).to.contain(cl1);
                });
            });
        }

        describe("ClassTag", function ()
        {
            const MockClassDecorator = Tag.create<MockTagData>(TagKind.Class);

            testBaseTag(MockClassDecorator, TagKind.Class, true, function ()
            {
                @MockClassDecorator({ id: 0 })
                class MockClass {}
                return MockClass;
            });

            describe("get", function ()
            {
                it("should return the tag data for the given class", function ()
                {
                    @MockClassDecorator({ id: 1 })
                    class MockClass {}

                    const data = MockClassDecorator.get(MockClass);
                    expect(data).to.deep.equal({ id: 1 });
                });
            });

            describe("getClassesWithTag", function ()
            {
                it("should return classes with the given data", function ()
                {
                    const data: MockTagData = { id: 2 };

                    @MockClassDecorator(data)
                    class MockClass {}

                    expect(MockClassDecorator.getClassesWithTag(data)).to.deep.equal([ MockClass ]);
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new class", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockClassDecorator.onApplied(spy);

                    @MockClassDecorator({ id: 3 })
                    class MockClass {}

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as ClassTagAppliedEventData<MockTagData>;
                    const shouldBe: ClassTagAppliedEventData<MockTagData> =
                    {
                        cl: MockClass,
                        target: MockClass,
                        val: { id: 3 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);

                    handle.dispose();
                });
            });
        });

        describe("MethodTag", function ()
        {
            const MockMethodDecorator = Tag.create<MockTagData>(TagKind.Method);

            testBaseTag(MockMethodDecorator, TagKind.Method, true, function ()
            {
                class MockClass
                {
                    @MockMethodDecorator({ id: 4 })
                    public method(arg: boolean)
                    {
                        return arg;
                    }
                }
                return MockClass;
            });

            describe("get", function ()
            {
                class MockClass
                {
                    @MockMethodDecorator({ id: 5 })
                    public static method(arg: boolean)
                    {
                        return arg;
                    }

                    @MockMethodDecorator({ id: 6 })
                    public method(arg: boolean)
                    {
                        return arg;
                    }
                }

                it("should return the static tag data for the given class", function ()
                {
                    const data = MockMethodDecorator.get(MockClass, MemberType.Static);
                    expect(data).to.deep.equal({ method: { id: 5 } });
                });

                it("should return the instance tag data for the given object", function ()
                {
                    const data = MockMethodDecorator.get(MockClass, MemberType.Instance);
                    expect(data).to.deep.equal({ method: { id: 6 } });
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new static method", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockMethodDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockMethodDecorator({ id: 7 })
                        public static method(arg: boolean)
                        {
                            return arg;
                        }
                    }

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as MethodTagAppliedEventData<MockTagData>;
                    const shouldBe: MethodTagAppliedEventData<MockTagData> =
                    {
                        cl: MockClass,
                        target: MockClass,
                        key: "method",
                        func: MockClass.method,
                        val: { id: 7 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);

                    handle.dispose();
                });

                it("should be published when the tag is applied to a new instance method", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockMethodDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockMethodDecorator({ id: 8 })
                        public method(arg: boolean)
                        {
                            return arg;
                        }
                    }

                    const obj = new MockClass();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as MethodTagAppliedEventData<MockTagData>;
                    const shouldBe: MethodTagAppliedEventData<MockTagData> =
                    {
                        cl: MockClass,
                        target: obj,
                        key: "method",
                        func: obj.method,
                        val: { id: 8 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);

                    handle.dispose();
                });
            });
        });

        describe("PropertyTag", function ()
        {
            const MockPropertyDecorator = Tag.create<MockTagData>(TagKind.Property);

            testBaseTag(MockPropertyDecorator, TagKind.Property, true, function ()
            {
                class MockClass
                {
                    @MockPropertyDecorator({ id: 9 })
                    public property: boolean = true;
                }
                return MockClass;
            });

            describe("get", function ()
            {
                class MockClass
                {
                    @MockPropertyDecorator({ id: 10 })
                    public static property: boolean = true;

                    @MockPropertyDecorator({ id: 11 })
                    public property: boolean = true;
                }

                it("should return the static tag data for the given class", function ()
                {
                    const data = MockPropertyDecorator.get(MockClass, MemberType.Static);
                    expect(data).to.deep.equal({ property: { id: 10 } });
                });

                it("should return the instance tag data for the given object", function ()
                {
                    const data = MockPropertyDecorator.get(MockClass, MemberType.Instance);
                    expect(data).to.deep.equal({ property: { id: 11 } });
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new static property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockPropertyDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockPropertyDecorator({ id: 12 })
                        public static property: boolean = true;
                    }

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as PropertyTagAppliedEventData<MockTagData>;
                    const shouldBe: PropertyTagAppliedEventData<MockTagData> =
                    {
                        cl: MockClass,
                        target: MockClass,
                        key: "property",
                        prop: Object.getOwnPropertyDescriptor(MockClass, "property"),
                        val: { id: 12 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);

                    handle.dispose();
                });

                it("should be published when the tag is applied to a new instance property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockPropertyDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockPropertyDecorator({ id: 13 })
                        public property: boolean = true;
                    }

                    const obj = new MockClass();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as PropertyTagAppliedEventData<MockTagData>;
                    const shouldBe: PropertyTagAppliedEventData<MockTagData> =
                    {
                        cl: obj.constructor,
                        target: MockClass.prototype,
                        key: "property",
                        prop: Object.getOwnPropertyDescriptor(obj.constructor, "property"),
                        val: { id: 13 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);

                    handle.dispose();
                });
            });
        });

        describe("ParameterTag", function ()
        {
            const MockParameterDecorator = Tag.create<MockTagData>(TagKind.Parameter);

            testBaseTag(MockParameterDecorator, TagKind.Parameter, true, function ()
            {
                class MockClass
                {
                    public method(@MockParameterDecorator({ id: 14 }) arg: boolean)
                    {
                        return arg;
                    }
                }
                return MockClass;
            });

            describe("get", function ()
            {
                class MockClass
                {
                    public static method(@MockParameterDecorator({ id: 15 }) arg: boolean)
                    {
                        return arg;
                    }

                    public method(@MockParameterDecorator({ id: 16 }) arg: boolean)
                    {
                        return arg;
                    }
                }

                it("should return the tag data for the given static method", function ()
                {
                    const data = MockParameterDecorator.get(MockClass, MemberType.Static, "method");
                    expect(data).to.deep.equal({ "0": { id: 15 } });
                });

                it("should return the tag data for the given instance method", function ()
                {
                    const data = MockParameterDecorator.get(MockClass, MemberType.Instance, "method");
                    expect(data).to.deep.equal({ "0": { id: 16 } });
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new static method property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockParameterDecorator.onApplied(spy);

                    class MockClass
                    {
                        public static method(@MockParameterDecorator({ id: 17 }) arg: boolean)
                        {
                            return arg;
                        }
                    }

                    handle.dispose();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as ParameterTagAppliedEventData<MockTagData>;
                    const shouldBe: ParameterTagAppliedEventData<MockTagData> =
                    {
                        cl: MockClass,
                        target: MockClass,
                        key: "method",
                        func: MockClass.method,
                        index: 0,
                        val: { id: 17 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);

                });

                it("should be published when the tag is applied to a new instance method property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockParameterDecorator.onApplied(spy);

                    class MockClass
                    {
                        public method(@MockParameterDecorator({ id: 18 }) arg: boolean)
                        {
                            return arg;
                        }
                    }

                    handle.dispose();

                    const obj = new MockClass();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as ParameterTagAppliedEventData<MockTagData>;
                    const shouldBe: ParameterTagAppliedEventData<MockTagData> =
                    {
                        cl: obj.constructor,
                        target: MockClass.prototype,
                        key: "method",
                        func: obj.method,
                        index: 0,
                        val: { id: 18 }
                    };
                    expect(ev, "event data should be correct").to.deep.equal(shouldBe);
                });
            });
        });

        /**
         * Expects an object to be a superset of another. Properties in subset must match, but superset can have additional properties
         *
         * This is because we don't mind if dataless tags re-use event data from data tags since TypeScript will hide that.
         */
        function expectSubset<T extends object>(superset: T, subset: Partial<T>, message?: string)
        {
            for (const key in subset)
            {
                expect(superset, message).to.have.ownProperty(key);
                expect(superset[key], message).to.deep.equal(subset[key]);
            }
        }

        describe("DatalessClassTag", function ()
        {
            const MockClassDecorator = Tag.createDataless(TagKind.Class);

            testBaseTag(MockClassDecorator, TagKind.Class, false, function ()
            {
                @MockClassDecorator
                class MockClass {}
                return MockClass;
            });

            describe("get", function ()
            {
                it("should return the tag data for the given class", function ()
                {
                    @MockClassDecorator
                    class MockClass {}

                    const data = MockClassDecorator.get(MockClass);
                    expect(data).to.equal(true);
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new class", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockClassDecorator.onApplied(spy);

                    @MockClassDecorator
                    class MockClass {}

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessClassTagAppliedEventData;
                    const shouldBe: DatalessClassTagAppliedEventData =
                    {
                        cl: MockClass,
                        target: MockClass
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");

                    handle.dispose();
                });
            });
        });

        describe("DatalessMethodTag", function ()
        {
            const MockMethodDecorator = Tag.createDataless(TagKind.Method);

            testBaseTag(MockMethodDecorator, TagKind.Method, false, function ()
            {
                class MockClass
                {
                    @MockMethodDecorator
                    public method(arg: boolean)
                    {
                        return arg;
                    }
                }
                return MockClass;
            });

            describe("get", function ()
            {
                class MockClass
                {
                    @MockMethodDecorator
                    public static method(arg: boolean)
                    {
                        return arg;
                    }

                    @MockMethodDecorator
                    public method(arg: boolean)
                    {
                        return arg;
                    }
                }

                it("should return the static tag data for the given class", function ()
                {
                    const data = MockMethodDecorator.get(MockClass, MemberType.Static);
                    expect(data).to.deep.equal(["method"]);
                });

                it("should return the instance tag data for the given object", function ()
                {
                    const data = MockMethodDecorator.get(MockClass, MemberType.Instance);
                    expect(data).to.deep.equal(["method"]);
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new static method", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockMethodDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockMethodDecorator
                        public static method(arg: boolean)
                        {
                            return arg;
                        }
                    }

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessMethodTagAppliedEventData;
                    const shouldBe: DatalessMethodTagAppliedEventData =
                    {
                        cl: MockClass,
                        target: MockClass,
                        key: "method",
                        func: MockClass.method
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");

                    handle.dispose();
                });

                it("should be published when the tag is applied to a new instance method", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockMethodDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockMethodDecorator
                        public method(arg: boolean)
                        {
                            return arg;
                        }
                    }

                    const obj = new MockClass();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessMethodTagAppliedEventData;
                    const shouldBe: DatalessMethodTagAppliedEventData =
                    {
                        cl: MockClass,
                        target: obj,
                        key: "method",
                        func: obj.method
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");

                    handle.dispose();
                });
            });
        });

        describe("DatalessPropertyTag", function ()
        {
            const MockPropertyDecorator = Tag.createDataless(TagKind.Property);

            testBaseTag(MockPropertyDecorator, TagKind.Property, false, function ()
            {
                class MockClass
                {
                    @MockPropertyDecorator
                    public property: boolean = true;
                }
                return MockClass;
            });

            describe("get", function ()
            {
                class MockClass
                {
                    @MockPropertyDecorator
                    public static property: boolean = true;

                    @MockPropertyDecorator
                    public property: boolean = true;
                }

                it("should return the static tag data for the given class", function ()
                {
                    const data = MockPropertyDecorator.get(MockClass, MemberType.Static);
                    expect(data).to.deep.equal(["property"]);
                });

                it("should return the instance tag data for the given object", function ()
                {
                    const data = MockPropertyDecorator.get(MockClass, MemberType.Instance);
                    expect(data).to.deep.equal(["property"]);
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new static property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockPropertyDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockPropertyDecorator
                        public static property: boolean = true;
                    }

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessPropertyTagTagAppliedEventData;
                    const shouldBe: DatalessPropertyTagTagAppliedEventData =
                    {
                        cl: MockClass,
                        target: MockClass,
                        key: "property",
                        prop: Object.getOwnPropertyDescriptor(MockClass, "property")
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");

                    handle.dispose();
                });

                it("should be published when the tag is applied to a new instance property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockPropertyDecorator.onApplied(spy);

                    class MockClass
                    {
                        @MockPropertyDecorator
                        public property: boolean = true;
                    }

                    const obj = new MockClass();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessPropertyTagTagAppliedEventData;
                    const shouldBe: DatalessPropertyTagTagAppliedEventData =
                    {
                        cl: obj.constructor,
                        target: MockClass.prototype,
                        key: "property",
                        prop: Object.getOwnPropertyDescriptor(obj.constructor, "property")
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");

                    handle.dispose();
                });
            });
        });

        describe("DatalessParameterTag", function ()
        {
            const MockParameterDecorator = Tag.createDataless(TagKind.Parameter);

            testBaseTag(MockParameterDecorator, TagKind.Parameter, false, function ()
            {
                class MockClass
                {
                    public method(@MockParameterDecorator arg: boolean)
                    {
                        return arg;
                    }
                }
                return MockClass;
            });

            describe("get", function ()
            {
                class MockClass
                {
                    public static method(@MockParameterDecorator arg: boolean)
                    {
                        return arg;
                    }

                    public method(@MockParameterDecorator arg: boolean)
                    {
                        return arg;
                    }
                }

                it("should return the tag data for the given static method", function ()
                {
                    const data = MockParameterDecorator.get(MockClass, MemberType.Static, "method");
                    expect(data).to.deep.equal([0]);
                });

                it("should return the tag data for the given instance method", function ()
                {
                    const data = MockParameterDecorator.get(MockClass, MemberType.Instance, "method");
                    expect(data).to.deep.equal([0]);
                });
            });

            describe("onApplied", function ()
            {
                it("should be published when the tag is applied to a new static method property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockParameterDecorator.onApplied(spy);

                    class MockClass
                    {
                        public static method(@MockParameterDecorator arg: boolean)
                        {
                            return arg;
                        }
                    }

                    handle.dispose();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessParameterTagTagAppliedEventData
                    const shouldBe: DatalessParameterTagTagAppliedEventData =
                    {
                        cl: MockClass,
                        target: MockClass,
                        key: "method",
                        func: MockClass.method,
                        index: 0
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");

                });

                it("should be published when the tag is applied to a new instance method property", function ()
                {
                    const spy = sinon.spy();
                    const handle = MockParameterDecorator.onApplied(spy);

                    class MockClass
                    {
                        public method(@MockParameterDecorator arg: boolean)
                        {
                            return arg;
                        }
                    }

                    handle.dispose();

                    const obj = new MockClass();

                    expect(spy.callCount, "event should be published").to.equal(1);

                    const ev = spy.lastCall.args[0] as DatalessParameterTagTagAppliedEventData;
                    const shouldBe: DatalessParameterTagTagAppliedEventData =
                    {
                        cl: obj.constructor,
                        target: MockClass.prototype,
                        key: "method",
                        func: obj.method,
                        index: 0
                    };
                    expectSubset(ev, shouldBe, "event data should be correct");
                });
            });
        });
    });
}