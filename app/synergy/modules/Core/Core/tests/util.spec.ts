namespace RS.Tests
{
    const { expect } = chai;

    describe("Util.ts", function ()
    {
        describe("stringCompare", function ()
        {
            it("should return 0 for equal strings", function ()
            {
                expect(Util.stringCompare("abc", "abc")).to.equal(0);
            });

            it("should return > 0 if the lexicographic value of the first different character in the first string is greater than that in the second string", function ()
            {
                expect(Util.stringCompare("abc", "aac")).to.be.greaterThan(0);
                expect(Util.stringCompare("xyz", "abc")).to.be.greaterThan(0);
            });

            it("should return < 0 if the lexicographic value of the first different character in the first string is less than that in the second string", function ()
            {
                expect(Util.stringCompare("aac", "abc")).to.be.lessThan(0);
                expect(Util.stringCompare("abc", "xyz")).to.be.lessThan(0);
            });
        });

        describe("call", function ()
        {
            it("should invoke the function in the given context", function ()
            {
                const ctx = { x: 2 };
                Util.call(function () { expect(this).to.equal(ctx); }, ctx);
            });

            it("should invoke the function with the given args", function ()
            {
                Util.call(function (...args: any[]) { expect(args).to.deep.equal([1, 2, 3, 4]); }, this, 1, 2, 3, 4);
            });
        });

        describe("bind", function ()
        {
            it("should return a function which is invoked in the given context", function ()
            {
                const ctx = { x: 2 };
                const fn = Util.bind(function () { expect(this).to.equal(ctx); }, ctx);
                fn();
            });
        });

        describe("apply", function ()
        {
            it("should invoke the function with the given args", function ()
            {
                Util.apply(function (...args: any[]) { expect(args).to.deep.equal([1, 2, 3, 4]); }, this, [1, 2, 3, 4]);
            });
        });

        describe("getClassName", function ()
        {
            it("should return the class name from an instance", function ()
            {
                class MyClass { }
                const instance = new MyClass();

                expect(Util.getClassName(instance)).to.equal("MyClass");
            })
        });

        describe("getFunctionName", function ()
        {
            it("should return the class name from a class", function ()
            {
                class MyClass { }
                expect(Util.getFunctionName(MyClass)).to.equal("MyClass");
            });

            it("should return the function name from a function", function ()
            {
                function myFunction() { return; }
                expect(Util.getFunctionName(myFunction)).to.equal("myFunction");
            });

            it("should return empty string from an anonymous function", function ()
            {
                const x = function () { return; }
                expect(Util.getFunctionName(x)).to.equal("");
            });

            it("should return empty-string from an arrow function", function ()
            {
                // This test is a bit meaningless now because of transpilation
                // If we ever switch to targeting es6 though it will become relevant

                const x = () => undefined;
                expect(Util.getFunctionName(x)).to.equal("");
            });
        });

        describe("getInheritanceChain", function ()
        {
            it("should return an array of derived classes from a class, excluding self", function ()
            {
                class ClassA { }
                class ClassB extends ClassA { }
                class ClassC extends ClassB { }

                const chain = Util.getInheritanceChain(ClassC, false);
                expect(chain).to.deep.equal([ ClassB, ClassA, Object ]);
            });

            it("should return an array of derived classes from a class, including self", function ()
            {
                class ClassA { }
                class ClassB extends ClassA { }
                class ClassC extends ClassB { }

                const chain = Util.getInheritanceChain(ClassC, true);
                expect(chain).to.deep.equal([ ClassC, ClassB, ClassA, Object ]);
            });
        });

        describe("assign", function ()
        {
            it("should shallow assign basic properties to an empty object", function ()
            {
                const target: any = {};
                const test = { x: 1, y: "hello", z: true, obj: { foo: "bar" }, arr: [ 1, 2, 3 ] };
                Util.assign(test, target, false);

                expect(target.x).to.equal(test.x);
                expect(target.y).to.equal(test.y);
                expect(target.z).to.equal(test.z);
                expect(target.obj).to.equal(test.obj);
                expect(target.arr).to.equal(test.arr);
            });

            it("should overwrite properties already on target", function ()
            {
                const target: any = { x: 3, y: "boo", z: false, obj: { x: 1, y: 2 }, arr: [ "a", "b" ] };
                const test = { x: 1, y: "hello", z: true, obj: { foo: "bar" }, arr: [ 1, 2, 3 ] };
                Util.assign(test, target, false);

                expect(target.x).to.equal(test.x);
                expect(target.y).to.equal(test.y);
                expect(target.z).to.equal(test.z);
                expect(target.obj).to.equal(test.obj);
                expect(target.arr).to.equal(test.arr);
            });

            it("should preserve properties in target that are not in source", function ()
            {
                const target: any = { x: 3, y: "boo", z: false, w: 20 };
                const test = { x: 1, y: "hello", z: true, obj: { foo: "bar" }, arr: [ 1, 2, 3 ] };
                Util.assign(test, target, false);

                expect(target.x).to.equal(test.x);
                expect(target.y).to.equal(test.y);
                expect(target.z).to.equal(test.z);
                expect(target.obj).to.equal(test.obj);
                expect(target.arr).to.equal(test.arr);
                expect(target.w).to.equal(20);
            });

            it("should preserve inner objects when deep assigning", function ()
            {
                const target: any = { obj: { bee: "wax" } };
                const test = { x: 1, y: "hello", z: true, obj: { foo: "bar" }, arr: [ 1, 2, 3 ] };
                Util.assign(test as any, target, true);

                expect(target.x).to.equal(test.x);
                expect(target.y).to.equal(test.y);
                expect(target.z).to.equal(test.z);
                expect(target.obj).to.not.equal(test.obj);
                expect(target.obj.foo).to.equal(test.obj.foo);
                expect(target.obj.bee).to.equal("wax");
            });

            it("should call constructors to instantiate null objects when deep assigning", function ()
            {
                class Class { public a: number; }
                const empty = new Class();
                const filled = new Class();
                filled.a = 1;

                const target: any = { }
                const test: any = { filled, empty, obj: { filled, empty }};
                Util.assign(test, target, true);

                expect(target).to.deep.equal(test);
            });

            it("should throw errors on functions", function()
            {
                const notConstructor = (str: string) => `${str}`;
                const target = {};
                const test = { a: "a", b: notConstructor, c: { f: notConstructor }};
                expect(() => Util.assign(test, target, true)).to.throw();
            });
        });

        describe("clone", function ()
        {
            it("should shallow clone an object", function ()
            {
                const test = { x: 1, y: "hello", z: true, obj: { foo: "bar" }, arr: [ 1, 2, 3 ] };
                const clone = Util.clone(test, false);
                expect(clone).to.not.equal(test);
                expect(clone.x).to.equal(test.x);
                expect(clone.y).to.equal(test.y);
                expect(clone.z).to.equal(test.z);
                expect(clone.obj).to.equal(test.obj);
                expect(clone.arr).to.equal(test.arr);
            });

            it("should deep clone an object", function ()
            {
                const test = { x: 1, y: "hello", z: true, obj: { foo: "bar" }, arr: [ 1, 2, 3 ] };
                const clone = Util.clone(test, true);
                expect(clone).to.not.equal(test);
                expect(clone.obj).to.not.equal(test.obj);
                expect(clone.arr).to.not.equal(test.arr);

                expect(clone).to.deep.equal(test);
            });
        });

        describe("matchAll", function ()
        {
            const textRegex = /[0-9]+/g;

            it("should return no matches", function ()
            {
                const matches = Util.matchAll(textRegex, "-abc-abc-abc-");
                expect(matches).to.have.lengthOf(0);
            });

            it("should return a single match", function ()
            {
                const matches = Util.matchAll(textRegex, "-abc-123-abc-");
                expect(matches).to.have.lengthOf(1);
                expect(matches[0][0]).to.equal("123");
            });

            it("should return multiple matches", function ()
            {
                const matches = Util.matchAll(textRegex, "-123-456-789-");
                expect(matches).to.have.lengthOf(3);
                expect(matches[0][0]).to.equal("123");
                expect(matches[1][0]).to.equal("456");
                expect(matches[2][0]).to.equal("789");
            });
        });

        describe("repeat", function ()
        {
            it("should return an empty string", function ()
            {
                expect(Util.repeat("abc", 0)).to.equal("");
            });

            it("should return a single repetition", function ()
            {
                expect(Util.repeat("abc", 1)).to.equal("abc");
            });

            it("should return multiple repetitions", function ()
            {
                expect(Util.repeat("abc", 3)).to.equal("abcabcabc");
            });
        });

        describe("padLeft", function ()
        {
            it("should do nothing to a string of sufficient length", function ()
            {
                expect(Util.padLeft("test", "-", 4)).to.equal("test");
                expect(Util.padLeft("test", "-", 3)).to.equal("test");
            });

            it("should insert single characters to reach the desired length", function ()
            {
                expect(Util.padLeft("test", "-", 5)).to.equal("-test");
                expect(Util.padLeft("test", "-", 6)).to.equal("--test");
            });

            it("should insert multiple characters to reach the desired length", function ()
            {
                expect(Util.padLeft("test", "ab", 6)).to.equal("abtest");
                expect(Util.padLeft("test", "ab", 8)).to.equal("ababtest");
            });
        });

        describe("padRight", function ()
        {
            it("should do nothing to a string of sufficient length", function ()
            {
                expect(Util.padRight("test", "-", 4)).to.equal("test");
                expect(Util.padRight("test", "-", 3)).to.equal("test");
            });

            it("should insert single characters to reach the desired length", function ()
            {
                expect(Util.padRight("test", "-", 5)).to.equal("test-");
                expect(Util.padRight("test", "-", 6)).to.equal("test--");
            });

            it("should insert multiple characters to reach the desired length", function ()
            {
                expect(Util.padRight("test", "ab", 6)).to.equal("testab");
                expect(Util.padRight("test", "ab", 8)).to.equal("testabab");
            });
        });

        describe("formatNumber", function ()
        {
            interface TestCase
            {
                value: number;
                expected: string;
            }
            function testFormat(format: string, testCases: TestCase[])
            {
                for (const testCase of testCases)
                {
                    expect(Util.formatNumber(testCase.value, format)).to.equal(testCase.expected);
                }
            }

            it("should format '0' correctly", function ()
            {
                testFormat("0",
                [
                    { value: 0, expected: "0" },
                    { value: 1, expected: "1" },
                    { value: 4.5, expected: "4" },
                    { value: 12.34, expected: "12" },
                    { value: 56.78, expected: "56" },

                    { value: -1, expected: "-1" },
                    { value: -4.5, expected: "-4" },
                    { value: -12.34, expected: "-12" },
                    { value: -56.78, expected: "-56" }
                ]);
            });

            it("should format '00.0' correctly", function ()
            {
                testFormat("00.0",
                [
                    { value: 0, expected: "00.0" },
                    { value: 1, expected: "01.0" },
                    { value: 4.5, expected: "04.5" },
                    { value: 12.34, expected: "12.3" },
                    { value: 56.78, expected: "56.8" },

                    { value: -1, expected: "-01.0" },
                    { value: -4.5, expected: "-04.5" },
                    { value: -12.34, expected: "-12.3" },
                    { value: -56.78, expected: "-56.8" }
                ]);
            });

            it("should format '000.000' correctly", function ()
            {
                testFormat("000.000",
                [
                    { value: 0, expected: "000.000" },
                    { value: 1, expected: "001.000" },
                    { value: 4.5, expected: "004.500" },
                    { value: 12.34, expected: "012.340" },
                    { value: 56.78, expected: "056.780" },

                    { value: -1, expected: "-001.000" },
                    { value: -4.5, expected: "-004.500" },
                    { value: -12.34, expected: "-012.340" },
                    { value: -56.78, expected: "-056.780" }
                ]);
            });

            it("should format 'N' correctly", function ()
            {
                testFormat("N",
                [
                    { value: 0, expected: "0" },
                    { value: 1, expected: "1" },
                    { value: 4.5, expected: "4.5" },
                    { value: 12.34, expected: "12.34" },
                    { value: 56.78, expected: "56.78" },

                    { value: -1, expected: "-1" },
                    { value: -4.5, expected: "-4.5" },
                    { value: -12.34, expected: "-12.34" },
                    { value: -56.78, expected: "-56.78" }
                ]);
            });

            it("should format '0x0' correctly", function ()
            {
                testFormat("0x0",
                [
                    { value: 0, expected: "0" },
                    { value: 1, expected: "1" },
                    { value: 4.5, expected: "4" },
                    { value: 12.34, expected: "c" },
                    { value: 56.78, expected: "38" },

                    { value: -1, expected: "-1" },
                    { value: -4.5, expected: "-4" },
                    { value: -12.34, expected: "-c" },
                    { value: -56.78, expected: "-38" }
                ]);
            });
        });

        describe("transpose2DArray", function ()
        {
            it("should not modify the array", function ()
            {
                const original =
                [
                    [ 1, 2 ],
                    [ 3, 4 ]
                ];

                const test =
                [
                    [ 1, 2 ],
                    [ 3, 4 ]
                ];

                Util.transpose2DArray(test);

                expect(test).to.deep.equal(original);

            });
            it("should transpose a simple 2x2 array", function ()
            {
                const original =
                [
                    [ 1, 2 ],
                    [ 3, 4 ]
                ];
                const expected =
                [
                    [ 1, 3 ],
                    [ 2, 4 ]
                ];
                expect(Util.transpose2DArray(original)).to.deep.equal(expected, "transform");
                expect(Util.transpose2DArray(expected)).to.deep.equal(original, "inverse transform");
            });
            it("should transpose a complex jagged array", function ()
            {
                const original =
                [
                    [ 1, 2 ],
                    [ 3, 4, 5, 6 ],
                    [ 7 ],
                    [ ],
                    [ 8, 9, 10 ],
                    [ 11 ],
                    [ 12, 13 ]
                ];
                const expected =
                [
                    [ 1, 3, 7, undefined, 8, 11, 12 ],
                    [ 2, 4, undefined, undefined, 9, undefined, 13 ],
                    [ undefined, 5, undefined, undefined, 10 ],
                    [ undefined, 6 ]
                ];
                expect(Util.transpose2DArray(original)).to.deep.equal(expected, "transform");
            });
        });

        describe("binarySearch", function ()
        {
            let testArray0: number[], testArray1: number[], testArray2: number[], testArray3: number[], testArray4: number[];

            const numberComparer = (a: number, b: number) => a - b;

            before(function ()
            {
                testArray0 = [ 2 ];
                testArray1 = [ 4, 7 ];
                testArray2 = [ 1, 3, 5 ];
                testArray3 = [ 6, 12, 13, 20 ];
                testArray4 = [ 0, 4, 8, 9, 15 ];
            });

            it("handle empty arrays", function ()
            {
                const testArray: number[] = [];

                expect(RS.Util.binarySearch(testArray, 10, numberComparer, true)).to.equal(-1);
                expect(RS.Util.binarySearch(testArray, 10, numberComparer, false)).to.equal(0);
            });

            it("should return the correct index of an exact match", function ()
            {
                {
                    expect(RS.Util.binarySearch(testArray0, 2, numberComparer, true)).to.equal(0, "test array 0");
                }
                {
                    expect(RS.Util.binarySearch(testArray1, 4, numberComparer, true)).to.equal(0, "test array 1");
                    expect(RS.Util.binarySearch(testArray1, 7, numberComparer, true)).to.equal(1, "test array 1");
                }
                {
                    expect(RS.Util.binarySearch(testArray2, 1, numberComparer, true)).to.equal(0, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 3, numberComparer, true)).to.equal(1, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 5, numberComparer, true)).to.equal(2, "test array 2");
                }
                {
                    expect(RS.Util.binarySearch(testArray3, 6, numberComparer, true)).to.equal(0, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 12, numberComparer, true)).to.equal(1, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 13, numberComparer, true)).to.equal(2, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 20, numberComparer, true)).to.equal(3, "test array 3");
                }
                {
                    expect(RS.Util.binarySearch(testArray4, 0, numberComparer, true)).to.equal(0, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 4, numberComparer, true)).to.equal(1, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 8, numberComparer, true)).to.equal(2, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 9, numberComparer, true)).to.equal(3, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 15, numberComparer, true)).to.equal(4, "test array 4");
                }
            });

            it("should return -1 when failing to find an exact match", function ()
            {
                expect(RS.Util.binarySearch(testArray0, 1, numberComparer, true)).to.equal(-1, "test array 0");
                expect(RS.Util.binarySearch(testArray1, 5, numberComparer, true)).to.equal(-1, "test array 1");
                expect(RS.Util.binarySearch(testArray2, 0, numberComparer, true)).to.equal(-1, "test array 2");
                expect(RS.Util.binarySearch(testArray3, 10, numberComparer, true)).to.equal(-1, "test array 3");
                expect(RS.Util.binarySearch(testArray4, 16, numberComparer, true)).to.equal(-1, "test array 4");
            });

            it("should return the nearest match when failing to find an exact match", function ()
            {
                {
                    expect(RS.Util.binarySearch(testArray0, 1, numberComparer, false)).to.equal(0, "test array 0");
                    expect(RS.Util.binarySearch(testArray0, 2, numberComparer, false)).to.equal(0, "test array 0");
                    expect(RS.Util.binarySearch(testArray0, 3, numberComparer, false)).to.equal(1, "test array 0");
                }
                {
                    expect(RS.Util.binarySearch(testArray1, 3, numberComparer, false)).to.equal(0, "test array 1");
                    expect(RS.Util.binarySearch(testArray1, 4, numberComparer, false)).to.equal(0, "test array 1");
                    expect(RS.Util.binarySearch(testArray1, 5, numberComparer, false)).to.equal(1, "test array 1");
                    expect(RS.Util.binarySearch(testArray1, 6, numberComparer, false)).to.equal(1, "test array 1");
                    expect(RS.Util.binarySearch(testArray1, 7, numberComparer, false)).to.equal(1, "test array 1");
                    expect(RS.Util.binarySearch(testArray1, 8, numberComparer, false)).to.equal(2, "test array 1");
                }
                {
                    expect(RS.Util.binarySearch(testArray2, 0, numberComparer, false)).to.equal(0, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 1, numberComparer, false)).to.equal(0, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 2, numberComparer, false)).to.equal(1, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 3, numberComparer, false)).to.equal(1, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 4, numberComparer, false)).to.equal(2, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 5, numberComparer, false)).to.equal(2, "test array 2");
                    expect(RS.Util.binarySearch(testArray2, 6, numberComparer, false)).to.equal(3, "test array 2");
                }
                {
                    expect(RS.Util.binarySearch(testArray3, 5, numberComparer, false)).to.equal(0, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 6, numberComparer, false)).to.equal(0, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 7, numberComparer, false)).to.equal(1, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 11, numberComparer, false)).to.equal(1, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 12, numberComparer, false)).to.equal(1, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 13, numberComparer, false)).to.equal(2, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 14, numberComparer, false)).to.equal(3, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 19, numberComparer, false)).to.equal(3, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 20, numberComparer, false)).to.equal(3, "test array 3");
                    expect(RS.Util.binarySearch(testArray3, 21, numberComparer, false)).to.equal(4, "test array 3");
                }
                {
                    expect(RS.Util.binarySearch(testArray4, -1, numberComparer, false)).to.equal(0, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 0, numberComparer, false)).to.equal(0, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 1, numberComparer, false)).to.equal(1, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 3, numberComparer, false)).to.equal(1, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 4, numberComparer, false)).to.equal(1, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 5, numberComparer, false)).to.equal(2, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 7, numberComparer, false)).to.equal(2, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 8, numberComparer, false)).to.equal(2, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 9, numberComparer, false)).to.equal(3, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 10, numberComparer, false)).to.equal(4, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 14, numberComparer, false)).to.equal(4, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 15, numberComparer, false)).to.equal(4, "test array 4");
                    expect(RS.Util.binarySearch(testArray4, 16, numberComparer, false)).to.equal(5, "test array 4");
                }
            });
        });

        describe("indexOf", function ()
        {
            it("should find the first index only when an array contains multiple matches", function ()
            {
                let output = RS.Util.indexOf(2, [1, 2, 3, 2, 5, 2, 2, 2, 1]);
                expect(output).to.equal(1);
            });

            it("should return -1 when an array contains no matches", function ()
            {
                let output = RS.Util.indexOf(2, [1, 3, 5]);
                expect(output).to.equal(-1);
            });

            it("should return -1 when passed an empty array", function ()
            {
                let output = RS.Util.indexOf(2, []);
                expect(output).to.equal(-1);
            });

            it("should accept a custom comparator function", function ()
            {
                let output = RS.Util.indexOf({ x: 2, y: 3}, [{x: 0, y: 1}, {x: 0, y: 3}, {x: 2, y: 3}, {x: 2, y: 3}], (a, b) => a.x === b.x && a.y === b.y);
                expect(output).to.equal(2);
            });
        });

        describe("lastIndexOf", function ()
        {
            it("should find the last index only when an array contains multiple matches", function ()
            {
                let output = RS.Util.lastIndexOf(2, [1, 2, 3, 2, 5, 2, 2, 2, 1]);
                expect(output).to.equal(7);
            });

            it("should return -1 when an array contains no matches", function ()
            {
                let output = RS.Util.lastIndexOf(2, [1, 3, 5]);
                expect(output).to.equal(-1);
            });
            it("should return -1 when passed an empty array", function ()
            {
                let output = RS.Util.lastIndexOf(2, []);
                expect(output).to.equal(-1);
            });

            it("should accept a custom comparator function", function ()
            {
                let output = RS.Util.lastIndexOf({ x: 2, y: 3}, [{x: 0, y: 1}, {x: 0, y: 3}, {x: 2, y: 3}, {x: 2, y: 3}], (a, b) => a.x === b.x && a.y === b.y);
                expect(output).to.equal(3);
            });
        });

        describe("insert", function ()
        {
            let comparatorFunc: (cand: { x: number; y: number }, item: { x: number; y: number }) => number;
            let reverseComparatorFunc: (cand: number, item: number) => number;

            before(function ()
            {
                comparatorFunc = (cand, item) => item.x < cand.x ? -1 : 0;
                reverseComparatorFunc = (cand, item) => item < cand ? 1 : 0;
            });

            it("should insert an item into an empty array and return 0", function ()
            {
                const array = [];
                const item = 6;

                let output = RS.Util.insert(item, array);
                expect(array[output], "the item should be at the return value").to.equal(item);
                expect(output, "the return value should be 0").to.equal(0);
            });

            it("should insert an item after the next smallest item", function ()
            {
                const array = [1, 2, 3];
                const item = 6;

                let output = RS.Util.insert(item, array);
                expect(array[output], "the item should be at the return value").to.equal(item);
                expect(output, "the return value should be 3").to.equal(3);
            });

            it("should insert an item before the next largest item", function ()
            {
                const array = [8, 11, 14];
                const item = 6;

                let output = RS.Util.insert(item, array);
                expect(array[output], "the item should be at the return value").to.equal(item);
                expect(output, "the return value should be 0").to.equal(0);
            });

            it("should insert an item between the next smallest item and the next largest item", function ()
            {
                const array = [1, 2, 3, 8, 11, 14];
                const item = 6;

                let output = RS.Util.insert(item, array);
                expect(array[output], "the item should be at the return value").to.equal(item);
                expect(output, "the return value should be 3").to.equal(3);
            });

            it("should accept a custom comparator function", function ()
            {
                const array = [{x: 0, y: 0}, {x: 3, y: 2}];
                const item = {x: 1, y: 6};
                const func = comparatorFunc;

                let output = RS.Util.insert(item, array, func);
                expect(array[output], "the item should be at the return value").to.equal(item);
                expect(output, "the return value should be 1").to.equal(1);
            });

            it("should insert an item after the next largest item when passed a reversed comparator", function ()
            {
                const array = [6];
                const item = 1;
                const func = reverseComparatorFunc;

                let output = RS.Util.insert(item, array, func);
                expect(array[output], "the item should be at the return value").to.equal(item);
                expect(output, "the return value should be 1").to.equal(1);
            });
        });

        describe("arraysEqual", function ()
        {
            it("should return true for two identical arrays of primitives", function ()
            {
                const arr1 = [ 1, 4, 10 ];
                const arr2 = [ 1, 4, 10 ];
                expect(RS.Util.arraysEqual(arr1, arr2)).to.be.true;
                expect(RS.Util.arraysEqual(arr2, arr1)).to.be.true;
            });

            it("should return true for two identical arrays of non-primitives and a comparator", function ()
            {
                const arr1 = [ { x: 1 }, { x: 4 }, { x: 10 } ];
                const arr2 = [ { x: 1 }, { x: 4 }, { x: 10 } ];
                expect(RS.Util.arraysEqual(arr1, arr2, (a, b) => a.x === b.x)).to.be.true;
                expect(RS.Util.arraysEqual(arr2, arr1, (a, b) => a.x === b.x)).to.be.true;
            });

            it("should return false for two arrays of primitives with different lengths", function ()
            {
                const arr1 = [ 1, 4, 10 ];
                const arr2 = [ 1, 4, 10, 1 ];
                expect(RS.Util.arraysEqual(arr1, arr2)).to.be.false;
                expect(RS.Util.arraysEqual(arr2, arr1)).to.be.false;
            });

            it("should return false for two arrays of primitives with different elements", function ()
            {
                const arr1 = [ 1, 4, 10 ];
                const arr2 = [ 1, 5, 10 ];
                expect(RS.Util.arraysEqual(arr1, arr2)).to.be.false;
                expect(RS.Util.arraysEqual(arr2, arr1)).to.be.false;
            });

            it("should return false for two arrays of primitives with different lengths and elements", function ()
            {
                const arr1 = [ 1, 4, 10 ];
                const arr2 = [ 1, 5, 10, 1 ];
                expect(RS.Util.arraysEqual(arr1, arr2)).to.be.false;
                expect(RS.Util.arraysEqual(arr2, arr1)).to.be.false;
            });

            it("should return false for two arrays of non-primitives and a comparator with different lengths", function ()
            {
                const arr1 = [ { x: 1 }, { x: 4 }, { x: 10 } ];
                const arr2 = [ { x: 1 }, { x: 4 }, { x: 10 }, { x: 1 } ];
                expect(RS.Util.arraysEqual(arr1, arr2, (a, b) => a.x === b.x)).to.be.false;
                expect(RS.Util.arraysEqual(arr2, arr1, (a, b) => a.x === b.x)).to.be.false;
            });

            it("should return false for two arrays of non-primitives and a comparator with different elements", function ()
            {
                const arr1 = [ { x: 1 }, { x: 4 }, { x: 10 } ];
                const arr2 = [ { x: 1 }, { x: 5 }, { x: 10 } ];
                expect(RS.Util.arraysEqual(arr1, arr2, (a, b) => a.x === b.x)).to.be.false;
                expect(RS.Util.arraysEqual(arr2, arr1, (a, b) => a.x === b.x)).to.be.false;
            });

            it("should return false for two arrays of non-primitives and a comparator with different lengths and elements", function ()
            {
                const arr1 = [ { x: 1 }, { x: 4 }, { x: 10 } ];
                const arr2 = [ { x: 1 }, { x: 5 }, { x: 10 }, { x: 1 } ];
                expect(RS.Util.arraysEqual(arr1, arr2, (a, b) => a.x === b.x)).to.be.false;
                expect(RS.Util.arraysEqual(arr2, arr1, (a, b) => a.x === b.x)).to.be.false;
            });
        });

        describe("hasOwnKeys", function ()
        {
            it("should fill out a map with true when the property is found or false when not", function ()
            {
                const obj = { x: 10, y: 20, z: 30 };
                const keys = { x: null, w: null, z: null, p: null };
                Util.hasOwnKeys(obj, keys);
                expect(keys.x).to.be.true;
                expect(keys.w).to.be.false;
                expect(keys.z).to.be.true;
                expect(keys.p).to.be.false;
            });
        });

        describe("getPropertyDescriptor", function ()
        {
            class MockSuperClass
            {
                public get returns1() { return 1; }
                public get overrideMe() { return false; }
            }

            class MockSubClass extends MockSuperClass
            {
                public get returns2() { return 2; }

                public get overrideMe() { return true; }
                public set overrideMe(value) { Log.debug(value.toString()); }
            }

            it("should return a non-inherited property descriptor", function ()
            {
                const obj = new MockSuperClass();
                const descriptor = Util.getPropertyDescriptor(obj, { returns1: true });
                expect(descriptor, "returns a descriptor").to.exist;
                expect(descriptor.get, "descriptor has getter").to.exist;
                expect(descriptor.set, "descriptor has no setter").to.not.exist;
                expect(descriptor.get(), "getter returns correct value").to.equal(1);
            });

            it("should return a non-inherited property descriptor for a non-root class", function ()
            {
                const obj = new MockSubClass();
                const subDescriptor = Util.getPropertyDescriptor(obj, { returns2: true });
                expect(subDescriptor, "returns a descriptor").to.exist;
                expect(subDescriptor.get, "descriptor has getter").to.exist;
                expect(subDescriptor.set, "descriptor has no setter").to.not.exist;
                expect(subDescriptor.get(), "getter returns correct value").to.equal(2);
            });

            it("should return an inherited property descriptor", function ()
            {
                const obj = new MockSubClass();
                const descriptor = Util.getPropertyDescriptor(obj, { returns1: true });
                expect(descriptor, "returns a descriptor").to.exist;
                expect(descriptor.get, "descriptor has getter").to.exist;
                expect(descriptor.set, "descriptor has no setter").to.not.exist;
                expect(descriptor.get(), "getter returns correct value").to.equal(1);
            });

            it("should return an overridden property descriptor", function ()
            {
                const obj = new MockSubClass();
                const descriptor = Util.getPropertyDescriptor(obj, { overrideMe: true });
                expect(descriptor, "returns a descriptor").to.exist;

                expect(descriptor.get, "descriptor has getter").to.exist;
                expect(descriptor.set, "descriptor has setter").to.exist;
                expect(descriptor.get(), "getter returns correct value").to.be.true;
            });
        });

        describe("encodeUTF8", function ()
        {
            function test(character: string, expectedValue: string, message?: string)
            {
                const code = character.charCodeAt(0);
                expect(RS.Util.encodeUTF8(code), message).to.equal(expectedValue);
            }

            it("should convert a unicode code-point to UTF-8", function ()
            {
                test("h", "h", "ASCII");
                test("a", "a", "ASCII");
                test("z", "z", "ASCII");
                test("A", "A", "ASCII");
                test("Z", "Z", "ASCII");
                test("0", "0", "ASCII");
                test("9", "9", "ASCII");

                test("£", "Â£", "U+00A3");
                test("¢", "Â¢", "U+00A2");
                test("¦", "Â¦", "U+00A6");
                test("ݪ", "Ýª", "U+076A");
                test("ࢠ", "à¢ ", "U+08A0");
                test("प", "à¤ª", "U+092A");
                test("€", "â¬", "U+20AC");
            });
        });

        describe("encodeUTF8String", function ()
        {
            function test(text: string, expectedValue: string, message?: string)
            {
                expect(RS.Util.encodeUTF8String(text), message).to.equal(expectedValue);
            }

            it("should convert a unicode string to a UTF-8 string", function ()
            {
                test("£03193.3931 is some money, like €31,22", "Â£03193.3931 is some money, like â¬31,22");
            });
        });

        describe("observe", function ()
        {
            class MockTargetClass { public y: number = 0; }

            it("should invoke a callback when the observed property is set on the target", function ()
            {
                let obj = { y: 0 };
                let changed = false;

                Util.observe(obj, { y: true }, function () { changed = true; });
                expect(changed, "not invoked immediately").to.be.false;

                obj.y = 10;
                expect(changed, "invoked on change").to.be.true;
            });

            it("should not invoke the callback when the observed property is set on another instance of the same class", function ()
            {
                let otherObj = new MockTargetClass();
                let obj = new MockTargetClass();
                let changed = false;

                Util.observe(obj, { y: true }, function () { changed = true; });
                expect(changed, "not invoked immediately").to.be.false;

                otherObj.y = 10;
                expect(changed, "not invoked on change of other object").to.be.false;
            });
        });
    });
}
