namespace RS.Tests
{
    const { expect } = chai;

    // TODO: (Synergy 1.2) revoke allowOverides boolean support

    describe("Strategy.ts", function ()
    {
        interface IMyStrategy
        {
            test(): number;
        }

        class MyStrategyClass implements IMyStrategy
        {
            public test(): number { return 10; }
        }

        class MyOtherStrategyClass implements IMyStrategy
        {
            public test(): number { return 20; }
        }

        function common(type: Strategy.Type)
        {
            it("should error when acquiring an instance with no registered class", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type);
                expect(() => myStrategy.get()).to.throw();
            });

            it("should not error when acquiring an instance with no registered class, if suppressErrors is true", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, true);
                expect(() => myStrategy.get()).not.to.throw();
            });

            it("should error when attempting to register null", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type);
                expect(() => myStrategy.register(null)).to.throw();
            });

            // Overridability

            it("should error when attempting to register a class if a class has already been registered", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type);
                myStrategy.register(MyStrategyClass);
                expect(() => myStrategy.register(MyOtherStrategyClass)).to.throw();
            });

            it("should error when attempting to register a class if a class has already been registered and allowOverrides is false", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, false);
                myStrategy.register(MyStrategyClass);
                expect(() => myStrategy.register(MyOtherStrategyClass)).to.throw();
            });

            it("should error when attempting to register a class if a class has already been registered and allowOverrides is Overridable.Never", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, Strategy.Overridable.Never);
                myStrategy.register(MyStrategyClass);
                expect(() => myStrategy.register(MyOtherStrategyClass)).to.throw();
            });

            it("should error when attempting to register a class if a class has already been registered, get() has already been called, and allowOverrides is Overridable.BeforeUse", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, Strategy.Overridable.BeforeUse);
                myStrategy.register(MyStrategyClass);
                myStrategy.get();
                expect(() => myStrategy.register(MyOtherStrategyClass)).to.throw();
            });

            it("should allow a class to be overridden if allowOverrides is Overridable.Always even if get() has been called", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, Strategy.Overridable.Always);
                myStrategy.register(MyStrategyClass);
                myStrategy.get();

                myStrategy.register(MyOtherStrategyClass);
                const inst = myStrategy.get();
                expect(inst).to.be.an.instanceOf(MyOtherStrategyClass);
            });

            it("should allow a class to be overridden if allowOverrides is Overridable.BeforeUse and get() has not been called", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, Strategy.Overridable.BeforeUse);
                myStrategy.register(MyStrategyClass);

                myStrategy.register(MyOtherStrategyClass);
                const inst = myStrategy.get()
                expect(inst).to.be.an.instanceOf(MyOtherStrategyClass);
            });

            // Default

            it("should allow a class registered with asDefault true to be overridden and get() has not been called", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type);
                myStrategy.register(MyStrategyClass, true);

                myStrategy.register(MyOtherStrategyClass);
                const inst = myStrategy.get();
                expect(inst).to.be.an.instanceOf(MyOtherStrategyClass);
            });

            it("should error when attempting to register a default class twice", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type);
                myStrategy.register(MyStrategyClass, true);
                expect(() => myStrategy.register(MyOtherStrategyClass, true)).to.throw();
            });

            it("should error when attempting to register a default class after a non-default class", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type);
                myStrategy.register(MyStrategyClass);
                expect(() => myStrategy.register(MyOtherStrategyClass, true)).to.throw();
            });

            it("should error when attempting to register a class if a class has already been registered with asDefault true and get() has already been called and allowOverrides is not Overridable.Always or true", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false);
                myStrategy.register(MyStrategyClass, true);
                myStrategy.get();

                expect(() => myStrategy.register(MyOtherStrategyClass)).to.throw();
            });

            it("should allow a class registered with asDefault true to be overridden if allowOverrides is Overridable.Always even if get() has been called", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, Strategy.Overridable.Always);
                myStrategy.register(MyStrategyClass, true);
                myStrategy.get();

                myStrategy.register(MyOtherStrategyClass);
                const inst = myStrategy.get();
                expect(inst).to.be.an.instanceOf(MyOtherStrategyClass);
            });

            it("should allow a class registered with asDefault true to be overridden if allowOverrides is true even if get() has been called", function ()
            {
                const myStrategy = Strategy.declare<IMyStrategy>(type, false, true);
                myStrategy.register(MyStrategyClass, true);
                myStrategy.get();

                myStrategy.register(MyOtherStrategyClass);
                const inst = myStrategy.get();
                expect(inst).to.be.an.instanceOf(MyOtherStrategyClass);
            });

            // Extensions

            it("should provide instances with given extension methods and properties", function ()
            {
                let methodCalled = false;
                const extensions =
                {
                    extensionMethod: function ()
                    {
                        methodCalled = true;
                        expect(this, "extension method should be called on implementation").to.be.an.instanceOf(MyStrategyClass);
                    },
                    extensionProperty:
                    {
                        get: function ()
                        {
                            expect(this, "extension property should be called on implementation").to.be.an.instanceOf(MyStrategyClass);
                            return 5;
                        }
                    }
                };

                const myStrategy = Strategy.declare<IMyStrategy, typeof extensions>(type, false, false, extensions);
                myStrategy.register(MyStrategyClass);

                const inst = myStrategy.get();
                inst.extensionMethod();
                expect(methodCalled, "extension method should work").to.be.true;
                expect(inst.extensionProperty, "extension property should work").to.equal(5);
            });

            it("should not mutate the original class", function ()
            {
                const extensions =
                {
                    extensionMethod: function () { return; },
                    extensionProperty:
                    {
                        get: function () { return 5; }
                    }
                };

                const myStrategy = Strategy.declare<IMyStrategy, typeof extensions>(type, false, false, extensions);
                myStrategy.register(MyStrategyClass);

                const inst = new MyStrategyClass();
                expect(inst, "original class should not have method").not.to.have.key("extensionMethod");
                expect(inst, "original class should not have property").not.to.have.key("extensionProperty");
            });
        }

        describe("instance strategies", function ()
        {
            common(Strategy.Type.Instance);

            it("should provide new instances of a strategy", function ()
            {
                const myInstanceStrategy = Strategy.declare<IMyStrategy>(Strategy.Type.Instance);
                myInstanceStrategy.register(MyStrategyClass);

                const myInstanceA = myInstanceStrategy.get();
                expect(myInstanceA).to.exist;
                expect(myInstanceA.test()).to.equal(10);

                const myInstanceB = myInstanceStrategy.get();
                expect(myInstanceB).to.exist;
                expect(myInstanceB.test()).to.equal(10);

                expect(myInstanceA).to.not.equal(myInstanceB);
            });
        });

        describe("singleton strategies", function ()
        {
            common(Strategy.Type.Singleton);

            it("should provide singleton instance of a strategy", function ()
            {
                const mySingletonStrategy = Strategy.declare<IMyStrategy>(Strategy.Type.Singleton);
                mySingletonStrategy.register(MyStrategyClass);

                const myInstanceA = mySingletonStrategy.get();
                expect(myInstanceA).to.exist;
                expect(myInstanceA.test()).to.equal(10);

                const myInstanceB = mySingletonStrategy.get();
                expect(myInstanceB).to.exist;
                expect(myInstanceB.test()).to.equal(10);

                expect(myInstanceA).to.equal(myInstanceB);
            });
        });
    });
}