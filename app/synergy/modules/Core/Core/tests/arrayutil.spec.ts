namespace RS.Tests
{
    const { expect } = chai;

    describe("ArrayUtil.ts", function ()
    {
        describe("copy", function ()
        {
            it("should append src array onto dst array", function ()
            {
                const arr1 = [ 1, 3, 5 ];
                const arr2 = [ 10, 12 ];
                RS.ArrayUtils.copy(arr1, arr2);
                expect(arr2).to.deep.equal([ 10, 12, 1, 3, 5 ]);
            });
        });

        describe("removeDuplicates", function ()
        {
            it("should remove duplicates from an array", function ()
            {
                const arr = [ 1, 3, 1, 2, 2, 5, 6, 7, 5, 8, 1 ];
                RS.ArrayUtils.removeDuplicates(arr);
                expect(arr).to.deep.equal([ 1, 3, 2, 5, 6, 7, 8 ]);
            });
        });

        describe("fastRemove", function ()
        {
            it("should remove items from an array", function ()
            {
                const arr = [ 1, 2, 3, 4, 5 ];
                RS.ArrayUtils.fastRemove(arr, 1);
                expect(arr).to.contain(1);
                expect(arr).to.not.contain(2);
                expect(arr).to.contain(3);
                expect(arr).to.contain(4);
                expect(arr).to.contain(5);
            });
        });

        describe("filterInPlace", function ()
        {
            it("should remove items from an array that don't pass the predicate function, preserving order", function ()
            {
                const onlyEven = (a: number) => a % 2 === 0;
                const arr = [ 1, 2, 3, 4, 5 ];
                RS.ArrayUtils.filterInPlace(arr, onlyEven, true);
                expect(arr).to.deep.equal([ 2, 4 ]);
            });

            it("should remove items from an array that don't pass the predicate function, not preserving order", function ()
            {
                const onlyEven = (a: number) => a % 2 === 0;
                const arr = [ 1, 2, 3, 4, 5 ];
                RS.ArrayUtils.filterInPlace(arr, onlyEven, false);
                expect(arr).to.not.contain(1);
                expect(arr).to.contain(2);
                expect(arr).to.not.contain(3);
                expect(arr).to.contain(4);
                expect(arr).to.not.contain(5);
            });
        });

        describe("contains", function ()
        {
            it("should return true if the array contains the item", function ()
            {
                const arr = [ 1, 2, 8, 5, 2 ];
                expect(RS.ArrayUtils.contains(arr, 1)).to.be.true;
                expect(RS.ArrayUtils.contains(arr, 2)).to.be.true;
                expect(RS.ArrayUtils.contains(arr, 8)).to.be.true;
                expect(RS.ArrayUtils.contains(arr, 5)).to.be.true;
            });

            it("should return false if the array does not contain the item", function ()
            {
                const arr = [ 1, 2, 8, 5, 2 ];
                expect(RS.ArrayUtils.contains(arr, 0)).to.be.false;
                expect(RS.ArrayUtils.contains(arr, 3)).to.be.false;
                expect(RS.ArrayUtils.contains(arr, 7.999)).to.be.false;
                expect(RS.ArrayUtils.contains(arr, -2)).to.be.false;
            });
        });
    });
}