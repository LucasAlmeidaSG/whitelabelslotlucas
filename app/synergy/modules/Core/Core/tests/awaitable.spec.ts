namespace RS.Tests
{
    const { expect } = chai;

    describe("Awaitable.ts", function ()
    {
        class MockAwaitable<T> extends Awaitable<T>
        {
            public resolve(value?: T): void { super.resolve(value); }
            public reject(value?: T): void { super.reject(value); }
            public reset() { this.clearAwaitableResult(); }
        }

        let awaitable: MockAwaitable<any>;
        beforeEach(function ()
        {
            awaitable = new MockAwaitable();
        });

        /** Defers callback using a promise. Use to ensure prior async Promise jobs have completed before running test assertions. */
        function deferPromise(done: MochaDone, callback: () => void, count: number = 1)
        {
            if (count === 1)
            {
                Promise.resolve().then(() =>
                {
                    try
                    {
                        callback();
                    }
                    catch (err)
                    {
                        done(err);
                        return;
                    }

                    done();
                });
            }
            else if (count > 1)
            {
                Promise.resolve().then(() => { deferPromise(done, callback, count - 1); });
            }
        }

        describe("resolve", function()
        {
            it("should resolve an awaiter", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                expect(resolved, "resolved").to.equal(true);
                expect(rejected, "not rejected").to.equal(false);
            });

            it("should cause any subsequent awaiters to be immediately resolved", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.resolve();
                awaitable.then(() => { resolved = true; }, () => { rejected = true; });

                expect(resolved, "resolved").to.equal(true);
                expect(rejected, "not rejected").to.equal(false);
            });

            it("should not resolve an awaiter twice", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                awaitable.then(() => { resolveCount++; }, () => { rejectCount++; });
                awaitable.resolve();
                awaitable.resolve();

                expect(resolveCount, "resolved once").to.equal(1);
                expect(rejectCount, "not rejected").to.equal(0);
            });

            it("should not resolve a rejected awaitable", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                expect(resolved, "not resolved").to.equal(false);
                expect(rejected, "rejected").to.equal(true);
            });

            it("should resolve a Promise returned from a then call", function (done)
            {
                let resolved = false;
                let rejected = false;

                awaitable.then().then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                deferPromise(done, () =>
                {
                    expect(resolved, "resolved").to.equal(true);
                    expect(rejected, "not rejected").to.equal(false);
                });
            });

            //#region catch

            it("should not invoke any catch callbacks", function (done)
            {
                let invokedCount = 0;

                awaitable.catch(() => { invokedCount++; });
                awaitable.resolve();

                deferPromise(done, () =>
                {
                    expect(invokedCount, "not invoked").to.equal(0);
                });
            });

            it("should resolve a Promise returned from a catch call", function (done)
            {
                let resolved = false;
                let rejected = false;

                awaitable.catch().then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                deferPromise(done, () =>
                {
                    expect(resolved, "resolved").to.equal(true);
                    expect(rejected, "not rejected").to.equal(false);
                });
            });

            //#endregion
            //#region finally

            it("should invoke any finally callbacks once", function (done)
            {
                let invokedCount = 0;

                awaitable.finally(() => { invokedCount++; });
                awaitable.resolve();

                deferPromise(done, () =>
                {
                    expect(invokedCount, "invoked once").to.equal(1);
                });
            });

            it("should resolve a Promise returned from a finally call", function (done)
            {
                let resolved = false;
                let rejected = false;

                awaitable.finally().then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                deferPromise(done, () =>
                {
                    expect(resolved, "resolved").to.equal(true);
                    expect(rejected, "not rejected").to.equal(false);
                });
            });

            // Using args defeats the purpose of finally so we shouldn't support it
            // This is in line with Promise.prototype.finally
            it("should invoke finally callbacks without arguments", function (done)
            {
                let result: IArguments;

                awaitable.finally(function () { result = arguments; });
                awaitable.resolve("not undefined");

                deferPromise(done, () =>
                {
                    expect(result, "callback should be invoked").to.exist;
                    for (let i = 0; i < result.length; i++)
                    {
                        expect(result[i], `argument ${i} should be undefined`).to.equal(undefined);
                    }
                });
            });

            //#endregion
        });

        describe("reject", function()
        {
            it("should reject any awaits", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                expect(rejected, "rejected").to.equal(true);
                expect(resolved, "not resolved").to.equal(false);
            });

            it("should cause any subsequent awaiters to be immediately rejected", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.reject();
                awaitable.then(() => { resolved = true; }, () => { rejected = true; });

                expect(rejected, "rejected").to.equal(true);
                expect(resolved, "not resolved").to.equal(false);
            });

            it("should not reject an awaiter twice", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                awaitable.then(() => { resolveCount++; }, () => { rejectCount++; });
                awaitable.reject();
                awaitable.reject();

                expect(rejectCount, "rejected once").to.equal(1);
                expect(resolveCount, "not resolved").to.equal(0);
            });

            it("should not reject a resolved awaitable", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                expect(rejected, "not rejected").to.equal(false);
                expect(resolved, "resolved").to.equal(true);
            });

            it("should reject a Promise returned from a then call", function (done)
            {
                let resolved = false;
                let rejected = false;

                awaitable.then().then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(rejected, "rejected").to.equal(true);
                    expect(resolved, "not resolved").to.equal(false);
                });
            });

            it("should resolve a Promise returned from a then call with a rejection callback", function (done)
            {
                let resolved = false;
                let rejected = false;
                let caught = false;

                awaitable.then(undefined, () => { caught = true; }).then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(resolved, "resolved").to.equal(true);
                    expect(rejected, "not rejected").to.equal(false);
                    expect(caught, "caught").to.equal(true);
                });
            });

            //#region catch

            it("should invoke any catch callbacks once", function (done)
            {
                let invokedCount = 0;

                awaitable.catch(() => { invokedCount++; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(invokedCount, "invoked once").to.equal(1);
                });
            });

            it("should reject a Promise returned from a catch call", function (done)
            {
                let resolved = false;
                let rejected = false;

                awaitable.catch().then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(rejected, "rejected").to.equal(true);
                    expect(resolved, "not resolved").to.equal(false);
                });
            });

            it("should resolve a Promise returned from a catch call with a callback", function (done)
            {
                let resolved = false;
                let rejected = false;
                let caught = false;

                awaitable.catch(() => { caught = true; }).then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(rejected, "not rejected").to.equal(false);
                    expect(resolved, "resolved").to.equal(true);
                    expect(resolved, "caught").to.equal(true);
                });
            });

            //#endregion
            //#region finally

            it("should invoke any finally callbacks once", function (done)
            {
                let invokedCount = 0;

                awaitable.finally(() => { invokedCount++; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(invokedCount, "invoked once").to.equal(1);
                });
            });

            it("should reject a Promise returned from a finally call", function (done)
            {
                let resolved = false;
                let rejected = false;

                awaitable.finally().then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                deferPromise(done, () =>
                {
                    expect(rejected, "rejected").to.equal(true);
                    expect(resolved, "not resolved").to.equal(false);
                });
            });

            // Using args defeats the purpose of finally so we shouldn't support it
            // This is in line with Promise.prototype.finally
            it("should invoke finally callbacks without arguments", function (done)
            {
                let result: IArguments;

                awaitable.finally(function () { result = arguments; });
                awaitable.reject("not undefined");

                deferPromise(done, () =>
                {
                    expect(result, "callback should be invoked").to.exist;
                    for (let i = 0; i < result.length; i++)
                    {
                        expect(result[i], `argument ${i} should be undefined`).to.equal(undefined);
                    }
                });
            });

            //#endregion
        });

        describe("clearAwaitableResult", function ()
        {
            it("should allow a resolution after a rejection", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                awaitable.reset();

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();
                expect(rejected, "rejected").to.equal(true);
                expect(resolved, "resolved").to.equal(true);
            });

            it("should allow a rejection after a resolution", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.resolve();

                awaitable.reset();

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });
                awaitable.reject();

                expect(rejected, "rejected").to.equal(true);
                expect(resolved, "resolved").to.equal(true);
            });

            it("should cause awaiters to no longer be immediately resolved or rejected", function ()
            {
                let resolved = false;
                let rejected = false;

                awaitable.resolve();
                awaitable.reset();

                awaitable.then(() => { resolved = true; }, () => { rejected = true; });

                expect(rejected, "not rejected").to.equal(false);
                expect(resolved, "not resolved").to.equal(false);
            });
        });
    });
}