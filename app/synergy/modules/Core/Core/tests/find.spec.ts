namespace RS.Tests
{
    const { expect } = chai;

    describe("Find.ts", function ()
    {
        describe("best", function ()
        {
            function identityFunc<T>(value: T): T { return value; }

            it("should find the highest item in an array", function ()
            {
                const items = [ 3, 1, 5, 6, 1, 0 ];

                const item = Find.best(items, identityFunc, Find.ScoreComparers.Higher);

                expect(item).to.equal(6);
            });

            it("should find the lowest item in an array", function ()
            {
                const items = [ 3, 1, 5, 6, 1, 0 ];

                const item = Find.best(items, identityFunc, Find.ScoreComparers.Lower);

                expect(item).to.equal(0);
            });

            it("should find the cloest item to 0 in an array", function ()
            {
                const items = [ 2, 3, -2.5, -10, 0.5, 7 ];

                const item = Find.best(items, identityFunc, Find.ScoreComparers.CloserToZero);

                expect(item).to.equal(0.5);
            });

            it("should find the farthest item from 0 in an array", function ()
            {
                const items = [ 2, 3, -2.5, -10, 0.5, 7 ];

                const item = Find.best(items, identityFunc, Find.ScoreComparers.FartherFromZero);

                expect(item).to.equal(-10);
            });
        });

        describe("sum", function ()
        {
            it("should return the sum of the values of an array according to a given scorer function", function ()
            {
                const arr = ["one", "two", "three", "three"];
                const valueMap = { "one": 1, "two": 2, "three": 3 };
                expect(Find.sum(arr, (item) => valueMap[item])).to.equal(9);
            });

            it("should return 0 for an empty array", function ()
            {
                const arr: number[] = [];
                expect(Find.sum(arr, (item) => item)).to.equal(0);
            });

            it("should use a default scorer for a numeric array", function ()
            {
                expect(Find.sum([1, 2, 3])).to.equal(6);
                expect(Find.sum([3, 9, 11])).to.equal(23);
            });
        });

        describe("ceiling", function ()
        {
            it("should return the last cumulative item according to the given scorer function", function ()
            {
                expect(Find.ceiling([1, 4, 8], (i) => i, 2)).to.equal(4);
                expect(Find.ceiling([1, 4, 8], (i) => i, 12)).to.equal(8);
                expect(Find.ceiling([1, 4, 8], (i) => i, 4)).to.equal(4);
                expect(Find.ceiling([1, 4, 8], (i) => i, 5)).to.equal(8);
            });

            it("should return null when the value given is equal to or higher than the sum", function ()
            {
                expect(Find.ceiling([1, 4, 8], (i) => i, 13)).to.equal(null);
            });

            it("should return null when passed a zero-length array", function ()
            {
                expect(Find.ceiling([], (i) => i, 4)).to.equal(null);
            });

            it("should use a default scorer for a numeric array", function ()
            {
                expect(Find.ceiling([1, 4, 8], 2)).to.equal(4);
                expect(Find.ceiling([1, 4, 8], 12)).to.equal(8);
            });
        });

        describe("sortedIndex", function ()
        {
            it("should return 0 for an empty array", function ()
            {
                const arr = [];
                const idx = Find.sortedIndex(arr, (a) => 1);
                expect(idx).to.equal(0);
            });

            it("should return the current index if the comparator returns 0", function ()
            {
                function testIndex(arr: number[], index: number)
                {
                    const idx = Find.sortedIndex(arr, (num, i) => i === index ? 0 : index - i);
                    expect(idx, `should return index ${index} in [${arr}]`).to.equal(index);
                }

                function testArray(arr: number[])
                {
                    for (let i = 0; i < arr.length; ++i)
                    {
                        testIndex(arr, i);
                    }
                }

                testArray([1, 2, 6, 2, 1]);
            });

            // Sorted insertion test
            it("should return the index at which the item would be placed if there is no exact match", function ()
            {
                function test(haystack: number[], needle: number, index: number)
                {
                    const idx = Find.sortedIndex(haystack, (a) => needle - a);
                    expect(idx, `${needle} should be at index ${index} in [${haystack}]`).to.equal(index);
                }

                test([1, 2, 6], 0, 0);
                test([1, 3, 6], 2, 1);
                test([1, 2, 6], 5, 2);
                test([1, 2, 6], 10, 3);
            });

            // Sorted search
            it("should return the index at which a match can be found if a match is present in the array", function ()
            {
                function testNeedle(haystack: number[], needle: number)
                {
                    const idx = Find.sortedIndex(haystack, (a) => needle - a);
                    expect(haystack[idx], `[${haystack}][${idx}] should equal ${needle}`).to.equal(needle);
                }

                function testHaystack(haystack: number[])
                {
                    for (let i = 0; i < haystack.length; ++i)
                    {
                        testNeedle(haystack, haystack[i]);
                    }
                }

                testHaystack([1, 2, 3, 5, 6]);
            });

            it("should return the leftmost index at which a match can be found if a match is present in the array", function ()
            {
                function test(haystack: number[], needle: number, index: number)
                {
                    const idx = Find.sortedIndex(haystack, (a) => needle - a);
                    expect(idx, `${needle} should be at index ${index} in [${haystack}]`).to.equal(index);
                }

                test([1, 2, 3, 3, 3, 5, 6], 3, 2);
            });
        });
    });
}