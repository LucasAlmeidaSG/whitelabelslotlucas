namespace RS.Tests
{
    const { expect } = chai;

    describe("Disposable.ts", function ()
    {
        class MockDisposable implements IDisposable
        {
            public get isDisposed() { return this._isDisposed; }

            @CheckDisposed
            public get testProperty() { return this._value; }
            public set testProperty(value: number) { this._value = value; }

            private _isDisposed: boolean = false;
            private _value: number = 10;

            @CheckDisposed
            public testMethod(): number
            {
                return 10;
            }

            public dispose()
            {
                if (this._isDisposed) { return; }
                this._isDisposed = true;
            }
        }

        class MockNonDisposable
        {
            public get testProperty() { return this._value; }
            public set testProperty(value: number) { this._value = value; }

            private _value: number = 10;

            public testMethod(): number
            {
                return 10;
            }
        }

        class MockAutoDisposer implements IDisposable
        {
            public get isDisposed() { return this._isDisposed; }

            @AutoDispose
            public thingToAutoDispose: IDisposable;

            @AutoDispose
            public arrayToAutoDispose: IDisposable[];

            public thingToNotAutoDispose: IDisposable;

            private _isDisposed: boolean = false;

            public dispose()
            {
                if (this._isDisposed) { return; }
                this._isDisposed = true;
            }
        }

        class MockAutoDisposerOnSet implements IDisposable
        {
            @AutoDisposeOnSet
            public static thingToAutoDispose: IDisposable;

            @AutoDisposeOnSet
            public static arrayToAutoDispose: IDisposable[];

            public get isDisposed() { return this._isDisposed; }

            @AutoDisposeOnSet
            public thingToAutoDispose: IDisposable;

            @AutoDisposeOnSet
            public arrayToAutoDispose: IDisposable[];

            @AutoDisposeOnSet
            public thingOrArrayToAutoDispose: OneOrMany<IDisposable>;

            public thingToNotAutoDispose: IDisposable;

            private _isDisposed: boolean = false;

            public dispose()
            {
                if (this._isDisposed) { return; }
                this._isDisposed = true;
            }
        }

        function createMockFunctionDisposable(): IDisposable & Function
        {
            const baseDisposable = new MockDisposable();
            const func: any = function () { /* not empty */ };
            func.dispose = baseDisposable.dispose.bind(baseDisposable);
            Object.defineProperties(func, { isDisposed: { get: () => baseDisposable.isDisposed } });
            return func;
        }

        describe("Is.disposable", function ()
        {
            it("should detect disposable functions", function ()
            {
                const mockDisposable = createMockFunctionDisposable();
                expect(Is.disposable(mockDisposable)).to.be.true;
            });

            it("should detect disposable objects", function ()
            {
                const mockDisposable = new MockDisposable();
                expect(Is.disposable(mockDisposable)).to.be.true;
            });

            it("should not detect non-disposable objects", function ()
            {
                const mockNonDisposable = new MockNonDisposable();
                expect(Is.disposable(mockNonDisposable)).to.be.false;
            });
        });

        describe("CheckDisposed", function ()
        {
            it("should not catch property reads on a non-disposed object", function ()
            {
                const mockDisposable = new MockDisposable();
                expect(() => mockDisposable.testProperty).not.to.throw();
            });

            it("should not catch property writes on a non-disposed object", function ()
            {
                const mockDisposable = new MockDisposable();
                expect(() => mockDisposable.testProperty = 20).not.to.throw();
            });

            it("should not catch method calls on a non-disposed object", function ()
            {
                const mockDisposable = new MockDisposable();
                expect(() => mockDisposable.testMethod()).not.to.throw();
            });

            it("should catch property reads on a disposed object", function ()
            {
                const mockDisposable = new MockDisposable();
                mockDisposable.dispose();
                expect(() => mockDisposable.testProperty).to.throw();
            });

            it("should catch property writes on a disposed object", function ()
            {
                const mockDisposable = new MockDisposable();
                mockDisposable.dispose();
                expect(() => mockDisposable.testProperty = 20).to.throw();
            });

            it("should catch method calls on a disposed object", function ()
            {
                const mockDisposable = new MockDisposable();
                mockDisposable.dispose();
                expect(() => mockDisposable.testMethod()).to.throw();
            });
        });

        describe("AutoDispose", function ()
        {
            it("should dispose and null member properties when class is disposed", function ()
            {
                const mockDisposable = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposer();
                mockAutoDisposer.thingToAutoDispose = mockDisposable;

                expect(mockAutoDisposer.thingToAutoDispose).to.exist;
                expect(mockDisposable.isDisposed).to.be.false;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.thingToAutoDispose).to.not.exist;
                expect(mockDisposable.isDisposed).to.be.true;
            });

            it("should dispose and null member array properties when class is disposed", function ()
            {
                const mockDisposableA = new MockDisposable();
                const mockDisposableB = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposer();
                mockAutoDisposer.arrayToAutoDispose = [ mockDisposableA, mockDisposableB ];

                expect(mockAutoDisposer.arrayToAutoDispose).to.exist;
                expect(mockDisposableA.isDisposed).to.be.false;
                expect(mockDisposableB.isDisposed).to.be.false;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.arrayToAutoDispose).to.not.exist;
                expect(mockDisposableA.isDisposed).to.be.true;
                expect(mockDisposableB.isDisposed).to.be.true;
            });

            it("should not dispose and null other member properties when class is disposed", function ()
            {
                const mockDisposable = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposer();
                mockAutoDisposer.thingToNotAutoDispose = mockDisposable;

                expect(mockAutoDisposer.thingToNotAutoDispose).to.exist;
                expect(mockDisposable.isDisposed).to.be.false;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.thingToNotAutoDispose).to.exist;
                expect(mockDisposable.isDisposed).to.be.false;
            });

            it("should not do anything to null member properties when class is disposed", function ()
            {
                const mockAutoDisposer = new MockAutoDisposer();

                expect(mockAutoDisposer.thingToAutoDispose).to.not.exist;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.thingToAutoDispose).to.not.exist;
            });

            it("should not do anything when used on non-disposable when class is disposed", function ()
            {
                const mockAutoDisposer = new MockAutoDisposer();
                mockAutoDisposer.thingToAutoDispose = new MockNonDisposable() as any;

                expect(() => mockAutoDisposer.dispose()).not.to.throw();
            });

            it("should not do anything when used on array containing non-disposables when class is disposed", function ()
            {
                const mockAutoDisposer = new MockAutoDisposer();
                mockAutoDisposer.arrayToAutoDispose = [new MockDisposable(), new MockNonDisposable() as any];

                expect(() => mockAutoDisposer.dispose()).not.to.throw();
            });
        });

        describe("bind", function ()
        {
            it("should cause an object to be disposed when the parent is disposed", function ()
            {
                const mockParent = new MockDisposable();
                const mockChild = new MockDisposable();

                Disposable.bind(mockChild, mockParent);
                expect(mockParent.isDisposed, "parent should not be disposed by bind call").to.equal(false);
                expect(mockChild.isDisposed, "child should not be disposed by bind call").to.equal(false);

                mockParent.dispose();
                expect(mockParent.isDisposed, "parent should be disposed by dispose call").to.equal(true);
                expect(mockChild.isDisposed, "child should be disposed by parent dispose call").to.equal(true);
            });

            it("should cause an object to be immediately disposed if bound to an already-disposed parent", function ()
            {
                const mockParent = new MockDisposable();
                const mockChild = new MockDisposable();

                mockParent.dispose();
                expect(mockParent.isDisposed, "parent should be disposed by dispose call").to.equal(true);

                Disposable.bind(mockChild, mockParent);
                expect(mockChild.isDisposed, "child should be disposed by bind call").to.equal(true);
            });

            it("should throw if called with a null parent", function ()
            {
                const mockChild = new MockDisposable();
                expect(() => Disposable.bind(mockChild, null)).to.throw();
            });

            it("should warn and return if called with a null child", function ()
            {
                const mockParent = new MockDisposable();
                const warnSpy = sinon.spy(Log, "warn");

                Disposable.bind(null, mockParent);
                expect(warnSpy.callCount, "a warning should be logged").to.equal(1);
                expect(() => mockParent.dispose(), "parent should dispose safely").not.to.throw();

                warnSpy.restore();
            });
        });

        describe("AutoDisposeOnSet", function ()
        {
            it("should dispose and null member properties when class is disposed", function ()
            {
                const mockDisposable = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.thingToAutoDispose = mockDisposable;

                expect(mockAutoDisposer.thingToAutoDispose).to.exist;
                expect(mockDisposable.isDisposed).to.be.false;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.thingToAutoDispose).to.not.exist;
                expect(mockDisposable.isDisposed).to.be.true;
            });

            it("should dispose and null member array properties when class is disposed", function ()
            {
                const mockDisposableA = new MockDisposable();
                const mockDisposableB = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.arrayToAutoDispose = [ mockDisposableA, mockDisposableB ];

                expect(mockAutoDisposer.arrayToAutoDispose).to.exist;
                expect(mockDisposableA.isDisposed).to.be.false;
                expect(mockDisposableB.isDisposed).to.be.false;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.arrayToAutoDispose).to.not.exist;
                expect(mockDisposableA.isDisposed).to.be.true;
                expect(mockDisposableB.isDisposed).to.be.true;
            });

            it("should not dispose and null other member properties when class is disposed", function ()
            {
                const mockDisposable = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.thingToNotAutoDispose = mockDisposable;

                expect(mockAutoDisposer.thingToNotAutoDispose).to.exist;
                expect(mockDisposable.isDisposed).to.be.false;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.thingToNotAutoDispose).to.exist;
                expect(mockDisposable.isDisposed).to.be.false;
            });

            it("should not do anything to null member properties when class is disposed", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();

                expect(mockAutoDisposer.thingToAutoDispose).to.not.exist;

                mockAutoDisposer.dispose();

                expect(mockAutoDisposer.thingToAutoDispose).to.not.exist;
            });

            it("should not do anything when used on non-disposable when class is disposed", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.thingToAutoDispose = new MockNonDisposable() as any;

                expect(() => mockAutoDisposer.dispose()).not.to.throw();
            });

            it("should not do anything when used on non-disposable when class is disposed", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.arrayToAutoDispose = [new MockDisposable(), new MockNonDisposable() as any];

                expect(() => mockAutoDisposer.dispose()).not.to.throw();
            });

            it("should dispose of properties when they are re-assigned", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                const mockDisposableA = new MockDisposable();

                mockAutoDisposer.thingToAutoDispose = mockDisposableA;
                mockAutoDisposer.thingToAutoDispose = null;

                expect(mockDisposableA.isDisposed, "re-assignment to null").to.be.true;

                const mockDisposableB = new MockDisposable();

                mockAutoDisposer.thingToAutoDispose = mockDisposableB;
                mockAutoDisposer.thingToAutoDispose = new MockDisposable();

                expect(mockDisposableB.isDisposed, "re-assignment to a new instance").to.be.true;
            });

            it("should not do anything when a property is re-assigned to the same value", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                const mockDisposable = new MockDisposable();

                mockAutoDisposer.thingToAutoDispose = mockDisposable;
                mockAutoDisposer.thingToAutoDispose = mockDisposable;

                expect(mockDisposable.isDisposed, "non-array value").to.be.false;

                const mockDisposableA = new MockDisposable();
                const mockDisposableB = new MockDisposable();

                mockAutoDisposer.arrayToAutoDispose = [mockDisposableA, mockDisposableB];
                mockAutoDisposer.arrayToAutoDispose = [];

                expect(mockDisposableA.isDisposed, "array value (item A)").to.be.true;
                expect(mockDisposableB.isDisposed, "array value (item B)").to.be.true;
            });

            it("should not do anything when a decorated property with a non-disposable value is re-assigned", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.thingToAutoDispose = new MockNonDisposable() as any;

                expect(() => mockAutoDisposer.thingToAutoDispose = null).not.to.throw();
            });

            it("should dispose each disposable member of an array when it is re-assigned", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();

                const arrayA = [new MockDisposable(), new MockDisposable()];
                mockAutoDisposer.arrayToAutoDispose = arrayA;
                mockAutoDisposer.arrayToAutoDispose = [];

                expect(arrayA[0].isDisposed, "re-assignment to a new instance (item A)").to.be.true;
                expect(arrayA[1].isDisposed, "re-assignment to a new instance (item B)").to.be.true;

                const arrayB = [new MockDisposable(), new MockDisposable()];
                mockAutoDisposer.arrayToAutoDispose = arrayB;
                mockAutoDisposer.arrayToAutoDispose = null;

                expect(arrayB[0].isDisposed, "re-assignment to null (item A)").to.be.true;
                expect(arrayB[1].isDisposed, "re-assignment to null (item B)").to.be.true;
            });

            it("should not dispose members of an array which are shared with a replacement array", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();

                const arrayA = [new MockDisposable(), new MockDisposable()];
                mockAutoDisposer.arrayToAutoDispose = arrayA;

                const arrayB = [new MockDisposable(), arrayA[0]];
                mockAutoDisposer.arrayToAutoDispose = arrayB;

                expect(arrayA[0].isDisposed, "non-disposal of shared member").to.be.false;
                expect(arrayA[1].isDisposed, "disposal of independent member").to.be.true;
            });

            it("should not dispose a value contained within a replacement array", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();

                mockAutoDisposer.thingOrArrayToAutoDispose = new MockDisposable();
                mockAutoDisposer.thingOrArrayToAutoDispose = [new MockDisposable(), mockAutoDisposer.thingOrArrayToAutoDispose];

                expect(mockAutoDisposer.thingOrArrayToAutoDispose[1].isDisposed).to.be.false;
            });

            it("should not dispose an array member equal to the new value", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();

                mockAutoDisposer.thingOrArrayToAutoDispose = [new MockDisposable(), new MockDisposable()];
                mockAutoDisposer.thingOrArrayToAutoDispose = mockAutoDisposer.thingOrArrayToAutoDispose[1];

                expect(mockAutoDisposer.thingOrArrayToAutoDispose.isDisposed).to.be.false;
            });

            it("should not attempt to dispose non-disposable values", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.thingToAutoDispose = { disposable: false } as any;
                expect(() => mockAutoDisposer.thingToAutoDispose = null).not.to.throw();
            });

            it("should not attempt to dispose non-disposable array members", function ()
            {
                const mockAutoDisposer = new MockAutoDisposerOnSet();
                mockAutoDisposer.arrayToAutoDispose = [ new MockDisposable(), { disposable: false } as any ];
                expect(() => mockAutoDisposer.arrayToAutoDispose = null).not.to.throw();
            });

            it("should not affect the get and set behaviour of decorated members", function ()
            {
                const mockAutoDisposerA = new MockAutoDisposerOnSet();
                mockAutoDisposerA.thingToAutoDispose = new MockDisposable();
                expect(mockAutoDisposerA.thingToAutoDispose, "property returns correct value after set").not.to.be.undefined;

                const mockAutoDisposerB = new MockAutoDisposerOnSet();
                expect(mockAutoDisposerB.thingToAutoDispose, "property returns correct value before set").to.be.undefined;

                mockAutoDisposerB.thingToAutoDispose = new MockDisposable();
                expect(mockAutoDisposerB.thingToAutoDispose, "property returns correct value for different instances").not.to.equal(mockAutoDisposerA.thingToAutoDispose);
            });

            it("should dispose of static properties when they are re-assigned", function ()
            {
                const disposable = new MockDisposable();
                MockAutoDisposerOnSet.thingToAutoDispose = disposable;

                MockAutoDisposerOnSet.thingToAutoDispose = null;
                expect(disposable.isDisposed, "non-array values").to.be.true;

                const arrayDisposable = [new MockDisposable(), new MockDisposable()];
                MockAutoDisposerOnSet.arrayToAutoDispose = arrayDisposable;

                MockAutoDisposerOnSet.arrayToAutoDispose = null;
                expect(arrayDisposable[0].isDisposed && arrayDisposable[1].isDisposed, "array values").to.be.true;
            });

            it("should not affect the independence of static and instance members with the same name", function ()
            {
                MockAutoDisposerOnSet.thingToAutoDispose = new MockDisposable();

                const mockAutoDisposer = new MockAutoDisposerOnSet();
                expect(mockAutoDisposer.thingToAutoDispose, "initial instance values should not be altered by homonymous static values").to.be.undefined;

                mockAutoDisposer.thingToAutoDispose = new MockDisposable();
                expect(mockAutoDisposer.thingToAutoDispose, "static values should not be altered by homonymous instance values").not.to.equal(MockAutoDisposerOnSet.thingToAutoDispose);
            });
        });
    });
}