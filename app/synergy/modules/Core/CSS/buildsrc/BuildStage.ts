/// <reference path="ValidateCSS.ts" />
/// <reference path="Constants.ts" />

namespace RS.CSS.BuildStages
{
    /**
     * Responsible for copying CSS to the output directory.
     */
    export class BuildCSS extends BuildStage.Base
    {
        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            // Gather CSS to build
            const cssPath = Path.combine(module.path, Constants.inCSSFolder);
            const allFiles = (await FileSystem.readFiles(cssPath))
                .filter((f) => Path.extension(f) === ".css");
            
            // Construct checksum
            const checksum = await Checksum.getComposite(allFiles);

            // Did it change?
            if (!module.checksumDB.diff("css", checksum)) { return { workDone: false, errorCount: 0 }; }
            if (allFiles.length > 0)
            {
                Log.info("Rebuilding CSS...");

                // Load all css files
                const cssSrcArr: string[] = [];
                for (const file of allFiles)
                {
                    const css = await FileSystem.readFile(file);

                    if (Constants.useValidation)
                    {
                        const validateResult = await validateCSS(css);
                        Log.pushContext(Path.baseName(file));
                        for (const error of validateResult.errors||[])
                        {
                            Log.warn(`${error.message.trim()} (line ${error.line})`);
                        }
                        Log.popContext(Path.baseName(file));
                    }

                    cssSrcArr.push(css);
                }

                // Concatenate them
                const cssSrc = cssSrcArr.join("\n");

                // Output
                await FileSystem.createPath(Path.combine(module.path, "build"));
                await FileSystem.writeFile(Path.combine(module.path, "build", Constants.outCSSFile), cssSrc);
            }

            // Write checksum
            module.checksumDB.set("css", checksum);
            await module.saveChecksums();

            return { workDone: true, errorCount: 0 };
        }
    }

    BuildStage.register({}, new BuildCSS());
}