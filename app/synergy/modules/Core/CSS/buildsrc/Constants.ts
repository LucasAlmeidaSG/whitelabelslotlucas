/**
 * Contains shared configuration constants.
 */
namespace RS.CSS.Constants
{
    /** Folder name for css, within module directory. */
    export const inCSSFolder = "css";
    
    /** File name for css, within build directory. */
    export const outCSSFile = "stylesheet.css";

    /** File name for css, within assemble build directory. */
    export const outCSSFileFinal = "app.css";

    /** Use CSS validation (via https://jigsaw.w3.org/css-validator). */
    export const useValidation = false;
}