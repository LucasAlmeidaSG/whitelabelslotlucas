/// <reference path="Constants.ts" />

namespace RS.CSS.AssembleStages
{
    /**
     * Responsible for copying CSS to the output directory.
     */
    export class CopyCSS extends AssembleStage.Base
    {
        protected _moduleCSS: Util.Map<string>;

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            this._moduleCSS = {};
            await super.execute(settings, moduleList);

            // Concatenate all CSS
            const cssSrc = Object.keys(this._moduleCSS)
                .map((k) => this._moduleCSS[k])
                .join("\n");
            
            // Write to output
            await FileSystem.createPath(Path.combine(settings.outputDir, Constants.inCSSFolder));
            await FileSystem.writeFile(Path.combine(settings.outputDir, Constants.inCSSFolder, Constants.outCSSFileFinal), cssSrc);
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // Load the CSS file
            try
            {
                const cssSrc = await FileSystem.readFile(Path.combine(module.path, "build", Constants.outCSSFile));
                this._moduleCSS[module.name] = cssSrc;
            }
            catch (err)
            {
                // This module probably doesn't have any CSS
            }
        }
    }

    AssembleStage.register({}, new CopyCSS());
}