namespace RS.CSS
{
    const cssValidator = require("css-validator");

    export function validateCSS(css: string): PromiseLike<validateCSS.Result>
    {
        return new Promise((resolve, reject) =>
        {
            // use a 1s timeout to prevent flooding the validate CSS service with too many requests in sequence
            setTimeout(() =>
            {
                cssValidator({
                    text: css,
                    profile: "css3"
                }, (err, data) =>
                {
                    resolve(data as validateCSS.Result);
                });
            }, 1000);
        });
    }

    export namespace validateCSS
    {
        export interface Result
        {
            validity: boolean;
            errors:
            {
                line: string;
                errortype: string;
                context: string;
                errorsubtype: string;
                skippedstring: string;
                message: string;
                error: string;
            }[];
            warnings:
            {
                line: string;
                level: string;
                message: string;
                warning: string;
            }[];
        }
    }
}