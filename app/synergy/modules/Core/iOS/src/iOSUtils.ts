namespace RS
{
    /**
     * Encapsulates orientation information about a specific device.
     */
    interface OrientationInfo
    {
        screenWidth: number;
        screenHeight: number;
    }

    /**
     * Encapsulates device orientation to device information
     */
    interface DeviceDetails
    {
        portrait: OrientationInfo;
        pixelDensity: number;
        oldDevice: boolean;
        minimumIOS?: number;
    }

    /**
     * Encapsulates a map of strings to device orientation.
     */
    interface DeviceMap
    {
        [key: string]: DeviceDetails;
    }

    /**
     * Methods regarding iOS devices. Displays swipeup on devices displaying browser bars.
     * Current support for devices up to iOS9.
     */
    export class IOS
    {
        /**
         * Device information for all known "old" iOS devices.
         * Devices which are classified as old devices, are those with 1GB of Memory or less.
         * These devices will struggle to run the game and as such should be detected and treated differently.
         */
        private static _deviceMap: DeviceMap =
            {
                "iPhone 4/iPhone 4S":
                {
                    "portrait":
                    {
                        screenWidth: 320,
                        screenHeight: 480
                    },
                    "pixelDensity": 2,
                    "oldDevice": true
                },
                "iPhone 5/iPhone 5S/iPhone 5C":
                {
                    "portrait":
                    {
                        screenWidth: 320,
                        screenHeight: 568
                    },
                    "pixelDensity": 2,
                    "oldDevice": true
                },
                "iPhone 6":
                {
                    "portrait":
                    {
                        screenWidth: 375,
                        screenHeight: 667
                    },
                    "pixelDensity": 2,
                    "oldDevice": true,
                    "minimumIOS": 10
                },
                "iPhone 6+":
                {
                    "portrait":
                    {
                        screenWidth: 414,
                        screenHeight: 736
                    },
                    "pixelDensity": 3,
                    "oldDevice": true,
                    "minimumIOS": 10
                },
                "iPad 1/iPad 2":
                {
                    "portrait":
                    {
                        screenWidth: 768,
                        screenHeight: 1024
                    },
                    "pixelDensity": 1,
                    "oldDevice": true
                },
                "iPad Mini 2/iPad Mini 3/iPad Air":
                {
                    "portrait":
                    {
                        screenWidth: 768,
                        screenHeight: 1024
                    },
                    "pixelDensity": 2,
                    "oldDevice": true
                },
                "iPad Mini 4":
                {
                    "portrait":
                    {
                        screenWidth: 768,
                        screenHeight: 1024
                    },
                    "pixelDensity": 2,
                    "oldDevice": true
                },
                "iPad 3/iPad 4":
                {
                    "portrait":
                    {
                        screenWidth: 768,
                        screenHeight: 1024
                    },
                    "pixelDensity": 2,
                    "oldDevice": true
                },
                "Unknown":
                {
                    "portrait":
                    {
                        screenWidth: -1,
                        screenHeight: -1
                    },
                    "pixelDensity": -1,
                    "oldDevice": false
                }
            
            };

        /**
         * Device names for all known devices.
         */
        private static _deviceNames = Object.getOwnPropertyNames(IOS._deviceMap);

        /**
         * The identified iPhone version.
         */
        private static _iPhoneVersion: string;

        /**
         * The identified iOS version.
         */
        private static _iOSVersion: string;

        private constructor() { }

        /**
         * Gets the iPhone version of the device being used, or "Unknown" if the device is not an iPhone.
         * Uses largest dimension (usually width) of the device to determine what version is running.
         * @returns The iPhone version (e.g. iPhone 4, iPhone 4s...) or "Unknown" if not an iPhone
         */
        public static get iPhoneVersion(): string
        {
            if (this.iOSVersion === "Unknown") { return "Unknown"; }

            if (this._iPhoneVersion)
            {
                return this._iPhoneVersion;
            }

            for (const deviceName of this._deviceNames)
            {
                let _portrait = 'portrait';
                let nav: any = window.navigator;
                
                const device: DeviceDetails = this._deviceMap[deviceName];
                const deviceInfo: OrientationInfo = device[_portrait];

                if (deviceInfo === null)
                {
                    return this._iPhoneVersion = "Unknown";
                }

                const deviceOS: number = parseInt(nav.appVersion.split("OS", 2)[1].split("_", 1));

                if ((window.screen.width === deviceInfo.screenWidth) && (window.screen.height === deviceInfo.screenHeight))
                {
                    let _pixelDensity = 'pixelDensity', _minimumIOS = 'minimumIOS';
                    if (window.devicePixelRatio !== device[_pixelDensity]) { continue; }
                    if (device[_minimumIOS] && deviceOS && deviceOS > device[_minimumIOS]) { continue; }
                    this._iPhoneVersion = deviceName;
                    return deviceName;
                }
            }
            return this._iPhoneVersion = "Unknown";
        }

        public static get isOldAppleDevice(): boolean
        {
            return this.iPhoneVersion !== "Unknown";
        }

        /**
         * Gets the iOS version of the device being used, or "Unknown" if the device is not on iOS.
         * @returns The iOS version (e.g. 7, 7.0.2...)
         */
        public static get iOSVersion(): string
        {
            if (!this._iOSVersion)
            {
                this._iOSVersion = Device.isAppleDevice ? Device.operatingSystem.version : "Unknown";
            }
            return this._iOSVersion;
        }

        /**
         * Query whether or not the device being used is on iOS.
         * @returns True if the device runs on iOS
         */
        public static get isDeviceIOS(): boolean
        {
            return this.iOSVersion !== "Unknown";
        }

        /**
         * Query whether or not the device being used is an iPhone.
         * @returns True if the device is an iPhone
         */
        public static get isDeviceIPhone(): boolean
        {
            return Device.deviceDetails === "iPhone";
        }
    }
}
