namespace RS.Tests
{
    const { expect } = chai;

    describe("Plane.ts", function ()
    {
        describe("Plane", function ()
        {
            function expectToEqual(plane: Math.ReadonlyPlane, ref: Math.ReadonlyPlane, message?: string)
            {
                expect(Math.Plane.equals(plane, ref)).to.equal(true, message);
            }
            describe("constructor", function ()
            {
                it("should initialise an empty plane", function ()
                {
                    expectToEqual(Math.Plane(), { x: 0, y: 0, z: 0, w: 0 });
                });

                it("should initialise a plane from a normal and a d", function ()
                {
                    expectToEqual(Math.Plane(Math.Vector3D(0, 1, 2), 3), { x: 0, y: 1, z: 2, w: 3 });
                });

                it("should initialise a plane from components", function ()
                {
                    expectToEqual(Math.Plane(0, 1, 2, 3), { x: 0, y: 1, z: 2, w: 3 });
                });
            });

            describe("fromNormalAndCoplanarPoint", function ()
            {
                it("should return a new plane from a normal and a point", function ()
                {
                    expectToEqual(Math.Plane.fromNormalAndCoplanarPoint(Math.Vector3D(0, 1, 0), Math.Vector3D(2, 2, 2)), { x: 0, y: 1, z: 0, w: -2 });
                });
            });

            describe("fromCoplanarPoints", function ()
            {
                it("should return a new plane from three coplanar points", function ()
                {
                    expectToEqual(Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1)), { x: 0, y: 0, z: 1, w: -1 });
                });
            });

            describe("distanceToPoint", function ()
            {
                it("should return the shortest distance from a point to the plane", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    const testPt = Math.Vector3D(3, 3, 3);
                    expect(Math.Plane.distanceToPoint(plane, testPt)).to.be.closeTo(2, 0.0001);
                });
            });

            describe("project", function ()
            {
                it("should project a point onto the plane", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    const testPt = Math.Vector3D(3, 3, 3);
                    expect(Math.Plane.project(plane, testPt)).to.deep.equal({ x: 3, y: 3, z: 1 });
                });
            });

            describe("createFlatProjection", function ()
            {
                it("should project a point onto the plane", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    const testPt = Math.Vector3D(3, 4, 3);
                    const mtx = Math.Plane.createFlatProjection(plane);
                    expect(Math.Matrix4.transform(mtx, testPt)).to.deep.equal({ x: 3, y: 4, z: 1 });
                });
            });

            describe("createDirectionalProjection", function ()
            {
                it("should project a point onto the plane", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    const testPt = Math.Vector4D(3, 4, 3, 1);
                    const mtx = Math.Plane.createDirectionalProjection(plane, Math.Vector3D(1, 1, -1));
                    const transformed = Math.Matrix4.transform(mtx, testPt) as Math.Vector4D;
                    transformed.x /= transformed.w;
                    transformed.y /= transformed.w;
                    transformed.z /= transformed.w;
                    expect(transformed).to.deep.equal({ x: 5, y: 6, z: 1, w: transformed.w });
                });
            });

            describe("createPointProjection", function ()
            {
                it("should project a point onto the plane", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    const testPt = Math.Vector4D(3, 4, 3, 1);
                    const mtx = Math.Plane.createPointProjection(plane, Math.Vector3D(1, 1, 4));
                    const transformed = Math.Matrix4.transform(mtx, testPt) as Math.Vector4D;
                    transformed.x /= transformed.w;
                    transformed.y /= transformed.w;
                    transformed.z /= transformed.w;
                    expect(transformed).to.deep.equal({ x: 7, y: 10, z: 1, w: transformed.w }, `${JSON.stringify(transformed)} != (7, 10, 1)`);
                });
            });

            describe("flip", function ()
            {
                it("should flip a plane in-place", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    Math.Plane.flip(plane);
                    expectToEqual(plane, { x: 0, y: 0, z: -1, w: 1 });
                });
            });

            describe("flipped", function ()
            {
                it("should flip a plane", function ()
                {
                    const plane = Math.Plane.fromCoplanarPoints(Math.Vector3D(1, 1, 1), Math.Vector3D(0, 1, 1), Math.Vector3D(1, 2, 1));
                    expectToEqual(Math.Plane.flipped(plane), { x: 0, y: 0, z: -1, w: 1 });
                });
            });
        });
    });
}