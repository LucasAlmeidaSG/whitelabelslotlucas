namespace RS.Tests
{
    const { expect } = chai;

    function expectVec3ToEqual(a: Math.ReadonlyVector3D, b: Math.ReadonlyVector3D, message?: string)
    {
        const eq = Math.Vector3D.equals(a, b, 0.00001);
        expect(eq).to.equal(true, `${message ? `${message}: ` : ``}(${a.x},${a.y},${a.z}) != (${b.x},${b.y},${b.z})`);
    }

    function expectQuatToEqual(a: Math.ReadonlyQuaternion, b: Math.ReadonlyQuaternion, message?: string)
    {
        const eq = Math.Quaternion.equals(a, b, 0.00001);
        expect(eq).to.equal(true, `${message ? `${message}: ` : ``}(${a.x},${a.y},${a.z},${a.w}) != (${b.x},${b.y},${b.z},${b.w})`);
    }

    function expectEulerToEqual(a: Math.ReadonlyEulerAngles, b: Math.ReadonlyEulerAngles, message?: string)
    {
        const eq = Math.EulerAngles.equals(a, b, 0.00001);
        expect(eq).to.equal(true, `${message ? `${message}: ` : ``}(${a.pitch},${a.yaw},${a.roll}) != (${b.pitch},${b.yaw},${b.roll})`);
    }

    describe("Quaternion.ts", function ()
    {
        describe("Quaternion", function ()
        {
            describe("transform", function ()
            {
                it("should not change a vector using identity quaternion", function ()
                {
                    const q = Math.Quaternion.identity;
                    const v = Math.Vector3D.unitZ;

                    const rot = Math.Quaternion.transform(q, v);
                    expectVec3ToEqual(rot, v);
                });

                it("should rotate a vector", function ()
                {
                    const q = Math.Quaternion.fromAxisAngle(Math.Vector3D.unitX, -Math.Angles.quarterCircle);
                    const v = Math.Vector3D.unitZ;

                    const rot = Math.Quaternion.transform(q, v);
                    expectVec3ToEqual(rot, Math.Vector3D.unitY);
                });
            });

            describe("toEuler", function ()
            {
                it("should decompose a quaternion into an euler rotation", function ()
                {
                    function testEuler(pitch: number, yaw: number, roll: number)
                    {
                        const original = Math.EulerAngles(pitch, yaw, roll);
                        const q = Math.Quaternion.fromEuler(original);
                        const decomposed = Math.Quaternion.toEuler(q);
                        expectEulerToEqual(original, decomposed)
                    }
                    testEuler(0.1, 0.0, 0.0);
                    testEuler(0.0, 0.1, 0.0);
                    testEuler(0.0, 0.0, 0.1);

                    testEuler(0.1, 0.2, 0.0);
                });
            });

            describe("findAboutAxis", function ()
            {
                it("should find the rotation about a particular axis", function ()
                {
                    const quat = Math.Quaternion.fromEuler(0.0, Math.Angles.halfCircle, 0.0);
                    const yawAmount = Math.Quaternion.findAboutAxis(quat, Math.Vector3D.unitY);
                    expect(yawAmount).to.be.approximately(Math.PI, 0.00001);
                });
            });

            describe("fromMatrix", function ()
            {
                it("should decompose a matrix into a quaternion", function ()
                {
                    const quat1 = Math.Quaternion.fromEuler(0.2, 0.3, 0.4);
                    const mtx = Math.Matrix3.createRotation(quat1);
                    const quat2 = Math.Quaternion.fromMatrix(mtx);
                    expectQuatToEqual(quat2, quat1);
                });
            });
        });
    });
}