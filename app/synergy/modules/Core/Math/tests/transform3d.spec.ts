namespace RS.Tests
{
    const { expect } = chai;

    describe("Transform3D.ts", function ()
    {
        const identity = Math.Transform3D.identity;

        describe("Is.range", function ()
        {
            it("should return true for a Transform3D", function ()
            {
                expect(Is.transform3D({x: 0, y: 0, z: 0, rx: 0, ry: 0, rz: 0, rw: 0, sx: 0, sy: 0, sz: 0})).to.equal(true);
                expect(Is.transform3D(Math.Transform3D())).to.equal(true);
                expect(Is.transform3D(identity)).to.equal(true);
            });

            it("should return false for a non Transform3D", function ()
            {
                expect(Is.transform3D(null)).to.equal(false);
                expect(Is.transform3D({ notATransform3D: true })).to.equal(false);
                expect(Is.transform3D({ x: true, y: 3 })).to.equal(false);
            });

            // It should type the arg appropriately. Otherwise fails compilation.
            const x: Math.Transform3D | number = null;
            if (Is.transform3D(x))
            {
                const t: Math.Transform3D = x;
            }
        });

        describe("Transform3D", function ()
        {
            describe("constructor", function()
            {
                it("should initialise identity values if created with null parameters", function()
                {
                    const pos = Math.Vector3D.zero;
                    const rot = Math.Quaternion.identity;
                    const scale = Math.Vector3D.one;

                    expect(Math.Transform3D(null, null, null)).to.deep.equal(identity);
                    expect(Math.Transform3D(null, null, scale)).to.deep.equal(identity);
                    expect(Math.Transform3D(null, rot, null)).to.deep.equal(identity);
                    expect(Math.Transform3D(null, rot, scale)).to.deep.equal(identity);
                    expect(Math.Transform3D(pos, null, null)).to.deep.equal(identity);
                    expect(Math.Transform3D(pos, null, scale)).to.deep.equal(identity);
                    expect(Math.Transform3D(pos, rot, null)).to.deep.equal(identity);
                    expect(Math.Transform3D(pos, rot, scale)).to.deep.equal(identity);
                });
            });

            describe("property extraction", function ()
            {
                const position = Math.Vector3D(0, 1, 2);
                const rotation = Math.Quaternion(3, 4, 5, 6);
                const scale = Math.Vector3D(7, 8, 9);
                const transform = Math.Transform3D(position, rotation, scale);

                describe("getPosition", function()
                {
                    it("should return the position component of a Transform3D", function ()
                    {
                        expect(Math.Transform3D.getPosition(identity)).to.deep.equal(Math.Vector3D.zero);
                        expect(Math.Transform3D.getPosition(transform)).to.deep.equal(position);
                    });
                });

                describe("getRotation", function()
                {
                    it("should return the rotation component of a Transform3D", function ()
                    {
                        expect(Math.Transform3D.getRotation(identity)).to.deep.equal(Math.Quaternion.identity);
                        expect(Math.Transform3D.getRotation(transform)).to.deep.equal(rotation);
                    });
                });

                describe("getScale", function()
                {
                    it("should return the scale component of a Transform3D", function ()
                    {
                        expect(Math.Transform3D.getScale(identity)).to.deep.equal(Math.Vector3D.one);
                        expect(Math.Transform3D.getScale(transform)).to.deep.equal(scale);
                    });
                });
            });

            const half = Math.Transform3D(Math.Vector3D(0.5, 0.5, 0.5), Math.Quaternion(0.5, 0.5, 0.5, 1), Math.Vector3D(1.5, 1.5, 1.5));
            const one = Math.Transform3D(Math.Vector3D(1, 1, 1), Math.Quaternion(1, 1, 1, 1), Math.Vector3D(2, 2, 2));
            const two = Math.Transform3D(Math.Vector3D(2, 2, 2), Math.Quaternion(2, 2, 2, 1), Math.Vector3D(3, 3, 3));
            const minusOne = Math.Transform3D(Math.Vector3D(-1, -1, -1), Math.Quaternion(-1, -1, -1, 1), Math.Vector3D(0, 0, 0));

            describe("linearInterpolate", function ()
            {
                it("should linearly interpolate between two transforms", function ()
                {
                    expect(Math.Transform3D.linearInterpolate(identity, one, 0)).to.deep.equal(identity);
                    expect(Math.Transform3D.linearInterpolate(identity, one, 0.5)).to.deep.equal(half);
                    expect(Math.Transform3D.linearInterpolate(identity, one, 1)).to.deep.equal(one);
                    expect(Math.Transform3D.linearInterpolate(identity, one, 2)).to.deep.equal(two);
                    expect(Math.Transform3D.linearInterpolate(identity, one, -1)).to.deep.equal(minusOne);
                });
            });

            describe("equals", function()
            {
                it("should return true for equal and approximately equal transforms", function ()
                {
                    expect(Math.Transform3D.equals(identity, identity)).to.equal(true);
                    expect(Math.Transform3D.equals(identity, half, 0.5)).to.equal(true);
                    expect(Math.Transform3D.equals(identity, one, 1)).to.equal(true);
                    expect(Math.Transform3D.equals(identity, minusOne, 1)).to.equal(true);
                });

                it("should return false for sufficiently different transforms", function()
                {
                    expect(Math.Transform3D.equals(identity, one)).to.equal(false);
                    expect(Math.Transform3D.equals(identity, half, 0.49)).to.equal(false);
                    expect(Math.Transform3D.equals(identity, one, 0.5)).to.equal(false);
                    expect(Math.Transform3D.equals(identity, minusOne, 0.5)).to.equal(false);
                });
            });
        });
    });
}