namespace RS.Tests
{
    const { expect } = chai;

    describe("AABB3D3D.ts", function ()
    {
        describe("AABB3D3D", function ()
        {
            const epsilon = 0.00001;

            describe("constructor", function ()
            {
                it("should create (0, 0, 0)(0, 0, 0) by default", function ()
                {
                    expect(Math.AABB3D()).to.deep.equal({ minX: 0, minY: 0, minZ: 0, maxX: 0, maxY: 0, maxZ: 0 });
                });

                it("should create from an explicit min and max", function ()
                {
                    expect(Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6))).to.deep.equal({ minX: 1, minY: 2, minZ: 3, maxX: 4, maxY: 5, maxZ: 6 });
                });
            });

            describe("getMinComponent", function ()
            {
                it("should retrieve a valid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.getMinComponent(aabb, 0)).to.be.equal(1);
                    expect(Math.AABB3D.getMinComponent(aabb, 1)).to.be.equal(2);
                    expect(Math.AABB3D.getMinComponent(aabb, 2)).to.be.equal(3);
                });

                it("should throw on an invalid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    let didError = false;
                    try { Math.AABB3D.getMinComponent(aabb, -1); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.AABB3D.getMinComponent(aabb, 3); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("setMinComponent", function ()
            {
                it("should set a valid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.setMinComponent(aabb, 0, 3)).to.be.equal(3);
                    expect(Math.AABB3D.setMinComponent(aabb, 1, 6)).to.be.equal(6);
                    expect(Math.AABB3D.setMinComponent(aabb, 2, 9)).to.be.equal(9);
                    expect(aabb).to.deep.equal({ minX: 3, minY: 6, minZ: 9, maxX: 4, maxY: 5, maxZ: 6 });
                });

                it("should throw on an invalid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    let didError = false;
                    try { Math.AABB3D.setMinComponent(aabb, -1, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.AABB3D.setMinComponent(aabb, 3, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("getMaxComponent", function ()
            {
                it("should retrieve a valid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.getMaxComponent(aabb, 0)).to.be.equal(4);
                    expect(Math.AABB3D.getMaxComponent(aabb, 1)).to.be.equal(5);
                    expect(Math.AABB3D.getMaxComponent(aabb, 2)).to.be.equal(6);
                });

                it("should throw on an invalid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    let didError = false;
                    try { Math.AABB3D.getMaxComponent(aabb, -1); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.AABB3D.getMaxComponent(aabb, 3); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("setMaxComponent", function ()
            {
                it("should set a valid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.setMaxComponent(aabb, 0, 3)).to.be.equal(3);
                    expect(Math.AABB3D.setMaxComponent(aabb, 1, 6)).to.be.equal(6);
                    expect(Math.AABB3D.setMaxComponent(aabb, 2, 9)).to.be.equal(9);
                    expect(aabb).to.deep.equal({ minX: 1, minY: 2, minZ: 3, maxX: 3, maxY: 6, maxZ: 9 });
                });

                it("should throw on an invalid component", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    let didError = false;
                    try { Math.AABB3D.setMaxComponent(aabb, -1, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.AABB3D.setMaxComponent(aabb, 3, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("add", function ()
            {
                it("should return the sum of two AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(3, 2, 1), Math.Vector3D(6, 5, 4));
                    expect(Math.AABB3D.add(a, b)).to.be.deep.equal({ minX: 4, minY: 4, minZ: 4, maxX: 10, maxY: 10, maxZ: 10 });
                });
            });

            describe("subtract", function ()
            {
                it("should return the difference of two AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(3, 2, 1), Math.Vector3D(6, 5, 4));
                    expect(Math.AABB3D.subtract(a, b)).to.be.deep.equal({ minX: -2, minY: 0, minZ: 2, maxX: -2, maxY: 0, maxZ: 2 });
                });
            });

            describe("multiply", function ()
            {
                it("should return the product of two AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(3, 2, 1), Math.Vector3D(6, 5, 4));
                    expect(Math.AABB3D.multiply(a, b)).to.be.deep.equal({ minX: 3, minY: 4, minZ: 3, maxX: 24, maxY: 25, maxZ: 24 });
                });

                it("should return the product of an AABB3D and a scalar", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.multiply(a, 2)).to.be.deep.equal({ minX: 2, minY: 4, minZ: 6, maxX: 8, maxY: 10, maxZ: 12 });
                });
            });

            describe("divide", function ()
            {
                it("should return the factor of two AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(3, 2, 1), Math.Vector3D(6, 5, 4));
                    expect(Math.AABB3D.divide(a, b)).to.be.deep.equal({ minX: 1/3, minY: 1, minZ: 3, maxX: 4/6, maxY: 1, maxZ: 6/4 });
                });

                it("should return the factor of an AABB3D and a scalar", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.divide(a, 2)).to.be.deep.equal({ minX: 1/2, minY: 1, minZ: 3/2, maxX: 2, maxY: 5/2, maxZ: 3 });
                });
            });

            describe("fromPoints", function ()
            {
                it("should create an AABB3D that encloses an array of vectors", function ()
                {
                    const a = Math.AABB3D.fromPoints([
                        Math.Vector3D(1, 2, 3),
                        Math.Vector3D(5, 2, 1),
                        Math.Vector3D(6, 7, 5),
                        Math.Vector3D(1, 4, 5)
                    ]);
                    expect(a).to.be.deep.equal({ minX: 1, minY: 2, minZ: 1, maxX: 6, maxY: 7, maxZ: 5 });
                });
            });

            describe("expand", function ()
            {
                const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                Math.AABB3D.expand(a, Math.Vector3D(0, 6, 4));
                expect(a).to.be.deep.equal({ minX: 0, minY: 2, minZ: 3, maxX: 4, maxY: 6, maxZ: 6 });
            });

            describe("intersects", function ()
            {
                it("should return true for two AABB3Ds that intersect", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(0, 1, 2), Math.Vector3D(3, 4, 5));
                    expect(Math.AABB3D.intersects(a, b)).to.be.true;
                });

                it("should return false for two AABB3Ds that don't intersect", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(0, 1, 2), Math.Vector3D(0.5, 4, 5));
                    const c = Math.AABB3D(Math.Vector3D(0, 1, 2), Math.Vector3D(3, 1.5, 5));
                    const d = Math.AABB3D(Math.Vector3D(0, 1, 2), Math.Vector3D(3, 4, 2.5));
                    expect(Math.AABB3D.intersects(a, b), "b").to.be.false;
                    expect(Math.AABB3D.intersects(a, c), "c").to.be.false;
                    expect(Math.AABB3D.intersects(a, d), "d").to.be.false;
                });
            });

            describe("corners", function ()
            {
                it("should find all 8 corners of an AABB3D", function ()
                {
                    const corners = Math.AABB3D.corners(Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6)));
                    expect(corners).to.deep.include({ x: 1, y: 2, z: 3 });
                    expect(corners).to.deep.include({ x: 1, y: 2, z: 6 });
                    expect(corners).to.deep.include({ x: 1, y: 5, z: 3 });
                    expect(corners).to.deep.include({ x: 1, y: 5, z: 6 });
                    expect(corners).to.deep.include({ x: 4, y: 2, z: 3 });
                    expect(corners).to.deep.include({ x: 4, y: 2, z: 6 });
                    expect(corners).to.deep.include({ x: 4, y: 5, z: 3 });
                    expect(corners).to.deep.include({ x: 4, y: 5, z: 6 });
                });
            });

            describe("normalise", function ()
            {
                it("should correct the min and max of an AABB3D", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(4, 2, 6), Math.Vector3D(1, 5, 3));
                    Math.AABB3D.normalise(a);
                    expect(a).to.be.deep.equal({ minX: 1, minY: 2, minZ: 3, maxX: 4, maxY: 5, maxZ: 6 });
                });
            });

            describe("normalised", function ()
            {
                it("should return an AABB3D with corrected min and max", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(4, 2, 6), Math.Vector3D(1, 5, 3));
                    const b = Math.AABB3D.normalised(a);
                    expect(a).to.not.equal(b);
                    expect(b).to.be.deep.equal({ minX: 1, minY: 2, minZ: 3, maxX: 4, maxY: 5, maxZ: 6 });
                });
            });

            describe("linearInterpolate", function ()
            {
                it("should linearly interpolate between two AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(7, 20, 17), Math.Vector3D(6, 7, 10));
                    expect(Math.AABB3D.linearInterpolate(a, b, 0.5)).to.be.deep.equal({ minX: 4, minY: 11, minZ: 10, maxX: 5, maxY: 6, maxZ: 8 });
                });
            });

            describe("equals", function ()
            {
                it("should return true for equal AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.equals(a, b)).to.be.true;
                });

                it("should return false for non-equal AABB3Ds", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 7));
                    const c = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 4, 6));
                    const d = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(5, 5, 6));
                    const e = Math.AABB3D(Math.Vector3D(1, 2, 2), Math.Vector3D(4, 5, 6));
                    const f = Math.AABB3D(Math.Vector3D(1, 3, 3), Math.Vector3D(4, 5, 6));
                    const g = Math.AABB3D(Math.Vector3D(2, 2, 3), Math.Vector3D(4, 5, 6));
                    expect(Math.AABB3D.equals(a, b)).to.be.false;
                    expect(Math.AABB3D.equals(a, c)).to.be.false;
                    expect(Math.AABB3D.equals(a, d)).to.be.false;
                    expect(Math.AABB3D.equals(a, e)).to.be.false;
                    expect(Math.AABB3D.equals(a, f)).to.be.false;
                    expect(Math.AABB3D.equals(a, g)).to.be.false;
                });
            });

            describe("clone", function ()
            {
                it("should return a deep clone of an AABB3D", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D.clone(a);
                    expect(a).to.not.equals(b);
                    expect(a).to.deep.equals(b);
                });
            });

            describe("copy", function ()
            {
                it("should copy the value of an AABB3D into another", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    const b = Math.AABB3D();
                    Math.AABB3D.copy(a, b);
                    expect(b).to.deep.equal(a);
                });
            });

            describe("set", function ()
            {
                it("should set the min and max of an AABB3D", function ()
                {
                    const a = Math.AABB3D(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6));
                    Math.AABB3D.set(a, Math.Vector3D(7, 8, 9), Math.Vector3D(10, 11, 12));
                    expect(a).to.be.deep.equal({ minX: 7, minY: 8, minZ: 9, maxX: 10, maxY: 11, maxZ: 12 });
                });
            });
        });
    });
}