namespace RS.Tests
{
    const { expect } = chai;

    describe("Matrix3.ts", function ()
    {
        describe("Matrix3", function ()
        {
            const epsilon = 0.00001;
            const remap = [ 0, 3, 6, 1, 4, 7, 2, 5, 8 ];

            function testEqual(mtx: Math.ReadonlyMatrix3, data: number[], message?: string)
            {
                for (let i = 0; i < 9; ++i)
                {
                    expect(mtx[i]).to.be.closeTo(data[remap[i]], epsilon, `[${i}]${message != null ? `: ${message}` : ""}`);
                }
            }

            function testFloatArrayEqual(a: Float32Array, b: Float32Array, message?: string)
            {
                expect(a.length).to.be.equal(b.length, message);
                if (a.length !== b.length) { return; }
                for (let i = 0; i < a.length; ++i)
                {
                    expect(a[i]).to.be.equal(b[i], message);
                    if (a[i] !== b[i]) { return; }
                }
            }

            function testVecEqual(a: Math.ReadonlyVector2D, b: Math.ReadonlyVector2D, message?: string)
            {
                expect(a.x).to.be.approximately(b.x, epsilon, `x${message != null ? `: ${message}` : ""}`);
                expect(a.y).to.be.approximately(b.y, epsilon, `x${message != null ? `: ${message}` : ""}`);
            }

            describe("constructor", function ()
            {
                it("should create a matrix from a Float32Array", function ()
                {
                    const arr = new Float32Array([ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]);
                    const mtx = Math.Matrix3(arr);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });

                it("should create a matrix from a Float32Array and offset", function ()
                {
                    const arr = new Float32Array([ -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8 ]);
                    const mtx = Math.Matrix3(arr, 2);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });

                it("should create a matrix from a 1D array", function ()
                {
                    const arr = [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ];
                    const mtx = Math.Matrix3(arr);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });

                it("should create a matrix from a 1D array and offset", function ()
                {
                    const arr = [ -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8];
                    const mtx = Math.Matrix3(arr, 2);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });

                it("should create a matrix from a 2D array", function ()
                {
                    const arr = [
                        [ 0, 1, 2 ],
                        [ 3, 4, 5 ],
                        [ 6, 7, 8 ],
                    ];
                    const mtx = Math.Matrix3(arr);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });

                it("should create an empty matrix", function ()
                {
                    const mtx = Math.Matrix3();
                    testEqual(mtx, [
                        0, 0, 0,
                        0, 0, 0,
                        0, 0, 0
                    ]);
                });
            });

            describe("identity", function ()
            {
                it("should be the identity matrix", function ()
                {
                    testEqual(Math.Matrix3.identity, [
                        1, 0, 0,
                        0, 1, 0,
                        0, 0, 1
                    ]);
                });
            });

            describe("setFromArray", function ()
            {
                it("should populate a matrix from a 1D array", function ()
                {
                    const arr = [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ];
                    const mtx = Math.Matrix3();
                    Math.Matrix3.setFromArray(mtx, arr);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });

                it("should populate a matrix from a 1D array and offset", function ()
                {
                    const arr = [ -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8 ];
                    const mtx = Math.Matrix3();
                    Math.Matrix3.setFromArray(mtx, arr, 2);
                    testEqual(mtx, [
                        0, 3, 6,
                        1, 4, 7,
                        2, 5, 8
                    ]);
                });
            });

            describe("mul", function ()
            {
                it("should pre-multiply two matrices", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Matrix3([ 7, 5, 8, 1, 8, 2, 9, 4, 3 ]);
                    const c = Math.Matrix3.mul(a, b);
                    testEqual(c, [
                        59, 11, 54,
                        108, 66, 66,
                        60, 24, 65
                    ]);
                });

                it("should pre-multiply two matrices into out", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Matrix3([ 7, 5, 8, 1, 8, 2, 9, 4, 3 ]);
                    const out = Math.Matrix3();
                    const c = Math.Matrix3.mul(a, b, out);
                    expect(out).to.equal(c);
                    testEqual(c, [
                        59, 11, 54,
                        108, 66, 66,
                        60, 24, 65
                    ]);
                });
            });

            // describe("combine", function ()
            // {
                
            // });

            describe("transform", function ()
            {
                it("should pre-multiply a matrix with a vector", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Vector2D(7, 2);
                    const c = Math.Matrix3.transform(a, b);
                    expect(c).to.deep.equal({ x: 38, y: 34 });
                });

                it("should pre-multiply a matrix with a vector into out", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Vector2D(7, 2);
                    const out = Math.Vector2D();
                    const c = Math.Matrix3.transform(a, b, out);
                    expect(out).to.equal(c);
                    expect(c).to.deep.equal({ x: 38, y: 34 });
                });
            });

            describe("transformNormal", function ()
            {
                it("should pre-multiply a matrix with a vector", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Vector2D(7, 2);
                    const c = Math.Matrix3.transformNormal(a, b);
                    expect(c).to.deep.equal({ x: 35, y: 26 });
                });

                it("should pre-multiply a matrix with a vector into out", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Vector2D(7, 2);
                    const out = Math.Vector2D();
                    const c = Math.Matrix3.transformNormal(a, b, out);
                    expect(out).to.equal(c);
                    expect(c).to.deep.equal({ x: 35, y: 26 });
                });
            });

            describe("write", function ()
            {
                it("should write the values to a 1D array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr: number[] = [];
                    Math.Matrix3.write(mtx, arr);
                    expect(arr).to.deep.equal([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                });

                it("should write the values to a 1D array with an offset", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr: number[] = [ 0, 0 ];
                    Math.Matrix3.write(mtx, arr, 2);
                    expect(arr).to.deep.equal([ 0, 0, 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                });

                it("should write the values to a Float32Array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = new Float32Array(9);
                    Math.Matrix3.write(mtx, arr);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]));
                });

                it("should write the values to a Float32Array with an offset", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = new Float32Array(11);
                    Math.Matrix3.write(mtx, arr, 2);
                    testFloatArrayEqual(arr, new Float32Array([ 0, 0, 5, 2, 6, 0, 6, 2, 3, 8, 1 ]));
                });

                it("should write the transposed values to a 1D array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr: number[] = [];
                    Math.Matrix3.write(mtx, arr, undefined, true);
                    expect(arr).to.deep.equal([ 5, 0, 3, 2, 6, 8, 6, 2, 1 ]);
                });

                it("should write the transposed values to a 1D array with an offset", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr: number[] = [ 0, 0 ];
                    Math.Matrix3.write(mtx, arr, 2, true);
                    expect(arr).to.deep.equal([ 0, 0, 5, 0, 3, 2, 6, 8, 6, 2, 1 ]);
                });

                it("should write the transposed values to a Float32Array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = new Float32Array(9);
                    Math.Matrix3.write(mtx, arr, undefined, true);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 0, 3, 2, 6, 8, 6, 2, 1 ]));
                });

                it("should write the transposed values to a Float32Array with an offset", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = new Float32Array(11);
                    Math.Matrix3.write(mtx, arr, 2, true);
                    testFloatArrayEqual(arr, new Float32Array([ 0, 0, 5, 0, 3, 2, 6, 8, 6, 2, 1 ]));
                });
            });

            describe("toArray", function ()
            {
                it("should return a 1D array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = Math.Matrix3.toArray(mtx);
                    expect(arr).to.deep.equal([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                });

                it("should return a transposed 1D array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = Math.Matrix3.toArray(mtx, true);
                    expect(arr).to.deep.equal([ 5, 0, 3, 2, 6, 8, 6, 2, 1 ]);
                });
            });

            describe("toTypedArray", function ()
            {
                it("should return a Float32Array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = Math.Matrix3.toTypedArray(mtx);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]));
                });

                it("should return a transposed Float32Array", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const arr = Math.Matrix3.toTypedArray(mtx, true);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 0, 3, 2, 6, 8, 6, 2, 1 ]));
                });
            });

            describe("clone", function ()
            {
                it("should return a copy of a matrix", function ()
                {
                    const mtx = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const copy = Math.Matrix3.clone(mtx);
                    expect(mtx).to.not.equal(copy);
                    expect(mtx).to.deep.equal(copy);
                });
            });

            describe("copy", function ()
            {
                it("should set the values of a matrix", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Matrix3();
                    Math.Matrix3.copy(a, b);
                    expect(a).to.deep.equal(b);
                });
            });

            describe("transpose", function ()
            {
                it("should transpose the values of a matrix", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Matrix3.transpose(a);
                    testEqual(b, [
                        5, 2, 6,
                        0, 6, 2,
                        3, 8, 1
                    ]);
                });

                it("should transpose the values of a matrix into out", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const out = Math.Matrix3();
                    const b = Math.Matrix3.transpose(a, out);
                    expect(b).to.equal(out);
                    testEqual(b, [
                        5, 2, 6,
                        0, 6, 2,
                        3, 8, 1
                    ]);
                });
            });

            describe("determinant", function ()
            {
                it("should find the determinant of a matrix", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const det = Math.Matrix3.determinant(a);
                    expect(det).to.equal(-146);
                });
            });

            describe("inverse", function ()
            {
                it("should find the inverse of a matrix", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const b = Math.Matrix3.inverse(a);
                    testEqual(b, [
                        5/73, -3/73, 9/73,
                        -23/73, 13/146, 17/73,
                        16/73, 5/73, -15/73
                    ]);
                });

                it("should find the inverse of a matrix into out", function ()
                {
                    const a = Math.Matrix3([ 5, 2, 6, 0, 6, 2, 3, 8, 1 ]);
                    const out = Math.Matrix3();
                    const b = Math.Matrix3.inverse(a, out);
                    expect(b).to.equal(out);
                    testEqual(b, [
                        5/73, -3/73, 9/73,
                        -23/73, 13/146, 17/73,
                        16/73, 5/73, -15/73
                    ]);
                });
            });

            describe("createTranslation", function ()
            {
                it("should create a matrix via vec2 which translates vectors", function ()
                {
                    const a = Math.Matrix3.createTranslation(Math.Vector2D(1, 2));
                    const b = Math.Vector2D(10, 15);
                    const c = Math.Matrix3.transform(a, b);
                    console.log(c);
                    testVecEqual(c, Math.Vector3D(11, 17));
                });

                it("should create a matrix via vec2 into out", function ()
                {
                    const out = Math.Matrix3();
                    const a = Math.Matrix3.createTranslation(Math.Vector2D(1, 2), out);
                    expect(out).to.equal(a);
                });

                it("should create a matrix via x/y/z which translates vectors", function ()
                {
                    const a = Math.Matrix3.createTranslation(1, 2);
                    const b = Math.Vector2D(10, 15);
                    const c = Math.Matrix3.transform(a, b);
                    testVecEqual(c, Math.Vector3D(11, 17));
                });

                it("should create a matrix via x/y/z into out", function ()
                {
                    const out = Math.Matrix3();
                    const a = Math.Matrix3.createTranslation(1, 2, out);
                    expect(out).to.equal(a);
                });
            });

            describe("createRotation", function ()
            {
                it("should create a matrix which rotates vectors", function ()
                {
                    const a = Math.Matrix3.createRotation(RS.Math.Angles.quarterCircle);
                    const b = Math.Vector2D(0, 15);
                    const c = Math.Matrix3.transform(a, b);
                    testVecEqual(c, Math.Vector2D(-15, 0));
                });

                it("should create a matrix into out", function ()
                {
                    const out = Math.Matrix3();
                    const a = Math.Matrix3.createRotation(RS.Math.Angles.quarterCircle, out);
                    expect(out).to.equal(a);
                });
            });

            describe("createScale", function ()
            {
                it("should create a matrix which scales vectors", function ()
                {
                    const a = Math.Matrix3.createScale(Math.Vector2D(1, 2));
                    const b = Math.Vector2D(10, 15);
                    const c = Math.Matrix3.transform(a, b);
                    testVecEqual(c, Math.Vector2D(10, 30));
                });

                it("should create a matrix into out", function ()
                {
                    const out = Math.Matrix3();
                    const a = Math.Matrix3.createScale(Math.Vector2D(1, 2), out);
                    expect(out).to.equal(a);
                });
            });
        });
    });
}