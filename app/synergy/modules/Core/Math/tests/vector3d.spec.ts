namespace RS.Tests
{
    const { expect } = chai;

    describe("Vector3D.ts", function ()
    {
        describe("Vector3D", function ()
        {
            const epsilon = 0.00001;

            describe("constructor", function ()
            {
                it("should create (0, 0, 0) by default", function ()
                {
                    expect(Math.Vector3D()).to.deep.equal({ x: 0, y: 0, z: 0 });
                });

                it("should create from an explicit x, y and z", function ()
                {
                    expect(Math.Vector3D(1, 2, 3)).to.deep.equal({ x: 1, y: 2, z: 3 });
                });

                it("should create from a vec2 and explicit z", function ()
                {
                    expect(Math.Vector3D(Math.Vector2D(1, 2), 3)).to.deep.equal({ x: 1, y: 2, z: 3 });
                    expect(Math.Vector3D(Math.Vector2D(1, 2))).to.deep.equal({ x: 1, y: 2, z: 0 });
                });
            });

            describe("getComponent", function ()
            {
                it("should retrieve a valid component", function ()
                {
                    const vec = Math.Vector3D(2, 4, 6);
                    expect(Math.Vector3D.getComponent(vec, 0)).to.be.equal(2);
                    expect(Math.Vector3D.getComponent(vec, 1)).to.be.equal(4);
                    expect(Math.Vector3D.getComponent(vec, 2)).to.be.equal(6);
                });

                it("should throw on an invalid component", function ()
                {
                    const vec = Math.Vector3D(2, 4, 6);
                    let didError = false;
                    try { Math.Vector3D.getComponent(vec, -1); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.Vector3D.getComponent(vec, 3); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("setComponent", function ()
            {
                it("should set a valid component", function ()
                {
                    const vec = Math.Vector3D(2, 4, 6);
                    expect(Math.Vector3D.setComponent(vec, 0, 3)).to.be.equal(3);
                    expect(Math.Vector3D.setComponent(vec, 1, 6)).to.be.equal(6);
                    expect(Math.Vector3D.setComponent(vec, 2, 9)).to.be.equal(9);
                    expect(vec).to.deep.equal({ x: 3, y: 6, z: 9 });
                });

                it("should throw on an invalid component", function ()
                {
                    const vec = Math.Vector3D(2, 4, 6);
                    let didError = false;
                    try { Math.Vector3D.setComponent(vec, -1, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.Vector3D.setComponent(vec, 3, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("add", function ()
            {
                it("should return the sum of two vectors", function ()
                {
                    expect(Math.Vector3D.add(Math.Vector3D(0, 1, 2), Math.Vector3D(1, 0, 0))).to.be.deep.equal(Math.Vector3D(1, 1, 2));
                    expect(Math.Vector3D.add(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6))).to.be.deep.equal(Math.Vector3D(5, 7, 9));
                });
            });

            describe("subtract", function ()
            {
                it("should return the difference of two vectors", function ()
                {
                    expect(Math.Vector3D.subtract(Math.Vector3D(0, 1, 0), Math.Vector3D(1, 0, 0))).to.be.deep.equal(Math.Vector3D(-1, 1, 0));
                    expect(Math.Vector3D.subtract(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6))).to.be.deep.equal(Math.Vector3D(-3, -3, -3));
                });
            });

            describe("multiply", function ()
            {
                it("should return the product of two vectors", function ()
                {
                    expect(Math.Vector3D.multiply(Math.Vector3D(0, 1, 0), Math.Vector3D(1, 0, 0))).to.be.deep.equal(Math.Vector3D(0, 0, 0));
                    expect(Math.Vector3D.multiply(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6))).to.be.deep.equal(Math.Vector3D(4, 10, 18));
                });

                it("should return the product of a vector and a scalar", function ()
                {
                    expect(Math.Vector3D.multiply(Math.Vector3D(2, 4, 6), 0.5)).to.be.deep.equal(Math.Vector3D(1, 2, 3));
                });
            });

            describe("divide", function ()
            {
                it("should return the factor of two vectors", function ()
                {
                    expect(Math.Vector3D.divide(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6))).to.be.deep.equal(Math.Vector3D(1/4, 2/5, 3/6));
                });

                it("should return the factor of a vector and a scalar", function ()
                {
                    expect(Math.Vector3D.divide(Math.Vector3D(2, 4, 6), 0.5)).to.be.deep.equal(Math.Vector3D(4, 8, 12));
                });
            });

            describe("addMultiply", function ()
            {
                it("should return a + b * c where c is a vector", function ()
                {
                    expect(Math.Vector3D.addMultiply(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6), Math.Vector3D(7, 8, 9))).to.be.deep.equal(Math.Vector3D(1 + 4 * 7, 2 + 5 * 8, 3 + 6 * 9));
                });

                it("should return a + b * c where c is a scalar", function ()
                {
                    expect(Math.Vector3D.addMultiply(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6), 7)).to.be.deep.equal(Math.Vector3D(1 + 4 * 7, 2 + 5 * 7, 3 + 6 * 7));
                });
            });

            describe("dot", function ()
            {
                it("should return the dot product of two vectors", function ()
                {
                    expect(Math.Vector3D.dot(Math.Vector3D(0, 1, 0), Math.Vector3D(1, 0, 0))).to.be.equal(0);
                    expect(Math.Vector3D.dot(Math.Vector3D(0, 1, 0), Math.Vector3D(0, 1, 0))).to.be.equal(1);
                    expect(Math.Vector3D.dot(Math.Vector3D(0, 1, 0), Math.Vector3D(0, -1, 0))).to.be.equal(-1);
                    expect(Math.Vector3D.dot(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 5, 6))).to.be.equal(32);
                });
            });

            describe("angleBetween", function ()
            {
                it("should return the angle between two vectors", function ()
                {
                    expect(Math.Vector3D.angleBetween(Math.Vector3D(0, 1, 0), Math.Vector3D(0, 1, 0))).to.be.approximately(0, epsilon);
                    expect(Math.Vector3D.angleBetween(Math.Vector3D(0, 1, 0), Math.Vector3D(1, 0, 0))).to.be.approximately(Math.Angles.quarterCircle, epsilon);
                    expect(Math.Vector3D.angleBetween(Math.Vector3D(0, 1, 0), Math.Vector3D(-1, 0, 0))).to.be.approximately(Math.Angles.quarterCircle, epsilon);
                    expect(Math.Vector3D.angleBetween(Math.Vector3D(0, 1, 0), Math.Vector3D(0, 0, 1))).to.be.approximately(Math.Angles.quarterCircle, epsilon);
                    expect(Math.Vector3D.angleBetween(Math.Vector3D(0, 1, 0), Math.Vector3D(0, 0, -1))).to.be.approximately(Math.Angles.quarterCircle, epsilon);
                    expect(Math.Vector3D.angleBetween(Math.Vector3D(0, 1, 0), Math.Vector3D(0, -1, 0))).to.be.approximately(Math.Angles.halfCircle, epsilon);
                });
            });

            describe("findBetween", function ()
            {
                it("should return a normalised direction between two points", function ()
                {
                    expect(Math.Vector3D.findBetween(Math.Vector3D(0, 0, 0), Math.Vector3D(0, 0, 10))).to.be.deep.equal(Math.Vector3D(0, 0, 1));
                });
            });

            describe("distanceBetween", function ()
            {
                it("should return the distance between two points", function ()
                {
                    expect(Math.Vector3D.distanceBetween(Math.Vector3D(0, 0, -2), Math.Vector3D(0, 0, 10))).to.be.equal(12);
                });
            });

            describe("sizeSq", function ()
            {
                it("should return the magnitude squared of a vector", function ()
                {
                    expect(Math.Vector3D.sizeSq(Math.Vector3D(1, 2, 3))).to.be.equal(14);
                });
            });

            describe("size", function ()
            {
                it("should return the magnitude of a vector", function ()
                {
                    expect(Math.Vector3D.size(Math.Vector3D(1, 2, 3))).to.be.equal(Math.sqrt(14));
                });
            });

            describe("normalise", function ()
            {
                it("should set the magnitude of a vector to 1", function ()
                {
                    const vec = Math.Vector3D(1, 2, 3);
                    Math.Vector3D.normalise(vec);
                    expect(Math.Vector3D.size(vec)).to.be.approximately(1, epsilon);
                });
            });

            describe("normalised", function ()
            {
                it("should return a vector with a magnitude of 1", function ()
                {
                    const vec = Math.Vector3D(1, 2, 3);
                    const vec2 = Math.Vector3D.normalised(vec);
                    expect(Math.Vector3D.size(vec2)).to.be.approximately(1, epsilon);
                    expect(Math.Vector3D.size(vec)).to.not.be.equal(1);
                });
            });

            describe("linearInterpolate", function ()
            {
                it("should linearly interpolate between two vectors", function ()
                {
                    expect(Math.Vector3D.linearInterpolate(Math.Vector3D(1, 2, 3), Math.Vector3D(3, 0, 5), 0.5)).to.be.deep.equal(Math.Vector3D(2, 1, 4));
                });
            });

            describe("equals", function ()
            {
                it("should return true for equal vectors", function ()
                {
                    expect(Math.Vector3D.equals(Math.Vector3D(1, 2, 3), Math.Vector3D(1, 2, 3))).to.be.true;
                });

                it("should return false for non-equal vectors", function ()
                {
                    expect(Math.Vector3D.equals(Math.Vector3D(1, 2, 3), Math.Vector3D(1, 3, 3))).to.be.false;
                    expect(Math.Vector3D.equals(Math.Vector3D(1, 2, 3), Math.Vector3D(2, 2, 3))).to.be.false;
                    expect(Math.Vector3D.equals(Math.Vector3D(1, 2, 3), Math.Vector3D(1, 2, 4))).to.be.false;
                    expect(Math.Vector3D.equals(Math.Vector3D(1, 2, 3), Math.Vector3D(2, 3, 4))).to.be.false;
                });
            });

            describe("clone", function ()
            {
                it("should return a deep clone of a vector", function ()
                {
                    const vec = Math.Vector3D(1, 2, 3);
                    const cl = Math.Vector3D.clone(vec);
                    expect(vec).to.not.equals(cl);
                    expect(vec).to.deep.equals(cl);
                });
            });

            describe("copy", function ()
            {
                it("should copy the value of a vector into another", function ()
                {
                    const vec = Math.Vector3D(1, 2, 3);
                    Math.Vector3D.copy(Math.Vector3D(4, 5, 6), vec);
                    expect(vec).to.deep.equal(Math.Vector3D(4, 5, 6));
                });
            });

            describe("set", function ()
            {
                it("should set the components of a vector", function ()
                {
                    const vec = Math.Vector3D(1, 2, 3);
                    Math.Vector3D.set(vec, 4, 5, 6);
                    expect(vec).to.deep.equal(Math.Vector3D(4, 5, 6));
                });
            });
        });
    });
}