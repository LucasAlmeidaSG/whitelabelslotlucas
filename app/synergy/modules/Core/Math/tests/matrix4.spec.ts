namespace RS.Tests
{
    const { expect } = chai;

    describe("Matrix4.ts", function ()
    {
        describe("Matrix4", function ()
        {
            const epsilon = 0.00001;
            const remap = [ 0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15 ];

            function testEqual(mtx: Math.ReadonlyMatrix4, data: number[], message?: string)
            {
                for (let i = 0; i < 16; ++i)
                {
                    expect(mtx[i]).to.be.closeTo(data[remap[i]], epsilon, `[${i}]${message != null ? `: ${message}` : ""}`);
                }
            }

            function testFloatArrayEqual(a: Float32Array, b: Float32Array, message?: string)
            {
                expect(a.length).to.be.equal(b.length, message);
                if (a.length !== b.length) { return; }
                for (let i = 0; i < a.length; ++i)
                {
                    expect(a[i]).to.be.equal(b[i], message);
                    if (a[i] !== b[i]) { return; }
                }
            }

            function testVecEqual(a: Math.ReadonlyVector3D, b: Math.ReadonlyVector3D, message?: string)
            {
                expect(a.x).to.be.approximately(b.x, epsilon, `x${message != null ? `: ${message}` : ""}`);
                expect(a.y).to.be.approximately(b.y, epsilon, `y${message != null ? `: ${message}` : ""}`);
                expect(a.z).to.be.approximately(b.z, epsilon, `z${message != null ? `: ${message}` : ""}`);
            }

            function testQuatEqual(a: Math.ReadonlyQuaternion, b: Math.ReadonlyQuaternion, message?: string)
            {
                expect(a.x).to.be.approximately(b.x, epsilon, `x${message != null ? `: ${message}` : ""}`);
                expect(a.y).to.be.approximately(b.y, epsilon, `y${message != null ? `: ${message}` : ""}`);
                expect(a.z).to.be.approximately(b.z, epsilon, `z${message != null ? `: ${message}` : ""}`);
                expect(a.w).to.be.approximately(b.w, epsilon, `w${message != null ? `: ${message}` : ""}`);
            }

            describe("constructor", function ()
            {
                it("should create a matrix from a Float32Array", function ()
                {
                    const arr = new Float32Array([ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ]);
                    const mtx = Math.Matrix4(arr);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });

                it("should create a matrix from a Float32Array and offset", function ()
                {
                    const arr = new Float32Array([ -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ]);
                    const mtx = Math.Matrix4(arr, 2);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });

                it("should create a matrix from a 1D array", function ()
                {
                    const arr = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ];
                    const mtx = Math.Matrix4(arr);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });

                it("should create a matrix from a 1D array and offset", function ()
                {
                    const arr = [ -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ];
                    const mtx = Math.Matrix4(arr, 2);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });

                it("should create a matrix from a 2D array", function ()
                {
                    const arr = [
                        [ 0, 1, 2, 3 ],
                        [ 4, 5, 6, 7 ],
                        [ 8, 9, 10, 11 ],
                        [ 12, 13, 14, 15 ]
                    ];
                    const mtx = Math.Matrix4(arr);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });

                it("should create an empty matrix", function ()
                {
                    const mtx = Math.Matrix4();
                    testEqual(mtx, [
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0
                    ]);
                });
            });

            describe("identity", function ()
            {
                it("should be the identity matrix", function ()
                {
                    testEqual(Math.Matrix4.identity, [
                        1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1
                    ]);
                });
            });

            describe("setFromArray", function ()
            {
                it("should populate a matrix from a 1D array", function ()
                {
                    const arr = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ];
                    const mtx = Math.Matrix4();
                    Math.Matrix4.setFromArray(mtx, arr);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });

                it("should populate a matrix from a 1D array and offset", function ()
                {
                    const arr = [ -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ];
                    const mtx = Math.Matrix4();
                    Math.Matrix4.setFromArray(mtx, arr, 2);
                    testEqual(mtx, [
                        0, 4, 8, 12,
                        1, 5, 9, 13,
                        2, 6, 10, 14,
                        3, 7, 11, 15
                    ]);
                });
            });

            describe("mul", function ()
            {
                it("should pre-multiply two matrices", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Matrix4([ 7, 5, 8, 0, 1, 8, 2, 6, 9, 4, 3, 8, 5, 3, 7, 9 ]);
                    const c = Math.Matrix4.mul(a, b);
                    testEqual(c, [
                        59, 17, 62, 55,
                        108, 114, 130, 156,
                        60, 54, 105, 88,
                        39, 45, 69, 87
                    ]);
                });

                it("should pre-multiply two matrices into out", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Matrix4([ 7, 5, 8, 0, 1, 8, 2, 6, 9, 4, 3, 8, 5, 3, 7, 9 ]);
                    const out = Math.Matrix4();
                    const c = Math.Matrix4.mul(a, b, out);
                    expect(out).to.equal(c);
                    testEqual(c, [
                        59, 17, 62, 55,
                        108, 114, 130, 156,
                        60, 54, 105, 88,
                        39, 45, 69, 87
                    ]);
                });
            });

            // describe("combine", function ()
            // {
                
            // });

            describe("transform", function ()
            {
                it("should pre-multiply a matrix with a vector", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Vector3D(7, 2, 6);
                    const c = Math.Matrix4.transform(a, b);
                    expect(c).to.deep.equal({ x: 54, y: 82, z: 57 });
                });

                it("should pre-multiply a matrix with a vector into out", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Vector3D(7, 2, 6);
                    const out = Math.Vector3D();
                    const c = Math.Matrix4.transform(a, b, out);
                    expect(out).to.equal(c);
                    expect(c).to.deep.equal({ x: 54, y: 82, z: 57 });
                });
            });

            describe("transformNormal", function ()
            {
                it("should pre-multiply a matrix with a vector", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Vector3D(7, 2, 6);
                    const c = Math.Matrix4.transformNormal(a, b);
                    expect(c).to.deep.equal({ x: 53, y: 74, z: 52 });
                });

                it("should pre-multiply a matrix with a vector into out", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Vector3D(7, 2, 6);
                    const out = Math.Vector3D();
                    const c = Math.Matrix4.transformNormal(a, b, out);
                    expect(out).to.equal(c);
                    expect(c).to.deep.equal({ x: 53, y: 74, z: 52 });
                });
            });

            describe("write", function ()
            {
                it("should write the values to a 1D array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr: number[] = [];
                    Math.Matrix4.write(mtx, arr);
                    expect(arr).to.deep.equal([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                });

                it("should write the values to a 1D array with an offset", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr: number[] = [ 0, 0 ];
                    Math.Matrix4.write(mtx, arr, 2);
                    expect(arr).to.deep.equal([ 0, 0, 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                });

                it("should write the values to a Float32Array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = new Float32Array(16);
                    Math.Matrix4.write(mtx, arr);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]));
                });

                it("should write the values to a Float32Array with an offset", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = new Float32Array(18);
                    Math.Matrix4.write(mtx, arr, 2);
                    testFloatArrayEqual(arr, new Float32Array([ 0, 0, 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]));
                });

                it("should write the transposed values to a 1D array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr: number[] = [];
                    Math.Matrix4.write(mtx, arr, undefined, true);
                    expect(arr).to.deep.equal([ 5, 0, 3, 1, 2, 6, 8, 8, 6, 2, 1, 5, 1, 0, 4, 6 ]);
                });

                it("should write the transposed values to a 1D array with an offset", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr: number[] = [ 0, 0 ];
                    Math.Matrix4.write(mtx, arr, 2, true);
                    expect(arr).to.deep.equal([ 0, 0, 5, 0, 3, 1, 2, 6, 8, 8, 6, 2, 1, 5, 1, 0, 4, 6 ]);
                });

                it("should write the transposed values to a Float32Array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = new Float32Array(16);
                    Math.Matrix4.write(mtx, arr, undefined, true);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 0, 3, 1, 2, 6, 8, 8, 6, 2, 1, 5, 1, 0, 4, 6 ]));
                });

                it("should write the transposed values to a Float32Array with an offset", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = new Float32Array(18);
                    Math.Matrix4.write(mtx, arr, 2, true);
                    testFloatArrayEqual(arr, new Float32Array([ 0, 0, 5, 0, 3, 1, 2, 6, 8, 8, 6, 2, 1, 5, 1, 0, 4, 6 ]));
                });
            });

            describe("toArray", function ()
            {
                it("should return a 1D array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = Math.Matrix4.toArray(mtx);
                    expect(arr).to.deep.equal([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                });

                it("should return a transposed 1D array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = Math.Matrix4.toArray(mtx, true);
                    expect(arr).to.deep.equal([ 5, 0, 3, 1, 2, 6, 8, 8, 6, 2, 1, 5, 1, 0, 4, 6 ]);
                });
            });

            describe("toTypedArray", function ()
            {
                it("should return a Float32Array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = Math.Matrix4.toTypedArray(mtx);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]));
                });

                it("should return a transposed Float32Array", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const arr = Math.Matrix4.toTypedArray(mtx, true);
                    testFloatArrayEqual(arr, new Float32Array([ 5, 0, 3, 1, 2, 6, 8, 8, 6, 2, 1, 5, 1, 0, 4, 6 ]));
                });
            });

            describe("clone", function ()
            {
                it("should return a copy of a matrix", function ()
                {
                    const mtx = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const copy = Math.Matrix4.clone(mtx);
                    expect(mtx).to.not.equal(copy);
                    expect(mtx).to.deep.equal(copy);
                });
            });

            describe("copy", function ()
            {
                it("should set the values of a matrix", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Matrix4();
                    Math.Matrix4.copy(a, b);
                    expect(a).to.deep.equal(b);
                });
            });

            describe("transpose", function ()
            {
                it("should transpose the values of a matrix", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Matrix4.transpose(a);
                    testEqual(b, [
                        5, 2, 6, 1,
                        0, 6, 2, 0,
                        3, 8, 1, 4,
                        1, 8, 5, 6
                    ]);
                });

                it("should transpose the values of a matrix into out", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const out = Math.Matrix4();
                    const b = Math.Matrix4.transpose(a, out);
                    expect(b).to.equal(out);
                    testEqual(b, [
                        5, 2, 6, 1,
                        0, 6, 2, 0,
                        3, 8, 1, 4,
                        1, 8, 5, 6
                    ]);
                });
            });

            describe("determinant", function ()
            {
                it("should find the determinant of a matrix", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const det = Math.Matrix4.determinant(a);
                    expect(det).to.equal(-976);
                });
            });

            describe("inverse", function ()
            {
                it("should find the inverse of a matrix", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const b = Math.Matrix4.inverse(a);
                    testEqual(b, [
                        29/244, -7/244, 21/244, -13/244,
                        -23/244, 35/244, 17/244, -57/244,
                        89/488, 29/488, -87/488, 19/488,
                        -69/488, -17/488, 51/488, 73/488
                    ]);
                });

                it("should find the inverse of a matrix into out", function ()
                {
                    const a = Math.Matrix4([ 5, 2, 6, 1, 0, 6, 2, 0, 3, 8, 1, 4, 1, 8, 5, 6 ]);
                    const out = Math.Matrix4();
                    const b = Math.Matrix4.inverse(a, out);
                    expect(b).to.equal(out);
                    testEqual(b, [
                        29/244, -7/244, 21/244, -13/244,
                        -23/244, 35/244, 17/244, -57/244,
                        89/488, 29/488, -87/488, 19/488,
                        -69/488, -17/488, 51/488, 73/488
                    ]);
                });
            });

            describe("createTranslation", function ()
            {
                it("should create a matrix via vec3 which translates vectors", function ()
                {
                    const a = Math.Matrix4.createTranslation(Math.Vector3D(1, 2, 3));
                    const b = Math.Vector3D(10, 15, 20);
                    const c = Math.Matrix4.transform(a, b);
                    console.log(c);
                    testVecEqual(c, Math.Vector3D(11, 17, 23));
                });

                it("should create a matrix via vec3 into out", function ()
                {
                    const out = Math.Matrix4();
                    const a = Math.Matrix4.createTranslation(Math.Vector3D(1, 2, 3), out);
                    expect(out).to.equal(a);
                });

                it("should create a matrix via x/y/z which translates vectors", function ()
                {
                    const a = Math.Matrix4.createTranslation(1, 2, 3);
                    const b = Math.Vector3D(10, 15, 20);
                    const c = Math.Matrix4.transform(a, b);
                    testVecEqual(c, Math.Vector3D(11, 17, 23));
                });

                it("should create a matrix via x/y/z into out", function ()
                {
                    const out = Math.Matrix4();
                    const a = Math.Matrix4.createTranslation(1, 2, 3, out);
                    expect(out).to.equal(a);
                });
            });

            describe("createRotation", function ()
            {
                it("should create a matrix via pitch/yaw/roll which rotates vectors", function ()
                {
                    const a = Math.Matrix4.createRotation(0, RS.Math.Angles.quarterCircle, 0);
                    const b = Math.Vector3D(0, 15, 20);
                    const c = Math.Matrix4.transform(a, b);
                    testVecEqual(c, Math.Vector3D(20, 15, 0));
                });

                it("should create a matrix via pitch/yaw/roll into out", function ()
                {
                    const out = Math.Matrix4();
                    const a = Math.Matrix4.createRotation(0, RS.Math.Angles.quarterCircle, 0, out);
                    expect(out).to.equal(a);
                });

                it("should create a matrix via euler vec3 which rotates vectors", function ()
                {
                    const a = Math.Matrix4.createRotation(Math.Vector3D(0, RS.Math.Angles.quarterCircle, 0));
                    const b = Math.Vector3D(0, 15, 20);
                    const c = Math.Matrix4.transform(a, b);
                    testVecEqual(c, Math.Vector3D(20, 15, 0));
                });

                it("should create a matrix via euler vec3 into out", function ()
                {
                    const out = Math.Matrix4();
                    const a = Math.Matrix4.createRotation(Math.Vector3D(0, RS.Math.Angles.quarterCircle, 0), out);
                    expect(out).to.equal(a);
                });

                it("should create a matrix via quaternion which rotates vectors", function ()
                {
                    const a = Math.Matrix4.createRotation(Math.Quaternion.fromAxisAngle(RS.Math.Vector3D.unitY, RS.Math.Angles.quarterCircle));
                    const b = Math.Vector3D(0, 15, 20);
                    const c = Math.Matrix4.transform(a, b);
                    testVecEqual(c, Math.Vector3D(20, 15, 0));
                });

                it("should create a matrix via quaternion into out", function ()
                {
                    const out = Math.Matrix4();
                    const a = Math.Matrix4.createRotation(Math.Quaternion.fromAxisAngle(RS.Math.Vector3D.unitY, RS.Math.Angles.quarterCircle), out);
                    expect(out).to.equal(a);
                });
            });

            describe("createScale", function ()
            {
                it("should create a matrix which scales vectors", function ()
                {
                    const a = Math.Matrix4.createScale(Math.Vector3D(1, 2, 3));
                    const b = Math.Vector3D(10, 15, 20);
                    const c = Math.Matrix4.transform(a, b);
                    testVecEqual(c, Math.Vector3D(10, 30, 60));
                });

                it("should create a matrix into out", function ()
                {
                    const out = Math.Matrix4();
                    const a = Math.Matrix4.createScale(Math.Vector3D(1, 2, 3), out);
                    expect(out).to.equal(a);
                });
            });

            // describe("createPerspective", function ()
            // {

            // });

            // describe("createOrthographic", function ()
            // {

            // });

            // describe("createLookat", function ()
            // {

            // });

            describe("decompose", function ()
            {
                const testMatrix = Math.Matrix4.combine([
                    Math.Matrix4.createScale(0.8, 0.7, 0.6),
                    Math.Matrix4.createRotation(Math.Quaternion.fromEuler(0.1, 0.2, 0.3)),
                    Math.Matrix4.createTranslation(2.0, 3.0, 4.0)
                ]);

                it("should recover the position from a transformation matrix", function ()
                {
                    const pos = Math.Vector3D(), rot = Math.Quaternion(), scale = Math.Vector3D();
                    Math.Matrix4.decompose(testMatrix, pos, rot, scale);
                    testVecEqual(pos, { x: 2.0, y: 3.0, z: 4.0 });
                });

                it("should recover the rotation from a transformation matrix", function ()
                {
                    const pos = Math.Vector3D(), rot = Math.Quaternion(), scale = Math.Vector3D();
                    Math.Matrix4.decompose(testMatrix, pos, rot, scale);
                    testQuatEqual(rot, Math.Quaternion.fromEuler(0.1, 0.2, 0.3));
                });

                it("should recover the scale from a transformation matrix", function ()
                {
                    const pos = Math.Vector3D(), rot = Math.Quaternion(), scale = Math.Vector3D();
                    Math.Matrix4.decompose(testMatrix, pos, rot, scale);
                    testVecEqual(scale, { x: 0.8, y: 0.7, z: 0.6 });
                });

                // it("should handle negative scales", function ()
                // {
                //     const scaledMatrix = Math.Matrix4.mul(Math.Matrix4.createScale(-1.0, 1.0, 1.0), testMatrix);
                //     const pos = Math.Vector3D(), rot = Math.Quaternion(), scale = Math.Vector3D();
                //     Math.Matrix4.decompose(scaledMatrix, pos, rot, scale);
                //     testVecEqual(pos, { x: -2.0, y: 3.0, z: 4.0 }, "pos");
                //     testVecEqual(scale, { x: -0.8, y: 0.7, z: 0.6 }, "scale");
                //     testQuatEqual(rot, Math.Quaternion.fromEuler(0.1, -0.2, 0.3), "rot");
                // });
            });
        });
    });
}