namespace RS.Tests
{
    const { expect } = chai;

    describe("Size2D.ts", function ()
    {
        describe("Size2D", function ()
        {
            describe("skew", function ()
            {
                const testSize: Math.Size2D = { w: 2, h: 5 };
                const epsilon = 0.01;

                it("should not change the size when angles are both 0", function ()
                {
                    expect(Math.Size2D.skew(testSize, { x: 0, y: 0 })).to.deep.equal(testSize);
                });

                it("should return the same result for equivalent angles", function ()
                {
                    const angles = [0.2, 1.8, 5.4, 3.4];
                    const circ = 2 * Math.PI;
                    const deltas = [-6 * circ, -circ, circ, 6 * circ];
                    for (const angle of angles)
                    {
                        const base = Math.Size2D.skew(testSize, { x: angle, y: angle });

                        for (const delta of deltas)
                        {
                            const newAngle = angle + delta;
                            const skewed = Math.Size2D.skew(testSize, { x: newAngle, y: newAngle });

                            expect(skewed.w, `w is incorrect (angle: ${angle}, delta: ${delta})`).to.be.approximately(base.w, epsilon);
                            expect(skewed.h, `h is incorrect (angle: ${angle}, delta: ${delta})`).to.be.approximately(base.h, epsilon);
                        }
                    }
                });

                const viewSize = { w: 2048, h: 1024 };

                it("should return the correct result when skewed on only the x-axis", function ()
                {
                    function test(value: number, width: number, height: number)
                    {
                        const skewedSize = Math.Size2D.skew(viewSize, { x: value, y: 0 });
                        expect(skewedSize.w).to.be.approximately(width, epsilon);
                        expect(skewedSize.h).to.be.approximately(height, epsilon);
                    }

                    test(0.5, 2538.931900024414, 898.64453125);
                    test(-0.5, 2538.931900024414, 898.64453125);

                    test(2.3, 2811.602066040039, 682.2666625976562);
                    test(-2.3, 2811.602066040039, 682.2666625976562);

                    test(5.7, 2611.9020080566406, 854.7459106445312);
                    test(-5.7, 2611.9020080566406, 854.7459106445312);
                });

                it("should return the correct result when skewed on only the y-axis", function ()
                {
                    function test(value: number, width: number, height: number)
                    {
                        const skewedSize = Math.Size2D.skew(viewSize, { x: 0, y: value });
                        expect(skewedSize.w).to.be.approximately(width, epsilon);
                        expect(skewedSize.h).to.be.approximately(height, epsilon);
                    }

                    test(0.5, 1797.289077758789, 2005.8634643554688);
                    test(-0.5, 1797.289077758789, 2005.8634643554688);

                    test(2.3, 1364.5332946777344, 2551.204216003418);
                    test(-2.3, 1364.5332946777344, 2551.204216003418);

                    test(5.7, 1709.4917526245117, 2151.8040161132812);
                    test(-5.7, 1709.4917526245117, 2151.8040161132812);
                });

                it("should return the correct result when skewed on both axes", function ()
                {
                    function test(x: number, y: number, width: number, height: number)
                    {
                        const skewedSize = Math.Size2D.skew(viewSize, { x, y });
                        expect(skewedSize.w).to.be.approximately(width, epsilon);
                        expect(skewedSize.h).to.be.approximately(height, epsilon);
                    }

                    test(0.5, 0.5, 2288.220962524414, 1880.5081176757812);
                    test(0.5, 2.3, 1855.4650573730469, 2425.848747253418);
                    test(0.5, 5.7, 2200.4236373901367, 2026.4485473632812);
                    test(2.3, 2.3, 2128.1354370117188, 2209.4710083007812);
                    test(5.7, 5.7, 2273.3937377929688, 1982.5499267578125);
                    test(5.7, 2.3, 1928.4352722167969, 2381.950065612793);
                    test(5.7, 0.5, 2361.1910705566406, 1836.6094360351562);
                });
            });

            describe("rotate", function ()
            {
                const testSize: Math.Size2D = { w: 2, h: 5 };
                const epsilon = 0.00001;

                function test(angle: number, expectedW: number, expectedH: number)
                {
                    const output = Math.Size2D.rotate(testSize, angle);

                    expect(output.w, "w is incorrect").to.be.approximately(expectedW, epsilon);
                    expect(output.h, "h is incorrect").to.be.approximately(expectedH, epsilon);
                }

                it("should not change the size when angle == 0", function ()
                {
                    test(0, 2, 5);
                });

                it("should transpose the size when angle == 1/4 circle", function ()
                {
                    test(Math.Angles.quarterCircle, 5, 2);
                });

                it("should always return a positive size", function ()
                {
                    test(-Math.Angles.quarterCircle, 5, 2);
                });

                it("should return the correct result when angle == 1/8 circle", function ()
                {
                    test(Math.Angles.eighthCircle, 4.949747468305833, 4.949747468305833);
                });

                it("should return the correct result when angle == 2/3 circle", function ()
                {
                    test(Math.Angles.twoThirdsCircle, 5.330127018922194, 4.232050807568879);
                })
            });
        });
    });
}