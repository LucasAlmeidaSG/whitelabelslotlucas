namespace RS.Tests
{
    const { expect } = chai;

    describe("Vector4D.ts", function ()
    {
        describe("Vector4D", function ()
        {
            const epsilon = 0.00001;

            describe("constructor", function ()
            {
                it("should create (0, 0, 0, 0) by default", function ()
                {
                    expect(Math.Vector4D()).to.deep.equal({ x: 0, y: 0, z: 0, w: 0 });
                });

                it("should create from an explicit x, y, z and w", function ()
                {
                    expect(Math.Vector4D(1, 2, 3, 4)).to.deep.equal({ x: 1, y: 2, z: 3, w: 4 });
                });

                it("should create from a vec3 and explicit w", function ()
                {
                    expect(Math.Vector4D(Math.Vector3D(1, 2, 3), 4)).to.deep.equal({ x: 1, y: 2, z: 3, w: 4 });
                    expect(Math.Vector4D(Math.Vector3D(1, 2, 3))).to.deep.equal({ x: 1, y: 2, z: 3, w: 0 });
                });
            });

            describe("add", function ()
            {
                it("should return the sum of two vectors", function ()
                {
                    expect(Math.Vector4D.add(Math.Vector4D(0, 1, 2, 3), Math.Vector4D(1, 0, 0, 0))).to.be.deep.equal(Math.Vector4D(1, 1, 2, 3));
                    expect(Math.Vector4D.add(Math.Vector4D(1, 2, 3, 4), Math.Vector4D(5, 6, 7, 8))).to.be.deep.equal(Math.Vector4D(6, 8, 10, 12));
                });
            });

            describe("subtract", function ()
            {
                it("should return the difference of two vectors", function ()
                {
                    expect(Math.Vector4D.subtract(Math.Vector4D(0, 1, 0, 1), Math.Vector4D(1, 0, 0, 1))).to.be.deep.equal(Math.Vector4D(-1, 1, 0, 0));
                    expect(Math.Vector4D.subtract(Math.Vector4D(1, 2, 3, 4), Math.Vector4D(5, 6, 7, 8))).to.be.deep.equal(Math.Vector4D(-4, -4, -4, -4));
                });
            });

            describe("multiply", function ()
            {
                it("should return the product of two vectors", function ()
                {
                    expect(Math.Vector4D.multiply(Math.Vector4D(0, 1, 0, 1), Math.Vector4D(1, 0, 0, 1))).to.be.deep.equal(Math.Vector4D(0, 0, 0, 1));
                    expect(Math.Vector4D.multiply(Math.Vector4D(1, 2, 3, 4), Math.Vector4D(5, 6, 7, 8))).to.be.deep.equal(Math.Vector4D(5, 12, 21, 32));
                });

                it("should return the product of a vector and a scalar", function ()
                {
                    expect(Math.Vector4D.multiply(Math.Vector4D(2, 4, 6, 8), 0.5)).to.be.deep.equal(Math.Vector4D(1, 2, 3, 4));
                });
            });

            describe("divide", function ()
            {
                it("should return the factor of two vectors", function ()
                {
                    expect(Math.Vector4D.divide(Math.Vector4D(1, 2, 3, 4), Math.Vector4D(5, 6, 7, 8))).to.be.deep.equal(Math.Vector4D(1/5, 2/6, 3/7, 4/8));
                });

                it("should return the factor of a vector and a scalar", function ()
                {
                    expect(Math.Vector4D.divide(Math.Vector4D(2, 4, 6, 8), 0.5)).to.be.deep.equal(Math.Vector4D(4, 8, 12, 16));
                });
            });

            describe("sizeSq", function ()
            {
                it("should return the magnitude squared of a vector", function ()
                {
                    expect(Math.Vector4D.sizeSq(Math.Vector4D(1, 2, 3, 4))).to.be.equal(30);
                });
            });

            describe("size", function ()
            {
                it("should return the magnitude of a vector", function ()
                {
                    expect(Math.Vector4D.size(Math.Vector4D(1, 2, 3, 4))).to.be.equal(Math.sqrt(30));
                });
            });

            describe("normalise", function ()
            {
                it("should set the magnitude of a vector to 1", function ()
                {
                    const vec = Math.Vector4D(1, 2, 3, 4);
                    Math.Vector4D.normalise(vec);
                    expect(Math.Vector4D.size(vec)).to.be.approximately(1, epsilon);
                });
            });

            describe("normalised", function ()
            {
                it("should return a vector with a magnitude of 1", function ()
                {
                    const vec = Math.Vector4D(1, 2, 3, 4);
                    const vec2 = Math.Vector4D.normalised(vec);
                    expect(Math.Vector4D.size(vec2)).to.be.approximately(1, epsilon);
                    expect(Math.Vector4D.size(vec)).to.not.be.equal(1);
                });
            });

            describe("linearInterpolate", function ()
            {
                it("should linearly interpolate between two vectors", function ()
                {
                    expect(Math.Vector4D.linearInterpolate(Math.Vector4D(1, 2, 3, 4), Math.Vector4D(3, 0, 5, 6), 0.5)).to.be.deep.equal(Math.Vector4D(2, 1, 4, 5));
                });
            });

            describe("equals", function ()
            {
                it("should return true for equal vectors", function ()
                {
                    expect(Math.Vector4D.equals(Math.Vector4D(1, 2, 3), Math.Vector4D(1, 2, 3))).to.be.true;
                });

                it("should return false for non-equal vectors", function ()
                {
                    expect(Math.Vector4D.equals(Math.Vector4D(1, 2, 3), Math.Vector4D(1, 3, 3))).to.be.false;
                    expect(Math.Vector4D.equals(Math.Vector4D(1, 2, 3), Math.Vector4D(2, 2, 3))).to.be.false;
                    expect(Math.Vector4D.equals(Math.Vector4D(1, 2, 3), Math.Vector4D(1, 2, 4))).to.be.false;
                    expect(Math.Vector4D.equals(Math.Vector4D(1, 2, 3), Math.Vector4D(2, 3, 4))).to.be.false;
                });
            });

            describe("clone", function ()
            {
                it("should return a deep clone of a vector", function ()
                {
                    const vec = Math.Vector4D(1, 2, 3);
                    const cl = Math.Vector4D.clone(vec);
                    expect(vec).to.not.equals(cl);
                    expect(vec).to.deep.equals(cl);
                });
            });

            describe("copy", function ()
            {
                it("should copy the value of a vector into another", function ()
                {
                    const vec = Math.Vector4D(1, 2, 3);
                    Math.Vector4D.copy(Math.Vector4D(4, 5, 6), vec);
                    expect(vec).to.deep.equal(Math.Vector4D(4, 5, 6));
                });
            });

            describe("set", function ()
            {
                it("should set the components of a vector", function ()
                {
                    const vec = Math.Vector4D(1, 2, 3);
                    Math.Vector4D.set(vec, 4, 5, 6);
                    expect(vec).to.deep.equal(Math.Vector4D(4, 5, 6));
                });
            });
        });
    });
}