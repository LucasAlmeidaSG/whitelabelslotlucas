namespace RS.Tests
{
    const { expect } = chai;

    describe("Range.ts", function ()
    {
        describe("Is.range", function ()
        {
            it("should return true for a range", function ()
            {
                expect(Is.range(Math.Range(20, 30))).to.equal(true);
                expect(Is.range(Math.Range())).to.equal(true);
                expect(Is.range(Math.Range(-3))).to.equal(true);
            });

            it("should return false for a non range", function ()
            {
                expect(Is.range(null)).to.equal(false);
                expect(Is.range({ notARange: true })).to.equal(false);
                expect(Is.range({ min: true, max: 3 })).to.equal(false);
            });

            // It should type the arg appropriately. Otherwise fails compilation.
            const x: Math.Range | number = null;
            if (Is.range(x))
            {
                const range: Math.Range = x;
            }
        });

        describe("Range", function ()
        {
            describe("constructor", function ()
            {
                it("should create an empty range by default", function ()
                {
                    expect(Math.Range()).to.deep.equal({ kind: "emptyRange" })
                });

                it("should create from an explicit min and max", function ()
                {
                    const range = Math.Range(1, 2);
                    expect(range.min).to.equal(1);
                    expect(range.max).to.equal(2);
                });

                it("should create from a magnitude", function ()
                {
                    const range = Math.range(25);
                    expect(range.min).to.equal(-25);
                    expect(range.max).to.equal(25);
                });
            });

            describe("pickNumber", function ()
            {
                it("should return a random number within the range", function ()
                {
                    const n = 100;
                    const range = Math.Range(23, 101);
                    for (let i = 0; i < n; i++)
                    {
                        const rand = Math.Range.pickNumber(range);
                        expect(rand, "number is within range").to.be.within(23, 101);
                    }
                });
            });

            describe("pickInteger", function ()
            {
                it("should return a random integer within the range", function ()
                {
                    const n = 100;
                    const range = Math.Range(23, 101);
                    for (let i = 0; i < n; i++)
                    {
                        const value = Math.Range.pickInteger(range);
                        expect(value, "number is within range").to.be.within(23, 101);
                        
                        const decimal = value % 1;
                        expect(decimal, "number is integer").to.equal(0);
                    }
                });
            });

            describe("pickIntegers", function ()
            {
                it("should return an array of random integers within the range", function ()
                {
                    const n = 100;
                    const range = Math.Range(23, 101);
                    const integers = Math.Range.pickIntegers(n, range);
                    for (const value of integers)
                    {
                        expect(value, "number is within range").to.be.within(23, 101);
                        
                        const decimal = value % 1;
                        expect(decimal, "number is integer").to.equal(0);
                    }
                });
            });

            describe("pickIntegersToSum", function ()
            {
                it("should return an array of random integers which add up to the given sum", function ()
                {
                    const sum = 253;
                    const range = Math.Range(23, 101);
                    const integers = Math.Range.pickIntegersToSum(sum, range);

                    let total = 0;
                    for (const value of integers)
                    {
                        expect(value, "number is within range").to.be.within(23, 101);
                        
                        const decimal = value % 1;
                        expect(decimal, "number is integer").to.equal(0);

                        total += value;
                    }

                    expect(total, "numbers add up to sum").to.equal(sum);
                });
            });

            describe("increment", function ()
            {
                it("should increase the given value up to the upper bound of the range", function ()
                {
                    const range = Math.Range(23, 101);
                    expect(Math.Range.increment(93, range), "number increases by 1").to.equal(94);
                    expect(Math.Range.increment(101, range), "number is clamped to upper bound").to.equal(101);
                });
            });

            describe("decrement", function ()
            {
                it("should increase the given value up to the upper bound of the range", function ()
                {
                    const range = Math.Range(23, 101);
                    expect(Math.Range.decrement(93, range), "number increases by 1").to.equal(92);
                    expect(Math.Range.decrement(23, range), "number is clamped to lower bound").to.equal(23);
                });
            });

            describe("clamp", function ()
            {
                it("should restrict the given value to within the range", function ()
                {
                    const range = Math.Range(23, 101);
                    expect(Math.Range.clamp(93, range), "number within range is unchanged").to.equal(93);
                    expect(Math.Range.clamp(1293, range), "number above range is clamped down").to.equal(101);
                    expect(Math.Range.clamp(-12, range), "number below range is clamped up").to.equal(23);
                });
            });

            describe("isWithin", function ()
            {
                it("should return whether or not the given value is within the range", function ()
                {
                    const range = Math.Range(23, 101);
                    expect(Math.Range.isWithin(93, range), "integer within range returns true").to.equal(true);
                    expect(Math.Range.isWithin(100.713249, range), "float within range returns true").to.equal(true);
                    expect(Math.Range.isWithin(101.313249, range), "float outside of range returns false").to.equal(false);
                    expect(Math.Range.isWithin(3931, range), "integer outside of range returns false").to.equal(false);
                });
            });

            describe("isAtEndOf", function ()
            {
                it("should return whether or not the given value is at the upper bound of the range", function ()
                {
                    const range = Math.Range(23, 101);
                    expect(Math.Range.isAtEndOf(93, range), "integer below upper bound returns false").to.equal(false);
                    expect(Math.Range.isAtEndOf(100.713249, range), "float below upper bound returns false").to.equal(false);
                    expect(Math.Range.isAtEndOf(101, range), "upper bound returns true").to.equal(true);
                });
            });

            describe("isAtStartOf", function ()
            {
                it("should return whether or not the given value is at the lower bound of the range", function ()
                {
                    const range = Math.Range(23, 101);
                    expect(Math.Range.isAtStartOf(93, range), "integer above lower bound returns false").to.equal(false);
                    expect(Math.Range.isAtStartOf(23.2913, range), "float above lower bound returns false").to.equal(false);
                    expect(Math.Range.isAtStartOf(23, range), "lower bound returns true").to.equal(true);
                });
            });
        });
    });
}