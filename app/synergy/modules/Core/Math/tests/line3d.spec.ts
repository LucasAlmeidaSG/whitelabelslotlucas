namespace RS.Tests
{
    const { expect } = chai;

    describe("Line3D.ts", function ()
    {
        describe("Line3D", function ()
        {
            const epsilon = 0.00001;

            function expectToEqual(line: Math.ReadonlyLine3D, ref: Math.ReadonlyLine3D, message?: string)
            {
                expect(Math.Line3D.equals(line, ref)).to.equal(true, message);
            }
            describe("constructor", function ()
            {
                it("should initialise an empty line", function ()
                {
                    expectToEqual(Math.Line3D(), { x: 0, y: 0, z: 0, dx: 0, dy: 0, dz: 0 });
                });

                it("should initialise a plane from a point and a direction", function ()
                {
                    expectToEqual(Math.Line3D(Math.Vector3D(0, 1, 2), Math.Vector3D(3, 4, 5)), { x: 0, y: 1, z: 2, dx: 3, dy: 4, dz: 5 });
                });

                it("should initialise a plane from components", function ()
                {
                    expectToEqual(Math.Line3D(0, 1, 2, 3, 4, 5), { x: 0, y: 1, z: 2, dx: 3, dy: 4, dz: 5 });
                });
            });

            describe("getDirComponent", function ()
            {
                it("should retrieve a valid component", function ()
                {
                    const line = Math.Line3D(1, 2, 3, 4, 5, 6);
                    expect(Math.Line3D.getDirComponent(line, 0)).to.be.equal(4);
                    expect(Math.Line3D.getDirComponent(line, 1)).to.be.equal(5);
                    expect(Math.Line3D.getDirComponent(line, 2)).to.be.equal(6);
                });

                it("should throw on an invalid component", function ()
                {
                    const line = Math.Line3D(1, 2, 3, 4, 5, 6);
                    let didError = false;
                    try { Math.Line3D.getDirComponent(line, -1); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.Line3D.getDirComponent(line, 3); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("setDirComponent", function ()
            {
                it("should set a valid component", function ()
                {
                    const line = Math.Line3D(1, 2, 3, 4, 5, 6);
                    expect(Math.Line3D.setDirComponent(line, 0, 3)).to.be.equal(3);
                    expect(Math.Line3D.setDirComponent(line, 1, 6)).to.be.equal(6);
                    expect(Math.Line3D.setDirComponent(line, 2, 9)).to.be.equal(9);
                    expect(line).to.deep.equal({ x: 1, y: 2, z: 3, dx: 3, dy: 6, dz: 9 });
                });

                it("should throw on an invalid component", function ()
                {
                    const line = Math.Line3D(1, 2, 3, 4, 5, 6);
                    let didError = false;
                    try { Math.Line3D.setDirComponent(line, -1, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                    didError = false;
                    try { Math.Line3D.setDirComponent(line, 3, 0); } catch { didError = true; }
                    expect(didError).to.be.true;
                });
            });

            describe("direction", function ()
            {
                it("should return the direction of a line", function ()
                {
                    const line = Math.Line3D(1, 2, 3, 4, 5, 6);
                    expect(Math.Line3D.direction(line)).to.deep.equal({ x: 4, y: 5, z: 6 });
                });
            });

            describe("intersects", function ()
            {
                it("should return true for a non-parallel line and plane", function ()
                {
                    const plane = Math.Plane(Math.Vector3D(0, 1, 0), 5);
                    const line = Math.Line3D(Math.Vector3D(1, 1, 3), Math.Vector3D(1, 1, 1));
                    expect(Math.Line3D.intersects(line, plane)).to.be.true;
                });

                it("should find the intersection point of a non-parallel line and plane", function ()
                {
                    const plane = Math.Plane(Math.Vector3D(0, 1, 0), 5);
                    const line = Math.Line3D(Math.Vector3D(3, 2, 1), Math.Vector3D(1, 1, 3));
                    const pt = Math.Vector3D();
                    Math.Line3D.intersects(line, plane, pt);
                    expect(pt).to.deep.equal({ x: 6, y: 5, z: 10 });
                });

                it("should return false for a parallel line and plane", function ()
                {
                    const plane = Math.Plane(Math.Vector3D(0, 1, 0), 5);
                    const line = Math.Line3D(Math.Vector3D(0, 5, 1), Math.Vector3D(1, 0, 1));
                    expect(Math.Line3D.intersects(line, plane)).to.be.false;
                });

                it("should return true and find the intersection point for a line that intersects an AABB", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(-1, -2, -3), Math.Vector3D(4, 5, 6));
                    let tmpOut = Math.Vector3D();
                    {
                        // Line starts outside AABB but aims at AABB
                        const line = Math.Line3D(Math.Vector3D(8, 8, 8), Math.Vector3D(-1, -1, -1));
                        expect(Math.Line3D.intersects(line, aabb, tmpOut)).to.be.true;
                        expect(tmpOut).to.deep.equal({ x: 4, y: 4, z: 4 });
                    }
                    {
                        // Line starts inside AABB
                        const line = Math.Line3D(Math.Vector3D(1, 2, -1), Math.Vector3D(2, 3, 1));
                        expect(Math.Line3D.intersects(line, aabb, tmpOut)).to.be.true;
                        expect(tmpOut).to.deep.equal({ x: 1, y: 2, z: -1 });
                    }
                });

                it("should return false for a line that does not intersect an AABB", function ()
                {
                    const aabb = Math.AABB3D(Math.Vector3D(-1, -2, -3), Math.Vector3D(4, 5, 6));
                    const line = Math.Line3D(Math.Vector3D(8, 8, 8), Math.Vector3D(1, -1, -1));
                    expect(Math.Line3D.intersects(line, aabb)).to.be.false;
                });
            });

            describe("fromPoints", function ()
            {
                it("should return a new line from two points", function ()
                {
                    expect(Math.Line3D.fromPoints(Math.Vector3D(1, 2, 3), Math.Vector3D(4, 8, 12))).to.deep.equal({ x: 1, y: 2, z: 3, dx: 3, dy: 6, dz: 9 });
                });
            });

            describe("distanceToPoint", function ()
            {
                it("should return the shortest distance from a point to the line", function ()
                {
                    const line = Math.Line3D(Math.Vector3D(3, 2, 1), Math.Vector3D(1, 1, 3));
                    const point = Math.Vector3D(5, 5, 5);
                    expect(Math.Line3D.distanceToPoint(line, point)).to.be.approximately(1.651445647689541, epsilon);
                });
            });

            describe("project", function ()
            {
                it("should project a point onto the line", function ()
                {
                    const line = Math.Line3D(Math.Vector3D(3, 2, 1), Math.Vector3D(1, 1, 3));
                    const point = Math.Vector3D(5, 5, 5);
                    const projPt = Math.Line3D.project(line, point);
                    expect(projPt.x).to.be.approximately(4.545454545454545, epsilon, "x");
                    expect(projPt.y).to.be.approximately(3.545454545454545, epsilon, "y");
                    expect(projPt.z).to.be.approximately(5.636363636363637, epsilon, "z");
                });
            });
        });
    });
}