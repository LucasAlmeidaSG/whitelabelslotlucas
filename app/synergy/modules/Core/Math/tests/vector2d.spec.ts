namespace RS.Tests
{
    const { expect } = chai;
    import Vec2 = Math.Vec2;

    describe("Vector2D.ts", function ()
    {
        describe("Vector2D", function ()
        {
            const epsilon = 0.00001;

            describe("constructor", function ()
            {
                it("should create (0, 0) by default", function ()
                {
                    expect(Math.Vector2D()).to.deep.equal({ x: 0, y: 0 });
                });

                it("should create from an explicit x and y", function ()
                {
                    expect(Math.Vector2D(1, 2)).to.deep.equal({ x: 1, y: 2 });
                });

                it("should create from an angle", function ()
                {
                    const vec = Math.Vector2D(Math.Angles.quarterCircle);
                    expect(vec.x).to.be.approximately(0, epsilon);
                    expect(vec.y).to.be.approximately(1, epsilon);
                });

                it("should create from a Size2D", function ()
                {
                    const sz: Math.Size2D = { w: 10, h: 20 };
                    expect(Math.Vector2D(sz)).to.deep.equal({ x: 10, y: 20 });
                });
            });

            describe("add", function ()
            {
                it("should return the sum of two vectors", function ()
                {
                    expect(Math.Vector2D.add(Math.Vector2D(0, 1), Math.Vector2D(1, 0))).to.be.deep.equal(Math.Vector2D(1, 1));
                    expect(Math.Vector2D.add(Math.Vector2D(1, 2), Math.Vector2D(3, 4))).to.be.deep.equal(Math.Vector2D(4, 6));
                });
            });

            describe("subtract", function ()
            {
                it("should return the difference of two vectors", function ()
                {
                    expect(Math.Vector2D.subtract(Math.Vector2D(0, 1), Math.Vector2D(1, 0))).to.be.deep.equal(Math.Vector2D(-1, 1));
                    expect(Math.Vector2D.subtract(Math.Vector2D(1, 2), Math.Vector2D(3, 4))).to.be.deep.equal(Math.Vector2D(-2, -2));
                });
            });

            describe("multiply", function ()
            {
                it("should return the product of two vectors", function ()
                {
                    expect(Math.Vector2D.multiply(Math.Vector2D(0, 1), Math.Vector2D(1, 0))).to.be.deep.equal(Math.Vector2D(0, 0));
                    expect(Math.Vector2D.multiply(Math.Vector2D(1, 2), Math.Vector2D(3, 4))).to.be.deep.equal(Math.Vector2D(3, 8));
                });

                it("should return the product of a vector and a scalar", function ()
                {
                    expect(Math.Vector2D.multiply(Math.Vector2D(2, 4), 0.5)).to.be.deep.equal(Math.Vector2D(1, 2));
                });
            });

            describe("divide", function ()
            {
                it("should return the factor of two vectors", function ()
                {
                    expect(Math.Vector2D.divide(Math.Vector2D(1, 2), Math.Vector2D(3, 4))).to.be.deep.equal(Math.Vector2D(1 / 3, 2 / 4));
                });

                it("should return the factor of a vector and a scalar", function ()
                {
                    expect(Math.Vector2D.divide(Math.Vector2D(2, 4), 0.5)).to.be.deep.equal(Math.Vector2D(4, 8));
                });
            });

            describe("rotate", function ()
            {
                const testVector: Math.Vector2D = { x: 0, y: 1 };

                function test(angle: number, expectedX: number, expectedY: number)
                {
                    const output = Math.Vector2D.rotate(testVector, angle);

                    expect(output.x, "x is incorrect").to.be.approximately(expectedX, epsilon);
                    expect(output.y, "y is incorrect").to.be.approximately(expectedY, epsilon);
                }

                it("should not change the vector when angle == 0", function ()
                {
                    test(0, 0, 1);
                });

                it("should transpose the vector when angle == 1/4 circle", function ()
                {
                    test(Math.Angles.quarterCircle, 1, 0);
                });

                it("should transpose and invert the vector when angle == -1/4 circle", function ()
                {
                    test(-Math.Angles.quarterCircle, -1, 0);
                });

                it("should flip the vector when angle == 1/2 circle", function ()
                {
                    test(Math.Angles.halfCircle, 0, -1);
                });

                it("should return the correct result when angle == 1/8 circle", function ()
                {
                    test(Math.Angles.eighthCircle, 0.70711, 0.70711);
                });
            });

            describe("dot", function ()
            {
                it("should return the dot product of two vectors", function ()
                {
                    expect(Math.Vector2D.dot(Math.Vector2D(0, 1), Math.Vector2D(1, 0))).to.be.equal(0);
                    expect(Math.Vector2D.dot(Math.Vector2D(0, 1), Math.Vector2D(0, 1))).to.be.equal(1);
                    expect(Math.Vector2D.dot(Math.Vector2D(0, 1), Math.Vector2D(0, -1))).to.be.equal(-1);
                    expect(Math.Vector2D.dot(Math.Vector2D(1, 2), Math.Vector2D(3, 4))).to.be.equal(11);
                });
            });

            describe("angleBetween", function ()
            {
                it("should return the angle between two vectors", function ()
                {
                    expect(Math.Vector2D.angleBetween(Math.Vector2D(0, 1), Math.Vector2D(0, 1))).to.be.approximately(0, epsilon);
                    expect(Math.Vector2D.angleBetween(Math.Vector2D(0, 1), Math.Vector2D(1, 0))).to.be.approximately(Math.Angles.quarterCircle, epsilon);
                    expect(Math.Vector2D.angleBetween(Math.Vector2D(0, 1), Math.Vector2D(-1, 0))).to.be.approximately(-Math.Angles.quarterCircle, epsilon);
                    expect(Math.Vector2D.angleBetween(Math.Vector2D(0, 1), Math.Vector2D(0, -1))).to.be.approximately(Math.Angles.halfCircle, epsilon);
                });
            });

            describe("findBetween", function ()
            {
                it("should return a normalised direction between two points", function ()
                {
                    expect(Math.Vector2D.findBetween(Math.Vector2D(0, 0), Math.Vector2D(0, 10))).to.be.deep.equal(Math.Vector2D(0, 1));
                });
            });

            describe("distanceBetween", function ()
            {
                it("should return the distance between two points", function ()
                {
                    expect(Math.Vector2D.distanceBetween(Math.Vector2D(0, -2), Math.Vector2D(0, 10))).to.be.equal(12);
                });
            });

            describe("sizeSq", function ()
            {
                it("should return the magnitude squared of a vector", function ()
                {
                    expect(Math.Vector2D.sizeSq(Math.Vector2D(1, 2))).to.be.equal(5);
                });
            });

            describe("size", function ()
            {
                it("should return the magnitude of a vector", function ()
                {
                    expect(Math.Vector2D.size(Math.Vector2D(1, 2))).to.be.equal(Math.sqrt(5));
                });
            });

            describe("normalise", function ()
            {
                it("should set the magnitude of a vector to 1", function ()
                {
                    const vec = Math.Vector2D(1, 2);
                    Math.Vector2D.normalise(vec);
                    expect(Math.Vector2D.size(vec)).to.be.approximately(1, epsilon);
                });
            });

            describe("normalised", function ()
            {
                it("should return a vector with a magnitude of 1", function ()
                {
                    const vec = Math.Vector2D(1, 2);
                    const vec2 = Math.Vector2D.normalised(vec);
                    expect(Math.Vector2D.size(vec2)).to.be.approximately(1, epsilon);
                    expect(Math.Vector2D.size(vec)).to.not.be.equal(1);
                });
            });

            describe("linearInterpolate", function ()
            {
                it("should linearly interpolate between two vectors", function ()
                {
                    expect(Math.Vector2D.linearInterpolate(Math.Vector2D(1, 2), Math.Vector2D(3, 0), 0.5)).to.be.deep.equal(Math.Vector2D(2, 1));
                });
            });

            describe("equals", function ()
            {
                it("should return true for equal vectors", function ()
                {
                    expect(Math.Vector2D.equals(Math.Vector2D(1, 2), Math.Vector2D(1, 2))).to.be.true;
                });

                it("should return false for non-equal vectors", function ()
                {
                    expect(Math.Vector2D.equals(Math.Vector2D(1, 2), Math.Vector2D(1, 3))).to.be.false;
                    expect(Math.Vector2D.equals(Math.Vector2D(1, 2), Math.Vector2D(2, 2))).to.be.false;
                    expect(Math.Vector2D.equals(Math.Vector2D(1, 2), Math.Vector2D(2, 3))).to.be.false;
                });
            });

            describe("clone", function ()
            {
                it("should return a deep clone of a vector", function ()
                {
                    const vec = Math.Vector2D(1, 2);
                    const cl = Math.Vector2D.clone(vec);
                    expect(vec).to.not.equals(cl);
                    expect(vec).to.deep.equals(cl);
                });
            });

            describe("copy", function ()
            {
                it("should copy the value of a vector into another", function ()
                {
                    const vec = Math.Vector2D(1, 2);
                    Math.Vector2D.copy(Math.Vector2D(3, 4), vec);
                    expect(vec).to.deep.equal(Math.Vector2D(3, 4));
                });
            });

            describe("set", function ()
            {
                it("should set the components of a vector", function ()
                {
                    const vec = Math.Vector2D(1, 2);
                    Math.Vector2D.set(vec, 3, 4);
                    expect(vec).to.deep.equal(Math.Vector2D(3, 4));
                });
            });

            describe("project", function ()
            {
                it("should return the projection of a vector on another vector", function ()
                {
                    expect(Math.Vector2D.project(Math.Vector2D(8, 8), Math.Vector2D(3, 0))).to.be.deep.equal(RS.Math.Vector2D(8, 0));
                    expect(Math.Vector2D.project(Math.Vector2D(5, 2), Math.Vector2D(0, 7))).to.be.deep.equal(RS.Math.Vector2D(0, 2));
                    expect(Math.Vector2D.project(Math.Vector2D(-7, -4), Math.Vector2D(7, 4))).to.be.deep.equal(RS.Math.Vector2D(-7, -4));
                    const projection = Math.Vector2D.project(Math.Vector2D(3, 9), Math.Vector2D(4, 6));
                    expect(projection.x).to.be.approximately(66 / 13, epsilon);
                    expect(projection.y).to.be.approximately(99 / 13, epsilon);
                });
            });
        });
    });

    describe("Vec2.ts", function ()
    {
        describe("Vec2", function ()
        {
            const epsilon = 0.00001;

            describe("constructor", function ()
            {
                it("should create a Vec2 with the given x and y", function ()
                {
                    expect(new Vec2(8, 7)).to.include({ x: 8, y: 7 });
                });
            });

            describe("create", function ()
            {
                it("should create a Vec2 with the given x and y", function ()
                {
                    expect(Vec2.create(4, -8)).to.include({ x: 4, y: -8 });
                    expect(Vec2.create(7, 3)).to.not.include({ x: 4, y: -8 });
                });
            });

            describe("zero", function ()
            {
                it("should create a Vec2 with coordinates (0, 0)", function ()
                {
                    expect(Vec2.zero).to.include({ x: 0, y: 0 });
                });
            });

            describe("add", function ()
            {
                it("should add one Vec2 to another", function ()
                {
                    const vec = Vec2.create(1, 4);
                    vec.add(Vec2.create(3, 11));
                    expect(vec).to.include({ x: 4, y: 15 });
                });

                it("should return the sum of two Vec2s", function ()
                {
                    expect(Vec2.add(Vec2.create(3, 3), Vec2.create(5, 2))).to.include({ x: 8, y: 5 });
                });
            });

            describe("subtract", function ()
            {
                it("should subtract one Vec2 from another", function ()
                {
                    const vec = Vec2.create(6, 7);
                    vec.subtract(Vec2.create(8, 3));
                    expect(vec).to.include({ x: -2, y: 4 });
                });

                it("should return the difference of two Vec2s", function ()
                {
                    expect(Vec2.subtract(Vec2.create(7, 8), Vec2.create(1, 10))).to.include({ x: 6, y: -2 });
                });
            });

            describe("multiply", function ()
            {
                it("should scale a Vec2 by a scalar", function ()
                {
                    const vec = Vec2.create(-4, 5);
                    vec.multiply(3);
                    expect(vec).to.include({ x: -12, y: 15 });
                });

                it("should return the product of a Vec2 and a scalar", function ()
                {
                    expect(Vec2.multiply(Vec2.create(1, 2), 2)).to.include({ x: 2, y: 4 });
                });
            });

            describe("divide", function ()
            {
                it("should divide a Vec2 by a scalar", function ()
                {
                    const vec = Vec2.create(10, 35);
                    vec.divide(5)
                    expect(vec).to.include({ x: 2, y: 7 });
                });

                it("should return the factor of a Vec2 and a scalar", function ()
                {
                    expect(Vec2.divide(Vec2.create(8, 20), 2)).to.include({ x: 4, y: 10 });
                });
            });

            describe("rotate", function ()
            {
                it("should rotate the Vec2 by some angle", function ()
                {
                    const vec = new Vec2(4, 4);
                    vec.rotate(Math.degreesToRadians(23));

                    expect(vec.x).to.be.approximately(2.1190948998527, epsilon);
                    expect(vec.y).to.be.approximately(5.2449439277669, epsilon);
                });
            });

            describe("dot", function ()
            {
                it("should return the dot product of two Vec2s", function ()
                {
                    expect(Vec2.create(3, 4).dot(Vec2.create(2, -3))).to.be.equal(-6);
                    expect(Vec2.dot(Vec2.create(6, 2), Vec2.create(8, 1))).to.be.equal(50);
                });
            });

            describe("size", function ()
            {
                it("should return the magnitude of a Vec2", function ()
                {
                    expect(Vec2.create(6, 3).size).to.be.approximately(Math.sqrt(45), epsilon);
                    expect(Vec2.create(-4, 8).size).to.be.approximately(Math.sqrt(80), epsilon);
                });
            });

            describe("normalise", function ()
            {
                it("should set the magnitude of a Vec2 to 1", function ()
                {
                    const vec = new Vec2(3, 4);
                    const norm = vec.size;
                    vec.normalise();
                    expect(vec.x).to.be.approximately(3 / norm, epsilon);
                    expect(vec.y).to.be.approximately(4 / norm, epsilon);
                });
            });

            describe("normalised", function ()
            {
                it("should return a Vec2 with a magnitude of 1", function ()
                {
                    const vec = new Vec2(8, 9);
                    const norm = vec.size;
                    const vec2 = vec.normalised();
                    expect(vec.x).to.be.equal(8);
                    expect(vec2.x).to.be.approximately(8 / norm, epsilon);
                    expect(vec2.y).to.be.approximately(9 / norm, epsilon);
                });
            });

            describe("clone", function ()
            {
                it("should return a deep clone of a Vec2", function ()
                {
                    const vec = new Vec2(3, 5);
                    const copy = vec.clone();
                    vec.set(3, 3);
                    expect(copy).to.include({ x: 3, y: 5 });
                });
            });

            describe("set", function ()
            {
                it("should set the components of a Vec2", function ()
                {
                    const vec = new Vec2(3, 3);
                    vec.set(4, 4);
                    expect(vec).to.include({ x: 4, y: 4 });
                });
            });

            describe("project", function ()
            {
                it("should return the projection of a Vec2 on another Vec2", function ()
                {
                    const projection1 = Vec2.create(4, 9).project(Vec2.create(3, 7));
                    expect(projection1.x).to.be.approximately(225 / 58, epsilon);
                    expect(projection1.y).to.be.approximately(525 / 58, epsilon);

                    const projection2 = Vec2.create(-4, 5).project(Vec2.create(7, -2));
                    expect(projection2.x).to.be.approximately(-266 / 53, epsilon);
                    expect(projection2.y).to.be.approximately(76 / 53, epsilon);
                });
            });
        });
    });
}