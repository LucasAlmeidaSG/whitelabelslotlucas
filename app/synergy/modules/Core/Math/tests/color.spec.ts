namespace RS.Tests
{
    const { expect } = chai;

    describe("Color.ts", function ()
    {
        describe("Color", function ()
        {
            function expectColorToBeNearlyEqual(color: Util.Color, to: Util.ColorLike, message?: string)
            {
                expect(color.r).to.be.closeTo(to.r, 1/256, message);
                expect(color.g).to.be.closeTo(to.g, 1/256, message);
                expect(color.b).to.be.closeTo(to.b, 1/256, message);
                if (to.a != null)
                {
                    expect(color.a).to.be.closeTo(to.a, 1/256, message);
                }
            }

            describe("constructor", function ()
            {
                it("should initialise a color from a tint", function ()
                {
                    const color = new Util.Color(0xff6699);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 102/255, b: 153/255, a: 1.0 });
                });

                it("should initialise a color from a tint and an alpha", function ()
                {
                    const color = new Util.Color(0xff6699, 0.5);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 102/255, b: 153/255, a: 0.5 });
                });

                it("should initialise a color from normalised rgb values [0-1]", function ()
                {
                    const color = new Util.Color(255/255, 102/255, 153/255);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 102/255, b: 153/255, a: 1.0 });
                });

                it("should initialise a color from normalised rgba values [0-1]", function ()
                {
                    const color = new Util.Color(255/255, 102/255, 153/255, 53/255);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 102/255, b: 153/255, a: 53/255 });
                });

                it("should initialise a color from non-normalised rgb values [0-255]", function ()
                {
                    const color = new Util.Color(255, 102, 153);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 102/255, b: 153/255, a: 1.0 });

                    const color2 = new Util.Color(1, 102, 153)
                    expectColorToBeNearlyEqual(color2, { r: 1/255, g: 102/255, b: 153/255, a: 1.0 });
                });

                it("should initialise a color from non-normalised rgba values [0-255]", function ()
                {
                    const color = new Util.Color(255, 102, 153, 53);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 102/255, b: 153/255, a: 53/255 });

                    const color2 = new Util.Color(1, 102, 153, 15)
                    expectColorToBeNearlyEqual(color2, { r: 1/255, g: 102/255, b: 153/255, a: 15/255 });

                    const color3 = new Util.Color(1, 102, 153, 1)
                    expectColorToBeNearlyEqual(color3, { r: 1/255, g: 102/255, b: 153/255, a: 1.0 });
                });

                it("should initialise a color from a name", function ()
                {
                    const color = new Util.Color("pink");
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 192/255, b: 203/255, a: 1.0 });
                });

                it("should initialise a color from a name and an alpha", function ()
                {
                    const color = new Util.Color("pink", 0.5);
                    expectColorToBeNearlyEqual(color, { r: 255/255, g: 192/255, b: 203/255, a: 0.5 });
                });
            });

            describe("set", function ()
            {
                it("should update all color channels", function ()
                {
                    const color = new Util.Color(0xffffff);
                    color.set(0.25, 0.5, 0.75);
                    expectColorToBeNearlyEqual(color, { r: 0.25, g: 0.5, b: 0.75 });
                });

                it("should only certain color channels", function ()
                {
                    const color = new Util.Color(0xffffff);
                    color.set(0.0, null, 0.5);
                    expectColorToBeNearlyEqual(color, { r: 0.0, g: 1.0, b: 0.5 });
                });
            });

        });
    });
}
