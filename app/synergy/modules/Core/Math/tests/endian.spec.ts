namespace RS.Tests
{
    const { expect } = chai;

    describe("Endian.ts", function ()
    {
        describe("swapUInt32", function ()
        {
            it("should swap the endianness of a uint32", function ()
            {
                expect(Math.Endian.swapUInt32(541478227)).to.be.equal(1397573152);
                expect(Math.Endian.swapUInt32(1397573152)).to.be.equal(541478227);
            });
        });

        describe("swapUInt32Array", function ()
        {
            it("should swap the endianness of each element of a uint32 array", function ()
            {
                const arr = new Uint32Array([ 541478227, 1397573152 ]);
                const swapped = Math.Endian.swapUInt32Array(arr);
                expect(swapped).to.not.equal(arr);
                expect(swapped[0]).to.be.equal(1397573152);
                expect(swapped[1]).to.be.equal(541478227);
            });
        });

        describe("swapUInt16", function ()
        {
            it("should swap the endianness of a uint16", function ()
            {
                expect(Math.Endian.swapUInt16(19795)).to.be.equal(21325);
                expect(Math.Endian.swapUInt16(21325)).to.be.equal(19795);
            });
        });

        describe("swapUInt16Array", function ()
        {
            it("should swap the endianness of each element of a uint16 array", function ()
            {
                const arr = new Uint16Array([ 19795, 21325 ]);
                const swapped = Math.Endian.swapUInt16Array(arr);
                expect(swapped).to.not.equal(arr);
                expect(swapped[0]).to.be.equal(21325);
                expect(swapped[1]).to.be.equal(19795);
            });
        });
    });    
}