namespace RS.Tests
{
    const { expect } = chai;

    describe("PIDController.ts", function ()
    {
        describe("PIDController", function ()
        {
            describe("constructor", function ()
            {
                it("should initialise to sensible defaults", function ()
                {
                    const controller = new Math.PIDController({ Kp: 5, Ki: 3, Kd: 3 });
                    expect(controller.processVariable).to.equal(0);
                    expect(controller.controlVariable).to.equal(0);
                    expect(controller.setPoint).to.equal(0);
                    expect(controller.integral).to.equal(0);
                    expect(controller.error).to.equal(0);
                });

                it("should initialise the error and integral from settings", function ()
                {
                    const controller = new Math.PIDController({ Kp: 5, Ki: 3, Kd: 3, initialError: 0.5, initialIntegral: 1.5 });
                    expect(controller.error).to.equal(0.5);
                    expect(controller.integral).to.equal(1.5);
                });
            });

            describe("advance", function ()
            {
                it("should converge on target value", function ()
                {
                    const controller = new Math.PIDController({ Kp: 2.5, Ki: 0.05, Kd: 0.01 });
                    let value = 0.0;
                    controller.setPoint = 10.0;
                    const dt = 100;
                    console.log(`0: controlVariable=0.0, value=${value}`);
                    for (let i = 0; i < 10; ++i)
                    {
                        controller.processVariable = value;
                        const accel = controller.advance(dt);
                        value += accel * (dt / 1000) * 2.0;
                        console.log(`${i+1}: controlVariable=${accel}, value=${value}`);
                    }
                    expect(value).to.be.approximately(controller.setPoint, 0.1);
                });
            });
        });
    });
}