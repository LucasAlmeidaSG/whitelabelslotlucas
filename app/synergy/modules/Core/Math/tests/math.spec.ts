namespace RS.Tests
{
    const { expect } = chai;

    describe("Math.ts", function ()
    {
        describe("intersect1D", function ()
        {
            it("should return true for intersecting ranges", function ()
            {
                // <-->
                // <-->
                expect(Math.intersect1D(0, 3, 0, 3)).to.be.true;

                // <-->
                //   <-->
                expect(Math.intersect1D(0, 3, 2, 5)).to.be.true;

                //   <-->
                // <-->
                expect(Math.intersect1D(2, 5, 0, 3)).to.be.true;

                // <---->
                // <-->
                expect(Math.intersect1D(0, 5, 0, 3)).to.be.true;

                // <---->
                //   <-->
                expect(Math.intersect1D(0, 5, 2, 5)).to.be.true;

                // <---->
                //  <-->
                expect(Math.intersect1D(1, 4, 2, 5)).to.be.true;

                // <-->
                // <---->
                expect(Math.intersect1D(0, 3, 0, 5)).to.be.true;

                //   <-->
                // <---->
                expect(Math.intersect1D(2, 5, 0, 5)).to.be.true;

                //  <-->
                // <---->
                expect(Math.intersect1D(1, 4, 0, 5)).to.be.true;

                // <-->
                //    <-->
                expect(Math.intersect1D(0, 3, 3, 6)).to.be.true;

                //    <-->
                // <-->
                expect(Math.intersect1D(3, 6, 0, 3)).to.be.true;
            });

            it("should return false for non-intersecting ranges", function ()
            {
                // <-->
                //     <-->
                expect(Math.intersect1D(0, 3, 4, 7)).to.be.false;

                //     <-->
                // <-->
                expect(Math.intersect1D(4, 7, 0, 3)).to.be.false;
            });
        });

        describe("sliceCircular", function ()
        {
            it("should return an empty array when passed an empty array", function ()
            {
                expect(Math.sliceCircular([], -21, 33)).to.be.empty;
                expect(Math.sliceCircular([], 0, 0)).to.be.empty;
            });

            it("should return a wrapped slice of an array", function ()
            {
                expect(Math.sliceCircular([1,2,3], 0, 3), "full-length slice").to.deep.equal([1, 2, 3]);
                expect(Math.sliceCircular([1,2,3], 0, 1), "partial slice").to.deep.equal([1]);
                expect(Math.sliceCircular([1,2,3], 0, 0), "empty slice").to.deep.equal([]);
                expect(Math.sliceCircular([1,2,3], -5, 8), "looping slice").to.deep.equal([2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2]);
            });
        })

        describe("scale", function ()
        {
            it("should return the scale to transform the first scale into the second in Stretch mode", function ()
            {
                expect(Math.scale({ w: 100, h: 200 }, { w: 100, h: 200 }, Math.ScaleMode.Stretch), "same size").to.deep.equal({ x: 1, y: 1 });

                expect(Math.scale({ w: 2, h: 4 }, { w: 6, h: 24 }, Math.ScaleMode.Stretch), "smaller size").to.deep.equal({ x: 3, y: 6 });

                expect(Math.scale({ w: 12, h: 96 }, { w: 6, h: 24 }, Math.ScaleMode.Stretch), "larger size").to.deep.equal({ x: 0.5, y: 0.25 });
            });

            it("should return the scale to fit the first size within the second in Contain mode", function ()
            {
                expect(Math.scale({ w: 100, h: 200 }, { w: 100, h: 200 }, Math.ScaleMode.Contain), "same size").to.deep.equal({ x: 1, y: 1 });

                expect(Math.scale({ w: 50, h: 100 }, { w: 100, h: 200 }, Math.ScaleMode.Contain), "same aspect ratio, smaller size").to.deep.equal({ x: 2, y: 2 });

                expect(Math.scale({ w: 200, h: 400 }, { w: 100, h: 200 }, Math.ScaleMode.Contain), "same aspect ratio, larger size").to.deep.equal({ x: 0.5, y: 0.5 });

                expect(Math.scale({ w: 960, h: 600 }, { w: 1920, h: 1080 }, Math.ScaleMode.Contain), "wider aspect ratio, smaller size").to.deep.equal({ x: 1.8, y: 1.8 });

                expect(Math.scale({ w: 3840, h: 2400 }, { w: 1920, h: 1080 }, Math.ScaleMode.Contain), "wider aspect ratio, larger size").to.deep.equal({ x: 0.45, y: 0.45 });

                expect(Math.scale({ w: 960, h: 400 }, { w: 1920, h: 1080 }, Math.ScaleMode.Contain), "narrower aspect ratio, smaller size").to.deep.equal({ x: 2, y: 2 });

                expect(Math.scale({ w: 3840, h: 1600 }, { w: 1920, h: 1080 }, Math.ScaleMode.Contain), "narrower aspect ratio, larger size").to.deep.equal({ x: 0.5, y: 0.5 });
            });

            it("should return the scale to cover the second size with the first in Cover mode", function ()
            {
                expect(Math.scale({ w: 100, h: 200 }, { w: 100, h: 200 }, Math.ScaleMode.Cover), "same size").to.deep.equal({ x: 1, y: 1 });

                expect(Math.scale({ w: 50, h: 100 }, { w: 100, h: 200 }, Math.ScaleMode.Cover), "same aspect ratio, smaller size").to.deep.equal({ x: 2, y: 2 });

                expect(Math.scale({ w: 200, h: 400 }, { w: 100, h: 200 }, Math.ScaleMode.Cover), "same aspect ratio, larger size").to.deep.equal({ x: 0.5, y: 0.5 });

                expect(Math.scale({ w: 960, h: 600 }, { w: 1920, h: 1080 }, Math.ScaleMode.Cover), "wider aspect ratio, smaller size").to.deep.equal({ x: 2, y: 2 });

                expect(Math.scale({ w: 3840, h: 2400 }, { w: 1920, h: 1080 }, Math.ScaleMode.Cover), "wider aspect ratio, larger size").to.deep.equal({ x: 0.5, y: 0.5 });

                expect(Math.scale({ w: 960, h: 400 }, { w: 1920, h: 1080 }, Math.ScaleMode.Cover), "narrower aspect ratio, smaller size").to.deep.equal({ x: 2.7, y: 2.7 });

                expect(Math.scale({ w: 3840, h: 1600 }, { w: 1920, h: 1080 }, Math.ScaleMode.Cover), "narrower aspect ratio, larger size").to.deep.equal({ x: 0.675, y: 0.675 });
            });
        });

        describe("intersect1DWrapped", function ()
        {
            it("should return true for intersecting ranges", function ()
            {
                // <-->
                // <-->
                expect(Math.intersect1DWrapped(0, 3, 0, 3, 0, 10)).to.be.true;

                // <-->
                //   <-->
                expect(Math.intersect1DWrapped(0, 3, 2, 5, 0, 10)).to.be.true;

                //   <-->
                // <-->
                expect(Math.intersect1DWrapped(2, 5, 0, 3, 0, 10)).to.be.true;

                // <---->
                // <-->
                expect(Math.intersect1DWrapped(0, 5, 0, 3, 0, 10)).to.be.true;

                // <---->
                //   <-->
                expect(Math.intersect1DWrapped(0, 5, 2, 5, 0, 10)).to.be.true;

                // <---->
                //  <-->
                expect(Math.intersect1DWrapped(1, 4, 2, 5, 0, 10)).to.be.true;

                // <-->
                // <---->
                expect(Math.intersect1DWrapped(0, 3, 0, 5, 0, 10)).to.be.true;

                //   <-->
                // <---->
                expect(Math.intersect1DWrapped(2, 5, 0, 5, 0, 10)).to.be.true;

                //  <-->
                // <---->
                expect(Math.intersect1DWrapped(1, 4, 0, 5, 0, 10)).to.be.true;

                // <-->
                //    <-->
                expect(Math.intersect1DWrapped(0, 3, 3, 6, 0, 10)).to.be.true;

                //    <-->
                // <-->
                expect(Math.intersect1DWrapped(3, 6, 0, 3, 0, 10)).to.be.true;

                // ->   <----
                // <-->
                expect(Math.intersect1DWrapped(5, 11, 0, 3, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(-5, 1, 0, 3, 0, 10)).to.be.true;

                // ->   <----
                //    <-->
                expect(Math.intersect1DWrapped(5, 11, 3, 6, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(-5, 1, 3, 6, 0, 10)).to.be.true;

                // ->   <----
                // >      <--
                expect(Math.intersect1DWrapped(5, 11, 7, 10, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(-5, 1, 7, 10, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(5, 11, 7, 0, -10, 10)).to.be.true;
                expect(Math.intersect1DWrapped(-5, 1, 7, 0, -10, 10)).to.be.true;

                // <-->
                // ->   <----
                expect(Math.intersect1DWrapped(0, 3, 5, 11, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(0, 3, -5, 1, 0, 10)).to.be.true;

                //    <-->
                // ->   <----
                expect(Math.intersect1DWrapped(3, 6, 5, 11, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(3, 6, -5, 1, 0, 10)).to.be.true;

                // >      <--
                // ->   <----
                expect(Math.intersect1DWrapped(7, 10, 5, 11, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(7, 10, -5, 1, 0, 10)).to.be.true;
                expect(Math.intersect1DWrapped(7, 0, 5, 11, -10, 10)).to.be.true;
                expect(Math.intersect1DWrapped(7, 0, -5, 1, -10, 10)).to.be.true;

                // --><------
                //      <-->
                expect(Math.intersect1DWrapped(13, 24, 5, 8, 0, 10)).to.be.true;

                //      <-->
                // --><------
                expect(Math.intersect1DWrapped(5, 8, 13, 24, 0, 10)).to.be.true;
            });

            it("should return false for non-intersecting ranges", function ()
            {
                // <-->
                //     <-->
                expect(Math.intersect1DWrapped(0, 3, 4, 7, 0, 10)).to.be.false;

                //     <-->
                // <-->
                expect(Math.intersect1DWrapped(4, 7, 0, 3, 0, 10)).to.be.false;

                // ->      <-
                //    <-->
                expect(Math.intersect1DWrapped(8, 11, 3, 6, 0, 10)).to.be.false;
                expect(Math.intersect1DWrapped(-2, 1, 3, 6, 0, 10)).to.be.false;

                //    <-->
                // ->      <-
                expect(Math.intersect1DWrapped(3, 6, 8, 11, 0, 10)).to.be.false;
                expect(Math.intersect1DWrapped(3, 6, -2, 1, 0, 10)).to.be.false;
            });
        });

        describe("shiftDecimal", function ()
        {
            it("should move the decimal point by the spaces specified", function ()
            {
                expect(Math.shiftDecimal(5.2343, 1)).to.equal(52.343);
                expect(Math.shiftDecimal(34501.23189, 4)).to.equal(345012318.9);
                expect(Math.shiftDecimal(5234.3, -1)).to.equal(523.43);
                expect(Math.shiftDecimal(34501.23189, -4)).to.equal(3.450123189);
                expect(Math.shiftDecimal(123.456, 0)).to.equal(123.456);
                expect(Math.shiftDecimal(754, 0)).to.equal(754);
            });

            it("should add extra zeroes when the places to shift will move the decimal point to or pass the end of the number", function ()
            {
                expect(Math.shiftDecimal(6455.142, 3)).to.equal(6455142);
                expect(Math.shiftDecimal(8234.7, 3)).to.equal(8234700);
                expect(Math.shiftDecimal(82347, 1)).to.equal(823470);
            });

            it("should add extra zeroes when the places to shift will move the decimal point to or pass the start of the number", function ()
            {
                expect(Math.shiftDecimal(645.5142, -3)).to.equal(0.6455142);
                expect(Math.shiftDecimal(8.2347, -3)).to.equal(0.0082347);
                expect(Math.shiftDecimal(82347, -6)).to.equal(0.082347);
            });
        });

        describe("ceil", function ()
        {
            it("should apply a given epsilon correctly", function ()
            {
                expect(Math.ceil(4.00000000001)).to.equal(5);
                expect(Math.ceil(4.99999999991)).to.equal(5);
                expect(Math.ceil(4.00000000001, 0.0000000001)).to.equal(4);
                expect(Math.ceil(4.99999999991, 0.0000000001)).to.equal(5);
                expect(Math.ceil(4.001, 0.0000000001)).to.equal(5);
                expect(Math.ceil(4, 0.0000000001)).to.equal(4);
            });
        });

        describe("floor", function ()
        {
            it("should apply a given epsilon correctly", function ()
            {
                expect(Math.floor(4.99999999991)).to.equal(4);
                expect(Math.floor(4.00000000001)).to.equal(4);
                expect(Math.floor(4.99999999991, 0.0000000001)).to.equal(5);
                expect(Math.floor(4.00000000001, 0.0000000001)).to.equal(4);
                expect(Math.floor(4.991, 0.0000000001)).to.equal(4);
                expect(Math.floor(4, 0.0000000001)).to.equal(4);
            });
        });

        describe("generateRandomNumber", function ()
        {
            it("should generate a number within the inclusive minumum & exclusive maximum range", function ()
            {
                const n = 10000;
                for (let i = 0; i < n; i++)
                {
                    const num = Math.generateRandomNumber(50.0, 100.0, false);

                    expect(num).to.be.within(50.0, 100.0);
                    expect(num).to.not.equal(100.0);
                }
            });
            it("should generate a number within the inclusive minumum & exclusive maximum range, and flip the sign of the generated number 50% of the time if invert is true", function ()
            {
                const n = 10000;

                let flipCount = 0;
                for (let i = 0; i < n; i++)
                {
                    const num = Math.generateRandomNumber(50.0, 100.0, true);
                    if (num < 0) { ++flipCount; }

                    expect(num).to.be.within(-100.0, 100.0);
                    expect(num).to.not.be.within(-49.99999999999999, 49.99999999999999);
                    expect(num).to.not.equal(100.0);
                    expect(num).to.not.equal(-100.0);
                }

                expect(flipCount).to.be.approximately(n / 2, 500);
            });
        });

        describe("generateRandomInt", function ()
        {
            it("should generate a number within the inclusive minumum & exclusive maximum range", function ()
            {
                const n = 10000;
                for (let i = 0; i < n; i++)
                {
                    const num = Math.generateRandomInt(50, 100, false);

                    expect(num).to.be.within(50, 99);
                }
            });
            it("should generate a integer within the inclusive minumum & exclusive maximum range, and flip the sign of the generated integer 50% of the time if invert is true", function ()
            {
                const n = 10000;

                let flipCount = 0;
                for (let i = 0; i < n; i++)
                {
                    const num = Math.generateRandomInt(50, 100, true);
                    if (num < 0) { ++flipCount; }

                    expect(num).to.be.within(-100, 100);
                    expect(num).to.not.be.within(-49, 49);
                }

                expect(flipCount).to.be.approximately(n / 2, 500);
            });
        });

        describe("clamp", function ()
        {
            it("should clamp values within a range", function ()
            {
                expect(Math.clamp(20, 0, 10)).to.equal(10);
                expect(Math.clamp(-10, 0, 10)).to.equal(0);
            });
            it("not alter values within the range", function ()
            {
                expect(Math.clamp(5, 0, 10)).to.equal(5);
            });
            it("throw an error if the bounds are invalid", function ()
            {
                const maxVal = 0;
                const minVal = 10;
                expect(() => Math.clamp(5, minVal, maxVal)).to.throw(`Maximum value (${maxVal}) is less than the minimum value (${minVal}).`);
            });
        });
    });
}