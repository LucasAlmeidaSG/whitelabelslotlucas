namespace RS.Tests
{
    const { expect } = chai;

    describe("Line2D.ts", function ()
    {
        describe("Line2D", function ()
        {
            describe("intersection", function ()
            {
                it("should return the correct point of intersection of two infinite-length lines", function ()
                {
                    expect(Math.Line2D.intersection({ x: 1, y: 1, dx: 4, dy: 0 }, { x: 2, y: 0, dx: 0, dy: 6 }), "horizontal and vertical").to.deep.equal({ x: 2, y: 1 });
                });

                it("should return NaN for parallel lines", function ()
                {
                    expect(Math.Line2D.intersection({ x: 1, y: 0, dx: 0, dy: 6 }, { x: 2, y: 0, dx: 0, dy: 6 }), "horizontal and vertical").to.deep.equal({ x: NaN, y: NaN });
                });
            });
        });
    });
}