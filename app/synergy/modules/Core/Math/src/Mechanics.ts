namespace RS.Math.Mechanics
{
    export interface InitialSpeed { initialSpeed: number; }
    export interface FinalSpeed { finalSpeed: number; }
    export interface Acceleration { acceleration: number; }
    export interface Time { time: number; }
    export interface Displacement { displacement: number; }

    // Le Suvat
    // (1) v = u + at
    // (2) s = ut + 0.5at^2
    // (3) s = 0.5(u + v)t
    // (4) v^2 = u^2 + 2as
    // (5) s = vt - 0.5at^2

    // == TIME ==
    // (1) t = (v - u) / a
    // (2) QUADRATIC 0.5at^2 + ut - s = 0
    // (3) t = 2s / (u + v)
    // (5) QUADRATIC -0.5at^2 + vt - s = 0

    export function solveTime(knowns: InitialSpeed & FinalSpeed & Acceleration): number;

    export function solveTime(knowns: Acceleration & InitialSpeed & Displacement): number;

    export function solveTime(knowns: Displacement & InitialSpeed & FinalSpeed): number;

    export function solveTime(knowns: Acceleration & FinalSpeed & Displacement): number;

    export function solveTime(p: Partial<Displacement & InitialSpeed & FinalSpeed & Acceleration & Time>): number|null
    {
        // Discover which knowns we have
        const
            haveDisplacement = Is.number(p.displacement),
            haveInitialSpeed = Is.number(p.initialSpeed),
            haveFinalSpeed = Is.number(p.finalSpeed),
            haveAcceleration = Is.number(p.acceleration),
            haveTime = Is.number(p.time);
        const numKnowns =
            (haveDisplacement ? 1 : 0) +
            (haveInitialSpeed ? 1 : 0) +
            (haveFinalSpeed ? 1 : 0) +
            (haveAcceleration ? 1 : 0) +
            (haveTime ? 1 : 0);
        const numUnknowns = 5 - numKnowns;

        if (haveInitialSpeed && haveFinalSpeed && haveAcceleration)
        {
            return (p.finalSpeed - p.initialSpeed) / p.acceleration;
        }
        else if (haveAcceleration && haveInitialSpeed && haveDisplacement)
        {
            const solution = Algebra.solveQuadratic(0.5 * p.acceleration, p.initialSpeed, -p.displacement);
            if (solution == null) { return null; }
            return Is.number(solution) ? solution : Math.max(solution.a, solution.b);
        }
        else if (haveDisplacement && haveInitialSpeed && haveFinalSpeed)
        {
            return (p.displacement * 2.0) / (p.initialSpeed + p.finalSpeed);
        }
        else if (haveAcceleration && haveFinalSpeed && haveDisplacement)
        {
            const solution = Algebra.solveQuadratic(-0.5 * p.acceleration, p.finalSpeed, -p.displacement);
            if (solution == null) { return null; }
            return Is.number(solution) ? solution : Math.max(solution.a, solution.b);
        }

        return null;
    }

    // == INITIAL SPEED ==
    // (1) u = v - at
    // (2) u = (s - 0.5at^2) / t
    // (3) u = 2st - v
    // (4) u = sqrt(v^2 - 2as)

    export function solveInitialSpeed(knowns: FinalSpeed & Acceleration & Time): number;

    export function solveInitialSpeed(knowns: Displacement & Acceleration & Time): number;

    export function solveInitialSpeed(knowns: Displacement & Time & FinalSpeed): number;

    export function solveInitialSpeed(knowns: FinalSpeed & Acceleration & Displacement): number;

    export function solveInitialSpeed(p: Partial<Displacement & InitialSpeed & FinalSpeed & Acceleration & Time>): number|null
    {
        // Discover which knowns we have
        const
            haveDisplacement = Is.number(p.displacement),
            haveInitialSpeed = Is.number(p.initialSpeed),
            haveFinalSpeed = Is.number(p.finalSpeed),
            haveAcceleration = Is.number(p.acceleration),
            haveTime = Is.number(p.time);
        const numKnowns =
            (haveDisplacement ? 1 : 0) +
            (haveInitialSpeed ? 1 : 0) +
            (haveFinalSpeed ? 1 : 0) +
            (haveAcceleration ? 1 : 0) +
            (haveTime ? 1 : 0);
        const numUnknowns = 5 - numKnowns;

        if (haveFinalSpeed && haveAcceleration && haveTime)
        {
            return p.finalSpeed - p.acceleration * p.time;
        }
        else if (haveDisplacement && haveAcceleration && haveTime)
        {
            return (p.displacement - 0.5 * p.acceleration * p.time) / p.time;
        }
        else if (haveDisplacement && haveTime && haveFinalSpeed)
        {
            return 2.0 * p.displacement * p.time - p.finalSpeed;
        }
        else if (haveFinalSpeed && haveAcceleration && haveDisplacement)
        {
            return Math.sqrt(p.finalSpeed * p.finalSpeed - 2.0 * p.acceleration * p.displacement);
        }

        return null;
    }

    // == FINAL SPEED ==
    // (1) v = u + at
    // (3) v = 2s/t - u
    // (4) v = sqrt(u^2 + 2as)
    // (5) v = (s + 0.5at^2) / t

    export function solveFinalSpeed(knowns: InitialSpeed & Acceleration & Time): number;

    export function solveFinalSpeed(knowns: Displacement & Time & InitialSpeed): number;

    export function solveFinalSpeed(knowns: InitialSpeed & Acceleration & Displacement): number;

    export function solveFinalSpeed(knowns: Displacement & Acceleration & Time): number;

    export function solveFinalSpeed(p: Partial<Displacement & InitialSpeed & FinalSpeed & Acceleration & Time>): number|null
    {
        // Discover which knowns we have
        const
            haveDisplacement = Is.number(p.displacement),
            haveInitialSpeed = Is.number(p.initialSpeed),
            haveFinalSpeed = Is.number(p.finalSpeed),
            haveAcceleration = Is.number(p.acceleration),
            haveTime = Is.number(p.time);
        const numKnowns =
            (haveDisplacement ? 1 : 0) +
            (haveInitialSpeed ? 1 : 0) +
            (haveFinalSpeed ? 1 : 0) +
            (haveAcceleration ? 1 : 0) +
            (haveTime ? 1 : 0);
        const numUnknowns = 5 - numKnowns;

        if (haveInitialSpeed && haveAcceleration && haveTime)
        {
            return p.initialSpeed + p.acceleration * p.time;
        }
        else if (haveDisplacement && haveTime && haveInitialSpeed)
        {
            return (2.0 * p.displacement) / p.time - p.initialSpeed;
        }
        else if (haveInitialSpeed && haveAcceleration && haveDisplacement)
        {
            return Math.sqrt(p.initialSpeed * p.initialSpeed + 2.0 * p.acceleration * p.displacement);
        }
        else if (haveDisplacement && haveAcceleration && haveTime)
        {
            return (p.displacement + 0.5 * p.acceleration * p.time * p.time) / p.time;
        }

        return null;
    }

    // == ACCELERATION ==
    // (1) a = (v - u) / t
    // (2) a = 2(s - ut) / t^2
    // (4) a = (v^2 - u^2) / 2s
    // (5) a = 2(vt - s) / t^2

    export function solveAcceleration(knowns: FinalSpeed & InitialSpeed & Time): number;

    export function solveAcceleration(knowns: Displacement & InitialSpeed & Time): number;

    export function solveAcceleration(knowns: FinalSpeed & InitialSpeed & Displacement): number;

    export function solveAcceleration(knowns: FinalSpeed & Time & Displacement): number;

    export function solveAcceleration(p: Partial<Displacement & InitialSpeed & FinalSpeed & Acceleration & Time>): number|null
    {
        // Discover which knowns we have
        const
            haveDisplacement = Is.number(p.displacement),
            haveInitialSpeed = Is.number(p.initialSpeed),
            haveFinalSpeed = Is.number(p.finalSpeed),
            haveAcceleration = Is.number(p.acceleration),
            haveTime = Is.number(p.time);
        const numKnowns =
            (haveDisplacement ? 1 : 0) +
            (haveInitialSpeed ? 1 : 0) +
            (haveFinalSpeed ? 1 : 0) +
            (haveAcceleration ? 1 : 0) +
            (haveTime ? 1 : 0);
        const numUnknowns = 5 - numKnowns;

        if (haveFinalSpeed && haveInitialSpeed && haveTime)
        {
            return (p.finalSpeed - p.initialSpeed) / p.time;
        }
        else if (haveDisplacement && haveInitialSpeed && haveTime)
        {
            return 2.0 * (p.displacement - p.initialSpeed * p.time) / (p.time * p.time);
        }
        else if (haveFinalSpeed && haveInitialSpeed && haveDisplacement)
        {
            return (p.finalSpeed * p.finalSpeed + p.initialSpeed * p.initialSpeed) / (2.0 * p.displacement);
        }
        else if (haveFinalSpeed && haveTime && haveDisplacement)
        {
            return 2.0 * (p.finalSpeed * p.time - p.displacement) / (p.time * p.time);
        }

        return null;
    }

    // == DISPLACEMENT ==
    // (2) s = ut + 0.5at^2
    // (3) s = 0.5(u + v)t
    // (4) s = (v^2 - u^2) / 2a
    // (5) s = vt - 0.5at^2

    export function solveDisplacement(knowns: InitialSpeed & Time & Acceleration): number;

    export function solveDisplacement(knowns: InitialSpeed & FinalSpeed & Time): number;

    export function solveDisplacement(knowns: FinalSpeed & InitialSpeed & Acceleration): number;

    export function solveDisplacement(knowns: FinalSpeed & Time & Acceleration): number;

    export function solveDisplacement(p: Partial<Displacement & InitialSpeed & FinalSpeed & Acceleration & Time>): number|null
    {
        // Discover which knowns we have
        const
            haveDisplacement = Is.number(p.displacement),
            haveInitialSpeed = Is.number(p.initialSpeed),
            haveFinalSpeed = Is.number(p.finalSpeed),
            haveAcceleration = Is.number(p.acceleration),
            haveTime = Is.number(p.time);
        const numKnowns =
            (haveDisplacement ? 1 : 0) +
            (haveInitialSpeed ? 1 : 0) +
            (haveFinalSpeed ? 1 : 0) +
            (haveAcceleration ? 1 : 0) +
            (haveTime ? 1 : 0);
        const numUnknowns = 5 - numKnowns;

        if (haveInitialSpeed && haveTime && haveAcceleration)
        {
            return (p.initialSpeed * p.time) + (0.5 * p.acceleration * p.time * p.time);
        }
        else if (haveInitialSpeed && haveFinalSpeed && haveTime)
        {
            return 0.5 * (p.initialSpeed + p.finalSpeed) * p.time;
        }
        else if (haveFinalSpeed && haveInitialSpeed && haveAcceleration)
        {
            return (p.finalSpeed * p.finalSpeed - p.initialSpeed * p.initialSpeed) / (2.0 * p.acceleration);
        }
        else if (haveFinalSpeed && haveTime && haveAcceleration)
        {
            return (p.finalSpeed * p.time) - (0.5 * p.acceleration * p.time * p.time);
        }

        return null;
    }
}