namespace RS.Math.Algebra
{
    /**
     * Finds x in the quadratic ax^2 + bx + c = 0.
     * Returns two values if there are solutions, one value if both solutions are equal, and null if there are no solutions.
     * @param a 
     * @param b 
     * @param c 
     */
    export function solveQuadratic(a: number, b: number, c: number): { a: number, b: number } | number | null
    {
        // x = (-b +- sqrt(b^2 - 4ac)) / 2a

        // Find the value inside the sqrt
        const insideSqrt = b * b - (4.0 * a * c);

        // If it's negative, there is no solution
        if (insideSqrt < 0) { return null; }

        // If it's zero, there is 1 solution
        if (insideSqrt === 0) { return -b / (2.0 * a); }

        // There are two solutions
        const sqrt = Math.sqrt(insideSqrt);
        return { a: (-b + sqrt) / (2.0 * a), b: (-b - sqrt) / (2.0 * a) };
    }
}