namespace RS.Math
{
    /**
     * Encapsulates a PID (proportional–integral–derivative) controller.
     * This attempts to minimise error between a measured value (process variable) and a desired value (set point) by applying some influence (control variable).
     * See https://en.wikipedia.org/wiki/PID_controller for more info.
     * This PID controller is not automatic and time must be advanced manually.
     */
    export class PIDController
    {
        protected _previousError: number;
        protected _previousProcessVariable: number;
        protected _integral: number;
        protected _setPoint: number;
        protected _processVariable: number;
        protected _controlVariable: number;

        /** Gets the current integral value. */
        public get integral() { return this._integral; }

        /** Gets the current error value. */
        public get error() { return this._previousError; }

        /** Gets or sets the set point (target value) of the PID controller. */
        public get setPoint() { return this._setPoint; }
        public set setPoint(value) { this._setPoint = value; }

        /** Gets or sets the process variable (measured input value) of the PID controller. */
        public get processVariable() { return this._processVariable; }
        public set processVariable(value) { this._processVariable = value; }

        /** Gets the control variable (output value) of the PID controller. */
        public get controlVariable() { return this._controlVariable; }

        public constructor(public readonly settings: PIDController.Settings)
        {
            this.reset();
            this._setPoint = 0.0;
            this._controlVariable = 0.0;
            if (settings.initialProcessVariable != null)
            {
                this._processVariable = this._previousProcessVariable = settings.initialProcessVariable;
            }
            else
            {
                this._processVariable = this._previousProcessVariable = 0.0;
            }
        }

        /**
         * Advances the timestep and runs PID controller logic.
         * @param deltaTime ms
         * @returns the new control variable
         */
        public advance(deltaTime: number): number
        {
            const dt = deltaTime / 1000;
            let error: number;
            if (this.settings.angular)
            {
                error = Math.Angle.diff(this._processVariable, this._setPoint);
                if (Math.abs(error) > Math.PI && Math.abs(this._previousError) > Math.PI && Math.sign(error) !== Math.sign(this._previousError))
                {
                    // sign of error has flipped meaning we should change direction
                    this._integral *= -1.0;
                    this._previousError *= -1.0;
                }
            }
            else
            {
                error = this._setPoint - this._processVariable;
            }
            this._integral += error * dt;
            const derivative = (error - this._previousError) / dt;
            this._controlVariable = this.settings.Kp * error + this.settings.Ki * this._integral + this.settings.Kd * derivative;
            let backCalculateIntegral = false;
            if (this.settings.cvMin != null && this._controlVariable < this.settings.cvMin)
            {
                this._controlVariable = this.settings.cvMin;
                backCalculateIntegral = true;
            }
            else if (this.settings.cvMax != null && this._controlVariable > this.settings.cvMax)
            {
                this._controlVariable = this.settings.cvMax;
                backCalculateIntegral = true;
            }
            if (backCalculateIntegral && this.settings.Ki !== 0)
            {
                this._integral = (this._controlVariable - this.settings.Kp * error - this.settings.Kd * derivative) / this.settings.Ki;
            }
            this._previousError = error;
            this._previousProcessVariable = this._processVariable;
            return this._controlVariable;
        }

        /**
         * Resets the PID controller's memory (but keeps the setpoint and measured value).
         */
        public reset(): void
        {
            this._previousProcessVariable = this._processVariable;
            this._previousError = this.settings.initialError || 0;
            this._integral = this.settings.initialIntegral || 0;
        }
    }
    
    export namespace PIDController
    {
        export interface Settings
        {
            /** Proportional term */
            Kp: number;

            /** Integral term */
            Ki: number;

            /** Derivative term */
            Kd: number;

            /** Minimum value for control variable. If not set, no minimum is assumed. */
            cvMin?: number;

            /** Maximum value for control variable. If not set, no maximum is assumed. */
            cvMax?: number;

            /** If true, the process variable and set point are assumed to be angles in the range [-pi, pi]. */
            angular?: boolean;

            initialError?: number;
            initialIntegral?: number;
            initialProcessVariable?: number;
        }
    }
}