namespace RS.Math
{
    // Import math library
    export const
    {
        LN2, LN10, LOG2E, LOG10E, PI, SQRT1_2, SQRT2,
        abs, acos, asin, atan, atan2, cos, exp, log, max, min, pow, sin, sqrt, tan
    } = window["Math"] as Math;

    /**
     * Represents a color in RGB format.
     */
    export type Color = Util.ColorLike;

    const RADIAN = 180 / Math.PI;

    /**
     * Converts degrees to radians.
     * @param degrees   Degrees to be converted to radians
     * @returns         Radians
     */
    export function degreesToRadians(degrees: number): number
    {
        return degrees / RADIAN;
    }

    /**
     * Converts radians to degrees.
     * @param           Radians Radians to be converted to degrees
     * @returns         Degrees
     */
    export function radiansToDegrees(radians: number): number
    {
        return radians * RADIAN;
    }

    /**
     * Rounds a value to the specifed number of decimal places.
     * @param value
     * @param dp
     */
    export function round(value: number, dp: number): number;

    /**
     * Rounds a value to the nearest integer.
     * @param value
     * @param dp
     */
    export function round(value: number): number;

    export function round(value: number, dp: number = 0): number
    {
        if (dp === 0)
        {
            return floor(value + 0.5);
        }
        else
        {
            const factor = pow(10, dp);
            return floor((value * factor) + 0.5) / factor;
        }
    }

    /**
     * Shifts a decimal point by a specified number of spaces.
     * @param value           Number to remove the decimal point from.
     * @param placesToShift   Places to shift the decimal by. A negative number increases the decimal places, a positive number decreases the decimal places.
     **/
    export function shiftDecimal(value: number, placesToShift: number)
    {
        const originalValueString = value.toString()
        let originalDecimalIndex = originalValueString.indexOf(".");

        let valueString: string = null;

        // get a new string with no decimal in it
        if (originalDecimalIndex === -1)
        {
            //if the original string has no decimal in it, just use that and set the decimal index to the end of the string
            originalDecimalIndex = originalValueString.length;
            valueString = originalValueString;
        }
        else
        {
            valueString = originalValueString.slice(0, originalDecimalIndex) + originalValueString.slice(originalDecimalIndex + 1, originalValueString.length);
        }

        //get the new index where the decimal should be
        const newDecimalIndex = originalDecimalIndex + placesToShift;

        //if the new index puts us before the start of the number, add zeroes at the start of the number
        if (newDecimalIndex <= 0)
        {
            //if we're at index 0, we always need to add a 0.
            let extraZeroes = "0.";

            for (let i = 0; i > newDecimalIndex; i--)
            {
                extraZeroes += "0";
            }

            return parseFloat(extraZeroes + valueString);
        }
        //if the new index puts us after the e d of the number, add zeroes at the end of the number
        else if (newDecimalIndex >= valueString.length)
        {
            const overflow = newDecimalIndex - valueString.length;

            let extraZeroes = "";

            for (let i = 0; i < overflow; i++)
            {
                extraZeroes += "0";
            }

            return parseFloat(valueString + extraZeroes);
        }

        return parseFloat(valueString.slice(0, newDecimalIndex) + "." + valueString.slice(newDecimalIndex, valueString.length));
    }

    /**
     * Returns the largest integer lesser than or equal to a given number.
     * @param value
     * @param epsilon   Minimum amount that the number needs to be from the top integer before it can floor, ceils if less
     **/
    export function floor(value: number, epsilon?: number): number
    {
        if (epsilon != null && ceil(value) - value < epsilon)
        {
            return ceil(value);
        }
        else
        {
            return window["Math"].floor(value);
        }
    }

    /**
     * Returns the largest integer greater than or equal to a given number.
     * @param value
     * @param epsilon   Minimum amount that the number needs to be from the bottom integer before it can ceil, floors if less
     **/
    export function ceil(value: number, epsilon?: number): number
    {
        if (epsilon != null && value - floor(value) < epsilon)
        {
            return floor(value);
        }
        else
        {
            return window["Math"].ceil(value);
        }
    }

    /**
     * Generates a random number between min (inclusive) and max (exclusive).
     * @param minVal     Minimum value to return (inclusive)
     * @param maxVal     Maximum number to return (exclusive)
     * @param randInvert Randomly inverts the generated number between positive and negative
     * @returns     Randomly generated number between min and max
     **/
    export function generateRandomNumber(minVal: number, maxVal: number, randInvert: boolean = false): number
    {
        if (minVal >= maxVal)
        {
            throw new RangeError(`Minimum value (${minVal}) is greater than or equal to maximum value (${maxVal})`);
        }

        const diff = maxVal - minVal;
        const randSign = random() >= 0.5 ? 1.0 : -1.0;
        const invert = randInvert ? randSign : 1.0;
        const randNum = (minVal + (random() * diff)) * invert;

        return randNum;
    }

    /**
     * Generates a random integer between min (inclusive) and max (exclusive).
     * @param minVal     Minimum value to return (inclusive)
     * @param maxVal     Maximum number to return (exclusive)
     * @param randInvert Randomly inverts the generated number between positive and negative
     * @returns     Randomly generated number between min and max
     */
    export function generateRandomInt(minVal: number, maxVal: number, randInvert: boolean = false): number
    {
        const randNum = generateRandomNumber(minVal, maxVal, randInvert);
        return Math.floor(randNum);
    }

    /**
     * Clamps a number between a range, and returns the clamped value.
     * @param value     Value to clamp
     * @param minVal       Minimum value the number can be
     * @param maxVal       Maximum value the number can be
     * @returns         The clamped value
     */
    export function clamp(value: number, minVal: number, maxVal: number): number
    {
        if (minVal > maxVal)
        {
            throw new Error(`Maximum value (${maxVal}) is less than the minimum value (${minVal}).`);
        }
        return value > maxVal ? maxVal : value < minVal ? minVal : value;
    }

    /**
     * Wraps a number between a range, and returns the wrapped value.
     * @param value     Value to wrap
     * @param minVal       Minimum value the number can be
     * @param maxVal       Maximum value (exclusive)
     * @returns         The wrapped value.
     */
    export function wrap(value: number, minVal: number, maxVal: number): number
    {
        const range = maxVal - minVal;
        return (((value - minVal) % range) + range) % range + minVal;
    }

    /**
     * Gets the sign of a number, e.g. 1 for positive, -1 for negative, 0 for 0.
     * @param value
     */
    export function sign(value: number): number
    {
        return value < 0 ? -1 : value > 0 ? 1 : 0;
    }

    /**
     * Performs linear interpolation between numbers a and b by mu.
     * @param a     The first number
     * @param b     The second number
     * @param mu    The interpolant (0-1)
     */
    export function linearInterpolate(a: number, b: number, mu: number): number
    {
        return (b - a) * mu + a;
    }

    /**
     * Finds how between min and max value is.
     * 
     * e.g. if min=1 and max=3, value=2 would return 0.5, value=1 would return 0.0, value=3 would return 1.0
     * @param a minimum value
     * @param b maximum value
     * @param value 
     */
    export function linearUninterpolate(a: number, b: number, value: number): number
    {
        return (value - a) / (b - a);
    }

    /**
     * Finds the previous power of 2 for the specified value.
     */
    export function prevPowerOf2(v: number): number
    {
        return Math.pow(2, Math.floor(Math.log(v) / Math.log(2)));
    }

    /**
     * Finds the next power of 2 for the specified value.
     */
    export function nextPowerOf2(v: number): number
    {
        return Math.pow(2, Math.ceil(Math.log(v) / Math.log(2)));
    }

    let tm: MersenneTwister = null;

    /**
     * Generates a random number between 0-1.
     * @param maxInclusive Whether or not to include 1 as a possible result. Defaults to false.
     * @param minInclusive Whether or not to include 0 as a possible result. Defaults to true.
     */
    export function random(maxInclusive: boolean = false, minInclusive: boolean = true)
    {
        if (tm == null)
        {
            tm = new MersenneTwister();
        }
        if (maxInclusive)
        {
            return minInclusive ? tm.randomIncl() : 1 - tm.random();
        }
        else
        {
            return minInclusive ? tm.random() : tm.randomExcl();
        }
    }

    /**
     * Seeds the random number generator.
     * @param seed
     */
    export function seedRandom(seed: number)
    {
        if (tm)
        {
            tm.initSeed(seed);
        }
        else
        {
            tm = new MersenneTwister(seed);
        }
    }

    /**
     * Finds the positive modulo (e.g. -1.5 % 1 = -0.5, mod(-1.5, 1) = 0.5).
     */
    export function mod(a: number, n: number)
    {
        return a - Math.floor(a/n) * n;
    }

    /**
     * Finds the shortest signed angle between two angles (in radians).
     */
    export function diffAngle(a: number, b: number)
    {
        const dA = b - a;
        return this.mod(dA + Math.PI, Math.PI * 2.0) - Math.PI;
    }

    /**
     * Shuffles array in place.
     * @param arr The array containing the items.
     * @param rng Optional rng to use.
     */
    export function shuffle<T>(arr: T[], rng?: Math.MersenneTwister)
    {
        for (let i = arr.length; i; i--)
        {
            const j = Math.floor((rng != null ? rng.random() : Math.random()) * i);
            [arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];
        }
    }

    /**
     * Returns a section of a circular array.
     * For example, the slice (-1, 4) of the circular array [1, 2, 3] is [3, 1, 2, 3, 1].
     * Returns an empty array if end is less than or equal to start.
     * @param array The repeating section of the circular array.
     * @param start The beginning of the specified portion of the circular array.
     * @param end The end of the specified portion of the circular array.
     */
    export function sliceCircular<T>(array: ArrayLike<T>, start: number, end: number): T[]
    {
        if (array.length === 0) { return []; }
        const result = new Array<T>(Math.max(0, end - start));
        for (let i = start; i < end; i++)
        {
            const wrappedIndex = Math.wrap(i, 0, array.length);
            result[i - start] = array[wrappedIndex];
        }
        return result;
    }

    /**
     * Returns the base 2 logarithm of a number.
     * @param value
     */
    export function log2(x: number): number
    {
        return log(x) / log(2);
    }

    /**
     * Gets if the range described by minA-maxA intersects the range described by minB-maxB.
     * @param minA
     * @param maxA
     * @param minB
     * @param maxB
     */
    export function intersect1D(minA: number, maxA: number, minB: number, maxB: number): boolean
    {
        return !(maxA < minB || minA > maxB);
    }

    /**
     * Gets if the range described by minA-maxA is fully contained within the range described by minB-maxB.
     * Note that the ranges must be normalised (maxA must be >= minA, and maxB must be >= minB).
     * @param minA
     * @param maxA
     * @param minB
     * @param maxB
     */
    export function contains1D(minA: number, maxA: number, minB: number, maxB: number): boolean
    {
        return minA >= minB && minA <= maxB && maxA >= minB && maxA <= maxB;
    }

    /**
     * Gets if the range described by minA-maxA intersects the range described by minB-maxB, in a non-continuous coordinate space.
     * Note that the ranges must be normalised (maxA must be >= minA, and maxB must be >= minB).
     * @param minA
     * @param maxA
     * @param minB
     * @param maxB
     * @param minRange
     * @param maxRange
     */
    export function intersect1DWrapped(minA: number, maxA: number, minB: number, maxB: number, minRange: number, maxRange: number): boolean
    {
        // Edge-case - if either A or B spans the entire coordinate space, intersection is guaranteed
        const totalRange = maxRange - minRange;
        if (maxB - minB >= totalRange || maxA - minA >= totalRange) { return true; }

        // Wrap all ranges
        minA = Math.wrap(minA, minRange, maxRange);
        maxA = Math.wrap(maxA, minRange, maxRange);
        minB = Math.wrap(minB, minRange, maxRange);
        maxB = Math.wrap(maxB, minRange, maxRange);

        // Is range A inversed?
        if (maxA < minA)
        {
            // Is range B inversed?
            if (maxB < minB)
            {
                // A is inversed, B is inversed
                return true;
            }
            else
            {
                // A is inversed, B is normal
                return !contains1D(minB, maxB, maxA, minA);
            }
        }
        else
        {
            // Is range B inversed?
            if (maxB < minB)
            {
                // A is normal, B is inversed
                return !contains1D(minA, maxA, maxB, minB);
            }
            else
            {
                // A is normal, B is normal
                return intersect1D(minA, maxA, minB, maxB);
            }
        }
    }

    export namespace Angles
    {
        /**
         * Gets an angle (in radians) that is a certain fraction of a circle.
         * @param fract
         */
        export function circle(fract: number): number
        {
            return Math.PI * 2.0 * fract;
        }

        export const fullCircle = circle(1);
        export const halfCircle = circle(1 / 2);
        export const quarterCircle = circle(1 / 4);
        export const threeQuartersCircle = circle(3 / 4);
        export const eighthCircle = circle(1 / 8);
        export const thirdCircle = circle(1 / 3);
        export const twoThirdsCircle = circle(2 / 3);
    }

    /**
     * Returns a scale to fit one size within another.
     * @param from
     * @param to
     * @param mode
     * @param out
     */
    export function scale(from: Size2D, to: Size2D, mode: ScaleMode = ScaleMode.Stretch, out: Vector2D = Vector2D()): Math.Vector2D
    {
        if (mode === ScaleMode.Stretch)
        {
            out.x = to.w / from.w;
            out.y = to.h / from.h;
        }
        else
        {
            const xScale = to.w / from.w;
            const yScale = to.h / from.h;

            let useYAxis: boolean;
            switch (mode)
            {
                case ScaleMode.Contain:
                    useYAxis = yScale < xScale;
                    break;

                case ScaleMode.Cover:
                    useYAxis = yScale > xScale;
                    break;
            }

            out.x = out.y = useYAxis ? yScale : xScale;
        }
        return out;
    }

    export enum ScaleMode
    {
        /** Stretches the object to fit in both dimensions. */
        Stretch,
        /** Scales the object to fit its largest dimension. */
        Contain,
        /** Scales the object to fit its smallest dimension. */
        Cover
    }
}
