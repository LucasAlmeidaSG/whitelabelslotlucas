namespace RS.Math
{
    /**
     * Computes a hash of the specified number sequence.
     * @param seq 
     */
    export function hash(seq: number[]): number
    {
        // djb2
        let result = 5381;
        for (const value of seq)
        {
            result = (result << 5) + result + value;
        }
        return result;
    }
}