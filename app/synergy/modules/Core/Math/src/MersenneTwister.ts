namespace RS.Math
{
    /*
     A C-program for MT19937, with initialization improved 2002/1/26.
     Coded by Takuji Nishimura and Makoto Matsumoto.

     Modified to fit with RSCore methodologies by Thomas Smith 2015/30/11.

     Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
     All rights reserved.

     Redistribution and use in source and binary forms, with or without
     modification, are permitted provided that the following conditions
     are met:

     1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
     products derived from this software without specific prior written
     permission.

     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
     "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
     LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
     A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
     CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
     PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
     PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
     LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
     NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

     Any feedback is very welcome.
     http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
     email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
     */

    /**
     * A seeded random number generator based on the Mersenne Twister algorithm.
     */
    export class MersenneTwister
    {
        /**
         * The period parameter 'N'.
         **/
        private static N = 624;

        /**
         * The period parameter 'M'.
         **/
        private static M = 397;

        /**
         * The constant vector 'a'.
         **/
        private static MATRIX_A = 0x9908b0df;

        /**
         * The most significant w-r bits.
         **/
        private static UPPER_MASK = 0x80000000;

        /**
         * The least significant r bits.
         **/
        private static LOWER_MASK = 0x7fffffff;

        /**
         * The state vector array.
         **/
        private mt: number[];
        private mti: number;

        /**
         * Initialises a new instance of the MersenneTwister class.
         * @param seed    The seed.
         **/
        constructor(seed?: number)
        {
            // Create a seed if needed
            if (seed == null)
            {
                seed = new Date().getTime();
            }

            // Create the state vector array
            this.mt = new Array(MersenneTwister.N);
            this.mti = MersenneTwister.N + 1;
            /* mti==N+1 means mt[N] is not initialized */

            // Init the vector
            this.initSeed(seed);
        }

        /**
         * Initializes mt[N] with a seed.
         * @param s        The seed
         **/
        public initSeed(s: number)
        {
            this.mt[0] = s >>> 0;
            //let mti = this.mti;
            let mti: number;
            for (mti = 1; mti < MersenneTwister.N; mti++)
            {
                const sVal = this.mt[mti - 1] ^ (this.mt[mti - 1] >>> 30);
                this.mt[mti] = (((((sVal & 0xffff0000) >>> 16) * 1812433253) << 16) + (sVal & 0x0000ffff) * 1812433253)
                    + mti;
                /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
                /* In the previous versions, MSBs of the seed affect   */
                /* only MSBs of the array mt[].                        */
                /* 2002/01/09 modified by Makoto Matsumoto             */
                this.mt[mti] >>>= 0;
                /* for >32 bit machines */
            }
            this.mti = mti;
        }

        /**
         * Generates a random signed integer.
         * @returns    Number on [0,0xffffffff]-interval
         **/
        public randomInt()
        {
            let y;
            const mag01 = new Array(0x0, MersenneTwister.MATRIX_A);
            /* mag01[x] = x * MATRIX_A  for x=0,1 */

            if (this.mti >= MersenneTwister.N) /* generate N words at one time */
            {
                let kk;

                if (this.mti == MersenneTwister.N + 1)  /* if init_seed() has not been called, */
                {
                    this.initSeed(5489);
                    /* a default initial seed is used */
                }

                for (kk = 0; kk < MersenneTwister.N - MersenneTwister.M; kk++)
                {
                    y = (this.mt[kk] & MersenneTwister.UPPER_MASK) | (this.mt[kk + 1] & MersenneTwister.LOWER_MASK);
                    this.mt[kk] = this.mt[kk + MersenneTwister.M] ^ (y >>> 1) ^ mag01[y & 0x1];
                }
                for (; kk < MersenneTwister.N - 1; kk++)
                {
                    y = (this.mt[kk] & MersenneTwister.UPPER_MASK) | (this.mt[kk + 1] & MersenneTwister.LOWER_MASK);
                    this.mt[kk] = this.mt[kk + (MersenneTwister.M - MersenneTwister.N)] ^ (y >>> 1) ^ mag01[y & 0x1];
                }
                y = (this.mt[MersenneTwister.N - 1] & MersenneTwister.UPPER_MASK) | (this.mt[0] & MersenneTwister.LOWER_MASK);
                this.mt[MersenneTwister.N - 1] = this.mt[MersenneTwister.M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

                this.mti = 0;
            }

            y = this.mt[this.mti++];

            /* Tempering */
            y ^= (y >>> 11);
            y ^= (y << 7) & 0x9d2c5680;
            y ^= (y << 15) & 0xefc60000;
            y ^= (y >>> 18);

            return y >>> 0;
        }

        /**
         * Generates a random unsigned integer.
         * @returns    Number on [0,0x7fffffff]-interval
         **/
        public randomUInt()
        {
            return (this.randomInt() >>> 1);
        }

        /**
         * Generates a random decimal in the 0-1 range, inclusive of 1.
         * @returns    Number on [0,1]-real-interval
         **/
        public randomIncl()
        {
            return this.randomInt() * (1.0 / 4294967295.0);
            /* divided by 2^32-1 */
        }

        /**
         * Generates a random decimal in the 0-1 range, exclusive of 1.
         * @returns    Number on [0,1)-real-interval
         **/
        public random()
        {
            return this.randomInt() * (1.0 / 4294967296.0);
            /* divided by 2^32 */
        }

        /**
         * Generates a random decimal in the 0-1 range, exclusive of 0 and 1.
         * @returns    Number on (0,1)-real-interval
         **/
        public randomExcl()
        {
            return (this.randomInt() + 0.5) * (1.0 / 4294967296.0);
            /* divided by 2^32 */
        }

        /**
         * Generates a random integer between min and max (exclusive of max).
         * @param min    The lower bound
         * @param max    The upper bound
         * @returns    Number on [min,max)-interval
         **/
        public randomIntRange(min: number, max: number)
        {
            return Math.floor(this.random() * (max - min) + min);
        }

        /**
         * Generates a random decimal between min and max (exclusive of max).
         * @param min    The lower bound
         * @param max    The upper bound
         * @returns    Number on [min,max)-real-interval
         **/
        public randomRange(min: number, max: number)
        {
            return this.random() * (max - min) + min;
        }
        
        /**
         * Initialize by an array with array-length.
         * @param initKey        The array for initializing keys
         * @param keyLength        The length of the array
         **/

        private initByArray(initKey: number[], keyLength: number)
        {
            let i, j, k;
            this.initSeed(19650218);
            i = 1;
            j = 0;
            k = (MersenneTwister.N > keyLength ? MersenneTwister.N : keyLength);
            for (; k; k--)
            {
                const s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
                this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1664525) << 16) + ((s & 0x0000ffff) * 1664525)))
                    + initKey[j] + j;
                /* non linear */
                this.mt[i] >>>= 0;
                /* for WORDSIZE > 32 machines */
                i++;
                j++;
                if (i >= MersenneTwister.N)
                {
                    this.mt[0] = this.mt[MersenneTwister.N - 1];
                    i = 1;
                }
                if (j >= keyLength) {
                    j = 0;
                }
            }
            for (k = MersenneTwister.N - 1; k; k--)
            {
                const s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
                this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1566083941) << 16) + (s & 0x0000ffff) * 1566083941))
                    - i;
                /* non linear */
                this.mt[i] >>>= 0;
                /* for WORDSIZE > 32 machines */
                i++;
                if (i >= MersenneTwister.N)
                {
                    this.mt[0] = this.mt[MersenneTwister.N - 1];
                    i = 1;
                }
            }

            this.mt[0] = 0x80000000;
            /* MSB is 1; assuring non-zero initial array */
        }
    }
}
