namespace RS.Math.Endian
{
    /**
     * Swaps the endianness of a uint32.
     * @param value 
     */
    export function swapUInt32(value: number): number
    {
        return (value << 24)
            | ((value & 0x0000FF00) << 8)
            | ((value & 0x00FF0000) >> 8)
            | (value >> 24);
    }

    /**
     * Returns a copy of a uint32 array with the endianness of each element swapped.
     * @param value 
     * @param out 
     */
    export function swapUInt32Array(value: Uint32Array, out?: Uint32Array): Uint32Array
    {
        out = out || new Uint32Array(value.length);
        for (let i = 0, l = value.length; i < l; ++i)
        {
            out[i] = swapUInt32(value[i]);
        }
        return out;
    }

    /**
     * Swaps the endianness of a uint16.
     * @param value 
     */
    export function swapUInt16(value: number): number
    {
        return ((value << 8) | (value >> 8)) & 0xFFFF;
    }

    /**
     * Returns a copy of a uint16 array with the endianness of each element swapped.
     * @param value 
     * @param out 
     */
    export function swapUInt16Array(value: Uint16Array, out?: Uint16Array): Uint16Array
    {
        out = out || new Uint16Array(value.length);
        for (let i = 0, l = value.length; i < l; ++i)
        {
            out[i] = swapUInt16(value[i]);
        }
        return out;
    }

    const tmpArr = new ArrayBuffer(4);
    const tmpUint32ArrView = new Uint32Array(tmpArr, 0, 1);
    const tmpFloat32ArrView = new Float32Array(tmpArr, 0, 1);

    /**
     * Swaps the endianness of a float32.
     * @param value 
     */
    export function swapFloat32(value: number): number
    {
        tmpFloat32ArrView[0] = value;
        tmpUint32ArrView[0] = swapUInt32(tmpUint32ArrView[0]);
        return tmpFloat32ArrView[0];
    }

    /**
     * Returns a copy of a float32 array with the endianness of each element swapped.
     * @param value 
     * @param out 
     */
    export function swapFloat32Array(value: Float32Array, out?: Float32Array): Float32Array
    {
        out = out || new Float32Array(value.length);
        const inUint32View = new Uint32Array(value.buffer);
        const outUint32View = new Uint32Array(out.buffer);
        swapUInt32Array(inUint32View, outUint32View);
        return out;
    }
}