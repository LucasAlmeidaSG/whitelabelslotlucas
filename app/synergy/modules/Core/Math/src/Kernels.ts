namespace RS.Math
{
    const GAUSSIAN_KERNEL_SIZE_TO_OMEGA: number = 0.84089642 / 5.0; // don't ask

    export type Kernel = number[];

    /**
     * The Gaussian function.
     * @param omega     Radius/scale
     * @param x         Distance from origin
     */
    function gaussian(omega: number, x: number): number
    {
        const omega2 = omega * omega;
        return Math.exp(-(x * x) / (2 * omega2)) / Math.sqrt(2 * Math.PI * omega2);
    }

    /**
     * Creates a gaussian kernel with a given size.
     * @param size      The total size of the kernel (e.g. radius * 2 + 1)
     */
    export function createGaussianKernel(size: number): Kernel
    {
        const kernel = new Array(size);
        const midPoint = (size - 1) / 2;
        for (let i = 0; i < Math.ceil(size / 2); i++)
        {
            kernel[i] = gaussian(size * GAUSSIAN_KERNEL_SIZE_TO_OMEGA, midPoint - i);
        }

        for (let i = midPoint + 1; i < size; i++)
        {
            kernel[i] = kernel[2 * midPoint - i];
        }

        return kernel;
    }

    /**
     * Normalises the specified kernel such that the sum of all elements is 1.0.
     * @param kernel    The kernel to normalise. Modified in place.
     */
    export function normaliseKernel(kernel: Kernel): void
    {
        let sum = 0;

        for (let i = 0; i < kernel.length; i++)
        {
            sum += kernel[i];
        }

        for (let i = 0; i < kernel.length; i++)
        {
            kernel[i] /= sum;
        }
    }
}
