/// <reference path="Vector3D.ts" />

namespace RS.Is
{
    export function quaternion(value: any): value is Math.Quaternion
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y) && Is.number(value.z) && Is.number(value.w);
    }
}

namespace RS.Math
{
    const orthoNormal1 = Math.Vector3D(), orthoNormal2 = Math.Vector3D();

    /**
     * Encapsulates an immutable Quaternion.
     */
    export type ReadonlyQuaternion = ReadonlyVector4D;

    /**
     * Encapsulates a quaternion used for rotation in 3D space.
     */
    export type Quaternion = Vector4D;

    /** Creates a new quaternion. */
    export function Quaternion(x?: number, y?: number, z?: number, w?: number): Quaternion
    {
        return { x: x || 0, y: y || 0, z: z || 0, w: w || 0 };
    }

    export namespace Quaternion
    {
        /** Identity quaternion */
        export const identity: ReadonlyQuaternion = Quaternion(0, 0, 0, 1);

        const tmpVecs = [Vector3D(), Vector3D(), Vector3D(), Vector3D()];

        /** Creates a quaternion from an axis and an angle about that axis. */
        export function fromAxisAngle(axis: ReadonlyVector3D, angle: number, out?: Quaternion): Quaternion
        {
            out = out || Quaternion();

            const normalised = Vector3D.normalised(axis, tmpVecs[0]);
            const half = angle * 0.5;
            const s = sin(half);
            const c = cos(half);

            out.x = normalised.x * s;
            out.y = normalised.y * s;
            out.z = normalised.z * s;
            out.w = c;

            Quaternion.normalise(out);

            return out;
        }

        /** Creates a quaternion from an euler rotation. */
        export function fromEuler(pitch: number, yaw: number, roll: number, out?: Quaternion): Quaternion;

        /** Creates a quaternion from an euler rotation. */
        export function fromEuler(euler: ReadonlyEulerAngles, out?: Quaternion): Quaternion;

        /** Creates a quaternion from an euler rotation. */
        export function fromEuler(p1: number | EulerAngles, p2?: number | Quaternion, p3?: number, p4?: Quaternion): Quaternion
        {
            // Resolve overload
            let pitch: number, yaw: number, roll: number;
            let out: Quaternion;
            if (Is.number(p1))
            {
                pitch = p1;
                yaw = p2 as number;
                roll = p3 as number;
                out = p4 || Quaternion();
            }
            else
            {
                pitch = p1.pitch;
                yaw = p1.yaw;
                roll = p1.roll;
                out = p2 as Quaternion || Quaternion();
            }

            // Maths it
            const halfRoll = roll * 0.5;
            const sinRoll = sin(halfRoll);
            const cosRoll = cos(halfRoll);
            const halfPitch = pitch * 0.5;
            const sinPitch = sin(halfPitch);
            const cosPitch = cos(halfPitch);
            const halfYaw = yaw * 0.5;
            const sinYaw = sin(halfYaw);
            const cosYaw = cos(halfYaw);

            out.x = (cosYaw * sinPitch * cosRoll) + (sinYaw * cosPitch * sinRoll);
            out.y = (sinYaw * cosPitch * cosRoll) - (cosYaw * sinPitch * sinRoll);
            out.z = (cosYaw * cosPitch * sinRoll) - (sinYaw * sinPitch * cosRoll);
            out.w = (cosYaw * cosPitch * cosRoll) + (sinYaw * sinPitch * sinRoll);

            Quaternion.normalise(out);

            return out;
        }

        /** Creates a quaternion from a rotation matrix. The matrix should be orthogonal (e.g. no scale or shear). */
        export function fromMatrix(mtx: ReadonlyMatrix3, out?: Quaternion): Quaternion
        {
            out = out || Quaternion();
            const trace = mtx[0] + mtx[4] + mtx[8];
            if (trace > 0)
            {
                const s = 0.5 / Math.sqrt(trace + 1.0);
                out.w = 0.25 / s;
                out.x = (mtx[5] - mtx[7]) * s;
                out.y = (mtx[6] - mtx[2]) * s;
                out.z = (mtx[1] - mtx[3]) * s;
            }
            else if (mtx[0] > mtx[4] && mtx[0] > mtx[8])
            {
                const s = 2.0 * Math.sqrt(1.0 + mtx[0] - mtx[4] - mtx[8]);
                out.w = (mtx[5] - mtx[7]) / s;
                out.x = 0.25 * s;
                out.y = (mtx[3] + mtx[1]) / s;
                out.z = (mtx[6] + mtx[2]) / s;
            }
            else if (mtx[4] > mtx[8])
            {
                const s = 2.0 * Math.sqrt(1.0 + mtx[4] - mtx[0] - mtx[8]);
                out.w = (mtx[6] - mtx[2]) / s;
                out.x = (mtx[3] + mtx[1]) / s;
                out.y = 0.25 * s;
                out.z = (mtx[7] + mtx[5]) / s;
            }
            else
            {
                const s = 2.0 * Math.sqrt(1.0 + mtx[8] - mtx[0] - mtx[4]);
                out.w = (mtx[1] - mtx[3]) / s;
                out.x = (mtx[6] + mtx[2]) / s;
                out.y = (mtx[7] + mtx[5]) / s;
                out.z = 0.25 * s;
            }
            return out;
        }

        /** Decomposes a quaternion into an euler rotation. */
        export function toEuler(a: ReadonlyQuaternion, out?: EulerAngles): EulerAngles
        {
            out = out || EulerAngles();

            const t0 = 2.0 * (a.w * a.x + a.y * a.z);
            const t1 = 1.0 - 2.0 * (a.x * a.x + a.y * a.y);
            const X = atan2(t0, t1);

            let t2 = 2.0 * (a.w * a.y - a.z * a.x);
            if (t2 > 1.0)
            {
                t2 = 1.0;
            }
            else if (t2 < -1.0)
            {
                t2 = -1.0;
            }
            const Y = asin(t2);

            const t3 = 2.0 * (a.w * a.z + a.x * a.y);
            const t4 = 1.0 - 2.0 * (a.y * a.y + a.z * a.z);
            const Z = atan2(t3, t4);

            out.pitch = X;
            out.yaw = Y;
            out.roll = Z;
            return out;
        }

        /** Finds a rotation from "a" to "b". */
        export function fromTo(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Quaternion): Quaternion
        {
            out = out || Quaternion();

            const v0 = Vector3D.normalised(a, tmpVecs[0]);
            const v1 = Vector3D.normalised(b, tmpVecs[1]);

            const d = Vector3D.dot(v0, v1);
            if (d >= 1.0) // If dot == 1, vectors are the same
            {
                copy(identity, out);
                return out;
            }
            else if (d <= -1.0) // exactly opposite
            {
                let axis = Vector3D.cross(Vector3D.unitX, v0, tmpVecs[2]);
                if (Vector3D.size(axis) === 0.0)
                {
                    axis = Vector3D.cross(Vector3D.unitY, v0, tmpVecs[2]);
                }
                // same as fromAngleAxis(core::PI, axis).normalize();
                Vector3D.copy(axis, out);
                out.w = 0.0;
                Quaternion.normalise(out);
                return out;
            }

            const s = sqrt((1.0 + d) * 2.0); // optimize inv_sqrt
            const invs = 1.0 / s;
            Vector3D.cross(v0, v1, out);
            out.x *= invs;
            out.y *= invs;
            out.z *= invs;
            out.w = s * 0.5;
            Quaternion.normalise(out);
            return out;
        }

        /** Returns a * b. */
        export function mul(a: ReadonlyQuaternion, b: ReadonlyQuaternion, out?: Quaternion): Quaternion
        {
            out = out || Quaternion();
            const lx = a.x, ly = a.y, lz = a.z, lw = a.w, rx = b.x, ry = b.y, rz = b.z, rw = b.w;
            out.x = (rx * lw + lx * rw + ry * lz) - (rz * ly);
            out.y = (ry * lw + ly * rw + rz * lx) - (rx * lz);
            out.z = (rz * lw + lz * rw + rx * ly) - (ry * lx);
            out.w = (rw * lw) - (rx * lx + ry * ly + rz * lz);
            return out;
        }

        /**
         * Combines a set of quaternions into a single quaternion via multiplication (e.g. arr[0] * ... * arr[n]).
         * When used to rotate a point, the rotations will be applied in the same order.
         */
        export function combine(arr: ReadonlyQuaternion[], out?: Quaternion): Quaternion
        {
            out = out || Quaternion();
            copy(identity, out);
            const tmp = Quaternion();
            for (let i = 0, l = arr.length; i < l; ++i)
            {
                copy(out, tmp);
                mul(arr[i], tmp, out);
            }
            free(tmp);
            return out;
        }

        /**
         * Finds the rotation of a about an arbitrary axis.
         * @param axis 
         */
        export function findAboutAxis(a: ReadonlyQuaternion, axis: ReadonlyVector3D): number
        {
            // TODO: Optimise to be memory-neutral
            const plane = Plane(axis, 0.0); // cache-me
            Plane.findOrthoNormals(plane, orthoNormal1, orthoNormal2);

            const transformed = transform(a, orthoNormal1); // cache-me

            // Project transformed vector onto plane
            const d = Vector3D.dot(transformed, axis);
            const flattened = Vector3D(transformed.x - d * axis.x, transformed.y - d * axis.y, transformed.z - d * axis.z); // cache-me
            Vector3D.normalise(flattened);

            // Get angle between original vector and projected transform to get angle around normal
            return acos(Vector3D.dot(orthoNormal1, flattened));
        }

        /** Find the dot product of a and b. */
        export function dot(a: ReadonlyQuaternion, b: ReadonlyQuaternion): number
        {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        /** Transforms the specified vector using this quaternion. */
        export function transform(quaternion: ReadonlyQuaternion, vec: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();

            const uv = Vector3D.cross(quaternion, vec, tmpVecs[0]);
            uv.x *= 2.0;
            uv.y *= 2.0;
            uv.z *= 2.0;

            const uv2 = Vector3D.cross(quaternion, uv, tmpVecs[1]);

            out.x = vec.x + quaternion.w * uv.x + uv2.x;
            out.y = vec.y + quaternion.w * uv.y + uv2.y;
            out.z = vec.z + quaternion.w * uv.z + uv2.z;

            return out;
        }

        /** Performs spherical linear interpolation between a and b by mu. */
        export function slerp(a: ReadonlyQuaternion, b: ReadonlyQuaternion, mu: number, out?: Quaternion): Quaternion
        {
            out = out || Quaternion();

            let opposite: number;
            let inverse: number;
            let d = dot(a, b);
            let flag = false;

            if (d < 0.0)
            {
                flag = true;
                d = -d;
            }

            if (d > 0.999999)
            {
                inverse = 1.0 - mu;
                opposite = flag ? -mu : mu;
            }
            else
            {
                const acos = Math.acos(d);
                const invSin = 1.0 / sin(acos);

                inverse = sin((1.0 - mu) * acos) * invSin;
                opposite = flag ? (-sin(mu * acos) * invSin) : (sin(mu * acos) * invSin);
            }

            out.x = (inverse * a.x) + (opposite * b.x);
            out.y = (inverse * a.y) + (opposite * b.y);
            out.z = (inverse * a.z) + (opposite * b.z);
            out.w = (inverse * a.w) + (opposite * b.w);

            return out;
        }

        /** Sets a to be of unit length. */
        export function normalise(a: Quaternion): void
        {
            Vector4D.normalise(a);
        }

        /** Finds if a and b are equal, within a certain threshold. */
        export function equals(a: ReadonlyQuaternion, b: ReadonlyQuaternion, threshold: number = 0.001): boolean
        {
            if (abs(a.x - b.x) > threshold) { return false; }
            if (abs(a.y - b.y) > threshold) { return false; }
            if (abs(a.z - b.z) > threshold) { return false; }
            if (abs(a.w - b.w) > threshold) { return false; }
            return true;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyQuaternion, out?: Quaternion): Quaternion
        {
            out = out || Quaternion();
            out.x = a.x;
            out.y = a.y;
            out.z = a.z;
            out.w = a.w;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyQuaternion, b: Quaternion): void
        {
            b.x = a.x;
            b.y = a.y;
            b.z = a.z;
            b.w = a.w;
        }

        /** Sets the individual components of a. */
        export function set(a: Quaternion, x?: number, y?: number, z?: number, w?: number): void
        {
            if (x != null) { a.x = x; }
            if (y != null) { a.y = y; }
            if (z != null) { a.z = z; }
            if (w != null) { a.w = w; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyQuaternion, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyQuaternion, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyQuaternion, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.z;
            arr[offset++] = a.w;
        }

        /** Indicates that the specified quaternion instance is no longer required and may be reused. */
        export function free(vec: Quaternion): void
        {
            // TODO: Resource pool logic
        }
    }
}