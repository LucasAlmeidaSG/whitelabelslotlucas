/// <reference path="Vector3D.ts" />

namespace RS.Is
{
    export function vector4D(value: any): value is Math.Vector4D
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y) && Is.number(value.z) && Is.number(value.w);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 4D Cartesian vector.
     */
    export interface ReadonlyVector4D extends ReadonlyVector3D
    {
        readonly w: number;
    }

    /**
     * Encapsulates a 4D Cartesian vector.
     */
    export interface Vector4D extends Vector3D
    {
        w: number;
    }

    /** Creates a new Vector4D. */
    export function Vector4D(x?: number, y?: number, z?: number, w?: number): Vector4D;

    /** Creates a new Vector4D from a Vector3D and a w. */
    export function Vector4D(vec: Vector3D, w?: number): Vector4D;

    export function Vector4D(p1?: Vector3D | number, p2?: number, p3?: number, p4?: number): Vector4D
    {
        if (Is.nonPrimitive(p1))
        {
            return { x: p1.x, y: p1.y, z: p1.z, w: p2 || 0 };
        }
        else
        {
            return { x: p1 || 0, y: p2 || 0, z: p3 || 0, w: p4 || 0 };
        }
    }

    export namespace Vector4D
    {
        /** (0, 0, 0, 0) */
        export const zero: ReadonlyVector4D = Vector4D(0, 0, 0, 0);

        /** (1, 0, 0, 0) */
        export const unitX: ReadonlyVector4D = Vector4D(1, 0, 0, 0);

        /** (0, 1, 0, 0) */
        export const unitY: ReadonlyVector4D = Vector4D(0, 1, 0, 0);

        /** (0, 0, 1, 0) */
        export const unitZ: ReadonlyVector4D = Vector4D(0, 0, 1, 0);

        /** (0, 0, 0, 1) */
        export const unitW: ReadonlyVector4D = Vector4D(0, 0, 0, 1);

        /** Returns a + b. */
        export function add(a: ReadonlyVector4D, b: ReadonlyVector4D, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            out.x = a.x + b.x;
            out.y = a.y + b.y;
            out.z = a.z + b.z;
            out.w = a.w + b.w;
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlyVector4D, b: ReadonlyVector4D, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            out.x = a.x - b.x;
            out.y = a.y - b.y;
            out.z = a.z - b.z;
            out.w = a.w - b.w;
            return out;
        }

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector4D, b: ReadonlyVector4D, out?: Vector4D): Vector4D;

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector4D, b: number, out?: Vector4D): Vector4D;

        export function multiply(a: ReadonlyVector4D, b: ReadonlyVector4D | number, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            if (Is.number(b))
            {
                out.x = a.x * b;
                out.y = a.y * b;
                out.z = a.z * b;
                out.w = a.w * b;
            }
            else
            {
                out.x = a.x * b.x;
                out.y = a.y * b.y;
                out.z = a.z * b.z;
                out.w = a.w * b.w;
            }
            return out;
        }

        /** Returns a / b. */
        export function divide(a: ReadonlyVector4D, b: ReadonlyVector4D, out?: Vector4D): Vector4D;

        /** Returns a / b. */
        export function divide(a: ReadonlyVector4D, b: number, out?: Vector4D): Vector4D;

        export function divide(a: ReadonlyVector4D, b: ReadonlyVector4D | number, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            if (Is.number(b))
            {
                out.x = a.x / b;
                out.y = a.y / b;
                out.z = a.z / b;
                out.w = a.w / b;
            }
            else
            {
                out.x = a.x / b.x;
                out.y = a.y / b.y;
                out.z = a.z / b.z;
                out.w = a.w / b.w;
            }
            return out;
        }

        /** Find the squared magnitude of a. */
        export function sizeSq(a: ReadonlyVector4D): number
        {
            return a.x ** 2 + a.y ** 2 + a.z ** 2 + a.w ** 2;
        }

        /** Find the magnitude of a. */
        export function size(a: ReadonlyVector4D): number
        {
            return Math.sqrt(a.x ** 2 + a.y ** 2 + a.z ** 2 + a.w ** 2);
        }

        /** Returns a of unit length. */
        export function normalised(a: ReadonlyVector4D, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            const len = size(a);
            out.x = a.x / len;
            out.y = a.y / len;
            out.z = a.z / len;
            out.w = a.w / len;
            return out;
        }

        /** Sets a to be of unit length. */
        export function normalise(a: Vector4D): void
        {
            const len = size(a);
            a.x = a.x / len;
            a.y = a.y / len;
            a.z = a.z / len;
            a.w = a.w / len;
        }

        /** Returns a of newLength length. */
        export function withLength(a: ReadonlyVector4D, newLength: number, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            const len = size(a);
            out.x = (a.x / len) * newLength;
            out.y = (a.y / len) * newLength;
            out.z = (a.z / len) * newLength;
            out.w = (a.w / len) * newLength;
            return out;
        }

        /** Returns a of newLength length. */
        export function setLength(a: Vector4D, newLength: number): void
        {
            const len = size(a);
            a.x = (a.x / len) * newLength;
            a.y = (a.y / len) * newLength;
            a.z = (a.z / len) * newLength;
            a.w = (a.w / len) * newLength;
        }

        /** Linearly interpolates between two vectors. */
        export function linearInterpolate(a: ReadonlyVector4D, b: ReadonlyVector4D, mu: number, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            out.z = Math.linearInterpolate(a.z, b.z, mu);
            out.w = Math.linearInterpolate(a.w, b.w, mu);
            return out;
        }

        /** Finds if a and b are equal, within a certain threshold. */
        export function equals(a: ReadonlyVector4D, b: ReadonlyVector4D, threshold: number = 0.001): boolean
        {
            if (Math.abs(a.x - b.x) > threshold) { return false; }
            if (Math.abs(a.y - b.y) > threshold) { return false; }
            if (Math.abs(a.z - b.z) > threshold) { return false; }
            if (Math.abs(a.w - b.w) > threshold) { return false; }
            return true;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyVector4D, out?: Vector4D): Vector4D
        {
            out = out || Vector4D();
            out.x = a.x;
            out.y = a.y;
            out.z = a.z;
            out.w = a.w;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyVector4D, b: Vector4D): void
        {
            b.x = a.x;
            b.y = a.y;
            b.z = a.z;
            b.w = a.w;
        }

        /** Sets the individual components of a. */
        export function set(a: Vector4D, x?: number, y?: number, z?: number, w?: number): void
        {
            if (x != null) { a.x = x; }
            if (y != null) { a.y = y; }
            if (z != null) { a.z = z; }
            if (w != null) { a.w = w; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyVector4D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyVector4D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyVector4D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.z;
            arr[offset++] = a.w;
        }

        /** Indicates that the specified vector instance is no longer required and may be reused. */
        export function free(vec: Vector4D): void
        {
            // TODO: Resource pool logic
        }
    }
}