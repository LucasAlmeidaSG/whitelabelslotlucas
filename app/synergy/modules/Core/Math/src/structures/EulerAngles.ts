/// <reference path="../Math.ts"/>

namespace RS.Is
{
    export function eulerAngles(value: any): value is Math.EulerAngles
    {
        return Is.object(value) && Is.number(value.pitch) && Is.number(value.yaw) && Is.number(value.roll);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable set of euler angles (pitch, yaw, roll)
     */
    export interface ReadonlyEulerAngles
    {
        readonly pitch: number;
        readonly yaw: number;
        readonly roll: number;
    }

    /**
     * Encapsulates a set of euler angles (pitch, yaw, roll)
     */
    export interface EulerAngles extends ReadonlyEulerAngles
    {
        pitch: number;
        yaw: number;
        roll: number;
    }

    /** Creates a new set of euler angles with the values (0, 0, 0). */
    export function EulerAngles(): EulerAngles;

    /** Creates a new set of euler angles with the values (pitch, yaw, roll). */
    export function EulerAngles(pitch: number, yaw: number, roll: number): EulerAngles;

    export function EulerAngles(p1?: number, p2?: number, p3?: number): EulerAngles
    {
        return { pitch: p1 || 0, yaw: p2 || 0, roll: p3 || 0 };
    }

    export namespace EulerAngles
    {
        /** Returns a + b. */
        export function add(a: ReadonlyEulerAngles, b: ReadonlyEulerAngles, out?: EulerAngles): EulerAngles
        {
            out = out || EulerAngles();
            out.pitch = a.pitch + b.pitch;
            out.yaw = a.yaw + b.yaw;
            out.roll = a.roll + b.roll;
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlyEulerAngles, b: ReadonlyEulerAngles, out?: EulerAngles): EulerAngles
        {
            out = out || EulerAngles();
            out.pitch = a.pitch - b.pitch;
            out.yaw = a.yaw - b.yaw;
            out.roll = a.roll - b.roll;
            return out;
        }

        /** Linearly interpolates between two sets of euler angles. */
        export function linearInterpolate(a: ReadonlyEulerAngles, b: ReadonlyEulerAngles, mu: number, out?: EulerAngles): EulerAngles
        {
            out = out || EulerAngles();
            out.pitch = Math.linearInterpolate(a.pitch, b.pitch, mu);
            out.yaw = Math.linearInterpolate(a.yaw, b.yaw, mu);
            out.roll = Math.linearInterpolate(a.roll, b.roll, mu);
            return out;
        }

        /** Gets if two sets of euler angles are equal, optionally within a threshold. */
        export function equals(a: ReadonlyEulerAngles, b: ReadonlyEulerAngles, threshold: number = 0.0): boolean
        {
            const diffP = Math.abs(b.pitch - a.pitch);
            const diffY = Math.abs(b.yaw - a.yaw);
            const diffR = Math.abs(b.roll - a.roll);
            return diffP <= threshold && diffY <= threshold && diffR <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyEulerAngles, out?: EulerAngles): EulerAngles
        {
            out = out || EulerAngles();
            out.pitch = a.pitch;
            out.yaw = a.yaw;
            out.roll = a.roll;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyEulerAngles, b: EulerAngles): void
        {
            b.pitch = a.pitch;
            b.yaw = a.yaw;
            b.roll = a.roll;
        }

        /** Sets the individual components of a. */
        export function set(a: EulerAngles, pitch?: number, yaw?: number, roll?: number): void
        {
            if (pitch != null) { a.pitch = pitch; }
            if (yaw != null) { a.yaw = yaw; }
            if (roll != null) { a.roll = roll; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyEulerAngles, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyEulerAngles, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyEulerAngles, arr: Float32Array|number[], offset: number = 0): void
        {
            arr[offset++] = a.pitch;
            arr[offset++] = a.yaw;
            arr[offset++] = a.roll;
        }
    }
}