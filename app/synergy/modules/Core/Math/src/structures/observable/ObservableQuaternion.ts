/// <reference path="../Quaternion.ts" />

namespace RS.Math
{
    /**
     * Encapsulates a mutable quaternion that can be watched for changes.
     */
    export interface IObservableQuaternion extends Quaternion
    {
        /** Published when any of the components of this quaternion have changed. */
        readonly onChanged: IEvent;

        /** Gets a native observable version of this quaternion. */
        readonly asObservable: IObservable<Quaternion>;

        /** Gets a native version of this quaternion. */
        readonly asQuaternion: Quaternion;

        /**
         * Sets new values of this quaternion.
         * @param from
         */
        set(from: ReadonlyQuaternion): void;

        /**
         * Sets new values of this quaternion.
         * @param newX 
         * @param newY 
         * @param newZ 
         * @param newW 
         */
        set(newX?: number, newY?: number, newZ?: number, newW?: number): void;
    }

    /**
     * Encapsulates a mutable quaternion that can be watched for changes.
     */
    export class ObservableQuaternion implements IObservableQuaternion
    {
        /** Published when any of the components of this quaternion have changed. */
        public readonly onChanged = createSimpleEvent();

        protected _x: number;
        protected _y: number;
        protected _z: number;
        protected _w: number;

        /** Gets or sets the x value of this quaternion. */
        public get x() { return this._x; }
        public set x(value)
        {
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the y value of this quaternion. */
        public get y() { return this._y; }
        public set y(value)
        {
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets or sets the z value of this quaternion. */
        public get z() { return this._z; }
        public set z(value)
        {
            this._z = value;
            this.onChanged.publish();
        }

        /** Gets or sets the w value of this quaternion. */
        public get w() { return this._w; }
        public set w(value)
        {
            this._w = value;
            this.onChanged.publish();
        }

        /** Gets a native observable version of this quaternion. */
        public get asObservable(): Observable<Quaternion>
        {
            const o = new Observable<Quaternion>(Quaternion.clone(this));
            this.onChanged(() => o.value = Quaternion.clone(this));
            return o;
        }

        /** Gets a native version of this quaternion. */
        public get asQuaternion(): Quaternion
        {
            return Quaternion.clone(this);
        }

        public constructor(initialValue: ReadonlyQuaternion);

        public constructor(x: number, y: number, z: number, w: number);

        public constructor();

        public constructor(p1?: ReadonlyQuaternion | number, p2?: number, p3?: number, p4?: number)
        {
            if (Is.number(p1))
            {
                this._x = p1;
                this._y = p2;
                this._z = p3;
                this._w = p4;
            }
            else if (p1 != null)
            {
                this._x = p1.x;
                this._y = p1.y;
                this._z = p1.z;
                this._w = p1.w;
            }
            else
            {
                this._x = this._y = this._z = this._w = 0;
            }
        }

        /**
         * Sets new values of this quaternion.
         * @param from 
         */
        public set(from: ReadonlyQuaternion): void;

        /**
         * Sets new values of this quaternion.
         * @param newX 
         * @param newY 
         * @param newZ 
         * @param newW 
         */
        public set(newX?: number, newY?: number, newZ?: number, newW?: number): void;

        public set(p1?: ReadonlyQuaternion | number, p2?: number, p3?: number, p4?: number): void
        {
            if (Is.nonPrimitive(p1))
            {
                this._x = p1.x;
                this._y = p1.y;
                this._z = p1.z;
                this._w = p1.w;
                this.onChanged.publish();
            }
            else
            {
                let didChange = false;
                if (p1 != null) { this._x = p1; didChange = true; }
                if (p2 != null) { this._y = p2; didChange = true; }
                if (p3 != null) { this._z = p3; didChange = true; }
                if (p4 != null) { this._w = p4; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
        }
    }
}