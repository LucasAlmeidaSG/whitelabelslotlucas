/// <reference path="../Vector3D.ts" />

namespace RS.Math
{
    /**
     * Encapsulates a mutable 4D vector that can be watched for changes.
     */
    export interface IObservableVector4D extends Vector4D
    {
        /** Published when any of the components of this vector have changed. */
        readonly onChanged: IEvent;

        /** Gets a native observable version of this vector. */
        readonly asObservable: IObservable<Vector4D>;

        /** Gets a native version of this vector. */
        readonly asVector: Vector4D;

        /**
         * Sets new values of this 4D vector.
         * @param from
         */
        set(from: Vector4D): void;

        /**
         * Sets new values of this 4D vector.
         * @param newX 
         * @param newY 
         * @param newZ 
         * @param newW 
         */
        set(newX?: number, newY?: number, newZ?: number, newW?: number): void;
    }

    /**
     * Encapsulates a mutable 4D vector that can be watched for changes.
     */
    export class ObservableVector4D implements IObservableVector4D
    {
        /** Published when any of the components of this vector have changed. */
        public readonly onChanged = createSimpleEvent();

        protected _x: number;
        protected _y: number;
        protected _z: number;
        protected _w: number;

        /** Gets or sets the x value of this vector. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the y value of this vector. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets or sets the z value of this vector. */
        public get z() { return this._z; }
        public set z(value)
        {
            if (this._z === value) { return; }
            this._z = value;
            this.onChanged.publish();
        }

        /** Gets or sets the w value of this vector. */
        public get w() { return this._w; }
        public set w(value)
        {
            if (this._w === value) { return; }
            this._w = value;
            this.onChanged.publish();
        }

        /** Gets a native observable version of this vector. */
        public get asObservable(): IObservable<Vector4D>
        {
            const o = new Observable<Vector4D>(Vector4D.clone(this));
            this.onChanged(() => o.value = Vector4D.clone(this));
            return o;
        }

        /** Gets a native version of this vector. */
        public get asVector(): Vector4D
        {
            return Vector4D.clone(this);
        }

        public constructor(initialValue: ReadonlyVector4D);

        public constructor(x: number, y: number, z: number, w: number);

        public constructor();

        public constructor(p1?: ReadonlyVector4D | number, p2?: number, p3?: number, p4?: number)
        {
            if (Is.number(p1))
            {
                this._x = p1;
                this._y = p2;
                this._z = p3;
                this._w = p4;
            }
            else if (p1 != null)
            {
                this._x = p1.x;
                this._y = p1.y;
                this._z = p1.z;
                this._w = p1.w;
            }
            else
            {
                this._x = this._y = this._z = this._w = 0;
            }
        }

        /**
         * Sets new values of this 4D vector.
         * @param from 
         */
        public set(from: ReadonlyVector4D): void;

        /**
         * Sets new values of this 4D vector.
         * @param newX 
         * @param newY 
         * @param newZ 
         * @param newW 
         */
        public set(newX?: number, newY?: number, newZ?: number, newW?: number): void;

        public set(p1?: ReadonlyVector4D | number, p2?: number, p3?: number, p4?: number): void
        {
            if (Is.nonPrimitive(p1))
            {
                let didChange = false;
                if (this._x !== p1.x) { this._x = p1.x; didChange = true; }
                if (this._y !== p1.y) { this._y = p1.y; didChange = true; }
                if (this._z !== p1.z) { this._z = p1.z; didChange = true; }
                if (this._w !== p1.w) { this._w = p1.w; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
            else
            {
                let didChange = false;
                if (p1 != null && p1 !== this._x) { this._x = p1; didChange = true; }
                if (p2 != null && p2 !== this._y) { this._y = p2; didChange = true; }
                if (p3 != null && p3 !== this._z) { this._z = p3; didChange = true; }
                if (p4 != null && p4 !== this._w) { this._w = p4; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
        }
    }
}