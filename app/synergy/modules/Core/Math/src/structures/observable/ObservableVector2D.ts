/// <reference path="../Vector3D.ts" />

namespace RS.Math
{
    /**
     * Encapsulates a mutable 2D vector that can be watched for changes.
     */
    export interface IObservableVector2D extends Vector2D
    {
        /** Published when any of the components of this vector have changed. */
        readonly onChanged: IEvent;

        /** Gets a native observable version of this vector. */
        readonly asObservable: IObservable<Vector2D>;

        /** Gets a native version of this vector. */
        readonly asVector: Vector2D;

        /**
         * Sets new values of this 2D vector.
         * @param from
         */
        set(from: ReadonlyVector2D): void;

        /**
         * Sets new values of this 2D vector.
         * @param newX 
         * @param newY 
         */
        set(newX?: number, newY?: number): void;
    }

    /**
     * Encapsulates a mutable 2D vector that can be watched for changes.
     */
    export class ObservableVector2D implements IObservableVector2D
    {
        /** Published when any of the components of this vector have changed. */
        public readonly onChanged = createSimpleEvent();

        protected _x: number;
        protected _y: number;

        /** Gets or sets the x value of this vector. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the y value of this vector. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets a native observable version of this vector. */
        public get asObservable(): IObservable<Vector2D>
        {
            const o = new Observable<Vector2D>(Vector2D.clone(this));
            this.onChanged(() => o.value = Vector2D.clone(this));
            return o;
        }

        /** Gets a native version of this vector. */
        public get asVector(): Vector2D
        {
            return Vector2D.clone(this);
        }

        public constructor(initialValue: ReadonlyVector2D);

        public constructor(x: number, y: number);

        public constructor();

        public constructor(p1?: ReadonlyVector2D | number, p2?: number)
        {
            if (Is.number(p1))
            {
                this._x = p1;
                this._y = p2;
            }
            else if (p1 != null)
            {
                this._x = p1.x;
                this._y = p1.y;
            }
            else
            {
                this._x = this._y = 0;
            }
        }

        /**
         * Sets new values of this 2D vector.
         * @param from
         */
        public set(from: ReadonlyVector2D): void;

        /**
         * Sets new values of this 2D vector.
         * @param newX 
         * @param newY 
         */
        public set(newX?: number, newY?: number): void;

        public set(p1?: ReadonlyVector2D | number, p2?: number): void
        {
            if (Is.nonPrimitive(p1))
            {
                let didChange = false;
                if (this._x !== p1.x) { this._x = p1.x; didChange = true; }
                if (this._y !== p1.y) { this._y = p1.y; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
            else
            {
                let didChange = false;
                if (p1 != null && p1 !== this._x) { this._x = p1; didChange = true; }
                if (p2 != null && p2 !== this._y) { this._y = p2; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
        }
    }
}