/// <reference path="../Vector3D.ts" />

namespace RS.Math
{
    /**
     * Encapsulates a mutable plane that can be watched for changes.
     */
    export interface IObservablePlane extends Plane
    {
        /** Published when any of the components of this plane have changed. */
        readonly onChanged: IEvent;

        /** Gets a native observable version of this plane. */
        readonly asObservable: IObservable<Plane>;

        /** Gets a native version of this plane. */
        readonly asPlane: Plane;

        /**
         * Sets new values of this plane.
         * @param from
         */
        set(from: Plane): void;

        /**
         * Sets new values of this planer.
         * @param newX 
         * @param newY 
         * @param newZ 
         * @param newW 
         */
        set(newX?: number, newY?: number, newZ?: number, newW?: number): void;
    }

    /**
     * Encapsulates a mutable plane that can be watched for changes.
     */
    export class ObservablePlane implements IObservablePlane
    {
        /** Published when any of the components of this plane have changed. */
        public readonly onChanged = createSimpleEvent();

        protected _x: number;
        protected _y: number;
        protected _z: number;
        protected _w: number;

        /** Gets or sets the x value of this plane. */
        public get x() { return this._x; }
        public set x(value)
        {
            if (this._x === value) { return; }
            this._x = value;
            this.onChanged.publish();
        }

        /** Gets or sets the y value of this vector. */
        public get y() { return this._y; }
        public set y(value)
        {
            if (this._y === value) { return; }
            this._y = value;
            this.onChanged.publish();
        }

        /** Gets or sets the z value of this plane. */
        public get z() { return this._z; }
        public set z(value)
        {
            if (this._z === value) { return; }
            this._z = value;
            this.onChanged.publish();
        }

        /** Gets or sets the w value of this plane. */
        public get w() { return this._w; }
        public set w(value)
        {
            if (this._w === value) { return; }
            this._w = value;
            this.onChanged.publish();
        }

        /** Gets a native observable version of this plane. */
        public get asObservable(): IObservable<Plane>
        {
            const o = new Observable<Plane>(Vector4D.clone(this));
            this.onChanged(() => o.value = Vector4D.clone(this));
            return o;
        }

        /** Gets a native version of this plane. */
        public get asPlane(): Plane
        {
            return Plane.clone(this);
        }

        public constructor(initialValue: ReadonlyPlane);

        public constructor(x: number, y: number, z: number, w: number);

        public constructor();

        public constructor(p1?: ReadonlyPlane | number, p2?: number, p3?: number, p4?: number)
        {
            if (Is.number(p1))
            {
                this._x = p1;
                this._y = p2;
                this._z = p3;
                this._w = p4;
            }
            else if (p1 != null)
            {
                this._x = p1.x;
                this._y = p1.y;
                this._z = p1.z;
                this._w = p1.w;
            }
            else
            {
                this._x = this._y = this._z = this._w = 0;
            }
        }

        /**
         * Sets new values of this plane.
         * @param from 
         */
        public set(from: ReadonlyPlane): void;

        /**
         * Sets new values of this plane.
         * @param newX 
         * @param newY 
         * @param newZ 
         * @param newW 
         */
        public set(newX?: number, newY?: number, newZ?: number, newW?: number): void;

        public set(p1?: ReadonlyPlane | number, p2?: number, p3?: number, p4?: number): void
        {
            if (Is.nonPrimitive(p1))
            {
                let didChange = false;
                if (this._x !== p1.x) { this._x = p1.x; didChange = true; }
                if (this._y !== p1.y) { this._y = p1.y; didChange = true; }
                if (this._z !== p1.z) { this._z = p1.z; didChange = true; }
                if (this._w !== p1.w) { this._w = p1.w; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
            else
            {
                let didChange = false;
                if (p1 != null && p1 !== this._x) { this._x = p1; didChange = true; }
                if (p2 != null && p2 !== this._y) { this._y = p2; didChange = true; }
                if (p3 != null && p3 !== this._z) { this._z = p3; didChange = true; }
                if (p4 != null && p4 !== this._w) { this._w = p4; didChange = true; }
                if (didChange) { this.onChanged.publish(); }
            }
        }
    }
}