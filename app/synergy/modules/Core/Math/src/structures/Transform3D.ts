/// <reference path="../Math.ts"/>
/// <reference path="Vector3D.ts" />
/// <reference path="Quaternion.ts" />
/// <reference path="Matrix4.ts" />

namespace RS.Is
{
    export function transform3D(value: any): value is Math.Transform3D
    {
        return Is.object(value) &&
            Is.number(value.x) && Is.number(value.y) && Is.number(value.z) &&
            Is.number(value.rx) && Is.number(value.ry) && Is.number(value.rz) && Is.number(value.rw) &&
            Is.number(value.sx) && Is.number(value.sy) && Is.number(value.sz);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable transform in 3D space.
     */
    export interface ReadonlyTransform3D
    {
        readonly x: number; readonly y: number; readonly z: number;
        readonly rx: number; readonly ry: number; readonly rz: number; readonly rw: number;
        readonly sx: number; readonly sy: number; readonly sz: number;
    }

    /**
     * Encapsulates a transform in 3D space.
     */
    export interface Transform3D extends ReadonlyTransform3D
    {
        x: number; y: number; z: number;
        rx: number; ry: number; rz: number; rw: number;
        sx: number; sy: number; sz: number;
    }

    /** Creates a new 3D transform with the specified properties. */
    export function Transform3D(position?: ReadonlyVector3D, rotation?: ReadonlyQuaternion, scale?: ReadonlyVector3D): Transform3D
    {
        return {
            x: position ? position.x : 0.0, y: position ? position.y : 0.0, z: position ? position.z : 0.0,
            rx: rotation ? rotation.x : 0.0, ry: rotation ? rotation.y : 0.0, rz: rotation ? rotation.z : 0.0, rw: rotation ? rotation.w : 1.0,
            sx: scale ? scale.x : 1.0, sy: scale ? scale.y : 1.0, sz: scale ? scale.z : 1.0
        };
    }

    /**
     * Convenience methods for Transform3D math.
     */
    export namespace Transform3D
    {
        const tmpVec1 = Vector3D(), tmpVec2 = Vector3D();
        const tmpQuat1 = Quaternion(), tmpQuat2 = Quaternion(), tmpQuat3 = Quaternion();
        const transformStack = [Matrix4(), Matrix4(), Matrix4()];

        /** The identity transform. */
        export const identity: ReadonlyTransform3D = Transform3D(Vector3D.zero, Quaternion.identity, Vector3D.one);

        /** Converts a transform to a matrix. */
        export function toMatrix(a: ReadonlyTransform3D, out?: Matrix4): Matrix4
        {
            tmpQuat1.x = a.rx;
            tmpQuat1.y = a.ry;
            tmpQuat1.z = a.rz;
            tmpQuat1.w = a.rw;
            Matrix4.createScale(a.sx, a.sy, a.sz, transformStack[0]);
            Matrix4.createRotation(tmpQuat1, transformStack[1]);
            Matrix4.createTranslation(a.x, a.y, a.z, transformStack[2]);
            return Matrix4.combine(transformStack, out);
        }

        /** Converts a matrix to a transform. */
        export function fromMatrix(a: ReadonlyMatrix4, out?: Transform3D): Transform3D
        {
            out = out || Transform3D();
            Matrix4.decompose(a, tmpVec1, tmpQuat1, tmpVec2);
            set(out, tmpVec1, tmpQuat1, tmpVec2);
            return out;
        }

        /** Decomposes a transform into position, rotation and scale as individual structures. */
        export function decompose(a: ReadonlyTransform3D, outPos?: Vector3D, outRot?: Quaternion, outScale?: Vector3D): void
        {
            if (outPos)
            {
                outPos.x = a.x;
                outPos.y = a.y;
                outPos.z = a.z;
            }
            if (outRot)
            {
                outRot.x = a.rx;
                outRot.y = a.ry;
                outRot.z = a.rz;
                outRot.w = a.rw;
            }
            if (outScale)
            {
                outScale.x = a.sx;
                outScale.y = a.sy;
                outScale.z = a.sz;
            }
        }

        /** Returns the position component of a transform */
        export function getPosition(a: ReadonlyTransform3D): Vector3D
        {
            return Vector3D(a.x, a.y, a.z);
        }

        /** Returns the rotation component of a transform */
        export function getRotation(a: ReadonlyTransform3D): Quaternion
        {
            return Quaternion(a.rx, a.ry, a.rz, a.rw);
        }

        /** Returns the scale component of a transform */
        export function getScale(a: ReadonlyTransform3D): Vector3D
        {
            return Vector3D(a.sx, a.sy, a.sz);
        }

        /** Linearly interpolates between two transforms. */
        export function linearInterpolate(a: ReadonlyTransform3D, b: ReadonlyTransform3D, mu: number, out?: Transform3D): Transform3D
        {
            out = out || Transform3D();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            out.z = Math.linearInterpolate(a.z, b.z, mu);
            out.sx = Math.linearInterpolate(a.sx, b.sx, mu);
            out.sy = Math.linearInterpolate(a.sy, b.sy, mu);
            out.sz = Math.linearInterpolate(a.sz, b.sz, mu);
            tmpQuat1.x = a.rx;
            tmpQuat1.y = a.ry;
            tmpQuat1.z = a.rz;
            tmpQuat1.w = a.rw;
            tmpQuat2.x = b.rx;
            tmpQuat2.y = b.ry;
            tmpQuat2.z = b.rz;
            tmpQuat2.w = b.rw;
            Quaternion.slerp(tmpQuat1, tmpQuat2, mu, tmpQuat3);
            out.rx = tmpQuat3.x;
            out.ry = tmpQuat3.y;
            out.rz = tmpQuat3.z;
            out.rw = tmpQuat3.w;
            return out;
        }

        /** Gets if two transforms are equal, optionally within a threshold. */
        export function equals(a: ReadonlyTransform3D, b: ReadonlyTransform3D, threshold: number = 0.0): boolean
        {
            return abs(b.x - a.x) <= threshold
                && abs(b.y - a.y) <= threshold
                && abs(b.z - a.z) <= threshold
                && abs(b.rx - a.rx) <= threshold
                && abs(b.ry - a.ry) <= threshold
                && abs(b.rz - a.rz) <= threshold
                && abs(b.rw - a.rw) <= threshold
                && abs(b.sx - a.sx) <= threshold
                && abs(b.sy - a.sy) <= threshold
                && abs(b.sz - a.sz) <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyTransform3D, out?: Transform3D): Transform3D
        {
            if (out)
            {
                copy(a, out);
            }
            else
            {
                out = { ...a };
            }
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyTransform3D, b: Transform3D): void
        {
            b.x = a.x;
            b.y = a.y;
            b.z = a.z;
            b.rx = a.rx;
            b.ry = a.ry;
            b.rz = a.rz;
            b.rw = a.rw;
            b.sx = a.sx;
            b.sy = a.sy;
            b.sz = a.sz;
        }

        /** Sets the individual components of a. */
        export function set(a: Transform3D, newPos?: ReadonlyVector3D, newRot?: ReadonlyQuaternion, newScale?: ReadonlyVector3D): void
        {
            if (newPos != null)
            {
                a.x = newPos.x;
                a.y = newPos.y;
                a.z = newPos.z;
            }
            if (newRot != null)
            {
                a.rx = newRot.x;
                a.ry = newRot.y;
                a.rz = newRot.z;
                a.rw = newRot.w;
            }
            if (newScale != null)
            {
                a.sx = newScale.x;
                a.sy = newScale.y;
                a.sz = newScale.z;
            }
        }
    }
}