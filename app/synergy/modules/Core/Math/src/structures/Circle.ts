/// <reference path="../Math.ts"/>
/// <reference path="Vector2D.ts" />
/// <reference path="Line2D.ts" />

namespace RS.Is
{
    export function circle(value: any): value is Math.Circle
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y) && Is.number(value.r);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 2D circle.
     */
    export interface ReadonlyCircle
    {
        readonly x: number;
        readonly y: number;
        readonly r: number;
    }

    /**
     * Encapsulates a 2D circle.
     */
    export interface Circle extends ReadonlyCircle
    {
        x: number;
        y: number;
        r: number;
    }

    /** Creates a new 2D circle with the values (0, 0). */
    export function Circle(): Circle;

    /** Creates a new 2D vector with the values (x, y). */
    export function Circle(x: number, y: number, r: number): Circle;

    /** Creates a new 2D vector with the values (cos(angle), sin(angle)). */
    export function Circle(p1?: number, p2?: number, p3?: number): Circle
    {
        if (Is.number(p1))
        {
            return { x: p1, y: p2, r: p3 };
        }
        else
        {
            return { x: 0, y: 0, r: 0 };
        }
    }

    /**
     * Convenience methods for 2D circle math.
     */
    export namespace Circle
    {
        /** Determines if a contains pt. */
        export function contains(a: ReadonlyCircle, pt: ReadonlyVector2D): boolean
        {
            const d2 = (pt.x - a.x) ** 2 + (pt.y - a.y) ** 2;
            return d2 <= a.r ** 2;
        }

        /** Finds the area of a. */
        export function area(a: ReadonlyCircle): number
        {
            return Math.PI * a.r ** 2;
        }

        /** Finds the circumference of a. */
        export function circumference(a: ReadonlyCircle): number
        {
            return 2.0 * Math.PI * a.r;
        }

        /**
         * Finds a circle that contains the curve starting at origin and covering a certain angle over a certain distance.
         * @param origin 
         * @param angle 
         * @param distance 
         * @param out 
         */
        export function findCurve(origin: ReadonlyVector2D, initialAngle: number, curveAngle: number, distance: number, out?: Circle): Circle
        {
            out = out || Circle();
            out.r = Math.abs(distance / curveAngle);
            const u = Math.sqrt(out.r ** 2 - (distance * 0.5) ** 2) * Math.sign(curveAngle);
            const s = Math.sin(initialAngle + curveAngle);
            const c = Math.cos(initialAngle + curveAngle);
            out.x = origin.x + s * distance * 0.5 + c * u;
            out.y = origin.y + c * distance * 0.5 - s * u;
            return out;
        }

        /**
         * Projects a point onto the circle.
         * @param a 
         * @param pt 
         * @param out 
         */
        export function project(a: ReadonlyCircle, pt: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            const dx = pt.x - a.x;
            const dy = pt.y - a.y;
            const len = Math.sqrt(dx ** 2 + dy ** 2);
            out.x = a.x + (dx / len) * a.r;
            out.y = a.y + (dy / len) * a.r;
            return out;
        }

        /** Linearly interpolates between two circles. */
        export function linearInterpolate(a: ReadonlyCircle, b: ReadonlyCircle, mu: number, out?: Circle): Circle
        {
            out = out || Circle();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            out.r = Math.linearInterpolate(a.r, b.r, mu);
            return out;
        }

        /** Gets if two vectors are equal, optionally within a threshold. */
        export function equals(a: ReadonlyCircle, b: ReadonlyCircle, threshold: number = 0.0): boolean
        {
            const diffX = Math.abs(b.x - a.x);
            const diffY = Math.abs(b.y - a.y);
            const diffR = Math.abs(b.r - a.r);
            return diffX <= threshold && diffY <= threshold && diffR <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyCircle, out?: Circle): Circle
        {
            out = out || Circle();
            out.x = a.x;
            out.y = a.y;
            out.r = a.r;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyCircle, b: Circle): void
        {
            b.x = a.x;
            b.y = a.y;
            b.r = a.r;
        }

        /** Sets the individual components of a. */
        export function set(a: Circle, x?: number, y?: number, r?: number): void
        {
            if (x != null) { a.x = x; }
            if (y != null) { a.y = y; }
            if (r != null) { a.r = r; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyCircle, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyCircle, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyCircle, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.r;
        }

        /** Indicates that the specified circle instance is no longer required and may be reused. */
        export function free(vec: Circle): void
        {
            // TODO: Resource pool logic
        }
    }
}
