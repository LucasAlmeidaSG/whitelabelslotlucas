/// <reference path="../Math.ts"/>
/// <reference path="Vector4D.ts" />

namespace RS.Is
{
    export function line2D(value: any): value is Math.Line2D
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y) && Is.number(value.dx) && Is.number(value.dy);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable line in 2D space.
     */
    export interface ReadonlyLine2D
    {
        readonly x: number;
        readonly y: number;
        readonly dx: number;
        readonly dy: number;
    }

    /**
     * Encapsulates a line in 2D space.
     */
    export interface Line2D extends ReadonlyLine2D
    {
        x: number;
        y: number;
        dx: number;
        dy: number;
    }

    /** Creates an undefined 2D line. */
    export function Line2D(): Line2D;

    /** Creates a new 2D line with the specified point and direction. */
    export function Line2D(point: Math.ReadonlyVector2D, direction: Math.ReadonlyVector2D): Line2D;

    /** Creates a new 2D line with the specified point and direction. */
    export function Line2D(x: number, y: number, dx: number, dy: number): Line2D;

    export function Line2D(p1?: ReadonlyVector2D | number, p2?: ReadonlyVector2D | number, p3?: number, p4?: number): Line2D
    {
        if (Is.vector2D(p1))
        {
            const d = p2 as ReadonlyVector2D;
            return { x: p1.x, y: p1.y, dx: d.x, dy: d.y };
        }
        else if (p1 != null)
        {
            return { x: p1, y: p2 as number, dx: p3, dy: p4 };
        }
        else
        {
            return { x: 0, y: 0, dx: 0, dy: 0 };
        }
    }

    /**
     * Convenience methods for Line2D math.
     */
    export namespace Line2D
    {
        const tmpVec1 = Math.Vector2D(), tmpVec2 = Math.Vector2D();

        /** Retrieves a.d[idx]. */
        export function getDirComponent(a: ReadonlyLine2D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.dx;
                case 1: return a.dy;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a.d[idx]. */
        export function setDirComponent(a: Line2D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.dx = value;
                case 1: return a.dy = value;
                default: throw new Error("Invalid index");
            }
        }

        /**
         * Gets the direction of the specified line.
         * @param line
         * @param out
         */
        export function direction(line: ReadonlyLine2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = line.dx;
            out.y = line.dy;
            return out;
        }

        /**
         * Finds a line that passes through two points.
         * @param a
         * @param b
         * @param out
         */
        export function fromPoints(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Line2D): Line2D
        {
            out = out || Line2D();
            Vector2D.copy(a, out);
            out.dx = b.x - a.x;
            out.dy = b.y - a.y;
            return out;
        }

        /**
         * Finds the shortest distance from the specified point to the line.
         * @param line
         * @param point
         */
        export function distanceToPoint(line: ReadonlyLine2D, point: ReadonlyVector2D): number
        {
            const projPt = project(line, point, tmpVec1);
            return Vector2D.distanceBetween(projPt, point);
        }

        /**
         * Projects the specified point onto the line.
         * @param line
         * @param point
         * @param out
         */
        export function project(line: ReadonlyLine2D, point: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            const ap = Vector2D.subtract(point, line, tmpVec1);
            const ab = direction(line, tmpVec2);
            const apDab = Vector2D.dot(ap, ab);
            const abDab = Vector2D.dot(ab, ab);
            Vector2D.addMultiply(line, ab, apDab / abDab, out);
            return out;
        }

        /**
         * Projects the specified point onto the line, returning a multiple of the line's direction.
         * @param line
         * @param point
         */
        export function projectLambda(line: ReadonlyLine2D, point: ReadonlyVector2D): number
        {
            const ap = Vector2D.subtract(point, line, tmpVec1);
            const ab = direction(line, tmpVec2);
            const apDab = Vector2D.dot(ap, ab);
            const abDab = Vector2D.dot(ab, ab);
            return apDab / abDab;
        }

        /** Linearly interpolates between two lines. */
        export function linearInterpolate(a: ReadonlyLine2D, b: ReadonlyLine2D, mu: number, out?: Line2D): Line2D
        {
            out = out || Line2D();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            out.dx = Math.linearInterpolate(a.dx, b.dx, mu);
            out.dy = Math.linearInterpolate(a.dy, b.dy, mu);
            return out;
        }

        /** Gets if two lines are equal, optionally within a threshold. */
        export function equals(a: ReadonlyLine2D, b: ReadonlyLine2D, threshold: number = 0.0): boolean
        {
            return abs(b.x - a.x) <= threshold
                && abs(b.y - a.y) <= threshold
                && abs(b.dx - a.dx) <= threshold
                && abs(b.dy - a.dy) <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyLine2D, out?: Line2D): Line2D
        {
            if (out)
            {
                out.x = a.x;
                out.y = a.y;
                out.dx = a.dx;
                out.dy = a.dy;
            }
            else
            {
                out = Line2D(a.x, a.y, a.dx, a.dy);
            }
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyLine2D, b: Line2D): void
        {
            b.x = a.x;
            b.y = a.y;
            b.dx = a.dx;
            b.dy = a.dy;
        }

        /** Sets the individual components of a. */
        export function set(a: Line2D, point?: Math.ReadonlyVector2D, dir?: Math.ReadonlyVector2D): void
        {
            if (point)
            {
                a.x = point.x;
                a.y = point.y;
            }
            if (dir)
            {
                a.dx = dir.x;
                a.dy = dir.y;
            }
        }

        /** Returns whether or not the two lines are parallel. */
        export function isParallel(a: ReadonlyLine2D, b: ReadonlyLine2D): boolean
        {
            return b.dy * a.dx - a.dy * b.dx === 0;
        }

        /**
         * Returns the intersection point of the two lines.
         * If the two lines do not intersect (i.e. are parallel), both dimensions of the resulting vector will be NaN.
         */
        export function intersection(a: ReadonlyLine2D, b: ReadonlyLine2D, out: Vector2D = Vector2D()): Vector2D
        {
            const determinant = b.dy * a.dx - a.dy * b.dx;
            if (determinant === 0)
            {
                out.x = out.y = NaN;
            }
            else
            {
                const aC = a.dy * a.x - a.dx * a.y;
                const bC = b.dy * b.x - b.dx * b.y;

                const factor = 1 / determinant;
                out.x = (a.dx * bC - b.dx * aC) * factor;
                out.y = (a.dy * bC - b.dy * aC) * factor;
            }

            return out;
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyLine2D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyLine2D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyLine2D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.dx;
            arr[offset++] = a.dy;
        }
    }
}
