namespace RS.Math
{
    /**
     * Meant as a fast, chainable, in-place implementation of Vector2D.
     */
    export class Vec2 implements Vector2D
    {
        public get x() { return this._x; }
        public set x(val)
        {
            this._x = val;
            this._sizeChanged = true;
        }
        
        public get y() { return this._y; }
        public set y(val)
        {
            this._y = val;
            this._sizeChanged = true;
        }

        protected _size: number;
        /**
         * @returns The size (norm) of the vector
         */
        public get size()
        {
            if (this._sizeChanged)
            {
                this._size = Math.sqrt(this._x * this._x + this._y * this._y);
                this._sizeChanged = false;
            }
            return this._size;
        };
        protected _sizeChanged: boolean = false;

        /**
         * @returns The angle of the vector in the plane (in radiants)
         */
        public get angle(): number
        {
            return Math.atan2(this._y, this._x);
        }

        constructor(protected _x: number, protected _y: number)
        {
            this._size = Math.sqrt(this._x * this._x + this._y * this._y);
        }

        /**
         * Creates a vector with the given components
         * @param x The x component of the vector
         * @param y The y component of the vector
         * @returns The created vector
         */
        public static create(x: number, y: number): Vec2
        {
            return new Vec2(x, y);
        }

        /**
         * Creates and returns the vector (0, 0)
         * @returns The created vector
         */
        public static get zero(): Vec2
        {
            return new Vec2(0, 0);
        }

        /**
         * Returns the addition of the two given vectors
         * @param first The first vector to add
         * @param second The second vector to add
         * @returns The addition of the first and second vector
         */
        public static add(first: Vec2, second: Vec2): Vec2
        {
            return first.clone().add(second);
        }

        /**
         * Returns the result of subtracting the first vector with the second vector
         * @param first The first vector
         * @param second The second vector
         * @returns The subtraction of the first vector and the second vector
         */
        public static subtract(first: Vec2, second: Vec2): Vec2
        {
            return first.clone().subtract(second);
        }

        /**
         * Returns the multiplication of a vector and a multiplier
         * @param vec The vector
         * @param multiplier The multiplier
         * @returns Copy of the vector that has been multiplied
         */
        public static multiply(vec: Vec2, multiplier: number): Vec2
        {
            return vec.clone().multiply(multiplier);
        }

        /**
         * Returns the division of a vector and a divider
         * @param vec The vector
         * @param divider The divider
         * @returns The vector divided by the divider
         */
        public static divide(vec: Vec2, divider: number): Vec2
        {
            return vec.clone().divide(divider);
        }

        /**
         * Returns the dot product of the two given vectors
         * @param first The first vector
         * @param second The second vector
         * @returns The dot product of the first and second vectors
         */
        public static dot(first: Vec2, second: Vec2): number
        {
            return first.dot(second);
        }

        /**
         * Returns the rotation of the given vector by the given angle
         * @param vec The vector to rotate
         * @param angle The rotation angle (in radiants)
         * @returns The rotated vector
         */
        public static rotate(vec: Vec2, angle: number): Vec2
        {
            return vec.clone().rotate(angle);
        }

        /**
         * Returns the wedge product of two vectors
         * @param first The first vector
         * @param second The second vector
         * @returns the wedge product of the vectors
         */
        public static wedge(first: Vec2, second: Vec2): number
        {
            return first.wedge(second);
        }

        /**
         * Returns the projection of the first vector onto the second vector
         * @param first The first vector
         * @param second The second vector
         * @returns Th projection of the first vector onto the second vector
         */
        public static project(first: Vec2, second: Vec2)
        {
            return Vec2.multiply(second, first.dot(second) / second.dot(second));
        }

        /**
         * Sets the components of the vector
         * @x The x component
         * @y The y component
         * @returns The current vector
         */
        public set(x: number, y: number): Vec2;
        /**
         * Sets the components of the vector to match the given vector
         * @vec The vector to match
         * @returns The current vector
         */
        public set(vec: Vec2): Vec2
        public set(p1?: number | Vec2, p2?: number | undefined): Vec2
        {
            if (p1 instanceof Vec2)
            {
                this._x = p1._x;
                this._y = p1._y;
            }
            else if (typeof p1 === "number" && typeof p2 === "number")
            {
                this._x = p1;
                this._y = p2;
            }
            this._sizeChanged = true;
            return this;
        }

        /**
         * Creates and returns a copy of the current vector
         * @returns A copy of the current vector
         */
        public clone(): Vec2
        {
            return new Vec2(this._x, this._y);
        }

        /**
         * Adds a vector to the current vector
         * @param vec The vector to add
         * @returns The current vector
         */
        public add(vec: Vector2D): Vec2
        {
            this._x += vec.x;
            this._y += vec.y;
            this._sizeChanged = true;
            return this;
        }

        /**
         * Subtracts a vector from the current vector
         * @param vec The vector to subtract
         * @returns The current vector
         */
        public subtract(vec: Vector2D): Vec2
        {
            this._x -= vec.x;
            this._y -= vec.y;
            this._sizeChanged = true;
            return this;
        }

        /**
         * Multiplies the current vector by a multiplier
         * @param vec The multiplier that will scale each vector component
         * @returns The current vector
         */
        public multiply(multiplier: number): Vec2
        {
            this._x *= multiplier;
            this._y *= multiplier;
            this._sizeChanged = true;
            return this;
        }

        /**
         * Divides the current vector by a divider
         * @param vec The divider that will divide each vector component
         * @returns The current vector
         */
        public divide(divider: number): Vec2
        {
            this._x /= divider;
            this._y /= divider;
            this._sizeChanged = true;
            return this;
        }

        /**
         * Normalises the current vector
         * @returns The current vector
         */
        public normalise(): Vec2
        {
            this.divide(this._size);
            this._size = 1;
            this._sizeChanged = false;
            return this;
        }

        /**
         * Returns a normalised copy of the current vector
         * @returns A normalised copy of the vector
         */
        public normalised(): Vec2
        {
            return this.clone().normalise();
        }

        /**
         * Returns the dot product of the current vector with another vector
         * @param vec The other vector
         * @returns The dot product
         */
        public dot(vec: Vec2): number
        {
            return this._x * vec._x + this._y * vec._y;
        }

        /**
         * Rotates the vector by the given angle
         * @param angle The rotation angle (in radiants)
         * @returns The current vector
         */
        public rotate(angle: number): Vec2
        {
            const cosVal = Math.cos(angle);
            const sinVal = Math.sin(angle);

            const x = this._x;
            const y = this._y;

            this._x = x * cosVal - y * sinVal;
            this._y = x * sinVal + y * cosVal;

            return this;
        }

        /**
         * Returns the wedge product of this vector with the given vector
         * @param vec The other vector
         * @returns the wedge product of the vectors
         */
        public wedge(vec: Vec2): number
        {
            return this._x * vec._y - this._y * vec._x;
        }

        /**
         * Projects the current vector onto the given vector
         * @param vec The vector on which the current vector will be projected
         * @returns The current vector
         */
        public project(vec: Vec2): Vec2
        {
            return this.set(Vec2.project(this, vec));
        }
    }
}