/// <reference path="Vector2D.ts" />
/// <reference path="Matrix3.ts" />

namespace RS.Is
{
    export function aabb2D(value: any): value is Math.AABB2D
    {
        return Is.object(value) && Is.number(value.minX) && Is.number(value.minY) && Is.number(value.maxX) && Is.number(value.maxY);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 2D axis-aligned bounding box.
     */
    export interface ReadonlyAABB2D
    {
        readonly minX: number, readonly minY: number;
        readonly maxX: number, readonly maxY: number;
    }

    /**
     * Encapsulates an 2D axis-aligned bounding box.
     */
    export interface AABB2D extends ReadonlyAABB2D
    {
        minX: number, minY: number;
        maxX: number, maxY: number;
    }

    /** Creates a new AABB. */
    export function AABB2D(): AABB2D;

    /** Creates a new AABB from a min and max vector. */
    export function AABB2D(min: Vector2D, max: Vector2D): AABB2D;

    export function AABB2D(min?: Vector2D, max?: Vector2D): AABB2D
    {
        if (min && max)
        {
            return {
                minX: min.x, minY: min.y,
                maxX: max.x, maxY: max.y
            };
        }
        else
        {
            return {
                minX: 0, minY: 0,
                maxX: 0, maxY: 0
            };
        }
    }

    export namespace AABB2D
    {
        /** Retrieves a.min[idx]. */
        export function getMinComponent(a: ReadonlyAABB2D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.minX;
                case 1: return a.minY;
                default: throw new Error("Invalid index");
            }
        }

        /** Retrieves a.max[idx]. */
        export function getMaxComponent(a: ReadonlyAABB2D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.maxX;
                case 1: return a.maxY;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a.min[idx]. */
        export function setMinComponent(a: AABB2D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.minX = value;
                case 1: return a.minY = value;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a.max[idx]. */
        export function setMaxComponent(a: AABB2D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.maxX = value;
                case 1: return a.maxY = value;
                default: throw new Error("Invalid index");
            }
        }
    
        /** Returns a + b. */
        export function add(a: ReadonlyAABB2D, b: ReadonlyAABB2D, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            out.minX = a.minX + b.minX;
            out.minY = a.minY + b.minY;
            out.maxX = a.maxX + b.maxX;
            out.maxY = a.maxY + b.maxY;
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlyAABB2D, b: ReadonlyAABB2D, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            out.minX = a.minX - b.minX;
            out.minY = a.minY - b.minY;
            out.maxX = a.maxX - b.maxX;
            out.maxY = a.maxY - b.maxY;
            return out;
        }

        /** Returns a * b. */
        export function multiply(a: ReadonlyAABB2D, b: ReadonlyAABB2D, out?: AABB2D): AABB2D;

        /** Returns a * b. */
        export function multiply(a: ReadonlyAABB2D, b: number, out?: AABB2D): AABB2D;

        export function multiply(a: ReadonlyAABB2D, b: ReadonlyAABB2D | number, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            if (Is.number(b))
            {
                out.minX = a.minX * b;
                out.minY = a.minY * b;
                out.maxX = a.maxX * b;
                out.maxY = a.maxY * b;
            }
            else
            {
                out.minX = a.minX * b.minX;
                out.minY = a.minY * b.minY;
                out.maxX = a.maxX * b.maxX;
                out.maxY = a.maxY * b.maxY;
            }
            return out;
        }

        /** Returns a / b. */
        export function divide(a: ReadonlyAABB2D, b: ReadonlyAABB2D, out?: AABB2D): AABB2D;

        /** Returns a / b. */
        export function divide(a: ReadonlyAABB2D, b: number, out?: AABB2D): AABB2D;

        export function divide(a: ReadonlyAABB2D, b: ReadonlyAABB2D | number, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            if (Is.number(b))
            {
                out.minX = a.minX / b;
                out.minY = a.minY / b;
                out.maxX = a.maxX / b;
                out.maxY = a.maxY / b;
            }
            else
            {
                out.minX = a.minX / b.minX;
                out.minY = a.minY / b.minY;
                out.maxX = a.maxX / b.maxX;
                out.maxY = a.maxY / b.maxY;
            }
            return out;
        }

        /** Finds an AABB that encloses a point cloud. */
        export function fromPoints(points: ArrayLike<Vector2D>, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            for (let i = 0, l = points.length; i < l; ++i)
            {
                const pt = points[i];
                if (i === 0)
                {
                    set(out, pt, pt);
                }
                else
                {
                    expand(out, pt);
                }
            }
            return out;
        }

        /** Expands the AABB to enclose the specified point. */
        export function expand(a: AABB2D, pt: ReadonlyVector2D): void
        {
            a.minX = min(a.minX, pt.x);
            a.minY = min(a.minY, pt.y);
            a.maxX = max(a.maxX, pt.x);
            a.maxY = max(a.maxY, pt.y);
        }

        /** Tests if a and b intersect. */
        export function intersects(a: ReadonlyAABB2D, b: ReadonlyAABB2D): boolean
        {
            return intersect1D(a.minX, a.maxX, b.minX, b.maxX) && intersect1D(a.minY, a.maxY, b.minY, b.maxY);
        }

        /** Gets all 4 corners of a. */
        export function corners(a: ReadonlyAABB2D, out?: Vector2D[]): Vector2D[]
        {
            out = out || [];
            out.length = 4;
            const v0 = out[0] = out[0] || Vector2D();
            const v1 = out[1] = out[1] || Vector2D();
            const v2 = out[2] = out[2] || Vector2D();
            const v3 = out[3] = out[3] || Vector2D();
            v0.x = a.minX; v0.y = a.minY;
            v1.x = a.minX; v1.y = a.minY;
            v2.x = a.minX; v2.y = a.maxY;
            v3.x = a.minX; v3.y = a.maxY;
            return out;
        }

        const tmpCorners = new Array<Vector2D>(4);
        const tmpVecs: Vector2D[] = [ Vector2D(), Vector2D(), Vector2D(), Vector2D() ];

        /** Transforms a through mtx. */
        export function transform(a: ReadonlyAABB2D, mtx: Matrix3, out?: AABB2D): AABB2D
        {
            corners(a, tmpCorners);
            Matrix3.transform(mtx, tmpCorners[0], tmpVecs[0]);
            Matrix3.transform(mtx, tmpCorners[1], tmpVecs[1]);
            Matrix3.transform(mtx, tmpCorners[2], tmpVecs[2]);
            Matrix3.transform(mtx, tmpCorners[3], tmpVecs[3]);
            return fromPoints(tmpVecs, out);
        }

        /** Find the size of a. */
        export function size(a: ReadonlyAABB2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = a.maxX - a.minX;
            out.y = a.maxY - a.minY;
            return out;
        }

        /** Returns a with corrected min and max. */
        export function normalised(a: ReadonlyAABB2D, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            out.minX = min(a.minX, a.maxX);
            out.minY = min(a.minY, a.maxY);
            out.maxX = max(a.minX, a.maxX);
            out.maxY = max(a.minY, a.maxY);
            return out;
        }

        /** Sets a to have corrected min and max. */
        export function normalise(a: AABB2D): void
        {
            const minX = min(a.minX, a.maxX);
            const minY = min(a.minY, a.maxY);
            const maxX = max(a.minX, a.maxX);
            const maxY = max(a.minY, a.maxY);
            a.minX = minX;
            a.minY = minY;
            a.maxX = maxX;
            a.maxY = maxY;
        }

        /** Linearly interpolates between two AABBs. */
        export function linearInterpolate(a: ReadonlyAABB2D, b: ReadonlyAABB2D, mu: number, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            out.minX = Math.linearInterpolate(a.minX, b.minX, mu);
            out.minY = Math.linearInterpolate(a.minY, b.minY, mu);
            out.maxX = Math.linearInterpolate(a.maxX, b.maxX, mu);
            out.maxY = Math.linearInterpolate(a.maxY, b.maxY, mu);
            return out;
        }

        /** Finds if a and b are equal, within a certain threshold. */
        export function equals(a: ReadonlyAABB2D, b: ReadonlyAABB2D, threshold: number = 0.001): boolean
        {
            if (abs(a.minX - b.minX) > threshold) { return false; }
            if (abs(a.minY - b.minY) > threshold) { return false; }
            if (abs(a.maxX - b.maxX) > threshold) { return false; }
            if (abs(a.maxY - b.maxY) > threshold) { return false; }
            return true;
        }

        /** Determines if a has infinite extent. */
        export function isInfinite(a: ReadonlyAABB2D): boolean
        {
            return a.minX === -Infinity && a.minY === -Infinity && a.maxX === Infinity && a.maxY === Infinity;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyAABB2D, out?: AABB2D): AABB2D
        {
            out = out || AABB2D();
            out.minX = a.minX;
            out.minY = a.minY;
            out.maxX = a.maxX;
            out.maxY = a.maxY;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyAABB2D, b: AABB2D): void
        {
            b.minX = a.minX;
            b.minY = a.minY;
            b.maxX = a.maxX;
            b.maxY = a.maxY;
        }

        /** Sets the individual components of a. */
        export function set(a: AABB2D, min: Math.Vector2D, max: Math.Vector2D): void
        {
            a.minX = min.x;
            a.minY = min.y;
            a.maxX = max.x;
            a.maxY = max.y;
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyAABB2D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyAABB2D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyAABB2D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.minX;
            arr[offset++] = a.minY;
            arr[offset++] = a.maxX;
            arr[offset++] = a.maxY;
        }

        /** Indicates that the specified AABB instance is no longer required and may be reused. */
        export function free(vec: AABB2D): void
        {
            // TODO: Resource pool logic
        }
    }
}