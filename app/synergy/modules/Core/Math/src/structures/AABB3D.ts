/// <reference path="Vector3D.ts" />
/// <reference path="Matrix4.ts" />

namespace RS.Is
{
    export function aabb3D(value: any): value is Math.AABB3D
    {
        return Is.object(value) && Is.number(value.minX) && Is.number(value.minY) && Is.number(value.minZ) && Is.number(value.maxX) && Is.number(value.maxY) && Is.number(value.maxZ);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 3D axis-aligned bounding box.
     */
    export interface ReadonlyAABB3D
    {
        readonly minX: number, readonly minY: number, readonly minZ: number;
        readonly maxX: number, readonly maxY: number, readonly maxZ: number;
    }

    /**
     * Encapsulates an 3D axis-aligned bounding box.
     */
    export interface AABB3D extends ReadonlyAABB3D
    {
        minX: number, minY: number, minZ: number;
        maxX: number, maxY: number, maxZ: number;
    }

    /** Creates a new AABB. */
    export function AABB3D(): AABB3D;

    /** Creates a new AABB from a min and max vector. */
    export function AABB3D(min: Vector3D, max: Vector3D): AABB3D;

    export function AABB3D(min?: Vector3D, max?: Vector3D): AABB3D
    {
        if (min && max)
        {
            return {
                minX: min.x, minY: min.y, minZ: min.z,
                maxX: max.x, maxY: max.y, maxZ: max.z
            };
        }
        else
        {
            return {
                minX: 0, minY: 0, minZ: 0,
                maxX: 0, maxY: 0, maxZ: 0
            };
        }
    }

    export namespace AABB3D
    {
        /** Retrieves a.min[idx]. */
        export function getMinComponent(a: ReadonlyAABB3D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.minX;
                case 1: return a.minY;
                case 2: return a.minZ;
                default: throw new Error("Invalid index");
            }
        }

        /** Retrieves a.max[idx]. */
        export function getMaxComponent(a: ReadonlyAABB3D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.maxX;
                case 1: return a.maxY;
                case 2: return a.maxZ;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a.min[idx]. */
        export function setMinComponent(a: AABB3D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.minX = value;
                case 1: return a.minY = value;
                case 2: return a.minZ = value;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a.max[idx]. */
        export function setMaxComponent(a: AABB3D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.maxX = value;
                case 1: return a.maxY = value;
                case 2: return a.maxZ = value;
                default: throw new Error("Invalid index");
            }
        }
    
        /** Returns a + b. */
        export function add(a: ReadonlyAABB3D, b: ReadonlyAABB3D, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            out.minX = a.minX + b.minX;
            out.minY = a.minY + b.minY;
            out.minZ = a.minZ + b.minZ;
            out.maxX = a.maxX + b.maxX;
            out.maxY = a.maxY + b.maxY;
            out.maxZ = a.maxZ + b.maxZ;
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlyAABB3D, b: ReadonlyAABB3D, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            out.minX = a.minX - b.minX;
            out.minY = a.minY - b.minY;
            out.minZ = a.minZ - b.minZ;
            out.maxX = a.maxX - b.maxX;
            out.maxY = a.maxY - b.maxY;
            out.maxZ = a.maxZ - b.maxZ;
            return out;
        }

        /** Returns a * b. */
        export function multiply(a: ReadonlyAABB3D, b: ReadonlyAABB3D, out?: AABB3D): AABB3D;

        /** Returns a * b. */
        export function multiply(a: ReadonlyAABB3D, b: number, out?: AABB3D): AABB3D;

        export function multiply(a: ReadonlyAABB3D, b: ReadonlyAABB3D | number, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            if (Is.number(b))
            {
                out.minX = a.minX * b;
                out.minY = a.minY * b;
                out.minZ = a.minZ * b;
                out.maxX = a.maxX * b;
                out.maxY = a.maxY * b;
                out.maxZ = a.maxZ * b;
            }
            else
            {
                out.minX = a.minX * b.minX;
                out.minY = a.minY * b.minY;
                out.minZ = a.minZ * b.minZ;
                out.maxX = a.maxX * b.maxX;
                out.maxY = a.maxY * b.maxY;
                out.maxZ = a.maxZ * b.maxZ;
            }
            return out;
        }

        /** Returns a / b. */
        export function divide(a: ReadonlyAABB3D, b: ReadonlyAABB3D, out?: AABB3D): AABB3D;

        /** Returns a / b. */
        export function divide(a: ReadonlyAABB3D, b: number, out?: AABB3D): AABB3D;

        export function divide(a: ReadonlyAABB3D, b: ReadonlyAABB3D | number, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            if (Is.number(b))
            {
                out.minX = a.minX / b;
                out.minY = a.minY / b;
                out.minZ = a.minZ / b;
                out.maxX = a.maxX / b;
                out.maxY = a.maxY / b;
                out.maxZ = a.maxZ / b;
            }
            else
            {
                out.minX = a.minX / b.minX;
                out.minY = a.minY / b.minY;
                out.minZ = a.minZ / b.minZ;
                out.maxX = a.maxX / b.maxX;
                out.maxY = a.maxY / b.maxY;
                out.maxZ = a.maxZ / b.maxZ;
            }
            return out;
        }

        /** Finds an AABB that encloses a point cloud. */
        export function fromPoints(points: ArrayLike<Vector3D>, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            for (let i = 0, l = points.length; i < l; ++i)
            {
                const pt = points[i];
                if (i === 0)
                {
                    set(out, pt, pt);
                }
                else
                {
                    expand(out, pt);
                }
            }
            return out;
        }

        /** Expands the AABB to enclose the specified point. */
        export function expand(a: AABB3D, pt: ReadonlyVector3D): void
        {
            a.minX = min(a.minX, pt.x);
            a.minY = min(a.minY, pt.y);
            a.minZ = min(a.minZ, pt.z);
            a.maxX = max(a.maxX, pt.x);
            a.maxY = max(a.maxY, pt.y);
            a.maxZ = max(a.maxZ, pt.z);
        }

        /** Tests if a and b intersect. */
        export function intersects(a: ReadonlyAABB3D, b: ReadonlyAABB3D): boolean
        {
            return intersect1D(a.minX, a.maxX, b.minX, b.maxX) && intersect1D(a.minY, a.maxY, b.minY, b.maxY) && intersect1D(a.minZ, a.maxZ, b.minZ, b.maxZ);
        }

        /** Gets all 8 corners of a. */
        export function corners(a: ReadonlyAABB3D, out?: Vector3D[]): Vector3D[]
        {
            out = out || [];
            out.length = 8;
            const v0 = out[0] = out[0] || Vector3D();
            const v1 = out[1] = out[1] || Vector3D();
            const v2 = out[2] = out[2] || Vector3D();
            const v3 = out[3] = out[3] || Vector3D();
            const v4 = out[4] = out[4] || Vector3D();
            const v5 = out[5] = out[5] || Vector3D();
            const v6 = out[6] = out[6] || Vector3D();
            const v7 = out[7] = out[7] || Vector3D();
            v0.x = a.minX; v0.y = a.minY; v0.z = a.minZ;
            v1.x = a.minX; v1.y = a.minY; v1.z = a.maxZ;
            v2.x = a.minX; v2.y = a.maxY; v2.z = a.minZ;
            v3.x = a.minX; v3.y = a.maxY; v3.z = a.maxZ;
            v4.x = a.maxX; v4.y = a.minY; v4.z = a.minZ;
            v5.x = a.maxX; v5.y = a.minY; v5.z = a.maxZ;
            v6.x = a.maxX; v6.y = a.maxY; v6.z = a.minZ;
            v7.x = a.maxX; v7.y = a.maxY; v7.z = a.maxZ;
            return out;
        }

        const tmpCorners = new Array<Vector3D>(8);
        const tmpVecs: Vector3D[] = [ Vector3D(), Vector3D(), Vector3D(), Vector3D(), Vector3D(), Vector3D(), Vector3D(), Vector3D() ];

        /** Transforms a through mtx. */
        export function transform(a: ReadonlyAABB3D, mtx: Matrix4, out?: AABB3D): AABB3D
        {
            corners(a, tmpCorners);
            Matrix4.transform(mtx, tmpCorners[0], tmpVecs[0]);
            Matrix4.transform(mtx, tmpCorners[1], tmpVecs[1]);
            Matrix4.transform(mtx, tmpCorners[2], tmpVecs[2]);
            Matrix4.transform(mtx, tmpCorners[3], tmpVecs[3]);
            Matrix4.transform(mtx, tmpCorners[4], tmpVecs[4]);
            Matrix4.transform(mtx, tmpCorners[5], tmpVecs[5]);
            Matrix4.transform(mtx, tmpCorners[6], tmpVecs[6]);
            Matrix4.transform(mtx, tmpCorners[7], tmpVecs[7]);
            return fromPoints(tmpVecs, out);
        }

        /** Find the size of a. */
        export function size(a: ReadonlyAABB3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = a.maxX - a.minX;
            out.y = a.maxY - a.minY;
            out.z = a.maxZ - a.minZ;
            return out;
        }

        /** Returns a with corrected min and max. */
        export function normalised(a: ReadonlyAABB3D, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            out.minX = min(a.minX, a.maxX);
            out.minY = min(a.minY, a.maxY);
            out.minZ = min(a.minZ, a.maxZ);
            out.maxX = max(a.minX, a.maxX);
            out.maxY = max(a.minY, a.maxY);
            out.maxZ = max(a.minZ, a.maxZ);
            return out;
        }

        /** Sets a to have corrected min and max. */
        export function normalise(a: AABB3D): void
        {
            const minX = min(a.minX, a.maxX);
            const minY = min(a.minY, a.maxY);
            const minZ = min(a.minZ, a.maxZ);
            const maxX = max(a.minX, a.maxX);
            const maxY = max(a.minY, a.maxY);
            const maxZ = max(a.minZ, a.maxZ);
            a.minX = minX;
            a.minY = minY;
            a.minZ = minZ;
            a.maxX = maxX;
            a.maxY = maxY;
            a.maxZ = maxZ;
        }

        /** Linearly interpolates between two AABBs. */
        export function linearInterpolate(a: ReadonlyAABB3D, b: ReadonlyAABB3D, mu: number, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            out.minX = Math.linearInterpolate(a.minX, b.minX, mu);
            out.minY = Math.linearInterpolate(a.minY, b.minY, mu);
            out.minZ = Math.linearInterpolate(a.minZ, b.minZ, mu);
            out.maxX = Math.linearInterpolate(a.maxX, b.maxX, mu);
            out.maxY = Math.linearInterpolate(a.maxY, b.maxY, mu);
            out.maxZ = Math.linearInterpolate(a.maxZ, b.maxZ, mu);
            return out;
        }

        /** Finds if a and b are equal, within a certain threshold. */
        export function equals(a: ReadonlyAABB3D, b: ReadonlyAABB3D, threshold: number = 0.001): boolean
        {
            if (abs(a.minX - b.minX) > threshold) { return false; }
            if (abs(a.minY - b.minY) > threshold) { return false; }
            if (abs(a.minZ - b.minZ) > threshold) { return false; }
            if (abs(a.maxX - b.maxX) > threshold) { return false; }
            if (abs(a.maxY - b.maxY) > threshold) { return false; }
            if (abs(a.maxZ - b.maxZ) > threshold) { return false; }
            return true;
        }

        /** Determines if a has infinite extent. */
        export function isInfinite(a: ReadonlyAABB3D): boolean
        {
            return a.minX === -Infinity && a.minY === -Infinity && a.minZ === -Infinity && a.maxX === Infinity && a.maxY === Infinity && a.maxZ === Infinity;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyAABB3D, out?: AABB3D): AABB3D
        {
            out = out || AABB3D();
            out.minX = a.minX;
            out.minY = a.minY;
            out.minZ = a.minZ;
            out.maxX = a.maxX;
            out.maxY = a.maxY;
            out.maxZ = a.maxZ;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyAABB3D, b: AABB3D): void
        {
            b.minX = a.minX;
            b.minY = a.minY;
            b.minZ = a.minZ;
            b.maxX = a.maxX;
            b.maxY = a.maxY;
            b.maxZ = a.maxZ;
        }

        /** Sets the individual components of a. */
        export function set(a: AABB3D, min: Math.Vector3D, max: Math.Vector3D): void
        {
            a.minX = min.x;
            a.minY = min.y;
            a.minZ = min.z;
            a.maxX = max.x;
            a.maxY = max.y;
            a.maxZ = max.z;
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyAABB3D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyAABB3D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyAABB3D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.minX;
            arr[offset++] = a.minY;
            arr[offset++] = a.minZ;
            arr[offset++] = a.maxX;
            arr[offset++] = a.maxY;
            arr[offset++] = a.maxZ;
        }

        /** Indicates that the specified AABB instance is no longer required and may be reused. */
        export function free(vec: AABB3D): void
        {
            // TODO: Resource pool logic
        }
    }
}