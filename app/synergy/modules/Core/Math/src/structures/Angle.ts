namespace RS.Math
{
    const PI = Math.PI;
    const twoPI = PI * 2.0;

    export type AngleChangeHandler = (angle: Angle, oldValue: number) => void;

    /** Encapsulates a rotation around a single axis. */
    export class Angle
    {
        /** The actual value. */
        protected _value: number;

        /** Callbacks for when the value has changed. */
        protected _onChangeHandlers: AngleChangeHandler[];

        /** Gets or sets the actual numeric value of this angle in radians (0 to 2pi). */
        public get value() { return this._value; }
        public set value(value)
        {
            const oldVal = this._value;
            this._value = (value > 0) ? (value % twoPI) : (value % twoPI) + twoPI;
            if (this._onChangeHandlers != null)
            {
                for (let i = 0; i < this._onChangeHandlers.length; i++)
                {
                    this._onChangeHandlers[i](this, oldVal);
                }
            }
        }

        /** Gets or sets the actual numeric value of this angle in degrees (0 to 360). */
        public get valueDeg() { return (this._value / twoPI) * 360; }
        public set valueDeg(value) { this.value = (value / 360) * twoPI; }

        /** Initialises a new instance of the Angle class from the specified angle in radians. */
        public constructor(value: number);

        /** Initialises a new instance of the Angle class from the specified Angle object. */
        public constructor(value: Angle);

        /** Initialises a new instance of the Angle class from the specified direction vector. */
        public constructor(direction: Vector2D);

        /** Initialises a new instance of the Angle class. */
        public constructor();

        public constructor(value?: number | Angle | Vector2D)
        {
            if (value == null)
            {
                this.value = 0.0;
            }
            else if (Is.number(value))
            {
                this.value = value;
            }
            else if (value instanceof Angle)
            {
                this.value = value.value;
            }
            else
            {
                this.value = Math.atan2(value.y, value.x);
            }
        }

        /**
         * Finds the shortest signed angle between two angles
         * @param a {(Angle | number)} First angle
         * @param b {(Angle | number)} Second angle
         */
        public static diff(a: Angle | number, b: Angle | number): number
        {
            const aNum = Is.number(a) ? a : a.value;
            const bNum = Is.number(b) ? b : b.value;
            const dA = bNum - aNum;
            return RS.Math.mod(dA + PI, twoPI) - PI;
        }

        /**
         * Linearly interpolates between the two specified angles by mu.
         * @param from {(Angle | number)} The first number
         * @param to {(Angle | number)} The second number
         * @param mu The interpolant (0-1)
         */
        public static lerp(from: Angle | number, to: Angle | number, mu: number): number
        {
            const fromNum = Is.number(from) ? from : from.value;
            const toNum = Is.number(to) ? to : to.value;

            // Use unit tested linearInterpolate
            return linearInterpolate(fromNum, toNum, mu);
        }

        /**
         * Combines the specified angles
         * @param a Angle First angle
         * @param b Angle Angle to add
         */
        public static combine(a: Angle, b: Angle): Angle
        {
            const ang = new Angle(a);
            ang.add(b);
            return ang;
        }

        /**
         * Adds a handler to be called when this angle has changed value
         * @param callback AngleChangeHandler
         */
        public onChange(callback: AngleChangeHandler): AngleChangeHandler
        {
            if (this._onChangeHandlers == null) { this._onChangeHandlers = []; }
            this._onChangeHandlers.push(callback);
            return callback;
        }

        /**
         * Removes a handler to be called when this angle has changed value
         * @param callback AngleChangeHandler
         */
        public offChange(callback: AngleChangeHandler): void
        {
            if (this._onChangeHandlers == null) { return; }
            const idx = this._onChangeHandlers.indexOf(callback);
            if (idx !== -1) { this._onChangeHandlers.splice(idx, 1); }
            return null;
        }

        /** Creates a copy of this angle. */
        public clone(): Angle { return new Angle(this); }

        /** Sets this angle to be equal to the other. */
        public copy(other: Angle): void { this.value = other.value; }

        /** Adds the specified offset to this angle. */
        public add(offset: Angle | number): void
        {
            if (Is.number(offset))
            {
                this.value += offset;
            }
            else
            {
                this.value += offset.value;
            }
        }

        /**
         * Subtracts the specified offset from this angle
         * @param offset {(Angle | number)} Angle to subtract
         */
        public subtract(offset: Angle | number): void
        {
            if (Is.number(offset))
            {
                this.value -= offset;
            }
            else
            {
                this.value -= offset.value;
            }
        }

        /**
         * Moves this angle towards the other, limited by the specified maximum turn amount.
         * @param other     The angle to turn towards.
         * @param maxAmount The maximum amount to turn by.
         * @param percent   Whether or not maxAmount represents a percentage (0-1) of the total difference, or a fixed angle.
        **/
        public turnTowards(other: Angle | number, maxAmount: number, percent: boolean = false): void
        {
            const diff = Angle.diff(this, other);
            if (diff < 0)
            {
                this.value -= Math.min(-diff, maxAmount * (percent ? Math.abs(diff) : 1.0));
            }
            else
            {
                this.value += Math.min(diff, maxAmount * (percent ? Math.abs(diff) : 1.0));
            }
        }

        /** Finds the shortest signed angle between two angles, and sets it on this angle. */
        public diff(a: Angle | number, b: Angle | number): void
        {
            this.value = Angle.diff(a, b);
        }

        /**
         * Linearly interpolates between the two specified angles by mu, and sets it on this angle
         * @param from {(Angle | number)} The first number
         * @param to {(Angle | number)} The second number
         * @param mu The interpolant (0-1)
         */
        public lerp(from: Angle | number, to: Angle | number, mu: number): void
        {
            this.value = Angle.lerp(from, to, mu);
        }

        /**
         * Determines if this angle is equal to the other
         * @param other Angle Angle to match
         * @param threshold number Accuracy threshold
         */
        public equals(other: Angle, threshold: number = 0.0001): boolean
        {
            return Math.abs(this.value - other.value) < threshold;
        }

        /** Determines if this angle is close to the other, within a certain range. */
        public closeTo(other: Angle, within: Angle | number): boolean
        {
            const withinNum = Is.number(within) ? within : within.value;
            return Angle.diff(this, other) <= withinNum;
        }

        /**
         * Determines if this angle sits within the SHORTEST range between two others, inclusive
         * @param a {(Angle | number)} First angle
         * @param b {(Angle | number)} Second angle
         */
        public within(a: Angle | number, b: Angle | number): boolean
        {
            const aNum = Is.number(a) ? a : a.value;
            const bNum = Is.number(b) ? b : b.value;
            return Angle.diff(this, aNum) <= Angle.diff(bNum, aNum);
        }

        /**
         * Gets a string representation of this angle
         */
        public toString(): string
        {
            return `${this.value / PI} π`;
        }
    }
}