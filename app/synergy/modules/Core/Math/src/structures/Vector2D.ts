/// <reference path="../Math.ts"/>
/// <reference path="Size2D.ts" />

namespace RS.Is
{
    export function vector2D(value: any): value is Math.ReadonlyVector2D
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 2D Cartesian vector.
     */
    export interface ReadonlyVector2D
    {
        readonly x: number;
        readonly y: number;
    }

    /**
     * Encapsulates a 2D Cartesian vector.
     */
    export interface Vector2D extends ReadonlyVector2D
    {
        x: number;
        y: number;
    }

    /** Creates a new 2D vector with the values (0, 0). */
    export function Vector2D(): Vector2D;

    /** Creates a new 2D vector with the values (x, y). */
    export function Vector2D(x: number, y: number): Vector2D;

    /** Creates a new 2D vector with the values (cos(angle), sin(angle)). */
    export function Vector2D(angle: number): Vector2D;

    /** Creates a new 2D vector from a 2D size. */
    export function Vector2D(size: Size2D): Vector2D;

    /** Creates a new 2D vector with the values (cos(angle), sin(angle)). */
    export function Vector2D(p1?: number | Size2D, p2?: number): Vector2D
    {
        if (Is.size2D(p1))
        {
            return { x: p1.w, y: p1.h };
        }
        else if (Is.number(p2))
        {
            return { x: p1, y: p2 };
        }
        else if (Is.number(p1))
        {
            return { x: Math.cos(p1), y: Math.sin(p1) };
        }
        else
        {
            return { x: 0, y: 0 };
        }
    }

    /**
     * Convenience methods for 2D vector math.
     */
    export namespace Vector2D
    {
        /** Returns a string representation of a 2D vector. */
        export function toString(vec: ReadonlyVector2D): string
        {
            return `${vec.x}, ${vec.y}`;
        }

        /** (0, 0) */
        export const zero: ReadonlyVector2D = Vector2D(0, 0);

        /** (1, 0) */
        export const unitX: ReadonlyVector2D = Vector2D(1, 0);

        /** (0, 1) */
        export const unitY: ReadonlyVector2D = Vector2D(0, 1);

        /** Retrieves a[idx]. */
        export function getComponent(a: ReadonlyVector2D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.x;
                case 1: return a.y;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a[idx]. */
        export function setComponent(a: Vector2D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.x = value;
                case 1: return a.y = value;
                default: throw new Error("Invalid index");
            }
        }

        /** Returns a + b. */
        export function add(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = a.x + b.x;
            out.y = a.y + b.y;
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = a.x - b.x;
            out.y = a.y - b.y;
            return out;
        }

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Vector2D): Vector2D;

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector2D, b: number, out?: Vector2D): Vector2D;

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector2D, b: ReadonlyVector2D | number, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = a.x * (Is.number(b) ? b : b.x);
            out.y = a.y * (Is.number(b) ? b : b.y);
            return out;
        }

        /** Returns a / b. */
        export function divide(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Vector2D): Vector2D;

        /** Returns a / b. */
        export function divide(a: ReadonlyVector2D, b: number, out?: Vector2D): Vector2D;

        /** Returns a / b. */
        export function divide(a: ReadonlyVector2D, b: ReadonlyVector2D | number, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = a.x / (Is.number(b) ? b : b.x);
            out.y = a.y / (Is.number(b) ? b : b.y);
            return out;
        }

        /** Returns a + b * c. */
        export function addMultiply(a: ReadonlyVector2D, b: ReadonlyVector2D, c: ReadonlyVector2D, out?: Vector2D): Vector2D;

        /** Returns a + b * c. */
        export function addMultiply(a: ReadonlyVector2D, b: ReadonlyVector2D, c: number, out?: Vector2D): Vector2D;

        export function addMultiply(a: ReadonlyVector2D, b: ReadonlyVector2D, c: ReadonlyVector2D | number, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            if (Is.number(c))
            {
                out.x = a.x + b.x * c;
                out.y = a.y + b.y * c;
            }
            else
            {
                out.x = a.x + b.x * c.x;
                out.y = a.y + b.y * c.y;
            }
            return out;
        }

        /** Applies a clockwise rotation to a given vector. */
        export function rotate(a: ReadonlyVector2D, angle: number, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            angle = -angle;
            const x = a.x;
            const y = a.y;
            const cos = Math.cos(angle);
            const sin = Math.sin(angle);
            out.x = cos * x - sin * y;
            out.y = sin * x + cos * y;
            return out;
        }

        /** Calculates the dot product of two vectors. */
        export function dot(a: ReadonlyVector2D, b: ReadonlyVector2D): number
        {
            return (a.x * b.x) + (a.y * b.y);
        }

        /**
         * Calculates the angle between two directional vectors.
         * The input vectors do not need to be normalised.
         */
        export function angleBetween(a: ReadonlyVector2D, b: ReadonlyVector2D): number
        {
            return Math.atan2(a.y, a.x) - Math.atan2(b.y, b.x);
        }

        /**
         * Calculates a normalised 2D direction vector from a to b.
         * Mathmatically equivalent to |b - a|.
         */
        export function findBetween(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            subtract(b, a, out);
            normalise(out);
            return out;
        }

        /**
         * Returns the distance between two points.
         */
        export function distanceBetween(p1: ReadonlyVector2D, p2: ReadonlyVector2D): number
        {
            const dx = (p2.x - p1.x) ** 2;
            const dy = (p2.y - p1.y) ** 2;

            return Math.sqrt(dx + dy);
        }

        /** Finds the squared magnitude of a vector. */
        export function sizeSq(v: ReadonlyVector2D): number
        {
            return v.x ** 2 + v.y ** 2;
        }

        /** Finds the magnitude of a vector. */
        export function size(v: ReadonlyVector2D): number
        {
            return Math.sqrt(v.x ** 2 + v.y ** 2);
        }

        /** Normalises a 2D vector in-place. */
        export function normalise(v: Vector2D): Vector2D
        {
            const length = size(v);
            v.x /= length;
            v.y /= length;
            return v;
        }

        /** Returns a normalised version of a 2D vector. */
        export function normalised(v: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            const length = size(v);
            out.x = v.x / length;
            out.y = v.y / length;
            return out;
        }

        /** Linearly interpolates between two vectors. */
        export function linearInterpolate(a: ReadonlyVector2D, b: ReadonlyVector2D, mu: number, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            return out;
        }

        /** Gets if two vectors are equal, optionally within a threshold. */
        export function equals(a: ReadonlyVector2D, b: ReadonlyVector2D, threshold: number = 0.0): boolean
        {
            const diffX = Math.abs(b.x - a.x);
            const diffY = Math.abs(b.y - a.y);
            return diffX <= threshold && diffY <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = a.x;
            out.y = a.y;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyVector2D, b: Vector2D): void
        {
            b.x = a.x;
            b.y = a.y;
        }

        /** Sets the individual components of a. */
        export function set(a: Vector2D, x?: number, y?: number): void
        {
            if (x != null) { a.x = x; }
            if (y != null) { a.y = y; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyVector2D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyVector2D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyVector2D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
        }

        /** Indicates that the specified vector instance is no longer required and may be reused. */
        export function free(vec: Vector2D): void
        {
            // TODO: Resource pool logic
        }

        /** Returns the projection of a on b. */
        export function project(a: ReadonlyVector2D, b: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out = multiply(b, dot(a, b) / dot(b, b), out);
            return out;
        }
    }
}
