/// <reference path="Vector3D.ts" />
/// <reference path="Vector4D.ts" />

namespace RS.Util
{
    const colorMap: { [name: string]: number } = {};

    export const RawColors =
    {
        white: colorMap["white"] = 0xffffff,
        silver: colorMap["silver"] = 0xc0c0c0,
        gray: colorMap["gray"] = 0x808080,
        black: colorMap["black"] = 0x000000,
        red: colorMap["red"] = 0xff0000,
        maroon: colorMap["maroon"] = 0x800000,
        yellow: colorMap["yellow"] = 0xffff00,
        olive: colorMap["olive"] = 0x808000,
        lime: colorMap["lime"] = 0x00ff00,
        green: colorMap["green"] = 0x008000,
        aqua: colorMap["aqua"] = 0x00ffff,
        teal: colorMap["teal"] = 0x008080,
        blue: colorMap["blue"] = 0x0000ff,
        navy: colorMap["navy"] = 0x000080,
        fuchsia: colorMap["fuchsia"] = 0xff00ff,
        purple: colorMap["purple"] = 0x800080,

        // X11 colours

        // Pinks
        pink: colorMap["pink"] = 0xffc0cb,
        lightpink: colorMap["lightpink"] = 0xffb6c1,
        hotpink: colorMap["hotpink"] = 0xff69b4,
        deeppink: colorMap["deeppink"] = 0xff1493,
        palevioletred: colorMap["palevioletred"] = 0xdb7093,
        mediumvioletred: colorMap["mediumvioletred"] = 0xc71585,

        // Reds
        lightsalmon: colorMap["lightsalmon"] = 0xffa07a,
        salmon: colorMap["salmon"] = 0xfa8072,
        darksalmon: colorMap["darksalmon"] = 0xe9967a,
        lightcoral: colorMap["lightcoral"] = 0xf08080,
        indianred: colorMap["indianred"] = 0xcd5c5c,
        crimson: colorMap["crimson"] = 0xdc143c,
        firebrick: colorMap["firebrick"] = 0xb22222,
        darkred: colorMap["darkred"] = 0x8b0000,

        // Oranges
        orangered: colorMap["orangered"] = 0xff4500,
        tomato: colorMap["tomato"] = 0xff6347,
        coral: colorMap["coral"] = 0xff7f50,
        darkorange: colorMap["darkorange"] = 0xff8c00,
        orange: colorMap["orange"] = 0xffa500,

        // Yellows
        lightyellow: colorMap["lightyellow"] = 0xffffe0,
        lemonchiffon: colorMap["lemonchiffon"] = 0xfffacd,
        lightgoldenrodyellow: colorMap["lightgoldenrodyellow"] = 0xfafad2,
        papayawhip: colorMap["papayawhip"] = 0xffefd5,
        moccasin: colorMap["moccasin"] = 0xffe4b5,
        peachpuff: colorMap["peachpuff"] = 0xffdab9,
        palegoldenrod: colorMap["palegoldenrod"] = 0xeee8aa,
        khaki: colorMap["khaki"] = 0xf0e68c,
        darkkhaki: colorMap["darkkhaki"] = 0xbdb76b,
        gold: colorMap["gold"] = 0xffd700,

        // Browns
        cornsilk: colorMap["cornsilk"] = 0xfff8dc,
        blanchedalmond: colorMap["blanchedalmond"] = 0xffebcd,
        bisque: colorMap["bisque"] = 0xffe4c4,
        navajowhite: colorMap["navajowhite"] = 0xffdead,
        wheat: colorMap["wheat"] = 0xf5deb3,
        burlywood: colorMap["burlywood"] = 0xdeb887,
        tan: colorMap["tan"] = 0xd2b48c,
        rosybrown: colorMap["rosybrown"] = 0xbc8f8f,
        sandybrown: colorMap["sandybrown"] = 0xf4a460,
        goldenrod: colorMap["goldenrod"] = 0xdaa520,
        darkgoldenrod: colorMap["darkgoldenrod"] = 0xb8860b,
        peru: colorMap["peru"] = 0xcd853f,
        chocolate: colorMap["chocolate"] = 0xd2691e,
        saddlebrown: colorMap["saddlebrown"] = 0x8b4513,
        sienna: colorMap["sienna"] = 0xa0522d,
        brown: colorMap["brown"] = 0xa52a2a,

        // Greens
        darkolivegreen: colorMap["darkolivegreen"] = 0x556b2f,
        olivedrab: colorMap["olivedrab"] = 0x6b8e23,
        yellowgreen: colorMap["yellowgreen"] = 0x9acd32,
        linegreen: colorMap["linegreen"] = 0x32cd32,
        lawngreen: colorMap["lawngreen"] = 0x7cfc00,
        chartreuse: colorMap["chartreuse"] = 0x7fff00,
        greenyellow: colorMap["greenyellow"] = 0xadff2f,
        springgreen: colorMap["springgreen"] = 0x00ff7f,
        mediumspringgreen: colorMap["mediumspringgreen"] = 0x00fa9a,
        lightgreen: colorMap["lightgreen"] = 0x90ee90,
        palegreen: colorMap["palegreen"] = 0x98fb98,
        darkseagreen: colorMap["darkseagreen"] = 0x8fbc85,
        mediumaquamarine: colorMap["mediumaquamarine"] = 0x66cdaa,
        mediumseagreen: colorMap["mediumseagreen"] = 0x3cb371,
        seagreen: colorMap["seagreen"] = 0x2e8b57,
        forestgreen: colorMap["forestgreen"] = 0x228b22,
        darkgreen: colorMap["darkgreen"] = 0x006400,

        // Cyans
        cyan: colorMap["cyan"] = 0x00ffff,
        lightcyan: colorMap["lightcyan"] = 0xe0ffff,
        paleturquoise: colorMap["paleturquoise"] = 0xafeeee,
        aquamarine: colorMap["aquamarine"] = 0x7fffd4,
        turquoise: colorMap["turquoise"] = 0x40e0d0,
        mediumturquoise: colorMap["mediumturquoise"] = 0x48d1cc,
        darkturquoise: colorMap["darkturquoise"] = 0x00ced1,
        lightseagreen: colorMap["lightseagreen"] = 0x20b2aa,
        cadetblue: colorMap["cadetblue"] = 0x5f9ea0,
        darkcyan: colorMap["darkcyan"] = 0x008b8b,

        // Blues
        lightsteelblue: colorMap["lightsteelblue"] = 0xb0c4de,
        powderblue: colorMap["powderblue"] = 0xb0e0e6,
        lightblue: colorMap["lightblue"] = 0xadd8e6,
        skyblue: colorMap["skyblue"] = 0x87ceeb,
        lightskyblue: colorMap["lightskyblue"] = 0x87cefa,
        deepskyblue: colorMap["deepskyblue"] = 0x00bfff,
        dodgerblue: colorMap["dodgerblue"] = 0x1e90ff,
        cornflowerblue: colorMap["cornflowerblue"] = 0x6495ed,
        steelblue: colorMap["steelblue"] = 0x4682b4,
        royalblue: colorMap["royalblue"] = 0x4169e1,
        mediumblue: colorMap["mediumblue"] = 0x0000cd,
        darkblue: colorMap["darkblue"] = 0x00008b,
        midnightblue: colorMap["midnightblue"] = 0x191970,

        // Purples
        lavander: colorMap["lavander"] = 0xe6e6fa,
        thistle: colorMap["thistle"] = 0xd8bfd8,
        plum: colorMap["plum"] = 0xdda0dd,
        violet: colorMap["violet"] = 0xee82ee,
        orchid: colorMap["orchid"] = 0xda70d6,
        magenta: colorMap["magenta"] = 0xff00ff,
        mediumorchid: colorMap["mediumorchid"] = 0xba55d3,
        mediumpurple: colorMap["mediumpurple"] = 0x9370db,
        blueviolet: colorMap["blueviolet"] = 0x8a2be2,
        darkviolet: colorMap["darkviolet"] = 0x9400d3,
        darkorchid: colorMap["darkorchid"] = 0x9932cc,
        darkmagenta: colorMap["darkmagenta"] = 0x8b008b,
        indigo: colorMap["indigo"] = 0x4b0082,
        darkslateblue: colorMap["darkslateblue"] = 0x483d8b,
        slateblue: colorMap["slateblue"] = 0x6a5acd,
        mediumslateblue: colorMap["mediumslateblue"] = 0x7b68ee,

        // Whites
        snow: colorMap["snow"] = 0xfffafa,
        honeydew: colorMap["honeydew"] = 0xf0fff0,
        mintcream: colorMap["mintcream"] = 0xf5fffa,
        azure: colorMap["azure"] = 0xf0ffff,
        aliceblue: colorMap["aliceblue"] = 0xf0f8ff,
        ghostwhite: colorMap["ghostwhite"] = 0xf8f8ff,
        seashell: colorMap["seashell"] = 0xfff5ee,
        beige: colorMap["beige"] = 0xf5f5dc,
        oldlace: colorMap["oldlace"] = 0xfdf5e6,
        floralwhite: colorMap["floralwhite"] = 0xfffaf0,
        ivory: colorMap["ivory"] = 0xfffff0,
        antiquewhite: colorMap["antiquewhite"] = 0xfaebd7,
        linen: colorMap["linen"] = 0xfaf0e6,
        lavenderblush: colorMap["lavenderblush"] = 0xfff0f5,
        mistyrose: colorMap["mistyrose"] = 0xffe4e1,

        // Grays
        gainsboro: colorMap["gainsboro"] = 0xdcdcdc,
        lightgrey: colorMap["lightgrey"] = 0xd3d3d3,
        darkgray: colorMap["darkgray"] = 0xa9a9a9,
        dimgray: colorMap["dimgray"] = 0x696969,
        lightslategray: colorMap["lightslategray"] = 0x778899,
        slategray: colorMap["slategray"] = 0x708090,
        darkslategray: colorMap["darkslategray"] = 0x2f4f4f,

        // Other colours
        rebeccapurple: colorMap["rebeccapurple"] = 0x663399
    };

    function mapObject<K extends string, T, U>(obj: Record<K, T>, f: (x: T) => U): Record<K, U>
    {
        const tmp = {} as Record<K, U>;
        for (const key in obj)
        {
            tmp[key] = f(obj[key]);
        }
        return tmp;
    }

    /**
     * Given a numeric tint-like color, returns the name of that color (or null if not found).
     * @param value
     */
    export function lookupColorValue(value: number): string | null
    {
        for (const key in colorMap)
        {
            if (colorMap[key] === value)
            {
                return key;
            }
        }
        return null;
    }

    /**
     * Encapsulates a color and alpha value, with components ranging from 0-1.
     */
    export interface ColorLike
    {
        r: number;
        g: number;
        b: number;
        a?: number;
    }

    /**
     * Encapsulates a color in HSV (hue saturation value) format.
     */
    export interface HSV
    {
        /** Hue (0-360). */
        h: number;

        /** Saturation (0-1). */
        s: number;

        /** Value (0-1). */
        v: number;
    }

    const gamma = 2.2;

    /**
     * Gets if the specified object is color-like.
     */
    export function isColorLike(obj: any): obj is ColorLike
    {
        if (obj instanceof Color) { return true; }
        if (!Is.object(obj)) { return false; }
        return Is.number(obj.r) && Is.number(obj.g) && Is.number(obj.b) && (Is.number(obj.a) || obj.a == null);
    }

    /**
     * Encapsulates a color and alpha value, with components ranging from 0-1.
     */
    export class Color implements ColorLike
    {
        private static _padStrings = [ "000000", "00000", "0000", "000", "00", "0", "" ];

        /** Red component (0-1) */
        public r: number;
        /** Green component (0-1) */
        public g: number;
        /** Blue component (0-1) */
        public b: number;
        /** Alpha component (0-1) */
        public a: number;

        /** Gets the red chanel of this color in linear space. */
        public get linearR() { return Math.pow(this.r, gamma); }

        /** Gets the red chanel of this color in linear space. */
        public get linearG() { return Math.pow(this.g, gamma); }

        /** Gets the red chanel of this color in linear space. */
        public get linearB() { return Math.pow(this.b, gamma); }

        public constructor(tint: number, a?: number);
        public constructor(r?: number, g?: number, b?: number, a?: number);
        public constructor(copyFrom: ColorLike);
        public constructor(copyFrom: Math.Vector3D);
        public constructor(copyFrom: Math.Vector4D);
        public constructor(value: string, a?: number);

        public constructor(r?: number|ColorLike|string|Math.Vector3D|Math.Vector4D, g?: number, b?: number, a?: number)
        {
            if (isColorLike(r))
            {
                this.r = r.r;
                this.g = r.g;
                this.b = r.b;
                this.a = r.a == null ? 1.0 : r.a;
            }
            else if (Is.string(r))
            {
                // Is it an encoded color? (e.g. "#123456")
                if (r.substr(0, 1) === "#")
                {
                    if (r.length === 7) // "#123456"
                    {
                        this.r = parseInt(r.substring(1, 3), 16) / 0xff;
                        this.g = parseInt(r.substring(3, 5), 16) / 0xff;
                        this.b = parseInt(r.substring(5, 7), 16) / 0xff;
                    }
                    else if (r.length === 4) // "#135"
                    {
                        this.r = parseInt(r.substring(1, 2), 16) / 0xf;
                        this.g = parseInt(r.substring(2, 3), 16) / 0xf;
                        this.b = parseInt(r.substring(3, 4), 16) / 0xf;
                    }
                    else
                    {
                        RS.Log.warn("Unknown color format");
                    }
                }
                // Is it a named color? (e.g. "red")
                else
                {
                    const lookupCol = Colors[r];
                    if (lookupCol != null)
                    {
                        this.copy(lookupCol);
                    }
                    else if (colorMap[r] != null)
                    {
                        this.tint = colorMap[r];
                    }
                    else
                    {
                        RS.Log.warn("Unknown color format");
                    }
                }
                this.a = g != null ? g : 1.0;
            }
            else if (Is.vector4D(r))
            {
                this.r = r.x;
                this.g = r.y;
                this.b = r.z;
                this.a = r.w;
            }
            else if (Is.vector3D(r))
            {
                this.r = r.x;
                this.g = r.y;
                this.b = r.z;
                this.a = 1.0;
            }
            else if (b == null && a == null)
            {
                this.tint = r;
                this.a = g != null ? g : 1.0;
            }
            else
            {
                this.r = r || 0;
                this.g = g || 0;
                this.b = b || 0;
                this.a = (a != null) ? a : 1.0;
            }

            if (this.r > 1 || this.g > 1 || this.b > 1)
            {
                this.r /= 0xff;
                this.g /= 0xff;
                this.b /= 0xff;
                if (this.a > 1)
                {
                    this.a /= 0xff;
                }
            }
        }

        /**
         * Creates a new color from the specified values in linear color space (0-1).
         * @param linearR
         * @param linearG
         * @param linearB
         */
       public static fromLinear(linearR: number, linearG: number, linearB: number): Color
       {
           return new Color(
               Math.pow(linearR, 1.0 / gamma),
               Math.pow(linearG, 1.0 / gamma),
               Math.pow(linearB, 1.0 / gamma)
           );
       }

        /**
         * Gets or sets this color as a PIXI-like tint value.
         */
        public get tint()
        {
            return (((this.r * 0xff)|0) << 16) | (((this.g * 0xff)|0) << 8) | ((this.b * 0xff)|0);
        }
        public set tint(value)
        {
            this.r = ((value >> 16) & 0xff) / 0xff;
            this.g = ((value >> 8) & 0xff) / 0xff;
            this.b = (value & 0xff) / 0xff;
        }

        /**
         * Gets or sets this color in HSV form (hue saturation value).
         * Does not consider alpha.
         */
        public get hsv(): HSV
        {
            // https://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both

            const out: HSV = { h: 0, s: 0.0, v: 0.0 };
            let min: number, max: number, delta: number;

            min = this.r < this.g ? this.r : this.g;
            min = min < this.b ? min : this.b;

            max = this.r > this.g ? this.r : this.g;
            max = max > this.b ? max : this.b;

            out.v = max;
            delta = max - min;
            if (delta < 0.00001)
            {
                out.s = 0;
                out.h = 0;
                return out;
            }
            if (max > 0.0)
            {
                out.s = (delta / max);
            }
            else
            {
                // if max is 0, then r = g = b = 0
                // s = 0, h is undefined
                out.s = 0.0;
                out.h = NaN;                            // its now undefined
                return out;
            }
            if (this.r >= max)
            {
                out.h = ( this.g - this.b ) / delta;        // between yellow & magenta
            }
            else if (this.g >= max)
            {
                out.h = 2.0 + ( this.b - this.r ) / delta;  // between cyan & yellow
            }
            else
            {
                out.h = 4.0 + ( this.r - this.g ) / delta;  // between magenta & cyan
            }

            out.h *= 60.0;                              // degrees

            if (out.h < 0.0)
            {
                out.h += 360.0;
            }

            return out;
        }
        public set hsv(value: HSV)
        {
            // https://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both

            let hh: number, p: number, q: number, t: number, ff: number;
            let i: number;

            if (value.s <= 0.0)
            {       // < is bogus, just shuts up warnings
                this.r = value.v;
                this.g = value.v;
                this.b = value.v;
                return;
            }
            hh = value.h;
            if (hh >= 360.0) { hh = 0.0; }
            hh /= 60.0;
            i = hh|0;
            ff = hh - i;
            p = value.v * (1.0 - value.s);
            q = value.v * (1.0 - (value.s * ff));
            t = value.v * (1.0 - (value.s * (1.0 - ff)));

            switch(i)
            {
                case 0:
                    this.r = value.v;
                    this.g = t;
                    this.b = p;
                    break;
                case 1:
                    this.r = q;
                    this.g = value.v;
                    this.b = p;
                    break;
                case 2:
                    this.r = p;
                    this.g = value.v;
                    this.b = t;
                    break;

                case 3:
                    this.r = p;
                    this.g = q;
                    this.b = value.v;
                    break;
                case 4:
                    this.r = t;
                    this.g = p;
                    this.b = value.v;
                    break;
                case 5:
                default:
                    this.r = value.v;
                    this.g = p;
                    this.b = q;
                    break;
            }
        }

        /**
         * Returns a vec4 colour for shaders uniforms.
         */
        public get vec4arr(): [number, number, number, number]
        {
            return [this.r, this.g, this.b, this.a];
        }

        /**
         * Returns a vec4 colour for shaders uniforms.
         */
        public get vec4(): Math.Vector4D
        {
            return Math.Vector4D(this.r, this.g, this.b, this.a);
        }

        /**
         * Returns a vec3 colour for shaders uniforms.
         */
        public get vec3(): Math.Vector3D
        {
            return Math.Vector3D(this.r, this.g, this.b);
        }

        /**
         * Sets the value of this color.
         */
        public set(color?: ColorLike): void;
        /**
         * Sets the components of this color.
         */
        public set(r?: number, g?: number, b?: number, a?: number): void;

        public set(r?: number | ColorLike, g?: number, b?: number, a?: number): void
        {
            if (isColorLike(r))
            {
                this.r = r.r;
                this.g = r.g;
                this.b = r.b;
                this.a = r.a;
            }
            else
            {
                this.r = (r == null ? this.r : r);
                this.g = (g == null ? this.g : g);
                this.b = (b == null ? this.b : b);
                this.a = (a == null ? this.a : a);
            }
            if (this.r > 1) { this.r /= 0xff; }
            if (this.g > 1) { this.g /= 0xff; }
            if (this.b > 1) { this.b /= 0xff; }
            if (this.a > 1) { this.a /= 0xff; }
        }

        /**
         * Sets the components of this color to be equal to the other color.
         */
        public copy(from: ColorLike): void
        {
            this.set(from.r, from.g, from.b, from.a);
        }

        /**
         * Gets if this color is equal to the other color.
         */
        public equals(other: ColorLike): boolean
        {
            return this.r === other.r && this.g === other.g && this.b === other.b && this.a === other.a;
        }

        /**
         * Creates a copy of this color.
         */
        public clone(): Color
        {
            return new Color(this);
        }

        /**
         * Creates a darker copy of this color.
         * @param by 0-1
         */
        public darker(by: number): Color
        {
            return new Color(
                Math.clamp(this.r * (1.0 - by), 0.0, 1.0),
                Math.clamp(this.g * (1.0 - by), 0.0, 1.0),
                Math.clamp(this.b * (1.0 - by), 0.0, 1.0)
            );
        }

        /**
         * Creates a brighter copy of this color.
         * @param by 0-1
         */
        public brighter(by: number): Color
        {
            const { linearR, linearG, linearB } = this;
            return Color.fromLinear(
                Math.clamp(linearR + (1.0 - linearR) * by, 0.0, 1.0),
                Math.clamp(linearG + (1.0 - linearG) * by, 0.0, 1.0),
                Math.clamp(linearB + (1.0 - linearB) * by, 0.0, 1.0)
            );
        }

        /**
         * Linearly interpolates between a and b by mu, and sets the result on this color.
         */
        public lerp(a: ColorLike, b: ColorLike, mu: number): void
        {
            this.r = Math.linearInterpolate(a.r, b.r, mu);
            this.g = Math.linearInterpolate(a.g, b.g, mu);
            this.b = Math.linearInterpolate(a.b, b.b, mu);
            this.a = Math.linearInterpolate(a.a, b.a, mu);
        }

        /**
         * Multiplies this color by the other.
         * Has the effect of masking this color out.
         */
        public multiply(other: ColorLike): void;

        /**
         * Multiplies this color by a scalar.
         */
        public multiply(value: number): void;

        public multiply(valueOrColor: number|ColorLike): void
        {
            if (isColorLike(valueOrColor))
            {
                this.r *= valueOrColor.r;
                this.g *= valueOrColor.g;
                this.b *= valueOrColor.b;
                this.a *= valueOrColor.a;
            }
            else
            {
                this.r *= valueOrColor;
                this.g *= valueOrColor;
                this.b *= valueOrColor;
                this.a *= valueOrColor;
            }
        }

        /**
         * Gets a hex string representation of this color (e.g. #ffffff)
         */
        public toString()
        {
            const str = this.tint.toString(16);
            return `#${Color._padStrings[str.length]}${str}`;
        }

        /**
         * Gets an rgb(r, g, b, a) string for this color (e.g. rgb(255, 255, 255, 1))
         */
        public toRGBAString()
        {
            return `rgba(${Math.round(this.r * 255)},${Math.round(this.g * 255)},${Math.round(this.b * 255)},${this.a})`;
        }
    }

    export const Colors = mapObject(RawColors, (color) => new Color(color));
}
