/// <reference path="Vector2D.ts" />

namespace RS.Is
{
    export function vector3D(value: any): value is Math.Vector3D
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y) && Is.number(value.z);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 3D Cartesian vector.
     */
    export interface ReadonlyVector3D extends ReadonlyVector2D
    {
        readonly z: number;
    }

    /**
     * Encapsulates a 3D Cartesian vector.
     */
    export interface Vector3D extends Vector2D
    {
        z: number;
    }

    /** Creates a new Vector3D. */
    export function Vector3D(x?: number, y?: number, z?: number): Vector3D;

    /** Creates a new Vector3D from a Vector2D and a z. */
    export function Vector3D(vec: Vector2D, z?: number): Vector3D;

    export function Vector3D(p1?: Vector2D | number, p2?: number, p3?: number): Vector3D
    {
        if (Is.nonPrimitive(p1))
        {
            return { x: p1.x, y: p1.y, z: p2 || 0 };
        }
        else
        {
            return { x: p1 || 0, y: p2 || 0, z: p3 || 0 };
        }
    }

    export namespace Vector3D
    {
        /** (0, 0, 0) */
        export const zero: ReadonlyVector3D = Vector3D(0, 0, 0);

        /** (1, 0, 0) */
        export const unitX: ReadonlyVector3D = Vector3D(1, 0, 0);

        /** (0, 1, 0) */
        export const unitY: ReadonlyVector3D = Vector3D(0, 1, 0);

        /** (0, 0, 1) */
        export const unitZ: ReadonlyVector3D = Vector3D(0, 0, 1);

        /** (1, 1, 1) */
        export const one: ReadonlyVector3D = Vector3D(1, 1, 1);

        /** Retrieves a[idx]. */
        export function getComponent(a: ReadonlyVector3D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.x;
                case 1: return a.y;
                case 2: return a.z;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a[idx]. */
        export function setComponent(a: Vector3D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.x = value;
                case 1: return a.y = value;
                case 2: return a.z = value;
                default: throw new Error("Invalid index");
            }
        }

        /** Returns a + b. */
        export function add(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = a.x + b.x;
            out.y = a.y + b.y;
            out.z = a.z + b.z;
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = a.x - b.x;
            out.y = a.y - b.y;
            out.z = a.z - b.z;
            return out;
        }

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Vector3D): Vector3D;

        /** Returns a * b. */
        export function multiply(a: ReadonlyVector3D, b: number, out?: Vector3D): Vector3D;

        export function multiply(a: ReadonlyVector3D, b: ReadonlyVector3D | number, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            if (Is.number(b))
            {
                out.x = a.x * b;
                out.y = a.y * b;
                out.z = a.z * b;
            }
            else
            {
                out.x = a.x * b.x;
                out.y = a.y * b.y;
                out.z = a.z * b.z;
            }
            return out;
        }

        /** Returns a / b. */
        export function divide(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Vector3D): Vector3D;

        /** Returns a / b. */
        export function divide(a: ReadonlyVector3D, b: number, out?: Vector3D): Vector3D;

        export function divide(a: ReadonlyVector3D, b: ReadonlyVector3D | number, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            if (Is.number(b))
            {
                out.x = a.x / b;
                out.y = a.y / b;
                out.z = a.z / b;
            }
            else
            {
                out.x = a.x / b.x;
                out.y = a.y / b.y;
                out.z = a.z / b.z;
            }
            return out;
        }

        /** Returns a + b * c. */
        export function addMultiply(a: ReadonlyVector3D, b: ReadonlyVector3D, c: ReadonlyVector3D, out?: Vector3D): Vector3D;

        /** Returns a + b * c. */
        export function addMultiply(a: ReadonlyVector3D, b: ReadonlyVector3D, c: number, out?: Vector3D): Vector3D;

        export function addMultiply(a: ReadonlyVector3D, b: ReadonlyVector3D, c: ReadonlyVector3D | number, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            if (Is.number(c))
            {
                out.x = a.x + b.x * c;
                out.y = a.y + b.y * c;
                out.z = a.z + b.z * c;
            }
            else
            {
                out.x = a.x + b.x * c.x;
                out.y = a.y + b.y * c.y;
                out.z = a.z + b.z * c.z;
            }
            return out;
        }

        /** Find the dot product of a and b. */
        export function dot(a: ReadonlyVector3D, b: ReadonlyVector3D): number
        {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        /** Find the cross product of a and b. */
        export function cross(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = a.y * b.z - a.z * b.y;
            out.y = a.z * b.x - a.x * b.z;
            out.z = a.x * b.y - a.y * b.x;
            return out;
        }

        /**
         * Calculates the angle between two directional vectors.
         * The input vectors do not need to be normalised.
         */
        export function angleBetween(a: ReadonlyVector3D, b: ReadonlyVector3D): number
        {
            const aNorm = normalised(a);
            const bNorm = normalised(b);
            const result = Math.acos(dot(aNorm, bNorm));
            free(aNorm);
            free(bNorm);
            return result;
        }

        /**
         * Calculates a normalised 3D direction vector from a to b.
         * Mathmatically equivalent to |b - a|.
         */
        export function findBetween(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            subtract(b, a, out);
            normalise(out);
            return out;
        }

        /**
         * Returns the distance between two points.
         */
        export function distanceBetween(p1: ReadonlyVector3D, p2: ReadonlyVector3D): number
        {
            const dx = (p2.x - p1.x) ** 2;
            const dy = (p2.y - p1.y) ** 2;
            const dz = (p2.z - p1.z) ** 2;

            return Math.sqrt(dx + dy + dz);
        }

        /** Find the squared magnitude of a. */
        export function sizeSq(a: ReadonlyVector3D): number
        {
            return a.x ** 2 + a.y ** 2 + a.z ** 2;
        }

        /** Find the magnitude of a. */
        export function size(a: ReadonlyVector3D): number
        {
            return Math.sqrt(a.x ** 2 + a.y ** 2 + a.z ** 2);
        }

        /** Returns a of unit length. */
        export function normalised(a: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            const len = size(a);
            out.x = a.x / len;
            out.y = a.y / len;
            out.z = a.z / len;
            return out;
        }

        /** Sets a to be of unit length. */
        export function normalise(a: Vector3D): void
        {
            const len = size(a);
            a.x = a.x / len;
            a.y = a.y / len;
            a.z = a.z / len;
        }

        /** Returns a of newLength length. */
        export function withLength(a: ReadonlyVector3D, newLength: number, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            const len = size(a);
            out.x = (a.x / len) * newLength;
            out.y = (a.y / len) * newLength;
            out.z = (a.z / len) * newLength;
            return out;
        }

        /** Returns a of newLength length. */
        export function setLength(a: Vector3D, newLength: number): void
        {
            const len = size(a);
            a.x = (a.x / len) * newLength;
            a.y = (a.y / len) * newLength;
            a.z = (a.z / len) * newLength;
        }

        /** Linearly interpolates between two vectors. */
        export function linearInterpolate(a: ReadonlyVector3D, b: ReadonlyVector3D, mu: number, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            out.z = Math.linearInterpolate(a.z, b.z, mu);
            return out;
        }

        /** Finds if a and b are equal, within a certain threshold. */
        export function equals(a: ReadonlyVector3D, b: ReadonlyVector3D, threshold: number = 0.001): boolean
        {
            if (Math.abs(a.x - b.x) > threshold) { return false; }
            if (Math.abs(a.y - b.y) > threshold) { return false; }
            if (Math.abs(a.z - b.z) > threshold) { return false; }
            return true;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = a.x;
            out.y = a.y;
            out.z = a.z;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyVector3D, b: Vector3D): void
        {
            b.x = a.x;
            b.y = a.y;
            b.z = a.z;
        }

        /** Sets the individual components of a. */
        export function set(a: Vector3D, x?: number, y?: number, z?: number): void
        {
            if (x != null) { a.x = x; }
            if (y != null) { a.y = y; }
            if (z != null) { a.z = z; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyVector3D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyVector3D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyVector3D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.z;
        }

        /** Indicates that the specified vector instance is no longer required and may be reused. */
        export function free(vec: Vector3D): void
        {
            // TODO: Resource pool logic
        }
    }
}