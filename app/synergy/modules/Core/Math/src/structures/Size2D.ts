/// <reference path="../Math.ts"/>

namespace RS.Is
{
    export function size2D(value: any): value is Math.ReadonlySize2D
    {
        return Is.object(value) && Is.number(value.w) && Is.number(value.h);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable 2D size (width and height).
     */
    export interface ReadonlySize2D
    {
        readonly w: number;
        readonly h: number;
    }

    /**
     * Encapsulates a 2D size (width and height).
     */
    export interface Size2D
    {
        w: number;
        h: number;
    }

    /** Creates a new 2D size with the values (0, 0). */
    export function Size2D(): Size2D;

    /** Creates a new 2D size with the values (w, h). */
    export function Size2D(w: number, h: number): Size2D;

    /** Creates a new 2D size from a 2D vector. */
    export function Size2D(vec: Vector2D): Size2D;

    export function Size2D(p1?: number | Vector2D, p2?: number): Size2D
    {
        if (Is.vector2D(p1))
        {
            return { w: p1.x, h: p1.y };
        }
        else
        {
            return { w: p1 || 0, h: p2 || 0 };
        }
    }

    /**
     * Convenience methods for 2D size math.
     */
    export namespace Size2D
    {
        /** Returns a string representation of a 2D size. */
        export function toString(size: ReadonlySize2D): string
        {
            return `${size.w} x ${size.h}`;
        }
        
        /** Returns a + b. */
        export function add(a: ReadonlySize2D, b: number, out?: Size2D): Size2D;

        /** Returns a + b. */
        export function add(a: ReadonlySize2D, b: ReadonlySize2D, out?: Size2D): Size2D;

        /** Returns a + b. */
        export function add(a: ReadonlySize2D, b: ReadonlySize2D|number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = a.w + (Is.number(b) ? b : b.w);
            out.h = a.h + (Is.number(b) ? b : b.h);
            return out;
        }

        /** Returns a - b. */
        export function subtract(a: ReadonlySize2D, b: number, out?: Size2D): Size2D;

        /** Returns a - b. */
        export function subtract(a: ReadonlySize2D, b: ReadonlySize2D, out?: Size2D): Size2D;

        /** Returns a - b. */
        export function subtract(a: ReadonlySize2D, b: ReadonlySize2D|number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = a.w - (Is.number(b) ? b : b.w);
            out.h = a.h - (Is.number(b) ? b : b.h);
            return out;
        }

        /** Returns a * b. */
        export function multiply(a: ReadonlySize2D, b: ReadonlyVector2D, out?: Size2D): Size2D;

        /** Returns a * b. */
        export function multiply(a: ReadonlySize2D, b: number, out?: Size2D): Size2D;

        /** Returns a * b. */
        export function multiply(a: ReadonlySize2D, b: ReadonlyVector2D|number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = a.w * (Is.number(b) ? b : b.x);
            out.h = a.h * (Is.number(b) ? b : b.y);
            return out;
        }

        /** Returns a / b. */
        export function divide(a: ReadonlySize2D, b: ReadonlyVector2D, out?: Size2D): Size2D;

        /** Returns a / b. */
        export function divide(a: ReadonlySize2D, b: number, out?: Size2D): Size2D;

        /** Returns a / b. */
        export function divide(a: ReadonlySize2D, b: ReadonlyVector2D|number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = a.w / (Is.number(b) ? b : b.x);
            out.h = a.h / (Is.number(b) ? b : b.y);
            return out;
        }

        /** Applies a direction-agnostic rotation to a given size, returning the resulting bounding box. */
        export function rotate(a: ReadonlySize2D, angle: number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            const w = a.w;
            const h = a.h;
            const cos = Math.cos(angle);
            const sin = Math.sin(angle);
            out.w = Math.abs(cos * w + sin * h);
            out.h = Math.abs(cos * h + sin * w);
            return out;
        }

        /** Skews a size by the given angles in the x and y dimensions, returning the resulting bounding box. */
        export function skew(a: ReadonlySize2D, angles: ReadonlyVector2D, out?: Size2D): Size2D
        {
            out = out || Size2D();

            let dx: number = 0;
            let dy: number = 0;

            if (angles.x != 0)
            {
                const sin = Math.abs(Math.sin(angles.x));
                const cos = Math.abs(Math.cos(angles.x));
               
                dx += sin * a.h;
                dy -= (1 - cos) * a.h;
            }

            if (angles.y != 0)
            {
                const sin = Math.abs(Math.sin(angles.y));
                const cos = Math.abs(Math.cos(angles.y));
                
                dx -= (1 - cos) * a.w;
                dy += sin * a.w;
            }

            out.w = a.w + dx;
            out.h = a.h + dy;

            return out;
        }

        /** Linearly interpolates between two sizes. */
        export function linearInterpolate(a: ReadonlySize2D, b: ReadonlySize2D, mu: number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = Math.linearInterpolate(a.w, b.w, mu);
            out.h = Math.linearInterpolate(a.h, b.h, mu);
            return out;
        }

        /** Gets if two sizes are equal, optionally within a threshold. */
        export function equals(a: ReadonlySize2D, b: ReadonlySize2D, threshold: number = 0.0): boolean
        {
            const diffX = Math.abs(b.w - a.w);
            const diffY = Math.abs(b.h - a.h);
            return diffX <= threshold && diffY <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlySize2D, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = a.w;
            out.h = a.h;
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlySize2D, b: Size2D): void
        {
            b.w = a.w;
            b.h = a.h;
        }

        /** Sets the individual components of a. */
        export function set(a: Size2D, w?: number, h?: number): void
        {
            if (w != null) { a.w = w; }
            if (h != null) { a.h = h; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlySize2D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlySize2D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlySize2D, arr: Float32Array|number[], offset: number = 0): void
        {
            arr[offset++] = a.w;
            arr[offset++] = a.h;
        }

        /** Applies a number function to each component of the given size and returns the resulting size. */
        export function mutate(a: ReadonlySize2D, func: (a: number) => number, out?: Size2D): Size2D
        {
            out = out || Size2D();
            out.w = func(a.w);
            out.h = func(a.h);
            return out;
        }
    }
}

namespace RS
{
    /**
     * Encapsulates a 2D size (width and height).
     * @deprecated use Math.Size2D instead
     */
    export type Dimensions = Math.Size2D;
}