/// <reference path="../Math.ts"/>
/// <reference path="Vector4D.ts" />

namespace RS.Is
{
    export function line3D(value: any): value is Math.Line3D
    {
        return Is.object(value) && Is.number(value.x) && Is.number(value.y) && Is.number(value.z) && Is.number(value.dx) && Is.number(value.dy) && Is.number(value.dz);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable line in 3D space.
     */
    export interface ReadonlyLine3D
    {
        readonly x: number;
        readonly y: number;
        readonly z: number;
        readonly dx: number;
        readonly dy: number;
        readonly dz: number;
    }

    /**
     * Encapsulates a line in 3D space.
     */
    export interface Line3D extends ReadonlyLine3D
    {
        x: number;
        y: number;
        z: number;
        dx: number;
        dy: number;
        dz: number;
    }

    /** Creates an undefined 3D line. */
    export function Line3D(): Line3D;

    /** Creates a new 3D line with the specified point and direction. */
    export function Line3D(point: Math.ReadonlyVector3D, direction: Math.ReadonlyVector3D): Line3D;

    /** Creates a new 3D line with the specified point and direction. */
    export function Line3D(x: number, y: number, z: number, dx: number, dy: number, dz: number): Line3D;

    export function Line3D(p1?: ReadonlyVector3D | number, p2?: ReadonlyVector3D | number, p3?: number, p4?: number, p5?: number, p6?: number): Line3D
    {
        if (Is.vector3D(p1))
        {
            const d = p2 as ReadonlyVector3D;
            return { x: p1.x, y: p1.y, z: p1.z, dx: d.x, dy: d.y, dz: d.z };
        }
        else if (p1 != null)
        {
            return { x: p1, y: p2 as number, z: p3, dx: p4, dy: p5, dz: p6 };
        }
        else
        {
            return { x: 0, y: 0, z: 0, dx: 0, dy: 0, dz: 0 };
        }
    }

    /**
     * Convenience methods for Line3D math.
     */
    export namespace Line3D
    {
        const tmpVec1 = Math.Vector3D(), tmpVec2 = Math.Vector3D();

        /** Retrieves a.d[idx]. */
        export function getDirComponent(a: ReadonlyLine3D, idx: number): number
        {
            switch (idx)
            {
                case 0: return a.dx;
                case 1: return a.dy;
                case 2: return a.dz;
                default: throw new Error("Invalid index");
            }
        }

        /** Sets a.d[idx]. */
        export function setDirComponent(a: Line3D, idx: number, value: number): number
        {
            switch (idx)
            {
                case 0: return a.dx = value;
                case 1: return a.dy = value;
                case 2: return a.dz = value;
                default: throw new Error("Invalid index");
            }
        }

        /**
         * Gets the direction of the specified line.
         * @param line 
         * @param out 
         */
        export function direction(line: ReadonlyLine3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            out.x = line.dx;
            out.y = line.dy;
            out.z = line.dz;
            return out;
        }
        
        /**
         * Finds a line that passes through two points.
         * @param a 
         * @param b 
         * @param out 
         */
        export function fromPoints(a: ReadonlyVector3D, b: ReadonlyVector3D, out?: Line3D): Line3D
        {
            out = out || Line3D();
            Vector3D.copy(a, out);
            out.dx = b.x - a.x;
            out.dy = b.y - a.y;
            out.dz = b.z - a.z;
            return out;
        }

        const quadrant: number[] = [0, 0, 0];
        const maxT: number[] = [0, 0, 0];
        const candidatePlane: number[] = [0, 0, 0];
        const RIGHT = 0, LEFT = 1, MIDDLE = 2;

        /**
         * Tests if a line intersects the specified plane.
         * If outPt is set, the point of intersection (if any) will be written there.
         * @param plane 
         * @param outPt 
         */
        export function intersects(line: ReadonlyLine3D, plane: ReadonlyPlane, outPt?: Math.Vector3D): boolean;

        /**
         * Tests if a line intersects the specified AABB.
         * If outPt is set, the (first) point of intersection (if any) will be written there.
         * @param plane 
         * @param outPt 
         */
        export function intersects(line: ReadonlyLine3D, aabb: ReadonlyAABB3D, outPt?: Math.Vector3D): boolean;

        export function intersects(line: ReadonlyLine3D, target: ReadonlyPlane | ReadonlyAABB3D, outPt?: Math.Vector3D): boolean
        {
            if (Is.plane(target))
            {
                const dir = direction(line, tmpVec1);
                const dDotN = Vector3D.dot(dir, target);
                if (abs(dDotN) < 0.00001) { return false; }
                if (outPt)
                {
                    const e = (target.w - Math.Vector3D.dot(line, target)) / dDotN;
                    Vector3D.addMultiply(line, dir, e, outPt);
                }
                return true;
            }
            else
            {
                // https://web.archive.org/web/20090803054252/http://tog.acm.org/resources/GraphicsGems/gems/RayBox.c

                let inside = true;

                // Find candidate planes
                for (let i = 0; i < 3; ++i)
                {
                    const origin = Vector3D.getComponent(line, i);
                    const minB = AABB3D.getMinComponent(target, i);
                    const maxB = AABB3D.getMaxComponent(target, i);
                    if (origin < minB)
                    {
                        quadrant[i] = LEFT;
                        candidatePlane[i] = minB;
                        inside = false;
                    }
                    else if (origin > maxB)
                    {
                        quadrant[i] = RIGHT;
                        candidatePlane[i] = maxB;
                        inside = false;
                    }
                    else
                    {
                        quadrant[i] = MIDDLE;
                        candidatePlane[i] = 0.0;
                    }
                }

                // Ray origin inside bounding box
                if (inside)
                {
                    if (outPt) { Vector3D.copy(line, outPt); }
                    return true;
                }

                // Calculate T distances to candidate planes
                for (let i = 0; i < 3; ++i)
                {
                    const origin = Vector3D.getComponent(line, i);
                    const dir = getDirComponent(line, i);
                    if (quadrant[i] !== MIDDLE && dir !== 0.0)
                    {
                        maxT[i] = (candidatePlane[i] - origin) / dir;
                    }
                    else
                    {
                        maxT[i] = -1.0;
                    }
                }

                // Get largest of the maxT's for final choice of intersection
                let whichPlane = 0;
                for (let i = 1; i < 3; ++i)
                {
                    if (maxT[whichPlane] < maxT[i])
                    {
                        whichPlane = i;
                    }
                }

                // Check final candidate actually inside box
                if (maxT[whichPlane] < 0.0) { return false; }
                for (let i = 0; i < 3; ++i)
                {
                    if (whichPlane !== i)
                    {
                        const origin = Vector3D.getComponent(line, i);
                        const minB = AABB3D.getMinComponent(target, i);
                        const maxB = AABB3D.getMaxComponent(target, i);
                        const dir = getDirComponent(line, i);
                        const p = origin + maxT[whichPlane] * dir;
                        if (p < minB || p > maxB)
                        {
                            return false;
                        }
                        if (outPt) { Vector3D.setComponent(outPt, i, p); }
                    }
                    else
                    {
                        if (outPt) { Vector3D.setComponent(outPt, i, candidatePlane[i]); }
                    }
                }
                return true;
            }
        }

        /**
         * Finds the shortest distance from the specified point to the line.
         * @param line 
         * @param point 
         */
        export function distanceToPoint(line: ReadonlyLine3D, point: ReadonlyVector3D): number
        {
            const projPt = project(line, point, tmpVec1);
            return Vector3D.distanceBetween(projPt, point);
        }

        /**
         * Projects the specified point onto the line.
         * @param line 
         * @param point 
         * @param out 
         */
        export function project(line: ReadonlyLine3D, point: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();
            const ap = Vector3D.subtract(point, line, tmpVec1);
            const ab = direction(line, tmpVec2);
            const apDab = Vector3D.dot(ap, ab);
            const abDab = Vector3D.dot(ab, ab);
            Vector3D.addMultiply(line, ab, apDab / abDab, out);
            return out;
        }

        /**
         * Projects the specified point onto the line, returning a multiple of the line's direction.
         * @param line 
         * @param point 
         */
        export function projectLambda(line: ReadonlyLine3D, point: ReadonlyVector3D): number
        {
            const ap = Vector3D.subtract(point, line, tmpVec1);
            const ab = direction(line, tmpVec2);
            const apDab = Vector3D.dot(ap, ab);
            const abDab = Vector3D.dot(ab, ab);
            return apDab / abDab;
        }

        /** Linearly interpolates between two lines. */
        export function linearInterpolate(a: ReadonlyLine3D, b: ReadonlyLine3D, mu: number, out?: Line3D): Line3D
        {
            out = out || Line3D();
            out.x = Math.linearInterpolate(a.x, b.x, mu);
            out.y = Math.linearInterpolate(a.y, b.y, mu);
            out.z = Math.linearInterpolate(a.z, b.z, mu);
            out.dx = Math.linearInterpolate(a.dx, b.dx, mu);
            out.dy = Math.linearInterpolate(a.dy, b.dy, mu);
            out.dz = Math.linearInterpolate(a.dz, b.dz, mu);
            return out;
        }

        /** Gets if two lines are equal, optionally within a threshold. */
        export function equals(a: ReadonlyLine3D, b: ReadonlyLine3D, threshold: number = 0.0): boolean
        {
            return abs(b.x - a.x) <= threshold
                && abs(b.y - a.y) <= threshold
                && abs(b.z - a.z) <= threshold
                && abs(b.dx - a.dx) <= threshold
                && abs(b.dy - a.dy) <= threshold
                && abs(b.dz - a.dz) <= threshold;
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyLine3D, out?: Line3D): Line3D
        {
            if (out)
            {
                out.x = a.x;
                out.y = a.y;
                out.z = a.z;
                out.dx = a.dx;
                out.dy = a.dy;
                out.dz = a.dz;
            }
            else
            {
                out = Line3D(a.x, a.y, a.z, a.dx, a.dy, a.dz);
            }
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyLine3D, b: Line3D): void
        {
            b.x = a.x;
            b.y = a.y;
            b.z = a.z;
            b.dx = a.dx;
            b.dy = a.dy;
            b.dz = a.dz;
        }

        /** Sets the individual components of a. */
        export function set(a: Line3D, point?: Math.ReadonlyVector3D, dir?: Math.ReadonlyVector3D): void
        {
            if (point)
            {
                a.x = point.x;
                a.y = point.y;
                a.z = point.z;
            }
            if (dir)
            {
                a.dx = dir.x;
                a.dy = dir.y;
                a.dz = dir.z;
            }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyLine3D, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyLine3D, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyLine3D, arr: Float32Array | number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.z;
            arr[offset++] = a.dx;
            arr[offset++] = a.dy;
            arr[offset++] = a.dz;
        }
    }
}