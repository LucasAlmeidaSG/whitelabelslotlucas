/// <reference path="Vector3D.ts" />
/// <reference path="Quaternion.ts" />
/// <reference path="Matrix3.ts" />

namespace RS.Is
{
    /** Gets if the specified value is a Matrix4. */
    export function matrix4(value: any): value is Math.Matrix4
    {
        if (value == null) { return false; }
        if (!Array.isArray(value)) { return false; }
        for (let i = 0; i < 16; ++i)
        {
            if (!Is.number(value[i])) { return false; }
        }
        return true;
    }
}

namespace RS.Math
{
    const epsilon = 0.00001;
    const tmpVec3 = Vector3D();
    const tmpMtx3 = Matrix3();

    function is2DArray<T>(arr: T[] | T[][]): arr is T[][]
    {
        if (!Array.isArray(arr)) { return false; }
        if (arr.length === 0) { return false; }
        for (let i = 0; i < arr.length; i++)
        {
            if (!Array.isArray(arr[i])) { return false; }
        }
        return true;
    }

    /** A 4x4 matrix. Column major order. */
    export interface ReadonlyMatrix4
    {
        readonly 0: number; readonly 1: number; readonly 2: number; readonly 3: number;
        readonly 4: number; readonly 5: number; readonly 6: number; readonly 7: number;
        readonly 8: number; readonly 9: number; readonly 10: number; readonly 11: number;
        readonly 12: number; readonly 13: number; readonly 14: number; readonly 15: number;
        readonly length: 16;
    }

    /** A 4x4 matrix. Column major order. */
    export interface Matrix4 extends ReadonlyMatrix4
    {
        0: number; 1: number; 2: number; 3: number;
        4: number; 5: number; 6: number; 7: number;
        8: number; 9: number; 10: number; 11: number;
        12: number; 13: number; 14: number; 15: number;
        readonly length: 16;
    }

    const pool: Matrix4[] = [];

    /** Creates a new matrix from the specified array. */
    export function Matrix4(src: Float32Array, offset?: number): Matrix4;

    /** Creates a new matrix from the specified array. */
    export function Matrix4(src: number[], offset?: number): Matrix4;

    /** Creates a new matrix from the specified 2D array. */
    export function Matrix4(src: number[][]): Matrix4;

    /** Creates a new empty matrix. */
    export function Matrix4(): Matrix4;

    export function Matrix4(p1?: Float32Array | number[] | number[][], p2?: number): Matrix4
    {
        const mtx = pool.length > 0 ? pool.pop() : new Array<number>(16) as any as Matrix4;

        if (p1 == null)
        {
            for (let i = 0; i < 16; ++i)
            {
                mtx[i] = 0;
            }
        }
        else if (p1 instanceof Float32Array)
        {
            Matrix4.setFromArray(mtx, p1, p2 || 0);
        }
        else if (is2DArray<number>(p1))
        {
            mtx[0] = p1[0][0];
            mtx[1] = p1[0][1];
            mtx[2] = p1[0][2];
            mtx[3] = p1[0][3];

            mtx[4] = p1[1][0];
            mtx[5] = p1[1][1];
            mtx[6] = p1[1][2];
            mtx[7] = p1[1][3];

            mtx[8] = p1[2][0];
            mtx[9] = p1[2][1];
            mtx[10] = p1[2][2];
            mtx[11] = p1[2][3];

            mtx[12] = p1[3][0];
            mtx[13] = p1[3][1];
            mtx[14] = p1[3][2];
            mtx[15] = p1[3][3];
        }
        else if (Array.isArray(p1))
        {
            Matrix4.setFromArray(mtx, p1, p2 || 0);
        }

        return mtx;
    }

    export namespace Matrix4
    {
        /** Populates a matrix from the specified array. */
        export function setFromArray(mtx: Matrix4, arr: number[], offset?: number): void;

        /** Populates a matrix from the specified array. */
        export function setFromArray(mtx: Matrix4, arr: Float32Array, offset?: number);

        export function setFromArray(mtx: Matrix4, arr: number[] | Float32Array, offset: number = 0)
        {
            mtx[0] = arr[offset + 0];
            mtx[1] = arr[offset + 1];
            mtx[2] = arr[offset + 2];
            mtx[3] = arr[offset + 3];

            mtx[4] = arr[offset + 4];
            mtx[5] = arr[offset + 5];
            mtx[6] = arr[offset + 6];
            mtx[7] = arr[offset + 7];

            mtx[8] = arr[offset + 8];
            mtx[9] = arr[offset + 9];
            mtx[10] = arr[offset + 10];
            mtx[11] = arr[offset + 11];

            mtx[12] = arr[offset + 12];
            mtx[13] = arr[offset + 13];
            mtx[14] = arr[offset + 14];
            mtx[15] = arr[offset + 15];
        }

        /** Identity matrix. */
        export const identity: ReadonlyMatrix4 = Matrix4([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);

        /** Returns a * b. */
        export function mul(a: ReadonlyMatrix4, b: ReadonlyMatrix4, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();

            let a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3];
            let a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7];
            let a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11];
            let a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

            // Cache only the current line of the second matrix
            let b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
            out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
            out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
            out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
            out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

            b0 = b[4]; b1 = b[5]; b2 = b[6]; b3 = b[7];
            out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
            out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
            out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
            out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

            b0 = b[8]; b1 = b[9]; b2 = b[10]; b3 = b[11];
            out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
            out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
            out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
            out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

            b0 = b[12]; b1 = b[13]; b2 = b[14]; b3 = b[15];
            out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
            out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
            out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
            out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

            return out;
        }

        /**
         * Combines a set of transformation matrices into a single matrix via multiplication (e.g. arr[0] * ... * arr[n]).
         * When used to transform a point, the transformations will be applied in the same order of the matrix.
         * e.g. "combine([scale, translate]) * p" will scale p and then translate the scaled p.
         */
        export function combine(arr: ReadonlyMatrix4[], out?: Matrix4): Matrix4
        {
            out = out || Matrix4();
            copy(identity, out);
            const tmp = Matrix4();
            //for (let i = arr.length - 1; i >= 0; --i)
            for (let i = 0, l = arr.length; i < l; ++i)
            {
                copy(out, tmp);
                mul(arr[i], tmp, out);
            }
            free(tmp);
            return out;
        }

        /** Transforms the specified positional vector using this matrix. */
        export function transform(mtx: ReadonlyMatrix4, vec: ReadonlyVector3D, out?: Vector3D): Vector3D;

        /** Transforms the specified positional vector using this matrix. */
        export function transform(mtx: ReadonlyMatrix4, vec: ReadonlyVector4D, out?: Vector4D): Vector4D;

        export function transform(mtx: ReadonlyMatrix4, vec: ReadonlyVector3D | ReadonlyVector4D, out?: Vector3D | Vector4D): Vector3D | Vector4D
        {
            if (Is.vector4D(vec))
            {
                const out4 = (out || Vector4D()) as Vector4D;

                out4.x = mtx[0] * vec.x + mtx[4] * vec.y + mtx[8] * vec.z + mtx[12] * vec.w;
                out4.y = mtx[1] * vec.x + mtx[5] * vec.y + mtx[9] * vec.z + mtx[13] * vec.w;
                out4.z = mtx[2] * vec.x + mtx[6] * vec.y + mtx[10] * vec.z + mtx[14] * vec.w;
                out4.w = mtx[3] * vec.x + mtx[7] * vec.y + mtx[11] * vec.z + mtx[15] * vec.w;

                return out4;
            }
            else
            {
                out = out || Vector3D();

                out.x = mtx[0] * vec.x + mtx[4] * vec.y + mtx[8] * vec.z + mtx[12];
                out.y = mtx[1] * vec.x + mtx[5] * vec.y + mtx[9] * vec.z + mtx[13];
                out.z = mtx[2] * vec.x + mtx[6] * vec.y + mtx[10] * vec.z + mtx[14];

                return out;
            }
        }

        /** Transforms the specified normal vector using this matrix. */
        export function transformNormal(mtx: ReadonlyMatrix4, vec: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            out = out || Vector3D();

            out.x = mtx[0] * vec.x + mtx[4] * vec.y + mtx[8] * vec.z;
            out.y = mtx[1] * vec.x + mtx[5] * vec.y + mtx[9] * vec.z;
            out.z = mtx[2] * vec.x + mtx[6] * vec.y + mtx[10] * vec.z;

            return out;
        }

        /** Linearly interpolates between two matrices. */
        export function linearInterpolate(a: ReadonlyMatrix4, b: ReadonlyMatrix4, mu: number, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();
            for (let i = 0; i < 16; ++i)
            {
                out[i] = Math.linearInterpolate(a[i], b[i], mu);
            }
            return out;
        }

        /** Decomposes a transform matrix into position, rotation and scale. */
        export function decompose(a: ReadonlyMatrix4, outPosition: Vector3D, outRotation: Quaternion, outScale: Vector3D): void
        {
            outPosition.x = a[12];
            outPosition.y = a[13];
            outPosition.z = a[14];
            tmpVec3.x = a[0];
            tmpVec3.y = a[1];
            tmpVec3.z = a[2];
            outScale.x = Vector3D.size(tmpVec3);
            tmpVec3.x = a[4];
            tmpVec3.y = a[5];
            tmpVec3.z = a[6];
            outScale.y = Vector3D.size(tmpVec3);
            tmpVec3.x = a[8];
            tmpVec3.y = a[9];
            tmpVec3.z = a[10];
            outScale.z = Vector3D.size(tmpVec3);
            const det = Matrix4.determinant(a);
            if (det < 0.0) { outScale.x *= -1.0; }
            tmpMtx3[0] = a[0] / outScale.x;
            tmpMtx3[1] = a[1] / outScale.x;
            tmpMtx3[2] = a[2] / outScale.x;
            tmpMtx3[3] = a[4] / outScale.y;
            tmpMtx3[4] = a[5] / outScale.y;
            tmpMtx3[5] = a[6] / outScale.y;
            tmpMtx3[6] = a[8] / outScale.z;
            tmpMtx3[7] = a[9] / outScale.z;
            tmpMtx3[8] = a[10] / outScale.z;
            Quaternion.fromMatrix(tmpMtx3, outRotation);
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyMatrix4, arr: number[], offset?: number, transposed?: boolean): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyMatrix4, arr: Float32Array, offset?: number, transposed?: boolean): void;

        export function write(a: ReadonlyMatrix4, arr: Float32Array | number[], offset: number = 0, transposed: boolean = false): void
        {
            if (transposed)
            {
                arr[offset + 0] = a[0];
                arr[offset + 1] = a[4];
                arr[offset + 2] = a[8];
                arr[offset + 3] = a[12];

                arr[offset + 4] = a[1];
                arr[offset + 5] = a[5];
                arr[offset + 6] = a[9];
                arr[offset + 7] = a[13];

                arr[offset + 8] = a[2];
                arr[offset + 9] = a[6];
                arr[offset + 10] = a[10];
                arr[offset + 11] = a[14];

                arr[offset + 12] = a[3];
                arr[offset + 13] = a[7];
                arr[offset + 14] = a[11];
                arr[offset + 15] = a[15];
            }
            else
            {
                arr[offset + 0] = a[0];
                arr[offset + 1] = a[1];
                arr[offset + 2] = a[2];
                arr[offset + 3] = a[3];

                arr[offset + 4] = a[4];
                arr[offset + 5] = a[5];
                arr[offset + 6] = a[6];
                arr[offset + 7] = a[7];

                arr[offset + 8] = a[8];
                arr[offset + 9] = a[9];
                arr[offset + 10] = a[10];
                arr[offset + 11] = a[11];

                arr[offset + 12] = a[12];
                arr[offset + 13] = a[13];
                arr[offset + 14] = a[14];
                arr[offset + 15] = a[15];
            }
        }

        /** Converts mtx to a 1D array. */
        export function toArray(mtx: Matrix4, transposed: boolean = false): number[]
        {
            const arr = new Array<number>(16);
            write(mtx, arr, 0, transposed);
            return arr;
        }

        /** Converts mtx to a typed array. */
        export function toTypedArray(mtx: Matrix4, transposed: boolean = false): Float32Array
        {
            const arr = new Float32Array(16);
            write(mtx, arr, 0, transposed);
            return arr;
        }

        /** Creates a copy of mtx. */
        export function clone(mtx: Matrix4, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();
            copy(mtx, out);
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyMatrix4, b: Matrix4): void
        {
            b[0] = a[0];
            b[1] = a[1];
            b[2] = a[2];
            b[3] = a[3];

            b[4] = a[4];
            b[5] = a[5];
            b[6] = a[6];
            b[7] = a[7];

            b[8] = a[8];
            b[9] = a[9];
            b[10] = a[10];
            b[11] = a[11];

            b[12] = a[12];
            b[13] = a[13];
            b[14] = a[14];
            b[15] = a[15];
        }

        /** Transposes mtx. */
        export function transpose(mtx: ReadonlyMatrix4, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();
            write(mtx, out as any, 0, true);
            return out;
        }

        /** Finds the determinant of mtx. */
        export function determinant(mtx: ReadonlyMatrix4): number
        {
            const a00 = mtx[0], a01 = mtx[1], a02 = mtx[2], a03 = mtx[3];
            const a10 = mtx[4], a11 = mtx[5], a12 = mtx[6], a13 = mtx[7];
            const a20 = mtx[8], a21 = mtx[9], a22 = mtx[10], a23 = mtx[11];
            const a30 = mtx[12], a31 = mtx[13], a32 = mtx[14], a33 = mtx[15];

            const b00 = a00 * a11 - a01 * a10;
            const b01 = a00 * a12 - a02 * a10;
            const b02 = a00 * a13 - a03 * a10;
            const b03 = a01 * a12 - a02 * a11;
            const b04 = a01 * a13 - a03 * a11;
            const b05 = a02 * a13 - a03 * a12;
            const b06 = a20 * a31 - a21 * a30;
            const b07 = a20 * a32 - a22 * a30;
            const b08 = a20 * a33 - a23 * a30;
            const b09 = a21 * a32 - a22 * a31;
            const b10 = a21 * a33 - a23 * a31;
            const b11 = a22 * a33 - a23 * a32;

            return b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
        }

        /** Finds the inverse of mtx. */
        export function inverse(mtx: ReadonlyMatrix4, out?: Matrix4): Matrix4 | null
        {
            const a00 = mtx[0], a01 = mtx[1], a02 = mtx[2], a03 = mtx[3];
            const a10 = mtx[4], a11 = mtx[5], a12 = mtx[6], a13 = mtx[7];
            const a20 = mtx[8], a21 = mtx[9], a22 = mtx[10], a23 = mtx[11];
            const a30 = mtx[12], a31 = mtx[13], a32 = mtx[14], a33 = mtx[15];

            const b00 = a00 * a11 - a01 * a10;
            const b01 = a00 * a12 - a02 * a10;
            const b02 = a00 * a13 - a03 * a10;
            const b03 = a01 * a12 - a02 * a11;
            const b04 = a01 * a13 - a03 * a11;
            const b05 = a02 * a13 - a03 * a12;
            const b06 = a20 * a31 - a21 * a30;
            const b07 = a20 * a32 - a22 * a30;
            const b08 = a20 * a33 - a23 * a30;
            const b09 = a21 * a32 - a22 * a31;
            const b10 = a21 * a33 - a23 * a31;
            const b11 = a22 * a33 - a23 * a32;

            // Calculate the determinant
            let det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

            if (!det) { return null; }
            det = 1.0 / det;

            out = out || Matrix4();

            out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
            out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
            out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
            out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
            out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
            out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
            out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
            out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
            out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
            out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
            out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
            out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
            out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
            out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
            out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
            out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

            return out;
        }

        /** Creates a translation matrix from the specified position. */
        export function createTranslation(translation: ReadonlyVector3D, out?: Matrix4): Matrix4;

        /** Creates a translation matrix from the specified position. */
        export function createTranslation(x: number, y: number, z: number, out?: Matrix4): Matrix4;

        export function createTranslation(p1: number | ReadonlyVector3D, p2?: number | Matrix4, p3?: number, p4?: Matrix4): Matrix4
        {
            // Resolve overload
            let x: number, y: number, z: number;
            let out: Matrix4;
            if (Is.vector3D(p1))
            {
                x = p1.x;
                y = p1.y;
                z = p1.z;
                out = p2 as Matrix4;
            }
            else
            {
                x = p1;
                y = p2 as number;
                z = p3;
                out = p4;
            }

            out = out || Matrix4();

            out[0] = 1;
            out[1] = 0;
            out[2] = 0;
            out[3] = 0;

            out[4] = 0;
            out[5] = 1;
            out[6] = 0;
            out[7] = 0;

            out[8] = 0;
            out[9] = 0;
            out[10] = 1;
            out[11] = 0;

            out[12] = x;
            out[13] = y;
            out[14] = z;
            out[15] = 1;

            return out;
        }

        /** Creates a rotation matrix from the specified euler rotation (radians). */
        export function createRotation(rotation: ReadonlyVector3D, out?: Matrix4): Matrix4;

        /** Creates a rotation matrix from the specified euler rotation (radians). */
        export function createRotation(pitch: number, yaw: number, roll: number, out?: Matrix4): Matrix4;

        /** Creates a rotation matrix from the specified quaternion. */
        export function createRotation(quaternion: ReadonlyQuaternion, out?: Matrix4): Matrix4;

        export function createRotation(p1: number | ReadonlyVector3D | ReadonlyQuaternion, p2?: number | Matrix4, p3?: number, p4?: Matrix4): Matrix4
        {
            // Resolve overload
            let out: Matrix4;
            let pitch: number, yaw: number, roll: number;
            if (Is.quaternion(p1))
            {
                out = p2 as Matrix4 || Matrix4();

                const x = p1.x, y = p1.y, z = p1.z, w = p1.w;
                const x2 = x + x;
                const y2 = y + y;
                const z2 = z + z;

                const xx = x * x2;
                const yx = y * x2;
                const yy = y * y2;
                const zx = z * x2;
                const zy = z * y2;
                const zz = z * z2;
                const wx = w * x2;
                const wy = w * y2;
                const wz = w * z2;

                out[0] = 1 - yy - zz;
                out[1] = yx + wz;
                out[2] = zx - wy;
                out[3] = 0;

                out[4] = yx - wz;
                out[5] = 1 - xx - zz;
                out[6] = zy + wx;
                out[7] = 0;

                out[8] = zx + wy;
                out[9] = zy - wx;
                out[10] = 1 - xx - yy;
                out[11] = 0;

                out[12] = 0;
                out[13] = 0;
                out[14] = 0;
                out[15] = 1;

                return out;
            }
            else if (Is.vector3D(p1))
            {
                out = p2 as Matrix4 || Matrix4();
                pitch = p1.x;
                yaw = p1.y;
                roll = p1.z;
            }
            else
            {
                out = p4 || Matrix4();
                pitch = p1 as number;
                yaw = p2 as number;
                roll = p3 as number;
            }

            // Rotation
            const c = Math.cos;
            const s = Math.sin;
            const cY = c(yaw), sY = s(yaw);
            const cR = c(roll), sR = s(roll);
            const cP = c(pitch), sP = s(pitch);

            out[0] = cR * cY;
            out[1] = sR * cY;
            out[2] = -sY;
            out[3] = 0;

            out[4] = cR * sY * sP - sR * cP;
            out[5] = sR * sY * sP + cR * cP;
            out[6] = cY * sP;
            out[7] = 0;

            out[8] = cR * sY * cP + sR * sP;
            out[9] = sR * sY * cP - cR * sP;
            out[10] = cY * cP;
            out[11] = 0;

            out[12] = 0;
            out[13] = 0;
            out[14] = 0;
            out[15] = 1;

            // Done
            return out;
        }

        /** Creates a scale matrix. */
        export function createScale(scale: ReadonlyVector3D, out?: Matrix4): Matrix4;

        /** Creates a scale matrix. */
        export function createScale(x: number, y: number, z: number, out?: Matrix4): Matrix4;

        /** Creates a scale matrix. */
        export function createScale(scale: number, out?: Matrix4): Matrix4;

        export function createScale(p1: number | ReadonlyVector3D, p2?: number | Matrix4, p3?: number, p4?: Matrix4): Matrix4
        {
            // Resolve overload
            let x: number, y: number, z: number;
            let out: Matrix4;
            if (Is.vector3D(p1))
            {
                x = p1.x;
                y = p1.y;
                z = p1.z;
                out = p2 as Matrix4;
            }
            else if (Is.number(p2))
            {
                x = p1;
                y = p2;
                z = p3;
                out = p4;
            }
            else
            {
                x = p1;
                y = p1;
                z = p1;
                out = p2;
            }

            // Scale
            out = out || Matrix4();

            out[0] = x;
            out[1] = 0;
            out[2] = 0;
            out[3] = 0;

            out[4] = 0;
            out[5] = y;
            out[6] = 0;
            out[7] = 0;

            out[8] = 0;
            out[9] = 0;
            out[10] = z;
            out[11] = 0;

            out[12] = 0;
            out[13] = 0;
            out[14] = 0;
            out[15] = 1;

            return out;
        }

        /**
         * Creates a perspective projection matrix.
         * @param fovY          Vertical field of view in radians.
         * @param nearZ         Near z plane.
         * @param farZ          Far z plane.
         * @param aspectRatio   Width / height.
         */
        export function createPerspective(fovY: number, nearZ: number, farZ: number, aspectRatio: number, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();

            const f = 1.0 / Math.tan(fovY / 2);
            out[0] = f / aspectRatio;
            out[1] = 0;
            out[2] = 0;
            out[3] = 0;

            out[4] = 0;
            out[5] = -f;
            out[6] = 0;
            out[7] = 0;

            out[8] = 0;
            out[9] = 0;
            out[11] = 1;

            out[12] = 0;
            out[13] = 0;
            out[15] = 0;
            if (farZ != null && farZ !== Infinity)
            {
                const nf = 1 / (nearZ - farZ);
                out[10] = (farZ + nearZ) * nf;
                out[14] = (2 * farZ * nearZ) * nf;
            }
            else
            {
                out[10] = -1;
                out[14] = -2 * nearZ;
            }
            out[10] *= -1;

            return out;
        }

        /**
         * Creates an orthographic projection matrix.
         * @param lef           Horizontal field of view in radians.
         * @param nearZ         Near z plane.
         * @param farZ          Far z plane.
         * @param aspectRatio   Width / height.
         */
        export function createOrthographic(left: number, right: number, top: number, bottom: number, far: number, near: number, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();

            const lr = 1 / (left - right);
            const bt = 1 / (bottom - top);
            const nf = 1 / (near - far);
            out[0] = -2 * lr;
            out[1] = 0;
            out[2] = 0;
            out[3] = 0;
            out[4] = 0;
            out[5] = -2 * bt;
            out[6] = 0;
            out[7] = 0;
            out[8] = 0;
            out[9] = 0;
            out[10] = -2 * nf;
            out[11] = 0;
            out[12] = (left + right) * lr;
            out[13] = (top + bottom) * bt;
            out[14] = (far + near) * nf;
            out[15] = 1;

            return out;
        }

        /**
         * Creates a "look-at" matrix, emulating a camera positioned at "pos" and facing towards "lookat".
         * @param pos       Position of the viewpoint.
         * @param lookat    Point to look towards.
         * @param up        Up vector.
         */
        export function createLookat(pos: ReadonlyVector3D, lookat: ReadonlyVector3D, up: ReadonlyVector3D, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();

            let x0, x1, x2, y0, y1, y2, z0, z1, z2, len;
            const eyex = pos.x;
            const eyey = pos.y;
            const eyez = pos.z;
            const upx = up.x;
            const upy = up.y;
            const upz = up.z;
            const centerx = lookat.x;
            const centery = lookat.y;
            const centerz = lookat.z;

            if (Math.abs(eyex - centerx) < epsilon &&
                Math.abs(eyey - centery) < epsilon &&
                Math.abs(eyez - centerz) < epsilon)
            {
                copy(identity, out);
                return out;
            }

            z0 = eyex - centerx;
            z1 = eyey - centery;
            z2 = eyez - centerz;

            len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
            z0 *= len;
            z1 *= len;
            z2 *= len;

            x0 = upy * z2 - upz * z1;
            x1 = upz * z0 - upx * z2;
            x2 = upx * z1 - upy * z0;
            len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
            if (!len)
            {
                x0 = 0;
                x1 = 0;
                x2 = 0;
            }
            else
            {
                len = 1 / len;
                x0 *= len;
                x1 *= len;
                x2 *= len;
            }

            y0 = z1 * x2 - z2 * x1;
            y1 = z2 * x0 - z0 * x2;
            y2 = z0 * x1 - z1 * x0;

            len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
            if (!len)
            {
                y0 = 0;
                y1 = 0;
                y2 = 0;
            }
            else
            {
                len = 1 / len;
                y0 *= len;
                y1 *= len;
                y2 *= len;
            }

            out[0] = x0;
            out[1] = y0;
            out[2] = z0;
            out[3] = 0;
            out[4] = x1;
            out[5] = y1;
            out[6] = z1;
            out[7] = 0;
            out[8] = x2;
            out[9] = y2;
            out[10] = z2;
            out[11] = 0;
            out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
            out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
            out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
            out[15] = 1;

            return out;
        }

        /** Indicates that the specified matrix instance is no longer required and may be reused. */
        export function free(mtx: Matrix4): void
        {
            pool.push(mtx);
        }
    }
}
