namespace RS.Is
{
    export function range(t: any): t is Math.Range
    {
        if (!Is.object(t) || !Is.string(t.kind)) { return false; }
        switch (t.kind)
        {
            case "minMaxRange": return Is.number(t.min) && Is.number(t.max);
            case "emptyRange": return true;
        }
    }
}

namespace RS.Math
{
    /** Creates a new empty range. */
    export function Range(): Range.BaseRange<"emptyRange">;

    /** Creates a range from min (inclusive) to max (inclusive). */
    export function Range(min: number, max: number): Range.MinMaxRange;

    /** Creates a range from -magnitude (inclusive) to +magnitude (inclusive). */
    export function Range(magnitude: number): Range.MinMaxRange;

    export function Range(p1?: number, max?: number): Range
    {
        if (p1 != null)
        {
            let min: number;
            if (max == null)
            {
                min = -p1;
                max = p1;
            }
            else
            {
                min = p1;
            }

            return {
                kind: "minMaxRange",
                min, max
            };
        }
        else
        {
            return {
                kind: "emptyRange"
            };
        }
    }

    export namespace Range
    {
        // #region Types

        export interface BaseRange<T extends string> { kind: T; }

        /** Encapsulates a range from a minimum to a maximum value. */
        export interface MinMaxRange extends BaseRange<"minMaxRange">
        {
            /** Smallest possible value of this range (inclusive). */
            min: number;
            /** Largest possible value of this range (inclusive). */
            max: number;
        }

        // #endregion

        // #region Picking Functions

        /**
         * Picks a number at random within the specified range.
         */
        export function pickNumber(value: Range): number;
        /**
         * Picks an number at random within the specified value using the specified RNG.
         */
        export function pickNumber(value: Range, rng: MersenneTwister): number;
        
        export function pickNumber(value: Range, rng?: MersenneTwister): number
        {
            switch (value.kind)
            {
                case "emptyRange":
                {
                    return NaN;
                }
                case "minMaxRange":
                {
                    const rand = rng == null ? random(true, true) : rng.randomIncl();
                    return value.min + rand * (value.max - value.min);
                }
                // default:
                // {
                //     Log.error(`Unknown value kind '${value.kind}'`);
                //     return NaN;
                // }
            }
        }

        /**
         * Picks an integer at random within the specified range.
         */
        export function pickInteger(value: Range): number;

        /**
         * Picks an integer at random within the specified value using the specified RNG.
         */
        export function pickInteger(value: Range, rng: MersenneTwister): number;

        export function pickInteger(value: Range, rng?: MersenneTwister): number
        {
            switch (value.kind)
            {
                case "emptyRange":
                {
                    return NaN;
                }
                case "minMaxRange":
                {
                    if (rng != null)
                    {
                        return rng.randomIntRange(value.min, value.max + 1);
                    }
                    else
                    {
                        return generateRandomInt(value.min, value.max + 1);
                    }
                }
                // default:
                // {
                //     Log.error(`Unknown value kind '${value.kind}'`);
                //     return NaN;
                // }
            }
        }

        /**
         * Picks multiple integers at random within the specified range.
         */
        export function pickIntegers(count: number, value: Range): number[];

        /**
         * Picks an integer at random within the specified range using the specified RNG.
         */
        export function pickIntegers(count: number, value: Range, rng: MersenneTwister): number[];

        export function pickIntegers(count: number, value: Range, rng?: MersenneTwister): number[]
        {
            const result = new Array<number>(count);
            for (let i = 0; i < count; i++)
            {
                result[i] = rng != null ? pickInteger(value, rng) : pickInteger(value);
            }
            return result;
        }

        /**
         * Picks multiple integers at random within the specified range that add up exactly to a sum.
         */
        export function pickIntegersToSum(sum: number, value: Range): number[]|null;

        /**
         * Picks multiple integers at random within the specified range using the specified RNG that add up exactly to a sum.
         */
        export function pickIntegersToSum(sum: number, value: Range, rng: MersenneTwister): number[]|null;

        export function pickIntegersToSum(sum: number, value: Range, rng?: MersenneTwister): number[]|null
        {
            const randomIntRange = rng ? rng.randomIntRange.bind(rng) : generateRandomInt;

            const result: number[] = [];
            let curSum: number = 0;
            let noMorePicks: boolean = false;
            let itCnt = 0;
            while (curSum !== sum)
            {
                // Infinite loop protection
                itCnt++;
                if (itCnt >= 512)
                {
                    // Something went wrong
                    RS.Log.error(`pickIntegersToSum: Infinite loop detected`);
                    debugger;
                    return null;
                }

                // Where are we?
                if (curSum > sum)
                {
                    const candidateIndices: number[] = [];

                    // We've exceeded the sum, try and lower a value
                    for (let i = 0; i < result.length; i++)
                    {
                        // Is this value not at the start of the range?
                        if (!isAtStartOf(result[i], value))
                        {
                            candidateIndices.push(i);
                        }
                    }
                    if (candidateIndices.length === 0)
                    {
                        if (result.length <= 1)
                        {
                            // We can't resolve this
                            RS.Log.error(`pickIntegersToSum: Cannot resolve`);
                            debugger;
                            return null;
                        }

                        // Splice one out
                        const removeIdx = randomIntRange(0, result.length);
                        sum -= result[removeIdx];
                        result.splice(removeIdx, 1);
                    }
                    else
                    {
                        // Pick a candidate to decrement
                        const pickedValue = candidateIndices[randomIntRange(0, candidateIndices.length)];
                        const oldVal = result[pickedValue];
                        const newVal = decrement(oldVal, value);
                        if (oldVal === newVal)
                        {
                            // Failed somehow
                            RS.Log.error(`pickIntegersToSum: Failed to decrement`);
                            debugger;
                            return null;
                        }
                        else
                        {
                            // Apply
                            curSum -= oldVal;
                            result[pickedValue] = newVal;
                            curSum += newVal;
                        }
                    }
                    
                    // We've had to cut down, so don't pick any more
                    noMorePicks = true;
                }
                else if (!noMorePicks)
                {
                    // We're below the sum, pick a new integer
                    const newVal = rng != null ? pickInteger(value, rng) : pickInteger(value);
                    result.push(newVal);
                    curSum += newVal;
                }
                else
                {
                    // We're below the sum but not allowed to make more picks, try and increase a value
                    const candidateIndices: number[] = [];
                    for (let i = 0; i < result.length; i++)
                    {
                        // Is this value not at the start of the range?
                        if (!isAtEndOf(result[i], value))
                        {
                            candidateIndices.push(i);
                        }
                    }
                    if (candidateIndices.length === 0)
                    {
                        // We can't resolve this
                        RS.Log.error(`pickIntegersToSum: Cannot resolve`);
                        debugger;
                        return null;
                    }
                    else
                    {
                        // Pick a candidate to increment
                        const pickedValue = candidateIndices[randomIntRange(0, candidateIndices.length)];
                        const oldVal = result[pickedValue];
                        const newVal = increment(oldVal, value);
                        if (oldVal === newVal)
                        {
                            // Failed somehow
                            RS.Log.error(`pickIntegersToSum: Failed to increment`);
                            debugger;
                            return null;
                        }
                        else
                        {
                            // Apply
                            curSum -= oldVal;
                            result[pickedValue] = newVal;
                            curSum += newVal;
                        }
                    }
                }
            }

            // Check our results
            if (result.some((i) => !isWithin(i, value)))
            {
                // Something went wrong
                RS.Log.error(`pickIntegersToSum: One or more values failed the isWithin check`);
                debugger;
                return null;
            }

            // Done
            return result;
        }

        // #endregion

        // #region Operation Functions

        /**
         * Attempts to increment the specified value while remaining in-range. Upon failure, returns the same value passed in.
         */
        export function increment(value: number, rangeValue: Range): number
        {
            switch (rangeValue.kind)
            {
                case "emptyRange":
                {
                    // No solution
                    return value;
                }
                case "minMaxRange":
                {
                    value++;
                    return isWithin(value, rangeValue) ? value : (value - 1);
                }
                // default:
                // {
                //     RS.Log.error(`Unknown range kind '${rangeValue.kind}'`);
                //     return value;
                // }
            }
        }

        /**
         * Attempts to decrement the specified value while remaining in-range. Upon failure, returns the same value passed in.
         */
        export function decrement(value: number, rangeValue: Range): number
        {
            switch (rangeValue.kind)
            {
                case "emptyRange":
                {
                    // No solution
                    return value;
                }
                case "minMaxRange":
                {
                    value--;
                    return isWithin(value, rangeValue) ? value : (value + 1);
                }
                // default:
                // {
                //     RS.Log.error(`Unknown range kind '${rangeValue.kind}'`);
                //     return value;
                // }
            }
        }

        /**
         * Clamps the specified value to within the range.
         * @param value
         * @param rangeValue
         */
        export function clamp(value: number, rangeValue: Range): number
        {
            switch (rangeValue.kind)
            {
                case "emptyRange":
                {
                    // No solution
                    return value;
                }
                case "minMaxRange":
                {
                    return Math.clamp(value, rangeValue.min, rangeValue.max);
                }
                // default:
                // {
                //     RS.Log.error(`Unknown range kind '${rangeValue.kind}'`);
                //     return value;
                // }
            }
        }

        // #endregion

        // #region Testing Functions

        /**
         * Gets if the specified value is within the range.
         * @param value
         * @param rangeValue
         */
        export function isWithin(value: number, rangeValue: Range): boolean
        {
            switch (rangeValue.kind)
            {
                case "emptyRange":
                {
                    return false;
                }
                case "minMaxRange":
                {
                    return value >= rangeValue.min && value <= rangeValue.max;
                }
                // default:
                // {
                //     RS.Log.error(`Unknown range kind '${rangeValue.kind}'`);
                //     return NaN;
                // }
            }
        }

        /**
         * Gets if the specified value is at the END of the range.
         * @param value
         * @param rangeValue
         */
        export function isAtEndOf(value: number, rangeValue: Range): boolean
        {
            switch (rangeValue.kind)
            {
                case "emptyRange":
                {
                    return false;
                }
                case "minMaxRange":
                {
                    return value === rangeValue.max;
                }
                // default:
                // {
                //     RS.Log.error(`Unknown range kind '${rangeValue.kind}'`);
                //     return NaN;
                // }
            }
        }

        /**
         * Gets if the specified value is at the START of the range.
         * @param value
         * @param rangeValue
         */
        export function isAtStartOf(value: number, rangeValue: Range): boolean
        {
            switch (rangeValue.kind)
            {
                case "emptyRange":
                {
                    return false;
                }
                case "minMaxRange":
                {
                    return value === rangeValue.min;
                }
                // default:
                // {
                //     RS.Log.error(`Unknown range kind '${rangeValue.kind}'`);
                //     return NaN;
                // }
            }
        }

        // #endregion
    }

    export type Range = Range.BaseRange<"emptyRange"> | Range.MinMaxRange;

    // Legacy.
    export const range = Range;
}