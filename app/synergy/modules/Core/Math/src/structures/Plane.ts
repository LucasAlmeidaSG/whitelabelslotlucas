/// <reference path="../Math.ts"/>
/// <reference path="Vector4D.ts" />
/// <reference path="Matrix4.ts" />

namespace RS.Is
{
    export function plane(value: any): value is Math.Plane
    {
        return Is.vector4D(value);
    }
}

namespace RS.Math
{
    /**
     * Encapsulates an immutable plane in 3D space.
     */
    export type ReadonlyPlane = ReadonlyVector4D;

    /**
     * Encapsulates a plane in 3D space.
     */
    export type Plane = Vector4D;

    /** Creates an undefined plane. */
    export function Plane(): Plane;

    /** Creates a new plane with the specified normal and constant. */
    export function Plane(normal: ReadonlyVector3D, c: number): Plane;

    /** Creates a new plane with the specified normal and offset. */
    export function Plane(nx: number, ny: number, nz: number, c: number): Plane;

    export function Plane(p1?: ReadonlyVector3D | number, p2?: number, p3?: number, p4?: number): Plane
    {
        if (Is.vector3D(p1))
        {
            return { x: p1.x, y: p1.y, z: p1.z, w: p2 };
        }
        else if (p1 != null)
        {
            return { x: p1, y: p2, z: p3, w: p4 };
        }
        else
        {
            return { x: 0, y: 0, z: 0, w: 0 };
        }
    }

    /**
     * Convenience methods for Plane math.
     */
    export namespace Plane
    {
        const tmpVec1 = Vector3D(), tmpVec2 = Vector3D();

        const orthoX = Matrix4.createRotation(Math.Angles.quarterCircle, 0.0, 0.0);
        const orthoY = Matrix4.createRotation(0.0, Math.Angles.quarterCircle, 0.0);

        /**
         * Creates a plane from the specified coplanar points.
         * @param p1 
         * @param p2 
         * @param p3 
         * @param out 
         */
        export function fromCoplanarPoints(p1: ReadonlyVector3D, p2: ReadonlyVector3D, p3: ReadonlyVector3D, out?: Plane): Plane
        {
            const d1 = Vector3D.subtract(p2, p1);
            const d2 = Vector3D.subtract(p3, p1);
            const n = Vector3D.cross(d2, d1);
            return fromNormalAndCoplanarPoint(n, p1, out);
        }

        /**
         * Creates a plane from a normal and a point on the plane.
         * @param n 
         * @param p 
         * @param out 
         */
        export function fromNormalAndCoplanarPoint(n: ReadonlyVector3D, p: ReadonlyVector3D, out?: Plane): Plane
        {
            out = out || Plane();
            out.x = n.x;
            out.y = n.y;
            out.z = n.z;
            out.w = -Vector3D.dot(p, n);
            return out;
        }

        /**
         * Flips the orientation of a plane in-place.
         * @param a 
         */
        export function flip(a: Plane): void
        {
            Vector4D.multiply(a, -1.0, a);
        }

        /**
         * Flips the orientation of a plane.
         * @param a 
         * @param out 
         */
        export function flipped(a: ReadonlyPlane, out?: Plane): Plane
        {
            out = out || Plane();
            Vector4D.multiply(a, -1.0, out);
            return out;
        }

        /**
         * Finds the shortest distance from the specified point to the plane.
         * @param plane 
         * @param point 
         */
        export function distanceToPoint(plane: ReadonlyPlane, point: ReadonlyVector3D): number
        {
            return Vector3D.dot(plane, point) + plane.w;
        }

        /**
         * Projects the specified point onto the plane.
         * @param plane 
         * @param point 
         * @param out 
         */
        export function project(plane: ReadonlyPlane, point: ReadonlyVector3D, out?: Vector3D): Vector3D
        {
            const d = distanceToPoint(plane, point);
            out = out || Vector3D();
            Vector3D.addMultiply(point, plane, -d, out);
            return out;
        }

        /**
         * Finds two orthogonal normals along the specified plane.
         * @param plane 
         * @param out1 
         * @param out2 
         */
        export function findOrthoNormals(plane: ReadonlyPlane, out1: Vector3D, out2: Vector3D): void
        {
            const normal = tmpVec1;
            Vector3D.copy(plane, normal);
            const w = Matrix4.transform(orthoX, normal, tmpVec2);
            const dot = Vector3D.dot(normal, w);
            if (Math.abs(dot) > 0.6)
            {
                Matrix4.transform(orthoY, normal, w);
            }
            Vector3D.normalise(w);
        
            Vector3D.cross(normal, w, out1);
            Vector3D.normalise(out1);
            Vector3D.cross(normal, out1, out2);
            Vector3D.normalise(out2);
        }

        /**
         * Transforms a plane using the specified matrix.
         * @param plane 
         * @param matrix 
         * @param out 
         */
        export function transform(plane: ReadonlyPlane, matrix: ReadonlyMatrix4, out?: Plane): Plane
        {
            out = out || Plane();
            Matrix4.transformNormal(matrix, plane, out);
            Vector3D.multiply(plane, plane.w, tmpVec1);
            Matrix4.transform(matrix, tmpVec1, tmpVec2);
            out.w = Vector3D.dot(tmpVec2, out);
            return out;
        }

        /**
         * Creates a matrix that projects points perpendicularly onto the plane.
         * Transforming vectors through this matrix is the equivalent of calling project on them.
         * @param plane
         * @param out 
         */
        export function createFlatProjection(plane: ReadonlyPlane, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();
            const n1 = plane.x, n2 = plane.y, n3 = plane.z, c = plane.w;

            out[0] = 1 - n1 * n1;
            out[4] = -n1 * n2;
            out[8] = -n1 * n3;
            out[12] = -n1 * c;

            out[1] = -n1 * n2;
            out[5] = 1 - n2 * n2;
            out[9] = -n1 * n3;
            out[13] = -n2 * c;

            out[2] = -n3 * n1;
            out[6] = -n3 * n2;
            out[10] = 1 - n3 * n3;
            out[14] = -n3 * c;

            out[3] = 0;
            out[7] = 0;
            out[11] = 0;
            out[15] = 1;

            return out;
        }

        /**
         * Creates a matrix that projects points along a direction vector onto the plane (e.g. a directional light).
         * @param direction
         * @param out 
         */
        export function createDirectionalProjection(plane: ReadonlyPlane, direction: ReadonlyVector3D, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();
            const n1 = plane.x, n2 = plane.y, n3 = plane.z, c = plane.w;
            const d1 = direction.x, d2 = direction.y, d3 = direction.z;
            const divisor = n1 * d1 + n2 * d2 + n3 * d3;

            out[0] = divisor - d1 * n1;
            out[4] = -d1 * n2;
            out[8] = -d1 * n3;
            out[12] = -d1 * c;

            out[1] = -d2 * n1;
            out[5] = divisor - d2 * n2;
            out[9] = -d2 * n3;
            out[13] = -d2 * c;

            out[2] = -d3 * n1;
            out[6] = -d3 * n2;
            out[10] = divisor - d3 * n3;
            out[14] = -d3 * c;

            out[3] = 0;
            out[7] = 0;
            out[11] = 0;
            out[15] = divisor;

            return out;
        }

        /**
         * Creates a matrix that projects points from a point onto the plane (e.g. a point light).
         * @param direction
         * @param out 
         */
        export function createPointProjection(plane: ReadonlyPlane, point: ReadonlyVector3D, out?: Matrix4): Matrix4
        {
            out = out || Matrix4();

            const n1 = plane.x, n2 = plane.y, n3 = plane.z, c = plane.w;
            const s1 = point.x, s2 = point.y, s3 = point.z;
            const divisor = Vector3D.dot(plane, point) + c;

            out[0] = divisor - n1 * s1;
            out[4] = -n2 * s1;
            out[8] = -n3 * s1;
            out[12] = -c * s1;

            out[1] = -n1 * s2;
            out[5] = divisor - n2 * s2;
            out[9] = -n3 * s2;
            out[13] = -c * s2;

            out[2] = -n1 * s3;
            out[6] = -n2 * s3;
            out[10] = divisor - n3 * s3;
            out[14] = -c * s3;

            out[3] = -n1;
            out[7] = -n2;
            out[11] = -n3;
            out[15] = divisor - c;

            return out;
        }

        /** Linearly interpolates between two planes. */
        export function linearInterpolate(a: ReadonlyPlane, b: ReadonlyPlane, mu: number, out?: Plane): Plane
        {
            return Vector4D.linearInterpolate(a, b, mu, out);
        }

        /** Gets if two planes are equal, optionally within a threshold. */
        export function equals(a: ReadonlyPlane, b: ReadonlyPlane, threshold: number = 0.0): boolean
        {
            return Vector4D.equals(a, b, threshold);
        }

        /** Creates a copy of a. */
        export function clone(a: ReadonlyPlane, out?: Plane): Plane
        {
            return Vector4D.clone(a, out);
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyPlane, b: Plane): void
        {
            Vector4D.copy(a, b);
        }

        /** Sets the individual components of a. */
        export function set(a: Plane, normal?: ReadonlyVector3D, d?: number): void
        {
            if (normal) { Vector3D.copy(normal, a); }
            if (d != null) { a.w = d; }
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyPlane, arr: number[], offset?: number): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyPlane, arr: Float32Array, offset?: number): void;

        export function write(a: ReadonlyPlane, arr: Float32Array|number[], offset: number = 0): void
        {
            arr[offset++] = a.x;
            arr[offset++] = a.y;
            arr[offset++] = a.z;
            arr[offset++] = a.w;
        }
    }
}