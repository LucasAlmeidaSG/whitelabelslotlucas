/// <reference path="Vector2D.ts" />

namespace RS.Is
{
    /** Gets if the specified value is a Matrix3. */
    export function matrix3(value: any): value is Math.Matrix3
    {
        if (value == null) { return false; }
        if (!Array.isArray(value.values)) { return false; }
        const values: number[][] = value.values;
        if (values.length !== 3) { return false; }
        for (let i = 0; i < 3; i++)
        {
            if (!Array.isArray(values[i])) { return false; }
            if (values[i].length !== 3) { return false; }
            for (let j = 0; j < 3; j++)
            {
                if (!Is.number(values[i][j])) { return false; }
            }
        }
        return true;
    }
}

namespace RS.Math
{
    function is2DArray<T>(arr: T[] | T[][]): arr is T[][]
    {
        if (!Array.isArray(arr)) { return false; }
        if (arr.length === 0) { return false; }
        for (let i = 0; i < arr.length; i++)
        {
            if (!Array.isArray(arr[i])) { return false; }
        }
        return true;
    }

    /** A 3x3 matrix. Column major order. */
    export interface ReadonlyMatrix3
    {
        readonly 0: number; readonly 1: number; readonly 2: number;
        readonly 3: number; readonly 4: number; readonly 5: number;
        readonly 6: number; readonly 7: number; readonly 8: number;
        readonly length: 9;
    }

    /** A 3x3 matrix. Column major order. */
    export interface Matrix3 extends ReadonlyMatrix3
    {
        0: number; 1: number; 2: number;
        3: number; 4: number; 5: number;
        6: number; 7: number; 8: number;
        readonly length: 9;
    }

    const pool: Matrix3[] = [];

    /** Creates a new matrix from the specified array. */
    export function Matrix3(src: Float32Array, offset?: number): Matrix3;

    /** Creates a new matrix from the specified array. */
    export function Matrix3(src: number[], offset?: number): Matrix3;

    /** Creates a new matrix from the specified array. */
    export function Matrix3(src: number[][]): Matrix3;

    /** Creates a new empty matrix. */
    export function Matrix3(): Matrix3;

    export function Matrix3(p1?: Float32Array | number[] | number[][], p2?: number): Matrix3
    {
        const mtx = pool.length > 0 ? pool.pop() : new Array<number>(9) as any as Matrix3;

        if (p1 == null)
        {
            for (let i = 0; i < 9; ++i)
            {
                mtx[i] = 0;
            }
        }
        else if (p1 instanceof Float32Array)
        {
            Matrix3.setFromArray(mtx, p1, p2 || 0);
        }
        else if (is2DArray<number>(p1))
        {
            mtx[0] = p1[0][0];
            mtx[1] = p1[0][1];
            mtx[2] = p1[0][2];

            mtx[3] = p1[1][0];
            mtx[4] = p1[1][1];
            mtx[5] = p1[1][2];

            mtx[6] = p1[2][0];
            mtx[7] = p1[2][1];
            mtx[8] = p1[2][2];
        }
        else if (Array.isArray(p1))
        {
            Matrix3.setFromArray(mtx, p1, p2 || 0);
        }

        return mtx;
    }

    export namespace Matrix3
    {
        /** Populates a matrix from the specified array. */
        export function setFromArray(mtx: Matrix3, arr: number[], offset?: number): void;

        /** Populates a matrix from the specified array. */
        export function setFromArray(mtx: Matrix3, arr: Float32Array, offset?: number);

        export function setFromArray(mtx: Matrix3, arr: number[] | Float32Array, offset: number = 0)
        {
            mtx[0] = arr[offset + 0];
            mtx[1] = arr[offset + 1];
            mtx[2] = arr[offset + 2];

            mtx[3] = arr[offset + 3];
            mtx[4] = arr[offset + 4];
            mtx[5] = arr[offset + 5];

            mtx[6] = arr[offset + 6];
            mtx[7] = arr[offset + 7];
            mtx[8] = arr[offset + 8];
        }

        /** Identity matrix. */
        export const identity: ReadonlyMatrix3 = Matrix3([1, 0, 0, 0, 1, 0, 0, 0, 1]);

        /** Returns a * b. */
        export function mul(a: ReadonlyMatrix3, b: ReadonlyMatrix3, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();

            let a00 = a[0], a01 = a[1], a02 = a[2];
            let a10 = a[3], a11 = a[4], a12 = a[5];
            let a20 = a[6], a21 = a[7], a22 = a[8];

            let b00 = b[0], b01 = b[1], b02 = b[2];
            let b10 = b[3], b11 = b[4], b12 = b[5];
            let b20 = b[6], b21 = b[7], b22 = b[8];

            out[0] = b00 * a00 + b01 * a10 + b02 * a20;
            out[1] = b00 * a01 + b01 * a11 + b02 * a21;
            out[2] = b00 * a02 + b01 * a12 + b02 * a22;

            out[3] = b10 * a00 + b11 * a10 + b12 * a20;
            out[4] = b10 * a01 + b11 * a11 + b12 * a21;
            out[5] = b10 * a02 + b11 * a12 + b12 * a22;

            out[6] = b20 * a00 + b21 * a10 + b22 * a20;
            out[7] = b20 * a01 + b21 * a11 + b22 * a21;
            out[8] = b20 * a02 + b21 * a12 + b22 * a22;

            return out;
        }

        /**
         * Combines a set of transformation matrices into a single matrix via multiplication (e.g. arr[0] * ... * arr[n]).
         * When used to transform a point, the transformations will be applied in the same order of the matrix.
         * e.g. "combine([scale, translate]) * p" will scale p and then translate the scaled p.
         */
        export function combine(arr: ReadonlyMatrix3[], out?: Matrix3): Matrix3
        {
            out = out || Matrix3();
            copy(identity, out);
            const tmp = Matrix3();
            //for (let i = arr.length - 1; i >= 0; --i)
            for (let i = 0, l = arr.length; i < l; ++i)
            {
                copy(out, tmp);
                mul(arr[i], tmp, out);
            }
            free(tmp);
            return out;
        }

        /** Transforms the specified positional vector using this matrix. */
        export function transform(mtx: ReadonlyMatrix3, vec: ReadonlyVector2D, out?: Vector2D): Vector2D;

        /** Transforms the specified positional vector using this matrix. */
        export function transform(mtx: ReadonlyMatrix3, vec: ReadonlyVector3D, out?: Vector3D): Vector3D;

        /** Transforms the specified positional vector using this matrix. */
        export function transform(mtx: ReadonlyMatrix3, vec: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            if (Is.vector3D(vec))
            {
                const out3 = (out || Vector3D()) as Vector3D;

                out3.x = vec.x * mtx[0] + vec.y * mtx[3] + vec.z * mtx[6];
                out3.y = vec.x * mtx[1] + vec.y * mtx[4] + vec.z * mtx[7];
                out3.z = vec.x * mtx[2] + vec.y * mtx[5] + vec.z * mtx[8];

                return out3;
            }
            else
            {
                out = out || Vector2D();

                out.x = vec.x * mtx[0] + vec.y * mtx[3] + mtx[6];
                out.y = vec.x * mtx[1] + vec.y * mtx[4] + mtx[7];

                return out;
            }
        }

        /** Transforms the specified normal vector using this matrix. */
        export function transformNormal(mtx: ReadonlyMatrix3, vec: ReadonlyVector2D, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();

            out.x = vec.x * mtx[0] + vec.y * mtx[3];
            out.y = vec.x * mtx[1] + vec.y * mtx[4];

            return out;
        }

        /** Linearly interpolates between two matrices. */
        export function linearInterpolate(a: ReadonlyMatrix3, b: ReadonlyMatrix3, mu: number, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();
            for (let i = 0; i < 9; ++i)
            {
                out[i] = Math.linearInterpolate(a[i], b[i], mu);
            }
            return out;
        }

        /** Writes a to an array. */
        export function write(a: ReadonlyMatrix3, arr: number[], offset?: number, transposed?: boolean): void;

        /** Writes a to a typed array. */
        export function write(a: ReadonlyMatrix3, arr: Float32Array, offset?: number, transposed?: boolean): void;

        export function write(a: ReadonlyMatrix3, arr: Float32Array | number[], offset: number = 0, transposed?: boolean): void
        {
            if (transposed)
            {
                arr[offset + 0] = a[0];
                arr[offset + 1] = a[3];
                arr[offset + 2] = a[6];

                arr[offset + 3] = a[1];
                arr[offset + 4] = a[4];
                arr[offset + 5] = a[7];

                arr[offset + 6] = a[2];
                arr[offset + 7] = a[5];
                arr[offset + 8] = a[8];
            }
            else
            {
                arr[offset + 0] = a[0];
                arr[offset + 1] = a[1];
                arr[offset + 2] = a[2];

                arr[offset + 3] = a[3];
                arr[offset + 4] = a[4];
                arr[offset + 5] = a[5];

                arr[offset + 6] = a[6];
                arr[offset + 7] = a[7];
                arr[offset + 8] = a[8];
            }
        }

        /** Converts mtx to a 1D array. */
        export function toArray(mtx: ReadonlyMatrix3, transposed: boolean = false): number[]
        {
            const arr = new Array<number>(9);
            write(mtx, arr, 0, transposed);
            return arr;
        }

        /** Converts mtx to a typed array. */
        export function toTypedArray(mtx: ReadonlyMatrix3, transposed: boolean = false): Float32Array
        {
            const arr = new Float32Array(9);
            write(mtx, arr, 0, transposed);
            return arr;
        }

        /** Creates a copy of mtx. */
        export function clone(mtx: ReadonlyMatrix3, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();
            copy(mtx, out);
            return out;
        }

        /** Copies a into b. */
        export function copy(a: ReadonlyMatrix3, b: Matrix3): void
        {
            b[0] = a[0];
            b[1] = a[1];
            b[2] = a[2];

            b[3] = a[3];
            b[4] = a[4];
            b[5] = a[5];

            b[6] = a[6];
            b[7] = a[7];
            b[8] = a[8];
        }

        /** Transposes mtx. */
        export function transpose(mtx: ReadonlyMatrix3, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();
            write(mtx, out as any, 0, true);
            return out;
        }

        /** Finds the determinant of mtx. */
        export function determinant(mtx: ReadonlyMatrix3): number
        {
            const a00 = mtx[0], a01 = mtx[1], a02 = mtx[2];
            const a10 = mtx[3], a11 = mtx[4], a12 = mtx[5];
            const a20 = mtx[6], a21 = mtx[7], a22 = mtx[8];

            return a00 * (a22 * a11 - a12 * a21) + a01 * (-a22 * a10 + a12 * a20) + a02 * (a21 * a10 - a11 * a20);
        }

        /** Finds the inverse of mtx. */
        export function inverse(mtx: ReadonlyMatrix3, out?: Matrix3): Matrix3 | null
        {
            const a00 = mtx[0], a01 = mtx[1], a02 = mtx[2];
            const a10 = mtx[3], a11 = mtx[4], a12 = mtx[5];
            const a20 = mtx[6], a21 = mtx[7], a22 = mtx[8];

            const b01 = a22 * a11 - a12 * a21;
            const b11 = -a22 * a10 + a12 * a20;
            const b21 = a21 * a10 - a11 * a20;

            // Calculate the determinant
            let det = a00 * b01 + a01 * b11 + a02 * b21;

            if (!det) { return null; }
            det = 1.0 / det;

            out = out || Matrix3();

            out[0] = b01 * det;
            out[1] = (-a22 * a01 + a02 * a21) * det;
            out[2] = (a12 * a01 - a02 * a11) * det;
            out[3] = b11 * det;
            out[4] = (a22 * a00 - a02 * a20) * det;
            out[5] = (-a12 * a00 + a02 * a10) * det;
            out[6] = b21 * det;
            out[7] = (-a21 * a00 + a01 * a20) * det;
            out[8] = (a11 * a00 - a01 * a10) * det;

            return out;
        }

        /** Creates a translation matrix from the specified position. */
        export function createTranslation(translation: ReadonlyVector2D, out?: Matrix3): Matrix3;

        /** Creates a translation matrix from the specified position. */
        export function createTranslation(x: number, y: number, out?: Matrix3): Matrix3;

        export function createTranslation(p1: number | ReadonlyVector2D, p2?: number | Matrix3, p3?: Matrix3): Matrix3
        {
            // Resolve overload
            let x: number, y: number;
            let out: Matrix3;
            if (Is.vector2D(p1))
            {
                x = p1.x;
                y = p1.y;
                out = p2 as Matrix3;
            }
            else
            {
                x = p1;
                y = p2 as number;
                out = p3;
            }

            // Translation
            out = out || Matrix3();

            out[0] = 1;
            out[1] = 0;
            out[2] = 0;

            out[3] = 0;
            out[4] = 1;
            out[5] = 0;

            out[6] = x;
            out[7] = y;
            out[8] = 1;

            return out;
        }

        /** Creates a 2D rotation matrix from the specified euler rotation about the z axis (radians). */
        export function createRotation(rotation: number, out?: Matrix3): Matrix3;

        /** Creates a 3D rotation matrix from the specified quaternion. */
        export function createRotation(quaternion: ReadonlyQuaternion, out?: Matrix3): Matrix3;

        /** Creates a rotation matrix from the specified euler rotation about the z axis (radians). */
        export function createRotation(p1: number | ReadonlyQuaternion, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();

            if (Is.number(p1))
            {
                // 2D rotation
                const c = cos(p1);
                const s = sin(p1);

                out[0] = c;
                out[1] = s;
                out[2] = 0;

                out[3] = -s;
                out[4] = c;
                out[5] = 0;

                out[6] = 0;
                out[7] = 0;
                out[8] = 1;
            }
            else
            {
                // 3D rotation
                const x = p1.x, y = p1.y, z = p1.z, w = p1.w;
                const x2 = x + x;
                const y2 = y + y;
                const z2 = z + z;

                const xx = x * x2;
                const yx = y * x2;
                const yy = y * y2;
                const zx = z * x2;
                const zy = z * y2;
                const zz = z * z2;
                const wx = w * x2;
                const wy = w * y2;
                const wz = w * z2;

                out[0] = 1 - yy - zz;
                out[1] = yx + wz;
                out[2] = zx - wy;

                out[3] = yx - wz;
                out[4] = 1 - xx - zz;
                out[5] = zy + wx;

                out[6] = zx + wy;
                out[7] = zy - wx;
                out[8] = 1 - xx - yy;
            }

            return out;
        }

        /** Creates a scale matrix. */
        export function createScale(scale: ReadonlyVector2D, out?: Matrix3): Matrix3;

        /** Creates a scale matrix. */
        export function createScale(x: number, y: number, out?: Matrix3): Matrix3;

        /** Creates a scale matrix. */
        export function createScale(scale: number, out?: Matrix3): Matrix3;

        export function createScale(p1: number | ReadonlyVector2D, p2?: number | Matrix3, p3?: Matrix3): Matrix3
        {
            // Resolve overload
            let x: number, y: number;
            let out: Matrix3;
            if (Is.vector2D(p1))
            {
                x = p1.x;
                y = p1.y;
                out = p2 as Matrix3;
            }
            else if (Is.number(p2))
            {
                x = p1;
                y = p2 as number;
                out = p3;
            }
            else
            {
                x = p1;
                y = p1;
                out = p2;
            }

            // Scale
            out = out || Matrix3();

            out[0] = x;
            out[1] = 0;
            out[2] = 0;

            out[3] = 0;
            out[4] = y;
            out[5] = 0;

            out[6] = 0;
            out[7] = 0;
            out[8] = 1;

            return out;
        }

        /** Creates a matrix that reflects across the line with the given point and direction. */
        export function createReflection(pt: Vector2D, dir: Vector2D, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();
            const ang = Math.atan2(dir.y, dir.x);
            const arr =
            [
                // shift to origin
                createTranslation(-pt.x, -pt.y),

                // rotate such that dir := (1, 0)
                createRotation(ang),

                // scale by 1, -1
                createScale(1, -1),

                // unrotate
                createRotation(-ang),

                // unshift
                createTranslation(pt.x, pt.y)
            ];
            combine(arr, out);
            free(arr[0]);
            free(arr[1]);
            free(arr[2]);
            free(arr[3]);
            free(arr[4]);
            return out;
        }

        /** Creates a combined translation-rotation-scale matrix. */
        export function createTRS(translation: Vector2D, rotation: number, scale: Vector2D, out?: Matrix3): Matrix3
        {
            out = out || Matrix3();
            const arr =
            [
                // rotate and scale in local space
                createRotation(rotation),
                createScale(scale),

                // translate after (don't want the translation to be affected by the rotation/scale)
                createTranslation(translation)
            ];
            combine(arr, out);
            free(arr[0]);
            free(arr[1]);
            free(arr[2]);
            return out;
        }

        /** Indicates that the specified matrix instance is no longer required and may be reused. */
        export function free(mtx: Matrix3): void
        {
            pool.push(mtx);
        }
    }
}