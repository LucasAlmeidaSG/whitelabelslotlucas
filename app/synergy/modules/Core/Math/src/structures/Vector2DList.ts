namespace RS.Math
{
    const tmpVec1 = Vector2D(), tmpVec2 = Vector2D();

    /**
     * A managed memory-friendly array of Vector2D's.
     */
    export class Vector2DList
    {
        protected _items: number[] = [];

        /**
         * Gets or sets the number of items in this list.
         */
        public get length() { return this._items.length >> 1; }
        public set length(value)
        {
            const oldLen = this._items.length >> 1;
            this._items.length = value << 1;
            for (let i = oldLen << 1, l = this._items.length; i < l; ++i)
            {
                this._items[i] = 0;
            }
        }

        /**
         * Gets if the shape formed by the points in this list is defined clockwise.
         */
        public get isClockwise()
        {
            return this.signedArea > 0.0;
        }

        /**
         * Gets the signed area of the polygon formed by the points in this list.
         */
        public get signedArea()
        {
            let area = 0;
            for (let i = 0, l = this.length; i < l; ++i)
            {
                const a = this.get(i, tmpVec1);
                const b = this.get((i + 1) % l, tmpVec2);
                area += (a.x * b.y) - (b.x * a.y);
            }
            return area * 0.5;
        }

        public constructor(initialCount: number);

        public constructor(items: ArrayLike<Vector2D>);

        public constructor();

        public constructor(p1?: number | ArrayLike<Vector2D>)
        {
            if (Is.number(p1))
            {
                this.length = p1;
            }
            else if (p1 != null)
            {
                this.length = p1.length;
                for (let i = 0, l = p1.length; i < l; ++i)
                {
                    this._items[(i << 1) | 0] = p1[i].x;
                    this._items[(i << 1) | 1] = p1[i].y;
                }
            }
        }

        /**
         * Retrieves a vector from this list.
         * @param idx 
         * @param out 
         */
        public get(idx: number, out?: Vector2D): Vector2D
        {
            out = out || Vector2D();
            out.x = this._items[(idx << 1) | 0];
            out.y = this._items[(idx << 1) | 1];
            return out;
        }

        /**
         * Sets a vector on this list.
         * @param idx 
         * @param out 
         */
        public set(idx: number, vec: Vector2D): void;

        /**
         * Sets a vector on this list.
         * @param idx 
         * @param out 
         */
        public set(idx: number, x: number, y: number): void;

        public set(idx: number, p2: Vector2D | number, p3?: number): void
        {
            if (this.length <= idx)
            {
                this.length = idx + 1;
            }
            if (Is.number(p2))
            {
                this._items[(idx << 1) | 0] = p2;
                this._items[(idx << 1) | 1] = p3;
            }
            else
            {
                this._items[(idx << 1) | 0] = p2.x;
                this._items[(idx << 1) | 1] = p2.y;
            }
        }

        /**
         * Clears this list.
         */
        public clear()
        {
            this.length = 0;
        }

        /**
         * Adds a vector to this list.
         * Returns the index of the added vector.
         * @param vec
         */
        public add(vec: Vector2D): number;

        /**
         * Adds a vector to this list.
         * Returns the index of the added vector.
         * @param vec
         */
        public add(x: number, y: number): number;

        public add(p1: Vector2D | number, p2?: number): number
        {
            const idx = this.length;
            if (Is.number(p1))
            {
                this.set(idx, p1, p2);
            }
            else
            {
                this.set(idx, p1);
            }
            return idx;
        }

        /**
         * Converts this list to a native array.
         */
        public toArray(): Vector2D[]
        {
            const l = this.length;
            const arr = new Array<Vector2D>(l);
            for (let i = 0; i < l; ++i)
            {
                arr[i] = this.get(i);
            }
            return arr;
        }
    }
}