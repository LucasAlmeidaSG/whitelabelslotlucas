/// <reference path="core/core.spec.ts"/>
namespace RS.Flow.Tests
{
    const { expect } = chai;

    describe("BaseElement.ts", function ()
    {
        core(() => RS.Flow.container.create({}));

        it("should account for scale-factor when positioning docked elements", function ()
        {
            const container = RS.Flow.container.create({ sizeToContents: true, legacy: false });

            const bottom = RS.Flow.background.create(
            {
                ...RS.Flow.Background.defaultSettings,
                ignoreParentSpacing: false,
                dock: RS.Flow.Dock.Bottom,

                sizeToContents: false,
                size: { w: 100, h: 100 }
            }, container);

            const top = RS.Flow.background.create(
            {
                ...RS.Flow.Background.defaultSettings,
                ignoreParentSpacing: false,
                dock: RS.Flow.Dock.Bottom,

                sizeToContents: false,
                size: { w: 100, h: 100 },

                scaleFactor: 0.5,
                expand: RS.Flow.Expand.Disallowed
            }, container);

            container.size = { w: 100, h: 150 };
            container.invalidateLayout();

            // x should be 1/2 container width - scale * desired width
            // y should be 0
            // w should be scale * desired width
            // h should be scale * desired height
            expect(getFlowLocalBounds(top), "scaled element's position and size should be correct").to.deep.equal({ x: 25, y: 0, w: 50, h: 50 });
            // Layout size should be unchanged
            expect(top.layoutSize, "layoutSize should be unchanged").to.deep.equal({ w: 100, h: 100 });
            // y should be top y + top actual height
            expect(getFlowLocalBounds(bottom).y, "neighbouring element's position should be correct").to.equal(50);

            container.dispose();
        });
    });
}