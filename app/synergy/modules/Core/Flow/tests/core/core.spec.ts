/// <reference path="invalidation.spec.ts"/>
namespace RS.Flow.Tests
{
    export function core<T extends RS.Flow.GenericElement>(elementFactory: () => T)
    {
        Core.invalidation(elementFactory);
    }
}