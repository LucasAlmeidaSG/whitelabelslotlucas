namespace RS.Flow.Tests.Core
{
    const { expect } = chai;

    export namespace Invalidation
    {
        export type PropertyTestMap<TProperties> =
        {
            [P in keyof TProperties]:
            {
                first: TProperties[P];
                second: TProperties[P];
                updatesParent: boolean;
            }
        };
    }

    export function invalidation<T extends Flow.GenericElement>(elementFactory: () => T, extraProperties?: Invalidation.PropertyTestMap<T>)
    {
        describe("invalidation", function ()
        {
            let element: T;
            beforeEach(function ()
            {
                element = elementFactory();
            });

            afterEach(function ()
            {
                element.dispose();
            });

            describe("invalidateLayout", function ()
            {
                it("should cause onLayouted to be published", function ()
                {
                    let layouted = false;
                    element.onLayouted.once(() => layouted = true);
                    element.invalidateLayout();
                    expect(layouted, "onLayouted should be published").to.equal(true);
                });

                it("should cause onLayouted to be published on its parent", function ()
                {
                    const parent = Flow.container.create({});
                    parent.addChild(element);

                    let layouted = false;
                    parent.onLayouted.once(() => layouted = true);

                    element.invalidateLayout();
                    expect(layouted, "onLayouted should be published by parent").to.equal(true);
                });

                // Property invalidation

                const defaultTestMap: Invalidation.PropertyTestMap<Partial<GenericElement>> =
                {
                    dock: { first: Dock.Top, second: Dock.Bottom, updatesParent: true },
                    floatPosition: { first: Math.Vector2D(0, 0), second: Math.Vector2D(1, 1), updatesParent: true },
                    floatOffset: { first: Math.Vector2D(0, 0), second: Math.Vector2D(100, 100), updatesParent: true },
                    legacy: { first: true, second: false, updatesParent: true },
                    expand: { first: Flow.Expand.Allowed, second: Flow.Expand.Disallowed, updatesParent: true },
                    enforceMinimumSize: { first: false, second: true, updatesParent: true },
                    contentAlignment: { first: Math.Vector2D(0, 0), second: Math.Vector2D(0.5, 0.5), updatesParent: false },
                    dockAlignment: { first: Math.Vector2D(0, 0), second: Math.Vector2D(0.5, 0.5), updatesParent: true },
                    scaleFactor: { first: 1, second: 4, updatesParent: true },
                    maskToLayoutArea: { first: false, second: true, updatesParent: false },
                    overflowMode: { first: OverflowMode.Overflow, second: OverflowMode.Shrink, updatesParent: false }
                };

                function runPropertyTests(map: Invalidation.PropertyTestMap<Partial<T | GenericElement>>)
                {
                    for (const key in defaultTestMap)
                    {
                        const entry = defaultTestMap[key as keyof Partial<ElementProperties>];
                        it(`should cause onLayouted to be published when ${key} changes`, function ()
                        {
                            const parent = Flow.container.create({});
                            parent.addChild(element);

                            let parentLayouted = false;
                            let elementLayouted = false;
                            parent.onLayouted(() => parentLayouted = true);
                            element.onLayouted(() => elementLayouted = true);

                            // Do an initial layout
                            element[key] = entry.first;
                            element.invalidateLayout();
                            expect(elementLayouted, "onLayouted should be published by child (initial)").to.equal(true);

                            elementLayouted = false;
                            parentLayouted = false;

                            element[key] = entry.second;
                            expect(elementLayouted, "onLayouted should be published by child").to.equal(true);
                        });
                    }
                }

                runPropertyTests(defaultTestMap);
                if (extraProperties) { runPropertyTests(extraProperties); }

                // Tree trimming tests

                it("should not cause onLayouted to be published on its parent if nothing has changed", function ()
                {
                    const parent = Flow.container.create({});
                    parent.addChild(element);

                    let parentLayouted = false;
                    let elementLayouted = false;
                    parent.onLayouted(() => parentLayouted = true);
                    element.onLayouted(() => elementLayouted = true);

                    // Do an initial layout
                    element.invalidateLayout();
                    expect(elementLayouted, "onLayouted should be published by child (initial)").to.equal(true);
                    expect(parentLayouted, "onLayouted should be published by parent (initial)").to.equal(true);

                    elementLayouted = false;
                    parentLayouted = false;

                    // Then check parent is not laid out if nothing has changed
                    element.invalidateLayout();
                    expect(elementLayouted, "onLayouted should be published by child").to.equal(true);
                    expect(parentLayouted, "onLayouted should not be published by parent").to.equal(false);
                });

                function runPropertyTestsForParent(map: Invalidation.PropertyTestMap<Partial<T | GenericElement>>)
                {
                    for (const key in defaultTestMap)
                    {
                        const entry = defaultTestMap[key as keyof Partial<ElementProperties>];
                        it(`${entry.updatesParent ? "should" : "should not"} cause onLayouted to be published on its parent when ${key} changes`, function ()
                        {
                            const parent = Flow.container.create({});
                            parent.addChild(element);

                            let parentLayouted = false;
                            let elementLayouted = false;
                            parent.onLayouted(() => parentLayouted = true);
                            element.onLayouted(() => elementLayouted = true);

                            // Do an initial layout
                            element[key] = entry.first;
                            element.invalidateLayout();
                            expect(elementLayouted, "onLayouted should be published by child (initial)").to.equal(true);

                            elementLayouted = false;
                            parentLayouted = false;

                            element[key] = entry.second;
                            expect(parentLayouted, `onLayouted ${entry.updatesParent ? "should" : "should not"} be published by parent`).to.equal(entry.updatesParent);
                        });
                    }
                }

                runPropertyTestsForParent(defaultTestMap);
                if (extraProperties) { runPropertyTestsForParent(extraProperties); }

                it("should cause onLayouted to be published on its parent if the desiredSize changes", function ()
                {
                    const parent = Flow.container.create({});
                    parent.addChild(element);

                    let parentLayouted = false;
                    let elementLayouted = false;
                    parent.onLayouted(() => parentLayouted = true);
                    element.onLayouted(() => elementLayouted = true);

                    // Do an initial layout
                    Object.defineProperties(element, { desiredSize: { get: () => Math.Size2D(10, 10), configurable: true }});
                    element.invalidateLayout();
                    expect(elementLayouted, "onLayouted should be published by child (initial)").to.equal(true);
                    expect(parentLayouted, "onLayouted should be published by parent (initial)").to.equal(true);

                    Object.defineProperties(element, { desiredSize: { get: () => Math.Size2D(20, 20), configurable: true }});
                    element.invalidateLayout();
                    expect(elementLayouted, "onLayouted should be published by child").to.equal(true);
                    expect(parentLayouted, "onLayouted should be published by parent").to.equal(true);
                });
            });

            describe("suppressLayouting", function ()
            {
                it("should prevent layouting", function ()
                {
                    let layouted = false;
                    element.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    element.invalidateLayout();
                    expect(layouted, "onLayouted should not be published").to.equal(false);
                });

                it("should prevent layouting on children", function ()
                {
                    const childElement = Flow.container.create({ sizeToContents: false, size: Math.Size2D(10, 10) }, element);
                    let layouted = false;
                    childElement.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    childElement.invalidateLayout();
                    expect(layouted, "onLayouted should not be published").to.equal(false);
                });
            });

            describe("restoreLayouting", function ()
            {
                it("should allow layouting to occur after a call to suppressLayouting", function ()
                {
                    let layouted = false;
                    element.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    element.restoreLayouting();
                    element.invalidateLayout();
                    expect(layouted, "onLayouted should be published").to.equal(true);
                });

                it("should cause a layout to occur if the layout was invalidated after a call to suppressLayouting", function ()
                {
                    let layouted = false;
                    element.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    element.invalidateLayout();
                    element.restoreLayouting();
                    expect(layouted, "onLayouted should be published").to.equal(true);
                });

                it("should cause a layout to occur if a child was invalidated after a call to suppress layouting", function ()
                {
                    const childElement = Flow.container.create({ sizeToContents: false, size: Math.Size2D(10, 10) }, element);
                    let layouted = false;
                    childElement.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    childElement.invalidateLayout();
                    element.restoreLayouting();
                    expect(layouted, "onLayouted should be published").to.equal(true);
                });

                it("should cause a layout to occur if called with forceInvalidation true", function ()
                {
                    let layouted = false;
                    element.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    element.restoreLayouting(true);
                    expect(layouted, "onLayouted should be published").to.equal(true);
                });

                it("should prevent a layout from occurring if called with forceInvalidation false when the layout was invalidated after a call to suppressLayouting", function ()
                {
                    let layouted = false;
                    element.onLayouted.once(() => layouted = true);
                    element.suppressLayouting();
                    element.invalidateLayout();
                    element.restoreLayouting(false);
                    expect(layouted, "onLayouted should not be published").to.equal(false);
                });
            });
        });
    }
}