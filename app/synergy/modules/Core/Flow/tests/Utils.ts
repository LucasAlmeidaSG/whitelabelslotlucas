namespace RS.Flow.Tests
{
    const { expect } = chai;

    export interface Bounds { x: number; y: number; w: number; h: number; }
    export function Bounds(x: number, y: number, w: number, h: number): Bounds
    {
        return { x, y, w, h };
    }

    /** Returns outerSize / 2 - innerSize / 2. */
    export function alignMiddle(outerSize: number, innerSize: number): number
    {
        return (outerSize - innerSize) / 2;
    }

    /** Returns outerSize - innerSize. */
    export function alignEnd(outerSize: number, innerSize: number): number
    {
        return outerSize - innerSize;
    }

    /** Returns the given element's Flow bounding box in its parent's space. */
    export function getFlowLocalBounds(object: RS.Flow.GenericElement): Bounds
    {
        const pivot = object.pivot;
        return {
            x: object.x - pivot.x * object.scaleFactor, y: object.y - pivot.y * object.scaleFactor,
            w: object.layoutSize.w * object.scaleFactor, h: object.layoutSize.h * object.scaleFactor
        };
    }

    /** Expects the children of the given Flow element to be laid out according to the given the given expected bounds. */
    export function expectLayout(target: RS.Flow.GenericElement, expectations: Bounds[], message: string = "")
    {
        if (message.length > 0) { message = `${message}: `; }
        for (let i = 0; i < Math.min(target.flowChildren.length, expectations.length); i++)
        {
            const child = target.flowChildren[i];
            const expectedBounds = expectations[i];
            const name = child.name || `child ${i}`;
            expect(getFlowLocalBounds(child), `${message}${name}`).to.deep.equal(expectedBounds);
        }
    }
}