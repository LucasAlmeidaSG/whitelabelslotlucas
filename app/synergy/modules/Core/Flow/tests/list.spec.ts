namespace RS.Flow.Tests
{
    const { expect } = chai;
    import List = RS.Flow.List;
    import Direction = List.Direction;

    const directions = [Direction.LeftToRight, Direction.TopToBottom, Direction.RightToLeft, Direction.BottomToTop];

    function runMultiTest(listSettings: Partial<List.Settings>, childSettings: Partial<RS.Flow.ElementProperties>[], expectations: { [direction: number]: Bounds[][] })
    {
        for (const direction of directions)
        {
            const list = RS.Flow.list.create({ ...listSettings, direction });

            const directionExpectations = expectations[direction];
            for (let i = 0; i < childSettings.length; i++)
            {
                const settings = childSettings[i];
                const child = RS.Flow.container.create(settings, list);
                
                list.invalidateLayout();
                const expectIndex = Math.min(directionExpectations.length - 1, i);
                const expectation = directionExpectations[expectIndex];
                expectLayout(list, expectation, `[${Direction[direction]}] (${i + 1} ${i > 0 ? "children" : "child"})`);
            }

            list.dispose();
        }
    }

    describe("List.ts", function ()
    {
        // Default behaviour: [|| ]
        it("should place items next to one another", function ()
        {
            runMultiTest(
            {
                size: { w: 100, h: 100 },
                sizeToContents: false,
                legacy: false
            },
            [
                {
                    size: { w: 39, h: 29 },
                    sizeToContents: false,
                    expand: RS.Flow.Expand.Disallowed
                },
                {
                    size: { w: 40, h: 21 },
                    sizeToContents: false,
                    expand: RS.Flow.Expand.Disallowed
                }
            ],
            {
                [Direction.LeftToRight]:
                [
                    [
                        Bounds(0, alignMiddle(100, 29), 39, 29),
                        Bounds(39, alignMiddle(100, 21), 40, 21)
                    ]
                ],

                [Direction.TopToBottom]:
                [
                    [
                        Bounds(alignMiddle(100, 39), 0, 39, 29),
                        Bounds(alignMiddle(100, 40), 29, 40, 21)
                    ]
                ],

                // Note: lists don't actually adjust their effective content alignment for inverted directions.
                // See https://nextgengaming.atlassian.net/browse/SCF-442.

                [Direction.RightToLeft]:
                [
                    [
                        Bounds(/*alignEnd(100, 39)*/ 0, alignMiddle(100, 29), 39, 29)
                    ],
                    [
                        Bounds(/*alignEnd(100, 39)*/ 40, alignMiddle(100, 29), 39, 29),
                        Bounds(/*alignEnd(100, 39 + 40)*/ 0, alignMiddle(100, 21), 40, 21)
                    ]
                ],

                [Direction.BottomToTop]:
                [
                    [
                        Bounds(alignMiddle(100, 39), /*alignEnd(100, 29)*/ 0, 39, 29)
                    ],
                    [
                        Bounds(alignMiddle(100, 39), /*alignEnd(100, 29)*/ 21, 39, 29),
                        Bounds(alignMiddle(100, 40), /*alignEnd(100, 29 + 21)*/ 0, 40, 21)
                    ]
                ]
            });
        });

        describe("evenlySpaceItems", function ()
        {
            // [ | ]
            it("should distribute items to fill the space", function ()
            {
                runMultiTest(
                {
                    size: { w: 100, h: 100 },
                    sizeToContents: false,
                    legacy: false,
                    evenlySpaceItems: true
                },
                [
                    {
                        size: { w: 39, h: 29 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Disallowed
                    },
                    {
                        size: { w: 40, h: 21 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Disallowed
                    }
                ],
                {
                    [Direction.LeftToRight]:
                    [
                        [
                            Bounds(alignMiddle(100, 39), alignMiddle(100, 29), 39, 29)
                        ],
                        [
                            Bounds(0, alignMiddle(100, 29), 39, 29),
                            Bounds(alignEnd(100, 40), alignMiddle(100, 21), 40, 21)
                        ]
                    ],

                    [Direction.TopToBottom]:
                    [
                        [
                            Bounds(alignMiddle(100, 39), alignMiddle(100, 29), 39, 29)
                        ],
                        [
                            Bounds(alignMiddle(100, 39), 0, 39, 29),
                            Bounds(alignMiddle(100, 40), alignEnd(100, 21), 40, 21)
                        ]
                    ],

                    [Direction.RightToLeft]:
                    [
                        [
                            Bounds(alignMiddle(100, 39), alignMiddle(100, 29), 39, 29)
                        ],
                        [
                            Bounds(alignEnd(100, 39), alignMiddle(100, 29), 39, 29),
                            Bounds(0, alignMiddle(100, 21), 40, 21)
                        ]
                    ],

                    [Direction.BottomToTop]:
                    [
                        [
                            Bounds(alignMiddle(100, 39), alignMiddle(100, 29), 39, 29)
                        ],
                        [
                            Bounds(alignMiddle(100, 39), alignEnd(100, 29), 39, 29),
                            Bounds(alignMiddle(100, 40), 0, 40, 21)
                        ]
                    ]
                });
            });
        });

        // The behaviour also known as 'distribute' which distributes items evenly across the space
        describe("spreadItems", function ()
        {
            it("should distribute items evenly across the space when false", function ()
            {
                runMultiTest(
                {
                    size: { w: 200, h: 200 },
                    sizeToContents: false,
                    legacy: false,
                    evenlySpaceItems: true,
                    spreadItems: false
                },
                [
                    {
                        size: { w: 25, h: 40 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Disallowed
                    },
                    {
                        size: { w: 55, h: 80 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Allowed
                    }
                ],
                {
                    [Direction.LeftToRight]:
                    [
                        [
                            Bounds(alignMiddle(200, 25), alignMiddle(200, 40), 25, 40)
                        ],
                        [
                            // space per element = ((outerSize - contentsSize) / numChildren) / 2
                            // => expected space = ((200 - (55 + 25)) / 2) / 2 = 30
                            Bounds(30, alignMiddle(200, 40), 25, 40),
                            Bounds(alignEnd(200, 55) - 30, 0, 55, 200)
                        ]
                    ],
                    
                    [Direction.TopToBottom]:
                    [
                        [
                            Bounds(alignMiddle(200, 25), alignMiddle(200, 40), 25, 40)
                        ],
                        [
                            // space per element = ((outerSize - contentsSize) / numChildren) / 2
                            // => expected space = ((200 - (40 + 80)) / 2) / 2 = 20
                            Bounds(alignMiddle(200, 25), 20, 25, 40),
                            Bounds(0, alignEnd(200, 80) - 20, 200, 80)
                        ]
                    ],

                    [Direction.RightToLeft]:
                    [
                        [
                            Bounds(alignMiddle(200, 25), alignMiddle(200, 40), 25, 40)
                        ],
                        [
                            // space per element = ((outerSize - contentsSize) / numChildren) / 2
                            // => expected space = ((200 - (55 + 25)) / 2) / 2 = 30
                            Bounds(alignEnd(200, 25) - 30, alignMiddle(200, 40), 25, 40),
                            Bounds(30, 0, 55, 200)
                        ]
                    ],

                    [Direction.BottomToTop]:
                    [
                        [
                            Bounds(alignMiddle(200, 25), alignMiddle(200, 40), 25, 40)
                        ],
                        [
                            // space per element = ((outerSize - contentsSize) / numChildren) / 2
                            // => expected space = ((200 - (40 + 80)) / 2) / 2 = 20
                            Bounds(alignMiddle(200, 25), alignEnd(200, 40) - 20, 25, 40),
                            Bounds(0, 20, 200, 80)
                        ]
                    ]
                });
            });
        });

        describe("expandItems", function ()
        {
            // [ | ]
            it("should expand items to fill the space", function ()
            {
                runMultiTest(
                {
                    size: { w: 200, h: 200 },
                    sizeToContents: false,
                    legacy: false,
                    expandItems: true
                },
                [
                    {
                        name: "child 0 (expanding)",
                        size: { w: 25, h: 40 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Allowed
                    },
                    {
                        name: "child 1 (expanding)",
                        size: { w: 75, h: 60 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Allowed
                    },
                    {
                        name: "child 0 (non-expanding)",
                        size: { w: 20, h: 30 },
                        sizeToContents: false,
                        expand: RS.Flow.Expand.Disallowed
                    }
                ],
                {
                    [Direction.LeftToRight]:
                    [
                        [
                            Bounds(0, 0, 200, 200)
                        ],
                        // 100 px extra space across two items = 50 px each
                        [
                            Bounds(0, 0, 75, 200),
                            Bounds(75, 0, 125, 200)
                        ],
                        // 80 px extra space across two items = 40 px each
                        [
                            Bounds(0, 0, 65, 200),
                            Bounds(65, 0, 115, 200),
                            Bounds(65 + 115, alignMiddle(200, 30), 20, 30)
                        ]
                    ],

                    [Direction.TopToBottom]:
                    [
                        [
                            Bounds(0, 0, 200, 200)
                        ],
                        // 100 px extra space across two items = 50 px each
                        [
                            Bounds(0, 0, 200, 90),
                            Bounds(0, 90, 200, 110)
                        ],
                        // 70 px extra space across two items = 35 px each
                        [
                            Bounds(0, 0, 200, 75),
                            Bounds(0, 75, 200, 95),
                            Bounds(alignMiddle(200, 20), 75 + 95, 20, 30)
                        ]
                    ],

                    [Direction.RightToLeft]:
                    [
                        [
                            Bounds(0, 0, 200, 200)
                        ],
                        // 100 px extra space across two items = 50 px each
                        [
                            Bounds(125, 0, 75, 200),
                            Bounds(0, 0, 125, 200)
                        ],
                        // 80 px extra space across two items = 40 px each
                        [
                            Bounds(115 + 20, 0, 65, 200),
                            Bounds(20, 0, 115, 200),
                            Bounds(0, alignMiddle(200, 30), 20, 30)
                        ]
                    ],

                    [Direction.BottomToTop]:
                    [
                        [
                            Bounds(0, 0, 200, 200)
                        ],
                        // 100 px extra space across two items = 50 px eachx
                        [
                            Bounds(0, 110, 200, 90),
                            Bounds(0, 0, 200, 110)
                        ],
                        // 70 px extra space across two items = 35 px each
                        [
                            Bounds(0, 95 + 30, 200, 75),
                            Bounds(0, 30, 200, 95),
                            Bounds(alignMiddle(200, 20), 0, 20, 30)
                        ]
                    ]
                });
            });
        });

        // Note: the combination of expandItems and evenlySpaceItems is currently undefined behaviour,
        // however it could possibly one day be used to allow non-expanding items to be given extra space isntead
        // See https://nextgengaming.atlassian.net/browse/SCF-443.

        it("should account for scale-factor when positioning elements (top to bottom)", function ()
        {
            const list = RS.Flow.list.create({
                size: { w: 100, h: 100 },
                sizeToContents: false,
                direction: RS.Flow.List.Direction.TopToBottom,
                legacy: false,
                spacing: RS.Flow.Spacing.axis(0, 10),
                evenlySpaceItems: true
            });

            const itemA = RS.Flow.container.create({
                size: { w: 36, h: 36 },
                scaleFactor: 1.5,
                sizeToContents: false,
                expand: RS.Flow.Expand.Disallowed
            }, list);

            list.invalidateLayout();

            expect(getFlowLocalBounds(itemA), "scaled element's position and size should be correct").to.deep.equal({ x: 23, y: 23, w: 54, h: 54 });
            expect(Math.Vector2D.clone(itemA.pivot)).to.deep.equal({ x: 18, y: 18 });

            list.dispose();
        });

        it("should account for scale-factor when positioning elements (left to right)", function ()
        {
            const list = RS.Flow.list.create({
                size: { w: 100, h: 100 },
                sizeToContents: false,
                direction: RS.Flow.List.Direction.LeftToRight,
                legacy: false,
                spacing: RS.Flow.Spacing.axis(10, 0),
                evenlySpaceItems: true
            });

            const itemA = RS.Flow.container.create({
                size: { w: 36, h: 36 },
                scaleFactor: 1.5,
                sizeToContents: false,
                expand: RS.Flow.Expand.Disallowed
            }, list);

            list.invalidateLayout();

            expect(getFlowLocalBounds(itemA), "scaled element's position and size should be correct").to.deep.equal({ x: 23, y: 23, w: 54, h: 54 });
            expect(Math.Vector2D.clone(itemA.pivot)).to.deep.equal({ x: 18, y: 18 });

            list.dispose();
        });
    });
}