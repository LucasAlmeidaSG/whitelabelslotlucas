namespace RS.Flow.Tests
{
    const { expect } = chai;
    describe("ComplexLabel.ts", function ()
    {
        describe("TextReader", function ()
        {
            it("should parse text correctly", function ()
            {
                const reader = new ComplexLabel.TextReader({
                    default: Label.defaultSettings,
                    "test": { ...Label.defaultSettings as ComplexLabel.StyleSettings }
                }, {});
                const result = reader.parse("Here is some text<test>Here is text in a test style<default>Here is the normal text again");
                expect(result.length).to.equal(3);
            });
        });
    });
}