namespace RS.Flow.Tests
{
    const { expect } = chai;

    describe("Grid.ts", function ()
    {
        describe("addChildAtCell", function ()
        {
            function createFlowChild() { return RS.Flow.container.create({ name: "FlowChild" }); }
            function createNonFlowChild() { return new RS.Rendering.DisplayObjectContainer("NonFlowChild"); }

            let grid: RS.Flow.Grid;
            beforeEach(function ()
            {
                grid = RS.Flow.grid.create(
                {
                    order: RS.Flow.Grid.Order.RowFirst,
                    colCount: 5
                });

                for (let i = 0; i < 20; i++)
                {
                    grid.addChild(createFlowChild());
                }
            });
            afterEach(function ()
            {
                grid.dispose();
            });

            it("should add the child at the correct index when given a cell index, replacing the child in its place", function ()
            {
                const childToReplace = grid.getChildAt(5);

                const childToAdd = createFlowChild();
                grid.addChildAtCell(childToAdd, 5);
                expect(grid.getChildIndex(childToAdd), "added child at correct index").to.equal(5);

                expect(grid.children.indexOf(childToReplace), "removed existing child").to.equal(-1);
                expect(grid.flowChildren.length, "maintained children array length").to.equal(20);
            });

            it("should add the child at the correct row and column for a fixed-width row-first grid, replacing the child in its place", function ()
            {
                const childToAdd = createFlowChild();
                grid.addChildAtCell(childToAdd, 0, 0);
                expect(grid.getChildIndex(childToAdd), "child at 0, 0 should have index 0").to.equal(0);

                const secondChildToAdd = createFlowChild();
                grid.addChildAtCell(secondChildToAdd, 3, 4);
                expect(grid.getChildIndex(secondChildToAdd), "child at 3, 4 should have index 23 (4 * 5 + 3)").to.equal(23);

                expect(grid.flowChildren.length, "maintained children array length").to.equal(24);
            });

            it("should add the child at the correct index when given a cell index even if there are non-Flow children, replacing the child in its place", function ()
            {
                const nonFlowChild = createNonFlowChild();
                grid.addChild(nonFlowChild, 0);

                const nonFlowChild2 = createNonFlowChild();
                grid.addChild(nonFlowChild2, 9);

                const nonFlowChild3 = createNonFlowChild();
                grid.addChild(nonFlowChild2, 3);
                
                const childToAdd = createFlowChild();
                grid.addChildAtCell(childToAdd, 5);
                expect(grid.getChildIndex(childToAdd)).to.equal(6);

                expect(grid.flowChildren.length, "maintained children array length").to.equal(20);
            });

            it("should allow addition of children to cells beyond the current number of grid cells", function ()
            {
                const childToAdd = createFlowChild();
                grid.addChildAtCell(childToAdd, 25);
                expect(grid.getChildIndex(childToAdd), "child added at correct index").to.equal(25);
            });
        });
    });
}