namespace RS.Build.Scaffolds
{
    const flowElementTemplate = `namespace {{namespace}}
{
    /**
     * The {{className}} element.
     */
    export class {{className:CamelCase}} extends RS.Flow.BaseElement<{{className:CamelCase}}.Settings>
    {
        public constructor(settings: {{className:CamelCase}}.Settings)
        {
            super(settings, undefined);
        }

    }

    export namespace {{className:CamelCase}}
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            
        }
    }

    /**
     * The {{className}} element.
     */
    export const {{className:camelCase}} = RS.Flow.declareElement({{className:CamelCase}});
}`;

    export const flowElement = new Scaffold();
    flowElement.addParam("className", "Class Name", "MyClass");
    flowElement.addTemplate(flowElementTemplate, "src/{{className}}.ts");
    registerScaffold("flowelement", flowElement);
}