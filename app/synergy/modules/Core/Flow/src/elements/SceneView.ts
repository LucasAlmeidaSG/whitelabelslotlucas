namespace RS.Flow
{
    /** An element that encapsulates a native scene view for 3D rendering. */
    export class SceneView extends BaseElement<SceneView.Settings, SceneView.RuntimeData>
    {
        @RS.AutoDisposeOnSet protected readonly _sprite: Rendering.ISceneView | null = null;

        protected _resolution: number = 1.0;

        /** Gets the inner sprite object that backs this image. */
        public get innerSprite() { return this._sprite; }

        /** Gets or sets the resolution of the scene view. */
        public get resolution() { return this._resolution; }
        public set resolution(value)
        {
            this._resolution = value;
            this.invalidateLayout();
        }

        public constructor(settings: SceneView.Settings, runtimeData: SceneView.RuntimeData)
        {
            super(settings, undefined);

            this._sprite = new RS.Rendering.SceneView({ w: 100, h: 100 }, runtimeData.camera);
            this.addChild(this._sprite);

            if (settings.blendMode) { this._sprite.blendMode = settings.blendMode; }
            if (settings.alpha != null) { this._sprite.alpha = settings.alpha; }
            if (settings.resolution != null) { this._resolution = settings.resolution; }
        }

        /** Calculates the minimum size we should be to fit the contents. */
        protected calculateContentsSize(): Math.Size2D
        {
            return { w: 100, h: 100 };
        }

        /** Called when this element has been layouted. */
        protected onLayout(): void
        {
            if (this._sprite == null) { return; }

            // Reset sprite transform
            this._sprite.x = 0;
            this._sprite.y = 0;
            this._sprite.scaleX = this._sprite.scaleY = (1.0 / this._resolution);
            
            // Update viewport size
            this._sprite.viewportSize = { w: (this._layoutSize.w * this._resolution)|0, h: (this._layoutSize.h * this._resolution)|0 };

            if (this.settings.mirrorX)
            {
                this._sprite.scaleX *= -1;
            }
            if (this.settings.mirrorY)
            {
                this._sprite.scaleY *= -1;
            }
        }
    }

    export namespace SceneView
    {
        /** Settings for the scene view element. */
        export interface Settings extends Partial<ElementProperties>
        {
            /** How this image should scale to fit its layout size. Defaults to Stretch, but Contain and Cover maintain aspect ratio. */
            blendMode?: RS.Rendering.BlendMode;
            alpha?: number;
            mirrorX?: boolean;
            mirrorY?: boolean;
            resolution?: number;
        }

        /** Runtime data for the scene view element. */
        export interface RuntimeData
        {
            camera: RS.Rendering.Scene.ICamera;
        }
    }

    /** An element that encapsulates a native scene view for 3D rendering. */
    export const sceneView = declareElement(SceneView, true);
}
