namespace RS.Flow
{
    /**
     * A generic spinner that allows the user to select a value through some means.
     */
    @HasCallbacks
    export abstract class BaseSpinner<TSettings extends BaseSpinner.Settings> extends BaseElement<TSettings>
    {
        /** Published when the value of this spinner has changed. */
        public readonly onValueChanged = createEvent<number|string>();

        protected _optionsSettings: BaseSpinner.OptionsSettings;
        protected _options: (number|string)[];

        protected _enabled: boolean = true;
        protected _value: number|string;
        protected _funcIndex: number;

        /** Gets or sets the current value of this spinner. */
        public get value() { return this._value; }
        public set value(value)
        {
            if (value === this._value) { return; }
            this._value = value;
            this.handleValueChanged();
            this.onValueChanged.publish(value);
        }

        /**
         * Gets or sets the options for this spinner.
         * This property is inert if the spinner type is not Options.
         */
        public get options()
        {
            if (this._optionsSettings.type !== Spinner.Type.Options) { return null; }
            return this._options;
        }
        public set options(value)
        {
            if (this._optionsSettings.type !== Spinner.Type.Options) { return; }
            let curIdx = this._options.indexOf(this._value);
            if (curIdx === -1)
            {
                curIdx = 0;
            }
            else if (curIdx >= value.length)
            {
                curIdx = value.length - 1;
            }
            this._options = value;
            this._value = this._options[curIdx];
            this.handleOptionsChanged();
            this.handleValueChanged();
            this.onValueChanged.publish(this._value);
        }

        /**
         * Gets or sets the current option index of this spinner.
         * This property is inert if the spinner type is not Options.
         */
        public get optionIndex()
        {
            if (this._optionsSettings.type !== Spinner.Type.Options) { return null; }
            return this._options.indexOf(this._value);
        }
        public set optionIndex(value)
        {
            if (this._optionsSettings.type !== Spinner.Type.Options) { return; }
            this.value = this._options[value];
        }

        public constructor(settings: TSettings)
        {
            super(settings, undefined);
            this._optionsSettings = settings.options;

            switch (this._optionsSettings.type)
            {
                case Spinner.Type.Numeric:
                    //If no step is defined, automatically set it to 1 so we always increment/decrement by something instead of NaN
                    if (this._optionsSettings.step == null)
                    {
                        this._optionsSettings.step = 1;
                    }
                    this.value = this._optionsSettings.initialValue != null ? this._optionsSettings.initialValue : this._optionsSettings.minValue;
                    break;
                case Spinner.Type.Options:
                    this._options = this._optionsSettings.options;
                    this.value = this._optionsSettings.options[this._optionsSettings.initialIndex || 0];
                    break;
                case Spinner.Type.Function:
                    this._funcIndex = this._optionsSettings.initialIndex != null ? this._optionsSettings.initialIndex : this._optionsSettings.minIndex;
                    this.value = this._optionsSettings.func(this._funcIndex || 0);
                    break;
            }
        }

        /**
         * Increments the value of this spinner.
         */
        @Callback
        public increment(): void
        {
            switch (this._optionsSettings.type)
            {
                case Spinner.Type.Numeric:
                case Spinner.Type.Options:
                    this.value = this.getIncremented(this.value);
                    break;
                case Spinner.Type.Function:
                    this._funcIndex++;
                    this._funcIndex = (this._optionsSettings.wrap ? Math.wrap : Math.clamp)(this._funcIndex, this._optionsSettings.minIndex, this._optionsSettings.maxIndex + 1);
                    this.value = this._optionsSettings.func(this._funcIndex);
                    break;
            }
        }

        /**
         * Decrements the value of this spinner.
         */
        @Callback
        public decrement(): void
        {
            switch (this._optionsSettings.type)
            {
                case Spinner.Type.Numeric:
                case Spinner.Type.Options:
                    this.value = this.getDecremented(this.value);
                    break;
                case Spinner.Type.Function:
                    this._funcIndex--;
                    this._funcIndex = (this._optionsSettings.wrap ? Math.wrap : Math.clamp)(this._funcIndex, this._optionsSettings.minIndex, this._optionsSettings.maxIndex + 1);
                    this.value = this._optionsSettings.func(this._funcIndex);
                    break;
            }
        }

        /**
         * Called when the value of this spinner has changed.
         */
        protected abstract handleValueChanged();

        /**
         * Called when the options of this spinner have changed.
         */
        protected abstract handleOptionsChanged();

        /**
         * Given a value, increments it using this spinner's inc/dec logic.
         * @param value
         */
        protected getIncremented(value: string | number): string | number
        {
            let curIdx: number;
            switch (this._optionsSettings.type)
            {
                case Spinner.Type.Numeric:
                    value = Is.number(value) ? value : parseFloat(value);
                    value += this._optionsSettings.step;
                    if (this._optionsSettings.wrap)
                    {
                        return Math.wrap(value, this._optionsSettings.minValue, this._optionsSettings.maxValue + this._optionsSettings.step);
                    }
                    else
                    {
                        return Math.clamp(value, this._optionsSettings.minValue, this._optionsSettings.maxValue);
                    }
                case Spinner.Type.Options:
                    curIdx = this._options.indexOf(value);
                    if (curIdx === -1)
                    {
                        return this._options[this._options.length - 1];
                    }
                    curIdx++;
                    curIdx = (this._optionsSettings.wrap ? Math.wrap(curIdx, 0, this._options.length) : Math.clamp(curIdx, 0, this._options.length - 1));
                    return this._options[curIdx];
                case Spinner.Type.Function:
                    curIdx = Is.number(value) ? value : parseFloat(value);
                    curIdx++;
                    curIdx = (this._optionsSettings.wrap ? Math.wrap : Math.clamp)(curIdx, this._optionsSettings.minIndex, this._optionsSettings.maxIndex + 1);
                    return this._optionsSettings.func(curIdx);
            }
            return null;
        }

        /**
         * Given a value, decrements it using this spinner's inc/dec logic.
         * @param value
         */
        protected getDecremented(value: string | number): string | number
        {
            let curIdx: number;
            switch (this._optionsSettings.type)
            {
                case Spinner.Type.Numeric:
                    value = Is.number(value) ? value : parseFloat(value);
                    value -= this._optionsSettings.step;
                    if (this._optionsSettings.wrap)
                    {
                        return Math.wrap(value, this._optionsSettings.minValue, this._optionsSettings.maxValue + this._optionsSettings.step);
                    }
                    else
                    {
                        return Math.clamp(value, this._optionsSettings.minValue, this._optionsSettings.maxValue);
                    }
                case Spinner.Type.Options:
                    curIdx = this._options.indexOf(value);
                    if (curIdx === -1)
                    {
                        return this._options[0];
                    }
                    curIdx--;
                    curIdx = (this._optionsSettings.wrap ? Math.wrap(curIdx, 0, this._options.length) : Math.clamp(curIdx, 0, this._options.length - 1));
                    return this._options[curIdx];
                case Spinner.Type.Function:
                    curIdx = Is.number(value) ? value : parseFloat(value);
                    curIdx--;
                    curIdx = (this._optionsSettings.wrap ? Math.wrap : Math.clamp)(curIdx, this._optionsSettings.minIndex, this._optionsSettings.maxIndex + 1);
                    return this._optionsSettings.func(curIdx);
            }
            return null;
        }
    }

    export namespace BaseSpinner
    {
        /** Spinner type. */
        export enum Type
        {
            /** Numeric bounded value. */
            Numeric,

            /** Numeric or string value from a set of fixed options. */
            Options,

            /** Numeric or string value from a function of an index. */
            Function
        }

        export interface BaseOptionsSettings<T extends Type>
        {
            wrap?: boolean;
            type: T;
        }

        export interface NumericOptionsSettings extends BaseOptionsSettings<Type.Numeric>
        {
            minValue: number;
            maxValue: number;
            initialValue?: number;
            step?: number;
        }

        export interface ExplicitOptionsSettings extends BaseOptionsSettings<Type.Options>
        {
            options: (number | string)[];
            initialIndex?: number;
        }

        export interface FunctionOptionsSettings extends BaseOptionsSettings<Type.Function>
        {
            minIndex: number;
            maxIndex: number;
            initialIndex?: number;
            func: (index: number) => number|string;
        }

        /** Settings for a spinner. */
        export type OptionsSettings = NumericOptionsSettings | ExplicitOptionsSettings | FunctionOptionsSettings;

        export interface Settings extends Partial<ElementProperties>
        {
            options: OptionsSettings;
        }

        export let defaultSettings: Settings =
        {
            options:
            {
                wrap: false,
                type: Type.Numeric,
                minValue: 0.0,
                maxValue: 10.0,
                step: 1.0
            }
        };
    }
}
