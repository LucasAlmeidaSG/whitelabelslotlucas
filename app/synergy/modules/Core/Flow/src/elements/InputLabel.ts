namespace RS.Flow
{
    /** A label which can be typed into. */
    export class InputLabel extends BaseElement<InputLabel.Settings>
    {
        private _focused = false;
        /** Whether or not the user is currently typing into this label. */
        @CheckDisposed
        public get focused() { return this._focused; }
        public set focused(value)
        {
            if (this._focused === value) { return; }
            if (value)
            {
                // Can't become focused if we're disabled
                if (!this._enabled) { return; }

                if (this._caretFlashInterval > 0)
                {
                    this._caretFlash = this.createCaretFlashTween(this._caret);
                }

                this.getHTMLParent().appendChild(this._domInput);
                this._domInput.focus();
                this._focused = true;
            }
            else
            {
                this._caretFlash = null;
                this._caret.alpha = 0;

                this._domInput.blur();
                this._domInput.remove();
                this._focused = false;
            }
            this.updatePlaceholderVisibility();
            this._onFocusChanged.publish(value);
        }

        private _enabled = true;
        /** Whether or not the user may type into this label. */
        @CheckDisposed
        public get enabled() { return this._enabled; }
        public set enabled(enabled)
        {
            if (this._enabled === enabled) { return; }
            if (enabled)
            {
                this.interactive = true;
                this.cursor = RS.Rendering.Cursor.Text;
                this._enabled = true;
            }
            else
            {
                this.focused = false;
                this.interactive = false;
                this.cursor = RS.Rendering.Cursor.Default;
                this._enabled = false;
            }
        }

        private readonly _onFocusChanged = RS.createEvent<boolean>();
        /** Published when the user focuses or blurs this text field. */
        public get onFocusChanged() { return this._onFocusChanged.public; }

        private readonly _onSubmitted = createSimpleEvent();
        /** Published when the user presses Enter whilst typing into this label. */
        @CheckDisposed
        public get onSubmitted(): IEvent { return this._onSubmitted; }

        @AutoDispose
        private readonly _onChanged = createEvent<string>();
        /** Published when the user enters a new character or removes a previous one. */
        @CheckDisposed
        public get onChanged(): IEvent<string> { return this._onChanged; }

        /** The text that has been entered into this label. */
        @CheckDisposed
        public get text(): string
        {
            const str = Localisation.resolveLocalisableString(this.locale, this._label.text);
            return this.filterInput(this._label, str);
        }
        public set text(text: string)
        {
            this._label.text = this.formatString(text);
            this.updatePlaceholderVisibility();
        }

        @AutoDisposeOnSet
        private _label: Label;
        @AutoDisposeOnSet
        private _caret: GenericElement;
        @AutoDisposeOnSet
        private _placeholder: Label;

        /** Tween used for the caret flash. */
        @AutoDisposeOnSet
        private _caretFlash: Tween<Image> | null = null;

        private _caretFlashInterval: number;
        private _maxLength: number | null;
        private _expression: RegExp | null;

        private readonly _domInput: HTMLInputElement;

        /** Gets the CSS ID for the selenium div element for this input label. */
        public get seleniumId() { return this.settings.seleniumId || null; }

        constructor(settings: InputLabel.Settings)
        {
            super(settings, null);

            this._domInput = document.createElement("input");
            this._domInput.type = "text";
            this._domInput.readOnly = false;
            this._domInput.className = "rs-text-input";
            // Try to prevent browser auto-zoom
            this._domInput.style.fontSize = "32px";
            this._domInput.style.position = "fixed";
            this._domInput.style.width = "100%";
            this._domInput.style.height = "100%";
            // Make invisible
            this._domInput.style.opacity = "1%";
            this._domInput.style.backgroundColor = "transparent";
            this._domInput.style.border = "0px";
            this._domInput.style.outline = "none";
            this._domInput.style.boxShadow = "none";
            this._domInput.style.zIndex = "-1";
            this._domInput.name = (settings.seleniumId || "") + "Input";

            this._domInput.onkeydown = this.handleKeyPress;
            this._domInput.oninput = this.handleDOMInput;

            this._domInput.onblur = () => this.focused = false;
            this.onClicked(() => this.focused = true);

            // Initial state: unfocused, enabled
            this.interactive = true;
            this.cursor = RS.Rendering.Cursor.Text;
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            this._domInput.blur();
            this._domInput.remove();
            super.dispose();
        }

        public apply(settings: Partial<InputLabel.Settings>)
        {
            let textChanged = false;

            let reformatString = false;
            if (settings.maxLength !== undefined && settings.maxLength !== this._maxLength)
            {
                this._maxLength = settings.maxLength;
                reformatString = true;
            }

            if (settings.expression !== undefined && settings.expression !== this._expression)
            {
                this._expression = settings.expression || null;
                reformatString = true;
            }

            if (settings.text)
            {
                if (settings.text.canWrap)
                {
                    Log.warn("Wrapping text inputs are not currently supported; reverting.");
                    settings.text.canWrap = false;
                }

                const newLabel = label.create(settings.text, this);
                if (this._label)
                {
                    // Carry over old filtered text for continuity
                    const oldText = this.filterInput(this._label, this._label.text as string);
                    newLabel.text = this.formatString(oldText);

                    // Check for changes
                    const newText = this.filterInput(newLabel, newLabel.text);
                    textChanged = newText !== oldText;
                }

                this._label = newLabel;
            }
            else if (reformatString)
            {
                const oldText = this._label.text as string;
                const newText = this._label.text = this.formatString(oldText);
                textChanged = newText !== oldText;
            }

            // Allow null to reset caretFlashInterval
            if (settings.caretFlashInterval !== undefined)
            {
                this._caretFlashInterval = settings.caretFlashInterval;
                if (this._caretFlashInterval > 0)
                {
                    if (this._caretFlash && !settings.caret)
                    {
                        // Recreate caret flash
                        this._caretFlash = this.createCaretFlashTween(this._caret);
                    }
                }
                else
                {
                    // Destroy existing caret flash
                    this._caretFlash = null;
                }
            }

            if (this._caretFlashInterval == null)
            {
                this._caretFlashInterval = 0;
            }

            if (settings.placeholder !== undefined)
            {
                if (settings.placeholder)
                {
                    this._placeholder = label.create(settings.placeholder, this);
                    this.updatePlaceholderVisibility();
                }
                else
                {
                    this._placeholder = null;
                }
            }

            if (settings.caret !== undefined)
            {
                if (settings.caret)
                {
                    const caret = this.createCaret(settings.caret, this);
                    caret.dock = Dock.None;
                    caret.alpha = this._caret ? this._caret.alpha : 0;

                    if (this._caretFlash)
                    {
                        // Recreate caret flash
                        this._caretFlash = this.createCaretFlashTween(caret);
                    }

                    this._caret = caret;
                }
                else
                {
                    this._caret = null;
                }
            }

            // Ensure caret above label and placeholder
            if (this._caret)
            {
                this.moveToTop(this._caret);
            }

            super.apply(settings);

            if (textChanged)
            {
                this.updatePlaceholderVisibility();
                this._onChanged.publish(this.filterInput(this._label, this._label.text as string));
            }
        }

        /** Gets specific attributes for the selenium component for this input label. */
        public seleniumDescribe(): [string, string | number][]
        {
            const attrs: [string, string | number][] = [];
            attrs.push([ "text", this.text ]);
            attrs.push([ "state", this._enabled ? "enabled" : "disabled" ]);
            return attrs;
        }

        protected updatePlaceholderVisibility()
        {
            if (!this._placeholder) { return; }
            this._placeholder.visible = this.text.length === 0;
        }

        protected getHTMLParent()
        {
            return document.getElementById("rs-w") || document.body;
        }

        @Callback
        protected handleKeyPress(ev: KeyboardEvent): void
        {
            switch (ev.keyCode)
            {
                case 8:  // backspace
                {
                    const text = Localisation.resolveLocalisableString(this.locale, this._label.text);
                    this._label.text = this.formatString(text.slice(0, -1));
                    this.updatePlaceholderVisibility();
                    this._onChanged.publish(this.filterInput(this._label, this._label.text));
                    break;
                }

                case 13: // enter
                    this.focused = false;
                    this._onSubmitted.publish();
                    break;
            }
        }

        @Callback
        protected handleDOMInput(): void
        {
            // KeyboardEvents are broken as hell cross-browser, use the DOM input to capture character string.
            const newChars = this._domInput.value;
            const existing = Localisation.resolveLocalisableString(this.locale, this._label.text);
            this._label.text = this.formatString(existing + newChars);
            this.updatePlaceholderVisibility();
            this._onChanged.publish(this.filterInput(this._label, this._label.text));
            this._domInput.value = "";
        }

        protected filterInput(label: Label, input: string): string
        {
            return label.innerText.textFilter ? label.innerText.textFilter(input) : input;
        }

        /** Formats the given string */
        protected formatString(str: string): string
        {
            if (this._expression)
            {
                const matches = str.match(this._expression);
                str = matches == null ? "" : matches.join("");
            }
            if (this._maxLength != null)
            {
                str = str.substr(0, this._maxLength);
            }
            return str;
        }

        protected onLayout()
        {
            super.onLayout();

            this.layoutCaretForLabel(this._caret, this._label);
        }

        protected layoutCaretForLabel(caret: Flow.GenericElement, label: Flow.Label): void
        {
            const { position, desiredSize, scaleFactor } = caret;

            const labelSize = label.layoutSize;
            Math.Vector2D.set(position, labelSize.w, labelSize.h / 2);
            caret.parent.toLocal(position, label, position);

            caret.scale.set(scaleFactor, scaleFactor);
            caret.pivot.set(desiredSize.w / 2, desiredSize.h / 2);
        }

        protected createCaret(settings: InputLabel.CaretSettings, parent?: GenericElement): GenericElement
        {
            switch (settings.caretKind)
            {
                case InputLabel.CaretKind.Image: return image.create(settings, parent);
                case InputLabel.CaretKind.Label: return label.create(settings, parent);
            }
            throw new Error(`Invalid caret settings: ${JSON.stringify(settings)}`);
        }

        protected createCaretFlashTween(caret: GenericElement): Tween<GenericElement>
        {
            const tween = Tween.get(caret, { loop: true, realTime: true })
                .set({ alpha: 1 })
                .wait(this._caretFlashInterval)
                .set({ alpha: 0 })
                .wait(this._caretFlashInterval);
            // Copy caret flash timing from existing caret flash
            if (this._caretFlash) { tween.position = this._caretFlash.position; }
            return tween;
        }
    }

    export namespace InputLabel
    {
        export enum CaretKind
        {
            Image,
            Label
        }

        type TypedCaretSettings<T, TKind extends CaretKind> = T & { caretKind: TKind; };

        export type LabelCaretSettings = TypedCaretSettings<Label.Settings, CaretKind.Label>;
        export type ImageCaretSettings = TypedCaretSettings<Image.Settings, CaretKind.Image>;

        export type CaretSettings = LabelCaretSettings | ImageCaretSettings;

        export interface Settings extends Partial<ElementProperties>
        {
            text: Label.Settings;
            /** Text caret/cursor, indicating to the user where text will be entered and removed from. */
            caret: CaretSettings;
            /** Optional caret flash interval, in milliseconds. */
            caretFlashInterval?: number;
            /** Optional placeholder label, shown in the absence of input. */
            placeholder?: Label.Settings;
            /** Optional maximum length of this text. Text after this will be truncated. */
            maxLength?: number;
            /** Optional regular expression describing a valid input string. */
            expression?: RegExp;
            /** Selenium ID for this input label. */
            seleniumId?: string;
        }

        export const defaultSettings: Settings =
        {
            name: "InputLabel",

            dock: Dock.Fill,
            expand: Expand.Allowed,
            sizeToContents: true,
            legacy: false,

            text:
            {
                ...Label.defaultSettings,
                name: "InputLabel",

                sizeToContents: true,
                dock: Dock.Left
            },
            caret:
            {
                name: "InputCaret",

                caretKind: CaretKind.Label,
                ...Label.defaultSettings,
                text: "|"
            }
        };
    }

    export const inputLabel = declareElement(InputLabel, InputLabel.defaultSettings);
}