namespace RS.Flow
{
    /** An image element. */
    export class Image extends BaseElement<Image.Settings>
    {
        @RS.AutoDisposeOnSet protected readonly _sprite: Rendering.IBitmap | Rendering.IAnimatedSprite | null = null;
        protected _scaleMode: Math.ScaleMode;

        protected _frameData: Rendering.Frame;
        protected _animationStarted: boolean = false;

        /** Gets the inner sprite object that backs this image. */
        public get innerSprite() { return this._sprite; }

        /** Gets or sets the scaling behaviour of this image. */
        @CheckDisposed
        public get scaleMode()
        {
            if (this._scaleMode == null)
            {
                return this.legacy ? Math.ScaleMode.Stretch : Math.ScaleMode.Contain;
            }

            return this._scaleMode;
        }
        public set scaleMode(value)
        {
            this._scaleMode = value;
            this.invalidateLayout({ reason: "scaleMode" });
        }

        public constructor(settings: Image.Settings)
        {
            super(settings, undefined);

            switch (settings.kind)
            {
                case Image.Kind.Bitmap:
                {
                    this.addChild(this._sprite = Factory.bitmap(settings.asset));
                    break;
                }

                case Image.Kind.Sprite:
                {
                    const asset = Asset.Store.getAsset(settings.asset);
                    const sheet = asset.asset as Rendering.ISpriteSheet;
                    const frameIndex = sheet.getAnimation(settings.animationName).frames[0];
                    this._frameData = sheet.getFrame(frameIndex);
                    this.addChild(this._sprite = Factory.sprite(settings.asset));
                    this._sprite.pivot.set(0, 0);
                    break;
                }

                case Image.Kind.SpriteFrame:
                {
                    let frameIndex: number;
                    const asset = Asset.Store.getAsset(settings.asset);
                    const sheet = asset.asset as Rendering.ISpriteSheet;
                    if (Is.number(settings.frameID))
                    {
                        frameIndex = settings.frameID;
                    }
                    else
                    {
                        frameIndex = sheet.getAnimation(settings.animationName).frames[0];
                    }
                    this._frameData = sheet.getFrame(frameIndex);
                    this.addChild(this._sprite = Factory.bitmap(settings.asset, frameIndex));
                    this._sprite.pivot.set(0, 0);
                    break;
                }

                case Image.Kind.Texture:
                {
                    this.addChild(this._sprite = new Rendering.Bitmap(settings.texture));
                    break;
                }

                default:
                {
                    this._sprite = null;
                    break;
                }
            }

            if (this._sprite && settings.blendMode != null) { this._sprite.blendMode = settings.blendMode; }
            if (this._sprite && settings.tint) { this._sprite.tint = settings.tint; }
            if (this._sprite && settings.alpha != null) { this._sprite.alpha = settings.alpha; }
        }

        @CheckDisposed
        public apply(props: Partial<Image.Settings>): void
        {
            if (props.scaleMode != null) { this._scaleMode = props.scaleMode; }
            super.apply(props);

            if (this._sprite)
            {
                // Apply tint even if it's null;
                this._sprite.tint = this.settings.tint = props.tint;
                if (props.blendMode != null) { this._sprite.blendMode = this.settings.blendMode = props.blendMode; }
                if (props.alpha != null) { this._sprite.alpha = this.settings.alpha = props.alpha; }
            }
        }

        public onFlowParentChanged(): void
        {
            super.onFlowParentChanged();

            this._animationStarted = false;
        }

        /** go to the start of an animation and begin playing */
        public gotoAndPlay(animationName: string, spritePlaySettings?: Rendering.IAnimatedSprite.PlaySettings): PromiseLike<void>
        {
            return this.goto(animationName, true, spritePlaySettings);
        }

        /** go to the start of an animation and pause there */
        public gotoAndStop(animationName: string, spritePlaySettings?: Rendering.IAnimatedSprite.PlaySettings): void
        {
            this.goto(animationName, false, spritePlaySettings);
        }

        /** go to the start of an animation and optionally pause there */
        protected goto(animationName: string, play: boolean, spritePlaySettings?: Rendering.IAnimatedSprite.PlaySettings): PromiseLike<void>
        {
            if (this.settings.kind === Image.Kind.Sprite)
            {
                let promise = null;
                const playSettings = spritePlaySettings ? spritePlaySettings : this.settings.playSettings;

                const sprite = this._sprite as RS.Rendering.IAnimatedSprite;
                if (play)
                {
                    promise = sprite.gotoAndPlay(animationName, playSettings);
                }
                else
                {
                    sprite.gotoAndStop(animationName);
                }
                this._animationStarted = true;
                return promise;
            }
            else
            {
                throw new Error(`gotoAnd${play ? "Play" : "Stop"}() is not supported on image kind other than sprite. (Image kind ${Image.Kind[this.settings.kind]})`);
            }
        }

        /** Calculates the minimum size we should be to fit the contents plus padding etc. */
        protected calculateContentsSize(): Math.Size2D
        {
            const innerSize = this.calculateInnerContentsSize();
            if (this.settings.border)
            {
                innerSize.w -= Spacing.widthOf(this.settings.border);
                innerSize.h -= Spacing.heightOf(this.settings.border);
            }

            return innerSize;
        }

        /** Calculates the size of the contents. */
        protected calculateInnerContentsSize(): Math.Size2D
        {
            switch (this.settings.kind)
            {
                case Image.Kind.Bitmap:
                {
                    const sprite = this._sprite as Rendering.IBitmap;
                    return { w: sprite.texture.width, h: sprite.texture.height };
                }
                case Image.Kind.Sprite:
                case Image.Kind.SpriteFrame:
                {
                    return {...this._frameData.untrimmedSize};
                }
                case Image.Kind.Texture:
                {
                    return { w: this.settings.texture.width, h: this.settings.texture.height };
                }
            }
        }

        /** Called when this element has been layouted. */
        protected onLayout(): void
        {
            if (this._sprite == null) { return; }

            // Reset sprite transform
            this._sprite.x = 0;
            this._sprite.y = 0;
            this._sprite.scaleX = this._sprite.scaleY = 1.0;

            switch (this.settings.kind)
            {
                case Image.Kind.Bitmap:
                case Image.Kind.Texture:
                {
                    // Pull sprite size and rotated layout area
                    const localBounds: Rendering.ReadonlyRectangle = this._sprite.localBounds;
                    const border = this.settings.border || Spacing.none;
                    const dims = { w: localBounds.w - Spacing.widthOf(border), h: localBounds.h - Spacing.heightOf(border) };

                    const targetDims: Math.Size2D = { ...this._layoutSize };

                    // Size our sprite to fit the layout size
                    const scale = Math.scale(dims, targetDims, this.scaleMode);
                    this._sprite.scaleX = scale.x;
                    this._sprite.scaleY = scale.y;
                    this._sprite.anchorX = this._sprite.anchorY = 0.5;

                    const alignment = this.contentAlignment;
                    this._sprite.x = this._layoutSize.w * alignment.x + localBounds.w * scale.x * (0.5 - alignment.x);
                    this._sprite.y = this._layoutSize.h * alignment.y + localBounds.h * scale.y * (0.5 - alignment.y);

                    break;
                }
                case Image.Kind.Sprite:
                case Image.Kind.SpriteFrame:
                {
                    const frameData = this._frameData;

                    const border = this.settings.border || Spacing.none;
                    const effectiveWidth = frameData.untrimmedSize.w - Spacing.widthOf(border);
                    const effectiveHeight = frameData.untrimmedSize.h - Spacing.heightOf(border);

                    const scale = Math.scale({ w: effectiveWidth, h: effectiveHeight }, this._layoutSize, this.scaleMode);

                    if (this.settings.kind === Image.Kind.SpriteFrame)
                    {
                        this._sprite.anchorX = frameData.reg.x / frameData.imageData.width;
                        this._sprite.anchorY = frameData.reg.y / frameData.imageData.height;
                    }

                    const pivotX = (frameData.reg.x + frameData.trimRect.x) / frameData.untrimmedSize.w;
                    const pivotY = (frameData.reg.y + frameData.trimRect.y) / frameData.untrimmedSize.h;

                    const widthDiff = Math.max(0, this._layoutSize.w - effectiveWidth * scale.x);
                    const heightDiff = Math.max(0, this._layoutSize.h - effectiveHeight * scale.y);

                    // Size our sprite to fit the layout size
                    this._sprite.scaleX = scale.x;
                    this._sprite.scaleY = scale.y;

                    const alignment = this.contentAlignment;
                    this._sprite.x = this._layoutSize.w * pivotX + (alignment.x - 0.5) * widthDiff;
                    this._sprite.y = this._layoutSize.h * pivotY + (alignment.y - 0.5) * heightDiff;

                    if (this.settings.kind === Image.Kind.Sprite && !this._animationStarted)
                    {
                        // Play the sprite's animation or fall back to settings if one not played yet, !== false ensures we default to autoplaying
                        this.goto((this._sprite as Rendering.IAnimatedSprite).currentAnimation || this.settings.animationName, this.settings.autoplay !== false);
                    }

                    break;
                }
            }

            if (this.settings.border)
            {
                const border = this.settings.border || Spacing.none;
                const xShift = border.right - border.left;
                const yShift = border.bottom - border.top;
                this._sprite.x += xShift * this._sprite.scaleX;
                this._sprite.y += yShift * this._sprite.scaleY;
            }

            if (this.settings.mirrorX)
            {
                this._sprite.scaleX *= -1;
            }
            if (this.settings.mirrorY)
            {
                this._sprite.scaleY *= -1;
            }
        }
    }

    export namespace Image
    {
        export enum Kind { Bitmap, Sprite, SpriteFrame, Texture }

        export interface BaseSettings<TKind extends Kind> extends Partial<ElementProperties>
        {
            kind: TKind;
            /** How this image should scale to fit its layout size. Defaults to Stretch, but Contain and Cover maintain aspect ratio. */
            scaleMode?: Math.ScaleMode;
            blendMode?: RS.Rendering.BlendMode;
            tint?: RS.Util.Color;
            alpha?: number;
            mirrorX?: boolean;
            mirrorY?: boolean;
            border?: Readonly<Spacing>;
        }

        export interface ImageSettings extends BaseSettings<Kind.Bitmap>
        {
            asset: Asset.ImageReference;
        }

        export interface SpriteSettings extends BaseSettings<Kind.Sprite>
        {
            asset: Asset.RSSpriteSheetReference;
            animationName: string;
            playSettings?: Rendering.IAnimatedSprite.PlaySettings;
            /** If the sprite should start playing when created */
            autoplay?: boolean;
        }

        export interface SpritesheetFrameSettings extends BaseSettings<Kind.SpriteFrame>
        {
            asset: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference;
            animationName: string;
            frameID?: number;
        }

        export interface TextureSettings extends BaseSettings<Kind.Texture>
        {
            texture: Rendering.ISubTexture;
        }

        /** Settings for the image element. */
        export type Settings = ImageSettings | SpriteSettings | SpritesheetFrameSettings | TextureSettings;
    }

    /** An image element. */
    export const image = declareElement(Image);
}
