namespace RS.Flow
{
    /** An element that layouts children in a specific direction. */
    export class List extends BaseElement<List.Settings>
    {
        protected _direction: List.Direction;

        /** Gets or sets the direction in which to layout elements. */
        @CheckDisposed
        public get direction() { return this._direction; }
        public set direction(value) { this._direction = value; this.invalidateLayout({ reason: "direction" }); }

        public constructor(settings: List.Settings)
        {
            super(settings, undefined);
            this._direction = settings.direction;
        }

        @CheckDisposed
        public apply(props: Partial<List.Settings>): void
        {
            if (props.direction != null) { this._direction = props.direction; }
            if (props.evenlySpaceItems != null) { this.settings.evenlySpaceItems = props.evenlySpaceItems; }
            if (props.spreadItems != null) { this.settings.spreadItems = props.spreadItems; }
            if (props.expandItems != null) { this.settings.expandItems = props.expandItems; }
            super.apply(props);
        }

        /** Calculates the minimum size we should be to fit the contents. */
        protected calculateContentsSize(): RS.Dimensions
        {
            const children = this.flowChildren;
            const sp = this._spacing;
            const result: RS.Dimensions = { w: 0, h: 0 };

            const limit = this.limitSize;
            const childLimitW = limit.w == null ? null : Math.floor(limit.w - (Spacing.widthOf(this.spacing) + Spacing.widthOf(this.padding) + Spacing.widthOf(this.innerPadding)));
            const childLimitH = limit.h == null ? null : Math.floor(limit.h - (Spacing.heightOf(this.spacing) + Spacing.heightOf(this.padding) + Spacing.heightOf(this.innerPadding)));

            switch (this._direction)
            {
                case List.Direction.TopToBottom:
                case List.Direction.BottomToTop:
                    for (const child of children)
                    {
                        child.outerLayoutSize.w = childLimitW;
                        child.outerLayoutSize.h = null;

                        if (child.visible)
                        {
                            const sz = child.desiredSize;
                            result.w = Math.max(result.w, sz.w * child.scaleFactor + (child.ignoreParentSpacing ? 0 : Spacing.widthOf(sp)));
                            result.h += sz.h * child.scaleFactor + (child.ignoreParentSpacing ? 0 : Spacing.heightOf(sp));
                        }
                    }
                    break;
                case List.Direction.LeftToRight:
                case List.Direction.RightToLeft:
                    for (const child of children)
                    {
                        child.outerLayoutSize.w = null;
                        child.outerLayoutSize.h = childLimitH;

                        if (child.visible)
                        {
                            const sz = child.desiredSize;
                            result.w += sz.w * child.scaleFactor + (child.ignoreParentSpacing ? 0 : Spacing.widthOf(sp));
                            result.h = Math.max(result.h, sz.h * child.scaleFactor + (child.ignoreParentSpacing ? 0 : Spacing.heightOf(sp)));
                        }
                    }
                    break;
            }

            return result;
        }

        /** Layouts this element AFTER it's layout size has been determined by the parent element. */
        protected performLayout()
        {
            if (this._layouting) { return; }
            this._layouting = true;

            let extraSize: number;
            if (this.settings.evenlySpaceItems || this.settings.expandItems)
            {
                const contentsSize = this.calculateContentsSize();
                if (this._direction === List.Direction.LeftToRight || this._direction === List.Direction.RightToLeft)
                {
                    extraSize = this._layoutSize.w - contentsSize.w;
                }
                else
                {
                    extraSize = this._layoutSize.h - contentsSize.h;
                }
            }
            else
            {
                extraSize = 0;
            }

            const children = this.flowChildren.filter((child) => child.visible);

            let spacingCount: number = children.length;
            if (!this.legacy && (children.length > 1 || !this.settings.evenlySpaceItems) && this.settings.spreadItems !== false)
            {
                spacingCount--;
            }
            if (this.settings.expandItems && !this.settings.evenlySpaceItems)
            {
                const vertical = this._direction === List.Direction.BottomToTop || this._direction === List.Direction.TopToBottom;
                const directionalExpand = vertical ? Expand.VerticalOnly : Expand.HorizontalOnly;
                spacingCount = children.filter((child) => child.expand === directionalExpand || child.expand === Expand.Allowed).length;
            }

            const extraSpacingPerItem = Math.max(spacingCount == 0 ? 0 : extraSize / spacingCount, 0);

            let ratio: number = 1;
            if (this.overflowMode === Flow.OverflowMode.Shrink)
            {
                const vertical = this._direction === List.Direction.BottomToTop || this._direction === List.Direction.TopToBottom;
                const contentsSizeXY = this.calculateContentsSize();

                const contentsSize = vertical ? contentsSizeXY.h : contentsSizeXY.w;
                const layoutSize = vertical ? this.innerLayoutSize.h : this.innerLayoutSize.w;
                if (contentsSize > layoutSize)
                {
                    let spacingSize = 0;

                    const spacingSizePerElement = vertical ? Spacing.heightOf(this._spacing) : Spacing.widthOf(this._spacing);
                    if (spacingSizePerElement > 0)
                    {
                        spacingSize = children.reduce((prev, curr) => curr.ignoreParentSpacing ? prev : prev + spacingSizePerElement, 0);
                    }

                    ratio = (layoutSize - spacingSize) / (contentsSize - spacingSize);
                }
            }

            switch (this._direction)
            {
                case List.Direction.TopToBottom:
                case List.Direction.BottomToTop:
                {
                    const flip = this._direction === List.Direction.BottomToTop;

                    let curY = 0;
                    for (let i = 0; i < children.length; i++)
                    {
                        const child = children[flip ? children.length - (i + 1) : i];
                        const transform = this.getChildFlowTransform(child);

                        child.outerLayoutSize.w = this._layoutSize.w;
                        child.outerLayoutSize.h = null;

                        const sp = child.ignoreParentSpacing ? Spacing.none : this._spacing;
                        const desiredSize = child.desiredSize;
                        Math.Size2D.copy(desiredSize, (child as List)._lastDesiredSize);
                        (child as List)._outerDirty = false;

                        const expandX = Expand.canExpandX(child.expand);
                        const expandY = Expand.canExpandY(child.expand);

                        child.layoutSize.w = (expandX ? this._layoutSize.w : Math.min(this._layoutSize.w, desiredSize.w * child.scaleFactor + Spacing.widthOf(sp))) - Spacing.widthOf(sp);
                        transform.x = (this._layoutSize.w - child.layoutSize.w) * this._contentAlignment.x;

                        child.layoutSize.h = child.desiredSize.h * child.scaleFactor * ratio;
                        transform.y = curY + sp.top;

                        // Expand items or just move them.
                        let extraSpacingFactor: number = 0;
                        if (this.settings.expandItems && expandY)
                        {
                            child.layoutSize.h += extraSpacingPerItem;
                            extraSpacingFactor = 0;
                        }
                        else if (this.settings.evenlySpaceItems)
                        {
                            if (i > 0 || this.legacy || this.settings.spreadItems === false)
                            {
                                transform.y += extraSpacingPerItem * 0.5;
                                extraSpacingFactor = 1;
                            }
                            else
                            {
                                if (children.length === 1) { transform.y += extraSpacingPerItem * 0.5; }
                                extraSpacingFactor = 0.5;
                            }
                        }

                        curY += child.layoutSize.h + Spacing.heightOf(sp) + extraSpacingPerItem * extraSpacingFactor;

                        transform.scaleX = transform.scaleY = child.scaleFactor;
                        transform.x += child.layoutSize.w * 0.5;
                        transform.y += child.layoutSize.h * 0.5;
                        child.layoutSize.w /= child.scaleFactor;
                        child.layoutSize.h /= child.scaleFactor;
                        child.layoutSize.w = Math.max(child.layoutSize.w, 0);
                        child.layoutSize.h = Math.max(child.layoutSize.h, 0);

                        this.updateTransformsOnChild(child);
                        this.performLayoutOnChild(child);

                        child.pivot.set(child.layoutSize.w * 0.5, child.layoutSize.h * 0.5);
                        child.skew.set(0, 0);
                    }

                    break;
                }

                case List.Direction.LeftToRight:
                case List.Direction.RightToLeft:
                {
                    const flip = this._direction === List.Direction.RightToLeft;

                    let curX = 0;
                    for (let i = 0; i < children.length; i++)
                    {
                        const child = children[flip ? children.length - (i + 1) : i];
                        const transform = this.getChildFlowTransform(child);

                        child.outerLayoutSize.w = null;
                        child.outerLayoutSize.h = this._layoutSize.h;

                        const sp = child.ignoreParentSpacing ? Spacing.none : this._spacing;
                        const desiredSize = child.desiredSize;
                        Math.Size2D.copy(desiredSize, (child as List)._lastDesiredSize);
                        (child as List)._outerDirty = false;

                        const expandX = Expand.canExpandX(child.expand);
                        const expandY = Expand.canExpandY(child.expand);

                        child.layoutSize.w = child.desiredSize.w * child.scaleFactor * ratio;
                        transform.x = curX + sp.left;

                        child.layoutSize.h = (expandY ? this._layoutSize.h : Math.min(this._layoutSize.h, desiredSize.h * child.scaleFactor + Spacing.heightOf(sp))) - Spacing.heightOf(sp);
                        transform.y = (this._layoutSize.h - child.layoutSize.h) * this._contentAlignment.y;

                        // Expand items or just move them.
                        let extraSpacingFactor: number = 0;
                        if (this.settings.expandItems && expandX)
                        {
                            child.layoutSize.w += extraSpacingPerItem;
                            extraSpacingFactor = 0;
                        }
                        else if (this.settings.evenlySpaceItems)
                        {
                            if (i > 0 || this.legacy || this.settings.spreadItems === false)
                            {
                                transform.x += extraSpacingPerItem * 0.5;
                                extraSpacingFactor = 1;
                            }
                            else
                            {
                                if (children.length === 1) { transform.x += extraSpacingPerItem * 0.5; }
                                extraSpacingFactor = 0.5;
                            }
                        }

                        curX += child.layoutSize.w + Spacing.widthOf(sp) + extraSpacingPerItem * extraSpacingFactor;

                        transform.scaleX = transform.scaleY = child.scaleFactor;
                        transform.x += child.layoutSize.w * 0.5;
                        transform.y += child.layoutSize.h * 0.5;
                        child.layoutSize.w /= child.scaleFactor;
                        child.layoutSize.h /= child.scaleFactor;

                        this.updateTransformsOnChild(child);
                        this.performLayoutOnChild(child);

                        child.pivot.set(child.layoutSize.w * 0.5, child.layoutSize.h * 0.5);
                        child.skew.set(0, 0);
                    }

                    break;
                }
            }
            if (this._maskToLayoutArea)
            {
                this.drawLayoutAreaMask();
            }

            this.updateDebugShape();

            this.onLayout();
            this._layouting = false;
            this.onLayouted.publish();
        }
    }

    export namespace List
    {
        export enum Direction { TopToBottom, LeftToRight, BottomToTop, RightToLeft }

        /** Settings for a list element. */
        export interface Settings extends Partial<ElementProperties>
        {
            /** What direction should we layout items in? */
            direction: Direction;

            /** If we are larger than we need to be, should we space items out evenly? */
            evenlySpaceItems?: boolean;

            /** If we're evenly spacing items, should we spread items to the edges of the list? Defaults true in non-legacy mode, false in legacy mode. */
            spreadItems?: boolean;

            /** If we are larger than we need to be, should we expand items? */
            expandItems?: boolean;
        }

        export let defaultSettings: Settings =
        {
            direction: Direction.TopToBottom,
            sizeToContents: true,
            evenlySpaceItems: false,
            expand: Expand.Disallowed
        };
    }

    /** An element that layouts children in a specific direction. */
    export const list = declareElement(List, List.defaultSettings);
}