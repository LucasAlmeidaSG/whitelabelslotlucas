namespace RS.Flow
{
    /** A component for sequentially displaying multiple sub-components */
    @HasCallbacks
    export class Carousel<T extends any> extends BaseElement<Carousel.Settings<T>>
    {
        @RS.AutoDisposeOnSet protected _carouselItems: Flow.GenericElement[] = [];
        
        protected _currentIndex: number = -1;
        @RS.AutoDisposeOnSet protected _advanceTween: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _tweens = [];

        constructor(settings: Carousel.Settings<T>)
        {
            super(settings, null);
            this.scheduleNextAdvance();
        }

        public addElement(element: GenericElement, index?: number)
        {
            if (index !== null)
            {
                this._carouselItems.splice(index, 0, element);
            }
            else
            {
                this._carouselItems.push(element);
            }
            this.addChild(element);
            this.resetVisibility();
            // If we just went from 1 to 2 elements, we should start animating
            if (this._carouselItems.length == 2)
            {
                this.advance();
            }
        }

        public removeElement(element: GenericElement)
        {
            const idx = this._carouselItems.indexOf(element);
            if (idx == -1) { return; }
            const item = this._carouselItems[idx];
            this._carouselItems.splice(idx, 1);
            item.visible = true;
            item.alpha = 1;
            item.postTransform.x = item.postTransform.y = 0;
            this.removeChild(element);
            this.resetVisibility();
        }

        protected async resetVisibility()
        {
            for (let i = 0; i < this._carouselItems.length; i++)
            {
                const child = this._carouselItems[i];
                if (!child.isDisposed && this.settings.reset)
                {
                    await this.settings.reset(child, this.settings.animationSettings.settings, i);
                }
                child.postTransform.x = child.postTransform.y = 0;
            }
            this._currentIndex = 0;
            if (this._carouselItems.length <= 1)
            {
                this._tweens = [];
            }
        }

        @CheckDisposed
        @Callback
        protected async advance(): Promise<void>
        {
            if (!await this.scheduleNextAdvance()) { return; }
            if (this._carouselItems.length <= 1) { return; }

            //Load up the next feature
            const nextIndex = RS.Math.wrap(this._currentIndex + 1, 0, this._carouselItems.length);
            const nextElement = this._carouselItems[nextIndex];
            
            await this.animateElement(nextElement, false);

            // Remove the current element
            if (this._currentIndex > -1 && !this.isDisposed && this._carouselItems.length > 1)
            {
                const currentElement = this._carouselItems[this._currentIndex];
                await this.animateElement(currentElement, true);
            }

            this._currentIndex = nextIndex;
        }

        protected async animateElement(element: GenericElement, out: boolean): Promise<void>
        {
            const settings = this.settings.animationSettings;
            if (out && settings && settings.animateOut)
            {
                await settings.animateOut(element, settings.settings);
            }
            else if (!out && settings && settings.animateIn)
            {
                await settings.animateIn(element, settings.settings);
            }
            else
            {
                element.visible = !out;
            }
        }

        @CheckDisposed
        protected async scheduleNextAdvance(): Promise<boolean>
        {
            try
            {
                await this.settings.display(this._carouselItems[this._currentIndex], this.settings.animationSettings.settings);
                if (this.isDisposed) { return false; }
                this.advance();
            }
            catch (exception)
            {
                if (exception instanceof RS.Tween.RejectionError) { return false; }
                throw exception;
            }
            return true;
        }
    }

    export const carousel = declareElement(Carousel);

    export namespace Carousel
    {
        export enum Direction { LeftToRight, RightToLeft, TopToBottom, BottomToTop };

        export interface Settings<T> extends Partial<ElementProperties>
        {
            animationSettings?:
            {
                settings: T;
                animateIn?: (element: GenericElement, animationSettings: T) => Promise<void>;
                animateOut?: (element: GenericElement, animationSettings: T) => Promise<void>;
            };
            display: (element: GenericElement, animationSettings: T) => Promise<void>;
            reset?: (element: GenericElement, animationSettings: T, index: number) => void;
        }

        export interface SlideAnimationSettings extends Object
        {
            direction: Direction;
            displayTime: number;
            slideSpeed: number;
            fadeSpeed: number;
            outDelay: number;
        }

        const slideElement = async (element: GenericElement, animationSettings: SlideAnimationSettings, out: boolean) =>
        {
            if (out)
            {
                await RS.Tween.wait(animationSettings.outDelay);
            }
            if (!element.isDisposed)
            {
                // Slide & fade the element
                const alphaTween = RS.Tween.get(element)
                    .set({ alpha: (out ? 1 : 0) })
                    .to({ alpha: (out ? 0 : 1) }, animationSettings.fadeSpeed);
                Disposable.bind(alphaTween, element);

                let xOffset = 0;
                let yOffset = 0;
                switch (animationSettings.direction)
                {
                    case Carousel.Direction.LeftToRight:
                        xOffset = element.localBounds.w;
                        break;
                    case Carousel.Direction.RightToLeft:
                        xOffset = -element.localBounds.w;
                        break;
                    case Carousel.Direction.BottomToTop:
                        yOffset = -element.localBounds.h;
                        break;
                    case Carousel.Direction.TopToBottom:
                        yOffset = element.localBounds.h;
                        break;
                }

                const slideTween = RS.Tween.get(element.postTransform)
                    .set({ x: out ? 0 : -xOffset, y: out ? 0 : -yOffset })
                    .to({ x: out ? xOffset : 0, y: out ? yOffset : 0 }, animationSettings.slideSpeed);
                Disposable.bind(slideTween, element);
                await slideTween;
            }
        }

        export const defaultSettings: Settings<SlideAnimationSettings> =
        {
            sizeToContents: true,
            animationSettings:
            {
                settings:
                {
                    direction: Direction.RightToLeft,
                    slideSpeed: 700,
                    outDelay: 250,
                    displayTime: 2100,
                    fadeSpeed: 700
                },
                animateIn: async (element: GenericElement, settings: SlideAnimationSettings) =>
                {
                    slideElement(element, settings, false);
                },
                animateOut: async (element: GenericElement, settings: SlideAnimationSettings) =>
                {
                    slideElement(element, settings, true);
                }
            },
            display: async (element: GenericElement, settings: SlideAnimationSettings) =>
            {
                await RS.Tween.wait(settings.displayTime);
            },
            reset: async (element: GenericElement, settings: SlideAnimationSettings, i: number) =>
            {
                element.alpha = i == 0 ? 1 : 0;
                // element.visible = i == 0;
                element.postTransform.x = element.postTransform.y = 0;
            }
        }
    }
}