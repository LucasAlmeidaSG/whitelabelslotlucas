/// <reference path="Label.ts" />
/// <reference path="Background.ts" />

namespace RS.Flow
{
    /**
     * A generic meter with a name and a numeric value.
     */
    export class Meter extends RS.Flow.BaseElement<Meter.Settings> implements SGI.Selenium.ISeleniumComponent, SGI.Selenium.ISeleniumDescriber
    {
        @AutoDispose protected _bg: Background|null;
        @AutoDispose protected _nameLabel: Label;
        @AutoDispose protected _valueLabel: Label;

        protected _value: number;
        private _textOverride: Localisation.LocalisableString | null;
        /** Gets or sets the value of this meter. */
        @RS.CheckDisposed
        public get value() { return this._value; }
        public set value(value)
        {
            if (this._value === value) { return; }
            this._value = value;
            this.updateText();
        }

        /** Text to display on this meter instead of the value. Set to null to show the value. */
        @RS.CheckDisposed
        public get textOverride() { return this._textOverride; }
        public set textOverride(value)
        {
            if (this._textOverride === value) { return; }
            this._textOverride = value;
            this.updateText();
        }

        /** Gets or sets the display text of this meter */
        public get nameLabelText() { return this._nameLabel.text }
        public set nameLabelText(str: RS.Localisation.LocalisableString) { this._nameLabel.text = RS.Localisation.resolveLocalisableString(this.locale, str); }

        /** Gets the CSS ID for the selenium div element for this meter. */
        public get seleniumId() { return this.settings.seleniumId || null; }

        public constructor(settings: Meter.Settings)
        {
            super(settings, undefined);

            if (settings.background)
            {
                this._bg = background.create(settings.background, this);
            }
            else
            {
                this._bg = null;
            }

            this._nameLabel = label.create(settings.nameLabel, this);
            this._valueLabel = label.create(settings.valueLabel, this);
        }

        /**
         * Gets specific attributes for the selenium component for this meter.
         */
        public seleniumDescribe(): [string, string | number][]
        {
            const attrs: [string, string | number][] =
            [
                [ "text", this._valueLabel.innerText.text ]
            ];
            if (this.settings.isCurrency)
            {
                attrs.push([ "currency", this.value ]);
            }
            else
            {
                attrs.push([ "value", this._value ]);
            }
            return attrs;
        }

        public apply(settings: Flow.Meter.Settings | Partial<Flow.ElementProperties>)
        {
            if (this._nameLabel && (settings as Flow.Meter.Settings).nameLabel) { this._nameLabel.apply((settings as RS.Flow.Meter.Settings).nameLabel); }
            if (this._valueLabel && (settings as Flow.Meter.Settings).valueLabel) { this._valueLabel.apply((settings as RS.Flow.Meter.Settings).valueLabel); }
            if (this._bg && (settings as Flow.Meter.Settings).background) { this._bg.apply((settings as RS.Flow.Meter.Settings).background); }
            if (this._valueLabel) { this.updateText(); }
            super.apply(settings);
        }

        protected onFlowParentChanged()
        {
            super.onFlowParentChanged();

            if (this.parent != null)
            {
                this.updateText();
            }
        }

        protected updateText()
        {
            if (this.textOverride != null)
            {
                this._valueLabel.text = this.textOverride;
                return;
            }

            const value = this._value;
            if (this.settings.isCurrency)
            {
                const cf = this.currencyFormatter;
                if (cf)
                {
                    this._valueLabel.text = cf.format(value);
                }
                else
                {
                    this._valueLabel.text = "NO CURRENCY FORMATTER";
                }
            }
            else
            {
                this._valueLabel.text = `${value}`;
            }
            if (this.settings.pluralNameText)
            {
                const str = (value === 1)
                    ? this.settings.nameLabel.text
                    : this.settings.pluralNameText;
                this._nameLabel.text = Localisation.resolveLocalisableString(this.locale, str);
            }
        }
    }

    export namespace Meter
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            nameLabel: Label.Settings;
            pluralNameText?: RS.Localisation.LocalisableString;
            valueLabel: Label.Settings;
            isCurrency: boolean;
            background?: Background.Settings;
            seleniumId?: string;
        }

        export let defaultSettings: Settings =
        {
            background: Background.defaultSettings,
            nameLabel:
            {
                font: Fonts.FrutigerWorld,
                fontSize: 24,
                textColor: Util.Colors.white,
                text: "NAME",
                sizeToContents: true,
                dock: Dock.Top
            },
            valueLabel:
            {
                font: Fonts.FrutigerWorld,
                fontSize: 20,
                textColor: Util.Colors.white,
                text: "0",
                sizeToContents: true,
                dock: Dock.Fill
            },
            spacing: Spacing.all(5),
            sizeToContents: true,
            isCurrency: false
        };
    }

    /**
     * A generic meter with a name and a value.
     */
    export const meter = RS.Flow.declareElement(Meter, Meter.defaultSettings);
}