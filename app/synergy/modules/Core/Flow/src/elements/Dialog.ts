/// <reference path="Label.ts" />
/// <reference path="Background.ts" />
/// <reference path="List.ts" />
/// <reference path="Button.ts" />

namespace RS.Flow
{
    /** A popup dialog. */
    export class Dialog extends BaseElement<Dialog.Settings> implements SGI.Selenium.ISeleniumDescriber
    {
        /** Published when an option was selected on this dialog. */
        public readonly onOptionSelected = createEvent<number>();//this._onOptionSelected.public;

        protected _background?: Background;
        protected _titleLabel?: Label;
        protected _messageLabel: OneOrMany<Label>;
        protected _buttonList: List;
        protected _messageList: List;
        protected _buttons: Button[];
        protected _enabled: boolean;
        protected _image: Image;

        //protected readonly _onOptionSelected = createEvent<number>();

        /** Gets or sets if this dialog is enabled and accepting user input. */
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            if (this._enabled === value) { return; }
            this._enabled = value;
            for (const btn of this._buttons)
            {
                btn.enabled = value;
            }
        }

        public get seleniumId() { return "dialog"; }

        public constructor(settings: Dialog.Settings)
        {
            super(settings, undefined);

            if (settings.background)
            {
                this._background = background.create(settings.background, this);
            }
            if (settings.titleLabel)
            {
                this._titleLabel = label.create(settings.titleLabel, this);
            }
            this._buttonList = list.create(settings.list, this);
            this._buttons = [];
            for (let i = 0, l = settings.buttons.length; i < l; ++i)
            {
                const btn = button.create(settings.buttons[i], this._buttonList);
                btn.dock = Dock.None;
                btn.onClicked(() => this.onOptionSelected.publish(i));
                this._buttons.push(btn);
            }

            if (RS.Is.array(settings.messageLabel))
            {
                this._messageLabel = [];
                this._messageList = list.create(settings.messageList, this);

                for (const labelSettings of settings.messageLabel)
                {
                    const messageLabel = label.create(labelSettings, this._messageList);
                    this._messageLabel.push(messageLabel);
                }
            }
            else
            {
                // Do not add to the list, for backwards compatibility
                this._messageLabel = label.create(settings.messageLabel, this);
            }

            if (this.settings.image)
            {
                this._image = image.create(this.settings.image, this);
            }

            this._enabled = true;
        }

        public seleniumDescribe(): [string, string | number][]
        {
            return [
                ["title", this._titleLabel.innerText.text ],
                ["message", Is.array(this._messageLabel) 
                    ? this._messageLabel.reduce((a, b) => a + b.innerText.text, "") 
                    : this._messageLabel.innerText.text ]
            ];
        }
    }

    export namespace Dialog
    {
        /** Settings for the dialog element. */
        export interface Settings extends Partial<ElementProperties>
        {
            background?: Background.Settings;
            titleLabel?: Label.Settings;
            messageLabel: OneOrMany<Label.Settings>;
            messageList?: List.Settings;
            list?: List.Settings;
            buttons: Button.Settings[];
            image?: Image.Settings;
        }

        /** Default settings for the dialog element. */
        export let defaultDesktopSettings: Settings =
        {
            background: Background.defaultSettings,
            titleLabel:
            {
                ...Label.defaultSettings,
                text: "TITLE",
                fontSize: 32,
                dock: Dock.Top,
                dockAlignment: { x: 0.5, y: 0.0 }
            },
            messageLabel:
            {
                ...Label.defaultSettings,
                text: "MESSAGE",
                fontSize: 24,
                dock: Dock.Fill,
                dockAlignment: { x: 0.5, y: 0.5 },
                canWrap: true,
                sizeToContents: false,
                expand: Expand.Allowed,
                align: Rendering.TextAlign.Middle
            },
            messageList: {
                ...List.defaultSettings,
                spacing: Spacing.vertical(8),
                direction: List.Direction.TopToBottom,
                dock: Dock.Fill,
                expand: Expand.Allowed,
                expandItems: false,
                overflowMode: OverflowMode.Shrink
            },
            list: { ...List.defaultSettings, dock: Dock.Bottom, direction: List.Direction.LeftToRight, contentAlignment: { x: 0.5, y: 0.5 } },
            buttons:
            [
                {
                    ...Button.defaultSettings,
                    textLabel: { ...Button.defaultSettings.textLabel, text: "OK" },
                    seleniumId: "button-ok"
                }
            ],
            sizeToContents: false,
            size: { w: 500, h: 300 },
            spacing: Spacing.all(15)
        };

        /** Default settings for the dialog element in mobile. */
        export let defaultMobileSettings: Settings =
        {
            ...defaultDesktopSettings,
            titleLabel:
            {
                ...defaultDesktopSettings.titleLabel,
                fontSize: 48
            },
            messageLabel:
            {
                ...defaultDesktopSettings.messageLabel,
                fontSize: 36
            },
            size: { w: 750, h: 450 }
        };

        export let defaultSettings: Settings = Device.isMobileDevice ? defaultMobileSettings : defaultDesktopSettings;
    }

    /** A popup dialog. */
    export const dialog = declareElement(Dialog, Dialog.defaultSettings);
}