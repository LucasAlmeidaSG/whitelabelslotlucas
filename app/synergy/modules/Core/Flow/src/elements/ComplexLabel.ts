namespace RS.Flow
{
    const tag = /<([A-Za-z][A-Za-z0-9-_]+)>|{([A-Za-z][A-Za-z0-9-_]+)}/gi;

    // TODO:
    // - binary search wrap algo

    export class ComplexLabel extends BaseElement<ComplexLabel.Settings>
    {
        private _text: Localisation.LocalisableString | Localisation.ComplexTranslationReference<object>;
        public get text() { return this._text; }
        public set text(text: Localisation.LocalisableString | Localisation.ComplexTranslationReference<object>)
        {
            if (this._text === text) { return; }
            this._text = text || "";
            this.rebuildText();
            this.invalidateLayout({ reason: "text" });
        }

        private _textReader: ComplexLabel.TextReader;

        @AutoDispose
        private readonly _lineContainer: Container = RS.Flow.container.create(
        {
            name: "LineList",
            dock: Dock.Fill,
            expand: Expand.HorizontalOnly,
            sizeToContents: true,
            spacing: this.settings.lineSpacing && Spacing.vertical(this.settings.lineSpacing / 2),
            contentAlignment: { x: this.textAlignToContentAlignment(this.settings.align), y: 0.5 }
        }, this);

        @AutoDispose
        private readonly _textObjects: GenericElement[] = [];

        @AutoDispose
        private readonly _lines: List[] = [this.createLineList(this._lineContainer)];

        constructor(settings: ComplexLabel.Settings)
        {
            super(settings, null);

            this._textReader = new ComplexLabel.TextReader(settings.styles, settings.elements);
            this._text = settings.text || "";
            this.rebuildText();
        }

        protected calculateContentsSize(out?: Math.Size2D): Math.Size2D
        {
            out = out || Math.Size2D();

            const limitSize = this.limitSize;
            if (this.settings.wrapType === ComplexLabel.WrapType.None || limitSize.w == null)
            {
                for (const { desiredSize, scaleFactor } of this._textObjects)
                {
                    out.w += desiredSize.w * scaleFactor;
                    out.h = Math.max(out.h, desiredSize.h * scaleFactor);
                }
            }
            else
            {
                this.wrap(limitSize.w, false, out);
            }

            out.w = Math.ceil(out.w);
            out.h = Math.ceil(out.h);
            return out;
        }

        protected onFlowParentChanged()
        {
            super.onFlowParentChanged();
            this.rebuildText();
        }

        /** Converts a Rendering.TextAlign to a horizontal Flow content alignment value. */
        protected textAlignToContentAlignment(align: Rendering.TextAlign): number
        {
            switch (align)
            {
                case Rendering.TextAlign.Left: return 0;
                case Rendering.TextAlign.Middle: return 0.5;
                case Rendering.TextAlign.Right: return 1.0;
                default: throw new Error(`Invalid alignment '${align}'`);
            }
        }

        protected performLayout()
        {
            if (this._textObjects)
            {
                if (this.settings.wrapType === ComplexLabel.WrapType.None)
                {
                    this.clear();
                    for (const object of this._textObjects)
                    {
                        this.addToLine(object, 0);
                    }
                }
                else
                {
                    this.wrap(this._layoutSize.w, true);
                }
            }

            super.performLayout();
        }

        /** Builds text using the current locale and text localisable string. */
        protected rebuildText(): void
        {
            const base = this._textObjects;

            // Destroy old objects.
            base.forEach((c) => c.dispose());

            // Parse text.

            const locale = this.locale;
            const str = Is.localisableString(this._text)
                ? Localisation.resolveLocalisableString(locale, this._text)
                : this._text.get(this.locale, {});
            const parts = this._textReader.parse(str);

            // Build new objects.
            base.length = parts.length;
            parts.forEach((p, i) => base[i] = this.createPartElement(p));
        }

        /** Returns a new element using the given parsed part. */
        protected createPartElement(part: ComplexLabel.Part): GenericElement
        {
            switch (part.type)
            {
                case "text": return label.create(part);
                case "image": return image.create(part.settings);
                case "delegate": return part.delegate();
                default: throw new Error(`Invalid part: ${JSON.stringify(part)}`);
            }
        }

        /** Creates a list representing a single text line. */
        protected createLineList(parent?: GenericElement): List
        {
            return list.create(
            {
                direction: List.Direction.LeftToRight,
                sizeToContents: true,
                expand: Expand.Disallowed,
                dock: Dock.Top
            }, parent);
        }

        protected clear()
        {
            for (const line of this._lines)
            {
                line.removeAllChildren();
            }
            this._lineContainer.removeAllChildren();
        }

        protected wrap(width: number, updateLines: boolean, out?: RS.Math.Size2D)
        {
            if (updateLines)
            {
                this.clear();
            }

            let cursorX = 0, cursorY = 0, lineH = 0, line = 0;
            for (const object of this._textObjects)
            {
                let desiredSize = object.desiredSize;
                if (cursorX + desiredSize.w <= width)
                {
                    // Add to this line
                    cursorX += desiredSize.w;
                    lineH = Math.max(lineH, desiredSize.h);

                    // Actually add to line?
                    if (updateLines)
                    {
                        this.addToLine(object, line);
                    }
                }
                else
                {
                    if (object instanceof Label)
                    {
                        object.suppressLayouting();
                        const text = object.text;

                        let sizeRemaining = desiredSize.w;
                        while (sizeRemaining > width - cursorX)
                        {
                            const string = RS.Localisation.resolveLocalisableString(this.locale, object.text);

                            // Find a good starting point to search for a place to split
                            const target = width - cursorX;
                            const scale = target / desiredSize.w;
                            const initialSplit = Math.max(1, Math.floor(string.length * scale));

                            // Figure out what will go on this line
                            let splitText = object.text = string.substr(0, initialSplit);
                            desiredSize = object.desiredSize;
                            if (desiredSize.w > target)
                            {
                                // Shrink text until it fits
                                // Require at least one character on the line
                                while (desiredSize.w > target && splitText.length > 1)
                                {
                                    splitText = splitText.substr(0, splitText.length - 1);
                                    object.text = splitText;
                                    desiredSize = object.desiredSize;
                                }
                            }
                            else if (desiredSize.w < target)
                            {
                                // Grow text until it overflows
                                while (desiredSize.w < target && splitText != string)
                                {
                                    splitText = splitText + string.charAt(splitText.length);
                                    object.text = splitText;
                                    desiredSize = object.desiredSize;
                                }

                                // Shrink by one character
                                splitText = splitText.substr(0, splitText.length - 1);
                                object.text = splitText;
                                desiredSize = object.desiredSize;
                            }

                            if (this.settings.wrapType === ComplexLabel.WrapType.Whitespace && string[splitText.length] !== " ")
                            {
                                // Backtrack to last whitespace
                                // Don't allow empty lines
                                const end = cursorX <= 0 ? 1 : 0;
                                for (let i = splitText.length - 1; i >= end; i--)
                                {
                                    const c = splitText[i];
                                    if (c === " " || i === 0)
                                    {
                                        splitText = splitText.substr(0, i);
                                        object.text = splitText;
                                        break;
                                    }
                                }
                            }

                            // Append first part to this line
                            if (out) { out.w = Math.max(out.w, cursorX + desiredSize.w); }
                            lineH = Math.max(lineH, desiredSize.h);

                            // Actually add to line?
                            if (updateLines)
                            {
                                const part = label.create(object.settings);
                                part.text = object.text;
                                this.addToLine(part, line);
                            }

                            // Update remaining text state
                            object.text = string.substr(splitText.length);
                            desiredSize = object.desiredSize;
                            // Wrapping should shrink, unless cursorX was non-zero (then we create a new line with cursorX at 0 and try again)
                            if (desiredSize.w >= sizeRemaining && cursorX <= 0) { throw new Error(`ComplexLabel.wrap(): failed to wrap to ${width}`); }
                            sizeRemaining = desiredSize.w;

                            if (out)
                            {
                                // Expand size to include cursor
                                out.w = Math.max(out.w, cursorX);
                                out.h = Math.max(out.h, cursorY + lineH);
                            }

                            // Start new line with the rest of the text
                            cursorX = 0;
                            cursorY += lineH;
                            ++line;
                        }

                        // Add last line?
                        if (object.text !== "")
                        {
                            cursorX += desiredSize.w;
                            lineH = Math.max(lineH, desiredSize.h);

                            // Actually add to line?
                            if (updateLines)
                            {
                                const part = label.create(object.settings);
                                part.text = object.text;
                                this.addToLine(part, line);
                            }
                        }

                        // Restore text
                        object.text = text;
                        object.restoreLayouting(false);
                    }
                    else
                    {
                        if (desiredSize.w > width)
                        {
                            // TODO shrink to fit
                            Log.warn("ComplexLabel.wrap(): text object does not fit within bounds");
                        }

                        // Add to next line
                        cursorX = desiredSize.w;
                        cursorY += lineH;
                        lineH = desiredSize.h;
                        ++line;

                        // Actually add to line?
                        if (updateLines)
                        {
                            this.addToLine(object, line);
                        }
                    }
                }

                if (out)
                {
                    // Expand size to include cursor
                    out.w = Math.max(out.w, cursorX);
                    out.h = Math.max(out.h, cursorY + lineH);
                }
            }
        }

        protected addToLine(object: GenericElement, lineIdx: number)
        {
            const line = this._lines[lineIdx] || (this._lines[lineIdx] = this.createLineList(this._lineContainer));
            if (line.parent !== this._lineContainer) { this._lineContainer.addChild(line); }
            line.addChild(object);
        }
    }

    export namespace ComplexLabel
    {
        export type StyleMap =
        {
            [name: string]: StyleSettings;
            /** Default text style. */
            default: StyleSettings;
            // TODO support variable styling
            // /** Default style for variables. */
            // variable?: StyleSettings;
        };

        export type ElementDelegate = () => GenericElement;

        export type ElementMap =
        {
            [name: string]: Image.Settings | ElementDelegate;
        };

        export enum WrapType
        {
            None,
            Word,
            Whitespace,
            // TODO WhitespacePreferred - wraps on word, but only as a last resort
        }

        export interface Settings extends Partial<ElementProperties>
        {
            text: Localisation.LocalisableString | Localisation.ComplexTranslationReference<object>;
            styles: StyleMap;
            elements: ElementMap;
            align: RS.Rendering.TextAlign;
            wrapType: WrapType;
            lineSpacing?: number;
        }

        export const defaultSettings: Settings =
        {
            wrapType: WrapType.Whitespace,//Preferred,
            sizeToContents: true,
            align: Rendering.TextAlign.Middle,
            text: null,
            styles: null,
            elements: null
        };

        export interface StyleSettings
        {
            font: Label.Settings["font"];
            fontSize: Label.Settings["fontSize"];
            textColor?: Label.Settings["textColor"];
            layers?: Label.Settings["layers"];
            textFilter?: Label.Settings["textFilter"];
            renderSettings?: Rendering.TextOptions.RenderSettings;
        }

        export interface TextPart extends StyleSettings
        {
            type: "text";
            text: string;
        }

        export interface ImagePart
        {
            type: "image";
            settings: Image.Settings;
        }

        export interface DelegatePart
        {
            type: "delegate";
            delegate: ElementDelegate;
        }

        export type Part = TextPart | ImagePart | DelegatePart;

        export class TextReader
        {
            private _currentStyle: StyleSettings;
            private _currentText: string;

            constructor(public readonly styles: ComplexLabel.StyleMap, public readonly elements: ComplexLabel.ElementMap) {}

            public parse(text: string): Part[]
            {
                if (text.length === 0) { return []; }

                const parts: Part[] = [];
                tag.lastIndex = 0;
                this._currentStyle = this.styles.default;
                this._currentText = "";

                this.parseNext(text, parts);
                return parts;
            }

            private parseNext(text: string, out: Part[]): void
            {
                let startIndex = tag.lastIndex;
                const match = tag.exec(text);

                if (match == null)
                {
                    this._currentText += text.slice(startIndex);
                    this.flushText(out);
                    return;
                }

                if (match.index > startIndex)
                {
                    this._currentText += text.slice(startIndex, match.index);
                }

                const [full] = match;
                const name = match[1] || match[2];

                if (full[0] === "<")
                {
                    const style = this.styles[name];
                    if (style)
                    {
                        this.flushText(out);
                        this._currentStyle = style;
                    }
                    else
                    {
                        if (this._currentStyle !== this.styles.default)
                        {
                            this.flushText(out);
                            this._currentStyle = this.styles.default;
                        }

                        this._currentText += `<${name}>`;
                    }
                }
                else
                {
                    const element = this.elements[name];
                    if (element)
                    {
                        this.flushText(out);
                        out.push(Is.func(element) ? { type: "delegate", delegate: element } : { type: "image", settings: element });
                    }
                    else
                    {
                        this._currentText += `{${name}}`;
                    }
                }

                this.parseNext(text, out);
            }

            private flushText(out: Part[]): void
            {
                if (!this._currentText) { return; }

                const { fontSize, font, layers, textColor, textFilter, renderSettings } = this._currentStyle;
                out.push({
                    type: "text",
                    text: this._currentText,

                    font,
                    fontSize,
                    layers,
                    textColor,
                    textFilter,
                    renderSettings
                });

                this._currentText = "";
            }
        }
    }

    export const complexLabel = declareElement(ComplexLabel, ComplexLabel.defaultSettings);
}