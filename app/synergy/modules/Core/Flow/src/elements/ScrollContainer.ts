namespace RS.Flow
{
    /**
     * A container whose internal size may be larger than it's exterior size and can scroll it's contents.
     * Take care to parent any children to scrollContainer.innerContainer and not directly to scrollContainer, otherwise they won't scroll!
     */
    export class ScrollContainer<TSettings extends ScrollContainer.Settings = ScrollContainer.Settings> extends RS.Flow.BaseElement<TSettings>
    {
        @AutoDisposeOnSet protected readonly _innerContainer: Container;

        protected readonly _scrollPosition = Math.Vector2D();

        /** The scroll position of this container. */
        public get scrollPosition(): Readonly<Math.Vector2D> { return this._scrollPosition; }
        public set scrollPosition(value)
        {
            Math.Vector2D.copy(value, this._scrollPosition);
            this.updateScrollPosition();
        }

        /** The scroll position of this container, relative to its contents. */
        public get relativeScrollPosition(): Readonly<Math.Vector2D>
        {
            const scrollPosition = Math.Vector2D.clone(this.scrollPosition);
            scrollPosition.x = this.maxScrollX > 0 ? scrollPosition.x / this.maxScrollX : 0;
            scrollPosition.y = this.maxScrollY > 0 ? scrollPosition.y / this.maxScrollY : 0;
            return scrollPosition;
        }
        public set relativeScrollPosition(value)
        {
            this._scrollPosition.x = this.maxScrollX * value.x;
            this._scrollPosition.y = this.maxScrollY * value.y;
            this.updateScrollPosition();
        }

        /**
         * Gets the inner container of this container.
         */
        public get innerContainer() { return this._innerContainer; }

        /** Maximum scroll in the x direction. */
        public get maxScrollX()
        {
            if (!this._innerContainer) { return 0; }

            const canScrollX = this.settings.scrollDirection === ScrollDirection.HorizontalOnly || this.settings.scrollDirection === ScrollDirection.Both;
            return canScrollX ? Math.max(0, this._innerContainer.layoutSize.w - this._layoutSize.w) : 0;
        }

        /** Maximum scroll in the y direction. */
        public get maxScrollY()
        {
            if (!this._innerContainer) { return 0; }

            const canScrollY = this.settings.scrollDirection === ScrollDirection.VerticalOnly || this.settings.scrollDirection === ScrollDirection.Both;
            return canScrollY ? Math.max(0, this._innerContainer.layoutSize.h - this._layoutSize.h) : 0;
        }

        public constructor(settings: TSettings)
        {
            super(settings, undefined);

            let expand: Expand;
            switch (settings.scrollDirection)
            {
                case ScrollDirection.None: expand = Expand.Allowed; break;
                case ScrollDirection.HorizontalOnly: expand = Expand.VerticalOnly; break;
                case ScrollDirection.VerticalOnly: expand = Expand.HorizontalOnly; break;
                case ScrollDirection.Both: expand = Expand.Disallowed; break;
            }
            this._innerContainer = container.create({
                name: "Inner Container",
                dock: RS.Flow.Dock.Fill,
                expand,
                sizeToContents: true
            }, this);
        }

        protected updateScrollPosition(): void
        {
            if (this.settings.restrictScrollPosition)
            {
                // Restrict scroll to limits based on actual size
                this._scrollPosition.x = Math.clamp(this._scrollPosition.x, 0, this.maxScrollX);
                this._scrollPosition.y = Math.clamp(this._scrollPosition.y, 0, this.maxScrollY);
            }

            const containerSize = this.innerContainer.layoutSize;
            const layoutSize = this.layoutSize;
            const alignment = this.contentAlignment;

            // Determine 0, 0 scroll position
            if (this.maxScrollX === 0)
            {
                // Contents smaller than size: place based on content alignment
                const origin = layoutSize.w * alignment.x;
                const offset = -containerSize.w * alignment.x;
                this._innerContainer.x = origin + offset;
            }
            else
            {
                // Place on the left
                this._innerContainer.x = this.padding.left + this.innerPadding.left;
            }

            if (this.maxScrollY === 0)
            {
                // Contents smaller than size: place based on content alignment
                const origin = layoutSize.h * alignment.y;
                const offset = -containerSize.h * alignment.y;
                this._innerContainer.y = origin + offset;
            }
            else
            {
                // Place at the top
                this._innerContainer.y = this.padding.top + this.innerPadding.top;
            }

            // Apply scroll offset
            this._innerContainer.x -= this._scrollPosition.x;
            this._innerContainer.y -= this._scrollPosition.y;
        }

        protected calculateContentsSize(out?: RS.Math.Size2D): Dimensions
        {
            out = out || Math.Size2D();

            const padW = Spacing.widthOf(this.padding);
            const padH = Spacing.heightOf(this.padding);

            const innerSize = this.calculateInnerContentsSize(out);
            innerSize.w += padW;
            innerSize.h += padH;

            return innerSize;
        }

        protected calculateInnerContentsSize(out?: RS.Math.Size2D): Dimensions
        {
            out = out || Math.Size2D();

            const child = this._innerContainer;
            if (child)
            {
                Math.Size2D.copy(child.desiredSize, out);
            }
            else
            {
                Math.Size2D.set(out, 0, 0);
            }
            return out;
        }

        /** Layouts this element AFTER it's layout size has been determined by the parent element. */
        protected performLayout()
        {
            if (this._layouting) { return; }
            this._layouting = true;

            const child = this._innerContainer;
            if (child)
            {
                // Set limit size.
                switch (this.settings.scrollDirection)
                {
                    case ScrollContainer.ScrollDirection.None:
                        Math.Size2D.copy(child.outerLayoutSize, this.innerLayoutSize);
                        break;

                    case ScrollContainer.ScrollDirection.HorizontalOnly:
                        child.outerLayoutSize.w = null;
                        child.outerLayoutSize.h = this.innerLayoutSize.h;
                        break;

                    case ScrollContainer.ScrollDirection.VerticalOnly:
                        child.outerLayoutSize.w = this.innerLayoutSize.w;
                        child.outerLayoutSize.h = null;
                        break;

                    default:
                        child.outerLayoutSize.w = child.outerLayoutSize.h = null;
                        break;
                }

                const desiredSize = child.desiredSize;
                const desiredW = desiredSize.w * child.scaleFactor;
                const desiredH = desiredSize.h * child.scaleFactor;
                const childSize = child.layoutSize;

                const expandX = Expand.canExpandX(child.expand);
                const expandY = Expand.canExpandY(child.expand);

                const scrollDirection = this.settings.scrollDirection;
                const scrollX = scrollDirection === ScrollDirection.HorizontalOnly || scrollDirection === ScrollDirection.Both;
                const scrollY = scrollDirection === ScrollDirection.VerticalOnly || scrollDirection === ScrollDirection.Both;

                if (scrollX)
                {
                    childSize.w = expandX ? Math.max(desiredSize.w, this.innerLayoutSize.w) : desiredSize.w;
                }
                else
                {
                    childSize.w = expandX ? this.innerLayoutSize.w : Math.min(this.innerLayoutSize.w, desiredSize.w);
                }

                if (scrollY)
                {
                    childSize.h = expandY ? Math.max(desiredSize.h, this.innerLayoutSize.h) : desiredSize.h;
                }
                else
                {
                    childSize.h = expandY ? this.innerLayoutSize.h : Math.min(this.innerLayoutSize.h, desiredSize.h);
                }

                child.scaleX = child.scaleY = child.scaleFactor;
                child.pivot.set(0, 0);
                child.skew.set(0, 0);
                child.rotation = 0.0;

                this.updateScrollPosition();
                this.performLayoutOnChild(child);
            }

            if (this._maskToLayoutArea)
            {
                this.drawLayoutAreaMask();
            }

            this.onLayout();
            this._layouting = false;

            this.onLayouted.publish();

            this.updateDebugShape();
        }
    }

    export namespace ScrollContainer
    {
        /* tslint:disable-next-line:no-shadowed-variable */
        export enum ScrollDirection { None, HorizontalOnly, VerticalOnly, Both }
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            /**
             * The axes along which scrolling is permitted.
             * Defaults to Both.
             */
            scrollDirection: ScrollDirection;

            /**
             * Whether or not to restrict scrolling to the bounds of the contained object.
             * Defaults to true.
             */
            restrictScrollPosition: boolean;
        }

        export const defaultSettings: Settings =
        {
            scrollDirection: ScrollDirection.Both,
            sizeToContents: false,
            restrictScrollPosition: true,
            maskToLayoutArea: true
        };
    }

    import ScrollDirection = ScrollContainer.ScrollDirection;

    /**
     * A container whose internal size may be larger than it's exterior size and can scroll it's contents.
     * Take care to parent any children to scrollContainer.innerContainer and not directly to scrollContainer, otherwise they won't scroll!
     */
    export const scrollContainer = RS.Flow.declareElement(ScrollContainer, ScrollContainer.defaultSettings);
}