/// <reference path="BaseSpinner.ts" />
/// <reference path="Label.ts" />
/// <reference path="Background.ts" />
/// <reference path="ScrollContainer.ts" />
/// <reference path="List.ts" />

namespace RS.Flow
{
    /**
     * A spinner that allows the user to select a value via scrolling a list of items.
     */
    @HasCallbacks
    export class ListSpinner extends BaseSpinner<ListSpinner.Settings>
    {
        protected _background: Background;
        protected _scrollContainer: ScrollContainer;
        protected _list: List;
        protected _dragHandler: RS.Flow.IDragHandler;
        protected _isDragging: boolean = false;

        protected _prevDragPt = Math.Vector2D();
        protected _scrollHead: number = 0;

        protected _itemMap: Map<Label> | null = null;

        /**
         * Gets or sets the current position of the scroll head.
         */
        public get scrollHead() { return this._scrollHead; }
        public set scrollHead(value)
        {
            this._scrollHead = value;
            this.updateScrollHead();
        }

        public constructor(settings: ListSpinner.Settings)
        {
            super(settings);

            if (this.settings.background)
            {
                this._background = background.create(settings.background, this);
            }

            this._scrollContainer = scrollContainer.create({
                name: "Spinner Scroll Container",
                dock: Dock.Fill,
                scrollDirection: settings.direction === ListSpinner.Direction.Horizontal ? ScrollContainer.ScrollDirection.HorizontalOnly : ScrollContainer.ScrollDirection.VerticalOnly,
                size: { w: 0, h: 0 },
                contentAlignment: settings.direction === ListSpinner.Direction.Horizontal ? { x: 0, y: 0.5 } : { x: 0.5, y: 0 },
                restrictScrollPosition: false,
                sizeToContents: true,
                maskToLayoutArea: true
            }, this);
            this._scrollContainer.onLayouted(this.updateScrollHead);
            this._dragHandler = IDragHandler.get(this._scrollContainer, this);
            this._scrollContainer.interactive = true;

            this._list = list.create({
                name: "Spinner List",
                dock: Dock.Fill,
                sizeToContents: true,
                direction: settings.direction === ListSpinner.Direction.Horizontal ? List.Direction.LeftToRight : List.Direction.TopToBottom,
                spacing: settings.direction === ListSpinner.Direction.Horizontal ? Spacing.horizontal(settings.itemSpacing || 0) : Spacing.vertical(settings.itemSpacing || 0),
            }, this._scrollContainer.innerContainer);

            this.handleOptionsChanged();
        }

        //#region IDragBehaviour
        public handleInstalled(object: RS.Rendering.IDisplayObject): void {
            return;
        }
        public shouldAcceptDrag(startPt: Math.Vector2D, endPt: Math.Vector2D, duration: number): boolean
        {
            const dx = endPt.x - startPt.x;
            const dy = endPt.y - startPt.y;
            const len = Math.sqrt(dx * dx + dy * dy);

            return len > 0 && duration > (50 / len);
        }
        public handleDragStart(startPt: Math.Vector2D): void
        {
            Math.Vector2D.copy(startPt, this._prevDragPt);
            this._isDragging = true;
            RS.Tween.removeTweens<ListSpinner>(this);
        }
        public handleDragUpdate(dragPt: Math.Vector2D): void
        {
            const dx = dragPt.x - this._prevDragPt.x;
            const dy = dragPt.y - this._prevDragPt.y;
            Math.Vector2D.copy(dragPt, this._prevDragPt);

            if (this.settings.direction === ListSpinner.Direction.Horizontal)
            {
                this._scrollHead -= dx;
            }
            else
            {
                this._scrollHead -= dy;
            }

            this.updateScrollHead();
        }
        public handleDragEnd(finalPt: Math.Vector2D): void
        {
            this._isDragging = false;
            this.snapToNearestItem();
        }
        //#endregion

        /**
         * Called when the value of this spinner has changed.
         */
        protected handleValueChanged()
        {
            if (!this._itemMap) { return; }
            const item = this._itemMap[this._value];
            if (!item)
            {
                Log.warn(`ListSpinner.handleValueChanged: no item seems to exist for the value '${this._value}'`);
                return;
            }
            this.scrollHead = this.getScrollHeadPosition(this._value);
        }

        /**
         * Called when the options of this spinner have changed.
         */
        protected handleOptionsChanged()
        {
            this.suppressLayouting();
            this._list.removeAllChildren();
            this._itemMap = {};
            for (const opt of this._options)
            {
                this._itemMap[opt] = RS.Flow.label.create({
                    ...this.settings.itemLabel,
                    text: `${opt}`
                }, this._list);
            }
            this.restoreLayouting(true);
            this.handleValueChanged();
        }

        /**
         *
         */
        @RS.Callback
        protected updateScrollHead()
        {
            const children = this._list.flowChildren;
            const firstItem = children[0] as Label;
            const lastItem = children[children.length - 1] as Label;
            if (firstItem == null || lastItem == null) { return; }

            if (this.settings.direction === ListSpinner.Direction.Horizontal)
            {
                const min = this._scrollContainer.layoutSize.w * -0.5 + firstItem.x
                const max = this._scrollContainer.layoutSize.w * -0.5 + lastItem.x;
                this._scrollHead = Math.clamp(this._scrollHead, min, max);
                this._scrollContainer.scrollPosition =
                {
                    x: this._scrollHead,
                    y: (this._scrollContainer.layoutSize.h - this._list.layoutSize.h) * -0.5
                };
            }
            else
            {
                const min = this._scrollContainer.layoutSize.h * -0.5 + firstItem.y;
                const max = this._scrollContainer.layoutSize.h * -0.5 + lastItem.y;
                this._scrollHead = Math.clamp(this._scrollHead, min, max);
                this._scrollContainer.scrollPosition =
                {
                    x: (this._scrollContainer.layoutSize.w - this._list.layoutSize.w) * -0.5,
                    y: this._scrollHead
                };
            }

            if (this.settings.fadeEdges)
            {
                const greatestPossibleDistance = (this.settings.direction === ListSpinner.Direction.Horizontal ? this._scrollContainer.layoutSize.w : this._scrollContainer.layoutSize.h) * 0.5;
                for (const itemKey in this._itemMap)
                {
                    const distFromScrollHead = Math.abs(this.getScrollHeadPosition(itemKey) - this._scrollHead);
                    this._itemMap[itemKey].alpha = Math.clamp(1.0 - (distFromScrollHead / greatestPossibleDistance), 0.0, 1.0);
                }
            }
        }

        protected snapToNearestItem(): void
        {
            let smallestDistance: number | null = null;
            let bestItemKey: string | null = null;
            for (const key in this._itemMap)
            {
                const distFromScrollHead = Math.abs(this.getScrollHeadPosition(key) - this._scrollHead);
                if (smallestDistance == null || distFromScrollHead < smallestDistance)
                {
                    smallestDistance = distFromScrollHead;
                    bestItemKey = key;
                }
            }
            if (smallestDistance == null) { return; }
            RS.Tween.get<ListSpinner>(this)
                .to({ scrollHead: this.getScrollHeadPosition(bestItemKey) }, 200, RS.Ease.quadInOut)
                .call((t) => this.value = bestItemKey);
        }

        protected getScrollHeadPosition(itemKey: string | number): number
        {
            if (!this._itemMap) { return; }
            const item = this._itemMap[itemKey];
            if (!item)
            {
                Log.warn(`ListSpinner.handleValueChanged: no item seems to exist for the value '${itemKey}'`);
                return;
            }
            if (this.settings.direction === ListSpinner.Direction.Horizontal)
            {
                return item.x - this._scrollContainer.layoutSize.w * 0.5;
            }
            else
            {
                return item.y - this._scrollContainer.layoutSize.h * 0.5;
            }
        }

        protected performLayout(): void
        {
            super.performLayout();
            if (!this._isDragging)
            {
                this.handleValueChanged();
            }
            else
            {
                this.updateScrollHead();
            }
        }
    }

    export namespace ListSpinner
    {
        export enum Direction { Horizontal, Vertical }

        export interface Settings extends BaseSpinner.Settings
        {
            background?: Background.Settings;
            itemLabel: Label.Settings;
            direction: Direction;
            itemSpacing?: number;
            fadeEdges?: boolean;
        }

        export let defaultSettings: Settings =
        {
            ...BaseSpinner.defaultSettings,
            background: Background.defaultSettings,
            itemLabel: { ...Label.defaultSettings, fontSize: 24, dock: Dock.None },
            direction: Direction.Vertical,
            itemSpacing: 10,
            fadeEdges: false
        };
    }

    /**
     * A spinner that allows the user to select a value via increment/decrement buttons.
     */
    export const listSpinner = declareElement(ListSpinner, ListSpinner.defaultSettings);
}