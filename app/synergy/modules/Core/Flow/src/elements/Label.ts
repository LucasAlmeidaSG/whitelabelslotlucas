namespace RS.Flow
{
    /** A text label. */
    export class Label<TSettings extends Label.Settings = Label.Settings> extends BaseElement<TSettings> implements SGI.Selenium.ISeleniumComponent, SGI.Selenium.ISeleniumDescriber
    {
        protected _text: Rendering.IText | Rendering.IBitmapText;
        protected _localisableText: Localisation.LocalisableString;

        /** Gets the inner text object that backs this label. */
        public get innerText(): Rendering.IText<opentype.Font | string> { return this._text; }

        /** Gets or sets the localisable text content of this label. */
        public get text() { return this._localisableText; }
        public set text(value)
        {
            if (value === this._localisableText) { return; }
            this._localisableText = value;
            this.updateText();
            if (!this.legacy) { this.invalidateLayout({ reason: "text" }); }
        }

        /** Gets or sets the tint of this element. */
        public get tint(): Util.Color { return this._text.tint; }
        public set tint(value: Util.Color)
        {
            this._text.tint = value;
        }

        /** Gets the CSS ID for the selenium div element for this button. */
        public get seleniumId() { return this.settings.seleniumId || null; }

        public constructor(settings: TSettings)
        {
            super(settings, undefined);
        }
        
        /**
         * Applies the specified set of properties to this element.
         */
        @CheckDisposed
        public apply(props: Partial<TSettings>): void
        {
            if (props.text != null) { this._localisableText = props.text; }
            let resolvedText: string
            if (props.bitmapFont != null)
            {
                if (this._text)
                {
                    resolvedText = this._text.text;
                    this.removeChild(this._text);
                    this._text.dispose();
                }
                const font = Asset.Store.getAsset(props.bitmapFont);
                this._text = new Rendering.BitmapText(font);
                this._text.font = font.definition.id;
            }
            else if (props.font || (props.textColor && this.settings.font))
            {
                if (this._text)
                {
                    resolvedText = this._text.text;
                    this.removeChild(this._text);
                    this._text.dispose();
                }
                let previousSettings = { ...(this.settings as Rendering.TextOptions.Settings), ...(props as Rendering.TextOptions.Settings)};
                this._text = new Rendering.Text({
                    ...previousSettings,
                    color: props.textColor,
                    baseline: RS.Rendering.TextBaseline.Alphabetic
                });
            }
            if (this._text)
            {
                if (resolvedText != null) { this._text.text = resolvedText; }
                if (props.fontSize != null) { this._text.fontSize = props.fontSize; }
                if (props.textColor != null) { this._text.tint = props.textColor; }
                if (props.align != null && this._text instanceof Rendering.BitmapText) { this._text.align = props.align; }
                if (props.slant != null && this._text instanceof Rendering.Text) { this._text.skew.x = Math.clamp(props.slant, -1, 1) * Math.PI * 0.5; }
                if (props.alpha != null) { this._text.alpha = props.alpha; }
                if (this._text.alpha == null) { this._text.alpha = 1; }
            }
            if (props.canWrap != null) { this.settings.canWrap = props.canWrap; }
            if (props.slant != null) { this.settings.slant = props.slant; }
            if (props.align != null) { this.settings.align = props.align; }
            if (props.bitmapFont || props.font || this.settings.font) { this.addChild(this._text); }
            if (this._text && this._localisableText != null && this.locale != null) { this.updateText(); }
            super.apply(props);
        }

        /**
         * Gets specific attributes for the selenium component for this label.
         */
        public seleniumDescribe(): [string, string | number][]
        {
            return [
                [ "text", this._text.text ]
            ];
        }

        protected updateText(): void
        {
            const resolvedText = Localisation.resolveLocalisableString(this.locale, this._localisableText);
            if (this._text.text !== resolvedText)
            {
                this._text.text = resolvedText;
                if (this.legacy) { this.invalidateLayout({ reason: "updateText" }); }
            }
        }

        /** Calculates the minimum size we should be to fit the contents without wrapping the text. */
        protected calculateContentsSize(): RS.Dimensions
        {
            const wrapWidth: number = this._text.wrapWidth;
            if (!this.legacy && this.settings.canWrap)
            {
                const expandX = this.expand === Expand.HorizontalOnly || this.expand === Expand.Allowed;
                this._text.wrapWidth = (expandX && this.outerLayoutSize.w != null) ? this.outerLayoutSize.w : Sizing.resolve(this.size, this).w;
            }

            const localBounds = this._text.localBounds;
            const skew = this._text.skew;

            if (!this.legacy && this.settings.canWrap)
            {
                this._text.wrapWidth = wrapWidth;
            }

            return RS.Math.Size2D.skew(localBounds, skew);
        }

        /** Called when this element has been layouted. */
        protected onLayout()
        {
            super.onLayout();

            if (this._text == null) { return; }

            // Reset text transform
            this._text.x = this._text.y = 0;
            this._text.scaleX = this._text.scaleY = 1;
            this._text.pivot.set(0, 0);
            if (this.settings.canWrap)
            {
                this._text.wrapWidth = this._layoutSize.w;
                const lb = this._text.localBounds;
                if (lb.h > this._layoutSize.h)
                {
                    this.wrapTextToFit(this._text, lb, this._layoutSize);
                }
            }
            else
            {
                this._text.wrapWidth = null;
            }

            // Make sure text is not bigger than our layout size
            const localBounds = this._text.localBounds;
            const skew = this._text.skew;
            const skewedBounds = RS.Math.Size2D.skew(localBounds, skew);

            this._text.pivot.set(localBounds.x, localBounds.y);

            if (skewedBounds.w > this._layoutSize.w || skewedBounds.h > this._layoutSize.h)
            {
                const scale = Math.scale(skewedBounds, this._layoutSize, Math.ScaleMode.Contain);
                this._text.scaleX = scale.x;
                this._text.scaleY = scale.y;
            }

            if (this.settings.slant < 0)
            {
                this._text.pivot.x += localBounds.w - skewedBounds.w;
            }

            // Position text within the layout area
            const alignMult = this.getAlignmentMultiplier();
            this._text.x = this._layoutSize.w * alignMult - skewedBounds.w * alignMult * this._text.scaleX;
            this._text.y = this._layoutSize.h * 0.5 - skewedBounds.h * 0.5 * this._text.scaleY;
        }

        protected wrapTextToFit(text: Rendering.IText | Rendering.IBitmapText, textBounds: Math.Size2D, layoutSize: Math.Size2D)
        {
            const tolerance = 25;
            const maxCycles = 10;

            if (!this.legacy)
            {
                let upper = layoutSize.w * (textBounds.h / layoutSize.h);
                let lower = layoutSize.w;

                // Initialise text wrapWidth halfway.
                text.wrapWidth = (upper + lower) * 0.5;

                let direction: number = -1;
                let cycle = 0;

                while (direction != 0 && cycle < maxCycles && (upper - lower) > 1)
                {
                    const midpoint = (upper + lower) * 0.5;

                    // Text too tall, increase wrapWidth.
                    if (direction < 0) { lower = midpoint; }
                    // Text too small, decrease wrapWidth.
                    else if (direction > 0) { upper = midpoint; }

                    // Set wrap to new midpoint.
                    text.wrapWidth = (upper + lower) * 0.5;

                    const scaledHeight = text.localBounds.h * (this.layoutSize.w / text.localBounds.w);
                    const delta = this.layoutSize.h - scaledHeight;
                    direction = Math.abs(delta) > tolerance ? Math.sign(delta) : 0;

                    cycle++;
                }
            }
            else
            {
                text.wrapWidth = layoutSize.w * (textBounds.h / layoutSize.h);
            }
        }

        protected getAlignmentMultiplier(): number
        {
            switch (this.settings.align)
            {
                case RS.Rendering.TextAlign.Left: return 0;
                case RS.Rendering.TextAlign.Middle: return 0.5;
                case RS.Rendering.TextAlign.Right: return 1;

                // Default align is left.
                default: return 0;
            }
        }

        /** Called when the parent of this element has changed. */
        protected onFlowParentChanged()
        {
            super.onFlowParentChanged();

            if (this.parent != null)
            {
                this.updateText();
            }
        }
    }

    export namespace Label
    {

        type ExcludedSettings = "wrapWidth" | "maxWidth" | "scaleWidth" | "scaleHeight" | "baseline" | "color";
        export interface Settings extends Partial<ElementProperties>, Omit<Rendering.TextOptions.Settings, ExcludedSettings>
        {
            text: Localisation.LocalisableString;
            /** A number from -1 to 1, describing how slanted this text is. */
            slant?: number;
            canWrap?: boolean;
            seleniumId?: string;
            textColor?: Util.Color;
            bitmapFont?: Asset.BitmapFontReference;
            alpha?: number;
        }

        /** Default settings for the label element. */
        export let defaultSettings: Settings =
        {
            text: "",
            font: Fonts.FrutigerWorld.Bold,
            fontSize: 16,
            align: Rendering.TextAlign.Middle,
            textColor: Util.Colors.white,
            canWrap: false,
            sizeToContents: true,
            expand: Expand.Disallowed
        };
    }

    /** A text label. */
    export const label = declareElement(Label, Label.defaultSettings);
}
