/// <reference path="Button.ts" />

namespace RS.Flow
{
    /** A toggleable button. */
    @HasCallbacks
    export class ToggleButton extends Button
    {
        @AutoDisposeOnSet public readonly toggled = new RS.Observable(false);

        @AutoDisposeOnSet protected readonly _upToggledBackground: Background;

        protected _autoToggle: boolean = false;

        /**
         * Gets or sets if clicking this button (when enabled) should toggle it automatically.
         */
        public get autoToggle() { return this._autoToggle; }
        public set autoToggle(value)
        {
            if (value === this._autoToggle) { return; }
            if (value)
            {
                this.onClicked(this.toggle);
            }
            else
            {
                this.onClicked.off(this.toggle);
            }
        }

        public constructor(public readonly settings: ToggleButton.Settings)
        {
            super(settings);

            if (settings.togglebackground)
            {
                this.suppressLayouting();
                this._upToggledBackground = this.createBackground(settings.togglebackground);
                this.setChildIndex(this._upToggledBackground, this.getChildIndex(this._currentBackground));
                this.restoreLayouting();
            }

            if (settings.autoToggle) { this.autoToggle = settings.autoToggle; }

            this.toggled.onChanged(this.updateBackground);
        }

        /**
         * Gets specific attributes for the selenium component for this button.
         */
        public seleniumDescribe(): [string, string | number][]
        {
            const attrs = super.seleniumDescribe();
            attrs.push([ "toggled", `${this.toggled.value}` ]);
            return attrs;
        }

        /**
         * Toggles this button.
         */
        @RS.Callback
        public toggle(): void
        {
            this.toggled.value = !this.toggled.value;
        }

        @RS.Callback
        protected updateBackground(): void
        {
            if (!this.toggled) { return super.updateBackground(); }
            const bg =
                (this._enabled ? (
                    (this._pressed && this._pressBackground) ||
                    (this._hovered && this._hoverBackground) ||
                    (this.toggled.value && this._upToggledBackground)
                ) : (
                    this._disabledBackground
                )) ||
                this._upBackground;

            this._currentBackground = bg;
        }
    }

    export namespace ToggleButton
    {
        /** Settings for the togglebutton element. */
        export interface Settings extends Button.Settings
        {
            togglebackground?: Background.Settings;
            autoToggle?: boolean;
        }

        /** Default settings for the togglebutton element. */
        export let defaultSettings: Settings =
        {
            ...Button.defaultSettings,
            hoverbackground: { dock: Dock.Fill, ignoreParentSpacing: true, kind: Background.Kind.SolidColor, color: Util.Colors.lightgrey, borderSize: 0 },
            autoToggle: true
        };
    }

    /** A toggleable button. */
    export const toggleButton = declareElement(ToggleButton, ToggleButton.defaultSettings);
}