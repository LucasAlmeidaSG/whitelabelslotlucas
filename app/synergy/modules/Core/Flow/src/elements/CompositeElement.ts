/// <reference path="Background.ts"/>
/// <reference path="CurrencyLabel.ts"/>
/// <reference path="Image.ts"/>
/// <reference path="Label.ts"/>
/// <reference path="List.ts"/>
namespace RS.Flow
{
    /**
     * An element which allows an arbitrary set of Flow elements to be defined using settings alone.
     * Useful when the constituent elements have no logical significance and are effectively just
     * extra decoration. Also supports further nested composite elements.
     */
    export class CompositeElement extends Flow.BaseElement<CompositeElement.Settings>
    {
        @RS.AutoDisposeOnSet protected readonly objects: GenericElement[] = this.buildObjects();

        constructor(settings: CompositeElement.Settings)
        {
            super(settings, null);
        }

        private buildObjects(): GenericElement[]
        {
            const objects: GenericElement[] = [];
            for (const component of this.settings.objects)
            {
                objects.push(this.buildComponent(component));
            }
            return objects;
        }

        private buildComponent(component: CompositeElement.Component): GenericElement
        {
            const element = component.factory(component.settings, this);
            this.addChild(element);
            return element;
        }
    }

    export namespace CompositeElement
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            objects: Component[];
        }

        export type ElementFactory<TSettings extends object> = (settings: TSettings, container: GenericElement) => GenericElement;

        export interface Component<TSettings extends BaseSettings = BaseSettings>
        {
            factory: ElementFactory<TSettings>;
            settings: TSettings;
        }

        export let defaultSettings: CompositeElement.Settings =
        {
            dock: Dock.Fill,
            ignoreParentSpacing: true,
            sizeToContents: true,
            spacing: Spacing.none,
            expand: Expand.Allowed,
            objects: []
        };

        export namespace Factories
        {
            export function fromFlowFactory<TObject extends BaseElement<TObjectSettings>, TObjectSettings extends object>(factory: IElementFactory<TObject, TObjectSettings>): ElementFactory<TObjectSettings>
            {
                return function (settings: TObjectSettings) { return factory.create(settings); };
            }

            export const background = fromFlowFactory(Flow.image);
            /* tslint:disable-next-line:no-shadowed-variable */
            export const compositeElement = (settings: Settings) => Flow.compositeElement.create(settings);
            export const currencyLabel = fromFlowFactory(Flow.currencyLabel);
            export const image = fromFlowFactory(Flow.image);
            export const label = fromFlowFactory(Flow.label);
            export const list = fromFlowFactory(Flow.list);
            export const complexLabel = fromFlowFactory(Flow.complexLabel);
        }

        /** Generates a texture from the given composite element settings. */
        export function composeTexture(settings: Settings, stage: Rendering.IStage, resolution: number = 1): Rendering.ISubTexture
        {
            const element = compositeElement.create(settings);
            element.size = element.desiredSize;
            element.invalidateLayout();
            const tex = element.renderToTexture(stage, resolution);
            element.dispose();
            return tex;
        }
    }

    export const compositeElement = Flow.declareElement(CompositeElement, CompositeElement.defaultSettings);
}