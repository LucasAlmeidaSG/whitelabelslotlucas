namespace RS.Flow
{
    /**
     * An element that layouts children in a grid.
     * The grid will use fixed size row heights and column widths.
     * The grid can use a fixed row and column count, or these can be variable.
     */
    export class Grid extends BaseElement<Grid.Settings>
    {
        @AutoDispose protected _border: Rendering.IGraphics | null = null;
        protected _activeSettings: Grid.Settings;

        public constructor(settings: Grid.Settings)
        {
            super(settings, undefined);
            if (settings.borderSize != null && settings.borderSize > 0)
            {
                this._border = new Rendering.Graphics();
                this.addChild(this._border);
            }
            const childRemovedHandler = this.onChildRemoved((child) =>
            {
                if (child instanceof Grid.EmptyCell) { return; }
                this.removeTrailingEmptyCells();
            });
            RS.Disposable.bind(childRemovedHandler, this);
            this._activeSettings = settings;
        }

        /**
         * Inserts the given flow element at the given column and row coordinates.
         */
        public addChildAtCell(child: GenericElement, column: number, row: number): this;
        /**
         * Inserts the given flow element at the given grid cell index.
         */
        public addChildAtCell(child: GenericElement, index: number): this;
        public addChildAtCell(child: GenericElement, p2: number, p3?: number): this
        {
            let index: number;
            if (p3 != null)
            {
                index = this.coordsToIndex(p2, p3);
            }
            else
            {
                index = p2;
            }

            const children = this.flowChildren;
            if (index == null) { index = children.length; }

            // If the cell index required is greater than the number of cells; i.e. children
            if (index >= children.length)
            {
                // Then the grid is padded out with EmptyCells...
                for (let i = children.length; i < index; i++)
                {
                    emptyCell.create({ name: "EmptyCell" }, this);
                }
                // ...and we add our child at the end.
                return this.addChild(child);
            }

            // Otherwise, get child index of child at index and add new child there.
            const otherChild = children[index];
            return this.replaceChild(otherChild, child);
        }

        /**
         * Removes the given flow element at the given column and row coordinates while preserving the grid shape.
         */
        public removeChildAtCell(column: number, row: number);
        /**
         * Removes the given flow element at the given grid cell index while preserving the grid shape.
         */
        public removeChildAtCell(index: number);
        public removeChildAtCell(p1: number, p2?: number)
        {
            const index: number = p2 != null ? this.coordsToIndex(p1, p2) : p1;
            const child = this.getChildAtCell(index);
            if (child && !(child instanceof Grid.EmptyCell))
            {
                const lastCell: boolean = index === this.flowChildren.length - 1;
                this.removeChild(child);
                // Don't replace the child if it's the last cell as there's no cells relying on it to maintain grid shape.
                if (!lastCell)
                {
                    this.addChild(emptyCell.create({ name: "EmptyCell" }), index);
                }
            }
        }

        /**
         * Gets the flow element at the given column and row coordinates.
         */
        public getChildAtCell(column: number, row: number);
        /**
         * Gets the flow element at the given grid cell index while preserving the grid shape.
         */
        public getChildAtCell(index: number);
        public getChildAtCell(p1: number, p2?: number): GenericElement
        {
            const index: number = p2 != null ? this.coordsToIndex(p1, p2) : p1;
            return this.flowChildren[index];
        }

        /** Converts col and row into index */
        public coordsToIndex(col: number, row: number): number
        {
            const major = this._activeSettings.order === Grid.Order.RowFirst ? row : col;
            const minor = this._activeSettings.order === Grid.Order.RowFirst ? col : row;
            const stride = this._activeSettings.order === Grid.Order.RowFirst ? this._activeSettings.colCount : this._activeSettings.rowCount;
            if (stride == null)
            {
                throw new Error("[Grid] Tried to convert grid coords to an index, but the grid was not of fixed size. This is not yet supported.");
            }
            const index = major * stride + minor;
            return index;
        }

        @CheckDisposed
        public apply(props: Partial<Grid.Settings>)
        {
            this._activeSettings = { ...this._activeSettings, ...props };
            super.apply(props);
        }

        /**
         * Removes trailing EmptyCells from the grid.
         */
        protected removeTrailingEmptyCells()
        {
            const children = this.flowChildren;
            for (let i = children.length - 1; i >= 0; i--)
            {
                const child = children[i];
                if (child instanceof Grid.EmptyCell)
                {
                    // Delete it.
                    this.removeChild(child);
                }
                else
                {
                    // Finish checking.
                    break;
                }
            }
        }

        /** Calculates the minimum size we should be to fit the contents. */
        protected calculateContentsSize(): RS.Dimensions
        {
            // Select children which are visible
            const children = this.flowChildren.filter((c) => c.visible);

            // Calculate their sizes
            const spacing = this.spacing;
            const childSizes = children.map((c) =>
            {
                const sz = c.desiredSize;
                if (c.ignoreParentSpacing)
                {
                    return sz;
                }
                else
                {
                    return { w: sz.w + spacing.left + spacing.right, h: sz.h + spacing.top + spacing.bottom };
                }
            });

            // Determine the row and column sizes
            const rowHeight = childSizes.map((sz) => sz.h).reduce((a, b) => Math.max(a, b), 0);
            const colWidth = childSizes.map((sz) => sz.w).reduce((a, b) => Math.max(a, b), 0);

            // Determine the row and column counts
            let rowCount = this._activeSettings.rowCount, colCount = this._activeSettings.colCount;
            if (rowCount == null)
            {
                if (colCount == null)
                {
                    if (!this.legacy)
                    {
                        // Use bounds to work out row and column counts, similar to text.
                        let wrapByWidth: boolean;
                        if (this.outerLayoutSize.w != null && this.outerLayoutSize.h != null)
                        {
                            wrapByWidth = this._activeSettings.order === Grid.Order.RowFirst;
                        }
                        else if (this.outerLayoutSize.w != null)
                        {
                            wrapByWidth = true;
                        }
                        else if (this.outerLayoutSize.h != null)
                        {
                            wrapByWidth = false;
                        }

                        if (wrapByWidth)
                        {
                            const expandX = this.expand === Expand.HorizontalOnly || this.expand === Expand.Allowed;
                            const wrapWidth = (expandX && this.outerLayoutSize.w != null) ? this.outerLayoutSize.w : Sizing.resolve(this.size, this).w;
                            colCount = Math.ceil(wrapWidth / colWidth);
                            rowCount = Math.ceil(children.length / colCount);
                        }
                        else
                        {
                            const expandY = this.expand === Expand.VerticalOnly || this.expand === Expand.Allowed;
                            const wrapHeight = (expandY && this.outerLayoutSize.h != null) ? this.outerLayoutSize.h : Sizing.resolve(this.size, this).h;
                            rowCount = Math.ceil(wrapHeight / rowHeight);
                            colCount = Math.ceil(children.length / rowCount);
                        }
                    }
                    else
                    {
                        // We have very little to go on, so guesstimate based on square
                        rowCount = Math.floor(Math.sqrt(children.length));
                        colCount = Math.ceil(children.length / rowCount);
                    }
                }
                else
                {
                    rowCount = Math.ceil(children.length / colCount);
                }
            }
            else
            {
                if (colCount == null)
                {
                    colCount = Math.ceil(children.length / rowCount);
                }
            }

            // Finish up
            return {
                w: colWidth * colCount,
                h: rowHeight * rowCount
            };
        }

        /** Layouts this element AFTER it's layout size has been determined by the parent element. */
        protected performLayout()
        {
            if (this._layouting) { return; }
            this._layouting = true;

            const layoutSize = this._layoutSize;

            // Select children which are visible
            const children = this.flowChildren.filter((c) => c.visible);

            // Calculate their sizes
            const spacing = this.spacing;
            const childSizes = children.map((c) =>
            {
                const sz = c.desiredSize;
                if (c.ignoreParentSpacing)
                {
                    return sz;
                }
                else
                {
                    return { w: sz.w + spacing.left + spacing.right, h: sz.h + spacing.top + spacing.bottom };
                }
            });

            // Determine the row and column counts + sizes
            let rowCount = this._activeSettings.rowCount, colCount = this._activeSettings.colCount;
            let rowHeight: number, colWidth: number;
            if (rowCount == null)
            {
                if (colCount == null)
                {
                    let fixedCount1: number, fixedCount2: number;
                    if (this._activeSettings.squared)
                    {
                        fixedCount1 = Math.floor(Math.sqrt(children.length));
                        fixedCount2 = Math.ceil(children.length / fixedCount1);
                    }
                    // We are variable size - let's work out our MINIMUM cell sizes
                    rowHeight = childSizes.map((sz) => sz.h).reduce((a, b) => Math.max(a, b), 0);
                    colWidth = childSizes.map((sz) => sz.w).reduce((a, b) => Math.max(a, b), 0);

                    if (this._activeSettings.order === Grid.Order.RowFirst)
                    {
                        if (fixedCount1)
                        {
                            colCount = fixedCount2;
                            rowCount = fixedCount1;
                        }
                        else
                        {
                            // How many cols can we fit?
                            colCount = Math.floor(layoutSize.w / colWidth);
                            if (colCount < 1) { colCount = 1; }
                            if (colCount > children.length) { colCount = children.length; }

                            // How many rows do we need?
                            rowCount = Math.ceil(children.length / colCount);
                        }
                    }
                    else
                    {
                        if (fixedCount1)
                        {
                            colCount = fixedCount1;
                            rowCount = fixedCount2;
                        }
                        else
                        {
                            // How many rows can we fit?
                            rowCount = Math.floor(layoutSize.h / rowHeight);
                            if (rowCount < 1) { rowCount = 1; }
                            if (rowCount > children.length) { rowCount = children.length; }

                            // How many cols do we need?
                            colCount = Math.ceil(children.length / rowCount);
                        }
                    }
                }
                else
                {
                    rowCount = Math.ceil(children.length / colCount);
                }
            }
            else
            {
                if (colCount == null)
                {
                    colCount = Math.ceil(children.length / rowCount);
                }
            }
            rowHeight = layoutSize.h / rowCount;
            colWidth = layoutSize.w / colCount;

            // Iterate each child
            const cell = new Rendering.Rectangle();
            let curCol = 0, curRow = 0;
            for (let i = 0, l = children.length; i < l; ++i)
            {
                const child = children[i];

                // Update the cell
                cell.x = curCol * colWidth;
                cell.y = curRow * rowHeight;
                cell.w = colWidth;
                cell.h = rowHeight;

                // Layout child within cell
                this.layoutChildInRect(child, cell, false);

                // Goto next cell
                if (this._activeSettings.order === Grid.Order.RowFirst)
                {
                    curCol++;
                    if (curCol >= colCount)
                    {
                        curCol = 0;
                        curRow++;
                    }
                }
                else
                {
                    curRow++;
                    if (curRow >= rowCount)
                    {
                        curRow = 0;
                        curCol++;
                    }
                }
            }

            if (this._maskToLayoutArea)
            {
                this.drawLayoutAreaMask();
            }
            this.onLayout();
            this._layouting = false;
            this.onLayouted.publish();

            if (this._activeSettings.borderSize != null && this._activeSettings.borderSize > 0 && this._border)
            {
                this._border.clear();
                this._border.lineStyle(this._activeSettings.borderSize, this._activeSettings.borderColor || RS.Util.Colors.black);

                // Outer border
                this._border.drawRect(0, 0, this._layoutSize.w, this._layoutSize.h);

                // Columns
                for (let i = 1; i < colCount; ++i)
                {
                    this._border.moveTo(i * colWidth, 0);
                    this._border.lineTo(i * colWidth, this._layoutSize.h);
                }

                // Rows
                for (let i = 1; i < rowCount; ++i)
                {
                    this._border.moveTo(0, i * rowHeight);
                    this._border.lineTo(this._layoutSize.w, i * rowHeight);
                }
            }

            this.updateDebugShape();
            if (this._debugShape)
            {
                this.removeChild(this._debugShape);
                this.addChild(this._debugShape);
            }
        }
    }

    export namespace Grid
    {
        export class EmptyCell extends Container { }

        /**
         * Determines how to organise children into grid layout.
         */
        export enum Order
        {
            /** Fill out row by row. */
            RowFirst,

            /** Fill out column by column. */
            ColumnFirst
        }

        /** Settings for a grid element. */
        export interface Settings extends Partial<ElementProperties>
        {
            rowCount?: number;
            colCount?: number;
            /**
             * If true, the grid will arrange its cells in as close to a square as possible.
             * For example, 15 children will be laid out in 4 columns and 4 rows.
             */
            squared?: boolean;
            /** Whether this grid should arrange itself row-first or column-first. */
            order: Order;
            borderSize?: number;
            borderColor?: Util.Color;
        }

        export let defaultSettings: Settings =
        {
            order: Order.RowFirst
        };
    }

    const emptyCell = declareElement(Grid.EmptyCell);
    export const grid = declareElement(Grid, Grid.defaultSettings);
}