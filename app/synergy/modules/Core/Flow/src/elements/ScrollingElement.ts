namespace RS.Flow
{
    export abstract class ScrollingElement<TElementSettings = void, TRuntimeData = void, TElement extends BaseElement<TElementSettings, TRuntimeData> = BaseElement<TElementSettings, TRuntimeData>, TSettings extends ScrollingElement.Settings = ScrollingElement.Settings> extends ScrollContainer<TSettings>
    {
        @AutoDisposeOnSet protected readonly _element: TElement = this.createElement();
        @AutoDisposeOnSet protected _timeline: Timeline;

        protected _lastScrollX: number = 0;

        protected _lastScrollY: number = 0;

        protected _direction: ScrollingElement.Direction;

        private _progress: number;

        public get isHorizontal(): boolean { return this._direction < 2; }

        public get isReverse(): boolean { return this._direction % 2 == 1; }

        public get progress(): number { return this._progress; }
        public set progress(value: number)
        {
            if (this._progress === value) { return; }
            this._progress = value;
            this.updateProgress();
        }

        public constructor(settings: TSettings)
        {
            super(settings);
            this._direction = settings.direction ? settings.direction : ScrollingElement.Direction.LeftToRight;
        }

        public resetTween()
        {
            this.updateTweenState(true);
        }

        protected onFlowParentChanged(): void
        {
            super.onFlowParentChanged();

            if (!this.innerContainer || this.innerContainer.isDisposed) { return; }

            if (this.parent)
            {
                this.updateTweenState(true);
            }
            else
            {
                this.reset();
            }
        }

        protected onLayout(): void
        {
            super.onLayout();

            if (!this.innerContainer) { return; }

            this.updateProgress();
            this.updateTweenState(false);
        }

        protected updateTweenState(reset: boolean): void
        {
            if (this.maxScrollX > 0 || this.maxScrollY > 0)
            {
                // Check if the size has changed.
                if (!reset && ((this.isHorizontal && this._lastScrollX === this.maxScrollX) || (!this.isHorizontal && this._lastScrollY === this.maxScrollY))) { return; }
                this._lastScrollX = this.maxScrollX;
                this._lastScrollY = this.maxScrollY;

                if (reset) { this.progress = 0; }
                this.createMarqueeTimeline();
            }
            else
            {
                this.reset();
            }
        }

        protected createMarqueeTimeline(): void
        {
            const distance = this.isHorizontal ? this.maxScrollX : this.maxScrollY;
            const scrollTime = distance / this.settings.speed;

            this.innerContainer.alpha = 1;
            this._timeline = Timeline.get(
            [
                Tween.get(this.innerContainer)
                    .wait(this.settings.fadeTime + this.settings.interval * 2 + scrollTime)
                    .to({ alpha: 0 }, this.settings.fadeTime)
                    .call(() =>
                    {
                        this._timeline = Timeline.get(
                        [
                            Tween.get(this.innerContainer)
                                .to({ alpha: 1 }, this.settings.fadeTime)
                                .wait(this.settings.interval * 2 + scrollTime)
                                .to({ alpha: 0 }, this.settings.fadeTime),

                            Tween.get<ScrollingElement<TElementSettings, TRuntimeData>>(this)
                                .set({ progress: 0 })
                                .wait(this.settings.fadeTime + this.settings.interval)
                                .to({ progress: 1 }, scrollTime)
                        ], { loop: true });
                    }),

                Tween.get<ScrollingElement<TElementSettings, TRuntimeData>>(this)
                    .set({ progress: 0 })
                    .wait(this.settings.fadeTime + this.settings.interval)
                    .to({ progress: 1 }, scrollTime)
            ]);
            this._timeline.onPositionChanged(this.onTweenUpdated);
            this._timeline.setPosition(this.progress * this._timeline.duration);
        }

        protected updateProgress(): void
        {
            const newProgress = this.isReverse ? 1 - this.progress : this.progress;
            this._scrollPosition.x = newProgress * this.maxScrollX;
            this._scrollPosition.y = newProgress * this.maxScrollY;
            this.updateScrollPosition();
        }

        @Callback
        protected onTweenUpdated(): void
        {
            this.updateScrollPosition();
        }

        protected reset()
        {
            this._timeline = null;
            this._lastScrollX = 0;
            this._lastScrollY = 0;
            this._scrollPosition.x = this.isReverse ? this.maxScrollX : 0;
            this._scrollPosition.y = this.isReverse ? this.maxScrollY : 0;
            this.updateScrollPosition();
            this.innerContainer.alpha = 1;
        }

        /** Creates the child element and adds it to this.innerElement as a child */
        protected abstract createElement(): TElement
    }

    export namespace ScrollingElement
    {
        export enum Direction
        {
            LeftToRight = 0,
            RightToLeft = 1,
            TopToBottom = 2,
            BottomToTop = 3
        }
        export interface Settings extends ScrollContainer.Settings
        {
            /** Duration of fade. */
            fadeTime: number;
            /** Time to hold the message still for. */
            interval: number;
            /** Speed at which to scroll the message, in pixels per millisecond. Used to calculate tween time. */
            speed: number;
            /** Specifies in which direction the scrolling should occur, default is LeftToRight */
            direction?: Direction;
        }
    }

}