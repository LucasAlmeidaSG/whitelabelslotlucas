/// <reference path="Label.ts" />

namespace RS.Flow
{
    /** A currency label. */
    export class CurrencyLabel extends Label
    {
        protected _value: number = 0;

        /** Gets or sets the localisable text content of this label. */
        public get text() { return this._localisableText; }
        public set text(value)
        {
            if (value == null || value === "") { return; }
            Log.warn(`Setting 'text' on Flow.CurrencyLabel not supported`);
        }

        /** Gets or sets the currency amount of this label. */
        public get currencyValue() { return this._value; }
        public set currencyValue(value)
        {
            if (value === this._value) { return; }
            this._value = value;
            this.updateText();
            if (!this.legacy) { this.invalidateLayout({ reason: "currencyValue" }); }
        }

        protected updateText(): void
        {
            let resolvedText: string;
            const cf = this.currencyFormatter;
            if (cf == null)
            {
                resolvedText = "NO CURRENCY FORMATTER";
            }
            else
            {
                resolvedText = cf.format(this._value);
            }
            if (this._text.text !== resolvedText)
            {
                this._text.text = resolvedText;
                if (this.legacy) { this.invalidateLayout({ reason: "updateText" }); }
            }
        }
    }

    /** A currency label. */
    export const currencyLabel = declareElement(CurrencyLabel, Label.defaultSettings);
}