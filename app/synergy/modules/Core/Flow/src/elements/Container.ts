namespace RS.Flow
{
    /** A basic container element. */
    export class Container extends BaseElement<Container.Settings>
    {
        public constructor(settings: Container.Settings) { super(settings, undefined); }
    }

    export namespace Container
    {   
        /** Settings for a container element. */
        export interface Settings extends Partial<ElementProperties>
        {

        }

        /** Creates a container which overlays all other children in a container without affecting any of them. */
        export function createLayer(parent?: GenericElement): Container
        {
            return container.create(
            {
                // Centre
                dock: Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 },
                
                // Take up full size
                sizeToContents: false,
                size: { w: Sizing.Fraction(1), h: Sizing.Fraction(1) }
            }, parent);
        }
    }

    /** A basic container element. */
    export const container = declareElement(Container);
}