/// <reference path="Label.ts" />
/// <reference path="Image.ts" />
/// <reference path="List.ts" />
namespace RS.Flow
{
    export namespace ComplexText
    {
        /** Generates a list of flow labels with different label settings for each part of a complex translation. */
        export function generateComplexText(settings: ComplexTextSettings, parent?: GenericElement)
        {
            const list = settings.listSettings instanceof List ? settings.listSettings : new Flow.List({ ...Flow.List.defaultSettings, ...settings.listSettings });
            const parts = settings.translation.getParts(settings.locale);
            let defaultTextSettings = settings.textSettings;
            if (defaultTextSettings == null)
            {
                Log.warn(`ComplexText textSettings were not defined, falling back to default label settings.`);
                defaultTextSettings = Flow.Label.defaultSettings;
            }
            for (const part of parts)
            {
                let labelSettings = defaultTextSettings;
                let labelText: string = "";
                switch (part.kind)
                {
                    case Localisation.TranslationPart.Kind.Text:
                        labelText = part.value;
                        break;

                    case Localisation.TranslationPart.Kind.Variable:
                        if (settings.imageVariableSettings)
                        {
                            const imageSettings = settings.imageVariableSettings(part.value);
                            if (imageSettings != null)
                            {
                                Flow.image.create(imageSettings, list);
                                continue;
                            }
                        }
                        labelSettings = resolveSettings(settings.variableSettings, labelSettings, part.value);
                        labelText = resolveSubstitution(settings.substitutions, part.value);
                        break;

                    case Localisation.TranslationPart.Kind.Protected:
                        labelSettings = resolveSettings(settings.protectedSettings, labelSettings, part.value);
                        labelText = resolveSubstitution(settings.substitutions, part.value);
                        break;
                }
                Flow.label.create({ ...labelSettings, text: labelText }, list);
            }

            if (parent)
            {
                parent.addChild(list);
            }
            return list;
        }

        /** resolves the subsitutions label settings */
        function resolveSettings(settingsFunction: Label.Settings | ((variableName: string) => Label.Settings | null), defaultSetting: Flow.Label.Settings, key: string): Flow.Label.Settings
        {
            if (Is.func(settingsFunction))
            {
                return settingsFunction(key) || defaultSetting;
            }
            else if (settingsFunction != null)
            {
                return settingsFunction;
            }
            else
            {
                return defaultSetting;
            }
        }

        /** Returns the appropriate subsitution key, if one is present */
        function resolveSubstitution(map: object, key: string): string
        {
            if (map[key] != null)
            {
                return map[key];
            }
            Log.error(`Failed to find substitution for complex text, substitution variable key was "${key}", ensure you use string keys to avoid potential issues when ClosureCompiling.`);
            return "";
        }

        export interface ComplexTextSettings
        {
            /** locale to translate labels into */
            locale: Localisation.Locale,
            /** base translation for the text */
            translation: Localisation.ComplexTranslationReference,
            /** key value pairs for subsitutions */
            substitutions: Localisation.Substitutions,
            /** settings for nonvariable and nonprotected text parts */
            textSettings: Label.Settings,
            /** settings for variable text parts, or a function which can return different settings pervariable */
            variableSettings?: Label.Settings | ((variableName: string) => Label.Settings | null),
            /** settings for protected term text parts, or a function which can return different settings perterm */
            protectedSettings?: Label.Settings | ((protectedName: string) => Label.Settings | null),
            /** settings for the list itself, or alternatively an actual list to put the text parts in */
            listSettings?: List.Settings | List,
            /** settings for variable text parts to be replaced with images, image substitutions are higher priority than text */
            imageVariableSettings?: ((variableName: string) => Image.Settings | null)
        }
    }
}