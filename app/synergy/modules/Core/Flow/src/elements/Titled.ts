/// <reference path="Label.ts" />

namespace RS.Flow
{
    /** An element that wraps another, adding a title label. */
    export class Titled<TInner extends BaseElement<any>> extends BaseElement<Titled.Settings, TInner>
    {
        protected _title: Label;
        protected _inner: TInner;

        /** Gets the inner element of this titled element. */
        public get innerElement() { return this._inner; }

        /** Gets the title label for this titled element. */
        public get label() { return this._title; }

        public constructor(settings: Titled.Settings, inner: TInner)
        {
            super(settings, inner);
            this._inner = inner;

            this._title = label.create(settings.title, this);
            this.updateTitleDock();

            this.addChild(inner);
            inner.dock = Dock.Fill;
        }

        protected updateTitleDock()
        {
            switch (this.settings.side)
            {
                case Titled.Side.Top:
                    this._title.dock = Dock.Top;
                    break;
                case Titled.Side.Right:
                    this._title.dock = Dock.Right;
                    break;
                case Titled.Side.Bottom:
                    this._title.dock = Dock.Bottom;
                    break;
                case Titled.Side.Left:
                    this._title.dock = Dock.Left;
                    break;
            }
        }
    }

    export namespace Titled
    {
        export enum Side { Top, Right, Bottom, Left }

        export interface Settings extends Partial<ElementProperties>
        {
            side: Side;
            title: Label.Settings;
        }

        export let defaultSettings: Settings =
        {
            side: Side.Top,
            title:
            {
                ...Label.defaultSettings,
                text: "TITLE"
            }
        };
    }

    /** An element that wraps another, adding a title label. */
    export const titled = declareElement<Titled<any>, Titled.Settings, GenericElement>(Titled, true, Titled.defaultSettings);
}