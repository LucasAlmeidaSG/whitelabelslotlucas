/// <reference path="Background.ts" />

namespace RS.Flow
{
    /** A progress bar. */
    export class ProgressBar extends BaseElement<ProgressBar.Settings> implements SGI.Selenium.ISeleniumDescriber
    {
        protected _progress: number;
        protected _actualProgress: number;
        protected _graphics: Rendering.IGraphics;
        protected _background: Background;

        /**
         * Gets or sets the progress of this bar.
         */
        @CheckDisposed
        public get progress() { return this._progress; }
        public set progress(value)
        {
            if (this.settings.smoothTime && value > this._actualProgress)
            {
                RS.Tween.get<any>(this, { onChange: () => this.redraw(), override: true })
                    .to({_progress: value}, this.settings.smoothTime, this.settings.smoothEase);
            }
            else
            {
                this._progress = value;
                this.redraw();
            }
            this._actualProgress = value;
        }

        public get seleniumId() { return this.settings.seleniumId || null; }

        public constructor(settings: ProgressBar.Settings)
        {
            super(settings, undefined);

            this._progress = 0;
            this._actualProgress = 0;

            if (settings.background)
            {
                this._background = background.create(settings.background, this);
            }

            this._graphics = new Rendering.Graphics();
            this.addChild(this._graphics);
        }

        public seleniumDescribe(): [string, string | number][]
        {
            return [[ "progress", (this._progress * 100).toFixed(0) ]];
        }

        /**
         * Disposes this progress bar.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }
            Tween.removeTweens<ProgressBar>(this);
            super.dispose();
        }

        /** Called when this element has been layouted. */
        protected onLayout()
        {
            super.onLayout();

            if (this._graphics == null) { return; }
            
            // Reset transform
            this._graphics.x = this._graphics.y = 0;
            this._graphics.scaleX = this._graphics.scaleY = 1.0;

            // Redraw
            this.redraw();
        }

        protected redraw()
        {
            const d = this._progress / this.settings.divisor;
            this._graphics.clear();
            if (this.settings.borderSize > 0)
            {
                this._graphics
                    .lineStyle(this.settings.borderSize, this.settings.borderColor, 1.0)
                    .moveTo(0, 0)
                    .lineTo(this._layoutSize.w, 0)
                    .lineTo(this._layoutSize.w, this._layoutSize.h)
                    .lineTo(0, this._layoutSize.h)
                    .lineTo(0, 0);
            }
            const s = this.settings.borderSize + this.settings.borderSpacing;
            const w = ((this._layoutSize.w - s*2) * d)|0;
            const r = Math.min(w, this.settings.barCornerRadius);
            if (r > 0)
            {
                this._graphics
                    .beginFill(this.settings.barColor, 1.0)
                    .drawRoundedRect(s, s, w, this._layoutSize.h - s*2, r)
                    .endFill();
            }
            else
            {
                this._graphics
                    .beginFill(this.settings.barColor, 1.0)
                    .drawRect(s, s, w, this._layoutSize.h - s*2)
                    .endFill();
            }
        }
    }

    export namespace ProgressBar
    {
        /** Settings for the progress bar element. */
        export interface Settings extends Partial<ElementProperties>
        {
            seleniumId?: string;

            divisor: number;
            smoothTime: number;
            smoothEase: EaseFunction;

            borderSize: number;
            borderColor: Util.Color;
            borderSpacing: number;

            barColor: Util.Color;
            barCornerRadius: number;

            background?: Background.Settings;
        }

        /** Default settings for the label element. */
        export let defaultSettings: Settings =
        {
            seleniumId: "progress_bar",

            divisor: 100.0,
            smoothTime: 400,
            smoothEase: RS.Ease.quadInOut,

            borderSize: 2,
            borderColor: Util.Colors.white,

            borderSpacing: 2,

            barColor: Util.Colors.white,
            barCornerRadius: 0
        };
    }

    /** A progress bar. */
    export const progressBar = declareElement(ProgressBar, ProgressBar.defaultSettings);
}