/// <reference path="BaseSpinner.ts" />
/// <reference path="Label.ts" />
/// <reference path="Button.ts" />
/// <reference path="Background.ts" />

namespace RS.Flow
{
    /**
     * A spinner that allows the user to select a value via increment/decrement buttons.
     */
    @HasCallbacks
    export class Spinner extends BaseSpinner<Spinner.Settings>
    {
        /** Published when the inner button was clicked. */
        public readonly onInnerButtonClicked = createSimpleEvent();

        protected _background: Background;
        protected _innerPanel: Container;
        protected _inner: Label | Button;
        protected _incButton: Button;
        protected _decButton: Button;
        protected _soundIndex: number = 0;

        /** Gets or sets if the spinner's buttons are enabled. */
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            this._enabled = value;
            this.updateButtonsEnabled();
        }

        public constructor(settings: Spinner.Settings)
        {
            super(settings);

            if (this.settings.incrementButton != null)
            {
                this._incButton = button.create(settings.incrementButton, this);
                this._incButton.onClicked(this.increment);
            }
            if (this.settings.decrementButton != null)
            {
                this._decButton = button.create(settings.decrementButton, this);
                this._decButton.onClicked(this.decrement);
            }
            this._innerPanel = container.create({ ...Spinner.defaultSettings.container, ...settings.container}, this);
            if (this.settings.background != null)
            {
                this._background = background.create(settings.background, this._innerPanel);
            }
            if (this.settings.initialSoundIndex != null)
            {
                this._soundIndex = settings.initialSoundIndex;
            }

            switch (settings.innerType)
            {
                case Spinner.InnerType.Button:
                    this._inner = button.create(settings.button, this._innerPanel);
                    this._inner.onClicked((ev) => this.onInnerButtonClicked.publish());
                    if (this._inner.label) { this._inner.label.text = `${this.value}`; }
                    break;

                case null:
                case undefined:
                case Spinner.InnerType.Label:
                    this._inner = label.create(settings.label, this._innerPanel);
                    this._inner.text = `${this.value}`;
                    break;
            }
        }

        /**
         * @inheritdoc
         */
        @Callback
        public increment(): void
        {
            super.increment();
            if (this.settings.sounds)
            {
                this._soundIndex = RS.Math.mod(this._soundIndex + 1, this.settings.sounds.length);
                Audio.play(this.settings.sounds[this._soundIndex], this.settings.soundsSettings);
            }
        }
        /**
         * @inheritdoc
         */
        @Callback
        public decrement(): void
        {
            super.decrement();
            if (this.settings.sounds)
            {
                this._soundIndex = RS.Math.mod(this._soundIndex - 1, this.settings.sounds.length);
                Audio.play(this.settings.sounds[this._soundIndex], this.settings.soundsSettings);
            }
        }

        /**
         * @inheritdoc
         */
        protected handleValueChanged()
        {
            if (this._inner)
            {
                if (this._inner instanceof Label)
                {
                    this._inner.text = `${this.value}`;
                }
                else if (this._inner instanceof Button && this._inner.label)
                {
                    this._inner.label.text = `${this.value}`;
                    if (this._inner.disabledLabel) { this._inner.disabledLabel.text = `${this.value}`; }
                }
            }
            this.updateButtonsEnabled();
        }

        /**
         * Called when the options of this spinner have changed.
         */
        protected handleOptionsChanged()
        {
            this.updateButtonsEnabled();
        }

        protected updateButtonsEnabled()
        {
            switch (this._optionsSettings.type)
            {
                case Spinner.Type.Numeric:
                case Spinner.Type.Options:
                    if (this._incButton) { this._incButton.enabled = this._enabled && this.getIncremented(this.value) !== this.value; }
                    if (this._decButton) { this._decButton.enabled = this._enabled && this.getDecremented(this.value) !== this.value; }
                    break;
                case Spinner.Type.Function:
                    if (this._incButton) { this._incButton.enabled = this._enabled && this.getIncremented(this._funcIndex) !== this.value; }
                    if (this._decButton) { this._decButton.enabled = this._enabled && this.getDecremented(this._funcIndex) !== this.value; }
                    break;
            }
            if (this._inner && this._inner instanceof Button)
            {
                this._inner.enabled = this.enabled;
            }
        }
    }

    export namespace Spinner
    {
        export import Type = BaseSpinner.Type;

        export enum InnerType { Label, Button }

        export interface BaseSettings<TInnerType extends InnerType> extends BaseSpinner.Settings
        {
            innerType?: TInnerType;
            background?: Background.Settings;
            incrementButton?: Button.Settings;
            decrementButton?: Button.Settings;
            sounds?: Asset.SoundReference[];
            soundsSettings?: Partial<RS.Audio.PlaySettings>;
            container?: Flow.Container.Settings;
            initialSoundIndex?: number;
        }

        export interface LabelSettings extends BaseSettings<InnerType.Label>
        {
            label: Label.Settings;
        }

        export interface ButtonSettings extends BaseSettings<InnerType.Button>
        {
            button: Button.Settings;
        }

        export type Settings = LabelSettings | ButtonSettings;

        export let defaultSettings: LabelSettings =
        {
            ...BaseSpinner.defaultSettings,
            innerType: InnerType.Label,
            label: { ...Label.defaultSettings, fontSize: 24, dock: Dock.Fill },
            background: Background.defaultSettings,
            incrementButton: { ...Button.defaultSettings, textLabel: { ...Button.defaultSettings.textLabel, text: "+", fontSize: 24 }, spacing: Spacing.all(3), dock: Dock.Right },
            decrementButton: { ...Button.defaultSettings, textLabel: { ...Button.defaultSettings.textLabel, text: "-" , fontSize: 24 }, spacing: Spacing.all(3), dock: Dock.Left },
            container: { dock: Dock.Fill, sizeToContents: true }
        };
    }

    /**
     * A spinner that allows the user to select a value via increment/decrement buttons.
     */
    export const spinner = declareElement(Spinner, Spinner.defaultSettings);
}
