/// <reference path="ScrollingElement.ts" />
namespace RS.Flow
{
    export class ScrollingComplexText<TSettings extends ScrollingComplexText.Settings = ScrollingComplexText.Settings> extends ScrollingElement<List.Settings, void, List, ScrollingComplexText.Settings>
    {
        protected createElement()
        {
            return ComplexText.generateComplexText({
                ...this.settings.text,
                locale: this.locale || this.settings.text.locale
            }, this.innerContainer);
        }
    }

    export namespace ScrollingComplexText
    {
        export interface Settings extends ScrollingElement.Settings
        {
            text: ComplexText.ComplexTextSettings;
        }

        export const defaultSettings: Settings =
        {
            fadeTime: 200,
            interval: 300,
            speed: 0.1,
            scrollDirection: ScrollContainer.ScrollDirection.HorizontalOnly,
            restrictScrollPosition: true,
            maskToLayoutArea: true,
            dock: RS.Flow.Dock.Fill,
            text: null,
            sizeToContents: false
        }
    }

    export const scrollingComplexText = declareElement(ScrollingComplexText, ScrollingComplexText.defaultSettings);
}