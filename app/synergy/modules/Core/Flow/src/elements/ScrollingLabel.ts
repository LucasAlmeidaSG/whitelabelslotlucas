/// <reference path="Label.ts" />

namespace RS.Flow
{
    // TODO: (Synergy 1.2) extend ScrollingElement instead
    /** A text label that can fit in a smaller space by horizontally scrolling the text. */
    export class ScrollingLabel extends Label<ScrollingLabel.Settings>
    {
        protected _scrolling: boolean = false;
        protected _scrollWidth: number = 0;
        protected _scrollPosition: number = 0;
        protected _scrollTween: Tween<Rendering.IText> | null = null;

        public constructor(settings: ScrollingLabel.Settings)
        {
            super(settings);
        }

        /** Disposes this element. */
        public dispose()
        {
            if (this.isDisposed) { return; }
            RS.Ticker.unregisterTickers(this);
            super.dispose();
        }

        protected updateText(): void
        {
            const resolvedText = Localisation.resolveLocalisableString(this.locale, this._localisableText);
            if (this._text.text !== resolvedText)
            {
                if (this._scrollTween)
                {
                    this._scrollTween.finish();
                    this._scrollTween = null;
                }
                this._scrolling = false;
                this._scrollPosition = 0;
                this._text.alpha = 1.0;
                this._text.text = resolvedText;
                this.invalidateLayout();
            }
        }

        /** Called when this element has been layouted. */
        protected onLayout()
        {
            super.onLayout();
            
            if (this._text == null) { return; }
            
            // Reset text transform
            this._text.setTransform(0, 0, 1, 1, 0, 0, 0, 0, 0);
            this._text.wrapWidth = null;

            // Make sure text is not bigger than our layout height (but width is ok as we're scrolling to deal with this)
            const localBounds = this._text.getLocalBounds();
            this._text.pivot.set(localBounds.x, localBounds.y);
            let sf = 1.0;
            // if (localBounds.width > this._layoutSize.w) { sf = Math.min(sf, this._layoutSize.w / localBounds.width); }
            if (localBounds.h > this._layoutSize.h) { sf = Math.min(sf, this._layoutSize.h / localBounds.h); }
            this._text.scaleX = this._text.scaleY = sf;

            // Determine how much we need to scroll
            this._scrollWidth = Math.max(0, localBounds.w - this._layoutSize.w) | 0;
            this._scrollPosition = Math.clamp(this._scrollPosition, 0, this._scrollWidth);

            // Center text within the layout area
            this._text.x = -(this._scrollPosition | 0);
            this._text.y = this._layoutSize.h * 0.5 - localBounds.h * 0.5 * this._text.scaleY;

            // If scroll width is 0, reset any pending tweens
            if (this._scrollWidth === 0)
            {
                if (this._scrollTween)
                {
                    this._scrollTween.finish();
                    this._scrollTween = null;
                }
                this._text.alpha = 1.0;
                this._scrolling = false;
            }
            else
            {
                if (!this._scrollTween && !this._scrolling)
                {
                    this.startScroll();
                }
            }

            RS.Ticker.registerTickers(this);
        }

        @Tick({ })
        protected tick(ev: RS.Ticker.Event): void
        {
            if (!this._scrolling) { return; }

            if (this._text.alpha >= 1.0)
            {
                this._scrollPosition += (ev.delta / 1000) * this.settings.scrollSpeed;
            }

            if (this._scrollPosition >= this._scrollWidth)
            {
                this._scrollPosition = this._scrollWidth;
                if (this._scrollTween)
                {
                    this._scrollTween.finish();
                    this._scrollTween = null;
                }
                this._scrollTween = RS.Tween.get(this._text)
                    .wait(this.settings.waitAtEndTime)
                    .to({ alpha: 0.0 }, this.settings.fadeTime / 2, this.settings.fadeEase)
                    .call((t) =>
                    {
                        this._scrollPosition = 0;
                        this.invalidateLayout();
                    })
                    .call((t) => this.startScroll());
                this._scrolling = false;
            }

            this.onLayout();            
        }

        protected startScroll(): void
        {
            if (this._scrollTween)
            {
                this._scrollTween.finish();
                this._scrollTween = null;
            }
            this._scrollPosition = 0;

            if (this._scrollWidth === 0) { return; }

            this._text.alpha = 0.0;
            this._scrollTween = RS.Tween.get(this._text)
                .to({alpha: 1.0}, this.settings.fadeTime)
                .wait(this.settings.waitAtStartTime)
                .call((t) => this._scrolling = true);
        }

        /** Called when the parent of this element has changed. */
        protected onFlowParentChanged()
        {
            super.onFlowParentChanged();
            this.updateText();
        }
    }

    export namespace ScrollingLabel
    {
        export interface Settings extends Label.Settings
        {
            /** How fast to scroll the text in pixels per second. */
            scrollSpeed: number;

            /** Ease of fade when resetting the scroll. */
            fadeEase: EaseFunction;

            /** Duration of fade when resetting the scroll (ms). */
            fadeTime: number;

            /** Time to wait before scrolling the text. */
            waitAtStartTime: number;

            /** Time to wait before resetting the text after having scrolled it. */
            waitAtEndTime: number;
        }

        /** Default settings for the label element. */
        export let defaultSettings: Settings =
        {
            ...Label.defaultSettings,
            maskToLayoutArea: true,

            scrollSpeed: 100,
            fadeEase: Ease.linear,
            fadeTime: 250,
            waitAtStartTime: 500,
            waitAtEndTime: 500
        };
    }

    /** A text label. */
    export const scrollingLabel = declareElement(ScrollingLabel, ScrollingLabel.defaultSettings);
}
