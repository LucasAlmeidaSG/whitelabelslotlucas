/// <reference path="Label.ts" />
/// <reference path="Background.ts" />

namespace RS.Flow
{
    /** A clickable button. */
    export class Button extends BaseElement<Button.Settings> implements SGI.Selenium.ISeleniumComponent, SGI.Selenium.ISeleniumDescriber
    {
        /** Published on mouse or touch down. */
        public readonly onPressed = createSimpleEvent();

        /** Published on mouse or touch up. */
        public readonly onReleased = createSimpleEvent();

        //protected get _bg() { return this._currentBackground; }
        protected set _currentBackground(bg: Background | Image)
        {
            const currentBg = this._currentBackground;
            this.suppressLayouting();
            if (currentBg) { currentBg.visible = false; }
            bg.visible = true;
            this.restoreLayouting();
        }
        protected get _currentBackground()
        {
            if (this._upBackground.visible) { return this._upBackground; }
            if (this._hoverBackground && this._hoverBackground.visible) { return this._hoverBackground; }
            if (this._pressBackground && this._pressBackground.visible) { return this._pressBackground; }
            if (this._disabledBackground && this._disabledBackground.visible) { return this._disabledBackground; }
            // Should never get here.
            return null;
        }

        protected set _currentLabel(label: Label)
        {
            const currentLabel = this._currentLabel;
            this.suppressLayouting();
            if (currentLabel) { currentLabel.visible = false; }
            label.visible = true;
            this.restoreLayouting();
        }
        protected get _currentLabel()
        {
            if (this._label && this._label.visible) { return this._label; }
            if (this._hoverLabel && this._hoverLabel.visible) { return this._hoverLabel; }
            if (this._pressLabel && this._pressLabel.visible) { return this._pressLabel; }
            if (this._disabledLabel && this._disabledLabel.visible) { return this._disabledLabel; }
            return null;
        }

        protected _animating: boolean;

        @AutoDisposeOnSet protected _upBackground: Background | Image;
        @AutoDisposeOnSet protected _hoverBackground: Background | Image | void;
        @AutoDisposeOnSet protected _pressBackground: Background | Image | void;
        @AutoDisposeOnSet protected _disabledBackground: Background | Image | void;

        protected readonly _idleAnimation: Button.IAnimator;
        protected readonly _pressAnimation: Button.IAnimator | void;
        protected readonly _releaseAnimation: Button.IAnimator | void;
        protected readonly _hoverAnimation: Button.IAnimator | void;
        protected readonly _outAnimation: Button.IAnimator | void;

        // | null to prevent errors for people that assume it is not null
        @AutoDisposeOnSet protected readonly _label: Label | null = null;
        @AutoDisposeOnSet protected readonly _hoverLabel: Label | null = null;
        @AutoDisposeOnSet protected readonly _pressLabel: Label | null = null;
        @AutoDisposeOnSet protected readonly _disabledLabel: Label | null = null;

        protected _enabled: boolean = false;
        protected _hovered: boolean = false;
        protected _pressed: boolean = false;
        protected _idleTime: number;

        /** Gets the backing label of this button. */
        @CheckDisposed
        public get label() { return this._label; }

        /** Gets the backing label of this button. */
        @CheckDisposed
        public get hoverLabel() { return this._hoverLabel; }

        /** Gets the backing label of this button. */
        @CheckDisposed
        public get pressLabel() { return this._pressLabel; }

        /** Gets the backing disabled label of this button. */
        @CheckDisposed
        public get disabledLabel() { return this._disabledLabel; }

        /** Gets or sets if this button should be enabled or not. */
        @CheckDisposed
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            if (this._enabled === value) { return; }
            this._enabled = value;
            this.interactive = value;
            this.cursor = value ? RS.Rendering.Cursor.Pointer : RS.Rendering.Cursor.Default;
            if (!value)
            {
                this._hovered = false;
                this._pressed = false;
                if (this._idleAnimation)
                {
                    RS.Ticker.unregisterTickers(this);
                }
            }
            else
            {
                if (this._idleAnimation)
                {
                    this._idleTime = 0;
                    RS.Ticker.registerTickers(this);
                }
            }
            this.updateBackground();
        }

        /** Gets the CSS ID for the selenium div element for this button. */
        public get seleniumId() { return this.settings.seleniumId || null; }

        public constructor(settings: Button.Settings)
        {
            super(settings, undefined);

            this.suppressLayouting();
            // Backgrounds
            this.createUpButton(settings);
            this.createPressButton(settings);
            this.createHoverButton(settings);
            this.createDisabledButton(settings);

            // Texts

            if (settings.textLabel)
            {
                this._label = this.createLabel(settings.textLabel);
            }
            if (settings.pressTextLabel)
            {
                this._pressLabel = this.createLabel(settings.pressTextLabel);
            }
            if (settings.hoverTextLabel)
            {
                this._hoverLabel = this.createLabel(settings.hoverTextLabel);
            }
            if (settings.disabledTextLabel)
            {
                this._disabledLabel = this.createLabel(settings.disabledTextLabel);
            }
            this.restoreLayouting();

            // Animations

            this._idleAnimation = settings.idleAnimation;
            this._pressAnimation = settings.pressAnimation;
            this._releaseAnimation = settings.releaseAnimation;
            this._hoverAnimation = settings.hoverAnimation;
            this._outAnimation = settings.outAnimation;

            // Listeners

            this.onOver(this.handleOver);
            this.onOut(this.handleOut);
            this.onDown(this.handlePressed);
            this.onUp(this.handleReleased);
            this.onClickCancelled(this.handleClickCancelled);
            // this.onUpOutside(this.handleReleased);

            // if (Device.isTouchDevice)
            // {
            //     this.addListener("tap", this.handleClicked);
            //     this.addListener("touchstart", this.handlePressed);
            //     this.addListener("touchend", this.handleReleased);
            //     this.addListener("touchendoutside", this.handleReleased);
            // }

            this.enabled = true;

            if (settings.hitArea)
            {
                this.hitArea = settings.hitArea;
            }

            if (settings.hitAreaPadding && !Spacing.equals(settings.hitAreaPadding, Spacing.none))
            {
                if (settings.hitArea)
                {
                    // There's extra work to support padding ellipses, polygons etc.
                    // It's not really worth it when you're already manually specifying the whole hit area
                    // So just pad it there
                    Log.warn("hitAreaPadding is not yet supported for hit area");
                }
                else
                {
                    this.onLayouted(this.updateHitAreaOnLayout);
                }
            }
        }

        public apply(settings: Partial<Button.Settings>)
        {
            if (settings.background || settings.image)
            {
                if (this._upBackground instanceof Background && settings.background)
                {
                    this._upBackground.apply(settings.background);
                }
                else if (this._upBackground instanceof Image && settings.image)
                {
                    this._upBackground.apply(settings.image);
                }
                else
                {
                    this.createUpButton(settings);
                }
            }
            if (settings.hoverbackground || settings.hoverImage)
            {
                if (this._hoverBackground instanceof Background && settings.hoverbackground)
                {
                    this._hoverBackground.apply(settings.hoverbackground);
                }
                else if (this._hoverBackground instanceof Image && settings.hoverImage)
                {
                    this._hoverBackground.apply(settings.hoverImage);
                }
                else
                {
                    this.createHoverButton(settings);
                }
            }
            if (settings.pressbackground || settings.pressImage)
            {
                if (this._pressBackground instanceof Background && settings.pressbackground)
                {
                    this._pressBackground.apply(settings.pressbackground);
                }
                else if (this._pressBackground instanceof Image && settings.pressImage)
                {
                    this._pressBackground.apply(settings.pressImage);
                }
                else
                {                    
                    this.createPressButton(settings);
                }
            }
            if (settings.disabledImage || settings.disabledbackground)
            {
                if (this._disabledBackground instanceof Background && settings.disabledbackground)
                {
                    this._disabledBackground.apply(settings.disabledbackground);
                }
                else if (this._disabledBackground instanceof Image && settings.disabledImage)
                {
                    this._disabledBackground.apply(settings.disabledImage);
                }
                else
                {
                    this.createDisabledButton(settings);    
                }
            }
            
            if (this._label && settings.textLabel) { this._label.apply(settings.textLabel); }
            if (this._disabledLabel && settings.disabledTextLabel) { this._disabledLabel.apply(settings.disabledTextLabel); }
            if (this.hitArea && settings.hitArea) { this.hitArea = settings.hitArea; }
            super.apply(settings);
        }

        /**
         * Gets specific attributes for the selenium component for this button.
         */
        public seleniumDescribe(): [string, string | number][]
        {
            const attrs: [string, string | number][] = [];
            const label = this._currentLabel;
            if (this._currentLabel)
            {
                attrs.push([ "text", label.innerText.text ]);
            }
            attrs.push([ "state", this._enabled
                ? this._pressed
                    ? "pressed"
                    : this._hovered
                        ? "hovered"
                        : "enabled"
                : "disabled" ]);
            return attrs;
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            if (this._idleAnimation)
            {
                RS.Ticker.unregisterTickers(this);
            }
            super.dispose();
        }

        //Up Button creation
        protected createUpButton(settings: Button.Settings)
        {
            this._upBackground = settings.image ?
                settings.image && this.createImage(settings.image) :
                settings.background && this.createBackground(settings.background);
        }

        //Hover Button creation
        protected createHoverButton(settings: Button.Settings)
        {
            this._hoverBackground = settings.hoverImage ?
                settings.hoverImage && this.createImage(settings.hoverImage) :
                settings.hoverbackground && this.createBackground(settings.hoverbackground);
        }
        
        //Press Button creation
        protected createPressButton(settings: Button.Settings)
        {
            this._pressBackground = settings.pressImage ?
                settings.pressImage && this.createImage(settings.pressImage) :
                settings.pressbackground && this.createBackground(settings.pressbackground);
        }

        //Disabled Button creation
        protected createDisabledButton(settings: Button.Settings)
        {
            this._disabledBackground = settings.disabledImage ?
                settings.disabledImage && this.createImage(settings.disabledImage) :
                settings.disabledbackground && this.createBackground(settings.disabledbackground);
        }

        @Tick({ kind: Ticker.Kind.Default })
        protected onTick(ev: Ticker.Event): void
        {
            this._idleTime += ev.delta / 1000;
            if (this._idleTime > this._idleAnimation.delay)
            {
                this._idleTime = 0;
                this._idleAnimation.animate(this);
            }
        }

        @Callback
        protected handleOver(): void
        {
            if (this._hovered) { return; }
            this._hovered = true;

            this.updateBackground();
            if (this._hoverAnimation)
            {
                this._hoverAnimation.animate(this);
            }
            if (this._hoverBackground instanceof Image && this.settings.hoverImage.kind === Image.Kind.Sprite)
            {
                this._hoverBackground.gotoAndPlay(this.settings.hoverImage.animationName, this.settings.hoverImage.playSettings);
            }

            if (this.settings.hoverSound)
            {
                if (Is.array(this.settings.hoverSound))
                {
                    if (this.settings.hoverSound.length > 0)
                    {
                        const randomIndex = Math.generateRandomInt(0, this.settings.hoverSound.length);
                        const sound = this.settings.hoverSound[randomIndex];
                        Audio.play(sound, this.settings.hoverSoundSettings);
                    }
                }
                else
                {
                    Audio.play(this.settings.hoverSound, this.settings.hoverSoundSettings);
                }
            }
        }

        @Callback
        protected async handleOut()
        {
            if (!this._hovered) { return; }
            this._hovered = false;

            if (this._outAnimation)
            {
                this._outAnimation.animate(this);
            }
            if (this._hoverBackground instanceof Image && this.settings.hoverImage.kind === Image.Kind.Sprite && this.settings.reverseHoverOutAnimation)
            {
                await this._hoverBackground.gotoAndPlay(this.settings.hoverImage.animationName, { ...this.settings.hoverImage.playSettings, reverse: true });
            }

            this.updateBackground();
        }

        @Callback
        protected handlePressed(): void
        {
            if (this._pressed) { return; }
            const inputBlock = this.stage.inputBlocked.declare(true);
            this._pressed = true;

            this.updateBackground();
            if (this._pressAnimation)
            {
                this._pressAnimation.animate(this);
            }

            if (this._pressBackground instanceof Image && this.settings.pressImage.kind === Image.Kind.Sprite)
            {
                this._pressBackground.gotoAndPlay(this.settings.pressImage.animationName, this.settings.pressImage.playSettings).then(() =>
                {
                    this._animating = false
                    this.updateBackground();
                });
                if (this.settings.awaitPressAnimation)
                {
                    this._animating = true;
                }
            }

            // Force a render before publishing the event.
            this.stage.render();

            this.onPressed.publish();
            inputBlock.dispose();
        }

        @Callback
        protected handleReleased(): void
        {
            if (!this._pressed) { return; }
            this._pressed = false;

            this.updateBackground();
            if (this._releaseAnimation)
            {
                this._releaseAnimation.animate(this);
            }
            if (this._idleAnimation)
            {
                this._idleTime = 0;
            }
            if (this._pressBackground instanceof Image && this.settings.pressImage.kind === Image.Kind.Sprite)
            {
                const sprite = this._pressBackground.innerSprite as Rendering.IAnimatedSprite;
                if (this._animating)
                {
                    sprite.onAnimationEnded.once(() => sprite.stop());
                }
            }

            if (this.settings.clickSound)
            {
                if (Is.array(this.settings.clickSound))
                {
                    if (this.settings.clickSound.length > 0)
                    {
                        const randomIndex = Math.generateRandomInt(0, this.settings.clickSound.length);
                        const sound = this.settings.clickSound[randomIndex];
                        Audio.play(sound, this.settings.clickSoundSettings);
                    }
                }
                else
                {
                    Audio.play(this.settings.clickSound, this.settings.clickSoundSettings);
                }
            }

            // Force a render before publishing the event.
            this.onReleased.publish();
        }

        @Callback
        protected handleClickCancelled(): void
        {
            if (!this._pressed) { return; }
            this._pressed = false;
            this.updateBackground();
            if (this._idleAnimation)
            {
                this._idleTime = 0;
            }
        }

        protected updateBackground(): void
        {
            const bg =
                (this._enabled ? (
                    (this._pressed && this._pressBackground) ||
                    (this._hovered && this._hoverBackground)
                ) : (
                    this._disabledBackground
                )) ||
                this._upBackground;

            const label =
                (this._enabled ? (
                    (this._pressed && this._pressLabel) ||
                    (this._hovered && this._hoverLabel)
                ) : (
                    this._disabledLabel
                )) ||
                this._label;

            if (label) { this._currentLabel = label; }

            if (!this._animating)
            {
                this._currentBackground = bg;
            }
        }

        /** Calculates the minimum size we should be to fit the contents. */
        protected calculateContentsSize(): RS.Dimensions
        {
            const bg = this._currentBackground;
            return {
                w: Math.max(bg && bg.desiredSize.w || 0, this._label && this._label.desiredSize.w || 0) + this._spacing.left + this._spacing.right,
                h: Math.max(bg && bg.desiredSize.h || 0, this._label && this._label.desiredSize.h || 0) + this._spacing.top + this._spacing.bottom
            };
        }

        /** Creates a background in its initial state. */
        protected createBackground(settings: Background.Settings): Background
        {
            const bg = background.create(settings, this);
            bg.visible = false;
            bg.interactive = false;
            return bg;
        }

        /** Creates a label in its initial state. */
        protected createLabel(settings: Label.Settings): Label
        {
            const text = label.create(settings, this);
            text.visible = false;
            text.interactive = false;
            return text;
        }

        @Callback
        protected updateHitAreaOnLayout()
        {
            const padding = this.settings.hitAreaPadding;
            this.hitArea = new RS.Rendering.Rectangle(
                -padding.left,
                -padding.top,
                this.layoutSize.w + Spacing.widthOf(padding),
                this.layoutSize.h + Spacing.heightOf(padding));
        }

        /** Creates an image in its initial state. */
        private createImage(settings: Image.Settings): Image
        {
            const img = image.create(settings, this);
            img.visible = false;
            img.interactive = false;
            return img;
        }
    }

    export namespace Button
    {
        /**
         * Responsible for animating the idle of a button.
         */
        export interface IAnimator
        {
            delay: number; // delay before playing the idle animation (in seconds)
            animate(button: Flow.Button): PromiseLike<void>;
        }

        /** Settings for the button element. */
        export interface Settings extends Partial<ElementProperties>
        {
            textLabel?: Label.Settings;
            hoverTextLabel?: Label.Settings;
            pressTextLabel?: Label.Settings;
            disabledTextLabel?: Label.Settings;
            background?: Background.Settings;
            hoverbackground?: Background.Settings;
            pressbackground?: Background.Settings;
            disabledbackground?: Background.Settings;
            clickSound?: OneOrMany<Asset.SoundReference>;
            clickSoundSettings?: Partial<Audio.PlaySettings>;
            hoverSound?: OneOrMany<Asset.SoundReference>;
            hoverSoundSettings?: Partial<Audio.PlaySettings>;
            pressAnimation?: IAnimator;
            releaseAnimation?: IAnimator;
            hoverAnimation?: IAnimator;
            outAnimation?: IAnimator;
            idleAnimation?: IAnimator;
            hitArea?: Rendering.ReadonlyShape | null;
            /** Padding for the hit area. NOTE: currently ignored if hitArea set, but this may change in the future. */
            hitAreaPadding?: Flow.Spacing;
            seleniumId?: string;

            image?: Image.Settings;
            hoverImage?: Image.Settings;
            pressImage?: Image.Settings;
            disabledImage?: Image.Settings;
            /* Set to true to wait for press animation to finish before going back to idle. Does nothing if pressimage isn't set */
            awaitPressAnimation?: boolean;

            /** Set to true to play a hover animation in reverse when moving mouse off the button. (Must be Sprite) */
            reverseHoverOutAnimation?: boolean;
        }

        /** Default settings for the button element. */
        export let defaultSettings: Settings =
        {
            sizeToContents: true,
            contentAlignment: { x: 0.5, y: 0.5 },
            textLabel: { ...Label.defaultSettings, text: "BUTTON", dock: Dock.Fill, ignoreParentSpacing: false },
            background: { dock: Dock.Fill, ignoreParentSpacing: true, kind: Background.Kind.SolidColor, color: Util.Colors.gray, borderSize: 0 },
            hoverbackground: { dock: Dock.Fill, ignoreParentSpacing: true, kind: Background.Kind.SolidColor, color: Util.Colors.lightgrey, borderSize: 0 },
            pressbackground: { dock: Dock.Fill, ignoreParentSpacing: true, kind: Background.Kind.SolidColor, color: Util.Colors.darkgray, borderSize: 0 },
            disabledbackground: { dock: Dock.Fill, ignoreParentSpacing: true, kind: Background.Kind.SolidColor, color: Util.Colors.gray.darker(0.5), borderSize: 0 },
            spacing: Spacing.all(10)
        };
    }

    /** A clickable button. */
    export const button = declareElement(Button, Button.defaultSettings);
}
