namespace RS.Flow
{
    const cornersTmp: Background.Corners<number> = { topLeft: 0, topRight: 0, bottomLeft: 0, bottomRight: 0 }

    /** A generic background element. */
    export class Background extends BaseElement<Background.Settings>
    {
        protected _sprite: Rendering.IBitmap;
        protected _graphics: Rendering.IGraphics;
        protected _transformTarget: Rendering.IContainer | Rendering.IBitmap;
        protected _scaleMode: Math.ScaleMode;

        protected _ninePatch: Background.PatchInfo[];

        protected _frameData: Rendering.Frame;

        protected _rotation: number | null;
        protected get _handlesRotation(): boolean { return Background.isImageLike(this.settings); }

        public get rotation() { return this._rotation || 0; }
        public set rotation(value) { this._rotation = value; this.invalidateLayout({ reason: "rotation" }); }

        /** Gets or sets the scaling behaviour of this image. */
        @CheckDisposed
        public get scaleMode() { return this._scaleMode; }
        public set scaleMode(value)
        {
            this._scaleMode = value;
            this.invalidateLayout({ reason: "scaleMode" });
        }

        public constructor(settings: Background.Settings)
        {
            super(settings, undefined);

            switch (settings.kind)
            {
                case Background.Kind.SolidColor:
                {
                    this.addChild(this._graphics = new Rendering.Graphics());
                    if (settings.blendMode != null) { this._graphics.blendMode = settings.blendMode; }
                    break;
                }
                case Background.Kind.Image:
                {
                    this._sprite = Factory.bitmap(settings.asset);
                    this._sprite.anchorX = this._sprite.anchorY = 0.5;

                    const transformTarget = new Rendering.Container();
                    transformTarget.addChild(this._sprite);
                    this._transformTarget = transformTarget;

                    this.addChild(this._transformTarget);
                    if (settings.tint) { this._sprite.tint = settings.tint; }
                    if (settings.blendMode != null) { this._sprite.blendMode = settings.blendMode; }
                    if (settings.alpha != null) { this._sprite.alpha = settings.alpha; }
                    this._sprite.shader = settings.shader;
                    break;
                }
                case Background.Kind.ImageFrame:
                {
                    let frameIndex: number;
                    const asset = Asset.Store.getAsset(settings.asset);
                    // const sheet = asset.as(Rendering.SpriteSheet);
                    const sheet = asset.asset as Rendering.ISpriteSheet;
                    if (Is.number(settings.frame))
                    {
                        frameIndex = settings.frame;
                    }
                    else
                    {
                        frameIndex = sheet.getAnimation(settings.frame).frames[0];
                    }
                    this._frameData = sheet.getFrame(frameIndex);
                    this._sprite = new Rendering.Bitmap(this._frameData);

                    const transformTarget = new Rendering.Container();
                    transformTarget.addChild(this._sprite);
                    this._transformTarget = transformTarget;
                    this._sprite.rotation = this.rotation;

                    this.addChild(this._transformTarget);
                    if (settings.tint) { this._sprite.tint = settings.tint; }
                    if (settings.blendMode != null) { this._sprite.blendMode = settings.blendMode; }
                    if (settings.alpha != null) { this._sprite.alpha = settings.alpha; }
                    this._sprite.shader = settings.shader;
                    break;
                }
                case Background.Kind.Gradient:
                {
                    if (settings.gradient.type === Rendering.Gradient.Type.Vertical)
                    {
                        this._sprite = new Rendering.Bitmap(RS.Rendering.Gradient.createSimpleTexture(settings.gradient.stops, true));
                    }
                    else if (settings.gradient.type === Rendering.Gradient.Type.Horizontal)
                    {
                        this._sprite = new Rendering.Bitmap(RS.Rendering.Gradient.createSimpleTexture(settings.gradient.stops, false));
                    }
                    else
                    {
                        this._sprite = new Rendering.Bitmap(RS.Rendering.Gradient.createTexture(settings.textureSize.w, settings.textureSize.h, settings.gradient));
                    }
                    this._sprite.anchorX = this._sprite.anchorY = 0.5;
                    this._transformTarget = this._sprite;
                    this.addChild(this._transformTarget);
                    if (settings.blendMode != null) { this._sprite.blendMode = settings.blendMode; }
                    if (settings.alpha != null) { this._sprite.alpha = settings.alpha; }
                    if (settings.border)
                    {
                        this.addChild(this._graphics = new Rendering.Graphics());
                        if (settings.blendMode != null) { this._graphics.blendMode = settings.blendMode; }
                    }
                    break;
                }
                case Background.Kind.NinePatch:
                {
                    this._ninePatch = [];
                    for (let patchX = 0; patchX < 3; ++patchX)
                    {
                        for (let patchY = 0; patchY < 3; ++patchY)
                        {
                            const patch = this.initPatch(settings, patchX, patchY);
                            if (patch)
                            {
                                this._ninePatch.push(patch);
                                if (settings.blendMode != null) { patch.sprite.blendMode = settings.blendMode; }
                                if (settings.alpha != null) { patch.sprite.alpha = settings.alpha; }
                                patch.sprite.shader = settings.shader;
                            }
                        }
                    }
                    break;
                }
            }
        }

        protected static isImageLike(props: Background.Settings): props is Background.ImageSettings | Background.ImageFrameSettings | Background.GradientSettings
        {
            return props.kind === Background.Kind.Image
                || props.kind === Background.Kind.ImageFrame
                || props.kind === Background.Kind.Gradient;
        }

        private static cornerTo(g: Rendering.IGraphics, x1: number, y1: number, x2: number, y2: number, r: number): void
        {
            if (r === 0)
            {
                g.lineTo(x1, y1);
                g.lineTo(x2, y2);
            }
            else
            {
                g.arcTo(x1, y1, x2, y2, r);
            }
        }

        @CheckDisposed
        public apply(props: Partial<Background.ImageLikeSettings>): void
        {
            if (Background.isImageLike(this.settings))
            {
                props = props as Background.ImageLikeSettings;
                if (props.scaleMode != null) { this._scaleMode = props.scaleMode; }
            }
            super.apply(props);
        }

        protected initPatch(settings: Background.NinePatchSettings, patchX: number, patchY: number): Background.PatchInfo | null
        {
            const asset = Asset.Store.getAsset(settings.asset);
            if (asset == null) { return null; }
            const rawAsset = asset.asset;
            let texture: Rendering.ISubTexture;
            const rectPix = new Rendering.Rectangle(), rectNorm = new Rendering.Rectangle();
            const border: Flow.Spacing = { ...Flow.Spacing.none };
            if (rawAsset instanceof Rendering.Texture)
            {
                switch (patchX)
                {
                    case 0:
                        rectPix.x = 0;
                        rectPix.w = settings.borderSize.left;
                        break;
                    case 1:
                        rectPix.x = settings.borderSize.left;
                        rectPix.w = rawAsset.width - (settings.borderSize.left + settings.borderSize.right);
                        break;
                    case 2:
                        rectPix.x = rawAsset.width - settings.borderSize.right;
                        rectPix.w = settings.borderSize.right;
                        break;
                }
                switch (patchY)
                {
                    case 0:
                        rectPix.y = 0;
                        rectPix.h = settings.borderSize.top;
                        break;
                    case 1:
                        rectPix.y = settings.borderSize.top;
                        rectPix.h = rawAsset.height - (settings.borderSize.top + settings.borderSize.bottom);
                        break;
                    case 2:
                        rectPix.y = rawAsset.height - settings.borderSize.bottom;
                        rectPix.h = settings.borderSize.bottom;
                        break;
                }
                const area = rectPix.w * rectPix.h;
                if (area <= 0) { return null; }
                rectNorm.x = rectPix.x / rawAsset.width;
                rectNorm.y = rectPix.y / rawAsset.height;
                rectNorm.w = rectPix.w / rawAsset.width;
                rectNorm.h = rectPix.h / rawAsset.height;
                texture = new Rendering.SubTexture(rawAsset, rectPix);
            }
            else if (rawAsset instanceof Rendering.SpriteSheet || rawAsset instanceof Rendering.RSSpriteSheet)
            {
                if (settings.frame == null) { throw new Error("Must specify frame if using spritesheet for nine patch"); }
                const frame = Is.string(settings.frame) ? rawAsset.getFrame(settings.frame, 0) : rawAsset.getFrame(settings.frame);
                const trimLeft = frame.trimRect.x, trimRight = frame.untrimmedSize.w - (frame.trimRect.x + frame.trimRect.w);
                const trimTop = frame.trimRect.y, trimBottom = frame.untrimmedSize.h - (frame.trimRect.y + frame.trimRect.h);
                border.left = trimLeft;
                border.right = trimRight;
                border.top = trimTop;
                border.bottom = trimBottom;
                switch (patchX)
                {
                    case 0:
                        rectPix.x = frame.imageRect.x;
                        rectPix.w = settings.borderSize.left - trimLeft;
                        break;
                    case 1:
                        rectPix.x = frame.imageRect.x + settings.borderSize.left - trimLeft;
                        rectPix.w = frame.imageRect.w - (settings.borderSize.left - trimLeft + settings.borderSize.right - trimRight);
                        break;
                    case 2:
                        rectPix.x = frame.imageRect.x + frame.imageRect.w - (settings.borderSize.right - trimRight);
                        rectPix.w = settings.borderSize.right - trimRight;
                        break;
                }
                switch (patchY)
                {
                    case 0:
                        rectPix.y = frame.imageRect.y;
                        rectPix.h = settings.borderSize.top - trimTop;
                        break;
                    case 1:
                        rectPix.y = frame.imageRect.y + settings.borderSize.top - trimTop;
                        rectPix.h = frame.imageRect.h - (settings.borderSize.top - trimTop + settings.borderSize.bottom - trimBottom);
                        break;
                    case 2:
                        rectPix.y = frame.imageRect.y + frame.imageRect.h - (settings.borderSize.bottom - trimBottom);
                        rectPix.h = settings.borderSize.bottom - trimBottom;
                        break;
                }
                rectNorm.x = rectPix.x / frame.imageData.texture.width;
                rectNorm.y = rectPix.y / frame.imageData.texture.height;
                rectNorm.w = rectPix.w / frame.imageData.texture.width;
                rectNorm.h = rectPix.h / frame.imageData.texture.height;
                const area = rectPix.w * rectPix.h;
                if (area <= 0) { return null; }
                texture = new Rendering.SubTexture(frame.imageData.texture, rectPix);
            }
            else
            {
                throw new Error("Asset was of unexpected type");
            }
            const sprite = new Rendering.Bitmap(texture);
            this.addChild(sprite);
            return {
                texture: texture,
                border,
                rectPix,
                rectNorm,
                patchX, patchY,
                sprite
            };
        }

        /** Calculates the minimum size we should be to fit the contents. */
        protected calculateContentsSize(): Math.Size2D
        {
            const baseSize = super.calculateContentsSize();
            switch (this.settings.kind)
            {
                case Background.Kind.Image:
                {
                    const border = this.settings.border || Spacing.none;
                    baseSize.w = Math.max(baseSize.w, this._sprite.texture.width - (border.left + border.right));
                    baseSize.h = Math.max(baseSize.h, this._sprite.texture.height - (border.top + border.bottom));
                    break;
                }
                case Background.Kind.ImageFrame:
                {
                    const border = this.settings.border || Spacing.none;
                    const imageSize =
                    {
                        w: this._frameData.untrimmedSize.w - (border.left + border.right),
                        h: this._frameData.untrimmedSize.h - (border.top + border.bottom)
                    };

                    if (this.rotation)
                    {
                        Math.Size2D.rotate(imageSize, this.rotation, imageSize);
                    }

                    baseSize.w = Math.max(baseSize.w, imageSize.w);
                    baseSize.h = Math.max(baseSize.h, imageSize.h);

                    break;
                }
                case Background.Kind.NinePatch:
                {
                    const asset = Asset.Store.getAsset(this.settings.asset);
                    if (asset != null)
                    {
                        const rawAsset = asset.asset;
                        if (rawAsset instanceof Rendering.Texture)
                        {
                            baseSize.w = Math.max(baseSize.w, rawAsset.width);
                            baseSize.h = Math.max(baseSize.h, rawAsset.height);
                        }
                        else if (rawAsset instanceof Rendering.SpriteSheet || rawAsset instanceof Rendering.RSSpriteSheet)
                        {
                            if (this.settings.frame != null)
                            {
                                const frame = Is.string(this.settings.frame) ? rawAsset.getFrame(this.settings.frame, 0) : rawAsset.getFrame(this.settings.frame);
                                baseSize.w = Math.max(baseSize.w, frame.untrimmedSize.w);
                                baseSize.h = Math.max(baseSize.h, frame.untrimmedSize.h);
                            }
                        }
                    }
                    break;
                }
            }
            return baseSize;
        }

        protected renderBorder(size: Flow.Spacing, color: RS.Util.Color, corners?: Background.Corners<number>): void
        {
            if (corners)
            {
                this.renderRoundedRect(this._graphics, corners, size, color);
            }
            else
            {
                this._graphics

                    .lineStyle(size.top, color)
                    .moveTo(0, 0)
                    .lineTo(this._layoutSize.w + size.right * 0.5, 0)

                    .lineStyle(size.right, color)
                    .moveTo(this._layoutSize.w, 0)
                    .lineTo(this._layoutSize.w, this._layoutSize.h + size.bottom * 0.5)

                    .lineStyle(size.bottom, color)
                    .moveTo(this._layoutSize.w, this._layoutSize.h)
                    .lineTo(-size.left * 0.5, this._layoutSize.h)

                    .lineStyle(size.left, color)
                    .moveTo(0, this._layoutSize.h)
                    .lineTo(0, -size.top * 0.5);
            }
        }

        protected renderSolidColor(settings: Background.SolidColorSettings): void
        {
            const r = Math.min(settings.cornerRadius || 0, this._layoutSize.w, this._layoutSize.h);
            this._graphics.clear();

            // BorderSize & corners to be replaced with single Spacing.
            const enabledCorners = settings.corners || Background.Corners.all;
            cornersTmp.topLeft = enabledCorners.topLeft ? r : 0;
            cornersTmp.topRight = enabledCorners.topRight ? r : 0;
            cornersTmp.bottomLeft = enabledCorners.bottomLeft ? r : 0;
            cornersTmp.bottomRight = enabledCorners.bottomRight ? r : 0;

            // Fill
            if (r > 0)
            {
                this._graphics.beginFill(settings.color, 1.0);
                this.renderRoundedRect(this._graphics, cornersTmp);
                this._graphics.endFill();
            }
            else
            {
                this._graphics
                    .beginFill(settings.color, 1.0)
                    .drawRect(0, 0, this._layoutSize.w, this._layoutSize.h)
                    .endFill();
            }

            if (!Is.number(settings.borderSize) || settings.borderSize > 0)
            {
                // Border
                this.renderBorder(Is.number(settings.borderSize) ? Spacing.all(settings.borderSize) : settings.borderSize, settings.borderColor, r > 0 ? cornersTmp : null);
            }

            this._graphics.alpha = settings.alpha != null ? settings.alpha : 1.0;
        }

        protected renderRoundedRect(graphics: RS.Rendering.IGraphics, corners: Background.Corners<number>, borders: Spacing, borderColor: Util.Color): void;
        protected renderRoundedRect(graphics: RS.Rendering.IGraphics, corners: Background.Corners<number>): void;
        protected renderRoundedRect(graphics: RS.Rendering.IGraphics, corners: Background.Corners<number>, borders?: Spacing, borderColor?: Util.Color): void
        {
            graphics.moveTo(corners.topLeft, 0);

            // top line
            if (borders) { graphics.lineStyle(borders.top, borderColor); }
            graphics.lineTo(this._layoutSize.w - corners.topRight, 0);

            // top right corner
            if (corners.topRight > 0) { Background.cornerTo(graphics, this._layoutSize.w, 0, this._layoutSize.w, corners.topRight, corners.topRight); }

            // right line
            if (borders) { graphics.lineStyle(borders.right, borderColor); }
            graphics.lineTo(this._layoutSize.w, this._layoutSize.h - corners.bottomRight);

            // bottom right corner
            if (corners.bottomRight > 0) { Background.cornerTo(graphics, this._layoutSize.w, this._layoutSize.h, this._layoutSize.w - corners.bottomRight, this._layoutSize.h, corners.bottomRight); }

            // bottom line
            if (borders) { graphics.lineStyle(borders.bottom, borderColor); }
            graphics.lineTo(corners.bottomLeft, this._layoutSize.h);

            // bottom left corner
            if (corners.bottomLeft > 0) { Background.cornerTo(graphics, 0, this._layoutSize.h, 0, this._layoutSize.h - corners.bottomLeft, corners.bottomLeft); }

            // left line
            if (borders) { graphics.lineStyle(borders.left, borderColor); }
            graphics.lineTo(0, corners.topLeft);

            // top left corner
            if (corners.topLeft > 0) { Background.cornerTo(graphics, 0, 0, corners.topLeft, 0, corners.topLeft); }
        }

        /** Called when this element has been layouted. */
        protected onLayout()
        {
            switch (this.settings.kind)
            {
                case Background.Kind.SolidColor:
                {
                    if (this._graphics == null) { return; }

                    this.renderSolidColor(this.settings);

                    break;
                }
                case Background.Kind.Image:
                {
                    if (this._sprite == null) { return; }

                    // Reset sprite transform
                    this._transformTarget.setTransform(this._layoutSize.w * 0.5, this._layoutSize.h * 0.5, 1.0, 1.0);
                    this._sprite.rotation = this.rotation;

                    const localBounds = this._transformTarget.localBounds;
                    if (this.settings.border)
                    {
                        const border = this.settings.border;
                        const borderSize = { w: border.left + border.right, h: border.top + border.bottom };
                        Math.Size2D.subtract(localBounds, borderSize, localBounds);

                        this._transformTarget.x -= (border.left - border.right) * 0.5;
                        this._transformTarget.y -= (border.top - border.bottom) * 0.5;
                    }

                    // Size our sprite to fit the layout size
                    const scale = Math.scale(localBounds, this._layoutSize, this._scaleMode);
                    this._transformTarget.scaleX = scale.x;
                    this._transformTarget.scaleY = scale.y;

                    if (this.settings.mirrorX)
                    {
                        this._transformTarget.scaleX *= -1;
                    }
                    if (this.settings.mirrorY)
                    {
                        this._transformTarget.scaleY *= -1;
                    }

                    break;
                }
                case Background.Kind.Gradient:
                {
                    if (this._sprite == null) { return; }

                    if (this.settings.border)
                    {
                        this._graphics.clear();
                        this.renderBorder(this.settings.borderSize, this.settings.borderColor);
                    }

                    // Reset sprite transform
                    this._transformTarget.setTransform(this._layoutSize.w * 0.5, this._layoutSize.h * 0.5, 1.0, 1.0);
                    this._sprite.rotation = this.rotation;

                    // Size our sprite to fit the layout size
                    const localBounds = this._transformTarget.localBounds;
                    const scale = Math.scale(localBounds, this._layoutSize, this._scaleMode);
                    this._transformTarget.scaleX = scale.x;
                    this._transformTarget.scaleY = scale.y;

                    break;
                }
                case Background.Kind.ImageFrame:
                {
                    if (this._sprite == null) { return; }

                    const frameData = this._frameData;

                    const border = this.settings.border || Spacing.none;

                    const effectiveSize: Math.Size2D =
                    {
                        w: frameData.untrimmedSize.w - (border.left + border.right),
                        h: frameData.untrimmedSize.h - (border.top + border.bottom)
                    };

                    if (this.rotation)
                    {
                        Math.Size2D.rotate(effectiveSize, this.rotation, effectiveSize);
                    }

                    const scale = Math.scale(effectiveSize, this._layoutSize, this._scaleMode);

                    this._sprite.anchorX = frameData.reg.x / frameData.imageRect.w;
                    this._sprite.anchorY = frameData.reg.y / frameData.imageRect.h;
                    this._sprite.rotation = this.rotation;

                    const pivotX = (frameData.reg.x + frameData.trimRect.x) / frameData.untrimmedSize.w;
                    const pivotY = (frameData.reg.y + frameData.trimRect.y) / frameData.untrimmedSize.h;

                    const bL = Math.clamp((1.0 - pivotX), 0.0, 1.0);
                    const bR = Math.clamp(pivotX, 0.0, 1.0);

                    const bT = Math.clamp((1.0 - pivotY), 0.0, 1.0);
                    const bB = Math.clamp(pivotY, 0.0, 1.0);

                    const xShift = border.right * bR - border.left * bL;
                    const yShift = border.bottom * bB - border.top * bT;

                    // Size our sprite to fit the layout size
                    this._transformTarget.x = this._layoutSize.w * pivotX + xShift * scale.x;
                    this._transformTarget.y = this._layoutSize.h * pivotY + yShift * scale.y;
                    this._transformTarget.scaleX = scale.x;
                    this._transformTarget.scaleY = scale.y;

                    if (this.settings.mirrorX)
                    {
                        this._transformTarget.scaleX *= -1;
                    }
                    if (this.settings.mirrorY)
                    {
                        this._transformTarget.scaleY *= -1;
                    }

                    break;
                }
                case Background.Kind.NinePatch:
                {
                    if (this._ninePatch == null) { return; }

                    for (const patchInfo of this._ninePatch)
                    {
                        const spr = patchInfo.sprite;

                        // Reset sprite transform
                        spr.setTransform(0, 0, 1.0, 1.0);

                        // Size our sprite to fit the layout size
                        switch (patchInfo.patchX)
                        {
                            case 0:
                                spr.x = patchInfo.border.left;
                                spr.width = this.settings.borderSize.left - patchInfo.border.left;
                                break;
                            case 1:
                                spr.x = this.settings.borderSize.left;
                                spr.width = this._layoutSize.w - (this.settings.borderSize.left + this.settings.borderSize.right);
                                break;
                            case 2:
                                spr.x = this._layoutSize.w - this.settings.borderSize.right;
                                spr.width = this.settings.borderSize.right - patchInfo.border.right;
                                break;
                        }
                        switch (patchInfo.patchY)
                        {
                            case 0:
                                spr.y = patchInfo.border.top;
                                spr.height = this.settings.borderSize.top - patchInfo.border.top;
                                break;
                            case 1:
                                spr.y = this.settings.borderSize.top;
                                spr.height = this._layoutSize.h - (this.settings.borderSize.top + this.settings.borderSize.bottom);
                                break;
                            case 2:
                                spr.y = this._layoutSize.h - this.settings.borderSize.bottom;
                                spr.height = this.settings.borderSize.bottom - patchInfo.border.bottom;
                                break;
                        }
                    }
                    break;
                }
            }
        }
    }

    export namespace Background
    {
        export enum Kind { Image, ImageFrame, SolidColor, Gradient, NinePatch }

        export interface Corners<T = boolean>
        {
            topLeft: T;
            topRight: T;
            bottomLeft: T;
            bottomRight: T;
        }

        export namespace Corners
        {
            export const all: Corners = { topLeft: true, topRight: true, bottomLeft: true, bottomRight: true };
            export const none: Corners = { topLeft: false, topRight: false, bottomLeft: false, bottomRight: false };
            export const topOnly: Corners = { topLeft: true, topRight: true, bottomLeft: false, bottomRight: false };
            export const bottomOnly: Corners = { topLeft: false, topRight: false, bottomLeft: true, bottomRight: true };
            export const leftOnly: Corners = { topLeft: true, topRight: false, bottomLeft: true, bottomRight: false };
            export const rightOnly: Corners = { topLeft: false, topRight: true, bottomLeft: false, bottomRight: true };
        }

        /** Settings common to image-like backgrounds. */
        export interface ImageLikeSettings extends Partial<ElementProperties>
        {
            /** How the background should scale to fit its layout size. Defaults to Stretch, but Contain and Cover maintain aspect ratio. */
            scaleMode?: Math.ScaleMode;
        }

        export interface PatchInfo
        {
            texture: RS.Rendering.ISubTexture;
            rectPix: RS.Rendering.Rectangle;
            rectNorm: RS.Rendering.Rectangle;
            patchX: number;
            patchY: number;
            sprite: RS.Rendering.IBitmap;
            border: Spacing;
        }

        /** Settings for the background element. */
        export interface BaseSettings<TKind extends Kind> extends Partial<ElementProperties>
        {
            kind: TKind;
            blendMode?: RS.Rendering.BlendMode;
            alpha?: number;
        }

        export interface ImageSettings extends BaseSettings<Kind.Image>, ImageLikeSettings
        {
            asset: Asset.ImageReference;
            tint?: Util.Color;
            shader?: Rendering.IShader;
            border?: Readonly<Spacing>;
            mirrorX?: boolean;
            mirrorY?: boolean;
        }

        export interface ImageFrameSettings extends BaseSettings<Kind.ImageFrame>, ImageLikeSettings
        {
            asset: Asset.SpriteSheetReference | Asset.RSSpriteSheetReference;
            frame: number | string;
            tint?: Util.Color;
            shader?: Rendering.IShader;
            border?: Readonly<Spacing>;
            mirrorX?: boolean;
            mirrorY?: boolean;
        }

        export interface SolidColorSettings extends BaseSettings<Kind.SolidColor>
        {
            color: Util.Color;
            borderSize: number | Spacing;
            borderColor?: Util.Color;
            cornerRadius?: number;
            corners?: Corners;
        }

        export interface GradientSettings extends BaseSettings<Kind.Gradient>, ImageLikeSettings
        {
            gradient: RS.Rendering.Gradient;
            textureSize: Dimensions;

            border?: boolean;
            borderSize?: Spacing;
            borderColor?: Util.Color;
        }

        export interface NinePatchSettings extends BaseSettings<Kind.NinePatch>
        {
            asset: Asset.ImageReference | Asset.SpriteSheetReference | Asset.RSSpriteSheetReference;
            frame?: number | string;
            borderSize: Spacing;
            shader?: Rendering.IShader;
        }

        export type Settings = ImageSettings | ImageFrameSettings | SolidColorSettings | GradientSettings | NinePatchSettings;

        /** Default settings for the background element. */
        export let defaultSettings: Settings =
        {
            kind: Kind.SolidColor,
            color: Util.Colors.black,
            alpha: 1,
            borderSize: 1,
            cornerRadius: 0,
            borderColor: Util.Colors.white,
            dock: Dock.Fill,
            ignoreParentSpacing: true,
            sizeToContents: true,
            spacing: Spacing.none,
            expand: Expand.Allowed
        };
    }

    /** A generic background element. */
    export const background = declareElement(Background, Background.defaultSettings);
}
