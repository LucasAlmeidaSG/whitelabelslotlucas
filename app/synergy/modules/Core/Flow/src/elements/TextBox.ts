/// <reference path="InputLabel.ts"/>
namespace RS.Flow
{
    /** A box which can be typed into. Supports labelling and backgrounds. */
    export class TextBox extends BaseElement<TextBox.Settings>
    {
        @RS.AutoDispose
        private readonly _background: Background;
        @RS.AutoDispose
        private readonly _label: Label;
        @RS.AutoDispose
        private readonly _input: InputLabel;

        /** Whether or not the user is currently typing into this text box. */
        public get focused() { return this._input.focused; }
        public set focused(v) { this._input.focused = v; }

        /** Whether or not the user may type into this text box. */
        public get enabled() { return this.interactive; }
        public set enabled(enabled)
        {
            this.interactive = enabled;
            this._input.enabled = enabled;
        }

        /** The optional label. */
        public get label() { return this._label; }

        /** Published when the user presses Enter. */
        public get onSubmitted() { return this._input.onSubmitted; }
        /** Published when the user enters a new character or removes a previous one. */
        public get onChanged() { return this._input.onChanged; }

        /** The text that has been entered into this text box. */
        public get text() { return this._input.text; }
        public set text(text) { this._input.text = text; }

        constructor(public readonly settings: TextBox.Settings)
        {
            super(settings, null);

            if (settings.background) { this._background = background.create(settings.background, this); }
            if (settings.label) { this._label = label.create(settings.label, this); }
            this._input = inputLabel.create(settings.input, this);

            this.onClicked(() => this._input.focused = true);

            // Initial state: unfocused, enabled
            this.interactive = true;
        }
    }

    export namespace TextBox
    {
        export interface Settings extends Partial<ElementProperties>
        {
            /** Settings for the input label. */
            input: InputLabel.Settings;
            /** Optional settings for the textbox background. If null, the box will have no background. */
            background?: Background.Settings;
            /** Optional settings for the title label. If null, the box will have no label. */
            label?: Label.Settings;
        }

        export const defaultSettings: Settings =
        {
            dock: Dock.Fill,
            expand: Expand.Disallowed,
            sizeToContents: true,
            legacy: false,

            input: InputLabel.defaultSettings,
            background: Background.defaultSettings
        };
    }

    export const textBox = declareElement(TextBox, TextBox.defaultSettings);
}