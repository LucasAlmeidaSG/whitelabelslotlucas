namespace RS.Is
{
    export function spacing(value: any): value is Flow.Spacing
    {
        return Is.object(value) && Is.number(value.left) && Is.number(value.top) && Is.number(value.right) && Is.number(value.bottom);
    }
}

namespace RS.Flow
{
    /** Describes how to space out child elements. */
    export interface Spacing
    {
        left: number;
        top: number;
        right: number;
        bottom: number;
    }
    
    /** Creates a new Spacing with all values 0. */
    export function Spacing(): Spacing;

    /** Creates a new Spacing with the given values. */
    export function Spacing(left: number, top: number, right: number, bottom: number): Spacing;

    /** Creates a new Spacing from the given partial Spacing. */
    export function Spacing(spacing: Partial<Spacing>): Spacing;

    export function Spacing(p1: number | Partial<Spacing> = 0, top: number = 0, right: number = 0, bottom: number = 0): Spacing
    {
        let left: number;
        if (Is.object(p1))
        {
            left = p1.left || 0;
            top = p1.top || 0;
            right = p1.right || 0;
            bottom = p1.bottom || 0;
        }
        else
        {
            left = p1;
        }
        return { left, top, right, bottom };
    }

    export namespace Spacing
    {
        /** Describes no spacing on all sides. */
        export const none: Readonly<Spacing> = { left: 0, top: 0, right: 0, bottom: 0 };

        /** Describes an equal spacing on all sides. */
        export function all(value: number): Spacing { return { left: value, right: value, top: value, bottom: value }; }

        /** Describes an equal spacing to the left and right and no spacing to the top and bottom. */
        export function horizontal(value: number): Spacing { return { left: value, right: value, top: 0, bottom: 0 }; }

        /** Describes an equal spacing to the top and bottom and no spacing to the left and right. */
        export function vertical(value: number): Spacing { return { left: 0, right: 0, top: value, bottom: value }; }

        /** Describes an equal spacing to the top and bottom and a different equal spacing to the left and right. */
        export function axis(x: number, y: number): Spacing { return { left: x, right: x, top: y, bottom: y }; }

        /** Describes half the amount of spacing as that provided. */
        export function half(other: Spacing): Spacing { return { left: other.left / 2, right: other.right / 2, top: other.top / 2, bottom: other.bottom / 2 }; }

        /** Copies one spacing into another, optionally applying a multiplier. */
        export function copy(from: Spacing, to: Spacing, multiplier: number = 1.0): Spacing
        {
            to.left = from.left * multiplier;
            to.right = from.right * multiplier;
            to.top = from.top * multiplier;
            to.bottom = from.bottom * multiplier;
            return to;
        }

        /** Returns the width of the given spacing. */
        export function widthOf(spacing: Spacing): number
        {
            return spacing.left + spacing.right;
        }

        /** Returns the height of the given spacing. */
        export function heightOf(spacing: Spacing): number
        {
            return spacing.top + spacing.bottom;
        }

        /** Gets if two Spacing object are equal, optionally within a threshold. */
        export function equals(a: Spacing, b: Spacing, threshold: number = 0.0): boolean
        {
            return Math.abs(b.left - a.left) <= threshold && Math.abs(b.top - a.top) <= threshold
                && Math.abs(b.right - a.right) <= threshold && Math.abs(b.bottom - a.bottom) <= threshold;
        }
    }
}