namespace RS.Flow
{
    /**
     * Encapsulates an interface to drag behaviour for an object.
     */
    export interface IDragHandler extends IDisposable
    {
        /** Gets the object to which this handler belongs. */
        readonly owner: Rendering.IDisplayObject;

        /** Gets if dragging is in progress on the object. */
        readonly dragging: boolean;

        /** Gets the drag behaviour. */
        readonly behaviour: IDragBehaviour;

        /** Published when dragging has started. */
        readonly onDragStarted: IEvent;

        /** Published when dragging has ended. */
        readonly onDragEnded: IEvent;

        /** Terminates any current drag state. */
        cancelDrag(): void;
    }

    @HasCallbacks
    class DragHandler implements IDragHandler
    {
        /** Published when dragging has started. */
        public readonly onDragStarted = createSimpleEvent();//this._onDragStarted.public;

        /** Published when dragging has ended. */
        public readonly onDragEnded = createSimpleEvent();//this._onDragEnded.public;

        private _state = DragHandler.State.Idle;
        private _isDisposed: boolean = false;
        private _dragStartPt: Math.Vector2D = { x: 0, y: 0 };
        private _dragEndPt: Math.Vector2D = { x: 0, y: 0 };
        private _dragTimer = new Timer();
        private _interactionID: number;

        /** Gets if dragging is in progress on the object. */
        public get dragging() { return this._state === DragHandler.State.Dragging; }

        /** Gets if this handler is disposed. */
        public get isDisposed() { return this._isDisposed; }

        //private readonly _onDragStarted = createSimpleEvent();
        //private readonly _onDragEnded = createSimpleEvent();

        public constructor(public readonly owner: Rendering.IDisplayObject, public readonly behaviour: IDragBehaviour)
        {
            owner.onDown(this.handleTouchStart);
            owner.onMoved(this.handleTouchMove);
            owner.onUp(this.handleTouchEnd);
            owner.onUpOutside(this.handleTouchEnd);
        }        

        /** Terminates any current drag state. */
        @CheckDisposed
        public cancelDrag(): void
        {
            if (this._state === DragHandler.State.Idle) { return; }

            const oldState = this._state;
            this._state = DragHandler.State.Idle;

            if (oldState === DragHandler.State.Dragging)
            {
                this.behaviour.handleDragEnd(this._dragEndPt);
                this.onDragEnded.publish();
            }
        }

        /** Disposes this drag handler. */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this.cancelDrag();
            this.owner.onDown.off(this.handleTouchStart);
            this.owner.onMoved.off(this.handleTouchMove);
            this.owner.onUp.off(this.handleTouchEnd);
            this.owner.onUpOutside.off(this.handleTouchEnd);
            this._isDisposed = true;
        }

        @Callback
        private handleTouchStart(ev: Rendering.InteractionEventData)
        {
            if (this._state !== DragHandler.State.Idle) { return; }
            this._interactionID = ev.pointerID;
            this._state = DragHandler.State.Held;
            const localPos = ev.target.toLocal(ev.localPosition, this.owner);
            this._dragStartPt.x = localPos.x;
            this._dragStartPt.y = localPos.y;
            this._dragTimer.reset();
        }

        @Callback
        private handleTouchMove(ev: Rendering.InteractionEventData)
        {
            if (this._state === DragHandler.State.Idle) { return; }
            if (ev.pointerID !== this._interactionID) { return; }
            const localPos = ev.target.toLocal(ev.localPosition, this.owner);
            this._dragEndPt.x = localPos.x;
            this._dragEndPt.y = localPos.y;
            if (this._state === DragHandler.State.Held)
            {
                if (this.behaviour.shouldAcceptDrag(this._dragStartPt, this._dragEndPt, this._dragTimer.elapsed))
                {
                    this._state = DragHandler.State.Dragging;
                    this.behaviour.handleDragStart(this._dragStartPt);
                    this.behaviour.handleDragUpdate(this._dragEndPt);
                    this.onDragStarted.publish();
                }
            }
            else
            {
                this.behaviour.handleDragUpdate(this._dragEndPt);
            }
        }

        @Callback
        private handleTouchEnd(ev: Rendering.InteractionEventData)
        {
            if (this._state === DragHandler.State.Idle) { return; }
            if (ev.pointerID !== this._interactionID) { return; }
            const localPos = ev.target.toLocal(ev.localPosition, this.owner);
            this._dragEndPt.x = localPos.x;
            this._dragEndPt.y = localPos.y;
            this.cancelDrag();
        }
    }

    namespace DragHandler
    {
        export enum State
        {
            Idle,
            Held,
            Dragging
        }
    }

    export namespace IDragHandler
    {
        /**
         * Creates a new drag handler for the specified object.
         * @param target 
         * @param behaviour 
         */
        export function get(target: Rendering.IDisplayObject, behaviour: IDragBehaviour): IDragHandler
        {
            return new DragHandler(target, behaviour);
        }
    }
}