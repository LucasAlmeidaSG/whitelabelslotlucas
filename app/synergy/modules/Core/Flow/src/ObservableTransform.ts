namespace RS.Flow
{
    /**
     * Encapsulates a transformation of position and scale. Notifies of changes.
     */
    export class ObservableTransform implements Transform, IDisposable
    {
        public readonly onChanged: IEvent<void>;

        public set x(value) { this._x = value; this._onChanged.publish(); }
        public get x() { return this._x; }

        public set y(value) { this._y = value; this._onChanged.publish(); }
        public get y() { return this._y; }

        public set scaleX(value) { this._scaleX = value; this._onChanged.publish(); }
        public get scaleX() { return this._scaleX; }

        public set scaleY(value) { this._scaleY = value; this._onChanged.publish(); }
        public get scaleY() { return this._scaleY; }

        public get isDisposed() { return this._isDisposed; }

        @RS.AutoDisposeOnSet protected readonly _onChanged = createSimpleEvent();

        protected _isDisposed = false;

        protected _x: number = 0;
        protected _y: number = 0;
        protected _scaleX: number = 1;
        protected _scaleY: number = 1;

        constructor(transform?: Transform)
        {
            this.onChanged = this._onChanged.public;
            if (transform) { this.mirror(transform); }
        }

        /** Copies the target transform's state into this transform. */
        public copy(target: Transform): void
        {
            this._x = target.x;
            this._y = target.y;
            this._scaleX = target.scaleX;
            this._scaleY = target.scaleY;
            this._onChanged.publish();
        }

        public mirror(target: Transform): void
        {
            this.copy(target);
            Util.mirror(target, this, { x: true, y: true, scaleX: true, scaleY: true });
        }

        /** Gets this transform's current state as a normal Transform object. */
        public toPlainObject(): Transform
        {
            return { x: this._x, y: this._y, scaleX: this._scaleX, scaleY: this._scaleY };
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }
    }

    @RS.TweenTypeHandler
    export class TransformTweenHandler implements ITweenTypeHandler<ObservableTransform>
    {
        public canHandle(obj: any): obj is ObservableTransform
        {
            return obj instanceof ObservableTransform;
        }

        public clone(target: ObservableTransform): ObservableTransform
        {
            const clone = new ObservableTransform();
            clone.copy(target);

            return clone;
        }

        public interpolate(from: ObservableTransform, to: ObservableTransform, mu: number): ObservableTransform
        {
            const plainFrom = from.toPlainObject();
            const plainTo = to.toPlainObject();
            const result = TweenUtils.interpolate(plainFrom, plainTo, mu);

            const observable = new ObservableTransform();
            observable.copy(result);

            return observable;
        }

        public apply(source: ObservableTransform, dest: object, key: string): void
        {
            dest[key] = source;
        }
    }

    /**
     * Encapsulates a transformation of position and scale.
     */
    export interface Transform
    {
        x: number;
        y: number;
        scaleX: number;
        scaleY: number;
    }

    export namespace Transform
    {
        /** Returns whether or not the given Transform contains any changes. */
        export function hasChanges(transform: Transform): boolean
        {
            return transform.scaleX != 1 || transform.scaleY != 1 || transform.x != 0 || transform.y != 0;
        }
    }
}