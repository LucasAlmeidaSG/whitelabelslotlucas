namespace RS.Flow
{
    /** Describes how an element should be sized within a given Flow context. */
    export interface Size
    {
        w: number | Sizing.ISizeHandler;
        h: number | Sizing.ISizeHandler;
    }

    export namespace Sizing
    {
        export enum SizeType { Width, Height }
        export interface ISizeHandler
        {
            /** Returns the numeric desired size for the given context and type. */
            get: (context: GenericElement, type: SizeType) => number;
            /** 
             * Whether or not the resulting size depends on the parent. If true, the element
             * will be ignored when calculating its parent's contents size, in order to
             * prevent circular references.
             */
            relative: boolean;
        }

        /** Describes a size which is a fraction of the parent's. */
        export function Fraction(fraction: number): ISizeHandler
        {
            return {
                get: function (context, type)
                {
                    const parent = context.flowParent;
                    if (!parent) { return 1; }

                    const size = type == SizeType.Width ? parent.innerLayoutSize.w : parent.innerLayoutSize.h;
                    return size * fraction;
                },
                relative: true
            };
        }

        /** Transforms the result of the given size handler using a simple function. */
        export function Transformed(handler: ISizeHandler, transformer: (value: number) => number): ISizeHandler
        {
            return {
                get: function (context, type)
                {
                    return transformer(handler.get(context, type));
                },
                relative: handler.relative
            };
        }

        /** Restricts the size to a given minimum value. */
        export const Min = (handler: ISizeHandler, value: number) => Transformed(handler, (handlerValue) => Math.max(handlerValue, value));
        /** Restricts the size to a given maximum value. */
        export const Max = (handler: ISizeHandler, value: number) => Transformed(handler, (handlerValue) => Math.min(handlerValue, value));

        export function resolve(size: Size, context: GenericElement, out?: Math.Size2D): Math.Size2D
        {
            out = out || Math.Size2D();
            out.w = RS.Is.number(size.w) ? size.w : size.w.get(context, SizeType.Width);
            out.h = RS.Is.number(size.h) ? size.h : size.h.get(context, SizeType.Height);
            return out;
        }

        function sizeDependsOnParent(child: GenericElement, width: boolean): boolean
        {
            const size = width ? child.size.w : child.size.h;
            return !child.sizeToContents && !RS.Is.number(size) && size.relative;
        }

        export function widthDependsOnParent(child: GenericElement): boolean
        {
            return sizeDependsOnParent(child, true);
        }

        export function heightDependsOnParent(child: GenericElement): boolean
        {
            return sizeDependsOnParent(child, false);
        }
    }
}