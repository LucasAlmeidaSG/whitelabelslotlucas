namespace RS.Flow
{
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.BaseSettings
     */
    export import BaseSettings = Rendering.BaseSettings;

    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.BaseRuntimeData
     */
    export import BaseRuntimeData = Rendering.BaseRuntimeData;
    /**
     * Allows creation of a particular element with predetermined settings.
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.IBoundElement
     */
    export import IBoundElement = Rendering.IBoundElement;

    /**
     * Allows creation of a particular element with predetermined settings.
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.IBoundElementNeedsRuntimeData
     */
    export import IBoundElementNeedsRuntimeData = Rendering.IBoundElementNeedsRuntimeData;

    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.ILooseBoundElement
     */
    export import ILooseBoundElement = Rendering.ILooseBoundElement;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.ILooseBoundElementNeedsRuntimeData
     */
    export import ILooseBoundElementNeedsRuntimeData = Rendering.ILooseBoundElementNeedsRuntimeData;

    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.IGenericBoundElement
     */
    export import IGenericBoundElement = Rendering.IGenericBoundElement;

    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.IGenericBoundElementNeedsRuntimeData
     */
    export import IGenericBoundElementNeedsRuntimeData = Rendering.IGenericBoundElementNeedsRuntimeData;

    /**
     * Allows creation of a particular element.
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.IElementFactory
     */
    export import IElementFactory = Rendering.IElementFactory;

    /**
     * Allows creation of a particular element.
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.IElementFactoryNeedsRuntimeData
     */
    export import IElementFactoryNeedsRuntimeData = Rendering.IElementFactoryNeedsRuntimeData;

    /**
     * Declares a type of element with no default settings and no runtime data.
     * @param ctor
     * TODO remove in synergy 1.2
     * @deprecated use RS.Rendering.declareElement
     */
    export import declareElement = Rendering.declareElement;
}