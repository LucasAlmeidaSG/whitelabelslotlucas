namespace RS.Flow
{
    /**
     * Encapsulates behaviour to exhibit when the user attempts to drag an object.
     */
    export interface IDragBehaviour
    {
        /** Called when this drag behaviour is installed on an object. */
        handleInstalled(object: RS.Rendering.IDisplayObject): void;

        /** Called to decide whether a gesture should initiate a drag or not. */
        shouldAcceptDrag(startPt: Math.Vector2D, endPt: Math.Vector2D, duration: number): boolean;

        /** Called when dragging has started. */
        handleDragStart(startPt: Math.Vector2D): void;

        /** Called when dragged has updated. */
        handleDragUpdate(dragPt: Math.Vector2D): void;

        /** Called when dragged has ended. */
        handleDragEnd(finalPt: Math.Vector2D): void;
    }
}