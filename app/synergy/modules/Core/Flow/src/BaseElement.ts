namespace RS.Flow
{
    const debugMode = URL.getParameterAsBool("flowdebug", false);

    /** Describes how an element should dock to it's parent. */
    export enum Dock { None, Left, Top, Right, Bottom, Fill, Float }

    /** Describes how an element should expand from it's desired size. */
    export enum Expand { Disallowed, HorizontalOnly, VerticalOnly, Allowed }

    export namespace Expand
    {
        export function canExpandX(expand: Expand)
        {
            return expand === Expand.HorizontalOnly || expand === Expand.Allowed;
        }

        export function canExpandY(expand: Expand)
        {
            return expand === Expand.VerticalOnly || expand === Expand.Allowed;
        }
    }

    /** Contains information about a particular layout invalidation call. */
    export interface LayoutInvalidation
    {
        /** The object whose layout was first invalidated. */
        source?: GenericElement;
        /** The reason for the invalidation, e.g. "dock". */
        reason: string;
    }

    const tmpSizeArr = [ Math.Size2D(), Math.Size2D(), Math.Size2D(), Math.Size2D(), Math.Size2D(), Math.Size2D() ];

    const logger = RS.Logging.getContext("Flow");

    /**
     * Encapsulates a set of properties for a generic flow element.
     */
    export interface ElementProperties
    {
        /** Whether or not to use legacy behaviour. Defaults to true but set to false for new elements. */
        legacy: boolean;
        /** How this element should be positioned within its parent. */
        dock: Dock;
        /**
         * Whether or not this element can expand when docked.
         * If disallowed, the element will be aligned within the space allocated to it by its parent according to their contentsAlignment.
         */
        expand: Expand;
        /** Gets or sets if this element should size itself to its contents. */
        sizeToContents: boolean;
        /** The size to use for this element if sizeToContents is false. */
        size: Readonly<Size>;
        /** Whether or not this element should maintain aspect ratio when being scaled down to fit. When being scaled to fit, its layout size will be kept the same. */
        enforceMinimumSize: boolean;
        /** The spacing in pixels between docked elements and their neighbours. */
        spacing: Readonly<Spacing>;
        /** The spacing in pixels between docked elements and the layout boundary. */
        padding: Readonly<Spacing>;
        /** The spacing in pixels between docked elements and the layout boundary. Does not affect the element's overall size. */
        innerPadding: Readonly<Spacing>;
        /** The alignment of docked flow children (if applicable), within the range 0-1. */
        contentAlignment: Readonly<Math.Vector2D>;
        /** The alignment relative to the float position, within the range 0-1. Does not affect non-floating elements. */
        dockAlignment: Readonly<Math.Vector2D>;
        /** The position of this element relative to its container, if dock is float. Within the range 0-1. */
        floatPosition: Readonly<Math.Vector2D>;
        /** This element's pixel offset from its floating position. */
        floatOffset: Readonly<Math.Vector2D>;
        /** How this element's children react to overflowing. */
        overflowMode: OverflowMode;
        /**
         * The scale factor of this element.
         * This will enlarge or shrink the element within it's container without affecting its layout size.
         */
        scaleFactor: number;
        rotation: number;
        /** Whether or not this element should be layouted within the parent with parent spacing ignored. */
        ignoreParentSpacing: boolean;
        /** Whether or not useful debug graphics should be drawn on this element. */
        debugMode: boolean;
        /** The name of this Flow object. Useful for debugging. */
        name: string;
        /**
         * The locale for this element.
         * If nothing is explicitly assigned, the first locale found from an ancestor will be used.
         */
        locale: Localisation.Locale;
        /**
         * The currency formatter for this element.
         * If nothing is explicitly assigned, the first formatter found from an ancestor will be used.
         */
        currencyFormatter: Localisation.CurrencyFormatter;
        /** Whether or not this element should be masked to its layout size. */
        maskToLayoutArea: boolean;
    }

    /** Describes how docked children react to their opposite boundary. */
    export enum OverflowMode
    {
        /** Allows docked children to overflow their opposite boundary. */
        Overflow,
        /** Prevents docked children from overflowing their opposite boundary. */
        Shrink
    }

    export type ObservableMapAndKey<T> = { map: ObservableMap<T>; key: string; };
    function isObservableMapAndKey(value: any): value is ObservableMapAndKey<any>
    {
        if (value == null || !Is.object(value)) { return false; }
        const valAs = value as ObservableMapAndKey<any>;
        return valAs.map instanceof ObservableMap && Is.string(valAs.key);
    }

    export type ObservableValue<T> = IReadonlyObservable<T> | { map: ObservableMap<T>; key: string; };

    export type ObservablePropertyMap<T> = { [K1 in ({ [K2 in keyof T]: T[K2] extends () => void ? never : K2 }[keyof T])]?: ObservableValue<T[K1]> };

    /**
     * A flow element that performs dynamic layouting.
     */
    @HasCallbacks
    export abstract class BaseElement<TSettings = void, TRuntimeData = void> extends Rendering.BindableDisplayObjectContainer<TSettings, TRuntimeData> implements ElementProperties, IDisposable
    {
        /** Published when layouting has finished for this element. */
        public readonly onLayouted = createSimpleEvent();//this._onLayouted.public;

        protected _parent: Rendering.IContainer;

        protected readonly _layoutSize: Math.Size2D = { w: 0, h: 0 };
        protected readonly _innerLayoutSize: Math.Size2D = { w: 0, h: 0 };
        protected readonly _outerLayoutSize: Nullable<Math.Size2D> = { w: null, h: null };
        protected _tmpRect: Rendering.Rectangle = new Rendering.Rectangle();
        protected _tmpSpacing: Spacing = { ...Spacing.none };
        protected _remainingSpace: Rendering.Rectangle = new Rendering.Rectangle();
        protected _layouting: boolean = false;
        protected _visible: boolean = true;
        @AutoDispose protected _pendingLayout: IDisposable | null = null;

        protected _locale: Localisation.Locale;
        protected _currencyFormatter: Localisation.CurrencyFormatter;

        protected _dock: Dock = Dock.None;
        protected _expand: Expand = Expand.Allowed;
        protected _sizeToContents: boolean = false;
        protected _size: Size = { w: 100, h: 100 };
        protected _enforceMinimumSize: boolean = false;
        protected _spacing: Spacing = Spacing.none;
        protected _padding: Spacing = Spacing.none;
        protected _innerPadding: Spacing = Spacing.none;
        protected _contentAlignment: Math.Vector2D = { x: 0.5, y: 0.5 };
        protected _dockAlignment: Math.Vector2D = { x: 0.5, y: 0.5 };
        protected _floatPos: Math.Vector2D = { x: 0.0, y: 0.0 };
        protected _floatOffset: Math.Vector2D = { x: 0.0, y: 0.0 };
        protected _scaleFactor: number = 1.0;
        protected _ignoreParentSpacing: boolean = false;
        protected _maskToLayoutArea: boolean = false;
        protected get _handlesRotation(): boolean { return false; }

        /** Whether or not our size/position is dirty and our parent therefore should be relayouted. */
        protected _outerDirty = true;
        /** Our desired size at the time of the last layout. */
        protected readonly _lastDesiredSize: Math.Size2D = { w: 0, h: 0 };

        protected _overflowMode: OverflowMode;

        protected _observableBindings: { [K in keyof this]?: IDisposable; } = {};

        protected readonly _flowTransform = { x: 0, y: 0, scaleX: 1, scaleY: 1 };
        @RS.AutoDisposeOnSet protected _postTransform = new ObservableTransform();

        protected _debugMode: boolean = debugMode;
        @RS.AutoDisposeOnSet protected _debugShape: Rendering.IGraphics|null = null;
        @RS.AutoDisposeOnSet protected _debugHitArea: Rendering.IGraphics|null = null;
        protected _debugHitAreaObserverSet = false;

        protected _thisLegacy = true;

        /** Gets or sets the transform to be applied to this element after Flow layouting. */
        @CheckDisposed
        public get postTransform(): Transform { return this._postTransform; }
        public set postTransform(value: Transform)
        {
            this._postTransform = new ObservableTransform(value);
            this._postTransform.onChanged(() => this.updateTransforms());
            this.updateTransforms();
        }

        @CheckDisposed
        public get overflowMode(): OverflowMode
        {
            if (this._overflowMode == null) { return this.legacy ? OverflowMode.Overflow : OverflowMode.Shrink; }
            return this._overflowMode;
        }
        public set overflowMode(mode)
        {
            if (mode === this._overflowMode) { return; }
            this._overflowMode = mode;
            this.invalidateLayout({ reason: "overflowMode" });
        }

        @CheckDisposed
        public get floatOffset(): Readonly<Math.Vector2D> { return this._floatOffset; }
        public set floatOffset(value)
        {
            if (Math.Vector2D.equals(value, this._floatOffset)) { return; }
            this._floatOffset = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "floatOffset" });
        }

        //protected readonly _onLayouted = createSimpleEvent();

        /** Gets or sets the parent of this element. */
        public get parent() { return this._parent; }
        public set parent(value)
        {
            if (value === this._parent) { return; }
            const oldParent = this.flowParent;

            // No ES5 transpilation for invoking super accessors
            // It's not pretty but we need to invoke this here
            const superDesc = Util.getPropertyDescriptor(Rendering.DisplayObjectContainer.prototype, { parent: true });
            Reflect.setValueUsingDescriptor(this, superDesc, value);

            if (this.isDisposed)
            {
                if (value == null)
                {
                    this._parent = null;
                }
                else
                {
                    throw new Error("Attempt to set parent on disposed element");
                }
                return;
            }
            this._parent = value;
            const newParent = this.flowParent;
            if (oldParent !== newParent)
            {
                this.onFlowParentChanged();
            }
        }

        @CheckDisposed
        public get dock() { return this._dock; }
        public set dock(value)
        {
            if (value === this._dock) { return; }
            this._dock = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "dock" });
        }

        @CheckDisposed
        public get legacy(): boolean { return this.flowParent && !this.flowParent.legacy ? false : this._thisLegacy; }
        public set legacy(value: boolean)
        {
            if (value === this._thisLegacy) { return; }
            this._thisLegacy = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "legacy" });
        }

        @CheckDisposed
        public get expand() { return this._expand; }
        public set expand(value)
        {
            if (value === this._expand) { return; }
            this._expand = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "expand" });
        }

        /** Gets all flow children. */
        @CheckDisposed
        public get flowChildren(): ReadonlyArray<GenericElement> { return this.children ? (this.children.filter((c) => Is.flowElement(c)) as GenericElement[]) : []; }

        /** Gets the flow parent. */
        @CheckDisposed
        public get flowParent(): GenericElement|null
        {
            const p = this._parent;
            return Is.flowElement(p) ? p : null;
        }

        @CheckDisposed
        public get sizeToContents() { return this._sizeToContents; }
        public set sizeToContents(value)
        {
            if (value === this._sizeToContents) { return; }
            this._sizeToContents = value;
            this.invalidateLayout({ reason: "sizeToContents" });
        }

        @CheckDisposed
        public get size(): Readonly<Size> { return this._size; }
        public set size(value)
        {
            if (this._size.w === value.w && this._size.h === value.h) { return; }
            this._size = value;
            this.invalidateLayout({ reason: "size" });
        }

        @CheckDisposed
        public get enforceMinimumSize() { return this._enforceMinimumSize; }
        public set enforceMinimumSize(value)
        {
            if (value === this._enforceMinimumSize) { return; }
            this._enforceMinimumSize = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "enforceMinimumSize" });
        }

        @CheckDisposed
        public get spacing(): Readonly<Spacing> { return this._spacing; }
        public set spacing(value)
        {
            if (Spacing.equals(value, this._spacing)) { return; }
            this._spacing = value;
            this.invalidateLayout({ reason: "spacing" });
        }

        @CheckDisposed
        public get padding(): Readonly<Spacing> { return this._padding; }
        public set padding(value)
        {
            if (Spacing.equals(value, this._padding)) { return; }
            this._padding = value;
            this.invalidateLayout({ reason: "padding" });
        }

        @CheckDisposed
        public get innerPadding(): Readonly<Spacing> { return this._innerPadding; }
        public set innerPadding(value)
        {
            if (Spacing.equals(value, this._innerPadding)) { return; }
            this._innerPadding = value;
            this.invalidateLayout({ reason: "innerPadding" });
        }

        @CheckDisposed
        public get debugMode() { return this._debugMode; }
        public set debugMode(value)
        {
            if (value === this._debugMode) { return; }
            this._debugMode = value;
            this.invalidateLayout({ reason: "debugMode" });
        }

        /** Gets the size that this element wants to be at. */
        @CheckDisposed
        public get desiredSize(): Readonly<Math.Size2D>
        {
            if (this._sizeToContents)
            {
                return this.calculateContentsSize();
            }
            else
            {
                return Sizing.resolve(this._size, this);
            }
        }

        @CheckDisposed
        public get contentAlignment(): Readonly<Math.Vector2D> { return this._contentAlignment; }
        public set contentAlignment(value)
        {
            if (Math.Vector2D.equals(value, this._contentAlignment)) { return; }
            this._contentAlignment = value;
            this.invalidateLayout({ reason: "contentAlignment" });
        }

        @CheckDisposed
        public get dockAlignment(): Readonly<Math.Vector2D> { return this._dockAlignment; }
        public set dockAlignment(value)
        {
            if (Math.Vector2D.equals(value, this._dockAlignment)) { return; }
            this._dockAlignment = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "dockAlignment" });
        }

        @CheckDisposed
        public get floatPosition(): Readonly<Math.Vector2D> { return this._floatPos; }
        public set floatPosition(value)
        {
            if (Math.Vector2D.equals(value, this._floatPos)) { return; }
            this._floatPos = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "floatPosition" });
        }

        @CheckDisposed
        public get scaleFactor() { return this._scaleFactor; }
        public set scaleFactor(value)
        {
            if (value === this._scaleFactor) { return; }
            this._scaleFactor = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "scaleFactor" }); }

        @CheckDisposed
        public get ignoreParentSpacing() { return this._ignoreParentSpacing; }
        public set ignoreParentSpacing(value)
        {
            if (value === this._ignoreParentSpacing) { return; }
            this._ignoreParentSpacing = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "ignoreParentSpacing" });
        }

        @CheckDisposed
        public get maskToLayoutArea() { return this._maskToLayoutArea; }
        public set maskToLayoutArea(value)
        {
            this._maskToLayoutArea = value;
            if (!value)
            {
                const oldMask = this.mask;
                if (oldMask instanceof RS.Rendering.DisplayObject)
                {
                    this.removeChild(oldMask);
                    this.mask = null;
                }
            }
            this.invalidateLayout({ reason: "maskToLayoutArea" });
        }

        /**
         * The maximum possible size that this element may be.
         * If there is no hard limit for a given dimension, the value will be null.
         *
         * This is used by elements whose width depends on their height and/or vice versa.
         */
        @CheckDisposed
        public get outerLayoutSize() { return this._outerLayoutSize; }

        /** Gets the current layout size of this element. */
        @CheckDisposed
        public get layoutSize() { return this._layoutSize; }

        /** Gets the maximum contents size of this element. */
        @CheckDisposed
        public get limitSize(): Readonly<Nullable<Math.Size2D>> { return this._sizeToContents ? this._outerLayoutSize : Sizing.resolve(this._size, this); }

        /** Gets the inner layout size of this element. */
        @CheckDisposed
        public get innerLayoutSize(): Readonly<Math.Size2D>
        {
            // TODO: change to an inner layout rect where x and y are the combined padding left + top
            const size = this._innerLayoutSize;
            Math.Size2D.copy(this._layoutSize, size);
            size.w -= Spacing.widthOf(this._innerPadding) + Spacing.widthOf(this._padding);
            size.h -= Spacing.heightOf(this._innerPadding) + Spacing.heightOf(this._padding);
            return size;
        }

        @CheckDisposed
        public set locale(value) { this._locale = value; }
        public get locale(): Localisation.Locale
        {
            if (this._locale) { return this._locale; }
            const p = this.flowParent;
            return p && p.locale || null;
        }

        @CheckDisposed
        public set currencyFormatter(value) { this._currencyFormatter = value; }
        public get currencyFormatter(): Localisation.CurrencyFormatter
        {
            if (this._currencyFormatter) { return this._currencyFormatter; }
            const p = this.flowParent;
            return p && p.currencyFormatter || null;
        }

        /** Gets the amount of space remaining after all docked children have been layouted. */
        @CheckDisposed
        public get remainingSpace(): Readonly<Rendering.Rectangle> { return this._remainingSpace; }

        /** Gets or sets the visibility of this element. */
        public get visible() { return this._visible; }
        public set visible(value)
        {
            if (value === this._visible) { return; }

            // It's not pretty but we need to invoke this here.
            const superDesc = Util.getPropertyDescriptor(Rendering.DisplayObjectContainer.prototype, { visible: true });
            Reflect.setValueUsingDescriptor(this, superDesc, value);

            this._visible = value;
            this._outerDirty = true;
            this.invalidateLayout({ reason: "visible" });
        }

        private get layoutSuppressed()
        {
            if (this._thisLayoutSuppressed) { return true; }
            const parent = this.flowParent;
            return parent ? parent.layoutSuppressed : false;
        }

        private get layoutAttempted() { return this._thisLayoutAttempted || this.flowChildren.some((child) => child.layoutAttempted); }
        private set layoutAttempted(value)
        {
            this._thisLayoutAttempted = value;
            if (!value) { this.flowChildren.forEach((child) => child.layoutAttempted = value); }
        }

        /** Whether or not layout has been suppressed directly or not. */
        private _thisLayoutSuppressed: boolean = false;
        private _thisLayoutAttempted: boolean = false;

        public constructor(public readonly settings: TSettings, public readonly runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);

            if (settings != null)
            {
                this.suppressLayouting();
                this.apply(settings);
                this.restoreLayouting(true);
            }

            this._postTransform.onChanged(() => this.updateTransforms());
        }

        /**
         * Given a layout size, rotates it and finds a new layout size that encompasses it.
         * @param dims
         * @param rotation radians
         */
        public static rotateLayoutArea(size: Math.Size2D, rotation: number): void
        {
            const c = Math.cos(-rotation);
            const s = Math.sin(-rotation);

            const newW = Math.abs(c * size.w) + Math.abs(s * size.h);
            const newH = Math.abs(s * size.w) + Math.abs(c * size.h);

            size.w = newW;
            size.h = newH;
        }

        /**
         * Given a previously rotated layout size, rotates it back and finds the original layout size.
         * @param dims
         * @param rotation radians
         */
        public static rotateLayoutAreaInv(size: Math.Size2D, rotation: number): void
        {
            const c = Math.cos(-rotation);
            const s = Math.sin(-rotation);

            const denom = c * c + s * s;

            const newW = (size.w * c - size.h * s) / denom;
            const newH = (size.h * c - size.w * s) / denom;

            size.w = Math.abs(newW);
            size.h = Math.abs(newH);
        }

        /**
         * Applies the specified set of properties to this element.
         */
        @CheckDisposed
        public apply(props: Partial<ElementProperties>): void
        {
            if (props.legacy != null) { this._thisLegacy = props.legacy; }
            if (props.dock != null) { this._dock = props.dock; }
            if (props.expand != null) { this._expand = props.expand; }
            if (props.sizeToContents != null) { this._sizeToContents = props.sizeToContents; }
            if (props.size != null) { this._size = props.size; }
            if (props.enforceMinimumSize != null) { this._enforceMinimumSize = props.enforceMinimumSize; }
            if (props.spacing != null) { this._spacing = props.spacing; }
            if (props.padding != null) { this._padding = props.padding; }
            if (props.innerPadding != null) { this._innerPadding = props.innerPadding; }
            if (props.contentAlignment != null) { this._contentAlignment = props.contentAlignment; }
            if (props.dockAlignment != null) { this._dockAlignment = props.dockAlignment; }
            if (props.floatPosition != null) { this._floatPos = props.floatPosition; }
            if (props.floatOffset != null) { this._floatOffset = props.floatOffset; }
            if (props.scaleFactor != null) { this._scaleFactor = props.scaleFactor; }
            if (props.rotation != null) { this.rotation = props.rotation; }
            if (props.ignoreParentSpacing != null) { this._ignoreParentSpacing = props.ignoreParentSpacing; }
            if (props.debugMode != null) { this._debugMode = props.debugMode; }
            if (props.name != null) { this.name = props.name; }
            if (props.locale != null) { this._locale = props.locale; }
            if (props.currencyFormatter != null) { this._currencyFormatter = props.currencyFormatter; }
            if (props.maskToLayoutArea != null) { this._maskToLayoutArea = props.maskToLayoutArea; }
            if (props.overflowMode != null) { this._overflowMode = props.overflowMode; }
            this.invalidateLayout({ reason: "apply" });
        }

        /**
         * Binds the specified properties of this element to the specified observables.
         * Bindings will override any existing ones.
         * Passing null as the observable will unbind that property.
         * @param propertyMap
         */
        @CheckDisposed
        public bindToObservables(propertyMap: ObservablePropertyMap<this>): void
        {
            for (const key in propertyMap)
            {
                if (this._observableBindings[key] != null)
                {
                    this._observableBindings[key].dispose();
                    delete this._observableBindings[key];
                }
                const oVal = propertyMap[key];
                if (isObservableMapAndKey(oVal))
                {
                    this._observableBindings[key] = oVal.map.onChanged((ev) =>
                    {
                        if (ev.key === oVal.key)
                        {
                            this[key] = ev.value;
                        }

                    });
                    this[key] = oVal.map.get(oVal.key);
                }
                else if (oVal != null)
                {
                    this._observableBindings[key] = oVal.onChanged((val) =>
                    {
                        this[key] = val;
                    });
                    this[key] = oVal.value;
                }
            }
        }

        /**
         * Unbinds ALL properties of this element from previously bound observables.
         * Use bindToObservables with null values to only unbind certain properties.
         */
        public unbindFromObservables(): void
        {
            for (const key in this._observableBindings)
            {
                this._observableBindings[key].dispose();
            }
            this._observableBindings = {};
        }

        /**
         * Invalidates the layout of this element.
         * @param invalidationData debug information about the invalidation.
         */
        @CheckDisposed
        public invalidateLayout(invalidationInfo: LayoutInvalidation = { reason: "invalidateLayout" }): void
        {
            if (this.layoutSuppressed)
            {
                this.layoutAttempted = true;
                return;
            }

            // Set the source; allows shorthand { reason: "foo" }
            if (!invalidationInfo.source) { invalidationInfo.source = this; }

            // Default sizing
            const parent = this.flowParent;
            if (!parent) { Sizing.resolve(this._size, this, this._layoutSize); }

            try
            {
                // Account for unknown external factors
                this._outerDirty = this._outerDirty || !Math.Size2D.equals(this.desiredSize, this._lastDesiredSize);
            }
            catch (err)
            {
                // Most likely encountered an error whilst getting desiredSize, so let's just assume we are dirty
                // If desiredSize is actually broken and throwing errors it'll throw during the layout itself so it's OK for us to ignore it
                this._outerDirty = true;
                Log.error(err);
            }

            if (this._outerDirty && parent)
            {
                if (parent._layouting) { return; }
                // Continue up the chain
                parent.invalidateLayout(invalidationInfo);
                return;
            }

            // Should we debug log?
            if (this._debugMode)
            {
                const stack = Util.getStackTrace();
                const detail = `invoked by ${invalidationInfo.source} due to ${invalidationInfo.reason || "<unknown>"}`;
                logger.debug(`Performing layout on ${this} (${detail}).\n${stack}`);

                this.performDebugLayout();
                return;
            }

            this.performLayout();
        }

        /**
         * Temporarily suppresses layouting on this element and all children.
         * Any calls to invalidateLayout will be ignored.
         * Note that if a parent layout is invalidated, this could still relayout this element, regardless of this element's layout suppression state.
         */
        @CheckDisposed
        public suppressLayouting(): this
        {
            this._thisLayoutSuppressed = true;
            this.layoutAttempted = false;
            return this;
        }

        /**
         * Restore previously suppressed layouting on this element.
         * If a call was made to invalidateLayout during the suppression period, the layout will be invalidated.
         * @param forceInvalidate If true, this will force a relayout regardless of if a layout attempt was made during the suppression period. If false, will NOT cause a relayout.
         */
        @CheckDisposed
        public restoreLayouting(forceInvalidate?: boolean): this
        {
            this._thisLayoutSuppressed = false;
            if ((this.layoutAttempted && forceInvalidate !== false) || forceInvalidate === true)
            {
                this.layoutAttempted = false;
                this.invalidateLayout({ reason: "restoreLayouting" });
            }
            return this;
        }

        /**
         * Disposes this element.
         */
        public dispose(): void
        {
            if (this.isDisposed) { return; }
            this.unbindFromObservables();
            for (const child of [...this.children])
            {
                if (Is.disposable(child))
                {
                    child.dispose();
                }
            }
            super.dispose();
        }

        /** Calculates the minimum size we should be to fit the contents. */
        protected calculateContentsSize(out?: Math.Size2D): Math.Size2D
        {
            if (this._layouting) { throw new Error("Attempt to calculateContentsSize while in performLayout"); }

            const limit = this.limitSize;
            const childLimitW = limit.w == null ? null : Math.max(0, Math.floor(limit.w - (Spacing.widthOf(this.spacing) + Spacing.widthOf(this.padding) + Spacing.widthOf(this.innerPadding))));
            const childLimitH = limit.h == null ? null : Math.max(0, Math.floor(limit.h - (Spacing.heightOf(this.spacing) + Spacing.heightOf(this.padding) + Spacing.heightOf(this.innerPadding))));

            const tmpRect = this._tmpRect;
            tmpRect.x = 0; tmpRect.y = 0; tmpRect.w = 0; tmpRect.h = 0;
            let width = 0, height = 0;
            const children = this.flowChildren;
            for (const child of children)
            {
                if (child.visible)
                {
                    switch (child._dock)
                    {
                        case Dock.Top:
                        case Dock.Bottom:
                            child.outerLayoutSize.w = childLimitW;
                            child.outerLayoutSize.h = null;
                            break;
                        case Dock.Left:
                        case Dock.Right:
                            child.outerLayoutSize.w = null;
                            child.outerLayoutSize.h = childLimitH;
                            break;
                        case Dock.Fill:
                            child.outerLayoutSize.w = childLimitW;
                            child.outerLayoutSize.h = childLimitH;
                            break;
                    }

                    const desiredSize = child.desiredSize;

                    const sp = child.ignoreParentSpacing ? Spacing.none : this._spacing;

                    const childSize = Math.Size2D.multiply(desiredSize, child.scaleFactor, tmpSizeArr[0]);

                    if (Sizing.widthDependsOnParent(child)) { childSize.w = 0; }
                    if (Sizing.heightDependsOnParent(child)) { childSize.h = 0; }

                    if (!child._handlesRotation && child.rotation !== 0) { BaseElement.rotateLayoutArea(childSize, child.rotation); }

                    childSize.w += Spacing.widthOf(sp);
                    childSize.h += Spacing.heightOf(sp);

                    if (child._dock !== Dock.None && (this.legacy || child._dock !== Dock.Float))
                    {
                        // If our active rect size isn't big enough, expand it
                        if (this._tmpRect.w < childSize.w)
                        {
                            const expand = childSize.w - this._tmpRect.w;
                            this._tmpRect.w += expand;
                            // width = Math.max(width, this._tmpRect.x + this._tmpRect.width);
                            width += expand;
                        }
                        if (this._tmpRect.h < childSize.h)
                        {
                            const expand = childSize.h - this._tmpRect.h;
                            this._tmpRect.h += expand;
                            // height = Math.max(height, this._tmpRect.y + this._tmpRect.height);
                            height += expand;
                        }
                    }

                    switch (child._dock)
                    {
                        case Dock.Top:
                            this._tmpRect.y += childSize.h;
                            this._tmpRect.h -= childSize.h;
                            break;
                        case Dock.Bottom:
                            this._tmpRect.h -= childSize.h;
                            break;
                        case Dock.Left:
                            this._tmpRect.x += childSize.w;
                            this._tmpRect.w -= childSize.w;
                            break;
                        case Dock.Right:
                            this._tmpRect.w -= childSize.w;
                            break;
                    }
                }
            }
            out = out || Math.Size2D();
            out.w = width + Spacing.widthOf(this._padding);
            out.h = height + Spacing.heightOf(this._padding);
            return out;
        }

        protected performLayoutOnChild(child: GenericElement): void
        {
            child.performLayout();
        }

        /** Layouts this element AFTER it's layout size has been determined by the parent element. */
        @Callback
        protected performLayout(): void
        {
            if (this._layouting) { return; }
            this._layouting = true;

            this._tmpRect.x = this._innerPadding.left + this._padding.left;
            this._tmpRect.y = this._innerPadding.top + this._padding.top;
            this._tmpRect.w = this._layoutSize.w - (Spacing.widthOf(this._innerPadding) + Spacing.widthOf(this._padding));
            this._tmpRect.h = this._layoutSize.h - (Spacing.heightOf(this._innerPadding) + Spacing.heightOf(this._padding));

            const children = this.flowChildren;
            for (const child of children)
            {
                if (child.visible)
                {
                    this.layoutChildInRect(child, this._tmpRect, true);
                }
            }

            this._remainingSpace.copy(this._tmpRect);

            if (this._maskToLayoutArea)
            {
                this.drawLayoutAreaMask();
            }

            this.onLayout();
            this._layouting = false;

            this.onLayouted.publish();

            this.updateDebugShape();

            this._pendingLayout = null;
        }

        protected drawLayoutAreaMask()
        {
            let g: Rendering.IGraphics;
            if (this.mask instanceof Rendering.Graphics)
            {
                g = this.mask;
            }
            else
            {
                this.mask = g = new Rendering.Graphics();
                g.name = "Mask";
                this.addChild(g);
            }

            g
                .clear()
                .beginFill(Util.Colors.black)
                .drawRect(Math.floor(-this.pivot.x) - 1, Math.floor(-this.pivot.y) - 1, Math.ceil(this._layoutSize.w) + 2, Math.ceil(this._layoutSize.h) + 2)
                .endFill();

            g.x = this.pivot.x;
            g.y = this.pivot.y;
        }

        /**
         * Docks the specified child within the specified rectangle, optionally updating the rectangle with the remaining space.
         * @param child
         * @param rect
         * @param updateRect
         */
        protected layoutChildInRect(child: GenericElement, rect: Rendering.Rectangle, updateRect: boolean): void
        {
            const sp = child.ignoreParentSpacing ? Spacing.none : this._spacing;

            /** The total size available for the child, relative to us. */
            const outerLayoutSize = tmpSizeArr[5];
            Math.Size2D.set(outerLayoutSize, rect.w - Spacing.widthOf(sp), rect.h - Spacing.heightOf(sp));

            if (child.ignoreParentSpacing)
            {
                outerLayoutSize.w += Spacing.widthOf(this._innerPadding) + Spacing.widthOf(this._padding);
                outerLayoutSize.h += Spacing.heightOf(this._innerPadding) + Spacing.heightOf(this._padding);
            }

            outerLayoutSize.w = Math.max(0, outerLayoutSize.w);
            outerLayoutSize.h = Math.max(0, outerLayoutSize.h);

            switch (child._dock)
            {
                case Dock.Top:
                case Dock.Bottom:
                    child.outerLayoutSize.w = outerLayoutSize.w;
                    child.outerLayoutSize.h = null;
                    break;
                case Dock.Left:
                case Dock.Right:
                    child.outerLayoutSize.w = null;
                    child.outerLayoutSize.h = outerLayoutSize.h;
                    break;
                case Dock.Fill:
                    child.outerLayoutSize.w = outerLayoutSize.w;
                    child.outerLayoutSize.h = outerLayoutSize.h;
                    break;
            }

            /** The size that the child wants to be, relative to itself. */
            const desiredSize = child.desiredSize;

            Math.Size2D.copy(child.desiredSize, child._lastDesiredSize);
            child._outerDirty = false;

            /** The allocated size for the child, relative to itself. */
            const childSize = child._layoutSize;

            /** The size that the child would occupy based on it's desired size, relative to us. */
            const desiredOuterSize = Math.Size2D.multiply(desiredSize, child._scaleFactor, tmpSizeArr[1]);

            const shouldRotateChild = !child._handlesRotation && child.rotation !== 0.0;
            if (shouldRotateChild) { BaseElement.rotateLayoutArea(desiredOuterSize, child.rotation); }

            // Dock desiredOuterSize within outerLayoutSize
            const dockedOuterSize = tmpSizeArr[2];
            Math.Size2D.copy(desiredOuterSize, dockedOuterSize);
            switch (child._dock)
            {
                case Dock.Top:
                case Dock.Bottom:
                    dockedOuterSize.w = outerLayoutSize.w;
                    break;
                case Dock.Left:
                case Dock.Right:
                    dockedOuterSize.h = outerLayoutSize.h;
                    break;
                case Dock.Fill:
                    dockedOuterSize.w = outerLayoutSize.w;
                    dockedOuterSize.h = outerLayoutSize.h;
                    break;
            }

            const expandX = Expand.canExpandX(child._expand);
            const expandY = Expand.canExpandY(child._expand);

            // Do we need to handle rotation?
            if (shouldRotateChild)
            {
                // We don't support expansion (yet), so just use the desired size
                Math.Size2D.copy(desiredSize, childSize);
            }
            else
            {
                // Transform docked size back down
                Math.Size2D.divide(dockedOuterSize, child._scaleFactor, childSize);

                // Check expansion rules
                if (childSize.w > desiredSize.w && !expandX) { childSize.w = desiredSize.w; }
                if (childSize.h > desiredSize.h && !expandY) { childSize.h = desiredSize.h; }
            }
            const childSizeOuter = Math.Size2D.multiply(childSize, child._scaleFactor, tmpSizeArr[3]);
            if (shouldRotateChild) { BaseElement.rotateLayoutArea(childSizeOuter, child.rotation); }

            // Is it bigger than we can fit?
            let layoutScale = 1.0;
            if (child._enforceMinimumSize)
            {
                if (shouldRotateChild)
                {
                    Log.warn(`enforceMinimumSize is not yet supported on rotated elements`);
                    // TODO: ^
                }
                else
                {
                    // The child doesn't fit, so let's adjust the layout scale
                    if (childSize.w < desiredSize.w)
                    {
                        layoutScale = Math.max(layoutScale, desiredSize.w / childSize.w);
                        childSize.w *= layoutScale;
                        childSize.h *= layoutScale;
                    }
                    if (childSize.h < desiredSize.h)
                    {
                        layoutScale = Math.max(layoutScale, desiredSize.h / childSize.h);
                        childSize.w *= layoutScale;
                        childSize.h *= layoutScale;
                    }

                    // If we don't allow expansion...
                    if (!expandX)
                    {
                        // Clamp the size
                        childSize.w = Math.min(desiredSize.w, childSize.w);
                    }
                    if (!expandY)
                    {
                        // Clamp the size
                        childSize.h = Math.min(desiredSize.h, childSize.h);
                    }
                }

                // Rotate back to local space
                Math.Size2D.multiply(childSize, child._scaleFactor / layoutScale, childSizeOuter);
                if (!child._handlesRotation) { BaseElement.rotateLayoutArea(childSizeOuter, child.rotation); }
            }

            const rectW = rect.w;
            const rectH = rect.h;

            // Determine child position and adjust layout rectangle accordingly
            const layoutX = rect.x + sp.left - (child._ignoreParentSpacing ? (this._innerPadding.left + this._padding.left) : 0);
            const layoutY = rect.y + sp.top - (child._ignoreParentSpacing ? (this._innerPadding.top + this._padding.top) : 0);
            const rotOffsetX = childSizeOuter.w * 0.5;
            const rotOffsetY = childSizeOuter.h * 0.5;
            switch (child._dock)
            {
                case Dock.Top:
                    child._flowTransform.x = layoutX + (outerLayoutSize.w - childSizeOuter.w) * this._contentAlignment.x + rotOffsetX;
                    child._flowTransform.y = layoutY + rotOffsetY;
                    if (updateRect)
                    {
                        rect.y += (childSizeOuter.h + Spacing.heightOf(sp));
                        rect.h -= (childSizeOuter.h + Spacing.heightOf(sp));
                    }
                    child.pivot.set(childSize.w * 0.5, childSize.h * 0.5);
                    break;
                case Dock.Bottom:
                    child._flowTransform.x = layoutX + (outerLayoutSize.w - childSizeOuter.w) * this._contentAlignment.x + rotOffsetX;
                    child._flowTransform.y = layoutY + outerLayoutSize.h - childSizeOuter.h + rotOffsetY;
                    if (updateRect)
                    {
                        rect.h -= (childSizeOuter.h + Spacing.heightOf(sp));
                    }
                    child.pivot.set(childSize.w * 0.5, childSize.h * 0.5);
                    break;
                case Dock.Left:
                    child._flowTransform.x = layoutX + rotOffsetX;
                    child._flowTransform.y = layoutY + (outerLayoutSize.h - childSizeOuter.h) * this._contentAlignment.y + rotOffsetY;
                    if (updateRect)
                    {
                        rect.x += (childSizeOuter.w + Spacing.widthOf(sp));
                        rect.w -= (childSizeOuter.w + Spacing.widthOf(sp));
                    }
                    child.pivot.set(childSize.w * 0.5, childSize.h * 0.5);
                    break;
                case Dock.Right:
                    child._flowTransform.x = layoutX + outerLayoutSize.w - childSizeOuter.w + rotOffsetX;
                    child._flowTransform.y = layoutY + (outerLayoutSize.h - childSizeOuter.h) * this._contentAlignment.y + rotOffsetY;
                    if (updateRect)
                    {
                        rect.w -= (childSizeOuter.w + Spacing.widthOf(sp));
                    }
                    child.pivot.set(childSize.w * 0.5, childSize.h * 0.5);
                    break;
                case Dock.Fill:
                    child._flowTransform.x = layoutX + (outerLayoutSize.w - childSizeOuter.w) * this._contentAlignment.x + rotOffsetX;
                    child._flowTransform.y = layoutY + (outerLayoutSize.h - childSizeOuter.h) * this._contentAlignment.y + rotOffsetY;
                    // Technically the following is correct behaviour, but not very useful
                    // Instead of destructively breaking any following docked elements, just leave the rect alone
                    // This means we can have a background element with dock fill as the first child without causing issues
                    // rect.x += rect.width;
                    // rect.y += rect.height;
                    // rect.width = 0;
                    // rect.height = 0;
                    child.pivot.set(childSize.w * 0.5, childSize.h * 0.5);
                    break;
                case Dock.Float:

                    const unrotatedChildSizeOuter = Math.Size2D.multiply(childSize, child._scaleFactor / layoutScale, tmpSizeArr[4]);

                    // Find the rectangle to float within.
                    let size: RS.Math.Size2D;
                    let left: number;
                    let top: number;
                    if (child.ignoreParentSpacing)
                    {
                        size = this._layoutSize;
                        left = 0;
                        top = 0;
                    }
                    else
                    {
                        size = this.innerLayoutSize;
                        left = this._innerPadding.left + this._padding.left;
                        top = this._innerPadding.top + this._padding.top;
                    }

                    child.pivot.set(childSize.w * 0.5, childSize.h * 0.5);
                    child._flowTransform.x = left + size.w * child._floatPos.x - (unrotatedChildSizeOuter.w * (child._dockAlignment.x - 0.5));
                    child._flowTransform.y = top + size.h * child._floatPos.y - (unrotatedChildSizeOuter.h * (child._dockAlignment.y - 0.5));

                    child._flowTransform.x += child._floatOffset.x;
                    child._flowTransform.y += child._floatOffset.y;

                    //child.pivot.x = (childSize.w / child.scaleFactor) * child.dockAlignment.x;
                    //child.pivot.y = (childSize.h / child.scaleFactor) * child.dockAlignment.y;

                    break;
                default:
                    child.pivot.set(0, 0);
                    break;
            }

            // Handle restricting children overflowing their opposite boundary.
            if (this.overflowMode === OverflowMode.Shrink)
            {
                switch (child._dock)
                {
                    case RS.Flow.Dock.Right:
                        // Need to also adjust position for right dock.
                        const dx = childSizeOuter.w - rectW;
                        if (dx > 0)
                        {
                            child._flowTransform.x += dx;
                        }
                    case RS.Flow.Dock.Left:
                        if (childSizeOuter.w > rectW)
                        {
                            // Scale down in our space, then convert that into child's space.
                            const scale = rectW / childSizeOuter.w;
                            childSize.w *= scale;
                        }
                        break;

                    case RS.Flow.Dock.Bottom:
                        // Need to also adjust position for bottom dock.
                        const dy = childSizeOuter.h - rectH;
                        if (dy > 0)
                        {
                            child._flowTransform.y += dy;
                        }
                    case RS.Flow.Dock.Top:
                        if (childSizeOuter.h > rectH)
                        {
                            // Scale down in our space, then convert that into child's space.
                            const scale = rectH / childSizeOuter.h;
                            childSize.h *= scale;
                        }
                        break;
                }
            }

            childSize.w = Math.round(childSize.w);
            childSize.h = Math.round(childSize.h);
            child._flowTransform.scaleX = child._flowTransform.scaleY = child._scaleFactor / layoutScale;
            child.skew.set(0, 0);

            if (child.dock !== Dock.None) { this.updateTransformsOnChild(child); }
            this.performLayoutOnChild(child);
        }

        protected getChildFlowTransform(child: GenericElement): Transform
        {
            return child._flowTransform;
        }

        protected updateTransformsOnChild(child: GenericElement): void
        {
            child.updateTransforms();
        }

        protected updateTransforms(): void
        {
            this.position.set(this._flowTransform.x + this._postTransform.x, this._flowTransform.y + this._postTransform.y);
            this.scale.set(this._flowTransform.scaleX * this._postTransform.scaleX, this._flowTransform.scaleY * this._postTransform.scaleY);
            this.updateDebugShape();
        }

        protected updateDebugShape(): void
        {
            this.updateDebugHitArea();

            if (this._debugMode)
            {
                this._debugShape = new Rendering.Graphics();
                this.addChild(this._debugShape);

                if (Transform.hasChanges(this._postTransform))
                {
                    this.updateDebugPostTransform();
                }

                this._debugShape
                    .lineStyle(2, Util.Colors.red)
                    .drawRect(0, 0, this._layoutSize.w, this._layoutSize.h);
            }
            else
            {
                this._debugShape = null;
            }
        }

        /** Draws the original position and size of the element prior to post-transformation. */
        protected updateDebugPostTransform(): void
        {
            const invertX = 1 / this._postTransform.scaleX;
            const invertY = 1 / this._postTransform.scaleY;

            const baseSize = this._layoutSize;
            const scaledW = baseSize.w * this._postTransform.scaleX;
            const scaledH = baseSize.h * this._postTransform.scaleY;

            const deltaW = scaledW - baseSize.w;
            const deltaH = scaledH - baseSize.h;

            const scaleOffsX = deltaW / 2;
            const scaleOffsY = deltaH / 2;

            this._debugShape
                .lineStyle(2, Util.Colors.cyan)
                .drawRect(-this._postTransform.x * invertX + scaleOffsX * invertX,
                          -this._postTransform.y * invertY + scaleOffsY * invertY,
                          this._layoutSize.w * invertX,
                          this._layoutSize.h * invertY);
        }

        /** Determines the current type of hit area and draws it for debugging */
        protected updateDebugHitArea(): void
        {
            if (this._debugMode && !this._debugHitAreaObserverSet)
            {
                Util.observe(this as BaseElement<TSettings, TRuntimeData>, { hitArea: true }, () => this.updateDebugHitArea());
                this._debugHitAreaObserverSet = true;
            }

            if (this._debugMode && this.hitArea)
            {
                this._debugHitArea = new Rendering.Graphics();
                this.addChild(this._debugHitArea);

                this._debugHitArea
                    .clear()
                    .lineStyle(4, Util.Colors.white)
                    .drawShape(this.hitArea);
            }
            else
            {
                this._debugHitArea = null;
            }
        }

        /** Called when this element has been layouted. */
        protected onLayout(): void
        {
            return;
        }

        /** Called when this element has a new flow parent. */
        protected onFlowParentChanged(): void
        {
            if (this.isDisposed) { return; }
            const children = this.flowChildren;
            for (const child of children)
            {
                child.onFlowParentChanged();
            }
        }

        /** Performs a layout with debug information. */
        private performDebugLayout()
        {
            const size = getHierarchySize(this);
            logger.debug(`Performing layout on ${size} elements.`);

            const startTime = performance.now();
            this.performLayout();
            const timeTaken = performance.now() - startTime;
            logger.debug(`Finished layout in ${timeTaken}ms.`);
        }
    }

    export type GenericElement = BaseElement<any, any>;
}

namespace RS.Is
{
    /**
     * Gets if the specified value is a flow element.
     * @param value
     */
    export function flowElement(value: any): value is Flow.GenericElement
    {
        return value != null && value instanceof Flow.BaseElement;
    }
}
