/// <reference path="elements/Label.ts" />
/// <reference path="elements/List.ts" />

namespace RS.Flow
{
    /** Returns the number of Flow elements in the given hierarchy, including the one passed in. */
    export function getHierarchySize(element: GenericElement)
    {
        return element.flowChildren.reduce((counter, thisElement) => thisElement.visible ? counter + getHierarchySize(thisElement) : counter, 1)
    }

    /**
     * Generates a list of flow labels with different label settings for each part of a complex translation.
     * @param locale
     * @param translation
     * @param substitutions
     * @param textSettings settings for non-variable and non-protected text parts
     * @param variableSettings settings for variable text parts, or a function which can return different settings per-variable
     * @param protectedSettings settings for protected term text parts, or a function which can return different settings per-term
     * @param listSettings settings for the list itself, or alternatively an actual list to put the text parts in
     * @param imageVariableSettings settings for variable text parts to be replaced with images, image substitutions are higher priority than text
     */
    export function generateComplexText<T extends object>(
        locale: RS.Localisation.Locale,
        translation: RS.Localisation.ComplexTranslationReference<T>,
        substitutions: T,
        textSettings: Label.Settings,
        variableSettings?: Label.Settings | ((variableName: string) => Label.Settings | null),
        protectedSettings?: Label.Settings | ((protectedName: string) => Label.Settings | null),
        listSettings?: List.Settings | List,
        imageVariableSettings?: ((variableName: string) => Image.Settings | null)
    ): Flow.List
    {
        const list = listSettings instanceof List ? listSettings : new Flow.List({ ...Flow.List.defaultSettings, ...listSettings });
        const parts = translation.getParts(locale);
        for (const part of parts)
        {
            let labelSettings = textSettings;
            let labelText: string = "";
            switch (part.kind)
            {
                case RS.Localisation.TranslationPart.Kind.Text:
                    labelText = part.value;
                    break;
                case RS.Localisation.TranslationPart.Kind.Variable:
                    if (imageVariableSettings)
                    {
                        const imageSettings = imageVariableSettings(part.value);
                        if (imageSettings != null)
                        {
                            Flow.image.create(imageSettings, list);
                            continue;
                        }
                    }
                    labelSettings = resolveSettings(variableSettings, labelSettings, part.value);
                    labelText = resolveSubsitution(substitutions, part.value);
                    break;
                case RS.Localisation.TranslationPart.Kind.Protected:
                    labelSettings = resolveSettings(protectedSettings, labelSettings, part.value);
                    labelText = resolveSubsitution(substitutions, part.value);
                    break;
            }
            Flow.label.create({ ...labelSettings, text: labelText }, list);
        }
        return list;
    }

    function resolveSettings(settingsFunction: Label.Settings | ((variableName: string) => Label.Settings | null), defaultSetting: RS.Flow.Label.Settings, key: string): RS.Flow.Label.Settings
    {
        if (Is.func(settingsFunction))
        {
            return settingsFunction(key) || defaultSetting;
        }
        else if (settingsFunction != null)
        {
            return settingsFunction;
        }
        else
        {
            return defaultSetting;
        }
    }

    function resolveSubsitution(map: object, key: string): string
    {
        if (map[key] != null)
        {
            return map[key];
        }
        RS.Log.error(`Failed to find substitution for complex text, substitution variable key was "${key}", ensure you use string keys to avoid potential issues when ClosureCompiling.`);
        return "";
    }

    /** turns debug mode on or off for provided element and
     * everything which is layouted to it
     * @param base the element to apply the debug to
     * @param value true to turn debug on, false to turn it off
     * */
    export function deepDebug(base: RS.Flow.BaseElement<any, any>, value: boolean)
    {
        const elements: RS.Flow.BaseElement[] = [];
        elements.push(base);
        for (const element of elements)
        {
            for (const child of element.flowChildren)
            {
                elements.push(child);
            }
            element.debugMode = value;
        }
    }
}