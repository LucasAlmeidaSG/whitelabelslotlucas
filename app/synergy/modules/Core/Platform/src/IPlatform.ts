namespace RS
{
    /**
     * Encapsulates the state that the game is in, as far as the platform needs to know.
     */
    export enum PlatformGameState
    {
        /** The initial game state before we have any server data. */
        Uninitialised,
        /** The game state between receiving initialising server data and the game being interactable. */
        NotReady,
        /** The game state in which player interaction is expected. */
        Ready,
        /** The game state in which the player is being prompted to resume into a previously-opened wager. */
        Resuming,
        /** The game state in which the player is currently playing through an open wager. */
        InPlay,
        /** The game state in which the player has completed the wager cycle but may not necessarily be able to begin a new one due to e.g. minimum duration regulations. */
        InPlayPostWinRendering
    }

    /**
     * Specifies how errors should be handled.
     */
    export enum PlatformMessageHandlingMode
    {
        Ignore,
        Display,
        PassToPlatform
    }

    /**
     * Specifies where translations come from.
     */
    export enum PlatformLocaleMode
    {
        ProvidedByPlatform,
        ProvidedByGame
    }

    /**
     * Specifies how internet connection lost should be handled.
     */
    export enum PlatformConnectionLostMode
    {
        Ignore,
        PauseWithMessage,
        Error
    }

    /**
     * Specifies the way max win will be handled.
     */
    export enum PlatformMaxWinMode
    {
        None,
        GreaterThan,
        GreaterThanOrEqual
    }

    /**
     * Specifies the way settings will be handled.
     */
    export enum PlatformSettingsHandlingMode
    {
        ProvidedByGame,
        ProvidedByPlatform
    }

    /**
     * Encapsulates the platform that the game sits upon.
     * This could be a wrapper, integration or environment.
     */
    export interface IPlatform
    {
        /**
         * Gets the root URL to load assets from.
         */
        readonly assetsRoot: string;

        /**
         * Gets the root URL to load translations from.
         */
        readonly translationsRoot: string;

        /**
         * Gets the code of the locale to use (e.g. "en_GB").
         */
        readonly localeCode: string;

        /**
         * Gets the code of the country the player is in.
         * May not match the country code in the locale code.
         */
        readonly countryCode: string;

        /**
         * Gets how translations are handled.
         */
        readonly localeMode: PlatformLocaleMode;

        /**
         * Gets how max win will be handled.
         */
        readonly maxWinMode: PlatformMaxWinMode;

        /**
         * Gets whether or not the current platform allows turbo-mode.
         */
        readonly turboModeEnabled: boolean;

        /**
         * Gets whether or not the current platform allows big bet.
         */
        readonly bigBetEnabled: boolean;

        /**
         * Gets the minimum duration of a wager (i.e. the minimum time between wagers).
         */
        readonly minWagerDuration: number;

        /**
         * Gets or sets the locale object to use (the game should either read or write this, depending on localeMode).
         */
        locale: Localisation.Locale;

        /**
         * Gets or sets the progress of the preload phase (0-1).
         */
        preloaderProgress: number;

        /**
         * Gets or sets the visibility of the preload bar.
         */
        preloaderVisible: boolean;

        /**
         * Gets or sets the progress of the load phase (0-1).
         */
        loaderProgress: number;

        /**
         * Gets the URL of the engine endpoint, or null to use the default.
         * @deprecated SG (RGS) specific; will be removed from the generic IPlatform interface at some point in the future.
         */
        readonly engineEndpointOverride: string | null;

        /** Gets whether or not we are currently in free-play i.e. demo mode/not playing for real money. */
        readonly isFreePlay: boolean;

        /**
         * Gets a generic adapter object for the platform.
         * The exact type and value of this will depend on the concrete platform implementation and should not be relied upon by user code.
         */
        readonly adapter: object | null;

        /**
         * Gets how network errors should be handled.
         */
        readonly networkErrorHandlingMode: PlatformMessageHandlingMode;

        /**
         * Gets how general errors should be handled.
         */
        readonly generalErrorHandlingMode: PlatformMessageHandlingMode;

        /**
         * Gets how internet connection lost should be handled.
         */
        readonly connectionLostHandlingMode: PlatformConnectionLostMode;

        /**
         * Gets how recovery messages should be handled.
         */
        readonly recoveryMessageHandlingMode: PlatformMessageHandlingMode;

        /**
         * Gets if the platform supports the "navigate to lobby" function.
         */
        readonly canNavigateToLobby: boolean;

        /**
         * Gets if the platform supports opening a settings menu.
         */
        readonly canAccessSettings: boolean;

        /**
         * Gets how settings menus should be handled.
         */
        readonly settingsHandlingMode: PlatformSettingsHandlingMode;

        /**
         * Gets if the platform supports the "navigate to external help" function.
         */
        readonly canNavigateToExternalHelp: boolean;

        /**
         * Gets if the game should display generic dev tools or not.
         */
        readonly shouldDisplayDevTools: boolean;

        /**
         * Gets if the game should display the demo tool or not.
         */
        readonly shouldDisplayDemoTool: boolean;

        /**
         * Gets if the game should display the gaffing tool or not.
         */
        readonly shouldDisplayGaffingTool: boolean;

        /**
         * Gets the partner code;
         */
        readonly partnerCode: string|null;

        /**
         * Published when the user's balance has been updated externally.
         */
        readonly onBalanceUpdatedExternally: IEvent<number>;

        /**
         * Initialises the platform.
         * This is called when the game class has been initialised.
         */
        init(settings: IPlatform.Settings): void;

        /**
         * Handles a network error.
         * Returns a promise that resolves when the error has been handled.
         */
        handleNetworkError(error: Error | string): PromiseLike<void>;

        /**
         * Handles a general error.
         * Returns a promise that resolves when the error has been handled.
         */
        handleGeneralError(error: Error | string): PromiseLike<void>;

        /**
         * Handles displaying the recovery message.
         * Returns a promise that resolves when the message has been dismissed.
         */
        handleRecoveryMessage(): PromiseLike<void>;

        /**
         * Handles displaying a general message.
         * Returns a promise that resolves when the message has been dismissed.
         */
        handleGeneralMessage(message: string, title?: string): PromiseLike<void>;

        /**
         * Directs the platform to navigate to the lobby.
         */
        navigateToLobby(): void;

        /**
         * Directs the platform to open the settings menu.
         */
        openSettingsMenu(): void;

        /**
         * Directs the platform to navigate to the external help page.
         */
        navigateToExternalHelp(externalHelpData?: RS.Map<any>): void;

        /**
         * Directs the platform to reload the page.
         */
        reload(): void;

        /**
         * Directs the platform to close the page.
         */
        close(): void;

        /**
         * Mutes the game using the platform class.
         * This is so we can keep the platform and internal mute buttons in sync
         */
        internalMute(mute: boolean): void;
    }

    export const IPlatform = Strategy.declare<IPlatform>(Strategy.Type.Singleton, false, true);

    export namespace IPlatform
    {
        export interface Settings
        {
            pauseArbiter: IArbiter<boolean>;
            muteArbiter: IArbiter<boolean>;
            uiEnabledArbiter: IArbiter<boolean>;
            uiContainer?: Rendering.IContainer;
        }

        /**
         * Encapsulates generic data about the current session.
         * @deprecated SG (RGS) specific; will be removed from the generic IPlatform module at some point in the future.
         */
        export interface SessionInfo
        {
            /** Username associated with the current session. */
            userName: string | null;

            /** User ID associated with the current session. */
            userID: number | null;

            /** Session ID associated with the current session. */
            sessionID: string | null;

            /** Affiliate ID (aka context ID) associated with the current session. */
            affiliateID: string | null;
        }
    }
}
