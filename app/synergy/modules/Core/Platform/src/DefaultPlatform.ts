/// <reference path="IPlatform.ts" />

namespace RS
{
    /**
     * Default implementation for the platform.
     */
    class DefaultPlatform implements IPlatform
    {
        /**
         * Gets the root URL to load assets from.
         */
        public readonly assetsRoot: string = "assets";

        /**
         * Gets the root URL to load translations from.
         */
        public readonly translationsRoot: string = "translations";

        /**
         * Gets the URL of the engine endpoint, or null to use the default.
         */
        public readonly engineEndpointOverride: string | null = null;

        /**
         * Gets the code of the locale to use (e.g. "en_GB").
         */
        public get localeCode() { return URL.getParameter("locale", "en_GB"); }

        /**
         * Gets the code of the country the player is in.
         * May not match the country code in the locale code.
         */
        public get countryCode() { return URL.getParameter("countrycode", "GB"); }

        /** Gets whether or not we are currently in free-play i.e. demo mode/not playing for real money. */
        public get isFreePlay() { return URL.getParameterAsBool("freeplay", false); }

        /**
         * Gets how translations are handled.
         */
        public readonly localeMode: PlatformLocaleMode = PlatformLocaleMode.ProvidedByGame;

        public readonly turboModeEnabled = true;

        /**
         * Gets whether or not the current platform allows big bet.
         */
        public readonly bigBetEnabled: boolean = true;

        /**
         * Gets how max win will be handled.
         */
        public readonly maxWinMode: PlatformMaxWinMode = PlatformMaxWinMode.None;

        /**
         * Gets the minimum duration of a wager (i.e. the minimum time between wagers).
         */
        public readonly minWagerDuration: number = 0;

        /**
         * Gets or sets the locale object to use (the game should either read or write this, depending on localeMode).
         */
        public get locale() { return this._locale; }
        public set locale(value) { this._locale = value; }

        /**
         * Gets or sets the progress of the preload phase (0-1).
         */
        public preloaderProgress: number = 0.0;

        /**
         * Gets or sets the visibility of the preload bar.
         */
        public preloaderVisible: boolean = true;

        /**
         * Gets or sets the progress of the load phase (0-1).
         */
        public loaderProgress: number = 0.0;

        /**
         * Gets a generic adapter object for the platform.
         * The exact type and value of this will depend on the concrete platform implementation and should not be relied upon by user code.
         */
        public readonly adapter: object | null = null;

        /**
         * Gets how network errors should be handled.
         */
        public readonly networkErrorHandlingMode: PlatformMessageHandlingMode = PlatformMessageHandlingMode.Display;

        /**
         * Gets how general errors should be handled.
         */
        public readonly generalErrorHandlingMode: PlatformMessageHandlingMode = PlatformMessageHandlingMode.Display;

        /**
         * Gets how recovery messages should be handled.
         */
        public readonly recoveryMessageHandlingMode: PlatformMessageHandlingMode = PlatformMessageHandlingMode.Display;

        /**
         * Gets how general messages should be handled.
         */
        public readonly generalMessageHandlingMode: PlatformMessageHandlingMode = PlatformMessageHandlingMode.Display;

        /**
         * Gets how internet connection lost should be handled.
         */
        public readonly connectionLostHandlingMode: PlatformConnectionLostMode = PlatformConnectionLostMode.PauseWithMessage;

        /**
         * Gets if the platform supports the "navigate to lobby" function.
         */
        public readonly canNavigateToLobby: boolean = false;

        /**
         * Gets if the platform supports opening a settings menu.
         */
        public readonly canAccessSettings: boolean = false;

        /**
         * Gets how settings menus should be handled.
         */
        public readonly settingsHandlingMode: PlatformSettingsHandlingMode = PlatformSettingsHandlingMode.ProvidedByGame;

        /**
         * Gets if the platform supports the "navigate to external help" function.
         */
        public readonly canNavigateToExternalHelp: boolean = false;

        /**
         * Gets if the game should display generic dev tools or not.
         */
        public readonly shouldDisplayDevTools: boolean = URL.getParameterAsBool("dev");

        /**
         * Gets if the game should display the demo tool or not.
         */
        public readonly shouldDisplayDemoTool: boolean = URL.getParameterAsBool("dev");

        /**
         * Gets if the game should display the gaffing tool or not.
         */
        public readonly shouldDisplayGaffingTool: boolean = URL.getParameterAsBool("dev");

        /**
         * Gets the partner code.
         */
        public readonly partnerCode: string|null = null;

        /**
         * Published when the user's balance has been updated externally.
         */
        public readonly onBalanceUpdatedExternally = createEvent<number>();//this._onBalanceUpdatedExternally.public;

        private _locale: Localisation.Locale;

        public constructor()
        {
            Log.debug("Using default platform");

            this.triggerInits();
        }

        /**
         * Initialises the platform.
         * This is called when the game class has been initialised.
         */
        public init(settings: IPlatform.Settings): void { return; }

        /**
         * Handles a network error.
         */
        public async handleNetworkError(error: Error | string) { return; }

        /**
         * Handles a general error.
         */
        public async handleGeneralError(error: Error | string) { return; }

        /**
         * Handles displaying the recovery message.
         */
        public async handleRecoveryMessage() { return; }

        /**
         * Handles displaying a general message.
         */
        public async handleGeneralMessage(message: string, title?: string) { return; }

        /**
         * Directs the platform to navigate to the lobby.
         */
        public navigateToLobby(): void { return; }

        /**
         * Directs the platform to open the settings menu.
         */
        public openSettingsMenu(): void { return; }

        /**
         * Directs the platform to navigate to the external help page.
         */
        public navigateToExternalHelp(externalHelpData?: RS.Map<string>): void { return; }

        public close()
        {
            window.close();
        }

        public reload()
        {
            location.reload();
        }

        public internalMute(mute: boolean): void { return; }

        @Callback
        private triggerInits()
        {
            DOM.Document.readyState.for(true).then(() => RS.triggerInits());
        }
    }

    IPlatform.register(DefaultPlatform);
}
