namespace RS.AssetPipeline
{
    const msgpack = require('@msgpack/msgpack');

    @AssetType("msgpack")
    export class MsgPack extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ ".msgpack" ]; }

        public get classModule() { return "Core.Asset.MsgPackPipeline"; }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitAssetReference(this, "RS.Asset.MsgPackReference");
        }
        /**
         * Processes a single format for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            if (format != ".msgpack")
            {
                Log.error(`Unsupported format '${format}'`);
                return null;
            }
            if (!dry)
            {
                const outputPath = Path.combine(outputDir, Path.sanitise(Path.baseName(Path.replaceExtension(assetPath, format))));
                // Minifies & validates syntax
                const obj = await RS.JSON.parseFile(assetPath);
                const buf = msgpack.encode(obj);
                await FileSystem.writeFile(outputPath, buf);
            }

            // Return variant data
            return {
                paths: [ Path.sanitise(Path.replaceExtension(assetPath, format)) ]
            };
        }
    }
}