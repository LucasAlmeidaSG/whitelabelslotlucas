namespace RS.SheetPacker
{
    const os: { cpus(): any[]; } = require("os") as any;

    /** Max length of a command string. Used to batch images to avoid ENAMETOOLONG errors when building a lot of images. */
    const maxCommandLength = 16000;

    type Frame = [number, number, number, number, number, number, number, number, number, number, number];
    enum FrameKey
    {
        X, Y,
        Width, Height,
        ImageID,
        PivotX, PivotY,

        SourceWidth, SourceHeight,
        TrimOffsetX, TrimOffsetY
    }

    interface Sheet
    {
        images: string[];
        frames: Frame[];
        framerate: number;
        animations:
        {
            [name: string]:
            {
                frames: number[],
                next: string | null;
            };
        };
        events: SheetEvent[];
        meta:
        {
            version: string;
            exported: string;
            options:
            {
                compact: boolean;
                sorted: boolean;
                fps: number;
            };
            smartKey?: string;
        };
    }

    interface SheetEvent
    {
        event: string;
        frame: number;
    }

    interface Image extends Size2D
    {
        /** Relative source path. */
        sourcePath: string;

        /**
         * Scale that has been applied to this image. \
         * NOTE: w and h are already scaled.
         */
        scale: number;

        /** Extrusion amount in pixels. */
        extrude: number;

        /** Width of the untrimmed rect. */
        trimOuterW: number;
        /** Height of the untrimmed rect. */
        trimOuterH: number;
        /** X offset of the trimmed rect. */
        trimOffsetX: number;
        /** Y offset of the trimmed rect. */
        trimOffsetY: number;

        /** Original trim rect x. Used to avoid recalculation of the trimmed bounds. */
        cropX: number;
        /** Original trim rect y. Used to avoid recalculation of the trimmed bounds. */
        cropY: number;
        /** Original trim rect w. Used to avoid recalculation of the trimmed bounds. */
        cropW: number;
        /** Original trim rect h. Used to avoid recalculation of the trimmed bounds. */
        cropH: number;
    }

    function Image(): Image
    {
        return {
            w: 0, h: 0,
            sourcePath: "",
            scale: 1,
            extrude: 0,
            trimOffsetX: 0, trimOffsetY: 0,
            trimOuterW: 0, trimOuterH: 0,

            cropW: 0, cropH: 0,
            cropX: 0, cropY: 0
        };
    }

    export class Packer
    {
        public readonly supportedImageFormats: ReadonlyArray<string> = [".png", ".psd", ".swf", ".tga", ".jpg", ".jpeg", ".tiff", ".bmp"];

        protected readonly _settings: Packer.Settings;

        constructor(settings: Packer.Settings)
        {
            if (settings.maxSize == null) { throw new Error("Max size must be specified"); }
            this._settings = { maxSize: settings.maxSize };

            const outputFormat = settings.outputFormat || this.supportedImageFormats[0];
            if (this.supportedImageFormats.indexOf(outputFormat) === -1) { throw new Error(`Unsupported format ${outputFormat}`); }
            this._settings.outputFormat = outputFormat;

            const scale = settings.scale == null ? 1 : settings.scale;
            if (scale <= 0) { throw new Error("Scale must be greater than 0"); }
            this._settings.scale = scale;

            const extrude = settings.extrude == null ? 1 : settings.extrude;
            if (extrude < 0) { throw new Error("Extrude must be 0 or greater"); }
            this._settings.extrude = extrude;

            const margin = settings.margin == null ? 0 : settings.margin;
            if (margin < 0) { throw new Error("Margin must be 0 or greater"); }
            this._settings.margin = Math.max(margin, 1);

            this._settings.defaultPivotX = settings.defaultPivotX == null ? 0.5 : settings.defaultPivotX;
            this._settings.defaultPivotY = settings.defaultPivotY == null ? 0.5 : settings.defaultPivotY;

            this._settings.constrainPOT = settings.constrainPOT || false;

            const framerate = settings.framerate == null ? 24 : settings.framerate;
            if (framerate <= 0) { throw new Error("Framerate must be greater than 0"); }
            this._settings.framerate = framerate;

            this._settings.verbose = settings.verbose || false;
            this._settings.quiet = settings.quiet || false;

            const trimThreshold = settings.trimThreshold == null ? 1 : settings.trimThreshold;
            if (trimThreshold < 0 || trimThreshold > 255) { throw new Error("Trim threshold must be between 0 and 255"); }
            this._settings.trimThreshold = trimThreshold;

            const concurrencyLimit = settings.concurrency == null ? os.cpus().length : settings.concurrency;
            if (concurrencyLimit < 1) { throw new Error("Concurrency limit must be at least 1"); }
            this._settings.concurrency = concurrencyLimit;
        }

        public async packSheet(imageDir: string, outputPath: string): Promise<void>
        {
            Profile.enter("prepare");

            try
            {
                await this.cleanUpSheet(outputPath);
            }
            catch (err)
            {
                // Failed to clean up old sheet, but doesn't matter
            }

            await FileSystem.createPath(Path.directoryName(outputPath));

            Profile.exit("prepare");
            Profile.enter("identify");

            const animFileMap: RS.Util.Map<string[]> = {};
            const images = await this.getImages(imageDir, animFileMap);

            Profile.exit("identify");
            Profile.enter("preprocess");

            // Apply global size pretransformations
            await this.doConcurrently(images, async (image) =>
            {
                try
                {
                    await this.trimImage(image, outputPath);
                }
                catch (err)
                {
                    // Warn prints to stderr
                    // No error, just print to stdout and keep going
                    console.log(`Failed to trim image: ${err}`);
                }

                // Scale, then extrude (don't scale the extrusion)
                this.scaleImage(image, this._settings.scale);
                this.extrudeImage(image, this._settings.extrude);
            });

            Profile.exit("preprocess");
            Profile.enter("pack");

            // Do pack
            const packs = this.packImages(images);

            Profile.exit("pack");
            Profile.enter("composite");

            // Get blank sheet
            const sheet = this.createEmptySpriteSheet();

            const defaultPivotX = this._settings.defaultPivotX;
            const defaultPivotY = this._settings.defaultPivotY;

            // Add frames, build animation map
            const frameIdxMap: RS.Util.Map<number> = {};
            await this.doConcurrently(packs, async (pack, i) =>
            {
                if (this._settings.constrainPOT)
                {
                    pack.size.w = nextPowerOf2(pack.size.w);
                    pack.size.h = nextPowerOf2(pack.size.h);
                }

                const outputFormat = this._settings.outputFormat;
                const imagePath = Path.replaceExtension(outputPath, `_${i}${outputFormat}`);
                await this.savePackImage(pack, imagePath);

                // Add image
                sheet.images[i] = Path.baseName(imagePath);

                // Add pack frame data
                for (let j = 0; j < pack.sizeRefs.length; j++)
                {
                    const frame = this.createBlankFrame();

                    const rect = pack.rects[j];
                    const image = pack.sizeRefs[j];

                    const extrusionSizeOffset = image.extrude * -2;

                    frame[FrameKey.X] = rect.x + image.extrude;
                    frame[FrameKey.Y] = rect.y + image.extrude;
                    frame[FrameKey.Width] = rect.w + extrusionSizeOffset;
                    frame[FrameKey.Height] = rect.h + extrusionSizeOffset;

                    frame[FrameKey.ImageID] = i;

                    frame[FrameKey.PivotX] = image.trimOuterW * defaultPivotX - image.trimOffsetX;
                    frame[FrameKey.PivotY] = image.trimOuterH * defaultPivotY - image.trimOffsetY;

                    frame[FrameKey.SourceWidth] = image.trimOuterW + extrusionSizeOffset;
                    frame[FrameKey.SourceHeight] = image.trimOuterH + extrusionSizeOffset;
                    frame[FrameKey.TrimOffsetX] = image.trimOffsetX;
                    frame[FrameKey.TrimOffsetY] = image.trimOffsetY;

                    // Push
                    const frameIdx = sheet.frames.length;
                    sheet.frames.push(frame);

                    // Add frameIDs for later
                    frameIdxMap[image.sourcePath] = frameIdx;
                }
            });

            Profile.exit("composite");
            Profile.enter("json");

            // Add animations
            this.mapAnimationFrames(sheet, animFileMap, frameIdxMap);

            await FileSystem.writeFile(outputPath, JSON.stringify(sheet));

            if (!this._settings.quiet)
            {
                console.info(`Saved ${outputPath}`);
            }

            Profile.exit("json");
        }

        /**
         * Executes the given function over the given array asynchronously and concurrently.
         * Resolves with an array of results.
         * The order of the resulting array matches the input array.
         */
        protected async doConcurrently<T, U>(array: ReadonlyArray<T>, executor: (value: T, index: number) => PromiseLike<U>): Promise<U[]>
        {
            if (array.length === 0) { return; }

            const concurrencyLimit = Math.min(this._settings.concurrency, array.length);
            const batchSize = Math.ceil(array.length / concurrencyLimit);

            const promises: PromiseLike<void>[] = [];
            const results = new Array<U>(array.length);
            for (let i = 0; i < concurrencyLimit; i++)
            {
                const promise = (async () =>
                {
                    const start = i * batchSize;
                    const end = Math.min(start + batchSize, array.length);
                    for (let j = start; j < end; j++)
                    {
                        results[j] = await executor(array[j], j);
                    }
                })();
                promises.push(promise);
            }
            await Promise.all(promises);
            return results;
        }

        protected trimThresholdToPercentage(threshold: number): number
        {
            return 100 * (threshold / 255);
        }

        /** Maps animation image files to frame arrays. */
        protected mapAnimationFrames(sheet: Sheet, animationFileMap: RS.Util.Map<string[]>, imageIndexMap: RS.Util.Map<number>)
        {
            for (const animName in animationFileMap)
            {
                const imageFrames = animationFileMap[animName];
                sheet.animations[animName] =
                {
                    frames: imageFrames.map((imageFile, index) =>
                    {
                        const frame = imageIndexMap[imageFile];
                        if (!Is.number(frame)) { throw new Error(`Internal error: frame ${index} of ${animName} is not a valid frame reference (got ${frame})`); }
                        return frame;
                    }),
                    next: null
                };
            }
        }

        /** Trims the given image, storing the result in outputDir and updating the Image objects accordingly. */
        protected async trimImage(image: Image, outputPath: string)
        {
            if (this._settings.trimThreshold <= 0) { return; }
            const fuzziness = this.trimThresholdToPercentage(this._settings.trimThreshold);

            const info = await MagickUtils.run("convert",
            [
                image.sourcePath,
                "-quiet",

                "-bordercolor", "transparent",
                "-border", "1x1",
                "-fuzz", `${Math.floor(Math.min(Math.max(fuzziness, 0), 100))}%`,

                "-format", "%@:%k",
                "info:"
            ]);

            const match = info.match(/\s*([0-9]+)x([0-9]+)\+([0-9]+)\+([0-9]+):([0-9]+)\s*/);
            if (!match) { throw new Error(`Could not read size from identification string: "${info}"`); }
            const [, wStr, hStr, xStr, yStr, cols] = match;

            // Negative offset compensates for 1x1 border
            const x = parseInt(xStr) - 1, y = parseInt(yStr) - 1,
                  w = parseInt(wStr), h = parseInt(hStr);

            if (parseInt(cols) <= 1)
            {
                // Empty image; magick returns the source image size so we have to set this manually
                image.trimOffsetX = 0;
                image.trimOffsetY = 0;
                image.w = 1;
                image.h = 1;
            }
            else
            {
                image.trimOffsetX = x;
                image.trimOffsetY = y;
                image.w = w;
                image.h = h;
            }

            image.cropX = image.trimOffsetX;
            image.cropY = image.trimOffsetY;
            image.cropW = image.w;
            image.cropH = image.h;
        }

        /** Extrudes an image by a pixel amount each side. */
        protected extrudeImage(image: Image, amount: number): void
        {
            if (!Is.integer(amount)) { throw new Error(`Internal error: invalid extrusion amount ${amount}`); }
            if (amount <= 0) { return; }

            const sizeOffset = amount * 2;
            image.extrude += amount;
            image.w += sizeOffset;
            image.h += sizeOffset;
            image.trimOuterW += sizeOffset;
            image.trimOuterH += sizeOffset;
        }

        /** Scales an image by a factor. */
        protected scaleImage(image: Image, factor: number): void
        {
            if (!Is.number(factor)) { throw new Error(`Internal error: invalid scale factor ${factor}`); }
            if (factor === 1) { return; }

            image.scale *= factor;
            image.w = Math.ceil(image.w * factor);
            image.h = Math.ceil(image.h * factor);
            image.trimOuterW = Math.ceil(image.trimOuterW * factor);
            image.trimOuterH = Math.ceil(image.trimOuterH * factor);
            image.trimOffsetX = image.trimOffsetX * factor;
            image.trimOffsetY = image.trimOffsetY * factor;
        }

        protected resolveIntermediateWorkingPath(image: Image, outputPath: string): string
        {
            return Path.replaceExtension(outputPath, `_${Path.baseName(image.sourcePath)}`);
        }

        /** Deletes the sheet at path. */
        protected async cleanUpSheet(path: string)
        {
            const dir = Path.directoryName(path);
            const dirFiles = await FileSystem.readFiles(dir);

            const data = await JSON.parseFile(path);
            if (!Is.object(data)) { return; }
            if (!Is.array(data.images)) { return; }

            for (const image of data.images)
            {
                if (!Is.string(image)) { continue; }

                const fullPathNoExt = Path.combine(dir, image);
                for (const ext of this.supportedImageFormats)
                {
                    const fullPath = Path.replaceExtension(fullPathNoExt, ext);
                    if (dirFiles.indexOf(fullPath) === -1) { continue; }
                    await FileSystem.deletePath(fullPath);
                }
            }

            await FileSystem.deletePath(path);
        }

        /** Returns an array of Images for a given directory. Updates passed in animation-file map. */
        protected async getImages(dir: string, animationFileMap: RS.Util.Map<string[]>): Promise<Image[]>
        {
            // Get all image files in lexicographical order
            const allFiles = await FileSystem.readFiles(dir);
            const imageFiles = allFiles
                .filter((file) => this.supportedImageFormats.indexOf(Path.extension(file)) !== -1)
                .sort();

            const out: Image[] = await this.doConcurrently(imageFiles, async (file) =>
            {
                const identity = Image();

                identity.sourcePath = file;

                // Find image size
                /** logo.png PNG 200x200 200x200+0+0 8-bit sRGB 60252B 0.000u 0:00.000 */
                const info = await MagickUtils.run("identify", [file]);
                if (!info) { throw new Error(`Failed to identify ${file}`); }

                const match = info.match(/\s([0-9]+)x([0-9]+)\s/);
                if (!match) { throw new Error(`Could not read size from identification string: "${info}"`); }

                identity.w = parseInt(match[1]);
                identity.h = parseInt(match[2]);

                identity.trimOuterW = identity.w;
                identity.trimOuterH = identity.h;
                identity.cropW = identity.w;
                identity.cropH = identity.h;

                // Identify animation
                this.addImageToAnimationFileMap(animationFileMap, file, dir);

                return identity;
            });

            // Fix frame numbering gaps
            for (const anim in animationFileMap)
            {
                animationFileMap[anim] = animationFileMap[anim].filter((f) => f != null);
            }

            return out;
        }

        /** Ascertains animation and frame number from an image's file name, and updates sheet data accordingly. */
        protected addImageToAnimationFileMap(map: RS.Util.Map<string[]>, imagePath: string, dir: string)
        {
            const relativePath = Path.relative(dir, imagePath);
            const ext = Path.extension(imagePath);
            const parts = Path.split(relativePath);

            let anim: string, frame: number;
            if (parts.length === 1)
            {
                const [fileName] = parts;
                anim = fileName.slice(0, -ext.length);

                // Look for a frame number and exclude it
                const match = anim.match(/[_]*([0-9]+)$/);
                if (match)
                {
                    anim = anim.slice(0, match.index);
                    frame = parseInt(match[1]);
                }
            }
            else
            {
                anim = "";
                for (let i = 0; i < parts.length - 1; i++)
                {
                    if (anim.length > 0) { anim += "."; }
                    anim += parts[i];
                }
            }

            if (anim.length === 0)
            {
                throw new Error(`Could not ascertain animation name from file '${imagePath}'`);
            }

            if (!(anim in map)) { map[anim] = []; }

            if (frame == null)
            {
                map[anim].push(imagePath);
            }
            else
            {
                map[anim][frame] = imagePath;
            }
        }

        /** Pacls the given Images into an array of Packs. */
        protected packImages(images: ReadonlyArray<Image>): Pack<Image>[]
        {
            return pack(images, this._settings.maxSize, this._settings.margin);
        }

        /** Saves the given Pack as an image at the given file path. */
        protected async savePackImage(pack: Pack<Image>, path: string): Promise<void>
        {
            // Start with a clean slate.
            await FileSystem.deletePath(path);

            /*
                convert -size 100x100 xc:skyblue \
                    balloon.gif  -geometry  +5+10  -composite \
                    medical.gif  -geometry +35+30  -composite \
                    present.gif  -geometry +62+50  -composite \
                    shading.gif  -geometry +10+55  -composite \
                    compose.gif
            */

            const args: string[] = [];
            let cmdLength = this.pushArgs(args,
                "-quiet",

                "-size", `${pack.size.w}x${pack.size.h}`,
                "xc:transparent"
            );

            /** Command length at which a batch is ended to allow the path to fit. */
            const batchEndLength = maxCommandLength - path.length;

            // Process first batch together
            if (pack.sizeRefs.length === 0) { throw new Error("Pack empty"); }

            // Debug logging
            if (this._settings.verbose)
            {
                this.logImageStart(pack, path);
                this.logBatchStart();
            }

            let firstBatchSize = pack.sizeRefs.length;
            for (let i = 0; i < pack.sizeRefs.length; i++)
            {
                const image = pack.sizeRefs[i];
                const rect = pack.rects[i];

                // Debug logging
                if (this._settings.verbose)
                {
                    this.logImageComposition(i, image, rect);
                }

                cmdLength += this.pushImageReferenceArgs(args, image, rect);
                cmdLength += this.pushImageCompositionArgs(args, image, rect);

                // End batch
                if (cmdLength >= batchEndLength)
                {
                    firstBatchSize = i + 1;
                    break;
                }
            }

            cmdLength += this.pushArgs(args, path);

            // Debug logging
            if (this._settings.verbose)
            {
                this.logBatchFinish(cmdLength);
            }

            try
            {
                await MagickUtils.run("convert", args);
            }
            catch (err)
            {
                throw new Error(`Failed to save pack image: ${err}`);
            }

            try
            {
                // Process remaining batches
                args.length = 0;
                cmdLength = this.pushArgs(args,
                    path,
                    "-quiet"
                );

                // Debug logging
                if (this._settings.verbose)
                {
                    this.logBatchStart();
                }

                for (let i = firstBatchSize; i < pack.sizeRefs.length; i++)
                {
                    const image = pack.sizeRefs[i];
                    const rect = pack.rects[i];

                    // Debug logging
                    if (this._settings.verbose)
                    {
                        this.logImageComposition(i, image, rect);
                    }

                    cmdLength += this.pushImageReferenceArgs(args, image, rect);
                    cmdLength += this.pushImageCompositionArgs(args, image, rect);

                    // End batch
                    if (cmdLength >= batchEndLength)
                    {
                        // Commit batch to image
                        cmdLength += this.pushArgs(args, path);

                        // Debug logging
                        if (this._settings.verbose)
                        {
                            this.logBatchFinish(cmdLength);
                        }

                        try
                        {
                            await MagickUtils.run("convert", args);
                        }
                        catch (err)
                        {
                            throw new Error(`Failed to save pack image: ${err}`);
                        }

                        // Reset
                        args.length = 0;
                        cmdLength = this.pushArgs(args, path);
                    }
                }

                // Commit final batch to image
                cmdLength += this.pushArgs(args, path);

                // Debug logging
                if (this._settings.verbose)
                {
                    this.logBatchFinish(cmdLength);
                }

                try
                {
                    await MagickUtils.run("convert", args);
                }
                catch (err)
                {
                    throw new Error(`Failed to save pack image: ${err}`);
                }
            }
            catch (err)
            {
                await FileSystem.deletePath(path);
                throw err;
            }

            FileSystem.cache.set(path, { type: FileIO.FileSystemCache.NodeType.File, content: null });

            // Debug logging
            if (!this._settings.quiet)
            {
                console.info(`Saved ${path} (${pack.size.w} x ${pack.size.h})`);
            }
        }

        //#region savePackImage debug logging

        protected logImageStart(pack: Pack<Image>, path: string)
        {
            console.debug(`${Path.baseName(path)}: compositing ${pack.sizeRefs.length} images`);
        }

        protected logBatchFinish(cmdLength: number)
        {
            console.debug(`\tcommand length ${cmdLength}`);
        }

        protected logImageComposition(index: number, image: Image, rect: ReadonlyRectangle)
        {
            console.debug(`\t\t${index}: ${Path.baseName(image.sourcePath)} x${image.scale} +${image.extrude} @${rect.w}x${rect.h}+${rect.x}+${rect.y}`);
        }

        protected logBatchStart()
        {
            console.debug("\tbatch:");
        }

        //#endregion

        /** Pushes args for compositing an image. */
        protected pushImageCompositionArgs(args: string[], image: Image, rect: ReadonlyRectangle): number
        {
            return this.pushArgs(args,
                "-gravity", "northwest",
                "-geometry", `+${rect.x}+${rect.y}`,
                "-composite"
                );
        }

        protected pushImageReferenceArgs(args: string[], image: Image, rect: ReadonlyRectangle): number
        {
            if (!this.hasTransformations(image))
            {
                // No image-specific modifications
                args.push(image.sourcePath);
                return image.sourcePath.length;
            }

            let length = this.pushArgs(args, "(", image.sourcePath);

            // Crop
            if (image.trimOffsetX > 0 || image.trimOffsetY > 0)
            {
                length += this.pushArgs(args, "-crop", `${image.cropW}x${image.cropH}+${image.cropX}+${image.cropY}`);
            }

            // Scale first
            if (image.scale !== 1)
            {
                const extrusionOffset = image.extrude * -2;
                length += this.pushArgs(args, "-resize", `${rect.w + extrusionOffset}x${rect.h + extrusionOffset}`);
            }

            // Extrude after scaling
            if (image.extrude > 0)
            {
                length += this.pushArgs(args,
                    // Expands the viewport
                    "-set", "option:distort:viewport", `${rect.w}x${rect.h}`,
                    // Tells PNGQuant to use the edge pixel colours for the newly added space
                    "-virtual-pixel", "edge",
                    // Offsets the image so it's in the centre rather than top-left
                    `-distort`, `srt`, `0,0 1 0, ${image.extrude},${image.extrude}`
                    );
            }

            length += this.pushArgs(args, ")");
            return length;
        }

        protected hasTransformations(image: Image): boolean
        {
            return image.scale !== 1 || image.extrude > 0 || image.trimOffsetX > 0 || image.trimOffsetY > 0;
        }

        protected pushArgs(arr: string[], ...args: string[]): number
        {
            arr.push(...args);
            return args.reduce((counter, arg) => counter + arg.length, 0);
        }

        protected createEmptySpriteSheet(): Sheet
        {
            return {
                images: [],
                frames: [],
                animations: {},
                events: [],
                framerate: this._settings.framerate,
                meta:
                {
                    version: "Synergy SheetPacker (Red7 Format 2.2)",
                    exported: "",
                    options:
                    {
                        compact: false,
                        sorted: true,
                        fps: this._settings.framerate
                    }
                }
            };
        }

        protected createBlankFrame(): Frame
        {
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
    }

    export namespace Packer
    {
        export interface Settings
        {
            /** Max allowed size */
            maxSize: Size2D;

            /** Output format (with preceding .) e.g. .png */
            outputFormat?: string;

            /** Scale multiplier */
            scale?: number;
            /** Extrude amount */
            extrude?: number;
            /** Margin between pack rects in px (does not add outer margin) */
            margin?: number;

            /** Default pivot x multiplier 0-1. */
            defaultPivotX?: number;
            /** Default pivot y multiplier 0-1. */
            defaultPivotY?: number;

            /** Whether or not to constrain to POT texture sizes */
            constrainPOT?: boolean;

            /** Framerate */
            framerate?: number;

            /** Verbose logging */
            verbose?: boolean;

            /** Disable info logging */
            quiet?: boolean;

            /** Alpha threshold. */
            trimThreshold?: number;

            /** Concurrency limit. */
            concurrency?: number;
        }
    }
}