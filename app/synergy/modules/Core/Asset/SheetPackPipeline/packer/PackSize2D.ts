/// <reference path="Util.ts"/>
namespace RS.SheetPacker
{
    export interface Pack<TSizeRef extends ReadonlySize2D = ReadonlySize2D>
    {
        /** Overall size of the pack. */
        size: Size2D;
        /** Array of rects, the order of which corresponds to the order of those rects passed into the packer. */
        rects: Rectangle[];
        /** Array of references to size objects in the same order as rects. */
        sizeRefs: TSizeRef[];
    }

    // TODO:
    // - Prepare function low utility packs (e.g. one very long rect and one very tall rect) across packs to improve utility?

    /**
     * Packs the given sizes into maxSize.
     * Returns an array of objects containing the resulting rectangles.
     * Space utilisation is probably about on par with TP.
     * Useful for baking multiple objects to a single texture for rendering purposes.
     * @param sizes The width and height of each rect to pack.
     *              For optimal results, sort using Pack.toSortedSizes() or otherwise pass in an array of sizes sorted in ascending width order.
     * @param maxSize The maximum size of the resulting packs.
     * @param strategy The strategy to use to pack.
     */
    export function pack<TSizeRef extends ReadonlySize2D = ReadonlySize2D>(
        sizes: ReadonlyArray<TSizeRef>,
        maxSize: ReadonlySize2D,
        margin = 0,
        strategy: Pack.Strategy = Pack.defaultStrategy): Pack<TSizeRef>[]
    {
        return strategy.pack(sizes, maxSize, margin);
    }

    export namespace Pack
    {
        /** Thrown if a size is greater than the spritesheet max size. */
        export class SizeTooLargeError extends BaseError
        {
            constructor(public readonly maxSize: ReadonlySize2D, public readonly size: ReadonlySize2D)
            {
                super(`Cannot fit ${size.w} x ${size.h} within ${maxSize.w} x ${maxSize.h}`);
            }
        }

        export interface Strategy
        {
            pack<TSizeRef extends ReadonlySize2D>(sizes: ReadonlyArray<TSizeRef>, maxSize: ReadonlySize2D, margin: number): Pack<TSizeRef>[];
            addTo<TSizeRef extends ReadonlySize2D>(packObj: Pack<TSizeRef>, size: TSizeRef, maxSize: ReadonlySize2D, margin: number): number;
        }

        export namespace Strategies
        {
            export class Tetris implements Strategy
            {
                constructor(private readonly sort: boolean) { }

                public static toSortedSizes<TSizeRef extends ReadonlySize2D>(sizes: ReadonlyArray<TSizeRef>)
                {
                    const sortedSizes: TSizeRef[] = [];
                    for (let i = 0, l = sizes.length; i < l; ++i)
                    {
                        const size = sizes[i];
                        Util.insert(size, sortedSizes, (a, b) => a.w > b.w ? 1 : 0);
                    }
                    return sortedSizes;
                }

                public pack<TSizeRef extends ReadonlySize2D>(sizes: ReadonlyArray<TSizeRef>, maxSize: ReadonlySize2D, margin: number): Pack<TSizeRef>[]
                {
                    sizes = Tetris.toSortedSizes(sizes);

                    const packArr: Pack<TSizeRef>[] = [];

                    let packObj: Pack<TSizeRef> = { size: Size2D(), rects: [], sizeRefs: [] };
                    packArr.push(packObj);

                    for (const size of sizes)
                    {
                        const idx = this.addTo(packObj, size, maxSize, margin);
                        if (idx === -1)
                        {
                            packObj = { size: Size2D(), rects: [], sizeRefs: [] };
                            packArr.push(packObj);

                            this.addTo(packObj, size, maxSize, margin);
                        }
                    }

                    return packArr;
                }

                public addTo<TSizeRef extends ReadonlySize2D>(packObj: Pack<TSizeRef>, size: TSizeRef, maxSize: ReadonlySize2D, margin: number): number
                {
                    if (size.w > maxSize.w || size.h > maxSize.h) { throw new SizeTooLargeError(maxSize, size); }
                    const packRect: Rectangle = { x: 0, y: 0, w: size.w, h: size.h };
                    if (packObj.rects.length > 0)
                    {
                        packRect.y = maxSize.h;
                        const maxX = maxSize.w - size.w;
                        for (const rect of packObj.rects)
                        {
                            const l = rect.x;

                            // The rect left bound is too far over to consider.
                            if (l > maxX) { continue; }

                            // Find the upper y limit here.
                            const contactPointL = this.findContactPoint(packObj.rects, l, size.w, margin);
                            if (contactPointL < packRect.y)
                            {
                                packRect.x = l;
                                packRect.y = contactPointL;
                                // We can't get any better than 0
                                if (packRect.y === 0) { break; }
                            }

                            const r = rect.x + rect.w + margin;

                            //  The rect right bound is too far over to consider.
                            if (r > maxX) { continue; }

                            const contactPointR = this.findContactPoint(packObj.rects, r, size.w, margin);
                            if (contactPointR < packRect.y)
                            {
                                packRect.x = r;
                                packRect.y = contactPointR;
                                // We can't get any better than 0
                                if (packRect.y === 0) { break; }
                            }
                        }
                    }

                    const newW = Math.max(packObj.size.w, packRect.x + packRect.w);
                    const newH = Math.max(packObj.size.h, packRect.y + packRect.h);
                    if (newW > maxSize.w || newH > maxSize.h) { return -1; }

                    packObj.size.w = newW;
                    packObj.size.h = newH;
                    packObj.rects.push(packRect);
                    packObj.sizeRefs.push(size);
                    return packObj.rects.length - 1;
                }

                /** Finds the contact height for a line of the given width at the given x. */
                protected findContactPoint(rects: ReadonlyArray<ReadonlyRectangle>, x: number, width: number, margin: number): number
                {
                    const lineRight = x + width;
                    let contactPoint = 0;
                    for (const rect of rects)
                    {
                        // This rect is ahead of us, skip it.
                        if (lineRight <= rect.x) { continue; }
                        const rectRight = rect.x + rect.w;
                        // This rect is behind us, skip it.
                        if (rectRight <= x) { continue; }
                        // This rect might push the contact point down.
                        const bottom = rect.y + rect.h + margin;
                        contactPoint = Math.max(contactPoint, bottom);
                    }
                    return contactPoint;
                }
            }
        }

        export const defaultStrategy = new Strategies.Tetris(true);

        /**
         * Attempts to add a rect of the given size to the given pack, returning the index of the resulting rect in the pack.
         * For best results, add objects to packs in ascending order of width.
         * Returns -1 if it fails to add the rect without expanding the pack to beyond max size.
         */
        export function addTo<TSizeRef extends ReadonlySize2D>(packObj: Pack<TSizeRef>, size: TSizeRef, maxSize: ReadonlySize2D, margin = 0, strategy: Pack.Strategy = Pack.defaultStrategy)
        {
            return strategy.addTo(packObj, size, maxSize, margin);
        }
    }
}