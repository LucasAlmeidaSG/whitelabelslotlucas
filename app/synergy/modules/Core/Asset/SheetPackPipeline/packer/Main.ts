/// <reference path="PackImage.ts" />
namespace RS.SheetPacker
{
    // Example of use
    // node packer.js dir outFile --maxSize=2048x2048 --scale=0.5 --fps=24
    (async function main()
    {
        try
        {
            await FileSystem.rebuildCache();

            const settings: Packer.Settings = { maxSize: { w: 2048, h: 2048 } };

            const maxSize = readArg("maxSize", process.argv);
            if (maxSize == null)
            {
                Log.warn(`--maxSize should be set; falling back to ${settings.maxSize.w}x${settings.maxSize.h}`);
            }
            else
            {
                const [w, h] = readCoords(maxSize);
                settings.maxSize.w = Math.floor(w);
                settings.maxSize.h = Math.floor(h);
            }

            const fps = readArg("fps", process.argv);
            if (fps != null)
            {
                settings.framerate = Read.integer(fps);
            }

            const margin = readArg("margin", process.argv);
            if (margin != null)
            {
                settings.margin = Read.integer(margin);
            }

            const extrude = readArg("extrude", process.argv);
            if (extrude != null)
            {
                settings.extrude = Read.integer(extrude);
            }

            const scale = readArg("scale", process.argv);
            if (scale != null)
            {
                settings.scale = Read.number(scale);
            }

            settings.constrainPOT = argExists("pot", process.argv);

            const format = readArg("format", process.argv);
            if (format)
            {
                settings.outputFormat = `.${format}`;
            }

            const pivot = readArg("pivot", process.argv);
            if (pivot)
            {
                const [x, y] = readCoords(pivot);
                settings.defaultPivotX = x;
                settings.defaultPivotY = y;
            }

            const trimThreshold = readArg("trim-threshold", process.argv);
            if (trimThreshold)
            {
                settings.trimThreshold = Read.integer(trimThreshold);
            }

            const concurrency = readArg("concurrency", process.argv);
            if (concurrency)
            {
                settings.concurrency = Read.integer(concurrency);
            }

            settings.verbose = argExists("verbose", process.argv);
            settings.quiet = argExists("quiet", process.argv);

            const [inputDir, outputPath] = process.argv.slice(2);

            const packer = new Packer(settings);
            await packer.packSheet(inputDir, outputPath);

            if (RS.EnvArgs.profile.value)
            {
                RS.Profile.dump();
            }
        }
        catch (err)
        {
            console.error(`Pack error: ${err}`);
        }
    })();

    /**
     * Parses a string as a two-dimensional vector XxY. \
     * Returns null on failure.
     */
    function readCoords(str: string): [number, number] | null
    {
        // 0.5x0.5
        // 12x12
        // .5x.5
        // 12.x12.
        const coords = str.match(/(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))x(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))/);
        if (coords == null) { return null; }
        const result: [number, number] = [Read.number(coords[1]), Read.number(coords[5])];
        if (result[0] == null || result[1] == null) { return null; }
        return result;
    }

    /** Returns whether or not the named argument exists in the given argv array. */
    function argExists(name: string, argv: string[]): boolean
    {
        const regExp = new RegExp(`^--${RS.Util.escapeRegExpChars(name)}(=(.+))?$`);
        for (const arg of argv)
        {
            const match = arg.match(regExp);
            if (match)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Reads an argument string from an argv array. \
     * NOTE: does not support quoted string values.
     */
    function readArg(name: string, argv: string[]): string
    {
        // --name=value
        const regExp = new RegExp(`^--${RS.Util.escapeRegExpChars(name)}=(.+)$`);
        for (const arg of argv)
        {
            const match = arg.match(regExp);
            if (match)
            {
                return match[1];
            }
        }
        return null;
    }
}