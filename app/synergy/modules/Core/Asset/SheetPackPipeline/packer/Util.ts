// TODO: TEMP
namespace RS.SheetPacker
{
    export interface Size2D { w: number; h: number; }
    export interface Rectangle { x: number; y: number; w: number; h: number; }
    export type ReadonlySize2D = Readonly<Size2D>;
    export type ReadonlyRectangle = Readonly<Rectangle>;
    export function Size2D(w = 0, h = 0) { return { w, h }; }

    export class BaseError
    {
        public get name() { return this.inner.name; }
        public get message() { return this.inner.message; }
        public get stack() { return this.inner.stack; }

        protected readonly inner: Error;

        constructor(message?: string)
        {
            this.inner = new Error(message);
        }

        public toString(): string
        {
            return this.inner.toString();
        }

        public valueOf(): object
        {
            return this.inner.valueOf();
        }
    }

    export function nextPowerOf2(v: number): number
    {
        return Math.pow(2, Math.ceil(Math.log(v) / Math.log(2)));
    }

    export namespace Util
    {
        function defaultComparatorFunc<T>(candidate: T, item: T): number
        {
            return item < candidate ? -1 : 0;
        }
        export function insert<T>(item: T, array: T[], comparator: (candidate: T, item: T, index: number) => number = defaultComparatorFunc): number
        {
            for (let i = 0; i < array.length; i++)
            {
                const comp = comparator(array[i], item, i);
                if (comp)
                {
                    const insertAt = comp > 0 ? i + comp : i + comp + 1;
                    array.splice(insertAt, 0, item);
                    return insertAt;
                }
            }

            return array.push(item) - 1;
        }
    }
}