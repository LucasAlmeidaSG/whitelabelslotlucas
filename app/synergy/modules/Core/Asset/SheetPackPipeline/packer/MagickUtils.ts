namespace RS.MagickUtils
{
    /** Returns whether or not we should use legacy mode i.e. with ImageMagick 6 support, before they put everything under the 'magick' command. */
    async function shouldUseLegacyMode()
    {
        if (await Command.exists("magick"))
        {
            return false;
        }
        else
        {
            const versionInfo = await Command.run("convert", ["-version"], null, false);
            if (versionInfo.indexOf("ImageMagick") === -1)
            {
                // It's probably not ImageMagick
                throw new Error("ImageMagick not found (ENOENT)");
            }
            return true;
        }
    }

    let imageMagickLegacy: boolean = null;
    /** Runs an ImageMagick program with ImageMagick <7 compatibility. */
    export async function run(program: string, args: string[]): Promise<string>
    {
        if (imageMagickLegacy == null) { imageMagickLegacy = await shouldUseLegacyMode(); }

        if (imageMagickLegacy)
        {
            return await Command.run(program, args, null, false);
        }
        else
        {
            return await Command.run("magick", [program, ...args], null, false);
        }
    }
}