namespace RS.AssetPipeline.SheetUtils
{
    export enum TrimModes
    {
        /** Keep transparent pixels. */
        None = "None",
        /** Remove transparent pixels, use original size. */
        Trim = "Trim",
        /** Remove transparent pixels, use trimmed size, flush position. */
        Crop = "Crop",
        /** Remove transparent pixels, use trimmed size, keep position. */
        CropKeepPos = "CropKeepPos",
        /** Approximate sprite contour with polygon path. */
        Polygon = "Polygon"
    }

    export enum Algorithms
    {
        /** Powerful packing algorithm (extended). */
        MaxRects = "MaxRects",
        /** Places sprites on regular grid, the largest sprite determines the cell size. */
        Grid = "Grid",
        /** Simple algorithm for tilemaps and atlases (free). */
        Basic = "Basic",
        /** Tight polygon packing. */
        Polygon = "Polygon"
    }

    export enum SizeConstraints
    {
        None = "AnySize",
        PowerOfTwo = "POT",
        WordAligned = "WordAligned"
    }

    export enum PackModes
    {
        Fast = "Fast",
        Good = "Good",
        Best = "Best"
    }

    export enum RotationModes
    {
        Default,
        ForceDisabled,
        ForceEnabled
    }

    export enum ScaleModes
    {
        /** Smooth scaling. */
        Smooth = "Smooth",
        /** Pixelated scaling. */
        NearestNeighbour = "Fast",
        /** Fixed 2x upscaling. */
        Upscale2x = "Scale2x",
        /** Fixed 3x upscaling. */
        Upscale3x = "Scale3x",
        /** Fixed 4x upscaling. */
        Upscale4x = "Scale4x",
        /** Fixed 2x upscaling. Eagle scaling. */
        Eagle2x = "Eagle",
        /** Fixed 2x upscaling. Hq scaling. */
        Hq2x = "Hq2x"
    }

    export enum AlphaHandlingModes
    {
        /** Transparent pixels are copied from sprite to sheet without any change. */
        KeepTransparentPixels = "KeepTransparentPixels",
        /** Color values of transparent pixels are set to 0 (transparent black). */
        ClearTransparentPixels = "ClearTransparentPixels",
        /** Transparent pixels get color of nearest solid pixel. */
        ReduceBorderArtifacts = "ReduceBorderArtifacts",
        /** All color values are multiplied with the alpha values. */
        PremultiplyAlpha = "PremultiplyAlpha"
    }

    export interface Variant
    {
        name: string;
        scale: number;
        filter?: string;
        allowFraction?: boolean;
        size?: [number, number];
    }

    export interface PackOptions
    {
        /** Format to write, default is red7. */
        format?: string;
        /** Create multiple sprite sheets if not all sprites match into a single one. Default is true. */
        multipack?: boolean;
        /** The texture file format. Defaults to "png". */
        textureFormat?: string;

        /** Scaled variant of the sheet. */
        variants?: Variant[];
        /**
         * Preserves the layout across multiple scaling variants.
         * Might require enabling allowfraction on some variants if no common
         * base factor can be derived.
         */
        forceIdenticalLayout?: boolean;

        /** Set dpi for output image. Default is 72. */
        dpi?: number;
        /** Defines how color values of transparent pixels are processed. */
        alphaHandling?: AlphaHandlingModes;
        /** Optimization level for pngs (0=off, 1=use 8-bit, 2..7=png-opt). */
        pngOptLevel?: number;
        /** Sets the quality for jpg export: -1 for default, 0..100 where 0 is low quality. */
        jpgQuality?: number;
        /** Quality level for WebP format (0=low, 100=high, >100=lossless). Default is lossless. */
        webpQuality?: number;

        /** If true, forces publish even if no changes are expected. */
        force?: boolean;

        /** Choose algorithm. */
        algorithm?: Algorithms;

        /** Sets fixed width for texture. */
        width?: number;
        /** Sets fixed height for texture. */
        height?: number;
        /** Sets the maximum width for the texture in auto size mode. Default is 2048. */
        maxWidth?: number;
        /** Sets the maximum height for the texture in auto size mode. Default is 2048. */
        maxHeight?: number;
        /** Sets the maximum width and height for the texture in auto size mode. Default is 2048. */
        maxSize?: number;
        /** Restrict sizes. */
        sizeConstraints?: SizeConstraints;
        /** Force squared texture. */
        forceSquared?: boolean;
        /** Optimization mode. */
        packMode?: PackModes;

        /** Sets default pivot point used for sprites, in relative co-ordinates. Default is (0.5, 0.5). */
        defaultPivotPoint?: [number, number];

        /** Sets a padding around each shape, in pixels. */
        shapePadding?: number;
        /** Sets a padding around the sheet, in pixels. */
        borderPadding?: number;
        /** Sets a padding around each shape and the sheet, in pixels. */
        padding?: number;
        /** Defines whether or not to allow rotation to achieve optimal packing. */
        rotationMode?: RotationModes;

        /** Remove transparent parts of a sprite to shrink atlas size and speed up rendering. Default is Crop. */
        trimMode?: TrimModes;
        /** Trim alpha values under the threshold value 1..255. Default is 1. */
        trimThreshold?: number;
        /** Transparent margin which is left over after trimming. */
        trimMargin?: number;
        /** Deviation of the polygon approximation from the exact sprite outline. Default is 200. */
        tracerTolerance?: number;

        /** Extrudes the sprites by given value of pixels to fix flickering problems in tile maps. */
        extrude?: number;
        /** Scales all images before creating the sheet. E.g. use 0.5 for half size. */
        scale?: number;
        /** Scale mode. Defaults to Smooth. */
        scaleMode?: ScaleModes;

        /** Whether or not to print the command string. */
        commandDebug?: boolean;
        /** Be verbose. */
        verbose?: boolean;
        /** No output except for errors. */
        quiet?: boolean;
        /** Creates boxes around shapes for debugging. */
        shapeDebug?: boolean;

        /** Frame-rate in frames per second. */
        frameRate?: number;
    }

    export const defaultOptions: PackOptions =
    {
        format: "red7",
        textureFormat: "png",
        trimMode: TrimModes.Trim,
        algorithm: Algorithms.MaxRects,
        multipack: true,
        sizeConstraints: SizeConstraints.PowerOfTwo,
        rotationMode: RotationModes.ForceDisabled,
        defaultPivotPoint: [0.5, 0.5],
        scaleMode: ScaleModes.Smooth,
        pngOptLevel: 0,
        maxWidth: 2048,
        maxHeight: 2048,
        frameRate: 24
    };

    function pushArg(args: string[], name: string, value: any, separator?: string)
    {
        if (value == null) { return; }

        if (Is.boolean(value))
        {
            if (value) { args.push(`--${name}`); }
            return;
        }

        if (separator == null)
        {
            args.push(`--${name}`, `${value}`);
        }
        else
        {
            args.push(`--${name}${separator}${value}`);
        }
    }

    function pushArgMap(args: string[], map: { [name: string]: any }, separator?: string)
    {
        for (const key in map)
        {
            pushArg(args, key, map[key], separator);
        }
    }

    /** Env-arg to force bespoke packer on. */
    const debugBespokePacker = new BooleanEnvArg("BESPOKEPACK", false);
    const __module = "Core.Asset.SheetPackPipeline";

    let isTexturePackerAvailableCache: boolean = null;
    const texturePackerCommand = "TexturePacker";
    const warningMsg = "fallback packer will be used; assets may not be optimally packed";

    async function checkTexturePackerLicense(): Promise<boolean>
    {
        /*
            Expired trial licence-info output:
            License
            -------

            Username:           essential user
            License type:       essential
            Key:
            Expiry:             none
            Free updates until:

            Single licence-info output:
            License
            -------
            Username:           user@email.com
            License type:       single
            Key:                TP-ASDF-1G2H-????-????
            Expiry:             none
            Free updates until: YYYY-MM-DD

            Company licence-info output:
            License
            -------
            Username:           user@email.com
            License type:       volume
            Key:                TP-ASDF-1G2H-????-????
            Expiry:             none
            Free updates until: YYYY-MM-DD
        */

        let licenceInfo: string;
        try
        {
            licenceInfo = await Command.run(texturePackerCommand, ["--license-info"], null, false);
        }
        catch (err)
        {
            Log.warn(`Failed to ascertain TexturePacker licence information: ${err}`);
            return false;
        }

        const licenceTypeMatch = licenceInfo.match(/License type:\s*([^\s]+)/);
        if (!licenceTypeMatch)
        {
            Log.warn("Failed to ascertain TexturePacker licence information; falling back to simple packer");
            Log.warn("If you see this message, please get in touch with @Derek Choi on Slack");
            return false;
        }

        if (licenceTypeMatch[1] === "essential")
        {
            Log.warn(`TexturePacker is not licensed, ${warningMsg}`);
            return false;
        }

        return true;
    }

    async function checkTexturePackerExporter(): Promise<boolean>
    {
        /*
            -- if exporter doesn't exist:
            TexturePacker:: error: Available Formats:
            TexturePacker:: error:     - 2dtoolkit
            TexturePacker:: error:     - amethyst
            ... (lists all available formats)
            TexturePacker:: error: Unknown format: red7

            -- if exporter exists:
            cli help +
            TexturePacker:: error: No files specified for processing.
         */

        // this always errors out
        try
        {
            await Command.run(texturePackerCommand, ["--format", "red7"], undefined, false);
        }
        catch (err)
        {
            return err.indexOf("TexturePacker:: error: Unknown format: red7") === -1;
        }

        //should never reach here
        return false;
    }

    export function isTexturePackerAvailable(): PromiseLike<boolean>;
    export async function isTexturePackerAvailable()
    {
        if (isTexturePackerAvailableCache != null) { return isTexturePackerAvailableCache; }

        if (debugBespokePacker.value)
        {
            Log.warn(`BESPOKEPACK is true, ${warningMsg}`);
            return (isTexturePackerAvailableCache = false);
        }

        if (!await Command.exists(texturePackerCommand))
        {
            Log.warn(`TexturePacker is not available, ${warningMsg}`);
            return (isTexturePackerAvailableCache = false);
        }

        if (!await checkTexturePackerLicense())
        {
            return (isTexturePackerAvailableCache = false);
        }

        if (!await checkTexturePackerExporter())
        {
            Log.warn(`Red7 exporter not available, please obtain from the downloads section of the Synergy repo. ${warningMsg}`);
            return (isTexturePackerAvailableCache = false);
        }

        return (isTexturePackerAvailableCache = true);
    }

    /** Packs a given folder, image file, array of image files or TPS file into a spritesheet. Requires TexturePacker. */
    export async function pack(src: string | ReadonlyArray<string>, dest: string, options?: Readonly<PackOptions>): Promise<string>
    {
        options = options ? { ...defaultOptions, ...options } : defaultOptions;
        dest = Path.replaceExtension(dest, ".json");

        if (await isTexturePackerAvailable())
        {
            return await packTexturePacker(src, dest, options);
        }
        else
        {
            return await packBespoke(src, dest, options);
        }
    }

    function packBespoke(src: string | ReadonlyArray<string>, dest: string, options: Readonly<PackOptions>): PromiseLike<string>
    {
        const args: string[] = [];

        const packerModule = Module.getModule(__module);
        const packerPath = Path.combine(packerModule.path, "build", "packer.js");
        args.push(packerPath);

        if (!Is.string(src)) { throw new Error("Array src not supported yet"); }
        args.push(src);
        args.push(dest);

        const argMap: { [name: string]: any } =
        {
            "scale": options.scale,
            "maxSize": `${options.maxWidth}x${options.maxHeight}`,
            "pot": options.sizeConstraints === SizeConstraints.PowerOfTwo,
            "format": options.textureFormat,
            "pivot": options.defaultPivotPoint && `${options.defaultPivotPoint[0]}x${options.defaultPivotPoint[1]}`,
            "fps": options.frameRate,
            "extrude": options.extrude,

            "verbose": options.verbose,
            "quiet": options.quiet
        };
        pushArgMap(args, argMap, "=");

        if (options.commandDebug)
        {
            Log.debug(`node ${args.join(" ")}`);
        }

        return Command.run("node", args);
    }

    function packTexturePacker(src: string | ReadonlyArray<string>, dest: string, options: Readonly<PackOptions>): PromiseLike<string>
    {
        const args: string[] = [];

        if (Is.string(src))
        {
            args.push(src);
        }
        else
        {
            args.push(...src);
        }

        const destDataFile = Path.replaceExtension(dest, ".json");

        const destImageFile = options.multipack
            ? Path.replaceExtension(dest, `_{n}.${options.textureFormat}`)
            : Path.replaceExtension(dest, `.${options.textureFormat}`);

        const argMap: { [name: string]: any } =
        {
            "data": destDataFile,
            "sheet": destImageFile,

            "force-publish": options.force,

            "format": options.format,
            "multipack": options.multipack,
            // "texture-format": options.textureFormat,

            "dpi": options.dpi,
            "alpha-handling": options.alphaHandling,
            "png-opt-level": options.pngOptLevel,
            "jpg-quality": options.jpgQuality,
            "webp-quality": options.webpQuality,

            "algorithm": options.algorithm,

            "force-identical-layout": options.forceIdenticalLayout,

            "width": options.width,
            "height": options.height,
            "max-width": options.maxWidth,
            "max-height": options.maxHeight,
            "max-size": options.maxSize,
            "size-constraints": options.sizeConstraints,
            "force-squared": options.forceSquared,
            "pack-mode": options.packMode,

            "default-pivot-point": options.defaultPivotPoint,

            "padding": options.padding,
            "shape-padding": options.shapePadding,
            "border-padding": options.borderPadding,

            "trim-mode": options.trimMode,
            "trim-threshold": options.trimThreshold,
            "trim-margin": options.trimMargin,
            "tracer-tolerance": options.tracerTolerance,

            "extrude": options.extrude,
            "scale": options.scale,
            "scale-mode": options.scaleMode,

            "verbose": options.verbose,
            "quiet": options.quiet,
            "shape-debug": options.shapeDebug,

            // TODO get framerate working for red7 exporter
            // "easeljs-framerate": options.frameRate
        };
        pushArgMap(args, argMap);

        if (options.variants)
        {
            for (const variant of options.variants)
            {
                let str = `${variant.scale}:${variant.name}`;
                if (variant.filter != null) { str += `:${variant.filter}`; }
                if (variant.allowFraction) { str += ":allowfraction"; }
                if (variant.size) { str += `:${variant.size[0]}:${variant.size[1]}`; }
                pushArg(args, "variant", str);
            }
        }

        switch (options.rotationMode)
        {
            case RotationModes.ForceDisabled:
                pushArg(args, "disable-rotation", true);
                break;

            case RotationModes.ForceEnabled:
                pushArg(args, "enable-rotation", true);
                break;
        }

        if (options.commandDebug)
        {
            Log.debug(`${texturePackerCommand} ${args.join(" ")}`);
        }

        return Command.run(texturePackerCommand, args);
    }

    /** Array of image formats that can be used as part of a spritesheet. Source: https://www.codeandweb.com/texturepacker */
    export const imageFormats: ReadonlyArray<string> = [".psd", ".swf", ".png", ".tga", ".jpg", ".jpeg", ".tiff", ".bmp"];

    /** Returns a promise for an array of files at path that can be used as part of a spritesheet. */
    export function readViableImageFiles(path: string): PromiseLike<string[]>;
    export async function readViableImageFiles(path: string): Promise<string[]>
    {
        const allFiles = await FileSystem.readFiles(path);
        return allFiles.filter((file) => imageFormats.indexOf(Path.extension(file)) !== -1);
    }

    /**
     * Returns a promise for an array of animation names from the given path.
     *
     * NOTE: the array order may not be stable across calls. Manually sort the array if a fixed order is required.
     */
    export function resolveAnimations(path: string, out?: string[]): PromiseLike<string[]>;
    export async function resolveAnimations(path: string, out?: string[]): Promise<string[]>
    {
        const directory = await FileSystem.isFolder(path) ? path : Path.directoryName(path);
        const files = await readViableImageFiles(directory);

        const result = out || [];

        for (const file of files)
        {
            const relativePath = Path.relative(directory, file);
            const parts = Path.split(relativePath);

            let anim: string;
            if (parts.length === 1)
            {
                const [fileName] = parts;
                const ext = Path.extension(fileName);
                anim = fileName.slice(0, -ext.length);

                // Look for a frame number and exclude it
                const match = anim.match(/[_]*[0-9]+$/);
                if (match)
                {
                    anim = anim.slice(0, match.index);
                }
            }
            else
            {
                anim = "";
                for (let i = 0; i < parts.length - 1; i++)
                {
                    if (anim.length > 0) { anim += "."; }
                    anim += parts[i];
                }
            }

            if (anim.length === 0)
            {
                Log.warn(`Could not ascertain animation name from file '${file}'`);
                continue;
            }

            if (result.indexOf(anim) === -1)
            {
                result.push(anim);
            }
        }

        return result;
    }
}