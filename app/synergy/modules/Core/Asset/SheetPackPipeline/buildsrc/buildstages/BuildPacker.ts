namespace RS.AssetPipeline.BuildStages
{
    export abstract class AddCompilationUnit extends BuildStage.Base
    {
        public get name() { return this.unitName; }

        constructor(public readonly unitName: string, public readonly dirName: string)
        {
            super();
        }

        public async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            if (!await module.hasCode(this.dirName)) { return { workDone: false, errorCount: 0 }}
            const unit = module.addCompilationUnit(this.unitName, this.dirName);
            this.setupUnit(unit);
            return { workDone: true, errorCount: 0 };
        }

        protected abstract setupUnit(unit: Build.CompilationUnit): void;
    }

    export class AddPackerUnit extends AddCompilationUnit
    {
        protected setupUnit(unit: Build.CompilationUnit)
        {
            Module.CompilationDependencies.addGulp(unit, unit.module);
        }
    }

    const __module = "Core.Asset.SheetPackPipeline";
    const checksumKey = "packer";

    export class AssemblePacker extends BuildStage.Base
    {
        private _codeSources: Assembler.CodeSource[];

        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            this._codeSources = [await this.getGulplibCodeSource()];

            const result = await super.execute(moduleList);
            if (result.errorCount > 0 || !result.workDone) { return result; }

            const cacheModule = Module.getModule(__module);
            const checksum = Checksum.getSimple(this._codeSources);
            if (!cacheModule.checksumDB.diff(checksumKey, checksum)) { return { workDone: false, errorCount: 0 }; }
            cacheModule.checksumDB.set(checksumKey, checksum);

            try
            {
                const path = Path.combine(cacheModule.path, "build");
                await Assembler.assembleJS(path, "packer", this._codeSources, { withHelpers: true, withSourcemaps: false, minify: false });
            }
            catch (err)
            {
                Log.error(`Failed to assemble packer: ${err}`);
                return { workDone: true, errorCount: 1 };
            }

            await cacheModule.saveChecksums();
            return { workDone: true, errorCount: 0 };
        }

        public async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const unit = module.getCompilationUnit("sheetpacker");
            if (!unit) { return { workDone: false, errorCount: 0 }; }

            const codeSource: Assembler.CodeSource =
            {
                name: unit.name,
                content: "",
                sourceMap: "",
                sourceMapDir: ""
            };

            codeSource.content = await FileSystem.readFile(unit.outJSFile);
            codeSource.sourceMap = await FileSystem.readFile(unit.outMapFile);
            codeSource.sourceMapDir = Path.combine(module.path, "build");

            this._codeSources.push(codeSource);
            return { workDone: true, errorCount: 0 };
        }

        protected async getGulplibCodeSource(): Promise<Assembler.CodeSource>
        {
            const codeSource: Assembler.CodeSource =
            {
                name: "gulplib",
                content: "",
                sourceMap: "",
                sourceMapDir: ""
            };

            codeSource.content = await FileSystem.readFile(__gulplib);
            codeSource.sourceMap = await FileSystem.readFile(Path.replaceExtension(__gulplib, ".js.map"));
            codeSource.sourceMapDir = Path.directoryName(__gulplib);

            return codeSource;
        }
    }

    const packerUnit = "sheetpacker";
    const packerDir = "packer";

    export const addPackerUnit = new AddPackerUnit(packerUnit, packerDir);
    export const buildPacker = new RS.BuildStages.Compile(packerUnit);
    export const assemblePacker = new AssemblePacker();

    BuildStage.register({}, addPackerUnit);
    BuildStage.register({ after: [addPackerUnit] }, buildPacker);
    BuildStage.register({ after: [buildPacker] }, assemblePacker);
}