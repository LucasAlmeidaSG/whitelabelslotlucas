namespace RS.AssetPipeline
{
    /**
     * Image asset specific definition data.
     */
    export interface TexturePackDefinition extends ImageDefinition
    {
        multipack?: boolean;

        packMode?: SheetUtils.PackModes;
        algorithm?: SheetUtils.Algorithms;

        pivotX?: number;
        pivotY?: number;

        extrude?: number;
        scaleMode?: SheetUtils.ScaleModes;

        trimMode?: SheetUtils.TrimModes;
        trimMargin?: number;
        tracerTolerance?: number;

        maxWidth?: number;
        maxHeight?: number;
        maxSize?: number;
        sizeConstraints?: SheetUtils.SizeConstraints;

        width?: number;
        height?: number;

        borderPadding?: number;
        shapePadding?: number;
        padding?: number;

        /** Frame-rate in frames per second. */
        frameRate?: number;
    }

    const debug = new RS.BooleanEnvArg("PACKDEBUG", false);

    /**
     * Represents an asset.
     */
    @AssetType("texturepack")
    @RequireCommands([ ImageRequirements.texturePacker, ImageRequirements.imageMagick, ImageRequirements.pngQuant, ImageRequirements.gzip ])
    @InferFromExtensions([".tps"])
    export class TexturePack extends Image
    {
        private _spriteSheetData: RSSpriteSheetData | null = null;
        private _animations: string[] = [];

        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        public get classModule() { return "Core.Asset.SheetPackPipeline"; }

        public async load()
        {
            await super.load();

            // Ascertain animation names for code generation
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = this._definition.variants[variantName];
                    if (!Is.string(variant.path)) { throw new Error("Expected single-type assetPath"); }
                    const fullPath = this.getFullPath(variant.path);
                    await SheetUtils.resolveAnimations(fullPath, this._animations);
                }
            }
            else
            {
                if (!Is.string(this._definition.path)) { throw new Error("Expected single-type assetPath"); }
                const fullPath = this.getFullPath(this._definition.path);
                await SheetUtils.resolveAnimations(fullPath, this._animations);
            }

            // Sort animations lexicographically so we don't rebuild every time.
            this._animations.sort();

            if (debug.value)
            {
                Log.debug(`Animations found for ${this._definition.id}: ${this._animations}`, this.id);
            }
        }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition | null
        {
            const baseReference = AssetPipeline.emitAssetReference(this, "RS.Asset.RSSpriteSheetReference", "rsspritesheet");
            for (const animName of this._animations)
            {
                const cleanedName = AssetPipeline.cleanName(animName);
                if (cleanedName === "") { continue; }
                addAssetReferenceProperty(baseReference, cleanedName, `"${animName}"`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "string" });
            }
            return baseReference;
        }

        /**
         * Given the specified asset as referred to by the definition, finds all source files to be used for checksum.
         * @param path
         * @param list
         */
        protected async gatherChecksumPaths(path: string, list: List<string>)
        {
            const isFolder = await FileSystem.isFolder(path);
            if (isFolder)
            {
                const imagePaths = await SheetUtils.readViableImageFiles(path);
                list.addRange(imagePaths);
            }
            else
            {
                // We could read fileList from the tps file, but it's easier and safer to just assume everything in this folder is part of it for checksum purposes
                const imagePaths = await SheetUtils.readViableImageFiles(Path.directoryName(path));
                list.addRange(imagePaths);
                list.add(path);
            }
        }

        protected resolvePackOptions(variant: TexturePackDefinition): SheetUtils.PackOptions
        {
            const options: SheetUtils.PackOptions = { scale: 1 };

            if (variant.res != null) { options.scale *= variant.res; }
            if (variant.scale != null) { options.scale *= variant.scale; }

            if (variant.multipack != null) { options.multipack = variant.multipack; }

            if (variant.packMode != null) { options.packMode = variant.packMode; }
            if (variant.algorithm != null) { options.algorithm = variant.algorithm; }

            options.defaultPivotPoint = [0.5, 0.5];
            if (variant.pivotX != null) { options.defaultPivotPoint[0] = variant.pivotX; }
            if (variant.pivotY != null) { options.defaultPivotPoint[1] = variant.pivotY; }

            if (variant.extrude != null) { options.extrude = variant.extrude; }
            if (variant.scaleMode != null) { options.scaleMode = variant.scaleMode; }

            if (variant.trimMode != null) { options.trimMode = variant.trimMode; }
            if (variant.trimThreshold != null) { options.trimThreshold = variant.trimThreshold; }
            if (variant.trimMargin != null) { options.trimMargin = variant.trimMargin; }
            if (variant.tracerTolerance != null) { options.tracerTolerance = variant.tracerTolerance; }

            if (variant.maxWidth != null) { options.maxWidth = variant.maxWidth; }
            if (variant.maxHeight != null) { options.maxHeight = variant.maxHeight; }
            if (variant.maxSize != null) { options.maxSize = variant.maxSize; }
            if (variant.sizeConstraints != null) { options.sizeConstraints = variant.sizeConstraints; }

            if (variant.width != null) { options.width = variant.width; }
            if (variant.height != null) { options.height = variant.height; }

            if (variant.borderPadding != null) { options.borderPadding = variant.borderPadding; }
            if (variant.shapePadding != null) { options.shapePadding = variant.shapePadding; }
            if (variant.padding != null) { options.padding = variant.padding; }

            return options;
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         */
        protected async processVariant(outputDir: string, assetPath: string, variant: TexturePackDefinition, dry: boolean): Promise<ManifestEntry | null>
        {
            // If we have a folder, pack to variant names in the folder
            // If we have a TPS file, pack the tps file to the normal asset directory with a name matching the TPS file.

            const isFolder = await FileSystem.isFolder(assetPath);

            const assetBaseName = Path.sanitise(Path.baseName(assetPath));
            const assetBuildPath = isFolder
                // If we have a folder, pack to folder/folder.json
                ? Path.combine(outputDir, assetBaseName, assetBaseName)
                // If we have a TPS file, pack the tps file to the normal asset directory with a name matching the TPS file.
                : Path.combine(outputDir, assetBaseName);

            if (isFolder)
            {
                variant.path = Path.combine(variant.path as string, assetBaseName);
            }

            if (!dry)
            {
                const options = this.resolvePackOptions(variant);
                await SheetUtils.pack(assetPath, assetBuildPath,
                {
                    ...options,

                    force: true,

                    commandDebug: debug.value,
                    verbose: debug.value,
                    quiet: !debug.value
                });

                // Update JSON file in cache
                const jsonBuildPath = Path.replaceExtension(assetBuildPath, ".json");
                FileSystem.cache.set(jsonBuildPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });

                // Load the sprite sheet data
                this._spriteSheetData = await JSON.parseFile(jsonBuildPath);

                // Update image files in cache
                for (let i = 0; i < this._spriteSheetData.images.length; i++)
                {
                    const image = this._spriteSheetData.images[i];

                    const imagePath = Path.combine(Path.directoryName(jsonBuildPath), image);
                    FileSystem.cache.set(imagePath, { type: FileIO.FileSystemCache.NodeType.File, content: null });

                    this._spriteSheetData.images[i] = Path.replaceExtension(Path.sanitise(image), "");
                }

                // Write out the json
                await FileSystem.writeFile(jsonBuildPath, JSON.stringify(this._spriteSheetData));
            }
            else
            {
                // Load the sprite sheet data
                const jsonPath = Path.replaceExtension(assetBuildPath, ".json");
                this._spriteSheetData = await JSON.parseFile(jsonPath);
            }

            // Process formats
            const baseResult = await super.processVariant(outputDir, assetPath, variant, dry);
            if (baseResult == null) { return null; }

            const mipmap = as(variant.mipmap, Is.boolean);
            if (mipmap != null) { (baseResult.variantData as TexturePackDefinition).mipmap = mipmap; }

            const variantPath = isFolder
                ? Path.replaceExtension(Path.combine(Path.sanitise(assetPath), assetBaseName), ".json")
                : Path.replaceExtension(Path.sanitise(assetPath), ".json");
            baseResult.paths.unshift(variantPath);

            baseResult.type = "rsspritesheet";

            return baseResult;
        }

        /**
         * Processes a single format for a single asset variant.
         * Note that the format might be passed as null here if no format is specified.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string, variant: ImageDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            const shouldGzip = this.shouldGzip(format);

            const isFolder = await FileSystem.isFolder(assetPath);
            const assetBaseName = Path.sanitise(Path.baseName(assetPath));
            const assetBuildDir = isFolder
                // If we have a folder, pack to folder/folder.json
                ? Path.combine(outputDir, assetBaseName)
                // If we have a TPS file, pack the tps file to the normal asset directory with a name matching the TPS file.
                : Path.combine(outputDir);

            if (!dry)
            {
                // Process each image as a normal image
                const processPromises: PromiseLike<any>[] = [];
                for (let i = 0, l = this._spriteSheetData.images.length; i < l; ++i)
                {
                    const imageName = this._spriteSheetData.images[i];
                    const srcPath = Path.replaceExtension(Path.combine(assetBuildDir, Path.baseName(imageName)), `.${SheetUtils.defaultOptions.textureFormat}`);
                    const process = this.processSpriteSheetImage(srcPath, variant, format, shouldGzip);
                    processPromises.push(process);
                }

                const results = await Promise.all(processPromises);
                if (results.some((r) => !r)) { return null; }
            }
            const paths = this._spriteSheetData.images.map((path) => Path.replaceExtension(Path.sanitise(path), `.${format}`));
            if (shouldGzip)
            {
                return { paths: [...paths, ...paths.map((p) => Path.replaceExtension(p, `${Path.extension(p)}.gz`))] };
            }
            else
            {
                return { paths };
            }
        }

        protected async processSpriteSheetImage(srcPath: string, variant: ImageDefinition, format: string, shouldGzip: boolean): Promise<boolean>
        {
            const tmpPath = Path.replaceExtension(srcPath, `_tmp.${format}`);

            const oldRes = variant.res;
            const oldScale = variant.scale;
            const oldTrimThreshold = variant.trimThreshold;
            variant.scale = 1.0;
            variant.res = 1.0;
            variant.trimThreshold = 0;

            try
            {
                if (!await this.processSpecificFormat(srcPath, tmpPath, format, variant)) { return false; }
                await FileSystem.copyFile(tmpPath, srcPath);
            }
            catch (err)
            {
                return false;
            }
            finally
            {
                variant.res = oldRes;
                variant.scale = oldScale;
                variant.trimThreshold = oldTrimThreshold;

                await FileSystem.deletePath(tmpPath);
            }

            if (shouldGzip)
            {
                // Produce gz version
                await this.gzip(srcPath);
            }

            return true;
        }
    }
}
