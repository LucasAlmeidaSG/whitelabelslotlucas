namespace RS.AssetPipeline
{
    /**
     * Video asset specific definition data.
     */
    export interface VideoDefinition extends BaseVariant
    {
        /** Configuration for the video's sound channels. */
        sound?: VideoSoundDefinition;
        /** Whether to quantize the posters (compress to 8-bit). Defaults to true. */
        usequant?: boolean;
        /** Whether or not to load and use start/end posters for the video. Defaults to true. */
        usePosters?: boolean;
        /** Whether to extract audio from the video or not. Defaults to true. */
        extractAudio?: boolean;
    }

    export interface VideoSoundDefinition
    {
        /** The formats to export this asset's sound channels to. */
        formats?: ReadonlyArray<string>;
    }

    export interface VideoManifestEntry extends ManifestEntry
    {
        variantData: VideoVariantData;
    }
    export interface VideoVariantData extends BaseVariant
    {
        sound:
        {
            formats: ReadonlyArray<string>;
        };
        duration: number;
        framerate: number;
    }

    export interface ModuleConfig
    {
        strategy?: "ffmpeg" | "magick";
    }

    /**
     * Represents an image-type asset.
     */
    @AssetType("video")
    @InferFromExtensions([".mp4", ".webm", ".mov", ".avi", ".mpeg"])
    @RequireCommands([
        { command: "ffmpeg", error: "Could not find command 'ffmpeg'. Please check that ffmpeg is installed correctly." },
        { command: "ffprobe", error: "Could not find command 'ffprobe'. Please check that ffmpeg is installed correctly." },
        { command: "pngquant", error: "Could not find command 'pngquant'. Please check that PNG Quant is installed correctly." }
    ])
    export class Video extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the video channel, if none are specified. */
        public get defaultFormats() { return ["mp4"]; }

        /** Gets the default formats for the sound channels, if none are specified. */
        public get defaultAudioFormats() { return ["mp3", "m4a"]; }

        public get classModule() { return "Core.Asset.VideoPipeline"; }

        protected get moduleConfig(): ModuleConfig { return (RS.Config.activeConfig.moduleConfigs[this.classModule] || {}) as ModuleConfig; }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition | null
        {
            return AssetPipeline.emitSimpleAssetReference(this, "RS.Asset.VideoReference");
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         */
        protected async processVariant(outputDir: string, assetPath: string | Util.Map<string>, variant: VideoDefinition, dry: boolean): Promise<VideoManifestEntry | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            const supportedFormat = "mp4";
            if (variant.formats.length != 1 || variant.formats[0] !== supportedFormat)
            {
                Log.error(`Different formats for video asset not yet supported`);
                return null;
            }

            // TODO move to processFormat
            const format = supportedFormat;

            const outputBaseName = Path.sanitise(Path.baseName(assetPath));
            const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath))), `.${format}`);

            const startPosterPath = Path.replaceExtension(outputPath, "_poster_start.png");
            const endPosterPath = Path.replaceExtension(outputPath, "_poster_end.png");

            const statsPath = Path.replaceExtension(outputPath, "_stats.json");

            const audioFormats = (variant.sound && variant.sound.formats) || this.defaultAudioFormats;
            const audioOutputPaths: ReadonlyArray<string> = audioFormats.map((fmt) => Path.replaceExtension(outputPath, `.${fmt}`));

            let videoStats: FFMPEG.VideoStats;

            if (dry)
            {
                videoStats = await JSON.parseFile(statsPath);
            }
            else
            {
                const originalStats: FFMPEG.VideoStats = await FFMPEG.getVideoStats(assetPath);

                // Use ffmpeg to convert the video over to the destination
                try
                {
                    await this.tryDeletePath(outputPath);
                    await Command.run("ffmpeg",
                    [
                        "-i", assetPath,
                        "-vcodec", "libx264",
                        "-pix_fmt", "yuv420p",
                        "-loglevel", "24",
                        "-y",
                        outputPath
                    ], null, false);
                    FileSystem.cache.set(outputPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                }
                catch (err)
                {
                    Log.error("Failed to convert video");
                    Log.error(err);
                    return null;
                }
                videoStats = await FFMPEG.getVideoStats(outputPath);
                if (originalStats.size < videoStats.size && originalStats.formatName.indexOf("mp4") > -1 && originalStats.pixelFormat == "yuv420p" && originalStats.codec == "h264")
                {
                    FileSystem.copyFile(assetPath, outputPath);
                }

                // Extract start poster
                try
                {
                    await this.extractFrame(assetPath, 0, startPosterPath, variant.usequant !== false);
                }
                catch (err)
                {
                    Log.error(`Failed to extract start poster: ${err}`);
                    return null;
                }

                // Extract end poster
                try
                {
                    await this.extractFrame(outputPath, videoStats.frameCount - 1, endPosterPath, variant.usequant !== false);
                }
                catch (err)
                {
                    Log.error(`Failed to extract end poster: ${err}`);
                    return null;
                }

                if (variant.extractAudio !== false)
                {
                    // Extract audio
                    for (const audioPath of audioOutputPaths)
                    {
                        try
                        {
                            const probe = await FFMPEG.FFProbe.run(assetPath);
                            const strm = FFMPEG.FFProbe.filterStreams(probe.streams, "audio")[0];

                            const sampleRate = parseInt(strm.sample_rate);
                            const channels = strm.channels;

                            await this.tryDeletePath(audioPath);
                            // -vn specifies to skip the video stream, -y overrites output files without asking, -async 1 corrects out of sync audio when extracting.
                            const audio = await Command.run("ffmpeg",
                                [
                                    "-async", "1",
                                    "-i", assetPath,
                                    "-vn",
                                    "-ar", `${sampleRate}`,
                                    "-ac", `${channels}`,
                                    "-y",

                                    "-loglevel", "24",
                                    audioPath
                                ], null, false);
                            FileSystem.cache.set(audioPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                        }
                        catch (err)
                        {
                            Log.error(`Failed to extract audio to ${audioPath}`);
                            Log.error(err);
                            return null;
                        }
                    }
                }

                // Save stats
                await FileSystem.writeFile(statsPath, JSON.stringify(videoStats));

                // Commit checksum
                this.module.checksumDB.set(this.checksumID, this.checksum);
            }

            const paths: string[] =
            [
                Path.replaceExtension(Path.sanitise(assetPath), `.${format}`)
            ];

            if (variant.extractAudio !== false)
            {
                paths.push(...audioOutputPaths);
            }

            if (variant.usePosters !== false)
            {
                paths.push(startPosterPath, endPosterPath);
            }

            // Return variant data
            return {
                id: this.id,
                type: AssetType.get(this),
                group: this.group,
                variantData:
                {
                    ...variant as BaseVariant,
                    duration: videoStats.duration,
                    framerate: videoStats.frameRate,
                    sound: variant.extractAudio !== false ?
                    {
                        formats: audioFormats
                    } : null
                },
                paths: paths,
                formats: variant.formats,
                exclude: this.exclude
            };
        }

        /** Extracts the given frame from the given video and puts it at outputPath, optionally quantizing the result. */
        protected async extractFrame(assetPath: string, frame: number, outputPath: string, useQuant: boolean)
        {
            // Clear out old results
            await this.tryDeletePath(outputPath);

            // Do the thing
            switch (this.moduleConfig.strategy)
            {
                default:
                    Log.warn(`Unknown strategy '${this.moduleConfig.strategy}', defaulting to 'ffmpeg'`);
                case null:
                case undefined:
                case "ffmpeg":
                    await FFMPEG.extractFrame(assetPath, frame, outputPath);
                    break;

                case "magick":
                    await ImageMagick.extractFrame(assetPath, frame, outputPath);
                    break;
            }

            // Quantize
            if (useQuant)
            {
                await ImageUtils.quantize(outputPath);
            }
        }

        /**
         * Processes a single format for a single asset variant.
         * Note that the format might be passed as null here if no format is specified.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, format: string | null): Promise<ProcessFormatResult | null>
        {
            return null;
        }

        protected async tryDeletePath(path: string)
        {
            if (await FileSystem.fileExists(path))
            {
                await FileSystem.deletePath(path);
            }
        }
    }
}
