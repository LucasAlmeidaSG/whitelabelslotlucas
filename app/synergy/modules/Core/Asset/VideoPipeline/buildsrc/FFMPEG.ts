namespace RS.FFMPEG
{
    export interface VideoStats
    {
        frameRate: number;
        frameCount: number;
        duration: number;
        size: number;
        formatName: string;
        pixelFormat: string;
        codec: string;
    }

    /**
     * Gets basic stats about a video file.
     * @param fileName
     */
    export async function getVideoStats(fileName: string): Promise<VideoStats>
    {
        const probe = await FFProbe.run(fileName);

        // TODO: Select the correct stream?
        const stream = FFProbe.filterStreams(probe.streams, "video")[0];

        const frameRateRaw = stream.avg_frame_rate.split("/");
        const frameRate = parseInt(frameRateRaw[0]) / parseInt(frameRateRaw[1]);

        const duration = parseFloat(stream.duration);
        const frameCount = parseInt(stream.nb_frames);

        const pixelFormat = stream.pix_fmt;

        const size = parseInt(probe.format.size);
        const formatName = probe.format.format_name;
        const codec = stream.codec_name;

        return { frameRate, duration, frameCount, size, formatName, pixelFormat, codec };
    }

    export async function extractFrame(inputPath: string, frame: number, outputPath: string): Promise<void>
    {
        // Resolve command
        await Command.run("ffmpeg",
        [
            "-i", inputPath,

            // Pick frames with index 'frame'
            "-vf", `select=eq(n\\,${frame})`,
            // Pick just one frame
            "-vframes", "1",
            // Stops it logging warnings to stderr which cause it to fail
            "-loglevel", "24",
            // Accept all confirmations
            "-y",

            outputPath
        ], null, false);
        FileSystem.cache.set(outputPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });

        // Check for unknown error
        if (!await FileSystem.fileExists(outputPath))
        {
            // ffmpeg likes to just fail to work without any log
            throw new Error(`FFMPEG failed to extract frame (unknown cause)`);
        }
    }
}
