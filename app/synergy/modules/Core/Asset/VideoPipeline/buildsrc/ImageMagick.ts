namespace RS.ImageMagick
{
    export async function extractFrame(inputPath: string, frame: number, outputPath: string)
    {
        const command = await AssetPipeline.resolveRequirement(AssetPipeline.ImageRequirements.imageMagick);
        const args = [`${inputPath}[${frame}]`, outputPath];
        if (command === "magick") { args.unshift("convert"); }

        // Run
        await Command.run(command, args, null, false);
        FileSystem.cache.set(outputPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
    }
}