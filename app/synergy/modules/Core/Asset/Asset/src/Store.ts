/// <reference path="Base.ts" />

namespace RS.Asset
{
    /**
     * Contains all loaded assets.
    **/
    export class Store
    {
        private static _subStores: { [group: string]: { [assetID: string]: Base } };

        private constructor() { }

        /**
         * Gets an asset from the global store by the specified asset ID.
         */
        public static getAsset(assetID: string): Base | null;

        /**
         * Gets an asset from a specific manifest by the specified asset ID.
         */
        public static getAsset(group: string, assetID: string): Base | null;

        /**
         * Gets an asset from a specific manifest by the specified asset ID.
         */
        public static getAsset(reference: Reference): Base | null;

        public static getAsset(p1: string | Reference, p2?: string): Base | null
        {
            // Resolve overloaded parameters
            const group = Is.assetReference(p1) ? p1.group : p2 != null ? p1 : globalManifest;
            const assetID = Is.assetReference(p1) ? p1.id : p2;

            // Locate it
            const subStore = this._subStores[group];
            if (!subStore) { return null; }
            const asset = subStore[assetID];
            if (!asset) { return null; }

            // Return it
            return asset;
        }

        /**
         * Adds an asset to this store.
        **/
        public static addAsset(asset: Base): void
        {
            // Store it
            const subStore = this._subStores[asset.group] || (this._subStores[asset.group] = {});
            subStore[asset.id] = asset;
        }

        /**
         * Remove an asset in this store.
        **/
        public static removeAsset(asset: Base): void
        {
            // Get the substore
            const subStore = this._subStores[asset.group];

            if (subStore && subStore[asset.id])
            {
                subStore[asset.id].dispose();
                subStore[asset.id] = null;
            }
        }

        @RS.Init()
        private static init()
        {
            this._subStores = {};
            this._subStores[globalManifest] = {};
        }
    }
}