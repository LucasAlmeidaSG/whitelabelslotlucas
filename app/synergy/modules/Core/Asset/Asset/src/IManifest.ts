namespace RS.Asset
{
    /**
     * Base definition for an asset.
     */
    export interface Definition
    {
        id: string;
        group: string;
        path: string;
        raw: object;
        type: string;
        formats: string[];
        exclude: string[];
    }

    /**
     * Encapsulates an asset manifest.
     */
    export interface IManifest
    {
        /** Gets if the manifest data has been loaded. */
        readonly loaded: IReadonlyObservable<boolean>;
        
        /** Gets all asset definitions from this manifest. */
        readonly assets: ReadonlyArray<Definition>;

        /** Gets this manifest's URL. */
        readonly path: string;

        /** Loads this manifest. */
        load(): PromiseLike<void>;
    }
}