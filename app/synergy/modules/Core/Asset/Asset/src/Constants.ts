namespace RS.Asset
{
    /** Manifest ID of assets which aren't explicitly assigned to a manifest. */
    export const globalManifest = "common";

    /** Standard asset variants. */
    export enum Variants
    {
        Shared = "shared",
        Desktop = "desktop",
        Mobile = "mobile"
    }
}