namespace RS.Asset
{
    /**
     * Encapsulates a load priority for an asset group.
     */
    export enum LoadPriority
    {
        /** Group should load now. Suspend game whilst this in progress. */
        Immediate,

        /** Group should load during the preload phase. */
        Preload,

        /** Group should background-load as soon as possible. */
        High,

        /** Group should background-load after all groups marked "High". */
        Low,

        /** Group should never load. */
        None
    }

    export enum ManifestState
    {
        /** There are unloaded manifests. */
        Unloaded,
        /** Manifests are loading. */
        Loading,
        /** All manifests are loaded. */
        Loaded
    }

    /**
     * Responsible for asset lifetime management.
     */
    export interface IController extends Controllers.IController<IController.Settings>
    {   
        /**
         * Published when an asset has failed to load.
         */
        readonly onAssetLoadFailed: IEvent<string>;

        /** Manifest load state. */
        readonly manifestState: IReadonlyObservable<ManifestState>;

        /** Gets or sets if loading should proceed. */
        loadEnabled: boolean;

        /** Gets or sets the minimum priority of asset to background load. */
        backgroundLoad: LoadPriority;

        /**
         * Loads all manifests as specified in the settings.
         * Does NOT load any assets themselves.
         */
        loadManifests(): PromiseLike<void>;

        /**
         * Sets the priority of the specified asset group.
         * Safe to call at any time (even before manifests have been loaded).
         */
        setGroupPriority(group: string, priority: LoadPriority);

        /**
         * Gets the priority of the specified asset group.
         * Returns null if no priority has been set.
         */
        getGroupPriority(group: string): LoadPriority | null;

        /**
         * Loads the specified asset groups now.
         * Returns a load operation.
         */
        loadGroups(groups: ReadonlyArray<string>): Scheduler.IOperation;

        /**
         * Loads the specified asset group now.
         * Returns a load operation.
         */
        loadGroup(group: string): Scheduler.IOperation;

        /**
         * Loads all asset groups with the specified priority now.
         */
        loadByPriority(priority: LoadPriority, suspendGame?: boolean): Scheduler.IOperation;

        /**
         * Gets if the specified asset group is completely loaded.
         */
        isGroupLoaded(group: string): boolean;
    }

    export const IController = Controllers.declare<IController, IController.Settings>(Strategy.Type.Instance);

    export namespace IController
    {
        /**
         * Settings for the asset controller.
         */
        export interface Settings
        {
            /** Path to manifest files to use. */
            manifests: string[];
        }
    }
}