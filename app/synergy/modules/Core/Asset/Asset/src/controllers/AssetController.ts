/// <reference path="IAssetController.ts" />
/// <reference path="../AssetsLoadTask.ts" />

namespace RS.Asset
{
    interface AssetGroup
    {
        priority: LoadPriority;
        loadTask: AssetsLoadTask;
        assets: Definition[];
    }

    class Controller implements IController
    {
        //protected _onAssetLoadFailed = createEvent<string>();
        /**
         * Published when an asset has failed to load.
         */
        public readonly onAssetLoadFailed = createEvent<string>();
        /**
         * Gets the progress of the currently active load operation.
         */
        public readonly loadProgress = new Observable(0);

        public settings: IController.Settings;

        protected _scheduler: RS.Scheduler;
        protected _allOps: RS.Scheduler.IOperation[] = [];

        protected _manifests: IManifest[] = [];
        protected _groups: Map<AssetGroup> = {};

        protected readonly _manifestState = new RS.Observable<ManifestState>(ManifestState.Unloaded);
        public get manifestState(): RS.IReadonlyObservable<ManifestState> { return this._manifestState; }

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this._manifests == null; }

        /** Gets or sets if asset loading should proceed. */
        public get loadEnabled() { return !this._scheduler.suspended; }
        public set loadEnabled(value)
        {
            this._scheduler.suspended = !value;
        }

        /** Gets or sets the minimum priority of asset to background load. */
        public get backgroundLoad(): LoadPriority { return this._scheduler.minPriority; }
        public set backgroundLoad(value) { this._scheduler.minPriority = value; }

        /** Initialises this controller. */
        public initialise(settings: IController.Settings)
        {
            this.settings = settings;
            this._scheduler = new RS.Scheduler();
            this._scheduler.suspended = true;
            this._scheduler.minPriority = -1;
        }
        
        /**
         * Loads all manifests as specified in the settings.
         * Does NOT load any assets themselves.
         */
        public async loadManifests()
        {
            if (this._manifestState.value !== ManifestState.Unloaded)
            {
                logger.warn(`loadManifests called, but manifests were not unloaded`);
                await this._manifestState.for(ManifestState.Loaded);
                return;
            }

            this._manifestState.value = ManifestState.Loading;
            this._scheduler.autoRefresh = false;

            // Iterate all manifests
            for (const manifestURL of this.settings.manifests)
            {
                await this.loadManifest(manifestURL);
            }

            // Seal the groups
            for (const name in this._groups)
            {
                if (this._groups[name])
                {
                    const group = this._groups[name];
                    if (group.loadTask == null)
                    {
                        group.loadTask = new AssetsLoadTask(group.assets, name, group.priority, ".");
                        this._scheduler.addTask(group.loadTask);
                    }
                }
            }

            // Enable
            this._scheduler.suspended = false;
            this._scheduler.autoRefresh = true;
            this._scheduler.refresh();
            this._manifestState.value = ManifestState.Loaded;
        }

        /**
         * Sets the priority of the specified asset group.
         * Safe to call at any time (even before manifests have been loaded).
         */
        public setGroupPriority(group: string, priority: LoadPriority)
        {
            const groupData = this.getOrCreateGroup(group);
            groupData.priority = priority;
            if (groupData.loadTask != null)
            {
                groupData.loadTask.priority = priority;
                this._scheduler.refresh();
            }
        }

        /**
         * Gets the priority of the specified asset group.
         * Returns null if no priority has been set.
         */
        public getGroupPriority(group: string): LoadPriority | null
        {
            const groupData = this._groups[group];
            return groupData != null ? groupData.priority : null;
        }

        /**
         * Loads the specified asset groups now.
         * Returns a load operation.
         */
        public loadGroups(groups: ReadonlyArray<string>): Scheduler.IOperation | null
        {
            const tasks = groups.map((g) => this.getOrCreateGroup(g)).filter((g) => g.assets.length > 0 && g.loadTask != null);
            if (tasks.length === 0) { return null; }
            const op = this._scheduler.addOperation(tasks.map((t) => t.loadTask), -1, groups.join(","));
            op.onErrored((err) => this.onAssetLoadFailed.publish(err));
            this._allOps.push(op);
            return op;
        }

        /**
         * Loads the specified asset group now.
         * Returns a load operation.
         */
        public loadGroup(group: string): Scheduler.IOperation | null
        {
            const groupData = this.getOrCreateGroup(group);
            if (groupData.assets.length === 0) { return null; }
            if (groupData.loadTask == null) { return null; }
            const op = this._scheduler.addOperation([ groupData.loadTask ], -1, group);
            op.onErrored((err) => this.onAssetLoadFailed.publish(err));
            this._allOps.push(op);
            return op;
        }

        /**
         * Loads all asset groups with the specified priority now.
         */
        public loadByPriority(priority: LoadPriority, suspendGame?: boolean): Scheduler.IOperation
        {
            const tasks: AssetsLoadTask[] = [];
            for (const group in this._groups)
            {
                if (this._groups[group])
                {
                    const groupData = this._groups[group];
                    if (groupData.priority === priority)
                    {
                        if (groupData.loadTask != null) { tasks.push(groupData.loadTask); }
                    }
                }
            }
            const op = this._scheduler.addOperation(tasks, -1, LoadPriority[priority]);
            op.onErrored((err) => this.onAssetLoadFailed.publish(err));
            this._allOps.push(op);
            return op;
        }

        /**
         * Gets if the specified asset group is completely loaded.
         */
        public isGroupLoaded(group: string): boolean
        {
            const groupData = this.getOrCreateGroup(group);
            return groupData.loadTask != null && groupData.loadTask.isComplete;
        }

        /**
         * Disposes this controller.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }
            this._scheduler.suspended = true;
            this._groups = null;
            this._manifests = null;
        }

        private async loadManifest(url: string)
        {
            const existing = Find.matching(this._manifests, (m) => m.path === url);
            if (existing)
            {
                await existing.load();
                return;
            }

            // Load the manifest
            const manifest = new Manifest(url);
            await manifest.load();
            this._manifests.push(manifest);

            const manifestDir = Path.directoryName(url);

            // Sort all assets into their appropiate places based on group
            for (const assetDef of manifest.assets)
            {
                const groupData = this.getOrCreateGroup(assetDef.group);
                groupData.assets.push({
                    ...assetDef,
                    path: Path.combine(manifestDir, assetDef.path)
                });
            }
        }

        private registerGroupAsset(group: AssetGroup, asset: Definition): void
        {
            const index = Find.sortedIndex(group.assets, (def) => RS.Util.stringCompare(asset.id, def.id));

            const existingAsset = group.assets[index];
            if (existingAsset && existingAsset.id === asset.id)
            {
                Log.debug(`Overwriting asset '${existingAsset.id}'`);
                group.assets[index] = asset;
                return;
            }

            group.assets.splice(index, 0, asset);
        }

        private getOrCreateGroup(name: string): AssetGroup
        {
            return this._groups[name] || (this._groups[name] =
            {
                priority: LoadPriority.None,
                loadTask: null,
                assets: []
            });
        }
    }

    IController.register(Controller);
}