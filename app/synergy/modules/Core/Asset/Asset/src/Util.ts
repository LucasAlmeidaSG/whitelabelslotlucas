namespace RS.Asset
{
    /**
     * Sends an asynchronous ajax request, formatting the response as plain-text.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType.Text): PromiseLike<string>;

    /**
     * Sends an asynchronous ajax request, formatting the response as an XML document.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType.XML): PromiseLike<Document>;

    /**
     * Sends an asynchronous ajax request, parsing the response as JSON.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType.JSON): PromiseLike<object>;

    /**
     * Sends an asynchronous ajax request, formatting the response as an ArrayBuffer.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType.Binary): PromiseLike<ArrayBuffer>;

    /**
     * Sends an asynchronous ajax request, formatting the response as a blob.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType.Blob): PromiseLike<Blob>;

    /**
     * Sends an asynchronous ajax request, formatting the response as an object URL pointing to a blob.
     * @param settings
     * @param responseType
     */
    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType.ObjectURL): PromiseLike<string>;

    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType): PromiseLike<string | Blob | ArrayBuffer | object | Document>;

    export function ajax(settings: Request.AJAXSettings, responseType: Request.AJAXResponseType)
    {
        settings = { ...settings, url: urlWithVersion(settings.url) };
        return Request.ajax(settings, responseType);
    }

    export function tag<K extends Request.TagName>(settings: Request.TagSettings<K>)
    {
        settings = { ...settings, url: urlWithVersion(settings.url) };
        return Request.tag(settings);
    }

    export function urlWithVersion(url: string): string
    {
        const version = "{!GAME_VERSION!}";
        if (/^[0-9]+\.[0-9]+\.[0-9]+$/.test(version))
        {
            return URL.setParameters({ "gamever": version }, url);
        }
        else
        {
            return url;
        }
    }
}