/// <reference path="IManifest.ts" />

namespace RS.Asset
{
    /**
     * Encapsulates an asset manifest sourced by a JSON file.
     */
    export class Manifest implements IManifest
    {
        /** Gets if the manifest data has been loaded. */
        public readonly loaded = new Observable<boolean>(false);

        protected _loadPromise: PromiseLike<void>;

        protected _assets: Definition[];
        public get assets() { return this._assets; }

        public constructor(public readonly path: string) { }

        /**
         * Loads this manifest.
         * Does not make any attempt to load the assets within.
         */
        public load()
        {
            return this._loadPromise || (this._loadPromise = this.loadInternal());
        }

        private async loadInternal()
        {
            const rawData = await Asset.ajax({ url: this.path }, Request.AJAXResponseType.JSON) as Map<Definition>;
            this._assets = [];
            for (const assetID in rawData)
            {
                if (rawData[assetID])
                {
                    const raw = rawData[assetID];
                    const def: Definition =
                    {
                        id: assetID,
                        group: raw["group"] || "common",
                        path: raw["path"],
                        type: raw["type"],
                        exclude: raw["exclude"],
                        formats: raw["formats"],
                        raw
                    };
                    this._assets.push(def);
                }
            }

            this.loaded.value = true;
        }
    }
}