/// <reference path="AssetsLoader.ts" />

namespace RS.Asset
{
    @RS.HasCallbacks
    export class AssetsLoadTask implements Scheduler.ITask
    {
        //protected readonly _onCompleted = createSimpleEvent();
        //protected readonly _onErrored = createEvent<string>();
        
        /** Published when the task was running and has now been completed. */
        public readonly onCompleted = createSimpleEvent();//this._onCompleted.public;

        /** Published when the task was cancelled due to an error. */
        public readonly onErrored = createEvent<string>();//this._onErrored.public;
        protected _priority: number;
        protected _assets: Definition[];
        protected _loader: AssetsLoader;
        protected _name: string;

        /** What priority is this task? (lower value = more important). */
        public get priority() { return this._priority; }
        public set priority(value) { this._priority = value; }

        /** Published when the progress of the task has changed. */
        public get onProgressChanged() { return this._loader.onProgressChanged; }

        /** Gets if the task is currently running. */
        public get isRunning() { return this._loader.state.value === AssetsLoader.State.Loading; }

        /** Gets if the task is currently complete. */
        public get isComplete() { return this._loader.state.value === AssetsLoader.State.Loaded; }

        /** Gets the current progress of the task (0-1). */
        public get progress() { return this._loader.progress; }

        /** Gets the progress weight of this task relative to other tasks. */
        public get progressWeight() { return this._assets.length; }

        /** Gets a name for this task (used for debugging e.g. log messages). */
        public get debugName() { return this._name; }

        public constructor(assets: Definition[], name: string, priority: number, baseURL: string)
        {
            this._assets = assets;
            this._loader = new AssetsLoader(assets, baseURL);
            this._name = name;
            this._priority = priority;
            this._loader.state.onChanged(this.handleLoaderStateChanged);
        }

        /** Suspends this tasks. */
        public async suspend()
        {
            await this._loader.suspend();
        }

        /** Resumes/starts this task. */
        public async resume()
        {
            if (this._loader.state.value === AssetsLoader.State.Suspended)
            {
                this._loader.resume();
            }
            else
            {
                this._loader.load();
            }
        }

        @Callback
        protected handleLoaderStateChanged()
        {
            if (this._loader.state.value === AssetsLoader.State.Loaded)
            {
                this.onCompleted.publish();
            }
            else if (this._loader.state.value === AssetsLoader.State.LoadError)
            {
                Log.error(this._loader.error);
                this.onErrored.publish(this._loader.error);
            }
        }
    }
}