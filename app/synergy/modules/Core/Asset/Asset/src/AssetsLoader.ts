namespace RS.Asset
{
    const loadThreadCount = 4;

    /**
     * Responsible for loading a set of assets from an array.
     */
    export class AssetsLoader implements IDisposable
    {
        //protected readonly _onProgressChanged = createEvent<number>();

        /** Published when the load progress of this loader has changed. */
        public readonly onProgressChanged = createEvent<number>();//this._onProgressChanged.public;
        /** Gets the current state of the manifest loader. */
        public readonly state = new Observable(AssetsLoader.State.None);

        protected _activeThreadCount = new Observable(0);
        protected _suspendedThreadCount = new Observable(0);
        protected _suspended = new Observable(false);

        protected _loadedItems: number = 0;
        protected _numItems: number = 0;

        /** Gets if this loader is disposed. */
        public get isDisposed() { return this.state.value === AssetsLoader.State.Disposed; }

        protected _loadQueue: Definition[];
        protected _loadThreads: AssetsLoader.LoadThread[];
        protected _threads: number;
        protected _error: string|null;

        /** If this loader is in error state, gets the error. */
        public get error() { return this._error; }

        /** Gets the current progress of this loader (0-1). */
        public get progress() { return this._numItems === 0 ? 1 : this._loadedItems / this._numItems; }

        /**
         * Initialises a new instance of the ManifestLoader class.
         * @param manifest The manifest to load assets from.
         * @param baseURL The URL to load assets relative from, usually the remote location of the manifest itself.
         */
        public constructor(assets: Definition[], public readonly baseURL: string)
        {
            this._loadQueue = [...assets];
            this._numItems = this._loadQueue.length;
            this._loadedItems = 0;
        }

        /**
         * Begins loading this manifest.
         */
        public load()
        {
            if (this.isDisposed) { throw new Error("Tried to begin loading when disposed"); }
            if (this.state.value !== AssetsLoader.State.None) { return; }

            this._loadThreads = [];
            for (let i = 0; i < loadThreadCount; ++i)
            {
                const loadThread = new AssetsLoader.LoadThread(this._loadQueue, async () => await this.workerThread());
                this._loadThreads.push(loadThread);
            }
            this._threads = loadThreadCount;

            this.state.value = AssetsLoader.State.Loading;
            
            for (const loadThread of this._loadThreads)
            {
                this.workerThread();
            }
        }

        /**
         * Suspends (pauses) loading on this manifest.
         * Returns a promise that resolves when all load threads have finished their current task and are suspended.
         */
        public async suspend()
        {
            if (this.state.value !== AssetsLoader.State.Loading) { return; }
            this.state.value = AssetsLoader.State.Suspending;
            this._suspended.value = true;

            // Either suspended thread count can increase to active thread count,
            // or active thread count can decrease to suspended thread count.
            // So we need to watch both.

            const suspendedChangeHandler = this._suspendedThreadCount.onChanged((suspended) =>
            {
                if (suspended === this._activeThreadCount.value)
                {
                    this.state.value = AssetsLoader.State.Suspended;
                }
            });

            const activeChangeHandler = this._activeThreadCount.onChanged((active) =>
            {
                if (active === this._suspendedThreadCount.value)
                {
                    this.state.value = AssetsLoader.State.Suspended;
                }
            });

            await this.state.for(AssetsLoader.State.Suspended);

            activeChangeHandler.dispose();
            suspendedChangeHandler.dispose();
        }

        /**
         * Resumes loading on this manifest (after having previously been suspended).
         */
        public resume()
        {
            if (this.state.value !== AssetsLoader.State.Suspended) { return; }
            this._suspended.value = false;
            this.state.value = AssetsLoader.State.Loading;
        }

        /** Disposes this loader. */
        public dispose()
        {
            if (this.isDisposed) { return; }
            // TODO: Cleanup
            this.state.value = AssetsLoader.State.Disposed;
        }

        protected async workerThread()
        {
            this._activeThreadCount.value++;
            while (this._loadQueue.length > 0 && this.state.value !== AssetsLoader.State.LoadError)
            {
                this._suspendedThreadCount.value++;
                await this._suspended.for(false);
                this._suspendedThreadCount.value--;
                const item = this._loadQueue.shift();
                if (item == null) { break; }
                try
                {
                    await this.processLoadItem(item);
                    this._loadedItems++;
                    this.onProgressChanged.publish(this.progress);
                }
                catch (err)
                {
                    this._error = err;
                    this.state.value = AssetsLoader.State.LoadError;
                    break;
                }
            }
            this._activeThreadCount.value--;
            this.onThreadExited();
        }

        protected async processLoadItem(def: Definition)
        {
            if (def.exclude && def.exclude.length > 0 && def.exclude.indexOf(RS.Device.browser) !== -1)
            {
                RS.Log.debug(`Asset was excluded from loading in browser (${RS.Device.browser})`);
                return;
            }
            // Find a loader for our type
            const assetCtor = Base.get(def.type);
            if (assetCtor == null)
            {
                throw new Error(`Could not find appropriate asset for type '${def.type}' (${def.id})`);
            }

            // Create, store & load asset
            const asset = new assetCtor(def);
            Store.addAsset(asset);
            const format = asset.format;
            if (format == null)
            {
                throw new Error(`Could not find supported format for type '${def.type}' (${def.id})`);
            }
            const url = Path.normalise(Path.combine(this.baseURL, def.path));
            await asset.load(url, asset.format);
            if (asset.state.value === Asset.State.LoadFailed)
            {
                throw new Error(`Asset load error (${def.id})`);
            }
        }

        protected onThreadExited()
        {
            if (this._activeThreadCount.value === 0 && this.state.value === AssetsLoader.State.Loading)
            {
                this.state.value = AssetsLoader.State.Loaded;
            }
        }
    }

    export namespace AssetsLoader
    {
        /**
         * State of a manifest loader.
         */
        export enum State
        {
            /** Initial state */
            None,

            /** Loading in progress */
            Loading,

            /** Waiting for current items to finish, so can suspend */
            Suspending,

            /** Suspended mid-load */
            Suspended,

            /** All items loaded */
            Loaded,

            /** One or more items failed to load */
            LoadError,

            /** Disposed */
            Disposed
        }

        export class LoadThread
        {
            public readonly suspended = new Observable<boolean>(false);
            public constructor(protected _loadQueue: Definition[], protected _threadFunc: () => PromiseLike<void>) { }
            public async run()
            {
                while (this._loadQueue.length > 0)
                {
                    await this.suspended.for(false);
                    await this._threadFunc();
                }
            }
        }
    }
}