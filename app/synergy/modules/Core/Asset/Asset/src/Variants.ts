namespace RS.Asset
{
    const availableVariants: string[] = [];
    let webglCaps: Capabilities.WebGL | null = null;
    const warnedSegs: Map<boolean> = {};
    const useCustomVariant: string[] = [];
    let useMobileAssetsIE: boolean = false;

    /**
     * Specifies what custom variants we can use
     * @param variants 
     */
    export function useCustomVariants(variants: string[]): string[]
    {
        for (const variant of variants)
        {
            if (useCustomVariant.indexOf(variant) === -1)
            {
                useCustomVariant.push(variant);
            }
        }
        return variants;
    }

    /**
     * Specifies whether mobile assets should be used for internet explorer.
     * @param value
     */
    export function setShouldUseMobileAssetsForIE(value: boolean): void
    {
        useMobileAssetsIE = value;
    }

    /**
     * Specifies a variant as valid and available.
     * @param variants 
     */
    export function registerVariants(variants: string[]): string[]
    {
        for (const variant of variants)
        {
            if (availableVariants.indexOf(variant) === -1)
            {
                availableVariants.push(variant);
            }
        }
        return variants;
    }

    /**
     * Gets all variants that should be active for this session.
     */
    export function getVariants(): string[]
    {
        if (!webglCaps) { webglCaps = Capabilities.WebGL.get(); }

        const result: string[] = [];
        for (const variant of availableVariants)
        {
            if (shouldUseVariant(variant))
            {
                result.push(variant);
            }
        }
        
        return result;
    }

    function shouldUseVariant(name: string): boolean
    {
        const segs = name.toLowerCase().split("_");

        if (useCustomVariant.indexOf(name) !== -1)
        {
            return true;
        }

        for (const seg of segs)
        {
            if (!shouldUseVariantSeg(seg)) { return false; }
        }
        return true;
    }

    function shouldUseVariantSeg(seg: string): boolean
    {
        switch (seg)
        {
            // -- SHARED -- \\
            case "shared":
                return true;
            
            // -- PLATFORM -- \\
            case "desktop":
                return Device.isDesktopDevice;
            case "mobile":
                return Device.isMobileDevice || (Device.browser === Browser.IE && useMobileAssetsIE);
            
            // -- TEXTURE FORMAT -- \\
            // Use S3TC if available, falling back to PVRTC if available, falling back to png.
            case "pvrtc":
                return webglCaps.available && webglCaps.supportsPVRTC && !webglCaps.supportsS3TC;
            case "s3tc":
                return webglCaps.available && webglCaps.supportsS3TC;
            case "png":
                return !webglCaps.available || (!webglCaps.supportsPVRTC && !webglCaps.supportsS3TC);
            
            // -- DEFAULT - User defined variants -- \\
            default:
                // Check if it can be used
                if (useCustomVariant.indexOf(seg) !== -1)
                {
                    return true;
                }
                return false;
        }
    }
}
