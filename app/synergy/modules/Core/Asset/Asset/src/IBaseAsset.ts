namespace RS.Asset {
    export interface IBaseAsset<T> {
        /** Gets the definition for this asset. */
        definition: Definition;

        /** Gets the ID of this asset. */
        id: string;

        /** Gets the group to which this asset belongs. */
        group: string;

        /** Gets the actual asset, or null if it hasn't loaded yet. */
        asset: T | null;

        /** Gets the current state of this asset. */
        state: Observable<State>;

        /** Gets if this asset has loaded and is ready for use. */
        loaded: boolean;

        /** Gets if this asset has been disposed. */
        isDisposed: boolean;

        /** Gets the asset type of this asset. */
        type: string;

        /** Gets the format that this asset will load. */
        format: string;

        /** Selects a format to use from the given options. */
        selectFormat(options: string[]): string | null;

        /** Loads this asset from the specified URL. */
        load(url: string, format: string);

        /** Casts this asset to the specified type, if it is correct. Returns null if it isn't. */
        as<Type extends object>(type: (...args)=>any & { prototype: Type }): Type | null;

        clone(): IBaseAsset<T>;
    }
}