/// <reference path="Base.ts" />

namespace RS.Asset
{
    /**
     * Encapsulates a static reference to an asset.
     */
    export interface Reference<T extends string = string>
    {
        id: string;
        group: string;
        type: T;
        clone(): Reference<T>;
    }

    export function clone<T extends string = string>(this: Reference<T>): Reference<T>
    {
        return RS.Util.clone(this, false);
    }

    /**
     * A weakly typed reference to an asset.
     */
    export type GenericReference = Reference<string>;
}

namespace RS.Is
{
    /**
     * Gets if the specified value is an RS.Asset.Reference.
     * @param value 
     */
    export function assetReference(value: any): value is Asset.Reference
    {
        return Is.object(value) &&
            Is.string(value.id) &&
            Is.string(value.group) &&
            Is.string(value.type) &&
            Is.func(value.clone);
    }

    /**
     * Gets if the specified value is an RS.Asset.Reference<type>.
     * @param value 
     * @param type 
     */
    export function assetReferenceTo<T extends string>(value: any, type: T): value is Asset.Reference<T>
    {
        return Is.assetReference(value) && value.type === type;
    }
}