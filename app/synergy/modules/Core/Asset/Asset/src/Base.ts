namespace RS.Asset
{
    /**
     * Encapsulates the state of an asset.
     */
    export enum State
    {
        /** Asset has not been loaded or preloaded. */
        None,

        /** Asset is loading. */
        Loading,

        /** Asset is loaded and ready to use. */
        Loaded,

        /** Asset failed to load. */
        LoadFailed,

        /** Asset has been disposed. */
        Disposed
    }

    /**
     * Represents a raw asset instance.
     */
    export type RawAsset = object | string;

    /**
     * Represents a constructor for an asset.
     */
    export interface AssetConstructor
    {
        prototype: Base;
        new(definition: Definition): Base;
    }

    /**
     * Marks an type for an asset class.
     */
    export const Type = Decorators.Tag.create<string>(Decorators.TagKind.Class);

    export const logger = Logging.getContext("Asset");

    /**
     * Encapsulates an abstract asset.
     */
    @Type("none")
    export abstract class Base<T extends RawAsset = RawAsset> implements IDisposable
    {
        private static _assetTypes: Map<AssetConstructor>;

        protected _def: Definition;
        protected _id: string;
        protected _group: string;
        protected _asset: T|null = null;
        protected _state = new Observable<State>(State.None);

        /** Gets the definition for this asset. */
        public get definition() { return this._def; }

        /** Gets the ID of this asset. */
        public get id() { return this._def.id; }

        /** Gets the group to which this asset belongs. */
        public get group() { return this._def.group; }

        /** Gets the actual asset, or null if it hasn't loaded yet. */
        public get asset() { return this._asset; }

        /** Gets the current state of this asset. */
        public get state() { return this._state; }

        /** Gets if this asset has loaded and is ready for use. */
        public get loaded() { return this._state.value === State.Loaded; }

        /** Gets if this asset has been disposed. */
        public get isDisposed() { return this._state.value === State.Disposed; }

        /** Gets the asset type of this asset. */
        public get type() { return Type.get(this); }

        /** Gets the format that this asset will load. */
        public get format() { return this.selectFormat(this._def.formats); }

        public constructor(definition: Definition)
        {
            this._def = definition;
        }

        /**
         * Gets a constructor for the specified asset type.
         * @param type
         */
        public static get(type: string): AssetConstructor|null
        {
            if (this._assetTypes == null) { this._assetTypes = {}; }
            const ctor: AssetConstructor = this._assetTypes[type];
            if (ctor) { return ctor; }
            const assetClasses = Type.getClassesWithTag(type);
            if (assetClasses.length === 0) { return null; }
            if (assetClasses.length > 1)
            {
                logger.error(`Multiple asset classes with type '${type}' found`);
                return null;
            }
            return this._assetTypes[type] = assetClasses[0] as any;
        }

        /** Selects a format to use from the given options. */
        public abstract selectFormat(options: string[]): string | null;

        public abstract clone(): Base<T>;

        /** Loads this asset from the specified URL. */
        public async load(url: string, format: string)
        {
            // Inspect state
            switch (this._state.value)
            {
                case State.Loading:
                    // Loading already in progress, just wait for it to be done
                    await this._state.for(State.Loaded);
                    return;
                case State.Loaded:
                    // Already loaded
                    return;
                case State.Disposed:
                    throw new Error("Tried to load asset when it was disposed");
            }

            this._state.value = State.Loading;
            try
            {
                logger.debug(`Loading asset '${this.id}' from '${url}'...`);
                this._asset = await this.performLoad(url, format);
            }
            catch (err)
            {
                logger.error(err);
                this._state.value = State.LoadFailed;
                return;
            }
            this._state.value = State.Loaded;
        }

        /** Casts this asset to the specified type, if it is correct. Returns null if it isn't. */
        public as<ClassType extends object>(type: Function & { prototype: ClassType }): ClassType|null
        {
            if (this instanceof type)
            {
                return this as object as ClassType;
            }
            else if (this._asset instanceof type)
            {
                return this._asset as object as ClassType;
            }
            else
            {
                return null;
            }
        }

        /** Disposes this asset. */
        public dispose()
        {
            if (this.isDisposed) { return; }
            this.unload();
            this._state.value = State.Disposed;
            this._state.dispose();
        }

        /**
         * Performs the actual load process for this asset.
         * @param url
         */
        protected abstract performLoad(url: string, format: string): PromiseLike<T>;

        /** Unloads this asset's data. */
        protected unload()
        {
            this._asset = null;
        }
    }
}