namespace RS.AssetPipeline
{
    /**
     * Sound asset specific definition data.
     */
    export interface SoundDefinition extends BaseVariant
    {
        channels?: number;
        sampleRate?: number;
        trim?: number;
        silencePadding?: number;
        bitRate?: number;
    }

    export interface SoundSpriteEntry
    {
        id: string;
        startTime: number;
        endTime: number;
    }

    const supportedInputFormats = [ ".mp3", ".mp2", ".aiff", ".aif", ".wav", ".flac", ".ogg", ".m4a" ];
    const supportedOutputFormats = [ "mp3", "mp2", "aiff", "aif", "wav", "flac", "ogg", "m4a" ];

    /**
     * Represents an sound-type asset.
     */
    @AssetType("sound")
    @InferFromExtensions(supportedInputFormats)
    @RequireCommands([
        // { command: "sox", error: "Could not find command 'sox'. Please check that sox is installed correctly." },
        { command: "ffmpeg", error: "Could not find command 'ffmpeg'. Please check that ffmpeg is installed correctly." },
    ])
    export class Sound extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "mp3", "m4a" ]; }

        /** Gets if this asset is linked via a single path or not. */
        public get supportedPathType() { return PathType.Both; }

        public get classModule() { return "Core.Asset.SoundPipeline"; }

        protected _isSoundSprite: boolean = false;
        protected _ssSampleRate: number;
        protected _ssChannels: number;
        protected _ssNames: string[];

        /**
         * Loads any required metadata for this asset.
         */
        public async load()
        {
            this._isSoundSprite = this.getIsSoundSprite();
            if (this._isSoundSprite)
            {
                const nameList = new List<string>();
                if (!Is.string(this._definition.path))
                {
                    nameList.addRange(Object.keys(this._definition.path), true);
                }
                if (this._definition.variants)
                {
                    for (const variantName in this._definition.variants)
                    {
                        if (this._definition.variants[variantName])
                        {
                            const variant = this._definition.variants[variantName];
                            if (!Is.string(variant.path))
                            {
                                nameList.addRange(Object.keys(variant.path), true);
                            }
                        }
                    }
                }
                this._ssNames = nameList.dataCopy;
            }
            await super.load();
        }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            if (this._isSoundSprite)
            {
                const assetRef = Manifest.getAssetReference(this.module.name, this.id);
                const value: Build.CodeGeneration.Value =
                {
                    kind: "objval",
                    name: this.id,
                    values: this._ssNames.map((name) =>
                    {
                        const val: Build.CodeGeneration.Value = {
                            kind: "keyval",
                            name,
                            value: {
                                kind: "objval",
                                name,
                                values: [
                                    { kind: "keyval", name: "id", value: `"${assetRef}.${name}"` },
                                    { kind: "keyval", name: "group", value: `"${this.group}"` },
                                    { kind: "keyval", name: "type", value: `"sound"` },
                                    { kind: "keyval", name: "clone", value: `RS.Asset.clone` },
                                    { kind: "keyval", name: "get", value: `RS.Asset.SoundReference.get` },
                                    { kind: "keyval", name: "play", value: `RS.Asset.SoundReference.play` }
                                ]
                            }
                        };
                        return val;
                    })
                };
                const type: CodeMeta.TypeRef =
                {
                    kind: CodeMeta.TypeRef.Kind.Inline,
                    properties: this._ssNames.map((name) =>
                    {
                        const prop: Build.CodeGeneration.PropertyDeclaration =
                        {
                            kind: "propertydeclr",
                            name,
                            type:
                            Build.CodeGeneration.intersect(
                            {
                                kind: CodeMeta.TypeRef.Kind.Raw,
                                name: "RS.Asset.SoundReference"
                            },
                            {
                                kind: CodeMeta.TypeRef.Kind.Inline,
                                properties:
                                [
                                    { name: "get", type: { kind: CodeMeta.TypeRef.Kind.Raw, name: "typeof RS.Asset.SoundReference.get" } },
                                    { name: "play", type: { kind: CodeMeta.TypeRef.Kind.Raw, name: "typeof RS.Asset.SoundReference.play" } }
                                ]
                            })
                        };
                        return prop;
                    })
                };
                return { type, value };
            }
            else
            {
                const ref = AssetPipeline.emitAssetReference(this, "RS.Asset.SoundReference");
                AssetPipeline.addAssetReferenceProperty(ref, "play", `RS.Asset.SoundReference.play`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "typeof RS.Asset.SoundReference.play" });
                return ref;
            }
        }

        /**
         * Examines the definition and determines if this is a sound sprite.
         * Throws if an inconsistency between single-type and map-type paths is detected.
         */
        protected getIsSoundSprite()
        {
            let isSoundSprite: boolean|null = null;
            if (this._definition.path)
            {
                isSoundSprite = Is.object(this._definition.path);
            }
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    if (this._definition.variants[variantName])
                    {
                        const variant = this._definition.variants[variantName];
                        if (variant.path)
                        {
                            if (Is.object(variant.path))
                            {
                                if (isSoundSprite == null)
                                {
                                    isSoundSprite = true;
                                }
                                else if (isSoundSprite === false)
                                {
                                    throw new Error(`All paths must be maps for a sound sprite.`);
                                }
                            }
                            else
                            {
                                if (isSoundSprite == null)
                                {
                                    isSoundSprite = false;
                                }
                                else if (isSoundSprite === true)
                                {
                                    throw new Error(`All paths must be maps for a sound sprite.`);
                                }
                            }
                        }
                    }
                }
            }
            return isSoundSprite || false;
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir 
         * @param assetPath 
         * @param variant 
         * @param dry 
         */
        protected async processVariant(outputDir: string, assetPath: string | Util.Map<string>, variant: SoundDefinition, dry: boolean, oldData?: object): Promise<ManifestEntry | null>
        {
            // Check normal sound
            if (Is.string(assetPath))
            {
                this._isSoundSprite = false;
                return await super.processVariant(outputDir, assetPath, variant, dry);
            }

            this._isSoundSprite = true;

            const tmpRawPath = Path.combine(outputDir, `${this.id.replace(".", "_")}.raw`);

            if (dry && oldData)
            {
                // We are doing a dry run (sound asset didn't change) and old data is present from the last run
                // In this case we want to reuse the old data to save time rebuilding the raw sound
                const entry: ManifestEntry = await super.processVariant(outputDir, tmpRawPath, variant, dry);
                (entry.variantData as any).spriteEntries = (oldData as any).spriteEntries;
                return entry;
            }

            const manifestDir = Path.directoryName(this._definition.manifestPath);
            const soundMap: Util.Map<FFMPEG.FFProbe> = {};

            // Iterate all sprite parts
            for (const key in assetPath)
            {
                if (assetPath[key])
                {
                    const sndPath = assetPath[key];
                    const inExt = Path.extension(sndPath);
                    if (supportedInputFormats.indexOf(inExt) === -1)
                    {
                        throw new Error(`Unsupported input format '${inExt}'`);
                    }
                    if (await FileSystem.fileExists(sndPath))
                    {
                        soundMap[key] = await FFMPEG.FFProbe.run(sndPath);
                    }
                    else
                    {
                        Log.warn(`Sound '${sndPath}' does not exist (sound sprite '${this.id}')`);
                    }
                }
            }

            // If there are no sounds in the map, fail the asset
            if (Object.keys(soundMap).length === 0)
            {
                throw new Error("Can't assemble sound sprite without any sound parts");
            }

            // Identify highest common sample rate and channel count
            let sampleRate = 0, channels = 0;
            const sampleSize = 2; // size of a sample in bytes
            this._ssNames = [];
            for (const key in soundMap)
            {
                if (soundMap[key])
                {
                    const strm = FFMPEG.FFProbe.filterStreams(soundMap[key].streams, "audio")[0];
                    sampleRate = Math.max(sampleRate, parseInt(strm.sample_rate));
                    channels = Math.max(channels, strm.channels);
                    this._ssNames.push(key);
                }
            }
            this._ssSampleRate = sampleRate;
            this._ssChannels = channels;

            // Identify amount of silence to insert as padding
            const silencePadding = variant.silencePadding != null ? variant.silencePadding : (this._definition as SoundDefinition).silencePadding;
            const silencePaddingInSamples = Math.ceil((silencePadding||0) * sampleRate);

            // Log.debug(`Generating raw concat'd sound sprite (sampleRate=${sampleRate}, channels=${channels})`);

            // Convert each sound to raw format
            const soundRaw: Util.Map<Buffer> = {};
            let totalRawSoundSize = 0;
            for (const key in soundMap)
            {
                if (assetPath[key])
                {
                    const inPath = assetPath[key];
                    const outPath = Path.replaceExtension(inPath, ".raw");

                    await Command.run("ffmpeg", [
                        "-i", inPath,
                        "-ar", `${sampleRate}`,
                        "-ac", `${channels}`,
                        "-f", `s16le`,
                        "-y",
                        "-loglevel", `error`,
                        outPath
                    ], null, false);
                    FileSystem.cache.set(outPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                    const buf = await FileSystem.readFile(outPath, true);
                    totalRawSoundSize += buf.length;
                    soundRaw[key] = buf;
                    await FileSystem.deletePath(outPath);
                }
            }

            // Concatenate
            const spriteEntries: Util.Map<SoundSpriteEntry> = {};
            // Log.debug(`Total raw buffer byte size: ${totalRawSoundSize}`);

            const sndCount = Object.keys(soundMap).length;
            const totalSize = totalRawSoundSize + (sndCount + 1) * silencePaddingInSamples * sampleSize * channels;
            const rawBuf = Buffer.alloc(totalSize);

            // Position in the buffer in bytes
            let cursorPos = 0;

            // Silence at start
            // buf.fill(0, 0, silencePaddingInSamples * sampleSize);
            cursorPos += silencePaddingInSamples * sampleSize * channels;

            for (const key in soundMap)
            {
                if (soundRaw[key])
                {
                    // Copy sound in
                    const srcBuf = soundRaw[key];
                    srcBuf.copy(rawBuf, cursorPos);
                    spriteEntries[key] =
                    {
                        id: key,
                        startTime: cursorPos / (sampleRate * sampleSize * channels),
                        endTime: (cursorPos + srcBuf.length) / (sampleRate * sampleSize * channels)
                    };
                    cursorPos += srcBuf.length;

                    // Silence at end
                    // buf.fill(cursorPos, 0, silencePaddingInSamples * sampleSize);
                    cursorPos += silencePaddingInSamples * sampleSize * channels;
                }
            }

            // Output buffer to file
            await FileSystem.writeFile(tmpRawPath, rawBuf);

            // Use raw as source asset
            const manifestEntry = await super.processVariant(outputDir, tmpRawPath, variant, dry);

            // Delete raw file
            await FileSystem.deletePath(tmpRawPath);

            // Shove our sound sprite data in
            (manifestEntry.variantData as any).spriteEntries = spriteEntries;

            // Done
            return manifestEntry;
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir 
         * @param assetPath 
         * @param variant 
         * @param dry 
         * @param format 
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: SoundDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { return null; }
            const inExt = Path.extension(assetPath);
            if (supportedInputFormats.indexOf(inExt) === -1 && (inExt !== ".raw" || !this._isSoundSprite))
            {
                Log.error(`Unsupported input format '${inExt}'`, this.id);
                return null;
            }
            if (supportedOutputFormats.indexOf(format) === -1)
            {
                Log.error(`Unsupported output format '${format}'`, this.id);
                return null;
            }
            if (!dry)
            {
                const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath))), `.${format}`);
                await this.tryDeletePath(outputPath);

                // Construct args
                const args = [];
                if (variant.trim != null) { args.push("-ss", `${variant.trim}`); }
                if (this._ssSampleRate != null) { args.push("-ar", `${this._ssSampleRate}`); }
                if (this._ssChannels != null) { args.push("-ac", `${this._ssChannels}`); }
                if (inExt === ".raw") { args.push("-f", "s16le"); }
                args.push("-i", assetPath);
                if (variant.sampleRate != null) { args.push("-ar", `${variant.sampleRate}`); }
                if (variant.bitRate != null) { args.push("-ab", `${variant.bitRate}`); }
                if (variant.channels != null) { args.push("-ac", `${variant.channels}`); }
                args.push("-y");
                args.push("-loglevel", "error");
                args.push(outputPath);

                // Log.debug(args.join(" "));

                // Run command
                await Command.run("ffmpeg", args);
                FileSystem.cache.set(outputPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
            }

            // Return variant data
            return {
                paths: [ Path.replaceExtension(Path.sanitise(assetPath), `.${format}`) ]
            };
        }

        protected async tryDeletePath(path: string)
        {
            if (await FileSystem.fileExists(path))
            {
                await FileSystem.deletePath(path);
            }
        }
    }
}
