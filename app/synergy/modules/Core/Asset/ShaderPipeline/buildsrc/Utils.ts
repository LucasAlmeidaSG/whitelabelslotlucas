namespace RS.ShaderPipeline
{
    export type VisitorFunc<T extends GLSL.Parser.Node> = (node: T) => void;

    /**
     * Visits every node in the hierarchy.
     * @param rootNode 
     * @param visitor 
     */
    export function visit(rootNode: GLSL.Parser.Node, visitor: VisitorFunc<GLSL.Parser.Node>): void;

    /**
     * Visits every node in the hierarchy of the specified type.
     * @param rootNode 
     * @param filter 
     * @param visitor 
     */
    export function visit<TType extends GLSL.Parser.Node.Type>(rootNode: GLSL.Parser.Node, filter: TType, visitor: VisitorFunc<Extract<GLSL.Parser.Node, GLSL.Parser.Node.Base<TType>>>): void;

    export function visit(rootNode: GLSL.Parser.Node, p2: VisitorFunc<GLSL.Parser.Node> | GLSL.Parser.Node.Type, p3?: VisitorFunc<GLSL.Parser.Node>): void
    {
        const visitor = Is.func(p2) ? p2 : p3;
        const filter = Is.func(p2) ? null : p2;
        for (const child of rootNode.children)
        {
            if (filter == null || child.type === filter)
            {
                visitor(child);
            }
            visit(child, filter, visitor);
        }
    }

    export function findScope(node: GLSL.Parser.Node): GLSL.Parser.Scope
    {
        if (node.type === GLSL.Parser.Node.Type.StatementList)
        {
            return node.scope;
        }
        else if (node.parent == null)
        {
            return null;
        }
        else
        {
            return findScope(node.parent);
        }
    }

    export function findScopes(node: GLSL.Parser.Node): GLSL.Parser.Scope[]
    {
        const result: GLSL.Parser.Scope[] = [];
        do
        {
            if (node.type === GLSL.Parser.Node.Type.StatementList)
            {
                result.push(node.scope);
            }
            node = node.parent;
        } while (node != null);
        return result;
    }

    export function searchScope(node: GLSL.Parser.Node, identifier: string): GLSL.Parser.Node.Identifier | null
    {
        const scopes = findScopes(node);
        for (const scope of scopes)
        {
            if (scope[identifier])
            {
                return scope[identifier] as GLSL.Parser.Node.Identifier;
            }
        }
        return null;
    }

    export function findComments(node: GLSL.Parser.Node): string|null
    {
        if (!node.token.preceding) { return null; }
        const result: string[] = [];
        for (const token of node.token.preceding)
        {
            switch (token.type)
            {
                case GLSL.Tokenizer.TokenType.LineComment:
                    result.push(token.data.replace(/\/\//g, "").trim());
                    break;
                case GLSL.Tokenizer.TokenType.BlockComment:
                    result.push(token.data.replace(/\/\*|\*\//g, "").trim());
                    break;
            }
        }
        if (result.length === 0) { return null; }
        return result.join(" ");
    }
}