/// <reference path="../ShaderDirectory.ts" />
/// <reference path="../ShaderFile.ts" />

namespace RS.AssetPipeline.BuildStages
{
    /**
     * Responsible for building all game shaders.
     */
    export class BuildShaders extends BuildStage.Base
    {
        private _globalShaderDirectory: ShaderPipeline.ShaderDirectory;

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            const ourName: string = Object.getPrototypeOf(this).constructor.name;
            Log.pushContext(ourName);

            try
            {
                this._globalShaderDirectory = new ShaderPipeline.ShaderDirectory();

                const shaderExports: Util.Map<ShaderPipeline.ShaderFile.Export[]> = {};

                // Scan all modules and find all shaders
                await super.execute(moduleList);

                const paths = this._globalShaderDirectory.paths;
                if (paths.length === 0) { return { workDone: false, errorCount: 0 }; }

                // Sort shaders to normalise #include dependencies
                const allShaders = new List(this._globalShaderDirectory.paths.map((path) => this._globalShaderDirectory.get(path)));
                allShaders.resolveOrder((item) => [], (item) => item.includes.map((inc) => this._globalShaderDirectory.get(inc)));

                // Compile all shaders
                let compileCnt = 0, errCnt = 0;
                for (const shaderFile of allShaders.data)
                {
                    try
                    {
                        await shaderFile.compile(this._globalShaderDirectory);
                        ++compileCnt;
                        const shaderExportsForModule = shaderExports[shaderFile.ownerModule.name] || (shaderExports[shaderFile.ownerModule.name] = []);
                        for (const shaderExport of shaderFile.exports)
                        {
                            shaderExportsForModule.push(shaderExport);
                        }
                    }
                    catch (err)
                    {
                        Log.error(`Failed to compile shader '${shaderFile.localPath}': ${err}`);
                        ++errCnt;
                    }
                }
                if (errCnt > 0) { return { workDone: true, errorCount: errCnt }; }

                // Write exported shaders
                let exportCnt = 0;
                for (const moduleName in shaderExports)
                {
                    const module = Module.getModule(moduleName);

                    /* tslint:disable:no-string-literal */
                    const ns: string = (module.info["shaderpipeline"] && module.info["shaderpipeline"]["namespace"]) || (module.info.primarynamespace && `${module.info.primarynamespace}.Shaders`) || "Shaders";
                    const refs: string[] = (module.info["shaderpipeline"] && module.info["shaderpipeline"]["refs"]) || [];
                    /* tslint:enable:no-string-literal */

                    const genPath = Path.combine(module.path, "src", "generated", "shaders");
                    await FileSystem.deletePath(genPath);
                    await FileSystem.createPath(genPath);

                    for (const shaderExport of shaderExports[moduleName])
                    {
                        const genFilePath = Path.combine(genPath, `${shaderExport.name.replace(".", "")}.ts`);

                        const formatter = new RS.Build.CodeGeneration.Formatter();
                        for (const ref of refs)
                        {
                            formatter.emitLine(`/// <reference path="${ref}" />`);
                        }
                        formatter.emitLine(`namespace ${ns}`);
                        formatter.enterScope();
                        ShaderPipeline.CodeGen.generateBindings(shaderExport, formatter);
                        formatter.exitScope();
                        await FileSystem.writeFile(genFilePath, formatter.toString());
                        ++exportCnt;
                    }
                }
                Log.info(`Compiled ${compileCnt} glsl files and exported ${exportCnt} shaders`);
            }
            catch (err)
            {
                Log.error(err);
                return { workDone: true, errorCount: 1 };
            }
            finally
            {
                Log.popContext(ourName);
            }

            return { workDone: true, errorCount: 0 };
        }

        // protected determineDidChange(shaderFile: ShaderPipeline.ShaderFile): boolean
        // {
        //     if (shaderFile.sourceDidChange) { return true; }
        //     for (const inc of shaderFile.includes)
        //     {
        //         const incShaderFile = this._globalShaderDirectory.get(inc);
        //         if (incShaderFile && this.determineDidChange(incShaderFile)) { return true; }
        //     }
        //     return false;
        // }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const shadersPath = Path.combine(module.path, "shaders");
            const allFiles = await FileSystem.readFiles(shadersPath);
            for (const file of allFiles)
            {
                if (Path.extension(file) === ".glsl")
                {
                    const shaderFile = new ShaderPipeline.ShaderFile(module, file);
                    await shaderFile.load();
                    this._globalShaderDirectory.set(shaderFile.localPath, shaderFile);
                }
            }

            return { workDone: true, errorCount: 0 };
        }
      
    }

    export const buildShaders = new BuildShaders();
    BuildStage.register({ before: [ RS.BuildStages.compile ] }, buildShaders);
}