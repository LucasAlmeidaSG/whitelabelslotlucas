/// <reference path="ShaderFile.ts" />

/* tslint:disable:no-string-literal */

import CodeGeneration = RS.Build.CodeGeneration;

namespace RS.ShaderPipeline.CodeGen
{
    const uniformTypeMap: Util.Map<string> =
    {
        [ShaderFile.VarType.Float]: "RS.Rendering.ShaderUniform.Float",
        [ShaderFile.VarType.Vec2]: "RS.Rendering.ShaderUniform.Vec2",
        [ShaderFile.VarType.Vec3]: "RS.Rendering.ShaderUniform.Vec3",
        [ShaderFile.VarType.Vec4]: "RS.Rendering.ShaderUniform.Vec4",
        [ShaderFile.VarType.Int]: "RS.Rendering.ShaderUniform.Int",
        [ShaderFile.VarType.IVec2]: "RS.Rendering.ShaderUniform.IVec2",
        [ShaderFile.VarType.IVec3]: "RS.Rendering.ShaderUniform.IVec3",
        [ShaderFile.VarType.IVec4]: "RS.Rendering.ShaderUniform.IVec4",
        [ShaderFile.VarType.Bool]: "RS.Rendering.ShaderUniform.Bool",

        [ShaderFile.VarType.Matrix2x2]: "RS.Rendering.ShaderUniform.Mat2x2",
        [ShaderFile.VarType.Matrix3x3]: "RS.Rendering.ShaderUniform.Mat3x3",
        [ShaderFile.VarType.Matrix4x4]: "RS.Rendering.ShaderUniform.Mat4x4",

        [ShaderFile.VarType.Sampler2D]: "RS.Rendering.ShaderUniform.Sampler2D"
    };
    const uniformArrayTypeMap: Util.Map<string> =
    {
        [ShaderFile.VarType.Float]: "RS.Rendering.ShaderUniform.FloatArray",
        [ShaderFile.VarType.Vec2]: "RS.Rendering.ShaderUniform.Vec2Array",
        [ShaderFile.VarType.Vec3]: "RS.Rendering.ShaderUniform.Vec3Array",
        [ShaderFile.VarType.Vec4]: "RS.Rendering.ShaderUniform.Vec4Array",
        [ShaderFile.VarType.Int]: "RS.Rendering.ShaderUniform.IntArray",
        [ShaderFile.VarType.IVec2]: "RS.Rendering.ShaderUniform.IVec2Array",
        [ShaderFile.VarType.IVec3]: "RS.Rendering.ShaderUniform.IVec3Array",
        [ShaderFile.VarType.IVec4]: "RS.Rendering.ShaderUniform.IVec4Array",
        [ShaderFile.VarType.Bool]: "RS.Rendering.ShaderUniform.BoolArray",

        [ShaderFile.VarType.Matrix2x2]: "RS.Rendering.ShaderUniform.Mat2x2Array",
        [ShaderFile.VarType.Matrix3x3]: "RS.Rendering.ShaderUniform.Mat3x3Array",
        [ShaderFile.VarType.Matrix4x4]: "RS.Rendering.ShaderUniform.Mat4x4Array",

        [ShaderFile.VarType.Sampler2D]: "RS.Rendering.ShaderUniform.Sampler2DArray"
    };

    const validOpTypes: string[] =
    [
        "None",
        "Multiply",
        "Divide",
        "Replace",
        "InverseX",
        "InverseY",
        "InverseXAll",
        "InverseYAll"
    ];

    function getOpType(op: string): string | null
    {
        op = op.toLowerCase();
        for (const validOp of validOpTypes)
        {
            if (validOp.toLowerCase() === op)
            {
                return validOp;
            }
        }
        return null;
    }

    export function generateBindings(shaderExport: ShaderFile.Export, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        const shaderNamespace = shaderExport.name.split(".");
        const shaderName = shaderNamespace.pop();

        formatter.emitLine("/* tslint:disable:no-string-literal */");

        if (shaderExport.vertexSrc)
        {
            formatter.emitLine(`const shaderVertexSrc = \`${shaderExport.vertexSrc}\`;`);
        }
        if (shaderExport.fragmentSrc)
        {
            formatter.emitLine(`const shaderFragmentSrc = \`${shaderExport.fragmentSrc}\`;`);
        }

        const switchNames = Object.keys(shaderExport.switches);
        const hasSwitches = switchNames.length > 0;
        const allSwitchesOptional = !switchNames
            .map((name) => shaderExport.switches[name])
            .some((switchData) => !("default" in switchData.metaData));

        // Shader program class
        if (shaderExport.isFilter)
        {
            formatter.emitLine(`class ${shaderName}ShaderProgram extends RS.Rendering.ShaderProgram`);
        }
        else
        {
            formatter.emitLine(`export class ${shaderName}ShaderProgram extends RS.Rendering.ShaderProgram`);
        }
        formatter.enterScope();

        const ctorParams: string[] = [];
        if (hasSwitches)
        {
            formatter.emitLine(`protected _settings: ${shaderName}ShaderProgram.Settings;`);
            if (allSwitchesOptional)
            {
                ctorParams.push(`settings: Partial<${shaderName}ShaderProgram.Settings> = {}`);
            }
            else
            {
                ctorParams.push(`settings: ${shaderName}ShaderProgram.Settings`);
            }
        }
        if (shaderExport.dynamic)
        {
            ctorParams.push(
                `vertexModifier?: (src: string) => string`,
                `fragmentModifier?: (src: string) => string`
            );
        }
        formatter.emitLine(`public constructor(${ctorParams.join(", ")})`);
        formatter.enterScope();

        const settingsObj: Build.CodeGeneration.Value =
        {
            kind: "objval",
            name: this.id,
            values: []
        };
        if (shaderExport.vertexSrc)
        {
            if (shaderExport.dynamic)
            {
                settingsObj.values.push({ kind: "keyval", name: "vertexSource", value: `vertexModifier ? vertexModifier(shaderVertexSrc) : shaderVertexSrc` });
            }
            else
            {
                settingsObj.values.push({ kind: "keyval", name: "vertexSource", value: `shaderVertexSrc` });
            }
        }
        if (shaderExport.fragmentSrc)
        {
            if (shaderExport.dynamic)
            {
                settingsObj.values.push({ kind: "keyval", name: "fragmentSource", value: `fragmentModifier ? fragmentModifier(shaderFragmentSrc) : shaderFragmentSrc` });
            }
            else
            {
                settingsObj.values.push({ kind: "keyval", name: "fragmentSource", value: `shaderFragmentSrc` });
            }
        }
        if (hasSwitches)
        {
            const definesObj: Build.CodeGeneration.Value =
            {
                kind: "objval",
                name: this.id,
                values: []
            };
            for (const switchName of switchNames)
            {
                const switchData = shaderExport.switches[switchName];
                const propName = (switchData.metaData["property"] as string) || cleanSwitchName(switchName);
                const defVal = switchData.metaData["default"] || "undefined";
                definesObj.values.push({
                    kind: "keyval",
                    name: `"${switchName}"`,
                    value: `settings.${propName} === true ? "1" : settings.${propName} === false ? undefined : ${defVal}`
                });
            }
            settingsObj.values.push({ kind: "keyval", name: "defines", value: definesObj });
        }

        formatter.emitLine("super(");

        formatter.emitNamed(settingsObj);

        formatter.emitLine(");");

        formatter.exitScope();

        formatter.emitLine(`public createShader(): ${shaderName}Shader`);
        formatter.enterScope();
        formatter.emitLine(`return new ${shaderName}Shader(this);`);
        formatter.exitScope();

        formatter.exitScope();

        // Shader program settings
        if (hasSwitches)
        {
            formatter.emitLine(`export namespace ${shaderName}ShaderProgram`);
            formatter.enterScope();

            const interfaceObj: CodeGeneration.Interface =
            {
                kind: "interface",
                name: "Settings",
                properties: []
            };
            for (const switchName of switchNames)
            {
                const switchData = shaderExport.switches[switchName];
                const propName = (switchData.metaData["property"] as string) || cleanSwitchName(switchName);
                interfaceObj.properties.push({
                    kind: "propertydeclr",
                    type: { kind: CodeMeta.TypeRef.Kind.Raw, name: "boolean" },
                    name: propName,
                    optional: ("default" in switchData.metaData) && !allSwitchesOptional
                });
            }
            formatter.emitNamed(interfaceObj);

            formatter.exitScope();
        }

        // Shader class
        if (shaderExport.isFilter)
        {
            formatter.emitLine(`class ${shaderName}Shader extends RS.Rendering.Shader`);
        }
        else
        {
            formatter.emitLine(`export class ${shaderName}Shader extends RS.Rendering.Shader`);
        }
        formatter.enterScope();

        let hasTimescale = false;

        // Typed uniforms
        for (const uniformName in shaderExport.uniforms)
        {
            const uniformData = shaderExport.uniforms[uniformName];
            const tsType = uniformData.dimension > 1 ? uniformArrayTypeMap[uniformData.type] : uniformTypeMap[uniformData.type];
            if (tsType == null)
            {
                Log.warn(`${shaderExport.name}: Unsupported uniform type '${uniformData.type}', uniform '${uniformName}' will not be exposed via the Shader class.`);
            }
            else
            {
                formatter.emitLine(`protected _${uniformName}: ${tsType};`);
            }
            if (uniformData.metaData["timescale"])
            {
                hasTimescale = true;
            }
        }

        if (hasTimescale) { formatter.emitLine("protected _enabled: boolean = false;"); }

        // Generate uniform API
        for (const uniformName in shaderExport.uniforms)
        {
            const uniformData = shaderExport.uniforms[uniformName];
            const tsType = uniformTypeMap[uniformData.type];
            if (tsType != null && !uniformData.metaData["private"])
            {
                if (Is.string(uniformData.metaData["property"]))
                {
                    emitUniformGetterSetter(uniformName, uniformData.metaData["property"] as string, uniformData, formatter);
                }
                else
                {
                    const cleanName = cleanUniformName(uniformName);
                    emitUniformGetterSetter(uniformName, cleanName, uniformData, formatter);
                }
            }
        }

        if (hasTimescale)
        {
            formatter.emitLine("/** Gets or sets if the shader is enabled (time uniforms are only advanced when enabled). */");
            formatter.emitLine("public get enabled() { return this._enabled; }");
            formatter.emitLine("public set enabled(value)");
            formatter.enterScope();
            formatter.emitLine("if (value === this._enabled) { return; }");
            formatter.emitLine("this._enabled = value;");
            formatter.emitLine("if (this._enabled)");
            formatter.enterScope();
            formatter.emitLine("RS.Ticker.registerTickers(this);");
            formatter.exitScope();
            formatter.emitLine("else");
            formatter.enterScope();
            formatter.emitLine("RS.Ticker.unregisterTickers(this);");
            formatter.exitScope();
            formatter.exitScope();
        }

        formatter.emitLine(`public constructor(shaderProgram: ${shaderName}ShaderProgram)`);
        formatter.enterScope();

        formatter.emitLine(`super(shaderProgram);`);

        // Populate typed uniforms
        for (const uniformName in shaderExport.uniforms)
        {
            const uniformData = shaderExport.uniforms[uniformName];
            const tsType = uniformData.dimension > 1 ? uniformArrayTypeMap[uniformData.type] : uniformTypeMap[uniformData.type];
            if (tsType != null)
            {
                formatter.emitLine(`this._${uniformName} = this.uniforms["${uniformName}"] as ${tsType};`);
                if (uniformData.metaData["inputsizeop"])
                {
                    const op = getOpType(uniformData.metaData["inputsizeop"] as string);
                    if (op == null)
                    {
                        Log.warn(`${shaderExport.name}: Invalid op type '${uniformData.metaData["inputsizeop"]}' for uniform '${uniformName}'`);
                    }
                    else
                    {
                        formatter.emitLine(`this._${uniformName}.inputSizeOp = RS.Rendering.ShaderUniform.Op.${op};`);
                    }
                }
                if (uniformData.metaData["inputsizeop:rt"])
                {
                    const op = getOpType(uniformData.metaData["inputsizeop:rt"] as string);
                    if (op == null)
                    {
                        Log.warn(`${shaderExport.name}: Invalid op type '${uniformData.metaData["inputsizeop:rt"]}' for uniform '${uniformName}'`);
                    }
                    else
                    {
                        formatter.emitLine(`this._${uniformName}.inputSizeOpRT = RS.Rendering.ShaderUniform.Op.${op};`);
                    }
                }
                if (uniformData.metaData["targetsizeop"])
                {
                    const op = getOpType(uniformData.metaData["targetsizeop"] as string);
                    if (op == null)
                    {
                        Log.warn(`${shaderExport.name}: Invalid op type '${uniformData.metaData["targetsizeop"]}' for uniform '${uniformName}'`);
                    }
                    else
                    {
                        formatter.emitLine(`this._${uniformName}.targetSizeOp = RS.Rendering.ShaderUniform.Op.${op};`);
                    }
                }
                if (uniformData.metaData["targetsizeop:rt"])
                {
                    const op = getOpType(uniformData.metaData["targetsizeop:rt"] as string);
                    if (op == null)
                    {
                        Log.warn(`${shaderExport.name}: Invalid op type '${uniformData.metaData["targetsizeop:rt"]}' for uniform '${uniformName}'`);
                    }
                    else
                    {
                        formatter.emitLine(`this._${uniformName}.targetSizeOpRT = RS.Rendering.ShaderUniform.Op.${op};`);
                    }
                }
            }
        }

        // Set defaults
        for (const uniformName in shaderExport.uniforms)
        {
            const uniformData = shaderExport.uniforms[uniformName];
            const tsType = uniformTypeMap[uniformData.type];
            if (tsType != null && uniformData.metaData["default"])
            {
                emitDefaultSet(uniformName, uniformData, `${uniformData.metaData["default"]}`, formatter);
            }
        }

        formatter.exitScope();

        //add clone function
        formatter.emitLine(`public clone(): ${shaderName}Shader`);
        formatter.enterScope();
        formatter.emitLine(`const clone = this.shaderProgram.createShader() as ${shaderName}Shader;`);
        formatter.emitLine(`RS.Util.assign(this.uniforms, clone.uniforms, false);`);
        for (const uniformName in shaderExport.uniforms)
        {
            formatter.emitLine(`clone._${uniformName} = this._${uniformName};`);
        }
        formatter.emitLine("return clone;");
        formatter.exitScope();

        if (hasTimescale)
        {
            formatter.emitLine("public dispose(): void");
            formatter.enterScope();
            formatter.emitLine("if (this.isDisposed) { return; }");
            formatter.emitLine("this.enabled = false;");
            formatter.emitLine("super.dispose();");
            formatter.exitScope();

            formatter.emitLine("@RS.Tick({ })");
            formatter.emitLine("protected tick(ev: RS.Ticker.Event): void");
            formatter.enterScope();
            formatter.emitLine("const deltaTime = ev.delta / 1000;");
            for (const uniformName in shaderExport.uniforms)
            {
                const uniformData = shaderExport.uniforms[uniformName];
                const tsType = uniformTypeMap[uniformData.type];
                if (tsType != null && uniformData.metaData["timescale"])
                {
                    const ts = parseFloat(`${uniformData.metaData["timescale"]}`);
                    if (!Is.number(ts))
                    {
                        Log.warn(`Invalid timescale for uniform '${uniformName}'`);
                    }
                    else
                    {
                        formatter.emitLine(`this._${uniformName}.value += deltaTime * ${ts};`);
                    }
                }
            }
            formatter.exitScope();
        }

        formatter.exitScope();

        // Filter class
        if (shaderExport.isFilter)
        {
            formatter.emitLine(`export class ${shaderName}Filter extends RS.Rendering.Filter`);
            formatter.enterScope();

            if (hasSwitches)
            {
                formatter.emitLine(`protected _settings: ${shaderName}ShaderProgram.Settings;`);
                formatter.emitLine(`@RS.AutoDispose private _shaderProgram: ${shaderName}ShaderProgram;`);
            }
            else
            {
                formatter.emitLine(`private static _shaderProgram: ${shaderName}ShaderProgram;`);
            }

            formatter.emitLine(`@RS.AutoDispose private _shader: ${shaderName}Shader;`);

            // Generate uniform API
            for (const uniformName in shaderExport.uniforms)
            {
                const uniformData = shaderExport.uniforms[uniformName];
                const tsType = uniformTypeMap[uniformData.type];
                if (tsType != null && !uniformData.metaData["private"])
                {
                    if (Is.string(uniformData.metaData["property"]))
                    {
                        emitProxyUniformGetterSetter(uniformData.metaData["property"] as string, uniformData, `this._shader`, formatter);
                    }
                    else
                    {
                        const cleanName = cleanUniformName(uniformName);
                        emitProxyUniformGetterSetter(cleanName, uniformData, `this._shader`, formatter);
                    }
                }
            }

            if (hasSwitches)
            {
                if (allSwitchesOptional)
                {
                    formatter.emitLine(`public constructor(settings: Partial<${shaderName}ShaderProgram.Settings> = {})`);
                }
                else
                {
                    formatter.emitLine(`public constructor(settings: ${shaderName}ShaderProgram.Settings)`);
                }
            }
            else
            {
                formatter.emitLine("public constructor()");
            }
            formatter.enterScope();

            if (hasSwitches)
            {
                formatter.emitLine(`const shaderProgram = new ${shaderName}ShaderProgram(settings);`);
                formatter.emitLine(`const shader = shaderProgram.createShader();`);
            }
            else
            {
                formatter.emitLine(`if (${shaderName}Filter._shaderProgram == null) { ${shaderName}Filter._shaderProgram = new ${shaderName}ShaderProgram(); }`);
                formatter.emitLine(`const shader = ${shaderName}Filter._shaderProgram.createShader();`);
            }
            formatter.emitLine(`super(shader);`);
            if (hasSwitches)
            {
                formatter.emitLine(`this._shaderProgram = shaderProgram;`);
            }
            formatter.emitLine(`this._shader = shader;`);

            formatter.exitScope();

            formatter.exitScope();
        }

        return;
    }

    function cleanUniformName(uniformName: string): string
    {
        if (uniformName[0] === "u") { uniformName = uniformName.substr(1); }
        return `${uniformName[0].toLowerCase()}${uniformName.substr(1)}`;
    }

    function cleanSwitchName(switchName: string): string
    {
        const spl = switchName
            .split("_")
            .map((seg) => seg.toLowerCase());
        for (let i = 1, l = spl.length; i < l; ++i)
        {
            spl[i] = `${spl[i][0].toUpperCase()}${spl[i].substr(1)}`;
        }
        return spl.join("");
    }

    function emitProxyUniformGetterSetter(propertyName: string, uniformData: ShaderFile.VarDetails, target: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        if (uniformData.comment)
        {
            formatter.emitLine(`/** ${uniformData.comment} */`);
        }
        formatter.emitLine(`public get ${propertyName}() { return ${target}.${propertyName}; }`);
        formatter.emitLine(`public set ${propertyName}(value) { ${target}.${propertyName} = value; }`);
    }

    function emitUniformGetterSetter(uniformName: string, propertyName: string, uniformData: ShaderFile.VarDetails, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        if (uniformData.comment)
        {
            formatter.emitLine(`/** ${uniformData.comment} */`);
        }
        if (uniformData.dimension > 1)
        {
            switch (uniformData.type)
            {
                case ShaderFile.VarType.Float:
                case ShaderFile.VarType.Int:
                    emitArrayUniformGetterSetter(uniformName, propertyName, "number", formatter);
                    break;
                case ShaderFile.VarType.Bool:
                    emitBooleanArrayUniformGetterSetter(uniformName, propertyName, "boolean", formatter);
                    break;
                case ShaderFile.VarType.Sampler2D:
                    emitArrayUniformGetterSetter(uniformName, propertyName, "RS.Rendering.ITexture | RS.Asset.ImageReference | null", formatter);
                    break;
                case ShaderFile.VarType.Vec2:
                case ShaderFile.VarType.IVec2:
                    emitArrayUniformGetterSetter(uniformName, propertyName, "RS.Math.Vector2D", formatter);
                    break;
                case ShaderFile.VarType.Vec3:
                case ShaderFile.VarType.IVec3:
                    if (uniformData.metaData["color"])
                    {
                        // emitArrayUniformGetterSetter(uniformName, propertyName, "RS.Util.Color", formatter);
                        Log.warn(`Color arrays are unsupported, uniform '${uniformName}' will not be exposed via the Shader class.`);
                    }
                    else
                    {
                        emitArrayUniformGetterSetter(uniformName, propertyName, "RS.Math.Vector3D", formatter);
                    }
                    break;
                case ShaderFile.VarType.Vec4:
                case ShaderFile.VarType.IVec4:
                    if (uniformData.metaData["color"])
                    {
                        // emitArrayUniformGetterSetter(uniformName, propertyName, "RS.Util.Color", formatter);
                        Log.warn(`Color arrays are unsupported, uniform '${uniformName}' will not be exposed via the Shader class.`);
                    }
                    else
                    {
                        emitArrayUniformGetterSetter(uniformName, propertyName, "RS.Math.Vector4D", formatter);
                    }
                    break;
                default:
                    Log.warn(`Unsupported uniform type '${uniformData.type}' (array), uniform '${uniformName}' will not be exposed via the Shader class.`);
                    break;
            }
        }
        else
        {
            switch (uniformData.type)
            {
                case ShaderFile.VarType.Float:
                case ShaderFile.VarType.Int:
                    emitPrimitiveUniformGetterSetter(uniformName, propertyName, "number", formatter);
                    break;
                case ShaderFile.VarType.Bool:
                    emitBooleanUniformGetterSetter(uniformName, propertyName, "boolean", formatter);
                    break;
                case ShaderFile.VarType.Sampler2D:
                    emitPrimitiveUniformGetterSetter(uniformName, propertyName, "RS.Rendering.ITexture | RS.Asset.ImageReference | null", formatter);
                    break;
                case ShaderFile.VarType.Vec2:
                case ShaderFile.VarType.IVec2:
                    emitStructLikeUniformGetterSetter(uniformName, propertyName, "RS.Math.Vector2D", formatter);
                    break;
                case ShaderFile.VarType.Vec3:
                case ShaderFile.VarType.IVec3:
                    if (uniformData.metaData["color"])
                    {
                        emitColorUniformGetterSetter(uniformName, propertyName, "vec3", formatter);
                    }
                    else
                    {
                        emitStructLikeUniformGetterSetter(uniformName, propertyName, "RS.Math.Vector3D", formatter);
                    }
                    break;
                case ShaderFile.VarType.Vec4:
                case ShaderFile.VarType.IVec4:
                    if (uniformData.metaData["color"])
                    {
                        emitColorUniformGetterSetter(uniformName, propertyName, "vec4", formatter);
                    }
                    else
                    {
                        emitStructLikeUniformGetterSetter(uniformName, propertyName, "RS.Math.Vector4D", formatter);
                    }
                    break;
                case ShaderFile.VarType.Matrix3x3:
                    emitStructLikeUniformGetterSetter(uniformName, propertyName, "RS.Math.Matrix3", formatter);
                    break;
                case ShaderFile.VarType.Matrix4x4:
                    emitStructLikeUniformGetterSetter(uniformName, propertyName, "RS.Math.Matrix4", formatter);
                    break;
                default:
                    Log.warn(`Unsupported uniform type '${uniformData.type}', uniform '${uniformName}' will not be exposed via the Shader class.`);
                    break;
            }
        }
    }

    function emitBooleanUniformGetterSetter(uniformName: string, propertyName: string, primitiveType: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        // Boolean uniforms have a number as value, so we need to convert to/from booleans.
        formatter.emitLine(`public get ${propertyName}(): ${primitiveType} { return this._${uniformName}.value === 1; }`);
        formatter.emitLine(`public set ${propertyName}(value: ${primitiveType}) { this._${uniformName}.value = value ? 1 : 0; }`);
    }

    function emitBooleanArrayUniformGetterSetter(uniformName: string, propertyName: string, primitiveType: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        // Boolean array uniforms have a number array as value, so we need to convert to/from booleans.
        formatter.emitLine(`public get ${propertyName}(): ${primitiveType} { return this._${uniformName}.value.map((i) => i === 1); }`);
        formatter.emitLine(`public set ${propertyName}(value: ${primitiveType}) { this._${uniformName}.value = value.map((i) => i ? 1 : 0; }`);
    }

    function emitPrimitiveUniformGetterSetter(uniformName: string, propertyName: string, primitiveType: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        formatter.emitLine(`public get ${propertyName}(): ${primitiveType} { return this._${uniformName}.value; }`);
        formatter.emitLine(`public set ${propertyName}(value: ${primitiveType}) { this._${uniformName}.value = value; }`);
    }

    function emitStructLikeUniformGetterSetter(uniformName: string, propertyName: string, structType: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        formatter.emitLine(`public get ${propertyName}(): Readonly<${structType}> { return this._${uniformName}.value; }`);
        formatter.emitLine(`public set ${propertyName}(value: Readonly<${structType}>) { this._${uniformName}.value = { ...value }; }`);
    }

    function emitColorUniformGetterSetter(uniformName: string, propertyName: string, accessor: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        formatter.emitLine(`public get ${propertyName}(): RS.Util.Color { return new RS.Util.Color(this._${uniformName}.value); }`);
        formatter.emitLine(`public set ${propertyName}(value: RS.Util.Color) { this._${uniformName}.value = value.${accessor}; }`);
    }

    function emitArrayUniformGetterSetter(uniformName: string, propertyName: string, innerType: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        formatter.emitLine(`public get ${propertyName}(): ReadonlyArray<${innerType}> { return this._${uniformName}.value; }`);
        formatter.emitLine(`public set ${propertyName}(value: ReadonlyArray<${innerType}>) { this._${uniformName}.value = [...value]; }`);
    }

    function emitDefaultSet(uniformName: string, uniformData: ShaderFile.VarDetails, defaultVal: string, formatter: RS.Build.CodeGeneration.Formatter): void
    {
        /* tslint:disable:no-shadowed-variable */
        switch (uniformData.type)
        {
            case ShaderFile.VarType.Float:
            {
                const defVal = parseFloat(defaultVal);
                if (!Is.number(defVal))
                {
                    Log.warn(`Default value '${defaultVal}' for uniform '${uniformName}' of type '${uniformData.type}' is invalid`);
                }
                else
                {
                    formatter.emitLine(`this._${uniformName}.value = ${defVal};`);
                }
                break;
            }
            case ShaderFile.VarType.Bool:
            {
                const defVal = defaultVal === "true" || defaultVal === "1";
                formatter.emitLine(`this._${uniformName}.value = ${defVal};`);
                break;
            }
            case ShaderFile.VarType.Vec2:
            case ShaderFile.VarType.IVec2:
            {
                emitStructLikeDefaultSet(uniformName, "RS.Math.Vector2D", 2, defaultVal, formatter);
                break;
            }
            case ShaderFile.VarType.Vec3:
            case ShaderFile.VarType.IVec3:
            {
                if (uniformData.metaData["color"])
                {
                    emitStructLikeDefaultSet(uniformName, "new RS.Util.Color", 3, defaultVal, formatter);
                }
                else
                {
                    emitStructLikeDefaultSet(uniformName, "RS.Math.Vector3D", 3, defaultVal, formatter);
                }
                break;
            }
            case ShaderFile.VarType.Vec4:
            case ShaderFile.VarType.IVec4:
            {
                if (uniformData.metaData["color"])
                {
                    emitStructLikeDefaultSet(uniformName, "new RS.Util.Color", 3, defaultVal, formatter);
                }
                else
                {
                    emitStructLikeDefaultSet(uniformName, "RS.Math.Vector4D", 4, defaultVal, formatter);
                }
                break;
            }
            default:
                Log.warn(`Unsupported uniform type '${uniformData.type}', uniform '${uniformName}' will not have a default value set.`);
                break;
        }
        /* tslint:enable:no-shadowed-variable */
    }

    function emitStructLikeDefaultSet(uniformName: string, ctor: string, arrLen: number, defaultVal: string, formatter: RS.Build.CodeGeneration.Formatter)
    {
        let finalArr: number[] | null = null;
        if (defaultVal.indexOf(",") !== -1)
        {
            const defVal = defaultVal.split(",").map((item) => parseFloat(item));
            if (!Is.arrayOf(defVal, Is.number))
            {
                Log.warn(`Default value '${defaultVal}' for uniform '${uniformName}' is invalid`);
            }
            else if (defVal.length === arrLen)
            {
                finalArr = defVal;
            }
            else
            {
                finalArr = new Array<number>(arrLen);
                for (let i = 0; i < arrLen; ++i)
                {
                    finalArr[i] = defVal[i % defVal.length] || 0;
                }
            }
        }
        else
        {
            const defVal = parseFloat(defaultVal);
            if (!Is.number(defVal))
            {
                Log.warn(`Default value '${defaultVal}' for uniform '${uniformName}' is invalid`);
            }
            else
            {
                finalArr = new Array<number>(arrLen);
                for (let i = 0; i < arrLen; ++i)
                {
                    finalArr[i] = defVal;
                }
            }
        }
        if (!finalArr) { return; }
        formatter.emitLine(`this._${uniformName}.value = ${ctor}(${finalArr.map((item) => `${item}`).join(", ")});`);
    }
}
