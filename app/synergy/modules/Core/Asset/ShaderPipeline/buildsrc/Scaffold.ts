namespace RS.Build.Scaffolds
{
    const shaderTemplate = `precision highp float;

attribute vec2 aVertexPosition;

attribute vec4 aColor;
varying vec4 vColor;

attribute vec2 aTextureCoord;
varying vec2 vTextureCoord;

attribute float aTextureId;
varying float vTextureId;

#pragma meta private
uniform sampler2D uSampler;

#pragma meta private
uniform mat3 projectionMatrix;

void {{shaderName:CamelCase}}Vertex()
{
    gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
    vTextureCoord = aTextureCoord;
    vTextureId = aTextureId;
    vColor = aColor;
}

void {{shaderName:CamelCase}}Fragment()
{
    vec4 color;
    float textureId = floor(vTextureId + 0.5);
    if (textureId == 0.0)
    {
        color = texture2D(uSampler, vTextureCoord);
    }
    gl_FragColor = color * vColor;
}

#pragma export {{shaderName:CamelCase}} Fragment={{shaderName:CamelCase}}Fragment Vertex={{shaderName:CamelCase}}Vertex`;

    export const shader = new Scaffold();
    shader.addTemplate(shaderTemplate, "shaders/{{shaderName:CamelCase}}.glsl");
    shader.addParam("shaderName", "Shader Name", "MyShader");
    registerScaffold("shader", shader);
}