namespace RS.AssetPipeline
{
    /**
     * Shader asset specific definition data.
     */
    /*export interface ShaderDefinition extends BaseVariant
    {
        
    }*/

    /**
     * Represents an shader-type asset.
     */
    @AssetType("shader")
    @InferFromExtensions([".ps", ".vs", ".fs"])
    export class Shader extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "glsl" ]; }

        public get classModule() { return "Core.Asset.ShaderPipeline"; }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitAssetReference(this, "RS.Asset.ShaderReference");
        }
        /**
         * Processes a single format for a single asset variant.
         * @param outputDir 
         * @param assetPath 
         * @param variant 
         * @param dry 
         * @param format 
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            if (format != "glsl")
            {
                Log.error(`Unsupported format '${format}'`);
                return null;
            }
            if (!dry)
            {
                const outputPath = Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath)));

                // Just copy it over
                await FileSystem.copyFile(assetPath, outputPath);
            }

            // Return variant data
            return {
                paths: [ Path.sanitise(assetPath) ]
            };
        }
    }
}