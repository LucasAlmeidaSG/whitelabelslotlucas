namespace RS.ShaderPipeline
{
    /**
     * Encapsulates a virtual file system of shader files.
     */
    export class ShaderDirectory
    {
        private _map: Util.Map<ShaderFile> = {};

        public get paths() { return Object.keys(this._map); }

        public get(path: string): ShaderFile | null
        {
            return this._map[path] || null;
        }

        public set(path: string, shaderFile: ShaderFile): void
        {
            this._map[path] = shaderFile;
        }
    }
}