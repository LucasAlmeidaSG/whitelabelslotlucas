namespace RS.ShaderPipeline
{
    const glslTokenString: GLSL.Tokenizer.StringTokenizer = require("glsl-tokenizer/string");
    const glslParseTokens: GLSL.Parser.DirectParser = require("glsl-parser/direct");

    /**
     * Encapsulates a single shader file.
     */
    export class ShaderFile
    {
        private _module: Module.Base;
        private _path: string;
        private _state: ShaderFile.State;

        private _src: string | null = null;
        private _rawTokens: GLSL.Tokenizer.Token[] | null = null;
        private _includes: string[] | null = null;
        private _srcChecksum: string | null = null;
        private _compiledTokens: GLSL.Tokenizer.Token[] | null = null;
        private _exports: ShaderFile.Export[] | null = null;
        private _functionData: Util.Map<ShaderFile.FunctionClassification> | null = null;
        private _varyingData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _uniformData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _attributeData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _constData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _structData: Util.Map<ShaderFile.StructClassification> | null = null;
        private _switchData: Util.Map<ShaderFile.SwitchDetails> | null = null;
        private _localVaryingData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _localUniformData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _localAttributeData: Util.Map<ShaderFile.VarDetails> | null = null;
        private _localConstData: Util.Map<ShaderFile.VarDetails> | null = null;

        public get ownerModule() { return this._module; }
        public get path() { return this._path; }
        public get localPath() {  return Path.relative(Path.combine(this._module.path, "shaders"), this._path); }
        public get sourceDidChange() { return this._module.checksumDB.get(`shader:${this.localPath}`) !== this.sourceChecksum; }
        public get source() { return this._src; }
        public get includes() { return this._includes; }
        public get sourceChecksum() { return this._srcChecksum; }
        public get exports() { return this._exports; }
        public get state() { return this._state; }

        public constructor(ownerModule: Module.Base, path: string)
        {
            this._module = ownerModule;
            this._path = path;
            this._state = ShaderFile.State.Unloaded;
        }

        /**
         * Loads the shader file.
         */
        public async load()
        {
            if (this._state !== ShaderFile.State.Unloaded) { return; }

            this._src = await FileSystem.readFile(this._path);
            this._srcChecksum = Checksum.getSimple(this._src);

            this._rawTokens = glslTokenString(this._src);
            this._includes = [];
            for (const token of this._rawTokens)
            {
                token.file = this._path;
                if (token.type === GLSL.Tokenizer.TokenType.Preprocessor)
                {
                    const splitBySpace = token.data.split(/\s+/g).map((item) => item.trim());
                    if (splitBySpace[0].toLowerCase() === "#include")
                    {
                        for (let i = 1, l = splitBySpace.length; i < l; ++i)
                        {
                            const item = splitBySpace[i];
                            const match = /"(.+)"/g.exec(item);
                            if (!match || match.length <= 1)
                            {
                                Log.warn(`Tried to include invalid path '${item}'`);
                            }
                            else
                            {
                                this._includes.push(match[1].replace(/\\|\//, Path.seperator));
                            }
                        }
                    }
                }
            }

            this._state = ShaderFile.State.Loaded;
        }

        /**
         * Compiles the shader file.
         * @param dir 
         */
        public async compile(dir: ShaderDirectory)
        {
            if (this._state === ShaderFile.State.Unloaded)
            {
                throw new Error("Can't compile shader when it's unloaded");
            }

            // Resolve #includes
            this._compiledTokens = this.resolveIncludes(dir);

            // Parse
            const ast = glslParseTokens(this._compiledTokens);
            // const cache: object[] = [];
            // const debugAst = JSON.stringify(ast, (key, val) =>
            // {
            //     if (Is.object(val))
            //     {
            //         if (cache.indexOf(val) === -1)
            //         {
            //             cache.push(val);
            //             return val;
            //         }
            //         else
            //         {
            //             return;
            //         }
            //     }
            //     else
            //     {
            //         return val;
            //     }
            // });
            // await FileSystem.writeFile(Path.replaceExtension(this._path, ".json"), debugAst);

            try
            {
                // Analyse AST
                this.classifyStructs(ast);
                this.classifyFunctions(ast);
                this.classifyVariables(ast);

                // Resolve exports
                this._exports = this.resolveExports(ast);
            }
            catch (err)
            {
                Log.error(err.stack || err);
                throw err;
            }
            
            // Compiled
            this._state = ShaderFile.State.Compiled;
        }

        protected classifyFunctions(ast: GLSL.Parser.Node.StatementList): void
        {
            this._functionData = {};
            for (const child of ast.children)
            {
                if (child.type === GLSL.Parser.Node.Type.Statement)
                {
                    if (child.children[0].type === GLSL.Parser.Node.Type.Declaration)
                    {
                        const declr = child.children[0];
                        for (const subChild of declr.children)
                        {
                            if (subChild.type === GLSL.Parser.Node.Type.Function)
                            {
                                this._functionData[subChild.children[0].data] = this.classifyFunction(subChild);
                            }
                        }
                    }
                }
            }
        }

        protected classifyVariables(ast: GLSL.Parser.Node.StatementList): void
        {
            this._varyingData = {};
            this._uniformData = {};
            this._attributeData = {};
            this._constData = {};
            this._switchData = {};
            let metaData: Util.Map<string | boolean> | null = null;
            let comment: string | null = null;
            for (const child of ast.children)
            {
                if (child.type === GLSL.Parser.Node.Type.Preprocessor)
                {
                    // #pragma meta key1=value key2=value ...
                    const splitBySpace = child.token.data.split(/\s+/g).map((item) => item.trim());
                    if (splitBySpace.length >= 2 && splitBySpace[0].toLowerCase() === "#pragma")
                    {
                        switch (splitBySpace[1].toLowerCase())
                        {
                            case "meta":
                                metaData = this.parseKeyValues(splitBySpace.slice(2));
                                break;
                            case "switch":
                                if (splitBySpace.length < 3)
                                {
                                    Log.warn(`${child.token.file}:${child.token.line} - Switch declaration has no name`);
                                }
                                else
                                {
                                    this._switchData[splitBySpace[2]] =
                                    {
                                        comment,
                                        metaData: metaData || {}
                                    };
                                }
                                metaData = null;
                                comment = null;
                                break;
                        }
                    }
                    comment = findComments(child);
                }
                else if (child.type === GLSL.Parser.Node.Type.Statement)
                {
                    // Check for declaration
                    const subChild = child.children[0];
                    if (subChild && subChild.type === GLSL.Parser.Node.Type.Declaration)
                    {
                        comment = comment || findComments(child);
                        const keywords = subChild.children.filter((c) => c.type === GLSL.Parser.Node.Type.Keyword) as GLSL.Parser.Node.Keyword[];
                        const declList = subChild.children.filter((c) => c.type === GLSL.Parser.Node.Type.DeclarationList)[0];
                        if (keywords.length > 0 && declList)
                        {
                            for (let i = 0, l = declList.children.length; i < l; ++i)
                            {
                                const ident = declList.children[i];
                                if (ident.type === GLSL.Parser.Node.Type.Identifier)
                                {
                                    const varDetails = this.identifyVarDetails(subChild, ident.data);
                                    varDetails.metaData = { ...varDetails.metaData, ...metaData };
                                    if (comment) { varDetails.comment = comment; }
                                    if (keywords[0] && keywords[0].token)
                                    {
                                        switch (keywords[0].token.data)
                                        {
                                            case "uniform":
                                                this._uniformData[ident.data] = varDetails;
                                                break;
                                            case "attribute":
                                                this._attributeData[ident.data] = varDetails;
                                                break;
                                            case "varying":
                                                this._varyingData[ident.data] = varDetails;
                                                break;
                                            case "const":
                                                const expr = declList.children[i + 1];
                                                if (i + 1 < l && expr.type === GLSL.Parser.Node.Type.Expression)
                                                {
                                                    varDetails.staticValue = this.evaluateStaticExpression(expr);
                                                }
                                                this._constData[ident.data] = varDetails;
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    metaData = null;
                    comment = null;
                }
            }
        }

        protected classifyStructs(ast: GLSL.Parser.Node): void
        {
            this._structData = {};
            function isStruct(node: GLSL.Parser.Node): node is GLSL.Parser.Node.Structure { return node.type === GLSL.Parser.Node.Type.Structure; }
            for (const child of ast.children)
            {
                if (child.type === GLSL.Parser.Node.Type.Statement)
                {
                    // Check for declaration
                    const subChild = child.children[0];
                    if (subChild && subChild.type === GLSL.Parser.Node.Type.Declaration)
                    {
                        // Find structs
                        for (const struct of subChild.children.filter<GLSL.Parser.Node.Structure>(isStruct))
                        {
                            const ident = struct.children[0];
                            if (ident.type === GLSL.Parser.Node.Type.Identifier)
                            {
                                this._structData[ident.data] = this.classifyStruct(struct);
                            }
                        }
                    }
                }
            }
        }

        protected resolveIncludes(dir: ShaderDirectory): GLSL.Tokenizer.Token[]
        {
            let tokens = [ ...this._rawTokens ];
            for (let i = 0; i < tokens.length; ++i)
            {
                const token = tokens[i];
                if (token.type === GLSL.Tokenizer.TokenType.Preprocessor)
                {
                    const splitBySpace = token.data.split(/\s+/g).map((item) => item.trim());
                    if (splitBySpace[0].toLowerCase() === "#include")
                    {
                        for (let j = 1, l = splitBySpace.length; j < l; ++j)
                        {
                            const item = splitBySpace[j];
                            const match = /"(.+)"/g.exec(item);
                            if (!match || match.length <= 1)
                            {
                                Log.warn(`Tried to include invalid path '${item}'`);
                            }
                            else
                            {
                                const incShaderFile = dir.get(match[1].replace(/\\|\//, Path.seperator));
                                if (!incShaderFile)
                                {
                                    throw new Error(`${token.file}:${token.line} - file '${match[1]}' not found`);
                                }
                                else
                                {
                                    if (incShaderFile.state !== ShaderFile.State.Compiled)
                                    {
                                        throw new Error(`${token.file}:${token.line} - file '${match[1]}' not compiled`);
                                    }
                                    else
                                    {
                                        const before = tokens.slice(0, i - 1);
                                        const after = tokens.slice(i + 1);
                                        tokens =
                                        [
                                            ...before,
                                            {
                                                type: GLSL.Tokenizer.TokenType.Whitespace,
                                                data: `\n`,
                                                file: token.file,
                                                position: token.position,
                                                line: token.line,
                                                column: token.column
                                            },
                                            {
                                                type: GLSL.Tokenizer.TokenType.Whitespace,
                                                data: `#line 1 ${incShaderFile.localPath}`,
                                                file: token.file,
                                                position: token.position,
                                                line: token.line,
                                                column: token.column
                                            },
                                            {
                                                type: GLSL.Tokenizer.TokenType.Whitespace,
                                                data: `\n`,
                                                file: token.file,
                                                position: token.position,
                                                line: token.line,
                                                column: token.column
                                            },
                                            ...incShaderFile._compiledTokens.filter((innerToken) => innerToken.type !== GLSL.Tokenizer.TokenType.EOF),
                                            {
                                                type: GLSL.Tokenizer.TokenType.Whitespace,
                                                data: `\n`,
                                                file: token.file,
                                                position: token.position,
                                                line: token.line,
                                                column: token.column
                                            },
                                            {
                                                type: GLSL.Tokenizer.TokenType.Whitespace,
                                                data: `#line ${after[0].line} ${this.localPath}`,
                                                file: token.file,
                                                position: token.position,
                                                line: token.line,
                                                column: token.column
                                            },
                                            ...after
                                        ];
                                        i += (incShaderFile._compiledTokens.length + 3);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return tokens;
        }

        protected classifyStruct(struct: GLSL.Parser.Node.Structure): ShaderFile.StructClassification
        {
            const result: ShaderFile.StructClassification =
            {
                usesStructs: []
            };
            for (const child of struct.children)
            {
                if (child.type === GLSL.Parser.Node.Type.Declaration)
                {
                    const children = child.children.filter((c) => c.type !== GLSL.Parser.Node.Type.Placeholder);
                    const typeNode = children[0];
                    if (typeNode.type === GLSL.Parser.Node.Type.Identifier)
                    {
                        if (this._structData[typeNode.data])
                        {
                            result.usesStructs.push(typeNode.data);
                        }
                        else
                        {
                            Log.warn(`${struct.token.file}:${struct.token.line} - Unknown type reference '${typeNode.data}', is this struct declared too early?`);
                        }
                    }
                }
            }
            return result;
        }

        protected classifyFunction(func: GLSL.Parser.Node.Function): ShaderFile.FunctionClassification
        {
            const [ ident, args, body ] = func.children;

            const result: ShaderFile.FunctionClassification =
            {
                calls: [],
                usesAttributes: [],
                usesUniforms: [],
                usesVaryings: [],
                usesConsts: [],
                usesStructs: []
            };

            visit(args, GLSL.Parser.Node.Type.Identifier, (node) =>
            {
                if (this._structData[node.data])
                {
                    if (result.usesStructs.indexOf(node.data) === -1)
                    {
                        result.usesStructs.push(node.data);
                    }
                }
            });

            visit(body, GLSL.Parser.Node.Type.Identifier, (node) =>
            {
                const ref = searchScope(node, node.data);
                if (ref)
                {
                    const parent = ref.parent;
                    if (parent)
                    {
                        if (parent.type === GLSL.Parser.Node.Type.DeclarationList)
                        {
                            const declr = parent.parent as GLSL.Parser.Node.Declaration;
                            switch (declr.token.data)
                            {
                                case "attribute":
                                    if (result.usesAttributes.indexOf(ref.data) === -1) { result.usesAttributes.push(ref.data); }
                                    break;
                                case "varying":
                                    if (result.usesVaryings.indexOf(ref.data) === -1) { result.usesVaryings.push(ref.data); }
                                    break;
                                case "uniform":
                                    if (result.usesUniforms.indexOf(ref.data) === -1) { result.usesUniforms.push(ref.data); }
                                    break;
                                case "const":
                                    if (result.usesConsts.indexOf(ref.data) === -1) { result.usesConsts.push(ref.data); }
                                    break;
                            }
                        }
                        else if (parent.type === GLSL.Parser.Node.Type.Function)
                        {
                            if (result.calls.indexOf(ref.data) === -1) { result.calls.push(ref.data); }
                        }
                        else if (parent.type === GLSL.Parser.Node.Type.Structure)
                        {
                            if (result.usesStructs.indexOf(ref.data) === -1) { result.usesStructs.push(ref.data); }
                        }
                    }
                }
            });
            
            return result;
        }

        protected getCompleteFunctionClassification(funcName: string): ShaderFile.FunctionClassification
        {
            const base = this._functionData[funcName];
            if (!base) { return null; }

            const result: ShaderFile.FunctionClassification =
            {
                usesUniforms: [],
                usesAttributes: [],
                usesVaryings: [],
                usesConsts: [],
                usesStructs: [],
                calls: []
            };

            function visitStruct(this: ShaderFile, data: ShaderFile.StructClassification): void
            {
                for (const structName of data.usesStructs)
                {
                    if (result.usesStructs.indexOf(structName) === -1)
                    {
                        result.usesStructs.push(structName);
                        visitStruct.call(this, this._structData[structName]);
                    }
                }
            }

            function checkForStructOrConst(this: ShaderFile, data: ShaderFile.VarDetails): void
            {
                for (const constName of data.usesConsts)
                {
                    if (result.usesConsts.indexOf(constName) === -1)
                    {
                        result.usesConsts.push(constName);
                    }
                }
                if (data.type !== ShaderFile.VarType.Struct) { return; }
                const dataAsStruct = data as ShaderFile.StructVarDetails;
                if (dataAsStruct.structName != null && result.usesStructs.indexOf(dataAsStruct.structName) === -1)
                {
                    result.usesStructs.push(dataAsStruct.structName);
                }
                visitStruct.call(this, dataAsStruct.structDetails);
            }

            function visitFunc(this: ShaderFile, data: ShaderFile.FunctionClassification): void
            {
                for (const uniform of data.usesUniforms)
                {
                    if (result.usesUniforms.indexOf(uniform) === -1)
                    {
                        result.usesUniforms.push(uniform);
                        checkForStructOrConst.call(this, this._uniformData[uniform]);
                    }
                }
                for (const attr of data.usesAttributes)
                {
                    if (result.usesAttributes.indexOf(attr) === -1)
                    {
                        result.usesAttributes.push(attr);
                        checkForStructOrConst.call(this, this._attributeData[attr]);
                    }
                }
                for (const varying of data.usesVaryings)
                {
                    if (result.usesVaryings.indexOf(varying) === -1)
                    {
                        result.usesVaryings.push(varying);
                        checkForStructOrConst.call(this, this._varyingData[varying]);
                    }
                }
                for (const constVar of data.usesConsts)
                {
                    if (result.usesConsts.indexOf(constVar) === -1)
                    {
                        checkForStructOrConst.call(this, this._constData[constVar]);
                        result.usesConsts.push(constVar);
                    }
                }
                for (const structName of data.usesStructs)
                {
                    if (result.usesStructs.indexOf(structName) === -1)
                    {
                        result.usesStructs.push(structName);
                        visitStruct.call(this, this._structData[structName]);
                    }
                }
                for (const callFuncName of data.calls)
                {
                    if (result.calls.indexOf(callFuncName) === -1)
                    {
                        result.calls.push(callFuncName);
                        const otherData = this._functionData[callFuncName];
                        if (otherData)
                        {
                            visitFunc.call(this, otherData);
                        }
                    }
                }
            }

            visitFunc.call(this, base);
            return result;
        }

        protected parseKeyValues(items: string[]): Util.Map<string | boolean>
        {
            const map: Util.Map<string | boolean> = {};
            for (const item of items)
            {
                const splitByEquals = item.split("=", 2);
                const key = splitByEquals[0].toLowerCase();
                if (splitByEquals.length === 1)
                {
                    if (key[0] === "!")
                    {
                        map[key.substr(1)] = false;
                    }
                    else
                    {
                        map[key] = true;
                    }
                }
                else
                {
                    map[key] = splitByEquals[1];
                }
            }
            return map;
        }

        protected resolveExports(ast: GLSL.Parser.Node.StatementList): ShaderFile.Export[]
        {
            const result: ShaderFile.Export[] = [];

            /* tslint:disable:no-string-literal */

            // Identify exports
            for (const child of ast.children)
            {
                if (child.type === GLSL.Parser.Node.Type.Preprocessor)
                {
                    const splitBySpace = child.token.data.split(/\s+/g).map((item) => item.trim());
                    if (splitBySpace.length >= 2 && splitBySpace[0].toLowerCase() === "#pragma" && splitBySpace[1].toLowerCase() === "export")
                    {
                        if (splitBySpace.length < 3)
                        {
                            throw new Error(`${child.token.file}:${child.token.line} - export directive missing name`);
                        }
                        const name = splitBySpace[2];
                        if (name.indexOf("=") !== -1)
                        {
                            throw new Error(`${child.token.file}:${child.token.line} - export directive missing name`);
                        }
                        const exportData = this.parseKeyValues(splitBySpace.slice(3));
                        if (!exportData["vertex"] && !exportData["fragment"])
                        {
                            Log.warn(`${child.token.file}:${child.token.line} - export directive missing vertex and fragment shader`);
                        }
                        else
                        {
                            result.push(this.resolveExport(ast, name, exportData["vertex"] as string, exportData["fragment"] as string, exportData["filter"] as boolean, exportData["dynamic"] as boolean));
                        }
                    }
                }
            }

            /* tslint:enable:no-string-literal */

            return result;
        }

        protected exportShader(ast: GLSL.Parser.Node.StatementList, entryPoint: string, shaderType: ShaderFile.ShaderType): string | null
        {
            const completeClassification = this.getCompleteFunctionClassification(entryPoint);

            const callsList = new List(completeClassification.calls);
            callsList.add(entryPoint);
            callsList.resolveOrder((item) => [], (item) => this._functionData[item] ? this._functionData[item].calls : []);

            const structsList = new List(completeClassification.usesStructs);
            structsList.resolveOrder((item) => [], (item) => this._structData[item] ? this._structData[item].usesStructs : []);
            
            const writer = new GLSLWriter(true);
            switch (shaderType)
            {
                case ShaderFile.ShaderType.Vertex:
                    writer.write("#define VERTEX");
                    writer.writeEmptyLine();
                    break;
                case ShaderFile.ShaderType.Fragment:
                    writer.write("#define FRAGMENT");
                    writer.writeEmptyLine();
                    break;
            }

            // Write any preamble from the ast
            for (const child of ast.children)
            {
                if (child.type === GLSL.Parser.Node.Type.Preprocessor)
                {
                    if (!/#(pragma (export|meta|switch)|include)/g.exec(child.token.data))
                    {
                        writer.writeNode(child);
                    }
                }
                else
                {
                    const inner = child.children[0];
                    switch (inner.type)
                    {
                        case GLSL.Parser.Node.Type.Precision:
                            writer.writeNode(inner);
                            break;
                    }
                }
            }

            // Write all structs
            for (const structName of completeClassification.usesStructs)
            {
                const ident = ast.scope[structName];
                if (ident == null) { throw new Error(`Tried to export structure '${structName}' which wasn't found in global scope`); }
                const parent = ident.parent.parent;
                if (parent.type !== GLSL.Parser.Node.Type.Declaration) { throw new Error(`Tried to export structure '${structName}' which wasn't found in global scope`); }
                writer.writeDeclarationFiltered(parent, [ structName ]);
            }
            if (completeClassification.usesStructs.length > 0) { writer.writeEmptyLine(); }

            // Write all consts
            for (const constName of completeClassification.usesConsts)
            {
                const ident = ast.scope[constName];
                if (ident == null) { throw new Error(`Tried to export const '${constName}' which wasn't found in global scope`); }
                const parent = ident.parent.parent;
                if (parent.type !== GLSL.Parser.Node.Type.Declaration) { throw new Error(`Tried to export const '${constName}' which wasn't found in global scope`); }
                writer.writeDeclarationFiltered(parent, [ constName ]);
                this._localConstData[constName] = this._constData[constName];
            }
            if (completeClassification.usesConsts.length > 0) { writer.writeEmptyLine(); }

            if (shaderType === ShaderFile.ShaderType.Vertex)
            {
                // Write all attributes
                let inCondition: string | boolean | null = null;
                for (const attrName of completeClassification.usesAttributes)
                {
                    const ident = ast.scope[attrName];
                    if (ident == null) { throw new Error(`Tried to export attribute '${attrName}' which wasn't found in global scope`); }
                    const parent = ident.parent.parent;
                    if (parent.type !== GLSL.Parser.Node.Type.Declaration) { throw new Error(`Tried to export attribute '${attrName}' which wasn't found in global scope`); }
                    const condition = this._attributeData[attrName].metaData.condition;
                    if (condition != null)
                    {
                        if (inCondition !== condition)
                        {
                            if (inCondition != null) { writer.writeLine("#endif"); }
                            writer.writeLine(`#ifdef ${condition}`);
                            inCondition = condition;
                        }
                    }
                    else
                    {
                        if (inCondition != null)
                        {
                            writer.writeLine("#endif");
                            inCondition = null;
                        }
                    }
                    writer.writeDeclarationFiltered(parent, [ attrName ]);
                    this._localAttributeData[attrName] = this._attributeData[attrName];
                }
                if (inCondition != null) { writer.writeLine("#endif"); }
                if (completeClassification.usesAttributes.length > 0) { writer.writeEmptyLine(); }
            }
            else if (completeClassification.usesAttributes.length > 0)
            {
                throw new Error(`Function '${entryPoint}' tried to use an attribute in a vertex shader`);
            }

            // Write all varyings
            if (completeClassification.usesVaryings.length > 0)
            {
                const sorted = [...completeClassification.usesVaryings].sort();
                let inCondition: string | boolean | null = null;
                for (const varyingName of sorted)
                {
                    const ident = ast.scope[varyingName];
                    if (ident == null) { throw new Error(`Tried to export varying '${varyingName}' which wasn't found in global scope`); }
                    const parent = ident.parent.parent;
                    if (parent.type !== GLSL.Parser.Node.Type.Declaration) { throw new Error(`Tried to export varying '${varyingName}' which wasn't found in global scope`); }
                    const condition = this._varyingData[varyingName].metaData.condition;
                    if (condition != null)
                    {
                        if (inCondition !== condition)
                        {
                            if (inCondition != null) { writer.writeLine("#endif"); }
                            writer.writeLine(`#ifdef ${condition}`);
                            inCondition = condition;
                        }
                    }
                    else
                    {
                        if (inCondition != null)
                        {
                            writer.writeLine("#endif");
                            inCondition = null;
                        }
                    }
                    writer.writeDeclarationFiltered(parent, [ varyingName ]);
                    this._localVaryingData[varyingName] = this._varyingData[varyingName];
                }
                if (inCondition != null) { writer.writeLine("#endif"); }
                writer.writeEmptyLine();
            }
            
            // Write all uniforms
            if (completeClassification.usesUniforms.length > 0)
            {
                let inCondition: string | boolean | null = null;
                for (const uniformName of completeClassification.usesUniforms)
                {
                    const ident = ast.scope[uniformName];
                    if (ident == null) { throw new Error(`Tried to export uniform '${uniformName}' which wasn't found in global scope`); }
                    const parent = ident.parent.parent;
                    if (parent.type !== GLSL.Parser.Node.Type.Declaration) { throw new Error(`Tried to export uniform '${uniformName}' which wasn't found in global scope`); }
                    const condition = this._uniformData[uniformName].metaData.condition;
                    if (condition != null)
                    {
                        if (inCondition !== condition)
                        {
                            if (inCondition != null) { writer.writeLine("#endif"); }
                            writer.writeLine(`#ifdef ${condition}`);
                            inCondition = condition;
                        }
                    }
                    else
                    {
                        if (inCondition != null)
                        {
                            writer.writeLine("#endif");
                            inCondition = null;
                        }
                    }
                    writer.writeDeclarationFiltered(parent, [ uniformName ]);
                    this._localUniformData[uniformName] = this._uniformData[uniformName];
                }
                if (inCondition != null) { writer.writeLine("#endif"); }
                writer.writeEmptyLine();
            }

            // Write all functions
            for (const funcName of callsList.data)
            {
                const ident = ast.scope[funcName];
                if (ident == null) { throw new Error(`Tried to export function '${funcName}' which wasn't found in global scope`); }
                const func = ident.parent;
                if (func == null || func.type !== GLSL.Parser.Node.Type.Function) { throw new Error(`Tried to export function '${funcName}' which wasn't found in global scope`); }
                if (funcName === entryPoint)
                {
                    ident.data = "main";
                }
                writer.writeDeclarationFiltered(func.parent as GLSL.Parser.Node.Declaration, [ funcName ]);
                if (funcName === entryPoint)
                {
                    ident.data = entryPoint;
                }
            }

            return writer.toString();
        }

        protected evaluateStaticExpression(expr: GLSL.Parser.Node.Expression, outUsedConsts?: string[]): number | boolean
        {
            const child = expr.children[0];
            return this.evaluateInnerStaticExpression(expr.token, child, outUsedConsts);
        }

        protected evaluateInnerStaticExpression(token: GLSL.Tokenizer.Token, child: GLSL.Parser.Node, outUsedConsts?: string[])
        {
            switch (child.type)
            {
                case GLSL.Parser.Node.Type.Literal:
                    const asNum = parseInt(child.data);
                    if (Is.number(asNum)) { return asNum; }
                    if (child.data === "true") { return true; }
                    if (child.data === "false") { return false; }
                    throw new Error(`${token.file}:${token.line} - Unsupported literal '${child.data}'`);
                case GLSL.Parser.Node.Type.Identifier:
                    const ref = searchScope(child, child.data);
                    if (!ref) { throw new Error(`${token.file}:${token.line} - Unknown identifier '${child.data}' when evaluating static expression`); }
                    if (this._constData[ref.data])
                    {
                        if (outUsedConsts && outUsedConsts.indexOf(ref.data) === -1) { outUsedConsts.push(ref.data); }
                        return this._constData[ref.data].staticValue;
                    }
                    else
                    {
                        throw new Error(`${token.file}:${token.line} - Identifier '${child.data}' does not refer to a const when evaluating static expression`);
                    }
                    break;
                case GLSL.Parser.Node.Type.Binary:
                    const left = this.evaluateInnerStaticExpression(token, child.children[0], outUsedConsts);
                    const right = this.evaluateInnerStaticExpression(token, child.children[1], outUsedConsts);
                    switch (child.data)
                    {
                        case "+":
                            if (Is.number(left) && Is.number(right))
                            {
                                return left + right;
                            }
                            else
                            {
                                throw new Error(`${token.file}:${token.line} - Unsupported binary operator '${child.data}' when evaluating static expression (either left or right wasn't a number)`);
                            }
                        case "-":
                            if (Is.number(left) && Is.number(right))
                            {
                                return left - right;
                            }
                            else
                            {
                                throw new Error(`${token.file}:${token.line} - Unsupported binary operator '${child.data}' when evaluating static expression (either left or right wasn't a number)`);
                            }
                        case "*":
                            if (Is.number(left) && Is.number(right))
                            {
                                return left * right;
                            }
                            else
                            {
                                throw new Error(`${token.file}:${token.line} - Unsupported binary operator '${child.data}' when evaluating static expression (either left or right wasn't a number)`);
                            }
                        case "/":
                            if (Is.number(left) && Is.number(right))
                            {
                                return left / right;
                            }
                            else
                            {
                                throw new Error(`${token.file}:${token.line} - Unsupported binary operator '${child.data}' when evaluating static expression (either left or right wasn't a number)`);
                            }
                        default:
                            throw new Error(`${token.file}:${token.line} - Unsupported binary operator '${child.data}' when evaluating static expression`);
                    }
                    //break;
                case GLSL.Parser.Node.Type.Group:
                    return this.evaluateInnerStaticExpression(token, child.children[0], outUsedConsts);
                default:
                    throw new Error(`${token.file}:${token.line} - Unsupported node '${child.type}' when evaluating static expression`);
            }
        }

        protected searchForQuantifier(node: GLSL.Parser.Node, id: string): GLSL.Parser.Node.Quantifier | null
        {
            for (let i = 0, l = node.children.length; i < l; ++i)
            {
                const subChild = node.children[i];
                if (subChild.type === GLSL.Parser.Node.Type.Identifier && subChild.data === id)
                {
                    const quantifier = node.children[i + 1];
                    if (i + 1 < l && quantifier.type === GLSL.Parser.Node.Type.Quantifier)
                    {
                        return quantifier;
                    }
                }
            }
            return null;
        }

        protected searchForInitExpr(node: GLSL.Parser.Node, id: string): GLSL.Parser.Node.Expression | null
        {
            for (let i = 0, l = node.children.length; i < l; ++i)
            {
                const subChild = node.children[i];
                if (subChild.type === GLSL.Parser.Node.Type.Identifier && subChild.data === id)
                {
                    const expr = node.children[i + 1];
                    if (i + 1 < l && expr.type === GLSL.Parser.Node.Type.Expression)
                    {
                        return expr;
                    }
                }
            }
            return null;
        }

        protected identifyVarDetails(declr: GLSL.Parser.Node.Declaration, name: string): ShaderFile.VarDetails
        {
            const children = declr.children.filter((child) => child.type !== GLSL.Parser.Node.Type.Placeholder);
            const child1 = children[1];
            if (child1.type === GLSL.Parser.Node.Type.Keyword)
            {
                let dimension = 1;
                const declList = children[2];
                const usesConsts: string[] = [];
                if (declList.type === GLSL.Parser.Node.Type.DeclarationList)
                {
                    const quantifier = this.searchForQuantifier(declList, name);
                    if (quantifier)
                    {
                        const expr = quantifier.children[0];
                        const evaluated = this.evaluateStaticExpression(expr, usesConsts);
                        if (!Is.number(evaluated))
                        {
                            throw new Error(`Unable to identify variable type for declaration '${name}' (array quantifier didn't evaluate to a number)`);
                        }
                        dimension = evaluated;
                    }
                    else
                    {
                        const expr = this.searchForInitExpr(declList, name);
                        if (expr)
                        {
                            this.evaluateStaticExpression(expr, usesConsts);
                        }
                    }
                }
                return {
                    type: children[1].token.data as ShaderFile.VarType,
                    dimension,
                    metaData: {},
                    usesConsts
                };
            }
            else if (child1.type === GLSL.Parser.Node.Type.Structure)
            {
                let dimension = 1;
                const quantifier = this.searchForQuantifier(child1, name);
                const usesConsts: string[] = [];
                if (quantifier)
                {
                    const expr = quantifier.children[0];
                    const evaluated = this.evaluateStaticExpression(expr, usesConsts);
                    if (!Is.number(evaluated))
                    {
                        throw new Error(`Unable to identify variable type for declaration '${name}' (array quantifier didn't evaluate to a number)`);
                    }
                    dimension = evaluated;
                }
                return {
                    type: ShaderFile.VarType.Struct,
                    structDetails: this.classifyStruct(child1),
                    structIsInline: true,
                    dimension,
                    metaData: {},
                    usesConsts
                };
            }
            else if (child1.type === GLSL.Parser.Node.Type.Identifier)
            {
                if (this._structData[child1.data])
                {
                    let dimension = 1;
                    const declList = children[2];
                    const usesConsts: string[] = [];
                    if (declList.type === GLSL.Parser.Node.Type.DeclarationList)
                    {
                        const quantifier = this.searchForQuantifier(declList, name);
                        if (quantifier)
                        {
                            const expr = quantifier.children[0];
                            const evaluated = this.evaluateStaticExpression(expr, usesConsts);
                            if (!Is.number(evaluated))
                            {
                                throw new Error(`Unable to identify variable type for declaration '${name}' (array quantifier didn't evaluate to a number)`);
                            }
                            dimension = evaluated;
                        }
                    }
                    return {
                        type: ShaderFile.VarType.Struct,
                        structName: child1.data,
                        structDetails: this._structData[child1.data],
                        structIsInline: false,
                        dimension,
                        metaData: {},
                        usesConsts
                    };
                }
                else
                {
                    throw new Error(`Unable to identify variable type '${child1.data}' for declaration '${name}'`);
                }
            }
            else
            {
                throw new Error(`Unable to identify variable type '${child1.type}' for declaration '${name}'`);
            }
        }

        protected resolveExport(ast: GLSL.Parser.Node.StatementList, name: string, vertexFunc?: string, fragmentFunc?: string, isFilter?: boolean, isDynamic?: boolean): ShaderFile.Export
        {
            this._localVaryingData = {};
            this._localUniformData = {};
            this._localAttributeData = {};
            this._localConstData = {};

            const result: ShaderFile.Export =
            {
                name,
                vertexSrc: null,
                fragmentSrc: null,
                isFilter: isFilter || false,
                varyings: this._localVaryingData,
                uniforms: this._localUniformData,
                attributes: this._localAttributeData,
                consts: this._localConstData,
                switches: this._switchData,
                dynamic: isDynamic || false
            };

            if (vertexFunc != null)
            {
                result.vertexSrc = this.exportShader(ast, vertexFunc, ShaderFile.ShaderType.Vertex);
            }
            if (fragmentFunc != null)
            {
                result.fragmentSrc = this.exportShader(ast, fragmentFunc, ShaderFile.ShaderType.Fragment);
            }

            this._localVaryingData = null;
            this._localUniformData = null;
            this._localAttributeData = null;
            this._localConstData = null;
            
            return result;
        }
    }

    export namespace ShaderFile
    {
        export enum State
        {
            Unloaded,
            Loaded,
            Compiled
        }

        export interface FunctionClassification
        {
            usesUniforms: string[];
            usesAttributes: string[];
            usesVaryings: string[];
            usesConsts: string[];
            usesStructs: string[];
            calls: string[];
        }

        export interface StructClassification
        {
            usesStructs: string[];
        }

        export enum ShaderType
        {
            Vertex,
            Fragment
        }

        export enum VarType
        {
            Float = "float",
            Vec2 = "vec2",
            Vec3 = "vec3",
            Vec4 = "vec4",
            Int = "int",
            IVec2 = "ivec2",
            IVec3 = "ivec3",
            IVec4 = "ivec4",
            Bool = "bool",
            BVec2 = "bvec2",
            BVec3 = "bvec3",
            BVec4 = "bvec4",
            Matrix2x2 = "mat2",
            Matrix3x3 = "mat3",
            Matrix4x4 = "mat4",
            Sampler1D = "sampler1D",
            Sampler2D = "sampler2D",
            Sampler3D = "sampler3D",
            SamplerCube = "samplerCube",
            Sampler1DShadow = "sampler1DShadow",
            Sampler2DShadow = "sampler2DShadow",
            Struct = "struct"
        }

        export interface BaseVarDetails<TType extends VarType>
        {
            type: TType;
            comment?: string;
            metaData: Util.Map<string | boolean>;
            dimension: number;
            usesConsts: string[];
            staticValue?: number | boolean;
        }

        export interface StructVarDetails extends BaseVarDetails<VarType.Struct>
        {
            structName?: string;
            structDetails: StructClassification;
            structIsInline: boolean;
        }

        export type VarDetails = StructVarDetails | BaseVarDetails<VarType>;

        export interface SwitchDetails
        {
            comment?: string;
            metaData: Util.Map<string | boolean>;
        }

        export interface Export
        {
            name: string;
            isFilter: boolean;
            vertexSrc?: string;
            fragmentSrc?: string;
            uniforms: Util.Map<VarDetails>;
            attributes: Util.Map<VarDetails>;
            varyings: Util.Map<VarDetails>;
            consts: Util.Map<VarDetails>;
            switches: Util.Map<SwitchDetails>;
            dynamic: boolean;
        }
    }
}