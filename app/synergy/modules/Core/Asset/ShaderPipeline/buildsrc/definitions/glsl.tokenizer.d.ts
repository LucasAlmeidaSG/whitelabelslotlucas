declare namespace GLSL.Tokenizer
{
    export const enum TokenType
    {
        BlockComment = "block-comment",
        LineComment = "line-comment",
        Preprocessor = "preprocessor",
        Operator = "operator",
        Float = "float",
        Identifier = "ident",
        BuiltIn = "builtin",
        EOF = "eof",
        Integer = "integer",
        Whitespace = "whitespace",
        Keyword = "keyword"
    }

    export interface Token
    {
        type: TokenType;
        data: string;
        position: number;
        line: number;
        column: number;
        file: string;
        preceding?: Token[];
    }

    export type StringTokenizer = (input: string) => Token[];
}