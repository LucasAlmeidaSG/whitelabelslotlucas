declare namespace GLSL.Parser
{
    export interface Scope
    {
        [key: string]: Node.Identifier;
    }

    export namespace Node
    {
        export const enum Type
        {
            StatementList = "stmtlist",
            Statement = "stmt",
            Structure = "struct",
            Function = "function",
            FunctionArgs = "functionargs",
            Declaration = "decl",
            DeclarationList = "decllist",
            ForLoop = "forloop",
            WhileLoop = "whileloop",
            If = "if",
            Expression = "expr",
            Precision = "precision",
            Comment = "comment",
            Preprocessor = "preprocessor",
            Keyword = "keyword",
            Identifier = "ident",
            Return = "return",
            Continue = "continue",
            Break = "break",
            Discard = "discard",
            DoWhile = "do-while",
            Binary = "binary",
            Ternary = "ternary",
            Unary = "unary",
            Suffix = "suffix",
            Assign = "assign",
            BuiltIn = "builtin",
            Call = "call",
            Operator = "operator",
            Literal = "literal",
            Group = "group",
            Quantifier = "quantifier",
            Placeholder = "placeholder"
        }

        export interface Base<TType extends Type>
        {
            type: TType;
            mode: number;
            id: string;
            children: Node[];
            token: GLSL.Tokenizer.Token;
            parent?: Node;
        }

        export interface StatementList extends Base<Type.StatementList>
        {
            scope: Scope;
            children: (Statement | Preprocessor)[];
        }

        export interface Statement extends Base<Type.Statement> { }

        export interface Structure extends Base<Type.Structure>
        {
            children: (Identifier | Declaration)[];
        }

        export interface Function extends Base<Type.Function>
        {
            children: [ Identifier, FunctionArgs, StatementList ];
        }

        export interface FunctionArgs extends Base<Type.FunctionArgs> { }

        export interface Declaration extends Base<Type.Declaration>
        {
            children: (Placeholder | Keyword | Identifier | DeclarationList | Structure | Function)[];
        }

        export interface DeclarationList extends Base<Type.DeclarationList>
        {
            children: (Identifier | Quantifier | Expression)[];
        }

        export interface ForLoop extends Base<Type.ForLoop>
        {
            children: [ Declaration | Expression, Expression, Expression, Statement | StatementList ];
        }

        export interface WhileLoop extends Base<Type.WhileLoop>
        {
            children: [ Expression, Statement | StatementList ];
        }

        export interface If extends Base<Type.If>
        {
            children: [ Expression, Statement | StatementList ] | [ Expression, Statement | StatementList, Statement ];
        }

        export interface Expression extends Base<Type.Expression> { }

        export interface Precision extends Base<Type.Precision>
        {
            children: [ Keyword, Keyword ];
        }

        export interface Comment extends Base<Type.Comment> { }
        export interface Preprocessor extends Base<Type.Preprocessor> { }
        export interface Keyword extends Base<Type.Keyword> { }

        export interface Identifier extends Base<Type.Identifier>
        {
            data: string;
        }

        export interface Return extends Base<Type.Return>
        {
            children: [ never ] | [ Expression ];
        }

        export interface Continue extends Base<Type.Continue> { }
        export interface Break extends Base<Type.Break> { }
        export interface Discard extends Base<Type.Discard> { }

        export interface DoWhile extends Base<Type.DoWhile>
        {
            children: [ Statement | StatementList, Expression ];
        }

        export interface Binary extends Base<Type.Binary>
        {
            data: string;
            children: [ Node, Node ];
        }

        export interface Ternary extends Base<Type.Ternary>
        {
            children: [ Node, Node, Node ];
        }

        export interface Unary extends Base<Type.Unary>
        {
            data: string;
            children: [ Node ];
        }

        export interface Suffix extends Base<Type.Suffix>
        {
            data: string;
            children: [ Node ];
        }

        export interface Assign extends Base<Type.Assign>
        {
            children: [ BuiltIn|Identifier, Node ];
            data: string;
        }

        export interface BuiltIn extends Base<Type.BuiltIn>
        {
            data: string;
        }

        export interface Call extends Base<Type.Call>
        {
            
        }

        export interface Operator extends Base<Type.Operator>
        {
            data: string;
        }

        export interface Literal extends Base<Type.Literal>
        {
            data: string;
        }

        export interface Group extends Base<Type.Group>
        {
            children: [ Node ];
        }

        export interface Quantifier extends Base<Type.Quantifier>
        {
            children: [ Expression ];
        }

        export interface Placeholder extends Base<Type.Placeholder> { }
    }

    export type Node =
        Node.StatementList |
        Node.Statement |
        Node.Structure |
        Node.Function |
        Node.FunctionArgs |
        Node.Declaration |
        Node.DeclarationList |
        Node.ForLoop |
        Node.WhileLoop |
        Node.If |
        Node.Expression |
        Node.Precision |
        Node.Comment |
        Node.Preprocessor |
        Node.Keyword |
        Node.Identifier |
        Node.Return |
        Node.Continue |
        Node.Break |
        Node.Discard |
        Node.DoWhile |
        Node.Binary |
        Node.Ternary |
        Node.Unary |
        Node.Suffix |
        Node.Assign |
        Node.BuiltIn |
        Node.Call |
        Node.Operator |
        Node.Literal |
        Node.Group |
        Node.Quantifier |

        Node.Placeholder;

    export type DirectParser = (input: GLSL.Tokenizer.Token[]) => Node.StatementList;
}