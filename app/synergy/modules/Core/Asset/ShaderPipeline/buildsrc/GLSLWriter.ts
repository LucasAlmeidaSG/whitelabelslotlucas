namespace RS.ShaderPipeline
{
    enum Location
    {
        StartOfLine,
        StartOfStatement,
        MidStatement,
        EndOfStatement
    }

    const indent = "    ";

    export class GLSLWriter
    {
        private _pretty: boolean = false;
        private _segs: string[] = [];
        private _loc: Location = Location.StartOfLine;
        private _identLevel: number = 0;

        public constructor(pretty: boolean)
        {
            this._pretty = pretty;
        }

        public writeNode(node: GLSL.Parser.Node): this
        {
            switch (node.type)
            {
                case GLSL.Parser.Node.Type.StatementList:
                    this.writeStatementList(node);
                    break;
                case GLSL.Parser.Node.Type.Statement:
                    this.writeStatement(node);
                    break;
                case GLSL.Parser.Node.Type.Structure:
                    this.writeStructure(node);
                    break;
                case GLSL.Parser.Node.Type.Function:
                    this.writeFunction(node);
                    break;
                case GLSL.Parser.Node.Type.FunctionArgs:
                    this.writeFunctionArgs(node);
                    break;
                case GLSL.Parser.Node.Type.Declaration:
                    this.writeDeclaration(node);
                    break;
                case GLSL.Parser.Node.Type.DeclarationList:
                    this.writeDeclarationList(node);
                    break;
                case GLSL.Parser.Node.Type.ForLoop:
                    this.writeForLoop(node);
                    break;
                case GLSL.Parser.Node.Type.WhileLoop:
                    this.writeWhileLoop(node);
                    break;
                case GLSL.Parser.Node.Type.If:
                    this.writeIf(node);
                    break;
                case GLSL.Parser.Node.Type.Expression:
                    this.writeExpression(node);
                    break;
                case GLSL.Parser.Node.Type.Precision:
                    this.writePrecision(node);
                    break;
                case GLSL.Parser.Node.Type.Comment:
                    this.writeComment(node);
                    break;
                case GLSL.Parser.Node.Type.Preprocessor:
                    this.writePreprocessor(node);
                    break;
                case GLSL.Parser.Node.Type.Keyword:
                    this.writeKeyword(node);
                    break;
                case GLSL.Parser.Node.Type.Identifier:
                    this.writeIdentifier(node);
                    break;
                case GLSL.Parser.Node.Type.Return:
                    this.writeReturn(node);
                    break;
                case GLSL.Parser.Node.Type.Continue:
                    this.writeContinue(node);
                    break;
                case GLSL.Parser.Node.Type.Break:
                    this.writeBreak(node);
                    break;
                case GLSL.Parser.Node.Type.Discard:
                    this.writeDiscard(node);
                    break;
                case GLSL.Parser.Node.Type.DoWhile:
                    this.writeDoWhile(node);
                    break;
                case GLSL.Parser.Node.Type.Binary:
                    this.writeBinary(node);
                    break;
                case GLSL.Parser.Node.Type.Ternary:
                    this.writeTernary(node);
                    break;
                case GLSL.Parser.Node.Type.Unary:
                    this.writeUnary(node);
                    break;
                case GLSL.Parser.Node.Type.Suffix:
                    this.writeSuffix(node);
                    break;
                case GLSL.Parser.Node.Type.Assign:
                    this.writeAssign(node);
                    break;
                case GLSL.Parser.Node.Type.BuiltIn:
                    this.writeBuiltIn(node);
                    break;
                case GLSL.Parser.Node.Type.Call:
                    this.writeCall(node);
                    break;
                case GLSL.Parser.Node.Type.Operator:
                    this.writeOperator(node);
                    break;
                case GLSL.Parser.Node.Type.Literal:
                    this.writeLiteral(node);
                    break;
                case GLSL.Parser.Node.Type.Group:
                    this.writeGroup(node);
                    break;
                case GLSL.Parser.Node.Type.Placeholder:
                    break;
                default:
                    Log.warn(`Unhandled node type '${(node as any).type}'`);
                    break;
            }
            return this;
        }

        public toString(): string
        {
            return this._segs.join("");
        }

        public write(str: string, onlyWhenPretty: boolean = false): this
        {
            if (!onlyWhenPretty || this._pretty)
            {
                this._segs.push(str);
            }
            if (this._loc === Location.StartOfStatement)
            {
                this._loc = Location.MidStatement;
            }
            return this;
        }

        public writeLine(line: string): this
        {
            if (this._loc !== Location.StartOfLine)
            {
                this.writeNewLine(true);
            }
            this.write(line);
            this._loc = Location.EndOfStatement;
            this.writeNewLine(true);
            return this;
        }

        public writeEmptyLine(force: boolean = false): this
        {
            this.writeNewLine();
            this._loc = Location.EndOfStatement;
            return this;
        }

        public writeDeclarationFiltered(declr: GLSL.Parser.Node.Declaration, filterNames: string[]): this
        {
            const clone = { ...declr };
            for (let i = 0, l = clone.children.length; i < l; ++i)
            {
                const child = clone.children[i];
                if (child.type === GLSL.Parser.Node.Type.DeclarationList)
                {
                    const filteredChildren: (GLSL.Parser.Node.Identifier | GLSL.Parser.Node.Quantifier | GLSL.Parser.Node.Expression)[] = [];
                    
                    for (let j = 0, l2 = child.children.length; j < l2; ++j)
                    {
                        const subChild = child.children[j];
                        if (subChild.type === GLSL.Parser.Node.Type.Identifier && filterNames.indexOf(subChild.data) !== -1)
                        {
                            filteredChildren.push(subChild);
                            while ((j + 1) < l2 && child.children[j + 1].type !== GLSL.Parser.Node.Type.Identifier)
                            {
                                filteredChildren.push(child.children[j + 1]);
                                ++j;
                            }
                        }
                    }

                    clone.children[i] =
                    {
                        ...child,
                        children: filteredChildren
                    };
                    if (clone.children[i].children.length === 0) { return this; }
                    break;
                }
                else if (child.type === GLSL.Parser.Node.Type.Structure)
                {
                    const ident = child.children[0];
                    if (ident.type === GLSL.Parser.Node.Type.Identifier && filterNames.indexOf(ident.data) !== -1)
                    {
                        clone.children = [ child ];
                        break;
                    }
                }
            }
            this.writeDeclaration(clone);
            return this;
        }

        protected writeNewLine(force: boolean = false): this
        {
            if (this._pretty || force)
            {
                this.write("\n");
            }
            this._loc = Location.StartOfLine;
            return this;
        }

        protected writeIndent(level: number): this
        {
            if (this._pretty)
            {
                for (let i = 0; i < level; ++i)
                {
                    this.write(indent);
                }
            }
            return this;
        }

        protected beginStatement(): this
        {
            switch (this._loc)
            {
                case Location.MidStatement:
                case Location.EndOfStatement:
                    this.writeNewLine();
                    // Intentional fall-through
                case Location.StartOfLine:
                    this.writeIndent(this._identLevel);
                    this._loc = Location.StartOfStatement;
                    break;
            }
            return this;
        }

        protected endStatement(withSemicolon: boolean = true): this
        {
            if (this._loc === Location.MidStatement && withSemicolon)
            {
                this.write(";");
            }
            this._loc = Location.EndOfStatement;
            return this;
        }

        // #region Node Writers

        private writeStatementList(node: GLSL.Parser.Node.StatementList, withBlock: boolean = false): this
        {
            if (withBlock)
            {
                this
                    .beginStatement()
                    .write("{")
                    .endStatement(false);
                ++this._identLevel;
            }
            for (const child of node.children)
            {
                this.writeNode(child);
            }
            if (withBlock)
            {
                --this._identLevel;
                this
                    .beginStatement()
                    .write("}")
                    .endStatement(false);
            }
            return this;
        }

        private writeStatement(node: GLSL.Parser.Node.Statement): this
        {
            this.beginStatement();
            for (const child of node.children)
            {
                if (child.type === GLSL.Parser.Node.Type.StatementList)
                {
                    this.writeStatementList(child, true);
                }
                else if (child.type === GLSL.Parser.Node.Type.Expression)
                {
                    if (child.children[0].type === GLSL.Parser.Node.Type.Call)
                    {
                        if (child.children[0].children[0].token.data === "touch")
                        {
                            // Log.debug(`Omitting call to 'touch'`);
                            return;
                        }
                    }
                    this.writeExpression(child);
                }
                else
                {
                    this.writeNode(child);
                }
            }
            this.endStatement();
            return this;
        }

        private writeStatementOrStatementList(node: GLSL.Parser.Node.Statement | GLSL.Parser.Node.StatementList, alwaysIndent: boolean = true): this
        {
            if (node.type === GLSL.Parser.Node.Type.Statement)
            {
                if (alwaysIndent) { ++this._identLevel; }
                this.writeStatement(node);
                if (alwaysIndent) { --this._identLevel; }
                return this;
            }
            else
            {
                return this.writeStatementList(node, true);
            }
        }

        private writeStructure(node: GLSL.Parser.Node.Structure): this
        {
            this.write("struct ");
            const ident = node.children[0];
            if (ident.type === GLSL.Parser.Node.Type.Identifier)
            {
                this.writeIdentifier(ident);
            }
            this
                .beginStatement()
                .write("{")
                .endStatement(false);
            ++this._identLevel;
            for (let i = 1, l = node.children.length; i < l; ++i)
            {
                const child = node.children[i];
                if (child.type === GLSL.Parser.Node.Type.Declaration)
                {
                    this.writeDeclaration(child);
                }
            }
            --this._identLevel;
            this
                .beginStatement()
                .write("}")
                .endStatement(false);
            this._loc = Location.MidStatement;
            return this;
        }

        private writeFunction(node: GLSL.Parser.Node.Function): this
        {
            this
                .write(node.children[0].data)
                .writeFunctionArgs(node.children[1])
                .writeStatementList(node.children[2], true);
            this._loc = Location.EndOfStatement;
            return this;
        }

        private writeFunctionArgs(node: GLSL.Parser.Node.FunctionArgs): this
        {
            this.write("(");
            for (let i = 0, l = node.children.length; i < l; ++i)
            {
                if (i > 0)
                {
                    this.write(",");
                    this.write(" ", true);
                }
                const child = node.children[i];
                if (child.type === GLSL.Parser.Node.Type.Declaration)
                {
                    this.writeInlineDeclaration(child);
                }
                else
                {
                    this.writeNode(child);
                }
            }
            this.write(")");
            this._loc = Location.EndOfStatement;
            return this;
        }

        private writeDeclaration(node: GLSL.Parser.Node.Declaration): this
        {
            this
                .beginStatement()
                .writeInlineDeclaration(node)
                .endStatement();
            return this;
        }

        private writeInlineDeclaration(node: GLSL.Parser.Node.Declaration): this
        {
            let lastThingWasStruct = false;
            for (const child of node.children)
            {
                switch (child.type)
                {
                    case GLSL.Parser.Node.Type.Keyword:
                        this.writeKeyword(child);
                        this.write(" ");
                        break;
                    case GLSL.Parser.Node.Type.Identifier:
                        this.writeIdentifier(child);
                        this.write(" ");
                        break;
                    case GLSL.Parser.Node.Type.Structure:
                        this.writeStructure(child);
                        lastThingWasStruct = true;
                        break;
                    case GLSL.Parser.Node.Type.DeclarationList:
                        if (lastThingWasStruct)
                        {
                            this.write(" ", true);
                            lastThingWasStruct = false;
                        }
                        this.writeDeclarationList(child);
                        break;
                    case GLSL.Parser.Node.Type.Function:
                        this.writeFunction(child);
                        break;
                }
            }
            // this._loc = Location.MidStatement;
            return this;
        }

        private writeDeclarationList(node: GLSL.Parser.Node.DeclarationList): this
        {
            for (let i = 0, l = node.children.length; i < l; ++i)
            {
                const child = node.children[i];
                switch (child.type)
                {
                    case GLSL.Parser.Node.Type.Identifier:
                        if (i > 0)
                        {
                            this.write(",");
                            this.write(" ", true);
                        }
                        this.writeIdentifier(child);
                        break;
                    case GLSL.Parser.Node.Type.Quantifier:
                        this
                            .write("[")
                            .writeExpression(child.children[0])
                            .write("]");
                        break;
                    case GLSL.Parser.Node.Type.Expression:
                        this
                            .write(" ", true)
                            .write("=")
                            .write(" ", true)
                            .writeExpression(child);
                        break;
                }
            }
            return this;
        }

        private writeForLoop(node: GLSL.Parser.Node.ForLoop): this
        {
            const [ init, cond, iter ] = node.children;
            this.beginStatement()
                .write("for")
                .write(" ", true)
                .write("(");

            if (init.type === GLSL.Parser.Node.Type.Expression)
            {
                this.writeExpression(init);
            }
            else
            {
                this.writeInlineDeclaration(init);
            }

            this
                .write(";")
                .write(" ", true )
                .writeExpression(cond)
                .write(";")
                .write(" ", true )
                .writeExpression(iter)
                .write(")")
                .endStatement(false);

            this.writeStatementOrStatementList(node.children[3]);
            
            return this;
        }

        private writeWhileLoop(node: GLSL.Parser.Node.WhileLoop): this
        {
            this
                .beginStatement()
                .write("while")
                .write(" ", true)
                .write("(")
                .writeExpression(node.children[0])
                .write(")")
                .endStatement(false);
            
            this.writeStatementOrStatementList(node.children[1]);

            return this;
        }

        private writeIf(node: GLSL.Parser.Node.If): this
        {
            const [ expr, body ] = node.children;

            this
                .beginStatement()
                .write("if")
                .write(" ", true)
                .write("(")
                .writeExpression(expr)
                .write(")")
                .endStatement(false);
            
            this.writeStatementOrStatementList(body);
            
            if (node.children.length > 2)
            {
                this
                    .beginStatement()
                    .write("else")
                    .endStatement(false);

                const elseBody = node.children[2] as (GLSL.Parser.Node.Statement | GLSL.Parser.Node.StatementList);
                this.writeStatementOrStatementList(elseBody, false);
            }

            return this;
        }

        private writeExpression(node: GLSL.Parser.Node.Expression): this
        {
            for (const child of node.children)
            {
                this.writeNode(child);
            }
            return this;
        }

        private writePrecision(node: GLSL.Parser.Node.Precision): this
        {
            this
                .beginStatement()
                .write("precision")
                .write(" ")
                .write(node.children[0].token.data)
                .write(" ")
                .write(node.children[1].token.data)
                .endStatement();
            return this;
        }

        private writeComment(node: GLSL.Parser.Node.Comment): this
        {
            this.beginStatement().write(`// ${node.type}`).endStatement(false);
            return this;
        }

        private writePreprocessor(node: GLSL.Parser.Node.Preprocessor): this
        {
            if (this._loc !== Location.StartOfLine)
            {
                this.writeNewLine(true);
            }
            this.write(node.token.data);
            this._loc = Location.EndOfStatement;
            this.writeNewLine(true);
            return this;
        }

        private writeKeyword(node: GLSL.Parser.Node.Keyword): this
        {
            this.write(node.token.data);
            return this;
        }

        private writeIdentifier(node: GLSL.Parser.Node.Identifier): this
        {
            this.write(node.token.data);
            return this;
        }

        private writeReturn(node: GLSL.Parser.Node.Return): this
        {
            this
                .beginStatement()
                .write("return");
            
            if (node.children.length > 0)
            {
                this.write(" ");
                this.writeExpression(node.children[0]);
            }

            this.endStatement();

            return this;
        }

        private writeContinue(node: GLSL.Parser.Node.Continue): this
        {
            this
                .beginStatement()
                .write("continue")
                .endStatement();
            return this;
        }

        private writeBreak(node: GLSL.Parser.Node.Break): this
        {
            this
                .beginStatement()
                .write("break")
                .endStatement();
            return this;
        }

        private writeDiscard(node: GLSL.Parser.Node.Discard): this
        {
            this
                .beginStatement()
                .write("discard")
                .endStatement();
            return this;
        }

        private writeDoWhile(node: GLSL.Parser.Node.DoWhile): this
        {
            this
                .beginStatement()
                .write("do")
                .endStatement(false);
            
            this.writeStatementOrStatementList(node.children[0]);
            
            this
                .beginStatement()
                .write("while")
                .write(" ", true)
                .write("(")
                .writeExpression(node.children[1])
                .write(")")
                .endStatement();
            
            return this;
        }

        private writeBinary(node: GLSL.Parser.Node.Binary): this
        {
            if (node.data === "[")
            {
                this
                    .writeNode(node.children[0])
                    .write("[")
                    .writeNode(node.children[1])
                    .write("]");
            }
            else
            {
                this
                    .writeNode(node.children[0])
                    .write(" ", true)
                    .write(node.data)
                    .write(" ", true)
                    .writeNode(node.children[1]);
            }
            return this;
        }

        private writeTernary(node: GLSL.Parser.Node.Ternary): this
        {
            this
                .writeNode(node.children[0])
                .write(" ", true)
                .write("?")
                .write(" ", true)
                .writeNode(node.children[1])
                .write(" ", true)
                .write(":")
                .write(" ", true)
                .writeNode(node.children[2]);
            return this;
        }

        private writeUnary(node: GLSL.Parser.Node.Unary): this
        {
            this
                .write(node.data)
                .writeNode(node.children[0]);
            return this;
        }

        private writeSuffix(node: GLSL.Parser.Node.Suffix): this
        {
            this
                .writeNode(node.children[0])
                .write(node.data);
            return this;
        }

        private writeAssign(node: GLSL.Parser.Node.Assign): this
        {
            this
                .writeNode(node.children[0])
                .write(" ", true)
                .write(node.data)
                .write(" ", true)
                .writeNode(node.children[1]);
            return this;
        }

        private writeCall(node: GLSL.Parser.Node.Call): this
        {
            // Log.debug(`Found call (${node.children[0].token.data}), parent is ${node.parent.type}`);
            this
                .writeNode(node.children[0])
                .write("(");
            
            for (let i = 1, l = node.children.length; i < l; ++i)
            {
                const child = node.children[i];
                if (i > 1)
                {
                    this.write(",");
                    this.write(" ", true);
                }
                this.writeNode(child);
            }

            this.write(")");
            
            return this;
        }

        private writeBuiltIn(node: GLSL.Parser.Node.BuiltIn): this
        {
            this.write(node.data);
            return this;
        }

        private writeOperator(node: GLSL.Parser.Node.Operator): this
        {
            if (node.data === ".")
            {
                this
                    .writeNode(node.children[0])
                    .write(".")
                    .writeNode(node.children[1]);
            }
            else
            {
                console.log(node);
                throw new Error("Unhandled operator node");
            }
            return this;
        }

        private writeLiteral(node: GLSL.Parser.Node.Literal): this
        {
            this.write(node.data);
            return this;
        }

        private writeGroup(node: GLSL.Parser.Node.Group): this
        {
            this
                .write("(")
                .writeNode(node.children[0])
                .write(")");
            return this;
        }

        // #endregion
    }
}