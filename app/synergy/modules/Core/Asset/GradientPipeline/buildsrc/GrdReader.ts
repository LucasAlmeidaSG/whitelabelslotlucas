/// <reference path="BufferStream.ts" />

namespace RS.AssetPipeline
{

    export interface ColourStopColour
    {
        r: number;
        g: number;
        b: number;
    }

    export interface ColourStopInfo
    {
        location: number;
        midpoint: number;
        type: "FrgC" | "BckC" | "UsrS";
        color?: ColourStopColour;
    }

    export interface TransStopInfo
    {
        location: number;
        midpoint: number;
        opacity: number;
    }

    export interface GradientInfo
    {
        name: string;
        form: "CstS" | "ClNs";
        interpolation?: number;
        colourStops?: ColourStopInfo[];
        transStops?: TransStopInfo[];
        showTrans?: boolean;
        vectorColor?: boolean;
        seed?: number;
        smooth?: number;
        colourSpace?: "RGBC" | "HSBl" | "LbCl";
        Mins?: number[];
        Maxs?: number[];
    }

    export interface GradientFile
    {
        gradients: { [name: string]: GradientInfo };
        foreground: string;
        background: string;
    }

    export class GrdReader
    {
        protected _json: GradientFile;
        protected _foreground: ColourStopColour;
        protected _background: ColourStopColour;

        public get json() { return this._json; }

        constructor(file: BufferStream)
        {
            const header = file.readInt32BE(0);

            if (header !== this.ToInt32("8BGR"))
            {
                throw new Error("Not a valid gradient file.");
            }

            const version = file.readInt16BE(0);

            if (version !== 5)
            {
                throw new Error(`Invalid Gradient file version, must be 5, got ${version}.`);
            }

            file.skip(14);

            while (!file.eof())
            {
                const descriptor = file.readInt32BE(0);
                switch (descriptor)
                {
                    case this.ToInt32("null"):
                        file.readInt32BE(0);
                        break;

                    case this.ToInt32("GrdL"):
                        this._json = this.parseGrdL(file);
                        break;
                    default:
                        throw new Error(`Invalid descriptor '${this.FromInt32(descriptor)}'.`);
                }
                file.readInt32BE(0);
            }
        }

        public getGradient(name: string): GradientInfo | null
        {
            if (this._json.gradients[name])
            {
                return this._json.gradients[name];
            }
            else
            {
                return null;
            }
        }

        protected ToInt32(str: string): number
        {
            let value = 0;
            for (const chr of str)
            {
                value = (value << 8) + chr.charCodeAt(0);
            }
            return value;
        }

        protected FromInt32(value: number): string
        {
            let str = "";
            while (value)
            {
                str = String.fromCharCode(value & 0xff) + str;
                value >>= 8;
            }

            return str;
        }

        protected parseVlLs(file: BufferStream): number
        {
            this.checkDescriptor(file, "VlLs");
            return file.readInt32BE(0);
        }

        protected parseUnicodeString(file: BufferStream): string
        {
            let result = "";
            const length = file.readInt32BE(0);

            for (let chr = 0; chr < length; ++chr)
            {
                const value = file.readInt16BE(0);
                if (value)
                {
                    result += String.fromCharCode(value);
                }
            }
            file.readInt32BE(0);
            return result;
        }

        protected readObject(file: BufferStream)
        {
            this.checkDescriptor(file, "Objc");
            const str = this.parseUnicodeString(file);
            return str;
        }

        protected checkDescriptor(file: BufferStream, desc: string)
        {
            const descriptor = file.readInt32BE(0);
            if (descriptor !== this.ToInt32(desc))
            {
                throw new Error(`Expected ${desc}, got "${this.FromInt32(descriptor)}".`);
            }
        }

        protected readString(file: BufferStream)
        {
            this.checkDescriptor(file, "TEXT");
            return this.parseUnicodeString(file);
        }

        protected readEnum(file: BufferStream)
        {
            this.checkDescriptor(file, "enum");
            file.readInt32BE(0);
            file.readInt32BE(0);    // Type
            file.readInt32BE(0);
            const value = file.readInt32BE(0);
            file.readInt32BE(0);

            return this.FromInt32(value);
        }

        protected readDouble(file: BufferStream)
        {
            this.checkDescriptor(file, "doub");
            const value = file.readDoubleBE(0);
            file.readInt32BE(0);
            return value;
        }

        protected readUntF(file: BufferStream)
        {
            this.checkDescriptor(file, "UntF");
            this.checkDescriptor(file, "#Prc");
            const value = file.readDoubleBE(0);
            file.readInt32BE(0);
            return value;
        }

        protected readAng(file: BufferStream)
        {
            this.checkDescriptor(file, "UntF");
            this.checkDescriptor(file, "#Ang");
            const value = file.readDoubleBE(0);
            file.readInt32BE(0);
            return value;
        }

        protected readLong(file: BufferStream)
        {
            this.checkDescriptor(file, "long");
            const value = file.readInt32BE(0);
            file.readInt32BE(0);
            return value;
        }

        protected rgb(r: number, g: number, b: number)
        {
            return { r: Math.round(r), g: Math.round(g), b: Math.round(b) };
        }

        protected readCmyk(file: BufferStream)
        {
            const items = file.readInt32BE(0);
            file.readInt32BE(0);
            let cyan = 0;
            let magenta = 0;
            let yellow = 0;
            let black = 0;

            for (let item = 0; item < items; ++item)
            {
                const descriptor = file.readInt32BE(0);
                switch (descriptor)
                {
                    case this.ToInt32("Cyn "):
                        cyan = this.readDouble(file) / 100;
                        break;
                    case this.ToInt32("Mgnt"):
                        magenta = this.readDouble(file) / 100;
                        break;
                    case this.ToInt32("Ylw "):
                        yellow = this.readDouble(file) / 100;
                        break;
                    case this.ToInt32("Blck"):
                        black = this.readDouble(file) / 100;
                        break;
                }
            }
            const red = 255 * (1 - cyan) * (1 - black);
            const green = 255 * (1 - magenta) * (1 - black);
            const blue = 255 * (1 - yellow) * (1 - black);

            return this.rgb(red, green, blue);
        }

        protected readGrey(file: BufferStream)
        {
            const items = file.readInt32BE(0);
            file.readInt32BE(0);
            let grey = 0;
            for (let item = 0; item < items; ++item)
            {
                this.checkDescriptor(file, "Gry ");
                grey = this.readDouble(file) / 100;
            }
            const red = 255 * grey;

            return this.rgb(red, red, red);
        }

        protected readRgb(file)
        {
            const items = file.readInt32BE(0);
            file.readInt32BE(0);

            let red = 0;
            let green = 0;
            let blue = 0;

            for (let item = 0; item < items; ++item)
            {
                const descriptor = file.readInt32BE(0);
                switch (descriptor)
                {
                    case this.ToInt32("Rd  "):
                        red = this.readDouble(file);
                        break;
                    case this.ToInt32("Grn "):
                        green = this.readDouble(file);
                        break;
                    case this.ToInt32("Bl  "):
                        blue = this.readDouble(file);
                        break;
                }
            }
            return this.rgb(red, green, blue);
        }

        protected readHsb(file)
        {
            const items = file.readInt32BE(0);
            file.readInt32BE(0);

            let hue = 0;
            let sat = 0;
            let bright = 0;

            for (let item = 0; item < items; ++item)
            {
                const descriptor = file.readInt32BE(0);

                switch (descriptor)
                {
                    case this.ToInt32("H   "):
                        hue = this.readAng(file);
                        break;
                    case this.ToInt32("Strt"):
                        sat = this.readDouble(file) / 100;
                        break;
                    case this.ToInt32("Brgh"):
                        bright = this.readDouble(file) / 100;
                        break;
                }
            }

            let red = 0;
            let green = 0;
            let blue = 0;

            let hh: number, p: number, q: number, t: number, ff: number;
            let i: number;

            if (sat <= 0.0)
            {       // < is bogus, just shuts up warnings
                red = bright;
                green = bright;
                blue = bright;
            }
            else
            {
                hh = hue;
                if (hh >= 360.0) { hh = 0.0; }
                hh /= 60.0;
                i = hh | 0;
                ff = hh - i;
                p = bright * (1.0 - sat);
                q = bright * (1.0 - (sat * ff));
                t = bright * (1.0 - (sat * (1.0 - ff)));

                switch (i)
                {
                    case 0:
                        red = bright;
                        green = t;
                        blue = p;
                        break;
                    case 1:
                        red = q;
                        green = bright;
                        blue = p;
                        break;
                    case 2:
                        red = p;
                        green = bright;
                        blue = t;
                        break;

                    case 3:
                        red = p;
                        green = q;
                        blue = bright;
                        break;
                    case 4:
                        red = t;
                        green = p;
                        blue = bright;
                        break;
                    case 5:
                    default:
                        red = bright;
                        green = p;
                        blue = q;
                        break;
                }
            }
            return this.rgb(red * 255, green * 255, blue * 255);
        }

        protected readColour(file: BufferStream): ColourStopColour
        {
            let colour = { r: 0, g: 0, b: 0 };
            this.readObject(file);

            const type = file.readInt32BE(0);
            switch (type)
            {
                case this.ToInt32("CMYC"):
                    colour = this.readCmyk(file);
                    break;
                case this.ToInt32("HSBC"):
                    colour = this.readHsb(file);
                    break;
                case this.ToInt32("RGBC"):
                    colour = this.readRgb(file);
                    break;
                case this.ToInt32("Grsc"):
                    colour = this.readGrey(file);
                    break;
            }

            return colour;
        }

        protected readColourStop(file: BufferStream): ColourStopInfo
        {
            const stop: ColourStopInfo = { location: 0, midpoint: 0, type: "UsrS" };
            const items = file.readInt32BE(0);
            file.readInt32BE(0);

            for (let item = 0; item < items; ++item)
            {
                const descriptor = file.readInt32BE(0);
                switch (descriptor)
                {
                    case this.ToInt32("Lctn"):
                        stop.location = this.readLong(file);
                        break;

                    case this.ToInt32("Mdpn"):
                        stop.midpoint = file.readInt32BE(4);
                        break;

                    case this.ToInt32("Type"):
                        stop.type = this.readEnum(file) as "BckC" | "FrgC" | "UsrS";
                        break;

                    case this.ToInt32("Clr "):
                        stop.color = this.readColour(file);
                        break;

                    default:
                        throw new Error(`Unknown Descriptor: ${this.FromInt32(descriptor)}`);
                }
            }

            return stop;
        }

        protected readColourStops(file: BufferStream): ColourStopInfo[]
        {
            const length = this.parseVlLs(file);
            const stops: ColourStopInfo[] = [];
            for (let stop = 0; stop < length; ++stop)
            {
                this.readObject(file);
                this.checkDescriptor(file, "Clrt");
                stops.push(this.readColourStop(file));
            }
            file.readInt32BE(0);

            return stops;
        }

        protected readTransStop(file: BufferStream): TransStopInfo
        {
            const stop: TransStopInfo = { location: 0, midpoint: 0, opacity: 0 };
            const items = file.readInt32BE(0);
            file.readInt32BE(0);

            for (let item = 0; item < items; ++item)
            {
                const descriptor = file.readInt32BE(0);
                switch (descriptor)
                {
                    case this.ToInt32("Lctn"):
                        stop.location = this.readLong(file);
                        break;

                    case this.ToInt32("Mdpn"):
                        stop.midpoint = this.readLong(file);
                        break;

                    case this.ToInt32("Opct"):
                        stop.opacity = this.readUntF(file);
                        break;

                    default:
                        throw new Error(`Unexpected descriptor: ${this.FromInt32(descriptor)}`);
                }
            }
            file.rewind(4);

            return stop;
        }

        protected readTransStops(file: BufferStream): TransStopInfo[]
        {
            const length = this.parseVlLs(file);
            const stops: TransStopInfo[] = [];
            for (let stop = 0; stop < length; ++stop)
            {
                this.readObject(file);
                this.checkDescriptor(file, "TrnS");
                stops.push(this.readTransStop(file));
            }
            file.readInt32BE(0);

            return stops;
        }

        protected readBool(file: BufferStream): boolean
        {
            this.checkDescriptor(file, "bool");
            const value = file.readInt8(0);
            file.readInt32BE(0);

            return value ? true : false;
        }

        protected readLongArray(file: BufferStream): number[]
        {
            const numbers = [];

            const items = this.parseVlLs(file);

            for (let item = 0; item < items; ++item)
            {
                numbers.push(this.readLong(file));
                file.rewind(4);
            }

            file.readInt32BE(0);

            return numbers;
        }

        protected parseGrdL(file: BufferStream): GradientFile
        {
            const grdl: GradientFile = { gradients: {}, foreground: "#ffffff", background: "#000000" };

            const items = this.parseVlLs(file);

            if (items === -1)
            {
                throw new Error(`Invalid item count ${items}.`);
            }

            for (let item = 0; item < items; ++item)
            {
                this.readObject(file);

                this.checkDescriptor(file, "Grdn");
                file.readInt32BE(0);
                file.readInt32BE(0);

                this.checkDescriptor(file, "Grad");
                this.readObject(file);

                this.checkDescriptor(file, "Grdn");
                const count = file.readInt32BE(0);
                file.readInt32BE(0);

                const grad: GradientInfo = { name: null, form: "CstS" };

                for (let value = 0; value < count; ++value)
                {
                    const descriptor = file.readInt32BE(0);
                    switch (descriptor)
                    {
                        case this.ToInt32("Nm  "):
                            grad.name = this.readString(file);
                            // If we've got an equals then use the part after the equals sign
                            if (grad.name.indexOf("=") >= 0)
                            {
                                grad.name = grad.name.split("=")[1];
                            }
                            break;

                        case this.ToInt32("GrdF"):
                            grad.form = this.readEnum(file) as "CstS" | "ClNs";
                            break;

                        case this.ToInt32("Intr"):
                            grad.interpolation = this.readDouble(file);
                            break;

                        case this.ToInt32("Clrs"):
                            grad.colourStops = this.readColourStops(file);
                            break;

                        case this.ToInt32("Trns"):
                            grad.transStops = this.readTransStops(file);
                            break;

                        case this.ToInt32("RndS"):
                            grad.seed = this.readLong(file);
                            break;

                        case this.ToInt32("ShTr"):
                            grad.showTrans = this.readBool(file);
                            break;

                        case this.ToInt32("VctC"):
                            grad.vectorColor = this.readBool(file);
                            break;

                        case this.ToInt32("Smth"):
                            grad.smooth = this.readLong(file);
                            break;

                        case this.ToInt32("Mnm "):
                            grad.Mins = this.readLongArray(file);
                            break;

                        case this.ToInt32("Mxm "):
                            grad.Maxs = this.readLongArray(file);
                            break;

                        case this.ToInt32("Smooth"):
                            grad.smooth = this.readLong(file);
                            break;

                        case this.ToInt32("ClrS"):
                            grad.colourSpace = this.readEnum(file) as "RGBC" | "HSBl" | "LbCl";
                            break;

                        default:
                            throw new Error(`Unknown descriptor: "${this.FromInt32(descriptor)}"`);
                    }
                }
                file.rewind(4);

                grdl.gradients[grad.name] = grad;
            }

            return grdl;
        }
    }
}
