// <reference path="GrdReader.ts" />
namespace RS.AssetPipeline
{
    /**
     * Gradient asset specific definition data.
     */
    export interface GradientDefinition extends BaseVariant
    {
        gradients?: string[];
        foreground?: string;
        background?: string;
    }

    /**
     * Represents an image-type asset.
     */
    @AssetType("gradient")
    @InferFromExtensions([".grd"])
    export class Gradient extends BaseAsset
    {
        private _gradients;

        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return ["json"]; }

        public get classModule() { return "Core.Asset.GradientPipeline"; }

        public async load()
        {
            await super.load();

            const assetPath = Path.combine(Module.getModule(this._definition.module).path, Constants.inAssetsFolder, Path.directoryName(this._definition.manifestPath), this._definition.path as string);
            const definition = this._definition as GradientDefinition;

            const buffer = new BufferStream(await FileSystem.readFile(assetPath, true));
            const reader = new GrdReader(buffer);

            const gradients: string[] = definition.gradients ? definition.gradients.slice() : [];

            if (gradients.length === 0)
            {
                for (const gradient in reader.json.gradients)
                {
                    gradients.push(gradient);
                }
            }

            const json: GradientFile = { gradients: {}, foreground: definition.foreground || "#ffffff", background: definition.background || "#000000" };

            for (const gradient of gradients)
            {
                json[gradient] = reader.json.gradients[gradient];
            }
            this._gradients = json;

        }
        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition | null
        {
            const ref = AssetPipeline.emitAssetReference(this, "RS.Asset.GradientReference");
            for (const gradientName of Object.getOwnPropertyNames(this._gradients))
            {
                const cleanedName = AssetPipeline.cleanName(gradientName);
                if (cleanedName !== "")
                {
                    AssetPipeline.addAssetReferenceProperty(ref, cleanedName, `() => ${this.id}.get("${gradientName}") as RS.Rendering.Gradient.Stop[]`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "() => RS.Rendering.Gradient.Stop[]" });
                }
            }
            return ref;
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: GradientDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            if (format !== "json")
            {
                Log.error(`Unsupported format '${format}'`);
                return null;
            }
            if (!dry)
            {
                const buffer = new BufferStream(await FileSystem.readFile(assetPath, true));
                const reader = new GrdReader(buffer);

                const outputPath = Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath)));

                const gradients: string[] = variant.gradients ? variant.gradients.slice() : [];

                if (gradients.length === 0)
                {
                    for (const gradient in reader.json.gradients)
                    {
                        gradients.push(gradient);
                    }
                }

                const json: { [name: string]: GradientInfo } = {};
                const outputFile = Path.replaceExtension(outputPath, ".json");

                for (const gradient of gradients)
                {
                    json[gradient] = reader.json.gradients[gradient];
                }
                this._gradients = json;
                await FileSystem.writeFile(outputFile, JSON.stringify(json));
            }

            // Return variant data
            return {
                paths: [Path.sanitise(Path.replaceExtension(assetPath, ".json"))]
            };
        }
    }
}
