namespace RS.AssetPipeline
{
    export class BufferStream
    {
        private _offset = 0;
        private _buffer: Buffer;

        constructor(buffer: Buffer)
        {
            this._buffer = buffer;
            this._offset = 0;
        }

        public skip(offset: number)
        {
            this._offset += offset;
        }

        public eof()
        {
            return (this._offset >= this._buffer.length);
        }

        public readInt32BE(offset: number)
        {
            this._offset += offset;
            if (this._offset <= this._buffer.length - 4)
            {
                const value = this._buffer.readInt32BE(this._offset);
                this._offset += 4;
                return value;
            }
            else
            {
                return 0;
            }
        }

        public readDoubleBE(offset: number)
        {
            this._offset += offset;
            if (this._offset <= this._buffer.length - 8)
            {
                const value = this._buffer.readDoubleBE(this._offset);
                this._offset += 8;

                return value;
            }
            else
            {
                return 0;
            }
        }

        public readInt16BE(offset: number)
        {
            this._offset += offset;
            if (this._offset <= this._buffer.length - 2)
            {
                const value = this._buffer.readInt16BE(this._offset);
                this._offset += 2;

                return value;
            }
            else
            {
                return 0;
            }
        }

        public readInt8(offset: number)
        {
            this._offset += offset;
            if (this._offset < this._buffer.length)
            {
                const value = this._buffer.readInt8(this._offset);
                this._offset += 1;

                return value;
            }
            else
            {
                return 0;
            }
        }

        public rewind(offset: number)
        {
            this._offset -= offset;
        }
    }

}
