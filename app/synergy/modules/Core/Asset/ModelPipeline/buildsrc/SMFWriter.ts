namespace RS.AssetPipeline
{
    export type VertexBufferForDataType = [ Float32Array, Uint8Array ];

    const magic = "SMF ";

    /**
     * Encapsulates a writer for SMF (Synergy Model Format) files.
     */
    export class SMFWriter
    {
        protected _vertexDescriptors: SMFWriter.VertexDescriptor[] = [];
        protected _vertexBuffers: VertexBufferForDataType[number][] = [];

        protected _elementDescriptors: SMFWriter.ElementDescriptor[] = [];
        protected _elementBuffers: Uint16Array[] = [];

        /** Gets a view of the vertex descriptors. */
        public get vertexDescriptors(): ReadonlyArray<SMFWriter.VertexDescriptor> { return this._vertexDescriptors; }

        /** Gets a view of the vertex buffers. */
        public get vertexBuffers(): ReadonlyArray<VertexBufferForDataType[number]> { return this._vertexBuffers; }

        /** Gets a view of the element descriptors. */
        public get elementDescriptors(): ReadonlyArray<SMFWriter.ElementDescriptor> { return this._elementDescriptors; }

        /** Gets a view of the element buffers. */
        public get elementBuffers(): ReadonlyArray<Uint16Array> { return this._elementBuffers; }

        /**
         * Adds a vertex descriptor with data to the SMF file.
         * @param descriptor
         * @param buffer
         */
        public addVertexDescriptor<T extends SMFWriter.DataType>(descriptor: SMFWriter.VertexDescriptor<T>, buffer: VertexBufferForDataType[T])
        {
            this._vertexDescriptors.push(descriptor);
            this._vertexBuffers.push(buffer);
        }

        /**
         * Adds an element descriptor with data to the SMF file.
         * @param descriptor
         * @param buffer
         */
        public addIndexDescriptor(descriptor: SMFWriter.ElementDescriptor, buffer: Uint16Array)
        {
            this._elementDescriptors.push(descriptor);
            this._elementBuffers.push(buffer);
        }

        /**
         * Writes the SMF file.
         */
        public write(): Buffer
        {
            const buffer = Buffer.alloc(this.getFileSize());
            const strm = new BufferStreamWriter(buffer);

            // Magic header
            strm.writeString(magic, BufferStream.StringType.Raw);

            // Header
            strm.writeUInt8(this._vertexDescriptors.length);
            strm.writeUInt8(this._elementDescriptors.length);
            strm.advance(2);

            // Vertex descriptors
            let bufferOffset = 8
                + 12 * this._vertexDescriptors.length
                + 12 * this._elementDescriptors.length;
            for (let i = 0, l = this._vertexDescriptors.length; i < l; ++i)
            {
                const vDesc = this._vertexDescriptors[i];
                const vBuffer = this._vertexBuffers[i];
                strm.writeUInt8(vDesc.dataType);
                strm.writeUInt8(vDesc.usage);
                strm.writeUInt8(vDesc.elementsPerVertex);
                strm.writeUInt8(vDesc.normalised ? 1 : 0);
                strm.writeUInt32(bufferOffset);
                strm.writeUInt32(vBuffer.byteLength);
                bufferOffset += vBuffer.byteLength;
            }

            // Element descriptors
            for (let i = 0, l = this._elementDescriptors.length; i < l; ++i)
            {
                const elDesc = this._elementDescriptors[i];
                const elBuffer = this._elementBuffers[i];
                strm.writeUInt8(elDesc.geometryType);
                strm.advance(3);
                strm.writeUInt32(bufferOffset);
                strm.writeUInt32(elBuffer.byteLength);
                bufferOffset += elBuffer.byteLength;
            }

            // Vertex buffers
            bufferOffset = 8
                + 12 * this._vertexDescriptors.length
                + 12 * this._elementDescriptors.length;
            if (strm.currentOffset !== bufferOffset)
            {
                Log.warn(`After writing descriptors, ptr is ${strm.currentOffset} but expected ${bufferOffset}`);
            }
            for (const vBuffer of this._vertexBuffers)
            {
                if (vBuffer instanceof Float32Array)
                {
                    for (let i = 0, l = vBuffer.length; i < l; ++i)
                    {
                        strm.writeFloat(vBuffer[i]);
                    }
                }
                else
                {
                    for (let i = 0, l = vBuffer.length; i < l; ++i)
                    {
                        strm.writeUInt8(vBuffer[i]);
                    }
                }
            }

            // Element buffers
            for (const elBuffer of this._elementBuffers)
            {
                for (let i = 0, l = elBuffer.length; i < l; ++i)
                {
                    strm.writeUInt16(elBuffer[i]);
                }
            }

            // Done
            return buffer;
        }

        /**
         * Calculates how large the output SMF file will be.
         */
        public getFileSize(): number
        {
            return 8
                + 12 * this._vertexDescriptors.length
                + 12 * this._elementDescriptors.length
                + this._vertexBuffers.map((b) => b.byteLength).reduce((a, b) => a + b, 0)
                + this._elementBuffers.map((b) => b.byteLength).reduce((a, b) => a + b, 0);
        }
    }

    export namespace SMFWriter
    {
        /**
         * Encapsulates a datatype for a vertex element.
         */
        export enum DataType
        {
            Float,
            Byte
        }

        /**
         * Encapsulates a geometry type.
         */
        export enum GeometryType
        {
            Lines,
            Triangles
        }

        /**
         * Encapsulates how vertex data should be consumed.
         */
        export enum VertexUsage
        {
            Position,
            Color,
            Normal,
            TexCoord
        }

        export interface VertexDescriptor<TDataType extends DataType = DataType>
        {
            /** The data type of each element. */
            dataType: TDataType;

            /** How data should be consumed. */
            usage: VertexUsage;

            /** The number of elements per vertex. */
            elementsPerVertex: number;

            /** Whether elements should be normalised. */
            normalised: boolean;
        }

        export interface ElementDescriptor
        {
            /** The type of geometry for the elements. */
            geometryType: GeometryType;
        }
    }
}