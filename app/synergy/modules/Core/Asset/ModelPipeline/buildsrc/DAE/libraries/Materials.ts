namespace RS.DAE.Libraries
{
    import XMLNode = XML2JSParser.XMLNode;

    export namespace Material
    {
        export enum Kind { Lambert, Phong, Blinn }

        export interface Base<TKind extends Kind>
        {
            kind: TKind;
            emission?: number[];
            ambient?: number[];
            diffuse?: number[];
            reflective?: number[];
            reflectivity?: number;
            transparent?: number[];
            transparency?: number;
            indexOfRefraction?: number;
        }

        export type Lambert = Base<Kind.Lambert>;

        export interface Blinn extends Base<Kind.Blinn>
        {
            specular?: number[];
            shininess?: number;
            indexOfRefraction?: number;
        }

        export interface Phong extends Base<Kind.Phong>
        {
            specular?: number[];
            shininess?: number;
            indexOfRefraction?: number;
        }
    }

    export interface MaterialData
    {
        materials: Util.Map<Material>;
    }

    export type Material = Material.Lambert | Material.Phong | Material.Blinn;

    export function parseLibraryMaterials(libraryMaterials: XMLNode, libraryEffects: XMLNode): MaterialData
    {
        const result: MaterialData =
        {
            materials: {}
        };
        const materials = libraryMaterials.material as XMLNode[];
        if (materials == null) { return result; }

        const effects = libraryEffects.effect as XMLNode[];
        if (effects == null) { return result; }
        const effectsMap: Util.Map<XMLNode> = {};
        for (const effect of effects)
        {
            effectsMap[effect.$.id] = effect;
        }

        for (const material of materials)
        {
            const id = material.$.id;
            const instanceEffect = material.instance_effect[0] as XMLNode;
            if (instanceEffect)
            {
                const ref = instanceEffect.$.url as string;
                let effect = effectsMap[ref.substr(1)];
                if (effect)
                {
                    const mat = parseEffect(effect);
                    if (mat) { result.materials[id] = mat; }
                }
                else
                {
                    Log.warn(`Material '${id}' refers to non-existant effect '${ref}'`);
                }
            }
        }
        return result;
    }

    function parseEffect(effect: XMLNode): Material | null
    {
        const profileCommon = effect.profile_COMMON as XMLNode[];
        if (!profileCommon || profileCommon.length === 0)
        {
            Log.warn(`Effect ${effect.$.id} has unsupported profile (only profile_COMMON is supported)`);
            return null;
        }
        const technique = profileCommon[0].technique[0] as XMLNode;
        if (technique == null)
        {
            Log.warn(`Effect ${effect.$.id} has no technique`);
            return null;
        }
        const phong = (technique.phong && technique.phong[0] || null) as XMLNode;
        if (phong != null)
        {
            return {
                kind: Material.Kind.Phong,
                emission: phong.emission && getColor(phong.emission[0] as XMLNode),
                ambient: phong.ambient && getColor(phong.ambient[0] as XMLNode),
                diffuse: phong.diffuse && getColor(phong.diffuse[0] as XMLNode),
                specular: phong.specular && getColor(phong.specular[0] as XMLNode),
                shininess: phong.shininess && getFloat(phong.shininess[0] as XMLNode),
                reflective: phong.reflective && getColor(phong.reflective[0] as XMLNode),
                reflectivity: phong.reflectivity && getFloat(phong.reflectivity[0] as XMLNode),
                transparent: phong.transparent && getColor(phong.transparent[0] as XMLNode),
                transparency: phong.transparency && getFloat(phong.transparency[0] as XMLNode) || 1.0,
                indexOfRefraction: phong.indexOfRefraction && getFloat(phong.indexOfRefraction[0] as XMLNode)
            };
        }
        const lambert = (technique.lambert && technique.lambert[0] || null) as XMLNode;
        if (lambert != null)
        {
            return {
                kind: Material.Kind.Lambert,
                emission: lambert.emission && getColor(lambert.emission[0] as XMLNode),
                ambient: lambert.ambient && getColor(lambert.ambient[0] as XMLNode),
                diffuse: lambert.diffuse && getColor(lambert.diffuse[0] as XMLNode),
                reflective: lambert.reflective && getColor(lambert.reflective[0] as XMLNode),
                reflectivity: lambert.reflectivity && getFloat(lambert.reflectivity[0] as XMLNode),
                transparent: lambert.transparent && getColor(lambert.transparent[0] as XMLNode),
                transparency: lambert.transparency && getFloat(lambert.transparency[0] as XMLNode) || 1.0,
                indexOfRefraction: lambert.indexOfRefraction && getFloat(lambert.indexOfRefraction[0] as XMLNode)
            };
        }
        const blinn = (technique.blinn && technique.blinn[0] || null) as XMLNode;
        if (blinn != null)
        {
            return {
                kind: Material.Kind.Blinn,
                emission: getColor(blinn.emission ? blinn.emission[0] as XMLNode : {}),
                ambient: getColor(blinn.ambient ? blinn.ambient[0] as XMLNode : {}),
                diffuse: getColor(blinn.diffuse ? blinn.diffuse[0] as XMLNode : {}),
                specular: getColor(blinn.specular ? blinn.specular[0] as XMLNode : {}),
                shininess: getFloat(blinn.shininess ? blinn.shininess[0] as XMLNode : {}),
                reflective: getColor(blinn.reflective ? blinn.reflective[0] as XMLNode : {}),
                reflectivity: getFloat(blinn.reflectivity ? blinn.reflectivity[0] as XMLNode : {}),
                transparent: getColor(blinn.transparent ? blinn.transparent[0] as XMLNode : {}),
                transparency: getFloat(blinn.transparency ? blinn.transparency[0] as XMLNode : {}) || 1.0,
                indexOfRefraction: getFloat(blinn.indexOfRefraction ? blinn.indexOfRefraction[0] as XMLNode : {})
            };
        }
        Log.warn(`Effect ${effect.$.id} a technique with an unsupported lighting model`);
        return null;
    }

    function getColor(node: XMLNode, defaultValue = [ 1.0, 1.0, 1.0, 1.0 ]): number[]
    {
        if (node.color && node.color.length > 0)
        {
            const raw = node.color[0];
            if (Is.string(raw))
            {
                return parseFloatArray(raw);
            }
            else
            {
                return parseFloatArray(raw._);
            }
        }
        else if (node.float && node.float.length > 0)
        {
            const raw = node.float[0];
            let val: number;
            if (Is.string(raw))
            {
                val = parseFloat(raw);
            }
            else
            {
                val = parseFloat(raw._.trim());
            }
            return [ val, val, val, 1.0 ];
        }
        else
        {
            return defaultValue;
        }
    }

    function getFloat(node: XMLNode, defaultValue = 1.0): number
    {
        const color = getColor(node, [ defaultValue, defaultValue, defaultValue ]);
        return (color[0] + color[1] + color[2]) / 3;
    }
}