namespace RS.DAE.Libraries
{
    import XMLNode = XML2JSParser.XMLNode;

    const mat4Multiply = require("gl-mat4/multiply");

    export interface ControllersData
    {
        armatureName?: string;
        vertexJointWeights: { [index: number]: number; }[];
        jointInverseBindPoses: { [index: number]: number[]; };
        jointNamePositionIndex: Util.Map<number>;
    }

    // TODO: Read technique_commons instead of hard coding attribute locations
    export function parseLibraryControllers(library_controllers: XMLNode): ControllersData | null
    {
        const controller = library_controllers.controller[0] as XMLNode;
        if (!controller) { return null; }

        // Get the name of the armature that this model is a child of
        const armatureName = controller.$.name || "";

        const skin = controller.skin[0] as XMLNode;
        const vertexWeights = skin.vertex_weights[0] as XMLNode;

        // Number of vertexes that need weights
        // var numVertices = controller[0].skin[0].vertex_weights[0].$.count

        // # of (joint,weight) pairs to read for each vertex
        // TODO: had to trim this.. should I trim everywhere?
        const jointWeightCounts = parseFloatArray(vertexWeights.vcount[0] as string);

        // An array of all possible weights (I think?)
        const weightsArray = parseFloatArray(((skin.source[2] as XMLNode).float_array[0] as XMLNode)._);

        // Every (joint,weight). Use jointWeightCounts to know how many to read per vertex
        const parsedVertexJointWeights: { [key: number]: number; }[] = [];
        const jointsAndWeights = parseFloatArray(vertexWeights.v[0] as string);
        for (const index of jointWeightCounts)
        {
            const numJointWeightsToRead = jointWeightCounts[index];
            parsedVertexJointWeights[index] = {}
            for (let j = 0; j < numJointWeightsToRead; ++j)
            {
                // The particular joint that we are dealing with, and its weighting on this vertex
                const jointNumber = jointsAndWeights.shift();
                const jointWeighting = jointsAndWeights.shift();
                parsedVertexJointWeights[index][jointNumber] = weightsArray[jointWeighting];
            }
        }

        // All of our model's joints
        const orderedJointNames = ((skin.source[0] as XMLNode).Name_array[0] as XMLNode)._.split(" ");

        // Bind shape matrix (inverse bind matrix)
        const bindShapeMatrix = parseFloatArray(skin.bind_shape_matrix[0] as string);

        // The matrices that transforms each of our joints from world space to model space.
        // You typically multiply this with all parent joint bind poses.
        // We do this in `parse-skeletal-animations.js`
        const jointInverseBindPoses: { [index: number]: number[]; } = {};

        const bindPoses = parseFloatArray(((skin.source[1] as XMLNode).float_array[0] as XMLNode)._);

        // A way to look up each joint's index using it's name
        // This is useful for creating bone groups using names
        // but then easily converting them to their index within
        // the collada-dae-parser data structure.
        //  (collada-dae-parser uses index's and not names to store bone data)
        const jointNamePositionIndex: Util.Map<number> = {};
        for (let i = 0, l = orderedJointNames.length; i < l; ++i)
        {
            const jointName = orderedJointNames[i];
            // If we've already encountered this joint we skip it
            // this is meant to handle an issue where Blender was
            // exporting the same joint name twice for my right side bones that were
            // duplicates of my original left side bones. Still not sure when/wju
            // this happens. Must have done something strange. Doesn't happen to
            // every model..
            if (jointNamePositionIndex[jointName] || jointNamePositionIndex[jointName] === 0) { return; }

            const bindPose = bindPoses.slice(16 * i, 16 * i + 16);
            mat4Multiply(bindPose, bindShapeMatrix, bindPose);
            jointInverseBindPoses[i] = bindPose;
            jointNamePositionIndex[jointName] = i;
        }
        // TODO: Should we also export the greatest number of joints for a vertex?
        // This might allow the consumer to use a shader that expects fewer joints
        // when skinning. i.e. mat4 vs mat3 or mat2 for weights
        return {
            jointInverseBindPoses: jointInverseBindPoses,
            jointNamePositionIndex: jointNamePositionIndex,
            vertexJointWeights: parsedVertexJointWeights,
            armatureName: armatureName
        }
    }
}