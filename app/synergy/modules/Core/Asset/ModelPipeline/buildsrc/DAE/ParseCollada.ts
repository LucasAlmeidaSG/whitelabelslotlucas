/// <reference path="libraries/Geometries.ts" />
/// <reference path="libraries/VisualScenes.ts" />
/// <reference path="libraries/Controllers.ts" />
/// <reference path="libraries/SkeletalAnimations.ts" />
/// <reference path="libraries/LotRotScaleAnimations.ts" />

/// <reference path="validation/NoControlBones.ts" />

namespace RS.DAE
{
    const xml2js: XML2JSParser = require("xml2js-parser");

    export interface ParsedCollada
    {
        vertexPositions: number[];
        vertexNormals: number[];
        vertexUVs?: number[];
        subMeshes: Libraries.GeometrySubmesh[];

        armatureName?: string;
        vertexJointWeights?: { [index: number]: number; }[];
        jointInverseBindPoses?: { [index: number]: number[]; };
        jointNamePositionIndex?: Util.Map<number>;
        keyframes?: Libraries.KeyframeData;
        jointParents?: Util.Map<string>;

        materialData?: Libraries.MaterialData;
    }

    // TODO:
    // Use input, accessor, and param attributes instead of hard coding lookups
    // Clean Up Code / less confusing var names
    export function parseCollada(colladaXML: string): ParsedCollada
    {
        const rootNode = xml2js.parseStringSync(colladaXML);
        const colladaNode = rootNode.COLLADA;

        const result: ParsedCollada = { vertexPositions: null, vertexNormals: null, subMeshes: null };

        const geometryData = Libraries.parseLibraryGeometries(colladaNode.library_geometries[0] as XML2JSParser.XMLNode);
        const visualSceneData = Libraries.parseLibraryVisualScenes(colladaNode.library_visual_scenes[0] as XML2JSParser.XMLNode);

        // The joint parents aren't actually joint parents so we get the joint parents..
        // This lib needs a refactor indeed
        // jointParents = {childBone: 'parentBone', anotherChild: 'anotherParent'}
        let jointParents: Util.Map<string> | null = null;
        if (Object.keys(visualSceneData.jointParents).length)
        {
            jointParents = Object.keys(visualSceneData.jointParents)
                .reduce<Util.Map<string>>((accumJointParents, jointName) =>
                {
                    // JSON.stringify {foo: undefined} = {}, os we replace undefined with null
                    // to make sure that we don't lose any keys
                    accumJointParents[jointName] = visualSceneData.jointParents[jointName].parent || null
                    return accumJointParents;
                }, {});
        }

        let controllerData: Libraries.ControllersData | null = null;
        if (colladaNode.library_controllers && colladaNode.library_controllers.length > 0 && !Is.string(colladaNode.library_controllers[0]))
        {
            controllerData = Libraries.parseLibraryControllers(colladaNode.library_controllers[0] as XML2JSParser.XMLNode);
            if (controllerData.armatureName) { result.armatureName = controllerData.armatureName; }
            if (controllerData.vertexJointWeights && Object.keys(controllerData.vertexJointWeights).length > 0)
            {
                result.vertexJointWeights = controllerData.vertexJointWeights;
                result.jointNamePositionIndex = controllerData.jointNamePositionIndex;
                result.jointInverseBindPoses = controllerData.jointInverseBindPoses;

                // The parser only supports deformation bones. Control bones' affects must be baked in before exporting
                Validation.noControlBones(Object.keys(visualSceneData.jointParents), Object.keys(controllerData.jointInverseBindPoses));
            }
        }

        // TODO: Also parse interpolation/intangent/outtangent
        if (colladaNode.library_animations && colladaNode.library_animations.length > 0 && !Is.string(colladaNode.library_animations[0]))
        {
            result.keyframes = Libraries.parseLocRotScaleAnim((colladaNode.library_animations[0] as XML2JSParser.XMLNode).animation as XML2JSParser.XMLNode[]);
            if (Object.keys(result.keyframes).length === 0)
            {
                delete result.keyframes;
            }
            if (controllerData)
            {
                const keyframes = Libraries.parseLibraryAnimations(colladaNode.library_animations[0] as XML2JSParser.XMLNode, visualSceneData, controllerData);
                if (Object.keys(keyframes).length > 0)
                {
                    result.keyframes = keyframes;
                }
            }
        }

        // Materials
        if (colladaNode.library_materials && colladaNode.library_materials.length > 0 && colladaNode.library_effects && colladaNode.library_effects.length > 0)
        {
            result.materialData = Libraries.parseLibraryMaterials(colladaNode.library_materials[0] as XML2JSParser.XMLNode, colladaNode.library_effects[0] as XML2JSParser.XMLNode);
        }

        // Return our parsed collada object
        result.subMeshes = geometryData.subMeshes;
        result.vertexNormals = geometryData.vertexNormals;
        result.vertexPositions = geometryData.vertexPositions;
        if (jointParents)
        {
            result.jointParents = jointParents;
        }
        if (geometryData.vertexUVs && geometryData.vertexUVs.length > 0)
        {
            result.vertexUVs = geometryData.vertexUVs;
        }
        return result;
    }
}