namespace RS.DAE.Libraries
{
    import XMLNode = XML2JSParser.XMLNode;

    export interface JointData
    {
        jointMatrix: number[];
        parent: string;
    }

    export interface VisualSceneData
    {
        jointParents: Util.Map<JointData>;
        armatureScale?: number[];
    }

    export function parseLibraryVisualScenes(library_visual_scenes: XMLNode): VisualSceneData
    {
        const visualScene = library_visual_scenes.visual_scene[0] as XMLNode;
        const parsedJoints: Util.Map<JointData> = {};
        const nodes = visualScene.node as XMLNode[];

        // Some .dae files will export a shrunken model. Here's how to scale it
        let armatureScale = null;
        for (const node of nodes)
        {
            // node.node is the location of all top level parent nodes
            if (node.node)
            {
                if (node.scale && node.scale.length > 0)
                {
                    armatureScale = parseFloatArray((node.scale[0] as XMLNode)._);
                }
                parseJoints(node.node as XMLNode[], null, parsedJoints);
            }

            // Check for an instance controller. If one exists we have a skeleton
            //if (node.instance_controller) {
            // TODO: Do I need to remove the leading `#` ?
            //joints = node.instance_controller[0].skeleton
            //}
        }

        return {
            jointParents: parsedJoints,
            armatureScale: armatureScale
        };
    }

    // Recursively parse child joints
    function parseJoints(node: XMLNode[], parentJointName: string, accumulator: Util.Map<JointData>): Util.Map<JointData>
    {
        accumulator = accumulator || {};
        for (const joint of node)
        {
            const jointData = accumulator[joint.$.sid] || (accumulator[joint.$.sid] = { parent: null, jointMatrix: null });
            // The bind pose of the matrix. We don't make use of this right now, but you would
            // use it to render a model in bind pose. Right now we only render the model based on
            // their animated joint positions, so we ignore this bind pose data
            jointData.jointMatrix = parseFloatArray((joint.matrix[0] as XMLNode)._);
            jointData.parent = parentJointName;
            if (joint.node)
            {
                parseJoints(joint.node as XMLNode[], joint.$.sid, accumulator);
            }
        }
        return accumulator;
    }
}