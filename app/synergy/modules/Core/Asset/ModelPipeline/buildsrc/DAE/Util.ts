namespace RS.DAE
{
    /**
     * Parses a whitespace delimited float array (e.g. "0.0 1.0 2.0").
     * @param str 
     */
    export function parseFloatArray(str: string): number[]
    {
        return str
            .trim()
            .split(/[\s\n\r]+/)
            .map(parseFloat);
    }
}