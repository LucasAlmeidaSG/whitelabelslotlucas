namespace RS.DAE.Libraries
{
    import XMLNode = XML2JSParser.XMLNode;

    const mat4Multiply = require("gl-mat4/multiply");
    const mat4Scale = require("gl-mat4/scale");
    const mat4Transpose = require("gl-mat4/transpose");

    export interface KeyframeData
    {
        [frameIndex: number]: number[][];
    }

    type KeyframeJointMatricesMap = { [index: number]: { [index: number]: number; }; };

    // 
    // Parse skinned animations from the libraryAnimations section of the collada file.
    // We only handle skinned animations here, regular location/rotation/scale animations
    // that apply to the entire mesh are handled in parse-loc-rot-scale.js
    // tl;dr if your model has animated bones/joints then that gets handled here
    // 
    // TODO: parse interpolation? or just only support linear interpolation?
    // TODO: Don't hard code attribute location
    // TODO: Make use of require('local-bone-to-world-bone')
    // 
    export function parseLibraryAnimations(libraryAnimations: XMLNode, visualSceneData: VisualSceneData, controllersData: ControllersData): KeyframeData
    {
        const animations = libraryAnimations.animation as XMLNode[];
        const allKeyframes: KeyframeData = {};
        const keyframeJointMatrices: KeyframeJointMatricesMap = {};

        const { jointParents, armatureScale } = visualSceneData;
        const { jointInverseBindPoses, jointNamePositionIndex } = controllersData;

        // First pass.. get all the joint matrices
        for (const anim of animations)
        {
            if (anim.$.id.indexOf("pose_matrix") !== -1)
            {
                // TODO: Is this the best way to get an animations joint target?
                const animatedJointName = (anim.channel[0] as XMLNode).$.target.split("/")[0];
                const currentKeyframes = parseFloatArray(((anim.source[0] as XMLNode).float_array[0] as XMLNode)._);
                const currentJointPoseMatrices = parseFloatArray(((anim.source[1] as XMLNode).float_array[0] as XMLNode)._);

                for (let keyframeIndex = 0, keyframeCnt = currentKeyframes.length; keyframeIndex < keyframeCnt; ++keyframeIndex)
                {
                    keyframeJointMatrices[currentKeyframes[keyframeIndex]] = keyframeJointMatrices[currentKeyframes[keyframeIndex]] || {};
                    const currentJointMatrix = currentJointPoseMatrices.slice(16 * keyframeIndex, 16 * keyframeIndex + 16);
                    if (!jointParents[animatedJointName].parent)
                    {
                        // apply library visual scene transformations to top level parent joint(s)
                        if (armatureScale)
                        {
                            mat4Scale(currentJointMatrix, currentJointMatrix, armatureScale);
                        }
                    }

                    keyframeJointMatrices[currentKeyframes[keyframeIndex]][animatedJointName] = currentJointMatrix;
                };
            }
        }
        // Second pass.. Calculate world matrices
        for (const anim of animations)
        {
            if (anim.$.id.indexOf("pose_matrix") !== -1)
            {
                const animatedJointName = (anim.channel[0] as XMLNode).$.target.split("/")[0];
                const currentKeyframes = parseFloatArray(((anim.source[0] as XMLNode).float_array[0] as XMLNode)._);

                for (let keyframeIndex = 0, keyframeCnt = currentKeyframes.length; keyframeIndex < keyframeCnt; ++keyframeIndex)
                {
                    const currentKeyframe = currentKeyframes[keyframeIndex];
                    allKeyframes[currentKeyframes[keyframeIndex]] = allKeyframes[currentKeyframe] || [];

                    // Multiply by parent world matrix
                    let jointWorldMatrix = getParentWorldMatrix(animatedJointName, currentKeyframe, jointParents, keyframeJointMatrices);

                    // Multiply our joint's inverse bind matrix
                    mat4Multiply(jointWorldMatrix, jointInverseBindPoses[jointNamePositionIndex[animatedJointName]], jointWorldMatrix);

                    // Turn our row major matrix into a column major matrix. OpenGL uses column major
                    mat4Transpose(jointWorldMatrix, jointWorldMatrix);

                    // Trim to 6 significant figures (Maybe even 6 is more than needed?)
                    jointWorldMatrix = jointWorldMatrix.map((val) => parseFloat(val.toFixed(6)));

                    allKeyframes[currentKeyframe][jointNamePositionIndex[animatedJointName]] = jointWorldMatrix;
                }
            }
        }

        return allKeyframes;
    }

    // TODO: Refactor. Depth first traversal might make all of this less hacky
    function getParentWorldMatrix(jointName: string, keyframe: number, jointParents: Util.Map<JointData>, keyframeJointMatrices: KeyframeJointMatricesMap): number[]
    {
        // child -> parent -> parent -> ...
        const jointMatrixTree = foo(jointName, keyframe, jointParents, keyframeJointMatrices);
        // TODO: Revisit this. Thrown in to pass tests. Maybe just return `jointMatrix`
        // when there aren't any parent matrices to factor in
        const worldMatrix = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
        for (const jointMatrix of jointMatrixTree)
        {
            // TODO: Still not sure why we multiply in this order
            mat4Multiply(worldMatrix, worldMatrix, jointMatrix);
        }
        return worldMatrix;
    }

    // TODO: Clean up... well.. at least it works now :sweat_smile:
    function foo(jointName: string, keyframe: number, jointParents: Util.Map<JointData>, keyframeJointMatrices: KeyframeJointMatricesMap): number[][]
    {
        const jointMatrix = keyframeJointMatrices[keyframe][jointName];
        const parentJointName = jointParents[jointName].parent;
        if (parentJointName)
        {
            return [jointMatrix].concat(foo(parentJointName, keyframe, jointParents, keyframeJointMatrices));
        }
        return [jointMatrix];
    }
}