namespace RS.DAE.Libraries
{
    import XMLNode = XML2JSParser.XMLNode;

    // TODO: Don't hard code dimension detection
    const xyzMap = { 0: 'x', 1: 'y', 2: 'z' };
    export function parseLocRotScaleAnim(libraryAnimations: XMLNode[]): KeyframeData
    {
        const allKeyframes: KeyframeData = {};

        for (let i = 0, l = libraryAnimations.length; i < l; ++i)
        {
            const anim = libraryAnimations[i] as XMLNode;
            if (anim.$.id.indexOf('location') !== -1 || anim.$.id.indexOf('rotation') !== -1 || anim.$.id.indexOf('scale') !== -1)
            {
                const animationSource = anim.source as XMLNode[];

                const currentKeyframes = parseFloatArray((animationSource[0].float_array[0] as XMLNode)._);
                const positions = parseFloatArray((animationSource[1].float_array[0] as XMLNode)._);

                // TODO: Interpolation, intangent, outtangent? Or just expect linear?
                // Depends how much of collada spec we want to support

                const xyz = xyzMap[i % 3];

                for (let j = 0, l2 = currentKeyframes.length; j < l2; ++j)
                {
                    let dimension: string;
                    if (i < 3)
                    {
                        dimension = "location";
                    }
                    else if (i < 6)
                    {
                        dimension = "rotation";
                    }
                    else
                    {
                        dimension = "scale";
                    }

                    allKeyframes[currentKeyframes[j]] = allKeyframes[currentKeyframes[j]] || [];
                    allKeyframes[currentKeyframes[j]][dimension] = allKeyframes[currentKeyframes[j]][dimension] || {};
                    allKeyframes[currentKeyframes[j]][dimension][xyz] = positions[j];
                }
            }
        }

        return allKeyframes;
    }
}