namespace RS.DAE.Libraries
{
    import XMLNode = XML2JSParser.XMLNode;

    export class MultipleMeshError extends Error
    {
        public constructor(numMeshes: number)
        {
            super(`It looks like you're trying to parse a model that has ${numMeshes} geometries.
            collada-dae-parser only supports collada files with 1 geometry.
            You might try opening your model in your favorite modeling tool and joining
            your geometries into one.
            Here's some documentation on how to do it in Blender:
            https://github.com/chinedufn/collada-dae-parser/blob/master/docs/blender-export/blender-export.md#multiple-meshes`);
        }
    }

    export interface GeometrySubmesh
    {
        vertexNormalIndices: number[];
        vertexPositionIndices: number[];
        vertexUVIndices?: number[];
        materialID?: string;
    }

    export interface GeometryData
    {
        vertexPositions: number[];
        vertexNormals: number[];
        vertexUVs?: number[];
        subMeshes: GeometrySubmesh[];
    }

    export function parseLibraryGeometries(library_geometries: XMLNode): GeometryData
    {
        // We only support models with 1 geometry. If the model zero or
        // multiple meshes we alert the user
        const geometry = library_geometries.geometry as XMLNode[];
        if (geometry.length !== 1)
        {
            throw new MultipleMeshError(geometry.length)
        }

        const geometryMesh = geometry[0].mesh[0] as XMLNode;

        const sources = geometryMesh.source as XMLNode[];

        const polylists = geometryMesh.polylist as XMLNode[] || [];
        const triangles = geometryMesh.triangles as XMLNode[] || [];
        const vertices = geometryMesh.vertices as XMLNode[];

        const parsedInputs: Util.Map<string> = {};

        const subMeshes: GeometrySubmesh[] = [];
        
        // Parse all polylists
        for (const polylist of polylists)
        {
            const inputs = polylist.input as XMLNode[];
            const vcount = parseFloatArray(polylist.vcount[0] as string);
            const pindices = parseFloatArray(polylist.p[0] as string);

            const indexStride = inputs.map((input) => parseInt(input.$.offset)).reduce((a, b) => Math.max(a, b), 0) + 1;

            const subVertexNormalIndices: number[] = [];
            const subVertexPositionIndices: number[] = [];
            const subVertexUVIndices: number[] = [];

            // Parse indices
            const tmpVertexNormalIndices: number[] = [];
            const tmpVertexPositionIndices: number[] = [];
            const tmpVertexUVIndices: number[] = [];
            let pPtr = 0;
            for (const poly of vcount)
            {
                const vCnt = poly|0;
                tmpVertexNormalIndices.length = 0;
                tmpVertexPositionIndices.length = 0;
                tmpVertexUVIndices.length = 0;

                // Extract all indices for the polygon
                for (let vIndex = 0; vIndex < vCnt; ++vIndex)
                {
                    for (const input of inputs)
                    {
                        const semantic = input.$.semantic;
                        const offset = parseInt(input.$.offset);
                        const existing = parsedInputs[semantic];
                        if (existing == null)
                        {
                            parsedInputs[semantic] = input.$.source;
                        }
                        else if (existing !== input.$.source)
                        {
                            throw new Error(`Mesh contains multiple polylists which refer to different sources, this is not yet supported (for ${semantic}, '${input.$.source}' vs '${existing}')`);
                        }
                        switch (semantic)
                        {
                            case "VERTEX": tmpVertexPositionIndices.push(pindices[pPtr + offset]|0); break;
                            case "NORMAL": tmpVertexNormalIndices.push(pindices[pPtr + offset]|0); break;
                            case "TEXCOORD": tmpVertexUVIndices.push(pindices[pPtr + offset]|0); break;
                        }
                    }
                    pPtr += indexStride;
                }

                // Triangulate
                for (let vIndex = 2; vIndex < vCnt; ++vIndex)
                {
                    if (tmpVertexNormalIndices.length > 0)
                    {
                        subVertexNormalIndices.push(tmpVertexNormalIndices[0]);
                        subVertexNormalIndices.push(tmpVertexNormalIndices[vIndex - 1]);
                        subVertexNormalIndices.push(tmpVertexNormalIndices[vIndex]);
                    }
                    if (tmpVertexPositionIndices.length > 0)
                    {
                        subVertexPositionIndices.push(tmpVertexPositionIndices[0]);
                        subVertexPositionIndices.push(tmpVertexPositionIndices[vIndex - 1]);
                        subVertexPositionIndices.push(tmpVertexPositionIndices[vIndex]);
                    }
                    if (tmpVertexUVIndices.length > 0)
                    {
                        subVertexUVIndices.push(tmpVertexUVIndices[0]);
                        subVertexUVIndices.push(tmpVertexUVIndices[vIndex - 1]);
                        subVertexUVIndices.push(tmpVertexUVIndices[vIndex]);
                    }
                }
            }

            subMeshes.push({
                vertexPositionIndices: subVertexPositionIndices,
                vertexNormalIndices: subVertexNormalIndices,
                vertexUVIndices: subVertexUVIndices,
                materialID: polylist.$.material
            });
        }
        for (const trilist of triangles)
        {
            const inputs = trilist.input as XMLNode[];
            const pindices = (trilist.p[0] as string).trim().split(" ");
            const cnt = parseInt(trilist.$.count);

            const indexStride = inputs.map((input) => parseInt(input.$.offset)).reduce((a, b) => Math.max(a, b), 0) + 1;

            const subVertexNormalIndices: number[] = [];
            const subVertexPositionIndices: number[] = [];
            const subVertexUVIndices: number[] = [];

            // Parse indices
            const tmpVertexNormalIndices: number[] = [];
            const tmpVertexPositionIndices: number[] = [];
            const tmpVertexUVIndices: number[] = [];
            let pPtr = 0;
            for (let polyID = 0; polyID < cnt; ++polyID)
            {
                tmpVertexNormalIndices.length = 0;
                tmpVertexPositionIndices.length = 0;
                tmpVertexUVIndices.length = 0;

                // Extract all indices for the polygon
                for (let vIndex = 0; vIndex < 3; ++vIndex)
                {
                    for (const input of inputs)
                    {
                        const semantic = input.$.semantic;
                        const offset = parseInt(input.$.offset);
                        const existing = parsedInputs[semantic];
                        if (existing == null)
                        {
                            parsedInputs[semantic] = input.$.source;
                        }
                        else if (existing !== input.$.source)
                        {
                            throw new Error(`Mesh contains multiple polylists which refer to different sources, this is not yet supported (for ${semantic}, '${input.$.source}' vs '${existing}')`);
                        }
                        switch (semantic)
                        {
                            case "VERTEX": tmpVertexPositionIndices.push(parseInt(pindices[pPtr + offset])); break;
                            case "NORMAL": tmpVertexNormalIndices.push(parseInt(pindices[pPtr + offset])); break;
                            case "TEXCOORD": tmpVertexUVIndices.push(parseInt(pindices[pPtr + offset])); break;
                        }
                    }
                    pPtr += indexStride;
                }

                // Triangulate
                if (tmpVertexNormalIndices.length > 0)
                {
                    subVertexNormalIndices.push(tmpVertexNormalIndices[0]);
                    subVertexNormalIndices.push(tmpVertexNormalIndices[1]);
                    subVertexNormalIndices.push(tmpVertexNormalIndices[2]);
                }
                if (tmpVertexPositionIndices.length > 0)
                {
                    subVertexPositionIndices.push(tmpVertexPositionIndices[0]);
                    subVertexPositionIndices.push(tmpVertexPositionIndices[1]);
                    subVertexPositionIndices.push(tmpVertexPositionIndices[2]);
                }
                if (tmpVertexUVIndices.length > 0)
                {
                    subVertexUVIndices.push(tmpVertexUVIndices[0]);
                    subVertexUVIndices.push(tmpVertexUVIndices[1]);
                    subVertexUVIndices.push(tmpVertexUVIndices[2]);
                }
            }

            subMeshes.push({
                vertexPositionIndices: subVertexPositionIndices,
                vertexNormalIndices: subVertexNormalIndices,
                vertexUVIndices: subVertexUVIndices,
                materialID: trilist.$.material
            });
        }

        // Parse vertex data
        let vertexPositions: number[] | null = null;
        let vertexNormals: number[] | null = null;
        let vertexUVs: number[] | null = null;

        const vertIDMap: Util.Map<string> = {};
        for (const vertDesc of vertices)
        {
            vertIDMap[vertDesc.$.id] = (vertDesc.input as XMLNode[]).filter((inputNode) => inputNode.$.semantic === "POSITION")[0].$.source;
        }
        for (const semantic in parsedInputs)
        {
            const sourceRaw = parsedInputs[semantic];
            //const semantic = input.$.semantic;
            const sourceID = semantic === "VERTEX" ? vertIDMap[sourceRaw.substr(1)].substr(1) : sourceRaw.substr(1);
            const source = sources.filter((src) => src.$.id === sourceID)[0];
            if (source)
            {
                switch (semantic)
                {
                    case "VERTEX": vertexPositions = parseFloatArray((source.float_array[0] as XMLNode)._); break;
                    case "NORMAL": vertexNormals = parseFloatArray((source.float_array[0] as XMLNode)._); break;
                    case "TEXCOORD": vertexUVs = parseFloatArray((source.float_array[0] as XMLNode)._); break;
                }
            }
        }

        return {
            vertexPositions: vertexPositions,
            vertexNormals: vertexNormals,
            vertexUVs: vertexUVs,
            subMeshes
        };
    }
}