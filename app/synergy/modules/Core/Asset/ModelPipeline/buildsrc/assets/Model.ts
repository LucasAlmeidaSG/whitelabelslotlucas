namespace RS.AssetPipeline
{
    /**
     * Model asset specific definition data.
     */
    export interface ModelDefinition extends BaseVariant
    {
        order?: string;
        reverseFaces?: boolean;
        pivot?: { x: number; y: number; z: number; };
        scale?: { x: number; y: number; z: number; };
        combine?: boolean;
    }

    /**
     * Represents a 3D model asset.
     */
    @AssetType("model")
    @InferFromExtensions([".obj", ".dae"])
    export class Model extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "smf" ]; }

        public get classModule() { return "Core.Asset.ModelPipeline"; }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitAssetReference(this, "RS.Asset.ModelReference");
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: ModelDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            if (format != "smf")
            {
                Log.error(`Unsupported format '${format}'`);
                return null;
            }

            const sanitisedAssetPath = Path.sanitise(assetPath);
            const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.baseName(sanitisedAssetPath)), ".smf");
            if (!dry)
            {
                switch (Path.extension(assetPath))
                {
                    case ".obj":
                    {
                        const writer = await this.processOBJ(assetPath, variant);
                        this.postProcessModel(writer, variant);
                        await FileSystem.writeFile(outputPath, writer.write());
                        break;
                    }
                    case ".dae":
                    {
                        const writer = await this.processDAE(assetPath, variant);
                        this.postProcessModel(writer, variant);
                        await FileSystem.writeFile(outputPath, writer.write());
                        break;
                    }
                    default:
                        Log.error(`Unsupported input model format '${Path.extension(assetPath)}'`);
                        return null;

                }
            }

            // Return variant data
            return {
                paths: [ Path.replaceExtension(sanitisedAssetPath, ".smf") ]
            };
        }

        protected postProcessModel(writer: SMFWriter, variant: ModelDefinition): void
        {
            // Do we need to apply a scale?
            if (variant.scale)
            {
                // Grab vertex positions and normals
                const [ vPosDesc, vPosIdx ] = writer.vertexDescriptors
                    .map<[SMFWriter.VertexDescriptor, number]>((value, index) => [value, index])
                    .filter((vDesc) => vDesc[0].usage === SMFWriter.VertexUsage.Position)[0];
                const posBuffer = writer.vertexBuffers[vPosIdx];
                const vertCount = posBuffer.length / vPosDesc.elementsPerVertex;
                const [ vNormDesc, vNormIdx ] = writer.vertexDescriptors
                    .map<[SMFWriter.VertexDescriptor, number]>((value, index) => [value, index])
                    .filter((vDesc) => vDesc[0].usage === SMFWriter.VertexUsage.Normal)[0];
                const normBuffer = writer.vertexBuffers[vNormIdx];

                // Apply scale
                const { x, y, z } = variant.scale;
                for (let i = 0; i < vertCount; ++i)
                {
                    if (vPosDesc.elementsPerVertex > 0)
                    {
                        posBuffer[i * vPosDesc.elementsPerVertex + 0] *= x;
                    }
                    if (vPosDesc.elementsPerVertex > 1)
                    {
                        posBuffer[i * vPosDesc.elementsPerVertex + 1] *= y;
                    }
                    if (vPosDesc.elementsPerVertex > 2)
                    {
                        posBuffer[i * vPosDesc.elementsPerVertex + 2] *= z;
                    }
                    if (normBuffer)
                    {
                        let sum = 0;
                        if (vNormDesc.elementsPerVertex > 0)
                        {
                            const nX = normBuffer[i * vNormDesc.elementsPerVertex + 0] *= x;
                            sum += nX * nX;
                        }
                        if (vNormDesc.elementsPerVertex > 1)
                        {
                            const nY = normBuffer[i * vNormDesc.elementsPerVertex + 1] *= y;
                            sum += nY * nY;
                        }
                        if (vNormDesc.elementsPerVertex > 2)
                        {
                            const nZ = normBuffer[i * vNormDesc.elementsPerVertex + 2] *= z;
                            sum += nZ * nZ;
                        }
                        const len = Math.sqrt(sum);
                        if (vNormDesc.elementsPerVertex > 0)
                        {
                            normBuffer[i * vNormDesc.elementsPerVertex + 0] /= len;
                        }
                        if (vNormDesc.elementsPerVertex > 1)
                        {
                            normBuffer[i * vNormDesc.elementsPerVertex + 1] /= len;
                        }
                        if (vNormDesc.elementsPerVertex > 2)
                        {
                            normBuffer[i * vNormDesc.elementsPerVertex + 2] /= len;
                        }
                    }
                }
            }

            // Do we need to correct the pivot?
            if (variant.pivot)
            {
                // Grab vertex positions
                const [ vPosDesc, vPosIdx ] = writer.vertexDescriptors
                    .map<[SMFWriter.VertexDescriptor, number]>((value, index) => [value, index])
                    .filter((vDesc) => vDesc[0].usage === SMFWriter.VertexUsage.Position)[0];
                const vBuffer = writer.vertexBuffers[vPosIdx];
                const vertCount = vBuffer.length / vPosDesc.elementsPerVertex;

                // Calculate bounds
                let minX = Infinity, maxX = -Infinity, minY = Infinity, maxY = -Infinity, minZ = Infinity, maxZ = -Infinity;
                for (let i = 0; i < vertCount; ++i)
                {
                    if (vPosDesc.elementsPerVertex > 0)
                    {
                        const x = vBuffer[i * vPosDesc.elementsPerVertex + 0];
                        minX = Math.min(x, minX);
                        maxX = Math.max(x, maxX);
                    }
                    if (vPosDesc.elementsPerVertex > 1)
                    {
                        const y = vBuffer[i * vPosDesc.elementsPerVertex + 1];
                        minY = Math.min(y, minY);
                        maxY = Math.max(y, maxY);
                    }
                    if (vPosDesc.elementsPerVertex > 2)
                    {
                        const z = vBuffer[i * vPosDesc.elementsPerVertex + 2];
                        minZ = Math.min(z, minZ);
                        maxZ = Math.max(z, maxZ);
                    }
                }

                // Compute offset to apply
                const oX = (maxX - minX) * (0.5 - variant.pivot.x) - (minX + maxX) * 0.5;
                const oY = (maxY - minY) * (0.5 - variant.pivot.y) - (minY + maxY) * 0.5;
                const oZ = (maxZ - minZ) * (0.5 - variant.pivot.z) - (minZ + maxZ) * 0.5;

                // Apply offset
                for (let i = 0; i < vertCount; ++i)
                {
                    if (vPosDesc.elementsPerVertex > 0)
                    {
                        vBuffer[i * vPosDesc.elementsPerVertex + 0] += oX;
                    }
                    if (vPosDesc.elementsPerVertex > 1)
                    {
                        vBuffer[i * vPosDesc.elementsPerVertex + 1] += oY;
                    }
                    if (vPosDesc.elementsPerVertex > 2)
                    {
                        vBuffer[i * vPosDesc.elementsPerVertex + 2] += oZ;
                    }
                }
            }
        }

        protected async processOBJ(assetPath: string, variant: ModelDefinition): Promise<SMFWriter>
        {
            // Load obj
            const objRaw = await FileSystem.readFile(assetPath);
            const folderPath = Path.directoryName(assetPath);

            // Parse
            let obj: ConvertedOBJ;
            try
            {
                obj = await parseOBJ(objRaw, variant.order, undefined, folderPath);
            }
            catch (err)
            {
                Log.error(err.stack || err);
                throw err;
            }

            // Construct SMF
            const writer = new SMFWriter();

            // Build vertices
            const vertCount = obj.vertices.length;
            const vertArr = new Float32Array(vertCount * 4);
            for (let i = 0; i < vertCount; ++i)
            {
                const vert = obj.vertices[i];
                vertArr[i * 4 + 0] = vert.x;
                vertArr[i * 4 + 1] = vert.y;
                vertArr[i * 4 + 2] = vert.z;
                vertArr[i * 4 + 3] = 1.0;
            }
            writer.addVertexDescriptor({
                dataType: SMFWriter.DataType.Float,
                usage: SMFWriter.VertexUsage.Position,
                elementsPerVertex: 4,
                normalised: false
            }, vertArr);

            if (obj.normals.length > 0)
            {
                // Build normals
                const normCount = obj.normals.length;
                const normArr = new Float32Array(normCount * 3);
                for (let i = 0; i < normCount; ++i)
                {
                    const norm = obj.normals[i];
                    normArr[i * 3 + 0] = norm.x;
                    normArr[i * 3 + 1] = norm.y;
                    normArr[i * 3 + 2] = norm.z;
                }
                writer.addVertexDescriptor({
                    dataType: SMFWriter.DataType.Float,
                    usage: SMFWriter.VertexUsage.Normal,
                    elementsPerVertex: 3,
                    normalised: false
                }, normArr);
            }

            if (obj.uvs.length > 0)
            {
                // Build uvs
                const uvCount = obj.uvs.length;
                const uvArr = new Float32Array(uvCount * 2);
                for (let i = 0; i < uvCount; ++i)
                {
                    const uv = obj.uvs[i];
                    uvArr[i * 2 + 0] = uv.u;
                    uvArr[i * 2 + 1] = uv.v;
                }
                writer.addVertexDescriptor({
                    dataType: SMFWriter.DataType.Float,
                    usage: SMFWriter.VertexUsage.TexCoord,
                    elementsPerVertex: 2,
                    normalised: false
                }, uvArr);
            }

            const colorCount = obj.vertices.length;
            const colorArr = new Uint8Array(colorCount * 4);
            for (let i = 0; i < colorCount; ++i)
            {
                colorArr[i * 4 + 0] = 255;
                colorArr[i * 4 + 1] = 255;
                colorArr[i * 4 + 2] = 255;
                colorArr[i * 4 + 3] = 255;
            }
            writer.addVertexDescriptor({
                dataType: SMFWriter.DataType.Byte,
                usage: SMFWriter.VertexUsage.Color,
                elementsPerVertex: 4,
                normalised: true
            }, colorArr);

            for (const objName in obj.objects)
            {
                const mesh = obj.objects[objName];
                for (const meshName in mesh.subMeshes)
                {
                    const subMesh = mesh.subMeshes[meshName];
                    // Build elements
                    const elArr = new Uint16Array(subMesh.faces.length * 3);
                    for (let i = 0; i < subMesh.faces.length; ++i)
                    {
                        const face = subMesh.faces[i];
                        if (variant.reverseFaces)
                        {
                            elArr[i * 3 + 0] = face[2];
                            elArr[i * 3 + 1] = face[1];
                            elArr[i * 3 + 2] = face[0];
                        }
                        else
                        {
                            elArr[i * 3 + 0] = face[0];
                            elArr[i * 3 + 1] = face[1];
                            elArr[i * 3 + 2] = face[2];
                        }
                    }
                    writer.addIndexDescriptor({
                        geometryType: SMFWriter.GeometryType.Triangles
                    }, elArr);
                }
            };

            return writer;
        }

        protected async processDAE(assetPath: string, variant: ModelDefinition): Promise<SMFWriter>
        {
            // Load dae
            const daeRaw = await FileSystem.readFile(assetPath);

            // Parse it
            let dae: DAE.ParsedCollada;
            try
            {
                Log.pushContext(this.id);
                dae = DAE.parseCollada(daeRaw);
                if (dae == null) { throw new Error("Failed to parse DAE"); }
            }
            catch (err)
            {
                Log.error(err.stack || err);
                return null;
            }
            finally
            {
                Log.popContext(this.id);
            }
            // await FileSystem.writeFile("testmodel_parsed.json", JSON.stringify(dae));

            // Construct SMF
            const writer = new SMFWriter();

            // Axis order
            const order = variant.order || "xyz";
            const xIdx = order.indexOf("x");
            const yIdx = order.indexOf("y");
            const zIdx = order.indexOf("z");

            // Positions and normals
            const vertCount = dae.subMeshes
                .map((sm) => Math.min(sm.vertexPositionIndices.length, sm.vertexNormalIndices.length))
                .reduce((a, b) => a + b, 0);
            const subMeshCount = dae.subMeshes.length;
            const vertArr = new Float32Array(vertCount * 4);
            const normArr = new Float32Array(vertCount * 3);
            const colArr = new Uint8Array(vertCount * 4);
            let vertexPtr = 0;
            for (let j = 0; j < subMeshCount; ++j)
            {
                const subMesh = dae.subMeshes[j];
                const subMeshVertCount = Math.min(subMesh.vertexPositionIndices.length, subMesh.vertexNormalIndices.length);
                const material = dae.materialData ? dae.materialData.materials[subMesh.materialID || ""] : null;
                for (let i = 0; i < subMeshVertCount; ++i)
                {
                    const vertID = vertexPtr + i;
                    const vertexPositionIndex = subMesh.vertexPositionIndices[i];
                    vertArr[vertID * 4 + 0] = dae.vertexPositions[vertexPositionIndex * 3 + xIdx];
                    vertArr[vertID * 4 + 1] = dae.vertexPositions[vertexPositionIndex * 3 + yIdx];
                    vertArr[vertID * 4 + 2] = dae.vertexPositions[vertexPositionIndex * 3 + zIdx];
                    vertArr[vertID * 4 + 3] = 1.0;

                    const vertexNormalIndex = subMesh.vertexNormalIndices[i];
                    normArr[vertID * 3 + 0] = dae.vertexNormals[vertexNormalIndex * 3 + xIdx];
                    normArr[vertID * 3 + 1] = dae.vertexNormals[vertexNormalIndex * 3 + yIdx];
                    normArr[vertID * 3 + 2] = dae.vertexNormals[vertexNormalIndex * 3 + zIdx];

                    if (material)
                    {
                        colArr[vertID * 4 + 0] = Math.round(material.diffuse[0] * 255);
                        colArr[vertID * 4 + 1] = Math.round(material.diffuse[1] * 255);
                        colArr[vertID * 4 + 2] = Math.round(material.diffuse[2] * 255);
                        colArr[vertID * 4 + 3] = Math.round(material.diffuse[3] * 255);
                    }
                    else
                    {
                        colArr[vertID * 4 + 0] = 255;
                        colArr[vertID * 4 + 1] = 255;
                        colArr[vertID * 4 + 2] = 255;
                        colArr[vertID * 4 + 3] = 255;
                    }
                }
                vertexPtr += subMeshVertCount;
            }
            writer.addVertexDescriptor({
                dataType: SMFWriter.DataType.Float,
                usage: SMFWriter.VertexUsage.Position,
                elementsPerVertex: 4,
                normalised: false
            }, vertArr);
            writer.addVertexDescriptor({
                dataType: SMFWriter.DataType.Float,
                usage: SMFWriter.VertexUsage.Normal,
                elementsPerVertex: 3,
                normalised: false
            }, normArr);
            writer.addVertexDescriptor({
                dataType: SMFWriter.DataType.Byte,
                usage: SMFWriter.VertexUsage.Color,
                elementsPerVertex: 4,
                normalised: true
            }, colArr);

            // Texcoords
            if (dae.subMeshes.some((sm) => sm.vertexUVIndices != null && sm.vertexUVIndices.length > 0))
            {
                const uvCount = dae.subMeshes.map((sm) => sm.vertexUVIndices.length).reduce((a, b) => a + b, 0);
                const uvArr = new Float32Array(uvCount * 2);
                vertexPtr = 0;
                for (let j = 0; j < subMeshCount; ++j)
                {
                    const subMesh = dae.subMeshes[j];
                    const subMeshUVCount = subMesh.vertexUVIndices.length;
                    for (let i = 0; i < subMeshUVCount; ++i)
                    {
                        const vertexUVIndex = subMesh.vertexUVIndices[i];
                        uvArr[(i + vertexPtr) * 2 + 0] = dae.vertexUVs[vertexUVIndex * 2 + 0];
                        uvArr[(i + vertexPtr) * 2 + 1] = dae.vertexUVs[vertexUVIndex * 2 + 1];
                    }
                    vertexPtr += subMeshUVCount;
                }
                writer.addVertexDescriptor({
                    dataType: SMFWriter.DataType.Float,
                    usage: SMFWriter.VertexUsage.TexCoord,
                    elementsPerVertex: 2,
                    normalised: false
                }, uvArr);
            }
            else
            {
                // Log.warn(`Mesh is missing UVs, texture mapping will not work!`, this.id);
                writer.addVertexDescriptor({
                    dataType: SMFWriter.DataType.Float,
                    usage: SMFWriter.VertexUsage.TexCoord,
                    elementsPerVertex: 2,
                    normalised: false
                }, new Float32Array(vertCount * 2));
            }

            // Indices
            if (variant.combine)
            {
                const indArr = new Uint16Array(vertCount);
                vertexPtr = 0;
                for (let j = 0; j < subMeshCount; ++j)
                {
                    const subMesh = dae.subMeshes[j];
                    const subMeshVertCount = Math.min(subMesh.vertexPositionIndices.length, subMesh.vertexNormalIndices.length);
                    const subMeshTriCount = subMeshVertCount / 3;

                    for (let i = 0, l = subMeshTriCount; i < l; ++i)
                    {
                        if (variant.reverseFaces)
                        {
                            indArr[(vertexPtr + i) * 3 + 0] = (vertexPtr + i) * 3 + 2;
                            indArr[(vertexPtr + i) * 3 + 1] = (vertexPtr + i) * 3 + 1;
                            indArr[(vertexPtr + i) * 3 + 2] = (vertexPtr + i) * 3 + 0;
                        }
                        else
                        {
                            indArr[(vertexPtr + i) * 3 + 0] = (vertexPtr + i) * 3 + 0;
                            indArr[(vertexPtr + i) * 3 + 1] = (vertexPtr + i) * 3 + 1;
                            indArr[(vertexPtr + i) * 3 + 2] = (vertexPtr + i) * 3 + 2;
                        }
                    }
                    vertexPtr += subMeshTriCount;
                }
                writer.addIndexDescriptor({
                    geometryType: SMFWriter.GeometryType.Triangles,
                }, indArr);
            }
            else
            {
                vertexPtr = 0;
                for (let j = 0; j < subMeshCount; ++j)
                {
                    const subMesh = dae.subMeshes[j];
                    const subMeshVertCount = Math.min(subMesh.vertexPositionIndices.length, subMesh.vertexNormalIndices.length);
                    const subMeshTriCount = subMeshVertCount / 3;
                    const indArr = new Uint16Array(subMeshVertCount);
                    for (let i = 0, l = subMeshTriCount; i < l; ++i)
                    {
                        if (variant.reverseFaces)
                        {
                            indArr[i * 3 + 0] = (vertexPtr + i) * 3 + 2;
                            indArr[i * 3 + 1] = (vertexPtr + i) * 3 + 1;
                            indArr[i * 3 + 2] = (vertexPtr + i) * 3 + 0;
                        }
                        else
                        {
                            indArr[i * 3 + 0] = (vertexPtr + i) * 3 + 0;
                            indArr[i * 3 + 1] = (vertexPtr + i) * 3 + 1;
                            indArr[i * 3 + 2] = (vertexPtr + i) * 3 + 2;
                        }
                    }
                    writer.addIndexDescriptor({
                        geometryType: SMFWriter.GeometryType.Triangles,
                    }, indArr);
                    vertexPtr += subMeshTriCount;
                }
            }

            return writer;
        }
    }
}
