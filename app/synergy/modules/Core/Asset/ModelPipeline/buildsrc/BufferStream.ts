namespace RS.AssetPipeline
{
    export class BufferStreamWriter
    {
        private _ptr: number;

        /** Gets the current position of the write head. */
        public get currentOffset() { return this._ptr; }

        /** Gets the number of bytes remaining in the buffer. */
        public get remaining() { return this.buffer.byteLength - this._ptr; }

        public constructor(public readonly buffer: Buffer, initialOffset: number = 0)
        {
            this._ptr = initialOffset;
        }

        public advance(bytes: number): this
        {
            this._ptr += bytes;
            return this;
        }

        public writeUInt8(value: number): this
        {
            this.buffer.writeUInt8(value, this._ptr);
            this._ptr += 1;
            return this;
        }

        public writeInt8(value: number): this
        {
            this.buffer.writeInt8(value, this._ptr);
            this._ptr += 1;
            return this;
        }

        public writeUInt16(value: number): this
        {
            this.buffer.writeUInt16LE(value, this._ptr);
            this._ptr += 2;
            return this;
        }

        public writeInt16(value: number): this
        {
            this.buffer.writeInt16LE(value, this._ptr);
            this._ptr += 2;
            return this;
        }

        public writeUInt32(value: number): this
        {
            this.buffer.writeUInt32LE(value, this._ptr);
            this._ptr += 4;
            return this;
        }

        public writeInt32(value: number): this
        {
            this.buffer.writeInt32LE(value, this._ptr);
            this._ptr += 4;
            return this;
        }

        public writeFloat(value: number): this
        {
            this.buffer.writeFloatLE(value, this._ptr);
            this._ptr += 4;
            return this;
        }

        public writeDouble(value: number): this
        {
            this.buffer.writeDoubleLE(value, this._ptr);
            this._ptr += 8;
            return this;
        }

        public writeString(value: string, kind = BufferStream.StringType.Raw): this
        {
            if (kind === BufferStream.StringType.PrependLength)
            {
                this._ptr += 2;
            }
            const bytesSize = this.buffer.write(value, this._ptr);
            if (kind === BufferStream.StringType.PrependLength)
            {
                this.buffer.writeUInt16LE(bytesSize, this._ptr - 2);
                this._ptr += bytesSize;
            }
            else if (kind === BufferStream.StringType.NullTerminated)
            {
                this.buffer.writeUInt8(0, this._ptr + bytesSize);
                this._ptr += bytesSize + 1;
            }
            else
            {
                this._ptr += bytesSize;
            }
            return this;
        }
    }

    export namespace BufferStream
    {
        export enum StringType
        {
            Raw,
            PrependLength,
            NullTerminated
        }
    }
}