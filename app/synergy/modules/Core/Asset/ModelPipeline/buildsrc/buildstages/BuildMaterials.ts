namespace RS.ModelPipeline.BuildStages
{
    interface Material
    {
        /** Ambient color - RGB Array */
        ambient?: number[];
        /** Diffuse color - RGB Array */
        diffuse?: number[];
        /** Specular color - RGB Array */
        specularColor?: number[];
        /** Specular exponent Ns - 0-1000 */
        specularWeight?: number;
        /** Transparency - 0-1: 1 = fully opaque */
        transparency?: number;
        /** Illumination Model - 0-10 */
        illuminationModel?: number;
    }
    type Materials = Util.Map<Material>;

    export class BuildMaterials extends BuildStage.Base
    {
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const genPath = Path.combine(module.path, "src", "generated", "materials");
            await FileSystem.deletePath(genPath);

            const path = Path.combine(module.path, "materials");
            // filter out non .mtl files
            const files = (await FileSystem.readFiles(path)).filter((s) => Path.extension(s) === ".mtl");

            let errorCount = 0;
            if (files.length > 0)
            {
                await FileSystem.createPath(genPath);
                for (const file of files)
                {
                    const segs = Path.split(file);
                    //not using Path.sanitize cause it does lower case too, don't need that
                    const filename = segs[segs.length - 1].split(".")[0].replace(/\s/g, "_");

                    /* tslint:disable:no-string-literal */
                    const ns: string = (module.info["assetpipeline"] && module.info["assetpipeline"]["materialsnamespace"]) || (module.info.primarynamespace && `${module.info.primarynamespace}.Materials`) || "Materials";
                    const refs: string[] = (module.info["assetpipeline"] && module.info["assetpipeline"]["refs"]) || [];
                    /* tslint:enable:no-string-literal */

                    const formatter = new RS.Build.CodeGeneration.Formatter();
                    for (const ref of refs)
                    {
                        formatter.emitLine(`/// <reference path="${ref}" />`);
                    }
                    formatter.emitLine(`namespace ${ns}.${filename}`);
                    formatter.enterScope();

                    const source = await FileSystem.readFile(file);
                    const materials = this.parseMaterials(source);
                    const namesAdded = [];

                    for (const name in materials)
                    {
                        const material = materials[name];
                        //replace .'s with _
                        const matName = Path.sanitise(name).replace(/\./g, "_");
                        if (namesAdded.indexOf(matName) > -1)
                        {
                            // only warn, duplicate material names tend to have the same data
                            Log.warn(`Duplicate material name: ${matName} in ${file}`);
                        }
                        else
                        {
                            namesAdded.push(matName);

                            formatter.emitLine(`export const ${matName}: RS.Rendering.Scene.Material =`);
                            formatter.enterScope();
                            formatter.emitLine(`model: RS.Rendering.Scene.Material.Model.LitSpec,`);
                            if (material.ambient)
                            {
                                formatter.emitLine(`ambientMapFallback: new RS.Util.Color(${material.ambient[0]}, ${material.ambient[1]}, ${material.ambient[2]}),`);
                            }
                            if (material.diffuse)
                            {
                                formatter.emitLine(`diffuseTint: new RS.Util.Color(${material.diffuse[0]}, ${material.diffuse[1]}, ${material.diffuse[2]}),`);
                            }
                            if (material.specularColor)
                            {
                                formatter.emitLine(`specularColorTint: new RS.Util.Color(${material.specularColor[0]}, ${material.specularColor[1]}, ${material.specularColor[2]}),`);
                            }
                            if (material.specularWeight)
                            {
                                formatter.emitLine(`specularPowerMult: ${material.specularWeight},`);
                            }
                            formatter.exitScope();
                        }
                    }
                    formatter.exitScope();
                    await FileSystem.writeFile(Path.combine(genPath, `${filename}.ts`), formatter.toString());
                }
            }
            return { workDone: true, errorCount }
        }

        protected parseMaterials(source: string): Materials
        {
            const out: Materials = {};
            const lines = source.split(/\r?\n/g);
            let currKey: string;
            for (const line of lines)
            {
                // remove tabs just in case cause 3ds Max exports indented materials
                const noTabs = line.replace(/\t/g, "");
                const params = noTabs.split(/ +/g);

                switch (params[0])
                {
                    case "newmtl":
                        currKey = params.slice(1).join(" ");
                        out[currKey] = {} as Material;
                        break;
                    case "Ka":
                        out[currKey].ambient = params.slice(1, 4).map((str) => parseFloat(str));
                        break;
                    case "Kd":
                        out[currKey].diffuse = params.slice(1, 4).map((str) => parseFloat(str));
                        break;
                    case "Ks":
                        out[currKey].specularColor = params.slice(1, 4).map((str) => parseFloat(str));
                        break;
                    case "Ns":
                        out[currKey].specularWeight = parseFloat(params[1]);
                        break;
                    case "d":
                        out[currKey].transparency = parseFloat(params[1]);
                        break;
                    case "Tr":
                        out[currKey].transparency = 1 - parseFloat(params[1]);
                        break;
                    case "illum":
                        out[currKey].illuminationModel = parseInt(params[1]);
                        break;
                }
            }
            return out;
        }
    }

    export const buildMaterials = new BuildMaterials();
    BuildStage.register({}, buildMaterials);
}
