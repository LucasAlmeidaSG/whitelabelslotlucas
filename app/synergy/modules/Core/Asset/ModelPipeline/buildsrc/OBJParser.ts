namespace RS.AssetPipeline
{
    export interface Vertex
    {
        x: number;
        y: number;
        z: number;
    }

    export interface UVs
    {
        u: number;
        v: number;
    }

    export interface SubMesh
    {
        material?: Material;
        faces: number[][];
    }

    export interface Obj
    {
        subMeshes: Util.Map<SubMesh>;
    }

    export interface ConvertedOBJ
    {
        objects: Util.Map<Obj>;
        vertices: Vertex[];
        uvs: UVs[];
        normals: Vertex[];
    }

    export interface Material
    {
        /** Ambient color - RGB Array */
        ambient?: number[];
        /** Diffuse color - RGB Array */
        diffuse?: number[];
        /** Specular color - RGB Array */
        specularColor?: number[];
        /** Specular exponent Ns - 0-1000 */
        specularWeight?: number;
        /** Transparency - 0-1: 1 = fully opaque */
        transparency?: number;
        /** Illumination Mode - 0-10 */
        illuminationModel?: number;
    }

    export type Materials = Util.Map<Material>;

    export async function parseOBJ(content: string, order: string = "xyz", yscale: number = 1, folderPath: string): Promise<ConvertedOBJ>
    {
        const result: ConvertedOBJ =
        {
            objects: {},
            vertices: [],
            uvs: [],
            normals: []
        };

        let lines = content.split(/\r?\n/g);
        let x = order.indexOf("x") + 1;
        let y = order.indexOf("y") + 1;
        let z = order.indexOf("z") + 1;

        const map: Util.Map<number> = {};

        const localVertices: Vertex[] = [];
        const localNormals: Vertex[] = [];
        const localUVs: UVs[] = [];

        let nextVertexID = 0;

        let objName: string = "undefined";
        let matName: string = "undefined";
        let materials: Materials = {};

        for (const line of lines)
        {
            const params = line.split(/ +/g);

            switch (params[0])
            {
                case "mtllib":
                    const filepath = Path.combine(folderPath, params[1]);
                    const material = await FileSystem.readFile(filepath);
                    materials = {
                        ...materials,
                        ...parseMaterial(material)
                    };
                    break;
                case "v":
                    localVertices.push({ x: parseFloat(params[x]), y: yscale * parseFloat(params[y]), z: parseFloat(params[z]) });
                    break;

                case "vt":
                    localUVs.push({ u: parseFloat(params[1]), v: 1.0 - parseFloat(params[2]) });
                    break;

                case "vn":
                    localNormals.push({ x: parseFloat(params[x]), y: yscale * parseFloat(params[y]), z: parseFloat(params[z]) });
                    break;

                case "o":
                    objName = params.slice(1).join(" ");
                    result.objects[objName] = { subMeshes: {} };
                    break;

                case "usemtl":
                    matName = params.slice(1).join(" ");
                    if (!result.objects[objName])
                    {
                        result.objects[objName] = { subMeshes: {} };
                    }
                    if (!result.objects[objName].subMeshes[matName])
                    {
                        result.objects[objName].subMeshes[matName] = { faces: [] };

                    }
                    result.objects[objName].subMeshes[matName].material = materials[matName];
                    break;

                case "f":
                    const face: number[] = [];
                    for (let i = 1, l = params.length; i < l; ++i)
                    {
                        const param = params[i];
                        if (param)
                        {
                            if (map[param])
                            {
                                face.push(map[param]);
                            }
                            else
                            {
                                const vals = param.split("/");
                                const vID = nextVertexID++;
                                map[param] = vID;
                                face.push(vID);
                                // v/vt/vn v//vn v/vt v//
                                if (vals.length === 2)
                                {
                                    result.vertices[vID] = localVertices[parseInt(vals[0]) - 1];
                                    result.uvs[vID] = localUVs[parseInt(vals[1]) - 1];
                                }
                                else
                                {
                                    result.vertices[vID] = localVertices[parseInt(vals[0]) - 1];
                                    if (vals[1].length > 0)
                                    {
                                        result.uvs[vID] = localUVs[parseInt(vals[1]) - 1];
                                    }
                                    else
                                    {
                                        result.uvs[vID] = { u: 0, v: 0 };
                                    }
                                    if (vals[2].length > 0)
                                    {
                                        result.normals[vID] = localNormals[parseInt(vals[2]) - 1];
                                    }
                                    else
                                    {
                                        result.normals[vID] = { x: 0, y: 0, z: 0 };
                                    }
                                }
                            }
                        }
                    }

                    // Triangulate
                    for (let i = 2; i < face.length; ++i)
                    {
                        result.objects[objName].subMeshes[matName].faces.push([ face[0], face[i - 1], face[i] ]);
                    }
                    break;
            }
        }
        return result;
    }

    function parseMaterial(source: string): Materials
    {
        const out: Materials = {};
        const lines = source.split(/\r?\n/g);
        let currKey: string;
        for (const line of lines)
        {
            const params = line.split(/ +/g);

            switch (params[0])
            {
                case "newmtl":
                    currKey = params.slice(1).join(" ");
                    out[currKey] = {} as Material;
                    break;
                case "Ka":
                    out[currKey].ambient = params.slice(1, 4).map((str) => parseFloat(str));
                    break;
                case "Kd":
                    out[currKey].diffuse = params.slice(1, 4).map((str) => parseFloat(str));
                    break;
                case "Ks":
                    out[currKey].specularColor = params.slice(1, 4).map((str) => parseFloat(str));
                    break;
                case "Ns":
                    out[currKey].specularWeight = parseFloat(params[1]);
                    break;
                case "d":
                    out[currKey].transparency = parseFloat(params[1]);
                    break;
                case "Tr":
                    out[currKey].transparency = 1 - parseFloat(params[1]);
                    break;
                case "illum":
                    out[currKey].illuminationModel = parseInt(params[1]);
                    break;
            }
        }
        return out;
    }
}
