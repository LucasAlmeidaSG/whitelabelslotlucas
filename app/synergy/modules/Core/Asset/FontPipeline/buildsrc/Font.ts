namespace RS.AssetPipeline
{
    /**
     * Font asset specific definition data.
     */
    export interface FontDefinition extends BaseVariant
    {
        /** If true, forces the use of the OS2 table instead of the standard OpenType HHEA table for metrics. */
        forceOS2?: boolean;
    }

    /**
     * Represents an image-type asset.
     */
    @AssetType("font")
    @InferFromExtensions([".woff", ".woff2", ".otf", ".ttf"])
    export class Font extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "" ]; }

        public get classModule() { return "Core.Asset.FontPipeline"; }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitSimpleAssetReference(this, "RS.Asset.FontReference");
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            if (format != "")
            {
                Log.error(`Unsupported format '${format}'`);
                return null;
            }
            if (!dry)
            {
                const outputPath = Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath)));
                await FileSystem.copyFile(assetPath, outputPath);
            }

            // Return variant data
            return {
                paths: [ Path.sanitise(assetPath) ]
            };
        }
    }
}