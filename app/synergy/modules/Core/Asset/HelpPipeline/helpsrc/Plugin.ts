namespace RS.ExternalHelp
{
    export type PluginConstructor = RS.Constructor1A<Plugin, IPage>;

    export class PluginRegistry
    {
        private readonly _plugins: PluginConstructor[] = [];

        public get plugins(): ReadonlyArray<PluginConstructor> { return this._plugins; }

        public register(pluginClass: PluginConstructor): void
        {
            if (this._plugins.indexOf(pluginClass) > -1) { return; }
            this._plugins.push(pluginClass);
        }
    }

    export class PluginManager
    {
        private readonly _plugins: Plugin[];

        constructor(registry: PluginRegistry, page: IPage)
        {
            this._plugins = registry.plugins.map((pluginClass) => new pluginClass(page));
        }

        public get(pluginClass: PluginConstructor): Plugin | null
        {
            return Find.matching(this._plugins, (plugin) => plugin instanceof pluginClass);
        }
    }

    export abstract class Plugin
    {
        public constructor(page: IPage)
        {
            page.onLoaded(() => this.onPageLoaded(page));
        }

        public abstract onPageLoaded(page: IPage): void;
    }

    export abstract class VisitorPlugin extends Plugin
    {
        public constructor(protected readonly page: IPage)
        {
            super(page);
            page.onElementLoaded((element) => this.onElementLoaded(element));
        }

        public onPageLoaded(page: IPage): void { /* Override me if you want */ }

        public abstract onElementLoaded(element: HTMLElement): void;
    }
}