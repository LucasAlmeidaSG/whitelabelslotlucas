namespace RS.ExternalHelp
{
    /** Iterates over each ancestor of the given element. */
    export function forEachAncestor(element: HTMLElement, callbackfn: (ancestor: HTMLElement) => boolean): void;
    export function forEachAncestor(element: HTMLElement, callbackfn: (ancestor: HTMLElement) => void): void;
    export function forEachAncestor(element: HTMLElement, callbackfn: (ancestor: HTMLElement) => (void | boolean)): void
    {
        for (let el = element.parentElement; el != null; el = el.parentElement)
        {
            const result = callbackfn(el);
            if (Is.boolean(result) && !result) { break; }
        }
    }

    /** Searches for an ancestor of the given element matching the given predicate. */
    export function findAncestor(element: HTMLElement, predicate: (ancestor: HTMLElement) => boolean): HTMLElement | null
    {
        for (let el = element.parentElement; el != null; el = el.parentElement)
        {
            if (predicate(el)) { return el; }
        }
        return null;
    }

    export function hideElement(element: HTMLElement)
    {
        element.style.display = "none";
    }

    export function hideLocalisation(element: HTMLElement)
    {
        let text = element;
        if (!text.classList.contains("rs-locale-text"))
        {
            text = findAncestor(element, (ancestor) => ancestor.classList.contains("rs-locale-text"));
        }

        if (text)
        {
            hideElement(text);
            // Hide all parent elements that only contain this element.
            forEachAncestor(text, (ancestor) =>
            {
                if (ancestor.children.length > 1) { return false; }
                hideElement(ancestor);
                return true;
            });
        }
    }
}
