/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class MaxWinPlugin extends VisitorPlugin
    {
        private readonly _maxWinString: string | null;

        public constructor(page: IPage)
        {
            super(page);
            this._maxWinString = URL.getParameter("maxWin");
        }

        public onElementLoaded(element: HTMLElement)
        {
            const isMaxWin = element.classList.contains("rs-variable-maxwin");
            const needsMaxWin = isMaxWin || element.classList.contains("rs-needs-maxwin");
            const needsNoMaxWin = element.classList.contains("rs-needs-nomaxwin");

            if (this._maxWinString && needsNoMaxWin)
            {
                element.style.display = "none";
                hideLocalisation(element);
            }

            if (!isMaxWin && !needsMaxWin) { return; }

            if (this._maxWinString)
            {
                if (isMaxWin)
                {
                    element.innerHTML = this._maxWinString;
                }
            }
            else
            {
                // Hide element, and if we're in a localisation, hide that too.
                element.style.display = "none";
                if (isMaxWin) { hideLocalisation(element); }
            }
        }
    }

    pluginRegistry.register(MaxWinPlugin);
}