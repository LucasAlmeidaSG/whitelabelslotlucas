/// <reference path="../Init.ts"/>
namespace RS.ExternalHelp
{
    class DocDownload extends Plugin
    {
        protected static ButtonID = "docGenerator";
        protected static DivID = "documentGenerator";
        protected _locale: string;

        public onPageLoaded(page: IPage): void
        {
            this._locale = page.localeCode;
            const showButton = URL.getParameterAsBool("generateDoc", false);
            if (document.getElementById(DocDownload.ButtonID))
            {
                this.showButton(showButton);
                document.getElementById(DocDownload.ButtonID).onclick = this.generateDoc.bind(this);
            }
        }

        protected generateDoc()
        {
            this.showButton(false);
            // convert images first
            this.convertImages();

            // create document
            const header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
                    "xmlns:w='urn:schemas-microsoft-com:office:word' " +
                    "xmlns='http://www.w3.org/TR/REC-html40'>" +
                    "<head><meta charset='utf-8'><title>" +
                    document.title +
                    "</title></head><body>";
            const footer = "</body></html>";
            const sourceHTML = header + document.getElementById("wrapper").innerHTML + footer;

            const source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
            // download document
            const fileDownload = document.createElement("a");
            document.body.appendChild(fileDownload);
            fileDownload.href = source;
            fileDownload.download = this._locale + '.doc';
            fileDownload.click();
            document.body.removeChild(fileDownload);

            this.showButton(true);
        }

        protected convertImages()
        {
            const regularImages = document.querySelectorAll("img");
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            regularImages.forEach((imgElement) =>
            {
                // preparing canvas for drawing
                canvas.width = imgElement.width;
                canvas.height = imgElement.height;
                ctx.clearRect(0, 0, canvas.width, canvas.height);

                ctx.drawImage(imgElement, 0, 0, imgElement.width, imgElement.height);
                // by default toDataURL() produces png image, but you can also export to jpeg
                const dataURL = canvas.toDataURL();
                imgElement.setAttribute('src', dataURL);
            });
            canvas.remove();
        }

        protected showButton(visible: boolean)
        {
            document.getElementById(DocDownload.DivID).style.display = visible ? "" : "none";
        }
    }

    pluginRegistry.register(DocDownload);
}