/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    const defaultYear: string = "2019";

    class CopyrightPlugin extends VisitorPlugin
    {
        private readonly _showSecondary: boolean;
        private readonly _year: string;

        public constructor(page: IPage)
        {
            super(page);

            this._showSecondary = page.languageCode !== "en";
            this._year = URL.getParameter("cryear", defaultYear);
        }

        public onPageLoaded(page: IPage)
        {
            if (!this._showSecondary)
            {
                const secondary = document.getElementById("rs-copyright-en");
                if (secondary)
                {
                    secondary.style.display = "none";
                    hideLocalisation(secondary);
                }
            }
        }

        public onElementLoaded(element: HTMLElement)
        {
            const isYear = element.classList.contains("rs-variable-cryear");
            if (!isYear) { return; }
            element.innerHTML = `${this._year}`;
        }
    }

    pluginRegistry.register(CopyrightPlugin);
}