/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class NTPlugin extends VisitorPlugin
    {
        private readonly _isNT: boolean;

        public constructor(page: IPage)
        {
            super(page);
            this._isNT = URL.getParameterAsBool("isNT", false);
        }

        public onElementLoaded(element: HTMLElement)
        {
            const ntOnly = element.classList.contains("rs-ntonly");
            // hide if NT.
            if (ntOnly && !this._isNT)
            {
                hideElement(element);
            }

            const notNT = element.classList.contains("rs-notnt");
            // hide if not NT.
            if (notNT && this._isNT)
            {
                hideElement(element);
            }
        }
    }

    pluginRegistry.register(NTPlugin);
}
