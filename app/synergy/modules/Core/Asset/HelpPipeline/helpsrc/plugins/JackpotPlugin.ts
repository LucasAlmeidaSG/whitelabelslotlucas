/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class JackpotPlugin extends VisitorPlugin
    {
        private readonly _hasJackpots: boolean;

        public constructor(page: IPage)
        {
            super(page);
            this._hasJackpots = URL.getParameterAsBool("hasJackpots", false);
        }

        public onElementLoaded(element: HTMLElement)
        {
            const notJackpots = element.classList.contains("rs-nojackpot");
            // hide if not jackpots.
            if (notJackpots && this._hasJackpots)
            {
                hideElement(element);
            }

            const jackpotsOnly = element.classList.contains("rs-jackpotonly");
            // hide if jackpots.
            if (jackpotsOnly && !this._hasJackpots)
            {
                hideElement(element);
            }
        }
    }

    pluginRegistry.register(JackpotPlugin);
}
