/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class ClientVersionPlugin extends VisitorPlugin
    {
        private readonly _versionString: string | null;

        public constructor(page: IPage)
        {
            super(page);
            this._versionString = Version.toString(page.version);
        }

        public onElementLoaded(element: HTMLElement)
        {
            const isMaxWin = element.classList.contains("rs-variable-clientversion");
            if (!isMaxWin) { return; }

            element.innerHTML = this._versionString;
        }
    }

    pluginRegistry.register(ClientVersionPlugin);
}