/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class RTPPlugin extends VisitorPlugin
    {
        private readonly _defaultRTP: number;
        private readonly _rtpTable: { [name: string]: number };

        public constructor(page: IPage)
        {
            super(page);

            const rawTable = URL.getParameterAsObject("RTPs");
            if (rawTable)
            {
                this._rtpTable = {};
                for (const rtpName in rawTable)
                {
                    const rtpValue = rawTable[rtpName];
                    if (rtpValue != null && !Is.number(rtpValue) && !Is.string(rtpValue))
                    {
                        this._rtpTable[rtpName] = 101;
                    }
                    else
                    {
                        this._rtpTable[rtpName] = rtpValue;
                    }
                }
            }

            this._defaultRTP = URL.getParameterAsNumber("RTP", -1);
        }

        public onElementLoaded(element: HTMLElement)
        {
            const isRTP = element.classList.contains("rs-variable-rtp");
            if (!isRTP) { return; }

            const rtpName = element.getAttribute("data-rtp-name");
            const rtp = rtpName == null ? this._defaultRTP : this.getNamedRTP(rtpName);
            if (rtp == null)
            {
                const nameString = rtpName == null ? "" : ` ${rtpName}`;
                Log.warn(`RTP${nameString} not found`);
                hideLocalisation(element);
            }
            element.innerHTML = this.rtpToString(rtp);
        }

        private rtpToString(value: number | string | null): string
        {
            if (Is.string(value)) { return value; }
            if (value === -1 || value == null) { return "N/A"; }
            if (value < 0 || value > 100) { return `&lt;RTP INVALID&gt;`; }
            return value.toFixed(2);
        }

        private getNamedRTP(name: string): number | null
        {
            // Show default RTP if RTP table not found
            if (!this._rtpTable)
            {
                if (name === "single") { return this._defaultRTP; }
                return null;
            }
            if (name in this._rtpTable)
            {
                // RTP with name present but undefined
                if (this._rtpTable[name] == null) { return -1; }
                // RTP with name present
                return this._rtpTable[name];
            }
            // RTP with name actually not present
            return null;
        }
    }

    pluginRegistry.register(RTPPlugin);
}