/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class URLPlugin extends VisitorPlugin
    {
        public onElementLoaded(element: HTMLElement)
        {
            const paramName = element.getAttribute("data-url-param");
            if (paramName == null) { return; }

            const paramData = URL.getParameter(paramName, "N/A");
            element.innerHTML = paramData;
        }
    }

    pluginRegistry.register(URLPlugin);
}