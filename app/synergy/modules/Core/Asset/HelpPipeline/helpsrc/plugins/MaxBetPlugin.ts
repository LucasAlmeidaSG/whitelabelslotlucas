/// <reference path="../Init.ts"/>

namespace RS.ExternalHelp
{
    class MaxBetPlugin extends VisitorPlugin
    {
        private readonly _maxBetString: string | null;

        public constructor(page: IPage)
        {
            super(page);
            this._maxBetString = URL.getParameter("maxBet");
        }

        public onElementLoaded(element: HTMLElement)
        {
            const isMaxBet = element.classList.contains("rs-variable-maxbet");
            if (!isMaxBet) { return; }

            if (this._maxBetString && isMaxBet)
            {
                element.innerHTML = this._maxBetString;
            }
            else
            {
                // Hide element, and if we're in a localisation, hide that too.
                element.style.display = "none";
                if (isMaxBet) { hideLocalisation(element); }
            }
        }
    }

    pluginRegistry.register(MaxBetPlugin);
}