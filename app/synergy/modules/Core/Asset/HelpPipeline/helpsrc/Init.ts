/// <reference path="Page.ts" />
/// <reference path="Plugin.ts" />

namespace RS.ExternalHelp
{
    const versionFromBuild = "{!GAME_VERSION!}";

    /** Tracks the plugins to be instantiated. */
    export const pluginRegistry = new PluginRegistry();
    // Hi
    /** Manages instantiated plugins. */
    export let pluginManager: PluginManager;

    const funcName = "initHelp";
    export function init(localeCode: string): void
    {
        const version = Version.fromString(versionFromBuild);
        const page: IPage = new Page(localeCode, version);
        pluginManager = new PluginManager(pluginRegistry, page);
        RS.Log.info(`External help libraries loaded (v${Version.toString(version)}).`);
        delete window[funcName];
    }
    window[funcName] = init;
}
