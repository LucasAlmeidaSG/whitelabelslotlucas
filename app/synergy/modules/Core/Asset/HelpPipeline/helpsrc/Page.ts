namespace RS.ExternalHelp
{
    @HasCallbacks
    export class Page implements IPage
    {
        public readonly onLoaded = createSimpleEvent();
        public readonly onElementLoaded = createEvent<HTMLElement>(true);

        public get languageCode() { return this.localeCode.split("_")[0]; }
        public get countryCode() { return this.localeCode.split("_")[1]; }

        constructor(public readonly localeCode: string, public readonly version: Version)
        {
            window.addEventListener("load", this.onLoad);
        }

        @Callback
        private onLoad()
        {
            window.removeEventListener("load", this.onLoad);
            if (this.onElementLoaded.listenerCount.value > 0) { this.publishLoadedEvent(document.body); }
            this.onLoaded.publish();
        }

        private publishLoadedEvent(element: HTMLElement): void
        {
            const children = element.children;
            for (let i = 0; i < children.length; i++)
            {
                const child = children[i];
                if (!(child instanceof HTMLElement)) { continue; }
                this.publishLoadedEvent(child);
            }

            this.onElementLoaded.publish(element);
        }
    }

    export interface IPage
    {
        readonly onLoaded: RS.IEvent;
        readonly onElementLoaded: RS.IEvent<HTMLElement>;

        readonly localeCode: string;
        readonly countryCode: string;
        readonly languageCode: string;

        readonly version: Version;
    }
}
