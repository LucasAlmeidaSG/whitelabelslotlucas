namespace RS.Build.Scaffolds
{
    const externalHelpTemplate = `<head>
    <title>My Game</title>
</head>

<body>
    <div id="wrapper">
        <div id="title">
            <h1 class="header">My Game</h1>
        </div>

        <div id="turbo"></div>

        <div id="legal"></div>
    </div>
</body>`;

    const externalHelpExample = `<head>
    <title>My Game</title>
    <link rel="stylesheet" type="text/css" href="example.css" />
</head>

<body>
    <div id="wrapper">
        <div id="title">
            <h1 class="header">My Game</h1>
        </div>

        <span>This is a variable: { MaxWin }</span><br/>
        <span>This is a function: { RTP(name="MyRTP") }</span><br/>
        <span>This is a translation: {{ MyGame.Translations.Example, MyStringSub = "This a string substitution", MyVarSub = RTP, MyFuncSub = RTP(name="MyRTP") }}</span><br/>

        <div id="legal">
            <ul>
                <li id="mygame-mylegal">This is an additional legal line: {{ MyGame.Translations.Legal.Example }}</li>
                <li id="rs-copyright" meta-overwrite>Only this text will appear.</li>
                <li id="rs-rtp-multi" meta-remove>This element won't appear in the result.</li>
            </ul>
        </div>
    </div>
</body>`;

    const externalHelpFormat = `/** Fonts */
@font-face {
    font-family: 'Neue Frutiger World';
    src: url(../assets/shared/fonts/frutigerworld/neuefrutigerworld-regular.ttf);
}

/* 
    Allows for the background to cover the whole of the text area and be scrollable
    Portrait mode also takes up a percentage of the screen to stop the text area becoming too small
*/
@media screen and (orientation: portrait)
{
    #wrapper
    {
        position: absolute;
        margin-top: 0;
        left: 0;
        right: 0;
        width: 70%;
        min-height: 100%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 40px;
        margin-bottom: 40px;
    }
}

@media screen and (orientation: landscape)
{
    #wrapper
    {
        position: absolute;
        margin-top: 0;
        left: 0;
        right: 0;
        width: 500px;
        min-height: 100%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 40px;
        margin-bottom: 40px;
    }
}

/* Styles the background so that it doesn't scroll and takes up the whole screen */
html, body
{
    min-width: 100%;
    min-height: 100%;
    width: 100%;
    height: auto;
    position: absolute;
    margin: 0;
    top: 0;
    left: 0;
    overflow: auto;
    background-size: 100%;
    background-repeat: no-repeat;
    background-position: bottom;
    background-size: cover;
}


.formattitle
{
    text-align: left;
}

/* Styles the main body of the text to be centred */
.formath2, .formath3
{
    text-align: center;
}

.formatul
{
    list-style-type: none;
    padding-bottom: 50px;
}

/* Styles text to have space in between each sentence and aligned to the left */
.formatli {
    padding-bottom: 6px;
    text-align: left;
}`;

    export const externalHelp = new Scaffold();
    externalHelp.addTemplate(externalHelpTemplate, "help/template.html");
    externalHelp.addTemplate(externalHelpExample, "help/example.html");
    externalHelp.addTemplate(externalHelpFormat, "help/example.css");
    registerScaffold("externalhelp", externalHelp);
}