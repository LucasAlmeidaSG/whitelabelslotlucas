namespace RS.ExternalHelp.Templating
{
    const variables: Util.Map<string> = {};

    export function registerVariable(name: string, value: string): void
    {
        if (name in variables) { Log.warn(`Variable ${name} is already defined`); }
        variables[name] = value;
    }

    export function unregisterVariable(name: string): void
    {
        delete variables[name];
    }

    /** @internal */
    export function resolveVariable(name: string): string | null
    {
        if (!(name in variables)) { throw new Error(`Variable ${name} is not defined`); }
        return variables[name];
    }

    export class VariableToken extends Compilation.Token
    {
        constructor(public readonly name: string) { super(); }
    }

    export const variableExpector: Compilation.IExpector = (stream) =>
    {
        if (!Compilation.expectString(stream, "{")) { return null; }
        Compilation.consumeWhitespace(stream);

        const token = expectVariable(stream);
        if (!token) { return null; }

        Compilation.consumeWhitespace(stream);
        if (!Compilation.expectString(stream, "}")) { throw new SyntaxError("missing } after variable"); }

        return token;
    };

    export const variableParser: Compilation.IParser = (stream) =>
    {
        const token = Compilation.expectToken(stream, VariableToken);
        if (!token) { return null; }
        return resolveVariable(token.name);
    };

    export function expectVariable(stream: Compilation.CharacterStream): VariableToken | null
    {
        const name = Compilation.expectChars(stream, /[A-Za-z_]/);
        if (name == null) { return null; }
        return new VariableToken(name);
    }
}