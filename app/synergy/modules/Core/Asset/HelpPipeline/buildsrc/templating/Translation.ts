namespace RS.ExternalHelp.Templating
{
    class TranslationToken extends Compilation.Token
    {
        constructor(public readonly isTitle: boolean, public readonly key: string, public readonly substitutions?: Arguments)
        {
            super();
        }
    }

    const keyPattern = /[a-zA-Z0-9_\.]/;
    const keyArgSep = /\,/;

    export const translationExpector: Compilation.IExpector = (stream: Compilation.CharacterStream) =>
    {
        const isTitle = Compilation.expectOptionalString(stream, "<title>") != null;
        if (!Compilation.expectString(stream, "{{")) { return null; }
        Compilation.consumeWhitespace(stream);

        const key: string = Compilation.expectChars(stream, keyPattern);
        if (key == null) { throw new SyntaxError("missing translation key"); }

        let substitutions: Arguments;
        if (Compilation.expectOptionalChar(stream, keyArgSep))
        {
            substitutions = expectArguments(stream);
        }

        Compilation.consumeWhitespace(stream);
        if (!Compilation.expectString(stream, "}}")) { throw new SyntaxError("missing }} after translation"); }
        if (isTitle)
        {
            Compilation.consumeWhitespace(stream);
            Compilation.expectString(stream, "</title>");
        }

        return new TranslationToken(isTitle, key, substitutions);
    };

    function wrapLocalisation(title: boolean, spanclass: string, key: string, value: string)
    {
        return title ?
            `<title data-key="${key}">${value}</title>` : // consumed the title tag in token parsing so we put it back here.
            `<span class="${spanclass}" data-key="${key}">${value}</span>`;
    }

    /**
     * Creates a translation token parser.
     * @param locale Map of valid localised strings.
     * @param fatal Whether or not missing localisations should throw an error.
     */
    export const translationParser = (locale: Util.Map<string>, fatal: boolean): Compilation.IParser =>
        (stream) =>
        {
            const token = Compilation.expectToken(stream, TranslationToken);
            if (!token) { return null; }

            const localeKey = token.key;
            let text = locale[localeKey];
            if (localeKey in locale)
            {
                if (text != null)
                {
                    text = HTML.escapeSpecialChars(text);
                }
            }
            else if (fatal)
            {
                throw new Error(`Key '${localeKey}' not found in locale`);
            }

            if (text == null)
            {
                text = `{{${HTML.SpecialChars.NonBreakingSpace}${localeKey}${HTML.SpecialChars.NonBreakingSpace}}}`;
            }

            if (token.substitutions)
            {
                const resolvedArgs: Util.Map<string> = resolveArguments(token.substitutions);

                const substitutions: Util.Map<string> = {};
                for (const key in resolvedArgs)
                {
                    const value = resolvedArgs[key];
                    const wrappedKey = `{${key}}`;
                    substitutions[wrappedKey] = wrapLocalisation(token.isTitle, "rs-locale-substitution", key, value);
                }

                const keys = Object.keys(substitutions);
                if (keys.length > 0)
                {
                    const regExp = new RegExp(`([\\\s]*)(${keys.join("|")})([\\\s]*)`, "g");
                    text = text.replace(regExp, (match: string, preSpacing: string, key: string, postSpacing: string) =>
                    {
                        const prefix = preSpacing.length > 0 ? HTML.SpecialChars.Space : "";
                        const suffix = postSpacing.length > 0 ? HTML.SpecialChars.Space : "";
                        return prefix + substitutions[key] + suffix;
                    });
                }
            }

            return wrapLocalisation(token.isTitle, "rs-locale-text", localeKey, text);
        };
}
