/* tslint:disable:ban-types */
namespace RS.ExternalHelp.Templating
{
    export type Function = (...args: string[]) => string;

    interface FunctionEntry
    {
        func: Function;
        args: string[];
    }

    const functions: Util.Map<FunctionEntry> = {};

    export function registerFunction(name: string, func: Function, args: string[]): void
    {
        functions[name] = { func, args };
    }

    /** @internal */
    export function resolveFunction(name: string): Function | null;
    export function resolveFunction(name: string, args: Arguments): string | null;
    export function resolveFunction(name: string, args: string[]): string | null;
    export function resolveFunction(name: string, args?: string[] | Util.Map<string | RuntimeArgument>): Function | string | null
    {
        if (!functions.hasOwnProperty(name)) { throw new Error(`Function ${name} is not defined`); }
        const entry = functions[name];
        const fn = entry.func;

        if (Is.object(args))
        {
            const fnArgs = entry.args;
            const orderedArgs: string[] = [];
            for (const key in args)
            {
                const index = fnArgs.indexOf(key);
                if (index === -1) { return null; }
                orderedArgs[index] = args[key];
            }

            args = orderedArgs;
        }

        return args ? fn(...args) : fn;
    }

    export class FunctionToken extends Compilation.Token
    {
        constructor(public readonly name: string, public readonly args: Util.Map<string | RuntimeArgument>) { super(); }
    }

    export function expectFunction(stream: Compilation.CharacterStream)
    {
        const name = Compilation.expectChars(stream, /[A-Za-z_]/);
        if (name == null) { return null; }

        if (!Compilation.expectString(stream, "(")) { return null; }
        // We are now definitely in a function; we'll throw syntax errors.

        const args = expectArguments(stream);

        if (!Compilation.expectString(stream, ")")) { throw new SyntaxError("missing ) after arguments list"); }
        return new FunctionToken(name, args);
    }

    export const functionExpector: Compilation.IExpector = (stream) =>
    {
        if (!Compilation.expectString(stream, "{")) { return null; }
        Compilation.consumeWhitespace(stream);

        const token = expectFunction(stream);
        if (!token) { return null; }

        Compilation.consumeWhitespace(stream);
        if (!Compilation.expectString(stream, "}")) { throw new SyntaxError("missing } after variable"); }

        return token;
    };

    export const functionParser: Compilation.IParser = (stream) =>
    {
        const token = Compilation.expectToken(stream, FunctionToken);
        if (!token) { return null; }
        return resolveFunction(token.name, token.args);
    };

    // Test function
    registerFunction("Concat", (a: string, b: string) =>
    {
        return a + b;
    }, ["a", "b"]);
}