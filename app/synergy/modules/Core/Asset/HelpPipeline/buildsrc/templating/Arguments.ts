namespace RS.ExternalHelp.Templating
{
    export type Argument = RuntimeArgument | string;
    export type Arguments = Util.Map<Argument>;
    export type ResolvedArguments = Util.Map<string>;

    export class RuntimeArgument
    {
        constructor(public readonly token: VariableToken | FunctionToken) {}
    }

    namespace Patterns
    {
        export const Separator = /\,/;
        export const Identifier = /[^\s=}]/;
        export const Quote = /[\"]/;
        export const Text = /[^"]/;
    }

    export function resolveArguments(args: Arguments): ResolvedArguments
    {
        const resolved: ResolvedArguments = {};
        for (const key in args)
        {
            const value = args[key];
            if (value instanceof RuntimeArgument)
            {
                const token = value.token;
                if (token instanceof VariableToken)
                {
                    resolved[key] = resolveVariable(token.name);
                }
                else
                {
                    resolved[key] = resolveFunction(token.name, token.args)
                }
            }
            else
            {
                resolved[key] = value;
            }
        }
        return resolved;
    }

    /** Expects an arguments section (any amount of whitespace, followed by comma-separated key="value" pairs, followed by any amount of whitespace). */
    export function expectArguments(stream: Compilation.CharacterStream): Arguments
    {
        const args: Arguments = {};

        do
        {
            Compilation.consumeWhitespace(stream);

            const name = Compilation.expectOptionalChars(stream, Patterns.Identifier);
            if (!name) { break; }

            const value = expectArgumentValue(stream)
            args[name] = value;

            Compilation.consumeWhitespace(stream);
        }
        while (Compilation.expectOptionalChar(stream, Patterns.Separator));

        return args;
    }

    function expectArgumentValue(stream: Compilation.CharacterStream): Argument
    {
        Compilation.consumeWhitespace(stream);
        if (!Compilation.expectChar(stream, /=/)) { throw new SyntaxError("missing = in argument"); }
        Compilation.consumeWhitespace(stream);

        let value: Argument;

        const quote = Compilation.expectOptionalChar(stream, Patterns.Quote);
        if (quote)
        {
            value = Compilation.expectChars(stream, Patterns.Text);
            if (!Compilation.expectString(stream, quote)) { throw new SyntaxError(`missing ${quote} after argument value`); }
        }
        else
        {
            const fork = stream.fork();
            let variable: VariableToken | FunctionToken = expectFunction(fork);
            if (variable)
            {
                stream.join(fork);
            }
            else
            {
                variable = expectVariable(stream);
            }

            if (variable == null) { throw new SyntaxError("missing argument value"); }
            value = new RuntimeArgument(variable);
        }

        return value;
    }
}