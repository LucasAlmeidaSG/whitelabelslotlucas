namespace RS.ExternalHelp
{
    export interface IModule extends RS.Localisation.IModule
    {
        info: IModule.Info;
    }

    export namespace IModule
    {
        export interface Info extends RS.Localisation.IModuleInfo
        {
            helpdependencies?: string[];
        }
    }

    export function validateInfo(info: IModule.Info)
    {
        if (info.helpdependencies != null)
        {
            if (!Is.arrayOf(info.helpdependencies, Is.string)) { return "help dependencies are invalid"; }
            return null;
        }
        return null;
    }
}