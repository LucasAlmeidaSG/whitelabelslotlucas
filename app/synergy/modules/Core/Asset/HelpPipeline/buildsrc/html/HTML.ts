namespace RS.ExternalHelp.HTML
{
    export class TagToken extends Compilation.Token { constructor(public readonly name: string, public attributes?: Util.Map<string>) { super(); } }
    export class TagOpenToken extends TagToken { }
    export class TagCloseToken extends TagToken { constructor(name: string) { super(name); } }
    export class SingletonToken extends TagToken { constructor(name: string, attributes: Util.Map<string>) { super(name, attributes); } }
    export class DeclarationToken extends TagToken { constructor(name: string, attributes: Util.Map<string>) { super(name, attributes); } }

    namespace CharacterSets
    {
        export const tagNameStartChar = "[!a-zA-Z0-9_:]";
        export const tagNameChar = `${tagNameStartChar}|[-.]`;

        export const attrNameStartChar = "[a-zA-Z]";
        export const attrNameChar = "[a-zA-Z0-9-_.]";
    }

    namespace RegularExpressions
    {
        export const tagNameStartChar = new RegExp(CharacterSets.tagNameStartChar);
        export const tagNameChar = new RegExp(CharacterSets.tagNameChar);

        export const attrNameStartChar = new RegExp(CharacterSets.attrNameStartChar);
        export const attrNameChar = new RegExp(CharacterSets.attrNameChar);
    }

    export const tagExpector: Compilation.IExpector = (stream) =>
    {
        if (!Compilation.expectChar(stream, /</)) { return null; }
        const closingTag = Compilation.expectOptionalChar(stream, /\//) != null;
        const markupDeclaration = !closingTag && Compilation.expectOptionalChar(stream, /!/) != null;

        let name: string = Compilation.expectChar(stream, RegularExpressions.tagNameStartChar);
        if (name == null) { return null; }

        let char: string;
        do
        {
            char = Compilation.expectOptionalChar(stream, RegularExpressions.tagNameChar);
            if (char == null) { break; }
            name += char;
        }
        while (char);

        Compilation.consumeWhitespace(stream);

        const attrs: Util.Map<string> = {};
        let attrName: string;
        do
        {
            attrName = Compilation.expectOptionalChar(stream, RegularExpressions.tagNameStartChar);
            if (!attrName) { break; }

            attrName += Compilation.expectOptionalChars(stream, RegularExpressions.tagNameChar);

            Compilation.consumeWhitespace(stream);
            if (Compilation.expectOptionalChar(stream, /=/))
            {
                Compilation.consumeWhitespace(stream);

                const quote = Compilation.expectChar(stream, /[\"\']/);
                if (!quote) { throw new Error("Invalid HTML"); }

                const attrValue = Compilation.expectOptionalChars(stream, new RegExp(`[^\\${quote}]`));
                if (!Compilation.expectString(stream, quote)) { throw new Error("Invalid HTML"); }

                attrs[attrName] = attrValue;
            }
            else
            {
                attrs[attrName] = null;
            }

            Compilation.consumeWhitespace(stream);
        }
        while (attrName)

        const singletonTag = !closingTag && Compilation.expectOptionalChar(stream, /\//) != null;
        if (!Compilation.expectChar(stream, />/)) { return null; }

        if (closingTag) { return new TagCloseToken(name); }
        if (singletonTag) { return new SingletonToken(name, attrs); }
        if (markupDeclaration) { return new DeclarationToken(name, attrs); }
        return new TagOpenToken(name, attrs);
    };

    export const tagParser: Compilation.IParser = (stream) =>
    {
        const token = Compilation.expectToken(stream, TagToken);
        if (token)
        {
            if (token instanceof TagOpenToken)
            {
                return `<${token.name}${getAttributeString(token.attributes)}>`;
            }
            else if (token instanceof TagCloseToken)
            {
                return `</${token.name}>`;
            }
            else if (token instanceof SingletonToken)
            {
                return `<${token.name}${getAttributeString(token.attributes)}/>`;
            }
            else if (token instanceof DeclarationToken)
            {
                return `<!${token.name}${getAttributeString(token.attributes)}>`;
            }
        }
        return null;
    };

    export const plainTextParser: Compilation.IParser = (stream) =>
    {
        const token = Compilation.expectToken(stream, Compilation.PlainTextToken);
        if (!token) { return null; }
        return token.text.replace(/\n[ ]*\n/g, "\n");
    };

    export const minifiedPlainTextParser: Compilation.IParser = (stream) =>
    {
        const token = Compilation.expectToken(stream, Compilation.PlainTextToken);
        if (!token) { return null; }
        return token.text
                        // Remove empty newlines 
                         .replace(/\n[ ]*/g, "")
                        // Remove redundant spaces
                         .replace(/[ ]+/g, " ");
    };
}