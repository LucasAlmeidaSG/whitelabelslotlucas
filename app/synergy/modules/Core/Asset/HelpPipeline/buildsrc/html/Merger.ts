namespace RS.ExternalHelp.HTML
{
    export class Merger
    {
        /** Tags for which there may be only one in the document. */
        private static readonly _uniqueTagNames = ["html", "body", "head", "doctype"];

        public merge(tokenSets: Compilation.Token[][]): Compilation.Token[]
        {
            const result: Compilation.Token[] = [];
            // We merge in reverse-dependency order because it makes it simpler to treat "higher-level" templates (i.e. those
            // that appear last in the array) as more important than "lower-level" templates.
            // Otherwise we'd be effectively adding dependencies in before the actual root template.
            // Just imagine lower-level templates as 'filters' applied to higher-level templates and it may make some sense.
            for (let iTokenSet = tokenSets.length - 1; iTokenSet >= 0; iTokenSet--)
            {
                const tokens = tokenSets[iTokenSet];
                const mergeStack: Merge[] = [];

                let depth = 0;
                let pointerIndex = 0;
                const stream = new Compilation.TokenStream(tokens);
                for (let token: Compilation.Token = stream.next(); token != null; token = stream.next())
                {
                    const merge = mergeStack[mergeStack.length - 1];

                    let discardToken = false;

                    // Track depth
                    if (token instanceof TagOpenToken && singletonTags.indexOf(token.name) === -1)
                    {
                        depth++;
                        this.log(`START BLOCK ${token.name}`);
                    }

                    // Discard if close token, where the close token depth === the depth of an open token that was merged
                    if (token instanceof TagCloseToken && singletonTags.indexOf(token.name) === -1)
                    {
                        this.log(`END BLOCK ${token.name}`);
                        if (merge && merge.depth === depth)
                        {
                            this.log(`END MERGE ${merge.name}`);
                            if (merge.name !== token.name) { throw new Error(`Tag merge mismatch; expected close tag for block "${merge.name}", got "${token.name}"`); }
                            // Move to the next matching close token.
                            const nextCloseIndex = this.findNextCloseToken(result, pointerIndex);
                            if (nextCloseIndex === -1 || !this.canMerge(result[nextCloseIndex], token)) { throw new Error(`Could not match closing tag whilst merging block "${token.name}"`); }
                            pointerIndex = this.findNextCloseToken(result, pointerIndex) + 1;
                            mergeStack.pop();
                            discardToken = true;
                        }
                        depth--;
                    }

                    // Discard on overwrite or remove meta switch
                    if (merge && merge.overwrite) { discardToken = true; }

                    if (!discardToken)
                    {
                        // Discard if should merge with some master token
                        for (let iMaster = 0; iMaster < result.length; iMaster++)
                        {
                            const masterToken = result[iMaster];
                            if (this.shouldMerge(masterToken, token))
                            {
                                // Handle meta switches.
                                let overwrite = false;
                                if (masterToken instanceof TagToken && masterToken.attributes != null)
                                {
                                    overwrite = "meta-overwrite" in masterToken.attributes;
                                }

                                // Inherit additional attributes.
                                if (token instanceof TagToken && token.attributes != null && masterToken instanceof TagToken)
                                {
                                    masterToken.attributes = { ...token.attributes, ...masterToken.attributes };
                                }

                                // If we're opening a block, deepen the merge stack.
                                if (token instanceof TagOpenToken && singletonTags.indexOf(token.name) === -1)
                                {
                                    mergeStack.push({ name: token.name, depth, overwrite });
                                    this.log(`START MERGE ${token.name} OVERWRITE ${overwrite}`);
                                }

                                // Discard this token and move to the next master token.
                                discardToken = true;
                                pointerIndex = iMaster + 1;

                                break;
                            }
                        }
                    }

                    if (discardToken) { continue; }

                    result.splice(pointerIndex, 0, token);
                    pointerIndex++;
                }

                this.log(`END TOKEN SET\n`);
                if (depth !== 0) { throw new Error(`${depth} unclosed ${depth === 1 ? "tag" : "tags"}`); }
            }

            // Run process
            let removalDepth = 0;
            for (let i = 0; i < result.length; i++)
            {
                const token = result[i];
                if (removalDepth === 0)
                {
                    if (token instanceof TagToken)
                    {
                        if (token.attributes && "meta-remove" in token.attributes)
                        {
                            if (token instanceof TagOpenToken && singletonTags.indexOf(token.name) === -1)
                            {
                                this.log(`START REMOVE ${token.name}`);
                                removalDepth++;
                            }
                            result.splice(i, 1);
                            --i;
                        }
                    }
                }
                else
                {
                    this.log(`REMOVE ${(token as any).name} ${removalDepth}`);
                    if (token instanceof TagOpenToken && singletonTags.indexOf(token.name) === -1) { removalDepth++; }
                    if (token instanceof TagCloseToken && singletonTags.indexOf(token.name) === -1) { removalDepth--; }
                    result.splice(i, 1);
                    --i;
                }
            }

            return result;
        }

        private log(message: string)
        {
            const debugMode = false;
            if (debugMode) { Log.debug(message); }
        }

        private findNextCloseToken(array: Compilation.Token[], from: number): number
        {
            this.log(`FIND CLOSE TOKEN FROM ${from}`);
            let depth = 1;
            for (let iMaster = from; iMaster < array.length; iMaster++)
            {
                const masterToken = array[iMaster];

                if (masterToken instanceof TagOpenToken && singletonTags.indexOf(masterToken.name) === -1)
                {
                    depth++;
                    this.log(`START TRAVERSE ${masterToken.name}`);
                }

                if (masterToken instanceof TagCloseToken && singletonTags.indexOf(masterToken.name) === -1)
                {
                    this.log(`END TRAVERSE ${masterToken.name}`);
                    depth--;
                    if (depth === 0)
                    {
                        this.log(`FOUND ${masterToken.name}`);
                        return iMaster;
                    }
                }
            }
            return -1;
        }

        private canMerge(a: Compilation.Token, b: Compilation.Token): boolean
        {
            // Are they actually HTML tags?
            if (!(a instanceof TagToken) || !(b instanceof TagToken)) { return false; }

            // Do the tag names match?
            if (a.name.toLowerCase() !== b.name.toLowerCase()) { return false; }

            // Are they matching tag types?
            if (a.constructor !== b.constructor) { return false; }

            return true;
        }

        private shouldMerge(a: Compilation.Token, b: Compilation.Token): boolean
        {
            // Are they actually HTML tags?
            if (!(a instanceof TagToken) || !(b instanceof TagToken)) { return false; }

            if (!this.canMerge(a, b)) { return false; }

            // Is it a tag that should only appear once in the document?
            const tagName = a.name.toLowerCase();
            if (Merger._uniqueTagNames.indexOf(tagName) !== -1) { return true; }

            // Do the tags share an ID?
            // Each ID should really only appear once so it's convenient to use as an inheritance marker.
            if (a.attributes && b.attributes
             && a.attributes["id"] != null
             && a.attributes["id"] === b.attributes["id"])
            {
                return true;
            }

            return false;
        }
    }

    interface Merge
    {
        name: string;
        depth: number;
        overwrite: boolean;
    }
}