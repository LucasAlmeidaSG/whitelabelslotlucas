namespace RS.ExternalHelp.HTML
{
    export namespace SpecialChars
    {
        export const NonBreakingSpace = "&nbsp;";
        export const Space = "&#32;";
        export const DoubleQuote = "&quot;";
        export const LessThan = "&lt;";
        export const GreaterThan = "&gt;";
        export const Ampersand = "&amp;";
        export const LineBreak = "<br/>";
    }

    /** An array of tags that MUST be singleton. */
    export const singletonTags = ["area", "base", "br", "col", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr"];

    const specialChars: Util.Map<string> =
    {
        ["<"]: SpecialChars.LessThan,
        [">"]: SpecialChars.GreaterThan,
        ["\""]: SpecialChars.DoubleQuote,
        ["&"]: SpecialChars.Ampersand,
        ["\n"]: SpecialChars.LineBreak
    };

    export function escapeSpecialChars(unescapedText: string): string
    {
        return substitute(unescapedText, specialChars);
    }

    export function getAttributeString(attrs: Util.Map<string>)
    {
        const pairs: string[] = [];
        for (const key in attrs)
        {
            if (key.substr(0, 5) === "meta-") { continue; }
            const value = attrs[key];
            const pair = value == null ? `${key}` : `${key}="${value}"`;
            pairs.push(pair);
        }

        return pairs.length > 0 ? ` ${pairs.join(" ")}` : "";
    }
}