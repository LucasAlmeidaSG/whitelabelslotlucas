namespace RS.ExternalHelp
{
    export function substitute(value: string, substitutions: Util.Map<string>): string
    {
        const keys = Object.keys(substitutions);
        if (keys.length === 0) { return value; }

        const regExp = new RegExp(`${keys.join("|")}`, "g");
        return value.replace(regExp, (key) => substitutions[key]);
    }

    let shouldAssemble: boolean;

    /** Checks whether or not we should assemble help files, for backwards compatibility with legacy external help pages. */
    export async function checkShouldAssemble(): Promise<boolean>
    {
        if (shouldAssemble != null) { return shouldAssemble; }
        const config = await Config.get();
        const rootModule = Module.getModule(config.rootModule);
        const result = await FileSystem.fileExists(Path.combine(rootModule.path, "help", "template.html"));
        if (!result) { Log.warn("No external help template folder found, skipping help assembly.", "ExternalHelp"); }
        shouldAssemble = result;
        return result;
    }

    export async function getOutputPath(outputDir: string)
    {
        const helpPath = Path.combine(outputDir, "help");
        if (!(await FileSystem.fileExists(helpPath))) { await FileSystem.createPath(helpPath); }
        return helpPath;
    }
}