namespace RS.ExternalHelp
{
    export class ModuleContext
    {
        constructor(public readonly templatePath: string, public readonly buildPath: string) { }

        public async write(data: string, locale: string)
        {
            const destPath = Path.combine(this.buildPath, `${locale}.html`);
            await FileSystem.createPath(this.buildPath);
            await FileSystem.writeFile(destPath, data);
        }
    }
}