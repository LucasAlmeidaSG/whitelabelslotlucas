namespace RS.ExternalHelp.AssembleStages
{
    const versionArg = new RS.EnvArg("VERSION", "dev");

    /**
     * Responsible for copying the built translations of modules.
     */
    export class AssembleLibraries extends AssembleStage.Base
    {
        private _moduleCode: Assembler.CodeSource[];

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<IModule>)
        {
            if (!(await checkShouldAssemble())) { return; }

            this._moduleCode = [];
            await super.execute(settings, moduleList);

            // Assemble helpsrc
            const helpPath = await getOutputPath(settings.outputDir);
            await Assembler.assembleJS(helpPath, "libs", this._moduleCode,
            {
                withHelpers: true,
                withSourcemaps: true,
                minify: false,
                replacements: versionArg.value === "dev" ? undefined : { "{!GAME_VERSION!}": versionArg.value }
            });
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: IModule)
        {
            // Find helpsrc
            const helpDependencies = module.info.helpdependencies;
            if (helpDependencies)
            {
                const moduleList = Module.getDependencySet(helpDependencies);
                for (const depModule of moduleList.data)
                {
                    await this.addCodeSource(depModule, "src");
                    await this.addCodeSource(depModule, "help");
                }
            }

            // Add bower dependencies
            const bowerDependencies = module.info.bowerdependencies;
            if (bowerDependencies)
            {
                for (const depName in bowerDependencies)
                {
                    // Skip if exists already
                    if (this._moduleCode.some((source) => source.name === depName)) { continue; }

                    try
                    {
                        const bowerInfoPath = Path.combine("bower_components", depName, "bower.json");
                        const bowerInfo = await JSON.parseFile(bowerInfoPath);

                        const mainPackagePath = bowerInfo["main"];
                        const mainPath = Path.combine("bower_components", depName, mainPackagePath);
                        const mainText = await FileSystem.readFile(mainPath);
                        const mapPath = Path.replaceExtension(mainPath, ".js.map");

                        let map: string;
                        if (await FileSystem.fileExists(mapPath))
                        {
                            map = await FileSystem.readFile(mapPath);
                        }
                        else
                        {
                            map = await Assembler.resolveIdentitySourceMap(module, mainText, mainPackagePath);
                        }

                        this._moduleCode.push({ name: depName, content: mainText, sourceMap: map, sourceMapDir: null });
                    }
                    catch (err)
                    {
                        throw new Error(`Couldn't locate bower.json file for bower package '${depName}': ${err}`);
                    }
                }
            }

            await this.addCodeSource(module, "help");
        }

        private async addCodeSource(module: RS.Module.Base, unitName: string)
        {
            const codeSrc = await this.getCodeSource(module, unitName);
            if (!codeSrc) { return; }

            if (this._moduleCode.some((source) => source.name === codeSrc.name)) { return; }
            this._moduleCode.push(codeSrc);
        }

        private async getCodeSource(module: RS.Module.Base, unitName: string)
        {
            const unit = module.getCompilationUnit(unitName);
            if (!unit) { return null; }

            const name = unit.name;
            const content = await FileSystem.readFile(unit.outJSFile);
            const rawSourceMap = await FileSystem.readFile(unit.outMapFile);

            let sourceMap: string;
            if (rawSourceMap == null)
            {
                // Generate an identity source map
                sourceMap = Assembler.generateIdentitySourcemap(content, `${name}.js`);
            }
            else
            {
                // Parse the sourcemap and edit the paths
                const sourceMapObj = JSON.parse(rawSourceMap);
                sourceMapObj.sources = (sourceMapObj.sources as string[])
                    .map((src) => src.replace("../src", module.name)
                                     .replace("../helpsrc", module.name));
                sourceMap = JSON.stringify(sourceMapObj);
            }

            return { name, content, sourceMap, sourceMapDir: Path.directoryName(unit.outJSFile) };
        }
    }

    export const assembleLibraries = new AssembleLibraries();
    AssembleStage.register({}, assembleLibraries);
}