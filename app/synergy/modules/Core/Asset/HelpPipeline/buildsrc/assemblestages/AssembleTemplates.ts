namespace RS.ExternalHelp.AssembleStages
{
    /**
     * Responsible for copying the built translations of modules.
     */
    export class AssembleTemplates extends AssembleStage.Base
    {
        private _tokens: Util.Map<Compilation.Token[][]>;
        private _lexer: Compilation.Lexer;

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<IModule>)
        {
            const minify = true;
            if (!(await checkShouldAssemble())) { return; }

            this._tokens = {};
            this._lexer = new Compilation.Lexer([ HTML.tagExpector ]);
            await super.execute(settings, moduleList);

            // Assemble templates
            const helpPath = await getOutputPath(settings.outputDir);
            const merger = new ExternalHelp.HTML.Merger();
            const plainTextParser = minify ? HTML.minifiedPlainTextParser : HTML.plainTextParser;
            const parser = new Compilation.Parser([ HTML.tagParser, plainTextParser ]);
            for (const helpFile in this._tokens)
            {
                const tokens = this._tokens[helpFile];
                const merged = merger.merge(tokens);
                const helpData = parser.parse(merged);

                const filePath = Path.combine(helpPath, helpFile);
                await FileSystem.writeFile(filePath, helpData);
            }
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: IModule)
        {
            // Find help templates
            const helpPath = Path.combine(module.path, "build", "help");
            const helpFiles = await FileSystem.readFiles(helpPath);
            for (const helpFile of helpFiles)
            {
                if (Path.extension(helpFile) === ".html")
                {
                    const fileName = Path.baseName(helpFile);
                    const helpData = await FileSystem.readFile(helpFile);

                    if (!this._tokens[fileName])
                    {
                        this._tokens[fileName] = [];
                    }

                    const tokens = this._lexer.parse(helpData);
                    this._tokens[fileName].push(tokens);
                }
                else
                {
                    const subDir = Path.relative(helpPath, helpFile);
                    const fileBuildPath = Path.combine(settings.outputDir, "help", subDir);
                    await FileSystem.createPath(Path.directoryName(fileBuildPath));
                    await FileSystem.copyFile(helpFile, fileBuildPath);
                }
            }
        }
    }

    export const assembleTemplates = new AssembleTemplates();
    AssembleStage.register({}, assembleTemplates);
}