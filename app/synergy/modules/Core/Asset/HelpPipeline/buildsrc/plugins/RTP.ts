/// <reference path="../templating/Variable.ts" />
/// <reference path="../templating/Function.ts" />

namespace RS.ExternalHelp.Plugins.RTP
{
    function rtp(name?: string): string
    {
        const nameString = name == null ? "" : `data-rtp-name="${name}"`;

        return `<span class=\"rs-variable-rtp\"${nameString}></span>`;
    }

    Templating.registerFunction("RTP", rtp, ["name"]);
    Templating.registerVariable("RTP", rtp());
}