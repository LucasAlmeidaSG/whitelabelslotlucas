/// <reference path="../templating/Function.ts" />

namespace RS.ExternalHelp.Plugins.URL
{
    function urlParam(name: string): string
    {
        if (!name) { throw new Error(`Missing argument 'name'`); }
        const nameString = `data-url-param="${name}"`;
        return `<span class=\"rs-variable-url\"${nameString}></span>`;
    }

    Templating.registerFunction("URLParam", urlParam, ["name"]);
}