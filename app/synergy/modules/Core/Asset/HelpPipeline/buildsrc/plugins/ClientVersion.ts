/// <reference path="../templating/Variable.ts" />

namespace RS.ExternalHelp.Plugins.ClientVersion
{
    Templating.registerVariable("ClientVersion", "<span class=\"rs-variable-clientversion\"></span>");
}