/// <reference path="../templating/Variable.ts" />
/// <reference path="../templating/Function.ts" />

namespace RS.ExternalHelp.Plugins.CopyrightYear
{
    Templating.registerVariable("CopyrightYear", "<span class=\"rs-variable-cryear\"></span>");
}