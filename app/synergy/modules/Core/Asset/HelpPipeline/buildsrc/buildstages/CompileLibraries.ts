namespace RS.ExternalHelp.BuildStages
{
    export const compileLibraries = new RS.BuildStages.Compile("help");
    BuildStage.register({ after: [ addCompilationUnit, RS.BuildStages.compile ] }, compileLibraries);
}
