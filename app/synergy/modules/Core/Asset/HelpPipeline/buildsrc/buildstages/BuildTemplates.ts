namespace RS.ExternalHelp.BuildStages
{
    /**
     * Responsible for building all external help templates.
     */
    export class BuildTemplates extends BuildStage.Base
    {
        private _localeMap: Util.Map<Util.Map<string>>;
        private _localeEncoding: number[];
        private _modules: ModuleContext[];
        private _localeKeys: Util.Map<boolean>;

        public async execute(moduleList: List<Module.Base>)
        {
            this._localeMap = {};
            this._localeEncoding = [];
            this._localeKeys = {};
            this._modules = [];

            const result = await super.execute(moduleList);
            if (!result.workDone || result.errorCount > 0) { return result; }

            Log.pushContext("BuildTemplates");
            Profile.enter("ReadTemplates");

            /* Tokenise templates. */
            Log.info(`Processing ${this._modules.length} templates...`);
            const lexer = new Compilation.Lexer(
            [
                Templating.translationExpector,
                Templating.functionExpector,
                Templating.variableExpector
            ]);
            const templateTokens: Compilation.Token[][] = [];
            for (let i = 0; i < this._modules.length; i++)
            {
                const context = this._modules[i];
                Log.info(`Reading external help template...`, context.module.name);
                const templateData = await FileSystem.readFile(context.templatePath);
                const tokens = lexer.parse(templateData);
                templateTokens[i] = tokens;
            }

            Profile.exit("ReadTemplates");
            Profile.enter("LocaliseTemplates");

            /* Parse templates to produce translated external help files. */
            for (const localeCode in this._localeMap)
            {
                const locale = this._localeMap[localeCode];

                // Normalise locale keys so that keys that exist in some locale but not this one are null rather than not present at all
                for (const key in this._localeKeys)
                {
                    if (!(key in locale))
                    {
                        locale[key] = null;
                    }
                }

                Templating.registerVariable("LocaleCode", localeCode);
                Templating.registerVariable("LanguageCode", localeCode.split("_")[0]);
                Templating.registerVariable("CountryCode", localeCode.split("_")[1]);

                const parser = new Compilation.Parser(
                [
                    Compilation.plainTextParser,
                    Templating.translationParser(locale, true),
                    Templating.functionParser,
                    Templating.variableParser
                ]);

                for (let i = 0; i < this._modules.length; i++)
                {
                    const context = this._modules[i];
                    try
                    {
                        const tokens = templateTokens[i];

                        const text = parser.parse(tokens);
                        await context.write(text, localeCode);
                        await context.module.saveChecksums();
                    }
                    catch (err)
                    {
                        throw new Error(`Failed to build '${context.module.name}' template for locale '${localeCode}': ${err}`);
                    }
                }

                Templating.unregisterVariable("LocaleCode");
                Templating.unregisterVariable("LanguageCode");
                Templating.unregisterVariable("CountryCode");
            }

            Profile.exit("LocaliseTemplates");
            Log.popContext("BuildTemplates");
            return result;
        }

        protected async executeModule(module: IModule): Promise<BuildStage.Result>
        {
            const checksumFiles: string[] = [];

            /* Parse built translations file. */
            const translationsPath = Path.combine(module.path, "build", "translations.json");
            if (await FileSystem.fileExists(translationsPath))
            {
                checksumFiles.push(translationsPath);
                const locales = await JSON.parseFile(translationsPath);
                for (const localeCode in locales)
                {
                    if (!this._localeMap[localeCode])
                    {
                        this._localeMap[localeCode] = {};
                        this._localeEncoding.push(...this.encode(localeCode));
                    }

                    const localeData = locales[localeCode];
                    for (const key in localeData)
                    {
                        const keyNamespace = Build.Utils.getTranslationNamespace(module);
                        const fullKey = `${keyNamespace}.${key}`;
                        const line = localeData[key];
                        this._localeMap[localeCode][fullKey] = line;
                        this._localeEncoding.push(...this.encode(fullKey), ...this.encode(line));
                        this._localeKeys[fullKey] = true;
                    }
                }
            }

            const result: BuildStage.Result = { workDone: false, errorCount: 0 };

            const templatePath = Path.combine(module.path, "help");
            if (!await FileSystem.fileExists(Path.combine(templatePath, "template.html"))) { return result; }

            const allFiles = await FileSystem.readFiles(templatePath);
            checksumFiles.push(...allFiles);

            if (await this.tryUpdateChecksum(module, checksumFiles))
            {
                const buildPath = Path.combine(module.path, "build", "help");
                await FileSystem.deletePath(buildPath);

                for (const filePath of allFiles)
                {
                    /* Use the first HTML file we find, for now. */
                    if (Path.baseName(filePath) === "template.html")
                    {
                        const context = new ModuleContext(module, filePath, buildPath);
                        this._modules.push(context);
                    }
                    else
                    {
                        const subDir = Path.relative(templatePath, filePath);
                        const fileBuildPath = Path.combine(buildPath, subDir);
                        await FileSystem.createPath(Path.directoryName(fileBuildPath));
                        await FileSystem.copyFile(filePath, fileBuildPath);
                    }
                }

                result.workDone = true;
            }

            return result;
        }

        private async tryUpdateChecksum(module: Module.Base, paths: string[]): Promise<boolean>
        {
            const checksumID = "extHelp";
            this._localeEncoding.sort((a, b) => a - b);

            const checksum = await RS.Checksum.getComposite(paths, this._localeEncoding);
            if (!module.checksumDB.diff(checksumID, checksum)) { return false; }

            module.checksumDB.set(checksumID, checksum);

            return true;
        }

        private encode(data: any): number[]
        {
            /* TODO: figure out why JSON encoding for hashing is inconsistent */
            return `${data}`.split("").map((char) => char.charCodeAt(0));
        }
    }

    export const buildTemplates = new BuildTemplates();
    BuildStage.register({ after: [ RS.Localisation.BuildStages.buildLocales ] }, buildTemplates);

    class ModuleContext
    {
        constructor(public readonly module: Module.Base, public readonly templatePath: string, public readonly buildPath: string) { }

        public async write(data: string, locale: string)
        {
            const destPath = Path.combine(this.buildPath, `${locale}.html`);
            await FileSystem.createPath(this.buildPath);
            await FileSystem.writeFile(destPath, data);
        }
    }
}
