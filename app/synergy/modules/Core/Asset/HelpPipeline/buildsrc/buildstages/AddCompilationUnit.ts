namespace RS.ExternalHelp.BuildStages
{
    const defConf: TSConfig =
    {
        compilerOptions:
        {
            // Basic requirements
            noEmitHelpers: true,
            rootDir: ".",

            declaration: true,
            declarationMap: true,

            // Source maps, optional
            sourceMap: true,
            inlineSources: true,
            inlineSourceMap: false,

            // Optional compiler features
            experimentalDecorators: true,
            stripInternal: true,

            outFile: "../build/helpsrc.js",

            target: "ES5",
            lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"]
        }
    };

    const self = "Core.Asset.HelpPipeline";

    /**
     * Responsible for creating the help compilation unit on modules.
     */
    export class AddCompilationUnit extends BuildStage.Base
    {
        protected async executeModule(module: IModule): Promise<BuildStage.Result>
        {
            if (module.getCompilationUnit("help")) { return { workDone: false, errorCount: 0 }; }
            if (!await module.hasCode("helpsrc")) { return { workDone: true, errorCount: 0 }; }

            const helpUnit = module.addCompilationUnit("help", "helpsrc", defConf);
            this.addDependencies(module, helpUnit);

            return { workDone: true, errorCount: 0 };
        }

        protected addDependencies(module: IModule, unit: Build.CompilationUnit)
        {
            const deps = this.resolveRootDependencies(module);
            const depSet = Module.getDependencySet(deps);
            for (const dep of depSet.data)
            {
                this.addModuleAsDependency(unit, dep);
            }
        }

        protected addModuleAsDependency(unit: Build.CompilationUnit, moduleToAdd: IModule)
        {
            const moduleSrc = moduleToAdd.getCompilationUnit(Module.CompilationUnits.Source);
            if (moduleSrc)
            {
                unit.dependencies.add(
                {
                    kind: Build.CompilationUnit.DependencyKind.CompilationUnit,
                    name: moduleSrc.name,
                    compilationUnit: moduleSrc
                });
            }

            const moduleHelpSrc = moduleToAdd.getCompilationUnit("help");
            if (moduleHelpSrc)
            {
                unit.dependencies.add(
                {
                    kind: Build.CompilationUnit.DependencyKind.CompilationUnit,
                    name: moduleHelpSrc.name,
                    compilationUnit: moduleHelpSrc
                });
            }
        }

        protected resolveRootDependencies(module: IModule): string[]
        {
            const helpDependencies = module.info.helpdependencies || [];
            if (module.name !== self && helpDependencies.indexOf(self) === -1)
            {
                helpDependencies.push(self);
            }
            return helpDependencies;
        }
    }

    export const addCompilationUnit = new AddCompilationUnit();
    BuildStage.register({}, addCompilationUnit);
}
