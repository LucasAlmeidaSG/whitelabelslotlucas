namespace RS.AssetPipeline
{
    import CodeGen = Build.CodeGeneration;

    /** Represents a node in a asset hierarchy. */
    interface Node
    {
        name: string;
        type: "map"|"array";
        children: List<Node>;
        asset?: BaseAsset;
    }

    /**
     * Extracts children with numeric keys from the specified node, in order.
     * @param node 
     */
    function getArrayChildren(node: Node): List<Node>
    {
        const result = node.children.filter(c => Is.number(parseInt(c.name)));
        result.sort((a, b) => parseInt(a.name) - parseInt(b.name));
        return result;
    }

    /**
     * Generates a node hierarchy for the specified list of assets.
     * @param assets 
     */
    function generateAssetHierarchy(assets: List<BaseAsset>): Node
    {
        const g: Node = { name: "g", type: "map", children: new List<Node>() };
        
        for (const asset of assets.data)
        {
            const namePath = asset.id.split(".");
            let curNode = g;
            for (let i = 0, l = namePath.length - 1; i < l; ++i)
            {
                const segment = namePath[i];
                let newNode: Node= curNode.children.find(n => n.name.toLowerCase() === segment.toLowerCase());
                if (newNode)
                {
                    curNode = newNode;
                }
                else
                {
                    newNode = { name: segment, type: "map", children: new List<Node>() };
                    curNode.children.add(newNode);
                    curNode = newNode;
                }
            }
            const assetNodeName = namePath[namePath.length - 1];
            let assetNode: Node = curNode.children.find(n => n.name.toLowerCase() === assetNodeName.toLowerCase());
            if (!assetNode)
            {
                assetNode = { name: assetNodeName, type: "map", children: new List<Node>() };
                curNode.children.add(assetNode);
            }
            if (assetNode.asset == null)
            {
                assetNode.asset = asset;
            }
            else
            {
                Log.warn(`Duplicate asset found by id '${asset.id}' when assembling asset hierarchy for code generation`);
            }
        }
        sortAssetHierarchy(g);
        return g;
    }

    function sortAssetHierarchy(rootNode: Node)
    {
        function sorter(a: Node, b: Node) { return a.name.localeCompare(b.name); }
        if (rootNode.children != null)
        {
            rootNode.children.sort(sorter);
            for (const child of rootNode.children.data)
            {
                sortAssetHierarchy(child);
            }
        }
    }

    function logAssetHierarchy(rootNode: Node, indent = 0)
    {
        const prefix = ("                ").substr(0, indent);
        Log.debug(`${prefix} - ${rootNode.name}`);
        for (const childNode of rootNode.children.data)
        {
            logAssetHierarchy(childNode, indent + 1);
        }
    }

    /**
     * Generates a type for the specified node.
     * @param node 
     */
    function generateType(node: Node): CodeMeta.TypeRef
    {
        const arrayChildren = getArrayChildren(node);

        let type: CodeMeta.TypeRef = null;
        if (node.asset != null)
        {
            // Let the asset have a go
            type = CodeGen.intersect(type, node.asset.emitCodeDefinition().type);
        }
        else if (arrayChildren.length === node.children.length)
        {
            // Simplify to an array type
            const childTypes = arrayChildren.map(c => generateType(c));
            const arrayInner = CodeGen.getUnion(childTypes.dataCopy);

            type = {
                kind: CodeMeta.TypeRef.Kind.Array,
                inner: arrayInner
            };
            return type;
        }

        // Does the node have children?
        if (node.children.length > 0)
        {
            // Create an inline type
            const inlineType: CodeMeta.TypeRef.Inline =
            {
                kind: CodeMeta.TypeRef.Kind.Inline,
                properties: []
            };
            for (const child of node.children.data)
            {
                const cleanChildName = cleanName(child.name);
                if (cleanChildName !== "")
                {
                    inlineType.properties.push({ name: child.name, type: generateType(child) });
                }
            }
            type = CodeGen.intersect(type, inlineType);
        }

        // Does the node have array children?
        if (arrayChildren.length > 0)
        {
            const childTypes = arrayChildren.map(c => generateType(c));
            type = CodeGen.intersect(type, { kind: CodeMeta.TypeRef.Kind.WithTypeArgs, inner: { kind: CodeMeta.TypeRef.Kind.Raw, name: "ArrayLike" }, genericTypeArgs: [ CodeGen.getUnion(childTypes.dataCopy) ] });
        }

        return type || { kind: CodeMeta.TypeRef.Kind.Inline, properties: [] };
    }

    /**
     * Generates a value for the specified node.
     * @param node 
     */
    function generateValue(node: Node): CodeGen.Value
    {
        let value: CodeGen.Value = null;

        const arrayChildren = getArrayChildren(node);

        // Does it have an asset attached?
        if (node.asset != null)
        {
            // Let the asset have a go
            value = node.asset.emitCodeDefinition().value;
        }
        if (value == null)
        {
            if (arrayChildren.length === node.children.length)
            {
                // We can simplify to an array
                value = {
                    kind: "arrval",
                    name: node.name,
                    values: arrayChildren.map(c => generateValue(c)).filter(v => v != null).dataCopy
                };
                return value;
            }
            value = {
                kind: "objval",
                name: node.name,
                values: []
            };
        }

        // If it's a object value, we support children
        if (CodeGen.isNamed(value) && value.kind === "objval")
        {
            // Does the node have array children?
            if (arrayChildren.length > 0)
            {
                value.values.push({
                    kind: "keyval",
                    name: "length",
                    value: `${arrayChildren.length}`
                });
                for (let i = 0, l = arrayChildren.length; i < l; ++i)
                {
                    const subnode = arrayChildren.data[i];
                    const valForSubnode = generateValue(subnode);
                    if (valForSubnode != null)
                    {
                        value.values.push({
                            kind: "keyval",
                            name: `${i}`,
                            value: valForSubnode
                        });
                    }
                }
            }

            // Iterate all subnodes
            for (const subnode of node.children.data)
            {
                if (!arrayChildren.contains(subnode))
                {
                    const valForSubnode = generateValue(subnode);
                    if (valForSubnode != null)
                    {
                        const name = cleanName(subnode.name);
                        if (name !== "")
                        {
                            value.values.push({
                                kind: "keyval",
                                name: name,
                                value: valForSubnode
                            });
                        }
                        else
                        {
                            Log.warn(`Skipped code generation for subnode '${subnode.name}', couldn't clean the name`);
                        }
                    }
                }
            }            
        }

        // Done
        return value;
    }

    /**
     * Performs code generation for the given asset node (top-level).
     * @param node 
     * @param rootNamespace 
     */
    function performCodeGenerationForNode(node: Node, rootNamespace: CodeGen.Namespace)
    {
        const name = cleanName(node.name);

        const value = generateValue(node);
        if (value != null)
        {
            if (name !== "")
            {
                // Does the asset have children?
                if (node.children.length > 0)
                {
                    // Emit a parent-type node
                    rootNamespace.children.push({
                        kind: "var",
                        isConst: true,
                        name: name,
                        type: { kind: CodeMeta.TypeRef.Kind.Raw, name },
                        value
                    });

                    const interfaceType: CodeGen.Interface =
                    {
                        kind: "interface",
                        name,
                        properties: []
                    };
                    if (node.asset)
                    {
                        const baseType = node.asset.emitCodeDefinition().type;

                        // If the type is a "simple" type...
                        if (baseType.kind === CodeMeta.TypeRef.Kind.Raw)
                        {
                            // ...just extend it
                            interfaceType.extends = [ baseType ];
                        }
                        else
                        {
                            // Otherwise, we need to setup a typedef for it and extend that
                            const baseTypeDef: CodeGen.Type =
                            {
                                kind: "type",
                                name: `${name}Base`,
                                type: baseType
                            };
                            rootNamespace.children.push(baseTypeDef);
                            interfaceType.extends = [ { kind: CodeMeta.TypeRef.Kind.Raw, name: `${name}Base` } ];
                        }
                        
                    }
                    for (const child of node.children.data)
                    {
                        const cleanChildName = cleanName(child.name);
                        if (cleanChildName !== "")
                        {
                            interfaceType.properties.push({
                                kind: "propertydeclr",
                                name: child.name,
                                type: generateType(child)
                            });
                        }
                    }
                    rootNamespace.children.push(interfaceType);
                }
                else
                {
                    // Emit a normal-type node
                    rootNamespace.children.push({
                        kind: "var",
                        isConst: true,
                        name: name,
                        type: generateType(node) || { kind: CodeMeta.TypeRef.Kind.Raw, name: `any` },
                        value
                    });
                }
            }
            else
            {
                Log.warn(`Skipped code generation for subnode '${node.name}', couldn't clean the name`);
            }
        }
    }

    /**
     * Performs code generation for the specified set of assets.
     * @param assets 
     */
    export async function performCodeGeneration(module: Module.Base, path: string, assets: List<BaseAsset>)
    {
        // Only generate for assets which support it
        assets = assets.filter(a => a.supportsCodeGen);

        // Don't bother if there are none
        if (assets.length === 0) { return; }

        // Generate asset hierarchy
        const g = generateAssetHierarchy(assets);

        // Determine root asset namespace
        const assetNamespace = (module.info as any).assetpipeline && (module.info as any).assetpipeline.namespace || `${module.name}.${Constants.generatedNamespace}`;

        // Root namespace
        const rootNamespace: CodeGen.Namespace =
        {
            name: assetNamespace,
            comment: `Contains asset definitions for the '${module.name}' module.`,
            kind: "namespace",
            children: []
        };

        // Iterate all nodes in the global node
        for (const node of g.children.data)
        {
            performCodeGenerationForNode(node, rootNamespace);
        }

        // Derive all groups
        const allGroups = assets.map(a => a.group);
        allGroups.removeDuplicates();
        allGroups.sort((a, b) => a.localeCompare(b));

        // Group namespace
        const groupNamespace: CodeGen.Namespace =
        {
            name: "Groups",
            comment: `Contains all asset groups used by the '${module.name}' module.`,
            kind: "namespace",
            children: []
        };
        rootNamespace.children.push(groupNamespace);

        // Populate groups
        for (const group of allGroups.data)
        {
            groupNamespace.children.push({
                kind: "var",
                isConst: true,
                name: cleanName(group),
                value: `"${group}"`
            });
        }

        // Gather all variants
        const allVariants = new List<string>();
        for (const asset of assets.data)
        {
            if (asset.definition.variants != null)
            {
                for (const variantName in asset.definition.variants)
                {
                    if (!allVariants.contains(variantName))
                    {
                        allVariants.add(variantName);
                    }
                }
            }
            else
            {
                if (!allVariants.contains(Constants.sharedVariantName))
                {
                    allVariants.add(Constants.sharedVariantName);
                }
            }
        }

        // Generate registerVariants call
        rootNamespace.children.push({
            kind: "var",
            name: "variants",
            isConst: true,
            value: `RS.Asset.registerVariants([${allVariants.data.map(v => `"${v}"`).join(", ")}])`
        });

        // Generate code
        const formatter = new CodeGen.Formatter();
        formatter.emitNamed(rootNamespace);
        const rawCode = formatter.toString();
        await FileSystem.writeFile(path, rawCode);
    }

    /**
     * Cleans a name or id for suitable use as an identifier or key in TypeScript.
     * @param name 
     */
    export function cleanName(name: string): string
    {
        // Trim
        name = name.trim();

        // Replace any unknown characters with underscores
        name = name.replace(/[^a-zA-Z0-9_]+/g, "_");

        // If it starts with any numbers, strip them
        name = name.replace(/^[0-9]+/g, "");

        // Done
        return name;
    }
}