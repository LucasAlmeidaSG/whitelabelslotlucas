namespace RS.AssetPipeline
{
    /**
     * A base variant for an asset definition.
     */
    export interface BaseVariant
    {
        /** Path to the asset, relative to the manifest. */
        path?: string | Util.Map<string>;

        /** Any formats for the asset to export. */
        formats?: string[];

        /** Any browsers where the asset won't be loaded */
        exclude?: string[];
    }

    /**
     * A base asset definition from a manifest.
     */
    export interface AssetDefinition extends BaseVariant
    {
        /** ID of the asset. */
        id: string;

        /** Path to the manifest, relative to the assets folder. */
        manifestPath: string;

        /** The group name of the asset. */
        group: string;

        /** The module to which the asset belongs. */
        module: string;

        /** The asset type. */
        type: string;

        /** Any variants for the asset. */
        variants?: Util.Map<BaseVariant>;
    }
}