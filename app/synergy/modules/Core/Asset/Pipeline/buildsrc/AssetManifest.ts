/// <reference path="AssetDefinition.ts" />
/// <reference path="BaseAsset.ts" />

namespace RS.AssetPipeline
{
    /**
     * Contains an entry for a particular asset variant on a particular module.
     */
    export interface ManifestEntry
    {
        /** ID of the asset. */
        id: string;

        /** Type of the asset. */
        type: string;

        /** Group of the asset. */
        group: string;

        /** Custom variant data (e.g. resolution for images etc). */
        variantData: object;

        /** Array of source files for the asset. The first entry MUST be the "root" file (e.g. the json file for a spritesheet). */
        paths: string[];

        /** Array of formats for the asset. */
        formats: string[];

        /** Array of browser where the asset won't be loaded */
        exclude: string[]
    }

    /**
     * Contains all assets for a particular variant on a particular module.
     */
    export class Manifest
    {
        protected _module: Module.Base;
        protected _variantName: string;
        protected _assets: Util.Map<ManifestEntry>;

        /**
         * Gets the module to which this manifest belongs.
         */
        public get module() { return this._module; }

        /**
         * Gets the variant name for this manifest.
         */
        public get variantName() { return this._variantName; }

        /**
         * Gets the location of this manifest. It may not exist yet.
         */
        public get path() { return Path.combine(this._module.path, "build", "assets", this._variantName, "manifest.json"); }

        public constructor(module: Module.Base, variantName: string)
        {
            this._module = module;
            this._variantName = variantName;
            this._assets = {};
        }

        public static getAssetReference(module: string, id: string)
        {
            return `${module}.${id}`;
        }

        /**
         * Loads this manifest from the cache, if it exists.
         */
        public async load()
        {
            if (await FileSystem.fileExists(this.path))
            {
                this._assets = await JSON.parseFile(this.path);
            }
            else
            {
                this._assets = {};
            }
        }

        /**
         * Writes this manifest to the cache.
         */
        public async save()
        {
            await FileSystem.createPath(Path.directoryName(this.path));
            await FileSystem.writeFile(this.path, JSON.stringify(this._assets));
        }

        /**
         * Gets an asset in this manifest.
         * @param assetID
         */
        public getAsset(assetID: string): ManifestEntry
        {
            const ref = this.getAssetReference(assetID);
            return this._assets[ref];
        }

        /**
         * Sets an asset in this manifest.
         * @param assetID
         */
        public setAsset(assetID: string, value: ManifestEntry)
        {
            const ref = this.getAssetReference(assetID);
            this._assets[ref] = value;
        }

        private getAssetReference(assetID: string): string
        {
            return Manifest.getAssetReference(this._module.name, assetID);
        }
    }
}