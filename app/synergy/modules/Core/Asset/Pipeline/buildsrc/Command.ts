namespace RS.AssetPipeline
{
    interface BaseCommandRequirement
    {
        readonly error: string;
    }

    export interface StrictCommandRequirement extends BaseCommandRequirement
    {
        readonly command: string;
    }

    export interface MultiCommandRequirement extends BaseCommandRequirement
    {
        /** Requires any one of these commands, in left-to-right order and with priority to non-deprecated commands. */
        readonly oneOf: ReadonlyArray<string>;
    }

    export type CommandRequirement = MultiCommandRequirement | StrictCommandRequirement;

    /**
     * Declares that an asset requires certain commands to be present when being processed.
     */
    export const RequireCommands = Tag.create<CommandRequirement[]>();

    /**
     * Resolves the given CommandRequirement to a command name.
     * Throws UnresolvedRequirementError if the requirement could not be resolved.
     */
    export function resolveRequirement(requirement: CommandRequirement): PromiseLike<string>;
    export async function resolveRequirement(requirement: CommandRequirement): Promise<string>
    {
        if ("oneOf" in requirement)
        {
            for (const command of requirement.oneOf)
            {
                if (await Command.exists(command))
                {
                    return command;
                }
            }
        }
        else
        {
            return await Command.exists(requirement.command) ? requirement.command : null;
        }

        throw new UnresolvedRequirementError(requirement.error, requirement);
    }

    export class UnresolvedRequirementError
    {
        public constructor(public readonly message: string, public readonly requirement: CommandRequirement) {}
        public toString() { return `UnresolvedRequirementError: ${this.message}`; }
    }
}