namespace RS.AssetPipeline
{
    const moduleName = "Core.Asset.Pipeline";

    /**
     * Module specific configuration.
     */
    export interface ModuleConfig
    {
        /** Don't export these formats for any assets. */
        blacklistFormats: string[];
    }

    export namespace ModuleConfig
    {
        export const defaultConfig: ModuleConfig =
        {
            blacklistFormats: []
        };

        /**
         * Gets the config for this module.
         */
        export function get(): ModuleConfig
        {
            const cfg = RS.Config.activeConfig.moduleConfigs[moduleName] as ModuleConfig;
            if (cfg)
            {
                return { ...defaultConfig, ...cfg };
            }
            else
            {
                return defaultConfig;
            }
        }
    }
}