namespace RS.AssetPipeline
{
    interface FoundAlias
    {
        // Name of the alias.
        name: string;
        // Source code (.ts) file the alias was found in.
        sourceFile: string;
        // The path to the manifest the alias was found in.
        manifestPath: string;
    }

    interface AliasInfo
    {
        // Name of the alias.
        name: string;
        // The path to the manifest the alias was found in.
        manifestPath: string;
    }

    export const assetListUnused = Build.task({ name: "asset-list-unused", description: "List unused assets" }, async () =>
    {
        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Filter out certain folders
        const blacklistedManifestDirs = ["src", "build", "translations"];
        const blacklistedManifestFiles: string[] = [];

        // Find all manifest files
        const allManifests = await findAllFilesMatching(rootModule.path, blacklistedManifestFiles, blacklistedManifestDirs, null, "manifest.json");

        // Get all manifest aliases
        const aliasInfoList = await getAllManifestAliases(allManifests);

        // Find all source files
        const blackListedSrcDirs = ["assets", "build", "translations"];
        const blackListedSrcFiles = ["Assets.ts"];

        // Get all source files
        const allSourceFiles = await findAllFilesMatching(rootModule.path, blackListedSrcFiles, blackListedSrcDirs, ".ts");

        // Get all aliases that are not found in any source file
        await checkSourceFilesForAssetAliases(allSourceFiles, aliasInfoList);

        // Display, allManifestKeys will be filtered down
        const output: string[] = [];
        for (const notFoundAlias of aliasInfoList)
        {
            output.push(`Alias: '${notFoundAlias.name}' was not found\nPath:  '${notFoundAlias.manifestPath}'`);
        }

        RS.Log.info(output.join("\n"));
    });

    export const assetListUsed = Build.task({ name: "asset-list-used", description: "List used assets" }, async () =>
    {
        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Filter out certain folders
        const blacklistedManifestDirs = ["src", "build", "translations"];
        const blacklistedManifestFiles: string[] = [];

        // Find all manifest files
        const allManifests = await findAllFilesMatching(rootModule.path, blacklistedManifestFiles, blacklistedManifestDirs, null, "manifest.json");

        // Get all manifest aliases
        const aliasInfoList = await getAllManifestAliases(allManifests);

        // Find all source files
        const blackListedSrcDirs = ["assets", "build", "translations"];
        const blackListedSrcFiles = ["Assets.ts"];

        // Get all source files
        const allSourceFiles = await findAllFilesMatching(rootModule.path, blackListedSrcFiles, blackListedSrcDirs, ".ts");

        // Get all aliases that are not found in any source file
        const usedAliasInfoList = await checkSourceFilesForAssetAliases(allSourceFiles, aliasInfoList);

        // Display, found alias
        const output: string[] = [];
        for (const foundAlias of usedAliasInfoList)
        {
            output.push(`Alias: '${foundAlias.name}' was found\nPath:  '${foundAlias.manifestPath}'\nPath:  '${foundAlias.sourceFile}'`);
        }

        RS.Log.info(output.join("\n"));
    });

    async function checkSourceFilesForAssetAliases(sourceFiles: string[], aliasInfoList: AliasInfo[]): Promise<FoundAlias[]>
    {
        // Store all aliases
        let foundAliases: FoundAlias[] = [];

        // Find each file as an object
        for (const sourceFile of sourceFiles)
        {
            // Check if any are in this file
            const fileFoundAliases = await checkSourceFileForAssetAliases(sourceFile, aliasInfoList);

            // Get the actual keys
            foundAliases = foundAliases.concat(fileFoundAliases);
        }

        // Return found aliases
        return foundAliases;
    }

    async function checkSourceFileForAssetAliases(sourceFile: string, aliasInfoList: AliasInfo[]): Promise<FoundAlias[]>
    {
        // Store all aliases
        const foundAliases: FoundAlias[] = [];

        // Read the file contents
        const file = await FileSystem.readFile(sourceFile);

        // Find each file as an object
        for (let index = aliasInfoList.length - 1; index >= 0; index--)
        {
            const aliasInfo = aliasInfoList[index];

            // The alias used in the manifest
            const assetAlias = aliasInfo.name;

            // Have we found this asset alias
            if (file.indexOf(assetAlias) >= 0)
            {
                // Store information on found alias
                const foundAlias: FoundAlias = {
                    name: sourceFile,
                    sourceFile: sourceFile,
                    manifestPath: aliasInfo.manifestPath
                };

                // Store as found alias
                foundAliases.push(foundAlias);

                // Remove found asset info from the aliasInfo list
                aliasInfoList.splice(index, 1);
            }
        }

        return foundAliases;
    }

    async function getAllManifestAliases(manifestPaths: string[]): Promise<AliasInfo[]>
    {
        // Store all aliases and concat to the same array
        let aliasInfoList: AliasInfo[] = [];

        // Find each file as an object
        for (const manifestPath of manifestPaths)
        {
            // Read the file contents
            const jsonObject = await JSON.parseFile(manifestPath);

            // Get the actual keys as aliases
            const manifestKeys = Object.keys(jsonObject);

            // Turn these keys into an alias info
            const fileInfoList = manifestKeys.map((key) =>
            {
                const info: AliasInfo = {
                    name: key,
                    manifestPath: manifestPath
                }

                return info;
            });

            aliasInfoList = aliasInfoList.concat(fileInfoList);
        }

        return aliasInfoList;
    }

    async function findAllFilesMatching(mainPath: string, blacklistedFiles: string[], blacklistedDirs: string[], fileExtension: string, matchFileName?: string): Promise<string[]>
    {
        // List of found file paths
        let foundFilePaths: string[] = [];

        const allFileOrFolders = await FileSystem.readDirectory(mainPath, true);

        // Remove named in the above black list for files
        const fileOrFolders = allFileOrFolders.filter((fileInfo) => fileInfo.type !== "file" || fileInfo.type === "file" && blacklistedFiles.indexOf(fileInfo.name) < 0);

        // Get all matching files
        let matchingFiles: FileIO.FileOrFolder[];
        if (matchFileName != null)
        {
            // use file name
            matchingFiles = fileOrFolders.filter((file) => file.type === "file" && file.name === matchFileName);
        }
        else
        {
            // Use extension
            matchingFiles = fileOrFolders.filter((file) => file.type === "file" && Path.extension(file.name) === fileExtension);
        }

        // Build paths for matching files
        foundFilePaths = matchingFiles.map((matched) => Path.combine(mainPath, matched.name));

        // Filter out files
        const allDirectories = fileOrFolders.filter((file) => file.type !== "file");

        // Remove named directories present in the directories black list
        const directories = allDirectories.filter((file) => blacklistedDirs.indexOf(file.name) <= -1);

        // Loop left over and find matching
        for (const dir of directories)
        {
            const subPath = Path.combine(mainPath, dir.name);
            const foundSubFiles = await findAllFilesMatching(subPath, blacklistedFiles, blacklistedDirs, fileExtension, matchFileName);

            // Combine arrays
            foundFilePaths = foundFilePaths.concat(foundSubFiles);
        }

        return foundFilePaths;
    }
}