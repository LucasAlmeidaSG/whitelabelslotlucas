/**
 * Contains shared configuration constants.
 */
namespace RS.AssetPipeline.Constants
{
    /** Folder name for assets, within module directory. */
    export const inAssetsFolder = "assets";

    /** Folder name for assets, within build directory. */
    export const outAssetsFolder = "assets";

    /** File name for source asset manifests. */
    export const inManifestName = "manifest.json";

    /** File name for asset manifest, within build directory. */
    export const outManifestName = "manifest.json";

    /** Folder to store generated code in. */
    export const generatedFolder = "generated";

    /** File to store generated code in. */
    export const generatedName = "Assets.ts";

    /** Default namespace to put code generated assets in (appended to module name). */
    export const generatedNamespace = "Assets";

    /** Name of the "global" variant. */
    export const sharedVariantName = "shared";

    /** Name of the asset group to use for assets not assigned to a group. */
    export const undefinedGroupName = "common";
}