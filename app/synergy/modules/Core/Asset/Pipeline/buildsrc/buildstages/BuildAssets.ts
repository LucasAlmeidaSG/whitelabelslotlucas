/// <reference path="../BaseAsset.ts" />
/// <reference path="../Constants.ts" />
/// <reference path="../CodeGeneration.ts" />

namespace RS.AssetPipeline.BuildStages
{
    /**
     * Responsible for building all game assets.
     */
    export class BuildAssets extends BuildStage.Base
    {
        protected _assetDefinitions: List<AssetDefinition>;
        protected _assetManifestMap:
        {
            [moduleName: string]:
            {
                [variantName: string]: Manifest;
            };
        };

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            this._assetDefinitions = new List<AssetDefinition>();
            this._assetManifestMap = {};

            const ourName: string = Object.getPrototypeOf(this).constructor.name;
            Log.pushContext(ourName);

            // Scan all modules and find all asset manifests
            await super.execute(moduleList);

            // Load existing manifests from the cache (doesn't matter if they don't yet exist)
            // These will only exist if the build process has previously been run
            for (const moduleName in this._assetManifestMap)
            {
                for (const variantName in this._assetManifestMap[moduleName])
                {
                    const manifest = this._assetManifestMap[moduleName][variantName];
                    await manifest.load();
                }
            }

            // Load all assets
            const assets = new List<BaseAsset>();
            let errorCount = 0;
            for (const definition of this._assetDefinitions.data)
            {
                try
                {
                    const asset = await this.loadAsset(definition);
                    if (asset != null)
                    {
                        assets.add(asset);
                    }
                    else
                    {
                        Log.error(`Asset by ID '${definition.id}' has an unknown type '${definition.type}'`);
                        ++errorCount;
                    }
                }
                catch (err)
                {
                    Log.error(err.stack, definition.id);
                    ++errorCount;
                }
            }
            assets.sort((a, b) => a > b ? 1 : a === b ? 0 : -1);

            // Code generation
            for (const moduleName in this._assetManifestMap)
            {
                // Delete old generated code
                const codeGenPath = Path.combine(Module.getModule(moduleName).path, "src", Constants.generatedFolder, Constants.generatedName);
                try
                {
                    await FileSystem.deletePath(codeGenPath);
                }
                catch (err)
                {
                    // Probably just didn't exist in the first place
                }

                // Check that the module actually wants codegen
                let wantsCodeGen = true;
                const moduleConfig = Config.activeConfig.moduleConfigs && Config.activeConfig.moduleConfigs[moduleName];
                if (moduleConfig)
                {
                    const moduleAssetPipelineConfig: object = (moduleConfig as any).assetpipeline;
                    if (moduleAssetPipelineConfig)
                    {
                        if ((moduleAssetPipelineConfig as any).codeGen === false)
                        {
                            wantsCodeGen = false;
                        }
                    }
                }
                if (wantsCodeGen)
                {
                    // Make sure generated directory exists
                    await FileSystem.createPath(Path.directoryName(codeGenPath));

                    // Find all assets belonging to this module
                    const assetsForModule = assets.filter(a => a.definition.module == moduleName);

                    // Generate code!
                    await performCodeGeneration(Module.getModule(moduleName), codeGenPath, assetsForModule);

                }
            }

            // Find assets that require processing
            const assetsToProcess = assets.filter(a => a.requiresProcessing);
            // if (assetsToProcess.length === 0) { return; }

            // Check commands
            const uniqueTypes = new List<()=>void>();
            for (const asset of assetsToProcess.data)
            {
                const ctor = Object.getPrototypeOf(asset).constructor;
                if (!uniqueTypes.contains(ctor))
                {
                    uniqueTypes.add(ctor);
                }
            }
            let skipCount = 0;
            for (const ctor of uniqueTypes.data)
            {
                const requiredCommands = RequireCommands.get(ctor);
                if (requiredCommands)
                {
                    for (const requirement of requiredCommands)
                    {
                        try
                        {
                            resolveRequirement(requirement);
                        }
                        catch (err)
                        {
                            if (err instanceof UnresolvedRequirementError)
                            {
                                Log.error(`${err}`);
                                const lenBefore = assetsToProcess.length;
                                assetsToProcess.removeRange(assetsToProcess.data.filter(asset => asset instanceof ctor));
                                skipCount += (lenBefore - assetsToProcess.length);
                                ++errorCount;
                            }
                            else
                            {
                                throw err;
                            }
                        }
                    }
                }
            }

            // Log out any skipped assets due to missing commands
            if (skipCount > 0)
            {
                if (assetsToProcess.length === 0)
                {
                    Log.error(`ALL assets will be skipped`);
                    Log.popContext(ourName);
                    return { workDone: true, errorCount };
                }
                else
                {
                    Log.error(`${skipCount} assets will be skipped`);
                }
            }

            // Dry process unchanged assets, so that we don't lose them from the manifest
            for (const asset of assets.data)
            {
                if (!asset.requiresProcessing)
                {
                    await asset.process(this._assetManifestMap[asset.module.name], true);
                }
            }

            // Process them
            if (assetsToProcess.length > 0)
            {
                const timer = new Timer();
                Log.info(`Processing ${assetsToProcess.length} of ${assets.length} assets...`);
                const parallel = new Build.ParallelProcessor(assetsToProcess.dataCopy, 4, async (asset, threadID) =>
                {
                    Log.info(`Processing ${asset.definition.type} '${asset.id}'...`);
                    await asset.process(this._assetManifestMap[asset.module.name], false);
                });
                await parallel.process();
                Log.info(`Processed all assets in ${Math.round(timer.elapsed / 10.0)/100.0} s`);
            }
            else
            {
                Log.info(`No changes found, no work to do`);
            }

            // Spit out manifests
            for (const moduleName in this._assetManifestMap)
            {
                const variants = this._assetManifestMap[moduleName];
                for (const variantName in variants)
                {
                    const manifest = variants[variantName];
                    await manifest.save();
                }
            }

            // Save checksums
            for (const moduleName in this._assetManifestMap)
            {
                await Module.getModule(moduleName).saveChecksums();
            }

            Log.popContext(ourName);

            return { workDone: true, errorCount };
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const manifestsByVariant = this._assetManifestMap[module.name] = {};

            // Search module for all asset manifests
            let errorCount = 0;
            const assetsPath = Path.combine(module.path, Constants.inAssetsFolder);
            const subFiles = await FileSystem.readFiles(assetsPath);
            for (const subFile of subFiles)
            {
                if (Path.baseName(subFile) === Constants.inManifestName)
                {
                    let manifestData: any;
                    try
                    {
                        manifestData = await JSON.parseFile(subFile);
                    }
                    catch (err)
                    {
                        Log.error(`Parse error in manifest '${subFile}': ${err}`);
                        ++errorCount;
                        continue;
                    }
                    /* tslint:disable-next-line */
                    delete manifestData["$schema"];

                    for (const assetID in manifestData)
                    {
                        const manifestEntry = manifestData[assetID];
                        if (manifestEntry.manifestID)
                        {
                            Log.warn(`manifestID is DEPRECATED, use group instead`, assetID);
                        }

                        const definition: AssetDefinition =
                        {
                            ...manifestEntry,
                            id: assetID,
                            path: manifestEntry.path,
                            manifestPath: Path.relative(assetsPath, subFile),
                            group: manifestEntry.group || Constants.undefinedGroupName,
                            module: module.name,
                        };
                        if (Is.object(manifestEntry.path))
                        {
                            let pathFound = false;
                            for (const key in manifestEntry.path)
                            {
                                if (Is.string(manifestEntry.path[key]))
                                {
                                    pathFound = true;
                                    break;
                                }
                            }
                            if (!pathFound)
                            {
                                Log.error(`Asset by ID '${assetID}' has empty map-type path in definition`);
                                ++errorCount;
                                continue;
                            }
                        }
                        else if (!Is.string(manifestEntry.path))
                        {
                            let pathFound = false;
                            if (manifestEntry.variants)
                            {
                                for (const variantName in definition.variants)
                                {
                                    if (definition.variants[variantName].path != null)
                                    {
                                        definition.path = definition.variants[variantName].path;
                                        pathFound = true;
                                        break;
                                    }
                                }
                            }
                            if (!pathFound)
                            {
                                Log.error(`Asset by ID '${assetID}' is missing path in definition`);
                                ++errorCount;
                                continue;
                            }
                        }
                        if (definition.variants)
                        {
                            for (const variantName in definition.variants)
                            {
                                const variant = definition.variants[variantName];
                                if (variant.path == null)
                                {
                                    variant.path = definition.path;
                                }
                                manifestsByVariant[variantName] = manifestsByVariant[variantName] || new Manifest(module, variantName);
                            }
                        }
                        else
                        {
                            manifestsByVariant[Constants.sharedVariantName] = manifestsByVariant[Constants.sharedVariantName] || new Manifest(module, Constants.sharedVariantName);
                        }
                        if (definition.type == null)
                        {
                            definition.type = await this.tryInferAssetType(definition);
                            if (definition.type == null)
                            {
                                Log.error(`Asset by ID '${assetID}' is missing type in definition, and was unable to infer type from other sources`);
                                ++errorCount;
                                continue;
                            }
                        }

                        this._assetDefinitions.add(definition);
                    }
                }
            }

            return { workDone: true, errorCount };
        }

        /**
         * Tries to work out an asset type from a file path.
         * @param definition
         */
        protected tryInferAssetTypeFromPath(path: string): string | null
        {
            const ext = Path.extension(path);
            const classes = InferFromExtensions.classes.filter(c => c.prototype instanceof BaseAsset);
            for (const c of classes)
            {
                const exts = InferFromExtensions.get(c);
                const assetType = AssetType.get(c);
                if (assetType != null && exts.indexOf(ext) !== -1)
                {
                    // We found a candidate
                    return assetType;
                }
            }
            return null;
        }

        /**
         * Tries to work out an asset type from an existing type-less definition.
         * @param definition
         */
        protected async tryInferAssetType(definition: AssetDefinition): Promise<string|null>
        {
            // Find all classes tagged with the InferFromExtensions decorator
            if (Is.string(definition.path))
            {
                const assetType = this.tryInferAssetTypeFromPath(definition.path);
                if (assetType != null) { return assetType; }
            }
            else
            {
                for (const key in definition.path)
                {
                    const assetType = this.tryInferAssetTypeFromPath(definition.path[key]);
                    if (assetType != null) { return assetType; }
                }
            }

            // Next, find all classes tagged with the InferCustom decorator
            const classes = InferCustom.classes.filter(c => c.prototype instanceof BaseAsset);
            for (const c of classes)
            {
                const assetType = AssetType.get(c);
                const func = InferCustom.get(c);
                if (assetType != null)
                {
                    const result = await func(definition);
                    if (result)
                    {
                        // We found a candidate
                        return assetType;
                    }
                }
            }

            return null;
        }

        /**
         * Loads the specified asset definition into an asset.
         * @param definition
         */
        protected async loadAsset(definition: AssetDefinition): Promise<BaseAsset|null>
        {
            const cls = AssetType.getClassesWithTag(definition.type);
            if (cls.length > 1)
            {
                Log.warn(`Multiple asset classes lay claim to asset type '${definition.type}'`);
            }
            else if (cls.length === 0)
            {
                return null;
            }
            const asset = new (cls[0] as any)(definition) as BaseAsset;
            Log.pushContext(definition.id);
            try
            {
                await asset.load();
            }
            finally
            {
                Log.popContext(definition.id);
            }
            return asset;
        }
    }

    export const buildAssets = new BuildAssets();
    BuildStage.register({ before: [ RS.BuildStages.compile ] }, buildAssets);
}