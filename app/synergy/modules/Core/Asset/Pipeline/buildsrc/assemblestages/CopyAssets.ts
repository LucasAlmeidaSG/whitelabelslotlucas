namespace RS.AssetPipeline.AssembleStages
{
    /**
     * Responsible for copying the built assets of modules.
     */
    export class CopyAssets extends AssembleStage.Base
    {
        protected _variants: Util.Map<{ manifest: Util.Map<AssetDefinition> }>;

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            this._variants = {};

            const assetsDir = Path.combine(settings.outputDir, Constants.outAssetsFolder);
            await super.execute(settings, moduleList);
            await FileSystem.createPath(assetsDir);

            // Write variant manifest files
            for (const variantName in this._variants)
            {
                const variantDir = Path.combine(assetsDir, variantName);
                await FileSystem.createPath(variantDir);
                await FileSystem.writeFile(Path.combine(variantDir, Constants.outManifestName), JSON.stringify(this._variants[variantName].manifest));
            }
        }

        protected async copyFile(src: string, dest: string)
        {
            await FileSystem.createPath(Path.directoryName(dest));
            await FileSystem.copyFile(src, dest);
        }

        protected async processAsset(module: Module.Base, assetID: string, assetVariant: ManifestEntry, variantName: string, outputDir: string)
        {
            const variantDir = Path.combine(outputDir, Constants.outAssetsFolder, variantName);

            // Sanitise and format path
            // NOTE: changing ' to " here results in a bad sourcemap
            const formattedPath = assetVariant.paths[0].split('\\').join('/');

            const assetDefinition: AssetDefinition =
            {
                ...assetVariant.variantData,
                id: assetVariant.id,
                type: assetVariant.type,
                group: assetVariant.group,
                path: formattedPath,
                manifestPath: undefined,
                module: module.name,
                formats: assetVariant.formats,
                exclude: assetVariant.exclude
            };

            for (const path of assetVariant.paths)
            {
                const inPath = Path.combine(module.path, "build", Constants.outAssetsFolder, variantName, path);
                const outPath = Path.combine(variantDir, path);
                await this.copyFile(inPath, outPath);
            }

            this._variants[variantName].manifest[assetID] = assetDefinition;
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // See if the module has any raw assets
            const rawAssetsPath = Path.combine(module.path, Constants.inAssetsFolder, "raw");
            try
            {
                const files = await FileSystem.readFiles(rawAssetsPath);
                const copies = files.map((f) => this.copyFile(f, Path.combine(settings.outputDir, Path.relative(rawAssetsPath, f))));
                await Promise.all(copies);
            }
            catch (err)
            {
                // Probably readFiles failed
                Log.error(err);
            }

            const assetsPath = Path.combine(module.path, "build", Constants.outAssetsFolder);

            // Gather all variants
            let variantList: string[];
            try
            {
                variantList = await FileSystem.readDirectory(assetsPath, false);
            }
            catch (err)
            {
                // Module doesn't have any assets
                return;
            }

            // Iterate all variants
            for (const variantName of variantList)
            {
                if (!this._variants[variantName]) { this._variants[variantName] = { manifest: {} }; }

                // Load the manifest
                const manifestPath = Path.combine(module.path, "build", Constants.outAssetsFolder, variantName, Constants.outManifestName);
                const manifest: Util.Map<ManifestEntry> = await JSON.parseFile(manifestPath);

                const promises: PromiseLike<void>[] = [];
                // Iterate all assets
                for (const assetID in manifest)
                {
                    const promise = this.processAsset(module, assetID, manifest[assetID], variantName, settings.outputDir);
                    promises.push(promise);
                }
                await Promise.all(promises);
            }
        }
    }

    export const copyAssets = new CopyAssets();
    AssembleStage.register({}, copyAssets);
}