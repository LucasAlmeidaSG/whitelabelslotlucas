namespace RS.AssetPipeline
{
    export function emitSimpleAssetReference(asset: BaseAsset, type: string, assetTypeOverride?: string): AssetCodeDefinition
    {
        const reference = Manifest.getAssetReference(asset.module.name, asset.id);
        const assetType = assetTypeOverride || AssetType.get(asset);
        return { 
            type:
            { 
                kind: CodeMeta.TypeRef.Kind.Raw,
                name: type
            },
            value:
            {
                kind: "objval",
                name: asset.id,
                values: [
                    { kind: "keyval", name: "id", value: `"${reference}"` },
                    { kind: "keyval", name: "group", value: `"${asset.group}"` },
                    { kind: "keyval", name: "type", value: `"${assetType}"` },
                    { kind: "keyval", name: "clone", value: `RS.Asset.clone` }
                ]
            }
        }
    }

    export function emitAssetReference(asset: BaseAsset, type: string, assetTypeOverride?: string): AssetCodeDefinition
    {
        const reference = Manifest.getAssetReference(asset.module.name, asset.id);
        const assetType = assetTypeOverride || AssetType.get(asset);
        const value: Build.CodeGeneration.ObjectValue =
        {
            kind: "objval",
            name: asset.id,
            values: [
                { kind: "keyval", name: "id", value: `"${reference}"` },
                { kind: "keyval", name: "group", value: `"${asset.group}"` },
                { kind: "keyval", name: "type", value: `"${assetType}"` },
                { kind: "keyval", name: "clone", value: `RS.Asset.clone` },
                { kind: "keyval", name: "get", value: `${type}.get` }
            ]
        };
        const intersect = Build.CodeGeneration.intersect(
        {
            kind: CodeMeta.TypeRef.Kind.Raw,
            name: type
        },
        {
            kind: CodeMeta.TypeRef.Kind.Inline,
            properties:
            [
                { name: "get", type: { kind: CodeMeta.TypeRef.Kind.Raw, name: `typeof ${type}.get` } },
            ]
        });
        return { type: intersect, value };
    }

    export function addAssetReferenceProperty(reference: AssetCodeDefinition, name: string, value: Build.CodeGeneration.Value, type: CodeMeta.TypeRef): void
    {
        if (!Is.object(reference.value) || reference.value.kind !== "objval") { throw new Error("Invalid type reference"); }
        reference.value.values.push({ kind: "keyval", name, value });

        if (reference.type.kind !== CodeMeta.TypeRef.Kind.Intersect) { throw new Error("Invalid type reference"); }
        const propObj: CodeMeta.TypeRef = reference.type.inners[1];
        if (propObj == null || propObj.kind !== CodeMeta.TypeRef.Kind.Inline) { throw new Error("Invalid type reference"); }
        propObj.properties.push({ name, type });
    }
}