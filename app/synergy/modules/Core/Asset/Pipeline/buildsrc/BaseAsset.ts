/// <reference path="AssetDefinition.ts" />
/// <reference path="Constants.ts" />

namespace RS.AssetPipeline
{
    /**
     * Associates a class with an asset type.
     */
    export const AssetType = Tag.create<string>();

    /**
     * Associates a class with one or more extensions.
     */
    export const InferFromExtensions = Tag.create<string[]>();

    /**
     * Specifies a custom function used to infer asset type.
     */
    export const InferCustom = Tag.create<(definition: AssetDefinition) => Promise<boolean>>();

    export interface AssetCodeDefinition<TValue extends Build.CodeGeneration.Value = Build.CodeGeneration.Value, TType extends CodeMeta.TypeRef = CodeMeta.TypeRef>
    {
        value: TValue;
        type: TType;
    }

    /**
     * Result of a processFormat call.
     */
    export interface ProcessFormatResult
    {
        /** Array of source files for the asset. The first entry MUST be the "root" file (e.g. the json file for a spritesheet). */
        paths: string[];
    }

    /**
     * Type of path that an asset supports.
     */
    export enum PathType { SingleOnly, MapOnly, Both }

    /**
     * Represents a single asset of a particular kind.
     */
    @AssetType("base")
    @InferFromExtensions([])
    export abstract class BaseAsset
    {
        protected _definition: AssetDefinition;
        protected _checksum: string;

        /** Gets the asset definition for this asset. */
        public get definition() { return this._definition; }

        /** Gets the asset ID for this asset. */
        public get id() { return this._definition.id; }

        /** Gets the asset group for this asset. */
        public get group() { return this._definition.group; }

        /** Gets the asset exclude list for this asset. */
        public get exclude() { return this._definition.exclude; }

        /** Gets the module to which this asset belongs. */
        public get module() { return Module.getModule(this._definition.module); }

        /** Gets the checksum for this asset's source. */
        public get checksum() { return this._checksum; }

        /** Gets a key to use in a checksum database for this asset. */
        public get checksumID() { return `asset:${this.id}`; }

        /** Gets if this asset requires processing or not. */
        public get requiresProcessing() { return this.module.checksumDB.diff(this.checksumID, this.checksum); }

        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return false; }

        /** Gets if this asset is linked via a single path or not. */
        public get supportedPathType() { return PathType.SingleOnly; }

        /** Gets the default formats for the asset, if none are specified. */
        public abstract get defaultFormats(): string[];

        /** Gets the module the asset belongs to. */
        public abstract get classModule(): string;

        public constructor(definition: AssetDefinition)
        {
            this._definition = definition;
        }

        /**
         * Loads any required metadata for this asset.
         */
        public async load()
        {
            // Compute checksum
            try
            {
                this._checksum = await this.getSourceChecksum();
            }
            catch (err)
            {
                Log.error(err.stack);
            }
        }

        /**
         * Processes all variants of this asset, producing output files.
         * @param manifests
         * @param dry
         */
        public async process(manifests: Util.Map<Manifest>, dry: boolean)
        {
            const manifestDir = Path.sanitise(Path.directoryName(this._definition.manifestPath));
            const pathType = this.supportedPathType;

            // Do we have variants?
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = { ...this._definition, ...this._definition.variants[variantName] };
                    if (variant.formats == null) { variant.formats = this._definition.formats; }
                    let assetPath: string;
                    if (Is.string(variant.path))
                    {
                        if (pathType !== PathType.SingleOnly && pathType !== PathType.Both) { throw new Error("Asset does not support single-type path but single-type path was provided"); }
                        assetPath = variant.path;
                    }
                    else
                    {
                        if (pathType !== PathType.MapOnly && pathType !== PathType.Both) { throw new Error("Asset does not support map-type path but single-type path was provided"); }
                        assetPath = this.id.replace(".", "_");
                    }
                    // Sanitise directory hierarchy
                    const outputAssetPath = Path.sanitise(assetPath);
                    const outputDir = Path.directoryName(Path.combine(this.module.path, "build", Constants.outAssetsFolder, variantName, manifestDir, outputAssetPath));
                    await FileSystem.createPath(outputDir);
                    try
                    {
                        const oldAssetData = manifests[variantName].getAsset(this.id);
                        if (variant.formats == null) { variant.formats = this.defaultFormats; }
                        if (variant.formats.length === 0) { throw new Error("No formats specified"); }
                        if (Is.string(variant.path))
                        {
                            const assetVariant = await this.processVariant(outputDir, this.getFullPath(variant.path), variant, dry, oldAssetData && oldAssetData.variantData);
                            if (assetVariant != null)
                            {
                                const variantDir = Path.sanitise(Path.directoryName(variant.path));
                                assetVariant.paths = assetVariant.paths
                                    .map(path => Path.combine(manifestDir, variantDir, Path.baseName(path)));
                                manifests[variantName].setAsset(this.id, assetVariant);
                            }
                        }
                        else
                        {
                            const fullPaths: Util.Map<string> = {};
                            for (const key in variant.path)
                            {
                                fullPaths[key] = this.getFullPath(variant.path[key]);
                            }
                            const assetVariant = await this.processVariant(outputDir, fullPaths, variant, dry, oldAssetData && oldAssetData.variantData);
                            if (assetVariant != null)
                            {
                                assetVariant.paths = assetVariant.paths
                                    .map(path => Path.combine(manifestDir, Path.baseName(path)));
                                manifests[variantName].setAsset(this.id, assetVariant);
                            }
                        }
                    }
                    catch (err)
                    {
                        Log.error(Is.string(err) ? err : err.stack);
                        continue;
                    }
                }
            }
            else
            {
                let assetPath: string;
                if (Is.string(this._definition.path))
                {
                    if (pathType !== PathType.SingleOnly && pathType !== PathType.Both) { throw new Error("Asset does not support single-type path but single-type path was provided"); }
                    assetPath = this._definition.path;
                }
                else
                {
                    if (pathType !== PathType.MapOnly && pathType !== PathType.Both) { throw new Error("Asset does not support map-type path but single-type path was provided"); }
                    assetPath = this.id.replace(".", "_");
                }
                // Sanitise directory hierarchy
                const outputAssetPath = Path.sanitise(assetPath);
                const outputDir = Path.directoryName(Path.combine(this.module.path, "build", Constants.outAssetsFolder, Constants.sharedVariantName, manifestDir, outputAssetPath));
                await FileSystem.createPath(outputDir);
                try
                {
                    const oldAssetData = manifests[Constants.sharedVariantName].getAsset(this.id);
                    const variant: AssetDefinition = { ...this._definition };
                    if (variant.formats == null) { variant.formats = this.defaultFormats; }
                    if (variant.formats.length === 0) { throw new Error("No formats specified"); }
                    if (Is.string(variant.path))
                    {
                        const assetVariant = await this.processVariant(outputDir, this.getFullPath(variant.path), variant, dry, oldAssetData && oldAssetData.variantData);
                        if (assetVariant != null)
                        {
                            const variantDir = Path.sanitise(Path.directoryName(variant.path));
                            assetVariant.paths = assetVariant.paths
                                .map(path => Path.combine(manifestDir, variantDir, Path.baseName(path)));
                            manifests[Constants.sharedVariantName].setAsset(this.id, assetVariant);
                        }
                    }
                    else
                    {
                        const fullPaths: Util.Map<string> = {};
                        for (const key in variant.path)
                        {
                            fullPaths[key] = this.getFullPath(variant.path[key]);
                        }
                        const assetVariant = await this.processVariant(outputDir, fullPaths, variant, dry, oldAssetData && oldAssetData.variantData);
                        if (assetVariant != null)
                        {
                            assetVariant.paths = assetVariant.paths
                                .map(path => Path.combine(manifestDir, Path.baseName(path)));
                            manifests[Constants.sharedVariantName].setAsset(this.id, assetVariant);
                        }
                    }
                }
                catch (err)
                {
                    Log.error(Is.string(err) ? err : err.stack);
                }
            }
        }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition | null { return null; }

        /** Gets the full path, relative to the cwd, to the asset pointed to by the specified definition path. */
        protected getFullPath(path: string): string
        {
            return Path.combine(this.module.path, Constants.inAssetsFolder, Path.directoryName(this._definition.manifestPath), path);
        }

        /** Returns a store reference for the asset pointed to by the specified ID. */
        protected getStoreReference(id: string): string
        {
            return `${this.module.name}.${id}`;
        }

        /**
         * Given the specified asset as referred to by the definition, finds all source files to be used for checksum.
         * @param path
         * @param list
         */
        protected async getSourceChecksum()
        {
            const fileList = new List<string>();

            // Add checksum dependency on our own buildsrc
            const classModule = Module.getModule(this.classModule);
            const buildDependencies = classModule.gatherDependencySet();
            for (const module of buildDependencies.data)
            {
                const buildUnit = module.getCompilationUnit("buildsrc");
                if (!buildUnit) { continue; }
                const buildSrc = buildUnit.outJSFile;
                fileList.add(buildSrc);
            }

            // Do we have variants?
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = this._definition.variants[variantName];
                    if (Is.string(variant.path))
                    {
                        await this.gatherChecksumPaths(this.getFullPath(variant.path), fileList);
                    }
                    else
                    {
                        for (const key in variant.path)
                        {
                            await this.gatherChecksumPaths(this.getFullPath(variant.path[key]), fileList);
                        }
                    }
                }
            }
            else
            {
                if (Is.string(this._definition.path))
                {
                    await this.gatherChecksumPaths(this.getFullPath(this._definition.path), fileList);
                }
                else
                {
                    for (const key in this._definition.path)
                    {
                        await this.gatherChecksumPaths(this.getFullPath(this._definition.path[key]), fileList);
                    }
                }
            }

            return Checksum.getComposite(fileList.dataCopy, this._definition);
        }

        /** Given the specified asset as referred to by the definition, finds all source files to be used for checksum. */
        protected async gatherChecksumPaths(path: string, list: List<string>)
        {
            // By default, only consider the given file for checksum
            // Custom assets may extend this behaviour (in particular assets made out of multiple files, like spritesheets)
            list.add(path);
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         */
        protected async processVariant(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, oldData?: object): Promise<ManifestEntry | null>
        {
            const variantData = { ...variant };
            delete variantData.formats;

            const cfg = ModuleConfig.get();
            const formats = variant.formats.filter(f => cfg.blacklistFormats.indexOf(f) === -1);

            const result: ManifestEntry =
            {
                id: this.id,
                type: AssetType.get(this),
                group: this.group,
                variantData,
                paths: [],
                formats,
                exclude: this.exclude
            };

            const promises: Promise<ProcessFormatResult>[] = [];
            for (const format of formats)
            {
                promises.push(this.processFormat(outputDir, assetPath, variant, dry, format));
            }
            for (const promise of promises)
            {
                const formatResult = await promise;
                if (formatResult == null) { return null; }
                for (const path of formatResult.paths)
                {
                    result.paths.push(path);
                }
            }

            if (!dry) { this.module.checksumDB.set(this.checksumID, this.checksum); }

            return result;
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected abstract processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, format: string): Promise<ProcessFormatResult | null>;
    }
}
