namespace RS.AssetPipeline.ImageRequirements
{
    export const imageMagick: CommandRequirement = { oneOf: ["magick", "convert"], error: "Could not find command 'magick' or 'convert'. Please check that ImageMagick is installed correctly." };
    export const pngQuant: CommandRequirement = { command: "pngquant", error: "Could not find command 'pngquant'. Please check that PNG Quant is installed correctly." };
    export const gzip: CommandRequirement = { command: "gzip", error: "Could not find command 'gzip'. Please check that gzip is installed correctly." };
    export const texturePacker: CommandRequirement = { command: "texturepacker", error: "Could not find command 'texturepacker'. Please check that TexturePacker is installed correctly, along with its CLI executable. See https://www.codeandweb.com/texturepacker/documentation/installation-and-licensing." };
}