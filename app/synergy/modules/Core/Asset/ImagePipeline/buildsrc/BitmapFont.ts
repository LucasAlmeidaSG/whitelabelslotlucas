/// <reference path="Image.ts" />
/// <reference path="Requirements.ts" />

namespace RS.AssetPipeline
{
    /**
     * Bitmap font asset specific definition data.
     */
    export interface FontDefinition extends BaseVariant
    {
        xAdvance?: number;
        fixedWidth?: number;
        signedDistanceField?: boolean;
        expand?: number;
    }

    // Export format
    interface BMChar
    {
        id: number;
        x: number;
        y: number;
        width: number;
        height: number;
        xOffset: number;
        yOffset: number;
        xAdvance: number;
        kerning: { [charCode: number]: number };
        pageIndex: number;
    }
    interface BMFont
    {
        font: string;
        size: number;
        lineHeight: number;
        pages: string[];
        chars: BMChar[];
        sdf: boolean;
    }

    /**
     * Represents a bitmap font asset.
     */
    @AssetType("bitmapfont")
    @InferFromExtensions([".fnt"])
    @RequireCommands([ ImageRequirements.imageMagick, ImageRequirements.pngQuant ])
    export class BitmapFont extends Image
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "png" ]; }

        private _fontData: BMFont;
        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitSimpleAssetReference(this, "RS.Asset.BitmapFontReference");
        }

        /**
         * Loads any required metadata for this asset.
         */
        public async load()
        {
            await super.load();

            // Search variants, try to find a fnt file to load
            let path: string;
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = this._definition.variants[variantName];
                    if (!Is.string(variant.path)) { throw new Error("Expected single-type assetPath"); }
                    path = this.getFullPath(variant.path);
                    break;
                }
            }
            else
            {
                if (!Is.string(this._definition.path)) { throw new Error("Expected single-type assetPath"); }
                path = this.getFullPath(this._definition.path);
            }

        }

        /**
         * Given the specified asset as referred to by the definition, finds all source files to be used for checksum.
         * @param path
         * @param list
         */
        protected async gatherChecksumPaths(path: string, list: List<string>)
        {
            // Load up the fnt
            const fontData = await FileSystem.readFile(path);

            // Add all images to the list
            for (const line of fontData.split("\n"))
            {
                const match = (/page id=[0-9]+ file="([a-zA-Z0-9_-]+\.[a-zA-Z]+)"/g).exec(line);
                if (match && match.length > 0)
                {
                    const imagePath = Path.combine(Path.directoryName(path), match[1]);
                    list.add(imagePath);
                }

            }

            // Add the fnt itself to the list
            list.add(path);
        }

        /**
         * Processes a single format for a single asset variant.
         * Note that the format might be passed as null here if no format is specified.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: FontDefinition, dry: boolean, format: string | null): Promise<ProcessFormatResult>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            const data = await FileSystem.readFile(assetPath);
            const fontData = {} as BMFont;

            // Property extraction utils
            const typeValues =
            {
                "string": "\"[a-zA-Z0-9\\s_\\-\\.]*\"",
                "number": "(-?)[0-9]+",
                "boolean": "(0|1)",
                "char": "\".\"",
                "number[]": "[\\-0-9,]+"
            };
            function extractProperty(line: string, key: string, type: "string"|"char", noErr?: boolean): string;
            function extractProperty(line: string, key: string, type: "number", noErr?: boolean): number;
            function extractProperty(line: string, key: string, type: "boolean", noErr?: boolean): boolean;
            function extractProperty(line: string, key: string, type: "number[]", noErr?: boolean): number[];
            function extractProperty(line: string, key: string, type: "string"|"number"|"boolean"|"char"|"number[]", noErr: boolean = false): any
            {
                if (type === "char") { line = line.replace("\"space\"", "\" \""); }
                const regex = new RegExp(`${key}=${typeValues[type]}`, "g");
                const match = line.match(regex);
                if (!match || match.length === 0)
                {
                    if (!noErr) { Log.warn(`Match failed (${key}): ${line}`); }
                    return null;
                }
                const rawVal = match[0].split("=")[1];
                switch (type)
                {
                    case "string":
                    case "char":
                        return rawVal.substr(1, rawVal.length - 2);
                    case "number":
                        return parseInt(rawVal);
                    case "number[]":
                        return rawVal.split(",").map(str => parseInt(str));
                    case "boolean":
                        return parseInt(rawVal) === 1;
                }
            }

            // Parse it
            const lines = data.split(/(\n|\r|\n\r|\r\n)/g);
            let charID = 0;
            for (const line of lines)
            {
                const firstSpace = line.indexOf(" ");
                const tagName = line.substr(0, firstSpace);
                switch (tagName)
                {
                    case "info":

                        fontData.font = extractProperty(line, "font", "string", true) || extractProperty(line, "face", "string");
                        fontData.size = extractProperty(line, "size", "number");
                        break;

                    case "common":

                        fontData.lineHeight = extractProperty(line, "lineHeight", "number");
                        fontData.pages = new Array<string>(extractProperty(line, "pages", "number"));
                        break;

                    case "page":

                        const id = extractProperty(line, "id", "number");
                        const path = extractProperty(line, "file", "string");
                        fontData.pages[id] = path;
                        break;

                    case "chars":

                        fontData.chars = [];
                        charID = 0;
                        break;

                    case "char":

                        const expand = variant.expand || 0;

                        const charCode = extractProperty(line, "letter", "char").charCodeAt(0);
                        const charData: BMChar =
                        {
                            id: extractProperty(line, "letter", "char").charCodeAt(0),
                            x: extractProperty(line, "x", "number") - expand,
                            y: extractProperty(line, "y", "number") - expand,
                            width: extractProperty(line, "width", "number") + expand * 2.0,
                            height: extractProperty(line, "height", "number") + expand * 2.0,
                            xOffset: extractProperty(line, "xoffset", "number"),
                            yOffset: extractProperty(line, "yoffset", "number"),
                            xAdvance: extractProperty(line, "xadvance", "number"),
                            pageIndex: extractProperty(line, "page", "number"),
                            kerning: {}
                        };
                        if (variant.fixedWidth) { charData.xAdvance = variant.fixedWidth; }
                        if (variant.xAdvance) { charData.xAdvance += variant.xAdvance; }
                        fontData.chars.push(charData);
                        break;
                }
            }
            fontData.sdf = !!variant.signedDistanceField;
            if (variant.xAdvance != null) { delete variant.xAdvance; }
            if (variant.fixedWidth != null) { delete variant.fixedWidth; }

            if (!dry)
            {
                // Process pages
                for (const pageName of fontData.pages)
                {
                    const inputPath = Path.combine(Path.directoryName(assetPath), pageName);
                    const outputPath = Path.combine(outputDir, Path.baseName(inputPath));

                    if (variant.signedDistanceField)
                    {
                        if (format != "png")
                        {
                            // Only support png for now
                            Log.error(`Format '${format}' does not support signed distance field fonts`);
                            return null;
                        }
                        else
                        {
                            const args = "-quiet -filter Jinc -resize 400% -threshold 30% \( +clone -negate -morphology Distance Euclidean -level 50%,-50% \) -morphology Distance Euclidean -compose Plus -composite -level 45%,55% -resize 25%".split(" ");
                            args.unshift(inputPath);
                            args.push(outputPath);

                            const command = await resolveRequirement(ImageRequirements.imageMagick);
                            await Command.run(command, args);
                        }
                    }
                    else
                    {
                        if (!await this.processSpecificFormat(inputPath, outputPath, format, variant)) { return null; }
                    }
                }

                // Write font data as json
                await FileSystem.writeFile(Path.combine(outputDir, Path.baseName(assetPath)).replace(".fnt", ".json"), JSON.stringify(fontData));
            }

            // Return variant data
            return {
                paths: [ assetPath.replace(".fnt", ".json"), ...fontData.pages ]
            };
        }
    }
}