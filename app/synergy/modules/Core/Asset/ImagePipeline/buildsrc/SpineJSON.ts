// http://esotericsoftware.com/spine-json-format
// This is here because thirdparty doesn't work in buildsrc and wouldn't be easy to add

declare namespace Esoteric
{
    interface Spine
    {
        skeleton: Spine.Skeleton;
        bones?: Spine.Bone[];
        slots?: Spine.Slot[];
        /** Inverse kinematics. */
        ik?: Spine.InverseKinematicConstraint[];
        transform?: Spine.TransformConstraint[];
        path?: Spine.PathConstraint[];
        skins?: Spine.Skin[];
        events?: Spine.Event[];
        animations?: { [name: string]: Spine.Animation; };
    }

    namespace Spine
    {
        interface Skeleton
        {
            hash: string;
            spine: string;

            x: number;
            y: number;
            width: number;
            height: number;

            fps?: number;
            images?: string;
            audio?: string;
        }

        interface Bone
        {
            name: string;

            length?: number;
            transform?: string;
            skin?: boolean;
            x?: number;
            y?: number;
            rotation?: number;
            scaleX?: number;
            scaleY?: number;
            shearX?: number;
            shearY?: number;
            color?: string;
        }

        interface Slot
        {
            name: string;
            bone: string;

            color?: string;
            dark?: string;
            attachment?: string;
            blend?: string;
        }

        interface BaseConstraint
        {
            name: string;
            order: number;
            skin?: boolean;
            bones: string[];
            target: string;
        }

        interface InverseKinematicConstraint extends BaseConstraint
        {
            mix?: number;
            softness?: number;
            bendPositive?: boolean;
            compress?: boolean;
            stretch?: boolean;
            uniform?: boolean;
        }

        interface TransformConstraint extends BaseConstraint
        {
            rotation?: number;
            x?: number;
            y?: number;

            scaleX?: number;
            scaleY?: number;
            shearX?: number;
            shearY?: number;

            rotateMix?: number;
            translateMix?: number;
            scaleMix?: number;
            shearMix?: number;

            local?: boolean;
            relative?: boolean;
        }

        interface PathConstraint extends BaseConstraint
        {
            positionMode?: string;
            spacingMode?: string;
            rotateMode?: string;

            rotation?: number;
            position?: number;
            spacing?: number;

            rotateMix?: number;
            translateMix?: number;
        }

        interface Skin
        {
            name: string;
            attachments: { [slot: string]: { [name: string]: Attachment; }; };
        }

        interface BaseAttachment
        {
            type?: string;
            name?: string;
        }

        interface RegionAttachment extends BaseAttachment
        {
            type?: "region";
            path?: string;

            x?: number;
            y?: number;
            scaleX?: number;
            scaleY?: number;
            rotation?: number;

            width: number;
            height: number;

            color?: string;
        }

        interface MeshAttachment extends BaseAttachment
        {
            type: "mesh";
            path?: string;

            uvs: number[];
            triangles: number[];
            vertices: number[];
            hull: number;
            edges?: number[];

            color?: string;
            width?: number;
            height?: number;
        }

        interface LinkedMeshAttachment extends BaseAttachment
        {
            type: "linkedmesh";
            path?: string;

            skin?: string;
            parent: string;
            deform?: boolean;

            color?: string;
            width?: number;
            height?: number;
        }

        interface BoundingBoxAttachment extends BaseAttachment
        {
            type: "boundingbox";

            vertexCount: number;
            vertices: number[];
            color?: string;
        }

        interface PathAttachment extends BaseAttachment
        {
            type: "path";

            closed?: boolean;
            constantSpeed?: boolean;
            lengths: number[];

            vertexCount: number;
            vertices: number[];
            color?: string;
        }

        interface PointAttachment extends BaseAttachment
        {
            type: "point",

            x?: number;
            y?: number;
            rotation?: number;
            color?: string;
        }

        interface ClippingAttachment extends BaseAttachment
        {
            type: "clipping",

            end: string;
            vertexCount: number;
            vertices: number[];
            color?: string;
        }

        type Attachment = RegionAttachment | MeshAttachment | LinkedMeshAttachment | BoundingBoxAttachment | PathAttachment | PointAttachment | ClippingAttachment;

        interface Event
        {
            name: string;

            int?: number;
            float?: number;
            string?: string;
            audio?: string;
            volume?: number;
            balance?: number;
        }

        interface Animation
        {
            bones?: { [boneName: string]: BoneTimeline };
            slots?: { [slotName: string]: SlotTimeline };
            /** Inverse kinematics. */
            ik?: { [constraintName: string]: InverseKinematicsConstraintKeyframe[] };
            transform?: { [constraintName: string]: TransformConstraint[] };
            path?: { [constraintName: string]: PathConstraintTimeline };
            deform?: { [skinName: string]: { [slotName: string]: { [meshName: string]: DeformKeyframe[] } } };
            event?: EventKeyframe[];
            draworder?: DrawOrderKeyframe[];
        }

        interface BaseKeyframe
        {
            time?: number;
        }

        interface CurvableKeyframe
        {
            curve?: number | "stepped";
            c2?: number;
            c3?: number;
            c4?: number;
        }

        interface BoneTimeline
        {
            rotate?: BoneRotateKeyframe[];
            translate?: BoneTranslateKeyframe[];
            scale?: BoneScaleKeyframe[];
            shear?: BoneShearKeyframe[];
        }

        interface PathConstraintTimeline
        {
            position?: PathConstraintKeyframe[];
            spacing?: PathConstraintKeyframe[];
            mix?: PathConstraintKeyframe[];
        }

        /* tslint:disable-next-line */
        interface BaseBoneKeyframe extends CurvableKeyframe, BaseKeyframe
        {
        }

        interface BoneRotateKeyframe extends BaseBoneKeyframe
        {
            angle?: number;
        }

        interface BoneTranslateKeyframe extends BaseBoneKeyframe
        {
            x?: number;
            y?: number;
        }

        interface BoneScaleKeyframe extends BaseBoneKeyframe
        {
            x?: number;
            y?: number;
        }

        interface BoneShearKeyframe extends BaseBoneKeyframe
        {
            x?: number;
            y?: number;
        }

        interface SlotTimeline
        {
            attachment?: SlotAttachmentKeyframe[];
            color?: SlotColorKeyframe[];
        }

        /* tslint:disable-next-line */
        interface BaseSlotKeyframe extends BaseKeyframe
        {
        }

        interface SlotAttachmentKeyframe extends BaseSlotKeyframe
        {
            name: string;
        }

        interface SlotColorKeyframe extends BaseSlotKeyframe, CurvableKeyframe
        {
            color: string;
        }

        interface InverseKinematicsConstraintKeyframe extends BaseKeyframe
        {
            mix?: number;
            softness?: number;
            bendPositive?: boolean;
            compress?: boolean;
            stretch?: boolean;
        }

        interface TransformConstraintKeyframe extends BaseKeyframe
        {
            rotateMix?: number;
            translateMix?: number;
            scaleMix?: number;
            shearMix?: number;
        }

        interface PathConstraintKeyframe extends BaseKeyframe
        {
            position?: number;
            spacing?: number;
            rotateMix?: number;
            translateMix?: number;
        }

        interface DeformKeyframe extends BaseKeyframe, CurvableKeyframe
        {
            offset?: number;
            vertices: number[];
        }

        interface EventKeyframe extends BaseKeyframe
        {
            name: string;

            int?: number;
            float?: number;
            string?: string;
            volume?: number;
            balance?: number;
        }

        interface DrawOrderKeyframe extends BaseKeyframe
        {
            offsets: DrawOrderOffset[];
        }

        interface DrawOrderOffset
        {
            slot: string;
            offset: number;
        }
    }
}