/// <reference path="Image.ts" />
/// <reference path="Requirements.ts" />

namespace RS.AssetPipeline
{
    /**
     * Image asset specific definition data.
     */
    export interface SpriteSheetDefinition extends ImageDefinition
    {
        /** If animation we convert SpriteSheet json to RSSpriteSheet json */
        isAnimation?: boolean;
    }

    /**
     * Given an asset definition, determines if it can be inferred as a spritesheet.
     * @param definition
     */
    async function InferSpritesheet(definition: AssetPipeline.AssetDefinition)
    {
        // Spritesheets don't support map-type paths
        if (!Is.string(definition.path)) { return false; }

        // First, check the extension
        if (Path.extension(definition.path) !== ".json") { return false; }

        if (definition.variants)
        {
            for (const variantName in definition.variants)
            {
                const variant = definition.variants[variantName];

                // Spritesheets don't support map-type paths
                if (!Is.string(variant.path)) { return false; }

                const assetPath = Path.combine(Module.getModule(definition.module).path, Constants.inAssetsFolder, Path.directoryName(definition.manifestPath), variant.path);
                try
                {
                    // Next, load it and look for spritesheet signature
                    const parsed = await JSON.parseFile(assetPath);
                    return Is.object(parsed.meta) &&
                        Is.arrayOf(parsed.frames, Is.object) &&
                        !(definition as SpriteSheetDefinition).isAnimation; //null or false
                    //return Is.arrayOf(parsed.images, Is.string) && Is.arrayOf(parsed.frames, Is.array) && Is.object(parsed.animations);
                }
                catch (err)
                {
                    // Something went wrong, just return false
                    return false;
                }
            }
        }
        else
        {
            const assetPath = Path.combine(Module.getModule(definition.module).path, Constants.inAssetsFolder, Path.directoryName(definition.manifestPath), definition.path);
            try
            {
                // Next, load it and look for spritesheet signature
                const parsed = await JSON.parseFile(assetPath);
                return Is.object(parsed.meta) &&
                    Is.arrayOf(parsed.frames, Is.object) &&
                    !(definition as SpriteSheetDefinition).isAnimation; //null or false
            }
            catch (err)
            {
                // Something went wrong, just return false
                return false;
            }
        }

        return false;
    }

    interface SpriteSheetData
    {
        frames: SpriteSheetFrame[];
        meta:
        {
            app: string;
            version: string;
            image: string;
            format: string;
            size: {
                w:number,
                h:number
            };
            scale: number;
            smartupdate: string;
        };
    }

    interface SpriteSheetFrame
    {
        filename: string;
        frame: {
            x:number,
            y:number,
            w:number,
            h:number
        };
        rotated: boolean;
        trimmed: boolean;
        spriteSourceSize: {
            x:number,
            y:number,
            w:number,
            h:number
        };
        sourceSize: {
            w:number,
            h:number
        };
        pivot: {
            x:number,
            y:number
        };
    }

    /**
     * Represents an image-type asset.
     */
    @AssetType("spritesheet")
    @RequireCommands([ ImageRequirements.imageMagick, ImageRequirements.pngQuant ])
    @InferCustom(InferSpritesheet)
    export class SpriteSheet extends Image
    {
        private _spriteSheetData: SpriteSheetData|null = null;

        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /**
         * Loads any required metadata for this asset.
         */
        public async load()
        {
            await super.load();

            // Search variants, try to find a json file to load
            let path: string;
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = this._definition.variants[variantName];
                    if (!Is.string(variant.path)) { throw new Error("Expected single-type assetPath"); }
                    path = this.getFullPath(variant.path);
                    break;
                }
            }
            else
            {
                if (!Is.string(this._definition.path)) { throw new Error("Expected single-type assetPath"); }
                path = this.getFullPath(this._definition.path);
            }
            this._spriteSheetData = await JSON.parseFile(path);
        }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitAssetReference(this, "RS.Asset.SpriteSheetReference");
        }

        /**
         * Given the specified asset as referred to by the definition, finds all source files to be used for checksum.
         * @param path
         * @param list
         */
        protected async gatherChecksumPaths(path: string, list: List<string>)
        {
            // Load up the json
            const spriteSheetData: SpriteSheetData = await JSON.parseFile(path);

            // Add all images to the list
            const imagePath = Path.combine(Path.directoryName(path), spriteSheetData.meta.image);
            list.add(imagePath);

            // Add the json itself to the list
            list.add(path);
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         */
        protected async processVariant(outputDir: string, assetPath: string, variant: SpriteSheetDefinition, dry: boolean): Promise<ManifestEntry | null>
        {
            // Load the sprite sheet data
            this._spriteSheetData = await JSON.parseFile(assetPath);

            // Process formats
            const baseResult = await super.processVariant(outputDir, assetPath, variant, dry);
            if (baseResult == null) { return null; }

            const mipmap = as(variant.mipmap, Is.boolean);
            if (mipmap != null) { (baseResult.variantData as SpriteSheetDefinition).mipmap = mipmap; }

            if (!dry)
            {
                // Strip out extensions
                this._spriteSheetData.meta.image = Path.replaceExtension(this._spriteSheetData.meta.image, "");
                //this._spriteSheetData.images = this._spriteSheetData.images.map(path => Path.replaceExtension(path, ""));

                // Write out the json
                await FileSystem.writeFile(Path.combine(outputDir, Path.baseName(assetPath)), JSON.stringify(this._spriteSheetData));
            }

            baseResult.paths.unshift(assetPath);

            return baseResult;
        }

        /**
         * Processes a single format for a single asset variant.
         * Note that the format might be passed as null here if no format is specified.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string, variant: SpriteSheetDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            const shouldGzip = this.shouldGzip(format);

            if (!dry)
            {
                // Iterate the images
                    const imageName = this._spriteSheetData.meta.image;

                    const inputPath = Path.combine(Path.directoryName(assetPath), imageName);
                    const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.baseName(inputPath)), `.${format}`);

                    const oldRes = variant.res;
                    variant.res = 1.0;

                    if (!await this.processSpecificFormat(inputPath, outputPath, format, variant)) { return null; }

                    if (shouldGzip)
                    {
                        // Produce gz version
                        await this.gzip(outputPath);
                    }

                    variant.res = oldRes;
            }
            const paths = [Path.replaceExtension(this._spriteSheetData.meta.image, `.${format}`)];
            if (shouldGzip)
            {
                return { paths: [ ...paths, ...paths.map(p => Path.replaceExtension(p, `${Path.extension(p)}.gz`)) ] };
            }
            else
            {
                return { paths };
            }
        }
    }
}