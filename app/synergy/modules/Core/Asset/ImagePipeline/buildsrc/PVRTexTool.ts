namespace RS.AssetPipeline.ImageUtils
{
    export enum PVRTexFormat { R8G8B8A8, PVRTC1_2, PVRTC1_4, PVRTC1_2_RGB, PVRTC1_4_RGB, PVRTC2_2, PVRTC2_4, ETC1, UYVY, YUY2, RGBE9995, RGBG8888, GRGB8888, ETC2_RGB, ETC2_RGBA, ETC2_RGB_A1, EAC_R11, EAC_RG11, ASTC_4x4, ASTC_5x4, ASTC_5x5, ASTC_6x5, ASTC_6x6, ASTC_8x5, ASTC_8x6, ASTC_8x8, ASTC_10x5, ASTC_10x6, ASTC_10x8, ASTC_10x10, ASTC_12x10, ASTC_12x12, ASTC_3x3x3, ASTC_4x3x3, ASTC_4x4x3, ASTC_4x4x4, ASTC_5x4x4, ASTC_5x5x4, ASTC_5x5x5, ASTC_6x5x5, ASTC_6x6x5, ASTC_6x6x6 }
    export enum PVRTexQuality { pvrtcfastest, pvrtcfast, pvrtcnormal, pvrtchigh, pvrtcbest, etcfast, etcslow, etcfastperceptual, etcslowperceptual, astcveryfast, astcfast, astcmedium, astcthorough, astcexhaustive }

    export interface PVRTexToolArgs
    {
        inputPath: string;
        outputPath: string;
        format: PVRTexFormat;
        quality?: PVRTexQuality;
    }

    const thisModule = Module.getModule("Core.Asset.ImagePipeline");
    const binPath = Path.combine(thisModule.path, "build_tools", process.platform, `PVRTexToolCLI${process.platform === "win32" ? ".exe" : ""}`);

    export async function PVRTexTool(args: PVRTexToolArgs)
    {
        const argArr: string[] = [];
        argArr.push("-i", args.inputPath);
        argArr.push("-o", args.outputPath);
        argArr.push("-f", PVRTexFormat[args.format]);
        argArr.push("-shh");
        if (args.quality != null) { argArr.push("-q", PVRTexQuality[args.quality]); }

        await FileSystem.createPath(Path.directoryName(args.outputPath));

        const outputText = await Command.run(binPath, argArr, null, true);

        FileSystem.cache.set(args.outputPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
    }
}