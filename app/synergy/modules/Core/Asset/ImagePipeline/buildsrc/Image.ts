/// <reference path="Requirements.ts" />

namespace RS.AssetPipeline
{
    export interface FormatSettings
    {
        ["crn"]?:
        {
            pixelFormat?: string;
        };
        ["pvr"]?:
        {

        };
    }

    /**
     * Image asset specific definition data.
     */
    export interface ImageDefinition extends BaseVariant
    {
        /** The scale of this image variant in video memory. Only affects quality, not apparent size. */
        res?: number;
        /** The scale of this image variant. Affects apparent size. */
        scale?: number;

        /** Optional trim threshold (0-255). Defaults to 0 (no trimming). */
        trimThreshold?: number;

        usequant?: boolean;
        formatSettings?: FormatSettings;
        wrapMode?: string;
        mipmap?: boolean;

        /** Minimum image quality. Will use the original if quality is less than this value. Defaults to 0. */
        qualityMin?: number;
        /** Maximum image quality. Will reduce colours if quality is higher than this value. Defaults to 100. */
        qualityMax?: number;
        /** Speed/quality trade-off. 1=slow, 3=default, 11=fast & rough. */
        speed?: number;
    }

    /**
     * Represents an image-type asset.
     */
    @AssetType("image")
    @InferFromExtensions([".png", ".jpg", ".jpeg", ".tga", ".bmp", ".svg"])
    @RequireCommands([ ImageRequirements.imageMagick, ImageRequirements.pngQuant, ImageRequirements.gzip ])
    export class Image extends BaseAsset
    {
        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /** Gets the default formats for the asset, if none are specified. */
        public get defaultFormats() { return [ "png" ]; }

        public get classModule() { return "Core.Asset.ImagePipeline"; }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition|null
        {
            return AssetPipeline.emitAssetReference(this, "RS.Asset.ImageReference");
        }

        /** Gets if we should provide a .gz version for the specified format. */
        protected shouldGzip(format: string)
        {
            return format === "pvr";
        }

        protected async gzip(path: string)
        {
            await Command.run("gzip",
            [
                "-k", // keep (don't delete) input files
                "-f", // force overwrite of output file and compress links
                "-9", // compress better
                path
            ], undefined, false);
            const gzPath = Path.replaceExtension(path, `${Path.extension(path)}.gz`);
            FileSystem.cache.set(gzPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         */
        protected async processVariant(outputDir: string, assetPath: string | Util.Map<string>, variant: ImageDefinition, dry: boolean, oldData?: object): Promise<ManifestEntry | null>
        {
            const mipmap = as(variant.mipmap, Is.boolean);
            const wrapMode = variant.wrapMode || (this._definition as ImageDefinition).wrapMode;
            if (wrapMode != null && wrapMode !== "clamp" && wrapMode !== "repeat")
            {
                Log.warn(`Invalid wrap mode '${wrapMode}'`, this.id);
            }
            const result = await super.processVariant(outputDir, assetPath, variant, dry, oldData);
            if (wrapMode != null) { (result.variantData as ImageDefinition).wrapMode = wrapMode; }
            if (mipmap != null) { (result.variantData as ImageDefinition).mipmap = mipmap; }
            return result;
        }

        /**
         * Processes a single format for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string | Util.Map<string>, variant: BaseVariant, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            if (!Is.string(assetPath)) { throw new Error("Expected single-type assetPath"); }

            const shouldGzip = this.shouldGzip(format);
            if (!dry)
            {
                const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath))), `.${format}`);
                if (!await this.processSpecificFormat(assetPath, outputPath, format, variant)) { return null; }
                if (shouldGzip)
                {
                    // Produce gz version
                    await this.gzip(outputPath);
                }
            }

            // Deal with extension
            assetPath = Path.replaceExtension(Path.sanitise(assetPath), `.${format}`);

            // Return variant data
            if (shouldGzip)
            {
                const gzPath = Path.replaceExtension(assetPath, `${Path.extension(assetPath)}.gz`);
                return {
                    paths: [ assetPath, gzPath ]
                };
            }
            else
            {
                return {
                    paths: [ assetPath ]
                };
            }
        }

        protected getFormatSettings<TKey extends keyof FormatSettings>(key: TKey, variant: ImageDefinition): FormatSettings[TKey]|null
        {
            const formatSettingsMap = variant.formatSettings || (this._definition as ImageDefinition).formatSettings;
            return formatSettingsMap ? formatSettingsMap[key] : null;
        }

        protected async processSpecificFormat(inputPath: string, outputPath: string, format: string, variant: ImageDefinition)
        {
            // Delete to ensure case is matched
            await this.tryDeletePath(outputPath);

            switch (format)
            {
                case "png":
                    if (!await this.processPNG(inputPath, outputPath, variant)) { return false; }
                    break;
                case "jpg":
                    if (!await this.processJPEG(inputPath, outputPath, variant)) { return false; }
                    break;
                case "jpeg":
                    if (!await this.processJPEG(inputPath, outputPath, variant)) { return false; }
                    break;
                case "pvr":
                    if (!await this.processPVR(inputPath, outputPath, variant)) { return false; }
                    break;
                case "crn":
                    if (!await this.processDXT(inputPath, outputPath, variant)) { return false; }
                    break;
                default:
                    Log.error(`Unknown format '${format}'`);
                    return false;
            }
            return true;
        }

        protected getVariantScale(variant: ImageDefinition)
        {
            let scale = 1;
            if (variant.res != null) { scale *= variant.res; }
            if (variant.scale != null) { scale *= variant.scale; }
            return scale;
        }

        /** Converts trim threshold from 0-255 to a fuzziness percentage for ImageMagick. */
        protected trimThresholdToPercentage(threshold: number): number
        {
            return 100 * (threshold / 255);
        }

        protected async processPNG(inputPath: string, outputPath: string, variant: ImageDefinition)
        {
            // Resize image into the destination folder
            const scale = this.getVariantScale(variant);
            await FileSystem.createPath(Path.directoryName(outputPath));
            await ImageUtils.resize(inputPath, outputPath, scale, this.trimThresholdToPercentage(variant.trimThreshold));

            // Quant it in-place
            if (variant.usequant !== false)
            {
                const options: ImageUtils.QuantOptions = {};

                options.quality = [0, 100];
                if (variant.qualityMin != null)
                {
                    options.quality[0] = variant.qualityMin;
                }
                if (variant.qualityMax != null)
                {
                    options.quality[1] = variant.qualityMax;
                }

                if (variant.speed != null)
                {
                    options.speed = variant.speed;
                }

                await ImageUtils.quantize(outputPath, options);
            }

            // Success
            return true;
        }

        protected async processJPEG(inputPath: string, outputPath: string, variant: ImageDefinition)
        {
            // Resize image into the destination folder
            const scale = this.getVariantScale(variant);
            await FileSystem.createPath(Path.directoryName(outputPath));
            await ImageUtils.resize(inputPath, outputPath, scale, this.trimThresholdToPercentage(variant.trimThreshold));

            // Success
            return true;
        }

        protected async processPVR(inputPath: string, outputPath: string, variant: ImageDefinition)
        {
            const tmpPath = Path.replaceExtension(outputPath, "_tmp_pvr.png");

            const formatSettings = this.getFormatSettings("pvr", variant);

            // Resize image into the destination folder
            const scale = this.getVariantScale(variant);
            await FileSystem.createPath(Path.directoryName(outputPath));
            await ImageUtils.resize(inputPath, tmpPath, scale, this.trimThresholdToPercentage(variant.trimThreshold));

            // Check power of 2
            const info = await ImageUtils.identify(tmpPath);
            if (info != null)
            {
                const powW = Math.log(info.width) / Math.log(2);
                const powH = Math.log(info.height) / Math.log(2);
                if ((powW|0) !== powW || (powH|0) !== powH)
                {
                    Log.error(`Image width or height is not a power of 2 (got '${info.width}x${info.height}'), PVR format not supported`, this.id);
                    await FileSystem.deletePath(tmpPath);
                    return false;
                }
                if (info.width !== info.height)
                {
                    Log.error(`Image is not square (got '${info.width}x${info.height}'), PVR format not supported`, this.id);
                    await FileSystem.deletePath(tmpPath);
                    return false;
                }
            }

            // Compress
            await ImageUtils.PVRTexTool({
                inputPath: tmpPath,
                outputPath,
                format: ImageUtils.PVRTexFormat.PVRTC1_4,
                quality: ImageUtils.PVRTexQuality.pvrtcnormal
            });

            // Delete png
            await FileSystem.deletePath(tmpPath);

            // Success
            return true;
        }

        protected async processDXT(inputPath: string, outputPath: string, variant: ImageDefinition)
        {
            const tmpPath = Path.replaceExtension(outputPath, "_tmp_crn.png");

            const formatSettings = this.getFormatSettings("crn", variant);

            // Resize image into the destination folder
            const scale = this.getVariantScale(variant);
            await FileSystem.createPath(Path.directoryName(outputPath));
            await ImageUtils.resize(inputPath, tmpPath, scale, this.trimThresholdToPercentage(variant.trimThreshold));

            // Check power of 2
            const info = await ImageUtils.identify(tmpPath);
            if (info != null)
            {
                const powW = Math.log(info.width) / Math.log(2);
                const powH = Math.log(info.height) / Math.log(2);
                if ((powW|0) !== powW || (powH|0) !== powH)
                {
                    Log.warn(`Image width or height is not a power of 2 (got '${info.width}x${info.height}'), DXT format may not load in all environments`, this.id);
                }
            }

            let pixelFormat = ImageUtils.CrunchPixelFormat.DXT5;
            if (formatSettings != null && formatSettings.pixelFormat != null)
            {
                pixelFormat = ImageUtils.CrunchPixelFormat.parse(formatSettings.pixelFormat);
                if (pixelFormat == null)
                {
                    Log.error(`Unknown pixel format '${formatSettings.pixelFormat}'`);
                    return false;
                }
            }

            // Compress
            await ImageUtils.crunch({
                inputPath: tmpPath,
                outputPath,
                fileFormat: ImageUtils.CrunchFileFormat.crn,
                pixelFormat,
                mipMaps: false,
                quality: 128
            });

            // Delete png
            await FileSystem.deletePath(tmpPath);

            // Success
            return true;
        }

        protected async tryDeletePath(path: string)
        {
            if (await FileSystem.fileExists(path))
            {
                await FileSystem.deletePath(path);
            }
        }
    }
}