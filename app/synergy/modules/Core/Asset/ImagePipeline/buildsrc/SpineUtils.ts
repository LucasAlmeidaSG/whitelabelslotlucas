namespace RS.AssetPipeline.SpineUtils
{
    // http://esotericsoftware.com/spine-atlas-format

    /* tslint:disable:no-string-literal */

    export interface Atlas
    {
        pages: Page[];
    }

    export interface Page
    {
        name: string;
        size: [number, number];
        format: string;
        filter: string[];
        repeat: string;
        items: { [name: string]: Item };
    }

    export interface Item
    {
        rotate: boolean;
        xy: [number, number];
        size: [number, number];
        orig: [number, number];
        offset: [number, number];
        index: number;

        /**
         * Nine-patch border size.
         *
         * Defines a 3x3 grid for a resizing an image without stretching all parts of the image.
         *
         * [left, right, top, bottom]
         **/
        split?: [number, number, number, number];
        /**
         * Nine-patch padding size.
         *
         * Allows content placed on top of a ninepatch to be inset differently from the splits.
         * Optional, and meaningless without split.
         *
         * [left, right, top, bottom]
         */
        pad?: [number, number, number, number];
    }

    class Parser
    {
        private readonly _lines: string[];
        private _lineIndex = -1;

        constructor(text: string)
        {
            this._lines = text.split("\n");
            this._lineIndex = 0;
        }

        public consume(pattern: RegExp): RegExpMatchArray | null
        {
            if (this._lineIndex >= this._lines.length) { return null; }

            const line = this._lines[this._lineIndex];
            const match = line.match(pattern);
            if (match == null) { return null; }

            ++this._lineIndex;
            return match;
        }
    }

    /** Applies a given scale factor to a parsed Spine atlas object. Note: throws if scale is less than 0. */
    export function scaleAtlas(atlas: Atlas, scale: number): void
    {
        // Scale = 1 -> atlas is already scaled
        if (scale === 1) { return; }
        // Scale < 0 -> the maths ceases to be correct then as we end up with negative sizes etc.
        if (scale < 0) { throw new Error("Negative scales are not supported"); }

        // TODO: fix imagemagick scale to sync with better 'fuzzy' strat
        const itemScaleStrategy = scaleItemSimple;
        for (const page of atlas.pages)
        {
            scaleCoords(page.size, scale);
            page.size[0] = Math.ceil(page.size[0]);
            page.size[1] = Math.ceil(page.size[1]);

            for (const itemName in page.items)
            {
                const item = page.items[itemName];
                itemScaleStrategy(item, scale);
            }
        }
    }

    function scaleItemSimple(item: Item, scale: number)
    {
        scaleCoords(item.xy, scale);
        roundCoords(item.xy);

        scaleCoords(item.size, scale);
        roundCoords(item.size);

        scaleCoords(item.orig, scale);
        roundCoords(item.orig);

        scaleCoords(item.offset, scale);
        roundCoords(item.offset);
    }

    function scaleItemFuzzy(item: Item, scale: number)
    {
        scaleCoords(item.xy, scale);

        // Find xy 'fuzz' i.e. floor delta so we can factor it in for size
        const fuzzX = item.xy[0] % 1;
        const fuzzY = item.xy[1] % 1;
        floorCoords(item.xy);

        scaleCoords(item.size, scale);

        item.size[0] += fuzzX;
        item.size[1] += fuzzY;
        ceilCoords(item.size);

        scaleCoords(item.orig, scale);
        ceilCoords(item.orig);

        scaleCoords(item.offset, scale);
        floorCoords(item.offset);
    }

    function roundCoords(coords: [number, number]): void
    {
        coords[0] = Math.round(coords[0]);
        coords[1] = Math.round(coords[1]);
    }

    function ceilCoords(coords: [number, number]): void
    {
        coords[0] = Math.ceil(coords[0]);
        coords[1] = Math.ceil(coords[1]);
    }

    function floorCoords(coords: [number, number]): void
    {
        coords[0] = Math.floor(coords[0]);
        coords[1] = Math.floor(coords[1]);
    }

    function scaleCoords(coords: [number, number], scale: number): void
    {
        coords[0] *= scale;
        coords[1] *= scale;
    }

    /** Reads a raw .atlas data string into a useful JSON object. */
    export function parseAtlas(text: string): Atlas
    {
        const parser = new Parser(text);

        const atlas: Atlas = { pages: [] };
        for (let page = parsePage(parser); page != null; page = parsePage(parser))
        {
            atlas.pages.push(page);
        }

        return atlas;
    }

    /** Writes an atlas JSON object to a raw .atlas data string. */
    export function writeAtlas(atlas: Atlas): string
    {
        let output: string = "";
        for (const page of atlas.pages)
        {
            output += "\n";
            output += `${page.name}\n`;
            output += `size: ${page.size.join(",")}\n`;
            output += `format: ${page.format}\n`;
            output += `filter: ${page.filter.join(",")}\n`;
            output += `repeat: ${page.repeat}\n`;
            for (const itemName in page.items)
            {
                output += `${itemName}\n`;

                const item = page.items[itemName];
                output += `  rotate: ${item.rotate}\n`;
                output += `  xy: ${item.xy.join(", ")}\n`;
                output += `  size: ${item.size.join(", ")}\n`;

                if (item.split)
                {
                    output += `  split: ${item.split.join(", ")}\n`;

                    if (item.pad)
                    {
                        output += `  pad: ${item.pad.join(", ")}\n`;
                    }
                }

                output += `  orig: ${item.orig.join(", ")}\n`;
                output += `  offset: ${item.offset.join(", ")}\n`;
                output += `  index: ${item.index}\n`;
            }
        }

        return output;
    }

    function consumeWhitespace(parser: Parser): void
    {
        let match: RegExpMatchArray;
        do
        {
            match = parser.consume(/^\s*$/);
        }
        while (match);
    }

    function parsePage(parser: Parser): Page | null
    {
        consumeWhitespace(parser);

        const name = parseString(parser);
        if (name == null) { return null; }

        const kv = parseKeyValues(parser);

        const page: Page =
        {
            name,
            size: parseCoords(kv["size"]),
            format: kv["format"],
            filter: kv["filter"].split(",").map((v) => v.trim()),
            repeat: kv["repeat"],
            items: parseItems(parser)
        };

        return page;
    }

    function parseItems(parser: Parser): Util.Map<Item>
    {
        const map: { [name: string]: Item } = {};

        let hasItems = true;
        while (hasItems)
        {
            hasItems = parseItem(parser, map)
        }

        return map;
    }

    function parseItem(parser: Parser, map: Util.Map<Item>): boolean
    {
        const name = parseString(parser);
        if (name == null) { return false; }

        const kv = parseKeyValues(parser);
        const item: Item = map[name] =
        {
            rotate: kv["rotate"] === "true",
            xy: parseCoords(kv["xy"]),
            size: parseCoords(kv["size"]),
            orig: parseCoords(kv["orig"]),
            offset: parseCoords(kv["offset"]),
            index: parseInt(kv["index"])
        };

        if ("split" in kv)
        {
            item.split = parseSpacing(kv["split"]);

            if ("pad" in kv)
            {
                item.pad = parseSpacing(kv["pad"]);
            }
        }

        return true;
    }

    function parseString(parser: Parser): string
    {
        // Match a non-null/space followed by any number of non-nulls and finally either a null terminator or a space
        const match = parser.consume(/[^\s\0]([^\0]*[^\s\0])?/);
        return match && match[0];
    }

    function parseKeyValues(parser: Parser): Util.Map<string>
    {
        const map: Util.Map<string> = {};
        for (let keyValue = parseKeyValue(parser); keyValue != null; keyValue = parseKeyValue(parser))
        {
            const [key, value] = keyValue;
            map[key] = value;
        }
        return map;
    }

    function parseKeyValue(parser: Parser): [string, string] | null
    {
        const match = parser.consume(/([^\s]+):\s*(.+)/);
        return match && [match[1], match[2]];
    }

    function parseCoords(value: string): [number, number]
    {
        const coordStr = value.match(/([0-9]+),\s*([0-9]+)/);
        return [parseInt(coordStr[1]), parseInt(coordStr[2])];
    }

    /** Parses a nine-patch split/pad string into [left, right, top, bottom]. */
    function parseSpacing(value: string): [number, number, number, number]
    {
        const spacingStr = value.match(/([0-9]+),\s*([0-9]+),\s*([0-9]+),\s*([0-9]+)/);
        return [spacingStr[1], spacingStr[2], spacingStr[3], spacingStr[4]].map((x) => parseInt(x)) as [number, number, number, number];
    }

    /** Scales a Spine 3.7 JSON by the given scale. Note: throws if scale is less than 0. */
    export function scaleSpine(spine: Esoteric.Spine, scale: number): void
    {
        // Scale = 1 -> atlas is already scaled
        if (scale === 1) { return; }
        // Scale < 0 -> the maths ceases to be correct then as we end up with negative sizes etc.
        if (scale < 0) { throw new Error("Negative scales are not supported"); }

        scaleXY(spine.skeleton, scale);
        scaleWH(spine.skeleton, scale);

        for (const skin of spine.skins)
        {
            for (const slot in skin.attachments)
            {
                const slotAttachments = skin.attachments[slot];
                for (const name in slotAttachments)
                {
                    const attachment = slotAttachments[name];
                    switch (attachment.type)
                    {
                        default:
                        case "region":
                        {
                            scaleXY(attachment, scale);
                            scaleWH(attachment, scale);
                            break;
                        }

                        case "mesh":
                        {
                            scaleWH(attachment, scale);
                            scaleVertices(attachment, scale);
                            break;
                        }

                        case "linkedmesh":
                        {
                            scaleWH(attachment, scale);
                            break;
                        }

                        case "boundingbox":
                        {
                            scaleVertices(attachment, scale);
                            break;
                        }

                        case "path":
                        {
                            scaleVertices(attachment, scale);
                            scaleLengths(attachment, scale);
                            break;
                        }

                        case "point":
                        {
                            scaleXY(attachment, scale);
                            break;
                        }

                        case "clipping":
                        {
                            scaleVertices(attachment, scale);
                            break;
                        }
                    }
                }
            }
        }

        for (const bone of spine.bones)
        {
            scaleXY(bone, scale);
            if (bone.length != null) { bone.length *= scale; }
        }

        for (const animationName in spine.animations)
        {
            const animation = spine.animations[animationName];

            if (animation.bones)
            {
                for (const boneName in animation.bones)
                {
                    const boneTimeline = animation.bones[boneName];

                    if (boneTimeline.translate)
                    {
                        for (const keyframe of boneTimeline.translate)
                        {
                            scaleXY(keyframe, scale);
                        }
                    }
                }
            }
        }

        for (const transformConstraint of spine.transform)
        {
            scaleXY(transformConstraint, scale);
        }
    }

    function scaleXY(target: { x?: number, y?: number }, scale: number): void
    {
        if (target.x != null) { target.x *= scale; }
        if (target.y != null) { target.y *= scale; }
    }

    function scaleWH(target: { width?: number, height?: number }, scale: number): void
    {
        if (target.width != null) { target.width *= scale; }
        if (target.height != null) { target.height *= scale; }
    }

    function scaleVertices(target: { vertices?: number[], vertexCount?: number, uvs?: number[] }, scale: number): void
    {
        if (target.vertices)
        {
            const vertexCount = target.uvs ? target.uvs.length : target.vertexCount;
            if (vertexCount == null) { throw new Error(`Could not ascertain vertex count from attachment: ${JSON.stringify(target, null, 4)}`); }

            const weighted = target.vertices.length > vertexCount;
            if (weighted)
            {
                for (let i = 0; i < target.vertices.length; i++)
                {
                    const boneCount = target.vertices[i];
                    for (let b = 0; b < boneCount; b++)
                    {
                        // + 1 = bone index
                        target.vertices[i + 2] *= scale;
                        target.vertices[i + 3] *= scale;
                        // + 4 = weight
                        i += 4;
                    }
                }
            }
            else
            {
                for (let i = 0; i < target.vertices.length; i++)
                {
                    target.vertices[i] *= scale;
                }
            }
        }
    }

    function scaleLengths(target: { lengths?: number[] }, scale: number): void
    {
        if (target.lengths)
        {
            for (let i = 0; i < target.lengths.length; i++)
            {
                target.lengths[i] *= scale;
            }
        }
    }
}