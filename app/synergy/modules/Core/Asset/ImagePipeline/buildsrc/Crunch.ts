namespace RS.AssetPipeline.ImageUtils
{
    export enum CrunchFileFormat { dds, ktx, crn, tga, bmp, png }

    export enum CrunchPixelFormat { DXT1, DXT2, DXT3, DXT4, DXT5, DXN, DXT5A, DXT5_CCxY, DXT5_xGxR, DXT5_xGBR, DXT5_AGBR, DXT1A, ETC1, R8G8B8, L8, A8, A8L8, A8R8G8B8 }

    export namespace CrunchPixelFormat
    {
        const map: Util.Map<CrunchPixelFormat> =
        {
            ["dxt1"]: CrunchPixelFormat.DXT1,
            ["dxt1a"]: CrunchPixelFormat.DXT1A,
            ["dxt3"]: CrunchPixelFormat.DXT3,
            ["dxt5"]: CrunchPixelFormat.DXT5,
            ["dxt5a"]: CrunchPixelFormat.DXT5A,
            ["a8"]: CrunchPixelFormat.A8
        };
        export function parse(str: string): CrunchPixelFormat | null
        {
            str = str.toLowerCase();
            return str in map ? map[str] : null;
        }
    }

    export interface CrunchArgs
    {
        inputPath: string;
        outputPath: string;

        mipMaps: boolean;

        /** The file format to use. */
        fileFormat: CrunchFileFormat;

        /** The pixel format to use. */
        pixelFormat: CrunchPixelFormat;

        /** 0-255, default 128 */
        quality?: number;
    }

    const thisModule = Module.getModule("Core.Asset.ImagePipeline");
    const binPath = Path.combine(thisModule.path, "build_tools", process.platform, `crunch${process.platform === "win32" ? ".exe" : ""}`);

    export async function crunch(args: CrunchArgs)
    {
        const fileFormat = CrunchFileFormat[args.fileFormat];

        const argArr: string[] = [];
        argArr.push("-file", args.inputPath);
        argArr.push(`-${CrunchPixelFormat[args.pixelFormat]}`);
        if (args.quality != null) { argArr.push("-quality", `${args.quality}`); }
        if (!args.mipMaps) { argArr.push("-mipmode", `none`); }
        argArr.push("-fileformat", fileFormat);
        argArr.push("-helperThreads", "0");
        // argArr.push("-noprogress");
        argArr.push("-quiet");

        const outputText = await Command.run(binPath, argArr, null, false);

        const compressedPath = Path.replaceExtension(Path.baseName(args.inputPath), `.${fileFormat}`);
        FileSystem.cache.set(compressedPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });

        await FileSystem.copyFile(compressedPath, args.outputPath);
        await FileSystem.deletePath(compressedPath);
    }
}