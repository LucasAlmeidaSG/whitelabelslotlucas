/// <reference path="Image.ts" />
/// <reference path="Requirements.ts" />

namespace RS.AssetPipeline
{
    const supportedSpineVersion = Version(3, 7);

    /**
     * Represents an Esoteric Spine asset.
     */
    export interface SpineDefinition extends ImageDefinition
    {
        /**
         * If true, scales the spine to meet the defined res, otherwise assumes the spine files are pre-scaled.
         *
         * Does not affect 'scale', which will always scale.
         */
        scaleToRes?: boolean;
        exportArrays?: boolean;
    }

    @AssetType("spine")
    @InferFromExtensions([".atlas"])
    @RequireCommands([ ImageRequirements.imageMagick, ImageRequirements.pngQuant ])
    export class Spine extends Image
    {
        private _animations: string[] = [];
        private _skins: Esoteric.Spine.Skin[] = [];
        private _slots: string[] = [];
        private _bones: string[] = [];

        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }

        /**
         * Loads any required metadata for this asset.
         */
        public async load()
        {
            await super.load();

            // Search variants, try to find a json file to load
            let path: string;
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = this._definition.variants[variantName];
                    if (!Is.string(variant.path)) { throw new Error("Expected single-type assetPath"); }
                    path = this.getFullPath(variant.path);
                    await this.loadSpineData(path);
                }
            }
            else
            {
                if (!Is.string(this._definition.path)) { throw new Error("Expected single-type assetPath"); }
                path = this.getFullPath(this._definition.path);
                await this.loadSpineData(path);
            }
        }

        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition | null
        {
            const ref = AssetPipeline.emitAssetReference(this, "RS.Asset.SpineReference");

            // Add all animation names
            for (const animName of this._animations)
            {
                const cleanedName = AssetPipeline.cleanName(animName);
                AssetPipeline.addAssetReferenceProperty(ref, cleanedName, `"${animName}"`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "string" });
            }

            const definition = this._definition as SpineDefinition;

            if (definition.exportArrays)
            {
                // Create array of all the animations in the file
                const anims = this._animations.map((v) => `"${v}"`).join(", ");
                AssetPipeline.addAssetReferenceProperty(ref, "animations", `[ ${anims} ]`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "string[]" });

                // Create array of all the skins in the file
                const skins = this._skins.map((skin) =>
                    `{
                    name: "${skin.name}",
                    attachments: [ ${Object.keys(skin.attachments).map(att => `"${att}"`).join(", ")} ]
                }`).join(",");
                AssetPipeline.addAssetReferenceProperty(ref, "skins", `[ ${skins} ]`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "any[]" });// TODO: RS.AssetPipeline.Spine.Skin[]

                // Create array of all the slots in the file
                const slots = this._slots.map((v) => `"${v}"`).join(", ");
                AssetPipeline.addAssetReferenceProperty(ref, "slots", `[ ${slots} ]`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "string[]" });

                // Create array of all the bones in the file
                const bones = this._bones.map((v) => `"${v}"`).join(", ");
                AssetPipeline.addAssetReferenceProperty(ref, "bones", `[ ${bones} ]`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "string[]" });
            }
            return ref;
        }

        protected parseObjects(object: object, key?: string)
        {
            const result: string[] = [];

            for (const name in object)
            {
                const value = key ? object[name][key] : name;
                result.push(value);
            }

            return result;
        }

        protected mergeArray(first: string[], second: string[], sort?: boolean)
        {
            const result: string[] = [...first];
            for (const value of second)
            {
                if (first.indexOf(value) === -1)
                {
                    result.push(value);
                }
            }
            return sort ? result.sort() : result;
        }

        protected async loadSpineData(path: string)
        {
            const json: Esoteric.Spine = await JSON.parseFile(Path.replaceExtension(path, ".json"));
            this.throwIfIncompatible(json);

            this._animations = this.mergeArray(this._animations, this.parseObjects(json.animations), true);
            this._skins = json.skins;
            this._slots = this.mergeArray(this._slots, this.parseObjects(json.slots, "name"), true);
            this._bones = this.mergeArray(this._bones, this.parseObjects(json.bones, "name"), true);
        }

        /**
         * Given the specified asset as referred to by the definition, finds all source files to be used for checksum.
         * @param path
         * @param list
         */
        protected async gatherChecksumPaths(path: string, list: List<string>)
        {
            // Load up the json
            const atlasText = await FileSystem.readFile(path);
            const atlasData = SpineUtils.parseAtlas(atlasText);

            for (const page of atlasData.pages)
            {
                const image = page.name;
                list.add(Path.combine(Path.directoryName(path), image));
            }

            // Add the json itself to the list
            list.add(Path.replaceExtension(path, ".json"));
            list.add(path);
        }

        protected async processVariant(outputDir: string, assetPath: string, variant: SpineDefinition, dry: boolean): Promise<ManifestEntry | null>
        {
            const baseResult = await super.processVariant(outputDir, assetPath, variant, dry);
            if (baseResult == null) { return null; }

            const atlasText = await FileSystem.readFile(assetPath);
            const atlasData = SpineUtils.parseAtlas(atlasText);

            // Sanitise image paths
            for (const page of atlasData.pages)
            {
                page.name = Path.sanitise(page.name);
            }

            const atlasPath = Path.baseName(assetPath);
            const jsonPath = Path.replaceExtension(atlasPath, ".json");

            const spineScale = variant.scale || 1;
            const imageScale = spineScale * (variant.scaleToRes ? (variant.res || 1) : 1);

            // TODO:
            //  imagemagick scale rounds rather than ceils; can lead to subtex off-by-one; ideally figure out how to make imagemagick ceil

            SpineUtils.scaleAtlas(atlasData, imageScale);
            await FileSystem.writeFile(Path.combine(outputDir, Path.sanitise(atlasPath)), SpineUtils.writeAtlas(atlasData));

            const spine: Esoteric.Spine = await JSON.parseFile(Path.replaceExtension(assetPath, ".json"));
            this.throwIfIncompatible(spine);

            SpineUtils.scaleSpine(spine, spineScale);
            await FileSystem.writeFile(Path.combine(outputDir, Path.sanitise(jsonPath)), JSON.stringify(spine));

            return baseResult;
        }

        /**
         * Processes a single format for a single asset variant.
         * Note that the format might be passed as null here if no format is specified.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string, variant: ImageDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            const shouldGzip = this.shouldGzip(format);

            const paths: string[] = [];

            const atlasText = await FileSystem.readFile(assetPath);
            const atlasData = SpineUtils.parseAtlas(atlasText);

            if (!dry)
            {
                // Iterate the images
                await Promise.all(atlasData.pages.map((page) => this.processSpineImage(outputDir, assetPath, variant, dry, format, shouldGzip, page.name)));
            }

            for (const page of atlasData.pages)
            {
                const path = Path.sanitise(Path.replaceExtension(page.name, `.${format}`));
                paths.push(path);
            }

            const atlasPath = Path.baseName(assetPath);
            const jsonPath = Path.replaceExtension(assetPath, ".json");
            paths.unshift(Path.sanitise(atlasPath));
            paths.push(Path.sanitise(jsonPath));

            if (shouldGzip)
            {
                return { paths: [...paths, ...paths.map(p => Path.replaceExtension(p, `${Path.extension(p)}.gz`))] };
            }
            else
            {
                return { paths };
            }
        }

        protected async processSpineImage(outputDir: string, assetPath: string, variant: ImageDefinition, dry: boolean, format: string, shouldGzip: boolean, imageName: string)
        {
            const inputPath = Path.combine(Path.directoryName(assetPath), imageName);
            const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.sanitise(Path.baseName(inputPath))), `.${format}`);

            if (!await this.processSpecificFormat(inputPath, outputPath, format, variant)) { return null; }

            if (shouldGzip)
            {
                // Produce gz version
                await this.gzip(outputPath);
            }
        }

        protected throwIfIncompatible(spine: Esoteric.Spine): void
        {
            const verString = spine.skeleton.spine;
            const version = Version.fromString(verString);
            if (!Version.compatible(version, supportedSpineVersion)) { throw new Error(`Spine ${verString} is not supported. Please update Spine to at least ${Version.toString(supportedSpineVersion)} and re-export.`); }
        }
    }
}
