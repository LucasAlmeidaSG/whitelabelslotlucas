/// <reference path="Image.ts" />
/// <reference path="Requirements.ts" />

namespace RS.AssetPipeline
{
    export interface RSSpriteSheetData
    {
        images: string[];
        frames: number[][];
        animations:
        {
            [name: string]:
            {
                frames: number[],
                next: string | null;
            };
        };
        framerate: number;
    }

    /**
     * Image asset specific definition data.
     */
    export interface RSSpriteSheetDefinition extends ImageDefinition
    {
        /** If animation we convert SpriteSheet json to RSSpriteSheet json */
        isAnimation?: boolean;
    }

    function isSpriteSheet(json: any, definition: RSSpriteSheetDefinition): boolean
    {
        return Is.object(json.meta) &&
            Is.arrayOf(json.frames, Is.object) &&
            definition.isAnimation;
    }

    function isRSSpriteSheet(json: any)
    {
        return Is.arrayOf(json.images, Is.string) &&
            Is.arrayOf(json.frames, Is.array) &&
            Is.object(json.animations);
    }

    /**
     * Given an asset definition, determines if it can be inferred as a spritesheet.
     * @param definition
     */
    async function InferSpritesheet(definition: AssetPipeline.AssetDefinition)
    {
        // Spritesheets don't support map-type paths
        if (!Is.string(definition.path)) { return false; }

        // First, check the extension
        if (Path.extension(definition.path) !== ".json") { return false; }

        if (definition.variants)
        {
            for (const variantName in definition.variants)
            {
                const variant = definition.variants[variantName];

                // Spritesheets don't support map-type paths
                if (!Is.string(variant.path)) { return false; }

                const assetPath = Path.combine(Module.getModule(definition.module).path, Constants.inAssetsFolder, Path.directoryName(definition.manifestPath), variant.path);
                try
                {
                    // Next, load it and look for spritesheet signature
                    const parsed = await JSON.parseFile(assetPath);
                    return isRSSpriteSheet(parsed) || isSpriteSheet(parsed, definition);
                }
                catch (err)
                {
                    // Something went wrong, just return false
                    return false;
                }
            }
        }
        else
        {
            const assetPath = Path.combine(Module.getModule(definition.module).path, Constants.inAssetsFolder, Path.directoryName(definition.manifestPath), definition.path);
            try
            {
                // Next, load it and look for spritesheet signature
                const parsed = await JSON.parseFile(assetPath);
                return isRSSpriteSheet(parsed) || isSpriteSheet(parsed, definition);
            }
            catch (err)
            {
                // Something went wrong, just return false
                return false;
            }
        }

        return false;
    }

    /**
     * Represents an image-type asset.
     */
    @AssetType("rsspritesheet")
    @RequireCommands([ ImageRequirements.imageMagick, ImageRequirements.pngQuant, ImageRequirements.gzip ])
    @InferCustom(InferSpritesheet)
    export class RSSpriteSheet extends Image
    {
        private _spriteSheetData: RSSpriteSheetData | null = null;

        /** Gets if this asset supports code generation or not. */
        public get supportsCodeGen() { return true; }
        /**
         * Loads any required metadata for this asset.
         */
        public async load()
        {
            await super.load();

            // Search variants, try to find a json file to load
            let data: RSSpriteSheetData;
            let path: string;
            if (this._definition.variants)
            {
                for (const variantName in this._definition.variants)
                {
                    const variant = this._definition.variants[variantName];
                    if (!Is.string(variant.path)) { throw new Error("Expected single-type assetPath"); }
                    path = this.getFullPath(variant.path);

                    const thisData: RSSpriteSheetData = await this.getRSSpriteSheetData(path)
                    if (!data)
                    {
                        data = thisData;
                    }
                    else
                    {
                        for (const animation in thisData.animations)
                        {
                            if (!data.animations[animation])
                            {
                                data.animations[animation] = thisData.animations[animation];
                            }
                        }
                    }
                }
            }
            else
            {
                if (!Is.string(this._definition.path)) { throw new Error("Expected single-type assetPath"); }
                path = this.getFullPath(this._definition.path);
                data = await this.getRSSpriteSheetData(path);
            }
            this._spriteSheetData = data;
        }
        /**
         * Emits a value to use for the code generation stage of this asset.
         */
        public emitCodeDefinition(): AssetCodeDefinition | null
        {
            const baseReference = AssetPipeline.emitAssetReference(this, "RS.Asset.RSSpriteSheetReference");
            for (const animName in this._spriteSheetData.animations)
            {
                const cleanedName = AssetPipeline.cleanName(animName);
                if (cleanedName !== "")
                {
                    addAssetReferenceProperty(baseReference, cleanedName, `"${animName}"`, { kind: CodeMeta.TypeRef.Kind.Raw, name: "string" });
                }
            }
            return baseReference;
        }
        /**
         * Given the specified asset as referred to by the definition, finds all source files to be used for checksum.
         * @param path
         * @param list
         */
        protected async gatherChecksumPaths(path: string, list: List<string>)
        {
            // Load up the json
            const spriteSheetData = await this.getRSSpriteSheetData(path);

            // Add all images to the list
            for (const imageName of spriteSheetData.images)
            {
                const imagePath = Path.combine(Path.directoryName(path), imageName);
                list.add(imagePath);
            }

            // Add the json itself to the list
            list.add(path);
        }

        /**
         * Processes all formats for a single asset variant.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         */
        protected async processVariant(outputDir: string, assetPath: string, variant: RSSpriteSheetDefinition, dry: boolean): Promise<ManifestEntry | null>
        {
            // Load the sprite sheet data
            this._spriteSheetData = await this.getRSSpriteSheetData(assetPath);

            // Process formats
            const baseResult = await super.processVariant(outputDir, assetPath, variant, dry);
            if (baseResult == null) { return null; }

            const mipmap = as(variant.mipmap, Is.boolean);
            if (mipmap != null) { (baseResult.variantData as RSSpriteSheetDefinition).mipmap = mipmap; }

            if (!dry)
            {
                // Strip out extensions and sanitise paths
                this._spriteSheetData.images = this._spriteSheetData.images.map((path) => Path.replaceExtension(Path.sanitise(path), ""));

                // Write out the json
                await FileSystem.writeFile(Path.combine(outputDir, Path.sanitise(Path.baseName(assetPath))), JSON.stringify(this._spriteSheetData));
            }

            baseResult.paths.unshift(Path.sanitise(assetPath));

            return baseResult;
        }

        /**
         * Processes a single format for a single asset variant.
         * Note that the format might be passed as null here if no format is specified.
         * @param outputDir
         * @param assetPath
         * @param variant
         * @param dry
         * @param format
         */
        protected async processFormat(outputDir: string, assetPath: string, variant: RSSpriteSheetDefinition, dry: boolean, format: string): Promise<ProcessFormatResult | null>
        {
            const shouldGzip = this.shouldGzip(format);

            if (!dry)
            {
                // Iterate the images
                for (let i = 0, l = this._spriteSheetData.images.length; i < l; ++i)
                {
                    const imageName = this._spriteSheetData.images[i];

                    const inputPath = Path.combine(Path.directoryName(assetPath), imageName);
                    const outputPath = Path.replaceExtension(Path.combine(outputDir, Path.sanitise(Path.baseName(inputPath))), `.${format}`);

                    const oldRes = variant.res;
                    variant.res = 1.0;

                    if (!await this.processSpecificFormat(inputPath, outputPath, format, variant)) { return null; }

                    if (shouldGzip)
                    {
                        // Produce gz version
                        await this.gzip(outputPath);
                    }

                    variant.res = oldRes;
                }
            }
            const paths = this._spriteSheetData.images.map(path => Path.replaceExtension(Path.sanitise(path), `.${format}`));
            if (shouldGzip)
            {
                return { paths: [...paths, ...paths.map(p => Path.replaceExtension(p, `${Path.extension(p)}.gz`))] };
            }
            else
            {
                return { paths };
            }
        }

        /**
         * Gets RSSpriteSheet data from given path. Converts to RSSpriteSheet json format if SpriteSheet
         * @param path
         */
        protected async getRSSpriteSheetData(path: string): Promise<RSSpriteSheetData>
        {
            const data = await JSON.parseFile(path);

            if (isRSSpriteSheet(data))
            {
                return data;
            }
            else
            {
                if (Is.array(data.frames))
                {
                    //get animation name filename of first name, sanitisation happens in code generation
                    const animationName: string = Path.replaceExtension(data.frames[0].filename, "");

                    //convert to rsspritesheet
                    const out: RSSpriteSheetData =
                    {
                        images: [ data.meta.image ], // There's always only going to be 1 related image
                        frames: [],
                        animations: {
                            [animationName]:
                            {
                                frames: [],
                                next: null
                            }
                        },
                        framerate: 24 //default in RSSpritesheet, not stored in the source format
                    };
                    data.frames.forEach((frameData, i) =>
                    {
                        // Obtained from red7 texturepacker exporter version 2.2 line 137
                        // x, y,
                        // width, height,
                        // textureId,
                        // pivot x, pivot y
                        // untrimmed width, untrimmed height
                        // corneroffset x, corneroffset y

                        out.frames.push([
                            frameData.frame.x, frameData.frame.y,
                            frameData.frame.w, frameData.frame.h,
                            0,
                            frameData.frame.w * frameData.pivot.x, frameData.frame.h * frameData.pivot.y,
                            frameData.sourceSize.w, frameData.sourceSize.h,
                            0, 0
                        ]);
                        out.animations[animationName].frames.push(i);
                    });

                    return out;
                }
                else
                {
                    throw new Error(`${path}: frames property not an array, can't convert to rsspritesheet`);
                }
            }
        }
    }
}