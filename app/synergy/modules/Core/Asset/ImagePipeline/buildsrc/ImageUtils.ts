/// <reference path="Requirements.ts" />

namespace RS.AssetPipeline.ImageUtils
{
    export interface QuantOptions
    {
        /** Overwrite existing output files. */
        force?: boolean;
        /** Only save converted files if they're smaller than original. */
        skipIfLarger?: boolean;
        /** [min, max]. Don't save below min, use fewer colors below max. Defaults to 0-100. */
        quality?: [number, number];
        /** Speed/quality trade-off. 1=slow, 3=default, 11=fast & rough. */
        speed?: number;
        /** Output lower-precision color (e.g. for ARGB4444 output). */
        posterize?: number;
        /** Remove optional metadata (default on Mac). */
        stripMetadata?: boolean;
        /** Print status messages. */
        verbose?: boolean;
    }

    export const defaultQuantOptions: QuantOptions =
    {
        force: true
    };

    function resolveDefaults<T extends object>(defaults: T, obj: T | undefined): T
    {
        if (obj == null)
        {
            return defaults;
        }
        else
        {
            return { ...(defaults as any), ...(obj as any) };
        }
    }

    /**
     * Resizes the specified image by a scale factor. Optionally trims the image at the same time.
     * @param srcPath       Source image to resize
     * @param dstPath       Where to write the new image
     * @param scaleFactor   Scale factor (1.0 = 100%, 0.5 = 50% etc)
     * @param trimThreshold Optional trim fuzziness percentage (0-100)
     */
    export async function resize(srcPath: string, dstPath: string, scaleFactor: number, trimThreshold?: number)
    {
        // http://www.imagemagick.org/Usage/resize/
        const command = await resolveRequirement(ImageRequirements.imageMagick);

        const args =
        [
            srcPath,
            "-quiet",
            "-resize", `${Math.floor(scaleFactor * 100)}%`,
            dstPath
        ];

        if (trimThreshold)
        {
            trimThreshold = Math.floor(Math.min(Math.max(trimThreshold, 0), 100));
            args.splice(1, 0, "-fuzz", `${trimThreshold}%`, "-trim", "+repage");
        }

        const output = await Command.run(command, args);
        FileSystem.cache.set(dstPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
        return output;
    }

    /**
     * Performs in-place quantization using pngquant.
     */
    export function quantize(path: string, options?: QuantOptions): PromiseLike<string>;

    /**
     * Performs quantization using pngquant.
     */
    export function quantize(srcPath: string, dstPath: string, options?: QuantOptions): PromiseLike<string>;

    export async function quantize(srcPath: string, p1?: string | QuantOptions, p2?: QuantOptions)
    {
        const dstPath: string = Is.string(p1) ? p1 : srcPath;
        const quantOptions = resolveDefaults(defaultQuantOptions, Is.object(p1) ? p1 : p2);

        const args = [srcPath, "-o", dstPath];

        if (quantOptions.force)
        {
            args.push("-f");
        }

        if (quantOptions.skipIfLarger)
        {
            args.push("--skip-if-larger");
        }

        if (quantOptions.quality)
        {
            const [min, max] = quantOptions.quality;
            args.push(`--quality=${min}-${max}`);
        }

        if (quantOptions.speed != null)
        {
            args.push(`-s${quantOptions.speed}`);
        }

        if (quantOptions.posterize != null)
        {
            args.push(`--posterize ${quantOptions.posterize}`);
        }

        if (quantOptions.stripMetadata)
        {
            args.push("--strip");
        }

        if (quantOptions.verbose)
        {
            args.push("--verbose");
        }

        const output = await Command.run("pngquant", args);
        if (dstPath != null) { FileSystem.cache.set(dstPath, { type: FileIO.FileSystemCache.NodeType.File, content: null }); }

        return output;
    }

    export interface ImageInfo
    {
        width: number;
        height: number;
    }

    /**
     * Gets basic information about an image via image magick "identify" command.
     * @param path
     */
    export async function identify(path: string): Promise<ImageInfo | null>
    {
        const output = await Command.run("identify", [ path ], undefined, false);
        const res = /([0-9]+)x([0-9]+)/g.exec(output);
        if (res == null) { return null; }
        return {
            width: parseInt(res[1]),
            height: parseInt(res[2])
        };
    }
}