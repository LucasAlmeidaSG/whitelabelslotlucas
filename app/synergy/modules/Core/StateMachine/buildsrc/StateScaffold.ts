namespace RS.Build.Scaffolds
{
    const stateTemplate = `namespace {{namespace}}.States
{
    /**
     * The {{className}} state.
     */
    @RS.State.Name(State.{{className:CamelCase}})
    export class {{className:CamelCase}}State extends RS.State.Base<{{context}}>
    {
        public onEnter(): void
        {
            super.onEnter();
            
        }
    }
}`;

    export const state = new Scaffold();
    state.addParam("context", "Context", null, "RS.Game.Context");
    state.addParam("className", "Class Name", "MyClass");
    state.addTemplate(stateTemplate, "src/states/{{className}}.ts");
    registerScaffold("state", state);
}