/// <reference path="State.ts" />

namespace RS.State
{
    const logger = Logging.getContext("State");
    const debugMode = logger.logLevel <= Logging.LogLevel.Debug;

    /**
     * A finite state machine that tracks a single current state and can transition to multiple possible other states.
    **/
    export class Machine<T extends object = object>
    {
        private static _activeMachines = 0;

        protected _context: T;
        protected _curState: Base<T>;
        protected _nestLevel: number;

        protected _stateMap: Map<Base<T>>;
        protected _nextStates: Map<string>;
        protected _inStateSwitch: boolean;
        protected _stateSwitchQueue: string[];

        protected _exited: boolean = false;
        protected _active: boolean = true;

        protected readonly _onStateChanged = createEvent<Machine.StateChangedData<T>>();
        protected readonly _onExited = createEvent<Machine.ExitedData>();

        /** Published when the state of this machine has changed. */
        public readonly onStateChanged = this._onStateChanged.public;

        /** Published when this machine has exited. */
        public readonly onExited = this._onExited.public;

        /** Gets the context associated with this state machine. */
        public get context() { return this._context; }

        /** Gets the currently active state. */
        public get currentState() { return this._curState; }

        /** Gets if this state machine has exited. */
        public get hasExited() { return this._exited; }

        /**
         * Gets the estimated nest level of this machine.
         * Computed using a heuristic so may not be accurate.
         * Use this only for debugging purposes.
        **/
        public get nestLevel() { return this._nestLevel; }

        /**
         * Active flag, this by itself does nothing in the default states
         * but you can use it in overridden state classes to test if the
         * state machine is active.
        **/
        public get active() { return this._active;}
        public set active(active: boolean)
        {
            this._active = active;
        }

        /**
         * Initialises a new instance of the StateMachine class.
         * @param definition    The state machine definition.
         * @param context       The context to use.
        **/
        constructor(definition: Machine.Definition<T>, context: T)
        {
            this._nestLevel = Machine._activeMachines++;
            this._context = context;
            this._stateMap = {};
            this._inStateSwitch = false;
            this._stateSwitchQueue = [];
            for (let i = 0; i < definition.states.length; i++)
            {
                const state = new definition.states[i](this, context);
                this._stateMap[state.name] = state;
            }
            this._nextStates = definition.nextStates || {};
            if (debugMode) { logger.debug("New machine"); }
            this.switchToState(definition.initialState);
        }

        /**
         * Switches this machine to a new state.
         * You should only call this from within a state, and even then use BaseState.transitionToState.
         * @param newStateName      The name of the state to transition to.
         * @param ignoreRefresh     If true, changing state to an already currently active state will fail silently. If false, the state will be exited and re-entered.
        **/
        public switchToState(newStateName: string, ignoreRefresh: boolean = true): void
        {
            if (this._inStateSwitch)
            {
                this._stateSwitchQueue.push(newStateName);
                return;
            }
            this._inStateSwitch = true;
            this.performSwitch(newStateName, ignoreRefresh);
            this._inStateSwitch = false;
            if (this._stateSwitchQueue.length > 0)
            {
                this.switchToState(this._stateSwitchQueue.shift());
            }
        }

        /**
         * Switches this machine to the next state in the sequence.
         * If no next state is present in the sequence, will use the default state passed in.
         * You should only call this from within a state, and even then use BaseState.transitionToState.
         * @param newStateName      The name of the state to transition to.
         * @param ignoreRefresh     If true, changing state to an already currently active state will fail silently. If false, the state will be exited and re-entered.
        **/
        public switchToNextState(defaultNextState: string, ignoreRefresh: boolean = true) : void
        {
            const nextState = this._nextStates[this._curState.name] || defaultNextState;
            this.switchToState(nextState, ignoreRefresh);
        }

        /**
         * Exits the machine.
        **/
       public exit(): void
       {
           if (this._exited) { return; }
           this._exited = true;
           Machine._activeMachines--;
           this.switchToState(null);
           this.debugLog("Exited machine");
           this._onExited.publish({ sender: this });
       }

        protected performSwitch(newStateName: string, ignoreRefresh: boolean): void
        {
            const oldState = this._curState;
            if (this._curState)
            {
                if (this._curState.name === newStateName && ignoreRefresh) { return; }
                this._curState.onExit();
                this._curState = null;
            }
            if (newStateName == null) { return; }
            this._curState = this._stateMap[newStateName];
            if (this._curState)
            {
                if (debugMode) { this.debugLog(`Switched to '${newStateName}'`); }
                this._curState.onEnter();
            }
            else if (newStateName !== "")
            {
                logger.warn(`State '${newStateName}' does not exist`);
            }
            this._onStateChanged.publish({ sender: this, oldState, newState: this._curState });
        }

        protected debugLog(message: string): void
        {
            const tmp: string[] = [];
            for (let i = 0, l = this._nestLevel; i <= l; ++i)
            {
                tmp.push("-");
            }
            logger.debug(`${tmp.join("")}| ${message}`);
        }
    }

    export namespace Machine
    {
        export interface StateChangedData<T extends object>
        {
            sender: Machine;
            oldState: Base<T>;
            newState: Base<T>;
        }

        export interface ExitedData
        {
            sender: Machine;
        }

        /**
         * Defines the states and initial state of a state machine.
        **/
        export interface Definition<T extends object = object>
        {
            /** Name of the initial state. */
            initialState: string;

            /** Array of all possible states that the machine can transition to. */
            states: Constructor<T>[];

            /** Mapping of states to their default following state. */
            nextStates?: Map<string>;
        }
    }
}
