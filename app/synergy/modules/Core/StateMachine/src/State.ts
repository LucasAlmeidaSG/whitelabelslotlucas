namespace RS.State
{
    /**
     * Assigns a name to a given state.
     * All states must have a name.
    **/
    export const Name = Decorators.Tag.create<string>(Decorators.TagKind.Class);

    /**
     * Encapsulates a constructor for a state.
     */
    export interface Constructor<T extends object>
    {
        new(machine: Machine<T>, context: T): Base<T>;
    }

    /**
     * The base class for a state within a state machine.
     * All states should extend this class and be tagged with a "State" decorator.
    **/
    @Name("base")
    export abstract class Base<T extends object = object>
    {
        protected _machine: Machine<T>;
        protected _context: T;

        /** Gets the name of this state. */
        public get name() { return Name.get(this); }

        constructor(machine: Machine<T>, context: T)
        {
            this._machine = machine;
            this._context = context;
        }

        /**
         * Called when this state has been transitioned to.
        **/
        public onEnter(): void
        {
            return;
        }

        /**
         * Called when this state has been transitioned away from.
        **/
        public onExit(): void
        {
            // Find all AutoDispose properties, clean them up
            for (const key of AutoDispose.get(this, Decorators.MemberType.Instance))
            {
                const val = this[key];
                if (Is.disposable(val))
                {
                    val.dispose();
                    this[key] = null;
                }
            }
        }

        /**
         * Transitions to the specified state.
         * @param newStateName      The name of the state to transition to.
         * @param ignoreRefresh     If true, changing state to an already currently active state will fail silently. If false, the state will be exited and re-entered.
        **/
        protected transitionToState(newStateName: string, ignoreRefresh: boolean = true): void
        {
            this._machine.switchToState(newStateName, ignoreRefresh);
        }

        protected transitionToNextState(defaultNextState: string, ignoreRefresh: boolean = true) : void
        {
            this._machine.switchToNextState(defaultNextState, ignoreRefresh);
        }
    }
}
