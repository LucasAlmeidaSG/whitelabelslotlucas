namespace RS.Tests
{
    const { expect } = chai;
    import XML = RS.Serialisation.XML;

    describe("XML.ts", function ()
    {
        function testXML(ctor: { new(): object; }, xml: string, expected: object): void
        {
            const obj = new ctor();
            const doc = new DOMParser().parseFromString(xml, "text/xml");
            XML.deserialise(obj, doc);
            expect(obj).to.deep.equal(expected, `${xml} != ${JSON.stringify(obj)}`);
        }

        describe("deserialise", function ()
        {
            it("should parse a string attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.String })
                    public foo: string;
                }
                testXML(Root, `<root foo="bar" />`, { foo: "bar" });
                testXML(Root, `<root foo="" />`, { foo: "" });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a number attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.Number })
                    public foo: number;
                }
                testXML(Root, `<root foo="25.6" />`, { foo: 25.6 });
                testXML(Root, `<root foo="" />`, { foo: NaN });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a boolean attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.Boolean })
                    public foo: boolean;
                }
                testXML(Root, `<root foo="y" />`, { foo: true });
                testXML(Root, `<root foo="Y" />`, { foo: true });
                testXML(Root, `<root foo="1" />`, { foo: true });
                testXML(Root, `<root foo="true" />`, { foo: true });
                testXML(Root, `<root foo="n" />`, { foo: false });
                testXML(Root, `<root foo="N" />`, { foo: false });
                testXML(Root, `<root foo="0" />`, { foo: false });
                testXML(Root, `<root foo="false" />`, { foo: false });
                testXML(Root, `<root foo="" />`, { foo: false });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited array of string attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.DelimitedArray(XML.String, "|") })
                    public foo: string[];
                }
                testXML(Root, `<root foo="abc|bar|def" />`, { foo: [ "abc", "bar", "def" ] });
                testXML(Root, `<root foo="abc" />`, { foo: [ "abc" ] });
                testXML(Root, `<root foo="" />`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited array of number attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.DelimitedArray(XML.Number, "|") })
                    public foo: number[];
                }
                testXML(Root, `<root foo="20.4|5|0" />`, { foo: [ 20.4, 5, 0 ] });
                testXML(Root, `<root foo="20.4" />`, { foo: [ 20.4 ] });
                testXML(Root, `<root foo="" />`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited array of boolean attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.DelimitedArray(XML.Boolean, "|") })
                    public foo: boolean[];
                }
                testXML(Root, `<root foo="y|Y|n|N|0|1" />`, { foo: [ true, true, false, false, false, true ] });
                testXML(Root, `<root foo="y" />`, { foo: [ true ] });
                testXML(Root, `<root foo="" />`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited 2D array of number attribute", function ()
            {
                class Root
                {
                    @XML.Property({ path: ".foo", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
                    public foo: number[][];
                }
                testXML(Root, `<root foo="1,2|3,4,5|6||7,8,9" />`, { foo: [ [ 1, 2 ], [ 3, 4, 5 ], [ 6 ], [ ], [ 7, 8, 9 ] ] });
                testXML(Root, `<root foo="" />`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a string tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.String })
                    public foo: string;
                }
                testXML(Root, `<root><foo>bar</foo></root>`, { foo: "bar" });
                testXML(Root, `<root><foo></foo></root>`, { foo: "" });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a number tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.Number })
                    public foo: number;
                }
                testXML(Root, `<root><foo>25.6</foo></root>`, { foo: 25.6 });
                testXML(Root, `<root><foo></foo></root>`, { foo: NaN });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a boolean tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.Boolean })
                    public foo: boolean;
                }
                testXML(Root, `<root><foo>y</foo></root>`, { foo: true });
                testXML(Root, `<root><foo>Y</foo></root>`, { foo: true });
                testXML(Root, `<root><foo>1</foo></root>`, { foo: true });
                testXML(Root, `<root><foo>true</foo></root>`, { foo: true });
                testXML(Root, `<root><foo>n</foo></root>`, { foo: false });
                testXML(Root, `<root><foo>N</foo></root>`, { foo: false });
                testXML(Root, `<root><foo>0</foo></root>`, { foo: false });
                testXML(Root, `<root><foo>false</foo></root>`, { foo: false });
                testXML(Root, `<root><foo></foo>`, { foo: false });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited array of string tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.DelimitedArray(XML.String, "|") })
                    public foo: string[];
                }
                testXML(Root, `<root><foo>abc|bar|def</foo></root>`, { foo: [ "abc", "bar", "def" ] });
                testXML(Root, `<root><foo>abc</foo></root>`, { foo: [ "abc" ] });
                testXML(Root, `<root><foo></foo></root>`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited array of number tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.DelimitedArray(XML.Number, "|") })
                    public foo: number[];
                }
                testXML(Root, `<root><foo>20.4|5|0</foo></root>`, { foo: [ 20.4, 5, 0 ] });
                testXML(Root, `<root><foo>20.4</foo></root>`, { foo: [ 20.4 ] });
                testXML(Root, `<root><foo></foo></root>`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited array of boolean tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.DelimitedArray(XML.Boolean, "|") })
                    public foo: boolean[];
                }
                testXML(Root, `<root><foo>y|Y|n|N|0|1</foo></root>`, { foo: [ true, true, false, false, false, true ] });
                testXML(Root, `<root><foo>y</foo></root>`, { foo: [ true ] });
                testXML(Root, `<root><foo></foo></root>`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse a delimited 2D array of number tag", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
                    public foo: number[][];
                }
                testXML(Root, `<root><foo>1,2|3,4,5|6||7,8,9</foo></root>`, { foo: [ [ 1, 2 ], [ 3, 4, 5 ], [ 6 ], [ ], [ 7, 8, 9 ] ] });
                testXML(Root, `<root><foo></foo></root>`, { foo: [ ] });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse an explicit object tag", function ()
            {
                class Foo
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "foo", type: XML.ExplicitObject(Foo) })
                    public foo: Foo;
                }
                testXML(Root, `<root><foo x="1" /></root>`, { foo: { x: 1 } });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse an implicit object tag", function ()
            {
                @XML.Type("unittest_foo")
                class Foo
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "unittest_foo", type: XML.Object })
                    public foo: Foo;
                }
                testXML(Root, `<root><unittest_foo x="1" /></root>`, { foo: { x: 1 } });
                testXML(Root, `<root />`, { foo: null });
            });

            it("should parse an array of tags inside a container", function ()
            {
                class Foo
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "foos", type: XML.Array(XML.ExplicitObject(Foo)) })
                    public foos: Foo[];
                }
                testXML(Root, `<root><foos><foo x="1" /><foo x="2" /></foos></root>`, { foos: [ { x: 1 }, { x: 2 } ] });
                testXML(Root, `<root><foos><foo x="1" /></foos></root>`, { foos: [ { x: 1 } ] });
                testXML(Root, `<root><foos></foos></root>`, { foos: [ ] });
                testXML(Root, `<root />`, { foos: null });
            });

            it("should parse an array of tags not inside a container", function ()
            {
                class Foo
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "", type: XML.Array(XML.ExplicitObject(Foo)) })
                    public foos: Foo[];
                }
                testXML(Root, `<root><foo x="1" /><foo x="2" /></root>`, { foos: [ { x: 1 }, { x: 2 } ] });
                testXML(Root, `<root><foo x="1" /></root>`, { foos: [ { x: 1 } ] });
                testXML(Root, `<root></root>`, { foos: [ ] });
            });

            it("should parse an array of tags not inside a container, via reference to tag name", function ()
            {
                class Foo
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "*foo", type: XML.Array(XML.ExplicitObject(Foo)) })
                    public foos: Foo[];
                }
                testXML(Root, `<root><foo x="1" /><foo x="2" /></root>`, { foos: [ { x: 1 }, { x: 2 } ] });
                testXML(Root, `<root><foo x="1" /></root>`, { foos: [ { x: 1 } ] });
                testXML(Root, `<root></root>`, { foos: [ ] });
            });

            it("should parse an array of tags not inside a container, mixed with other tags", function ()
            {
                class Foo
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Other
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;

                    @XML.Property({ path: ".y", type: XML.Number })
                    public y: number;
                }
                class Root
                {
                    @XML.Property({ path: "*foo", type: XML.Array(XML.ExplicitObject(Foo)) })
                    public foos: Foo[];

                    @XML.Property({ path: "other", type: XML.ExplicitObject(Other) })
                    public other: Other;
                }
                testXML(Root, `<root><other x="1" y="2" /><foo x="1" /><foo x="2" /></root>`,
                {
                    foos:
                    [
                        { x: 1 },
                        { x: 2 }
                    ],
                    other: { x: 1, y: 2 }
                });
                testXML(Root, `<root><other x="1" y="2" /><foo x="1" /></root>`,
                {
                    foos:
                    [
                        { x: 1 }
                    ],
                    other: { x: 1, y: 2 }
                });
                testXML(Root, `<root><other x="1" y="2" /></root>`,
                {
                    foos: [ ],
                    other: { x: 1, y: 2 }
                });
            });

            it("should parse a single attribute at the end of a space-delimited multi-tag path", function ()
            {
                class Root
                {
                    @XML.Property({ path: "foo bar ting.x", type: XML.Number })
                    public x: number;
                }
                testXML(Root, `<root><foo><bar><ting x="2"/></bar></foo></root>`, { x: 2 });
            });

            it("should parse a single object at the end of a space-delimited multi-tag path", function ()
            {
                class Ting
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "foo bar ting", type: XML.ExplicitObject(Ting) })
                    public ting: Ting;
                }
                testXML(Root, `<root><foo><bar><ting x="2"/></bar></foo></root>`, { ting: { x: 2 } });
            });

            it("should parse an array of objects at the end of a space-delimited multi-tag path", function ()
            {
                class Ting
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "foo bar", type: XML.Array(XML.ExplicitObject(Ting)) })
                    public tings: Ting[];
                }
                testXML(Root, `<root><foo><bar><ting x="2"/><ting x="1"/></bar></foo></root>`, { tings: [ { x: 2 }, { x: 1 } ] });
            });

            it("should parse an array of objects at the end of a space-delimited multi-tag path, via reference to tag name", function ()
            {
                class Ting
                {
                    @XML.Property({ path: ".x", type: XML.Number })
                    public x: number;
                }
                class Root
                {
                    @XML.Property({ path: "foo bar *ting", type: XML.Array(XML.ExplicitObject(Ting)) })
                    public tings: Ting[];
                }
                testXML(Root, `<root><foo><bar><ting x="2"/><ting x="1"/></bar></foo></root>`, { tings: [ { x: 2 }, { x: 1 } ] });
            });

            it("should cope with excess whitespace in a multi-tag path", function ()
            {
                class Root
                {
                    @XML.Property({ path: "  foo   bar  ting.x    ", type: XML.Number })
                    public x: number;
                }
                testXML(Root, `<root><foo><bar><ting x="2"/></bar></foo></root>`, { x: 2 });
            });
        });
    });
}