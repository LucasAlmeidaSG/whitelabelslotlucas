namespace RS.Serialisation.XML
{
    export interface PathElement
    {
        type: "tag"|"attr";
        value: string;
        filter?: Map<string>;
        multi?: boolean;
    }

    function parseFilter(filter: string): Map<string>
    {
        const result = {};
        const segs = filter.split(",");
        for (const seg of segs)
        {
            const keyVal = seg.split("=", 2);
            result[keyVal[0]] = keyVal[1];
        }
        return result;
    }

    function parsePathTag(el: string): PathElement
    {
        let multi = false;
        if (el[0] === "*")
        {
            el = el.substr(1);
            multi = true;
        }
        const filterMatch = /<(.+)>/g.exec(el);
        if (filterMatch != null && filterMatch.length > 1)
        {
            return { type: "tag", value: el.substr(0, filterMatch.index), filter: parseFilter(filterMatch[1]), multi };
        }
        else
        {
            return { type: "tag", value: el, multi };
        }
    }

    function parsePath(path: string): PathElement[]
    {
        if (path === "") { return []; }
        const result: PathElement[] = [];
        for (const el of path.trim().split(/\s+/g))
        {
            const indexOfDot: number = el.indexOf(".");
            if (el.substr(0, 1) === ".")
            {
                result.push({ type: "attr", value: el.substr(1) });
            }
            else if (indexOfDot !== -1)
            {
                result.push(parsePathTag(el.substr(0, indexOfDot)));
                result.push({ type: "attr", value: el.substr(indexOfDot + 1) });
            }
            else
            {
                result.push(parsePathTag(el));
            }
        }
        return result;
    }

    function stringifyPath(path: PathElement[]): string
    {
        const segs: string[] = [];
        for (const el of path)
        {
            if (el.type === "tag")
            {
                if (segs.length > 0) { segs.push(" "); }
                if (el.multi) { segs.push("*"); }
                segs.push(el.value);
            }
            else if (el.type === "attr")
            {
                segs.push(".");
                segs.push(el.value);
            }
            if (el.filter != null)
            {
                segs.push("<");
                let first = true;
                for (const key in el.filter)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        segs.push(",");
                    }
                    segs.push(key);
                    segs.push("=");
                    segs.push(el.filter[key]);
                }
                segs.push(">");
            }
        }
        return segs.join("");
    }

    function getChildren(parent: Element, tagName: string): Element[]
    {
        tagName = tagName.toLowerCase();
        const result: Element[] = [];
        const children = parent.childNodes;
        const len = children.length;
        for (let i = 0; i < len; ++i)
        {
            const child = children.item(i);
            if (child instanceof Element)
            {
                if (child.tagName.toLowerCase() === tagName)
                {
                    result.push(child);
                }
            }
        }
        return result;
    }

    function getOrCreateChild(parent: Element, name: string): Element
    {
        const els = getChildren(parent, name);
        if (els.length === 0)
        {
            const newEl = parent.ownerDocument.createElement(name);
            parent.appendChild(newEl);
            return newEl;
        }
        else
        {
            if (els.length > 1) { Log.warn(`getOrCreateChild: More than 1 element found`); }
            return els[0];
        }
    }

    /**
     * Reads the value at the specified path from the specified element.
     * @param src 
     * @param path 
     * @param arrOffset 
     */
    function read(src: Element, path: PathElement[], arrOffset: number = 0): Element[]|Element|string|null
    {
        if (path.length <= arrOffset) { return src; }
        const element = path[arrOffset];
        if (element.type === "attr")
        {
            return src.getAttribute(element.value);
        }
        else
        {
            const els = getChildren(src, element.value);

            const candidates: Element[] = [];
            for (let i = 0, l = els.length; i < l; ++i)
            {
                const item = els[i];
                if (element.filter != null)
                {
                    let passesFilter = true;
                    for (const key in element.filter)
                    {
                        const val = element.filter[key];
                        if (key === "")
                        {
                            if (val !== item.tagName)
                            {
                                passesFilter = false;
                                break;
                            }
                        }
                        else
                        {
                            const actualVal = item.getAttribute(key);
                            if (val !== actualVal)
                            {
                                passesFilter = false;
                                break;
                            }
                        }
                    }
                    if (passesFilter)
                    {
                        candidates.push(item);
                    }
                }
                else
                {
                    candidates.push(item);
                }
            }

            // Does it want to match multi?
            if (element.multi)
            {
                // Are we at the end of the path?
                if (arrOffset >= path.length - 1)
                {
                    return candidates;
                }
                else
                {
                    throw new Error(`Can't have a multi-match segment not at end of path '${stringifyPath(path)}'!`);
                }
            }

            if (candidates.length === 0) { return null; }
            if (candidates.length === 1)
            {
                return read(candidates[0], path, arrOffset + 1);
            }
            Log.warn(`More than 1 element found matching path '${stringifyPath(path)}'!`);
            return null;
        }
    }

    /**
     * Writes the value to the specified path in the specified element.
     * @param src 
     * @param path 
     * @param arrOffset 
     */
    function write(src: Element, path: PathElement[], item: Element[]|Element|string, arrOffset: number = 0): void
    {
        if (arrOffset >= path.length && Is.string(item))
        {
            src.textContent = item;
            return;
        }
        if (path.length === 0)
        {
            // Special case - the path is empty so just write straight into src
            if (item instanceof Element)
            {
                src.appendChild(item);
                return;
            }
            else if (Is.array(item))
            {
                for (const subItem of item)
                {
                    if (subItem != null)
                    {
                        src.appendChild(subItem);
                    }
                }
                return;
            }
            return;
        }
        const endOfPath = arrOffset === path.length - 1;
        const element = path[arrOffset];
        if (element.filter != null)
        {
            throw new Error("Serialisation of paths containing a filter currently not supported");
        }
        if (element.type === "attr")
        {
            if (!Is.string(item))
            {
                throw new Error("XML.write: can't serialise an Element or Element[] into an attribute");
            }
            src.setAttribute(element.value, item);
        }
        else
        {
            if (endOfPath)
            {
                if (item instanceof Element)
                {
                    src.appendChild(item);
                    return;
                }
                else if (Is.array(item))
                {
                    const elem = getOrCreateChild(src, path[path.length - 1].value);
                    for (const subItem of item)
                    {
                        if (subItem != null)
                        {
                            elem.appendChild(subItem);
                        }
                    }
                    return;
                }
            }
            const el = getOrCreateChild(src, element.value);
            write(el, path, item, arrOffset + 1);
        }
    }

    export type Serialiser<T> = (doc: Document, value: T|null) => Element[]|Element|string|null;
    export type Deserialiser<T> = (obj: Element[]|Element|string|null) => T|null;

    export type SerialiserPair<T> = { serialiser: Serialiser<T>; deserialiser: Deserialiser<T>; };

    export interface PropertyData
    {
        /**
         * Path to the desired property.
         *
         * Attributes are marked with a '.'.
         * E.g. "MyTag.myAttr"
         *
         * Sub-tags can be delimited by spaces.
         * E.g. "MyParentTagA MyParentTagB MyChildTag"
         *
         * An array of tags can be specified by either the path to the parent, or using * and the path to a tag,
         * E.g. "MyParentTag" or "MyParentTag *MyChildTag"
         */
        path: string;
        /** The parser for this property, e.g. XML.Number. */
        type: SerialiserPair<any>;
        /** 
         * If true, this property will not be serialised if set to null.
         * Defaults to false.
         */
        ignoreIfNull?: boolean;
        /** @internal */
        parsedPath?: PathElement[];
    }

    /** Declares a class to have the specified XML type. */
    export const Type = Decorators.Tag.create<string>(Decorators.TagKind.Class);

    /** Declares a property to be serialised and deserialised. */
    export const Property = Decorators.Tag.create<PropertyData>(Decorators.TagKind.Property);
    Property.onApplied((ev) =>
    {
        ev.val.parsedPath = parsePath(ev.val.path);
    });

    /** Provides customisation for XML serialisation of an object. */
    export interface ISerialisableObject
    {
        getTagName(): string;
    }

    export namespace ISerialisableObject
    {
        export function is(obj: any): obj is ISerialisableObject
        {
            return Is.object(obj) && Is.func(obj.getTagName);
        }
    }

    /** Serialises the specified object to a new XML document. */
    export function serialise<T extends object>(obj: T): Document;

    /** Serialises the specified object to the specified existing element. */
    export function serialise<T extends object>(obj: T, el: Element): void;

    /** Serialises the specified object to a new XML document. */
    export function serialise<T extends object>(obj: T, el?: Element): Document|void
    {
        if (obj == null) { return null; }
        let doc: Document;
        let rootEl: Element;
        if (el == null)
        {
            const objType = Type.get(obj);
            if (objType == null)
            {
                throw new Error("Failed to serialise object (has no XML type)");
            }
            doc = document.implementation.createDocument(null, objType, null);
            rootEl = doc.documentElement;
        }
        else
        {
            rootEl = el;
        }
        const props = Property.get(obj, Decorators.MemberType.Instance);
        for (const propName in props)
        {
            if(props[propName]) {
                const propData = props[propName];
                const propValue = obj[propName];
                if (propData.type.serialiser != null && !(propValue == null && propData.ignoreIfNull))
                {
                    const item = propData.type.serialiser(doc != null ? doc : rootEl.ownerDocument, propValue);
                    if (item != null)
                    {
                        write(rootEl, propData.parsedPath, item);
                    }
                }
            }
        }
        if (el == null) { return doc; }
    }

    /** Populates the specified object from an XML document. */
    export function deserialise<T extends object>(obj: T, doc: Document | Element): void
    {
        if (obj == null) { return null; }
        const rootEl = doc instanceof Document ? doc.documentElement : doc;
        const props = Property.get(obj, Decorators.MemberType.Instance);
        for (const propName in props)
        {
            if(props[propName]) {
                const propData = props[propName];
                if (propData.type.deserialiser != null)
                {
                    const rawItem = read(rootEl, propData.parsedPath);
                    obj[propName] = propData.type.deserialiser(rawItem);
                }
            }
        }
    }

    /** Specifies that the property is to be serialised and deserialised as an empty tag. */
    export const EmptyTag: SerialiserPair<string> =
    {
        serialiser: (doc, value) =>
        {
            return doc.createElement(value);
        },
        deserialiser: (obj) =>
        {
            if (obj == null)
            {
                return null;
            }
            else if (obj instanceof Element)
            {
                return obj.tagName;
            }
            else if (Is.array(obj))
            {
                throw new Error(`Failed to deserialise EmptyTag (received array)`);
            }
            else
            {
                throw new Error(`Failed to deserialise EmptyTag (received string)`);
            }
        }
    };

    /** Specifies that the property is to be serialised and deserialised as a string. */
    export const String: SerialiserPair<string> =
    {
        serialiser: (doc, value) =>
        {
            return value;
        },
        deserialiser: (obj) =>
        {
            if (obj == null)
            {
                return null;
            }
            else if (obj instanceof Element)
            {
                return obj.textContent || "";
            }
            else if (Is.array(obj))
            {
                throw new Error(`Failed to deserialise string (received array)`);
            }
            else
            {
                return obj;
            }
        }
    };

    /** Specifies that the property is to be serialised and deserialised as a number. */
    export const Number: SerialiserPair<number> =
    {
        serialiser: (doc, value) =>
        {
            return value != null ? value.toString() : "undefined";
        },
        deserialiser: (obj) =>
        {
            if (obj == null)
            {
                return null;
            }
            else if (obj instanceof Element)
            {
                return parseFloat(obj.textContent || "");
            }
            else if (Is.array(obj))
            {
                throw new Error(`Failed to deserialise number (received array)`);
            }
            else
            {
                return parseFloat(obj);
            }
        }
    };

    /**
     * Specifies that the property is to be serialised and deserialised as a boolean.
     * More customisable than the basic Boolean.
     */
    export function BooleanEx(settings: Partial<BooleanEx.Settings>): SerialiserPair<boolean>
    {
        const actualSettings = { ...BooleanEx.defaultSettings, ...settings };
        return {
            serialiser: (doc, value) =>
            {
                if (value === true) { return actualSettings.serialiseTrueAs; }
                if (value === false) { return actualSettings.serialiseFalseAs; }
                return "undefined";
            },
            deserialiser: (obj) =>
            {
                if (obj == null)
                {
                    return null;
                }
                else if (obj instanceof Element)
                {
                    const txt = obj.textContent && obj.textContent.toLowerCase();
                    if (txt != null)
                    {
                        return actualSettings.acceptedAsTrue.indexOf(txt) !== -1 ? true :
                            actualSettings.acceptedAsFalse.indexOf(txt) !== -1 ? false :
                            actualSettings.default;
                    }
                }
                else if (Is.array(obj))
                {
                    throw new Error(`Failed to deserialise boolean (received array)`);
                }
                else
                {
                    const txt = obj.toLowerCase();
                    return actualSettings.acceptedAsTrue.indexOf(txt) !== -1 ? true :
                        actualSettings.acceptedAsFalse.indexOf(txt) !== -1 ? false :
                        actualSettings.default;
                }
            }
        };
    }

    export namespace BooleanEx
    {
        export interface Settings
        {
            serialiseTrueAs: string;
            serialiseFalseAs: string;
            acceptedAsTrue: string[];
            acceptedAsFalse: string[];
            default: boolean;
        }

        export const defaultSettings: Settings =
        {
            serialiseTrueAs: "true",
            serialiseFalseAs: "false",
            acceptedAsTrue: [ "true", "1", "y", "yes" ],
            acceptedAsFalse: [ "false", "0", "n", "no" ],
            default: false
        };
    }

    /** Specifies that the property is to be serialised and deserialised as a boolean. */
    export const Boolean: SerialiserPair<boolean> = BooleanEx(BooleanEx.defaultSettings);

    /**
     * Specifies that the property is to be serialised and deserialised as an object.
     * There must be a class with an XML.Type decorator matching the tag name.
     */
    export const Object: SerialiserPair<object> =
    {
        serialiser: (doc, value) =>
        {
            if (value == null) { return null; }

            // Create a new element for it
            let el: HTMLElement;
            if (ISerialisableObject.is(value))
            {
                el = doc.createElement(value.getTagName());
            }
            else
            {
                const objType = Type.get(value);
                if (objType == null)
                {
                    throw new Error("Failed to serialise object (has no XML type)");
                }
                el = doc.createElement(objType);
            }
            serialise(value, el);
            return el;
        },
        deserialiser: (obj) =>
        {
            if (obj == null)
            {
                return null;
            }
            else if (obj instanceof Element)
            {
                const clArr = Type.getClassesWithTag(obj.tagName);
                if (clArr.length === 0)
                {
                    throw new Error(`Failed to deserialise object (unknown XML type '${obj.tagName}')`);
                }
                else if (clArr.length > 1)
                {
                    throw new Error(`Failed to deserialise object (more than one class lays claim to XML type '${obj.tagName}')`);
                }
                else
                {
                    const result = new clArr[0]();
                    deserialise(result, obj);
                    return result;
                }
            }
            else if (Is.array(obj))
            {
                throw new Error(`Failed to deserialise object (received array)`);
            }
            else
            {
                throw new Error("Failed to deserialise object (path lead to attribute)");
            }
        }
    };

    /** Specifies that the property is to be serialised and deserialised as an object of the specified class. */
    export function ExplicitObject(type: { new(): object; }): SerialiserPair<object>
    {
        return {
            serialiser: Object.serialiser,
            deserialiser: (obj) =>
            {
                if (obj == null)
                {
                    return null;
                }
                else if (obj instanceof Element)
                {
                    const result = new type();
                    deserialise(result, obj);
                    return result;
                }
                else if (Is.array(obj))
                {
                    throw new Error(`Failed to deserialise object (received array)`);
                }
                else
                {
                    throw new Error("Failed to deserialise object (path lead to attribute)");
                }
            }
        };
    }

    /**
     * Specifies that the property is to be serialised and deserialised as an array of the specified inner type.
     * Optionally this can be restricted to a particular tag name, which is useful if the path points to an element containing multiple different tags.
     */
    export function Array<T>(innerType: SerialiserPair<T>, tagName?: string): SerialiserPair<T[]>
    {
        return {
            serialiser: (doc, value) =>
            {
                if (value == null) { return null; }
                const result: Element[] = [];
                for (const item of value)
                {
                    const serialised = innerType.serialiser(doc, item);
                    if (serialised != null)
                    {
                        if (serialised instanceof Element)
                        {
                            result.push(serialised);
                        }
                        else if (Is.array(serialised))
                        {
                            Log.warn("Serialisation of 2D arrays is not supported");
                        }
                        else
                        {
                            Log.warn("Serialisation of string arrays is not supported (use DelimitedArray!)");
                        }
                    }
                }
                return result;
            },
            deserialiser: (obj) =>
            {
                if (obj == null)
                {
                    return null;
                }
                else if (Is.array(obj))
                {
                    return obj.map((item) => innerType.deserialiser(item));
                }
                else if (obj instanceof Element)
                {
                    const result = [];
                    for (let i = 0, l = obj.childNodes.length; i < l; ++i)
                    {
                        const item = obj.childNodes.item(i);
                        if (item instanceof Element)
                        {
                            if (tagName == null || item.tagName === tagName)
                            {
                                const val = innerType.deserialiser(item);
                                if (val != null)
                                {
                                    result.push(val);
                                }
                            }
                        }
                    }
                    return result;
                }
                else
                {
                    return [ innerType.deserialiser(obj) ];
                }
            }
        };
    }

    /** Specifies that the property is to be serialised and deserialised as an array of the specified inner type, formatted as a string. */
    export function DelimitedArray<T>(innerType: SerialiserPair<T>, delimiter: string = ","): SerialiserPair<T[]>
    {
        return {
            serialiser: (doc, value) =>
            {
                if (value == null) { return null; }
                const result: string[] = [];
                for (const item of value)
                {
                    const serialised = innerType.serialiser(doc, item);
                    if (serialised != null)
                    {
                        if (serialised instanceof Element)
                        {
                            Log.warn("Serialisation of object arrays is not supported (use Array!)");
                        }
                        else if (Is.array(serialised))
                        {
                            Log.warn("Serialisation of 2D arrays is not supported");
                        }
                        else
                        {
                            result.push(serialised);
                        }
                    }
                }
                return result.join(delimiter);
            },
            deserialiser: (obj) =>
            {
                let splitArray: (string | Element)[] | null;
                if (obj == null)
                {
                    splitArray = null;
                }
                else if (Is.array(obj))
                {
                    splitArray = obj;
                }
                else if (obj instanceof Element)
                {
                    if (obj.textContent === "")
                    {
                        splitArray = [];
                    }
                    else
                    {
                        splitArray = obj.textContent
                            .split(delimiter);
                    }
                }
                else
                {
                    if (obj === "")
                    {
                        splitArray = [];
                    }
                    else
                    {
                        splitArray = obj
                            .split(delimiter);
                    }
                }
                if (splitArray)
                {
                    return splitArray
                        .map((item) => innerType.deserialiser(item));
                }
                else
                {
                    return null;
                }
            }
        };
    }    
}