/// <reference path="../controllers/IViewController.ts" />

namespace RS.View
{
    /**
     * The base class for a single transition.
     */
    export abstract class Transition<TSettings extends Transition.Settings = Transition.Settings, TNewView extends AnyView = AnyView, TOldView extends AnyView = AnyView> extends RS.Rendering.Container
    {
        constructor(public readonly settings: TSettings)
        {
            super("Transition");
        }

        /**
         * Starts a transition.
         *
         * @param viewController - controller giving access to current views
         * @param openTask - async callback opening a new view
         * @param closeTask - async callback closing the current view
         */
        public abstract open(viewController: IController, openTask: () => Promise<TNewView>, closeTask: () => Promise<any>, oldView?: TOldView): Promise<any>;
    }

    export namespace Transition
    {
        export interface Settings extends Base.Settings
        {
            duration?: number;
        }
    }
}