namespace RS.View
{
    /**
     * The ResolutionViewer element.
     */
    export class ResolutionViewer extends Flow.BaseElement<ResolutionViewer.Settings>
    {
        @AutoDisposeOnSet protected _list: Flow.List;
        @AutoDisposeOnSet protected _resText: Flow.Label;
        @AutoDisposeOnSet protected _platformText: Flow.Label;

        public constructor(settings: ResolutionViewer.Settings)
        {
            super(settings, undefined);

            this._list = Flow.list.create(settings.innerList, this);
            this._resText = Flow.label.create(settings.resolutionText, this._list);
            this._platformText = Flow.label.create(settings.platformText, this._list);
        }

        public setResolution(res: Math.Size2D, scale: number): void
        {
            this._resText.text = `${Math.round(scale * 100)}% [${res.w}x${res.h}]`;
        }

        public setOrientation(orientation: DeviceOrientation)
        {
            this._platformText.text = `${RS.Device.isMobileDevice ? "MOBILE" : "DESKTOP"} ${DeviceOrientation[orientation].toUpperCase()}`;
        }

    }

    export namespace ResolutionViewer
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            innerList: Flow.List.Settings;
            resolutionText: Flow.Label.Settings;
            platformText: Flow.Label.Settings;
        }

        export const defaultSettings: Settings =
        {
            sizeToContents: true,
            innerList:
            {
                sizeToContents: true,
                dock: Flow.Dock.Fill,
                direction: Flow.List.Direction.TopToBottom,
                legacy: false,
                spacing: Flow.Spacing.all(5),
                contentAlignment: { x: 1.0, y: 0.5 }
            },
            resolutionText:
            {
                font: Fonts.Frutiger.Bold,
                fontSize: 42,
                text: "",
                layers:
                [
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        offset: { x: 0.00, y: 0.00 },
                        color: RS.Util.Colors.white
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        offset: { x: 0.00, y: 0.00 },
                        size: 1
                    }
                ]
            },
            platformText:
            {
                font: Fonts.Frutiger.Bold,
                fontSize: 36,
                text: RS.Device.isMobileDevice ? "MOBILE" : "DESKTOP",
                layers:
                [
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        offset: { x: 0.00, y: 0.00 },
                        color: RS.Util.Colors.white
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        offset: { x: 0.00, y: 0.00 },
                        size: 1
                    }
                ]
            }
        };
    }

    /**
     * The ResolutionViewer element.
     */
    export const resolutionViewer = Flow.declareElement(ResolutionViewer, false, ResolutionViewer.defaultSettings);
}