namespace RS.View
{
    /**
     * Specifies that the view requires the specified asset groups before it can be opened.
     */
    export const RequiresAssets = Decorators.Tag.create<string[]>(Decorators.TagKind.Class);

    /**
     * Specifies a screen name for the view (used for analytics).
     */
    export const ScreenName = Decorators.Tag.create<string>(Decorators.TagKind.Class);

    /**
     * The base class for a single view.
     * A view should contain logic for layouting and displaying a single aspect of the game.
     * For example, a Slots game might have a MainGameView, a PaytableView and a seperate view for each bonus mode.
     * Views are managed by the ViewManager which maintains a stack of all active views.
     * A view is implemented as a PIXI container.
     */
    @HasCallbacks
    export abstract class Base<TSettings extends Base.Settings = Base.Settings, TContext extends object = object, TRuntimeData extends Base.RuntimeData<TContext> = Base.RuntimeData<TContext>> extends Flow.BaseElement<TSettings, TRuntimeData>
    {
        /** The context used by this view. */
        public readonly context: TContext;

        protected _controller: IController;
        protected visibleRegionPanel: Flow.Container;

        private _baseBounds: Rendering.Rectangle;
        @AutoDisposeOnSet private _screenShakeTickHandler: IDisposable | null = null;
        private _screenShakeIntensity: number = 0;

        @AutoDisposeOnSet private _onOrientationChanged: IDisposable;

        /** Gets the controller that this view is attached to. */
        public get controller() { return this._controller; }

        /** Gets the screen name for this view, if it has one. */
        public get screenName() { return ScreenName.get(this) || null; }
        /** Gets or sets the current amount of screen shake to use. */
        public get screenShakeIntensity() { return this._screenShakeIntensity; }
        public set screenShakeIntensity(value)
        {
            if (value === this._screenShakeIntensity) { return; }
            if (value > 0 && this._screenShakeIntensity === 0)
            {
                this._screenShakeTickHandler = ITicker.get().add(this.updateScreenShake);
            }
            else if (value === 0 && this._screenShakeIntensity > 0)
            {
                this._screenShakeTickHandler = null;
                this.resetTransform();
            }
            this._screenShakeIntensity = value;
        }

        public constructor(public readonly settings: TSettings, public readonly runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);
            this.name = Util.getClassName(this);
            this.context = runtimeData.context;
            this._controller = runtimeData.viewController;
            this.visibleRegionPanel = Flow.container.create({ dock: Flow.Dock.None, size: this._size, sizeToContents: false, name: "Visible Region Panel" }, this);
            this.handleViewportResized();
        }

        /** Called when this view has been opened. */
        public onOpened(): void
        {
            this._onOrientationChanged = this.controller.orientation.onChanged(this.handleOrientationChanged);
        }

        /** Called when this view has been resized. */
        public onResized(): void
        {
            this.handleViewportResized();
        }

        /** Called when this view has been closed. */
        public onClosed(): void
        {
            return;
        }

        /** Called when a new view has been opened on top of this one. */
        public onCovered(): void
        {
            return;
        }

        /** Called when the view on top of this one has been closed. */
        public onUncovered(): void
        {
            return;
        }

        /** Cleans up any components used by this view. */
        public dispose(): void
        {
            super.dispose();
            if (this.isDisposed) { return; }
            this._controller = null;
        }

        /** Resets the transform of this view. */
        protected resetTransform()
        {
            this._baseBounds = new Rendering.Rectangle(0, 0, this._controller.width, this._controller.height);
            this.pivot.set(this._controller.width / 2, this._controller.height / 2);
            this.x = this.pivot.x;
            this.y = this.pivot.y;
        }

        /** Tweens screen shake intensity between two values. */
        protected async screenShake(fromIntensity: number, toIntensity: number, duration: number)
        {
            this.screenShakeIntensity = fromIntensity;
            await Tween.get<Base<TSettings, TContext, TRuntimeData>>(this)
                .to({screenShakeIntensity: toIntensity}, duration);
        }

        @Callback
        protected handleViewportResized()
        {
            this.suppressLayouting();
            this._size = { w: this._controller.width, h: this._controller.height };
            this._baseBounds = new Rendering.Rectangle(0, 0, this._controller.width, this._controller.height);
            const visibleRegion = this._controller.visibleRegion;
            this.visibleRegionPanel.size = { w: visibleRegion.w, h: visibleRegion.h };
            this.visibleRegionPanel.x = visibleRegion.x;
            this.visibleRegionPanel.y = visibleRegion.y;
            this.restoreLayouting(true);
        }

        @Callback
        protected handleOrientationChanged(newOrientation: DeviceOrientation)
        {
            return;
        }

        /** Updates screen shake every frame. */
        @Callback
        private updateScreenShake(ev: Ticker.Event)
        {
            const angle = (ev.runTime / 1000) * (Math.PI * 2.0) * 8.0; // 8 revolutions a second
            const offsetX = Math.cos(angle) * 8.0 * this._screenShakeIntensity;
            const offsetY = Math.sin(angle) * 8.0 * this._screenShakeIntensity;

            this.x = Math.round(this.pivot.x + offsetX);
            this.y = Math.round(this.pivot.y + offsetY);

            // We could shift by 8 pixels in any direction per intensity
            // To compensate, adjust scale so that the opposite border will never leave it's bounds

            const scaleFactor = Math.max(
                1.0 + (16.0 * this._screenShakeIntensity) / this._baseBounds.w,
                1.0 + (16.0 * this._screenShakeIntensity) / this._baseBounds.h
            );

            this.scaleX = this.scaleY = scaleFactor;
        }
    }

    export namespace Base
    {
        export type Settings = {};

        export interface RuntimeData<TContext extends object>
        {
            context: TContext;
            viewController: IController;
        }
    }
}
