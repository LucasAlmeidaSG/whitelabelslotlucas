/// <reference path="IViewController.ts" />

namespace RS.View
{
    @HasCallbacks
    class ClickCapture extends Rendering.DisplayObjectContainer implements IClickCapture, SGI.Selenium.ISeleniumComponent, SGI.Selenium.ISeleniumDescriber
    {
        @AutoDisposeOnSet protected _resizeHandler: IDisposable | null = null;
        protected _viewController: ViewController;

        public get seleniumId() { return "FullscreenClickCapture"; }

        public constructor(viewController: ViewController)
        {
            super();

            this.name = "Click Capture";
            this._viewController = viewController;
            this.interactive = true;
            this.buttonMode = true;
            this._resizeHandler = viewController.onResized(this.handleResized);
            this.handleResized();
        }

        /*
        * Gets the state attribute of the selenium component for click capture.
        */
        public seleniumDescribe(): [string, string | number][]
        {
            return [[ "state", this.interactive ? "enabled" : "disabled" ]];
        }

        @Callback
        protected handleResized(): void
        {
            this.hitArea = new Rendering.Rectangle(0, 0, this._viewController.width, this._viewController.height);
        }
    }

    const logger = Logging.getContext("View");

    interface ViewBinding
    {
        settings: Base.Settings;
        runtimeData: Base.RuntimeData<object>;
    }

    @HasCallbacks
    class ViewController implements IController
    {
        public readonly onResized = createSimpleEvent();

        public settings: IController.Settings;

        private _stage: Rendering.IStage;
        private _viewContainer: Rendering.IContainer;
        private _transitionContainer: Rendering.IContainer;
        @AutoDisposeOnSet private _debugLayer: Flow.Container;
        @AutoDisposeOnSet private _resViewer: ResolutionViewer;
        private _viewStack: AnyView[] = [];
        private _isDisposed: boolean = false;
        private _settingsMap = new Util.TypeMap<AnyViewType, ViewBinding>();
        private _screenName: string|null = null;
        private _context: object;

        @AutoDisposeOnSet private _seleniumTicker: IDisposable | null;
        @AutoDisposeOnSet private _seleniumDivTree: SGI.Selenium.SeleniumDivTree | null;

        /** Gets the stage that this controller is for. */
        public get stage() { return this._stage; }

        // Orientation properties used when we aren't given an orientation observable and instead listen to RS.Orientation
        @RS.AutoDisposeOnSet
        private _orientation: Orientation.IReadonlyObservable = null;

        /** Gets the orientation that this controller's viewport is in. */
        public get orientation(): Orientation.IReadonlyObservable
        {
            return (this.settings && this.settings.orientation) || this._orientation;
        }

        /** Gets the width of this controller's viewport. */
        public get width() { return this._stage.stageWidth; }

        /** Gets the height of this controller's viewport. */
        public get height() { return this._stage.stageHeight; }

        /** Gets the currently open top-most view. */
        public get top() { return this._viewStack[this._viewStack.length - 1]; }

        /** Gets the current view stack. */
        public get viewStack(): ReadonlyArray<AnyView> { return this._viewStack; }

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /** Visibility of the debug layer that contains the screen resolution text. */
        public get debugLayerVisibility() { return this._debugLayer ? this._debugLayer.visible : false; }
        public set debugLayerVisibility(newVisibility: boolean)
        {
            if (this._debugLayer)
            {
                this._debugLayer.visible = newVisibility;
            }
        }

        /**
         * Gets the region of the stage (in stage coordinates) that is visible and not clipped by the window.
         */
        public get visibleRegion(): Rendering.Rectangle
        {
            const sz = new Rendering.Rectangle();

            const canvas = this._stage.canvas;
            const rect = canvas.getBoundingClientRect();
            const wW = Viewport.dimensions.w, wH = Viewport.dimensions.h;

            sz.x = Math.floor(Math.clamp(-rect.left / rect.width, 0.0, 1.0) * canvas.width);
            sz.y = Math.floor(Math.clamp(-rect.top / rect.height, 0.0, 1.0) * canvas.height);
            sz.w = wW === 0 ? 0 : (1.0 - Math.clamp(-(wW - (rect.left + rect.width)) / rect.width, 0.0, 1.0)) * canvas.width - sz.x;
            sz.h = wH === 0 ? 0 : (1.0 - Math.clamp(-(wH - (rect.top + rect.height)) / rect.height, 0.0, 1.0)) * canvas.height - sz.y;

            sz.x = sz.x / this._stage.scaleX;
            sz.y = sz.y / this._stage.scaleY;
            sz.w = Math.ceil(sz.w) / this._stage.scaleX;
            sz.h = Math.ceil(sz.h) / this._stage.scaleY;

            // Done
            return sz;
        }

        /** Initialises this controller. */
        public initialise(settings: IController.Settings, context?: object)
        {
            this.settings = settings;
            this._context = context;

            this._stage = settings.stage;
            if (!settings.orientation)
            {
                this._orientation = this.createOrientationObservable();
            }

            if (URL.getParameterAsBool("selenium"))
            {
                this._seleniumDivTree = new SGI.Selenium.SeleniumDivTree(DOM.Component.wrapper.element, this._stage);
                this._seleniumTicker = RS.ITicker.get().add(this.updateSelenium,
                {
                    kind: Ticker.Kind.MultipleFixedStep,
                    data: 100,
                    priority: Ticker.Priority.PostUpdate
                });
                SGI.Selenium.SeleniumControl.createConsole(DOM.Component.wrapper.element, 500, 25);
            }
            else
            {
                this._seleniumDivTree = null;
            }

            this._viewContainer = new Rendering.Container();
            this._viewContainer.name = "View Container";
            this._stage.addChild(this._viewContainer);

            this._transitionContainer = new Rendering.Container();
            this._transitionContainer.name = "Transition Container";
            this._stage.addChild(this._transitionContainer);

            this._debugLayer = new Flow.Container({ dock: Flow.Dock.Fill, sizeToContents: false });
            this._debugLayer.name = "Debug Layer";
            this._stage.addChild(this._debugLayer);
        }

        /** Initialises the debug layer - this must be called once font assets have been loaded. */
        public initialiseDebugLayer(): void
        {
            this._resViewer = resolutionViewer.create({
                ...ResolutionViewer.defaultSettings,
                dock: Flow.Dock.Float,
                floatPosition: { x: 1.0, y: 0.0 },
                dockAlignment: { x: 1.0, y: 0.0 },
                expand: Flow.Expand.Disallowed
            }, this._debugLayer);
            this.updateResViewer();
        }

        /** Specifies settings to use for the specified view type. */
        public bindSettings<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>>(viewType: ConstrainedViewType<TSettings, TContext, TRuntimeData>, settings: TSettings, runtimeData?: TRuntimeData): void
        {
            this._settingsMap.set(viewType, { settings, runtimeData });
        }

        /**
         * Opens the specified view.
         * Will open on top of any existing views.
         */
        public async open<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, index: number, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>);
        public async open<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>);
        public async open<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, p2?: number | Partial<TSettings>, p3?: Partial<TSettings> | ExtraRuntimeData<TRuntimeData, TContext>, p4?: ExtraRuntimeData<TRuntimeData, TContext>)
        {
            let index: number;
            let settingsOverride: Partial<TSettings>;
            let extraRuntimeData: ExtraRuntimeData<TRuntimeData, TContext>;
            if (Is.number(p2))
            {
                index = p2;
                settingsOverride = p3 as Partial<TSettings>;
                extraRuntimeData = p4;
            }
            else
            {
                index = this._viewStack.length;
                settingsOverride = p2;
                extraRuntimeData = p3 as ExtraRuntimeData<TRuntimeData, TContext>;
            }

            const requiredAssets = RequiresAssets.get(viewType);
            if (requiredAssets != null)
            {
                if (this.settings.assetRequirer == null)
                {
                    throw new Error("Tried to open view which requires assets but no asset requirer provided to the view controller!");
                }
                await this.settings.assetRequirer.require(requiredAssets);
            }
            const viewBinding = this.getSettingsForView(viewType);
            if (!viewBinding && !settingsOverride)
            {
                logger.warn(`No settings provided for view '${Util.getClassName(viewType)}'`);
            }
            const settings: TSettings =
            {
                ...(viewBinding && viewBinding.settings || {}),
                ...(settingsOverride as object || {})
            } as any;
            const runtimeData: TRuntimeData =
            {
                viewController: this,
                context: this._context,
                ...(viewBinding && viewBinding.runtimeData || {}),
                ...(extraRuntimeData as object || {})
            } as any;
            const newView: TView = new viewType(settings, runtimeData);
            const top = this.top;
            if (top != null) { top.onCovered(); }

            this._viewStack.splice(index, 0, newView);
            this._viewContainer.addChild(newView, index);

            newView.onOpened();
            if (!newView.isDisposed)
            {
                newView.invalidateLayout();
            }
            const newScreenName = this.computeScreenName();
            if (newScreenName != null && newScreenName !== this._screenName)
            {
                this._screenName = newScreenName;
                const analytics = Analytics.IAnalytics.get();
                if (analytics != null)
                {
                    analytics.getTracker().screen({ name: newScreenName });
                }
            }
            logger.debug(`Opened view '${Util.getClassName(newView)}'`);
            return newView;
        }

        /**
         * Gets the top-most open view of the specified type, or null if no such view is open.
         * @param type
         */
        public get<TView extends AnyView>(type: GenericViewType<TView>): TView | null
        {
            // Crawl the view stack and look for it
            for (let i = this._viewStack.length - 1; i >= 0; --i)
            {
                if (this._viewStack[i] instanceof type)
                {
                    return this._viewStack[i] as TView;
                }
            }
            return null;
        }

        public async changeFromTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(oldView: AnyView | GenericViewType<AnyView>, newViewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>)
        {
            const index = Is.func(oldView) ? Util.indexOf(oldView, this._viewStack, (cand, search) => cand instanceof search) : this.viewStack.indexOf(oldView);
            await this.close(oldView);
            return await this.open(newViewType, index, settingsOverride, extraRuntimeData);
        }

        /**
         * Changes the top view to the specified view.
         */
        public async changeTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>)
        {
            return await this.changeFromTo(this.top, viewType, settingsOverride, extraRuntimeData);
        }

        public async transitionFromTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(oldView: AnyView | GenericViewType<AnyView>, newViewType: ViewType<TSettings, TContext, TRuntimeData, TView>, transition: Transition, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>)
        {
            let oldViewObject: AnyView;
            let index: number;
            if (Is.func(oldView))
            {
                index = Util.indexOf(oldView, this._viewStack, (cand, search) => cand instanceof search);
                oldViewObject = this._viewStack[index];
            }
            else
            {
                index = this.viewStack.indexOf(oldView);
                oldViewObject = oldView;
            }

            let newView: TView;
            const openTask = () => newView = this.open(newViewType, index, settingsOverride, extraRuntimeData);
            const closeTask = () => this.close(oldView);

            this._transitionContainer.addChild(transition);
            await transition.open(this, openTask, closeTask, oldViewObject);
            this._transitionContainer.removeChild(transition);

            return newView;
        }

        /**
         * Changes the top view to the specified view with a transition.
         */
        public async transitionTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, transition: Transition, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>)
        {
            return await this.transitionFromTo(this.top, viewType, transition, settingsOverride, extraRuntimeData);
        }

        public async close(p1?: GenericViewType<AnyView> | AnyView)
        {
            const top = this.top;
            if (p1 instanceof Base)
            {
                if (top === p1)
                {
                    await this.close();
                    return;
                }
                for (let i = 0, l = this._viewStack.length; i < l; ++i)
                {
                    if (this._viewStack[i] === p1)
                    {
                        const view = this._viewStack[i];
                        this._viewStack.splice(i, 1);
                        view.onClosed();
                        this._viewContainer.removeChild(view);
                        logger.debug(`Closed view '${Util.getClassName(view)}'`);
                        view.dispose();
                        return;
                    }
                }
            }
            else if (p1 != null)
            {
                if (top != null && top instanceof p1)
                {
                    await this.close();
                    return;
                }
                for (let i = 0, l = this._viewStack.length; i < l; ++i)
                {
                    if (this._viewStack[i] instanceof p1)
                    {
                        const view = this._viewStack[i];
                        this._viewStack.splice(i, 1);
                        view.onClosed();
                        this._viewContainer.removeChild(view);
                        logger.debug(`Closed view '${Util.getClassName(view)}'`);
                        view.dispose();
                        return;
                    }
                }
                logger.warn(`Tried to close view of specific type (${p1}) but none of that type were open`);
            }
            else
            {
                if (top != null)
                {
                    top.onClosed();
                    this._viewContainer.removeChild(top);
                    logger.debug(`Closed view '${Util.getClassName(top)}'`);
                    top.dispose();
                    this._viewStack.length--;
                    if (this._viewStack.length > 0)
                    {
                        this._viewStack[this._viewStack.length - 1].onUncovered();
                    }
                    return;
                }
                logger.warn(`Tried to close view none were open`);
            }
        }

        /**
         * Captures all clicks until the click capture object is disposed.
         */
        public captureFullScreenClicks(): IClickCapture
        {
            const cc = new ClickCapture(this);
            this._stage.addChild(cc);
            return cc;
        }

        /**
         * Notifies the view controller that the stage has resized.
         */
        public resize(): void
        {
            for (const view of this._viewStack)
            {
                view.onResized();
            }
            this.onResized.publish();
            const visibleRegion = this.visibleRegion;
            this._debugLayer.suppressLayouting();
            this._debugLayer.size = { w: visibleRegion.w, h: visibleRegion.h };
            this._debugLayer.position.set(visibleRegion.x, visibleRegion.y);
            if (this._resViewer)
            {
                this.updateResViewer();
            }
            this._debugLayer.restoreLayouting(true);
        }

        /**
         * Moves top-most view of given type to top of view stack
         */
        public async moveToTop(p1: AnyViewType | AnyView)
        {
            const view = p1 instanceof Base ? p1 : this.get(p1);
            if (!view)
            {
                logger.warn("MoveToTop: View doesn't exist in view stack");
            }
            else
            {
                if (this._viewStack.length === 1)
                {
                    logger.warn("MoveToTop: No other view in view stack");
                    return;
                }

                const index = this._viewStack.indexOf(view);

                if (index === -1)
                {
                    throw new Error("MoveToTop: View doesn't exist in view stack");
                }

                if (index === this._viewStack.length - 1)
                {
                    // View is already on top of view stack
                    // We don't need to log about this, it just means the caller's goal was achieved already
                    return;
                }

                this._viewStack.splice(index, 1);
                this._viewStack.push(view);
                for (let i = 0; i < this._viewStack.length - 1; i++)
                {
                    this._viewStack[i].onCovered();
                }
                view.onUncovered();

                this._viewContainer.moveToTop(view);
            }
        }

        /**
         * Disposes this controller.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }
            this._stage.removeChild(this._viewContainer);
            this._stage = null;
            this._viewContainer.removeAllChildren();
            this._viewContainer = null;
            while (this._viewStack.length > 0)
            {
                const view = this._viewStack.pop();
                view.onClosed();
                view.dispose();
            }
            this._isDisposed = true;
        }

        private createOrientationObservable(): Orientation.IReadonlyObservable
        {
            const obs = new Orientation.Observable(RS.Orientation.currentOrientation);
            const listener = RS.Orientation.onChanged((orientation) => obs.value = orientation);
            RS.Disposable.bind(listener, obs);
            return obs;
        }

        @Callback
        private updateSelenium(): void
        {
            this._seleniumDivTree.draw(1.0, 1.0);
        }

        private getSettingsForView(viewType: AnyViewType): ViewBinding
        {
            return this._settingsMap.get(viewType) || null;
        }

        /**
         * Gets the current screen name.
         */
        private computeScreenName(): string | null
        {
            for (const view of this._viewStack)
            {
                const screenName = view.screenName;
                if (screenName) { return screenName; }
            }
            return null;
        }

        /** Updates the resolution viewer size and orientation */
        private updateResViewer()
        {
            const orientation = this.settings.orientation ? this.settings.orientation.value : RS.Orientation.currentOrientation;
            this._resViewer.setResolution({ w: this._stage.canvas.width, h: this._stage.canvas.height }, this._stage.canvas.width / this.width);
            this._resViewer.setOrientation(orientation);
        }
    }

    IController.register(ViewController);
}
