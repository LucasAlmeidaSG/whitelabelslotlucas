namespace RS.View
{
    /** A type of view with explicit types for settings, context, runtime data and view type. */
    export type ViewType<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>> = { new(settings: TSettings, runtimeData: TRuntimeData): TView; };

    /** An instance of any kind of view. */
    export type AnyView = View.Base<Base.Settings, object>;

    /** An instantiable type of any kind of view. */
    export type AnyViewType = ViewType<Base.Settings, object, View.Base.RuntimeData<object>, AnyView>;

    /** A type of some kind of view, not necessarily instantiable. */
    /* tslint:disable-next-line:ban-types */
    export type GenericViewType<TView extends AnyView> = Function & { prototype: TView; };

    /** A type of view with explicit types for settings, context and runtime data. */
    export type ConstrainedViewType<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext> = View.Base.RuntimeData<TContext>> = ViewType<TSettings, TContext, TRuntimeData, Base<TSettings, TContext, TRuntimeData>>;

    /** The extra runtime data for a given view. */
    export type ExtraRuntimeData<TRuntimeData extends View.Base.RuntimeData<TContext>, TContext extends object> = Omit<TRuntimeData, keyof View.Base.RuntimeData<TContext>>;

    /**
     * A general purpose view controller.
     */
    export interface IController extends Controllers.IController<IController.Settings>
    {
        /** Gets the stage that this controller is for. */
        readonly stage: Rendering.IStage;

        /** Gets the orientation that this controller's viewport is in. */
        readonly orientation: Orientation.IReadonlyObservable;

        /** Gets the width of this controller's viewport. */
        readonly width: number;

        /** Gets the height of this controller's viewport. */
        readonly height: number;

        /** Gets the region of the stage (in stage coordinates) that is visible and not clipped by the window. */
        readonly visibleRegion: Rendering.Rectangle;

        /** Gets the currently open top-most view. */
        readonly top: AnyView;

        /** Gets the current view stack. */
        readonly viewStack: ReadonlyArray<AnyView>;

        /** Sets the visibility of the debug layer that contains the screen resolution text. */
        debugLayerVisibility: boolean;

        /** Initialises this controller. */
        initialise(settings: IController.Settings, context?: object): void;

        /** Initialises the debug layer - this must be called once font assets have been loaded. */
        initialiseDebugLayer(): void;

        /** Specifies settings to use for the specified view type. */
        bindSettings<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>>(viewType: ConstrainedViewType<TSettings, TContext, TRuntimeData>, settings: TSettings): void;

        /** Specifies settings and runtime data to use for the specified view type. */
        bindSettings<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>>(viewType: ConstrainedViewType<TSettings, TContext, TRuntimeData>, settings: TSettings, runtimeData: TRuntimeData): void;

        /** Specifies settings to use for the specified view type. */
        //bindSettings<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settings: TSettings, runtimeData: TRuntimeData): void;

        /**
         * Opens the specified view.
         * Will open on top of any existing views.
         */
        open<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>): PromiseLike<TView>;

        /**
         * Opens the specified view.
         * Will open on top of any existing views.
         */
        open<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settingsOverride: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>): PromiseLike<TView>;

        /**
         * Gets the top-most open view of the specified type, or null if no such view is open.
         */
        get<TView extends AnyView>(type: GenericViewType<TView>): TView | null;

        /**
         * Changes the given view to the specified view.
         */
        changeFromTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(oldView: AnyView | GenericViewType<AnyView>, newViewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>): PromiseLike<TView>;

        /**
         * Changes the top view to the specified view.
         */
        changeTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>): PromiseLike<TView>;

        /**
         * Changes the top view to the specified view.
         */
        changeTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, settingsOverride: TSettings, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>): PromiseLike<TView>;

        /**
         * Changes the given view to the specified view with a transition.
         */
        transitionFromTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(oldView: AnyView | GenericViewType<AnyView>, newViewType: ViewType<TSettings, TContext, TRuntimeData, TView>, transition: Transition, settingsOverride?: Partial<TSettings>, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>): PromiseLike<TView>;

        /**
         * Changes the top view to the specified view with a transition.
         */
        transitionTo<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>, transition: Transition, viewSettingsOverride?: TSettings, extraRuntimeData?: ExtraRuntimeData<TRuntimeData, TContext>): PromiseLike<TView>;

        /**
         * Closes the specified view.
         */
        close(type: GenericViewType<AnyView>): PromiseLike<void>;

        /**
         * Closes the specified view.
         */
        close(view: AnyView): PromiseLike<void>;

        /**
         * Closes the top view.
         */
        close(): PromiseLike<void>;

        /**
         * Captures all clicks until the click capture object is disposed.
         */
        captureFullScreenClicks(): IClickCapture;

        /**
         * Notifies the view controller that the stage has resized.
         */
        resize(): void;

        /**
         * Moves top-most view of given type to top of view stack
         */
        moveToTop<TSettings extends Base.Settings, TContext extends object, TRuntimeData extends View.Base.RuntimeData<TContext>, TView extends Base<TSettings, TContext, TRuntimeData>>(viewType: ViewType<TSettings, TContext, TRuntimeData, TView>): void;
        // TODO: switch to this signature
        // moveToTop(viewType: AnyViewType): void;

        /**
         * Moves the given view to top of view stack
         */
        moveToTop(view: AnyView): void;
    }

    export const IController = Controllers.declare<IController, IController.Settings>(Strategy.Type.Instance);

    export namespace IController
    {
        export interface IAssetRequirer
        {
            require(groups: string[]): PromiseLike<void>;
        }

        export interface Settings
        {
            stage: Rendering.IStage;
            assetRequirer?: IAssetRequirer;
            orientation?: Orientation.IReadonlyObservable;
        }
    }
}
