namespace RS.View
{
    /**
     * Represents a full-screen click capture.
     */
    export interface IClickCapture extends IDisposable
    {
        readonly onClicked:        IPrivateEvent<Rendering.InteractionEventData>;
    }
}