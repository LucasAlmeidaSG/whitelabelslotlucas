namespace RS.Build.Scaffolds
{
    const viewTemplate = `namespace {{namespace}}.Views
{
    /**
     * The {{className}} view.
     */
    // @RS.View.RequiresAssets([ ]) // TODO: Add any required asset groups here
    // @RS.View.ScreenName("") // TODO: Add screen name here
    export class {{className:CamelCase}} extends RS.View.Base<{{className:CamelCase}}.Settings, {{context}}>
    {
        /**
         * Called when this view has been opened.
         */
        public onOpened()
        {
            super.onOpened();
        }

        /**
         * Called when this view has been closed.
         */
        public onClosed()
        {
            super.onClosed();
        }

    }

    export namespace {{className:CamelCase}}
    {
        export interface Settings extends RS.View.Base.Settings
        {
            
        }
    }
}`;

    export const view = new Scaffold();
    view.addParam("context", "Context", null, "RS.Game.Context");
    view.addParam("className", "Class Name", "MyClass");
    view.addTemplate(viewTemplate, "src/views/{{className}}.ts");
    registerScaffold("view", view);
}