/// <reference path="../DtsToExterns.ts" />

namespace RS.ClosureCompilation
{
    const checksumKey = "externs";

    /**
     * Responsible for gathering externs for the closure compiler.
     * Externs define the identifiers that should not be minified.
     */
    export class GatherExterns extends BuildStage.Base
    {
        protected _numExterns: number;
        protected _gennedExterns: number;

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<BuildStage.Result>
        {
            Log.pushContext("GatherExterns");
            this._numExterns = 0;
            this._gennedExterns = 0;

            const time = new RS.Timer();
            const result = await super.execute(moduleList);
            time.pause();

            if (this._numExterns > 0)
            {
                if (this._gennedExterns === this._numExterns)
                {
                    Log.info(`Gathered ${this._numExterns} externs for closure compilation in ${time}, all of which were generated from definition files`);
                }
                else if (this._gennedExterns > 0)
                {
                    Log.info(`Gathered ${this._numExterns} externs for closure compilation in ${time}, of which ${this._gennedExterns} were generated from definition files`);
                }
                else
                {
                    Log.info(`Gathered ${this._numExterns} externs for closure compilation in ${time}`);
                }
            }
            Log.popContext("GatherExterns");
            return result;
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            const allExterns = [`/**\n * @fileoverview Externs for ${module.name}\n * @externs\n */`];

            // The output externs are determined by the content of files at externsPath,
            // plus the content of module definitions

            const files = await this.gatherExternPaths(module);

            const checksum = await Checksum.getComposite(files);
            if (!module.checksumDB.diff(checksumKey, checksum)) { return { workDone: false, errorCount: 0 }; }

            this._numExterns += files.length;

            let errorCount = 0;
            for (const file of files)
            {
                let theseExterns: string;
                try
                {
                    switch (Path.extension(file))
                    {
                        case ".js":
                            theseExterns = await FileSystem.readFile(file);
                            break;
                        case ".d.ts":
                            const dts = await FileSystem.readFile(file);
                            theseExterns = generateExternsForDTS(dts);
                            this._gennedExterns++;
                            break;
                    }
                }
                catch (err)
                {
                    Log.error(`Failed to generate externs for '${file}'`);
                    Log.error(err.stack);
                    ++errorCount;
                    continue;
                }
                allExterns.push(`// ${file}\n${theseExterns}`);
            }

            const externsOutputPath = Path.combine(module.path, "build", "externs.js");
            if (allExterns.length > 1)
            {
                await FileSystem.createPath(Path.directoryName(externsOutputPath));
                await FileSystem.writeFile(externsOutputPath, allExterns.join("\n"));
            }
            else if (await FileSystem.fileExists(externsOutputPath))
            {
                await FileSystem.deletePath(externsOutputPath);
            }

            module.checksumDB.set(checksumKey, checksum);
            await module.saveChecksums();

            return { workDone: true, errorCount };
        }

        protected async gatherExternPaths(module: Module.Base): Promise<string[]>
        {
            const externsPath = Path.combine(module.path, "externs");
            const files = await FileSystem.readFiles(externsPath);

            if (module.info.definitions)
            {
                for (const name in module.info.definitions)
                {
                    if (name)
                    {
                        const dtsPath = Path.combine(module.path, "src", "dependencies", `${name}.d.ts`);
                        if (!await FileSystem.fileExists(dtsPath))
                        {
                            // File not found, probably because module lacks a src folder, just skip
                            continue;
                        }

                        files.push(dtsPath);
                    }
                }
            }

            return files;
        }
    }

    export const gatherExterns = new GatherExterns();
    BuildStage.register({ after: [ BuildStages.compile ] }, gatherExterns);
}