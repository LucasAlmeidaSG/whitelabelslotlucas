namespace RS.ClosureCompilation
{
    import CodeGen = Build.CodeGeneration;
    import Reflection = CodeGen.Reflection;

    interface Context
    {
        data: Reflection.ReflectionData;
        nsPath: CodeGen.Namespace[];
        inlineTypesGenned: Util.Map<boolean>;
        namedPath: CodeGen.Named[];
        writeLine(line?: string): void;
    }

    interface Annotation
    {
        key: string;
        value?: string;
    }

    /**
     * Generates closure compiler externs from the specified dts code.
     * @param dts 
     */
    export function generateExternsForDTS(dts: string): string
    {
        // Reflect it
        const data: Reflection.ReflectionData =
        {
            globalNamespace:
            {
                kind: "namespace",
                name: "g",
                children: []
            }
        };
        Build.CodeGeneration.Reflection.reflect(dts, data);

        // Line writer
        const lines: string[] = [];
        function writer(line?: string) { lines.push(line || ""); }

        // Setup context
        const ctx: Context = { data, writeLine: writer, nsPath: [], inlineTypesGenned: {}, namedPath: [] };

        // Generate for each child of global namespace
        for (const child of data.globalNamespace.children)
        {
            generateExternsForNamed(ctx, child, "");

        }

        // Concat and return
        return lines.join("\n");
    }

    /**
     * Generates closure compiler externs from the specified named entity.
     * @param location String reference to the entity's location. Leave empty if defined in the global scope.
     */
    function generateExternsForNamed(ctx: Context, named: Build.CodeGeneration.Named, location: string): void
    {
        ctx.namedPath.push(named);
        switch (named.kind)
        {
            case "namespace":
            {
                ctx.writeLine(`${location}${named.name} = {};`);
                ctx.nsPath.push(named);
                for (const child of named.children)
                {
                    generateExternsForNamed(ctx, child, `${location}${named.name}.`);
                }
                ctx.nsPath.pop();
                break;
            }
            case "class":
            {
                const annotations: Annotation[] = [];
                if (named.isAbstract) { annotations.push({ key: "abstract" }); }
                annotations.push({ key: "constructor" });
                if (named.extends != null)
                {
                    for (const baseType of named.extends)
                    {
                        const typeStr = generateTypeString(ctx, baseType);
                        if (typeStr != null)
                        {
                            annotations.push({ key: "extends", value: typeStr });
                        }
                    }
                }
                const ctor = named.methods && named.methods.filter((m) => m.name === "constructor")[0] || null;
                const ctorParams = ctor != null ? ctor.signature.parameters : [];
                const paramStr = generateParameters(ctx, ctorParams, annotations);
                generateAnnotations(ctx, annotations);
                ctx.writeLine(`${location}${named.name} = function(${paramStr}) { };`);
                ctx.writeLine(`${location}${named.name}.prototype = {};`);
                
                if (named.properties)
                {
                    for (const prop of named.properties)
                    {
                        if (prop.storageQualifier === CodeMeta.StorageQualifier.Static)
                        {
                            generateExternsForProperty(ctx, prop, `${location}${named.name}`);
                        }
                        else
                        {
                            generateExternsForProperty(ctx, prop, `${location}${named.name}.prototype`);
                        }
                    }
                }

                if (named.methods)
                {
                    for (const method of named.methods)
                    {
                        if (method.name !== "constructor")
                        {
                            if (method.storageQualifier === CodeMeta.StorageQualifier.Static)
                            {
                                generateExternsForMethod(ctx, method, `${location}${named.name}`);
                            }
                            else
                            {
                                generateExternsForMethod(ctx, method, `${location}${named.name}.prototype`);
                            }
                        }
                    }
                }
                break;
            }
            case "interface":
            {
                const annotations: Annotation[] = [];
                annotations.push({ key: "struct" });
                if (named.extends != null)
                {
                    for (const baseType of named.extends)
                    {
                        const typeStr = generateTypeString(ctx, baseType);
                        if (typeStr != null)
                        {
                            annotations.push({ key: "extends", value: typeStr });
                        }
                    }
                }
                generateAnnotations(ctx, annotations);
                ctx.writeLine(`${location}${named.name} = {};`);

                if (named.properties)
                {
                    for (const prop of named.properties)
                    {
                        generateExternsForProperty(ctx, prop, `${location}${named.name}`);
                    }
                }

                if (named.methods)
                {
                    for (const method of named.methods)
                    {
                        generateExternsForMethod(ctx, method, `${location}${named.name}`);
                    }
                }
                break;
            }
            case "enum":
            {
                const annotations: Annotation[] = [];
                let numStr = 0, numNum = 0;
                for (const key in named.enumerators)
                {
                    if (Is.string(named.enumerators[key]))
                    {
                        numStr++;
                    }
                    else if (Is.number(named.enumerators[key]))
                    {
                        numNum++;
                    }
                }
                const typeArr: string[] = [];
                if (numStr > 0) { typeArr.push("string"); }
                if (numNum > 0) { typeArr.push("number"); }
                annotations.push({ key: "enum", value: typeArr.length > 0 ? typeArr.join(" | ") : "{any}" });
                generateAnnotations(ctx, annotations);
                ctx.writeLine(`${location}${named.name} = {`);
                const lines: string[] = [];
                for (const key in named.enumerators)
                {
                    if(named.enumerators[key]) {
                        const val = named.enumerators[key];
                        if (Is.string(val))
                        {
                            lines.push(`\t${key}: "${val}"`);
                        }
                        else if (Is.number(val))
                        {
                            lines.push(`\t${key}: ${val}`);
                        }
                    }
                }
                ctx.writeLine(lines.join(",\n"));
                ctx.writeLine("};");
                
                break;
            }
            case "var":
            {
                const annotations: Annotation[] = [];
                const typeStr = generateTypeString(ctx, named.type);
                if (typeStr != null)
                {
                    annotations.push({ key: "type", value: `{${typeStr}}` });
                }
                generateAnnotations(ctx, annotations);
                if (location)
                {
                    ctx.writeLine(`${location}${named.name};`);
                }
                else
                {
                    ctx.writeLine(`var ${named.name};`);
                }

                break;
            }
            case "func":
            {
                const annotations: Annotation[] = [];
                const paramStr = generateParameters(ctx, named.signature.parameters, annotations);
                if (named.signature.returnType)
                {
                    const typeStr = generateTypeString(ctx, named.signature.returnType);
                    if (typeStr != null)
                    {
                        annotations.push({ key: "return", value: `{${typeStr}}` });
                    }
                }
                generateAnnotations(ctx, annotations);
                if (location)
                {
                    ctx.writeLine(`${location}${named.name} = function(${paramStr}) { };`);
                }
                else
                {
                    ctx.writeLine(`function ${named.name}(${paramStr}) { };`);
                }

                break;
            }
        }
        ctx.namedPath.pop();
    }

    function generateParameters(ctx: Context, params: CodeMeta.FunctionParameter[], annotations?: Annotation[]): string
    {
        const result: string[] = [];
        for (const p of params)
        {
            let name = p.name;
            if (name === "this") { name = "_this"; }
            if (annotations)
            {
                const type = p.type && generateTypeString(ctx, p.type) || null;
                if (type != null)
                {
                    annotations.push({ key: "param", value: `{${type}} ${name}` });
                }
                else
                {
                    annotations.push({ key: "param", value: `${name}` });
                }
            }
            result.push(name);
        }
        return result.join(", ");
    }

    /** 
     * Generates a property access string i.e. .prop or ["prop"]
     * @param name The raw property name including quotes for string literals e.g. prop or "prop"
     */
    function generatePropertyAccess(name: string): string
    {
        if (!name) { throw new Error("Internal error: empty property name"); }
        // Is it a string or number literal?
        const quoteCode = 34; // Avoids the sourcemap error
        if (name.charCodeAt(0) === quoteCode || !isNaN(parseInt(name[0])))
        {
            return `[${name}]`;
        }
        else
        {
            return `.${name}`;
        }
    }

    /**
     * Generates closure compiler externs from the specified method.
     * @param location String reference to the entity's location. Leave empty if defined in the global scope.
     */
    function generateExternsForProperty(ctx: Context, prop: CodeMeta.PropertyDeclaration, location: string): void
    {
        const annotations: Annotation[] = [];
        switch (prop.accessQualifier)
        {
            case CodeMeta.AccessQualifier.Private:
                annotations.push({ key: "private" });
                break;
            case CodeMeta.AccessQualifier.Public:
                annotations.push({ key: "public" });
                break;
            case CodeMeta.AccessQualifier.Protected:
                annotations.push({ key: "protected" });
                break;
        }
        if (prop.type)
        {
            const typeStr = generateTypeString(ctx, prop.type);
            if (typeStr != null)
            {
                annotations.push({ key: "type", value: `{${typeStr}}` });
            }
        }
        generateAnnotations(ctx, annotations);
        const access = generatePropertyAccess(prop.name);
        ctx.writeLine(`${location}${access};`);
    }

    /**
     * Generates closure compiler externs from the specified method.
     * @param location String reference to the method's location. Leave empty if defined in the global scope.
     */
    function generateExternsForMethod(ctx: Context, method: CodeMeta.MethodDeclaration, location: string): void
    {
        const annotations: Annotation[] = [];
        switch (method.accessQualifier)
        {
            case CodeMeta.AccessQualifier.Private:
                annotations.push({ key: "private" });
                break;
            case CodeMeta.AccessQualifier.Public:
                annotations.push({ key: "public" });
                break;
            case CodeMeta.AccessQualifier.Protected:
                annotations.push({ key: "protected" });
                break;
        }
        const paramStr = generateParameters(ctx, method.signature.parameters, annotations);
        if (method.signature.returnType)
        {
            const typeStr = generateTypeString(ctx, method.signature.returnType);
            if (typeStr != null)
            {
                annotations.push({ key: "return", value: `{${typeStr}}` });
            }
        }
        generateAnnotations(ctx, annotations);
        const access = generatePropertyAccess(method.name);
        ctx.writeLine(`${location}${access} = function(${paramStr}) { };`);
    }

    /** Generates closure compiler extern documenting comments with the specified annotations. */
    function generateAnnotations(ctx: Context, annotations: Annotation[]): void
    {
        if (annotations.length === 0) { return; }
        ctx.writeLine("/**");
        for (const annotation of annotations)
        {
            if (annotation.value != null)
            {
                ctx.writeLine(` * @${annotation.key} ${annotation.value}`);
            }
            else
            {
                ctx.writeLine(` * @${annotation.key}`);
            }
        }
        ctx.writeLine(" */");
    }

    function generateTypeString(ctx: Context, type: CodeMeta.TypeRef): string|null
    {
        if (type.kind === "basictyperef" && type.name === "void") { return null; }
        return stringifyType(ctx, type);
    }

    /**
     * Gets a string representation of the specified type.
     * @param type 
     */
    function stringifyType(ctx: Context, type: CodeMeta.TypeRef = null, useBrackets: boolean = false): string
    {
        let value: string;
        let brackets: boolean;
        if (type == null)
        {
            value = "*";
            brackets = false;
        }
        else
        {
            // CodeGen.normaliseTypeRef(type, ctx.nsPath);
            switch (type.kind)
            {
                case CodeMeta.TypeRef.Kind.Raw:
                    value = type.name;
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.LiteralNumber:
                    value = "number";
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.LiteralString:
                    value = "string";
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.LiteralBool:
                    value = "boolean";
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.WithTypeArgs:
                    value = `${stringifyType(ctx, type.inner, true)}<${type.genericTypeArgs.map((t) => stringifyType(ctx, t)).join(", ")}>`;
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.Array:
                    value = `${stringifyType(ctx, type.inner, true)}[]`;
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.FixedArray:
                    value = `*[]`;
                    brackets = false;
                    break;
                case CodeMeta.TypeRef.Kind.Intersect:
                    value = type.inners.map((t) => stringifyType(ctx, t, true)).join(" & ");
                    brackets = true;
                    break;
                case CodeMeta.TypeRef.Kind.Union:
                    value = type.inners.map((t) => stringifyType(ctx, t, true)).join(" | ");
                    brackets = true;
                    break;
                case CodeMeta.TypeRef.Kind.Function:
                    value = `function(${type.signature.parameters.map((p) => `${stringifyType(ctx, p.type)}`).join(", ")}): ${stringifyType(ctx, type.signature.returnType)}`;
                    brackets = true;
                    break;
                case CodeMeta.TypeRef.Kind.Is:
                    value = "boolean";
                    brackets = true;
                    break;
                // case CodeGen.TypeRef.Kind.TypeOf:
                //     // type.typeRef
                //     break;
                case CodeMeta.TypeRef.Kind.Inline:
                    const named = ctx.namedPath[ctx.namedPath.length - 1];
                    const inlineTypeName = `${named.name}_InlineType`;
                    if (!ctx.inlineTypesGenned[inlineTypeName])
                    {
                        generateAnnotations(ctx, [
                            { key: "struct" }
                        ]);
                        ctx.writeLine(`var ${inlineTypeName} = {};`);
                        if (type.properties)
                        {
                            for (const prop of type.properties)
                            {
                                generateExternsForProperty(ctx, prop, `${inlineTypeName}`);
                            }
                        }
                        if (type.methods)
                        {
                            for (const method of type.methods)
                            {
                                generateExternsForMethod(ctx, method, `${inlineTypeName}`);
                            }
                        }
                        ctx.inlineTypesGenned[inlineTypeName] = true;
                    }
                    value = inlineTypeName;
                    brackets = false;
                    break;
                default:
                    // Log.debug(`Unhandled type ref '${type.kind}'`);
                    value = "*";
                    brackets = false;
                    break;
            }
        }
        if (brackets && useBrackets)
        {
            return `(${value})`;
        }
        else
        {
            return value;
        }
    }
}