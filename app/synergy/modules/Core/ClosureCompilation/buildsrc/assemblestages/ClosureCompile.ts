namespace RS.AssetPipeline.AssembleStages
{
    /**
     * Responsible for closure compiling the built js.
     */
    export class ClosureCompile extends AssembleStage.Base
    {
        protected _allExterns: string[];

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            this._allExterns = [];

            await super.execute(settings, moduleList);

            Log.pushContext("ClosureCompile");

            if (!await Command.exists("java"))
            {
                Log.popContext("ClosureCompile");
                throw new Error("Java not found, cannot proceed with closure compilation.");
            }

            // Write externs
            const tmpExternsPath = Path.combine(settings.outputDir, "js", "externs.js");
            await FileSystem.writeFile(tmpExternsPath, this._allExterns.join("\n"));

            const promises: PromiseLike<void>[] = [];

            // Closure compile main game
            const appJS = Path.combine(settings.outputDir, "js", "app.js");
            promises.push(this.doClosureCompile(appJS, tmpExternsPath));

            // Closure compile help src if found
            const helpJS = Path.combine(settings.outputDir, "help", "libs.js");
            if (await FileSystem.fileExists(helpJS)) { promises.push(this.doClosureCompile(helpJS, tmpExternsPath)); }

            try
            {
                await Promise.all(promises);
            }
            finally
            {
                Log.popContext("ClosureCompile");
            }
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // Grab externs
            try
            {
                this._allExterns.push(await FileSystem.readFile(Path.combine(module.path, "build", "externs.js")));
            }
            catch
            {
                // Probably didn't have any externs
            }
        }

        protected async doClosureCompile(sourceFile: string, externsFile: string)
        {
            Log.info(`Starting closure compilation of ${Path.baseName(sourceFile)}`);

            const jarPath = Path.combine(Module.getModule("Core.ClosureCompilation").path, "thirdparty", "closure-compiler.jar"); // todo: better way of fetching "this" module?

            const mapFile = `${sourceFile}.map`;

            const fileName = sourceFile.substr(0, sourceFile.lastIndexOf(Path.extension(sourceFile)));
            const minifiedFile = `${fileName}-min.js`;
            const minifiedMapFile = `${fileName}-min.js.map`;

            const timer = new Timer();
            await this.runJava([
                "-jar", jarPath,
                "--compilation_level", "ADVANCED_OPTIMIZATIONS",
                "--language_in", "ECMASCRIPT5_STRICT",
                "--language_out", "ECMASCRIPT5_STRICT",
                "--warning_level", "QUIET",
                "--externs", externsFile,
                "--jscomp_off", "duplicate",
                "--jscomp_off", "checkVars",
                "--js", sourceFile,
                // "--source_map_input", srcMapInPath,
                "--create_source_map", minifiedMapFile,
                "--source_map_include_content", "true",
                "--js_output_file", minifiedFile
            ], null, false);

            FileSystem.cache.set(minifiedFile, { type: FileIO.FileSystemCache.NodeType.File, content: null });
            FileSystem.cache.set(minifiedMapFile, { type: FileIO.FileSystemCache.NodeType.File, content: null });

            const elapsed = timer.elapsed;
            Log.info(`Finished ${Path.baseName(sourceFile)} after ${Math.round(elapsed / 10.0) / 100.0}s`);

            const compiledJS = await FileSystem.readFile(minifiedFile);
            await FileSystem.writeFile(sourceFile, `${compiledJS}\n//# sourceMappingURL=${Path.baseName(mapFile)}`);

            // await FileSystem.copyFile(jsOutPath, jsInPath);
            await FileSystem.copyFile(minifiedMapFile, mapFile);

            await FileSystem.deletePath(minifiedFile);
            await FileSystem.deletePath(minifiedMapFile);
        }

        protected async runJava(args: string[], cwd?: string, logOutput?: boolean): Promise<string>
        {
            const output = await Command.run("java", args, cwd, logOutput);
            // Because Java apparently doesn't just write errors to stderr like a normal program
            if (output.indexOf("Error occurred") !== -1)
            {
                throw new Error(output);
            }
            return output;
        }
    }

    export const closureCompile = new ClosureCompile();
    AssembleStage.register({ after: [ RS.AssembleStages.code ] }, closureCompile);
}