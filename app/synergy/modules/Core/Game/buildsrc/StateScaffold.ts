namespace RS.Build.Scaffolds
{
    const stateTemplate = `namespace {{namespace}}.States
{
    /**
     * The {{className}} state.
     */
    @RS.State.Name(State.{{className}})
    export class {{className}}State extends RS.State.Base<{{context}}>
    {
        public onEnter(): void
        {
            super.onEnter();

        }
    }
}`;

    export const slotsState = new Scaffold();
    slotsState.addParam("className", "Class Name", "MyClass");
    slotsState.addParam("context", "Context", null, "RS.Slots.Game.Context");
    slotsState.addTemplate(stateTemplate, "src/states/{{className}}.ts");
    registerScaffold("state", slotsState);
}
