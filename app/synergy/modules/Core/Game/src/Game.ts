/// <reference path="views/Loading.ts" />
/// <reference path="generated/Translations.ts" />
/// <reference path="DialogDelegate.ts" />

namespace RS
{
    const versionFromBuild = "{!GAME_VERSION!}";
    const synergyHashFromBuild = "{!SYNERGY_HASH!}";

    // When the document is ready, start the platform off
    //DOM.Document.readyState.for(true).then(() => IPlatform.get());

    const defaultSettings: Partial<Game.Settings> =
    {
        stateMachine: { initialState: null, states: [] },
        assetGroupPriorities: {},
        loadingView: Views.Loading,
        replayDialogDisplayTime: 1000
    };

    /**
     * The core game class that brings everything together.
     * Is it intended that the developer extends this class and then instantiates that in an @RS.Init() handler.
     */
    @HasCallbacks
    export abstract class Game implements IDisposable
    {
        public readonly settings: Game.Settings;

        protected _isDisposed: boolean;

        @AutoDispose protected _autoscaleContainer: DOM.AutoscaleContainer;
        @AutoDispose protected _stage: RS.Rendering.IStage;
        @AutoDispose protected _canvas: DOM.Canvas;
        @AutoDispose protected _cover: DOM.Component;
        @AutoDisposeOnSet protected _coverPauseHandle: IDisposable;

        @AutoDisposeOnSet protected _uiController: UI.IUIController;
        @AutoDisposeOnSet protected _viewController: View.IController;
        @AutoDisposeOnSet protected _assetController: Asset.IController;
        @AutoDisposeOnSet protected _musicController: Music.IController;
        @AutoDisposeOnSet protected _minWagerDurationController: IMinWagerDurationController;

        @AutoDispose protected _uiContainer: Flow.Container;

        @AutoDisposeOnSet protected _pageShownHandler: IDisposable;
        @AutoDisposeOnSet protected _pageHiddenHandler: IDisposable;
        @AutoDisposeOnSet protected _orientationChangeHandler: IDisposable;
        @AutoDisposeOnSet protected _connectionStatusChangedHandler: IDisposable;
        @AutoDisposeOnSet protected _uiEnabledArbiterHandle: IDisposable;
        @AutoDisposeOnSet protected _viewOnlyModeUiEnabledArbiterHandle: IDisposable;
        @AutoDisposeOnSet protected _handlePlatformGameStateChanged: IDisposable;

        @AutoDisposeOnSet protected _handleBalanceChanged: IDisposable;
        @AutoDisposeOnSet protected _handleWinAmountChanged: IDisposable;
        @AutoDisposeOnSet protected _handleTotalBetChanged: IDisposable;
        @AutoDisposeOnSet protected _handleViewportResized: IDisposable;

        @AutoDisposeOnSet protected _canSkipHandler: IDisposable;
        @AutoDisposeOnSet protected _turboHandle: RS.ArbiterHandle<boolean>;

        @AutoDisposeOnSet protected _pauseWhenPageHiddenHandler: IArbiterHandle<boolean>;

        @AutoDispose protected _fpsCounter: DOM.FramerateCounter;

        protected _context: Game.Context;
        protected _engine: Engine.IEngine<Models, object>;
        protected _models: Models;
        protected _stateMachine: State.Machine<Game.Context>;
        protected _arbiters: Game.Arbiters;
        protected _observables: Game.Observables;
        protected _locale: Localisation.Locale;
        protected _localeCode: string;
        protected _gameVersion: Version;

        protected _currencyFormatter: Localisation.CurrencyFormatter;

        @AutoDispose protected _loadingAssetsDialog: DialogDelegate;
        @AutoDispose protected _connectionLostDialog: DialogDelegate;

        @AutoDisposeOnSet protected _idleTimer: RS.Tween<Game>;

        @AutoDispose private readonly _orientation = new Orientation.Observable(null);
        /**
         * Observable for the orientation of the game.
         * This is dependent on both device orientation (via RS.Orientation) and game settings.
         * For example, if the game settings do not define a portrait orientation, this value will always be landscape, even if the device orientation is portrait.
         */
        public get orientation(): Orientation.IReadonlyObservable { return this._orientation; }

        @AutoDispose private readonly __dumpHandler: IDisposable;
        /** Tracks whether the ticker was enabled when the page was hidden. */
        private __tickerWasEnabled: boolean;
        /** Game pause declaration on page hide. */
        @RS.AutoDisposeOnSet private __pageHiddenPauseHandle: RS.IDisposable;

        /** Gets the context. */
        public get context() { return this._context; }

        /** Gets the autoscale container. */
        public get autoscaleContainer() { return this._autoscaleContainer; }

        /** Gets the view controller. */
        public get viewController() { return this._viewController; }

        /** Gets the asset controller. */
        public get assetController() { return this._assetController; }

        /** Gets the master state machine. */
        public get stateMachine() { return this._stateMachine; }

        /** Gets the arbiters for this game. */
        public get arbiters() { return this._arbiters; }

        /** Gets the observables for this game. */
        public get observables() { return this._observables; }

        /** Gets or sets the locale for this game. */
        public get locale() { return this._locale; }
        public set locale(value)
        {
            if (this._locale === value) { return; }
            this._locale = value;
            this.handleLocaleChanged(value);
        }

        /** Gets the locale code for this game (e.g. "en_GB"). */
        public get localeCode() { return this._localeCode; }

        /** Gets the engine interface for this game. */
        public get engine() { return this._engine; }

        /** Gets the models for this game. */
        public get models() { return this._models; }

        /** Gets the UI controller for this game. */
        public get uiController() { return this._uiController; }

        /** Gets the minimum wager duration controller for this game. */
        public get minWagerDurationController() { return this._minWagerDurationController; }

        /** Gets the current dimensions of the game, factoring in orientation. */
        public get orientationData()
        {
            return this.orientation.value === DeviceOrientation.Landscape ? this.settings.landscape : this.settings.portrait;
        }

        /** Gets or sets the version of this game. */
        public get gameVersion() { return this._gameVersion; }

        /** Gets if this game has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets or sets the currency formatter for this game. */
        public get currencyFormatter() { return this._currencyFormatter; }
        public set currencyFormatter(value)
        {
            if (this._currencyFormatter === value) { return; }
            this._currencyFormatter = value;
            this.handleCurrencyFormatterChanged(value);
        }

        public constructor(settings: Game.Settings)
        {
            this.settings = { ...defaultSettings, ...settings };
            this.initialiseStorage();
            this.createArbiters();
            this.createContext();
            this.createDOM();
            this.createObservables();
            this.initialiseModels();
            this.initialiseEngine();
            this.createControllers();
            this.createUIContainer();
            this.initialiseAssetPriorities();
            this.createStateMachine();
            this.initialiseViews();
            this.initialiseDialogs();

            const dump = RS.IDump.get();
            const gameDump = dump.getSubDump("Game");
            gameDump.clearOnSave = true;
            this.__dumpHandler = dump.onPreSave(() => this.dump(gameDump));
        }

        /**
         * Launches core game logic.
         */
        public async run()
        {
            // Initialise platform
            this.initialisePlatform();

            // Get version and locale
            this._gameVersion = await this.determineGameVersion();
            this._localeCode = IPlatform.get().localeCode;

            // Initialise analytics
            this.initialiseAnalytics();

            // Switch to loading state
            this._stateMachine.switchToState(GameState.Loading);
        }

        /**
         * Handles any error contained within the model.
         * Returns true if the game should continue/recover.
         * Async.
         */
        public async handleError()
        {
            const errModel = this.models.error;
            if (errModel.type === RS.Models.Error.Type.None) { return; }
            if (errModel.type === RS.Models.Error.Type.Server)
            {
                const errStr = Localisation.resolveLocalisableString(this._locale, errModel.message);
                switch (RS.IPlatform.get().networkErrorHandlingMode)
                {
                    case PlatformMessageHandlingMode.Ignore:
                        Log.warn(errStr);
                        return true;
                    case PlatformMessageHandlingMode.PassToPlatform:
                        Log.warn(errStr);
                        await RS.IPlatform.get().handleNetworkError(errStr);
                        return true;
                }
            }
            if (errModel.type === RS.Models.Error.Type.Client)
            {
                const errStr = Localisation.resolveLocalisableString(this._locale, errModel.message);
                switch (RS.IPlatform.get().generalErrorHandlingMode)
                {
                    case PlatformMessageHandlingMode.Ignore:
                        Log.warn(errStr);
                        return true;
                    case PlatformMessageHandlingMode.PassToPlatform:
                        Log.warn(errStr);
                        await RS.IPlatform.get().handleGeneralError(errStr);
                        if (errModel.options == null) { return true; }
                        if (errModel.options.length !== 1) { return true; }
                        return this.actionErrorOption(errModel.options[0].type);
                }
            }
            const baseSettings = this.settings.errorDialog || Flow.Dialog.defaultSettings;
            const dialogSettings: Flow.Dialog.Settings =
            {
                ...baseSettings,
                titleLabel: { ...baseSettings.titleLabel, text: errModel.title },
                messageLabel: { ...baseSettings.messageLabel, text: errModel.message },
                buttons: errModel.options.map((o) =>
                {
                    let buttonSettings: Flow.Button.Settings;
                    switch (o.type)
                    {
                        case RS.Models.Error.OptionType.Continue:
                            buttonSettings = this.settings.positiveDialogButton || this.settings.neutralDialogButton || Flow.Button.defaultSettings;
                            break;
                        case RS.Models.Error.OptionType.Close:
                            buttonSettings = this.settings.negativeDialogButton || this.settings.neutralDialogButton || Flow.Button.defaultSettings;
                            break;
                        case RS.Models.Error.OptionType.Refresh:
                            buttonSettings = this.settings.negativeDialogButton || this.settings.neutralDialogButton || Flow.Button.defaultSettings;
                            break;
                        default:
                            buttonSettings = this.settings.neutralDialogButton || RS.Flow.Button.defaultSettings;
                            break;
                    }
                    return { ...buttonSettings, textLabel: { ...buttonSettings.textLabel, text: o.text } };
                })
            };
            const optIndex = await this.displayDialog(dialogSettings);
            if (errModel.options == null) { return true; }
            const opt = errModel.options[optIndex];
            return this.actionErrorOption(opt.type);
        }

        public displayDialog(dialogSettings: Flow.Dialog.Settings, autoCloseTime?: number): PromiseLike<number>;
        public displayDialog(dialogSettings: Flow.Dialog.Settings, animator: Views.Dialog.IAnimator, autoCloseTime?: number): PromiseLike<number>;
        /**
         * Displays a dialog and waits for the user to select an option.
         * Async.
         * @param dialogSettings
         */
        public async displayDialog(dialogSettings: Flow.Dialog.Settings, animatorOrAutoCloseTime?: Views.Dialog.IAnimator | number, autoCloseTime?: number): Promise<number>
        {
            if (this.models.state.isReplay)
            {
                // Remove all dialog buttons in replay
                dialogSettings.buttons = [];

                // Resolve arguments in order to set auto close time unless overridden
                if (animatorOrAutoCloseTime !== undefined)
                {
                    if (Is.number(animatorOrAutoCloseTime))
                    {
                        // Move number into autoCloseTime
                        autoCloseTime = animatorOrAutoCloseTime;

                        // Let the super method handle the undefined animator
                        animatorOrAutoCloseTime = undefined;
                    }
                }

                // Apply default auto-close time if not overridden
                if (autoCloseTime == null)
                {
                    autoCloseTime = this.settings.replayDialogDisplayTime;
                }
            }

            let animator: Views.Dialog.IAnimator = this.settings.defaultDialogAnimator;
            if (animatorOrAutoCloseTime !== undefined)
            {
                if (Is.number(animatorOrAutoCloseTime))
                {
                    autoCloseTime = animatorOrAutoCloseTime;
                }
                else
                {
                    animator = animatorOrAutoCloseTime;
                }
            }

            const settings: Views.Dialog.Settings = { dialogSettings, animator, displayTime: autoCloseTime };
            const view = await this._viewController.open(Views.Dialog, settings);
            const option = await view.onOptionSelected;
            await this._viewController.close();
            return option;
        }

        /**
         * Displays an error message dialog and waits for the user to select OK.
         * Async.
         * @param errorMessage
         */
        public async displayErrorDialog(errorMessage: string, animator?: Views.Dialog.IAnimator)
        {
            const baseSettings = this.settings.errorDialog || Flow.Dialog.defaultSettings;
            const settings: Flow.Dialog.Settings =
            {
                ...baseSettings,
                titleLabel: { ...baseSettings.titleLabel, text: "ERROR" },
                messageLabel: { ...baseSettings.messageLabel, text: errorMessage }
            };
            return await this.displayDialog(settings, animator);
        }

        public async displayMaxWinDialog()
        {
            await this.displayDialog(
            {
                ...RS.Flow.Dialog.defaultSettings,
                ...this.settings.maxWinDialog,
                titleLabel:
                {
                    ...RS.Flow.Dialog.defaultSettings.titleLabel,
                    ...this.settings.maxWinDialog && this.settings.maxWinDialog.titleLabel,
                    text: Translations.MaxWin.Title
                },
                messageLabel:
                {
                    ...RS.Flow.Dialog.defaultSettings.messageLabel,
                    ...this.settings.maxWinDialog && this.settings.maxWinDialog.messageLabel,
                    text: Translations.MaxWin.Message.get(this.locale, { maxWinAmount: this.currencyFormatter.format(this.models.config.maxWinAmount) })
                },
                buttons: []
            }, 1400);
        }

        public initialiseUI(): void
        {
            this._uiController.attach(this._uiContainer);
            this._uiContainer.invalidateLayout();
        }

        public resetTurboMode(): void
        {
            this._turboHandle = null;
        }

        /** Disposes this game. */
        public dispose()
        {
            if (this._isDisposed) { return; }
            for (const key in this._arbiters)
            {
                const arbiter = this._arbiters[key];
                if (Is.disposable(arbiter))
                {
                    arbiter.dispose();
                }
            }
            this._arbiters = null;
            this._isDisposed = true;
        }

        /** Called on locale changed. */
        protected handleLocaleChanged(locale: RS.Localisation.Locale)
        {
            this._uiContainer.locale = locale;
        }

        protected handleCurrencyFormatterChanged(currencyFormatter: RS.Localisation.CurrencyFormatter)
        {
            this._uiContainer.currencyFormatter = currencyFormatter;
        }

        /** Initialises game WebStorage. */
        protected initialiseStorage()
        {
            const { storageKey } = this.settings;
            if (storageKey)
            {
                RS.Storage.init(storageKey, "{!GAME_VERSION!}");
            }
        }

        /** Gets the game version. */
        protected async determineGameVersion()
        {
            return Version.fromString(versionFromBuild);
        }

        protected createArbiters()
        {
            this._arbiters =
            {
                pause: new Arbiter<boolean>(Arbiter.OrResolver, false),
                mute: new Arbiter<boolean>(Arbiter.OrResolver, false),
                uiEnabled: new Arbiter<boolean>(Arbiter.AndResolver, false),
                viewOnlyMode: new Arbiter<boolean>(Arbiter.OrResolver, false),
                showButtonArea: new Arbiter<boolean>(Arbiter.AndResolver, false),
                messageText: new Arbiter<Localisation.LocalisableString>(Arbiter.StackResolver, ""),
                music: new Arbiter<Music.Definition | null>(Arbiter.StackResolver, null),
                musicState: new Arbiter<Music.State>(Arbiter.MaxResolver, Music.State.Previous),
                musicVolume: new Arbiter<number>(Arbiter.MinResolver, 1.0),
                canSpin: new Arbiter<boolean>(Arbiter.AndResolver, false),
                canSkip: new Arbiter<boolean>(Arbiter.AndResolver, false),
                spinOrSkip: new Arbiter<"spin"|"skip">(Arbiter.StackResolver, "spin"),
                canChangeBet: new Arbiter<boolean>(Arbiter.AndResolver, false),
                betShouldSkip: new Arbiter<boolean>(Arbiter.AndResolver, false),
                turboMode: new Arbiter<boolean>(Arbiter.AndResolver, false),
                showTurboUI: new Arbiter<boolean>(Arbiter.AndResolver, false),
                paytableVisible: new Arbiter<boolean>(Arbiter.OrResolver, false),
                showMeterPanel: new Arbiter<boolean>(Arbiter.AndResolver, true),
                showMessageBar: new Arbiter<boolean>(Arbiter.AndResolver, false)
            };

            this._arbiters.pause.onChanged(this.handlePauseStateChanged);
            this._arbiters.mute.onChanged(this.handleMuteStateChanged);
            this._arbiters.viewOnlyMode.onChanged(this.handleViewOnlyModeStateChanged);

            this._pageShownHandler = Visibility.onPageShown(this.handlePageShown);
            this._pageHiddenHandler = Visibility.onPageHidden(this.handlePageHidden);

            const turboModeHandler = this._arbiters.turboMode.onChanged(this.handleTurboModeChanged);
            RS.Disposable.bind(turboModeHandler, this);
        }

        @Callback
        protected handleTurboModeChanged(enabled: boolean)
        {
            // Turbo mode enabled -> min duration DISabled
            this._minWagerDurationController.enabled = !enabled;
        }

        @Callback
        protected handlePageShown()
        {
            this.__pageHiddenPauseHandle = null;
            RS.ITicker.get().enabled = this.__tickerWasEnabled;
        }

        @Callback
        protected handlePageHidden()
        {
            this.__pageHiddenPauseHandle = this.arbiters.pause.declare(true);
            const ticker = RS.ITicker.get();
            this.__tickerWasEnabled = ticker.enabled;
            ticker.enabled = false;
        }

        @Callback
        protected handlePauseStateChanged()
        {
            ITicker.get().paused = this._arbiters.pause.value;
            Audio.ISoundController.get().paused = this._arbiters.pause.value;
            if (this._arbiters.pause.value)
            {
                Log.info(`Paused game`);
            }
            else
            {
                if (RS.Device.isAppleDevice && RS.Device.browser === RS.Browser.Safari)
                {
                    // Some devices do not correctly resume rendering when unlocked, so force them to render
                    // The true issue is somewhere in PIXI
                    const orientationData = this.orientationData;
                    const stageScale = orientationData.stageScale != null ? orientationData.stageScale : (this.settings.stageScale != null ? this.settings.stageScale : 1);
                    const stageDims = Math.Size2D.multiply(orientationData.stageDimensions, stageScale);
                    // Resize to fix locking issue.
                    this._stage.resize(this._stage.stageWidth, this._stage.stageHeight);
                    // Resize to the correct size.
                    this._stage.resize(stageDims.w, stageDims.h);
                }

                Log.info(`Unpaused game`);
            }
        }

        @Callback
        protected handleMuteStateChanged()
        {
            Audio.ISoundController.get().muted = this._arbiters.mute.value;
        }

        @Callback
        protected handleViewOnlyModeStateChanged()
        {
            const state = this._arbiters.viewOnlyMode.value;
            this._stage.interactive = !state;
            this._stage.interactiveChildren = !state;

            if (this._viewOnlyModeUiEnabledArbiterHandle)
            {
                this._viewOnlyModeUiEnabledArbiterHandle.dispose();
                this._viewOnlyModeUiEnabledArbiterHandle = null;
            }

            if (state)
            {
                this._viewOnlyModeUiEnabledArbiterHandle = this._arbiters.uiEnabled.declare(false);
            }
        }

        protected createContext()
        {
            this._context =
            {
                game: this,
                loadOp: new Observable(null)
            };
        }

        protected createDOM()
        {
            // Setup metas
            DOM.Metas.statusBarStyle = DOM.Metas.StatusBarStyle.BlackTranslucent;
            if (Device.isAppleDevice)
            {
                DOM.Metas.webAppCapable = true;
                DOM.Metas.viewport =
                {
                    initialScale: 1.0,
                    maximumScale: 1.0,
                    userScalable: false,
                    minimalUI: true
                };
            }

            if (this.settings.themeColor)
            {
                DOM.Metas.themeColor = this.settings.themeColor;
            }

            // Initialise orientation observable value.
            if (this.settings.landscape && (Orientation.isLandscape || !this.settings.portrait))
            {
                this._orientation.value = DeviceOrientation.Landscape;
            }
            else if (this.settings.portrait && (Orientation.isPortrait || !this.settings.landscape))
            {
                this._orientation.value = DeviceOrientation.Portrait;
            }
            else
            {
                throw new Error("No orientation settings configured.");
            }

            // Retrieve dimensions based on orientation
            const orientationData = this.orientationData;
            if (orientationData == null) { throw new Error("Invalid orientation"); }

            const stageScale = orientationData.stageScale != null ? orientationData.stageScale : (this.settings.stageScale != null ? this.settings.stageScale : 1);
            const dims = Math.Size2D.multiply(orientationData.dimensions, stageScale);
            const stageDims = Math.Size2D.multiply(orientationData.stageDimensions, stageScale);

            // Setup autoscale container
            this._autoscaleContainer = new DOM.AutoscaleContainer(dims.w, dims.h);
            this._autoscaleContainer.onAutoscaled(this.handleAutoscaleContainerAutoscaled);
            this._autoscaleContainer.addToWrapper();

            // Setup canvas
            this._canvas = new DOM.Canvas(dims.w, dims.h);
            this._canvas.css("left", `${(dims.w - stageDims.w) * 0.5}px`);
            this._canvas.css("top", `${(dims.h - stageDims.h) * 0.5}px`);
            this._canvas.parent = this._autoscaleContainer;

            // Setup cover for orientation switching
            this._cover = new DOM.Component().addClass("rs-cover").addToWrapper();
            this._cover.hide();
            Viewport.onResizeAttempted(this.showCover);

            // Setup stage
            this._stage = new RS.Rendering.Stage(
                this._canvas.element,
                stageDims.w, stageDims.h,
                RS.URL.getParameterAsBool("forcecanvas", false) ? [ RS.Rendering.StageRenderStyle.CANVAS ] : this.settings.stageRenderStyles,
                this.settings.transparentStage
            );
            this._stage.scaleX = this._stage.scaleY = stageScale;
            // this._stage.enabled = true;

            // Initial autoscale
            this._autoscaleContainer.autoscale();

            // Handle orientation change
            this._orientationChangeHandler = Orientation.onChanged(this.onOrientationChanged);

            // FPS counter
            if (URL.getParameterAsBool("profile"))
            {
                this._fpsCounter = new DOM.FramerateCounter();
            }
        }

        @Callback
        protected showCover()
        {
            this._cover.show();
            this._coverPauseHandle = this._arbiters.pause.declare(true);
        }

        protected hideCover()
        {
            this._cover.hide();
            this._coverPauseHandle = null;
        }

        @Callback
        protected onOrientationChanged(newOrientation: RS.DeviceOrientation)
        {
            // Check we're in a supported orientation.
            if ((newOrientation === DeviceOrientation.Landscape && !this.settings.landscape) || (newOrientation === DeviceOrientation.Portrait && !this.settings.portrait)) { return; }

            this.showCover();

            // Retrieve dimensions based on orientation
            const orientationData = newOrientation === DeviceOrientation.Landscape ? this.settings.landscape : this.settings.portrait;
            if (orientationData == null) { throw new Error("Invalid orientation"); }

            const stageScale = orientationData.stageScale != null ? orientationData.stageScale : (this.settings.stageScale != null ? this.settings.stageScale : 1);
            const dims = Math.Size2D.multiply(orientationData.dimensions, stageScale);
            const stageDims = Math.Size2D.multiply(orientationData.stageDimensions, stageScale);

            this._canvas.css("left", `${(dims.w - stageDims.w) * 0.5}px`);
            this._canvas.css("top", `${(dims.h - stageDims.h) * 0.5}px`);

            // Reconfigure the AS and canvas
            this._autoscaleContainer.width = dims.w;
            this._autoscaleContainer.height = dims.h;
            this._stage.resize(stageDims.w, stageDims.h);
            this._stage.scaleX = this._stage.scaleY = stageScale;
            this._autoscaleContainer.autoscale();

            this._orientation.value = newOrientation;
        }

        @Callback
        protected handleTurboRequested(): void
        {
            this._turboHandle = this._arbiters.turboMode.declare(true);
        }

        @Callback
        protected handleTurboStopRequested(): void
        {
            this._turboHandle = this._arbiters.turboMode.declare(false);
        }

        @Callback
        protected handleAutoscaleContainerAutoscaled(): void
        {
            if (!this._viewController || !this._uiContainer) { return; }
            this._viewController.resize();
            this.hideCover();

            this.updateUIContainerRect();
        }

        protected determineManifests(): string[]
        {
            const root = IPlatform.get().assetsRoot;
            return Asset.getVariants().map((v) => Path.combine(root, v, `manifest.json`));
        }

        protected createControllers()
        {
            // Registering possible user custom variants
            if (this.settings.userDefinedAllowedVariants)
            {
                RS.Asset.useCustomVariants(this.settings.userDefinedAllowedVariants);
            }

            // Should use mobile assets for internet explorer?
            if (this.settings.useMobileAssetsIE)
            {
                RS.Asset.setShouldUseMobileAssetsForIE(true);
            }

            // Initialise controllers
            this._assetController = Asset.IController.get();
            this._assetController.initialise({
                manifests: this.determineManifests()
            });

            for (const group in this.settings.assetGroupPriorities)
            {
                this._assetController.setGroupPriority(group, this.settings.assetGroupPriorities[group]);
            }
            this._viewController = View.IController.get();
            this._viewController.initialise({
                stage: this._stage,
                assetRequirer: { require: this.requireAssets },
                orientation: this.orientation
            }, this._context);

            this._musicController = Music.IController.get();
            this._musicController.initialise({
                musicArbiter: this._arbiters.music,
                volumeArbiter: this._arbiters.musicVolume,
                stateArbiter: this._arbiters.musicState,
                pauseArbiter: this._arbiters.pause,
                fadeTime: 500,
                lingerTime: 3000
            });

            this._minWagerDurationController = IMinWagerDurationController.get();
            this._minWagerDurationController.initialise({
                allowSpinArbiter: this._arbiters.canSpin,
            });

            this._models.config.turboModeEnabled = RS.IPlatform.get().turboModeEnabled;

            this.createUIController();
            this._uiController.onSettingsRequested(this.handleSettingsRequested);
        }

        // TODO inadvertently untyped, fix this
        protected abstract createUIController();

        protected createObservables()
        {
            this._observables =
            {
                balance: new Observable<number>(0),
                balances: new ObservableMap<number>(),
                win: new Observable<number>(0),
                totalBet: new Observable<number>(0),
                platformGameState: new Observable<PlatformGameState>(PlatformGameState.Uninitialised),
                playerIdleState: new Observable(false)
            }

            this._handleBalanceChanged = this._observables.balance.onChanged(this.handleBalanceChanged);
            this._handleWinAmountChanged = this._observables.win.onChanged(this.handleWinAmountChanged);
            this._handlePlatformGameStateChanged = this._observables.platformGameState.onChanged(this.handlePlatformGameStateChanged);
            this._handleTotalBetChanged = this._observables.totalBet.onChanged(this.handleTotalBetChanged);
        }

        @Callback
        protected handlePlatformGameStateChanged(newState: PlatformGameState)
        {
            if (this.settings.playerIdleTime == null) { return; }

            this._observables.playerIdleState.value = false;
            if (newState === PlatformGameState.Ready)
            {
                this._idleTimer = RS.Tween
                    .wait<Game>(this.settings.playerIdleTime, this)
                    .call(() => this._observables.playerIdleState.value = true);
            }
            else
            {
                this._idleTimer = null;
            }
        }

        @Callback
        protected handleBalanceChanged(newAmount: number)
        {
            this.uiController.setBalance(newAmount);
        }

        @Callback
        protected handleWinAmountChanged(newAmount: number)
        {
            this.uiController.setWin(newAmount, false);
        }

        @Callback
        protected handleTotalBetChanged(newAmount: number)
        {
            this.uiController.setTotalBet(newAmount);
        }

        /**
         * Returns a dictionary of numeric RTP values.
         * These are stored in a single RTP object encoded using URL.setParameter.
         * These can be accessed from an external help template using RTP(name="myrtp").
         */
        protected getRTPTable(): { [name: string]: number }
        {
            if (this.models.config.minRTP === this.models.config.maxRTP)
            {
                const rtp = this.models.config.minRTP == null ? -1 : this.models.config.minRTP;
                return { "single": rtp };
            }
            return { "min": this.models.config.minRTP, "max": this.models.config.maxRTP };
        }

        /**
         * Populates a dictionary containing data to be passed to the external help page.
         * Values can be of any type; they will be stringified and URI-encoded appropriately.
         */
        protected populateExternalHelpData(data: { [name: string]: any }): void
        {
            const legacyRTPkey = "RTP";
            const defaultRTP = this._currencyFormatter.formatDecimal(RS.Math.shiftDecimal(this.models.config.minRTP || this.models.config.maxRTP || -1, 2), false);
            data[legacyRTPkey] = defaultRTP;

            const rtpTable = this.getRTPTable();
            const formattedRtpTable: { [name: string]: string } = {};
            for (const key in rtpTable)
            {
                const decimal = RS.Math.shiftDecimal(rtpTable[key], 2);
                formattedRtpTable[key] = this._currencyFormatter.formatDecimal(decimal, false);
            }

            const rtpTableKey = "RTPs";
            data[rtpTableKey] = rtpTable;
            const maxBetKey = "maxBet";
            data[maxBetKey] = this._currencyFormatter.format(this.getMaxBet(), true);
            const jackpotKey = "hasJackpots";
            data[jackpotKey] = `${ this.settings.hasJackpots || false }`;

            const maxWinEnabled = RS.IPlatform.get().maxWinMode !== RS.PlatformMaxWinMode.None;
            if (maxWinEnabled)
            {
                const maxWinKey = "maxWin";
                data[maxWinKey] = this._currencyFormatter.format(this.models.config.maxWinAmount, true);
            }

            if (this.settings.copyrightYear != null)
            {
                const key = "cryear";
                data[key] = this.settings.copyrightYear;
            }

            const turboModeEnabled = RS.IPlatform.get().turboModeEnabled && this.context.game.arbiters.showTurboUI.value;
            if (turboModeEnabled)
            {
                const turboKey = "turbo";
                data[turboKey] = turboModeEnabled;
            }
        }

        /** Called when the user clicks an external help button. */
        @Callback
        protected handleHelpRequested(): void
        {
            const externalHelpData: { [name: string]: any } = {};
            this.populateExternalHelpData(externalHelpData);
            IPlatform.get().navigateToExternalHelp(externalHelpData);
        }

        @Callback
        protected handleSettingsRequested()
        {
            IPlatform.get().openSettingsMenu();
        }

        protected getMaxBet(): number
        {
            return Find.highest(this.models.stake.betOptions, (v) => v.value).value;
        }

        @Callback
        protected handlePaytableRequested(): void
        {
            // Do we have a paytable view assigned?
            if (!this.settings.paytableView)
            {
                Log.warn(`Tried to open the paytable with no paytableView defined in Game settings.`);
                return;
            }

            // Is the paytable view already open?
            if (this.viewController.viewStack.some((v) => v instanceof this.settings.paytableView))
            {
                // Close paytable
                this.viewController.close(this.settings.paytableView);
            }
            else
            {
                // Open paytable
                this.viewController.open(this.settings.paytableView);
            }
        }

        protected createUIContainer()
        {
            this._uiContainer = Flow.container.create({
                name: "UI Container",
                dock: Flow.Dock.None,
                locale: this.locale,
                currencyFormatter: this.currencyFormatter,
                legacy: false
            }, this._viewController.stage);
            this.updateUIContainerRect();
        }

        @Callback
        protected async requireAssets(groups: ReadonlyArray<string>)
        {
            await this._assetController.manifestState.for(Asset.ManifestState.Loaded);
            const op = this._assetController.loadGroups(groups);
            if (op == null)
            {
                // All groups were empty
                RS.Log.warn("requireAssets called but nothing was found to load");
                return;
            }
            op.onErrored(this.handleAssetLoadFailed);
            const tween = RS.Tween
                .wait(500)
                .call(() =>
                {
                    this._loadingAssetsDialog.open();
                });

            await op.complete.for(true).then(() =>
            {
                this._loadingAssetsDialog.close();
                tween.finish();
            });
        }

        @Callback
        protected handleAssetLoadFailed(str: string): void
        {
            // Notify analytics
            const analytics = Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().error({
                    message: str,
                    fatal: true
                });
            }

            // Show fatal error
            const message = `${Translations.AssetLoadError.Message1.get(this._context.game.locale)}\n\n${Translations.AssetLoadError.Message2.get(this._context.game.locale)}`;
            const platform = IPlatform.get();
            switch (platform.generalErrorHandlingMode)
            {
                case PlatformMessageHandlingMode.Ignore:
                    break;
                case PlatformMessageHandlingMode.PassToPlatform:
                    platform.handleGeneralError(message)
                        .then(() => location.reload());
                    break;
                case PlatformMessageHandlingMode.Display:
                    this._context.game.displayErrorDialog(message)
                        .then(() => location.reload());
                    break;
            }
        }

        protected initialiseAssetPriorities()
        {
            this._assetController.setGroupPriority("common", Asset.LoadPriority.Immediate);
            this._assetController.setGroupPriority("shaders", Asset.LoadPriority.Immediate);
        }

        protected createStateMachine()
        {
            this._stateMachine = new State.Machine<Game.Context>(this.settings.stateMachine, this._context);
            if (URL.getParameterAsBool("selenium"))
            {
                this._stateMachine.onStateChanged((ev) =>
                {
                    SGI.Selenium.SeleniumControl.setText(ev.newState.name);
                });
            }
        }

        protected initialiseViews()
        {
            const loadingRuntimeData: Views.Loading.RuntimeData =
            {
                context: this._context,
                viewController: this._viewController,
                assetController: this._assetController
            };
            this._viewController.bindSettings(Views.Loading, {}, loadingRuntimeData);
        }

        protected initialiseDialogs()
        {
            this._loadingAssetsDialog = new DialogDelegate({
                viewController: this._viewController,
                animator: this.settings.defaultDialogAnimator,
                dialogSettings: this.settings.loadingAssetsDialog ||
                {
                    ...Flow.Dialog.defaultSettings,
                    titleLabel: { ...Flow.Dialog.defaultSettings.titleLabel, text: Translations.LoadingAssetsDialog.Title },
                    messageLabel: { ...Flow.Dialog.defaultSettings.messageLabel, text: Translations.LoadingAssetsDialog.Message },
                    buttons: []
                },
                pauseGameWhenOpen: false,
                pauseArbiter: this._arbiters.pause
            });
            this._connectionLostDialog = new DialogDelegate({
                viewController: this._viewController,
                animator: this.settings.defaultDialogAnimator,
                dialogSettings: this.settings.connectionLostDialog ||
                {
                    ...Flow.Dialog.defaultSettings,
                    titleLabel: { ...Flow.Dialog.defaultSettings.titleLabel, text: Translations.ConnectionLostDialog.Title },
                    messageLabel: { ...Flow.Dialog.defaultSettings.messageLabel, text: Translations.ConnectionLostDialog.Message },
                    buttons: []
                },
                pauseGameWhenOpen: true,
                pauseArbiter: this._arbiters.pause
            });
            this._connectionStatusChangedHandler = Connection.connected.onChanged(this.handleConnectionStatusChange);
        }

        @Callback
        protected handleConnectionStatusChange(newStatus: boolean): void
        {
            const platform = IPlatform.get();
            switch (platform.connectionLostHandlingMode)
            {
                case PlatformConnectionLostMode.Error:

                    switch (platform.generalErrorHandlingMode)
                    {
                        case PlatformMessageHandlingMode.Display:
                            this.displayErrorDialog(Translations.ConnectionLostError.Message.get(this._locale))
                                .then(() => location.reload());
                            break;
                        case PlatformMessageHandlingMode.PassToPlatform:
                            platform.handleGeneralError(Translations.ConnectionLostError.Message.get(this._locale))
                                .then(() => location.reload());
                            break;
                    }

                    break;
                case PlatformConnectionLostMode.PauseWithMessage:
                    if (!newStatus)
                    {
                        this._connectionLostDialog.open();
                        if (!this._uiEnabledArbiterHandle)
                        {
                            this._uiEnabledArbiterHandle = this._arbiters.uiEnabled.declare(false);
                        }
                    }
                    else
                    {
                        this._connectionLostDialog.close();
                        if (this._uiEnabledArbiterHandle)
                        {
                            this._uiEnabledArbiterHandle.dispose();
                            this._uiEnabledArbiterHandle = null;
                        }
                    }
                    break;
            }
        }

        /**
         * Actions the specified error option type.
         * Returns true if the game should continue/recover.
         */
        protected actionErrorOption(opt: RS.Models.Error.OptionType): boolean
        {
            switch (opt)
            {
                case Models.Error.OptionType.Continue:
                    return true;
                case Models.Error.OptionType.Close:
                    IPlatform.get().close();
                    return false;
                case Models.Error.OptionType.Refresh:
                    IPlatform.get().reload();
                    return false;
                default:
                    return true;
            }
        }

        protected initialiseModels(): void
        {
            this._models = {} as Models;
            Models.clear(this._models);
        }

        protected initialiseEngine(): void
        {
            if (this.settings.engine == null)
            {
                throw new Error("No engine specified, check that the build included the correct modules");
            }
            this._engine = new this.settings.engine(this.settings.engineSettings, this._models);
        }

        protected initialisePlatform(uiContainer: RS.Rendering.IContainer = null)
        {
            const platform = IPlatform.get();
            platform.init({
                pauseArbiter: this._arbiters.pause,
                muteArbiter: this._arbiters.mute,
                uiEnabledArbiter: this._arbiters.uiEnabled,
                uiContainer
            });
            IDump.get().enabled = platform.shouldDisplayDevTools;
        }

        protected initialiseAnalytics()
        {
            if (this.settings.analyticsSettings == null) { return; }
            const analytics = Analytics.IAnalytics.get();
            if (analytics == null) { return; }
            analytics.initialise({
                name: this.settings.analyticsSettings.appName,
                version: this.gameVersion
            }, this.settings.analyticsSettings.apiKey);
        }

        protected dump(target: RS.ISubDump)
        {
            target.add(
            {
                ["version"]: Version.toString(this._gameVersion),
                ["synergy"]: synergyHashFromBuild,
                ["platform"]: Device.summary
            });
        }

        private updateUIContainerRect(): void
        {
            const visibleRegion = this._viewController.visibleRegion;
            this._uiContainer.x = visibleRegion.x;
            this._uiContainer.y = visibleRegion.y;
            this._uiContainer.size = { w: visibleRegion.w, h: visibleRegion.h };
        }
    }

    export namespace Game
    {
        export interface Context
        {
            /** Main game class */
            game: Game;
            /** Handles loading operations progress */
            loadOp: Observable<Scheduler.IOperation | null>;
            playEnded?: boolean;
            isResuming?: boolean;
            shouldCycle?: boolean;
            spinIndex?: number;
        }

        /**
         * Contains state arbiters for the game.
         */
        export interface Arbiters
        {
            /** Indicates whether the game should be paused or not. */
            pause: IArbiter<boolean>;

            /** Indicates whether all audio should be muted or not. */
            mute: IArbiter<boolean>;

            /** Indicates whether the user can interact with the UI or not. */
            uiEnabled: IArbiter<boolean>;

            /** Whether or not the game is in view-only mode (nothing can be interacted with). */
            viewOnlyMode: IArbiter<boolean>;

            /** Indicates whether or not to show the main button area. */
            showButtonArea: Arbiter<boolean>;

            /** Any status text that should be displayed. */
            messageText: Arbiter<Localisation.LocalisableString>;

            /** The current music track to play. */
            music: Arbiter<Music.Definition | null>;

            /** The current state that the music should be in. */
            musicState: Arbiter<Music.State>;

            /** The volume the music should be at. */
            musicVolume: Arbiter<number>;

            /** Indicates whether the user can initiate a spin or not. */
            canSpin: Arbiter<boolean>;

            /** Indicates whether the user can skip the current state or not. */
            canSkip: Arbiter<boolean>;

            /** Indicates whether the spin button should display "spin" or "skip". */
            spinOrSkip: Arbiter<"spin"|"skip">;

            /** Indicates whether the user can change their bet amount or not. */
            canChangeBet: Arbiter<boolean>;

            /** Indicates whether attempting to change their bet amount should cause a skip event or not. */
            betShouldSkip: Arbiter<boolean>;

            /** Indicates whether the game should attempt to play as fast as possible. */
            turboMode: Arbiter<boolean>;

            /** Indicates wheter the turbo toggle should be shown or not if possible */
            showTurboUI: Arbiter<boolean>;

            /** Whether or not the paytable is visible. */
            paytableVisible: IArbiter<boolean>;

            /** Indicates wheter the meter panel UI should be shown or not if possible */
            showMeterPanel: Arbiter<boolean>;

            /** Indicates wheter the message bar UI should be shown or not if possible */
            showMessageBar: Arbiter<boolean>;
        }

        /**
         * Contains common observables for the game.
         * These are NOT models but instead values to display on the UI, or be passed to the platform.
         */
        export interface Observables
        {
            /** Customer's balance. */
            balance: Observable<number>;

            /** Customer's named balances. */
            balances: ObservableMap<number>;

            /** Current win amount. */
            win: Observable<number>;

            /** Current total bet amount. */
            totalBet: Observable<number>;

            /** Whether or not a game is currently in progress. */
            platformGameState: Observable<PlatformGameState>;

            /** Whether or not the player is idle. */
            playerIdleState: Observable<boolean>;
        }

        export interface OrientationSettings
        {
            dimensions: Dimensions;
            stageDimensions: Dimensions;
            stageScale?: number;
        }

        export interface Settings
        {
            /** Stage scale. */
            stageScale?: number;

            /** Dimensions to use when the game is in landscape. */
            landscape?: OrientationSettings;

            /** Dimensions to use when the game is in portrait. */
            portrait?: OrientationSettings;

            /** Definition for the master state machine. */
            stateMachine: State.Machine.Definition<Context>;

            /** Default load priorities for all asset groups. */
            assetGroupPriorities: Map<Asset.LoadPriority>;

            /** The view to display while loading. */
            loadingView?: View.AnyViewType;

            /** The view to display when idle. */
            primaryView?: View.AnyViewType;

            /** Fatal error dialog. */
            errorDialog?: Flow.Dialog.Settings;

            /** Loading assets dialog. */
            loadingAssetsDialog?: Flow.Dialog.Settings;

            /** Internet connection lost dialog. */
            connectionLostDialog?: Flow.Dialog.Settings;

            /** Default dialog animator to use. */
            defaultDialogAnimator?: Views.Dialog.IAnimator;

            /** Analytics settings. */
            analyticsSettings?:
            {
                appName: string;
                apiKey: string;
            };

            // TODO 1.2 make mandatory
            /** WebStorage key. */
            storageKey?: string;

            /** Theme color to use for the browser status bar. */
            themeColor?: Util.Color;

            /** Defines custom asset variants that would be loaded */
            userDefinedAllowedVariants?: string[];

            /** Show mobile assets be used on IE? */
            useMobileAssetsIE?: boolean;

            /** Transparent stage? */
            transparentStage?: boolean;

            /** Settings to use for positive buttons on dialogs (e.g. "OK"). */
            positiveDialogButton?: Flow.Button.Settings;

            /** Settings to use for negative buttons on dialogs (e.g. "Cancel"). */
            negativeDialogButton?: Flow.Button.Settings;

            /** Settings to use for non-positive and non-negative buttons on dialogs. */
            neutralDialogButton?: Flow.Button.Settings;

            /** The default time that dialogs should display for during replay. */
            replayDialogDisplayTime: number;

            /** The game name */
            gameName?: Localisation.LocalisableString;

            /** The view to display when the help button has been clicked. */
            paytableView?: View.AnyViewType;

            /** Time before player deemed idle. */
            playerIdleTime?: number;

            engine: Engine.EngineType<Models, object>;
            engineSettings: object;

            /** Render styles to use for the stage. */
            stageRenderStyles?: string[];

            /** Configuration for the dialog shown when maximum win has been achieved. */
            maxWinDialog?: RS.Flow.Dialog.Settings;

            /** Losing force for local balance tweaking */
            loseForce?: Engine.ForceResult;

            /** Copyright year to be displayed in external help pages. */
            copyrightYear?: number | string;

            /** Used to generate help information based on if the game has jackpots or not */
            hasJackpots?: boolean;
        }
    }
}
