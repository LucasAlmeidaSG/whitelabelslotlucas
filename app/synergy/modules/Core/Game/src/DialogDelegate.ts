/// <reference path="views/Dialog.ts" />

namespace RS
{
    /**
     * Responsible for managing a single notification dialog which does not allow user interaction.
     */
    @HasCallbacks
    export class DialogDelegate implements IDisposable
    {
        /** The current state of this dialog delegate. */
        public readonly state = new Observable(DialogDelegate.State.Closed);
        
        protected _desiredState: DialogDelegate.State | null = null;
        @AutoDispose protected _view: Views.Dialog | null = null;
        @AutoDispose protected _pauseArbiterHandle: IDisposable;
        protected _isDisposed = false;

        /** Gets if this dialog delegate has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        public constructor(public readonly settings: DialogDelegate.Settings)
        {
            this.state.onChanged(this.checkState);
        }

        /**
         * Opens the dialog.
         */
        public open(): void
        {
            this._desiredState = DialogDelegate.State.Open;
            this.checkState();
        }

        /**
         * Closes the dialog.
         */
        public close(): void
        {
            this._desiredState = DialogDelegate.State.Closed;
            this.checkState();
        }

        /** Disposes this dialog delegate. */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }

        protected async doOpen()
        {
            if (this.state.value !== DialogDelegate.State.Closed) { return; }
            this.state.value = DialogDelegate.State.Opening;
            if (this.settings.pauseGameWhenOpen && this.settings.pauseArbiter)
            {
                this._pauseArbiterHandle = this.settings.pauseArbiter.declare(true);
            }
            const view = await this.settings.viewController.open(Views.Dialog, {
                dialogSettings: this.settings.dialogSettings,
                animator: this.settings.animator
            });
            this._view = view;
            this.state.value = DialogDelegate.State.Open;
        }

        protected async doClose()
        {
            if (this.state.value !== DialogDelegate.State.Open) { return; }
            this.state.value = DialogDelegate.State.Closing;
            await this._view.exit();
            await this.settings.viewController.close(this._view);
            this.state.value = DialogDelegate.State.Closed;
            this._view = null;
            if (this._pauseArbiterHandle)
            {
                this._pauseArbiterHandle.dispose();
                this._pauseArbiterHandle = null;
            }
        }

        @Callback
        protected checkState(): void
        {
            switch (this.state.value)
            {
                case DialogDelegate.State.Closed:
                    if (this._desiredState === DialogDelegate.State.Open)
                    {
                        this.doOpen();
                    }
                    this._desiredState = null;
                    break;
                case DialogDelegate.State.Open:
                    if (this._desiredState === DialogDelegate.State.Closed)
                    {
                        this.doClose();
                    }
                    this._desiredState = null;
                    break;
            }
        }
    }

    export namespace DialogDelegate
    {
        export enum State
        {
            Closed,
            Opening,
            Open,
            Closing
        }

        export interface Settings
        {
            viewController: View.IController;
            dialogSettings: Flow.Dialog.Settings;
            pauseGameWhenOpen: boolean;
            pauseArbiter?: IArbiter<boolean>;
            animator?: Views.Dialog.IAnimator;
        }
    }
}