namespace RS
{
    /**
     * Responsible for ensuring a minimum time passes between the user clicking "Spin" to place a wager, and the user being able to click "Spin" again to place a new wager.
     */
    export interface IMinWagerDurationController extends RS.Controllers.IController<IMinWagerDurationController.Settings>
    {
        enabled: boolean;

        /** Gets if the wager timer is going. */
        readonly wagerInProgress: boolean;

        /** Indicates that a wager began and initiates the timer. */
        beginWager(): void;

        /** Indicates that a wager was cancelled and stops the timer. */
        cancelWager(): void;
    }

    export namespace IMinWagerDurationController
    {
        export interface Settings
        {
            allowSpinArbiter: Arbiter<boolean>;
            allowQuickSpinArbiter?: Arbiter<boolean>;
        }
    }

    export const IMinWagerDurationController = Controllers.declare<IMinWagerDurationController, IMinWagerDurationController.Settings>(Strategy.Type.Instance);
}
