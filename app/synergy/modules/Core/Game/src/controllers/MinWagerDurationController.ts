/// <reference path="IMinWagerDurationController.ts" />

namespace RS
{
    @RS.HasCallbacks
    export class MinWagerDurationController implements IMinWagerDurationController
    {
        protected _isDisposed: boolean = false;
        protected _settings: IMinWagerDurationController.Settings;
        protected _minWagerDuration: number;

        @RS.AutoDisposeOnSet
        protected _timer: Tween<any> | null;
        @RS.AutoDisposeOnSet
        protected _allowSpinHandle: IArbiterHandle<boolean> | null;
        @RS.AutoDisposeOnSet
        protected _allowQuickSpinHandle: IArbiterHandle<boolean> | null;

        /** Gets if the wager timer is going. */
        public get wagerInProgress() { return this._timer != null; }

        public get settings() { return this._settings; }

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        private _enabled = true;
        public get enabled() { return this._enabled; }
        public set enabled(enabled)
        {
            if (this._enabled === enabled) { return; }
            if (enabled)
            {
                // TODO support enabling min wager duration mid-wager
                // - would require that wager finish notifies us; maybe in the idle state?
            }
            else
            {
                if (this._timer)
                {
                    this._timer.finish();
                    this._timer = null;
                    Log.debug(`Minimum wager timer cancelled (timestamp: ${Date.now()})`);
                }

                this._allowQuickSpinHandle = null;
                this._allowSpinHandle = null;
            }
            this._enabled = enabled;
        }

        public initialise(settings: IMinWagerDurationController.Settings)
        {
            this._settings = settings;
            this._minWagerDuration = RS.IPlatform.get().minWagerDuration;
        }

        /** Indicates that a wager began and initiates the timer. */
        public beginWager(): void
        {
            if (!this._enabled) { return; }

            if (this.wagerInProgress) { this.cancelWager(); }

            this._allowSpinHandle = this.settings.allowSpinArbiter.declare(false);
            if (this.settings.allowQuickSpinArbiter) { this._allowQuickSpinHandle = this.settings.allowQuickSpinArbiter.declare(false); }

            this._timer = RS.Tween.wait(this._minWagerDuration);
            this._timer.then(this.handleMinWagerDurationMet);
            Log.debug(`Minimum wager timer started (timestamp: ${Date.now()})`);
        }

        /** Indicates that a wager was cancelled and stops the timer. */
        public cancelWager(): void
        {
            if (!this._enabled) { return; }

            if (this._timer)
            {
                this._timer.finish();
                this._timer = null;
                Log.debug(`Minimum wager timer ended (timestamp: ${Date.now()})`);
            }

            this._allowSpinHandle = null;
            this._allowQuickSpinHandle = null;
        }

        /** Disposes this controller. */
        public dispose()
        {
            if (this._isDisposed) { return; }
            this.cancelWager();
            this._isDisposed = true;
        }

        @RS.Callback
        protected handleMinWagerDurationMet(): void
        {
            this.cancelWager();
        }
    }

    IMinWagerDurationController.register(MinWagerDurationController);
}
