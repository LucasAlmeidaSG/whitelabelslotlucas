namespace RS.Views
{
    /**
     * View that contains a popup dialog.
     */
    @HasCallbacks
    export class Dialog extends View.Base<Dialog.Settings, RS.Game.Context>
    {
        /** Published when an option has been selected on this dialog. */
        public readonly onOptionSelected = createEvent<number>();//this._onOptionSelected.public;

        @AutoDispose protected _dialog: Flow.Dialog;

        protected _selectedOption: number;

        //protected readonly _onOptionSelected = createEvent<number>();

        protected autoCloseTween: RS.Tween<any>;

        /** Called when this view has been opened. */
        public onOpened(): void
        {
            super.onOpened();

            this.locale = this.context.game.locale;

            this._dialog = Flow.dialog.create({
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 },
                ...this.settings.dialogSettings
            }, this);

            this._dialog.onOptionSelected(this.handleDialogOptionSelected);
            this.enter();
        }

        public async exit()
        {
            this._dialog.enabled = false;
            if (this.settings.animator)
            {
                await this.settings.animator.exit(this._dialog);
            }
            if (this._selectedOption != null) { this.onOptionSelected.publish(this._selectedOption); }
        }

        @Callback
        protected handleDialogOptionSelected(value: number): void
        {
            if (this.autoCloseTween)
            {
                this.autoCloseTween.finish();
                this.autoCloseTween = null;
            }

            this._selectedOption = value;
            this.exit();
        }

        @Callback
        protected onAutoClose(): void
        {
            this.autoCloseTween = null;
            this.handleDialogOptionSelected(0);
        }

        protected async enter()
        {
            if (this.settings.animator)
            {
                this._dialog.enabled = false;
                await this.settings.animator.enter(this._dialog);
                if (!this._dialog) { return; }
                this._dialog.enabled = true;
            }

            if (this.settings.displayTime)
            {
                this.autoCloseTween = Tween.wait(this.settings.displayTime);
                this.autoCloseTween.then(this.onAutoClose);
            }
        }
    }

    export namespace Dialog
    {
        export interface Settings extends View.Base.Settings
        {
            dialogSettings: Flow.Dialog.Settings;
            animator?: IAnimator;
            displayTime?: number;
        }

        /**
         * Responsible for animating the entry/exit of a dialog.
         */
        export interface IAnimator
        {
            enter(dialog: Flow.Dialog): PromiseLike<void>;
            exit(dialog: Flow.Dialog): PromiseLike<void>;
        }
    }
}