/// <reference path="../generated/Translations.ts" />

namespace RS.Views
{
    /**
     * View to display while the game is loading.
     */
    @HasCallbacks
    export class Loading<TSettings extends Loading.Settings = Loading.Settings, TContext extends RS.Game.Context = RS.Game.Context, TRuntimeData extends Loading.RuntimeData<TContext> = Loading.RuntimeData<TContext>> extends View.Base<TSettings, TContext, TRuntimeData> implements GameState.LoadingState.IClickToContinueView
    {
        protected _background: Flow.Background;
        protected _loadingPanel: Flow.Container;
        protected _loadingLabel: Flow.Label;
        protected _clickToContinueLabel: Flow.Label;
        protected _loadingBar: Flow.ProgressBar;
        protected _dgeCertLabel: Flow.Label;

        @AutoDispose protected _onLoadOpChanged: IDisposable;
        @AutoDisposeOnSet protected _onLoadProgressChanged: IDisposable;
        @AutoDispose protected _clickCapturer: View.IClickCapture;

        @AutoDispose protected _clickToContinue: ISimplePrivateEvent;

        public get onClickToContinue() { return this._clickToContinue.public; }

        /** Called when this view has been opened. */
        public onOpened(): void
        {
            super.onOpened();

            this.locale = this.context.game.locale;

            // Create background
            this._background = Flow.background.create(this.settings.background || {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.SolidColor,
                color: Util.Colors.gray,
                borderSize: 0,
                cornerRadius: 0
            }, this);

            // Check the url params to see if the DGE/NJ certification label should be created.
            // urlParms is a string of all the window params, DGECert=true is added onto this even though it does not display in the URL itself.
            const parmString = "urlParms";
            const parms = window[parmString];
            const dgeCertParm = Is.string(parms) && (parms.search("DGECert=true") > -1 || parms.search("DGECert=1") > -1);
            const urlParam = URL.getParameterAsBool("DGECert");
            if ((urlParam || dgeCertParm) && this.settings.showDgeCert)
            {
                this._dgeCertLabel = Flow.label.create(this.settings.dgeCertLabel ||
                    {
                        dock: Flow.Dock.Top,
                        text: Translations.LoadingView.DGE.Cert,
                        font: Fonts.FrutigerWorld,
                        fontSize: 34,
                        canWrap: true,
                        sizeToContents: false,
                        size: { w: Flow.Sizing.Fraction(1), h: Flow.Sizing.Fraction(0.15) },
                        wrapType: Rendering.TextOptions.WrapType.WhiteSpace,
                        align: Rendering.TextOptions.Align.Middle
                    }, this);
            }

            // Create loading element
            this.createLoadingElement();

            // Bind progress
            this._onLoadOpChanged = this.context.loadOp.onChanged(this.handleLoadOpChanged);
            this._clickToContinue = createSimpleEvent();
        }

        /**
         * Wait for click to continue
         */
        protected async waitForClickToContinue(): Promise<void>
        {
            this._clickCapturer = this.context.game.viewController.captureFullScreenClicks();
            await this._clickCapturer.onClicked;
            this._clickCapturer.dispose();

            // Handle that we now clicked on the click Capturer
            this.handleClickedToContinue();
        }

        protected handleClickedToContinue(): void
        {
            // Tell loading state we can
            this._clickToContinue.publish();
        }

        @Callback
        protected handleLoadComplete()
        {
            // Create element just for click to continue
            this.createClickToContinueElement();

            // Handle click to continue
            this.waitForClickToContinue();
        }

        @Callback
        protected handleLoadOpChanged(newLoadOp: Scheduler.IOperation | null)
        {
            if (newLoadOp == null) { return; }
            // Listen to loading completed
            this.context.loadOp.value.complete.for(true).then(this.handleLoadComplete);

            this._onLoadProgressChanged = newLoadOp.progress.onChanged(this.handleLoadProgressChanged);
            this.handleLoadProgressChanged(newLoadOp.progress.value);
        }

        @Callback
        protected handleLoadProgressChanged(newProgress: number)
        {
            this._loadingBar.progress = newProgress;
        }

        protected createLoadingElement()
        {
            // If any of the settings are explicitly null, don't create that element
            // If undefined, use default settings

            if (this.settings.loadingPanel === null) { return; }

            // Create loading panel
            this._loadingPanel = Flow.container.create(this.settings.loadingPanel || {
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 },
                size: { w: 400, h: 100 },
                spacing: Flow.Spacing.all(5)
            }, this);

            // Create progress bar
            if (this.settings.loadingBar !== null)
            {
                this._loadingBar = Flow.progressBar.create(this.settings.loadingBar || {
                    ...Flow.ProgressBar.defaultSettings,
                    dock: Flow.Dock.Bottom,
                    size: { w: 0, h: 40 },
                    divisor: 1.0
                }, this._loadingPanel);
            }

            // Create label
            if (this.settings.loadingLabel !== null)
            {
                this._loadingLabel = Flow.label.create(this.settings.loadingLabel || {
                    dock: Flow.Dock.Fill,
                    text: Translations.LoadingView.Message,
                    font: Fonts.FrutigerWorld,
                    fontSize: 32
                }, this._loadingPanel);
            }

        }

        protected createClickToContinueElement()
        {
            // Create click to continue label
            const settings = (this.settings.clickToContinueLabel == null) ? Loading.defaultLabelSettings : this.settings.clickToContinueLabel;

            // Create a label
            this._clickToContinueLabel = Flow.label.create(settings, this);
        }
    }

    export namespace Loading
    {
        /**
         * Settings for default label.
         */
        export const defaultLabelSettings: Flow.Label.Settings =
        {
            font: Fonts.FrutigerWorld.Bold,
            fontSize: 38,
            sizeToContents: false,
            canWrap: true,
            align: Rendering.TextOptions.Align.Middle,
            overflowMode: Flow.OverflowMode.Shrink,
            size: { w: 575, h: 105 },
            dockAlignment: { x: 0.5, y: 0.5 },
            floatPosition: { x: 0.5, y: 0.65 },
            dock: Flow.Dock.Float,
            layers:
                [
                    {
                        fillType: Rendering.TextOptions.FillType.SolidColor,
                        color: Util.Colors.black,
                        layerType: Rendering.TextOptions.LayerType.Border,
                        offset: { x: 0.00, y: 0.00 },
                        size: 1.5
                    },
                    {
                        layerType: Rendering.TextOptions.LayerType.Fill,
                        fillType: Rendering.TextOptions.FillType.SolidColor,
                        offset: { x: 0.00, y: 0.00 },
                        color: Util.Colors.lightblue
                    }
                ],
            text: Device.isDesktopDevice ? Translations.Loading.ClickToContinue : Translations.Loading.TapToContinue
        }

        export interface Settings extends View.Base.Settings
        {
            background?: Flow.Background.Settings;
            loadingPanel?: Flow.Container.Settings;
            loadingBar?: Flow.ProgressBar.Settings;
            loadingLabel?: Flow.Label.Settings;
            clickToContinueLabel?: Flow.Label.Settings;
            dgeCertLabel?: Flow.Label.Settings;

            /**
             * Whether or not to show DGE/NJ certfifcation message if the param is present. Shouldn't be shown in a live environment
             * Defaults to false
             */
            showDgeCert?: boolean;
        }

        export interface RuntimeData<TContext extends RS.Game.Context = RS.Game.Context> extends View.Base.RuntimeData<TContext>
        {
            assetController: Asset.IController;
        }
    }
}