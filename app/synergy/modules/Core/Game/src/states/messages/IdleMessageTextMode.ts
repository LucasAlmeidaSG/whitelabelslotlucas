namespace RS
{
    export type IdleMessageTextMode = (context: Game.Context, totalBet: number) => RS.Localisation.LocalisableString;

    export namespace IdleMessageTextMode
    {
        /**
        * Returns the total bet message
        *
        * @param locale The locale
        * @param models The models
        */
        export function TotalBet(context: Game.Context, totalBet: number): RS.Localisation.LocalisableString
        {
            return Localisation.bind(Translations.MessageTextMode.TotalBet, { totalBet: context.game.currencyFormatter.format(totalBet) });
        }
        /**
        * Returns the game name message
        *
        * @param locale The locale
        * @param models The models
        */
        export function GameName(context: Game.Context, totalBet: number): RS.Localisation.LocalisableString
        {
            return context.game.settings.gameName || "";
        }
        /**
        * Returns an empty message
        *
        * @param locale The locale
        * @param models The models
        */
        export function Empty(context: Game.Context, totalBet: number): RS.Localisation.LocalisableString
        {
            return "";
        }
    }
}
