namespace RS.GameState
{
    export const PlayFinished = "playFinished";

    /**
     * The current game has finished and we're ready to close the game
     */
    @HasCallbacks
    @State.Name(PlayFinished)
    export abstract class PlayFinishedState<T extends Game.Context = Game.Context> extends State.Base<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;

        public onEnter()
        {
            super.onEnter();

            const { arbiters, models } = this._context.game;

            this._canSpin = arbiters.canSpin.declare(false); // We can't spin
            this._spinOrSkip = arbiters.spinOrSkip.declare("skip"); // We should now be set to "skip"

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            this._context.isResuming = false;

            // // Make sure we stop if we've had a single win over our stop limit
            // if (AutospinController.isAutospinning)
            // {
            //     AutospinController.checkForAutospinEnd();
            // }

            // Is the game open?
            this.checkCloseGame();
        }

        /**
         * Checks if the game is open, and closes it if needed.
         */
        protected async checkCloseGame()
        {
            if (this._context.game.models.state.state !== Models.State.Type.Closed)
            {
                if (!this._context.playEnded)
                {
                    this._context.playEnded = true;
                }
                if (!await this.doCloseGame()) { return; }
            }
            await this.checkRealityCheck();
            this.transitionToNextState(Idle);
        }

        /**
         *
         */
        protected async doCloseGame(response?: Engine.IResponse)
        {
            //for overriding - if response is passed we won't close twice
            if (!response)
            {
                response = await this._context.game.engine.close();
            }

            const models = this._context.game.models;

            // Was there an error?
            if (response.isError)
            {
                // Update analytics
                const analytics = Analytics.IAnalytics.get();
                if (analytics != null)
                {
                    const isFatal = !models.error.options.some((o) => o.type === RS.Models.Error.OptionType.Continue);
                    analytics.getTracker().error({
                        message: Localisation.resolveLocalisableString(this._context.game.locale, models.error.message),
                        fatal: isFatal
                    });
                }

                // Handle the error
                if (await this._context.game.handleError())
                {
                    // "Continue" - in this case, reset the spin, don't show any suspense
                    return this.shouldReturnToIdle();
                }
                else
                {
                    // Probably fatal
                    return false;
                }
            }

            this.updateBalance();

            return this.shouldReturnToIdle();
        }

        protected updateBalance()
        {
            const models = this._context.game.models;
            const spinResult = this.getResultsModel(models)[this._context.spinIndex];
            this._context.game.observables.balance.value = spinResult.balanceAfter.primary;
            this._context.game.observables.balances.value = spinResult.balanceAfter.named;
        }

        protected shouldReturnToIdle()
        {
            return true;
        }

        protected async checkRealityCheck()
        {
            // TODO: See if we need to display reality check here
        }

        protected abstract getResultsModel(models: RS.Models): Models.GameResults
    }
}
