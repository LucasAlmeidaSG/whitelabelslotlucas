namespace RS.GameState
{
    /**
    * It's time to render any wins.
    * It could be that there are no wins to render, in which case we move on immediately.
    */
    export const WinRendering = "winRendering";

    @HasCallbacks
    @State.Name(WinRendering)
    export abstract class WinRenderingState<T extends Game.Context = Game.Context> extends RS.State.Base<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: RS.ArbiterHandle<boolean>;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _betShouldSkip: IDisposable;
        @AutoDisposeOnSet protected _musicStateHandle: IDisposable;
        @AutoDisposeOnSet protected _skipRequestedHandlerUI: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;
        
        protected _skipped: boolean = false;

        public async onEnter()
        {
            super.onEnter();

            const { models, arbiters } = this._context.game;

            if (models.state.isMaxWin)
            {
                this.transitionToState(MaxWin);
                return;
            }

            this._canSpin = arbiters.canSpin.declare(false); // We can no longer spin
            this.declareCanSkipHandler();
            this._spinOrSkip = arbiters.spinOrSkip.declare("skip"); // We should now be set to "skip"
            this._musicStateHandle = arbiters.musicState.declare(Music.State.Foreground);
            this._betShouldSkip = arbiters.betShouldSkip.declare(true);

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            this._skipRequestedHandlerUI = this._context.game.uiController.onSkipRequested(this.handleSkipRequested);

            await this.renderWins();

            this._context.game.observables.platformGameState.value = PlatformGameState.InPlayPostWinRendering;

            await this.onComplete();
        }

        protected declareCanSkipHandler()
        {
            //slots overrides this
            const arbiters = this._context.game.arbiters;
            this._canSkip = arbiters.canSkip.declare(true); //We can skip at any point
        }

        protected async renderWins()
        {
            //render wins
            this._skipped = false;
        }
        protected async onComplete()
        {
            //actions to do when win rendering is done
            this.transitionToNextState(PlayFinished);
        }

        /**
         * Called when a skip is requested.
         */
        @Callback
        protected handleSkipRequested()
        {
            const arbiters = this._context.game.arbiters;

            // Check the arbiter
            if (!arbiters.canSkip.value) { return; }

            // Consume the skip
            this._canSkip.dispose();
            this._canSkip = arbiters.canSkip.declare(false);

            // Handle it
            this.handleSkip();
        }

        /**
         * Handles a skip request.
         */
        protected handleSkip(): void
        {
            const analytics = Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Slot Game",
                    action: "SkipWin"
                });
            }
            this._context.shouldCycle = false;
            this.stopWinRendering();
        }

        /**
         * Skips win rendering.
         * The done callback passed to startWinRender will be called asap.
         */
        protected stopWinRendering(): void
        {
            // Sanity check
            if (this._skipped) { return; }

            // Tell the win renderers to skip
            this._skipped = true;
            //override
        }
    }

}
