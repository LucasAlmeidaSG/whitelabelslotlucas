namespace RS.GameState
{
    /** State to do anything between idle and spin */
    export const PrePlay = "prePlay";

    @HasCallbacks
    @State.Name(PrePlay)
    export abstract class PrePlayState<T extends Game.Context = Game.Context> extends State.Base<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _musicStateHandle: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;

        public onEnter()
        {
            super.onEnter();

            const { arbiters, models } = this._context.game;

            this._canSpin = arbiters.canSpin.declare(false); // We can no longer spin
            this._canSkip = arbiters.canSkip.declare(false); // We can no longer skip
            this._spinOrSkip = arbiters.spinOrSkip.declare("spin"); // We should now be set to "spin", disabled spin button
            this._musicStateHandle = arbiters.musicState.declare(Music.State.Foreground); // Start spin music

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            // We are now in-play
            this._context.game.observables.platformGameState.value = PlatformGameState.InPlay;

            this.sendLogicRequest();
        }

        protected async sendLogicRequest()
        {
            this.doAnalytics();

            const response = await this._context.game.engine.bet(this.getEngineParams());

            // Was there an error?
            if (response.isError)
            {
                this.handleEngineError();

                return;
            }

            this.updateUI();

            this.onLogicResponseReceived(response);
        }

        protected updateUI(): void
        {
            // Update total bet observable with newly parsed total bet for edgecases where total bet may have been edited externally, eg. via Charles
            const models = this._context.game.models;
            if (models.stake.override != null)
            {
                this._context.game.uiController.setTotalBet(models.stake.override.totalBet);
                this._context.game.observables.totalBet.value = models.stake.override.totalBet;
            }
            else
            {
                const currentBetOption = RS.Models.Stake.getCurrentBet(models.stake);
                this._context.game.uiController.setTotalBet(currentBetOption.value);
                this._context.game.observables.totalBet.value = currentBetOption.value;
            }
        }

        protected getBetAmount(): Models.StakeAmount
        {
            const models = this._context.game.models;
            const override = models.stake.override;
            return override != null ? { value: override.totalBet } : Models.Stake.getCurrentBet(models.stake);
        }

        protected getEngineParams(): { betAmount: Models.StakeAmount }
        {
            const betAmount = this.getBetAmount();
            return { betAmount: betAmount }
        }

        protected doAnalytics(fail: boolean = false): void
        {
            const analytics = Analytics.IAnalytics.get();
            const models = this._context.game.models;
            if (analytics != null)
            {
                if (fail)
                {
                    if (analytics != null)
                    {
                        const isFatal = !models.error.options.some((o) => o.type === RS.Models.Error.OptionType.Continue);
                        analytics.getTracker().error({
                            message: Localisation.resolveLocalisableString(this._context.game.locale, models.error.message),
                            fatal: isFatal
                        });
                    }
                }
                else
                {
                    analytics.getTracker().event({
                        category: "Game",
                        action: "Spin",
                        value: Models.Stake.getCurrentBet(models.stake).value
                    });
                }
            }
        }

        protected async handleEngineError()
        {
            this._context.game.minWagerDurationController.cancelWager();
            this.doAnalytics(true);

            // Handle the error
            if (await this._context.game.handleError())
            {
                this.onRecoverFromError();
            }
        }

        /**
         * Called when a the place bet response has been received.
         */
        protected onLogicResponseReceived(response: RS.Engine.IResponse): void
        {
            // Calculate new balance
            this.updateBalance();

            this.transitionToNextState(Playing);
        }

        protected updateBalance()
        {
            const models = this._context.game.models;
            const baseBalance = this.getResultsModel(models)[0].balanceBefore;
            this._context.game.observables.balance.value = baseBalance.primary;
            this._context.game.observables.balances.value = baseBalance.named;
        }

        /**
         * Called when a recoverable error has been handled.
         * This might be "insufficient funds".
         */
        protected onRecoverFromError(): void
        {
            // "Continue" - in this case, go back to idle state
            this.transitionToState(Idle);
        }

        protected abstract getResultsModel(models: RS.Models): Models.GameResults
    }
}
