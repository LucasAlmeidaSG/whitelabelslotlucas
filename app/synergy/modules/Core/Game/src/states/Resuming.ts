namespace RS.GameState
{
    /**
     * The game has been resumed and is waiting for user input to progress.
     */
    export const Resuming = "resuming"

    @State.Name(Resuming)
    export abstract class ResumingState<T extends Game.Context = Game.Context> extends State.Base<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _musicStateHandle: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;

        public async onEnter()
        {
            const { arbiters, models } = this._context.game;

            this._canSpin = arbiters.canSpin.declare(false); // We can no longer spin
            this._canSkip = arbiters.canSkip.declare(false); // We can't skip
            this._spinOrSkip = arbiters.spinOrSkip.declare("spin"); // We should now be set to "spin", disabled spin button
            this._musicStateHandle = arbiters.musicState.declare(Music.State.Foreground);

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            // Restore into the 1st spin
            this._context.game.observables.platformGameState.value = PlatformGameState.Resuming;
            this._context.spinIndex = 0;
            this._context.isResuming = true;

            this.updateWin();

            await this.openView();
            await this.displayResumeDialog();
        }

        protected updateWin()
        {
            const winThisPlay = this.getResultsModel(this._context.game.models).map((s) => s.win.totalWin).reduce((a, b) => a + b, 0);
            this._context.game.observables.win.value = winThisPlay;
        }

        protected async openView()
        {
            // Open the view
            await this._context.game.viewController.open(this._context.game.settings.primaryView);
        }

        protected async displayResumeDialog()
        {
            const platform = RS.IPlatform.get();
            switch (platform.recoveryMessageHandlingMode)
            {
                case RS.PlatformMessageHandlingMode.Display:
                    await this._context.game.displayDialog({
                        ...Flow.Dialog.defaultSettings,
                        titleLabel:
                        {
                            ...Flow.Dialog.defaultSettings.titleLabel,
                            text: Translations.ResumeDialog.Title
                        },
                        messageLabel:
                        {
                            ...Flow.Dialog.defaultSettings.messageLabel,
                            text: Translations.ResumeDialog.Message
                        },
                        buttons:
                        [
                            { ...Flow.Button.defaultSettings, textLabel: { ...Flow.Button.defaultSettings.textLabel, text: Translations.ResumeDialog.Button } }
                        ]
                    });
                    break;
                case RS.PlatformMessageHandlingMode.PassToPlatform:
                    await platform.handleRecoveryMessage();
                    break;
            }
            await this.onDialogAccepted();
        }

        /**
         * Called when the user has clicked OK on the unfinished game dialog.
         */
        protected async onDialogAccepted(): Promise<void>
        {
            // Tell platform we're now in play
            this._context.game.observables.platformGameState.value = PlatformGameState.InPlay;

            if (this._context.game.models.state.state === Models.State.Type.Open)
            {
                this.transitionToNextState(Playing);
            }
            else
            {
                this.transitionToState(Idle);
            }
        }

        protected abstract getResultsModel(models: RS.Models): Models.GameResults
    }
}
