namespace RS.GameState
{
    /**
     * A play has been requested and a response received.
     * Any in-reel features (that occur during a spin) should be handled here.
     * Then it should spin down the reels.
     */
    export const Playing = "playing";

    @HasCallbacks
    @State.Name(Playing)
    export abstract class PlayingState<T extends Game.Context = Game.Context> extends State.Base<T>
    {
        /** Whether or not skip should be allowed during spin. Defaults to false for UKGC compliance. */
        public static allowSkip = false;

        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _musicStateHandle: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;
        @AutoDisposeOnSet protected _skipRequestedHandlerUI: IDisposable;

        protected _skipped: boolean;

        public onEnter()
        {
            super.onEnter()

            const shouldAllowSkip: boolean = this.shouldAllowSkip();

            const { arbiters, models } = this._context.game;
            this._canSpin = arbiters.canSpin.declare(false); // We can no longer spin
            this._canSkip = arbiters.canSkip.declare(shouldAllowSkip); // We can maybe skip?
            this._spinOrSkip = arbiters.spinOrSkip.declare(shouldAllowSkip ? "skip" : "spin"); // Show skip button if allowed, else disabled spin button
            this._skipRequestedHandlerUI = this._context.game.uiController.onSkipRequested(this.handleSkipRequested);

            this._musicStateHandle = arbiters.musicState.declare(Music.State.Foreground); // keep spin music

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            this._skipped = false;

            this.updateBalance();
        }

        protected updateBalance()
        {
            const models = this._context.game.models;
            const spinResult = this.getResultsModel(models)[this._context.spinIndex];
            this._context.game.observables.balance.value = spinResult.balanceBefore.primary;
            this._context.game.observables.balances.value = spinResult.balanceBefore.named;
        }

        /** Returns whether or not quick-stop should be allowed. */
        protected shouldAllowSkip(): boolean
        {
            return PlayingState.allowSkip;
        }

        /**
         * Called when a skip is requested.
         */
        @Callback
        protected handleSkipRequested(): void
        {
            // Check the arbiter
            if (!this._context.game.arbiters.canSkip.value) { return; }

            // Consume the skip
            if (this._canSkip)
            {
                this._canSkip.dispose();
                this._canSkip = null;

                // Handle it
                this.handleSkip();
            }
        }

        protected handleSkip()
        {
            const analytics = Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Slot Game",
                    action: "SkipReels"
                });
            }
            this._skipped = true;
        }

        protected abstract getResultsModel(models: RS.Models): Models.GameResults
    }
}
