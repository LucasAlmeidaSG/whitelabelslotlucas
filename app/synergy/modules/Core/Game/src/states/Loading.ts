namespace RS.GameState
{
    /** Responsible for preloading any required assets and performing other initialisation logic. */
    export const Loading = "loading";

    @HasCallbacks
    @State.Name(Loading)
    export abstract class LoadingState<T extends Game.Context = Game.Context> extends State.Base<T>
    {
        protected _isError: boolean = false;
        protected _loadTimer: Timer | null = null;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;

        @AutoDispose protected _errorHandler: IDisposable;

        /** Whether or not to hang to allow messing with the layout. */
        public get locked() { return this._locked; }
        public set locked(enabled)
        {
            if (this._locked === enabled) { return; }
            this._locked = enabled;
            if (!enabled)
            {
                this._onUnlocked.publish();
            }
        }

        /** Published when debug mode is disabled to allow the game to resume after hanging. */
        protected get onUnlocked() { return this._onUnlocked.public; }

        private _locked = false;
        @AutoDispose
        private readonly _onUnlocked = createSimpleEvent();

        constructor(machine: State.Machine<T>, context: T)
        {
            super(machine, context);

            this.locked = URL.getParameterAsBool("loaddebug", false);
        }

        public async onEnter()
        {
            super.onEnter();

            this._loadTimer = new Timer();

            this._errorHandler = this._context.game.assetController.onAssetLoadFailed(this.handleAssetLoadFailed);

            await this.loadLocale();
            if (this._isError) { return; }
            await this.preload();
            if (this._isError) { return; }
            await this.load();
            if (this._isError) { return; }
            await this.postload();
        }

        @Callback
        protected handleAssetLoadFailed(str: string): void
        {
            this._isError = true;

            // Notify analytics
            const analytics = Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().error({
                    message: str,
                    fatal: true
                });
            }

            // Show fatal error
            const message = `${Translations.AssetLoadError.Message1.get(this._context.game.locale)}\n\n${Translations.AssetLoadError.Message2.get(this._context.game.locale)}`;
            const platform = IPlatform.get();
            switch (platform.generalErrorHandlingMode)
            {
                case PlatformMessageHandlingMode.Ignore:
                    break;
                case PlatformMessageHandlingMode.PassToPlatform:
                    platform.handleGeneralError(message)
                        .then(() => location.reload());
                    break;
                case PlatformMessageHandlingMode.Display:
                    this._context.game.displayErrorDialog(message)
                        .then(() => location.reload());
                    break;
            }
        }

        /**
         * Loads essential assets before the loading view is even displayed.
         * Async.
         */
        protected async preload()
        {
            const platform = IPlatform.get();

            platform.preloaderProgress = 0.0;
            platform.preloaderVisible = true;

            // Load manifests
            const assetController = this._context.game.assetController;
            assetController.setGroupPriority(Asset.globalManifest, Asset.LoadPriority.Immediate);
            await assetController.loadManifests();

            // Load all assets marked as "Immediate" priority
            this._context.loadOp.value = assetController.loadByPriority(Asset.LoadPriority.Immediate);
            this._context.loadOp.value.progress.onChanged((val) => platform.preloaderProgress = val);
            await this._context.loadOp.value.complete.for(true);
            this._context.loadOp.value = null;
            if (this._isError) { throw new Error("Asset load error"); }

            platform.preloaderProgress = 1.0;
            platform.preloaderVisible = false;
        }

        /**
         * Loads locale data.
         */
        protected async loadLocale()
        {
            // Check platform
            const platform = IPlatform.get();
            if (platform.localeMode === PlatformLocaleMode.ProvidedByPlatform)
            {
                this._context.game.locale = platform.locale;
                return;
            }

            let localeCode = this._context.game.localeCode;

            // Load translation file
            let translations: Map<string>;
            try
            {
                const translationsRaw = await Request.ajax({ url: this.getTranslationFileLocation() }, Request.AJAXResponseType.Text);
                translations = JSON.parse(translationsRaw);
            }
            catch (err)
            {
                // TODO it could also be
                if (err instanceof RS.Request.ResponseError && err.code === 404)
                {
                    // Resolve fallback locale by language
                    const [targetLang] = this._context.game.localeCode.split("_");
                    localeCode = Find.matching(RS.Translations.supportedLocaleCodes, (code) =>
                    {
                        const [thisLang] = code.split("_");
                        return thisLang === targetLang;
                    });

                    // Resort to the absolute fallback otherwise
                    if (!localeCode) { localeCode = RS.Translations.defaultLocaleCode; }

                    Log.warn(`Translation file for '${this._context.game.localeCode}' does not seem to exist, falling back to '${localeCode}'...`);

                    // Try fallback
                    try
                    {
                        const translationsRaw = await Request.ajax({ url: this.getTranslationFileLocation(localeCode) }, Request.AJAXResponseType.Text);
                        translations = JSON.parse(translationsRaw);
                    }
                    catch (fallbackErr)
                    {
                        this.handleLocaleLoadError(fallbackErr);
                        return;
                    }
                }
                else
                {
                    this.handleLocaleLoadError(err);
                    return;
                }
            }

            const [lang, country] = localeCode.split("_");
            this._context.game.locale = platform.locale = new Localisation.Locale(lang, country);
            for (const key in translations)
            {
                this._context.game.locale.addString(key, translations[key]);
            }
        }

        protected handleLocaleLoadError(err): void
        {
            Log.error(`Error loading locale: ${err}`);

            // Show fatal error
            const message = `An error has occurred in the game client.`;
            const platform = IPlatform.get();
            switch (platform.generalErrorHandlingMode)
            {
                case PlatformMessageHandlingMode.Ignore:
                    break;
                case PlatformMessageHandlingMode.PassToPlatform:
                    platform.handleGeneralError(message)
                        .then(() => location.reload());
                    break;
                case PlatformMessageHandlingMode.Display:
                    this._context.game.displayErrorDialog(message)
                        .then(() => location.reload());
                    break;
            }
            this._isError = true;
        }

        /**
         * Gets the location of the translation file to load.
         */
        protected getTranslationFileLocation(localeCode: string = this._context.game.localeCode): string
        {
            const version = Version.toString(this._context.game.gameVersion);
            return Path.combine(IPlatform.get().translationsRoot, `${localeCode}.json`) + `?gamever=${version}`;
        }

        /**
         * Performs general initialisation logic, after loading screen has been displayed but before assets have started loading.
         * Initialises the engine and sends init request.
         * Async.
         */
        protected async init()
        {
            const initResponse = await this._context.game.engine.init();
            if (initResponse == null || initResponse.isError)
            {
                // Let game handle error but don't allow continue/recover as this isn't possible on init repsonse
                await this._context.game.handleError();
                this._isError = true;
                return false;
            }

            this.initObservables();

            // Move from Uninitialised -> NotReady (after all other observables set)
            this._context.game.observables.platformGameState.value = RS.PlatformGameState.NotReady;

            const { models } = this._context.game;
            const resultsModel = this.getResultsModel(models);
            // Only proceed with tweaking balance if not recovering, replaying or closure-compiled
            if (!models.state.isReplay && resultsModel.length === 0 && !RS.Util.isClosureCompiled())
            {
                const targetBalance = RS.URL.getParameterAsNumber("targetbalance", 0); //returns 0 if not present
                if (targetBalance > 0)
                {
                    await this.tweakBalance(targetBalance);
                }
            }

            // Initialise currency formatter
            this._context.game.currencyFormatter = new Localisation.CurrencyFormatter(
                models.customer.currencySettings ||
                Localisation.currencyMap[models.customer.currencyCode] ||
                Localisation.defaultCurrencySettings
            );

            return true;
        }

        protected initObservables()
        {
            const { models, observables, arbiters } = this._context.game;
            const resultsModel = this.getResultsModel(models);
            if (resultsModel.length > 0)
            {
                // Update balances to pre-spin
                const gameResult = resultsModel[this._context.spinIndex || 0];
                observables.balance.value = gameResult.balanceBefore.primary;
                observables.balances.value = gameResult.balanceBefore.named;

                // Update win to pre-spin
                const finalWin = resultsModel[resultsModel.length - 1].win.accumulatedWin;
                const winThisPlay = resultsModel.map((s) => s.win.totalWin).reduce((a, b) => a + b, 0);
                observables.win.value = finalWin - winThisPlay;
            }
            else
            {
                // Update balance to latest
                observables.balance.value = models.customer.finalBalance.primary;
                observables.balances.value = models.customer.finalBalance.named;

                // Clear win
                observables.win.value = 0;
            }

            // Update stake
            const override = models.stake.override;
            if (override != null)
            {
                observables.totalBet.value = override.totalBet;
            }
            else
            {
                const totalBet = Models.Stake.getCurrentBet(models.stake).value;
                observables.totalBet.value = totalBet;
            }

            if (models.state.isReplay)
            {
                // Attach a permanent UI-blocking handle to the game
                const handle = arbiters.viewOnlyMode.declare(true);
                RS.Disposable.bind(handle, this._context.game);
            }

            // Move from Initialised -> NotReady
            observables.platformGameState.value = PlatformGameState.NotReady;
        }

        /**
         * Displays a loading screen and performs asset load operations.
         * Async.
         */
        protected async load()
        {
            // Open the view
            if (this._context.game.settings.loadingView)
            {
                await this._context.game.viewController.open(this._context.game.settings.loadingView);
            }

            // Init the debug layer
            const platform = IPlatform.get();
            if (platform.shouldDisplayDevTools)
            {
                this._context.game.viewController.initialiseDebugLayer();
            }

            // Init
            if (!await this.init()) { return; }

            // Load all assets marked as "Preload" priority
            platform.loaderProgress = 0.0;
            this._context.loadOp.value = this._context.game.assetController.loadByPriority(Asset.LoadPriority.Preload);
            this._context.loadOp.value.progress.onChanged((val) => platform.loaderProgress = val);
            await this._context.loadOp.value.complete.for(true);
            this._context.loadOp.value = null;
            platform.loaderProgress = 1.0;

            // Get the click-to-continue promise ASAP
            const view = this._context.game.viewController.get(this._context.game.settings.loadingView);
            let clickToContinue: Promise<void>;
            if (this.shouldClickToContinue(view))
            {
                clickToContinue = this.awaitClickToContinue(view);
            }

            // Give it 500ms for the progress bar to catch up
            await ITicker.get().after(500);

            // Hang in debug mode
            if (this.locked)
            {
                await this.onUnlocked;
            }

            // Await click to continue promise.
            await clickToContinue;

            // Close the view
            await this.closeLoadingView();

            // Enable background loading
            this._context.game.assetController.backgroundLoad = Asset.LoadPriority.Low;
        }

        protected finishLoadTimer()
        {
            const loadTime = Math.round(this._loadTimer.elapsed);
            const analytics = Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().timing({
                    category: "Assets",
                    var: "Preload",
                    value: loadTime
                });
            }
        }

        protected shouldClickToContinue(view: RS.View.Base)
        {
            return !this._context.game.models.state.isReplay && isClickToContinueView(view);
        }

        protected async awaitClickToContinue(view: LoadingState.IClickToContinueView)
        {
            await view.onClickToContinue;
        }

        protected async closeLoadingView()
        {
            if (this._context.game.settings.loadingView)
            {
                await this._context.game.viewController.close();
            }
        }

        /**
         * Handles post-load logic, after the loading screen has been closed and we're ready to move on.
         * Async.
         */
        protected async postload()
        {
            // Finish timer
            this.finishLoadTimer();

            const game = this._context.game;
            const { models, observables } = game;

            // TODO - remove in 1.2, now created in init()
            // backwards compatibility for games that copy entire loading functions
            if (!game.currencyFormatter)
            {
                game.currencyFormatter = new Localisation.CurrencyFormatter(
                    models.customer.currencySettings ||
                    Localisation.currencyMap[models.customer.currencyCode] ||
                    Localisation.defaultCurrencySettings
                );
            }

            this._turboModeHandle = game.arbiters.showTurboUI.declare(models.config.turboModeEnabled);
            this.createUIController();

            game.uiController.setBalance(observables.balance.value);
            game.uiController.setWin(observables.win.value, true);

            this.setStakes();

            game.uiController.setReplayMode(game.models.state.isReplay);

            game.initialiseUI();

            // Handle restore
            if (await this.doRestore()) { return; }

            // Not restoring, default behaviour
            await this.openPrimaryView();

            // Go to idle state
            this.transitionToNextState(Idle);
        }

        protected async openPrimaryView()
        {
            const game = this._context.game;
            await game.viewController.open(game.settings.primaryView);
        }

        protected createUIController()
        {
            this._context.game.uiController.create({
                locale: this._context.game.locale,
                currencyFormatter: this._context.game.currencyFormatter
            });
        }

        protected setStakes(): void
        {
            const models = this._context.game.models;
            const override = models.stake.override;
            if (override != null)
            {
                this._context.game.uiController.setTotalBet(override.totalBet);
                this._context.game.observables.totalBet.value = override.totalBet;
            }
            else
            {
                const totalBet = Models.Stake.getCurrentBet(models.stake).value;
                this._context.game.uiController.setTotalBet(totalBet);
                this._context.game.observables.totalBet.value = totalBet;
            }
        }

        protected async doRestore(): Promise<boolean>
        {
            if (this._context.game.models.state.state !== Models.State.Type.Closed)
            {
                this.transitionToState(Resuming);
                return true;
            }
            return false;
        }

        /**
         * Attempts to reduce the starting balance on a local build to the given value by forcing losing spins
         * Errors out if target balance is greater than starting balance, or if no losing force is given in the game settings
         * @param targetBalance - value to aim for
         */
        protected async tweakBalance(targetBalance: number)
        {
            const models = this._context.game.models;
            const initBalance = models.customer.finalBalance.primary;
            if (targetBalance >= initBalance)
            {
                RS.Log.warn("Target balance higher than init balance. Not tweaking");
                return;
            }

            const force = this._context.game.settings.loseForce;
            if (!force)
            {
                RS.Log.warn("Tried to tweak balance without loseForce setting in game settings.");
                return;
            }

            //get bets to place
            let toSubtract = initBalance - targetBalance;
            const bets: Models.StakeAmount[] = [];
            for (let i = models.stake.betOptions.length - 1; i >= 0; i--)
            {
                const bet = models.stake.betOptions[i];

                const mod = RS.Math.floor(toSubtract / bet.value);
                for (let times = 0; times < mod; times++) { bets.push(bet); }

                toSubtract -= (mod * bet.value);
            }

            for (const bet of bets)
            {
                this._context.game.engine.force(force);
                const spin = await this._context.game.engine.bet({
                    betAmount: bet
                });

                // Was there an error?
                if (spin.isError)
                {
                    await this._context.game.handleError();
                    break;
                }

                const end = await this._context.game.engine.close();
                // Was there an error?
                if (end.isError)
                {
                    await this._context.game.handleError();
                    break;
                }

                //check if actually decreasing balance
                if (models.customer.finalBalance.primary >= initBalance)
                {
                    RS.Log.warn("Balance didn't go down after first spin, stopping tweak");
                    break;
                }
            }

            //reset back to default bet index
            models.stake.currentBetIndex = models.stake.defaultBetIndex;
        }

        protected abstract getResultsModel(models: RS.Models): Models.GameResults
    }

    function isClickToContinueView(view: RS.View.Base): view is LoadingState.IClickToContinueView
    {
        return Is.func((view as LoadingState.IClickToContinueView).onClickToContinue);
    }

    export namespace LoadingState
    {
        export interface IClickToContinueView extends RS.View.Base
        {
            readonly onClickToContinue: RS.IEvent<void>;
        }
    }
}
