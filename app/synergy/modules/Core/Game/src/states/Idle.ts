namespace RS.GameState
{
    export let idleMessageTextMode: IdleMessageTextMode = null;

    /** Standard game state. */
    export const Idle = "idle";

    @HasCallbacks
    @State.Name(Idle)
    export abstract class IdleState<T extends Game.Context = Game.Context> extends RS.State.Base<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _uiInteractable: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _canChangeBet: IDisposable;
        @AutoDisposeOnSet protected _musicStateHandle: IDisposable;
        @AutoDisposeOnSet protected _statusTextHandle: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;

        @AutoDisposeOnSet protected _spinRequestedHandler: IDisposable;

        @AutoDisposeOnSet protected _canSpinHandler: IDisposable;
        @AutoDisposeOnSet protected _autospinWaitTween: Tween<{}>;

        @AutoDisposeOnSet protected _balanceUpdatedExternallyHandler: IDisposable;

        public onEnter()
        {
            super.onEnter();

            // // Open the view
            // if (this._context.game.settings.primaryView)
            // {
            //     await this._context.game.viewController.open(this._context.game.settings.primaryView);
            // }

            this._context.spinIndex = 0;

            // We can now spin
            const { arbiters, models } = this._context.game;
            this._musicStateHandle = arbiters.musicState.declare(Music.State.Background);
            this._canChangeBet = arbiters.canChangeBet.declare(true);
            this._spinRequestedHandler = this._context.game.uiController.onSpinRequested(this.handleSpinRequested);

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            this.updateObservables();

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            this.doReplay();

            // Display bet breakdown message
            const messageText = this.getBetBreakdown();
            if (messageText != null)
            {
                this._statusTextHandle = this._context.game.arbiters.messageText.declare(messageText);
            }

            this.checkAutoBet();
        }

        protected updateObservables()
        {
            const models = this._context.game.models;

            // We are no longer in-play
            this._context.game.observables.platformGameState.value = PlatformGameState.Ready;

            // Update balance(s)
            this._context.game.observables.balance.value = models.customer.finalBalance.primary;
            this._context.game.observables.balances.value = models.customer.finalBalance.named;

            // Deal with external balance updates
            this._balanceUpdatedExternallyHandler = RS.IPlatform.get().onBalanceUpdatedExternally((newBalance) =>
            {
                RS.Log.debug(`Got external balance update (${newBalance})`);
                this._context.game.observables.balance.value = newBalance;
            });
        }

        /**
         * Called when we try to spin again when in autoplay.
         * Waits 1s to give the game a slight delay between spins.
         */
        protected autospinWaitTween(): void
        {
            this._autospinWaitTween = RS.Tween.wait(1000)
                .call((t) => this.requestSpin());
        }

        /**
         * Gets called at the end of onEnter
         * e.g. override for freespins
         */
        protected checkAutoBet()
        {
            this.stayIdle();
        }

        protected stayIdle()
        {
            const arbiters = this._context.game.arbiters;

            this._spinOrSkip = arbiters.spinOrSkip.declare("spin"); // We should now be set to "spin"
            this._canSkip = arbiters.canSkip.declare(false); // We can no longer skip
            this._uiInteractable = arbiters.uiEnabled.declare(true);
            this._canSpin = arbiters.canSpin.declare(true);
        }

        /**
         * Gets the bet breakdown message to show.
         * Will return null to show no bet breakdown.
         */
        protected getBetBreakdown(): RS.Localisation.LocalisableString | null
        {
            return null;
        }

        protected requestSpin()
        {
            this.handleSpinRequested();
        }

        @Callback
        protected handleSpinRequested(): void
        {
            if (this._context.game.observables.platformGameState.value === PlatformGameState.Ready)
            {
                this._context.game.minWagerDurationController.beginWager();
            }
            this.transitionToNextState(PrePlay);
        }

        protected abstract doReplay(): void;
        protected abstract getResultsModel(models: RS.Models): Models.GameResults;
    }
}
