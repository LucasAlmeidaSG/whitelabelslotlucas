namespace RS.GameState
{
    export const MaxWin = "maxWin";
    @HasCallbacks
    @State.Name(MaxWin)
    export class MaxWinState<T extends Game.Context = Game.Context> extends State.Base<T>
    {
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;
        
        public async onEnter()
        {
            super.onEnter();

            const { models, observables, arbiters }  = this._context.game;

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            if (RS.IPlatform.get().maxWinMode == RS.PlatformMaxWinMode.None)
            {
                this.transitionToNextState(PlayFinished);
                return;
            }

            observables.win.value = models.config.maxWinAmount;

            await this._context.game.displayMaxWinDialog();

            this.transitionToNextState(PlayFinished);
        }
    }
}
