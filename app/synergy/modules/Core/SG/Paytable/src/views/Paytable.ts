namespace SG.CommonUI.Views
{
    /**
     * The Paytable view.
     */
    @RS.HasCallbacks
    @RS.View.RequiresAssets([ Assets.Groups.commonUI ])
    @RS.View.ScreenName("Paytable")
    export class Paytable<TSettings extends Paytable.Settings = Paytable.Settings, TContext extends RS.Game.Context = RS.Game.Context> extends RS.View.Base<TSettings, TContext>
    {
        @RS.AutoDispose protected _background: RS.Flow.Background | void;
        @RS.AutoDispose protected _navBar: SG.CommonUI.PaytableNavBar;
        @RS.AutoDispose protected _body: SG.CommonUI.PaytableBody;

        @RS.AutoDispose protected _rightPanel: RS.Flow.Container;
        @RS.AutoDispose protected _leftPanel: RS.Flow.Container;

        @RS.AutoDispose protected _portraitLogo: RS.Flow.Image | void;
        @RS.AutoDispose protected _landscapeLogo: RS.Flow.Image | void;

        /** The back button. */
        @RS.AutoDispose protected _prevButton: RS.Flow.Button;
        @RS.AutoDispose protected _nextButton: RS.Flow.Button;
        @RS.AutoDispose protected _closeButton: RS.Flow.Button;

        @RS.AutoDispose protected _buttonList: RS.Flow.List;

        @RS.AutoDisposeOnSet protected _hideButtonAreaHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _hideMeterPanelHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _hideMessageBarHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _uiEnabledHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _paytableVisibleHandle: RS.IDisposable;

        /** True once the paytable has been drawn for the first time. */
        protected _drawn: boolean = false;

        protected _suppressionCount: number = 0;

        /** The paytable sections, which contain the pages. */
        protected _sections: Paytable.Section[];

        /** Current section being displayed. */
        protected _curSectionIndex: number;
        /** Current page being displayed. */
        protected _curPageIndex: number;

        protected get navigationEnabled()
        {
            return this._closeButton.enabled || this._prevButton.enabled || this._nextButton.enabled;
        }
        protected set navigationEnabled(value: boolean)
        {
            this.suppressLayouting();
            this._closeButton.enabled = value;
            this._prevButton.enabled = value;
            this._nextButton.enabled = value;
            this.restoreLayouting();
        }

        public suppressLayouting()
        {
            ++this._suppressionCount;
            if (this._suppressionCount === 1)
            {
                return super.suppressLayouting();
            }
            return this;
        }

        public restoreLayouting(forceInvalidate?: boolean)
        {
            if (this._suppressionCount === 0)
            {
                return this;
            }
            --this._suppressionCount;
            if (this._suppressionCount === 0)
            {
                return super.restoreLayouting(forceInvalidate);
            }
            return this;
        }

        public onOpened()
        {
            super.onOpened();

            this.suppressLayouting();

            this.locale = this.context.game.locale;
            this.currencyFormatter = this.context.game.currencyFormatter;

            this.initBody();
            this.buildSections();

            this.initBackground();
            this.initLeftPanel();
            this.initRightPanel();

            this.initLandscapeLogo();
            this.initPortraitLogo();
            this.initNextButton();
            this.initPrevButton();
            this.initCloseButton();
            this.initNavBar();
            this.initButtonList();

            // Finishes opening.
            const { arbiters } = this.context.game;
            this._uiEnabledHandle = arbiters.uiEnabled.declare(false);
            this._hideButtonAreaHandle = arbiters.showButtonArea.declare(false);
            this._hideMeterPanelHandle = arbiters.showMeterPanel.declare(false);
            this._hideMessageBarHandle = arbiters.showMessageBar.declare(false);
            this._paytableVisibleHandle = arbiters.paytableVisible.declare(true);

            this.gotoPage(0, 0);

            this.doLayout(this.context.game.orientation.value);

            this.restoreLayouting();
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            for (let i = 0; i < this._sections.length; i++)
            {
                for (let j = 0; j < this._sections[i].pages.length; j++)
                {
                    this._sections[i].pages[j].element.dispose();
                }
            }
            super.dispose();
        }

        protected initBackground(): void
        {
            if (!this.settings.background) { return; }
            this._background = RS.Flow.background.create(this.settings.background, this.visibleRegionPanel);
            this.visibleRegionPanel.moveToBottom(this._background);
        }

        protected initBody(): void
        {
            this._body = paytableBody.create(this.settings.body, this.visibleRegionPanel);
        }

        protected initLeftPanel(): void
        {
            this._leftPanel = RS.Flow.container.create(
            {
                ...this.settings.sidePanels,
                name: "LeftPanel",
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0, y: 0 },
                dockAlignment: { x: 0, y: 0 },
                expand: RS.Flow.Expand.Disallowed,
                contentAlignment: { x: 0, y: 0.5 },
                sizeToContents: false
            });
        }

        protected initRightPanel(): void
        {
            this._rightPanel = RS.Flow.container.create(
            {
                ...this.settings.sidePanels,
                name: "RightPanel",
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 1, y: 0 },
                dockAlignment: { x: 1, y: 0 },
                expand: RS.Flow.Expand.Disallowed,
                contentAlignment: { x: 1, y: 0.5 },
                sizeToContents: false
            });
        }

        protected initLandscapeLogo(): void
        {
            const settings = this.settings.landscapeLogo || this.settings.logo;
            if (settings)
            {
                this._landscapeLogo = RS.Flow.image.create(settings);
            }
        }

        protected initPortraitLogo(): void
        {
            const settings = this.settings.portraitLogo || this.settings.logo;
            if (settings)
            {
                this._portraitLogo = RS.Flow.image.create(settings);
            }
        }

        protected initNextButton(): void
        {
            this._nextButton = RS.Flow.button.create(this.settings.rightButton);
            this._nextButton.onClicked(this.handleRightClicked);
        }

        protected initCloseButton(): void
        {
            this._closeButton = RS.Flow.button.create(this.settings.closeButton);
            this._closeButton.onClicked(this.handleCloseClicked);
        }

        protected initPrevButton(): void
        {
            this._prevButton = RS.Flow.button.create(this.settings.leftButton);
            this._prevButton.onClicked(this.handleLeftClicked);
        }

        protected initNavBar(): void
        {
            this._navBar = paytableNavBar.create(this.settings.navBar, { sections: this._sections.map((s) => s.pages.length) });
        }

        protected initButtonList(): void
        {
            this._buttonList = RS.Flow.list.create(
            {
                name: "Button List",
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,

                // Default to TopToBottom direction.
                direction: RS.Flow.List.Direction.TopToBottom
            });
        }

        protected buildSections(): void
        {
            this._sections = [];
            for (const section of this.settings.sections)
            {
                const s: Paytable.Section = { pages: [] };
                for (const pageInfo of section.pages)
                {
                    let pages = pageInfo.element.create(
                    {
                        configModel: this.context.game.models.config,
                        stakeModel: this.context.game.models.stake
                    });

                    if (!RS.Is.array(pages))
                    {
                        pages = [pages];
                    }

                    for (const pageElement of pages)
                    {
                        s.pages.push(
                        {
                            title: pageInfo.title,
                            element: pageElement
                        });
                    }
                }
                if (s.pages.length > 0)
                {
                    this._sections.push(s);
                }
            }

            if (this._sections.length === 0)
            {
                const tmpLabel: RS.Flow.Label.Settings =
                {
                    dock: RS.Flow.Dock.Fill,
                    text: "Add some paytable pages to your game!",
                    font: RS.Fonts.Frutiger.CondensedMedium,
                    fontSize: 32,
                    textColor: RS.Util.Colors.white
                };
                this._body.page = RS.Flow.label.create(tmpLabel);
            }
        }

        protected gotoPage(sectionIndex: number, pageIndex: number): void
        {
            this.suppressLayouting();

            this.disableButtonsForDuration();

            this._curSectionIndex = sectionIndex;
            this._curPageIndex = pageIndex;
            this.normalisePage();
            const curSection = this._sections[this._curSectionIndex];
            if (curSection == null) { return; }

            this._body.page = curSection.pages[this._curPageIndex].element;
            this._navBar.sectionIndex = this._curSectionIndex;
            this._navBar.pageIndex = this._curPageIndex;
            this._body.pageTitle = curSection.pages[this._curPageIndex].title;

            // Calculate the global page index
            let pageID = 0;
            for (let i = 0; i < sectionIndex; i++)
            {
                pageID += this._sections[i].pages.length;
            }
            pageID += pageIndex;
            // Better way to expose this data?
            SGI.Selenium.SeleniumControl.setText(`${pageID}`);
            this.restoreLayouting();
        }

        protected normalisePage(): void
        {
            this._curSectionIndex = RS.Math.clamp(this._curSectionIndex, 0, this._sections.length);
            const curSection = this._sections[this._curSectionIndex];
            if (curSection == null) { return; }
            this._curPageIndex = RS.Math.clamp(this._curPageIndex, 0, curSection.pages.length);
        }

        protected gotoNextPage(): void
        {
            this.normalisePage();
            const curSection = this._sections[this._curSectionIndex];
            this._curPageIndex++;
            if (this._curPageIndex >= curSection.pages.length)
            {
                this._curPageIndex = 0;
                this._curSectionIndex++;
                if (this._curSectionIndex >= this._sections.length)
                {
                    this._curSectionIndex = 0;
                }
            }
            this.gotoPage(this._curSectionIndex, this._curPageIndex);
        }

        protected gotoPreviousPage(): void
        {
            this.normalisePage();
            this._curPageIndex--;
            if (this._curPageIndex < 0)
            {
                this._curSectionIndex--;
                if (this._curSectionIndex < 0)
                {
                    this._curSectionIndex = this._sections.length - 1;
                }
                this._curPageIndex = this._sections[this._curSectionIndex].pages.length - 1;
            }
            this.gotoPage(this._curSectionIndex, this._curPageIndex);
        }

        protected disableButtonsForDuration()
        {
            this.navigationEnabled = false;
            const duration = this.settings.pageDelay == null ? 250 : this.settings.pageDelay;
            RS.ITicker.get().after(duration).then(() => this.navigationEnabled = true);
        }

        @RS.Callback
        protected handleCloseClicked(): void
        {
            this.navigationEnabled = false;
            this.controller.close();
        }

        @RS.Callback
        protected handleLeftClicked(): void
        {
            this.gotoPreviousPage();
        }

        @RS.Callback
        protected handleRightClicked(): void
        {
            this.gotoNextPage();
        }

        @RS.Callback
        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation): void
        {
            super.handleOrientationChanged(newOrientation);

            this.doLayout(newOrientation);
        }

        protected updateFixedLayout(orientation: RS.DeviceOrientation): void
        {
            if (!this._body) { return; }

            this.suppressLayouting();
            const bottomBarHeight: number = this.context.game.uiController.enclosedSpace.bottom;
            switch (orientation)
            {
                case RS.DeviceOrientation.Landscape:
                {
                    const paytableHeight = this.visibleRegionPanel.desiredSize.h - bottomBarHeight;

                    this._body.size =
                    {
                        w: this.settings.dimensions.landscapeWidth,
                        h: paytableHeight
                    };

                    this.visibleRegionPanel.innerPadding =
                    {
                        ...RS.Flow.Spacing.none,
                        bottom: bottomBarHeight
                    };

                    if (this._leftPanel && this._rightPanel)
                    {
                        const bodyWidth = this._body.desiredSize.w;
                        const totalWidth = this.visibleRegionPanel.desiredSize.w;
                        const panelWidth = (totalWidth - bodyWidth) / 2;
                        this._leftPanel.size = this._rightPanel.size =
                        {
                            w: panelWidth,

                            // Multiply bottomBarHeight by two as side panels are vertically centred.
                            h: paytableHeight
                        };
                    }

                    break;
                }

                case RS.DeviceOrientation.Portrait:
                {
                    this._body.size =
                    {
                        w: this.visibleRegionPanel.desiredSize.w,
                        h: this.settings.dimensions.portraitHeight
                    };

                    this.visibleRegionPanel.innerPadding =
                    {
                        ...RS.Flow.Spacing.none,
                        bottom: bottomBarHeight
                    };

                    break;
                }
            }
            this.restoreLayouting();
        }

        @RS.Callback
        protected handleViewportResized(): void
        {
            super.handleViewportResized();

            this.updateFixedLayout(this.context.game.orientation.value);
        }

        protected doLayout(orientation: RS.DeviceOrientation): void
        {
            this.suppressLayouting();

            this._leftPanel.removeAllChildren();
            this._rightPanel.removeAllChildren();
            this._buttonList.removeAllChildren();

            // Stop the body re-layouting when it's remove since it no longer has a suppressed parent.
            this._body.suppressLayouting();
            this.visibleRegionPanel.removeAllChildren();
            this.visibleRegionPanel.addChild(this._body);
            this._body.restoreLayouting(false);

            this.updateFixedLayout(orientation);

            switch (orientation)
            {
                case RS.DeviceOrientation.Landscape:
                {
                    this.visibleRegionPanel.spacing = RS.Flow.Spacing.none;

                    // Attach body to top of screen.
                    this._body.dock = RS.Flow.Dock.Float;
                    this._body.floatPosition = { x: 0.5, y: 0 };
                    this._body.dockAlignment = { x: 0.5, y: 0 };

                    this._body.mode = SG.CommonUI.PaytableBody.Mode.Landscape;

                    // Display left and right panels.
                    this.visibleRegionPanel.addChildren(this._leftPanel, this._rightPanel);

                    // Move prev button and nav bar to the left side.
                    this._leftPanel.addChild(this._navBar);
                    this._navBar.dock = RS.Flow.Dock.Left;
                    this._navBar.mode = SG.CommonUI.PaytableNavBar.Mode.Portrait;

                    this._leftPanel.addChild(this._prevButton);
                    this._prevButton.dock = RS.Flow.Dock.Left;

                    // Move next and close buttons to the right side.
                    this._rightPanel.addChildren(this._nextButton, this._closeButton);
                    this._nextButton.dock = RS.Flow.Dock.Fill;
                    this._closeButton.dock = RS.Flow.Dock.Bottom;

                    // Place landscape logo above the button list.
                    if (this._landscapeLogo)
                    {
                        this._rightPanel.addChild(this._landscapeLogo);
                        this._landscapeLogo.dock = RS.Flow.Dock.Top;
                        this._landscapeLogo.expand = RS.Flow.Expand.Disallowed;
                    }

                    break;
                }

                case RS.DeviceOrientation.Portrait:
                {
                    this.visibleRegionPanel.spacing = RS.Flow.Spacing.vertical(this.settings.portraitElementSpacing);

                    // Stretch body across view horizontally.
                    this._body.dock = RS.Flow.Dock.Bottom;
                    this._body.mode = SG.CommonUI.PaytableBody.Mode.Portrait;

                    this.visibleRegionPanel.addChild(this._buttonList, 0);
                    this._buttonList.dock = RS.Flow.Dock.Bottom;
                    this._buttonList.direction = RS.Flow.List.Direction.LeftToRight;
                    this._buttonList.spacing = RS.Flow.Spacing.horizontal(this.settings.portraitButtonSpacing);

                    this._buttonList.addChildren(this._prevButton, this._closeButton, this._nextButton);
                    this._prevButton.dock = RS.Flow.Dock.None;

                    // Move nav-bar to the top.
                    this.visibleRegionPanel.addChild(this._navBar);
                    this._navBar.dock = RS.Flow.Dock.Bottom;
                    this._navBar.mode = SG.CommonUI.PaytableNavBar.Mode.Landscape;

                    // Place portrait logo above everything.
                    if (this._portraitLogo)
                    {
                        this.visibleRegionPanel.addChild(this._portraitLogo, 0);
                        this._portraitLogo.dock = RS.Flow.Dock.Top;
                        this._portraitLogo.expand = RS.Flow.Expand.Disallowed;
                    }

                    break;
                }
            }

            if (this._background) { this.visibleRegionPanel.addChild(this._background, 0); }

            this.restoreLayouting();
        }
    }

    export namespace Paytable
    {
        export interface Section
        {
            pages:
            {
                title: RS.Localisation.LocalisableString;
                element: RS.Flow.GenericElement;
            }[];
        }

        export interface PageInfo
        {
            title: RS.Localisation.LocalisableString;
            element: RS.Paytable.IPageFactory<RS.Paytable.Pages.Base.Settings, RS.Paytable.Pages.Base.RuntimeData>;
        }

        export interface SectionInfo
        {
            title: RS.Localisation.LocalisableString;
            pages: PageInfo[];
        }

        export interface DimensionSettings
        {
            landscapeWidth: number;
            portraitHeight: number;
        }

        export interface Settings extends RS.View.Base.Settings
        {
            dimensions: DimensionSettings;
            navBar: SG.CommonUI.PaytableNavBar.Settings;
            body: SG.CommonUI.PaytableBody.Settings;
            background?: RS.Flow.Background.Settings;
            sidePanels?: Partial<RS.Flow.ElementProperties>;

            leftButton: RS.Flow.Button.Settings;
            rightButton: RS.Flow.Button.Settings;
            closeButton: RS.Flow.Button.Settings;

            portraitElementSpacing: number;
            portraitButtonSpacing: number;

            logo?: RS.Flow.Image.Settings;
            portraitLogo?: RS.Flow.Image.Settings;
            landscapeLogo?: RS.Flow.Image.Settings;

            sections: SectionInfo[];

            /**
             * Delay in ms between page changes. Prevents spamming of next/prev buttons that lead to reaching memory limits.
             * Defaults to 250
            */
            pageDelay?: number;
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.CommonUI.BottomBar.Buttons,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export let defaultSettings: Settings =
        {
            navBar: SG.CommonUI.PaytableNavBar.defaultSettings,
            body: SG.CommonUI.PaytableBody.defaultSettings,

            sidePanels:
            {
                spacing: RS.Flow.Spacing.axis(16, 10)
            },

            portraitElementSpacing: 16,
            portraitButtonSpacing: 80,

            dimensions:
            {
                landscapeWidth: 660,
                portraitHeight: 1000
            },

            leftButton:
            {
                name: "Left",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left_down },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "PreviousHelpButton"
            },
            rightButton:
            {
                name: "Right",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right_down },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "NextHelpButton"
            },
            closeButton:
            {
                name: "Close",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close_down },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "ExitHelpButton"
            },
            sections: [],
            pageDelay: 250
        };
    }
}
