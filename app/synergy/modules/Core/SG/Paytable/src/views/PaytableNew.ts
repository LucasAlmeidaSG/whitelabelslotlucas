/// <reference path="../elements/PaytableNavBarNew.ts"/>

namespace SG.CommonUI.Views
{
    @RS.HasCallbacks
    @RS.View.ScreenName("Paytable New")
    @RS.View.RequiresAssets([SG.CommonUI.Assets.Groups.commonUI])
    export class PaytableNew<TSettings extends PaytableNew.Settings = PaytableNew.Settings, TContext extends RS.Game.Context = RS.Game.Context> extends RS.View.Base<TSettings, TContext>
    {
        @RS.AutoDisposeOnSet protected _background: RS.Flow.Background | void;
        @RS.AutoDisposeOnSet protected _navBar: SG.CommonUI.PaytableNavBarNew;
        @RS.AutoDisposeOnSet protected _body: SG.CommonUI.PaytableBody;

        @RS.AutoDisposeOnSet protected _logo: RS.Flow.Image;

        @RS.AutoDisposeOnSet protected _prevButton: RS.Flow.Button;
        @RS.AutoDisposeOnSet protected _nextButton: RS.Flow.Button;
        @RS.AutoDisposeOnSet protected _closeButton: RS.Flow.Button;

        @RS.AutoDisposeOnSet protected _buttonList: RS.Flow.List;

        @RS.AutoDisposeOnSet protected _pipClickedHandle: RS.IDisposable;

        protected _suppressionCount: number = 0;

        protected _sections: Paytable.Section[];

        protected _curSectionIndex: number;
        protected _curPageIndex: number;

        protected _changingPage: boolean = false;
        protected _sectionPageSetting: PaytableNew.PageSetting[][];

        protected get navigationEnabled()
        {
            return this._closeButton.enabled || this._prevButton.enabled || this._nextButton.enabled;
        }
        protected set navigationEnabled(value: boolean)
        {
            this.suppressLayouting();
            this._closeButton.enabled = value;
            this._prevButton.enabled = value;
            this._nextButton.enabled = value;
            this._navBar.interactiveChildren = value;
            this.restoreLayouting();
        }

        public suppressLayouting()
        {
            ++this._suppressionCount;
            if (this._suppressionCount === 1)
            {
                return super.suppressLayouting();
            }
            return this;
        }

        public restoreLayouting(forceInvalidate?: boolean)
        {
            if (this._suppressionCount === 0)
            {
                return this;
            }
            --this._suppressionCount;
            if (this._suppressionCount === 0)
            {
                return super.restoreLayouting(forceInvalidate);
            }
            return this;
        }

        public onOpened()
        {
            super.onOpened();

            this.suppressLayouting();

            this.locale = this.context.game.locale;
            this.currencyFormatter = this.context.game.currencyFormatter;

            const currentOrientation: RS.DeviceOrientation = this.context.game.orientation.value;
            const isLandscape: boolean = currentOrientation === RS.DeviceOrientation.Landscape;
            const orientationSettings: PaytableNew.OrientationSettings = isLandscape ? this.settings.landscape : this.settings.portrait;

            this.initBody();

            this.buildSections();

            this.initButtonList(orientationSettings);
            this.initBackground();
            this.initLogo(orientationSettings);
            this.initNextButton();
            this.initPrevButton();
            this.initCloseButton();
            this.initNavBar();

            const { arbiters } = this.context.game;

            const uiDisableHandle = arbiters.uiEnabled.declare(false);
            RS.Disposable.bind(uiDisableHandle, this);

            const hideButtonAreaHandle = arbiters.showButtonArea.declare(false);
            RS.Disposable.bind(hideButtonAreaHandle, this);

            const hideMessageBarHandle = arbiters.showMessageBar.declare(false);
            RS.Disposable.bind(hideMessageBarHandle, this);

            const hideMeterPanelhandle = arbiters.showMeterPanel.declare(false);
            RS.Disposable.bind(hideMeterPanelhandle, this);

            const paytableVisibleHandle = arbiters.paytableVisible.declare(true);
            RS.Disposable.bind(paytableVisibleHandle, this);

            this.gotoPage(0, 0);

            this.doLayout(currentOrientation);

            this.restoreLayouting();
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            for (let i = 0; i < this._sections.length; i++)
            {
                for (let j = 0; j < this._sections[i].pages.length; j++)
                {
                    if (this._sections[i].pages[j].element)
                    {
                        this._sections[i].pages[j].element.dispose();
                        this._sections[i].pages[j].element = null;
                    }
                }
            }
            super.dispose();
        }

        //#region Create

        protected initBackground(): void
        {
            if (!this.settings.background) { return; }
            this._background = RS.Flow.background.create(this.settings.background, this.visibleRegionPanel);
            this.visibleRegionPanel.moveToBottom(this._background);
        }

        protected initBody(): void
        {
            this._body = paytableBody.create(this.settings.body, this.visibleRegionPanel);
        }

        protected initButtonList(orientationSettings: PaytableNew.OrientationSettings): void
        {
            this._buttonList = RS.Flow.list.create(orientationSettings.buttonList);
        }

        protected initLogo(orientationSettings: PaytableNew.OrientationSettings): void
        {
            if (orientationSettings.logo != null)
            {
                this._logo = RS.Flow.image.create(orientationSettings.logo, this.visibleRegionPanel);
            }
        }

        protected initNextButton(): void
        {
            this._nextButton = RS.Flow.button.create(this.settings.rightButton);
            this._nextButton.onClicked(this.handleRightClicked);
        }

        protected initCloseButton(): void
        {
            this._closeButton = RS.Flow.button.create(this.settings.closeButton);
            this._closeButton.onClicked(this.handleCloseClicked);
        }

        protected initPrevButton(): void
        {
            this._prevButton = RS.Flow.button.create(this.settings.leftButton);
            this._prevButton.onClicked(this.handleLeftClicked);
        }

        protected initNavBar(): void
        {
            this._navBar = paytableNavBarNew.create(this.settings.navBar, { sections: this._sections.map((s) => s.pages.length) });

            this._pipClickedHandle = this._navBar.onPipClicked(({ section, page }) =>
            {
                this.gotoPage(section, page);
            });
        }

        protected buildSections(): void
        {
            this._sections = [];
            let sectionIndex = 0;
            this._sectionPageSetting = [];
            for (const section of this.settings.sections)
            {
                const s: Paytable.Section = { pages: [] };
                let pageIndex = 0;
                this._sectionPageSetting[sectionIndex] = [];
                for (const pageInfo of section.pages)
                {
                    let pages = this.buildPage(pageInfo);

                    for (let i = 0; i < pages.length; i++)
                    {
                        this._sectionPageSetting[sectionIndex][pageIndex] =
                        {
                            element: pageInfo,
                            pageIndex: i
                        };
                        pageIndex += 1;
                    }

                    if (!RS.Is.array(pages))
                    {
                        pages = [pages];
                    }

                    for (const pageElement of pages)
                    {
                        s.pages.push(
                        {
                            title: pageInfo.title,
                            element: pageElement
                        });
                    }
                }
                if (s.pages.length > 0)
                {
                    this._sections.push(s);
                    sectionIndex += 1;
                }
            }

            if (this._sections.length === 0)
            {
                const tmpLabel: RS.Flow.Label.Settings =
                {
                    dock: RS.Flow.Dock.Fill,
                    text: "Add some paytable pages to your game!",
                    font: RS.Fonts.Frutiger.CondensedMedium,
                    fontSize: 32,
                    textColor: RS.Util.Colors.white
                };
                this._body.page = RS.Flow.label.create(tmpLabel);
            }
        }

        protected buildPage(pageInfo: Paytable.PageInfo)
        {
            let pages = pageInfo.element.create(
            {
                configModel: this.context.game.models.config,
                stakeModel: this.context.game.models.stake
            });
            if (!RS.Is.array(pages))
            {
                pages = [pages];
            }
            return pages;
        }

        //#endregion Create
        //#region Update

        protected gotoPage(sectionIndex: number, pageIndex: number): void
        {
            if (this._changingPage) { return; }
            // only change one page at a time, to avoid errors
            this._changingPage = true;
            this.suppressLayouting();

            this.disableButtonsForDuration();
            const oldSectionIndex = this._curSectionIndex;
            const oldPageIndex = this._curPageIndex;

            this._curSectionIndex = sectionIndex;
            this._curPageIndex = pageIndex;

            //rebuild page if needed
            const section = this._sections[this._curSectionIndex];
            const pageElement = section.pages[this._curPageIndex].element;
            if (!pageElement)
            {
                const pageInfo = this._sectionPageSetting[this._curSectionIndex][this._curPageIndex].element;
                //incase there are multiple pages from this info we now know which one to use
                const elementIndex = this._sectionPageSetting[this._curSectionIndex][this._curPageIndex].pageIndex;
                const builtPages = this.buildPage(pageInfo);
                this._sections[this._curSectionIndex].pages[this._curPageIndex].element = builtPages[elementIndex];
            }

            this.normalisePage();
            const curSection = this._sections[this._curSectionIndex];
            if (curSection == null) { return; }

            this._body.page = curSection.pages[this._curPageIndex].element;
            this._navBar.sectionIndex = this._curSectionIndex;
            this._navBar.pageIndex = this._curPageIndex;
            this._body.pageTitle = curSection.pages[this._curPageIndex].title;

            // Calculate the global page index
            let pageID = 0;
            for (let i = 0; i < sectionIndex; i++)
            {
                pageID += this._sections[i].pages.length;
            }
            pageID += pageIndex;
            // Better way to expose this data?
            SGI.Selenium.SeleniumControl.setText(`${pageID}`);
            this.restoreLayouting();
            this.invalidateLayout();
            // delete old page for performance(memory) increase
            if (oldSectionIndex != null && oldPageIndex != null)
            {
                this._sections[oldSectionIndex].pages[oldPageIndex].element.dispose();
                this._sections[oldSectionIndex].pages[oldPageIndex].element = null;
            }
            this._changingPage = false;
        }

        protected normalisePage(): void
        {
            this._curSectionIndex = RS.Math.clamp(this._curSectionIndex, 0, this._sections.length);
            const curSection = this._sections[this._curSectionIndex];
            if (curSection == null) { return; }
            this._curPageIndex = RS.Math.clamp(this._curPageIndex, 0, curSection.pages.length);
        }

        protected gotoNextPage(): void
        {
            this.normalisePage();
            const curSection = this._sections[this._curSectionIndex];
            let nextPage = this._curPageIndex + 1;
            let nextSection = this._curSectionIndex;
            if (nextPage >= curSection.pages.length)
            {
                nextPage = 0;
                nextSection++;
                if (nextSection >= this._sections.length)
                {
                    nextSection = 0;
                }
            }
            this.gotoPage(nextSection, nextPage);
        }

        protected gotoPreviousPage(): void
        {
            this.normalisePage();
            let nextPage = this._curPageIndex - 1;
            let nextSection = this._curSectionIndex;
            if (nextPage < 0)
            {
                nextSection--;
                if (nextSection < 0)
                {
                    nextSection = this._sections.length - 1;
                }
                nextPage = this._sections[nextSection].pages.length - 1;
            }
            this.gotoPage(nextSection, nextPage);
        }

        protected disableButtonsForDuration()
        {
            this.navigationEnabled = false;
            const duration = this.settings.pageDelay == null ? 250 : this.settings.pageDelay;
            RS.ITicker.get().after(duration).then(() => this.navigationEnabled = true);
        }

        //#endregion Update
        //#region Event

        @RS.Callback
        protected handleCloseClicked(): void
        {
            this.navigationEnabled = false;
            this.controller.close();
        }

        @RS.Callback
        protected handleLeftClicked(): void
        {
            this.gotoPreviousPage();
        }

        @RS.Callback
        protected handleRightClicked(): void
        {
            this.gotoNextPage();
        }

        //#endregion Event

        @RS.Callback
        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation): void
        {
            super.handleOrientationChanged(newOrientation);

            this.doLayout(newOrientation);
        }

        protected doLayout(orientation: RS.DeviceOrientation): void
        {
            this.suppressLayouting();

            const isLandscape: boolean = orientation === RS.DeviceOrientation.Landscape;
            const orientationSettings: PaytableNew.OrientationSettings = isLandscape ? this.settings.landscape : this.settings.portrait;
            const bottomBarHeight: number = this.context.game.uiController.enclosedSpace.bottom;

            this._buttonList.removeAllChildren();

            this._body.suppressLayouting();
            this.visibleRegionPanel.removeAllChildren();
            this.visibleRegionPanel.addChild(this._body);
            this._body.restoreLayouting(false);

            this.initButtonList(orientationSettings);

            this.visibleRegionPanel.innerPadding =
            {
                ...RS.Flow.Spacing.none,
                bottom: bottomBarHeight
            };

            if (orientation === RS.DeviceOrientation.Landscape)
            {
                this._body.mode = SG.CommonUI.PaytableBody.Mode.Landscape;

                this._buttonList.addChildren(this._nextButton, this._prevButton, this._closeButton);
            }
            else if (orientation === RS.DeviceOrientation.Portrait)
            {
                this._body.mode = SG.CommonUI.PaytableBody.Mode.Portrait;

                this._buttonList.addChildren(this._prevButton, this._closeButton, this._nextButton);
            }

            this._body.body.addChild(this._navBar, 0);
            if (this.settings.spacePriorityToButtons)
            {
                this.visibleRegionPanel.addChildUnder(this._buttonList, this._body);
            }
            else
            {
                this.visibleRegionPanel.addChild(this._buttonList);
            }

            this.initLogo(orientationSettings);

            if (this._background) { this.visibleRegionPanel.addChild(this._background, 0); }

            this.restoreLayouting();
        }
    }

    export namespace PaytableNew
    {
        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.CommonUI.BottomBar.Buttons,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export interface OrientationSettings
        {
            buttonList: RS.Flow.List.Settings;
            logo?: RS.Flow.Image.Settings;
        }

        export interface Settings extends RS.View.Base.Settings
        {
            portrait: OrientationSettings;
            landscape: OrientationSettings;

            navBar: PaytableNavBarNew.Settings;
            body: PaytableBody.Settings;
            background?: RS.Flow.Background.Settings;

            leftButton: RS.Flow.Button.Settings;
            rightButton: RS.Flow.Button.Settings;
            closeButton: RS.Flow.Button.Settings;

            sections: Paytable.SectionInfo[];

            /**
             * If set to true, it will add the buttons list as a child before the body, in the visible region panel.
             * The body will therefore shrink to ensure the buttons are always full size
             */
            spacePriorityToButtons?: boolean;

            /**
             * Delay in ms between page changes. Prevents spamming of next/prev buttons that lead to reaching memory limits.
             * Defaults to 250
            */
            pageDelay?: number;
        }

        export let defaultSettings: Settings =
        {
            landscape:
            {
                buttonList:
                {
                    dock: RS.Flow.Dock.Right,
                    sizeToContents: true,
                    direction: RS.Flow.List.Direction.TopToBottom,
                    spacing: RS.Flow.Spacing(0, 20, 50, 20)
                }
            },
            portrait:
            {
                buttonList:
                {
                    dock: RS.Flow.Dock.Bottom,
                    sizeToContents: true,
                    direction: RS.Flow.List.Direction.LeftToRight,
                    spacing: RS.Flow.Spacing(20, 0, 20, 50)
                }
            },
            leftButton:
            {
                name: "Left",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_left_down },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "PreviousHelpButton"
            },
            rightButton:
            {
                name: "Right",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_right_down },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "NextHelpButton"
            },
            closeButton:
            {
                name: "Close",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.pbtn_close_down },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "ExitHelpButton"
            },
            navBar: PaytableNavBarNew.defaultSettings,
            body:
            {
                ...PaytableBody.defaultSettings,
                dock: RS.Flow.Dock.Fill,
                size: RS.Math.Size2D(1080, 1080)
            },
            sections: [],
            pageDelay: 250
        }

        export interface PageSetting
        {
            element: Paytable.PageInfo;
            pageIndex: number;
        }
    }
}
