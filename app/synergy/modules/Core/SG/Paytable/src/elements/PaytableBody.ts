namespace SG.CommonUI
{
    export class PaytableBody extends RS.Flow.BaseElement<PaytableBody.Settings>
    {
        @RS.AutoDispose protected _bgLandscape: RS.Flow.Background;
        @RS.AutoDispose protected _bgPortrait: RS.Flow.Background;
        @RS.AutoDispose protected _title: RS.Flow.Label;
        @RS.AutoDispose protected _body: RS.Flow.Container;

        protected _page: RS.Flow.GenericElement | null = null;
        protected _mode: PaytableBody.Mode;

        /** Gets or sets the paytable page to be displayed. */
        public get page() { return this._page; }
        public set page(value)
        {
            if (value === this._page) { return; }
            if (this._page != null)
            {
                this._page.suppressLayouting();
                this._body.removeChild(this._page);
                this._page.restoreLayouting(false);
                this._page = null;
            }
            this._page = value;
            if (this._page != null)
            {
                this._page.suppressLayouting();
                this._body.addChild(this._page);
                this._page.restoreLayouting(false);
            }
            this.invalidateLayout();
        }

        public get body() { return this._body; }

        /** Gets or sets the title of the paytable page to be displayed. */
        public get pageTitle() { return this._title.text; }
        public set pageTitle(value) { this._title.text = value; }

        /** Gets or sets the mode of this nav bar. */
        public get mode() { return this._mode; }
        public set mode(value)
        {
            if (this._mode === value) { return; }
            this._mode = value;
            this.suppressLayouting();
            switch (value)
            {
                case PaytableBody.Mode.Landscape:
                    this._bgLandscape.visible = true;
                    this._bgPortrait.visible = false;
                    break;
                case PaytableBody.Mode.Portrait:
                    this._bgLandscape.visible = false;
                    this._bgPortrait.visible = true;
                    break;
            }
            this.restoreLayouting();
        }

        public constructor(settings: PaytableBody.Settings)
        {
            super(settings, undefined);

            this._bgLandscape = RS.Flow.background.create(settings.backgroundLandscape, this);
            this._bgPortrait = RS.Flow.background.create(settings.backgroundPortrait, this);
            this._title = RS.Flow.label.create(settings.title, this);
            this._body = RS.Flow.container.create({
                sizeToContents: false,
                dock: RS.Flow.Dock.Fill,
                legacy: false,
            }, this);

            this.mode = PaytableBody.Mode.Landscape;
        }
    }

    export const paytableBody = RS.Flow.declareElement(PaytableBody);

    export namespace PaytableBody
    {
        export enum Mode
        {
            Landscape,
            Portrait
        }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            backgroundLandscape: RS.Flow.Background.Settings;
            backgroundPortrait: RS.Flow.Background.Settings;
            title: RS.Flow.Label.Settings;
        }

        export let defaultSettings: Settings =
        {
            backgroundLandscape:
            {
                kind: RS.Flow.Background.Kind.NinePatch,
                asset: Assets.CommonUI.Paytable.Background.Landscape,
                borderSize: RS.Flow.Spacing.horizontal(8)
            },
            backgroundPortrait:
            {
                kind: RS.Flow.Background.Kind.NinePatch,
                asset: Assets.CommonUI.Paytable.Background.Portrait,
                borderSize: RS.Flow.Spacing.vertical(8)
            },
            title:
            {
                dock: RS.Flow.Dock.Top,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 32,
                textColor: RS.Util.Colors.white,
                sizeToContents: true,
                dockAlignment: { x: 0.5, y: 0.5 },
                text: ""
            },
            spacing: RS.Flow.Spacing.all(20),
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            dockAlignment: { x: 0.5, y: 0.5 },
            sizeToContents: false,
            expand: RS.Flow.Expand.Disallowed
        };
    }
}
