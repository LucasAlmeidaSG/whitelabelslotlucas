namespace SG.CommonUI
{
    export class PaytableNavBar extends RS.Flow.BaseElement<PaytableNavBar.Settings, PaytableNavBar.RuntimeData>
    {
        @RS.AutoDispose protected _bg: RS.Flow.Background;
        @RS.AutoDispose protected _list: RS.Flow.List;

        protected _categories: PaytableNavBar.Section[];

        protected _settings: PaytableNavBar.Settings;

        protected _sectionIndex: number = 0;
        protected _pageIndex: number = 0;
        protected _mode: PaytableNavBar.Mode;

        /** Gets or sets the currently selected section index. */
        public get sectionIndex() { return this._sectionIndex; }
        public set sectionIndex(value)
        {
            if (this._sectionIndex == value) { return; }
            this._sectionIndex = value;
            this.update();
        }

        /** Gets or sets the currently selected section index. */
        public get pageIndex() { return this._sectionIndex; }
        public set pageIndex(value)
        {
            if (this._pageIndex == value) { return; }
            this._pageIndex = value;
            this.update();
        }

        /** Gets or sets the mode of this nav bar. */
        public get mode() { return this._mode; }
        public set mode(value)
        {
            if (this._mode === value) { return; }
            this._mode = value;
            this.suppressLayouting();
            switch (value)
            {
                case PaytableNavBar.Mode.Landscape:
                {
                    if (this._settings.backgroundPortrait)
                    {
                        if (this._bg) { this._bg.dispose(); }
                        this._bg = RS.Flow.background.create(this._settings.background, this);
                        this.swapChildren(this._bg, this._list);
                    }
                    else if (this._settings.rotateBackground)
                    {
                        if (this.settings.background.rotation)
                        {
                            this._bg.rotation = this.settings.background.rotation;
                        }
                        else
                        {
                            this._bg.rotation = 0;
                        }
                    }

                    this._list.direction = RS.Flow.List.Direction.TopToBottom;
                    this._list.rotation = -RS.Math.Angles.quarterCircle;

                    break;
                }

                case PaytableNavBar.Mode.Portrait:
                {
                    if (this._settings.backgroundPortrait)
                    {
                        if (this._bg) { this._bg.dispose(); }
                        this._bg = RS.Flow.background.create(this._settings.backgroundPortrait, this);
                        this.swapChildren(this._bg, this._list);
                    }
                    else if (this._settings.rotateBackground)
                    {
                        if (this.settings.background.rotation)
                        {
                            this._bg.rotation = this.settings.background.rotation - RS.Math.Angles.quarterCircle;
                        }
                        else
                        {
                            this._bg.rotation = -RS.Math.Angles.quarterCircle;
                        }
                    }

                    this._list.direction = RS.Flow.List.Direction.TopToBottom;
                    this._list.rotation = 0;

                    break;
                }
            }
            this.restoreLayouting();
        }

        public constructor(settings: PaytableNavBar.Settings, runtimeData: PaytableNavBar.RuntimeData)
        {
            super(settings, runtimeData);
            this._settings = settings;

            this._bg = RS.Flow.background.create(settings.background, this);
            this._list = RS.Flow.list.create(
            {
                name: "PageIndicators",
                direction: RS.Flow.List.Direction.TopToBottom,
                dock: RS.Flow.Dock.Fill,
                contentAlignment: { x: 0.5, y: 0.5 },
                spacing: settings.listSpacing,
                sizeToContents: true
            }, this);

            const catCount = runtimeData.sections.length;
            this._categories = new Array<PaytableNavBar.Section>(catCount);
            for (let catIndex = 0; catIndex < catCount; catIndex++)
            {
                const inactiveDot = RS.Flow.image.create(settings.inactiveDot, this._list);
                const activeDot = RS.Flow.image.create(settings.activeDot, this._list);

                const dotName = `[${catIndex}]`;
                inactiveDot.name = `${dotName} (Inactive)`;
                activeDot.name = `${dotName} (Active)`;

                const section: PaytableNavBar.Section = { inactiveDot, activeDot, inactiveStripes: [], activeStripes: [] };

                const pageCount = runtimeData.sections[catIndex];
                for (let pageIndex = 0; pageIndex < pageCount; pageIndex++)
                {
                    const inactiveStripe = RS.Flow.image.create(settings.inactiveStripe, this._list);
                    const activeStripe = RS.Flow.image.create(settings.activeStripe, this._list);

                    const stripeName = `[${catIndex}, ${pageIndex}]`;
                    inactiveStripe.name = `${stripeName} (Inactive)`;
                    activeStripe.name = `${stripeName} (Active)`;

                    section.inactiveStripes.push(inactiveStripe);
                    section.activeStripes.push(activeStripe);
                }

                this._categories[catIndex] = section;
            }

            this.mode = PaytableNavBar.Mode.Portrait;

            this.update();
        }

        protected update(): void
        {
            this.suppressLayouting();
            const catCount = this.runtimeData.sections.length;
            for (let catIndex = 0; catIndex < catCount; catIndex++)
            {
                const section = this._categories[catIndex];

                section.activeDot.visible = catIndex <= this._sectionIndex;
                section.inactiveDot.visible = !section.activeDot.visible;

                const pageCount = this.runtimeData.sections[catIndex];
                for (let pageIndex = 0; pageIndex < pageCount; pageIndex++)
                {
                    section.activeStripes[pageIndex].visible = catIndex == this._sectionIndex ? pageIndex <= this._pageIndex : section.activeDot.visible;
                    section.inactiveStripes[pageIndex].visible = !section.activeStripes[pageIndex].visible;
                }
            }
            this.restoreLayouting();
        }
    }

    export const paytableNavBar = RS.Flow.declareElement(PaytableNavBar, true);

    export namespace PaytableNavBar
    {
        export enum Mode
        {
            Landscape,
            Portrait
        }

        export interface Section
        {
            inactiveDot: RS.Flow.Image;
            activeDot: RS.Flow.Image;
            inactiveStripes: RS.Flow.Image[];
            activeStripes: RS.Flow.Image[];
        }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            background: RS.Flow.Background.Settings;
            backgroundPortrait?: RS.Flow.Background.Settings;
            rotateBackground?: boolean; // ignored if backgroundPortrait is defined
            listSpacing: RS.Flow.Spacing;
            activeDot: RS.Flow.Image.Settings;
            activeStripe: RS.Flow.Image.Settings;
            inactiveDot: RS.Flow.Image.Settings;
            inactiveStripe: RS.Flow.Image.Settings;
        }

        export interface RuntimeData
        {
            sections: number[];
        }

        export let defaultSettings: Settings =
        {
            background:
            {
                kind: RS.Flow.Background.Kind.NinePatch,
                asset: Assets.CommonUI.Paytable.NavBar.Background,
                borderSize: RS.Flow.Spacing.all(25),
                expand: RS.Flow.Expand.Allowed,
                size: { w: 50, h: 50 }
            },
            activeDot: // 31 x 30
            {
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.nav_section_on,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                border: RS.Flow.Spacing.all(4)
            },
            inactiveDot: // 23 x 22
            {
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.nav_section_off,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                border: RS.Flow.Spacing.none
            },
            activeStripe: // 44 x 12
            {
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.nav_page_on,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                border: RS.Flow.Spacing.all(4)
            },
            inactiveStripe: // 36 x 4
            {
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.nav_page_off,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                border: RS.Flow.Spacing.none
            },
            listSpacing: RS.Flow.Spacing.axis(5, 15),
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed,
            dock: RS.Flow.Dock.Left,
            dockAlignment: { x: 0.5, y: 0.5 },
            spacing: RS.Flow.Spacing.all(10)
        };
    }
}