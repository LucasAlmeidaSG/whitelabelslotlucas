namespace SG.CommonUI
{
    @RS.HasCallbacks
    export class PaytableNavBarNew extends RS.Flow.BaseElement<PaytableNavBarNew.Settings, PaytableNavBarNew.RuntimeData>
    {
        @RS.AutoDisposeOnSet protected readonly _onPipClicked: RS.IPrivateEvent<{ section: number, page: number }> = RS.createEvent<{ section: number, page: number }>();

        @RS.AutoDisposeOnSet protected readonly _bg: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected readonly _list: RS.Flow.List;

        @RS.AutoDisposeOnSet protected _sections: PaytableNavBarNew.Section[][];

        @RS.AutoDisposeOnSet protected _sectionIndex: number = 0;
        @RS.AutoDisposeOnSet protected _pageIndex: number = 0;

        public get onPipClicked() { return this._onPipClicked.public; }

        /** Gets or sets the currently selected section index. */
        public get sectionIndex() { return this._sectionIndex; }
        public set sectionIndex(value)
        {
            if (this._sectionIndex == value) { return; }
            this._sectionIndex = value;
            this.update();
        }

        /** Gets or sets the currently selected section index. */
        public get pageIndex() { return this._sectionIndex; }
        public set pageIndex(value)
        {
            if (this._pageIndex == value) { return; }
            this._pageIndex = value;
            this.update();
        }

        public constructor(settings: PaytableNavBarNew.Settings, runtimeData: PaytableNavBarNew.RuntimeData)
        {
            super(settings, runtimeData);

            if (settings.background != null)
            {
                this._bg = RS.Flow.background.create(settings.background, this);
            }

            this._list = RS.Flow.list.create(settings.dotList, this);

            const catCount = runtimeData.sections.length;
            this._sections = new Array<Array<PaytableNavBarNew.Section>>(catCount);

            for (let catIndex = 0; catIndex < catCount; catIndex++)
            {
                const pageCount: number = runtimeData.sections[catIndex];

                this._sections[catIndex] = new Array<PaytableNavBarNew.Section>(pageCount);

                for (let pageIndex: number = 0; pageIndex < pageCount; pageIndex++)
                {
                    const inactiveDot = RS.Flow.image.create(settings.inactiveDot, this._list);
                    inactiveDot.onClicked(this.handleClick);
                    inactiveDot.buttonMode = true;

                    const activeDot = RS.Flow.image.create(settings.activeDot, this._list);

                    this._sections[catIndex][pageIndex] = { inactiveDot, activeDot };
                }
            }

            this.update();
        }

        protected update(): void
        {
            this.suppressLayouting();

            const catCount = this.runtimeData.sections.length;

            for (let catIndex = 0; catIndex < catCount; catIndex++)
            {
                const pageCount: number = this.runtimeData.sections[catIndex];

                for (let pageIndex: number = 0; pageIndex < pageCount; pageIndex++)
                {
                    const section = this._sections[catIndex][pageIndex];

                    section.activeDot.visible = catIndex === this._sectionIndex && pageIndex === this._pageIndex;
                    section.inactiveDot.visible = !section.activeDot.visible;
                }
            }

            this.restoreLayouting();
        }

        @RS.Callback
        protected handleClick(data: RS.Rendering.InteractionEventData): void
        {
            const index: number = this._list.getChildIndex(data.target);

            if (index > -1)
            {
                let sectionIndex: number = 0;
                let pageIndex: number = 0;
                let runningTotal: number = 0;

                const isInactivePip: boolean = index % 2 === 0;
                const adjustedSectionIndex: number = isInactivePip ? index / 2 : (index - 1) / 2;

                this._sections.forEach((section: PaytableNavBarNew.Section[], sIndex: number) =>
                {
                    section.forEach((page: PaytableNavBarNew.Section, pIndex: number) =>
                    {
                        if (runningTotal === adjustedSectionIndex)
                        {
                            sectionIndex = sIndex;
                            pageIndex = pIndex;
                        }

                        runningTotal++;
                    });
                });

                this._onPipClicked.publish({section: sectionIndex, page: pageIndex});

                if (this.settings.clickSound != null)
                {
                    RS.Audio.play(this.settings.clickSound);
                }
            }
        }
    }

    export const paytableNavBarNew = RS.Flow.declareElement(PaytableNavBarNew, true);

    export namespace PaytableNavBarNew
    {
        export interface Section
        {
            inactiveDot: RS.Flow.Image;
            activeDot: RS.Flow.Image;
        }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            background?: RS.Flow.Background.Settings;
            dotList: RS.Flow.List.Settings;
            activeDot: RS.Flow.Image.Settings;
            inactiveDot: RS.Flow.Image.Settings;
            clickSound?: RS.Asset.SoundReference;
        }

        export interface RuntimeData
        {
            sections: number[];
        }

        export let defaultSettings: Settings =
        {
            background:
            {
                kind: RS.Flow.Background.Kind.NinePatch,
                asset: Assets.CommonUI.Paytable.NavBar.Background,
                borderSize: RS.Flow.Spacing.all(25),
                expand: RS.Flow.Expand.Allowed,
                size: { w: 50, h: 50 }
            },
            activeDot: // 31 x 30
            {
                name: "Page Indicator Active",
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.nav_section_on,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                border: RS.Flow.Spacing.all(4)
            },
            inactiveDot: // 23 x 22
            {
                name: "Page Indicator Inactive",
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.nav_section_off,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                border: RS.Flow.Spacing.none
            },
            dotList:
            {
                name: "Page Indicator List",
                direction: RS.Flow.List.Direction.LeftToRight,
                dock: RS.Flow.Dock.Bottom,
                contentAlignment: { x: 0.5, y: 0.5 },
                sizeToContents: true,
                spacing: RS.Flow.Spacing.axis(5, 15),
                evenlySpaceItems: true
            },
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed,
            dock: RS.Flow.Dock.Bottom,
            spacing: RS.Flow.Spacing.all(10)
        };
    }
}