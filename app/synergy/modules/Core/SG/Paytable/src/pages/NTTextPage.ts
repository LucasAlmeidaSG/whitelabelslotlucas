namespace Core.SG.Paytable.Pages
{
    export class NTTextPage<TSettings extends NTTextPage.Settings = NTTextPage.Settings> extends RS.Paytable.Pages.TextPage<TSettings>
    {
        protected shouldExcludeElement(element: NTTextPage.NTElementSettings): boolean
        {
            const platform: IPlatform = RS.IPlatform.get();
            const isNorskTipping = platform.isNT || false;

            if (super.shouldExcludeElement(element)) { return true; }
            if (element.ntDisplayMode == NTTextPage.NTDisplayMode.NotNT && isNorskTipping) { return true; }
            if (element.ntDisplayMode == NTTextPage.NTDisplayMode.NTOnly && !isNorskTipping) { return true; }
            return false;
        }
    }

    export namespace NTTextPage
    {
        export type NTElementSettings = RS.Paytable.Pages.TextPage.Element & { ntDisplayMode?: NTDisplayMode };

        export interface Settings extends RS.Paytable.Pages.TextPage.Settings
        {
            /** List of elements to be added to the page */
            elements: NTElementSettings[];
        }

        export enum NTDisplayMode
        {
            Always,
            NTOnly,
            NotNT
        }
    }
}
