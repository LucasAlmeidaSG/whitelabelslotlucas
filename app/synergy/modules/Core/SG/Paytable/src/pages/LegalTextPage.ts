/// <reference path="NTTextPage.ts" />
namespace Core.SG.Paytable.Pages
{
    export namespace LegalTextPage
    {
        // We may well be able to set this to ${(new Date()).getFullYear()} but pending legal
        export let copyrightYear = "2019";

        export const Settings = (rtpParagraph: RS.Paytable.Pages.TextPage.RtpTextSettings | RS.Paytable.Pages.TextPage.SingleRtpTextSettings): NTTextPage.Settings =>
        ({
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            elements:
            [
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.MaxWinText,
                    text: RS.Translations.Paytable.Disclaimer.MaxWin
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Independent
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Outcome
                },
                rtpParagraph,
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Malfunction
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.VersionText,
                    text: RS.Translations.Paytable.Disclaimer.Client
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.YearText,
                    text: RS.Translations.Paytable.Disclaimer.Copyright,
                    ntDisplayMode: NTTextPage.NTDisplayMode.NotNT
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: `TM and ©${copyrightYear} Scientific Games Corp. and its Subsidiaries. All rights reserved.`,
                    languageCodeBlackList: ["en"],
                    ntDisplayMode: NTTextPage.NTDisplayMode.NotNT
                }
            ],
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultPageSettings,
                forceYear: copyrightYear
            }
        });

        export const singleRTPSettings: RS.Paytable.Pages.TextPage.Settings =
        {
            ...Settings(
            {
                kind: RS.Paytable.Pages.TextPage.ElementKind.SingleRTPText,
                text: RS.Translations.Paytable.Disclaimer.SingleRTP
            })
        };

        export const rangeRTPSettings: RS.Paytable.Pages.TextPage.Settings =
        {
            ...Settings(
            {
                kind: RS.Paytable.Pages.TextPage.ElementKind.RTPText,
                text: RS.Translations.Paytable.Disclaimer.RTP
            })
        };
    }
}
