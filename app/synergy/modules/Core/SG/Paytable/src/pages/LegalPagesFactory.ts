
/// <reference path="LegalTextPage.ts" />
/// <reference path="NTLegalPage.ts" />
namespace Core.SG.Paytable.Pages
{
    export class LegalPagesFactory implements RS.Paytable.IPageFactory<NTTextPage.Settings, RS.Paytable.Pages.Base.RuntimeData>
    {
        constructor(protected readonly ctor: new (settings: NTTextPage.Settings, runtimeData: RS.Paytable.Pages.Base.RuntimeData) => NTTextPage = NTTextPage, protected readonly settings: LegalPagesFactory.Settings = LegalPagesFactory.defaultSettings)
        {

        }

        public create(overrideSettings: Partial<NTTextPage.Settings>, runtimeData: RS.Paytable.Pages.Base.RuntimeData): NTTextPage[];

        public create(runtimeData: RS.Paytable.Pages.Base.RuntimeData): NTTextPage[];

        public create(overrideSettingsOrRuntimeData: Partial<NTTextPage.Settings> | RS.Paytable.Pages.Base.RuntimeData, runtimeData?: RS.Paytable.Pages.Base.RuntimeData): NTTextPage[]
        {
            let overrideSettings: Partial<NTTextPage.Settings> = {};
            if (runtimeData)
            {
                overrideSettings = overrideSettingsOrRuntimeData;
            }
            else
            {
                runtimeData = overrideSettingsOrRuntimeData as RS.Paytable.Pages.Base.RuntimeData;
            }

            const platform: IPlatform = RS.IPlatform.get();
            const isNorskTipping: boolean = platform.isNT || false;
            const pageSettingsList: NTTextPage.Settings[] = [];
            if (runtimeData.configModel.minRTP == runtimeData.configModel.maxRTP)
            {
                pageSettingsList.push(this.settings.singleRTPPage);
            }
            else
            {
                pageSettingsList.push(this.settings.rangeRTPPage);
            }
            if (isNorskTipping)
            {
                pageSettingsList.push(this.settings.ntPage);
            }

            const pages: NTTextPage[] = [];
            for (const pageSettings of pageSettingsList)
            {
                if (!pageSettings) { continue; }
                const page = new this.ctor(
                {
                    ...overrideSettings,
                    ...pageSettings,
                }, runtimeData);
                pages.push(page);
            }

            return pages;
        }
    }

    export namespace LegalPagesFactory
    {
        export interface Settings extends RS.Paytable.Pages.Base.Settings
        {
            singleRTPPage: RS.Paytable.Pages.TextPage.Settings;
            rangeRTPPage: RS.Paytable.Pages.TextPage.Settings;
            ntPage: NTTextPage.Settings;
        }

        export const defaultSettings: Settings =
        {
            singleRTPPage: LegalTextPage.singleRTPSettings,
            rangeRTPPage: LegalTextPage.rangeRTPSettings,
            ntPage: NTLegalTextPage.Settings(false)
        }
    }
}
