namespace Core.SG.Paytable.Pages
{
    export namespace NTLegalTextPage
    {
        const jackpotLines: (hasJackpots: boolean) => RS.Paytable.Pages.TextPage.Element[] = (hasJackpots: boolean) =>
        {
            return hasJackpots ? [
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.HasJackpot
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Winnings
                },
            ] :
            [
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.NoJackpot
                }
            ]
        }

        export const Settings: (hasJackpots: boolean) => RS.Paytable.Pages.TextPage.Settings = (hasJackpots: boolean) =>
        ({
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                listSettings:
                {
                    ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings.listSettings,
                    spacing: RS.Flow.Spacing.vertical(10),
                },
            },
            elements:
            [
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.MaxBetText,
                    text: RS.Translations.Paytable.Disclaimer.MaxBet
                },
                ...jackpotLines(hasJackpots),
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Skills
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.NoInfluence
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Unfinished
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.UnfinishedExample
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Resuming
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Stored
                },
                {
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: RS.Translations.Paytable.Disclaimer.Timeout
                }
            ]
        });
    }
}
