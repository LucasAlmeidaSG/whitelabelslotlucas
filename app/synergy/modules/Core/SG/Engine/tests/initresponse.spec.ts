/// <reference path="TestXML.ts" />

namespace RS.Tests
{
    const { expect } = chai;

    describe("InitResponse.ts", function ()
    {
        it("should correctly parse a response", function ()
        {
            const xmlResponse = new DOMParser().parseFromString(glmInitResponse, "text/xml");
            const response = new SG.GLM.Core.InitResponse(null);
            RS.Serialisation.XML.deserialise(response, xmlResponse);

            const observed = JSON.parse(JSON.stringify(response));
            const expected = JSON.parse(glmInitResponseParsed);

            expect(observed).to.deep.equal(expected);
        });

    });
}
