/// <reference path="TestXML.ts" />

namespace RS.Tests
{
    const { expect } = chai;

    describe("LogicResponse.ts", function ()
    {
        it("should correctly parse a response", function ()
        {
            const xmlResponse = new DOMParser().parseFromString(glmLogicResponse, "text/xml");
            const response = new SG.GLM.Core.LogicResponse(null);
            RS.Serialisation.XML.deserialise(response, xmlResponse);

            const observed = JSON.parse(JSON.stringify(response));
            const expected = JSON.parse(glmLogicResponseParsed);

            expect(observed).to.deep.equal(expected);
        });

    });
}
