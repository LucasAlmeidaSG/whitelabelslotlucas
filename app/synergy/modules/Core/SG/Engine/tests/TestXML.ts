namespace RS.Tests
{
    export const glmInitResponse: string =
`<GameResponse type="Init">
    <Header sessionID="OVZDy6eCax1mV01KNXm3PrOZJQwAPAv1kzfwSJlsPus=" ccyCode="en_GB" deciSep="." thousandSep="," lang="en_GB" gameID="20240" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
    <AccountData></AccountData>
    <Balances>
        <Balance name="CASH_BALANCE" value="100000"/>
    </Balances>
    <Stakes count="9" defaultIndex="2" type="0">50|100|200|300|500|1000|2000|3000|5000</Stakes>
    <GameVariantInfo highRtpMinStake="200" lowRtp="95.63" highRtp="95.63"/>
</GameResponse>`;

    export const glmInitResponseParsed: string = `{"request":null,"error":null,"header":{"affiliate":null,"ccyCode":"en_GB","channel":null,"freePlay":null,"gameCodeRGI":null,"gameID":20240,"glsID":null,"lang":"en_GB","promotions":null,"sessionID":"OVZDy6eCax1mV01KNXm3PrOZJQwAPAv1kzfwSJlsPus=","userID":null,"userType":null,"versionID":"1_0","fullVersionID":"1.0.-1","isRecovering":false},"accountData":{"currencyInformation":null,"currencyMultiplier":null,"payload":null},"balances":[{"name":"CASH_BALANCE","value":100000}],"stakes":[50,100,200,300,500,1000,2000,3000,5000],"defaultStakeIndex":2,"gameVariantInfo":{"lowRTP":95.63,"highRTP":95.63},"gameInfo":null}`;

    export const glmLogicResponse: string =
`<GameResponse type="Logic">
    <Header sessionID="Avd0OYsn/teVLZ06fWIEAfLvJXiu5LoeY5j+RY99Y8UyrVuBWVthXiTc4oR81gRDSo/xl9Zq0xhyI/RklUa7/KWl4Q+QUEHMb35CvxLLeC0=" ccyCode="en_GB" deciSep="." thousandSep="," lang="en_GB" gameID="20240" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
    <AccountData>
        <AccountData>
            <CurrencyMultiplier>1</CurrencyMultiplier>
        </AccountData>
    </AccountData>
    <Balances>
        <Balance name="CASH_BALANCE" value="99820"/>
    </Balances>
    <GameResult stake="200" stakePerLine="4" paylineCount="50" totalWin="20" betID="">
    </GameResult>
</GameResponse>`;

    export const glmLogicResponseParsed: string = `{"request":null,"error":null,"header":{"affiliate":null,"ccyCode":"en_GB","channel":null,"freePlay":null,"gameCodeRGI":null,"gameID":20240,"glsID":null,"lang":"en_GB","promotions":null,"sessionID":"Avd0OYsn/teVLZ06fWIEAfLvJXiu5LoeY5j+RY99Y8UyrVuBWVthXiTc4oR81gRDSo/xl9Zq0xhyI/RklUa7/KWl4Q+QUEHMb35CvxLLeC0=","userID":null,"userType":null,"versionID":"1_0","fullVersionID":"1.0.-1","isRecovering":false},"accountData":{"currencyInformation":null,"currencyMultiplier":null,"payload":null},"balances":[{"name":"CASH_BALANCE","value":99820}],"gameResult":{"stake":200,"totalWin":20,"betID":""}}`;
}
