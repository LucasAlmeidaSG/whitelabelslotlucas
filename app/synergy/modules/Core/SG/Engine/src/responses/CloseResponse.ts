/// <reference path="../CommonData.ts" />

namespace SG.GLM.Core
{
    import XML = RS.Serialisation.XML;

    /*
    <GameResponse type="EndGame">
        <Header sessionID="6tc5f2dTNJh8t/Hacf13ocQz779Og0L93rrg/OBtkcE=" ccyCode="" deciSep="." thousandSep="," lang="en_gb" gameID="20232" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
        <AccountData></AccountData>
        <Balances>
            <Balance name="CASH_BALANCE" value="100000"/>
        </Balances>
    </GameResponse>
    */

    /* tslint:disable:member-ordering */

    export class CloseResponse implements RS.Engine.IResponse
    {
        public constructor(public readonly request: CloseRequest) { }

        @XML.Property({ path: "Error", type: XML.Object })
        public error?: Error;

        @XML.Property({ path: "Header", type: XML.Object })
        public header: Header;

        @XML.Property({ path: "AccountData", type: XML.Object })
        public accountData?: AccountData;

        @XML.Property({ path: "Balances", type: XML.Array(XML.Object) })
        public balances: Balance[];

        public get isError() { return this.error != null; }
    }

    /* tslint:enable:member-ordering */
}
