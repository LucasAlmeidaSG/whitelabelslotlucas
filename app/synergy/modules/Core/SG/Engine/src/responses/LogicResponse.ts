/// <reference path="../CommonData.ts" />

namespace SG.GLM.Core
{
    import XML = RS.Serialisation.XML;

    /*
    <GameResponse type="Logic">
        <Header sessionID="Phaa/gibeD2orcQKfny2ZGGaSnI+IAVAta0B6viWzJYw06lRBbmWS9+A3HgQVwld/aEY31msl/a2KbnMh7ZP2eQJfDYzuYmM6AgOj1NfwVg=" ccyCode="" deciSep="." thousandSep="," lang="en_gb" gameID="20232" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
        <AccountData></AccountData>
        <Balances>
            <Balance name="CASH_BALANCE" value="112500"/>
        </Balances>
        <GameResult stake="50000" stakePerLine="2500" paylineCount="20" totalWin="62500" betID="">
            <ReelResults numSpins="1">
                <ReelSpin spinIndex="0" reelsetIndex="0" winCountPL="2" winCountSC="0" spinWins="62500" freeSpin="N" bonusAwarded="N">
                    <ReelStops>15|51|4|14|49</ReelStops>
                    <PaylineWin index="4" winVal="12500" awardIndex="2" awardTableIndex="0">2|6|10</PaylineWin>
                    <PaylineWin index="14" winVal="50000" awardIndex="11" awardTableIndex="0">7|10|11</PaylineWin>
                </ReelSpin>
            </ReelResults>
            <BGInfo totalWagerWin="62500" bgWinnings="62500" baseGameSpinsRemaining="0" isBigBet="0" isMaxWin="0"/>
        </GameResult>
    </GameResponse>
    */

    /* tslint:disable:member-ordering */

    @XML.Type("GameResult")
    export class GameResult
    {
        @XML.Property({ path: ".stake", type: XML.Number })
        public stake: number;

        @XML.Property({ path: ".totalWin", type: XML.Number })
        public totalWin: number;

        @XML.Property({ path: ".betID", type: XML.String })
        public betID: string;
    }

    export class LogicResponse implements RS.Engine.IResponse
    {
        public constructor(public readonly request: LogicRequest) { }

        @XML.Property({ path: "Error", type: XML.Object })
        public error?: Error;

        @XML.Property({ path: "Header", type: XML.Object })
        public header: Header;

        @XML.Property({ path: "AccountData", type: XML.Object })
        public accountData?: AccountData;

        @XML.Property({ path: "Balances", type: XML.Array(XML.Object) })
        public balances: Balance[];

        @XML.Property({ path: "GameResult", type: XML.Object })
        public gameResult: GameResult;

        public get isError() { return this.error != null; }
    }

    /* tslint:enable:member-ordering */
}
