namespace SG.GLM.Core
{
    import XML = RS.Serialisation.XML;

    @XML.Type("Header")
    export class Header
    {
        @XML.Property({ path: ".affiliate", type: XML.String })
        public affiliate?: string;

        @XML.Property({ path: ".ccyCode", type: XML.String })
        public ccyCode: string;

        @XML.Property({ path: ".channel", type: XML.String })
        public channel?: string;

        @XML.Property({ path: ".freePlay", type: XML.BooleanEx({ serialiseTrueAs: "Y", serialiseFalseAs: "N" }) })
        public freePlay?: boolean;

        @XML.Property({ path: ".gameCodeRGI", type: XML.String })
        public gameCodeRGI?: string;

        @XML.Property({ path: ".gameID", type: XML.Number })
        public gameID: number;

        @XML.Property({ path: ".glsID", type: XML.Number })
        public glsID?: number;

        @XML.Property({ path: ".lang", type: XML.String })
        public lang: string;

        @XML.Property({ path: ".promotions", type: XML.BooleanEx({ serialiseTrueAs: "Y", serialiseFalseAs: "N" }) })
        public promotions?: boolean;

        @XML.Property({ path: ".sessionID", type: XML.String })
        public sessionID: string;

        @XML.Property({ path: ".userID", type: XML.String, ignoreIfNull: true })
        public userID?: string;

        @XML.Property({ path: ".userType", type: XML.String })
        public userType?: string;

        @XML.Property({ path: ".versionID", type: XML.String })
        public versionID: string;

        @XML.Property({ path: ".fullVersionID", type: XML.String })
        public fullVersionID: string;

        @XML.Property({ path: ".isRecovering", type: XML.BooleanEx({ serialiseTrueAs: "Y", serialiseFalseAs: "N" }), ignoreIfNull: true })
        public isRecovering?: boolean;
    }

    @XML.Type("REPLAY")
    export class ReplayData
    {
        @XML.Property({ path: "Playback.state", type: XML.String })
        public state: string;
    }

    @XML.Type("CurrencyInformation")
    export class CurrencyInformation
    {
        @XML.Property({ path: "Currency", type: XML.String })
        public currency: string;

        @XML.Property({ path: "ExchangeRate", type: XML.Number })
        public exchangeRate: number;

        @XML.Property({ path: "CurrencySymbol", type: XML.String })
        public currencySymbol?: string;

        @XML.Property({ path: "Grouping", type: XML.Number })
        public grouping: number;

        @XML.Property({ path: "GroupingSeparator", type: XML.String })
        public groupingSeparator: string;

        @XML.Property({ path: "FracDigits", type: XML.Number })
        public fracDigits: number;

        @XML.Property({ path: "DecimalSeparator", type: XML.String })
        public decimalSeparator: string;

        @XML.Property({ path: "CurrencyCodeSpacing", type: XML.Number })
        public currencyCodeSpacing: number;

        @XML.Property({ path: "CodeBeforeAmount", type: XML.Boolean })
        public codeBeforeAmount: boolean;

        @XML.Property({ path: "CurrencySymbolSpacing", type: XML.Number })
        public currencySymbolSpacing: number;

        @XML.Property({ path: "SymbolBeforeAmount", type: XML.Boolean })
        public symbolBeforeAmount: boolean;

        @XML.Property({ path: "CurrencyMultiplier", type: XML.Number })
        public currencyMultiplier: number;
    }

    @XML.Type("AccountData")
    export class AccountData
    {
        @XML.Property({ path: "CurrencyInformation", type: XML.Object, ignoreIfNull: true })
        public currencyInformation?: CurrencyInformation;

        @XML.Property({ path: "CurrencyMultiplier", type: XML.Number, ignoreIfNull: true })
        public currencyMultiplier?: number;

        @XML.Property({ path: "Payload", type: XML.Object, ignoreIfNull: true })
        public payload?: Payload;

        // @XML.Property({ path: "BetConfiguration", type: XML.Object, ignoreIfNull: true })
        // public betConfig?: object;
    }

    @XML.Type("Balance")
    export class Balance
    {
        @XML.Property({ path: ".name", type: XML.String })
        public name: string;

        @XML.Property({ path: ".value", type: XML.Number })
        public value: number;
    }

    @XML.Type("Error")
    export class Error
    {
        @XML.Property({ path: ".msg", type: XML.String })
        public message: string;

        @XML.Property({ path: ".instruction", type: XML.Number })
        public instruction: number;
    }

    @XML.Type("map-item")
    export class MapItem
    {
        @XML.Property({ path: "key", type: XML.String })
        public key: string;

        @XML.Property({ path: "value", type: XML.String })
        public value: string;
    }

    @XML.Type("PayloadItem")
    export class PayloadItem
    {
        @XML.Property({ path: ".request", type: XML.String })
        public request: string;

        @XML.Property({ path: "Payload", type: XML.Array(XML.ExplicitObject(MapItem)) })
        public mapItems: MapItem[];
    }

    @XML.Type("Payload")
    export class Payload
    {
        @XML.Property({ path: "", type: XML.Array(XML.ExplicitObject(PayloadItem)) })
        public items: PayloadItem[];
    }
}
