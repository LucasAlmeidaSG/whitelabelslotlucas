namespace SG.GLM.Core
{
    /*
    <GameRequest type="Init">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="SESSION_ID" userID="1000018628" userType="C" versionID="1_0" />
    </GameRequest>
    */

    /**
     * Base request functionality.
     */
    export abstract class BaseRequest
    {
        protected _networkResponse: string; // Contains the last network response that was received
        public get networkResponse() { return this._networkResponse; }

        public constructor(public readonly networkSettings: Engine.NetworkSettings, public readonly userSettings: Engine.UserSettings) { }

        protected createHeader()
        {
            const header = new Header();
            header.affiliate = this.networkSettings.affiliate.toString();
            header.ccyCode = this.userSettings.currencyCode;
            header.channel = this.networkSettings.channel;
            header.freePlay = this.userSettings.freePlay;
            header.gameCodeRGI = this.networkSettings.gameCode;
            header.gameID = this.networkSettings.gameID;
            header.glsID = this.networkSettings.glsID;
            header.lang = this.userSettings.language;
            header.promotions = false;
            header.sessionID = this.userSettings.sessionID;
            header.userID = this.userSettings.userID;
            header.userType = this.networkSettings.userType;
            header.versionID = this.networkSettings.versionID;
            return header;
        }

        /**
         * Sends the actual network request.
         * @param req
         */
        protected sendNetworkRequest(payload: object, response: object): PromiseLike<void>
        {
            // Serialise payload to XML
            const requestDoc = RS.Serialisation.XML.serialise(payload);
            return new Promise((resolve, reject) =>
            {
                const req = new XMLHttpRequest();
                req.addEventListener("load", (ev) =>
                {
                    const dump = RS.IDump.get().getSubDump("GLMRequests");
                    dump.add({ time: RS.Dump.getTimestamp(), stamp: Date.now(), text: req.responseText });

                    this.onComplete(req);
                    if (req.status !== 200)
                    {
                        reject(req.statusText);
                        return;
                    }
                    let responseDoc: Document | null = null;
                    if (req.responseXML != null)
                    {
                        responseDoc = req.responseXML;
                    }
                    else if (req.responseText != null)
                    {
                        try
                        {
                            const parser = new DOMParser();
                            responseDoc = parser.parseFromString(req.responseText, "text/xml");
                        }
                        catch (err)
                        {
                            RS.Log.error(err);
                            reject(err);
                            return;
                        }
                    }

                    if (responseDoc == null)
                    {
                        reject(ev);
                    }
                    else
                    {
                        this._networkResponse = req.responseText;
                        try
                        {
                            RS.Serialisation.XML.deserialise(response, responseDoc);
                        }
                        catch (err)
                        {
                            RS.Log.error(err);
                            reject(err);
                            return;
                        }
                        resolve();
                    }
                });
                req.addEventListener("error", (ev) =>
                {
                    this.onComplete(req);
                    reject(ev);
                });
                req.addEventListener("abort", (ev) =>
                {
                    this.onComplete(req);
                    reject("aborted");
                });
                req.open("POST", this.networkSettings.endpoint, true);
                const headers = this.getRequestHeaders();
                for (const header in headers)
                {
                    req.setRequestHeader(header, headers[header]);
                }
                req.setRequestHeader("Content-type", this.networkSettings.contentType);
                req.send(requestDoc);
            });
        }

        protected getRequestHeaders(): RS.Map<string>
        {
            return { "Content-Type": this.networkSettings.contentType };
        }

        /** Called when response is received, override to call platform networkResponseReceived(req) */
        protected abstract onComplete(req: XMLHttpRequest): void;
    }
}
