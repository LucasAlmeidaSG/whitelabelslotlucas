/// <reference path="../responses/CloseResponse.ts" />

namespace SG.GLM.Core
{
    import XML = RS.Serialisation.XML;

    /*
    <GameRequest type="EndGame">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="Tfxz7fWLLbVKUS9mer9rC/yfzVG2EU8b6EIUzJer56jCVialR8bvCq5E70Zbt3sj" userID="1000018628" userType="C" versionID="1_0" />
    </GameRequest>
    */

    export abstract class CloseRequest extends BaseRequest implements RS.Engine.ICloseRequest<RS.Engine.ICloseRequest.Payload, CloseResponse>
    {
        public async send(payload: RS.Engine.ICloseRequest.Payload)
        {
            const request = this.createRequest(payload);
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createRequest(payload: RS.Engine.ICloseRequest.Payload): CloseRequest.Payload
        {
            const request = new CloseRequest.Payload();
            request.header = this.createHeader();

            return request;
        }

        protected createResponse(): CloseResponse
        {
            return new CloseResponse(this);
        }
    }

    export namespace CloseRequest
    {
        /* tslint:disable:member-ordering */

        @XML.Type("GameRequest")
        export class Payload
        {
            @XML.Property({ path: ".type", type: XML.String })
            protected type: string = "EndGame";

            @XML.Property({ path: "Header", type: XML.Object })
            public header: Header;
        }

        /* tslint:enable:member-ordering */
    }
}
