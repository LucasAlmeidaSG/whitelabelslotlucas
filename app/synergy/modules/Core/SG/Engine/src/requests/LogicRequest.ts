namespace SG.GLM.Core
{
    import XML = RS.Serialisation.XML;

    /*
    <GameRequest type="Logic">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="Tfxz7fWLLbVKUS9mer9rC/yfzVG2EU8b6EIUzJer56jCVialR8bvCq5E70Zbt3sj" userID="1000018628" userType="C" versionID="1_0" />
        <Stake total="50000"/>

        <FORCE><ReelSpin stopIndices="0|0|0|0|0" bandsetIndex="1"/></FORCE>
    </GameRequest>
    */

    /*
    <GameRequest type="Logic">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="Tfxz7fWLLbVKUS9mer9rC/yfzVG2EU8b6EIUzJer56jCVialR8bvCq5E70Zbt3sj" userID="1000018628" userType="C" versionID="1_0" />
        <AccountData>
            <CurrencyMultiplier>1</CurrencyMultiplier>
        </AccountData>
        <Stake total="50000"/>

        <FORCE>
            <ReelSpin stopIndices="0|0|0|0|0" bandsetIndex="1"/>
            <ReelSpin stopIndices="1|2|3|4|5" bandsetIndex="1"/>
            <ReelSpin stopIndices="6|7|8|9|10" bandsetIndex="1"/>
        </FORCE>

    </GameRequest>
    */

    export interface LogicRequestPayload extends RS.Engine.ILogicRequest.Payload
    {
        forceResult? : RS.Engine.ForceResult;
        replay?: SG.Replay.IReplay;
        currencyMultiplier?: number;
    }

    export abstract class LogicRequest extends BaseRequest implements RS.Engine.ILogicRequest<LogicRequestPayload, LogicResponse>
    {
        public async send(payload: LogicRequestPayload)
        {
            const request = this.createRequest(payload);
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createRequest(payload: LogicRequestPayload): LogicRequest.Payload
        {
            const request = this.createPayload();
            request.header = this.createHeader();
            request.totalStake = payload.betAmount.value;
            if (payload.forceResult)
            {
                request.forceData = this.createForceData();
                this.fillForceData(request.forceData, payload.forceResult);
            }

            if (payload.replay)
            {
                request.replayData = this.createReplayData();
                this.fillReplayData(request.replayData, payload.replay);
            }

            if (payload.currencyMultiplier != null)
            {
                request.accountData = new AccountData();
                request.accountData.currencyMultiplier = payload.currencyMultiplier;
            }

            return request;
        }

        protected createForceData(): LogicRequest.ForceData
        {
            return new LogicRequest.ForceData();
        }

        protected createReplayData()
        {
            return new ReplayData();
        }

        protected fillReplayData(replayData: ReplayData, replay: SG.Replay.IReplay): void
        {
            replayData.state = replay.currentState;
            replay.advance();
        }

        protected fillForceData(forceData: LogicRequest.ForceData, forceResult: RS.Engine.ForceResult): void
        {
            //override to fill forces
        }

        protected createResponse(): LogicResponse
        {
            return new LogicResponse(this);
        }

        protected createPayload(): LogicRequest.Payload
        {
            return new LogicRequest.Payload();
        }
    }

    export namespace LogicRequest
    {
        /* tslint:disable:member-ordering */

        @XML.Type("GameRequest")
        export class Payload
        {
            @XML.Property({ path: ".type", type: XML.String })
            protected type: string = "Logic";

            @XML.Property({ path: "AccountData", type: XML.Object })
            public accountData?: AccountData;

            @XML.Property({ path: "Header", type: XML.Object })
            public header: Header;

            @XML.Property({ path: "Stake.total", type: XML.Number })
            public totalStake: number;

            @XML.Property({ path: "REPLAY", type: XML.Object })
            public replayData?: ReplayData;

            @XML.Property({ path: "FORCE", type: XML.Object })
            public forceData?: ForceData;
        }

        @XML.Type("FORCE")
        export class ForceData
        {
            //override for forces
        }

        /* tslint:enable:member-ordering */
    }
}
