/// <reference path="../responses/InitResponse.ts" />

namespace SG.GLM.Core
{
    import XML = RS.Serialisation.XML;

    export interface InitRequestPayload extends RS.Engine.IInitRequest.Payload
    {
        replay?: SG.Replay.IReplay;
    }

    /*
    <GameRequest type="Init">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="SESSION_ID" userID="1000018628" userType="C" versionID="1_0" />
    </GameRequest>
    */

    export abstract class InitRequest extends BaseRequest implements RS.Engine.IInitRequest<RS.Engine.IInitRequest.Payload, InitResponse>
    {
        public async send(payload: InitRequestPayload)
        {
            const request = this.createRequest(payload);
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createRequest(payload: InitRequestPayload): InitRequest.Payload
        {
            const request = new InitRequest.Payload();
            request.header = this.createHeader();

            if (payload.replay)
            {
                request.replayData = this.createReplayData();
                this.fillReplayData(request.replayData, payload.replay);
            }

            return request;
        }

        protected createResponse(): InitResponse
        {
            return new InitResponse(this);
        }

        protected createReplayData()
        {
            return new ReplayData();
        }

        protected fillReplayData(replayData: ReplayData, replay: SG.Replay.IReplay): void
        {
            replayData.state = replay.currentState;
        }
    }

    export namespace InitRequest
    {
        /* tslint:disable:member-ordering */

        @XML.Type("GameRequest")
        export class Payload
        {
            @XML.Property({ path: ".type", type: XML.String })
            protected type: string = "Init";

            @XML.Property({ path: "Header", type: XML.Object })
            public header: Header;

            @XML.Property({ path: "REPLAY", type: XML.Object })
            public replayData?: ReplayData;
        }

        /* tslint:enable:member-ordering */
    }
}
