namespace SG.GLM.Core
{
    const balanceNameMap: RS.Map<string> = {};
    balanceNameMap["CASH_BALANCE"] = RS.Models.Balances.Cash;
    balanceNameMap["BONUS_BALANCE"] = RS.Models.Balances.Promotional;

    const primaryBalanceName = "CASH_BALANCE";
    export const GREGEnabled = RS.URL.getParameterAsBool("greg");

    export const errorResponse =
    {
        isError: true,
        request: null,
        header: null,
        balances: null,
        gameResult: null
    };

    /**
     * Encapsulates a slots engine interface for a GLM.
     */
    export abstract class Engine<TModels extends RS.Models = RS.Models, TForceResult extends RS.Engine.ForceResult = RS.Engine.ForceResult> implements RS.Engine.IEngine<TModels, Engine.Settings, Engine.ReplayData, TForceResult>
    {
        /** Published when an init response has been received and fully processed. */
        public readonly onInitResponseReceived = RS.createEvent<RS.Engine.IResponse>();

        /** Published when a bet response has been received and fully processed. */
        public readonly onBetResponseReceived = RS.createEvent<RS.Engine.IResponse>();

        /** Published when a close response has been received and fully processed. */
        public readonly onCloseResponseReceived = RS.createEvent<RS.Engine.IResponse>();

        /** Gets if this engine supports replay. */
        public get supportsReplay() { return !this._isFreePlay; }

        /** Gets the version for this engine if known. */
        public get version() { return this._version; }

        protected readonly _components: RS.Engine.IComponent[];

        /** Array of wager replay information. */
        protected _replayData: RS.Map<Engine.ReplayData> = {};

        protected _currencyMultiplier = 1;

        protected _replayEngine: SG.Replay.IEngine = SG.Replay.IEngine.get();
        public get replayEngine(): SG.Replay.IEngine { return this._replayEngine; }

        protected _gameState: RS.Models.State.Type;
        protected _force: TForceResult[] = [];
        protected _isFreePlay: boolean = false;
        protected _isRecovering: boolean = false;
        protected _version: RS.Version | null = null;

        protected _userSettings: Engine.UserSettings | null = null;
        protected _networkSettings: Engine.NetworkSettings | null = null;

        protected _initResponse: string; // Contains the unparsed init response

        protected _GREGData: RS.Engine.GREGData[] = []; // Contains the data necessary to generate GREG replay files
        protected _currentGREGData: RS.Engine.GREGData; // GREG data for the current wager
        protected _GREGDataCounter: number = 0; // Counter incremented to assign unique ID's to each GREG replay
        protected readonly _maxGREGCount: number = 10; // Maximum  number of GREG replays stored at any time

        public constructor(public readonly settings: Engine.Settings, public readonly models: TModels)
        {
            const sessionInfo = this.getSessionInfo();
            this._userSettings =
            {
                userID: sessionInfo.userName || `${sessionInfo.userID}`,
                sessionID: sessionInfo.sessionID || "SESSION_ID",
                language: RS.IPlatform.get().localeCode,
                freePlay: RS.IPlatform.get().isFreePlay,
                currencyMultiplier: 0,
                currencyCode: ""
            };

            const sessionOverride = RS.URL.getParameter("sessionoverride", "");
            if (sessionOverride) { this._userSettings.sessionID = sessionOverride; }

            const userOverride = RS.URL.getParameter("useroverride", "");
            if (userOverride) { this._userSettings.userID = userOverride; }

            this._networkSettings = { ...settings.networkSettings };
            this._networkSettings.endpoint = RS.IPlatform.get().engineEndpointOverride || RS.URL.getParameter("glsoverride") || this._networkSettings.endpoint;
            this._networkSettings.affiliate = sessionInfo.affiliateID || this._networkSettings.affiliate;

            if (this._userSettings.freePlay)
            {
                this._isFreePlay = true;
                this._networkSettings.glsID = 65535;
            }

            this._components = [];
            this.createComponents(this._components);
        }

        /**
         * Rigs the next spin request.
         */
        public force(forceResults: RS.OneOrMany<TForceResult>)
        {
            //override to add force to next spin
            if (!RS.Is.array(forceResults))
            {
                forceResults = [forceResults];
            }

            this._force.length = 0;
            for (const forceResult of forceResults)
            {
                const force = RS.Util.clone(forceResult, true);
                this._force.push(force);
            }
        }

        /**
         * Starts a replay with the given data.
         */
        public replay(data: Engine.ReplayData): void
        {
            const url: string = RS.URL.setParameters(
            {
                "HistoryServerURL": data.historyServerURL,
                "betID": data.betID
            });

            window.open(url);
        }

        /**
         * Initialises the engine and sends an init request.
         * Async.
         */
        public async init(): Promise<RS.Engine.IResponse>
        {
            try
            {
                await this.initReplay();
            }
            catch (e)
            {
                this.handleReplayError(e);
                return { isError: true, request: null };
            }

            const req = new this.settings.initRequest(this._networkSettings, this._userSettings);
            const initPayload = { replay: this._replayEngine.replay };
            for (const component of this._components)
            {
                component.populateInitPayload(initPayload);
            }

            let response: InitResponse;
            try
            {
                response = await req.send(initPayload);
                this._initResponse = req.networkResponse;
            }
            catch (err)
            {
                this.handleNetworkError(err);
                return { isError: true, request: req };
            }

            // Parse error
            if (response.error)
            {
                this.parseError(response.error);
                return response;
            }

            // there is no error
            this.models.error.type = RS.Models.Error.Type.None;

            // init the models
            this.handleInitSuccess(response);

            try
            {
                // Allow components to parse init response
                for (const component of this._components)
                {
                    component.populateInitModels(response, this.models);
                }
            }
            catch (err)
            {
                RS.Log.warn(err)
                return errorResponse;
            }

            this.onInitResponseReceived.publish(response);

            // Check for recovery
            if (response.header.isRecovering)
            {
                // Send a spin request
                const recoverResponse = await this.recover();
                if (recoverResponse.isError)
                {
                    return { isError: true, request: recoverResponse.request };
                }
            }

            return response;
        }

        public async bet(payload: RS.Engine.ILogicRequest.Payload): Promise<RS.Engine.IResponse>
        {
            this.clearResultsModel(this.models);

            const response = await this.logic(payload);
            if (!response.isError)
            {
                this.parseGame(response.gameResult);
                this.onBetResponseReceived.publish(response);
            }

            return response;
        }

        /**
         * Sends a close request.
         * Async.
         */
        public async close(): Promise<RS.Engine.IResponse>
        {
            if (!this.shouldMakeCloseRequest())
            {
                // Emulate close request for consistency with other engines.
                this.models.state.state = this._gameState = RS.Models.State.Type.Closed;
                return { isError: false, request: null };
            }

            // Check internet connection
            if (!RS.Connection.connected.value)
            {
                this.models.error.type = RS.Models.Error.Type.Client;
                this.models.error.title = Translations.ConnectionError.Title;
                this.models.error.message = Translations.ConnectionError.Message;
                this.models.error.options =
                [
                    { type: RS.Models.Error.OptionType.Refresh, text: Translations.ConnectionError.OK }
                ];
                return {
                    isError: true,
                    request: null
                };
            }

            const req = new this.settings.closeRequest(this._networkSettings, this._userSettings);
            const closePayload = {};
            for (const component of this._components)
            {
                component.populateClosePayload(closePayload);
            }

            let response: CloseResponse;
            try
            {
                response = await req.send(closePayload);
            }
            catch (err)
            {
                this.handleNetworkError(err);
                return { isError: true, request: req };
            }

            // Parse error
            if (response.error)
            {
                this.parseError(response.error);
                return response;
            }
            else
            {
                this.models.error.type = RS.Models.Error.Type.None;
            }

            // New session ID
            this._userSettings.sessionID = response.header.sessionID;

            // We're now closed
            this.models.state.state = this._gameState = RS.Models.State.Type.Closed;

            try
            {
                // Allow components to parse close response
                for (const component of this._components)
                {
                    component.populateCloseModels(response, this.models);
                }
            }
            catch (err)
            {
                RS.Log.warn(err)
                return errorResponse;
            }

            this.onCloseResponseReceived.publish(response);
            return response;
        }

        public async getReplayData()
        {
            return this._replayData;
        }

        public async getGREGData()
        {
            const output = {};
            for (const data of this._GREGData)
            {
                output[`GREG ${data.id}`] = data;
            }
            return output;
        }

        // override to get the relevant platform.sessionInfo
        protected abstract getSessionInfo(): RS.IPlatform.SessionInfo;

        /** override to create relevant components and add to array */
        protected createComponents(arr: RS.Engine.IComponent[])
        {
            return;
        }

        protected preLogicChecks()
        {
            // Check for a finished replay
            if (this._replayEngine.isReplay && this._replayEngine.replay.isComplete)
            {
                this.models.error.type = RS.Models.Error.Type.Client;
                this.models.error.fatal = true;
                this.models.error.title = "REPLAY ERROR";
                this.models.error.message = "Tried to send a spin request for a completed replay"
                this.models.error.options =
                [
                    { type: RS.Models.Error.OptionType.Refresh, text: Translations.ConnectionError.OK }
                ];
                return true;
            }

            // Check internet connection
            if (!RS.Connection.connected.value)
            {
                this.models.error.type = RS.Models.Error.Type.Client;
                this.models.error.title = Translations.ConnectionError.Title;
                this.models.error.message = Translations.ConnectionError.Message;
                this.models.error.options =
                [
                    { type: RS.Models.Error.OptionType.Refresh, text: Translations.ConnectionError.OK }
                ];

                return true;
            }

            return false;
        }

        /**
         * Sends an spin request.
         * Async.
         */
        protected async logic(payload: RS.Engine.ILogicRequest.Payload): Promise<LogicResponse>
        {
            if (this.preLogicChecks())
            {
                return errorResponse;
            }

            let networkResponse: string = null;
            let response: LogicResponse;

            const req = new this.settings.spinRequest(this._networkSettings, this._userSettings);
            const logicPayload = this.fillRequestPayload(payload);
            for (const component of this._components)
            {
                component.populateLogicPayload(logicPayload);
            }

            try
            {
                response = await req.send(logicPayload);
                networkResponse = req.networkResponse;
            }
            catch (err)
            {
                this.handleNetworkError(err);
                return errorResponse;
            }

            // Parse error
            if (response.error)
            {
                this.parseError(response.error);
                return response;
            }
            else
            {
                this.models.error.type = RS.Models.Error.Type.None;
            }

            if (GREGEnabled && networkResponse != null)
            {
                this.createGREGData(networkResponse, response, RS.Models.Stake.getCurrentBet(this.models.stake).value);
            }

            this.parseLogicResponse(response);

            try
            {
                // Allow components to parse logic response
                for (const component of this._components)
                {
                    component.populateLogicModels(response, this.models);
                }
            }
            catch (err)
            {
                RS.Log.warn(err);
                return errorResponse;
            }

            return response;
        }

        protected createGREGData(networkResponse: string, response: LogicResponse, totalBet: number)
        {
            // New wager, create new GREG data
            if (this._currentGREGData == null)
            {
                const ID = response.gameResult.betID ? response.gameResult.betID : this._GREGDataCounter;
                this._currentGREGData =
                {
                    id: ID,
                    networkResponses: [],
                    initResponse: this._initResponse,
                    stake: totalBet,
                    win: 0
                }
                this._GREGDataCounter++;
            }

            // Update win amount (some wagers last multiple spins, so total win amount might change)
            this._currentGREGData.win = this.getResultsModel(this.models).map((sr) => sr.win.accumulatedWin).reduce((a, b) => a + b);

            // Add current spin response to the list of responses for the current wager
            this._currentGREGData.networkResponses.push(networkResponse);

            // End of wager, add current GREG data to the list
            if (this.shouldMakeCloseRequest())
            {
                this._GREGData.push(this._currentGREGData);
                if (this._GREGData.length > this._maxGREGCount)
                {
                    this._GREGData.shift(); // Remove old GREG data objects when there are too many
                }
                this._currentGREGData = null;
            }
        }

        protected fillRequestPayload(payload: RS.Engine.ILogicRequest.Payload): LogicRequestPayload
        {
            const totalBet = payload.betAmount; //RS.Models.Stake.getCurrentBet(this.models.stake);
            totalBet.value /= this._currencyMultiplier;
            const p: LogicRequestPayload = {
                currencyMultiplier: this._userSettings.currencyMultiplier,
                betAmount: totalBet,
                forceResult: this._force.shift(),
                replay: this._replayEngine.replay
            }

            return p;
        }

        /**
         * Handles a successful init request
         */
        protected handleInitSuccess(response: InitResponse): InitResponse
        {
            // const transformCurrency = (v: number) => v * this._currencyMultiplier;
            if (response.header.freePlay != null) { this._isFreePlay = response.header.freePlay; }
            if (response.header.fullVersionID != null) { this._version = RS.Version.fromString(response.header.fullVersionID); }

            this.models.config.isFreePlay = this._isFreePlay;

            this.initStakes(response);
            this.initReplayBetIndex();

            this.getResultsModel(this.models).length = 0;

            // Initial state
            this.models.state.state = this._gameState = RS.Models.State.Type.Closed;

            this.initCustomer(response);

            if (response.balances)
            {
                this.parseBalances(response.balances);
            }

            if (response.accountData && response.accountData.payload)
            {
                this.parsePayload(response.accountData.payload);
            }

            // Parse game variant
            if (response.gameVariantInfo)
            {
                this.parseGameVariant(response.gameVariantInfo);
            }
            else if (response.gameInfo)
            {
                this.parseGameInfo(response.gameInfo);
            }

            // New session ID
            this._userSettings.sessionID = response.header.sessionID;

            return response;
        }

        /**
         * Init state isReplay
         * Async.
         */
        protected async initReplay()
        {
            await this._replayEngine.init();

            if (this._replayEngine.isReplay)
            {
                if (this._replayEngine.replay.gameID != this._networkSettings.gameID)
                {
                    RS.Log.warn(`[SG.GLM.Engine] Replay gameID mismatch - replay: ${this._replayEngine.replay.gameID}; actual: ${this._networkSettings.gameID}`);
                }

                this.models.state.isReplay = true;
            }
        }

        /**
         * Init stake betOptions from init request response.
         */
        protected initStakes(response: InitResponse)
        {
            // Parse stakes
            this.models.stake.betOptions = response.stakes.map((value) =>
            {
                return { value };
            });

            this.models.stake.defaultBetIndex = response.defaultStakeIndex;
            this.models.stake.currentBetIndex = response.defaultStakeIndex;
        }

        /**
         * Init stake currentBetIndex from init request response.
         */
        protected initReplayBetIndex()
        {
            if (!this._replayEngine.isReplay || this._replayEngine.error)
            {
                return;
            }

            const stake: number = this._replayEngine.replay.stake;
            for (let i: number = 0; i < this.models.stake.betOptions.length; i++)
            {
                const betOption = this.models.stake.betOptions[i];
                if (betOption.value == stake)
                {
                    this.models.stake.currentBetIndex = i;
                    break;
                }
            }
        }

        /**
         * Init custom from init account data request response.
         */
        protected initCustomer(response: InitResponse)
        {
            // Parse customer
            this.models.customer.currencyCode = response.header.ccyCode || "GBP";
            this.models.customer.currencyMultiplier = 1.0;
            const accountData = response.accountData;
            if (accountData && accountData.currencyInformation)
            {
                const ci = accountData.currencyInformation;

                // Replace all space characters with non-breaking spaces.
                const spaceCharacter = RS.Localisation.SpecialChars.NonBreakingSpace;
                for (const key in ci)
                {
                    const value = ci[key];
                    if (RS.Is.string(value))
                    {
                        ci[key] = ci[key].replace(/\s/g, spaceCharacter);
                    }
                }

                this.models.customer.currencySettings =
                {
                    name: ci.currency,
                    symbol: ci.currencySymbol || "",
                    exchangeRate: ci.exchangeRate,
                    grouping: ci.grouping,
                    groupingSeparator: ci.groupingSeparator,
                    fractionalDigits: ci.fracDigits,
                    fractionalSeparator: ci.decimalSeparator,
                    currencyCodeSpacing: ci.currencyCodeSpacing,
                    currencyCodeBeforeAmount: ci.codeBeforeAmount,
                    currencySymbolSpacing: ci.currencySymbolSpacing,
                    currencySymbolBeforeAmount: ci.symbolBeforeAmount,
                    currencyMultiplier: ci.currencyMultiplier,
                    displaySymbol: ci.currencySymbol != null,
                    displayCode: ci.currencySymbol == null
                };

                this._userSettings.currencyMultiplier = ci.currencyMultiplier;
                this._userSettings.currencyCode = response.header.ccyCode;
            }
            else
            {
                this.models.customer.currencySettings =
                {
                    ...(RS.Localisation.currencyMap[this.models.customer.currencyCode] || RS.Localisation.currencyMap["GBP"]),
                    displaySymbol: false
                };

                this._userSettings.currencyMultiplier = 1.0;
                this._userSettings.currencyCode = "";
            }

            if (this._replayEngine.isReplay)
            {
                this.models.customer.currencySettings.displaySymbol = false;
                this.models.customer.currencySettings.currencyMultiplier = 1.0;
            }
        }

        /**
         * Sends a recovery spin request.
         * Async.
         */
        protected async recover()
        {
            this._isRecovering = true;
            const response = await this.bet({
                betAmount: this.models.stake.betOptions[this.models.stake.currentBetIndex]
            });
            this._isRecovering = false;
            return response;
        }

        protected parsePayload(payload: Payload): void
        {
            for (const item of payload.items)
            {
                if (item.request === "init" || item.request === "outcome" || item.request === "wager")
                {
                    if (item.mapItems)
                    {
                        for (const mapItem of item.mapItems)
                        {
                            if (mapItem.key === "OPENBET_CONTAINER_PAYLOAD")
                            {
                                const matches = RS.Util.matchAll(/([^=]+)=([^=;]+);/g, mapItem.value);
                                this.models.customer.finalBalance.primary = 0;
                                for (const match of matches)
                                {
                                    const key = match[1].toLowerCase(), value = parseInt(match[2]);
                                    switch (key)
                                    {
                                        case "freebet":
                                            this.models.customer.finalBalance.primary += value;
                                            this.models.customer.finalBalance.named[RS.Models.Balances.Promotional] = value;
                                            break;

                                        case "cash":
                                            this.models.customer.finalBalance.primary += value;
                                            this.models.customer.finalBalance.named[RS.Models.Balances.Cash] = value;
                                            break;

                                        default:
                                            this.models.customer.finalBalance.named[key] = value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /**
         * Returns whether or not an actual close request should be sent.
         */
        protected shouldMakeCloseRequest(): boolean
        {
            if (this.replayEngine.isReplay)
            {
                return false;
            }

            return true;
        }

        protected isBalanceDataEmpty(data: Balance[]): boolean
        {
            return data.length == 1 && !data[0].name;
        }

        protected parseBalances(data: Balance[])
        {
            if (this.isBalanceDataEmpty(data)) { return; }
            if (!this._isRecovering) { RS.Models.Balances.clear(this.models.customer.finalBalance); }
            for (const balance of data)
            {
                if (balance.name === primaryBalanceName)
                {
                    this.models.customer.finalBalance.primary = balance.value * this._currencyMultiplier;
                }
                if (balance.name in balanceNameMap)
                {
                    this.models.customer.finalBalance.named[balanceNameMap[balance.name]] = balance.value * this._currencyMultiplier;
                }
            }
        }

        protected checkMaxWin(data: GameResult): boolean
        {
            return false;
            // return RS.IPlatform.get().maxWinMode !== RS.PlatformMaxWinMode.None && data.bgInfo.isMaxWin;
        }

        /**
         * Parse game result xml into models
         * Call super.parseGame() AFTER parsing for balance and win fudging
         */
        protected parseGame(data: GameResult): void
        {
            // Balance and win fudging.
            this.models.state.isMaxWin = this.checkMaxWin(data);
            this.calculateBalances(data);
            this.freezeBalances(data);
            this.allocateWins(data);
            this.parseSeed(data);
        }

        /** distributes accumulated wins to game results */
        protected abstract allocateWins(data: GameResult): void

        /**
         * Gets whether or not the balance from the engine includes the wager win and
         * should therefore have the win subtracted from it to freeze the balance.
         */
        protected isEngineBalanceFrozen(gameResult: GameResult): boolean
        {
            // Balance is frozen when not in real-money mode.
            if (this._isFreePlay) { return false; }

            // Balance frozen when max win hit.
            if (this.models.state.isMaxWin) { return false; }

            // Base game spin or last free spin; GLM balance has been updated
            return false;
        }

        /**
         * Updates the before and after balances on all spin results.
         */
        protected calculateBalances(gameResult: GameResult)
        {
            const endBalance = this.models.customer.finalBalance;
            const shouldSubtractWin: boolean = !this.isEngineBalanceFrozen(gameResult);

            const resultsModel = this.getResultsModel(this.models);

            let lastBalance = RS.Models.Balances.clone(endBalance);
            // Start at the final balance, and work backwards through each spin result
            for (let i = resultsModel.length - 1; i >= 0; --i)
            {
                const result = resultsModel[i];
                result.balanceBefore = lastBalance;
                result.balanceAfter = lastBalance;

                if (shouldSubtractWin)
                {
                    let totalWin;
                    if (i == 0)
                    {
                        totalWin = gameResult.totalWin;
                        result.balanceBefore = RS.Models.Balances.clone(endBalance);
                    }
                    else
                    {
                        totalWin = result.win.totalWin;
                        result.balanceBefore = RS.Models.Balances.clone(lastBalance);
                    }

                    this.fudgeWinFromBalances(result.balanceBefore, totalWin);
                    lastBalance = result.balanceBefore;
                }
            }
        }

        /**
         * Subtracts the given win value from the given balances.
         */
        protected fudgeWinFromBalances(balances: RS.Models.Balances, win: number): void
        {
            balances.named[RS.Models.Balances.Cash] = Math.max(balances.named[RS.Models.Balances.Cash] - win, 0);
            balances.primary = Math.max(balances.primary - win, 0);
        }

        /**
         * Returns whether or not the given spin result should result in a balance update.
         */
        protected shouldUpdateBalance(result: RS.Models.GameResult, gameResult: GameResult): boolean
        {
            // Max win spin should always be the last spin.
            if (this.models.state.isMaxWin) { return true; }

            // Balance frozen until last spin.
            if (result.index < this.getResultsModel(this.models).length - 1) { return false; }

            // Base game spin or last free spin; show final balance after
            return true;
        }

        /**
         * Freezes the balances so as to update only at the correct time.
         */
        protected freezeBalances(gameResult: GameResult): void
        {
            const resultsModel = this.getResultsModel(this.models);
            const startBalance = resultsModel[0].balanceBefore;
            for (let i = 0; i < resultsModel.length; i++)
            {
                const spinResult = resultsModel[i];

                if (i > 0)
                {
                    const prevSpinResult = resultsModel[i - 1];
                    spinResult.balanceBefore = prevSpinResult.balanceAfter;
                }

                if (!this.shouldUpdateBalance(spinResult, gameResult))
                {
                    spinResult.balanceAfter = spinResult.balanceBefore;
                }
            }

            this.models.customer.finalBalance = resultsModel[resultsModel.length - 1].balanceAfter;
        }

        protected parseSeed(gameResult: GameResult): void
        {
            let betIDstring: string = "";
            this.models.seed = Date.now();

            if (this.models.state.isReplay)
            {
                betIDstring = SG.Replay.IHistoryServerConfigurator.get().getBetID();
            }
            else
            {
                betIDstring = gameResult.betID;
            }

            if (betIDstring.length > 0)
            {
                this.models.seed = Number(betIDstring.replace(/\D/g, ""));
            }
        }

        protected parseError(error: Error)
        {
            this.models.error.type = RS.Models.Error.Type.Server;
            this.models.error.title = "SERVER ERROR";
            this.models.error.message = error.message;
            const [text, action, display, sessionID] = error.message.split("|");
            switch (action || "OK")
            {
                case "OK":
                case "REALITY_CHECK":
                    this.models.error.options =
                    [
                        { type: RS.Models.Error.OptionType.Continue, text: "OK" }
                    ];
                    break;
                case "EXIT":
                    this.models.error.fatal = true;
                    this.models.error.options =
                    [
                        { type: RS.Models.Error.OptionType.Close, text: "OK" }
                    ];
                    break;
                case "REDIRECT":
                case "POLL":
                    this.models.error.options =
                    [
                        { type: RS.Models.Error.OptionType.Refresh, text: "OK" }
                    ];
                    break;
                default:
                    this.models.error.options =
                    [
                        { type: RS.Models.Error.OptionType.Refresh, text: "OK" }
                    ];
                    break;
            }

            if (sessionID != null)
            {
                this._userSettings.sessionID = sessionID;
            }
        }

        protected parseGameVariant(gameVariant: GameVariantInfo)
        {
            this.models.config.minRTP = gameVariant.lowRTP;
            this.models.config.maxRTP = gameVariant.highRTP;
        }

        protected parseGameInfo(gameInfo: GameInfo)
        {
            this.models.config.minRTP = gameInfo.RTP;
            this.models.config.maxRTP = gameInfo.RTP;
        }

        protected handleNetworkError(err?: ProgressEvent | Error | string)
        {
            if (err instanceof ProgressEvent)
            {
                err = "No response received from the server";
            }
            else if (err instanceof Error)
            {
                err = err.message;
                RS.Log.error(err);
            }
            else if (!err)
            {
                err = "Unknown server error";
            }
            this.models.error.type = RS.Models.Error.Type.Server;
            this.models.error.title = "NETWORK ERROR";
            this.models.error.message = err;
            this.models.error.options =
            [
                { type: RS.Models.Error.OptionType.Refresh, text: "OK" }
            ];
        }

        protected handleReplayError(err?: string | ProgressEvent | Error)
        {
            if (err instanceof ProgressEvent)
            {
                err = "No response received from the history server";
            }
            else if (err instanceof Error)
            {
                err = err.message;
                RS.Log.error(err);
            }
            else if (!err)
            {
                err = "Unknown replay error";
            }

            this.models.error.type = RS.Models.Error.Type.Client;
            this.models.error.fatal = true;
            this.models.error.title = "REPLAY ERROR";
            this.models.error.message = err;
            this.models.error.options =
            [
                { type: RS.Models.Error.OptionType.Refresh, text: "OK" }
            ];
        }

        protected abstract getResultsModel(models: RS.Models): RS.Models.GameResults;
        protected abstract clearResultsModel(models: RS.Models);

        private parseLogicResponse(response: LogicResponse)
        {
            // Parse customer
            this.models.customer.currencyCode = response.header.ccyCode || "GBP";
            this.parseBalances(response.balances);
            if (response.accountData && response.accountData.payload)
            {
                this.parsePayload(response.accountData.payload);
            }

            // New session ID
            this._userSettings.sessionID = response.header.sessionID;

            // We're now open
            this.models.state.state = this._gameState = RS.Models.State.Type.Open;
            if (this._replayEngine.replay) { this.models.state.isReplayComplete = this._replayEngine.replay.isComplete; }

            if (!response.gameResult.betID && response.gameResult.betID != "REPLAY")
            {
                this._replayData[response.gameResult.betID] =
                {
                    historyServerURL: this._networkSettings.historyServer,
                    betID: response.gameResult.betID
                };
            }
        }
    }

    export namespace Engine
    {
        export interface NetworkSettings
        {
            endpoint: string;
            historyServer: string;
            contentType: string;
            gameCode: string;
            gameID: number;
            glsID: number;
            channel: string;
            affiliate: number | string;
            userType: string;
            versionID: string;
        }

        export interface UserSettings
        {
            userID: string;
            sessionID?: string;
            language: string;
            freePlay: boolean;
            currencyCode: string;
            currencyMultiplier: number;
        }

        export interface Settings
        {
            networkSettings: NetworkSettings;
            initRequest: { new(networkSettings: NetworkSettings, userSettings: UserSettings): InitRequest; };
            spinRequest: { new(settings: NetworkSettings, userSettings: UserSettings): LogicRequest; };
            closeRequest?: { new(settings: NetworkSettings, userSettings: UserSettings): CloseRequest; };
        }

        /**
         * Data for triggering a replay.
         */
        export interface ReplayData
        {
            historyServerURL: string;
            betID: string;
        }
    }
}
