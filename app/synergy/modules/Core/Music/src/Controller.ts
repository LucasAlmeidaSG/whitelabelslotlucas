/// <reference path="IController.ts" />

namespace RS.Music
{
    /**
     * Current state of the music controller.
     */
    enum MusicLayer
    {
        None,
        Background,
        Foreground
    }

    const logger = Logging.getContext("Music");

    @HasCallbacks
    class Controller implements IController
    {
        private _settings: IController.Settings;

        private _isDisposed: boolean = false;

        @AutoDispose private _music: Audio.LayeredSound;
        private _musicDef: Definition;
        private _musicPlaying: boolean = false;
        private _curLayer: MusicLayer = MusicLayer.None;
        private _startMusicProps: Partial<Audio.PlaySettings> = { loop: true, paused: false, volume: 0.0 };

        @AutoDispose private _stateArbiterChangeHandler: IDisposable;
        @AutoDispose private _volumeArbiterChangeHandler: IDisposable;
        @AutoDispose private _musicArbiterChangeHandler: IDisposable;

        private _tweenNoVol = { volume: 0 };
        private _tweenFullVol = { volume: 1 };

        private _fadeTime = 500;
        private _lingerTime = 1000;

        private _beatCount = 0;

        private _winRenderMusicVol = 0.4;

        public get lingerTime() { return  this._lingerTime; }
        public set lingerTime(value) { this._lingerTime = value; }

        public get settings() { return this._settings; }

        public get isDisposed() { return this._isDisposed; }

        /**
         * Initialises the music controller.
         */
        public initialise(settings: IController.Settings)
        {
            this._settings = settings;

            // Subscribe to events
            this._stateArbiterChangeHandler = settings.stateArbiter.onChanged(this.updateMusicState);
            this._volumeArbiterChangeHandler = settings.volumeArbiter.onChanged(this.updateMusicState);
            this._musicArbiterChangeHandler = settings.musicArbiter.onChanged(this.updateMusicState);
        }

        /**
         * Crossfades to the specified spin music.
         * Will only work if there's currently spin music playing.
         * @param newMusic      New music settings to use.
         * @param duration      Duration of the entire crossfade.
         * @param background    Use the background track of the new music.
         */
        public crossfadeTo(newMusic: Definition, duration: number, background?: boolean): void
        {
            if (!this._music || this._music.isDisposed || this._music.paused)
            {
                logger.warn("Tried to crossfade when no music was playing!");
                this._musicDef = newMusic;
                this.turnOn(this._music, 0);
                return;
            }

            const oldSpinMusic = this._music;
            this._music = null;
            RS.Tween.get(oldSpinMusic)
                .to({masterVolume: 0}, duration*0.5)
                .call((t) =>
                {
                    oldSpinMusic.dispose();
                });

            this._musicDef = newMusic;
            if (newMusic == null)
            {
                this.updateMusicState();
                this._musicPlaying = false;
                return;
            }

            this._music = this.createMusic(newMusic);
            this._beatCount = 0;

            RS.Tween.get(this._music)
                .wait(duration * 0.5)
                //.call(t => this._spinMusic.fadeLayerVolume(background ? 1 : 0, 1, duration * 0.5));
                .call((t) => this.updateMusicState());
        }

        /**
         * Disposes this controller.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }
            this._musicDef = null;
            this._isDisposed = true;
        }

        /**
         * Creates a music instance that loops and starts playing with 0 volume.
         */
        private createMusic(settings: Definition): RS.Audio.LayeredSound
        {
            const sourceTrack = RS.Asset.Store.getAsset(settings.foreground.asset).asset as Audio.ISoundSource;
            if (sourceTrack)
            {
                this._startMusicProps.startTime = settings.foreground.start || 0;
                // this._startMusicProps.duration = Math.round(sourceTrack.length - this._startMusicProps.startTime - (settings.foreground.end || 0));
            }
            else
            {
                logger.warn(`Music asset '${settings.foreground.asset.id}' not found!`);
                this._startMusicProps.startTime = 0;
                // this._startMusicProps.duration = undefined;
            }
            const snd = new Audio.LayeredSound(Audio.play(settings.foreground.asset, this._startMusicProps));
            snd.setLayerVolume(0, 0);
            if (settings.background)
            {
                const srcTrack = RS.Asset.Store.getAsset(settings.background.asset).asset as Audio.ISoundSource;
                if (srcTrack)
                {
                    this._startMusicProps.startTime = settings.background.start || 0;
                    // this._startMusicProps.duration = Math.round(srcTrack.length - this._startMusicProps.startTime - (settings.background.end || 0));
                }
                else
                {
                    logger.warn(`Music asset '${settings.background.asset.id}' not found!`);
                    this._startMusicProps.startTime = 0;
                    // this._startMusicProps.duration = undefined;
                }
                snd.addSoundLayer(Audio.play(settings.background.asset, this._startMusicProps));
                snd.setLayerVolume(1, 0);
            }
            snd.masterVolume = settings.masterVolume != null ? settings.masterVolume : 1.0;
            return snd;
        }

        /**
         * Turns off the specified music completely.
         */
        private turnOff(music: Audio.LayeredSound)
        {
            if (!this._musicPlaying) { return; }
            this._musicPlaying = false;
            Ticker.unregisterTickers(this);
            this._music.fadeVolume(0, this._fadeTime, () =>
            {
                if (this._music) { this._music.paused = true; }
            });
        }

        /**
         * Turns on the specified music.
         */
        private turnOn(music: Audio.LayeredSound, layerIndex: number, volume: number = 1)
        {
            if (this._musicPlaying) { return; }
            this._musicPlaying = true;
            Ticker.registerTickers(this);
            music.paused = false;
            music.fadeLayerVolume(layerIndex, volume, this._fadeTime);
        }

        /**
         * Updates the state of the music. Should only be called when something happens to change the state.
         */
        @Callback
        private updateMusicState(): void
        {
            if (this.settings.musicArbiter.value !== this._musicDef)
            {
                if (this._musicPlaying)
                {
                    this.crossfadeTo(this._settings.musicArbiter.value, this.settings.fadeTime);
                }
                else
                {
                    this._musicDef = this.settings.musicArbiter.value;
                    this._musicPlaying = false;
                }
                logger.debug("Music swapped over");
            }

            let masterVolume = 1;
            let foregroundVolume = 1;
            let backgroundVolume = 1;

            if (this._musicDef != null)
            {
                masterVolume = this._musicDef.masterVolume != undefined ? this._musicDef.masterVolume : 1;
                foregroundVolume = (this._musicDef.foreground.volume != undefined ? this._musicDef.foreground.volume : 1) * masterVolume;
                backgroundVolume = (this._musicDef.background ? (this._musicDef.background.volume != undefined ? this._musicDef.background.volume : 1) : 1) * masterVolume;
            }
            switch (this.settings.stateArbiter.value)
            {
                case State.Previous:
                    // Only update volume
                    switch (this._curLayer)
                    {
                        case MusicLayer.Foreground:
                            this.switchToLayer(MusicLayer.Foreground, false, this.settings.volumeArbiter.value * foregroundVolume);
                            break;
                        case MusicLayer.Background:
                            this.switchToLayer(MusicLayer.Background, false, this.settings.volumeArbiter.value * backgroundVolume);
                            break;
                    }
                    break;
                case State.Silent:
                    this.switchToLayer(MusicLayer.None, false);
                    this._curLayer = MusicLayer.None;
                    break;
                case State.Background:
                    this.switchToLayer(MusicLayer.Background, this._curLayer === MusicLayer.Foreground, this.settings.volumeArbiter.value * backgroundVolume);
                    this._curLayer = MusicLayer.Background;
                    break;
                case State.Foreground:
                    this.switchToLayer(MusicLayer.Foreground, false, this.settings.volumeArbiter.value * foregroundVolume);
                    this._curLayer = MusicLayer.Foreground;
                    break;
            }
            // logger.debug(`updateMusicState (volume=${this.settings.volumeArbiter.state}, state=${State[this.settings.stateArbiter.state]})`);
        }

        /**
         * Initiates a switch to the specified music layer.
         */
        private switchToLayer(layer: MusicLayer, linger: boolean = false, volume: number = 1): void
        {
            if (!this._musicDef) { return; }
            if (this._musicDef.alwaysForeground && layer === MusicLayer.Background) { layer = MusicLayer.Foreground; }
            if (!this._music || this._music.isDisposed)
            {
                RS.Tween.removeTweens<Controller>(this);
                this._music = this.createMusic(this._musicDef);
                this._beatCount = 0;
                switch (layer)
                {
                    case MusicLayer.Foreground:
                        this.turnOn(this._music, 0, volume);
                        break;
                    case MusicLayer.Background:
                        this.turnOn(this._music, 1, volume);
                        break;
                }
            }
            else if (linger)
            {
                RS.Tween.get<Controller>(this, {override: true})
                    .wait(this.settings.lingerTime)
                    .call((t) =>
                    {
                        this.doLayerFade(layer, volume);
                        this._musicPlaying = true;
                    });
            }
            else
            {
                RS.Tween.removeTweens<Controller>(this);
                this.doLayerFade(layer, volume);
                this._musicPlaying = true;
            }
        }

        /**
         * Immediately performs a switch to the specified music layer.
         */
        private doLayerFade(layer: MusicLayer, volume: number = 1): void
        {
            // logger.log(`Switching to '${MusicLayer[layer]}' music layer`, RS.LogLevel.Info);
            if (this._music == null)
            {
                logger.warn("Failed to perform layer fade");
                return;
            }
            this._music.fadeLayerVolume(0, layer === MusicLayer.Foreground ? volume : 0, this._fadeTime);
            this._music.fadeLayerVolume(1, layer === MusicLayer.Background ? volume : 0, this._fadeTime);
        }

        /**
         * Handles a tick event.
         */
        @Tick({ kind: Ticker.Kind.RealTime })
        private tick(ev: Ticker.Event)
        {
            if (!this._music) { return; }
            // if (this._musicDef.beatOffset == null || this._musicDef.beatDelay == null) { return; }

            // const beatID = Math.floor((this._spinMusic.position - this._spinMusicSettings.beatOffset) / this._spinMusicSettings.beatDelay);
            // if (beatID > this._beatCount)
            // {
            //     this._beatCount = beatID;
            //     RS.EventManager.publish(MusicController.BeatEvent.empty);
            // }
        }
    }

    IController.register(Controller);

}