namespace RS.Music
{
    /**
     * Defines a music track.
     */
    export interface Definition
    {
        /** The foreground layer. */
        foreground: Layer;

        /** The background layer - if missing, will be silence. */
        background?: Layer;

        /** Modulate both layers by this volume - will be applied on top of individual layer volume. */
        masterVolume?: number;

        /** If true, will always use the foreground layer. */
        alwaysForeground?: boolean;
    }

    /**
     * Defines a layer for a music track.
     */
    export interface Layer
    {
        /** The asset to use for this layer. */
        asset: Asset.SoundReference;

        /** The volume for this layer. */
        volume?: number;

        /** Start playing this layer at the specified offset (ms), useful for clipping out silence. */
        start?: number;

        /** Stop playing this layer and loop at the specified offset (ms), useful for clipping out silence. */
        end?: number;
    }
}