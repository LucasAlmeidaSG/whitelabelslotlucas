namespace RS.Music
{
    /**
     * The state of a music track.
     */
    export enum State { Previous, Silent, Background, Foreground }

    /**
     * Responsible for managing music for the game.
     */
    export interface IController extends Controllers.IController<IController.Settings>
    {
        
    }

    export const IController = Controllers.declare<IController, IController.Settings>(Strategy.Type.Instance);

    export namespace IController
    {
        export interface Settings
        {
            musicArbiter: IReadonlyObservable<Definition | null>;
            volumeArbiter: IReadonlyObservable<number>;
            stateArbiter: IReadonlyObservable<State>;
            pauseArbiter: IReadonlyObservable<boolean>;
            fadeTime: number;
            lingerTime: number;
        }
    }
}