namespace RS
{
    /**
     * Encapsulates a local engine with no actual server communication.
     * This can be configured to match the requirements of a game (e.g. specific symbols, bandsets and winlines) via settings.
     */
    export abstract class DummyEngine<TModels extends Models = Models, TSettings extends DummyEngine.Settings = DummyEngine.Settings, TReplayData extends {} = {}, TForceResult extends Engine.ForceResult = Engine.ForceResult> implements Engine.IEngine<TModels, TSettings, TReplayData, TForceResult>
    {
        /** Published when an init response has been received and fully processed. */
        public readonly onInitResponseReceived = createEvent<Engine.IResponse>();

        /** Published when a bet response has been received and fully processed. */
        public readonly onBetResponseReceived = createEvent<Engine.IResponse>();

        /** Published when a close response has been received and fully processed. */
        public readonly onCloseResponseReceived = createEvent<Engine.IResponse>();

        /** Gets if this engine supports replay. */
        public readonly supportsReplay: boolean = false;

        /** Gets if this engine has reel sets. */
        public readonly hasReelSets: boolean = true;

        protected _force: OneOrMany<TForceResult>;
        protected _rng: Math.MersenneTwister;
        protected _gameState: Models.State.Type;
        protected _balance: number;

        public constructor(public readonly settings: TSettings, public readonly models: TModels)
        {
            this._rng = new Math.MersenneTwister((Math.random() * (1 << 30))|0);
        }

        /**
         * Rigs the next logic request.
         */
        public force(forceResult: OneOrMany<TForceResult>)
        {
            this._force = forceResult;
        }

        /**
         * Starts a replay with the given data.
         */
        public replay(replayData: TReplayData): void
        {
            Log.warn("Replay not supported for DummyEngine!");
        }

        /**
         * Initialises the engine and sends an init request.
         * Async.
         */
        public async init(): Promise<Engine.IResponse>
        {
            this.models.state.state = this._gameState = Models.State.Type.Closed;

            this.configureConfigModel(this.models.config);
            this.configureStakeModel(this.models.stake);

            this._balance = 1000 * 100;
            this.configureCustomerModel(this.models.customer);

            this.getResultsModel(this.models).length = 0;

            const recoveryData = RS.Storage.load<DummyEngine.RecoveryData | null>("DummyEngine/RecoveryData", null);
            if (recoveryData)
            {
                this.recover(recoveryData);
            }

            const dummyResponse = { isError: false, request: null };
            this.onInitResponseReceived.publish(dummyResponse);
            return dummyResponse;
        }

        public async bet(payload: Engine.ILogicRequest.Payload): Promise<Engine.IResponse>
        {
            const response = await this.logic(payload);
            this.onBetResponseReceived.publish(response);
            return response;
        }

        /**
         * Sends a close request.
         * Async.
         */
        public async close(): Promise<RS.Engine.IResponse>
        {
            const error = this.getCloseError();
            if (error) { return error; }

            // Close game
            this.models.error.type = RS.Models.Error.Type.None;
            this.models.state.state = this._gameState = Models.State.Type.Closed;

            // Store recovery data
            RS.Storage.save("DummyEngine/RecoveryData", this.captureDataForRecovery());

            // Done
            const dummyResponse = { isError: false, request: null };
            this.onCloseResponseReceived.publish(dummyResponse);
            return dummyResponse;
        }

        /**
         * Gets an array of all triggerable replays.
         */
        public async getReplayData() { return {}; }

        /**
         * Gets an array of all GREG replays.
         */
        public async getGREGData() { return {}; }

        protected configureCustomerModel(model: Models.Customer): void
        {
            model.finalBalance.primary = this._balance;
            model.currencyCode = "GBP";
            model.currencyMultiplier = 1.0;
        }

        protected configureConfigModel(model: Models.Config): void
        {
            model.maxWinAmount = this.settings.maxWinAmount;
            model.minRTP = this.settings.minRTP;
            model.maxRTP = this.settings.maxRTP;
        }

        protected configureStakeModel(model: Models.Stake): void
        {
            model.betOptions = this.settings.betOptions.map((value) =>
            {
                return {value};
            });
            model.defaultBetIndex = this.settings.defaultBetOption;
            model.currentBetIndex = this.settings.defaultBetOption;

            this.applyBetConfiguration(model);

            RS.assert(model.betOptions.length > 0, "models.stake must have one or more bet options");
        }

        protected applyBetConfiguration(model: Models.Stake)
        {
            const minBet = URL.getParameterAsNumber("minbet", null);
            const maxBet = URL.getParameterAsNumber("maxbet", null);
            Find.remove(model.betOptions, (betOption) =>
            {
                const value = betOption.value;
                return (minBet != null && value < minBet) || (maxBet != null && value > maxBet);
            });
            model.defaultBetIndex = Math.clamp(model.defaultBetIndex, 0, model.betOptions.length - 1);
            model.currentBetIndex = Math.clamp(model.currentBetIndex, 0, model.betOptions.length - 1);
        }

        /**
         * Gets an error to be returned when requesting a game.
         */
        protected getLogicError(): Engine.IResponse | null
        {
            if (this._gameState !== Models.State.Type.Closed)
            {
                return this.error("Can't start a new wager when the previous wager has not been closed.", true);
            }
        }

        /**
         * Gets an error to be returned when attempting to close.
         */
        protected getCloseError(): Engine.IResponse | null
        {
            return null;
        }

        /**
         * Captures the current state of play into an arbitrary object for recovery.
         */
        protected captureDataForRecovery(): DummyEngine.RecoveryData
        {
            // If we're closed, there is no recovery data
            if (this._gameState === Models.State.Type.Closed)
            {
                return {
                    isOpen: false,
                    balance: this._balance
                };
            }

            // Encode data we need to reproduce the game result
            const totalBet = Models.Stake.getCurrentBet(this.models.stake);
            const gameResult = this.getResultsModel(this.models)[0];
            return {
                isOpen: true,
                totalStake: totalBet.value,
                balance: this._balance,
                initialBalance: gameResult.balanceBefore.primary,
                accumulatedWin: gameResult.win.accumulatedWin
            };
        }

        /**
         * Restores the state of play from a previously captured recovery object.
         * @param data
         */
        protected recover(data: DummyEngine.RecoveryData): void
        {
            this.clearResultsModel(this.models);
            if (data.isOpen === true)
            {
                this.recoverOpenGame(data);
            }
            else if (data.isOpen === false)
            {
                this.recoverClosedGame(data);
            }
            this.models.state.state = this._gameState;
            RS.Models.Balances.clear(this.models.customer.finalBalance);
            this.models.customer.finalBalance.primary = this._balance;
        }

        protected recoverOpenGame(data: DummyEngine.RecoveryData.Open)
        {
            // Recover balance and state
            this._balance = data.initialBalance;
            this._gameState = Models.State.Type.Open;

            // Recover stake
            const totalStake: Models.StakeAmount = { value: data.totalStake };
            let found = false;
            this.models.stake.override = null;
            for (let i = 0, l = this.models.stake.betOptions.length; i < l; ++i)
            {
                const betOption = this.models.stake.betOptions[i];
                if (betOption.value === totalStake.value)
                {
                    this.models.stake.currentBetIndex = i;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                // Set override stake since it's not in our stake list
                this.models.stake.override = { totalBet: totalStake.value };

                // Set the stake index to the nearest one in our stake list
                const validIndices = Object.keys(this.models.stake.betOptions);
                const nearestIndex = Find.closestToZero(validIndices, (index) => this.models.stake.betOptions[index].value - totalStake.value);
                this.models.stake.currentBetIndex = parseInt(nearestIndex);
            }

            // Force the stored result
            this.applyReplayForce(data);

            // Replay the game
            this.doLogic(totalStake.value);
        }

        protected applyReplayForce(data: DummyEngine.RecoveryData.Open)
        {
            this._force = null;
        }

        protected recoverClosedGame(data: DummyEngine.RecoveryData.Closed)
        {
            this._balance = data.balance;
            this._gameState = Models.State.Type.Closed;
        }

        /**
         * Sends a logic request.
         * Async.
         */
        protected async logic(payload: Engine.ILogicRequest.Payload): Promise<Engine.IResponse>
        {
            const error = this.getLogicError();
            if (error) { return error; }

            // Determine stake per line and validate everything
            const totalBet = payload.betAmount;
            if (!this.models.stake.override && this.settings.betOptions.indexOf(totalBet.value) === -1) { return this.error("Invalid bet amount", false); }
            if (totalBet.value > this._balance) { return this.error("Insufficient funds to place this bet", false); }

            // Consume stake
            this._balance -= totalBet.value;

            // Clear error and advance state
            this.models.error.type = RS.Models.Error.Type.None;
            this.models.state.state = this._gameState = Models.State.Type.Open;

            // Do logic
            try
            {
                this.doLogic(totalBet.value);
            }
            catch (err)
            {
                Log.error(err);
                this.models.state.state = this._gameState = Models.State.Type.Closed;
                return this.error(`${err}`, false);
            }

            // Update balance
            RS.Models.Balances.clear(this.models.customer.finalBalance);
            this.models.customer.finalBalance.primary = this._balance;

            // Store recovery data
            RS.Storage.save("DummyEngine/RecoveryData", this.captureDataForRecovery());

            // Done, remove first force result
            if (Is.array(this._force) && this._force.length > 0)
            {
                this._force.shift();
            }
            else
            {
                this._force = null;
            }

            const dummyResponse = { isError: false, request: null };
            return dummyResponse;
        }

        /**
         * Performs logic for a single play.
         * @param stakePerLine
         * @param accumulatedWin total win up to (but NOT including) this game
         */
        protected doLogic(totalStake: number): void
        {
            // Generate game result
            const gameResults = this.getResultsModel(this.models);
            gameResults.length = 1;
            gameResults[0] = this.generateResults(totalStake);

            // Award winnings
            this._balance += this.getResultsModel(this.models)[0].win.totalWin;
            gameResults[0].balanceAfter.primary = this._balance;
        }

        protected generateResults(totalStake: number): Models.GameResult
        {
            const force = Is.array(this._force) && this._force.length > 0 ? this._force[0] : this._force as Engine.ForceResult;

            // Determine result
            if (force)
            {
                //use force
            }
            // Compute wins
            const totalWin = 0;

            // Assemble game result
            const gameResult: Models.GameResult =
            {
                index: 0,
                win:
                {
                    totalWin: totalWin,
                    accumulatedWin: totalWin
                },
                balanceBefore: { primary: this._balance, named: {} },
                balanceAfter: { primary: this._balance, named: {} }
            };
            return gameResult;
        }

        /**
         * Generates a dummy error response.
         * @param message
         * @param fatal
         */
        protected error(message: string, fatal: boolean): Engine.IResponse
        {
            this.models.error.type = RS.Models.Error.Type.Server;
            this.models.error.title = "DUMMY ENGINE ERROR";
            this.models.error.message = message;
            this.models.error.options = fatal
                ? [ { type: RS.Models.Error.OptionType.Close, text: "OK" } ]
                : [ { type: RS.Models.Error.OptionType.Continue, text: "OK" } ];
            return { isError: true, request: null };
        }

        protected abstract getResultsModel(models: Models): Models.GameResults;
        protected abstract clearResultsModel(models: Models);
    }

    export namespace DummyEngine
    {
        export namespace RecoveryData
        {
            export interface Base
            {
                isOpen: boolean;
                balance: number;
            }

            export interface Open extends Base
            {
                isOpen: true;
                totalStake: number;
                initialBalance: number;
                accumulatedWin: number;
            }

            export interface Closed extends Base
            {
                isOpen: false;
            }
        }

        export type RecoveryData = RecoveryData.Open | RecoveryData.Closed;

        export interface Settings
        {
            betOptions: number[];
            defaultBetOption: number;
            maxWinAmount: number;
            minRTP: number;
            maxRTP: number;
        }

        export const defaultSettings: Settings =
        {
            betOptions: [ 10, 20, 50, 100, 200, 500, 1000, 2000, 5000 ],
            defaultBetOption: 4,
            maxWinAmount: 2.5e7,
            minRTP: 96.5,
            maxRTP: 96.5,
        };
    }
}
