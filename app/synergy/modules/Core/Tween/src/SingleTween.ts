/// <reference path="Tween.ts" />

namespace RS
{
    /** Represents the type of an action */
    enum TweenActionType { To, Set, Wait, Call, PlaySound, PlayAnim }

    /** Base action */
    interface BaseTweenAction<T1 extends object, T2 extends TweenActionType>
    {
        type: T2;
        startTime?: number;
        endTime?: number;
        duration: number;
        predictedProps: object;
    }

    interface ToTweenAction<T extends object> extends BaseTweenAction<T, TweenActionType.To> { props: object; ease: EaseFunction; }
    interface SetTweenAction<T extends object> extends BaseTweenAction<T, TweenActionType.Set> { props: object; }
    interface WaitTweenAction<T extends object> extends BaseTweenAction<T, TweenActionType.Wait> { }
    interface CallTweenAction<T extends object> extends BaseTweenAction<T, TweenActionType.Call> { func: (t: Tween<T>) => void; }

    /** An action to be executed by a tween */
    type TweenAction<T extends object> = ToTweenAction<T> | SetTweenAction<T> | WaitTweenAction<T> | CallTweenAction<T>;

    /**
     * Tweens properties for a single target object over time.
    **/
    export class SingleTween<T extends object> extends Tween<T>
    {
        protected readonly _target: TweenTarget<T>;

        private _actionQueue: TweenAction<T>[] = [];
        /** Current position in queue (e.g. 0 = start of queue) */
        private _curActionIndex: number = 0;

        /** Gets the target object for this tween. */
        public get target(): T { return this._target; }

        /**
         * Initialises a new instance of the Tween class.
         * @param target        Target object instance.
         * @param manualTick    If true, do not use Tween.registerTween.
        **/
        public constructor(target: TweenTarget<T>, manualTick: boolean = false)
        {
            super(manualTick);
            if (target == null)
            {
                Log.error(`Attempt to create new tween with null target`);
                return null;
            }
            this._target = target;
            if (target == null) { return; }
            if (target._activeTweens == null) { target._activeTweens = []; }
            target._activeTweens.push(this);
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            if (this._target._activeTweens)
            {
                const myIdx = this._target._activeTweens.indexOf(this);
                if (myIdx !== -1) { this._target._activeTweens.splice(myIdx, 1); }
            }
            super.dispose();
        }

        // ============= \\
        // = OPERATORS = \\
        // ============= \\

        /**
         * Interpolates to the specified properties over a duration, using an ease.
        **/
        @CheckDisposed
        public to(props: TweenProps<T>, duration: number, ease: EaseFunction = Tween.defaultEase): this
        {
            if (duration < 0) { Log.warn("Negative duration passed to tween"); duration = 0; }

            this._actionQueue.push({
                type: TweenActionType.To,
                duration, props, ease,
                predictedProps: null
            });

            this._dirty = true;
            return this;
        }

        /**
         * Interpolates from the specified properties back to the starting properties over a duration, using an ease.
        **/
       @CheckDisposed
       public from(props: TweenProps<T>, duration: number, ease: EaseFunction = Tween.defaultEase): this
       {
            if (duration < 0) { Log.warn("Negative duration passed to tween"); duration = 0; }

            const originalProps: TweenProps<T> = {};
            for (const key in props)
            {
                if (props.hasOwnProperty(key))
                {
                    originalProps[key] = TweenUtils.capture(this._target[key]);
                }
            }

            return this.set(props).to(originalProps, duration, ease);
       }

        /**
         * Sets the specified properties without interpolation or delay.
        **/
        @CheckDisposed
        public set(props: TweenProps<T>): this
        {
            this._actionQueue.push({
                type: TweenActionType.Set,
                duration: 0, props,
                predictedProps: null
            });

            this._dirty = true;
            return this;
        }

        /**
         * Waits for the specified duration.
        **/
        @CheckDisposed
        public wait(duration: number): this
        {
            if (duration < 0) { Log.warn("Negative duration passed to tween"); duration = 0; }

            this._actionQueue.push({
                type: TweenActionType.Wait,
                duration,
                predictedProps: null
            });

            this._dirty = true;
            return this;
        }

        /**
         * Calls the specified function.
        **/
        @CheckDisposed
        public call(func: (t: Tween<T>) => void): this
        {
            this._actionQueue.push({
                type: TweenActionType.Call,
                duration: 0,
                func,
                predictedProps: null
            });

            this._dirty = true;
            return this;
        }

        /** Animates a wiggle using rotation and position. */
        @CheckDisposed
        public wiggle(settings: SingleTween.WiggleSettings): this
        {
            const obj = this.target;

            function forceNarrow<T2>(props: TweenProps<T>): props is (TweenProps<T> & T2) { return true; }
            interface WithRotation
            {
                rotation: number;
            }
            function hasRotation(objVar: any): objVar is WithRotation { return Is.object(objVar) && Is.number(objVar.rotation); }

            //cache initial props to return to after wiggle
            const cache: TweenProps<T> = {};
            if (Is.vector2D(obj) && forceNarrow<Math.Vector2D>(cache)) // this is a bit of a hack
            {
                cache.x = obj.x;
                cache.y = obj.y;
            }
            if (hasRotation(obj) && forceNarrow<WithRotation>(cache))
            {
                cache.rotation = obj.rotation;
            }

            const iterations = Math.floor(settings.duration / settings.frequency);
            for (let i = 0; i < iterations; i++)
            {
                const props: TweenProps<T> = {};

                const elapsed = (settings.initialElapsed || 0) + i * settings.frequency;
                const intensity = settings.startIntensity + (elapsed / 1000.0) * settings.rampIntensity;

                if (Is.vector2D(obj) && forceNarrow<Math.Vector2D>(props)) // this is a bit of a hack
                {
                    props.x = obj.x + RS.Math.linearInterpolate(settings.positionMin.x, settings.positionMax.x, Math.random()) * intensity;
                    props.y = obj.y + RS.Math.linearInterpolate(settings.positionMin.y, settings.positionMax.y, Math.random()) * intensity;
                }
                if (hasRotation(obj) && forceNarrow<WithRotation>(props))
                {
                    props.rotation = obj.rotation + RS.Math.linearInterpolate(settings.rotation.min, settings.rotation.max, Math.random()) * Math.PI * intensity;
                }

                if (i === iterations - 1 && settings.reset)
                {
                    this.to(cache, settings.frequency, settings.ease || Tween.defaultEase);
                }
                else
                {
                    this.to(props, settings.frequency, settings.ease || Tween.defaultEase);
                }
            }

            return this;
        }

        // ================ \\
        // = FLOW CONTROL = \\
        // ================ \\

        /**
         * Stops this tween immediately.
         * Will NOT advance the tween - e.g. any pending actions will NOT be executed.
         * @param reject    If true, rejects all pending promise callbacks.
        **/
        @CheckDisposed
        public finish(reject: boolean = false): void
        {
            if (this._dirty) { this.refresh(); }
            if (this._finished) { return; }
            this._position = this._duration;
            this._finished = true;
            this._curActionIndex = this._actionQueue.length;
            if (this._target._activeTweens != null)
            {
                const myIdx = this._target._activeTweens.indexOf(this);
                if (myIdx !== -1) { this._target._activeTweens.splice(myIdx, 1); }
            }
            if (!this._manualTick) { Tween.unregisterTween(this); }
            reject ? this.reject(new Tween.FinishedRejection()) : this.resolve();
        }

        /**
         * Sets the new position of this tween in ms.
         * Executes all actions between current and new positions.
         * @param canMoveBack   If true and position < current position, will reverse back through actions, otherwise will wrap around the end
         * @returns             True if the tween finished by this call.
        **/
        @CheckDisposed
        public setPosition(position: number, mode: StepBehaviour = StepBehaviour.ForwardsOnly): boolean
        {
            if (position === this._position) { return false; }
            if (this._dirty) { this.refresh(); }

            // Clamp/wrap position to between valid endpoints as required
            if (position > this._duration)
            {
                if (this._loop)
                {
                    position %= this._duration;
                }
                else
                {
                    position = this._duration;
                }
            }
            if (position < 0.0)
            {
                if (this._loop)
                {
                    position = (position % this._duration) + this._duration;
                }
                else
                {
                    position = 0.0;
                }
            }

            // Skip
            if (mode === StepBehaviour.Skip || mode === StepBehaviour.SkipAfter)
            {
                this._position = position;
                if (!this._manualTick && this._finished) { Tween.registerTween(this); }
                if (this._target._activeTweens.indexOf(this) === -1) { this._target._activeTweens.push(this); }
                this._finished = false;
                this.clearAwaitableResult();

                const skipAfter = mode === StepBehaviour.SkipAfter;
                const instantaneousDelta = skipAfter ? 1.0 : 0.0;

                // Reset target properties to initial state.
                for (let i = this._actionQueue.length - 1; i >= 0; i--)
                {
                    const action = this._actionQueue[i];
                    // If new position is after current action started, stop.
                    TweenUtils.applyProps(this._target, action.predictedProps);
                }

                // Find and execute current action.
                for (let i = 0; i < this._actionQueue.length; i++)
                {
                    const action = this._actionQueue[i];
                    if (position < action.startTime) { break; }

                    TweenUtils.applyProps(this._target, action.predictedProps);
                    if (position >= action.startTime && position <= action.endTime)
                    {
                        this._curActionIndex = i;
                        this.executeAction(action, action.duration === 0 ? instantaneousDelta : (this._position - action.startTime) / action.duration);
                        // Check if action lead to disposal of tween
                        if (this._isDisposed) { return true; }
                        // If skipping to before this position, go no further.
                        if (!skipAfter) { break; }
                    }
                }
                if (this._position >= this._duration)
                {
                    this.finish();
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // Determine where we're stepping to
            let totalDelta: number, stepDirection: number;
            if (position > this._position)
            {
                if (mode === StepBehaviour.ForwardsOnly)
                {
                    totalDelta = position - this._position;
                    stepDirection = 1.0;
                }
                else
                {
                    totalDelta = this._duration - (position - this._position);
                    stepDirection = -1.0;
                }
            }
            else
            {
                if (mode === StepBehaviour.BackwardsOnly)
                {
                    totalDelta = this._position - position;
                    stepDirection = -1.0;
                }
                else
                {
                    totalDelta = this._duration - (this._position - position);
                    stepDirection = 1.0;
                }
            }

            // Advance
            return this.advance(totalDelta * stepDirection);
        }

        /**
         * Advances the tween by the specified amount.
         * @param totalDelta    How much to advance by. May be negative.
         * @returns             True if the tween finished by this call.
        **/
        @CheckDisposed
        public advance(totalDelta: number): boolean
        {
            // Sanity check
            if (totalDelta === 0.0) { return false; }

            // Refresh if needed
            if (this._dirty) { this.refresh(); }
            const stepDirection = totalDelta > 0.0 ? 1.0 : -1.0;

            // Find out where we currently are
            let curAction = this._actionQueue[this._curActionIndex];
            if (curAction == null)
            {
                // If we're at an endpoint and we're moving away from the actions, exit here
                if (this._curActionIndex >= this._actionQueue.length && stepDirection > 0.0) { return; }
                if (this._curActionIndex < 0 && stepDirection < 0.0) { return; }

                // Shift to the endpoint
                this._curActionIndex = RS.Math.clamp(this._curActionIndex, 0, this._actionQueue.length - 1);
                curAction = this._actionQueue[this._curActionIndex];
                if (curAction == null) { return false; }
            }

            // Iterate until we're out of delta and there are no instantaneous actions at the current position
            while (totalDelta > 0.0 || curAction.duration === 0.0)
            {
                // Identify delta through current action
                const projectedPosition = this._position + totalDelta * stepDirection;
                const actionDelta = curAction.duration === 0 ? (stepDirection > 0.0 ? 1.0 : 0.0) : ((projectedPosition - curAction.startTime) / curAction.duration);

                // Check if we reached edge of action
                if (stepDirection > 0.0 && actionDelta >= 1.0)
                {
                    // Step forwards
                    this.executeAction(curAction, 1.0);
                    // Check if action lead to disposal of tween
                    if (this._isDisposed) { return true; }
                    this._position = curAction.endTime;
                    totalDelta = projectedPosition - this._position;
                    this._curActionIndex++;
                    curAction = this._actionQueue[this._curActionIndex];
                    if (curAction == null)
                    {
                        // Loop around
                        if (this._loop)
                        {
                            this._curActionIndex = 0;
                            this._position = 0.0;
                            curAction = this._actionQueue[this._curActionIndex];
                        }
                        else
                        {
                            this.finish();
                            return true;
                        }
                    }
                    this.executeAction(curAction, 0.0);
                    // Check if action lead to disposal of tween
                    if (this._isDisposed) { return true; }
                }
                else if (stepDirection < 0.0 && actionDelta <= 0.0)
                {
                    // Step backwards
                    this.executeAction(curAction, 0.0);
                    // Check if action lead to disposal of tween
                    if (this._isDisposed) { return true; }
                    this._position = curAction.startTime;
                    totalDelta = this._position - projectedPosition;
                    this._curActionIndex--;
                    curAction = this._actionQueue[this._curActionIndex];
                    if (curAction == null)
                    {
                        // Loop around
                        if (this._loop)
                        {
                            this._curActionIndex = this._actionQueue.length - 1;
                            this._position = this._duration;
                            curAction = this._actionQueue[this._curActionIndex];
                        }
                        else
                        {
                            this.finish();
                            return true;
                        }
                    }
                    this.executeAction(curAction, 1.0);
                    // Check if action lead to disposal of tween
                    if (this._isDisposed) { return true; }
                }
                else
                {
                    // Step through action
                    this.executeAction(curAction, actionDelta);
                    // Check if action lead to disposal of tween
                    if (this._isDisposed) { return true; }
                    this._position = curAction.startTime + curAction.duration * actionDelta;
                    totalDelta = 0.0;
                }
            }

            // Change
            this.onPositionChanged.publish();

            // Done
            return false;
        }

        /**
         * Updates duration and times after an action change.
        **/
        protected refresh(): void
        {
            if (!this._dirty) { return; }
            this._dirty = false;
            let curTime = 0;
            const predictedProps: object = {};
            for (let i = 0; i < this._actionQueue.length; i++)
            {
                const action = this._actionQueue[i];
                action.startTime = curTime;
                curTime += action.duration;
                action.endTime = curTime;
                switch (action.type)
                {
                    case TweenActionType.Set:
                    case TweenActionType.To:
                    {
                        for (const key in action.props)
                        {
                            if (predictedProps[key] === undefined)
                            {
                                predictedProps[key] = TweenUtils.capture(this._target[key]);
                            }
                            // if (this._target instanceof PIXI.DisplayObject && (key === "x" || key === "y") && predictedProps["position"] === undefined)
                            // {
                            //     predictedProps["position"] = Tween.capture(this._target["position"]);
                            // }
                        }
                        break;
                    }
                }
                action.predictedProps = TweenUtils.capture(predictedProps);
                switch (action.type)
                {
                    case TweenActionType.Set:
                    case TweenActionType.To:
                    {
                        for (const key in action.props)
                        {
                            predictedProps[key] = TweenUtils.capture(action.props[key]);

                            // if (this._target instanceof PIXI.DisplayObject)
                            // {
                            //     if (key === "position" && action.props["position"] instanceof PIXI.Point)
                            //     {
                            //         const pos: PIXI.Point = action.props["position"];
                            //         if (pos.x != null) { predictedProps["x"] = pos.x; }
                            //         if (pos.y != null) { predictedProps["y"] = pos.y; }
                            //     }
                            //     if (key === "x" || key === "y")
                            //     {
                            //         const pos: PIXI.Point = predictedProps["position"] || (predictedProps["position"] = new PIXI.Point());
                            //         pos.x = action.props["x"] != null ? action.props["x"] : this._target["x"];
                            //         pos.y = action.props["y"] != null ? action.props["y"] : this._target["y"];
                            //     }
                            // }
                        }
                        break;
                    }
                }
            }
            this._duration = curTime;
        }

        /**
         * Executes the specified action. Might be called multiple times for the same action.
         * @param action    The action to execute.
         * @param delta     How far along the action (0-1).
        **/
        private executeAction(action: TweenAction<T>, delta: number): void
        {
            // { To, Set, Wait, Call, PlaySound, PlayAnim };
            switch (action.type)
            {
                case TweenActionType.To:
                {
                    // Compile new props
                    const newProps = delta >= 1.0 ? action.props : TweenUtils.interpolate(action.predictedProps, action.props, action.ease(delta));

                    // Apply them
                    TweenUtils.applyProps(this._target, newProps);

                    break;
                }

                case TweenActionType.Set:
                {
                    // Apply props only if we reach the end of this action
                    if (delta >= 1.0)
                    {
                        TweenUtils.applyProps(this._target, action.props);
                    }
                    break;
                }

                case TweenActionType.Wait:
                {
                    // Take no action here
                    break;
                }

                case TweenActionType.Call:
                {
                    // Call function only if we reach the end of this action
                    if (delta >= 1.0)
                    {
                        action.func(this);
                    }
                    break;
                }
            }
        }
    }

    export namespace SingleTween
    {
        /** Settings for the wiggle function. */
        export interface WiggleSettings
        {
            /** Range of angle to rotate between, in radians */
            rotation: RS.Math.Range.MinMaxRange;
            /** Minimum relative position, in pixels */
            positionMin: RS.Math.Vector2D;
            /** Maximum relative position, in pixels */
            positionMax: RS.Math.Vector2D;
            /** Time it takes to complete the wiggle */
            duration: number;
            /** Duration of a wiggle segment in milliseconds */
            frequency: number;
            /** Starting scale factor for position and rotation changes */
            startIntensity: number;
            /** Itensity added to the starting intensity, in proportion to the progress through the tween */
            rampIntensity: number;
            /** Time offset in milliseconds for intensity calculation */
            initialElapsed?: number;
            /** Tween ease function for controlling the rate of progress over time */
            ease?: RS.EaseFunction;
            reset?: boolean;
        }
    }
}
