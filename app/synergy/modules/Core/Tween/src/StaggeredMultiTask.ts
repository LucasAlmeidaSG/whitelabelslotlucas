namespace RS
{
    /**
     * Encapsulates an asynchronous task comprised of a number of sub-tasks.
     * Sub-tasks will be executed staggered.
     * All sub-tasks must be complete before this task is considered complete.
    **/
    export class StaggeredMultiTask<T> extends MultiTask<T>
    {
        protected _staggerTime: number;

        public constructor(subTasks: Task<T>[], staggerTime: number, failIfOneFails: boolean = true, reducer?: (a: T, b: T) => T)
        {
            super(subTasks, true, failIfOneFails, reducer);
            this._staggerTime = staggerTime;
        }

        /**
         * Cancels this task.
         * @param reject    If true, rejects all pending promises.
        **/
        public cancel(reject: boolean = false)
        {
            RS.Tween.removeTweens<StaggeredMultiTask<T>>(this);
            super.cancel(reject);
        }

        /** Performs the async task. */
        protected doTask(resolve: (value: T) => void, reject: (reason?: string) => void)
        {
            this._numSucceed = 0;
            this._numFail = 0;
            this._pendingSubTasks = this._subTasks.slice();
            this._resolve = resolve;
            this._reject = reject;
            this._shouldAttemptEarlyTermination = true;
            const tween = RS.Tween.get<StaggeredMultiTask<T>>(this);
            for (let i = 0; i < this._subTasks.length; i++)
            {
                const subTask = this._subTasks[i];
                subTask.then((v) =>
                    {
                        this.onSubTaskSucceed(subTask);
                        return undefined;
                    }, (r) =>
                    {
                        this.onSubTaskFail(subTask);
                        return undefined;
                    }
                );

                tween.call((t) => subTask.start());
                tween.wait(this._staggerTime);
            }
        }
    }
}