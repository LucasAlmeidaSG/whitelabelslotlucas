/// <reference path="Tween.ts" />
/// <reference path="SingleTween.ts" />

namespace RS
{
    class ComponentTween<T extends object> extends SingleTween<T>
    {
        constructor(private readonly multiTween: MultiTween<T>, target: T, manualTick = false)
        {
            super(target, manualTick);
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            this.multiTween.notifyTweenDisposed(this);
            super.dispose();
        }
    }

    /**
     * Tweens properties for a set of target objects over time.
    **/
    export class MultiTween<T extends object> extends Tween<T>
    {
        protected _targets: ReadonlyArray<TweenTarget<T>>;
        @RS.AutoDisposeOnSet protected readonly _innerTweens: SingleTween<T>[];

        /** Gets the target objects for this tween. */
        @CheckDisposed
        public get targets(): ReadonlyArray<T> { return this._targets; }

        /** Gets or sets if this tween should loop when complete. */
        @CheckDisposed
        public get loop() { return this._loop; }
        public set loop(value)
        {
            this._loop = value;
            for (const tween of this._innerTweens)
            {
                tween.loop = value;
            }
        }

        /**
         * Initialises a new instance of the Tween class.
         * @param target        Target object array.
         * @param manualTick    If true, do not use Tween.registerTween.
        **/
        public constructor(targets: ReadonlyArray<TweenTarget<T>>, manualTick: boolean = false)
        {
            super(manualTick);
            if (targets == null)
            {
                Log.error(`Attempt to create new tween with null target`);
                return null;
            }
            this._targets = targets;
            if (targets == null) { return; }
            this._innerTweens = targets.map((target) => new ComponentTween<T>(this, target, true));
        }

        // ============= \\
        // = OPERATORS = \\
        // ============= \\

        /** @internal */
        public notifyTweenDisposed(tween: SingleTween<T>)
        {
            const idx = this._innerTweens.indexOf(tween);
            this._innerTweens.splice(idx, 1);
        }

        /**
         * Interpolates to the specified properties over a duration, using an ease.
        **/
        @CheckDisposed
        public to(props: TweenProps<T>, duration: number, ease: EaseFunction = Tween.defaultEase): this
        {
            for (const innerTween of this._innerTweens)
            {
                innerTween.to(props, duration, ease);
            }
            this._dirty = true;
            return this;
        }

        /**
         * Interpolates from the specified properties back to the starting properties over a duration, using an ease.
        **/
        @CheckDisposed
        public from(props: TweenProps<T>, duration: number, ease: EaseFunction = Tween.defaultEase): this
        {
            for (const innerTween of this._innerTweens)
            {
                innerTween.from(props, duration, ease);
            }
            this._dirty = true;
            return this;
        }

        /**
         * Sets the specified properties without interpolation or delay.
        **/
        @CheckDisposed
        public set(props: TweenProps<T>): this
        {
            for (const innerTween of this._innerTweens)
            {
                innerTween.set(props);
            }
            this._dirty = true;
            return this;
        }

        /**
         * Waits for the specified duration.
        **/
        @CheckDisposed
        public wait(duration: number): this
        {
            for (const innerTween of this._innerTweens)
            {
                innerTween.wait(duration);
            }
            this._dirty = true;
            return this;
        }

        /**
         * Calls the specified function.
        **/
        @CheckDisposed
        public call(func: (t: Tween<T>) => void): this
        {
            for (const innerTween of this._innerTweens)
            {
                innerTween.call(func);
            }
            this._dirty = true;
            return this;
        }

        // ================ \\
        // = FLOW CONTROL = \\
        // ================ \\

        /**
         * Stops this tween immediately.
         * Will NOT advance the tween - e.g. any pending actions will NOT be executed.
         * @param reject    If true, rejects all pending promise callbacks.
        **/
        @CheckDisposed
        public finish(reject: boolean = false): void
        {
            if (this._dirty) { this.refresh(); }
            if (this._finished) { return; }
            this._position = this._duration;
            this._finished = true;
            for (const innerTween of this._innerTweens)
            {
                innerTween.finish(false);
            }
            if (!this._manualTick) { Tween.unregisterTween(this); }
            reject ? this.reject(new Tween.FinishedRejection()) : this.resolve();
        }

        /**
         * Sets the new position of this tween in ms.
         * Executes all actions between current and new positions.
         * @param canMoveBack   If true and position < current position, will reverse back through actions, otherwise will wrap around the end
         * @returns             True if the tween finished by this call.
        **/
        @CheckDisposed
        public setPosition(position: number, mode: StepBehaviour = StepBehaviour.ForwardsOnly): boolean
        {
            if (position === this._position) { return false; }
            if (this._dirty) { this.refresh(); }

            const wasFinished = this._finished;
            for (const innerTween of this._innerTweens)
            {
                innerTween.setPosition(position, mode);
            }

            this._position = position;
            // Check if a tween disposed the multi-tween
            if (this.isDisposed) { return !wasFinished && this._finished; }
            if (this._innerTweens.map((t) => t.finished).reduce((a, b) => a && b, true) && !this._finished)
            {
                this.finish();
                return true;
            }
            return false;
        }

        /**
         * Advances the tween by the specified amount.
         * @param totalDelta    How much to advance by. May be negative.
         * @returns             True if the tween finished by this call.
        **/
        @CheckDisposed
        public advance(totalDelta: number): boolean
        {
            // Sanity check
            if (totalDelta === 0.0) { return false; }
            // Refresh if needed
            if (this._dirty) { this.refresh(); }

            const wasFinished = this._finished;
            for (const innerTween of this._innerTweens)
            {
                innerTween.advance(totalDelta * innerTween.timeScale);
            }

            this._position += totalDelta;
            // Check if a tween disposed the multi-tween
            if (this.isDisposed) { return !wasFinished && this._finished; }
            if (this._innerTweens.map((t) => t.finished).reduce((a, b) => a && b, true))
            {
                this.finish();
                return true;
            }
            return false;
        }

        /**
         * Updates duration and times after an action change.
        **/
        protected refresh(): void
        {
            if (!this._dirty) { return; }
            this._dirty = false;
            this._duration = this._innerTweens.reduce((acc, tween) => Math.max(acc, tween.duration), 0);
        }
    }
}
