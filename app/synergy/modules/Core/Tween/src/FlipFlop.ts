/// <reference path="Utils.ts" />

namespace RS
{
    /**
     * A special form of tween that interpolates between two states.
     */
    export class FlipFlopTween<TTarget extends object> implements IDisposable
    {

        /** Published when the left position has been reached. */
        public get onLeft() { return this._onLeft.public };

        /** Published when the right position has been reached. */
        public get onRight() { return this._onRight.public };

        /** Published when we are no longer at either position. */
        public get onMiddle() { return this._onMiddle.public };

        protected _target: TTarget;
        protected _targetState: FlipFlopTween.State = FlipFlopTween.State.Left;
        protected _position: number = 0.0;
        protected _isDisposed: boolean = false;

        /**
         * Gets if this flip flop tween has been disposed.
         */
        public get isDisposed() { return this._isDisposed; }

        /**
         * Gets or sets the target state of this flip flop tween.
         */
        public get state() { return this._targetState; }
        public set state(value) { this._targetState = value; }

        /**
         * Gets or sets the current position of this flip flop tween (0 = left, 1 = right).
         */
        public get position() { return this._position; }
        public set position(value)
        {
            if (value === this._position) { return; }
            this._position = value;
            this.refresh();
        }

        @RS.AutoDispose protected readonly _onLeft = createSimpleEvent();
        @RS.AutoDispose protected readonly _onRight = createSimpleEvent();
        @RS.AutoDispose protected readonly _onMiddle = createSimpleEvent();

        public constructor(target: TTarget, public readonly settings: FlipFlopTween.Settings<TTarget>)
        {
            this._target = target;
            Ticker.registerTickers(this);
        }

        /**
         * Recomputes the tween properties for the object.
         */
        public refresh()
        {
            const newProps = TweenUtils.interpolate(this.settings.left, this.settings.right, (this.settings.ease || Tween.defaultEase)(this._position));
            TweenUtils.applyProps(this._target, newProps);
        }

        /**
         * Disposes this object.
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            Ticker.unregisterTickers(this);
            this._isDisposed = true;
        }

        @Tick({ })
        protected tick(ev: Ticker.Event): void
        {
            const trueDelta = ev.delta / this.settings.duration;

            const oldPos = this.position;
            let newPos = oldPos;
            if (this.state === FlipFlopTween.State.Left)
            {
                newPos = Math.max(0.0, oldPos - trueDelta);
            }
            else
            {
                newPos = Math.min(1.0, oldPos + trueDelta);
            }
            this.position = newPos;

            if (newPos === 0.0 && oldPos > 0.0)
            {
                this._onLeft.publish();
            }
            else if (newPos === 1.0 && oldPos < 1.0)
            {
                this._onRight.publish();
            }
            else if ((oldPos === 0.0 || oldPos === 1.0) && (newPos !== 0.0 && newPos !== 1.0))
            {
                this._onMiddle.publish();
            }
        }
    }

    export namespace FlipFlopTween
    {
        export enum State { Left, Right }

        export interface Settings<TTarget extends object>
        {
            left: TweenProps<TTarget>;
            right: TweenProps<TTarget>;
            duration: number;
            ease?: EaseFunction;
        }
    }
}