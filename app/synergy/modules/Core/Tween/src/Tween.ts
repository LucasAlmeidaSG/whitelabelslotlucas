/// <reference path="Ease.ts" />
/// <reference path="Timeline.ts" />
/// <reference path="Utils.ts" />

namespace RS
{
    /**
     * Settings for a tween object.
     */
    export interface TweenSettings
    {
        loop?: boolean;
        override?: boolean;
        ignoreGlobalPause?: boolean;
        /** Whether or not to reset the target to its original state on disposal. */
        disposeBehaviour?: Tween.DisposeBehaviour;
        realTime?: boolean;
        onChange?: () => void;
        manualTick?: boolean;
        /** Whether or not to start paused. */
        paused?: boolean;
    }

    /**
     * A non-array object. TODO: make work.
     */
    export type SingularObject = object;

    /**
     * Encapsulates properties for a tween target.
     */
    export type TweenProps<T extends SingularObject> = { [P in keyof T]?: T[P]; };

    /**
     * Represents a target object for a tween.
     */
    export type TweenTarget<T extends SingularObject> = T & { _activeTweens?: Tween<T>[]; };

    /**
     * Action stepping behaviour for Tween.setPosition.
     */
    export enum StepBehaviour
    {
        /**
         * Only step forwards through actions.
         * If requested position is smaller than current position and the tween isn't looping, will move to end of actions queue and finish.
         * Default behaviour.
         */
        ForwardsOnly,

        /**
         * Only step backwards through actions.
         * If requested position is smaller than current position and the tween isn't looping, will move to end of actions queue and finish.
         */
        BackwardsOnly,

        /**
         * Don't step through actions at all, just move straight to target position.
         * This will also re-enable the tween if it has finished.
         * Instantaneous actions ('set' and 'call') at the target position will NOT be executed.
         */
        Skip,

        /**
         * As for skip, except instantaneous actions ('set' and 'call') WILL be executed etc.
         */
        SkipAfter
    }

    /**
     * Tweens properties for an arbitrary target over time.
     */
    export abstract class Tween<T extends SingularObject> extends Awaitable<void> implements IDisposable
    {
        // #region Static Members

        protected static _allTweens: Tween<any>[] = [];
        protected static _pendingAddTweens: Tween<any>[] = [];
        protected static _iterating: boolean = false;
        protected static _removedWhileIterating: boolean = false;

        // #endregion

        /** Published when the position of this tween has changed. */
        public readonly onPositionChanged = createSimpleEvent();

        protected _position: number = 0.0;
        protected _duration: number = 0.0;
        protected _paused: boolean = false;
        protected _loop: boolean = false;
        protected _finished: boolean = false;
        protected _isDisposed: boolean = false;
        protected _ignoreGlobalPause: boolean = false;
        protected _realTime: boolean = false;
        protected _dirty: boolean = false;
        protected _manualTick: boolean = false;
        protected _timeScale: number = 1.0;
        protected _disposeBehaviour: Tween.DisposeBehaviour = Tween.DisposeBehaviour.None;

        /** Gets or sets if this tween has been disposed of. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets or sets if this tween is paused. */
        @CheckDisposed
        public get paused() { return this._paused; }
        public set paused(value) { this._paused = value; }

        /** Gets if this tween has finished. */
        @CheckDisposed
        public get finished() { return this._finished; }

        /** Gets the duration of this tween (ms). */
        @CheckDisposed
        public get duration()
        {
            if (this._dirty) { this.refresh(); }
            return this._duration;
        }

        /** Gets or sets the position of this tween (ms). */
        @CheckDisposed
        public get position() { return this._position; }
        public set position(value) { this.setPosition(value); }

        /** Gets or sets if this tween should ignore global pause. */
        @CheckDisposed
        public get ignoreGlobalPause() { return this._ignoreGlobalPause; }
        public set ignoreGlobalPause(value) { this._ignoreGlobalPause = value; }

        /** Gets or sets if this tween should ignore global time scale. */
        @CheckDisposed
        public get realTime() { return this._realTime; }
        public set realTime(value) { this._realTime = value; }

        /** Gets or sets if this tween should loop when complete. */
        @CheckDisposed
        public get loop() { return this._loop; }
        public set loop(value) { this._loop = value; }

        /** Gets or sets the time scale of this tween. */
        @CheckDisposed
        public get timeScale() { return this._timeScale; }
        public set timeScale(value) { this._timeScale = value; }

        /** Gets or sets if this tween's ticking will be manually controlled. */
        @CheckDisposed
        public get manualTick() { return this._manualTick; }
        public set manualTick(value)
        {
            if (this._manualTick === value) { return; }
            this._manualTick = value;
            if (value)
            {
                Tween.unregisterTween(this);
            }
            else if (!value && !this._finished)
            {
                Tween.registerTween(this);
            }
        }

        /** Gets or sets whether this tween will reset to the start on disposal. */
        @CheckDisposed
        public get disposeBehaviour() { return this._disposeBehaviour; }
        public set disposeBehaviour(value) { this._disposeBehaviour = value; }

        /**
         * Initialises a new instance of the Tween class.
         * @param manualTick    If true, do not use Tween.registerTween.
         */
        public constructor(manualTick: boolean = false)
        {
            super();

            this._manualTick = manualTick;
            if (!manualTick) { Tween.registerTween(this); }
        }

        // #region Static Interface

        /**
         * Removes ALL tweens.
         * @param reject    Whether to reject any promise callbacks on the tweens
         * @returns         Number of tweens removed.
         */
        public static removeAllTweens(reject?: boolean): number
        {
            Log.warn("[Tween] Removing ALL tweens.");
            if (this._iterating)
            {
                let removeCount = 0;
                for (let i = 0; i < this._allTweens.length; i++)
                {
                    if (this._allTweens[i] != null)
                    {
                        this._allTweens[i].finish();
                        removeCount++;
                    }
                }
                return removeCount;
            }
            const tmp = [...this._allTweens];
            for (let i = 0; i < tmp.length; i++)
            {
                tmp[i].finish(reject);
            }
            return tmp.length;
        }

        /** Registers a new tween to be ticked. */
        protected static registerTween(tween: Tween<any>): void
        {
            if (this._iterating)
            {
                this._pendingAddTweens.push(tween);
            }
            else
            {
                this._allTweens.push(tween);
            }

        }

        /** Unregisters a tween. */
        protected static unregisterTween(tween: Tween<any>): void
        {
            let idx = Tween._allTweens.indexOf(tween);
            if (idx !== -1)
            {
                if (this._iterating)
                {
                    Tween._allTweens[idx] = null;
                    this._removedWhileIterating = true;
                }
                else
                {
                    Tween._allTweens.splice(idx, 1);
                }
            }
            idx = Tween._pendingAddTweens.indexOf(tween);
            if (idx !== -1)
            {
                Tween._pendingAddTweens.splice(idx, 1);
            }
        }

        /** Handles tick event. */
        protected static tick(ev: Ticker.Event): void
        {
            this._iterating = true;
            for (let i = 0; i < this._allTweens.length; i++)
            {
                const tween = this._allTweens[i];
                if (tween != null && (!ev.paused || tween.ignoreGlobalPause))
                {
                    try
                    {
                        tween.tick(tween.realTime ? ev.realDelta : ev.delta);
                    }
                    catch (err)
                    {
                        // Report and isolate
                        Log.error(`TweenError`, err);
                        this._removedWhileIterating = true;
                        this._allTweens[i] = null;
                    }
                }
            }
            this._iterating = false;
            if (this._removedWhileIterating)
            {
                for (let i = this._allTweens.length - 1; i >= 0; i--)
                {
                    if (this._allTweens[i] == null)
                    {
                        this._allTweens.splice(i, 1);
                    }
                }
            }
            for (let i = 0; i < this._pendingAddTweens.length; i++)
            {
                this._allTweens.push(this._pendingAddTweens[i]);
            }
            this._pendingAddTweens.length = 0;
        }

        /** Initialises the tween logic. */
        @RS.Init()
        private static init(): void
        {
            ITicker.get().add(this.tick.bind(this), { kind: Ticker.Kind.Always });
        }

        // #endregion

        // #region Operators

        /**
         * Interpolates to the specified properties over a duration, using an ease.
         */
        public abstract to(props: TweenProps<T>, duration: number, ease?: EaseFunction): this;

        /**
         * Interpolates from the specified properties back to the starting properties over a duration, using an ease.
        **/
        public abstract from(props: TweenProps<T>, duration: number, ease?: EaseFunction): this;

        /**
         * Sets the specified properties without interpolation or delay.
         */
        public abstract set(props: TweenProps<T>): this;

        /**
         * Waits for the specified duration.
         */
        public abstract wait(duration: number): this;

        /**
         * Calls the specified function.
         */
        public abstract call(func: (t: Tween<T>) => void): this;

        // #endregion

        // #region Flow Control

        /**
         * Stops this tween immediately.
         * Will NOT advance the tween - e.g. any pending actions will NOT be executed.
         * @param reject    If true, rejects all pending promise callbacks.
         */
        @CheckDisposed
        public finish(reject: boolean = false): void
        {
            if (this._dirty) { this.refresh(); }
            if (this._finished) { return; }
            this._position = this._duration;
            this._finished = true;
            if (!this._manualTick) { Tween.unregisterTween(this); }
            reject ? this.reject(new Tween.FinishedRejection()) : this.resolve();
        }

        /**
         * Sets the new position of this tween in ms.
         * Executes all actions between current and new positions.
         * @param canMoveBack   If true and position < current position, will reverse back through actions, otherwise will wrap around the end
         * @returns             True if the tween finished by this call.
         */
        public abstract setPosition(position: number, mode?: StepBehaviour): boolean;

        /**
         * Advances the tween by the specified amount.
         * @param totalDelta    How much to advance by. May be negative.
         * @returns             True if the tween finished by this call.
         */
        public abstract advance(totalDelta: number): boolean;

        // #endregion

        /**
         * Disposes of the tween.
         * @public
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }

            switch (this._disposeBehaviour)
            {
                case Tween.DisposeBehaviour.Skip:
                    this.setPosition(this.duration, StepBehaviour.SkipAfter);
                    break;

                case Tween.DisposeBehaviour.Reset:
                    this.setPosition(0, StepBehaviour.Skip);
                    break;
            }

            this._isDisposed = true;
            if (!this._finished)
            {
                if (!this._manualTick)
                {
                    Tween.unregisterTween(this);
                }

                this.reject(new Tween.DisposedRejection());
            }
        }

        /**
         * Updates duration and times after an action change.
         */
        protected abstract refresh(): void;

        /**
         * Advances the position of this tween by the specified time step.
         * @param timeStep  Time step (ms).
         */
        protected tick(timeStep: number): void
        {
            if (this._paused || this._finished) { return; }
            if (this._dirty) { this.refresh(); }
            this.advance(timeStep * this._timeScale);
        }
    }

    export namespace Tween
    {
        export let defaultEase = Ease.linear;

        export enum DisposeBehaviour { None, Reset, Skip }

        export class RejectionError extends RS.BaseError {}
        export class FinishedRejection extends RejectionError
        {
            constructor() { super("Tween was finished with rejection."); }
        }

        export class DisposedRejection extends RejectionError
        {
            constructor() { super("Tween was disposed."); }
        }

        const defaultSettings: TweenSettings = {};

        /**
         * Creates a new tween with the specified target array.
         * @returns     The new tween object.
        **/
        export function get<T extends SingularObject>(target: ReadonlyArray<T>, settings?: TweenSettings): MultiTween<T>;

        /**
         * Creates a new tween with the specified target.
         * @returns     The new tween object.
        **/
        export function get<T extends SingularObject>(target: T, settings?: TweenSettings): SingleTween<T>;

        /**
         * Creates a new tween with the specified target or target array.
         * @returns     The new tween object.
         */
        export function get<T extends SingularObject>(target: T | ReadonlyArray<T>, settings?: TweenSettings): Tween<T>;

        export function get<T extends SingularObject>(target: T | ReadonlyArray<T>, settings: TweenSettings = defaultSettings): Tween<T>
        {
            let tween: Tween<T>;
            if (Is.readonlyArray(target))
            {
                if (settings.override) { for (let i = 0; i < target.length; i++) { removeTweens(target[i]); } }
                tween = new MultiTween(target, settings.manualTick);
            }
            else
            {
                if (settings.override) { removeTweens(target); }
                tween = new SingleTween(target, settings.manualTick);
            }
            if (settings.ignoreGlobalPause != null) { tween.ignoreGlobalPause = settings.ignoreGlobalPause; }
            if (settings.realTime != null) { tween.realTime = settings.realTime; }
            if (settings.loop != null) { tween.loop = settings.loop; }
            if (settings.disposeBehaviour != null) { tween.disposeBehaviour = settings.disposeBehaviour; }
            if (settings.onChange != null)
            {
                tween.onPositionChanged(settings.onChange);
            }
            if (settings.paused) { tween.paused = true; }
            return tween;
        }

        export interface MotionSegment<T extends SingularObject>
        {
            props?: RS.TweenProps<T>;
            time: number;
            ease?: RS.EaseFunction;
        }

        export type MotionSequence<T extends SingularObject> = OneOrMany<MotionSegment<T>>

        function chainTween<T extends SingularObject>(t: RS.Tween<T>, {props, time, ease}: MotionSegment<T>)
        {
            if (!props && !time)
            {
                Log.warn(`[Tween.fromMotionSequence] invalid segment, props: ${props} | time: ${time} | ease: ${ease}. Skipping`);
                return;
            }

            if (props && !time) { t.set(props); }
            else if (!props && time) { t.wait(time); }
            else { t.to(props, time, ease); }
        }

        /**
         * Creates a new tween from specified motion settings
         * @param settings
         * @returns created tween
         */
        export function fromMotionSequence<T extends SingularObject>(target: T, segments: MotionSequence<T>): Tween<T>
        {
            const tween = get(target);
            if (Is.array(segments))
            {
                for (const segment of segments)
                {
                    chainTween(tween, segment);
                }
            }
            else
            {
                chainTween(tween, segments);
            }
            return tween;
        }

        /**
         * Shorthand for Tween.get(target || Tween).wait(duration).
         * @returns     The new tween object.
        **/
        export function wait<T extends SingularObject>(duration: number, target?: TweenTarget<T>, settings?: TweenSettings): Tween<T>
        {
            return Tween.get<T>(target || this, settings).wait(duration);
        }

        /**
         * Removes all tweens on the specified target.
         * @param target    Target object
         * @param reject    Whether to reject any promise callbacks on the tweens
         * @returns         Number of tweens removed.
        **/
        export function removeTweens<T extends SingularObject>(target: TweenTarget<T>, reject?: boolean): number
        {
            if (target._activeTweens == null) { return 0; }
            const tmp = [...target._activeTweens];
            for (const tween of tmp)
            {
                if (tween != null && !tween.isDisposed)
                {
                    tween.finish(reject);
                    tween.dispose();
                }
            }
            tmp.length = 0;
            target._activeTweens.length = 0;
            target._activeTweens = null;
            return tmp.length;
        }

        /**
         * Gets an array of all tweens on the specified target.
         * @param target    Target object
         * @param reject    Whether to reject any promise callbacks on the tweens
         * @returns         Number of tweens removed.
        **/
        export function getTweens<T extends SingularObject>(target: TweenTarget<T>, reject?: boolean): Tween<T>[]
        {
            if (target._activeTweens == null) { return []; }
            return [...target._activeTweens];
        }

        /**
         * Gets if the specified target has any active tweens.
         * @param target    Target object
        **/
        export function hasActiveTweens<T extends SingularObject>(target: TweenTarget<T>): boolean
        {
            return target._activeTweens != null ? target._activeTweens.length > 0 : false;
        }

        function tweenFinisher(obj: any): void
        {
            if (!(obj instanceof RS.Tween)) { return; }
            obj.finish();
        }
        addDestroyHandler(tweenFinisher);
    }
}
