/// <reference path="Utils.ts" />

namespace RS
{
    export type Positionable = { x: number, y: number, rotation?: number };

    const tmpSamplePoint: Bezier.Path.SamplePoint = { x: 0, y: 0, localT: 0, pathT: 0, heading: 0, curve: null };

    /**
     * A tween wrapper to use bezier paths to interpolate the values.
     */
    export class BezierTween<Target extends Positionable> implements IDisposable
    {
        public readonly onComplete = createSimpleEvent();

        protected readonly _targets: Target[];
        protected _isDisposed = false;

        @AutoDisposeOnSet protected readonly _path: Bezier.Path;
        @AutoDisposeOnSet protected _tween: Tween<any>;
        protected _currentPoint: number;
        protected _accumulatedLength: number;

        protected _timeScale: number;
        public get timeScale() { return this._timeScale; }
        public set timeScale(value)
        {
            this._timeScale = value;
            if (this._tween != null)
            {
                this._tween.timeScale = this._timeScale;
            }
        }

        public get isDisposed() { return this._isDisposed; }

        constructor(target: OneOrMany<Target>, public readonly settings: BezierTween.Settings<Target>)
        {
            this._timeScale = settings.timeScale == null ? 1 : settings.timeScale;
            this._targets = Is.array(target) ? target : [target];
            if (settings.points instanceof Bezier.Path)
            {
                this._path = settings.points;
            }
            else if (Is.arrayOf(settings.points, Is.vector2D))
            {
                this._path = Bezier.Path.fromPointsCubic(settings.points);
            }
            else
            {
                this._path = new Bezier.Path(settings.points.map((arr) => new Bezier.Curve(arr)));
            }
            this.reset();
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }

        /** Finishes the tween immediately */
        public finish(): void
        {
            this._tween.finish();
        }

        /**
         * Resets the bezier tween values.
        **/
        public reset(): void
        {
            this._currentPoint = 0;
            this._accumulatedLength = 0;
        }

        /**
         * Tweens to the next bezier point of the path.
        **/
        public async next(duration: number, ease?: EaseFunction)
        {
            if (this._currentPoint === this._path.curves.length)
            {
                if (this.settings.loop)
                {
                    this.reset();
                }
                else
                {
                    this.forceLastPoint(this._accumulatedLength);
                    return null;
                }
            }
            const interpolator: any = { value: 0 };
            const currentSegmentLength: number = this._path.curves[this._currentPoint].length;
            this._tween = RS.Tween.get(interpolator,
            {
                ...this.settings as TweenSettings,
                onChange: () =>
                {
                    this.update(this._accumulatedLength + currentSegmentLength * interpolator.value);
                    if (this.settings.onChange != null)
                    {
                        this.settings.onChange();
                    }
                }
            })
            .to({ value: 1 }, duration, ease);

            this._tween.timeScale = this._timeScale;
            await this._tween;

            this._currentPoint++;
            this._accumulatedLength += currentSegmentLength;
        }

        /**
         * Tweens to a specific amount of points of the path.
        **/
        public async advance(count: number, duration: number, ease?: EaseFunction)
        {
            for (let i = 0; i < count; i++)
            {
                // Catches console errors if bezier tween gets disposed
                try
                {
                    await this.next(duration, ease);
                }
                catch (er)
                {
                    if (er instanceof Tween.DisposedRejection)
                    {
                        break;
                    }
                    throw er;
                }
            }
        }

        /**
         * Tweens to the last bezier point of the path.
        **/
        public async end(duration: number, ease?: EaseFunction)
        {
            const totalLength: number = this._path.length;
            const interpolator: any = { value: this._accumulatedLength / totalLength };
            const doLoop: boolean = this.settings.loop == null ? false : this.settings.loop;
            this._tween = Tween.get(interpolator,
            {
                ...this.settings as TweenSettings,
                loop: doLoop,
                onChange: () =>
                {
                    this.update(totalLength * interpolator.value);
                    if (this.settings.onChange != null)
                    {
                        this.settings.onChange();
                    }
                }
            })
            .to({ value: 1 }, duration, ease);

            await this._tween;
            if (this._isDisposed) { return; }
            this.forceLastPoint(totalLength);
        }

        protected update(value: number): void
        {
            const newPoint = this._path.sample(value, tmpSamplePoint);
            for (const target of this._targets)
            {
                target.x = newPoint.x;
                target.y = newPoint.y;
                if (this.settings.rotateAlongPath)
                {
                    target.rotation = -newPoint.heading;
                }
            }
        }

        protected forceLastPoint(length: number): void
        {
            this.update(length - 0.0001);
            this.onComplete.publish();
        }
    }

    export namespace BezierTween
    {
        export interface Settings<Target extends Positionable> extends TweenSettings
        {
            points: Bezier.Path | Math.Vector2D[] | Math.Vector2D[][];
            rotateAlongPath?: boolean;
            timeScale?: number;
        }
    }
}
