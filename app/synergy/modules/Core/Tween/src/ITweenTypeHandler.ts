namespace RS
{
    /**
     * Extends the tween's value interpolation capabilities.
     */
    export interface ITweenTypeHandler<T extends object = object>
    {
        /** Gets if this handler can handle the specified object. */
        canHandle(obj: any): obj is T;

        /** Clones the specified object. */
        clone(original: T): T;

        /** Creates a new object, interpolated between a and b by mu. */
        interpolate(a: T, b: T, mu: number): T;

        /** Copies properties of src into dst. */
        apply(src: T, dst: object, dstKey: string): void;
    }

    /**
     * Registers a tween type handler with the system.
     */
    export const TweenTypeHandler = Decorators.Tag.createDataless(Decorators.TagKind.Class);

}