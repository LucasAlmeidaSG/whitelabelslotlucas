namespace RS
{
    /**
     * Settings for a timeline object.
     */
    export interface TimelineSettings
    {
        loop?: boolean;
        ignoreGlobalPause?: boolean;
        /** Whether or not to reset the target to its original state on disposal. */
        disposeBehaviour?: Tween.DisposeBehaviour;
        realTime?: boolean;
        onChange?: () => void;
        manualTick?: boolean;
        /** Whether or not to start paused. */
        paused?: boolean;
    }

    /**
     * Encapsulates a set of tweens.
     */
    @HasCallbacks
    export class Timeline extends Awaitable<void> implements IDisposable
    {
        /** Fired when the tween is advanced. */
        @RS.AutoDisposeOnSet public readonly onPositionChanged = createSimpleEvent();
        protected _isDisposed = false;
        @RS.AutoDisposeOnSet protected _tweens: Tween<any>[];
        protected _position: number = 0.0;
        protected _paused: boolean = false;
        protected _loop: boolean = false;
        protected _finished: boolean = false;
        protected _ignoreGlobalPause: boolean = false;
        @RS.AutoDisposeOnSet protected _tickerHandle: IDisposable;
        protected _timeScale: number = 1.0;
        protected _disposeBehaviour: Tween.DisposeBehaviour = Tween.DisposeBehaviour.None;
        protected _realTime: boolean = false;
        protected _manualTick: boolean;

        public get manualTick() { return this._manualTick; }
        public set manualTick(value)
        {
            if (this._manualTick === value) { return; }
            this._manualTick = value;
            if (value)
            {
                this._tickerHandle = null;
            }
            else
            {
                this._tickerHandle = ITicker.get().add(this.tick, { kind: Ticker.Kind.Always });
            }
        }

        public get isDisposed()
        {
            return this._isDisposed;
        }

        /** Gets the duration of this timeline in ms. */
        public get duration()
        {
            return this._tweens
                .map((t) => t.duration)
                .reduce((a, b) => Math.max(a, b), 0);
        }

        /** Gets or sets the position of this timeline (ms). */
        public get position() { return this._position; }
        public set position(value) { this.setPosition(value); }

        /** Gets or sets if this timeline should ignore global pause. */
        public get ignoreGlobalPause() { return this._ignoreGlobalPause; }
        public set ignoreGlobalPause(value) { this._ignoreGlobalPause = value; }

        /** Gets or sets if this tween should ignore global time scale. */
        public get realTime() { return this._realTime; }
        public set realTime(value) { this._realTime = value; }

        /** Gets or sets if this timeline should loop when complete. */
        public get loop() { return this._loop; }
        public set loop(value) { this._loop = value; }

        /** Gets or sets the time scale of this timeline. */
        public get timeScale() { return this._timeScale; }
        public set timeScale(value) { this._timeScale = value; }

        /** Gets or sets if this timeline is paused. */
        public get paused() { return this._paused; }
        public set paused(value) { this._paused = value; }

        /** Gets if this timeline has finished. */
        public get finished() { return this._finished; }

        /** Whether or not to reset the target to its original state on disposal. */
        public get disposeBehaviour() { return this._disposeBehaviour; }
        public set disposeBehaviour(value) { this._disposeBehaviour = value; }

        /**
         * Initialises a new instance of the Timeline class.
         * @param tweens    Target tweens.
         */
        public constructor(tweens: Tween<any>[], manualTick: boolean = false)
        {
            super();

            this._tweens = tweens;
            for (const tween of tweens)
            {
                tween.manualTick = true;
                tween.setPosition(this._position, StepBehaviour.Skip);
            }
            this.manualTick = manualTick;
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }
            // TODO: uncomment when safe
            // if (!this._finished)
            // {
            //     this.reject(new Tween.DisposedRejection());
            // }
            switch (this._disposeBehaviour)
            {
                case Tween.DisposeBehaviour.Skip:
                    this.setPosition(this.duration, StepBehaviour.SkipAfter);
                    break;

                case Tween.DisposeBehaviour.Reset:
                    this.setPosition(0, StepBehaviour.Skip);
                    break;
            }

            this._isDisposed = true;
        }

        // #region Flow Control

        /**
         * Sets the new position of this timeline in ms.
         * Executes all actions between current and new positions.
         * Does NOT publish onPositionChanged.
         * @returns     True if the timeline finished by this call.
         */
        public setPosition(position: number, mode: StepBehaviour = StepBehaviour.ForwardsOnly): boolean
        {
            if (position === this._position) { return false; }
            const wasFinished = this._finished;
            for (const tween of this._tweens)
            {
                tween.setPosition(position * tween.timeScale, mode);
            }
            // Check if a tween disposed the timeline
            if (this._isDisposed) { return !wasFinished && this._finished; }
            return this.updatePosition(false);
        }

        /**
         * Advances the timeline by the specified amount.
         * @param totalDelta    How much to advance by. May be negative.
         * @returns             True if the tween finished by this call.
         */
        public advance(totalDelta: number): boolean
        {
            // Clamp delta to stop looping sub-tweens from jumping ahead.
            const maxDelta = this.loop ? Infinity : this.duration - this._position;
            const tweens = this._tweens;
            const wasFinished = this._finished;
            for (const tween of tweens)
            {
                if (!tween.finished) { tween.advance(Math.min(maxDelta, totalDelta * tween.timeScale)); }
            }

            // Check if a tween disposed the timeline
            if (this._isDisposed) { return !wasFinished && this._finished; }
            return this.updatePosition(true);
        }

        /**
         * Stops this timeline immediately.
         * Will NOT timeline the tween - e.g. any pending actions will NOT be executed.
         * @param reject    If true, rejects all pending promise callbacks.
         */
        public finish(reject: boolean = false): void
        {
            for (const tween of this._tweens)
            {
                tween.finish(reject);
            }
            this._position = this.duration;
            this._finished = true;
            reject ? this.reject(new Tween.FinishedRejection()) : this.resolve();
            this._tickerHandle = null;
        }

        // #endregion

        @Callback
        protected tick(ev: Ticker.Event)
        {
            if (ev.paused && !this._ignoreGlobalPause) { return; }
            if (this._paused || this._finished) { return; }
            this.advance((this._realTime ? ev.realDelta : ev.delta) * this._timeScale);
        }

        /**
         * @param publishEvent Whether or not to publish onPositionChanged.
         * @returns True if the timeline finished by this call.
         */
        protected updatePosition(publishEvent: boolean): boolean
        {
            this._position = this._tweens
                .map((t) => t.position)
                .reduce((a, b) => Math.max(a, b), 0);
            if (this._position >= this.duration)
            {
                if (this.loop)
                {
                    this.setPosition(this._position % this.duration, StepBehaviour.Skip);
                    return false;
                }

                // Force tweens to final position in case of precision errors during advance().
                this._tweens.forEach((tween) => tween.setPosition(this.position * tween.timeScale, StepBehaviour.SkipAfter));
                if (publishEvent && !this._isDisposed) { this.onPositionChanged.publish(); }
                this.finish();
                return true;
            }
            else
            {
                if (publishEvent) { this.onPositionChanged.publish(); }
                return false;
            }
        }
    }

    export namespace Timeline
    {
        const defaultSettings: TimelineSettings = {};

        /**
         * Creates a new timeline from the specified tweens.
         * @returns     The new tween object.
         */
        export function get(tweens: Tween<any>[], settings: TimelineSettings = defaultSettings): Timeline
        {
            const timeline = new Timeline(tweens, settings.manualTick);
            if (settings.ignoreGlobalPause != null) { timeline.ignoreGlobalPause = settings.ignoreGlobalPause; }
            if (settings.disposeBehaviour != null) { timeline.disposeBehaviour = settings.disposeBehaviour; }
            if (settings.realTime != null) { timeline.realTime = settings.realTime; }
            if (settings.loop != null) { timeline.loop = settings.loop; }
            if (settings.onChange != null)
            {
                timeline.onPositionChanged(settings.onChange);
            }
            if (settings.paused) { timeline.paused = true; }
            return timeline;
        }

        function timelineFinisher(obj: any): void
        {
            if (!(obj instanceof RS.Timeline)) { return; }
            obj.finish();
        }
        addDestroyHandler(timelineFinisher);
    }
}
