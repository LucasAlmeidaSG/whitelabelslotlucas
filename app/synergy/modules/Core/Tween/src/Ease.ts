namespace RS
{
    export type EaseFunction = Func<number>;

    export namespace Ease
    {
        // https://github.com/CreateJS/TweenJS/blob/master/src/tweenjs/Ease.js

        export function linear(t: number): number { return t; }
        export const none: EaseFunction = linear;

        /**
         * Mimics the simple -100 to 100 easing in Adobe Flash/Animate.
         * @param amount    A value from -1 (ease in) to 1 (ease out) indicating the strength and direction of the ease.
        **/
        export function get(amount: number): EaseFunction
        {
            if (amount < -1) { amount = -1; }
            else if (amount > 1) { amount = 1; }
            return function(t)
            {
                if (amount == 0) { return t; }
                if (amount < 0) { return t * (t * -amount + 1 + amount); }
                return t * ((2 - t) * amount + (1 - amount));
            };
        }

        /**
         * Configurable exponential ease.
         * @param pow       The exponent to use (ex. 3 would return a cubic ease).
        **/
        export function getPowIn(pow: number): EaseFunction
        {
            return function(t)
            {
                return Math.pow(t, pow);
            };
        }

        /**
         * Configurable exponential ease.
         * @param pow       The exponent to use (ex. 3 would return a cubic ease).
        **/
        export function getPowOut(pow: number): EaseFunction
        {
            return function(t)
            {
                return 1 - Math.pow(1 - t, pow);
            };
        }

        /**
         * Configurable exponential ease.
         * @method getPowInOut
         * @param pow       The exponent to use (ex. 3 would return a cubic ease).
        **/
        export function getPowInOut(pow: number): EaseFunction
        {
            return function(t)
            {
                t *= 2;
                if (t < 1) { return 0.5 * Math.pow(t, pow); }
                return 1 - 0.5 * Math.abs(Math.pow(2 - t, pow));
            };
        }

        export const quadIn: EaseFunction = getPowIn(2);
        export const quadOut: EaseFunction = getPowOut(2);
        export const quadInOut: EaseFunction = getPowInOut(2);

        export const cubicIn: EaseFunction = getPowIn(3);
        export const cubicOut: EaseFunction = getPowOut(3);
        export const cubicInOut: EaseFunction = getPowInOut(3);

        export const quartIn: EaseFunction = getPowIn(4);
        export const quartOut: EaseFunction = getPowOut(4);
        export const quartInOut: EaseFunction = getPowInOut(4);

        export const quintIn: EaseFunction = getPowIn(5);
        export const quintOut: EaseFunction = getPowOut(5);
        export const quintInOut: EaseFunction = getPowInOut(5);

        export function sineIn(t: number): number
        {
            return 1 - Math.cos(t * Math.PI / 2);
        }
        export function sineOut(t: number): number
        {
            return Math.sin(t * Math.PI / 2);
        }
        export function sineInOut(t: number): number
        {
            return -0.5 * (Math.cos(Math.PI * t) - 1);
        }

        /**
         * Configurable "back in" ease.
         * @param amount    The strength of the ease.
        **/
        export function getBackIn(amount: number): EaseFunction
        {
            return function(t)
            {
                return t * t * ((amount + 1) * t - amount);
            };
        }
        export const backIn: EaseFunction = getBackIn(1.7);

        /**
         * Configurable "back out" ease.
         * @param amount    The strength of the ease.
        **/
        export function getBackOut(amount: number): EaseFunction
        {
            return function(t)
            {
                return (--t * t * ((amount + 1) * t + amount) + 1);
            };
        }
        export const backOut: EaseFunction = getBackOut(1.7);

        /**
         * Configurable "back in out" ease.
         * @param amount    The strength of the ease.
        **/
        export function getBackInOut(amount: number): EaseFunction
        {
            amount *= 1.525;
            return function(t)
            {
                t *= 2;
                if (t < 1)
                {
                    return 0.5 * (t * t * ((amount + 1) * t - amount));
                }
                return 0.5 * ((t -= 2) * t * ((amount + 1) * t + amount) + 2);
            };
        }
        export const backInOut = Ease.getBackInOut(1.7);

        export function circIn(t: number): number
        {
            return -(Math.sqrt(1 - t * t) - 1);
        }
        export function circOut(t: number): number
        {
            return Math.sqrt(1 - (--t) * t);
        }
        export function circInOut(t: number): number
        {
            t *= 2;
            if (t < 1)
            {
                return -0.5 * (Math.sqrt(1 - t * t) - 1);
            }
            return 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
        }

        export function bounceIn(t: number): number
        {
            return 1 - bounceOut(1 - t);
        }
        export function bounceOut(t: number): number
        {
            if (t < 1 / 2.75)
            {
                return (7.5625 * t * t);
            }
            else if (t < 2 / 2.75)
            {
                return (7.5625 * (t -= 1.5 / 2.75) * t + 0.75);
            }
            else if (t < 2.5/2.75)
            {
                return (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375);
            }
            else
            {
                return (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375);
            }
        }
        export function bounceInOut(t: number): number
        {
            if (t < 0.5)
            {
                return Ease.bounceIn (t * 2) * 0.5;
            }
            return Ease.bounceOut(t * 2 - 1) * 0.5 + 0.5;
        }

        /** Configurable elastic ease. */
        export function getElasticIn(amplitude: number, period: number): EaseFunction
        {
            const pi2 = Math.PI*2;
            return function(t)
            {
                if (t == 0 || t == 1)
                {
                    return t;
                }
                const s = period / pi2 * Math.asin(1 / amplitude);
                return -(amplitude * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * pi2 / period));
            };
        }
        export const elasticIn = getElasticIn(1,0.3);

        /** Configurable elastic ease. */
        export function getElasticOut(amplitude: number, period: number): EaseFunction
        {
            const pi2 = Math.PI * 2;
            return function(t)
            {
                if (t == 0 || t == 1)
                {
                    return t;
                }
                const s = period / pi2 * Math.asin(1 / amplitude);
                return (amplitude * Math.pow(2, -10 * t) * Math.sin((t - s) * pi2 / period) + 1);
            };
        }
        export const elasticOut = getElasticOut(1,0.3);

        /** Configurable elastic ease. */
        export function getElasticInOut(amplitude: number, period: number): EaseFunction
        {
            const pi2 = Math.PI*2;
            return function(t)
            {
                const s = period / pi2 * Math.asin(1 / amplitude);
                t *= 2;
                if (t < 1)
                {
                    return -0.5 * (amplitude * Math.pow(2, 10 * (t-=1)) * Math.sin((t - s) * pi2 / period));
                }
                return amplitude * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - s) * pi2 / period) * 0.5 + 1;
            };
        }
        export const elasticInOut = getElasticInOut(1, 0.3 * 1.5);

        /**
         * Gets an ease function that is linear up to n and then eases out as it approaches 1, as if to tease the final value.
        **/
        export function getTease(n: number): EaseFunction
        {
            // Determine gradient
            const g = (1.0 + n) / (n * 2.0);
            const p = (1.0 + n) / 2.0;

            // Return ease function
            const tease = this.transformEase((d) => d - (d * d * 0.5), n, p, 1.0 - n, 1.0 - n);
            return (wx: number) => wx <= n ? wx * g : tease(wx);
        }

        /**
         * Ease function that returns 0 when t < 0.5, and 1 when t >= 0.5
         */
        export function square(t: number): number
        {
            return RS.Math.round(t);
        }

        /**
         * Configurable sawtooth ease
         * @param n - number of sawteeth (basically number of linear eases compressed into 1 ease)
         */
        export function getSawtooth(n: number): EaseFunction
        {
            return function(t)
            {
                return 0.5 - (1 / RS.Math.PI) * RS.Math.atan(1 / RS.Math.tan((t * RS.Math.PI) / (1 / n)));
            };
        }
        export const sawtooth: EaseFunction = getSawtooth(2);

        // Util functions
        
        /**
         * Adds bounces of the given width and height to the given ease function.
         * The input ease is scaled to fit.
         */
        export function addBounces(ease: EaseFunction, bounces: Math.Size2D[]): EaseFunction
        {
            let bouncesWidth = 0;
            const bounceData = bounces.map((bounce) =>
            ({
                width: bounce.w,
                cumulative: (bouncesWidth += bounce.w),
                parabola: getParabolicTrajectory(bounce.w, bounce.h)
            }));
            if (bouncesWidth > 1) { throw new Error("Bounce widths cannot add up to more than 1."); }

            const firstBounceX = 1 - bouncesWidth;
            const easeScale = 1 / firstBounceX;

            return function (x)
            {
                if (x === 1) { return x; }
                if (x < firstBounceX) { return ease(x * easeScale); }
                const bounce = Find.matching(bounceData, (b) => firstBounceX + b.cumulative > x);
                const parabolaOffsetX = firstBounceX + bounce.cumulative - bounce.width;
                return 1 - bounce.parabola(x - parabolaOffsetX);
            };
        }

        /** Creates a new ease function which returns the result of the specified ease function but transformed to fit in the specified rectangle. */
        export function transformEase(func: EaseFunction, x: number = 0, y: number = 0, width: number = 1, height: number = 1): EaseFunction
        {
            return (wx: number) =>
            {
                // Take x from "world space" into "local space"
                const lx = (wx - x) / width;

                // Run through function
                const ly = func(lx);

                // Convert back to "world space";
                return (ly * height) + y;
            };
        }
        /** Returns a parabolic function that fits within the given width and height. */
        function getParabolicTrajectory(w: number, h: number)
        {
            // Offset parabola to fit within 0 -> w & 0 -> h.
            const xOffset = -w / 2, yOffset = h;
            // Scale quadratic to fit w and h.
            const xScale = 2 / w, yScale = -h;
            return function (x: number)
            {
                return yOffset + yScale * ((xScale * (x + xOffset)) ** 2);
            };
        }
    }
}
