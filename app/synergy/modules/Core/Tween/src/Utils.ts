namespace RS.TweenUtils
{
    let typeHandlers: ITweenTypeHandler[]|null = null;
    
    /**
     * Gets all available tween type handlers.
     */
    export function getTypeHandlers()
    {
        if (typeHandlers == null)
        {
            typeHandlers = TweenTypeHandler.classes
                .map((cl) => new cl() as ITweenTypeHandler);
        }
        return typeHandlers;
    }
    
    /**
     * Linearly interpolates between two values through mu.
     */
    export function interpolate(from: any, to: any, mu: number): any
    {
        if (from == null || to == null)
        {
            return mu < 0.5 ? from : to;
        }
        else if (Is.number(to))
        {
            return (to - from) * mu + from;
        }
        else if (Is.string(to))
        {
            return Util.narrowString(mu < 0.5 ? from : to);
        }
        else if (Is.object(to))
        {
            if (Object.getPrototypeOf(to) === Object.prototype)
            {
                const newObj = {};
                for (const key in to)
                {
                    newObj[key] = interpolate(from[key], to[key], mu);
                }
                return newObj;
            }
            for (const typeHandler of getTypeHandlers())
            {
                if (typeHandler.canHandle(from) && typeHandler.canHandle(to))
                {
                    return typeHandler.interpolate(from, to, mu);
                }
            }
            return null;
        }
        else
        {
            return mu < 0.5 ? from : to;
        }
    }

    /**
     * Applies the specified properties to the target.
     */
    export function applyProps<T extends object>(target: T, props: TweenProps<T>)
    {
        for (const key in props)
        {
            const prop = props[key];

            for (const typeHandler of getTypeHandlers())
            {
                if (typeHandler.canHandle(prop))
                {
                    typeHandler.apply(prop, target, key);
                    continue;
                }
            }
            if (prop == null) { continue; }
            try
            {
                target[key] = prop;
            }
            catch (err)
            {
                Log.error(err);
            }
        }
    }

    const captureIgnoreKeys =
    {
        "__proto__": true
    };

    /**
     * Creates a deep snapshot of the specified value.
     */
    export function capture<T>(obj: T): T
    {
        if (obj == null)
        {
            return null;
        }
        else if (Is.nonPrimitive(obj))
        {
            if (Object.getPrototypeOf(obj) === Object.prototype)
            {
                // Simple object, clone all keys
                const newObj = {} as T&object;
                for (const key in obj)
                {
                    if (!captureIgnoreKeys[key as string])
                    {
                        const orig = obj[key];
                        newObj[key] = capture(orig);
                    }
                }
                return newObj;
            }
            
            for (const typeHandler of getTypeHandlers())
            {
                if (typeHandler.canHandle(obj))
                {
                    return typeHandler.clone(obj) as any;
                }
            }

            return null;
        }
        else
        {
            return obj;
        }
    }
}