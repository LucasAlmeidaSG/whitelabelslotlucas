namespace RS.Tests
{
    export interface IAdvanceable<TReturnType>
    {
        advance(delta: number): TReturnType;
    }

    export function advance<TReturnType>(target: IAdvanceable<TReturnType>, time: number, targetFps: number = 60): TReturnType
    {
        const frameTime = 1000 / targetFps;
        let timePassed = 0;
        let result: TReturnType;
        while (timePassed < time)
        {
            result = target.advance(frameTime);
            timePassed += frameTime;
        }
        return result;
    }
}