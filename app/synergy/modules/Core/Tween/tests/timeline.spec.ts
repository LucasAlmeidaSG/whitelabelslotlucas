namespace RS.Tests
{
    const { expect } = chai;

    describe("Timeline.ts", function ()
    {
        type TweenMutator = (tween: RS.Tween<Math.Vector2D>) => void;

        const simpleMutator: TweenMutator = (tween) => tween.to({ x: 100 }, 500);
        const simpleSetterMutator: TweenMutator = (tween) => tween.set({ x: 50, y: 20 }).to({ x: 100 }, 500);
        const complexMutator: TweenMutator = (tween) => tween
            .set({ x: 50, y: 20 })
            .to({ x: 500 }, 500)
            .set({ x: 318, y: 210 })
            .to({ x: -20, y: -55 }, 500)
            .set({ x: 999, y: -999 });

        describe("finish", function ()
        {
            let timeline: RS.Timeline;
            beforeEach(function ()
            {
                const object = { x: 0, y: 0 };
                const tween = RS.Tween.get(object).to({ x: 20, y: 20 }, 500);
                timeline = RS.Timeline.get([tween]);
            });

            it("should resolve if reject is false or unspecified", function ()
            {
                let resolved = false;
                let rejected = false;

                timeline.then(() => resolved = true, (reason) => rejected = true);
                timeline.finish();

                expect(resolved, "resolved").to.equal(true);
                expect(rejected, "not rejected").to.equal(false);
            });

            it("should reject with a FinishedRejection reason if reject is true", function ()
            {
                let resolved = false;
                let rejectionReason: any;

                timeline.then(() => resolved = true, (reason) => rejectionReason = reason);
                timeline.finish(true);

                expect(rejectionReason, "is FinishedRejection").to.be.an.instanceOf(RS.Tween.FinishedRejection);
                expect(resolved, "not resolved").to.equal(false);
            });
        });

        describe("dispose", function ()
        {
            let timeline: RS.Timeline;
            beforeEach(function ()
            {
                const object = { x: 0, y: 0 };
                const tween = RS.Tween.get(object).to({ x: 20, y: 20 }, 500);
                timeline = RS.Timeline.get([tween]);
            });

            /* Temporarily disabled (see TODO in Timeline.dispose)
            it("should reject with a DisposeRejection reason", function ()
            {
                let resolved = false;
                let rejectionReason: any;

                timeline.then(() => resolved = true, (reason) => rejectionReason = reason);
                timeline.dispose();

                expect(rejectionReason, "is DisposedRejection").to.be.an.instanceOf(RS.Tween.DisposedRejection);
                expect(resolved, "not resolved").to.equal(false);
            });

            it("should reject any awaiters if the timeline has not finished", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                timeline.then(() => resolveCount++, () => rejectCount++);
                timeline.dispose();

                expect(rejectCount, "rejected").to.equal(1);
                expect(resolveCount, "not resolved").to.equal(0);

                timeline.then(() => resolveCount++, () => rejectCount++);
                expect(rejectCount, "rejected immediately").to.equal(2);
                expect(resolveCount, "not resolved immediately").to.equal(0);
            });

            it("should not reject any awaiters if the timeline has finished", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                timeline.finish();
                expect(timeline.finished, "timeline finished by finish() call").to.equal(true);

                timeline.then(() => resolveCount++, () => rejectCount++);
                timeline.dispose();

                expect(resolveCount, "resolved").to.equal(1);
                expect(rejectCount, "not rejected").to.equal(0);

                timeline.then(() => resolveCount++, () => rejectCount++);
                expect(resolveCount, "resolved immediately").to.equal(2);
                expect(rejectCount, "not rejected immediately").to.equal(0);
            });
            */
        });

        describe("setPosition", function ()
        {
            function doTest(name: string, mode: RS.StepBehaviour, mutator: (tween: RS.Tween<RS.Math.Vector2D>) => void, states: (RS.Math.Vector2D & { p: number, f?: boolean })[])
            {
                const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];

                const tweens = objects.map((obj) => RS.Tween.get<RS.Math.Vector2D>(obj));
                tweens.forEach((tween) => mutator(tween));
                const timeline = RS.Timeline.get(tweens);

                for (const state of states)
                {
                    const finished = timeline.setPosition(state.p, mode);
                    const progress = state.p / timeline.duration;
                    for (const object of objects)
                    {
                        expect(object.x, name + ` (time: ${state.p}, progress: ${progress}) (x)`).to.equal(state.x);
                        expect(object.y, name + ` (time: ${state.p}, progress: ${progress}) (y)`).to.equal(state.y);
                    }

                    // TODO: uncomment and fix in a breaking build.
                    // const shouldFinish = state.f || false;
                    // expect(finished, `${name} (time: ${state.p}, progress: ${progress}) (${shouldFinish ? "" : "not "}finished)`).to.equal(state.f || false);
                }

                timeline.dispose();
            }

            it("should be able to set the target to its exact state immediately before any arbitrary point", function ()
            {
                doTest("simple", StepBehaviour.Skip, simpleMutator,
                [
                    { p: 500, x: 100, y: 0 }, // final
                    { p: 0, x: 0, y: 0 },     // back to initial
                    { p: 250, x: 50, y: 0 }   // halfway
                ]);

                doTest("simple with immediate setter", StepBehaviour.Skip, simpleSetterMutator,
                [
                    { p: 500, x: 100, y: 20 }, // final
                    { p: 0, x: 0, y: 0 },      // back to initial
                    { p: 250, x: 75, y: 20 },  // halfway
                    { p: 1, x: 50.1, y: 20 }   // immediately after set
                ]);

                doTest("complex tween with setter at start, middle and end", StepBehaviour.Skip, complexMutator,
                [
                    { p: 1000, x: -20, y: -55 },
                    { p: 0, x: 0, y: 0 }
                ]);
            });

            it("should be able to set the target to its exact state immediately after any arbitrary point", function ()
            {
                doTest("simple", StepBehaviour.SkipAfter, simpleMutator,
                [
                    { p: 500, x: 100, y: 0, f: true }, // final
                    { p: 0, x: 0, y: 0 },     // back to initial
                    { p: 250, x: 50, y: 0 }   // halfway
                ]);

                doTest("simple with immediate setter", StepBehaviour.SkipAfter, simpleSetterMutator,
                [
                    { p: 500, x: 100, y: 20, f: true }, // final
                    { p: 0, x: 50, y: 20 },      // back to initial
                    { p: 250, x: 75, y: 20 },  // halfway
                    { p: 1, x: 50.1, y: 20 }   // immediately after set
                ]);

                doTest("complex tween with setter at start middle and end", StepBehaviour.SkipAfter, complexMutator,
                [
                    { p: 1000, x: 999, y: -999, f: true },
                    { p: 0, x: 50, y: 20 }
                ]);
            });

            it("should still finish correctly if a tween lead to the disposal of the timeline", function ()
            {
                const time = 500;

                let timeline: Timeline;
                const tween = Tween.wait(time).call(() => { timeline.dispose(); });
                timeline = Timeline.get([ tween ], { manualTick: true });

                /* tslint:disable-next-line:no-unused-expression */
                expect(function () { timeline.setPosition(time, RS.StepBehaviour.SkipAfter); }).not.to.throw;
            });
        });

        describe("disposeBehaviour", function ()
        {
            describe("DisposeBehaviour.Reset", function ()
            {
                function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void)
                {
                    const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
                    
                    const tweens = objects.map((obj) => RS.Tween.get<RS.Math.Vector2D>(obj));
                    tweens.forEach((tween) => mutator(tween));
                    const timeline = RS.Timeline.get(tweens, { disposeBehaviour: Tween.DisposeBehaviour.Reset });

                    timeline.setPosition(timeline.duration, StepBehaviour.SkipAfter);
                    timeline.dispose();

                    for (const object of objects)
                    {
                        expect(object.x, name + " (x)").to.equal(0);
                        expect(object.y, name + " (y)").to.equal(0);
                    }
                }

                it("should cause the tween to reset the target to their initial properties when disposed", function ()
                {
                    doTest("simple", simpleMutator);
                    doTest("simple with immediate setter", simpleSetterMutator);
                    doTest("complex tween with setter at start, middle and end", complexMutator);
                });
            });

            describe("DisposeBehaviour.Skip", function ()
            {
                function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void, finalState: Math.Vector2D)
                {
                    const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];

                    const tweens = objects.map((obj) => RS.Tween.get<RS.Math.Vector2D>(obj));
                    tweens.forEach((tween) => mutator(tween));
                    const timeline = RS.Timeline.get(tweens, { disposeBehaviour: Tween.DisposeBehaviour.Skip });

                    timeline.setPosition(timeline.duration, StepBehaviour.SkipAfter);
                    timeline.dispose();

                    for (const object of objects)
                    {
                        expect(object.x, name + " (x)").to.equal(finalState.x);
                        expect(object.y, name + " (y)").to.equal(finalState.y);
                    }
                }

                it("should cause the tween to reset the target to their final properties when disposed", function ()
                {
                    doTest("simple", simpleMutator, { x: 100, y: 0 });
                    doTest("simple with immediate setter", simpleSetterMutator, { x: 100, y: 20 });
                    doTest("complex tween with setter at start, middle and end", complexMutator, { x: 999, y: -999 });
                });
            });
        });

        describe("advance", function ()
        {
            it("should invoke instantaneous actions at the very end of tweens", function ()
            {
                const time = 500;
                
                let invoked = 0;
                const tween = Tween.wait(time)
                    .call(() => { invoked++; })
                    .call(() => { invoked++; });
                const timeline = Timeline.get([ tween ], { manualTick: true });

                Tests.advance(timeline, time);
                expect(timeline.finished, "timeline finished").to.equal(true);
                expect(tween.finished, "tween finished").to.equal(true);
                expect(invoked, "action invoked").to.be.greaterThan(0);
                expect(invoked, "all actions invoked").to.equal(2);

                timeline.dispose();
            });

            it("should not throw an error if a tween lead to the disposal of the timeline", function ()
            {
                const time = 500;

                let timeline: Timeline;
                const tween = Tween.wait(time).call(() => { timeline.dispose(); });
                timeline = Timeline.get([ tween ], { manualTick: true });

                /* tslint:disable-next-line:no-unused-expression */
                expect(function () { Tests.advance(timeline, time); }).not.to.throw;
            });
        });
    });
}