namespace RS.Tests
{
    const { expect } = chai;

    describe("Ease.ts", function()
    {
        describe("square", function()
        {
            it("should return 0 if t < 0.5 and 1 if t >= 1", function()
            {
                expect(Ease.square(0), "Ease.square(0)").to.equal(0);
                expect(Ease.square(0.5), "Ease.square(0.5)").to.equal(1);
                expect(Ease.square(1), "Ease.square(1)").to.equal(1);
            });
        });

        describe("sawtooth ease", function()
        {
            it("should return correct values for 2 sawteeth", function()
            {
                expect(Ease.sawtooth(0), "Ease.sawtooth(0)").to.equal(0);
                expect(Ease.sawtooth(0.4999), "Ease.sawtooth(0.4999)").to.below(1);
                expect(Ease.sawtooth(0.5), "Ease.sawtooth(0.5)").to.equal(1);
                expect(Ease.sawtooth(0.9999), "Ease.sawtooth(0.9999)").to.below(1);
                expect(Ease.sawtooth(1), "Ease.sawtooth(1)").to.equal(1);
            });
        });
    });
}
