namespace RS.Tests
{
    const { expect } = chai;

    describe("SingleTween.ts", function ()
    {
        type TweenMutator = (tween: RS.Tween<Math.Vector2D>) => void;

        const simpleMutator: TweenMutator = (tween) => tween.to({ x: 100 }, 500);
        const simpleSetterMutator: TweenMutator = (tween) => tween.set({ x: 50, y: 20 }).to({ x: 100 }, 500);
        const complexMutator: TweenMutator = (tween) => tween
            .set({ x: 50, y: 20 })
            .to({ x: 500 }, 500)
            .set({ x: 318, y: 210 })
            .to({ x: -20, y: -55 }, 500)
            .set({ x: 999, y: -999 });

        describe("finish", function ()
        {
            let tween: RS.Tween<any>;
            beforeEach(function ()
            {
                const object = { x: 0, y: 0 };
                tween = RS.Tween.get(object).to({ x: 20, y: 20 }, 500);
            });

            it("should resolve if reject is false or unspecified", function ()
            {
                let resolved = false;
                let rejected = false;

                tween.then(() => resolved = true, (reason) => rejected = true);
                tween.finish();

                expect(resolved, "resolved").to.equal(true);
                expect(rejected, "not rejected").to.equal(false);
            });

            it("should reject with a FinishedRejection reason if reject is true", function ()
            {
                let resolved = false;
                let rejectionReason: any;

                tween.then(() => resolved = true, (reason) => rejectionReason = reason);
                tween.finish(true);

                expect(rejectionReason, "is FinishedRejection").to.be.an.instanceOf(RS.Tween.FinishedRejection);
                expect(resolved, "not resolved").to.equal(false);
            });
        });

        describe("duration", function ()
        {
            it("should be zero if there are no actions", function ()
            {
                const tween = RS.Tween.get({ x: 0, y: 0 });
                expect(tween.duration).to.equal(0);
            });

            it("should be equal to the sum of the durations of the actions", function ()
            {
                const tween = RS.Tween.get({ x: 0, y: 0 })
                    .set({ x: 5, y: 5 })
                    .to({ x: 31, y: 418 }, 100)
                    .to({ x: 20, y: 20 }, 200)
                    .to({ x: 20, y: 20 }, 300);
                expect(tween.duration).to.equal(100 + 200 + 300);
            });
        });

        describe("dispose", function ()
        {
            let tween: RS.Tween<any>;
            beforeEach(function ()
            {
                const object = { x: 0, y: 0 };
                tween = RS.Tween.get(object).to({ x: 20, y: 20 }, 500);
            });

            it("should reject with a DisposeRejection reason", function ()
            {
                let resolved = false;
                let rejectionReason: any;

                tween.then(() => resolved = true, (reason) => rejectionReason = reason);
                tween.dispose();

                expect(rejectionReason, "is DisposedRejection").to.be.an.instanceOf(RS.Tween.DisposedRejection);
                expect(resolved, "not resolved").to.equal(false);
            });

            it("should reject any awaiters if the tween has not finished", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                tween.then(() => resolveCount++, () => rejectCount++);
                tween.dispose();

                expect(rejectCount, "rejected").to.equal(1);
                expect(resolveCount, "not resolved").to.equal(0);

                tween.then(() => resolveCount++, () => rejectCount++);
                expect(rejectCount, "rejected immediately").to.equal(2);
                expect(resolveCount, "not resolved immediately").to.equal(0);
            });

            it("should not reject any awaiters if the tween has finished", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                tween.finish();
                expect(tween.finished, "tween finished by finish() call").to.equal(true);

                tween.then(() => resolveCount++, () => rejectCount++);
                tween.dispose();

                expect(resolveCount, "resolved").to.equal(1);
                expect(rejectCount, "not rejected").to.equal(0);

                tween.then(() => resolveCount++, () => rejectCount++);
                expect(resolveCount, "resolved immediately").to.equal(2);
                expect(rejectCount, "not rejected immediately").to.equal(0);
            });
        });

        describe("setPosition", function ()
        {
            function doTest(name: string, mode: RS.StepBehaviour, mutator: (tween: RS.Tween<RS.Math.Vector2D>) => void, states: (RS.Math.Vector2D & { p: number, f?: boolean })[])
            {
                const object = { x: 0, y: 0 };

                const tween = RS.Tween.get(object);
                mutator(tween);

                for (const state of states)
                {
                    const finished = tween.setPosition(state.p, mode);
                    const progress = state.p / tween.duration;
                    expect(object.x, `${name} (time: ${state.p}, progress: ${progress}) (x)`).to.equal(state.x);
                    expect(object.y, `${name} (time: ${state.p}, progress: ${progress}) (y)`).to.equal(state.y);

                    // TODO: uncomment and fix in a breaking build.
                    // const shouldFinish = state.f || false;
                    // expect(finished, `${name} (time: ${state.p}, progress: ${progress}) (${shouldFinish ? "" : "not "}finished)`).to.equal(state.f || false);
                }
                
                tween.dispose();
            }

            it("should be able to set the target to its exact state immediately before any arbitrary point", function ()
            {
                doTest("simple", StepBehaviour.Skip, simpleMutator,
                [
                    { p: 500, x: 100, y: 0 }, // final
                    { p: 0, x: 0, y: 0 },     // back to initial
                    { p: 250, x: 50, y: 0 }   // halfway
                ]);

                doTest("simple with immediate setter", StepBehaviour.Skip, simpleSetterMutator,
                [
                    { p: 500, x: 100, y: 20 }, // final
                    { p: 0, x: 0, y: 0 },      // back to initial
                    { p: 250, x: 75, y: 20 },  // halfway
                    { p: 1, x: 50.1, y: 20 }   // immediately after set
                ]);

                doTest("complex tween with setter at start middle and end", StepBehaviour.Skip, complexMutator,
                [
                    { p: 1000, x: -20, y: -55 },
                    { p: 0, x: 0, y: 0 }
                ]);
            });

            it("should be able to set the target to its exact state immediately after any arbitrary point", function ()
            {
                doTest("simple", StepBehaviour.SkipAfter, simpleMutator,
                [
                    { p: 500, x: 100, y: 0, f: true }, // final
                    { p: 0, x: 0, y: 0 },     // back to initial
                    { p: 250, x: 50, y: 0 }   // halfway
                ]);

                doTest("simple with immediate setter", StepBehaviour.SkipAfter, simpleSetterMutator,
                [
                    { p: 500, x: 100, y: 20, f: true }, // final
                    { p: 0, x: 50, y: 20 },      // back to initial
                    { p: 250, x: 75, y: 20 },  // halfway
                    { p: 1, x: 50.1, y: 20 }   // immediately after set
                ]);

                doTest("complex tween with setter at start middle and end", StepBehaviour.SkipAfter, complexMutator,
                [
                    { p: 1000, x: 999, y: -999, f: true },
                    { p: 0, x: 50, y: 20 }
                ]);
            });

            it("should resolve any awaiters if used to move the tween to its end duration", function ()
            {
                const object = { x: 0, y: 0 };
                const tween = RS.Tween.get(object).to({ x: 20, y: 20 }, 200);

                let resolveCount = 0;
                tween.then(() => resolveCount++);

                tween.setPosition(200, StepBehaviour.SkipAfter);
                expect(resolveCount, "resolved on finish").to.equal(1);
            });

            it("should reset the tween awaitable state if used to move a finished tween backwards", function ()
            {
                const object = { x: 0, y: 0 };
                const tween = RS.Tween.get(object).to({ x: 20, y: 20 }, 200);

                let resolveCount = 0;

                tween.then(() => resolveCount++);
                tween.setPosition(200, StepBehaviour.SkipAfter);

                tween.setPosition(150, StepBehaviour.Skip);
                tween.then(() => resolveCount++);

                expect(resolveCount, "finished tween no longer resolves after being skipped backwards").to.equal(1);
            });
        });

        describe("advance", function ()
        {
            function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void, states: (RS.Math.Vector2D & { d: number; f?: boolean; })[])
            {
                const object = { x: 0, y: 0 };

                const tween = RS.Tween.get(object);
                mutator(tween);

                for (const state of states)
                {
                    const finished = tween.advance(state.d);
                    expect(object.x, name + ` (delta: ${state.d}) (x)`).to.be.approximately(state.x, 0.1);
                    expect(object.y, name + ` (delta: ${state.d}) (y)`).to.be.approximately(state.y, 0.1);

                    const shouldFinish = state.f || false;
                    expect(finished, name + ` (delta: ${state.d}) (${shouldFinish ? "" : "not "}finished)`).to.equal(state.f || false);
                }
                
                tween.dispose();
            }

            it("should advance the tween state by the specified delta", function ()
            {
                doTest("simple", simpleMutator,
                [
                    {d: 0, x: 0, y: 0 },
                    {d: 200, x: 40, y: 0 },
                    {d: 100, x: 60, y: 0 },
                    {d: 200, x: 100, y: 0, f: true }
                ]);

                doTest("simple with immediate setter", simpleSetterMutator,
                [
                    {d: 0, x: 0, y: 0 },
                    // 50 -> 100 in 500 ms
                    {d: 200, x: 70, y: 20 },
                    {d: 100, x: 80, y: 20 },
                    {d: 200, x: 100, y: 20, f: true }
                ]);

                doTest("complex tween with setter at start middle and end", complexMutator,
                [
                    {d: 0, x: 0, y: 0},

                    // 50 -> 500 in 500 ms
                    {d: 200, x: 230, y: 20 },
                    // (318, 210) -> (-20, -55) in 500 ms (200ms in - 0.4 in)
                    // x = 318 + (-20 - 318) * 0.4
                    // y = 210 + (-55 - 210) * 0.4
                    {d: 500, x: 182.8, y: 104 },
                    // 0 + 200 + 500 + 1000 = 1
                    {d: 1000, x: 999, y: -999, f: true }
                ]);
            });

            it("should invoke instantaneous actions at the very end of the tween when advanced frame-by-frame", function ()
            {
                const time = 500;
                
                let invoked = 0;
                const target = {};
                const tween = Tween.wait(time, target, { manualTick: true })
                                   .call(() => { invoked++; })
                                   .call(() => { invoked++; });

                Tests.advance(tween, time);
                expect(tween.finished, "tween finished").to.equal(true);
                expect(invoked, "action invoked").to.be.greaterThan(0);
                expect(invoked, "all actions invoked").to.equal(2);

                tween.dispose();
            });

            it("should invoke instantaneous actions at the very end of the tween when advanced to the final position", function ()
            {
                const time = 500;
                
                let invoked = 0;
                const target = {};
                const tween = Tween.wait(time, target, { manualTick: true })
                                   .call(() => { invoked++; })
                                   .call(() => { invoked++; });

                tween.advance(time);
                expect(tween.finished, "tween finished").to.equal(true);
                expect(invoked, "action invoked").to.be.greaterThan(0);
                expect(invoked, "all actions invoked").to.equal(2);

                tween.dispose();
            });

            it("should invoke instantaneous actions at the precise moment the tween is advanced to", function ()
            {
                const time = 500;
                
                let invoked = 0;
                const target = {};
                const tween = Tween
                    .wait(time, target, { manualTick: true })
                    .call(() => { invoked++; })
                    .call(() => { invoked++; })
                    .wait(time);

                tween.advance(time);
                expect(tween.finished, "tween not finished").to.equal(false);
                expect(invoked, "action invoked").to.be.greaterThan(0);
                expect(invoked, "all actions invoked").to.equal(2);

                tween.dispose();
            });

            it("should loop the tween if the loop property is true", function ()
            {
                const time = 1000;
                const advanceAmount = 1500;
                
                const object = { x: 0 };
                const tween = Tween.get(object, {manualTick: true, loop: true})
                    .to({x: 1}, time)

                tween.advance(advanceAmount);
                expect(tween.finished, "tween not finished").to.equal(false);
                expect(object.x, "object looped").to.equal(0.5);

                tween.dispose();
            });
        });

        describe("disposeBehaviour", function ()
        {
            describe("DisposeBehaviour.Reset", function ()
            {
                function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void)
                {
                    const object = { x: 0, y: 0 };
                    const tween = Tween.get(object, { disposeBehaviour: Tween.DisposeBehaviour.Reset });
                    mutator(tween);

                    tween.setPosition(tween.duration, StepBehaviour.SkipAfter);
                    tween.dispose();

                    expect(object.x, name + " (x)").to.equal(0);
                    expect(object.y, name + " (y)").to.equal(0);
                }

                it("should cause the tween to reset the target to their initial properties when disposed", function ()
                {
                    doTest("simple", simpleMutator);
                    doTest("simple with immediate setter", simpleSetterMutator);
                    doTest("complex tween with setter at start, middle and end", complexMutator);
                });
            });

            describe("DisposeBehaviour.Skip", function ()
            {
                function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void, finalState: Math.Vector2D)
                {
                    const object = { x: 0, y: 0 };
                    const tween = Tween.get(object, { disposeBehaviour: Tween.DisposeBehaviour.Skip });
                    mutator(tween);

                    tween.setPosition(tween.duration, StepBehaviour.SkipAfter);
                    tween.dispose();

                    expect(object.x, name + " (x)").to.equal(finalState.x);
                    expect(object.y, name + " (y)").to.equal(finalState.y);
                }

                it("should cause the tween to reset the target to their final properties when disposed", function ()
                {
                    doTest("simple", simpleMutator, { x: 100, y: 0 });
                    doTest("simple with immediate setter", simpleSetterMutator, { x: 100, y: 20 });
                    doTest("complex tween with setter at start, middle and end", complexMutator, { x: 999, y: -999 });
                });
            });
        });
    });
}