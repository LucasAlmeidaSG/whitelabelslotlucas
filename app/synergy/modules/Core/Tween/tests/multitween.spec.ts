namespace RS.Tests
{
    const { expect } = chai;

    describe("MultiTween.ts", function ()
    {
        type TweenMutator = (tween: RS.Tween<Math.Vector2D>) => void;

        const simpleMutator: TweenMutator = (tween) => tween.to({ x: 100 }, 500);
        const simpleSetterMutator: TweenMutator = (tween) => tween.set({ x: 50, y: 20 }).to({ x: 100 }, 500);
        const complexMutator: TweenMutator = (tween) => tween
            .set({ x: 50, y: 20 })
            .to({ x: 500 }, 500)
            .set({ x: 318, y: 210 })
            .to({ x: -20, y: -55 }, 500)
            .set({ x: 999, y: -999 });

        describe("finish", function ()
        {
            let tween: RS.MultiTween<RS.Math.Vector2D>;
            beforeEach(function ()
            {
                const object = { x: 0, y: 0 };
                tween = RS.Tween.get([object]).to({ x: 20, y: 20 }, 500);
            });

            it("should resolve if reject is false or unspecified", function ()
            {
                let resolved = false;
                let rejected = false;

                tween.then(() => resolved = true, (reason) => rejected = true);
                tween.finish();

                expect(resolved, "resolved").to.equal(true);
                expect(rejected, "not rejected").to.equal(false);
            });

            it("should reject with a FinishedRejection reason if reject is true", function ()
            {
                let resolved = false;
                let rejectionReason: any;

                tween.then(() => resolved = true, (reason) => rejectionReason = reason);
                tween.finish(true);

                expect(rejectionReason, "is FinishedRejection").to.be.an.instanceOf(RS.Tween.FinishedRejection);
                expect(resolved, "not resolved").to.equal(false);
            });
        });

        describe("duration", function ()
        {
            it("should be zero if there are no targets", function ()
            {
                const tween = RS.Tween.get([]);
                expect(tween.duration).to.equal(0);
            });

            it("should be zero if there are no actions", function ()
            {
                const tween = RS.Tween.get([ { x: 0, y: 0 }, { x: 5, y: 5 } ]);
                expect(tween.duration).to.equal(0);
            });

            it("should be equal to the sum of the durations of the actions", function ()
            {
                const tween = RS.Tween.get([ { x: 0, y: 0 }, { x: 5, y: 5 } ])
                    .set({ x: 5, y: 5 })
                    .to({ x: 31, y: 418 }, 100)
                    .to({ x: 20, y: 20 }, 200)
                    .to({ x: 20, y: 20 }, 300);
                expect(tween.duration).to.equal(100 + 200 + 300);
            });
        });

        it("should continue to function if a child tween is disposed (e.g. via removeTweens)", function ()
        {
            const checkTarget = { x: 5, y: 5 };
            const removeTarget = { x: 5, y: 5 };
            const tween = RS.Tween.get([ checkTarget, removeTarget ])
                .to({ x: 31, y: 418 }, 1000);

            RS.Tween.removeTweens(removeTarget);
            Tests.advance(tween, 1000);

            expect(tween.position).to.equal(1000);
            expect(tween.finished, "tween should be finished").to.equal(true);
            expect(checkTarget.x, "x should be correct").to.equal(31);
            expect(checkTarget.y, "y should be correct").to.equal(418);
        });

        describe("dispose", function ()
        {
            let tween: RS.MultiTween<RS.Math.Vector2D>;
            beforeEach(function ()
            {
                tween = RS.Tween.get([ { x: 0, y: 0 }, { x: 5, y: 5 } ])
                    .to({ x: 20, y: 20 }, 500);
            });

            it("should reject with a DisposeRejection reason", function ()
            {
                let resolved = false;
                let rejectionReason: any;

                tween.then(() => resolved = true, (reason) => rejectionReason = reason);
                tween.dispose();

                expect(rejectionReason, "is DisposedRejection").to.be.an.instanceOf(RS.Tween.DisposedRejection);
                expect(resolved, "not resolved").to.equal(false);
            });

            it("should reject any awaiters if the multi-tween has not finished", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                tween.then(() => resolveCount++, () => rejectCount++);
                tween.dispose();

                expect(rejectCount, "rejected").to.equal(1);
                expect(resolveCount, "not resolved").to.equal(0);

                tween.then(() => resolveCount++, () => rejectCount++);
                expect(rejectCount, "rejected immediately").to.equal(2);
                expect(resolveCount, "not resolved immediately").to.equal(0);
            });

            it("should not reject any awaiters if the multi-tween has finished", function ()
            {
                let resolveCount = 0;
                let rejectCount = 0;

                tween.finish();
                expect(tween.finished, "multi-tween finished by finish() call").to.equal(true);

                tween.then(() => resolveCount++, () => rejectCount++);
                tween.dispose();

                expect(resolveCount, "resolved").to.equal(1);
                expect(rejectCount, "not rejected").to.equal(0);

                tween.then(() => resolveCount++, () => rejectCount++);
                expect(resolveCount, "resolved immediately").to.equal(2);
                expect(rejectCount, "not rejected immediately").to.equal(0);
            });
        });

        describe("setPosition", function ()
        {
            function doTest(name: string, mode: RS.StepBehaviour, mutator: (tween: RS.Tween<RS.Math.Vector2D>) => void, states: (RS.Math.Vector2D & { p: number, f?: boolean })[])
            {
                const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];

                const tween = RS.Tween.get<RS.Math.Vector2D>(objects);
                mutator(tween);

                for (const state of states)
                {
                    const finished = tween.setPosition(state.p, mode);
                    const progress = state.p / tween.duration;
                    for (const object of objects)
                    {
                        expect(object.x, name + ` (time: ${state.p}, progress: ${progress}) (x)`).to.equal(state.x);
                        expect(object.y, name + ` (time: ${state.p}, progress: ${progress}) (y)`).to.equal(state.y);
                    }

                    // TODO: uncomment and fix in a breaking build.
                    // const shouldFinish = state.f || false;
                    // expect(finished, `${name} (time: ${state.p}, progress: ${progress}) (${shouldFinish ? "" : "not "}finished)`).to.equal(state.f || false);
                }
                
                tween.dispose();
            }

            it("should be able to set the target to its exact state immediately before any arbitrary point", function ()
            {
                doTest("simple", StepBehaviour.Skip, simpleMutator,
                [
                    { p: 500, x: 100, y: 0 }, // final
                    { p: 0, x: 0, y: 0 },     // back to initial
                    { p: 250, x: 50, y: 0 }   // halfway
                ]);

                doTest("simple with immediate setter", StepBehaviour.Skip, simpleSetterMutator,
                [
                    { p: 500, x: 100, y: 20 }, // final
                    { p: 0, x: 0, y: 0 },      // back to initial
                    { p: 250, x: 75, y: 20 },  // halfway
                    { p: 1, x: 50.1, y: 20 }   // immediately after set
                ]);

                doTest("complex tween with setter at start, middle and end", StepBehaviour.Skip, complexMutator,
                [
                    { p: 1000, x: -20, y: -55 },
                    { p: 0, x: 0, y: 0 }
                ]);
            });

            it("should be able to set the target to its exact state immediately after any arbitrary point", function ()
            {
                doTest("simple", StepBehaviour.SkipAfter, simpleMutator,
                [
                    { p: 500, x: 100, y: 0, f: true }, // final
                    { p: 0, x: 0, y: 0 },     // back to initial
                    { p: 250, x: 50, y: 0 }   // halfway
                ]);

                doTest("simple with immediate setter", StepBehaviour.SkipAfter, simpleSetterMutator,
                [
                    { p: 500, x: 100, y: 20, f: true }, // final
                    { p: 0, x: 50, y: 20 },      // back to initial
                    { p: 250, x: 75, y: 20 },  // halfway
                    { p: 1, x: 50.1, y: 20 }   // immediately after set
                ]);

                doTest("complex tween with setter at start middle and end", StepBehaviour.SkipAfter, complexMutator,
                [
                    { p: 1000, x: 999, y: -999, f: true },
                    { p: 0, x: 50, y: 20 }
                ]);
            });

            it("should not throw an error if an inner tween lead to the disposal of the multi-tween", function ()
            {
                const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
                const time = 500;
                const tween = Tween.get(objects)
                                   .wait(time)
                                   .call(() => { tween.dispose(); });

                /* tslint:disable-next-line:no-unused-expression */
                expect(function () { tween.setPosition(time, RS.StepBehaviour.SkipAfter); }).not.to.throw;
            });
        });

        describe("disposeBehaviour", function ()
        {
            describe("DisposeBehaviour.Reset", function ()
            {
                function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void)
                {
                    const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
                    const tween = RS.Tween.get(objects, { disposeBehaviour: Tween.DisposeBehaviour.Reset });
                    mutator(tween);

                    tween.setPosition(tween.duration, StepBehaviour.SkipAfter);
                    tween.dispose();

                    for (const object of objects)
                    {
                        expect(object.x, name + " (x)").to.equal(0);
                        expect(object.y, name + " (y)").to.equal(0);
                    }
                }

                it("should cause the tween to reset the target to their initial properties when disposed", function ()
                {
                    doTest("simple", simpleMutator);
                    doTest("simple with immediate setter", simpleSetterMutator);
                    doTest("complex tween with setter at start, middle and end", complexMutator);
                });
            });

            describe("DisposeBehaviour.Skip", function ()
            {
                function doTest(name: string, mutator: (tween: Tween<Math.Vector2D>) => void, finalState: Math.Vector2D)
                {
                    const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
                    const tween = RS.Tween.get(objects, { disposeBehaviour: Tween.DisposeBehaviour.Skip });
                    mutator(tween);

                    tween.setPosition(tween.duration, StepBehaviour.SkipAfter);
                    tween.dispose();

                    for (const object of objects)
                    {
                        expect(object.x, name + " (x)").to.equal(finalState.x);
                        expect(object.y, name + " (y)").to.equal(finalState.y);
                    }
                }

                it("should cause the tween to reset the target to their final properties when disposed", function ()
                {
                    doTest("simple", simpleMutator, { x: 100, y: 0 });
                    doTest("simple with immediate setter", simpleSetterMutator, { x: 100, y: 20 });
                    doTest("complex tween with setter at start, middle and end", complexMutator, { x: 999, y: -999 });
                });
            });
        });

        describe("advance", function ()
        {
            it("should invoke instantaneous actions at the very end of tweens", function ()
            {
                const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
                const time = 500;
                let invoked = 0;
                const tween = Tween.get(objects)
                                   .wait(time)
                                   .call(() => { invoked++; })
                                   .call(() => { invoked++; });

                const finished = Tests.advance(tween, time);
                expect(finished, "multi-tween finished").to.equal(true);
                expect(invoked, "action invoked").to.be.greaterThan(0);
                expect(invoked, "all actions invoked on all targets").to.equal(4);
            });

            it("should not throw an error if an inner tween lead to the disposal of the multi-tween", function ()
            {
                const objects = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
                const time = 500;
                const tween = Tween.get(objects)
                                   .wait(time)
                                   .call(() => { tween.dispose(); });

                /* tslint:disable-next-line:no-unused-expression */
                expect(function () { Tests.advance(tween, time); }).not.to.throw;
            });

            it("should loop all owned tweens if the loop property is true", function ()
            {
                const time = 1000;
                const advanceAmount = 1500;
                
                const objects = [{ x: 0 }, { x: 0 }];
                const tween = Tween.get(objects, {manualTick: true, loop: true})
                    .to({x: 1}, time)

                tween.advance(advanceAmount);
                expect(tween.finished, "tween not finished").to.equal(false);
                expect(objects[0].x, "object 1 looped").to.equal(0.5);
                expect(objects[1].x, "object 2 looped").to.equal(0.5);

                tween.dispose();
            });
        });
    });
}