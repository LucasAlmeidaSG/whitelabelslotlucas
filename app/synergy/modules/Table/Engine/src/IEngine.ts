namespace RS.Table.Engine
{
    /**
     * Data for a "force" or "rig".
     */
    /* tslint:disable-next-line:no-empty-interface */
    export interface ForceResult extends RS.Engine.ForceResult
    {
        cards?:
        {
            dealer: Models.Card[];
            player: OneOrMany<Models.Card[]>;
        }
    }

    /**
     * Encapsulates a slots engine interface.
     */
    /* tslint:disable-next-line:no-empty-interface */
    export interface IEngine<
        TModels extends Models,
        TSettings extends object,
        TReplayData extends object = {},
        TForceResult extends ForceResult = ForceResult
    > extends RS.Engine.IEngine<TModels, TSettings, TReplayData, TForceResult>
    {

    }

    export type EngineType<
        TModels extends Models,
        TSettings extends object,
        TReplayData extends object = {},
        TForceResult extends ForceResult = ForceResult
    > = { new(settings: TSettings, models: TModels): IEngine<TModels, TSettings, TReplayData, TForceResult>; };
}
