/// <reference path="IPlatform.ts" />

namespace RS.Table
{
    class DefaultPlatform implements IPlatform
    {
        /**
         * Gets if autoplay is supported by the platform.
         */
        public readonly supportsAutoplay = PlatformAutoplaySupport.None;

        private _settings: IPlatform.Settings;
        private _sessionInfo: RS.IPlatform.SessionInfo;

        public get sessionInfo() { return this._sessionInfo; }

        /**
         * Initialises the platform.
         * This is called when the game class has been initialised.
         */
        public init(settings: IPlatform.Settings): void
        {
            this._settings = settings;
            this._sessionInfo =
            {
                userName: null,
                userID: null,
                sessionID: null,
                affiliateID: null
            };
        }

        /**
         * Signals that a response has been received to a network request.
         * This should be used for network requests sent to the engine.
         */
        public networkResponseReceived(request: XMLHttpRequest): void { return; }

        /**
         * Requests that the platform initiates autoplay.
         * Will typically open a menu with autoplay settings.
         */
        public requestAutoplay(): void { return; }
        public requestAutoplayStop(): void { return; }
    }

    IPlatform.register(DefaultPlatform);
}
