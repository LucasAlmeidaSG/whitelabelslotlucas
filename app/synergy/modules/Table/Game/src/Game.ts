namespace RS.Table
{
    export class Game<TSettings extends Game.Settings = Game.Settings, TContext extends Game.Context = Game.Context> extends RS.Game
    {
        public readonly settings: TSettings;

        protected _arbiters: Game.Arbiters;
        /** Gets the arbiters for this game. */
        public get arbiters() { return this._arbiters; }

        protected _context: TContext;
        public get context() { return this._context; }

        protected _uiController: UI.IUIController;
        public get uiController() { return this._uiController; }

        protected _models: Table.Models;
        protected _currencyFormatter: RS.Localisation.CurrencyFormatter;

        /** Gets the models for this game. */
        public get models() { return this._models; }

        protected createUIController(): void
        {
            this._uiController = RS.Table.UI.IUIController.get();
            this._uiController.initialise({
                allowSpinArbiter: this._arbiters.canSpin,
                allowSkipArbiter: this._arbiters.canSkip,
                allowChangeBetArbiter: this._arbiters.canChangeBet,
                spinOrSkipArbiter: this._arbiters.spinOrSkip,
                enableInteractionArbiter: this._arbiters.uiEnabled,
                messageTextArbiter: this._arbiters.messageText,
                showButtonAreaArbiter: this._arbiters.showButtonArea,
                orientationObservable: this.orientation,
                ui: this.settings.uiSettings,
                showTurboArbiter: this._arbiters.showTurboUI,
                turboModeArbiter: this._arbiters.turboMode
            });
            this._uiController.onHelpRequested(this.handleHelpRequested);
            this._uiController.onPaytableRequested(this.handlePaytableRequested);
        }

        protected initialisePlatform(): void
        {
            super.initialisePlatform();
            IPlatform.get().init({
                balanceObservable: this._observables.balance,
                balancesObservable: this._observables.balances,
                winObservable: this._observables.win,
                stakeObservable: this._observables.totalBet,
                gameStateObservable: this._observables.platformGameState,

                interactionEnabledArbiter: this._arbiters.uiEnabled,
                uiContainer: this._uiContainer,

                stakeChangeEnabledArbiter: this._arbiters.canChangeBet
            });
        }

        protected createArbiters()
        {
            super.createArbiters();
            this._arbiters =
            {
                ...this._arbiters,
                shouldAutoBet: new Arbiter<boolean>(Arbiter.AndResolver, false)
            };
        }

        protected initialiseModels(): void
        {
            super.initialiseModels();
            Models.clear(this._models);
        }
    }

    export namespace Game
    {
        export interface Settings extends RS.Game.Settings
        {
            uiSettings: RS.Table.UI.AbstractUI.Settings

            engine: Engine.EngineType<Models, object>;
        }

        export interface Arbiters extends RS.Game.Arbiters
        {
            /** Indicates whether the game should spin on behalf of the user (e.g. replay). */
            shouldAutoBet: Arbiter<boolean>;
        }

        export interface Context extends RS.Game.Context
        {
            game: Game;
        }
    }
}
