namespace RS.Table.Paytable.Pages.Settings
{
    export const titleSettings = (text: string = "", font: RS.Asset.FontReference = RS.Fonts.FrutigerWorld.Black): RS.Flow.Label.Settings =>
    ({
        ...RS.Flow.Label.defaultSettings,
        dock: RS.Flow.Dock.Top,
        font: font,
        text: text,
        size: { w: 1100, h: 300 },
        fontSize: 40
    });

    export const headingSettings = (text: string = "", align: number = RS.Rendering.TextAlign.Middle, font: RS.Asset.FontReference = RS.Fonts.FrutigerWorld.Bold, fontSize: number = 26): RS.Flow.Label.Settings =>
    ({
        ...RS.Flow.Label.defaultSettings,
        dock: RS.Flow.Dock.Top,
        text: text,
        font: font,
        fontSize: fontSize,
        align: align,
        canWrap: true,
        sizeToContents: true,
        expand: RS.Flow.Expand.Allowed
    });

    export const listItemSettings = (text: string = "", align: number = RS.Rendering.TextAlign.Left, font: RS.Asset.FontReference = RS.Fonts.FrutigerWorld, fontSize: number = 28): RS.Flow.Label.Settings =>
    ({
        ...RS.Flow.Label.defaultSettings,
        dock: RS.Flow.Dock.Top,
        text: text,
        font: font,
        fontSize: fontSize,
        align: align,
        canWrap: true,
        sizeToContents: true,
        expand: RS.Flow.Expand.Allowed,
        renderSettings: { ...RS.Rendering.TextOptions.defaultCanvasRenderSettings, padding: 10 }
    });

    export const textSettings = (text: string = "", align: number = RS.Rendering.TextAlign.Middle, font: RS.Asset.FontReference = RS.Fonts.FrutigerWorld, fontSize: number = 28): RS.Flow.Label.Settings =>
    ({
        ...RS.Flow.Label.defaultSettings,
        dock: RS.Flow.Dock.Top,
        text: text,
        font: font,
        fontSize: fontSize,
        align: align,
        canWrap: true,
        sizeToContents: true,
        expand: RS.Flow.Expand.Allowed,
        renderSettings: { ...RS.Rendering.TextOptions.defaultCanvasRenderSettings, padding: 10 }
    });

    export const footerSettings = (text: string = "", align: number = RS.Rendering.TextAlign.Middle, font: RS.Asset.FontReference = RS.Fonts.FrutigerWorld.Bold, fontSize: number = 28): RS.Flow.Label.Settings =>
    ({
        ...RS.Flow.Label.defaultSettings,
        dock: RS.Flow.Dock.Top,
        font: RS.Fonts.FrutigerWorld.Black,
        text: text,
        fontSize: 24,
        align: RS.Rendering.TextAlign.Middle,
        canWrap: true,
        sizeToContents: true,
        expand: RS.Flow.Expand.Allowed,
        renderSettings: { ...RS.Rendering.TextOptions.defaultCanvasRenderSettings, padding: 10 }
    });

    export const defaultImageSettings = (settings: RS.Flow.Image.Settings): RS.Flow.Image.Settings =>
    ({
        dock: RS.Flow.Dock.Float,
        scaleMode: RS.Math.ScaleMode.Cover,
        dockAlignment: { x: 0.5, y: 0.5 },
        ...settings,
    });

    export const payoutsGridSettings: RS.Flow.Grid.Settings =
    {
        dock: 5,
        rowCount: 11,
        colCount: 2,
        order: RS.Flow.Grid.Order.ColumnFirst,
        size: { w: 1100, h: 600 },
        expand: 0
    }

    export const spacerSettings = (height: number = 10): RS.Flow.Label.Settings =>
    ({
        ...RS.Flow.Label.defaultSettings,
        dock: RS.Flow.Dock.Top,
        expand: RS.Flow.Expand.Allowed,
        sizeToContents: false,
        size: { w: 1100, h: height }
    });
}