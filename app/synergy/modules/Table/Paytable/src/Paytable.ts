/// <reference path="./generated/Assets.ts" />

namespace RS.Table.Paytable
{
    @RS.HasCallbacks
    export class Paytable extends RS.Flow.Container
    {
        // Paytable Events //
        @RS.AutoDispose public onPaytableOpened = RS.createSimpleEvent();
        @RS.AutoDispose public onPaytableClosed = RS.createSimpleEvent();

        // Paytable Contents //
        // Pages
        @RS.AutoDispose protected _currentPage: RS.Flow.Container;
        @RS.AutoDispose protected _pages: RS.Flow.Container[] = [];
        // UI
        @RS.AutoDispose protected _nextButton: RS.Flow.Button;
        @RS.AutoDispose protected _prevButton: RS.Flow.Button;
        @RS.AutoDispose protected _closeButton: RS.Flow.Button;
        // Design
        @RS.AutoDispose protected _background: RS.Flow.Image;
        @RS.AutoDispose protected _logo: RS.Flow.Image;
        @RS.AutoDispose protected _dots: RS.Flow.List;
        @RS.AutoDispose protected _fade: RS.Rendering.IGraphics;

        // Members //
        protected _settings: Paytable.Settings;
        protected _context:  RS.Game.Context;

        protected _pageIndex: number = 0;
        protected _isOpened: boolean = false;

        public get isOpened() { return this._isOpened; }

        public constructor(settings: Paytable.Settings, context: RS.Game.Context)
        {
            super(RS.Table.Paytable.Settings.PaytableContainer);
            this._settings = settings;
            this._context  = context;

            this.alpha = 0.0;
            this.buildPaytable();
        }

        /** Handle Paytable Rotation */
        public handleOrientationChanged()
        {
            this.buildLogo();
        }

        /** Animate the paytable in */
        @RS.Callback
        public async openPaytable()
        {
            if (!this._isOpened)
            {
                RS.Tween.get<RS.Flow.Container>(this)
                    .set({ alpha: 0.0 })
                    .to({ alpha: 1.0}, 400, RS.Ease.backOut);

                this.onPaytableOpened.publish();
                this._isOpened = true;
            }
        }

        /** Animate the paytable out */
        @RS.Callback
        public async closePaytable()
        {
            if (this._isOpened)
            {
                await RS.Tween.get<RS.Flow.Container>(this)
                    .set({ alpha: 1.0})
                    .to({ alpha: 0.0 }, 400, RS.Ease.backIn);

                this.onPaytableClosed.publish();
                this._isOpened = false;
            }
        }

        /** Go forward one page */
        @RS.Callback
        protected async nextPage()
        {
            // Remove current active page
            this._background.removeChild(this._currentPage);

            // Adjust current page index
            this._pageIndex = this._pageIndex < this._pages.length - 1 ? this._pageIndex + 1 : 0;

            // Rebuild page
            this.pageChanged();
        }

        /** Go back one page */
        @RS.Callback
        protected async prevPage()
        {
            // Remove current page
            this._background.removeChild(this._currentPage);

            // Adjust current page index
            this._pageIndex = this._pageIndex > 0 ? this._pageIndex - 1 : this._pages.length - 1;

            // Rebuild page
            this.pageChanged();
        }

        /** Updates page and dot layout when page changed */
        protected async pageChanged()
        {
            // Add new page
            this._currentPage = this._pages[this._pageIndex];
            this._background.addChild(this._currentPage);

            // Recreate progress dots
            this.buildProgressDots();
        }

        /** Create all the elements of the paytable */
        protected buildPaytable()
        {
            // Build the _background _fade
            this.buildFade();

            // Build the _background image
            this.buildBackground();

            // Build the game _logo
            this.buildLogo();

            // Build all the pages and enable the first
            this.buildPages();

            // Build the UI Buttons
            this.buildButtons();

            // Build the dots along the bottom of the paytable
            this.buildProgressDots()
        }

        /** Build the _background _fade */
        protected buildFade()
        {
            this._fade = new RS.Rendering.Graphics();
            this._fade.beginFill(RS.Util.Colors.black, 0.8);
            this._fade.drawRect(-2000, -2000, screen.width + 4000, screen.height + 6000);
            this._fade.endFill();
            this.addChild(this._fade);
        }

        /** Build the paytable _background */
        protected buildBackground()
        {
            if (this._background) { this._background.dispose(); }
            let bgSettings = RS.Table.Paytable.Settings.Background(this._settings.backgroundImage);
            this._background = RS.Flow.image.create(bgSettings);
            this.addChild(this._background);
        }

        /** Build the paytable _logo */
        protected buildLogo()
        {
            const _logo = RS.Orientation.isLandscape ? this._settings.landscapeLogo : this._settings.portraitLogo;
            if (this._logo) { this._logo.dispose(); }
            this._logo = RS.Flow.image.create(RS.Table.Paytable.Settings.Logo(_logo.image, _logo.position, _logo.scaleFactor), this);
        }

        /** Build all the pages of the paytable and add the first to the view */
        protected buildPages()
        {
            // Create page for each page in settings
            this._settings.pages.forEach((page) =>
            {
                // Check page type and create page content
                switch(page.type)
                {
                    case 'BasePage':
                    {
                        this._pages.push(new RS.Table.Paytable.Pages.BasePage(page, this._context));
                        break;
                    }
                    case 'LegacyPage':
                    {
                        this._pages.push(new RS.Table.Paytable.Pages.LegacyPage(page, this._context));
                        break;
                    }
                    case 'PayoutsPage':
                    {
                        this._pages.push(new RS.Table.Paytable.Pages.PayoutsPage(page, this._context));
                        break;
                    }
                }
            });

            // Add first page in array to _background
            this._background.addChild(this._currentPage = this._pages[0]);
        }

        /** Build the paytable UI buttons */
        protected buildButtons()
        {
            /** Create next button */
            if (this._nextButton) { this._nextButton.dispose(); }

            this._nextButton = RS.Flow.button.create(Table.Paytable.Settings.NextButton(
                this._settings.nextButton.asset,
                this._settings.nextButton.frameUp,
                this._settings.nextButton.frameHover,
                this._settings.nextButton.frameDown,
                this._settings.nextButton.sound,
                this._settings.nextButton.rotation,
            ), this);

            this._nextButton.onClicked(this.nextPage);
            this._nextButton.onPressed(() => { this.animateButtonClick(this._nextButton, 0.95) });
            this._nextButton.onReleased(() => { this.animateButtonClick(this._nextButton, 1.0) });

            /** Create previous button */
            if (this._prevButton) { this._prevButton.dispose(); }

            this._prevButton = RS.Flow.button.create(Table.Paytable.Settings.PrevButton(
                this._settings.prevButton.asset,
                this._settings.prevButton.frameUp,
                this._settings.prevButton.frameHover,
                this._settings.prevButton.frameDown,
                this._settings.prevButton.sound,
                this._settings.prevButton.rotation,
            ), this);

            this._prevButton.onClicked(this.prevPage);
            this._prevButton.onPressed(() => { this.animateButtonClick(this._prevButton, 0.95) });
            this._prevButton.onReleased(() => { this.animateButtonClick(this._prevButton, 1.0) });

            /** Create close button */
            if (this._closeButton) { this._closeButton.dispose(); }

            this._closeButton = RS.Flow.button.create(Table.Paytable.Settings.CloseButton(
                this._settings.closeButton.asset,
                this._settings.closeButton.frameUp,
                this._settings.closeButton.frameHover,
                this._settings.closeButton.frameDown,
                this._settings.closeButton.sound,
                this._settings.closeButton.rotation,
            ), this);

            this._closeButton.onClicked(this.closePaytable);
        }

        protected animateButtonClick(button: Flow.Button, scale: number)
        {
            Tween.removeTweens(button);
            Tween.get<Flow.Button>(button)
                .to({ scaleFactor: scale }, 100);
        }

        /** Build the progress dots base on the page number count */
        protected buildProgressDots()
        {
            if (this._dots) { this._dots.dispose(); }
            this._dots = RS.Flow.list.create(Table.Paytable.Settings.DotList, this._background);

            this._pages.forEach((page, index) =>
            {
                let container = RS.Flow.container.create({ size: { w: 80, h: 80 } }, this._dots);
                let newDot = new RS.Rendering.Graphics();
                let alpha = index <= this._pageIndex ? 1.0 : 0.3;
                newDot.beginFill(RS.Util.Colors.white, alpha);
                newDot.drawCircle(0, 0, 10);
                newDot.endFill();
                newDot.pivot.x = newDot.width / 2;
                newDot.pivot.y = newDot.height / 2;
                container.addChild(newDot);
            });
        }
    }

    export namespace Paytable
    {
        /** Label type */
        export enum LabelType
        {
            /** For Major Headings (Bold) */
            PageHeading = 0,
            /** For Subheadings */
            PageListItem = 1,
            /** For regular text */
            PageText = 2,
            /** Centralised page footer */
            PageFooter = 3,
            /** Image */
            PageImage = 4
        }

        /** Logo Settings */
        export interface LogoSettings
        {
            /** Image to use */
            image: RS.Asset.ImageReference;
            /** Float position on screen */
            position: RS.Math.Vector2D;
            /** Scale to display at */
            scaleFactor: number
        }

        /** Paytable Button Settings */
        export interface ButtonSettings
        {
            /** Leave null for default click sound */
            sound?: RS.Asset.SoundReference,
            /** Leave null to use default settings */
            asset?: RS.Asset.RSSpriteSheetReference,
            /** Leave null to use default settings */
            frameUp?: string,
            /** Leave null to use default settings */
            frameHover?: string;
            /** Leave null to use default settings */
            frameDown?:  string;
            /** Button rotation */
            rotation?: number;
        }

        /** Paytable label settings */
        export interface PaytableLabel
        {
            /** Label Type */
            type: LabelType;
            /** Label content */
            text?: string;
            /** Image content */
            image?: RS.Flow.Image.Settings;
            /** Label alignment, default is middle */
            align?:  RS.Rendering.TextAlign;
            /** Label font, default is FrutigerWorld Regular */
            font?: RS.Asset.FontReference;
            /** Font size, default is 28 */
            size?: number;
            /** Margin below line, default is 10 */
            marginBelow?: number;
        }

        /** Paytable settings */
        export interface Settings
        {
            /** Page data within the paytable */
            pages: (Pages.BasePage.BasePageSettings | Pages.LegacyPage.LegacyPageSettings | Pages.PayoutsPage.PayoutsPageSettings)[];
            /** Landscape Logo settings */
            landscapeLogo: LogoSettings;
            /** Portrait Logo Settings */
            portraitLogo: LogoSettings;

            /** Paytable Panel Image Settings */
            backgroundImage: RS.Asset.ImageReference;
            /** Next Page Button Settings */
            nextButton: ButtonSettings;
            /** Previous Page Button Settings */
            prevButton: ButtonSettings;
            /** Close Button Settings */
            closeButton: ButtonSettings;
        }
    }
}