/// <reference path="../components/CardForceMenu.ts"/>
namespace RS.Table.DevTools
{
    export interface IController<TSettings extends IController.Settings = IController.Settings> extends RS.DevTools.IController<TSettings>
    {
        forceMenu?: CardForceMenu;
    }

    export const IController = Controllers.declare<IController<IController.Settings>, IController.Settings>(Strategy.Type.Instance);

    export namespace IController
    {
        export type ForceData = Map<RS.DevTools.Helpers.ItemSelector.Entry<OneOrMany<Engine.ForceResult>>>;

        export interface Settings extends RS.DevTools.IController.Settings
        {
            engine?: RS.Table.Engine.IEngine<Models, object>;
            force?: RS.DevTools.UI.Button.Settings;
            forceMenu?: CardForceMenu.Settings;
            forceData?: ForceData;
        }
    }

    export const defaultSettings: IController.Settings =
    {
        ...RS.DevTools.defaultSettings,
        engine: null,
        force:
        {
            ...RS.DevTools.UI.Button.defaultSettings,
            color: new Util.Color(213, 94, 0),
            text: "FORCE"
        },
        forceMenu: CardForceMenu.defaultSettings
    }
}
