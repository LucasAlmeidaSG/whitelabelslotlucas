/// <reference path="IController.ts" />

namespace RS.Table.DevTools
{
    @HasCallbacks
    export class Controller extends RS.DevTools.Controller<IController.Settings> implements IController
    {
        @AutoDisposeOnSet protected _forceMenu: CardForceMenu | null;
        public get forceMenu() { return this._forceMenu; }

        protected get showForceButton() { return this.settings.force != null; }

        public create(gameVersion: Version): void
        {
            super.create(gameVersion);

            this._forceMenu = cardForceMenu.create(this.settings.forceMenu);
            this._forceMenu.visible = false;
        }

        public attach(container: RS.Rendering.IContainer, props?: Partial<Flow.ElementProperties>): void
        {
            super.attach(container, props);

            container.addChild(this._forceMenu);
        }

        public detach(container?: RS.Rendering.IContainer): void
        {
            super.detach(container);

            this._forceMenu.parent.removeChild(this._forceMenu);
        }

        protected createUI(gameVersion: Version): void
        {
            if (this._ui && !this._ui.isDisposed)
            {
                Log.warn("[DevToolsController] .create() called again when dev tools ui already exists.");
                return;
            }

            this._gameVersion = gameVersion;

            let titleText: string;
            if (this.settings.engine && this.settings.engine.version != null)
            {
                titleText = `C: ${Version.toString(gameVersion)}\nE: ${Version.toString(this.settings.engine.version)}`;
            }
            else
            {
                titleText = Version.toString(gameVersion);
            }

            this._ui = RS.DevTools.UI.buttonList.create({
                ...RS.DevTools.UI.ButtonList.defaultSettings,
                ...this.settings.buttonList,
                titleLabel:
                {
                    ...Flow.Label.defaultSettings,
                    text: titleText,
                    spacing: Flow.Spacing.all(3),
                    dock: Flow.Dock.Fill,
                    sizeToContents: true,
                    expand: Flow.Expand.Disallowed
                },
                buttons:
                [
                    this.showDemoButton ? this.settings.demo : null,
                    this.showTestButton ? this.settings.tests : null,
                    this.showForceButton ? this.settings.force : null,
                    this.showPauseButton ? this.settings.pause : null,
                    this.showReplayButton ? this.settings.replay : null,
                    this.showGREGButton ? this.settings.GREG : null,
                    this.showTimeButton ? this.settings.time : null,
                    this.showHideButton ? this.settings.hide : null,
                    this.showScreenButton ? this.settings.screen : null,
                    this.showMusicMuteButton ? this.settings.muteMusic : null,
                    this.showDumpButton ? this.settings.dump : null
                ].filter((b) => b != null),
                dock: Flow.Dock.Float
            });
            this._ui.onButtonClicked(this.handleButtonClicked);
            this._ui.onCollapsed(this.handleUICollapsed);
            this._ui.onOpened(this.handleUIOpened);
        }

        @Callback
        protected async handleButtonClicked(buttonID: number)
        {
            const settings = this._ui.settings.buttons[buttonID];
            if (settings === this.settings.force)
            {
                this._forceMenu.visible = true;
                this._showButtonAreaHandle = this.settings.showButtonAreaArbiter.declare(false);
                this._forceMenu.updateCards();
                this._forceMenu.onClose.once(this.handleForceMenuClosed);
                this.hideUI();
            }
            else
            {
                super.handleButtonClicked(buttonID);
            }
        }

        @Callback
        protected handleForceMenuClosed(forceResult: Engine.ForceResult): void
        {
            this._showButtonAreaHandle.dispose();
            this._forceMenu.visible = false;
            this.settings.engine.force(forceResult);
            this.showUI();
        }
    }

    IController.register(Controller);
}
