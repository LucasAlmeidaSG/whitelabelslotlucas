namespace RS.Table.DevTools
{
    @HasCallbacks
    export class CardSelector extends Flow.BaseElement<CardSelector.Settings>
    {
        @AutoDisposeOnSet protected _cardClicked = createEvent<Models.Card>()
        public get cardClicked() { return this._cardClicked.public; }
        @AutoDisposeOnSet protected _background: Flow.Background;
        @AutoDisposeOnSet protected _grid: Flow.Grid;
        @AutoDisposeOnSet protected _cards: Flow.Image[] = [];

        constructor(settings: CardSelector.Settings)
        {
            super(settings, null);
            this.createComponents();
            Orientation.onChanged(this.handleOrientationChanged);
            this.handleOrientationChanged();
        }

        protected createComponents()
        {
            this.suppressLayouting();

            const orientationSettings = RS.Orientation.isPortrait ? this.settings.portrait : this.settings.landscape;
            this.size = orientationSettings.size;
            this._background = Flow.background.create(this.settings.background, this);
            this._grid = Flow.grid.create(orientationSettings.grid, this);
            this.createCards();

            this.restoreLayouting(true);
        }

        protected createCards(): void
        {
            const spriteSheet = Asset.Store.getAsset(this.settings.card.asset).asset as RS.Rendering.ISpriteSheet;

            for (const animation of this.settings.animations)
            {
                const frames = spriteSheet.getAnimation(animation).frames;
                frames.forEach((frame, index) =>
                {
                    const settings =
                    {
                        ...this.settings.card,
                        animationName: animation,
                        frameID: frame
                    };
                    const card = Flow.image.create(settings, this._grid);
                    this._cards.push(card);
                    const cardHandle = card.onClicked(() => this._cardClicked.publish(
                        {
                            suit: animation,
                            value: index
                        }
                    ));
                    Disposable.bind(cardHandle, this);
                });
            }
        }

        @Callback
        protected handleOrientationChanged()
        {
            const settings = Orientation.isLandscape ? this.settings.landscape : this.settings.portrait; 
            this._grid.apply(settings.grid);
            this.size = settings.size;
            this.invalidateLayout();
        }
    }

    export const cardSelector = Flow.declareElement(CardSelector);

    export namespace CardSelector
    {
        export interface OrientationSettings
        {
            grid: Flow.Grid.Settings;
            size: Flow.Size;
        }
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            card: Flow.Image.SpritesheetFrameSettings;
            landscape: OrientationSettings;
            portrait: OrientationSettings;
            background: Flow.Background.Settings;
            animations: Models.Card.Suit[];
        }

        const defaultGridSettings: Flow.Grid.Settings =
        {
            order: null,
            rowCount: null,
            colCount: null,
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            spacing: Flow.Spacing.all(2) 
        }

        export const CardSelectorSettings: Settings =
        {
            name: "Card selector",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            expand: Flow.Expand.Allowed,
            sizeToContents: false,
            landscape:
            {
                size: { w: 1900, h: 1000 },
                grid:
                {
                    ...defaultGridSettings,
                    order: Flow.Grid.Order.RowFirst,
                    rowCount: 4,
                    colCount: 13
                },
            },
            portrait:
            {
                size: { w: 1050, h: 1750 },
                grid:
                {
                    ...defaultGridSettings,
                    order: Flow.Grid.Order.ColumnFirst,
                    rowCount: 13,
                    colCount: 4
                },
            },
            card:
            {
                name: "Card image",
                sizeToContents: true,
                dock: Flow.Dock.Fill,
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Rendering.Cards.Assets.Front,
                animationName: "",
                scaleMode: Math.ScaleMode.Contain
            },
            background:
            {
                kind: Flow.Background.Kind.SolidColor,
                color: Util.Colors.black,
                alpha: 0.8,
                borderSize: 0
            },
            animations:
            [
                Models.Card.Suit.DIAMONDS,
                Models.Card.Suit.CLUBS,
                Models.Card.Suit.HEARTS,
                Models.Card.Suit.SPADES
            ]
        }
    }
}
