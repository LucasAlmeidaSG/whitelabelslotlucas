/// <reference path="CardSelector.ts"/>
namespace RS.Table.DevTools
{
    export class Cards extends Flow.BaseElement<Cards.Settings>
    {
        @AutoDispose protected _onCardClicked = createEvent<Cards.ClickEvent>();
        public get onCardClicked() { return this._onCardClicked.public; }

        @AutoDisposeOnSet protected _mainContainer: Flow.Container;
        @AutoDisposeOnSet protected _handsList: Flow.List;
        @AutoDisposeOnSet protected _hands: Flow.Grid[] = [];
        @AutoDisposeOnSet protected _cards: Flow.Image[][] = [];
        @AutoDisposeOnSet protected _cardSelector: CardSelector;

        constructor(settings: Cards.Settings)
        {
            super(settings, null);
            this.createComponents();
        }

        public reset(dealer?: Models.Card[], player?: Models.Card[])
        {
            this._hands = [];
            this._cards = [];

            this.createComponents(dealer, player);
        }

        protected createComponents(dealer?: Models.Card[], player?: Models.Card[])
        {
            this._mainContainer = Flow.container.create(this.settings.container, this);
            this._handsList = Flow.list.create(this.settings.handsList, this._mainContainer);

            // Creates card grids.
            for (let handIndex = 0; handIndex < 2; handIndex++)
            {
                const dock = handIndex == 0 ? Flow.Dock.Top : Flow.Dock.Bottom;
                const cardGrid = Flow.grid.create({...this.settings.cardGrid, dock}, this._handsList);

                for (let cardIndex = 0; cardIndex < this.settings.cardsPerHand; cardIndex++)
                {
                    if (cardIndex == 0) { this._cards[handIndex] = []; }

                    let card: Models.Card;
                    if (handIndex == 0 && dealer)
                    {
                        card = dealer[cardIndex];
                    }
                    else if (player)
                    {
                        card = player[cardIndex];
                    }

                    const cardImg = this.getCard(cardIndex, handIndex, cardGrid, card);
                    this._cards[handIndex][cardIndex] = cardImg;
                }

                this._hands.push(cardGrid);
            }

            this.invalidateLayout();
        }

        // Opens card selector, and creates the card front.
        protected async cardClicked(cardIndex: number, handIndex: number)
        {
            this._handsList.interactiveChildren = false;
            this._cardSelector = cardSelector.create(this.settings.cardSelector, this.parent);
            this.invalidateLayout();

            const card = await this._cardSelector.cardClicked;
            this._onCardClicked.publish({ handIndex, cardIndex, card });

            this._hands[handIndex].removeChild(this._cards[handIndex][cardIndex]);
            this._cards[handIndex][cardIndex] = this.getCard(cardIndex, handIndex, this._hands[handIndex], card);
            this._handsList.interactiveChildren = true;

            this._handsList.invalidateLayout();
            this._cardSelector.dispose();
        }

        // Gets the card from the spritesheet.
        protected getCard(cardIndex: number, handIndex: number, parent: RS.Rendering.IContainer, card?: Models.Card): Flow.Image
        {
            let img: Flow.Image;
            if (card)
            {
                const spriteSheet = Asset.Store.getAsset(this.settings.cardFront.asset).asset as RS.Rendering.ISpriteSheet;
                const frameNumber = spriteSheet.getAnimation(card.suit).frames[card.value];
                const settings =
                {
                    ...this.settings.cardFront,
                    animationName: card.suit,
                    frameID: frameNumber
                }
                img = Flow.image.create(settings);
            }
            else
            {
                img = Flow.image.create(this.settings.cardBack);
            }
            parent.addChild(img, cardIndex);
            img.onClicked(() => this.cardClicked(cardIndex, handIndex));
            return img;
        }
    }

    export const cards = Flow.declareElement(Cards);

    export namespace Cards
    {
        export interface ClickEvent
        {
            handIndex: number;
            cardIndex: number;
            card: Models.Card
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            cardSelector: CardSelector.Settings;
            cardsPerHand: number;
            container: Flow.Container.Settings;
            handsList: Flow.List.Settings;
            /** in order of hands */
            cardGrid: Flow.Grid.Settings;
            cardBack: Flow.Image.Settings;
            cardFront: Flow.Image.SpritesheetFrameSettings;
        }

        const defaultImage: Flow.Image.Settings =
        {
            kind: Flow.Image.Kind.Bitmap,
            asset: null,
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            scaleMode: Math.ScaleMode.Contain,
            scaleFactor: 0.5
        }

        export const defaultSettings: Settings =
        {
            name: "Cards",
            dock: Flow.Dock.Top,
            sizeToContents: true,
            cardSelector: CardSelector.CardSelectorSettings,
            cardsPerHand: 5,
            container:
            {
                sizeToContents: true,
                dock: Flow.Dock.Fill
            },
            handsList:
            {
                name: "Hands List",
                direction: Flow.List.Direction.TopToBottom,
                sizeToContents: true,
                dock: Flow.Dock.Fill
            },
            cardGrid:
            {
                name: "Card grid",
                colCount: 5,
                rowCount: 1,
                dock: Flow.Dock.Fill,
                order: Flow.Grid.Order.RowFirst,
                sizeToContents: false,
                size: { w: 900, h: 300 }
            },
            cardBack:
            {
                ...defaultImage,
                name: "Card back",
                kind: Flow.Image.Kind.Bitmap,
                asset: Rendering.Cards.Assets.Back
            },
            cardFront:
            {
                ...defaultImage,
                name: "Card front",
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Rendering.Cards.Assets.Front,
                animationName: null
            }
        }
    }
}
