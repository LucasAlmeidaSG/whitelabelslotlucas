/// <reference path="Cards.ts"/>

namespace RS.Table.DevTools
{
    @HasCallbacks
    export class CardForceMenu<TSettings extends CardForceMenu.Settings = CardForceMenu.Settings> extends Flow.BaseElement<TSettings>
    {
        @AutoDisposeOnSet protected _onClose = createEvent<Engine.ForceResult>();
        public get onClose() { return this._onClose.public; }

        @AutoDisposeOnSet protected _buttonContainer: Flow.Container;
        @AutoDisposeOnSet protected _buttonList: Flow.List;
        @AutoDisposeOnSet protected _applyButton: Flow.Button;
        @AutoDisposeOnSet protected _resetButton: Flow.Button;
        @AutoDisposeOnSet protected _mainBackground: Flow.Background;

        @AutoDisposeOnSet protected _cards: Cards;

        protected _dealer: Models.Card[] = [];
        protected _player: Models.Card[] = [];

        constructor(settings: TSettings)
        {
            super(settings, null);
            this.createComponents();
        }

        @Callback
        public reset()
        {
            this._dealer = [];
            this._player = [];
            this._cards.reset();

            this._applyButton.enabled = true;
            this._applyButton.label.text = this.settings.applyButton.textLabel.text;
        }

        public updateCards()
        {
            this._cards.reset(this._dealer, this._player);
        }

        protected createComponents()
        {
            this.suppressLayouting();

            this._mainBackground = Flow.background.create(this.settings.mainBackground, this);
            this._cards = cards.create(this.settings.cards, this);
            this._buttonContainer = Flow.container.create(this.settings.buttonContainer, this);
            this._buttonList = Flow.list.create(this.settings.buttonList, this._buttonContainer);
            this._resetButton = Flow.button.create(this.settings.resetButton, this._buttonList);
            this._applyButton = Flow.button.create(this.settings.applyButton, this._buttonList);

            this.restoreLayouting(true);
            this.bindButtons();
        }

        protected bindButtons()
        {
            const resetClicked = this._resetButton.onClicked(this.reset);
            Disposable.bind(resetClicked, this);
            const applyclicked = this._applyButton.onClicked(this.handleApplyButtonClicked);
            Disposable.bind(applyclicked, this);
            const cardsClicked = this._cards.onCardClicked(this.handleCardClicked);
            Disposable.bind(cardsClicked, this);
        }

        // Randomises unselected cards if any and commits hands.
        @Callback
        protected handleApplyButtonClicked()
        {
            const dealer = [...this._dealer];
            const player = [...this._player];

            for (let i = 0; i < this.settings.cards.cardsPerHand; i++)
            {
                if (!player[i])
                {
                    this.randomiseCardInHand(player, i);
                }

                if (!dealer[i])
                {
                    this.randomiseCardInHand(dealer, i);
                }
            }

            this._onClose.publish(
                {
                    cards:
                    {
                        dealer: dealer,
                        player: player
                    }
                }
            );
        }

        @Callback
        protected handleCardClicked(ev: Cards.ClickEvent)
        {
            // Index 0 is dealer's hand.
            if (ev.handIndex == 0)
            {
                this._dealer[ev.cardIndex] = ev.card;
            }
            else
            {
                this._player[ev.cardIndex] = ev.card;
            }

            // disable apply button if have duplicates
            this._applyButton.enabled = !Models.Card.hasDuplicateCards(this.getAllCards());
        }

        protected randomiseCardInHand(hand: Models.Card[], index: number): void
        {
            let loopCounter = 0;
            let duplicatedCard = true;
            while (duplicatedCard)
            {
                if (++loopCounter > 100)
                {
                    Log.warn("[CardForceMenu] randomiseCardInHand: Broke infinite loop");
                    break;
                }

                const card = Models.Card.generateRandomCard();
                const allCards = this.getAllCards();
                if (!ArrayUtils.contains(allCards, card, Models.Card.isDuplicateCard))
                {
                    hand[index] = card;
                    duplicatedCard = false;
                }
            }
        }

        protected getAllCards(): Models.Card[]
        {
            const allCards = [];

            const addCards = (cards: Models.Card[]) =>
            {
                if (cards)
                {
                    cards.forEach((c) =>
                    {
                        if (c != null)
                        {
                            allCards.push(c);
                        }
                    });
                }
            }

            addCards(this._dealer);
            addCards(this._player);

            return allCards;
        }
    }

    export const cardForceMenu = Flow.declareElement(CardForceMenu, false);

    export namespace CardForceMenu
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            buttonContainer: Flow.Container.Settings;
            mainBackground: Flow.Background.Settings;
            buttonList: Flow.List.Settings;
            applyButton: Flow.Button.Settings;
            resetButton: Flow.Button.Settings;
            cards: Cards.Settings;
        }

        const defaultButtonSettings: Flow.Button.Settings =
        {
            ...Flow.Button.defaultSettings,
            dock: Flow.Dock.Fill,
            expand: Flow.Expand.Disallowed,
            textLabel:
            {
                ...Flow.Button.defaultSettings.textLabel,
                fontSize: 25,
                padding: Flow.Spacing.all(2)
            }
        }

        export const defaultSettings: Settings =
        {
            name: "Card Force Menu",
            sizeToContents: true,
            dock: Flow.Dock.Fill,
            innerPadding: Flow.Spacing(0, 0, 0, 130),
            mainBackground:
            {
                name: "Main background",
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.SolidColor,
                color: Util.Colors.black,
                alpha: 0.7,
                borderSize: 0
            },
            buttonContainer:
            {
                name: "Buttons container",
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Top,
                padding: Flow.Spacing(0, 50, 0, 0)
            },
            buttonList:
            {
                name: "Button list",
                ...Flow.List.defaultSettings,
                direction: Flow.List.Direction.LeftToRight,
                dock: Flow.Dock.Bottom,
                spacing: Flow.Spacing(10, 50, 10, 0)
            },
            applyButton:
            {
                ...defaultButtonSettings,
                name: "Apply button",
                dock: Flow.Dock.Bottom,
                textLabel:
                {
                    ...defaultButtonSettings.textLabel,
                    text: "APPLY"
                },
                disabledTextLabel:
                {
                    ...defaultButtonSettings.textLabel,
                    text: "DUPLICATED CARD"
                }
            },
            resetButton:
            {
                ...defaultButtonSettings,
                name: "Reset button",
                dock: Flow.Dock.Bottom,
                textLabel:
                {
                    ...defaultButtonSettings.textLabel,
                    text: "RESET"
                }
            },
            cards: Cards.defaultSettings
        }
    }
}
