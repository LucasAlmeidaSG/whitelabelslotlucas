namespace RS.Table
{
    export class Chip
    {
        private _value: number;			// Stake of the Chip
        private _multiplier: number;	// Multiplier. Set to mark a multiplication for the previous bets
        private _group: number;	// Flag to set a chip as part of a group. When removed will removed the rest of the group chips

        public get value() { return this._value; }
        public set value(value: number) { this._value = value; }

        public get multiplier() { return this._multiplier; }
        public set multiplier(value:number) { this._multiplier = value; }

        public get group() { return this._group; }

        constructor(settings: Chip.Settings)
        {
            this._value = settings.value;
            this._multiplier = settings.multiplier;
            this._group = settings.group;
        }
    }

    export namespace Chip
    {
        export interface Settings
        {
            value: number;
            multiplier?: number;
            group?: number;
        }
    }
}
