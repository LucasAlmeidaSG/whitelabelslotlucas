namespace RS.Table
{
    /**
     * Encapsulates the chips that store and manage the chips used within a table game..
     */
    export interface IChips
    {
        /** Event that gets fired whe the Chips change */
        readonly onChipsChanged: IEvent;

        /** Gets the Current selected chip value */
        readonly selectedChipValue: number;

        /** Gets or sets the currency Formatter used for the chip system */
        currencyFormatter: RS.Localisation.CurrencyFormatter;

        /** Gets or sets the stake model used for the chip system */
        stakeModel: Models.Stake;

        /** Gets or sets the chip values */
        chipValues: number[];

        /** Sets or gets the container used for the chips tool tips.. */
        toolTipContainer: RS.Rendering.IContainer;

        /** Sets or gets whether the chips are disabled/enabled. */
        enabled: boolean;

        /** Sets or gets the chip spritesheet */
        chipSpriteSheet: RS.Rendering.ISpriteSheet;

        /** Sets stake model, currency formatter + spritesheet from given settings */
        initialise(settings: IChips.InitSettings): void;

        /** Creates some buttons for debugging */
        startDebugMenu(): RS.Rendering.IContainer;

        /** Move the chip area to a new id key */
        changeChipAreaId(oldId: string, newId: string): void;

        /** Creates the chip sprite sheet */
        setChipSpriteSheet(asset: RS.Asset.RSSpriteSheetReference | RS.Asset.SpriteSheetReference): void;

        /** Returns the chip sprite sheet */
        getChipSpriteSheet(): RS.Rendering.ISpriteSheet;

        /** Enables/Disables a Chip area */
        enableChipArea(id: string, enable: boolean): void;

         /** Clears all the bets */
        clearBets(): void;

        /** Undo last bet */
        undoBet(): void;

        /** Rebet last bet */
        reBet(bypassChecks?: boolean): void;

        /** Multiply Bet */
        multiplyBet(multiplier: number): void;

        /** Sets mirrored bet areas */
        mirrorAreas(chipAreas: string[]): void;

        /** Checks if there are chips on the table */
        areChips(): boolean;

        /** Checks if there are chips to rebet stored */
        areRebetChips(): boolean;

        /** Save bets for rebet */
        saveCurrentBets(): void;

        /** Return the sum of total bets */
        getTotalBets(): number;

        /** Save bets for rebet and clear arrays */
        clearAndSave(): void;

        /** Checks if all bet areas have rebet chips and won't exceed max bet.
         * @param startValueOverride optional - the start value to be added to when checking if rebetting will breach max bet - by default will be the total chips in the area */
        canRebet(startValueOverride?: number): boolean;

        /** Check to see if all active bets(total bet > 0) meet the min bet. */
        exceedsMinBets(publishError?: boolean): boolean;
    }

    export namespace IChips
    {
        export interface InitSettings
        {
            stakeModel: Models.Stake;
            currencyFormatter: Localisation.CurrencyFormatter;
            chipSpriteSheet: RS.Asset.RSSpriteSheetReference | RS.Asset.SpriteSheetReference;
        }
    }
}
