namespace RS.Table
{
    export interface IChips2D extends IChips
    {
        /** Sets last Chip Area used to keep track on where was the last bet made */
        setLastChipAreaUsed(chipArea: ChipArea2D): void;

        /** Creates and stores a new Chip area */
        createChipArea(settings: ChipArea2D.Settings, runtimeData: ChipArea2D.RuntimeData): ChipArea2D;

        /** Returns a chip Area by its Id */
        getChipAreaById(id: string): ChipArea2D;
    }
    export const IChips2D = Strategy.declare<IChips2D>(Strategy.Type.Singleton, false, true);
}
