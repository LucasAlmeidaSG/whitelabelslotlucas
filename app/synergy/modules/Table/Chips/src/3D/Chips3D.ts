/// <reference path="IChips3D.ts" />

namespace RS.Table
{
    /**
     * Chips System that controls the chips funtionality
     */
    @HasCallbacks
    export class Chips3D implements IChips3D
    {
        public readonly onChipsChanged = createSimpleEvent();

        protected _mouseDown: boolean = false;
        protected _storedChipAreas: Array<ChipArea3D> = [];
        protected _lastChipAreaUsed: string[] = [];
        protected _chipSpriteSheet: RS.Rendering.ISpriteSheet;
        protected _stakeModel: Models.Stake;
        protected _chipValues: number[];
        protected _chipsGroupId: number = 0;
        protected _currencyFormatter: Localisation.CurrencyFormatter;
        protected _enabled: boolean = true;
        protected _toolTipContainer: RS.Rendering.IContainer;

        private _stage: RS.Rendering.IStage;

        /** Sets or gets whether the chips are disabled/enabled. */
        public get enabled(): boolean { return this._enabled }
        public set enabled(enable: boolean) { this._enabled = enable }

        /** Sets or gets the container used for the chips tool tips.. */
        public get toolTipContainer(): RS.Rendering.IContainer { return this._toolTipContainer }
        public set toolTipContainer(container: RS.Rendering.IContainer) { this._toolTipContainer = container }

        /** Gets or sets the currency Formatter used for the chip system */
        public set currencyFormatter(currenctFormatter: Localisation.CurrencyFormatter) { this._currencyFormatter = currenctFormatter; }
        public get currencyFormatter(): Localisation.CurrencyFormatter
        {
            if (!this._currencyFormatter)
            {
                Log.error("Not currency formatter have been set for the Chips System");
            }
            return this._currencyFormatter;
        }

        public get chipSpriteSheet(): RS.Rendering.ISpriteSheet { return this._chipSpriteSheet; }
        public set chipSpriteSheet(value: RS.Rendering.ISpriteSheet) { this._chipSpriteSheet = value; }

        /** Gets or sets the stake model used for the chip system */
        public set stakeModel(model: Models.Stake)
        {
            this._stakeModel = model;
            this._chipValues = this._stakeModel.betOptions.map((o) => o.value);
        }
        public get stakeModel(): Models.Stake { return this._stakeModel; }

        /** Gets the Current selected chip value */
        public get selectedChipValue(): number { return Models.Stake.getCurrentBet(this._stakeModel).value; }

        /** Gets or sets the chip values */
        public get chipValues(): number[] { return this._chipValues; }
        public set chipValues(value: number[]) { this._chipValues = value; }

        public initialise(settings: IChips3D.InitSettings): void
        {
            this.stakeModel = settings.stakeModel;
            this.currencyFormatter = settings.currencyFormatter;
            this.setChipSpriteSheet(settings.chipSpriteSheet);
            this._stage = settings.stage;
        }

        /** Creates some buttons for debugging */
        public startDebugMenu(): RS.Rendering.IContainer
        {
            const debugContainer = new RS.Rendering.Container();
            debugContainer.name = "Chip Debug Menu";
            debugContainer.interactive = true;

            const undoButton = this.debugButton("UNDO", Math.Vector2D(0, 0), this.undoBet);
            debugContainer.addChild(undoButton);
            const clearButton = this.debugButton("CLEAR", Math.Vector2D(120, 0), this.clearBets);
            debugContainer.addChild(clearButton);
            const rebetButton = this.debugButton("REBET", Math.Vector2D(240, 0), this.reBet);
            debugContainer.addChild(rebetButton);

            return debugContainer;
        }

        /** Creates and stores a new Chip area */
        public createChipArea(settings: ChipArea3D.Settings, runtimeData: ChipArea3D.RuntimeData): ChipArea3D
        {
            const chipArea = new ChipArea3D(settings, runtimeData);
            if (!this._toolTipContainer)
            {
                this._toolTipContainer = new RS.Rendering.Container();
            }

            if (this.addChipArea(chipArea))
            {
                this._toolTipContainer.addChild(chipArea.toolTipContainer);
                return chipArea;
            }
            return null;
        }

        /** Returns a chip Area by its Id */
        public getChipAreaById(id: string): ChipArea3D
        {
            return this._storedChipAreas[id];
        }

        /** Move the chip area to a new id key */
        public changeChipAreaId(oldId: string, newId: string): void
        {
            if (!this._storedChipAreas[oldId])
            {
                Log.warn("A chip area not found with id " + oldId);
                return;
            }
            if (this._storedChipAreas[newId])
            {
                Log.warn("A chip area was overwritten for id " + newId);
            }
            this._storedChipAreas[newId] = this._storedChipAreas[oldId];
            delete this._storedChipAreas[oldId];
        }

        /** Creates the chip sprite sheet */
        public setChipSpriteSheet(asset: Asset.RSSpriteSheetReference | Asset.SpriteSheetReference): void
        {
            this._chipSpriteSheet = Asset.Store
                .getAsset(asset)
                .asset as RS.Rendering.ISpriteSheet;
        }

        /** Sets last Chip Area used to keep track on where was the last bet made */
        public setLastChipAreaUsed(chipArea: ChipArea3D): void
        {
            this._lastChipAreaUsed.push(chipArea.id);
        }

        /** Returns the chip sprite sheet */
        public getChipSpriteSheet(): RS.Rendering.ISpriteSheet
        {
            if (this._chipSpriteSheet)
            {
                return this._chipSpriteSheet;
            }
            return null;
        }

        /** Enables/Disables a Chip area */
        public enableChipArea(id: string, enable: boolean): void
        {
            if (!this._storedChipAreas[id])
            {
                Log.warn("A chip area not found with id " + id);
                return;
            }

            this._storedChipAreas[id].enabled = enable;
        }

        /** Clears all the bets */
        public clearBets(): void
        {
            if (!this.enabled)
            {
                Log.debug("Chips System not enabled");
                return
            }

            for (const key in this._storedChipAreas)
            {
                if (!this._storedChipAreas[key].frozen)
                {
                    this._storedChipAreas[key].clearBets();
                }
            }
            this._lastChipAreaUsed = [];

            this.chipsChangedEvent();
        }

        /** Undo last bet */
        public undoBet(): void
        {
            if (!this.enabled)
            {
                Log.debug("Chips System not enabled");
                return
            }

            if (this._lastChipAreaUsed.length > 0)
            {
                const chipAreaId: string = this._lastChipAreaUsed.pop();
                const chip: Chip = this._storedChipAreas[chipAreaId].undoBets();
                if (chip.group > 0)
                {
                    for (const key in this._storedChipAreas)
                    {
                        if (!this._storedChipAreas[key].frozen)
                        {
                            this._storedChipAreas[key].removeAllGroupBets(chip.group);
                        }
                    }
                }
            }

            this.chipsChangedEvent();
        }

        /**
         * Rebet last bet
         * @param bypassChecks - whether to bypass the canRebet checks
         */
        public reBet(bypassChecks: boolean = false): void
        {
            if (!this.enabled)
            {
                Log.debug("Chips System not enabled");
                return
            }

            // Check to see if rebetting will exceed max bet
            if (!bypassChecks && !this.canRebet())
            {
                Log.debug("Can not rebet without exceeding max bet");
                return;
            }

            const newGroupId: number = ++this._chipsGroupId;
            let keyIndex: string;
            for (const key in this._storedChipAreas)
            {
                if (!this._storedChipAreas[key].frozen && this._storedChipAreas[key].hasRebetChips())
                {
                    keyIndex = key;
                    this._storedChipAreas[key].reBet(newGroupId, bypassChecks);
                }
            }

            // We need to add an entry to the bet queue
            if (keyIndex)
            {
                this._lastChipAreaUsed.push(keyIndex);
            }

            this.chipsChangedEvent();
        }

        /**
         * Check to see if all active bets (total bet > 0) meet the min bet.
         */
        public exceedsMinBets(publishError: boolean = false): boolean
        {
            for (const key in this._storedChipAreas)
            {
                if (this._storedChipAreas[key].getTotalBet() && !this._storedChipAreas[key].exceedsMinBet(0, publishError))
                {
                    return false;
                }
            }
            return true;
        }

        /** Multiply Bet */
        public multiplyBet(multiplier: number): void
        {
            if (!this.enabled)
            {
                Log.debug("Chips System not enabled");
                return
            }

            const newGroupId: number = ++this._chipsGroupId;
            let keyIndex: string;
            for (const key in this._storedChipAreas)
            {
                if (!this._storedChipAreas[key].frozen)
                {
                    keyIndex = key;
                    this._storedChipAreas[key].multiplyBet(multiplier, newGroupId);
                }
            }
            // We need to add an entry to the bet queue
            if (keyIndex)
            {
                this._lastChipAreaUsed.push(keyIndex);
            }

            this.chipsChangedEvent();
        }

        /** Sets mirrored bet areas */
        public mirrorAreas(chipAreas: string[]): void
        {
            for (let i: number = 0; i < chipAreas.length; i++)
            {
                let chipArea: ChipArea3D = this._storedChipAreas[chipAreas[i]];
                if (chipArea)
                {
                    for (let x: number = 0; x < chipAreas.length; x++)
                    {
                        let mirrorArea = this._storedChipAreas[chipAreas[x]];
                        if (mirrorArea !== chipArea)
                        {
                            chipArea.addMirroredBetArea(mirrorArea);
                        }
                    }
                }
            }
        }

        /** Checks if there are chips on the table */
        public areChips(): boolean
        {
            for (const key in this._storedChipAreas)
            {
                if (!this._storedChipAreas[key].frozen && this._storedChipAreas[key].hasChips())
                {
                    return true;
                }
            }
            return false;
        }

        /** Checks if there are chips to rebet stored */
        public areRebetChips(): boolean
        {
            for (const key in this._storedChipAreas)
            {
                if (!this._storedChipAreas[key].frozen && this._storedChipAreas[key].hasRebetChips())
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * Checks if all bet areas have rebet chips and won't exceed max bet.
         * @param startValueOverride optional - the start value to be added to when checking if rebetting will breach max bet - by default will be the total chips in the area
         */
        public canRebet(startValueOverride?: number): boolean
        {
            if (!this.areRebetChips()) { return false; }

            for (const key in this._storedChipAreas)
            {
                if (!this._storedChipAreas[key].canRebet(startValueOverride))
                {
                    return false;
                }
            }
            return true;
        }

        /** Save bets for rebet and clear arrays */
        public clearAndSave(): void
        {
            this._lastChipAreaUsed = [];
            for (const key in this._storedChipAreas)
            {
                this._storedChipAreas[key].saveAndClear();
            }
        }

        /** Save bets for rebet */
        public saveCurrentBets(): void
        {
            this._lastChipAreaUsed = [];
            for (const key in this._storedChipAreas)
            {
                this._storedChipAreas[key].save();
            }
        }

        /** Return the sum of total bets */
        public getTotalBets(): number
        {
            let totalBet: number = 0;
            for (const key in this._storedChipAreas)
            {
                totalBet += this._storedChipAreas[key].getTotalBet();
            }

            return totalBet;
        }

        protected handleMouseDown(chipArea: ChipArea3D)
        {
            if (chipArea.enabled)
            {
                this._mouseDown = true;
            }
        }

        protected handleChipAreaOver(chipArea: ChipArea3D)
        {
            const currentChipAreaBet = IChips3D.get().selectedChipValue;
            const lastChipAreaUsed = this._lastChipAreaUsed[this._lastChipAreaUsed.length - 1];
            if (this._mouseDown && chipArea.enabled && lastChipAreaUsed != chipArea.id)
            {
                IChips3D.get().getChipAreaById(chipArea.id).addChip(currentChipAreaBet);
            }
        }

        /** Stores a new chip area */
        protected addChipArea(chipArea: ChipArea3D): boolean
        {
            if (this._storedChipAreas[chipArea.id])
            {
                Log.error("Chip area already existed for id " + chipArea.id);
                return false;
            }
            this._storedChipAreas[chipArea.id] = chipArea;

            const chipsAdded = chipArea.onChipsAdded(this.chipsChangedEvent);
            Disposable.bind(chipsAdded, chipArea);
            const onOver = chipArea.visual.onOver(this.handleChipAreaOver.bind(this, chipArea));
            Disposable.bind(onOver, chipArea.visual);
            const onDown = chipArea.visual.onDown(() => this.handleMouseDown(chipArea));
            Disposable.bind(onDown, chipArea.visual);
            const onUp = chipArea.visual.onUp(() => this._mouseDown = false);
            Disposable.bind(onUp, chipArea.visual);
            const onUpOut = chipArea.visual.onUpOutside(() => this._mouseDown = false);
            Disposable.bind(onUpOut, chipArea.visual);

            return true;
        }

        /** Creates a button */
        private debugButton(text: string, position: Math.Vector2D, buttonCall: () => void): RS.Rendering.IContainer
        {
            const button = new RS.Rendering.Container();
            button.interactive = true;
            button.x = position.x;
            button.y = position.y;
            button.pivot.set(50, 35 * 0.5);

            const bg = new RS.Rendering.Graphics();
            bg.beginFill(new Util.Color(0xFC000E));
            bg.drawRect(0, 0, 100, 35);
            bg.endFill();
            button.addChild(bg);

            const undoText = new RS.Rendering.Text({
                font: RS.Rendering.Assets.Fonts.MPlus2P,
                fontSize: 32
            }, text);
            button.addChild(undoText);

            button.onClicked(buttonCall);

            return button;
        }

        /** Event that gets fired whe the Chips change */
        @Callback
        private chipsChangedEvent(): void
        {
            this.onChipsChanged.publish();
        }
    }

    IChips3D.register(Chips3D);
}
