namespace RS.Table
{
    export interface IChips3D extends IChips
    {
        /** Sets last Chip Area used to keep track on where was the last bet made */
        setLastChipAreaUsed(chipArea: ChipArea3D): void;

        /** Creates and stores a new Chip area */
        createChipArea(settings: ChipArea3D.Settings, runtimeData: ChipArea3D.RuntimeData): ChipArea3D;

        /** Returns a chip Area by its Id */
        getChipAreaById(id: string): ChipArea3D;

        initialise(settings: IChips3D.InitSettings): void;
    }

    export const IChips3D = Strategy.declare<IChips3D>(Strategy.Type.Singleton, false, true);

    export namespace IChips3D
    {
        export interface InitSettings extends IChips.InitSettings
        {
            stage: RS.Rendering.IStage;
        }
    }
}
