namespace RS.Table
{
    /**
     * Physical Area where the chips are placed.
     * Accepts chips drop on, dragging them from and adding by clicking on it
     */
    @HasCallbacks
    export class ChipArea3D implements IDisposable
    {
        protected _onChipsAdded = createSimpleEvent();
        public get onChipsAdded() { return this._onChipsAdded.public; }

        protected _onChipsRemoved = createSimpleEvent();
        public get onChipsRemoved() { return this._onChipsRemoved.public; }

        protected _onChipsError = createEvent<ChipArea3D.ErrorEvent>();
        public get onChipsError() { return this._onChipsError.public; }

        protected readonly _settings: ChipArea3D.Settings;
        protected readonly _runtimeData: ChipArea3D.RuntimeData;

        protected _storedChips: Array<Chip> = [];
        protected _lastBet: Array<Chip> = [];
        protected _visual: Rendering.Chips.ChipArea3D;
        protected _mirroredAreas: Array<ChipArea3D> = [];

        protected _clickFunction: () => void;

        protected _disposed: boolean = false;
        protected _saveEnabled: boolean = true;
        protected _enabled: boolean = true;
        // Freezing a bet area will make the Chips system
        // to ignore it when doing checks
        protected _freeze: boolean = false;

        /** Gets if this element is disposed. */
        public get isDisposed() { return this._disposed; }

        /** Set/Gets if the area is enabled */
        public get enabled() { return this._enabled; }
        public set enabled(value: boolean)
        {
            this._enabled = value;
            this._visual.interactiveChildren = value;
        }

        /** Set/Gets if the area is frozen */
        public get frozen() { return this._freeze; }
        public set frozen(value: boolean)
        {
            this.enabled = !value;
            this._freeze = value;
        }

        /** Set/Gets the area id*/
        public get id(): string { return this._settings.id; }
        public set id(value: string)
        {
            IChips3D.get().changeChipAreaId(this._settings.id, value);
            this._settings.id = value;
        }

        /** Set/Gets the area visual component */
        public get visual() { return this._visual; }
        public set visual(value)
        {
            this._visual = value;
        }

        /** Gets the tooltip container */
        public get toolTipContainer() { return this._visual.tooltipContainer; }

        /** Set/Gets the click function */
        public get clickFunction() { return this._clickFunction; }
        public set clickFunction(value)
        {
            this._clickFunction = value;
        }

        constructor(settings: ChipArea3D.Settings, runtimeData: ChipArea3D.RuntimeData)
        {
            this._settings = settings;
            this._runtimeData = runtimeData;

            if (this._settings.saveEnabled !== undefined)
            {
                this._saveEnabled = this._settings.saveEnabled;
            }

            this.createVisual();

            this._clickFunction = this.defaultClickFunctionality;
        }

        /**
         * Multiply bet. Adds a special chip that make the total bet
         * calculation to be multiplied by its multiplier when found
         */
        public multiplyBet(multiplier: number, groupId?: number): void
        {
            const chipSettings: Chip.Settings = {
                value: 0,
                multiplier: multiplier,
                group: groupId
            }

            const chip: Chip = new Chip(chipSettings);
            this._storedChips.push(chip);

            this.recreateVisualChips();
        }

        /**
         * Returns the total bet value on this bet area
         */
        public getTotalBet(chips: Chip[] = this._storedChips): number
        {
            let totalChipValues: number = 0;
            for (let chip of chips)
            {
                if (chip.multiplier)
                {
                    totalChipValues *= chip.multiplier;
                }
                else
                {
                    totalChipValues += chip.value;
                }
            }
            return totalChipValues;
        }

        /**
         * Clear all bets
         */
        public clearBets(): void
        {
            this._storedChips = [];
            this.recreateVisualChips();

            this._onChipsRemoved.publish();
        }

        /**
         * Undo last Bet
         */
        public undoBets(): Chip
        {
            const chip: Chip = this._storedChips.pop();

            this.recreateVisualChips();
            this.handleMirroredAreas();

            this._onChipsRemoved.publish();
            return chip;
        }

        /**
         * Removes all the Group Bets
         */
        public removeAllGroupBets(groupId: number)
        {
            for (let i: number = 0; i < this._storedChips.length; i++)
            {
                const storedChip: Chip = this._storedChips[i];
                if (storedChip.group === groupId)
                {
                    this._storedChips.splice(i, 1);
                    i--;
                }
            }
            this.recreateVisualChips();
        }

        /**
         *
         * @param startValueOverride optional - the start value to be added to when checking if rebetting will breach max bet - by default will be the total chips in the area
         */
        public canRebet(startValueOverride?: number)
        {
            return !this.exceedsMaxBet(this.getTotalBet(this._lastBet), startValueOverride);
        }

        /**
         * Rebet last Bet
         * @param bypassChecks - whether to bypass any min/max bet checks that would prevent rebetting
         */
        public async reBet(groupId: number, bypassChecks: boolean = false)
        {
            if (this._lastBet.length > 0)
            {
                if (!this._settings.newRebet)
                {
                    this._storedChips = [];
                }
                for (let savedChip of this._lastBet)
                {
                    if (!bypassChecks && this.checkForErrors(savedChip.value))
                    {
                        return;
                    }
                    const chipSettings: Chip.Settings = {
                        value: savedChip.value,
                        multiplier: savedChip.multiplier,
                        group: groupId
                    }
                    const chip: Chip = new Chip(chipSettings);
                    this._storedChips.push(chip);
                }
                this.recreateVisualChips();
                this._onChipsAdded.publish();

                if (!this._settings.visualSettings.tooltip.alwaysVisible)
                {
                    await this.hideTooltip(200);
                }
            }
        }

        /**
         * Save bets and clears arrays
         */
        public saveAndClear()
        {
            if (this._saveEnabled)
            {
                this.saveBet();
                this._storedChips = [];
                this.recreateVisualChips();
            }
        }

        /**
         * Save bets
         */
        public save()
        {
            if (this._saveEnabled)
            {
                this.saveBet();
            }
        }

        /**
         * Clears the saved bet
         */
        public clearSavedBet(): void
        {
            this._lastBet = [];
        }

        /**
         * Clear visual chips
         * Removes the visual chips but they are still stored
         */
        public clearVisualBets(): void
        {
            this._visual.clearChips();
            this._visual.showTooltip(null);
        }

        /**
         * Checks to see if adding an amount will exceed the max bet.
         * @param selectedChipValue the value to add to the total
         * @param startValue the start value to be added to - the total bet by default
         */
        public exceedsMaxBet(selectedChipValue: number = 0, startValue: number = this.getTotalBet()): boolean
        {
            return (startValue + selectedChipValue > this._runtimeData.maxBet);
        }

        /**
         * Checks to see if adding an amount will exceed the min bet
         * @param selectedChipValue the value to add to the total
         */
        public exceedsMinBet(selectedChipValue: number = 0, publishError: boolean = false): boolean
        {
            const totalChipValues: number = this.getTotalBet();
            if (totalChipValues + selectedChipValue < this._runtimeData.minBet)
            {
                if (publishError)
                {
                    this._onChipsError.publish({ error: this._settings.errors.minStakeError });
                    Log.warn("Max bet reached on this bet area");
                }
                return false;
            }
            return true;
        }

        /**
         * Sets another bet area to mirror
         */
        public addMirroredBetArea(chipArea: ChipArea3D)
        {
            if (this._mirroredAreas.indexOf(chipArea) === -1)
            {
                this._mirroredAreas.push(chipArea);
                this.handleMirroredAreas();
            }
        }

        /**
         * Returns if the bet area has chips on it
         */
        public hasChips(): boolean
        {
            return this._storedChips.length > 0 ? true : false;
        }

        /**
         * Returns if the bet area has rebet chips stored
         */
        public hasRebetChips(): boolean
        {
            return this._lastBet.length > 0 ? true : false;
        }

        /**
         * Adds a new Chip to the chip total and recreate visuals to best represent that value
         */
        public async addChip(value: number)
        {
            if (this.checkForErrors(value))
            {
                return;
            }

            const chipSettings: Chip.Settings = {
                value: value
            }

            const chip: Chip = new Chip(chipSettings);
            this._storedChips.push(chip);

            this.recreateVisualChips(this._settings.dropChips);

            IChips3D.get().setLastChipAreaUsed(this);

            if (!this._settings.visualSettings.tooltip.alwaysVisible)
            {
                await this.hideTooltip(200);
            }
        }

        /**
         * Updates the tooltip position to be relative to the bet area
         */
        public setTooltipPosition(position: Math.Vector2D)
        {
            this._visual.setTooltipPosition(position);
        }

        /**
         * Disposes this element.
         */
        public dispose(): void
        {
            if (this.isDisposed) { return; }

            this.visual.dispose();
            this.visual = null;

            this._disposed = true;
        }

        public showTooltip()
        {
            const value = this.getTotalBet() > 0 ? IChips3D.get().currencyFormatter.format(this.getTotalBet()) : null;
            this.visual.showTooltip(value);
        }

        /**
         * Checks if the chip to be placed is will create an error
         */
        protected checkForErrors(chipAdded: number): boolean
        {
            //Check if bet is above max bet
            if (this.exceedsMaxBet(chipAdded))
            {
                this._onChipsError.publish({ error: this._settings.errors.maxStakeError });
                Log.warn("Max bet reached on this bet area");
                return true;
            }
            return false;
        }

        /**
         * Creates the visual part of the chip area and set up the listeners
         */
        protected createVisual()
        {
            this._visual = new Rendering.Chips.ChipArea3D(this._settings.visualSettings, this._runtimeData.stage);

            // Add listeners
            const onClicked = this._settings.roulette ?
                this._visual.onDown(this.handleAreaClicked) :
                this._visual.onClicked(this.handleAreaClicked);
            Disposable.bind(onClicked, this._visual);
            const onOver = this._visual.onOver(this.handleAreaEnter);
            Disposable.bind(onOver, this._visual);
            const onOut = this._visual.onOut(this.handleAreaLeave);
            Disposable.bind(onOut, this._visual);
        }

        protected async hideTooltip(delay: number = 0)
        {
            await Tween.wait(delay);
            this.visual.hideTooltip();
        }

        /**
         * Handles the behaviour when the chip area is clicked
         */
        @Callback
        protected handleAreaClicked(ev: RS.Rendering.InteractionEventData)
        {
            if (!IChips3D.get().enabled)
            {
                Log.debug("Chips System not enabled");
                return
            }

            if (!this._enabled)
            {
                Log.debug("Button " + this.id + " not enabled");
                return
            }

            Log.debug("Area " + this.id + " clicked");
            this._clickFunction();
        }

        /**
         * Default functionality for the bet area when is clicked
         */
        protected defaultClickFunctionality()
        {
            const totalChipValues: number = this.getTotalBet();
            const selectedChipValue: number = IChips3D.get().selectedChipValue;

            if (this.checkForErrors(selectedChipValue))
            {
                return;
            }

            if (this._settings.sound.asset)
            {
                Audio.play(this._settings.sound.asset, this._settings.sound.settings);
            }

            this.addChip(selectedChipValue);

            this.handleMirroredAreas();
            this._onChipsAdded.publish();
        }

        /**
         * Iterates the mirrored areas and triggers their mirror functionality
         */
        protected handleMirroredAreas()
        {
            for (let mirrorArea of this._mirroredAreas)
            {
                mirrorArea.onMirroredBet(this);
            }
        }

        /**
         * Adds/Removes new chips when a mirrored bet area adds chips
         */
        protected async onMirroredBet(mirroredArea: ChipArea3D)
        {
            const selectedChipValue: number = IChips3D.get().selectedChipValue;
            const differentChips: number = this._storedChips.length - mirroredArea._storedChips.length
            if (differentChips < 0)
            {
                this.addChip(selectedChipValue);
            }
            else
            {
                this._storedChips.pop();
                this.recreateVisualChips();
            }
            const totalBet: number = this.getTotalBet();
            const tooltipValue = totalBet > 0 ? IChips3D.get().currencyFormatter.format(totalBet) : null;
            this.visual.showTooltip(tooltipValue);

            if (!this._settings.visualSettings.tooltip.alwaysVisible)
            {
                await this.hideTooltip(200);
            }
        }

        /**
         * Handles the behaviour when the mouse enters the area
         */
        @Callback
        protected handleAreaEnter(ev: RS.Rendering.InteractionEventData)
        {
            if (!IChips3D.get().enabled)
            {
                Log.debug("Chip System not enabled");
                return
            }
            const totalBet: number = this.getTotalBet();
            const tooltipValue = totalBet > 0 ? IChips3D.get().currencyFormatter.format(totalBet) : null;
            this.visual.showTooltip(tooltipValue);
        }

        /**
         * Handles the behaviour when the mouse enters the area
         */
        @Callback
        protected handleAreaLeave(ev: RS.Rendering.InteractionEventData)
        {
            if (!IChips3D.get().enabled)
            {
                Log.debug("Chip System not enabled");
                return
            }

            if (!this._settings.visualSettings.tooltip.alwaysVisible)
            {
                this.visual.hideTooltip();
            }
        }

        /**
         * Calculates the visual chips we need to represent the total bet on this area
         * using the minimum amount of available chips
         */
        protected recreateVisualChips(dropAnimation: boolean = false)
        {
            let totalChipValues: number = this.getTotalBet();
            const totalBet: number = totalChipValues;

            let chipsSettings: Array<Rendering.Chips.Chip3D.Settings> = [];
            const chipValues: number[] = IChips3D.get().chipValues;
            const chipStackValues: number[] = [];

            // Find minimum multipliers
            const multipliers = [chipValues[0]];
            for (let i: number = 1; i < chipValues.length; i++)
            {
                const possibleMultiplier = chipValues[i];
                let isMultiplier = true;
                for (const multiplier of multipliers)
                {
                    if ((possibleMultiplier % multiplier) === 0)
                    {
                        isMultiplier = false;
                        break;
                    }
                }
                if (isMultiplier)
                {
                    multipliers.push(possibleMultiplier);
                }
            }

            while (totalChipValues > 0)
            {
                let maxChipValue: number;
                let frameIndex: number;
                let error: boolean = true;
                for (frameIndex = chipValues.length - 1; frameIndex >= 0; frameIndex--)
                {
                    maxChipValue = chipValues[frameIndex];
                    let possibleNextValue: number = (totalChipValues - maxChipValue);
                    let isMultiplier = false;
                    for (const multiplier of multipliers)
                    {
                        if ((possibleNextValue % multiplier) === 0)
                        {
                            isMultiplier = true;
                            break;
                        }
                    }
                    if (maxChipValue <= totalChipValues && isMultiplier)
                    {
                        error = false;
                        break;
                    }
                }
                if (error)
                {
                    Log.error("It was an error recreating the visual Chips for betArea " + this._settings.id);
                    Log.error("The script ran into an infinite loop");
                    return;
                }
                chipStackValues.push(maxChipValue);
                totalChipValues -= maxChipValue;
            }

            chipStackValues.sort((a, b) => a - b);
            const chipSpriteSheet: RS.Rendering.ISpriteSheet = IChips3D.get().getChipSpriteSheet();
            for (let i = chipStackValues.length - 1; i >= 0; i--)
            {
                const chipStackValue = chipStackValues[i];
                const chipValue: string = i > 0 ? "" : IChips3D.get().currencyFormatter.format(chipStackValue, false, true)
                const frameIndex = chipValues.indexOf(chipStackValue) + (this._settings.chipIndexOffset | 0);
                const chipSetting: Rendering.Chips.Chip3D.Settings = {
                    texture: chipSpriteSheet.getFrame(frameIndex).imageData,
                    value: chipValue,
                    chip: this._settings.chipSettings,
                    textSettings: this._settings.chipTextSettings
                }
                chipsSettings.push(chipSetting);
            }
            this.visual.recreateChips(chipsSettings);
            const tooltipValue = totalBet > 0 ? IChips3D.get().currencyFormatter.format(totalBet) : null;
            this.visual.showTooltip(tooltipValue, !this._settings.visualSettings.tooltip.alwaysVisible);
        }

        /**
         * Save current Bet
         */
        protected saveBet(): void
        {
            this._lastBet = [];
            for (let storedChip of this._storedChips)
            {
                const chipSettings: Chip.Settings = {
                    value: storedChip.value,
                    multiplier: storedChip.multiplier
                }
                const chip: Chip = new Chip(chipSettings);
                this._lastBet.push(chip);
            }
        }
    }

    export namespace ChipArea3D
    {
        export interface ErrorEvent
        {
            error: Localisation.LocalisableString;
        }

        export interface Settings
        {
            id: string;
            visualSettings: Rendering.Chips.ChipArea3D.Settings;
            saveEnabled?: boolean;
            /**
             * Whether or not to use new rebet function:
             *  Rebet gets added instead of replaced
             */
            newRebet?: boolean;
            chipTextSettings?: Flow.Label.Settings;
            chipIndexOffset?: number;
            sound:
            {
                asset: Asset.Reference<"sound">;
                settings: Partial<Audio.PlaySettings>;
            };
            errors:
            {
                minStakeError: Localisation.LocalisableString;
                maxStakeError: Localisation.LocalisableString;
            };
            chipSettings:
            {
                radius: number;
                divisions?: number;
                uvRadius?: number;
            };
            dropChips?: boolean;
            /** set true to place chip onDown rather than onClick. Enables for drag functionality */
            roulette?: boolean;
        }

        export interface RuntimeData
        {
            maxBet: number;
            minBet: number;
            stage: RS.Rendering.IStage;
        }
    }
}
