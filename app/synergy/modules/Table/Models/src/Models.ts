/// <reference path="StakeRange.ts" />

namespace RS.Table
{
    export interface Models extends RS.Models
    {
        stake: Models.Stake;
        gameResults: RS.Models.GameResults;
        stakeRanges: Models.StakeRanges;
    }

    export namespace Models
    {
         /** Populates the models with default values. */
        export function clear(models: Models): void
        {
            RS.Models.clear(models);

            models.stake = models.stake || {} as Models.Stake;
            Models.Stake.clear(models.stake);

            models.gameResults = models.gameResults || [];
            RS.Models.GameResults.clear(models.gameResults);

            models.stakeRanges = models.stakeRanges || [];
            Models.StakeRanges.clear(models.stakeRanges);
        }

        export namespace State
        {
            //backwards compatibility
            export import Type = RS.Models.State.Type;
        }
    }
}
