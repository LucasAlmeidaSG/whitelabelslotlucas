namespace RS.Table.Models
{
    export interface Card
    {
        suit: Card.Suit;
        value: number;
    }

    export function isCard(value: any): value is Models.Card
    {
        return Is.object(value) &&
            Object.keys(value).length === 2 &&
            Is.number(value.value) &&
            Is.string(value.suit);
    }

    export namespace Card
    {
        export enum Suit
        {
            DIAMONDS = "DIAMONDS",
            CLUBS = "CLUBS",
            HEARTS = "HEARTS",
            SPADES = "SPADES"
        }

        /**
         * Converts a glm card e.g. "3C" to the card model
         */
        export function convertGLMCardtoGame(card: string): Card
        {
            if (card.length != 2)
            {
                Log.error("[Util.formatCard] Invalid Card: " + card);
                return;
            }
            else if (card === "XX")
            {
                return {suit: null, value: null}
            }

            let value: number;
            const valueStr = card.charAt(0);
            switch (valueStr)
            {
                case "T": value = 9; break;
                case "J": value = 10; break;
                case "Q": value = 11; break;
                case "K": value = 12; break;
                case "A": value = 0; break;
                // -1 as A is at 0 instead of 1
                default: value = parseInt(valueStr) - 1;
            }

            //"D", "C", "H", "S"
            const suitStr = card.charAt(1);
            let suit: Suit;
            switch (suitStr)
            {
                case "D": suit = Suit.DIAMONDS; break;
                case "C": suit = Suit.CLUBS; break;
                case "H": suit = Suit.HEARTS; break;
                case "S": suit = Suit.SPADES; break;
            }

            return {suit, value};
        }

        export function convertGameCardtoGLM(suit: Suit, value: number): string
        {
            let valueStr: string;
            switch (value)
            {
                case 9: valueStr = "T"; break;
                case 10: valueStr = "J"; break;
                case 11: valueStr = "Q"; break;
                case 12: valueStr = "K"; break;
                case 0: valueStr = "A"; break;
                // + 1 as A is at 0 and when converting we -1
                default: valueStr = (value + 1).toString();
            }

            let suitStr: string;
            switch (suit)
            {
                case Suit.DIAMONDS: suitStr = "D"; break;
                case Suit.CLUBS: suitStr = "C"; break;
                case Suit.HEARTS: suitStr = "H"; break;
                case Suit.SPADES: suitStr = "S"; break;
            }

            return `${valueStr}${suitStr}`;
        }

        export function generateRandomCard(): Card
        {
            const suits = Object.keys(Suit);
            const key = suits[Math.generateRandomInt(0, suits.length)];
            const suit = Suit[key];
            const value = RS.Math.generateRandomInt(0, 13);

            return {suit, value};
        }

        export function isDuplicateCard(a: Card, b: Card): boolean
        {
            return a.suit === b.suit && a.value === b.value;
        }

        export function hasDuplicateCards(hand: Card[]): boolean
        {
            const cards: Card[] = [];
            const duplicates: Card[] = [];
            hand.forEach((card) =>
            {
                if (ArrayUtils.contains(cards, card, isDuplicateCard))
                {
                    duplicates.push(card);
                }
                else
                {
                    cards.push(card);
                }
            });
        ​
            for (const card of duplicates)
            {
                Log.debug(`[Models.Card] duplicate card: ${card.suit}${card.value}`);
            }

            return duplicates.length > 0;
        }

        export function checkCardExists(cards: Card[], card: Card): boolean
        {
            for (const c of cards)
            {
                if (isDuplicateCard(c, card))
                {
                    return true;
                }
            }
            return false;
        }
    }
}