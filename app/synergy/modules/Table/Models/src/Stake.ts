namespace RS.Table.Models
{
    /**
     * Model for chips data, this min/max bet is only used if specific min/max bets are not provided
     * for individual chip areas in the StakeRanges tag. Usually for roulette etc.
     */
    export interface Stake extends RS.Models.Stake
    {
        /** Minimum bet */
        minBet: number;

        /** Maximum bet */
        maxBet: number;
    }

    export namespace Stake
    {
        /** Populates the customer model with default values. */
        export function clear(stake: Stake): void
        {
            stake.minBet = 0;
            stake.maxBet = 0;
            RS.Models.Stake.clear(stake);
        }

        export import getCurrentBet = RS.Models.Stake.getCurrentBet;
    }
}
