namespace RS.Table.Models
{
    export namespace StakeRange
    {
        export function clear(stakeRange: StakeRange): void
        {
            stakeRange.maxBet = 0;
            stakeRange.maxPayout = 0;
            stakeRange.type = "";
            stakeRange.minBet = 0;
        }
    }

    /**
     * Model for the ranges of stakes each bet type can have. In card games min/max bet are found here.
     */
    export interface StakeRange
    {
        /** The type of the stake, usually the bet type. Ante, Insurance etc. */
        type: string;

        /** Minimum bet */
        minBet: number;

        /** Maximum bet */
        maxBet: number;

        /** Maximum payout */
        maxPayout: number
    }

    /**
     * Multiple stake ranges
     */
    export type StakeRanges = StakeRange[];

    export namespace StakeRanges
    {
        /** Populates the stake ranges model with default values. */
        export function clear(stakeRanges: StakeRanges): void
        {
            stakeRanges.length = 0;
        }

        export function getStakeRangeByName(stakeRanges: StakeRanges, name: string): StakeRange
        {
            for (const stakeRange of stakeRanges)
            {
                if (stakeRange.type === name)
                {
                    return stakeRange;
                }
            }
        }
    }
}
