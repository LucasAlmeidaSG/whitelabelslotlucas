namespace RS.Table.Rendering.Chips
{
    @HasCallbacks
    export class Chip3D extends RS.Rendering.Scene.SceneObject
    {
        protected readonly _settings: Chip3D.Settings;
        protected readonly _stage: RS.Rendering.IStage;

        private _chip: RS.Rendering.Scene.IMeshObject;
        private _chipMaterial: RS.Rendering.Scene.Material;
        private _text: Flow.Label;
        private _texture: RS.Rendering.IRenderTexture;
        private _bitmap: Flow.Image;

        constructor(settings: Chip3D.Settings, stage: RS.Rendering.IStage)
        {
            super();

            this._settings = settings;
            this._stage = stage;

            //chip params from American Roulette
            const chipGeo = RS.Rendering.Scene.Geometry.createCircle(settings.chip.radius, settings.chip.divisions, settings.chip.uvRadius);
            this._chipMaterial = this.createChipMaterial();
            this._chip = new RS.Rendering.Scene.MeshObject(chipGeo, [this._chipMaterial]);
            this._chip.scale.set(1, 1, -1); //unflip

            if (this._settings.scale)
            {
                this.scale.set(this._settings.scale);
            }

            this.addChild(this._chip);
        }

        public updateText(value: string)
        {
            this._text.text = value;
            this._bitmap.invalidateLayout();
            this.updateChipMaterial();
        }

        protected createChipMaterial(): RS.Rendering.Scene.Material
        {
            this._bitmap = RS.Flow.image.create(
                {
                    kind: RS.Flow.Image.Kind.Texture,
                    texture: this._settings.texture,
                    sizeToContents: false,
                    size: {w: this._settings.texture.width * 0.9, h: this._settings.texture.height * 0.9}
                });

            this._text = Flow.label.create(
                {
                    sizeToContents: false,
                    size: { w: this._bitmap.width * 0.9, h: this._bitmap.height * 0.9 },
                    ...this._settings.textSettings,
                    text: this._settings.value,
                }, this._bitmap);
            this._bitmap.invalidateLayout();

            this._texture = new RS.Rendering.RenderTexture(this._bitmap.width, this._bitmap.height);
            this._bitmap.renderToTexture(this._texture, false, this._stage);

            //fixes text not being positioned properly
            ITicker.get().once(this.updateChipMaterial);

            return {
                model: RS.Rendering.Scene.Material.Model.Unlit,
                diffuseMap: this._texture,
                blend: RS.Rendering.Scene.Material.Blend.Opaque
            }
        }

        @Callback
        protected updateChipMaterial()
        {
            this._texture.clear(this._stage, new RS.Util.Color(0, 0, 0, 0));
            this._bitmap.renderToTexture(this._texture, false, this._stage);
            this._chipMaterial.diffuseMap = this._texture;
        }
    }

    export namespace Chip3D
    {
        export interface Settings
        {
            scale?: Math.Vector3D;
            chip:
            {
                radius: number;
                divisions?: number;
                uvRadius?: number;
            };
            texture: RS.Rendering.ISubTexture;
            value: string;
            textSettings: Flow.Label.Settings;
        }
    }
}
