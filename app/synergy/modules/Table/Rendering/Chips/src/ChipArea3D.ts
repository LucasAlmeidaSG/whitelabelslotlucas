namespace RS.Table.Rendering.Chips
{
    export class ChipArea3D extends RS.Rendering.Scene.SceneObject
    {
        public get onDown() { return this._backgroundPlane.onDown; }
        public get onUp() { return this._backgroundPlane.onUp; }
        public get onUpOutside() { return this._backgroundPlane.onUpOutside; }
        public get onClicked() { return this._backgroundPlane.onClicked; }
        public get onOver() { return this._backgroundPlane.onOver; }
        public get onOut() { return this._backgroundPlane.onOut; }

        protected readonly _settings: ChipArea3D.Settings;
        protected readonly _stage: RS.Rendering.IStage;

        protected _visualChips: Array<Chip3D> = [];

        protected _chipContainer: RS.Rendering.Scene.ISceneObject;

        protected _backgroundPlane: RS.Rendering.Scene.IMeshObject;
        protected _backgroundContainer: Flow.Container;
        protected _backgroundTexture: RS.Rendering.IRenderTexture;

        protected _background: Flow.Background;
        public get background() { return this._background; }

        protected _hover: Flow.Background;

        protected _tooltipContainer: RS.Flow.Container;
        protected _tooltipText: Flow.Label;
        public get tooltipContainer() { return this._tooltipContainer; }

        @AutoDisposeOnSet protected _tooltipTween: Tween<Flow.Container>;
        @AutoDisposeOnSet protected _dropTween: Tween<Math.IObservableVector3D>;

        constructor(settings: ChipArea3D.Settings, stage: RS.Rendering.IStage)
        {
            super("Chip Area 3D");

            this.position.set(settings.position);
            this.scale.set(settings.scale);

            this._settings = settings;
            this._stage = stage;

            this.createBackground();
            this.createTooltip();

            this._chipContainer = new RS.Rendering.Scene.SceneObject("Chip Container");
            this.addChild(this._chipContainer);
        }

        public setTooltipPosition(position: Math.Vector2D)
        {
            this._tooltipContainer.x = position.x;
            this._tooltipContainer.y = position.y;
        }

        public dispose()
        {
            super.dispose();
        }

        public clearChips(): void
        {
            if (this._dropTween)
            {
                this._dropTween.finish();
                this._dropTween = null;
            }

            for (const chip of this._visualChips)
            {
                chip.dispose();
            }
            this._visualChips.length = 0;
            this._chipContainer.removeAllChildren();
        }

        public recreateChips(chipSettings: Chip3D.Settings[])
        {
            this.clearChips();

            const offset: Math.Vector3D = Math.Vector3D();
            let chip: Chip3D;
            for (const settings of chipSettings)
            {
                chip = new Chip3D(settings, this._stage);
                offset.y += this._settings.chipHeight;
                chip.position.set(offset);
                this._chipContainer.addChild(chip);
                this._visualChips.push(chip);
            }
            if (chip)
            {
                if (this._dropTween)
                {
                    this._dropTween.finish();
                }
                this._dropTween = Tween.get(chip.position)
                    .set({y: chip.position.y + this._settings.drop.height})
                    .to({y: chip.position.y}, this._settings.drop.duration, Ease.backIn);
            }
        }

        public showTooltip(value?: string, hideAfter: boolean = false)
        {
            this._tooltipTween = null;

            if (this._tooltipContainer)
            {
                if (value !== null)
                {
                    this._tooltipText.text = value;
                    if (this._tooltipContainer.alpha < 1)
                    {
                        this._tooltipContainer.alpha = 0;
                    }
                    this._tooltipTween = Tween.get(this._tooltipContainer)
                        .to({alpha: 1}, this._settings.tooltip.tweenTime, Ease.backOut)
                        .call(() =>
                        {
                            if (hideAfter)
                            {
                                this.hideTooltip();
                            }
                        });
                }
                else
                {
                    this._tooltipContainer.alpha = 0;
                }
            }
            else
            {
                Log.warn("showTooltip called on 3D chip area without a tooltip");
            }
        }

        public hideTooltip()
        {
            this._tooltipTween = null;

            if (this._tooltipContainer)
            {
                if (this._tooltipContainer.alpha > 0)
                {
                    this._tooltipContainer.alpha = 1;
                }
                this._tooltipTween = Tween.get(this._tooltipContainer)
                    .to({ alpha: 0 }, this._settings.tooltip.tweenTime, Ease.backIn);
            }
            else
            {
                Log.warn("hideTooltip called on 3D chip area without a tooltip");
            }
        }

        protected createBackground()
        {
            const settings = this._settings.background;
            this._backgroundContainer = Flow.container.create(settings.container);
            this._background = Flow.background.create(settings.background, this._backgroundContainer);

            this._backgroundContainer.invalidateLayout();
            this._backgroundTexture = new RS.Rendering.RenderTexture(this._backgroundContainer.width as number, this._backgroundContainer.height as number);
            this.updateRenderTextures();

            const scale = this._settings.globalScale == null ? 1 : this._settings.globalScale;
            const planeGeo = RS.Rendering.Scene.Geometry.createPlane({x: this._backgroundContainer.width * scale, y: this._backgroundContainer.height * scale});
            const planeMat: RS.Rendering.Scene.Material =
            {
                model: RS.Rendering.Scene.Material.Model.Unlit,
                diffuseMap: this._backgroundTexture,
                blend: RS.Rendering.Scene.Material.Blend.Transparent
            }
            this._backgroundPlane = new RS.Rendering.Scene.MeshObject(planeGeo, [planeMat]);
            this._backgroundPlane.interactive = true;

            if (settings.hover)
            {
                this._hover = Flow.background.create(settings.hover, this._backgroundContainer);
                this._hover.visible = false;
                this._backgroundPlane.onOver(() =>
                {
                    this._hover.visible = true;
                    this.updateRenderTextures();
                });
                this._backgroundPlane.onOut(() =>
                {
                    this._hover.visible = false;
                    this.updateRenderTextures();
                });
            }

            this.addChild(this._backgroundPlane);
            this.updateRenderTextures();
        }

        protected createTooltip()
        {
            const settings = this._settings.tooltip;
            if (!settings) { return; }

            this._tooltipContainer = Flow.container.create(settings.container);
            Flow.background.create(settings.background, this._tooltipContainer);
            this._tooltipText = Flow.label.create(settings.text, this._tooltipContainer);

            this._tooltipContainer.invalidateLayout();
            this._tooltipContainer.alpha = 0;
        }

        protected updateRenderTextures()
        {
            this._backgroundTexture.clear(this._stage, new RS.Util.Color(0, 0, 0, 0));
            this._backgroundTexture.render(this._stage, this._backgroundContainer);
        }
    }

    export namespace ChipArea3D
    {
        export interface Settings
        {
            position: Math.Vector3D;
            scale: Math.Vector3D;
            chipHeight: number;
            stackOffset: Math.Vector3D;

            /** scale used when rendering 2d objects onto a plane */
            globalScale?: number;

            background:
            {
                container: Flow.Container.Settings;
                background: Flow.Background.Settings;
                hover?: Flow.Background.Settings;
            };

            tooltip?:
            {
                container: Flow.Container.Settings;
                background: Flow.Background.Settings;
                text: Flow.Label.Settings;
                tweenTime: number;
                alwaysVisible?: boolean
            };

            drop:
            {
                duration: number;
                height: number;
            };
        }
    }
}
