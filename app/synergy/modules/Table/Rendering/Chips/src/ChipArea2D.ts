namespace RS.Table.Rendering.Chips
{
    export class ChipArea2D extends Flow.BaseElement<ChipArea2D.Settings>
    {
        public get onClicked() { return this._button.onClicked; }
        public get onOver() { return this._button.onOver; }
        public get onOut() { return this._button.onOut; }
        public get onPressed() { return this._button.onPressed; }
        public get onReleased() { return this._button.onReleased; }

        protected _enabled: boolean = true;
        public get enabled() { return this._enabled; }
        public set enabled(val)
        {
            if (val === this._enabled) { return; }
            this._enabled = val;
            this._button.enabled = val;
            if (this.settings.hideButtonOnDisable)
            {
                this._button.visible = val;
            }
        }

        @AutoDisposeOnSet
        protected _chipContainer: Flow.Container;
        @AutoDisposeOnSet
        protected _visualChips: Array<Chip2D> = [];

        @AutoDisposeOnSet
        protected _tooltipContainer: Flow.Container;
        @AutoDisposeOnSet
        protected _tooltipText: Flow.Label;
        @AutoDisposeOnSet
        protected _tooltipBackground: OneOrMany<Flow.Background>;
        public get tooltipContainer() { return this._tooltipContainer; }

        @AutoDisposeOnSet
        protected _labelBackground: Flow.Background;
        public get labelBackground() { return this._labelBackground; }

        @AutoDisposeOnSet
        protected _button: Flow.Button;

        @AutoDisposeOnSet
        protected _tooltipTween: Tween<Flow.Container>;
        @AutoDisposeOnSet
        protected _dropTween: Tween<Chip2D>;

        constructor(settings: ChipArea2D.Settings)
        {
            super(settings, null);

            this.suppressLayouting();
            this._button = Flow.button.create(settings.button, this);
            this._chipContainer = Flow.container.create(settings.chipsContainer, this);
            this.fixLabel();
            this.createTooltip();

            this.restoreLayouting();
        }

        public apply(settings: Partial<ChipArea2D.Settings>)
        {
            this.suppressLayouting();
            if (settings.chipsContainer && this._chipContainer) { this._chipContainer.apply(settings.chipsContainer); }
            if (settings.button && this._button) { this._button.apply(settings.button); }
            if (settings.labelBackground && this._labelBackground) { this._labelBackground.apply(settings.labelBackground); }
            if (settings.tooltip)
            {
                if (settings.tooltip.container && this._tooltipContainer) { this._tooltipContainer.apply(settings.tooltip.container); }
                if (settings.tooltip.background && this._tooltipBackground)
                {
                    if (Is.array(settings.tooltip.background))
                    {
                        if (Is.array(this._tooltipBackground))
                        {
                            for (let i = 0; i < settings.tooltip.background.length; i++)
                            {
                                this._tooltipBackground[i].apply(settings.tooltip.background[i]);
                            }
                        }
                        else
                        {
                            Log.warn("Tried to apply multiple background settings to single background");
                        }
                    }
                    else
                    {
                        if (Is.array(this._tooltipBackground))
                        {
                            for (const background of this._tooltipBackground)
                            {
                                background.apply(settings.tooltip.background);
                            }
                        }
                        else
                        {
                            this._tooltipBackground.apply(settings.tooltip.background);
                        }
                    }
                }
                if (settings.tooltip.text && this._tooltipText){ this._tooltipText.apply({ ...settings.tooltip.text, text: this._tooltipText.text }); }
            }
            this.restoreLayouting();
            super.apply(settings);
        }

        public clearChips(): void
        {
            if (this._dropTween)
            {
                this._dropTween.finish();
                this._dropTween = null;
            }

            if (this._tooltipTween)
            {
                this._tooltipTween.finish();
            }

            this._visualChips = [];
            this._chipContainer.removeAllChildren();
        }

        public recreateChips(chipSettings: Chip2D.Settings[])
        {
            this.clearChips();

            const offset: Math.Vector2D = Math.Vector2D();
            let chip: Chip2D;
            for (const settings of chipSettings)
            {
                chip = new Chip2D(settings);
                chip.position.set(offset.x, offset.y);
                offset.x += this.settings.chipOffset.x + settings.offset.x;
                offset.y += this.settings.chipOffset.y + settings.offset.y;
                this._chipContainer.addChild(chip);
                this._visualChips.push(chip);
            }
            if (chip)
            {
                if (this._dropTween)
                {
                    this._dropTween.finish();
                }

                this._dropTween = Tween.get(chip)
                    .set({ y: chip.y + this.settings.drop.height })
                    .to({ y: chip.y }, this.settings.drop.duration, Ease.backIn);
            }
        }

        public showTooltip(value?: string, hideAfter: boolean = false)
        {
            if (this._tooltipContainer)
            {
                if (value !== null)
                {
                    this._tooltipText.text = value;
                    if (this._tooltipContainer.alpha < 1)
                    {
                        this._tooltipContainer.alpha = 0;
                    }
                    this._tooltipTween = Tween.get(this._tooltipContainer)
                        .to({alpha: 1}, this.settings.tooltip.tweenTime, Ease.backOut)
                        .call(() =>
                        {
                            if (hideAfter)
                            {
                                this.hideTooltip();
                            }
                        });
                }
                else
                {
                    this._tooltipContainer.alpha = 0;
                }
            }
            else
            {
                Log.warn("showTooltip called on 2D chip area without a tooltip");
            }
        }

        public hideTooltip()
        {

            if (this._tooltipContainer)
            {
                if (this._tooltipContainer.alpha > 0)
                {
                    this._tooltipContainer.alpha = 1;
                }
                this._tooltipTween = Tween.get(this._tooltipContainer)
                    .to({ alpha: 0 }, this.settings.tooltip.tweenTime, Ease.backIn);
            }
            else
            {
                Log.warn("hideTooltip called on 3D chip area without a tooltip");
            }
        }

        protected createTooltip()
        {
            const settings = this.settings.tooltip;
            if (!settings) { return; }

            this._tooltipContainer = Flow.container.create(settings.container);
            if (settings.background)
            {
                if (Is.array(settings.background))
                {
                    this._tooltipBackground = [];
                    if (Is.array(this._tooltipBackground))
                    {
                        for (const backgroundSettings of settings.background)
                        {
                            this._tooltipBackground.push(Flow.background.create(backgroundSettings, this._tooltipContainer));
                        }
                    }
                } else
                {
                    this._tooltipBackground = Flow.background.create(settings.background, this._tooltipContainer);
                }
            }
            this._tooltipText = Flow.label.create(settings.text, this._tooltipContainer);

            this._tooltipContainer.invalidateLayout();
            this._tooltipContainer.alpha = 0;
        }

        protected fixLabel()
        {
            if (!this.settings.labelBackground || !this._button.label) { return; }

            this._labelBackground = Flow.background.create(this.settings.labelBackground, this);
            this._labelBackground.addChild(this._button.label);
        }
    }

    export const chipArea2D = Flow.declareElement(ChipArea2D, false);

    export namespace ChipArea2D
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            chipsContainer: Flow.Container.Settings;
            chipOffset: Math.Vector2D;

            tooltip?:
            {
                container: Flow.Container.Settings;
                background?: OneOrMany<Flow.Background.Settings>;
                text: Flow.Label.Settings;
                tweenTime: number;
                alwaysVisible?: boolean;
            };

            button: Flow.Button.Settings;

            labelBackground?: Flow.Background.Settings;

            /** Chip Drop */
            drop:
            {
                duration: number;
                height: number;
            };

            /** Defaults to false, will show button's disabled state */
            hideButtonOnDisable?: boolean;
        }

        export const defaultSettings: Settings =
        {
            name: "ChipArea",
            dock: Flow.Dock.Float,
            sizeToContents: false,
            size: {w: 100, h: 100},

            chipsContainer:
            {
                name: "ChipsArea ChipsContainer",
                dock: Flow.Dock.Float,
                floatPosition: {x: 0.5, y: 0.5},
                scaleFactor: 0.85
            },
            chipOffset: {x: 0, y: -5},
            button:
            {
                name: "ChipArea Button",
                dock: Flow.Dock.Fill,
                background:
                {
                    dock: Flow.Dock.Fill,
                    kind: Flow.Background.Kind.SolidColor,
                    color: Util.Colors.darkgray,
                    borderSize: 0,
                    cornerRadius: 10,
                    alpha: 0.5
                },
                hoverbackground:
                {
                    dock: Flow.Dock.Fill,
                    kind: Flow.Background.Kind.SolidColor,
                    color: Util.Colors.lightgrey,
                    borderSize: 0,
                    cornerRadius: 10,
                    alpha: 0.5
                },
                hoverAnimation:
                {
                    delay: 1,
                    animate: async function(button: Flow.Button)
                    {
                        await Tween.get(button.postTransform)
                            .to({scaleX: 1.25, scaleY: 1.25}, 500)
                            .to({scaleX: 1, scaleY: 1}, 500)
                    }
                },
                textLabel: null,
                pressbackground: null,
                disabledbackground:
                {
                    dock: Flow.Dock.Fill,
                    kind: Flow.Background.Kind.SolidColor,
                    color: Util.Colors.lightgrey,
                    borderSize: 0,
                    cornerRadius: 10,
                    alpha: 0.2
                },
            },
            tooltip:
            {
                container:
                {
                    name: "ChipArea Tooltip Container",
                    dock: Flow.Dock.Float,
                    floatPosition: {x: 0.5, y: 0.75},
                    sizeToContents: true,
                    spacing: Flow.Spacing.all(20)
                },
                background:
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: Util.Colors.black,
                    borderSize: 0,
                    cornerRadius: 10,
                    alpha: 0.5
                },
                text:
                {
                    name: "ChipArea Tooltip Text",
                    dock: Flow.Dock.Fill,
                    fontSize: 30,
                    font: RS.Rendering.Assets.Fonts.MyriadPro.Regular,
                    text: "tooltip text"
                },
                tweenTime: 500
            },
            drop:
            {
                duration: 250,
                height: -5
            }
        };
    }
}
