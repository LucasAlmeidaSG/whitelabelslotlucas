namespace RS.Table.Rendering.Chips
{
    export class Chip2D extends RS.Rendering.DisplayObjectContainer
    {
        @AutoDisposeOnSet private _text: Flow.Label;
        @AutoDisposeOnSet private _chip: Flow.Image;

        constructor(protected readonly settings: Chip2D.Settings)
        {
            super();

            if (this.settings.scale)
            {
                this.scale.set(this.settings.scale.x, this.settings.scale.y);
            }

            this._chip = this.createChip();

            this.addChild(this._chip);
        }

        public updateText(value: string)
        {
            this._text.text = value;
            this._chip.invalidateLayout();
        }

        protected createChip(): RS.Flow.Image
        {
            this._chip = RS.Flow.image.create(
                {
                    kind: RS.Flow.Image.Kind.Texture,
                    texture: this.settings.texture
                });

            this._text = Flow.label.create(
                {
                    sizeToContents: false,
                    size: { w: this._chip.width * 0.9, h: this._chip.height * 0.9 },
                    ...this.settings.textSettings,
                    text: this.settings.value,
                }, this._chip);

            this._chip.invalidateLayout();

            return this._chip;
        }
    }

    export namespace Chip2D
    {
        export interface Settings
        {
            scale?: Math.Vector2D;
            offset?: Math.Vector2D;
            value: string;
            textSettings: Flow.Label.Settings;
            texture: RS.Rendering.ISubTexture;
        }
    }
}
