precision lowp float;

attribute vec2 aVertexPosition;
attribute vec2 aTextureCoord;
attribute vec4 aColor;
uniform mat3 projectionMatrix;
uniform mat3 modelViewMatrix;
uniform float rotationAngle;
uniform vec2 localPosition;
varying vec2 vTextureCoord;

void main(void){

    mat4 rotMat = mat4( 1.0, 0.0,                 0.0,                 0.0,
                        0.0, cos(rotationAngle),  -sin(rotationAngle), 0.0,
                        0.0, -sin(rotationAngle), cos(rotationAngle),  0.0,
                        0.0, 0.0,                 0.0,                 1.0);

    mat4 projection = mat4(3.0 / 4.0, 0.0, 0.0, 0.0,
                           0.0, 1.0, 0.0, 0.0,
                           0.0, 0.0, 1.0, 0.40,
                           0.0, 0.0, 0.0, 1.0);

    mat4 transToPosition = mat4( 1.0, 0.0, 0.0, localPosition.x,
    	                         0.0, 1.0, 0.0, localPosition.y,
    	                         0.0, 0.0, 1.0, 0.0,
    	                         0.0, 0.0, 0.0, 1.0);

    mat4 scale = mat4(4.0 / 3.0, 0.0, 0.0, 0.0,
                      0.0, 1.0, 0.0, 0.0,
                      0.0, 0.0, 1.0, 0.0,
                      0.0, 0.0, 0.0, 1.0);
    
    mat4 transToOrigin  = mat4( 1.0, 0.0, 0.0, 0.0,
      	                        0.0, 1.0, 0.0, 0.0,
      	                        0.0, 0.0, 1.0, 0.0,
      	                        -localPosition.x, -localPosition.y, 0.0, 1.0);

    vec4 pos = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
    gl_Position =  projection  * rotMat  * scale  * transToOrigin * pos * transToPosition ;

    vTextureCoord = aTextureCoord;
}