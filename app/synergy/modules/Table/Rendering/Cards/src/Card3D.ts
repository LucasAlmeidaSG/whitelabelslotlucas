namespace RS.Table.Rendering.Cards
{
    /**
     * 3D Card
     */
    export class Card3D extends RS.Rendering.Scene.SceneObject
    {
        protected _settings: Card3D.Settings;
        protected _stage: RS.Rendering.IStage;

        @AutoDisposeOnSet protected _back: RS.Rendering.Scene.IMeshObject;
        @AutoDisposeOnSet protected _front: RS.Rendering.Scene.IMeshObject;

        private _isRevealed: boolean = false;
        public get isRevealed() { return this._isRevealed; }

        private _rotateProxy = {val: 0};

        /** Gets the settings */
        public get settings() { return this._settings; }

        constructor(settings: Card3D.Settings, stage: RS.Rendering.IStage)
        {
            super("3D Card Visual");

            this._settings = settings;
            this._stage = stage;

            this.createComponents();

            this.setInitialSettings();
        }

        public async flip(delay: number, initiallyHiddenFace: RS.Rendering.Scene.IMeshObject, initiallyShownFace: RS.Rendering.Scene.IMeshObject, instant: boolean = false, direction: Card3D.FlipDirection = Card3D.FlipDirection.LeftToRight): Promise<void>
        {
            const target = {val: this._rotateProxy.val + (direction === Card3D.FlipDirection.LeftToRight ? -0.5 : 0.5)};
            //todo
            if (instant)
            {
                initiallyHiddenFace.visible = true;
                initiallyShownFace.visible = false;
                const targetRot = Math.Quaternion.fromEuler(0, 0, Math.Angles.circle(target.val));
                this.rotation.set(targetRot);
            }
            else
            {
                await Tween.wait(delay);

                let visibleFlipped = false;
                const flipTween = RS.Tween.get(this._rotateProxy).to(target, this._settings.flipSpeed, Ease.cubicInOut);
                flipTween.onPositionChanged(() =>
                {
                    const targetRot = Math.Quaternion.fromEuler(0, 0, Math.Angles.circle(this._rotateProxy.val));
                    this.rotation.set(targetRot);
                    if (!visibleFlipped && flipTween.position > this._settings.flipSpeed / 2)
                    {
                        visibleFlipped = true;
                        initiallyHiddenFace.visible = true;
                        initiallyShownFace.visible = false;
                    }
                });

                const flipRaiseHeight: number = this._settings.flipRaiseHeight | 60;
                const yCache = this.position.y;
                const raiseTween = Tween.get(this.position)
                    .to({y: yCache + flipRaiseHeight}, this._settings.flipSpeed / 2, Ease.cubicIn)
                    .to({y: yCache}, this._settings.flipSpeed / 2, Ease.cubicOut);

                await Promise.all([flipTween, raiseTween]);
            }
        }

        public async peek(): Promise<void>
        {
            const proxy = {val: this._rotateProxy.val};
            const peekTween = Tween.get(proxy)
                .to({val: this._rotateProxy.val - 0.1}, this._settings.peekSpeed / 2, Ease.cubicIn)
                .wait(this._settings.peekDelay)
                .to({val: this._rotateProxy.val}, this._settings.peekSpeed / 2, Ease.cubicOut);
            peekTween.onPositionChanged(() =>
            {
                const targetRot = Math.Quaternion.fromEuler(Math.Angles.circle(proxy.val), 0, 0);
                this.rotation.set(targetRot);
            });

            const peekRaiseHeight: number = this._settings.peekRaiseHeight | 60;
            const yCache = this.position.y;
            const raiseTween = Tween.get(this.position)
                .to({y: yCache + peekRaiseHeight}, this.settings.peekSpeed / 2, Ease.cubicIn)
                .wait(this._settings.peekDelay)
                .to({y: yCache}, this.settings.peekSpeed / 2, Ease.cubicOut);

            await Promise.all([peekTween, raiseTween]);
        }

        public async reveal(suit: string, index: number, delay: number, instant: boolean = false, direction?: Card3D.FlipDirection): Promise<void>
        {
            if (this._isRevealed)
            {
                Log.warn("Revealing card already revealed");
            }
            this.changeFrontTexture(suit, index);
            await this.flip(delay, this._front, this._back, instant, direction);
            this._isRevealed = true;
        }

        public async hide(delay: number, instant: boolean = false, direction?: Card3D.FlipDirection): Promise<void>
        {
            if (!this._isRevealed)
            {
                Log.warn("Hiding card already hidden");
            }
            await this.flip(delay, this._back, this._front, instant, direction);
            this._isRevealed = false;
        }

        public async raise(raiseOffset: number = this._settings.raiseHeight): Promise<void>
        {
            Tween.removeTweens(this.position);
            await Tween.get(this.position)
                .to({y: this.position.y + raiseOffset}, 300, Ease.linear);
        }

        public async lower(): Promise<void>
        {
            Tween.removeTweens(this.position);
            await Tween.get(this.position).to({y: 0}, 300, Ease.linear);
        }

        public setInitialSettings(): void
        {
            //todo
            this.setLocalTransform(this._settings.transform);

            //show back first
            this._back.visible = true;
            this._front.visible = false;
            Math.Quaternion.fromEuler(0, 0, Math.Angles.circle(0.5), this._front.rotation);

            //gets flipped for some reason, so flip back
            this._front.scale.x = -this._front.scale.x;
            this._back.scale.x = -this._back.scale.x;
        }

        protected createComponents(): void
        {
            this.createBack();
            this.createFront();
        }

        protected createBack(): void
        {
            const texture = Asset.Store.getAsset(this._settings.back).asset as RS.Rendering.ITexture;

            const geo = RS.Rendering.Scene.Geometry.createPlane({x: texture.actualWidth, y: texture.actualHeight});
            this._back = new RS.Rendering.Scene.MeshObject(geo, [this.createMaterialFromTexture(texture)]);
            this.addChild(this._back);
        }

        protected createFront(): void
        {
            const spriteSheet = Asset.Store.getAsset(this._settings.front).asset as RS.Rendering.ISpriteSheet;
            const texture = this.getFrontTexture(spriteSheet.animations[0], 0);

            const geo = RS.Rendering.Scene.Geometry.createPlane({x: texture.actualWidth, y: texture.actualHeight});
            this._front = new RS.Rendering.Scene.MeshObject(geo, [this.createMaterialFromTexture(texture)]);
            this.addChild(this._front);
        }

        protected getFrontTexture(suit: string, index: number): RS.Rendering.ITexture
        {
            const spriteSheet = Asset.Store.getAsset(this._settings.front).asset as RS.Rendering.ISpriteSheet;
            const frameNumber = spriteSheet.getAnimation(suit).frames[index];
            const subTexture = spriteSheet.getFrame(frameNumber).imageData;
            const texture = new RS.Rendering.RenderTexture(subTexture.width, subTexture.height);
            const bitmap = new RS.Rendering.Bitmap(spriteSheet.getFrame(frameNumber).imageData);
            bitmap.renderToTexture(texture, false, this._stage);
            return texture;
        }

        protected changeFrontTexture(suit: string, index: number): void
        {
            this._front.materials[0].diffuseMap = this.getFrontTexture(suit, index);
        }

        protected createMaterialFromTexture(texture: RS.Rendering.ITexture): RS.Rendering.Scene.Material
        {
            return {
                model: RS.Rendering.Scene.Material.Model.LitSpec,
                diffuseMap: texture,
                blend: RS.Rendering.Scene.Material.Blend.Opaque
            };
        }
    }

    export namespace Card3D
    {
        export interface Settings
        {
            stage?: RS.Rendering.IContainer;
            flipSpeed: number;
            peekSpeed: number;
            peekDelay: number;
            transform: Math.ReadonlyTransform3D;
            back: RS.Asset.ImageReference;
            front: RS.Asset.RSSpriteSheetReference | RS.Asset.SpriteSheetReference;
            flipRaiseHeight?: number;
            peekRaiseHeight?: number;
            raiseHeight?: number;
        }

        export enum FlipDirection
        {
            LeftToRight,
            RightToLeft
        }
    }
}
