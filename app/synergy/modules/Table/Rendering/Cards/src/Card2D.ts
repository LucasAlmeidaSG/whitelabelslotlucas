namespace RS.Table.Rendering.Cards
{
    /**
     * 2D Card
     */
    export class Card2D extends RS.Rendering.Container
    {
        protected static _flipShaderProgram: Shaders.FlipShaderProgram | null = null;
        protected static _peekShaderProgram: Shaders.PeekShaderProgram | null = null;

        protected _settings: Card2D.Settings;

        @AutoDispose protected _back: RS.Rendering.IBitmap;
        @AutoDispose protected _front: RS.Rendering.IBitmap;
        @AutoDispose protected _shadow: RS.Rendering.IBitmap;
        @AutoDisposeOnSet protected _flipFaceTween: RS.Tween<RS.Rendering.IBitmap>;
        @AutoDisposeOnSet protected _flipShadowTween: RS.Tween<RS.Rendering.IBitmap>;
        @AutoDisposeOnSet protected _flipShaderTween: RS.Tween<Shaders.FlipShader>;
        @AutoDisposeOnSet protected _flipSkewTween: RS.Tween<RS.Rendering.IBitmap>;
        @AutoDisposeOnSet protected _peekFaceTween: RS.Tween<RS.Rendering.IBitmap>;
        @AutoDisposeOnSet protected _peekShaderTween: RS.Tween<Shaders.PeekShader>;
        @AutoDisposeOnSet protected _revealed: boolean;

        /** Gets the settings */
        public get settings() { return this._settings; }

        /** Gets whether this cards visual has been revealed  */
        public get revealed() { return this._revealed; }

        constructor(settings: Card2D.Settings)
        {
            super();
            this.name = "Card Visual";

            this._settings = settings;

            this.setInitialValues();

            this.createComponents();

            this.setInitialSettings();
        }

        /**
         * @param currentScreenDimensions This is either the screen dimensions OR the size of the projection containers filter area, if using one, and it's bigger.
         * @param worldPosOffset This should normally be x:0,y:0. This is used when using a projection container filter area that is larger than the screen dimensions.
         */
        public async flip(currentScreenDimensions: Math.Size2D, delay: number, initiallyHiddenFace: RS.Rendering.IBitmap, initiallyShownFace: RS.Rendering.IBitmap, instant: boolean = false, worldPosOffset: RS.Math.Vector2D = { x: 0, y: 0 })
        {
            initiallyHiddenFace.scaleX = initiallyHiddenFace.scaleY = 1.0;
            initiallyShownFace.scaleX = initiallyShownFace.scaleY = 1.0;
            // Skip the animations if instant
            if (instant)
            {
                initiallyShownFace.alpha = 0;
                initiallyHiddenFace.alpha = 1;
            }
            else
            {
                this._flipFaceTween = null;
                this._flipShaderTween = null;
                this._flipShadowTween = null;
                this._flipSkewTween = null;

                initiallyHiddenFace.alpha = 0;
                initiallyShownFace.alpha = 1;
                const originalY: number = initiallyShownFace.y;
                const heightToRaiseWhenFlipping: number = (this._settings.usingFlipShader && this._settings.stage) ? this.settings.flipShaderRaiseHeight : this.settings.flipNonShaderRaiseHeight;

                if (this._settings.usingFlipShader && this._settings.stage)
                {
                    if (Card2D._flipShaderProgram == null)
                    {
                        Card2D._flipShaderProgram = new Shaders.FlipShaderProgram();
                    }

                    const flipShader = Card2D._flipShaderProgram.createShader();
                    const flipFilter = new RS.Rendering.Filter(flipShader);
                    flipFilter.autoFit = false;

                    const worldPos = initiallyShownFace.toGlobal(Math.Vector2D(0.0, 0.0), null, true);
                    worldPos.x += worldPosOffset.x;
                    worldPos.y += worldPosOffset.y;

                    const vertexPos: Math.Vector2D = this.calculateVertexPos(currentScreenDimensions, worldPos);

                    // when you get the filters array it returns a "wrapper" array, when you change that it won't update the corresponding native array
                    // therefore, can't do hiddenFace.filters.push() unless you dod hiddenFace.filters = hiddenFace.filters afterwards.
                    initiallyHiddenFace.filters = [...initiallyHiddenFace.filters, flipFilter];
                    initiallyShownFace.filters = [...initiallyShownFace.filters, flipFilter];

                    flipShader.localPosition = vertexPos;

                    let shadow: RS.Rendering.IBitmap;
                    if (this._settings.shadow)
                    {
                        shadow = RS.Factory.bitmap(this._settings.shadow);
                        shadow.anchorX = shadow.anchorY = 0.5;
                        this.addChild(shadow, 0);

                        this._flipShadowTween = RS.Tween.get(shadow)
                            .wait(delay)
                            .to({ scaleX: 0.1, scaleY: 1, alpha: 0.5 }, this._settings.flipSpeed / 2, RS.Ease.cubicIn)
                            .call(() =>
                            {
                                shadow.scaleX = -0.1;
                                this._flipShadowTween = RS.Tween.get(shadow)
                                    .to({ scaleX: -1.0, scaleY: 1.0, alpha: 1 }, this._settings.flipSpeed / 2, RS.Ease.cubicOut).
                                    call(() =>
                                    {
                                        this.removeChild(shadow);
                                    });
                            });
                    }

                    this._flipFaceTween = RS.Tween.get([initiallyShownFace, initiallyHiddenFace]);
                    this._flipFaceTween.wait(delay)
                        .to({ y: originalY - heightToRaiseWhenFlipping }, (this._settings.flipSpeed / 2), RS.Ease.circIn)
                        .to({ y: originalY }, (this._settings.flipSpeed / 2), RS.Ease.circOut);

                    this._flipShaderTween = RS.Tween.get(flipShader);
                    await this._flipShaderTween.wait(delay)
                        .to({ rotationAngle: RS.Math.degreesToRadians(85) }, this._settings.flipSpeed / 2, RS.Ease.cubicIn)
                        .call(async () =>
                        {
                            initiallyShownFace.alpha = 0;
                            initiallyHiddenFace.alpha = 1;
                            initiallyShownFace.filters = [];

                            flipShader.rotationAngle = RS.Math.degreesToRadians(275);
                            initiallyHiddenFace.alpha = 1;
                        });

                    this._flipShaderTween = RS.Tween.get(flipShader);
                    await this._flipShaderTween.to({ rotationAngle: RS.Math.degreesToRadians(360) }, this._settings.flipSpeed / 2, RS.Ease.cubicOut)
                        .call(() =>
                        {
                            // when you get the filters array it returns a "wrapper" array, when you change that it won't update the corresponding native array
                            // therefore, can't just do RS.Find.remove() unless you do initiallyHiddenFace.filters = initiallyHiddenFace.filters afterwards.
                            RS.Find.remove(initiallyShownFace.filters, (filter) => filter === flipFilter);
                            initiallyShownFace.filters = initiallyShownFace.filters;
                            RS.Find.remove(initiallyHiddenFace.filters, (filter) => filter === flipFilter);
                            initiallyHiddenFace.filters = initiallyHiddenFace.filters;
                        });
                }
                else
                {
                    this._flipFaceTween = RS.Tween.get([initiallyShownFace, initiallyHiddenFace]);
                    this._flipFaceTween.wait(delay)
                        .to({ y: originalY - heightToRaiseWhenFlipping, scaleX: 0, scaleY: 1.05 }, (this._settings.flipSpeed / 2), RS.Ease.circIn)
                        .then(() =>
                        {
                            initiallyShownFace.alpha = 0;
                            initiallyHiddenFace.alpha = 1;
                            this._flipFaceTween = RS.Tween.get([initiallyShownFace, initiallyHiddenFace])
                                .to({ y: originalY, scaleX: 1, scaleY: 1 }, (this._settings.flipSpeed / 2), RS.Ease.circOut);
                        });

                    this._flipSkewTween = RS.Tween.get(initiallyShownFace.skew);
                    await this._flipSkewTween.wait(delay)
                        .to({ y: 0.25 }, (this._settings.flipSpeed / 2), RS.Ease.circIn)
                        .then(async () =>
                        {
                            initiallyHiddenFace.skew.y = -0.25;
                        });
                    this._flipSkewTween = RS.Tween.get([initiallyShownFace.skew, initiallyHiddenFace.skew]);
                    await this._flipSkewTween.to({ y: 0.0 }, (this._settings.flipSpeed / 2), RS.Ease.circOut);
                }
            }
        }

        /**
         * @description - Used by the dealer to check the value of a card without displaying it to the player
         * Very basic, it simply rotates the card toward the dealer.
         * @param currentScreenDimensions This is either the screen dimensions OR the size of the projection containers filter area, if using one, and it's bigger.
         * @param worldPosOffset This should normally be x:0,y:0. This is used when using a projection container filter area that is larger than the screen dimensions.
         */
        public async peek(currentScreenDimensions: Math.Size2D, worldPosOffset: RS.Math.Vector2D = { x: 0, y: 0 })
        {
            const hiddenFace = this._front;
            const shownFace = this._back;

            this._peekFaceTween = null;
            this._peekShaderTween = null;

            const heightToRaiseWhenPeeking: number = (this._settings.usingFlipShader && this._settings.stage) ? this.settings.peekShaderRaiseHeight : this.settings.peekNonShaderRaiseHeight;

            if (this._settings.usingFlipShader && this._settings.stage)
            {
                if (Card2D._peekShaderProgram == null)
                {
                    Card2D._peekShaderProgram = new Shaders.PeekShaderProgram();
                }

                const peekShader = Card2D._peekShaderProgram.createShader();
                const peekFilter = new RS.Rendering.Filter(peekShader);
                peekFilter.autoFit = false;

                const worldPos = shownFace.toGlobal(Math.Vector2D(0.0, 0.0), null, true);
                worldPos.x += worldPosOffset.x;
                worldPos.y += worldPosOffset.y;

                const vertexPos: Math.Vector2D = this.calculateVertexPos(currentScreenDimensions, worldPos);

                // when you get the filters array it returns a "wrapper" array, when you change that it won't update the corresponding native array
                // therefore, can't do hiddenFace.filters.push() unless you dod hiddenFace.filters = hiddenFace.filters afterwards.
                hiddenFace.filters = [...hiddenFace.filters, peekFilter];
                shownFace.filters = [...shownFace.filters, peekFilter];

                peekShader.localPosition = vertexPos;

                const originalY: number = shownFace.y;

                peekShader.rotationAngle = 0;

                // Rotate the card to the dealer, hold, then return
                this._peekShaderTween = RS.Tween.get(peekShader);
                this._peekShaderTween.to({ rotationAngle: RS.Math.degreesToRadians(-25), }, this._settings.peekSpeed / 2, RS.Ease.linear)
                    .wait(500)
                    .to({ rotationAngle: RS.Math.degreesToRadians(0) }, this._settings.peekSpeed / 2, RS.Ease.linear)
                    .call(() =>
                    {
                        // when you get the filters array it returns a "wrapper" array, when you change that it won't update the corresponding native array
                        // therefore, can't just do RS.Find.remove() unless you do hiddenFace.filters = hiddenFace.filters afterwards.
                        RS.Find.remove(shownFace.filters, (filter) => filter === peekFilter);
                        shownFace.filters = shownFace.filters;
                        RS.Find.remove(hiddenFace.filters, (filter) => filter === peekFilter);
                        hiddenFace.filters = hiddenFace.filters;
                    })
                    .onPositionChanged(() =>
                    {
                        const updatedWorldPos: RS.Math.Vector2D = shownFace.toGlobal(Math.Vector2D(0.0, 0.0), null, true);
                        updatedWorldPos.x += worldPosOffset.x;
                        updatedWorldPos.y += worldPosOffset.y;

                        // Allow the shader to work even if the cards are moving.
                        peekShader.localPosition = this.calculateVertexPos(currentScreenDimensions, updatedWorldPos)
                    });

                // Raise above the table, hold, then return
                this._peekFaceTween = RS.Tween.get([shownFace, hiddenFace]);
                await this._peekFaceTween.to({ y: originalY - heightToRaiseWhenPeeking }, this._settings.peekSpeed / 2, RS.Ease.linear)
                    .wait(500)
                    .to({ y: originalY }, this._settings.peekSpeed / 2, RS.Ease.linear);
            }
            else
            {
                // Scale down in y, hold, then return, to imitate the card rotating.
                this._peekFaceTween = RS.Tween.get([shownFace, hiddenFace]);
                await this._peekFaceTween.to({ scaleY: 0.8 }, (this._settings.peekSpeed / 2), RS.Ease.circIn)
                    .wait(500)
                    .to({ scaleY: 1 }, (this._settings.peekSpeed / 2), RS.Ease.circIn);
            }

        }

        /**
         * @param currentScreenDimensions This is either the screen dimensions OR the size of the projection containers filter area, if using one, and it's bigger.
         * @param worldPosOffset This should normally be x:0,y:0. This is used when using a projection container filter area that is larger than the screen dimensions.
         */
        public async reveal(type: string, index: number, currentScreenDimensions: Math.Size2D, delay: number, instant: boolean = false, worldPosOffset: RS.Math.Vector2D = { x: 0, y: 0 })
        {
            this.changeFrontTexture(type, index);
            await this.flip(currentScreenDimensions, delay, this._front, this._back, instant, worldPosOffset);
            this._revealed = true;
        }

        /**
         * @param currentScreenDimensions This is either the screen dimensions OR the size of the projection containers filter area, if using one, and it's bigger.
         * @param worldPosOffset This should normally be x:0,y:0. This is used when using a projection container filter area that is larger than the screen dimensions.
         */
        public async hide(currentScreenDimensions: Math.Size2D, delay: number, instant: boolean = false, worldPosOffset: RS.Math.Vector2D = { x: 0, y: 0 })
        {
            await this.flip(currentScreenDimensions, delay, this._back, this._front, instant, worldPosOffset);
            this._revealed = false;
        }

        /**
         * Used to raise the card off the table, displays a shadow below and can be adjusted
         * @param raiseOffset How much to raise the card by, default = 20
         */
        public async raise(raiseOffset: number = this._settings.raiseHeight | 20): Promise<void>
        {
            if (this._settings.shadow && !this._shadow)
            {
                this._shadow = RS.Factory.bitmap(this._settings.shadow);
                this._shadow.alpha = 0.0;
                this._shadow.anchorX = this._shadow.anchorY = 0.5;
                this.addChild(this._shadow, 0);
            }

            const promises = [];
            RS.Tween.removeTweens(this._front);
            promises.push(RS.Tween.get(this._front)
                .to({ scaleX: 1 + (raiseOffset / 1000), scaleY: 1 + (raiseOffset / 1000), y: -raiseOffset }, 300, RS.Ease.linear));

            RS.Tween.removeTweens(this._shadow);
            promises.push(RS.Tween.get(this._shadow)
                .to({ scaleX: 1, scaleY: 1, alpha: 0.4 }, 300, RS.Ease.linear));

            await Promise.all(promises);
        }

        /**
         * Used to lower the card back down if has been raised.
         */
        public async lower(): Promise<void>
        {
            if (this._shadow)
            {
                const promises = [];
                RS.Tween.removeTweens(this._front);
                promises.push(RS.Tween.get(this._front)
                    .to({ scaleX: 1, scaleY: 1, y: 0 }, 300, RS.Ease.linear));

                RS.Tween.removeTweens(this._shadow);
                promises.push(RS.Tween.get(this._shadow)
                    .to({ scaleX: 1.0, scaleY: 1.0, alpha: 0.0 }, 300, RS.Ease.linear)
                    .call(() => {
                        this._shadow.dispose();
                        this._shadow = null;
                    }));

                await Promise.all(promises);
            }
        }

        public setInitialSettings(): void
        {
            this._front.alpha = 0;
            this._front.scaleX = 1;
            this._front.scaleY = 1;
            this._front.skew.y = 0;
            this._front.skew.y = 0;

            this._back.alpha = 1;
            this._back.scaleX = 1;
            this._back.scaleY = 1;
            this._back.skew.y = 0;
            this._back.skew.y = 0;
        }

        /**
         * We need to multiply by currentScreenDimensions/stageBounds in order to get values of -1 to 1 relative
         * to the screen size, not the stage size. Otherwise assets can influence how the shader works.
         */
        protected calculateVertexPos(currentScreenDimensions: Math.Size2D, worldPos: Math.Vector2D): Math.Vector2D
        {
            const stageBounds = this._settings.stage.bounds;
            const centerPointX = currentScreenDimensions.w * 0.5;
            const centerPointY = currentScreenDimensions.h * 0.5;

            // Bounds are less that screen dimensions, don't modify it.
            const multiplierX = Math.min(currentScreenDimensions.w / stageBounds.w, 1);
            const multiplierY = Math.min(currentScreenDimensions.h / stageBounds.h, 1);

            const vertexPos: Math.Vector2D = {
                x: ((worldPos.x - centerPointX) / centerPointX) * multiplierX,
                y: (worldPos.y / centerPointY) * multiplierY
            };

            return vertexPos;
        }

        protected setInitialValues(): void
        {
            this.x = this._settings.position.x;
            this.y = this._settings.position.y;

            if (this._settings.scale != null)
            {
                this.scale.set(this._settings.scale.x, this._settings.scale.y);
            }

            this.interactive = true;

            if (this._settings.rotation != null)
            {
                this.rotation = this._settings.rotation;
            }

            this._revealed = false;
        }

        protected createComponents(): void
        {
            this.createBack();
            this.createFront();
        }

        protected createBack(): void
        {
            this._back = RS.Factory.bitmap(this._settings.back as RS.Asset.ImageReference);
            this._back.anchorX = this._back.anchorY = 0.5;
            this.addChild(this._back);
        }

        protected createFront(): void
        {
            const spriteSheet = RS.Asset.Store
                .getAsset(this._settings.front)
                .asset as RS.Rendering.ISpriteSheet;

            const texture = this.getFrontTexture(spriteSheet.animations[0], 0)

            this._front = new RS.Rendering.Bitmap(texture);
            this._front.anchorX = this._front.anchorY = 0.5;
            this.addChild(this._front);
        }

        protected getFrontTexture(type: string, index: number): RS.Rendering.ISubTexture
        {
            const spriteSheet = RS.Asset.Store
                .getAsset(this._settings.front)
                .asset as RS.Rendering.ISpriteSheet;
            const frameNumber = spriteSheet.getAnimation(type).frames[index];
            return spriteSheet.getFrame(frameNumber).imageData;
        }

        protected changeFrontTexture(type: string, index: number): void
        {
            this._front.texture = this.getFrontTexture(type, index);
        }
    }

    export namespace Card2D
    {
        export interface Settings
        {
            stage?: RS.Rendering.IContainer;
            flipSpeed: number;
            peekSpeed: number;
            position: Math.Vector2D;
            back: RS.Asset.ImageReference;
            front: RS.Asset.RSSpriteSheetReference | RS.Asset.SpriteSheetReference;
            shadow?: RS.Asset.ImageReference;
            rotation?: number;
            scale?: Math.Vector2D;
            raiseHeight?: number;
            usingFlipShader?: boolean;
            flipShaderRaiseHeight?: number;
            flipNonShaderRaiseHeight?: number;
            peekShaderRaiseHeight?: number;
            peekNonShaderRaiseHeight?: number;
        }
    }
}
