/// <reference path="../generated/Assets.ts" />

namespace RS.Table.Rendering.Cards.Shaders
{
    /* tslint:disable:no-string-literal */

    export class PeekShaderProgram extends RS.Rendering.ShaderProgram
    {
        public constructor()
        {
            super({
                vertexSource: Assets.Shaders.Peek.Vertex,
                fragmentSource: Assets.Shaders.Peek.Fragment
            });
        }

        public createShader(): PeekShader
        {
            return new PeekShader(this);
        }
    }

    export class PeekShader extends RS.Rendering.Shader
    {
        protected _uRotationAngle: RS.Rendering.ShaderUniform.Float;
        protected _uLocalPosition: RS.Rendering.ShaderUniform.Vec2;

        protected _speed: number;

        /** Gets or sets the rotation angle Z */
        public get rotationAngle() { return this._uRotationAngle.value; }
        public set rotationAngle(value)
        {
            this._uRotationAngle.value = value;
        }

        /** Sets the local position */
        public get localPosition() { return this._uLocalPosition.value; }
        public set localPosition(value)
        {
            this._uLocalPosition.value = value;
        }

        public constructor(shaderProgram: RS.Rendering.IShaderProgram)
        {
            super(shaderProgram);
            this._uRotationAngle = this.uniforms["rotationAngle"] as RS.Rendering.ShaderUniform.Float;
            this._uLocalPosition = this.uniforms["localPosition"] as RS.Rendering.ShaderUniform.Vec2;
        }
    }
}