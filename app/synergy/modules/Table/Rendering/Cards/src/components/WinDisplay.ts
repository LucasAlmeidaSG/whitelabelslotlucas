namespace RS.Table.Rendering.Cards.Components
{
    export class WinDisplay<TSettings extends WinDisplay.Settings = WinDisplay.Settings> extends Flow.BaseElement<TSettings>
    {
        @AutoDispose protected _background: Flow.Background;
        @AutoDisposeOnSet protected _text: Flow.Label;
        protected _isShown: boolean = false;

        public get shown() { return this._isShown; }

        constructor(settings: TSettings)
        {
            super(settings, null);

            this.createComponents();
        }

        public async showResult(win?: boolean, instant?: boolean)
        {
            if (!this._isShown)
            {
                this._isShown = true;

                this._text = Flow.label.create(win ? this.settings.winText : this.settings.loseText, this._background);

                if (instant)
                {
                    this._background.alpha = 1;
                }
                else
                {
                    await Tween.get(this._background).to({ alpha: 1 }, this.settings.fadeTime);
                }
            }
        }

        public async hideResult(instant?: boolean)
        {
            if (this._isShown)
            {
                this._isShown = false;
                if (instant)
                {
                    this._background.alpha = 0;
                }
                else
                {
                    await Tween.get(this._background).to({ alpha: 0 }, this.settings.fadeTime);
                }
                this._text = null;
            }
        }

        protected createComponents(): void
        {
            this.createBackground();
        }

        protected createBackground(): void
        {
            this._background = Flow.background.create(this.settings.background, this);
            this._background.alpha = 0;
        }
    }

    export const winDisplay = Flow.declareElement(WinDisplay, WinDisplay.defaultSettings);

    export namespace WinDisplay
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            background: Flow.Background.Settings;
            winText: Flow.Label.Settings;
            loseText: Flow.Label.Settings;
            fadeTime: number;
        }

        export const defaultSettings: Settings =
        {
            background: Flow.Background.defaultSettings,
            winText: Flow.Label.defaultSettings,
            loseText: Flow.Label.defaultSettings,
            fadeTime: 500
        }
    }
}