namespace RS.Table.Rendering.Cards.Components
{
    export class Tally extends Flow.BaseElement<Tally.Settings>
    {
        protected _background: Flow.Background;
        protected _arrow: Flow.Image;
        protected _text: Flow.Label;
        protected _isActive: boolean = false;
        protected _isShown: boolean = false;
        protected _originalArrowHeight: number;
        protected _value: number = 0;

        public set value(value: number)
        {
            if (value != null && Is.number(value))
            {
                this._value = value;
                if (this._isShown)
                {
                    this._text.text = value == 0 ? "" : value.toString();
                }
            }
        }

        public get value(): number { return this._value; }

        public get isActive(): boolean { return this._isActive; }

        public get isShown(): boolean { return this._isActive; }

        constructor(settings: Tally.Settings)
        {
            super(settings, null);
            this.createComponents();

            this.showTally(false, true);
        }

        public async moveTally(active: boolean = true)
        {
            if (active && !this._isActive)
            {
                const tweens = [];
                if (this._arrow)
                {
                    tweens.push(Tween.get(this._arrow.postTransform)
                        .set({ scaleX: 0, scaleY: 0 })
                        .to({ scaleX: 1, scaleY: 1 }, 500, Ease.backOut));
                }

                tweens.push(Tween.get(this._background.postTransform)
                    .to({ y: -this._originalArrowHeight }, 500, Ease.backOut));

                await Timeline.get(tweens);
            }
            else if (!active && this._isActive)
            {
                const tweens = [];
                if (this._arrow)
                {
                    tweens.push(Tween.get(this._arrow.postTransform)
                        .to({ scaleX: 0, scaleY: 0 }, this.settings.animationTime, RS.Ease.backOut));
                }

                tweens.push(Tween.get(this._background.postTransform)
                    .to({ y: 0 }, this.settings.animationTime, Ease.backOut));

                await Timeline.get(tweens);
            }
            this._isActive = active;
        }

        public async showTally(show: boolean = true, instant: boolean = false)
        {
            const scale = show ? 1 : 0;
            if (instant)
            {
                this.postTransform.scaleX = this.postTransform.scaleY = scale;
            }
            else
            {
                await Tween.get(this.postTransform)
                    .to({ scaleX: scale, scaleY: scale }, this.settings.animationTime, show ? Ease.backOut : Ease.backIn);
            }
            this._isShown = show;
            this.value = this._value;
        }

        protected createComponents(): void
        {
            this._background = Flow.background.create(this.settings.background, this);
            this._text = Flow.label.create(this.settings.text, this._background);
            if (this.settings.arrow)
            {
                this._arrow = Flow.image.create(this.settings.arrow, this);
            }
        }
    }

    export const tally = Flow.declareElement(Tally, Tally.defaultSettings);

    export namespace Tally
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            text: Flow.Label.Settings;
            background: Flow.Background.Settings;
            arrow?: Flow.Image.Settings;
            animationTime: number;
        }

        export const defaultSettings: Settings =
        {
            text: Flow.Label.defaultSettings,
            background: Flow.Background.defaultSettings,
            animationTime: 500
        }
    }
}
