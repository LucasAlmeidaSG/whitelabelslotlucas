namespace RS.Table.Rendering.Cards
{
    export class Cards3D<TSettings extends Cards3D.Settings = Cards3D.Settings> extends RS.Rendering.Scene.SceneObject
    {
        protected readonly _settings: TSettings;
        protected readonly _overlayContainer: Flow.Container;

        protected _cards: Card3D[] = [];
        protected _dealPos: Math.Vector3D;
        protected _stacked: boolean = true;

        @AutoDispose protected _tally: Components.Tally;
        @AutoDispose protected _winDisplay: Components.WinDisplay;

        @AutoDisposeOnSet protected _stackTweens: Tween<Math.IObservableVector3D>[];
        @AutoDisposeOnSet protected _centerTween: Tween<Math.IObservableVector3D>;

        public get cards() { return this._cards; }
        public get tally() { return this._tally; }
        public get winDisplay() { return this._winDisplay; }

        public set stacked(value: boolean) { this._stacked = value; }
        public get stacked() { return this._stacked; }

        constructor(settings: TSettings, overlayContainer: Flow.Container)
        {
            super(settings.name);
            this._settings = settings;
            this._overlayContainer = overlayContainer;

            this.setLocalTransform(this._settings.transform);

            //calculate local deal position
            const global = this._settings.globalDealingPosition;
            const dealPos = RS.Math.Vector3D.subtract(global, this.position);
            const invertScale = RS.Math.Vector3D(1 / this.scale.x, 1 / this.scale.y, 1 / this.scale.z);
            this._dealPos = RS.Math.Vector3D.multiply(invertScale, dealPos);

            this.createTally();
            this.createWinDisplay();
        }

        /**
         * Adds given card as child and animates(deals) into place
         */
        public async dealCard(card: Rendering.Cards.Card3D, suit?: Models.Card.Suit, value?: number, instant?: boolean): Promise<void>
        {
            this.addChild(card);
            this._cards.push(card);
            const offset = this._stacked ? this._settings.stackedOffset : this._settings.separatedOffset;
            const cardIndex = this._cards.length - 1;
            const targetPos = Math.Vector3D.multiply(offset, cardIndex);

            if (instant)
            {
                card.position.set(targetPos);
            }
            else
            {
                card.position.set(this._dealPos);
                await Tween.get(card.position).to(targetPos, this._settings.dealTime, this._settings.dealEase);
            }

            if (suit != null && value != null)
            {
                if (!instant)
                {
                    await Tween.wait(this._settings.dealRevealDelay);
                }
                await this._cards[cardIndex].reveal(suit, value, 0, instant, this._settings.revealFlipDirection);
            }
        }

        public async centerCards(tweenTime: number = this._settings.centerTweenTime, numCards: number = this._cards.length): Promise<void>
        {
            if (numCards === 0) { return; }
            //animate so hand in centered again
            //original position - offset * half card count
            const offset = this._stacked ? this._settings.stackedOffset : this._settings.separatedOffset;
            const targetPos = {
                x: this._settings.transform.x - (((numCards - 1) / 2) * offset.x * this._settings.transform.sx),
                y: this._settings.transform.y - (((numCards - 1) / 2) * offset.y * this._settings.transform.sy),
                z: this._settings.transform.z - (((numCards - 1) / 2) * offset.z * this._settings.transform.sz)
            };
            this._centerTween = Tween.get(this.position).to(targetPos, tweenTime);
            try
            {
                await this._centerTween;
            }
            catch (er)
            {
                if (!(er instanceof Tween.DisposedRejection))
                {
                    throw er;
                }
            }
        }

        public async changeStackType(tweenTime: number): Promise<void>
        {
            const promises = [];
            const offset = this._stacked ? this._settings.stackedOffset : this._settings.separatedOffset;
            this._cards.forEach((card, i) =>
            {
                promises.push(Tween.get(card.position).to(Math.Vector3D.multiply(offset, i), tweenTime));
            })
            promises.push(this.centerCards(tweenTime));
            await Promise.all(promises);
        }

        public async discardCards(keepWinDisplay?: boolean, instant?: boolean): Promise<void>
        {
            if (!instant)
            {
                const promises = [];
                this._cards.forEach((card) =>
                {
                    promises.push(Tween.get(card.position).to(this._dealPos, this._settings.dealTime));
                });
                await Promise.all(promises);
            }
            for (const card of this._cards)
            {
                this.removeChild(card);
                card.dispose();
            }
            this._cards = [];

            this._tally.showTally(false, true);
            if (this._overlayContainer)
            {
                this._overlayContainer.removeChild(this._tally);
                if (!keepWinDisplay)
                {
                    this._overlayContainer.removeChild(this._winDisplay);
                }
            }
        }

        public async peek(): Promise<void>
        {
            const promises = [];
            this._cards.forEach((card) =>
            {
                promises.push(card.peek());
            });
            await Promise.all(promises);
        }

        public removeCard(card: Card3D): void
        {
            this._cards.splice(this._cards.indexOf(card), 1);
            this.removeChild(card);
        }

        public addCard(card: Card3D): void
        {
            this._cards.push(card);
            this.addChild(card);
        }

        protected createTally()
        {
            this._tally = this._settings.tally.create(this._overlayContainer);
        }

        protected createWinDisplay()
        {
            this._winDisplay = this._settings.winDisplay.create(this._overlayContainer);
        }
    }

    export namespace Cards3D
    {
        export interface Settings
        {
            name?: string;

            id: number;
            transform: Math.ReadonlyTransform3D;
            stackedOffset: Math.Vector3D;
            separatedOffset: Math.Vector3D;
            stackTweenTime: number;
            separateTweenTime: number;

            globalDealingPosition: Math.Vector3D;
            dealTime: number;
            dealEase: EaseFunction;
            dealRevealDelay: number;
            centerTweenTime: number;

            revealFlipDirection: Card3D.FlipDirection;
            hideFlipDirection: Card3D.FlipDirection;

            winDisplay: Flow.IBoundElement<Components.WinDisplay, Components.WinDisplay.Settings>;
            tally: Flow.IBoundElement<Components.Tally, Components.Tally.Settings>;
        }
    }
}
