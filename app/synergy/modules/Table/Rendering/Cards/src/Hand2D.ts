namespace RS.Table.Rendering.Cards
{
    export class Hand2D extends RS.Rendering.DisplayObjectContainer
    {
        protected _cards: Rendering.Cards.Card2D[] = [];
        protected _dealPos: Math.Vector2D;
        protected _discardPos: Math.Vector2D;
        protected _stacked: boolean = true;

        @AutoDisposeOnSet protected _stackTweens: Tween<Math.Vector2D>[];
        @AutoDisposeOnSet protected _centerTween: Tween<Math.Vector2D>;

        public get cards() { return this._cards; }

        public set stacked(value: boolean) { this._stacked = value; }
        public get stacked() { return this._stacked; }

        constructor(protected readonly _settings: Hand2D.Settings, protected readonly _context: Game.Context, dealDiscardRelativeTo?: RS.Rendering.IDisplayObject)
        {
            super("Hand of Cards");

            this.scale.set(this._settings.scale.x, this._settings.scale.y);
            this.position.set(this._settings.position.x, this._settings.position.y);

            //calculate deal position
            const globalDealing = this._settings.globalDealingPosition;
            const dealPos = RS.Math.Vector2D.subtract(globalDealing, dealDiscardRelativeTo ? dealDiscardRelativeTo.position : this.position);
            const invertScale = Math.Vector3D(1 / this.scale.x, 1 / this.scale.y);
            this._dealPos = Math.Vector2D.multiply(invertScale, dealPos);

            //calculate discard position
            const globalDiscard = this._settings.globalDiscardPosition;
            const discardPos = RS.Math.Vector2D.subtract(globalDiscard, dealDiscardRelativeTo ? dealDiscardRelativeTo.position : this.position);
            this._discardPos = Math.Vector2D.multiply(invertScale, discardPos);
        }

        public async dealCard(card: Card2D, suit?: string, value?: number): Promise<void>
        {
            //create card, animate into place
            this.addChild(card);
            this._cards.push(card);
            card.position.set(this._dealPos.x, this._dealPos.y);

            const offset = this._stacked ? this._settings.stackedOffset : this._settings.separatedOffset
            const cardIndex = this._cards.length - 1;

            const dealToPos = {x: offset.x * cardIndex, y: offset.y * cardIndex};

            if (this._settings.dealDiscardBezierOffset != null)
            {
                const bezierOffset = {x: ((dealToPos.x + this._dealPos.x) * 0.5) + this._settings.dealDiscardBezierOffset.x, y: ((dealToPos.y + this._dealPos.y) * 0.5) + this._settings.dealDiscardBezierOffset.y};
                const dealPath: RS.Bezier.Path = RS.Bezier.Path.fromPointsCubic([this._dealPos, bezierOffset, dealToPos]);

                const bezierTween = new RS.BezierTween(card, {points: dealPath});
                if (this._settings.rotateBack)
                {
                    RS.Tween.get(card).to({ rotationDegrees: 0 }, this._settings.dealTime, this._settings.dealEase);
                }
                await bezierTween.end(this._settings.dealTime, this._settings.dealEase);
            }
            else
            {
                if (this._settings.rotateBack)
                {
                    RS.Tween.get(card).to({ rotationDegrees: 0 }, this._settings.dealTime, this._settings.dealEase);
                }
                await Tween.get(card.position).to({x: dealToPos.x, y: dealToPos.y}, this._settings.dealTime, this._settings.dealEase);
            }
        }

        public async centerCards(tweenTime: number = this._settings.centerTweenTime, numCards: number = this._cards.length): Promise<void>
        {
            if (numCards === 0) { return; }
            //animate so hand in centered again
            //original position - offset * half card count
            const offset = this._stacked ? this._settings.stackedOffset : this._settings.separatedOffset;
            const targetPos = {
                x: this._settings.position.x - (((numCards - 1) / 2) * offset.x * this._settings.scale.x),
                y: this._settings.position.y - (((numCards - 1) / 2) * offset.y * this._settings.scale.y)
            };
            this._centerTween = Tween.get(this.position).to(targetPos, tweenTime);
            try
            {
                await this._centerTween;
            }
            catch (er)
            {
                if (!(er instanceof Tween.DisposedRejection))
                {
                    throw er;
                }
            }
        }

        public async changeStackType(tweenTime: number): Promise<void>
        {
            const promises = [];
            this._cards.forEach((card, i) =>
            {
                promises.push(Tween.get(card.position).to(this.getCardPos(i), tweenTime));
            })
            promises.push(this.centerCards(tweenTime));
            await Promise.all(promises);
        }

        public async clear(): Promise<void>
        {
            const promises = [];
            this._cards.forEach((card) =>
            {
                if (this._settings.dealDiscardBezierOffset != null)
                {
                    const bezierOffset = {x: ((card.position.x + this._discardPos.x) * 0.5) + this._settings.dealDiscardBezierOffset.x, y: ((card.position.y + this._discardPos.y) * 0.5) + this._settings.dealDiscardBezierOffset.y};
                    const dealPath: RS.Bezier.Path = RS.Bezier.Path.fromPointsCubic([card.position, bezierOffset, this._discardPos]);

                    const bezierTween = new RS.BezierTween(card, {points: dealPath});
                    promises.push(bezierTween.end(this._settings.dealTime, this._settings.dealEase));
                }
                else
                {
                    promises.push(Tween.get(card.position).to(this._discardPos, this._settings.dealTime));
                }
            });
            await Promise.all(promises);
            for (const card of this._cards)
            {
                this.removeChild(card);
                card.dispose();
            }
            this._cards = [];
        }

        public async peek(): Promise<void>
        {
            const promises = [];
            this._cards.forEach((card) =>
            {
                promises.push(card.peek({ w: this._context.game.viewController.stage.stageWidth + 1000, h: this._context.game.viewController.stage.stageHeight + 1000 }, { x: 500, y: 0 }));
            });
            await Promise.all(promises);
        }

        protected getCardPos(index: number): Math.Vector2D
        {
            const offset = this._stacked ? this._settings.stackedOffset : this._settings.separatedOffset;
            return {x: offset.x * index, y: offset.y * index};
        }
    }

    export namespace Hand2D
    {
        export interface Settings
        {
            id: number;
            position: Math.Vector2D;
            scale: Math.Vector2D;
            stackedOffset: Math.Vector2D;
            separatedOffset: Math.Vector2D;
            stackTweenTime: number;
            separateTweenTime: number;
            rotateBack?: boolean;

            globalDealingPosition: Math.Vector2D;
            globalDiscardPosition: Math.Vector2D;
            dealTime: number;
            dealEase: EaseFunction;
            centerTweenTime: number;
            // The offset to use for bezier path center point.
            dealDiscardBezierOffset?: RS.Math.Vector2D;
        }
    }
}
