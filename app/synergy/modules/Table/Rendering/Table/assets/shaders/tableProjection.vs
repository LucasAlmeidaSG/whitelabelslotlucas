precision lowp float;

attribute vec2 aVertexPosition;
attribute vec2 aTextureCoord;
attribute vec4 aColor;
uniform mat3 projectionMatrix;
uniform mat3 modelViewMatrix;
uniform float rotationAngleZ;
varying vec2 vTextureCoord;

void main(void)
{
	// This vector adds an offset to the y to move the projection position downwards, therefore not cutting off as much of the container at the top. However it does seem to mess with hit areas.. quite awkward.
	//vec4 pos = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).x, (projectionMatrix * vec3(aVertexPosition, 1.0)).y + 0.2 , 0.0, 1.0);
	vec4 pos = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);

    mat4 rotMatZ = mat4(1.0, 0.0 ,0.0, 0.0,
    				    0.0, cos(rotationAngleZ), sin(rotationAngleZ), 0.0, 
    				    0.0, -sin(rotationAngleZ),cos(rotationAngleZ), 0.0,
    				    0.0, 0.0, 0.0, 1.0);

    mat4 projection = mat4(3.1 / 4.0, 0.0, 0.0, 0.0,
    					   0.0, 1.0, 0.0, 0.0,
    					   0.0, 0.0, 0.45, 0.45,
    					   0.0, 0.0, 0.0, 1.0);

    mat4 scale = mat4(4.0 / 3.0, 0.0, 0.0, 0.0,
        			  0.0, 1.0, 0.0, 0.0,
        			  0.0, 0.0, 1.0, 0.0,
        			  0.0, 0.0, 0.0, 1.0);

    vec4 projected_position = projection * scale  * rotMatZ * pos;
    
    gl_Position = projected_position;

    vTextureCoord = aTextureCoord;
}