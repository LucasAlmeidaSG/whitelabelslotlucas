/// <reference path="../generated/Assets.ts" />
namespace RS.Table.Rendering.Table.Shaders
{
    export class TableShaderProgram extends RS.Rendering.ShaderProgram
    {
        public constructor()
        {
            super({
                vertexSource: Assets.Shaders.Projection.Vertex,
                fragmentSource: Assets.Shaders.Projection.Fragment,
            });
        }

        public createShader(): TableProjectionShader
        {
            return new TableProjectionShader(this);
        }
    }

    export class TableProjectionShader extends RS.Rendering.Shader
    {
        protected _uRotationAngleZ: RS.Rendering.ShaderUniform.Float;

        public constructor(shaderProgram: RS.Rendering.IShaderProgram)
        {
            super(shaderProgram);
            const uRotAngleZ = "rotationAngleZ";
            this._uRotationAngleZ = this.uniforms[uRotAngleZ] as RS.Rendering.ShaderUniform.Float;
            this.uniforms[uRotAngleZ].value = 0.45;
        }
    }
}
