namespace SG.GLM
{
    //export imports for backwards compatibility
    export import Header = Core.Header;
    export import ReplayData = Core.ReplayData;
    export import CurrencyInformation = Core.CurrencyInformation;
    export import AccountData = Core.AccountData;
    export import Balance = Core.Balance;
    export import Error = Core.Error;
    export import MapItem = Core.MapItem;
    export import PayloadItem = Core.PayloadItem;
    export import Payload = Core.Payload;

    export import CloseResponse = Core.CloseResponse;

    export import errorResponse = Core.errorResponse;
}
