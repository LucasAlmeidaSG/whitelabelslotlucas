/// <reference path="../responses/InitResponse.ts" />

namespace SG.GLM
{
    export interface InitRequestPayload extends SG.GLM.Core.InitRequestPayload
    {
        replay?: SG.Replay.IReplay;
    }

    /*
    <GameRequest type="Init">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="SESSION_ID" userID="1000018628" userType="C" versionID="1_0" />
    </GameRequest>
    */

    export class InitRequest extends SG.GLM.Core.InitRequest
    {
        protected onComplete(req: XMLHttpRequest)
        {
            return RS.Table.IPlatform.get().networkResponseReceived(req);
        }
    }

    //export imports for backwards compatibility
    export namespace InitRequest
    {
        export import Payload = Core.InitRequest.Payload;
    }
}
