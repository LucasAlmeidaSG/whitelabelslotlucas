namespace SG.GLM
{
    export class CloseRequest extends Core.CloseRequest
    {
        protected onComplete(req: XMLHttpRequest)
        {
            return RS.Table.IPlatform.get().networkResponseReceived(req);
        }
    }
}
