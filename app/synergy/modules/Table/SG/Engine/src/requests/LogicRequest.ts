/// <reference path="../responses/LogicResponse.ts" />

namespace SG.GLM
{
    export interface LogicRequestPayload extends SG.GLM.Core.LogicRequestPayload
    {
        forceResult? : RS.Table.Engine.ForceResult;
        replay?: SG.Replay.IReplay;
        currencyMultiplier?: number;
    }

    export class LogicRequest extends SG.GLM.Core.LogicRequest
    {
        protected onComplete(req: XMLHttpRequest)
        {
            return RS.Table.IPlatform.get().networkResponseReceived(req);
        }
    }
}
