namespace SG.GLM
{
    /**
     * Encapsulates a table engine interface for a GLM.
     */
    export class Engine<TModels extends RS.Table.Models = RS.Table.Models, TForceResult extends RS.Table.Engine.ForceResult = RS.Table.Engine.ForceResult> extends SG.GLM.Core.Engine<TModels, TForceResult> implements RS.Table.Engine.IEngine<TModels, Engine.Settings, Core.Engine.ReplayData, TForceResult>
    {
        public constructor(public readonly settings: Engine.Settings, public readonly models: TModels)
        {
            super(settings, models);
        }

        public async bet(payload: RS.Engine.ILogicRequest.Payload): Promise<LogicResponse>
        {
            const response = await this.logic(payload);
            if (!response.isError)
            {
                this.clearResultsModel(this.models);
                this.parseGame(response.gameResult);
            }
            this.onBetResponseReceived.publish(response);

            return response as LogicResponse;
        }

        protected getSessionInfo(): RS.IPlatform.SessionInfo
        {
            return RS.Table.IPlatform.get().sessionInfo;
        }

        /**
         * Handles a successful init request
         */
        protected handleInitSuccess(response: InitResponse)
        {
            if (response.stakeRanges)
            {
                this.parseStakeRanges(response.stakeRanges);
            }
            return super.handleInitSuccess(response);
        }

        protected parseStakeRanges(data: StakeRange[])
        {
            for (const value of data)
            {
                const stakeRange: RS.Table.Models.StakeRange =
                {
                    maxBet: value.maxBet,
                    maxPayout: value.maxPayout,
                    minBet: value.minBet,
                    type: value.type
                };

                this.models.stakeRanges.push(stakeRange);
            }
        }

        protected allocateWins(data: GameResult): void
        {
            const resultsModel = this.getResultsModel(this.models);
            let accumulatedWin: number = data.totalWin;
            for (let i: number = resultsModel.length - 1; i >= 0; i--)
            {
                const result = resultsModel[i];
                result.win.accumulatedWin = accumulatedWin;
                accumulatedWin -= result.win.totalWin;
            }
        }

        /**
         * Updates the before and after balances on all spin results.
         */
        protected calculateBalances(gameResult: GameResult)
        {
            const endBalance = this.models.customer.finalBalance;
            const shouldSubtractWin: boolean = !this.isEngineBalanceFrozen(gameResult);

            const resultsModel = this.getResultsModel(this.models);

            let lastBalance = RS.Models.Balances.clone(endBalance);
            // Start at the final balance, and work backwards through each spin result
            for (let i = resultsModel.length - 1; i >= 0; --i)
            {
                const result = resultsModel[i];
                result.balanceBefore = lastBalance;
                result.balanceAfter = lastBalance;

                if (shouldSubtractWin)
                {
                    const totalWin = result.win.totalWin;
                    result.balanceBefore = RS.Models.Balances.clone(lastBalance);

                    this.fudgeWinFromBalances(result.balanceBefore, totalWin);
                    lastBalance = result.balanceBefore;
                }
            }
        }

        protected parseGameVariant(gameVariant: GameVariantInfo)
        {
            if (gameVariant.lowRTP != null)
            {
                this.models.config.minRTP = gameVariant.lowRTP;
            }
            else
            {
                this.models.config.minRTP = gameVariant.RTP;
            }
            if (gameVariant.highRTP != null)
            {
                this.models.config.maxRTP = gameVariant.highRTP;
            }
            else
            {
                this.models.config.maxRTP = gameVariant.RTP;
            }
        }

        protected getResultsModel(models: TModels): RS.Models.GameResults
        {
            return models.gameResults;
        }

        protected clearResultsModel(models: TModels)
        {
            RS.Models.GameResults.clear(this.getResultsModel(models));
        }
    }

    export namespace Engine
    {
        //export imports for backwards compatibility
        export import NetworkSettings = Core.Engine.NetworkSettings;
        export import UserSettings = Core.Engine.UserSettings;
        export import ReplayData = Core.Engine.ReplayData;

        export interface Settings extends SG.GLM.Core.Engine.Settings
        {
            initRequest: { new(networkSettings: Core.Engine.NetworkSettings, userSettings: Core.Engine.UserSettings): InitRequest; };
            spinRequest: { new(settings: Core.Engine.NetworkSettings, userSettings: Core.Engine.UserSettings): LogicRequest; };
        }
    }
}
