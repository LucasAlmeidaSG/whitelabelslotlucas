/// <reference path="../CommonData.ts" />

namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    /**
    Super Fun 21 Extreme Init
    <?xml version="1.0" encoding="UTF-8"?>
    <GameResponse type="Init">
       <Header sessionID="al2ZMSvWg2AX0VNG9+z4EFd/kZ6/qUZHDUsIxT/KRWUi71aoDb1evQQy9fmUWopw" ccyCode="" deciSep="." thousandSep="," lang="en_GB" gameID="20362" versionID="1_0" fullVersionID="1.0.4" isRecovering="N" readyForEndGame="N" />
       <AccountData />
       <Balances>
          <Balance name="CASH_BALANCE" value="100000" />
       </Balances>
       <Stakes count="7" defaultIndex="3" type="0">10|50|100|500|1000|2000|4000</Stakes>
       <StakeRanges>
          <Stake type="ANTE" minBet="10" maxBet="4000" />
          <Stake type="DIAMOND" minBet="0" maxBet="4000" />
       </StakeRanges>
       <RequestTypes deal="0" hit="1" split="2" double="3" stand="4" insurance="5" surrender="6" />
       <GameVariantInfo RTP="99.33" />
    </GameResponse>
    */

    /* tslint:disable:member-ordering */

    export class GameVariantInfo extends SG.GLM.Core.GameVariantInfo
    {
        @XML.Property({ path: ".RTP", type: XML.Number })
        public RTP: number;
    }

    @XML.Type("Stake")
    export class StakeRange
    {
        @XML.Property({ path: ".type", type: XML.String })
        public type: string;

        @XML.Property({ path: ".minBet", type: XML.Number })
        public minBet: number;

        @XML.Property({ path: ".maxBet", type: XML.Number })
        public maxBet: number;

        @XML.Property({ path: ".maxPayout", type: XML.Number })
        public maxPayout: number;
    }

    export class InitResponse extends SG.GLM.Core.InitResponse
    {
        public constructor(public readonly request: InitRequest) { super(request); }

        @XML.Property({ path: "StakeRanges", type: XML.Array(XML.Object) })
        public stakeRanges: StakeRange[];

        @XML.Property({ path: "GameVariantInfo", type: XML.ExplicitObject(GameVariantInfo) })
        public gameVariantInfo?: GameVariantInfo;
    }

    /* tslint:enable:member-ordering */
}
