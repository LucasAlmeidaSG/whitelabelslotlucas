/// <reference path="../CommonData.ts" />

namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    /*
    American Roulette Win
    <GameResponse type="Logic">
      <Header sessionID="0IhezXeoP0gxfDOS8ln7fwunHqTVzcpkrQthdYg/kh9wRbBb8L8I0KmJdKvyGP5MCPINovL+niCWl+cLYLr6GOQYX66MvArEWUlHKYjTias=" ccyCode="en_GB" deciSep="." thousandSep="," lang="en_GB" gameID="20352" versionID="1_0" fullVersionID="1.0.8" isRecovering="N" readyForEndGame="Y"/>
      <AccountData>
        <AccountData>
          <CurrencyMultiplier>1</CurrencyMultiplier>
        </AccountData>
      </AccountData>
      <Balances>
        <Balance name="CASH_BALANCE" value="106800"/>
      </Balances>
      <GameResult stake="200" totalWin="7200" betID="">
        <BallPosition positionOnWheel="21" resultingNumber="10"/>
        <Wins>
          <Bet id="10" stake="200" win="7200"/>
        </Wins>
        <History numbers="10|12|17|24|37|23|23|34|14|5|5|37|37|37|32|2|17|17|17|32"/>
        <Stake>
          <Bet id="10" stakeAmount="200"/>
        </Stake>
        <SplitBets numPairBets="0" numSixBets="0" numStreetBets="0" numPickBets="1"/>
      </GameResult>
    </GameResponse>
    */

    /* tslint:disable:member-ordering */

    @XML.Type("Bet")
    export class Win
    {
        @XML.Property({ path: ".id", type: XML.Number })
        public id: number;

        @XML.Property({ path: ".stake", type: XML.Number })
        public stake: number;

        @XML.Property({ path: ".win", type: XML.Number })
        public win: number;
    }

    export class GameResult extends SG.GLM.Core.GameResult
    {
        @XML.Property({ path: "Wins", type: XML.Array(XML.ExplicitObject(Win)) })
        public wins: Win[];
    }

    export class LogicResponse extends SG.GLM.Core.LogicResponse
    {
        public constructor(public readonly request: LogicRequest) { super(request); }

        @XML.Property({ path: "GameResult", type: XML.ExplicitObject(GameResult) })
        public gameResult: GameResult;
    }

    /* tslint:enable:member-ordering */
}
