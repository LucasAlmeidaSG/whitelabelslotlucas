namespace SG.UI
{
    @RS.HasCallbacks
    export class TableUIController implements RS.Table.UI.IUIController
    {
        public settings: RS.Table.UI.IUIController.Settings;

        //on<x>Requested kept for legacy purposes?
        /** Published when a spin has been requested by the user. */
        @RS.AutoDispose
        protected _onBetClicked = RS.createSimpleEvent();
        public get onSpinRequested() { return this._onBetClicked.public; }
        public get onSpinClicked() { return this._onBetClicked.public; }
        public get onBetClicked() { return this._onBetClicked.public; }

        /** Published when a skip has been requested by the user. */
        @RS.AutoDispose
        protected _onSkipClicked = RS.createSimpleEvent();
        public get onSkipRequested() { return this._onSkipClicked.public; }
        public get onSkipClicked() { return this._onSkipClicked.public; }

        /** Published when the paytable has been requested by the user. */
        @RS.AutoDispose
        protected _onPaytableClicked = RS.createSimpleEvent();
        public get onPaytableRequested() { return this._onPaytableClicked.public; }
        public get onPaytableClicked() { return this._onPaytableClicked.public; }

        /** Published when the help page has been requested by the user. */
        @RS.AutoDispose
        protected _onHelpClicked = RS.createSimpleEvent();
        public get onHelpRequested() { return this._onHelpClicked.public; }
        public get onHelpClicked() { return this._onHelpClicked.public; }

        /** Published when undo has been requested by the user. */
        @RS.AutoDispose
        protected _onUndoClicked = RS.createSimpleEvent();
        public get onUndoClicked() { return this._onUndoClicked.public; }

        /** Published when clear has been requested by the user. */
        @RS.AutoDispose
        protected _onClearClicked = RS.createSimpleEvent();
        public get onClearClicked() { return this._onClearClicked.public; }

        /** Published when the settings control has been requested by the user. */
        @RS.AutoDispose
        protected _onSettingsRequested = RS.createSimpleEvent();
        public get onSettingsRequested() { return this._onSettingsRequested.public; }

        protected _isDisposed: boolean = false;
        protected _selectedInputOptions: number[] = [];

        @RS.AutoDispose protected _orientationChangedListener: RS.IDisposable;

        @RS.AutoDispose protected _bottomBar: CommonUI.BottomBar;
        @RS.AutoDispose protected _ui: RS.Table.UI.AbstractUI;

        @RS.AutoDisposeOnSet protected _uiEnabledHandle: RS.IArbiterHandle<boolean>;

        protected _desktopSpinners: CommonUI.DesktopButtonPanel.SpinnerSettings[];
        protected _mobileSpinners: CommonUI.MobileBetMenu.SpinnerSettings[];

        /** Gets a rectangle representing how much of the attached container is fully occluded by the UI on all sides. */
        public get enclosedSpace(): RS.Flow.Spacing
        {
            if (this._bottomBar == null) { return RS.Flow.Spacing.none; }
            return {
                left: 0,
                top: 0,
                right: 0,
                bottom: Math.round(this._bottomBar.bottomPanelHeight * this._bottomBar.scaleY)
            };
        }

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        public initialise(settings: RS.Table.UI.IUIController.Settings): void
        {
            this.settings = settings;

            this._orientationChangedListener = settings.orientationObservable ? settings.orientationObservable.onChanged(this.handleOrientationChanged) : RS.Orientation.onChanged(this.handleOrientationChanged);
        }

        /** Creates the UI with the specified settings. */
        public create(settings: RS.Table.UI.IUIController.InitSettings): void
        {
            const platform = RS.IPlatform.get();
            this._bottomBar = CommonUI.bottomBar.create(CommonUI.BottomBar.defaultSettings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    messageTextArbiter: this.settings.messageTextArbiter,
                    showBackButton: platform.canNavigateToLobby,
                    showHelpButton: platform.canNavigateToExternalHelp
                });

            this._bottomBar.locale = settings.locale;
            this._bottomBar.currencyFormatter = settings.currencyFormatter;
            this._bottomBar.onBackClicked(this.handleBackClicked);
            this._bottomBar.onSettingsClicked(this.handleSettingsClicked);
            this._bottomBar.onHelpClicked(this.handleHelpClicked);
            this._bottomBar.bindToObservables({ cornerListExpanded: this.settings.enableInteractionArbiter });

            this.createUI(settings);

            this.handleOrientationChanged(RS.Orientation.currentOrientation);
        }

        public updateLabel(props: Partial<RS.Flow.Label.Settings>): void
        {
            this._ui.updateLabel(props);
        }

        /** Updates the balance to be displayed on the UI. */
        public setBalance(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.balance = value;
        }

        /** Updates the win amount to be displayed on the UI. */
        public setWin(value: number, final: boolean)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.winAmount = value;
        }

        /** Updates the total bet to be displayed on the UI. */
        public setTotalBet(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.totalBet = value;
        }

        /** Updates the line count to be displayed on the UI. */
        public setLines(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.lines = value;
        }

        public setWays(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.ways = value;
        }

        /** Sets replay mode. */
        public setReplayMode(value: boolean)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.replayMode = value;
        }

        /** Attaches the UI to the specified container. */
        public attach(view: RS.Rendering.IContainer): void
        {
            if (this._bottomBar != null) { view.addChild(this._bottomBar); }
            if (this._ui != null) { view.addChild(this._ui); }
        }

        /** Detaches the UI from the specified container, if it is attached to that. */
        public detach(view: RS.Rendering.IContainer): void
        {
            if (this._bottomBar.parent === view) { view.removeChild(this._bottomBar); }
            if (this._ui.parent === view) { view.removeChild(this._ui); }
        }

        public setChipSelectorVisible(visible: boolean): void
        {
            if (this._ui && this._ui.chipSelector)
            {
                this._ui.chipSelector.visible = visible;
            }
            else
            {
                RS.Log.warn("chip selector not found");
            }
        }

        public setButtonGroup(group: number): void
        {
            if (this._ui)
            {
                this._ui.setButtonGroup(group);
            }
            else
            {
                RS.Log.warn("ui not found");
            }
        }

        public setButtonsEnabled(enabled: boolean)
        {
            this._ui.enabled = enabled;
        }

        public setAlwaysVisibleButtonsEnabled(enabled: boolean)
        {
            this._ui.alwaysVisibleEnabled = enabled;
        }

        /**
         * Disposes this controller.
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }

            this._isDisposed = true;
        }

        @RS.Callback
        protected handleBetClicked()
        {
            this._onBetClicked.publish();
        }

        @RS.Callback
        protected handleClearClicked()
        {
            this._onClearClicked.publish();
        }

        @RS.Callback
        protected handleUndoClicked()
        {
            this._onUndoClicked.publish();
        }

        @RS.Callback
        protected handleBackClicked()
        {
            RS.IPlatform.get().navigateToLobby();
        }

        @RS.Callback
        protected handleSettingsClicked()
        {
            this._onSettingsRequested.publish();
            RS.IPlatform.get().openSettingsMenu();
        }

        @RS.Callback
        protected handleHelpClicked()
        {
            this._onHelpClicked.publish();
        }

        @RS.Callback
        protected handlePaytableClicked()
        {
            this._onPaytableClicked.publish();
        }

        @RS.Callback
        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation)
        {
            if (this._bottomBar != null)
            {
                if (newOrientation === RS.DeviceOrientation.Landscape)
                {
                    this._bottomBar.mode = RS.Device.isMobileDevice ? CommonUI.BottomBar.Mode.MobileLandscape : CommonUI.BottomBar.Mode.Landscape;
                }
                else
                {
                    this._bottomBar.mode = RS.Device.isMobileDevice ? CommonUI.BottomBar.Mode.MobilePortrait : CommonUI.BottomBar.Mode.Portrait;
                }
            }
        }

        protected createUI(settings: RS.Table.UI.IUIController.InitSettings): void
        {
            // override to create ui
            // this._ui = ...

            this._ui.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            this._ui.locale = settings.locale;
            this._ui.currencyFormatter = settings.currencyFormatter;
            this.setUIButtonListeners();
        }

        protected setUIButtonListeners(): void
        {
            this._ui.onBetClicked(this.handleBetClicked);
            this._ui.onClearClicked(this.handleClearClicked);
            this._ui.onUndoClicked(this.handleUndoClicked);
            this._ui.onHelpClicked(this.handlePaytableClicked);
        }
    }

    RS.UI.IUIController.register(TableUIController);
}
