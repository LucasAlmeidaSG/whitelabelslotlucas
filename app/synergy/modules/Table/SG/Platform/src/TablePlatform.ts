namespace SG.Environment
{
    enum WinUpdateState { None, NextTick, Now }

    @RS.HasCallbacks
    class TablePlatform implements RS.Table.IPlatform
    {
        private _settings: RS.Table.IPlatform.Settings;
        private _stateMachine: RS.ObservableStateMachine<RS.PlatformGameState>;
        private _balanceUpdateRequired: boolean;
        private _winUpdateRequired: WinUpdateState = WinUpdateState.None;
        private _didUpdateWinThisPlay: boolean;

        private get platform(): Platform { return RS.IPlatform.get() as Platform; }
        private get partnerAdapter(): SGI.IPartnerAdapter { return this.platform.adapter; }

        private get cashBalance() { return this._settings.balancesObservable.get(RS.Models.Balances.Cash) || 0; }
        private get freeBetsBalance() { return this._settings.balancesObservable.get(RS.Models.Balances.Promotional) || 0; }
        private get totalBalance() { return this._settings.balanceObservable.value; }

        public get sessionInfo(): RS.IPlatform.SessionInfo
        {
            const partnerAdapter = this.partnerAdapter;
            if (partnerAdapter == null) { return { userName: null, userID: null, sessionID: null, affiliateID: null }; }
            const sessionData = partnerAdapter.getSessionData();
            if (sessionData == null) { return { userName: null, userID: null, sessionID: null, affiliateID: null }; }
            return {
                userName: sessionData.getPlayerID(),
                userID: parseInt(sessionData.getPlayerID()),
                sessionID: sessionData.getSessionID(),
                affiliateID: sessionData.getContextID()
            };
        }

        public init(settings: RS.Table.IPlatform.Settings): void
        {
            this._settings = settings;
            this._settings.balanceObservable.onChanged(this.handleBalanceChanged);
            this._settings.balancesObservable.onChanged(this.handleNamedBalanceChanged);
            this._settings.winObservable.onChanged(this.handleWinAmountChanged);
            this._settings.stakeObservable.onChanged(this.handleStakeChanged);

            this._stateMachine = new RS.ObservableStateMachine(settings.gameStateObservable);

            // Not ready -> ready, standard startup routine
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.NotReady, RS.PlatformGameState.Ready, () =>
            {
                this._winUpdateRequired = WinUpdateState.None;
                this._balanceUpdateRequired = false;
                this.handleGameInitialised(false, this.cashBalance, this.freeBetsBalance, this.totalBalance);
                (RS.IPlatform.get() as SG.Environment.Platform).handleBalanceDisplay(this.cashBalance, this.freeBetsBalance, this.totalBalance);
                this.updateWin();
                this.partnerAdapter.updateBet(this._settings.stakeObservable.value);

                this.partnerAdapter.gameReady();
                this._didUpdateWinThisPlay = false;
            });

            // Not ready -> in play, usually when resuming
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.NotReady, RS.PlatformGameState.InPlay, () =>
            {
                this.handleGameInitialised(true, this.cashBalance, this.freeBetsBalance, this.totalBalance);
                this.updateBalanceWinAndStakeNow();
                this.partnerAdapter.gameReady();
                this.partnerAdapter.startedPlay();
                this._didUpdateWinThisPlay = false;
            });

            // Not ready -> resuming
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.NotReady, RS.PlatformGameState.Resuming, () =>
            {
                this.handleGameInitialised(true, this.cashBalance, this.freeBetsBalance, this.totalBalance);
                this.partnerAdapter.gameReady();
                this._didUpdateWinThisPlay = false;
            });

            // Resuming -> in play, called when dialog is accepted
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.Resuming, RS.PlatformGameState.InPlay, () =>
            {
                this.partnerAdapter.startedPlay();
            });

            // Ready -> in play, user input
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.Ready, RS.PlatformGameState.InPlay, () =>
            {
                this.partnerAdapter.startedPlay();
                this._didUpdateWinThisPlay = false;
            });

            // In play post win rendering -> ready, back to ready after showing win
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.InPlayPostWinRendering, RS.PlatformGameState.Ready, this.onPlayFinished);

            // In play -> ready, usually when win rendering was skipped for some reason (maybe max win) and game switches back to idle
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.InPlay, RS.PlatformGameState.Ready, this.onPlayFinished);

            RS.Ticker.registerTickers(this);
        }

        public networkResponseReceived(request: XMLHttpRequest): void
        {
            if (request.status !== 200)
            {
                let callback: () => void;
                const lobbyNavService = this.partnerAdapter.getLobbyNavigationService();
                if (lobbyNavService && lobbyNavService.canGoToLobby())
                {
                    callback = () => this.partnerAdapter.goHome();
                }
                else
                {
                    callback = () => this.partnerAdapter.reload();
                }

                const locale = RS.IPlatform.get().locale;
                this.partnerAdapter.showError(Translations.Error.Title.get(locale), Translations.Error.Generic.Message.get(locale), Translations.Error.Button.get(locale), callback);
                return;
            }

            // for whatever reason the PA decides to throw on a server error
            // let's not let it screw up the rest of our stack frame
            try
            {
                this.partnerAdapter.receivedGameLogicResponse(request);
            }
            catch (err)
            {
                RS.Log.error(`Partner adapter error: ${err}`);
            }
        }

        @RS.Callback
        protected onPlayFinished()
        {
            // Don't re-order this method unless explicitly told to

            if (!this._didUpdateWinThisPlay)
            {
                this.updateWin();
                this._didUpdateWinThisPlay = true;
            }

            // Shubham Halle: finishedPostGameAnimations() should be called after finishedPlay()
            this.partnerAdapter.finishedPlay();
            this.partnerAdapter.finishedPostGameAnimations();

            // Order copied from rainbowriches_prt
            this.updateBalanceWinAndStakeNow();
        }

        protected handleGameInitialised(resume: boolean, cash: number, freebet: number, balance: number): void
        {
            (RS.IPlatform.get() as SG.Environment.Platform).handleGameInitialised(resume, cash, freebet, balance);
        }

        protected updateBalanceWinAndStakeNow()
        {
            this.updateWin();
            this._balanceUpdateRequired = true;
            this.partnerAdapter.updateBet(this._settings.stakeObservable.value);
        }

        protected updateWin(): void
        {
            (RS.IPlatform.get() as SG.Environment.Platform).handleWinDisplay(this._settings.winObservable.value);
        }

        @RS.Callback
        protected handleBalanceChanged(newBalance: number): void
        {
            this._balanceUpdateRequired = true;
        }

        @RS.Callback
        protected handleNamedBalanceChanged(data: RS.ObservableMap.OnChangedData<number>): void
        {
            this._balanceUpdateRequired = true;
        }

        @RS.Callback
        protected handleStakeChanged(newStake: number): void
        {
            this.partnerAdapter.updateBet(newStake);
        }

        @RS.Tick({ kind: RS.Ticker.Kind.FixedStep, data: 100 })
        protected tick(): void
        {
            if (this._balanceUpdateRequired)
            {
                this._balanceUpdateRequired = false;
                (RS.IPlatform.get() as SG.Environment.Platform).handleBalanceDisplay(this.cashBalance, this.freeBetsBalance, this.totalBalance);
            }
            if (this._winUpdateRequired === WinUpdateState.NextTick)
            {
                this._winUpdateRequired = WinUpdateState.Now;
            }
            else if (this._winUpdateRequired === WinUpdateState.Now)
            {
                this._winUpdateRequired = WinUpdateState.None;
                this.updateWin();
                this._didUpdateWinThisPlay = true;
            }
        }

        @RS.Callback
        protected handleWinAmountChanged(newWinAmount: number): void
        {
            this._winUpdateRequired = WinUpdateState.NextTick;
        }
    }

    RS.Table.IPlatform.register(TablePlatform);
}
