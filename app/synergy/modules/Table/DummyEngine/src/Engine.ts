namespace RS.Table
{
    /**
     * Encapsulates a slots engine interface for the William Hill engine.
     */
    export class DummyEngine<TModels extends Table.Models = Table.Models, TSettings extends DummyEngine.Settings = DummyEngine.Settings, TReplayData = {}, TForceResult extends Table.Engine.ForceResult = Table.Engine.ForceResult> extends RS.DummyEngine<TModels, TSettings, TReplayData, TForceResult>
    {
        public async init(): Promise<RS.Engine.IResponse>
        {
            this.models.stake.minBet = this.settings.minBet;
            this.models.stake.maxBet = this.settings.maxBet;
            return await super.init();
        }

        protected getResultsModel(models: TModels): RS.Models.GameResults
        {
            return models.gameResults;
        }

        protected clearResultsModel(models: TModels): void
        {
            RS.Models.GameResults.clear(this.getResultsModel(models));
        }
    }

    export namespace DummyEngine
    {
        export interface Settings extends RS.DummyEngine.Settings
        {
            minBet: number;
            maxBet: number;
        }

        export const defaultSettings: Settings =
        {
            ...RS.DummyEngine.defaultSettings,
            //settings taken from blackjack
            betOptions: [ 10, 50, 100, 500, 1000, 2500, 5000 ],
            defaultBetOption: 3,
            minBet: 10,
            maxBet: 10000
        };
    }
}
