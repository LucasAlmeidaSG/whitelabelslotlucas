namespace RS.Table.UI
{
    export class ChipSelector<TSettings extends ChipSelector.Settings = ChipSelector.Settings, TRuntimeData extends ChipSelector.RuntimeData = ChipSelector.RuntimeData> extends Flow.BaseElement<TSettings, TRuntimeData>
    {
        @AutoDisposeOnSet protected _rightArrow: Flow.Button;
        @AutoDisposeOnSet protected _leftArrow: Flow.Button;
        @AutoDisposeOnSet protected _chipList: Flow.List;
        @AutoDisposeOnSet protected _chipContainer: Flow.Container;
        @AutoDisposeOnSet protected _chipContainerMask: RS.Rendering.IGraphics;
        @AutoDisposeOnSet protected _showTween: Tween<ChipSelector>;

        @AutoDisposeOnSet protected chips: Chip[];
        protected offset: number;

        public set enabled(value: boolean)
        {
            this._rightArrow.enabled = this._leftArrow.enabled = value;
            if (value)
            {
                this.updateArrows();
            }
            this.interactiveChildren = value;
        }

        constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);
            this.createComponents();
        }

        public apply(settings: Partial<ChipSelector.Settings>)
        {
            this.suppressLayouting();
            if (settings.leftArrow && this._leftArrow)
            {
                this._leftArrow.apply(settings.leftArrow);
            }
            if (settings.rightArrow && this._rightArrow)
            {
                this._rightArrow.apply(settings.rightArrow);
            }
            if (settings.chipList && this._chipList)
            {
                this._chipList.apply(settings.chipList);
            }
            if (this._chipContainer)
            {
                if (settings.chipContainer)
                {
                    this._chipContainer.apply(settings.chipContainer);
                }
                this.updateMask(settings.chipContainerMask);
            }
            if (settings.genericChip && this.chips)
            {
                this.chips.forEach((chip) => chip.apply(settings.genericChip));
            }
            super.apply(settings);
            this.restoreLayouting();
        }

        public async show(value: boolean, animated: boolean = true)
        {
            if (value)
            {
                this.visible = true;
                if (animated)
                {
                    if (this._showTween)
                    {
                        this._showTween.finish(true);
                        this._showTween = null;
                    }
                    this._showTween = Tween.get(this.postTransform).from({ y: 0 }, 300, Ease.quadOut);
                    await this._showTween;
                    this.nativeObject.interactiveChildren = true;
                }
                else
                {
                    this.nativeObject.interactiveChildren = true;
                }
            }
            else
            {
                this.nativeObject.interactiveChildren = false;
                if (animated)
                {
                    if (this._showTween)
                    {
                        this._showTween.finish(true);
                        this._showTween = null;
                    }
                    this._showTween = Tween.get(this.postTransform).to({ y: this.height }, 300, Ease.quadIn);
                    await this._showTween;
                    this.visible = false;
                }
            }
        }

        public isEnabled(): boolean
        {
            return this.nativeObject.interactiveChildren;
        }

        protected createComponents()
        {
            const bg = Flow.background.create(this.settings.background, this);
            bg.name = "Background";

            if (this.settings.overlay)
            {
                const overlay = Flow.background.create(this.settings.overlay, this);
                overlay.name = "Overlay";
            }

            this._chipContainer = Flow.container.create(
                {
                    ...this.settings.chipContainer,
                    dock: Flow.Dock.Fill,
                    name: "Chip Container"
                }, bg);

            this._chipList = Flow.list.create(this.settings.chipList, this._chipContainer);

            this.updateMask(this.settings.chipContainerMask);

            this._leftArrow = Flow.button.create(this.settings.leftArrow, this);
            this._leftArrow.onClicked(this.leftClicked);

            this._rightArrow = Flow.button.create(this.settings.rightArrow, this);
            this._rightArrow.onClicked(this.rightClicked);

            //create chips
            this.chips = [];
            const offset = this.settings.chipIndexOffset == null ? 0 : this.settings.chipIndexOffset
            this.runtimeData.stakeModel.betOptions.forEach((stake, i) =>
            {
                const settings = { ...this.settings.genericChip, image: { ...this.settings.genericChip.image, animationName: (i + offset).toString() } };
                const runtimeData = { labelValue: this._currencyFormatter.format(stake.value, false, true), index: i };

                const chip = UI.chip.create(settings, runtimeData, this._chipList);
                chip.buttonMode = true;
                chip.onClicked(() => { this.chipClicked(i) });

                this.chips.push(chip);
            });
            this.invalidateLayout();
            if (this.chips[1])
            {
                this.offset = this.chips[0].x - this.chips[1].x;
            }
            else
            {
                this.offset = 0;
            }

            this.update(true);
        }

        protected async update(instant: boolean = false, difference: number = 1)
        {
            const targetX = this.offset * this.runtimeData.stakeModel.currentBetIndex;
            if (instant)
            {
                this._chipList.postTransform.x = targetX;
                this.chips.forEach((chip, i) =>
                {
                    chip.postTransform = i == this.runtimeData.stakeModel.currentBetIndex ? this.settings.activeTransform : this.settings.inactiveTransform;
                });
            }
            else
            {
                const promises = [];
                this._leftArrow.interactive = this._rightArrow.interactive = false;
                promises.push(Tween.get(this._chipList.postTransform).to({ x: targetX }, this.settings.animationTime * difference));
                this.chips.forEach((chip, i) =>
                {
                    const targetTransform = i == this.runtimeData.stakeModel.currentBetIndex ? this.settings.activeTransform : this.settings.inactiveTransform;
                    promises.push(Tween.get(chip.postTransform).to(targetTransform, this.settings.animationTime * difference));
                });
                await Promise.all(promises);
            }

            //handle arrows
            this.updateArrows();
        }

        protected updateArrows()
        {
            this._leftArrow.interactive = this._leftArrow.enabled = true;
            this._rightArrow.interactive = this._rightArrow.enabled = true;
            if (this.runtimeData.stakeModel.currentBetIndex == 0)
            {
                this._leftArrow.enabled = false;
            }
            if (this.runtimeData.stakeModel.currentBetIndex == this.runtimeData.stakeModel.betOptions.length - 1)
            {
                this._rightArrow.enabled = false;
            }
        }

        @Callback
        protected chipClicked(index: number)
        {
            if (index != this.runtimeData.stakeModel.currentBetIndex)
            {
                const difference = Math.abs(this.runtimeData.stakeModel.currentBetIndex - index);
                this.runtimeData.stakeModel.currentBetIndex = index;
                this.update(false, difference);
            }
        }

        @Callback
        protected leftClicked()
        {
            this.runtimeData.stakeModel.currentBetIndex--;
            this.update();
        }

        @Callback
        protected rightClicked()
        {
            this.runtimeData.stakeModel.currentBetIndex++;
            this.update();
        }

        protected updateMask(mask: RS.Rendering.Shape | RS.Rendering.IGraphics | RS.Rendering.IMovieClip | RS.Rendering.IBitmap)
        {
            if (mask instanceof RS.Rendering.Graphics)
            {
               this._chipContainer.addChild(this._chipContainerMask);
               return;
            }

            if (mask instanceof RS.Rendering.Bitmap || mask instanceof RS.Rendering.MovieClip)
            {
                this._chipContainer.mask = mask;
                return;
            }

            if (!this._chipContainerMask)
            {
                this._chipContainerMask = new RS.Rendering.Graphics();
            }
            if (mask)
            {
                this._chipContainerMask.clear();
                this._chipContainerMask.beginFill(Util.Colors.red, 1);
                if (mask instanceof RS.Rendering.Rectangle)
                {
                    this._chipContainerMask.drawRect(mask.x, mask.y, mask.w, mask.h);
                }
                else if (mask instanceof RS.Rendering.Circle)
                {
                    this._chipContainerMask.drawCircle(mask.x, mask.y, mask.r);
                }
                else if (mask instanceof RS.Rendering.Ellipse)
                {
                    this._chipContainerMask.drawEllipse(mask.x, mask.y, mask.w, mask.h);
                }
                else if (mask instanceof RS.Rendering.RoundedRectangle)
                {
                    this._chipContainerMask.drawRoundedRect(mask.x, mask.y, mask.w, mask.h, mask.r);
                }
                else if (mask instanceof RS.Rendering.Polygon)
                {
                    this._chipContainerMask.drawPolygon(mask.points);
                }
                this._chipContainerMask.endFill();
            }
            else
            {
                this._chipContainerMask.drawRect(new RS.Rendering.Rectangle(85, -500, 370, 670));
            }
            this._chipContainer.mask = this._chipContainerMask;
            this._chipContainer.addChild(this._chipContainerMask);
        }
    }

    export const chipSelector = Flow.declareElement(ChipSelector, true);

    export namespace ChipSelector
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            chipContainer?: Flow.Container.Settings;
            chipContainerMask?: RS.Rendering.Shape | RS.Rendering.IGraphics | RS.Rendering.IMovieClip | RS.Rendering.IBitmap;
            rightArrow: Flow.Button.Settings;
            leftArrow: Flow.Button.Settings;
            background: Flow.Background.Settings;
            overlay?: Flow.Background.Settings;
            chipList: Flow.List.Settings;
            genericChip: Chip.Settings;
            activeTransform: Flow.Transform;
            inactiveTransform: Flow.Transform;
            animationTime: number;
            chipIndexOffset?: number;
        }

        export interface RuntimeData
        {
            stakeModel: Models.Stake;
        }
    }
}
