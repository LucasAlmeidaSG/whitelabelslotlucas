namespace RS.Table.UI
{
    @HasCallbacks
    export abstract class AbstractUI<TButtons extends AbstractUI.Buttons = AbstractUI.Buttons, TSettings extends AbstractUI.Settings = AbstractUI.Settings, TRuntimeData extends AbstractUI.RuntimeData = AbstractUI.RuntimeData> extends Flow.BaseElement<TSettings, TRuntimeData>
    {
        @AutoDisposeOnSet protected _label: Flow.Label;

        protected _buttons: AbstractUI.ButtonType<TButtons>;
        @AutoDisposeOnSet protected _buttonList: Flow.List;
        @AutoDisposeOnSet protected _chipSelector: ChipSelector;
        public get chipSelector() { return this._chipSelector; }

        protected _currentButtonGroup: number;
        public get currentButtonGroup() { return this._currentButtonGroup; }

        protected _enabled: boolean;
        public get enabled() { return this._enabled; }
        public set enabled(value: boolean)
        {
            if (this._enabled === value) { return; }
            this._enabled = value;
            this.alwaysVisibleEnabled = value;

            for (const buttonStr in this._buttons)
            {
                const button = this._buttons[buttonStr];
                button.enabled = this._enabled;
            }

            if (this._enabled)
            {
                this.updateButtonStates();
            }
        }

        protected _alwaysVisibleEnabled: boolean;
        public get alwaysVisibleEnabled() { return this._alwaysVisibleEnabled; }
        public set alwaysVisibleEnabled(value: boolean)
        {
            if (this._alwaysVisibleEnabled === value) { return; }
            this._alwaysVisibleEnabled = value;
            for (const buttonStr in this._buttons)
            {
                const button = this._buttons[buttonStr];
                let alwaysVisible = false;
                if (Is.array(button.groups))
                {
                    alwaysVisible = button.groups.indexOf(this.settings.alwaysVisibleGroup) > -1;
                }
                else
                {
                    alwaysVisible = button.groups == this.settings.alwaysVisibleGroup;
                }

                if (alwaysVisible)
                {
                    button.enabled = this._alwaysVisibleEnabled;
                }
            }
        }

        /** Published when the bet button has been clicked. */
        public get onBetClicked(): IEvent<RS.Rendering.InteractionEventData> { return this._buttons.bet.onClicked; }

        /** Published when the clear button has been clicked. */
        public get onClearClicked(): IEvent<RS.Rendering.InteractionEventData> { return this._buttons.clear.onClicked; }

        /** Published when the undo button has been clicked. */
        public get onUndoClicked(): IEvent<RS.Rendering.InteractionEventData> { return this._buttons.undo.onClicked; }

        /** Published when the undo button has been clicked. */
        public get onHelpClicked(): IEvent<RS.Rendering.InteractionEventData> { return this._buttons.help.onClicked; }

        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);

            this.initialiseButtons();
            this.createComponents();

            this.setButtonGroup(this.settings.alwaysVisibleGroup);

            const chips = this.settings.is3D ? IChips3D.get() : IChips2D.get();
            const chipsHandler = chips.onChipsChanged(this.updateButtonStates);
            RS.Disposable.bind(chipsHandler, this);

            const interactionHandler = runtimeData.interactionArbiter.onChanged(this.handleInteractionChanged);
            RS.Disposable.bind(interactionHandler, this);

            const orientationHandle = Orientation.onChanged(this.handleOrientationChanged.bind(this));
            RS.Disposable.bind(orientationHandle, this);
        }

        public updateLabel(props: Partial<Flow.Label.Settings>)
        {
            if (this._label)
            {
                this._label.apply(props);
            }
        }

        public setButtonGroup(activeGroup: number)
        {
            for (const buttonStr in this._buttons)
            {
                const button = this._buttons[buttonStr];

                let setVisible = false;
                if (Is.array(button.groups))
                {
                    setVisible = button.groups.indexOf(activeGroup) > -1 || button.groups.indexOf(this.settings.alwaysVisibleGroup) > -1;
                }
                else
                {
                    setVisible = button.groups == activeGroup || button.groups == this.settings.alwaysVisibleGroup;
                }

                button.visible = setVisible;
            }
            this._currentButtonGroup = activeGroup;

            this.updateButtonStates();
        }

        //initialise the buttons object with things in TButtons
        protected initialiseButtons()
        {
            this._buttons = {
                bet: null,
                clear: null,
                undo: null,
                help: null
            } as AbstractUI.ButtonType<TButtons>;
        }

        protected createComponents(): void
        {
            if (this.settings.label)
            {
                this._label = Flow.label.create(this.settings.label, this);
            }

            this._buttonList = Flow.list.create(this.settings.buttonList, this);

            this.createButtons(this._buttonList);
            this.createChipSelector();
        }

        protected createButtons(parent: Flow.Container): void
        {
            this._buttons.bet = button.create(this.settings.buttons.bet, this.isFloat(this.settings.buttons.bet) ? this : parent);
            this._buttons.clear = button.create(this.settings.buttons.clear, this.isFloat(this.settings.buttons.clear) ? this : parent);
            this._buttons.undo = button.create(this.settings.buttons.undo, this.isFloat(this.settings.buttons.undo) ? this : parent);

            this._buttons.help = button.create(this.settings.buttons.help, this);
        }

        protected isFloat(settings: Partial<Flow.ElementProperties>)
        {
            return settings.dock === Flow.Dock.Float;
        }

        protected createChipSelector()
        {
            this._chipSelector = chipSelector.create({ ...this.settings.chipSelector, currencyFormatter: this.settings.currencyFormatter }, { stakeModel: this.runtimeData.models.stake }, this);
        }

        @Callback
        protected handleInteractionChanged(enabled: boolean)
        {
            if (enabled)
            {
                //enable the right buttons
                this.updateButtonStates();
            }
            else
            {
                //always disable all interaction if false
                for (const key in this._buttons)
                {
                    (this._buttons[key]).enabled = enabled;
                }
            }
        }

        @Callback
        protected updateButtonStates()
        {
            const chips = this.settings.is3D ? IChips3D.get() : IChips2D.get();

            const chipsOnTable = chips.areChips();
            this._buttons.bet.enabled = chipsOnTable;
            this._buttons.clear.enabled = chipsOnTable;
            this._buttons.undo.enabled = chipsOnTable;

            this._buttons.help.enabled = true;
        }

        protected abstract handleOrientationChanged(newOrientation: DeviceOrientation): void;
    }

    export namespace AbstractUI
    {
        export type ButtonType<TButtons> = { [P in keyof TButtons]: Button };

        export interface Buttons
        {
            bet: Button;
            clear: Button;
            undo: Button;
            help: Button;
        }

        export interface ButtonsSettings
        {
            help: Button.Settings;
            bet: Button.Settings;
            clear: Button.Settings;
            undo: Button.Settings;
        }

        // tslint:disable-next-line
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            label?: Flow.Label.Settings;
            buttons: ButtonsSettings | any;
            chipSelector: ChipSelector.Settings;
            buttonList: Flow.List.Settings;

            //group name for buttons that will always be visible regardless of active group
            alwaysVisibleGroup?: number;

            /** To get either 2D or 3D chips, defaults to 2D */
            is3D?: boolean;
        }

        // tslint:disable-next-line
        export interface RuntimeData
        {
            models: Models;
            interactionArbiter: IArbiter<boolean>;
        }
    }
}
