namespace RS.Table.UI
{
    export class Chip extends Flow.BaseElement<Chip.Settings, Chip.RuntimeData>
    {
        @AutoDisposeOnSet protected _image: Flow.Image;
        @AutoDisposeOnSet protected _shadow: Flow.Image;
        @AutoDisposeOnSet protected _label: Flow.Label;

        constructor(settings: Chip.Settings, runtimeData: Chip.RuntimeData)
        {
            super(settings, runtimeData);
            this.createElements();
        }

        protected createElements()
        {
            if (this.settings.shadow)
            {
                this._shadow = Flow.image.create(this.settings.shadow, this);
            }
            this._image = Flow.image.create(this.settings.image, this);
            this._label = Flow.label.create({ ...this.settings.label, text: this.runtimeData.labelValue }, this._image);
        }
    }

    export const chip = Flow.declareElement(Chip, true);

    export namespace Chip
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            image: Flow.Image.Settings;
            label: Flow.Label.Settings;
            shadow?: Flow.Image.Settings;
        }

        export interface RuntimeData
        {
            index: number;
            labelValue: string;
        }
    }
}
