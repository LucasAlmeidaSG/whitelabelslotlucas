namespace RS.Table.UI
{
    export class Button extends Flow.Button
    {
        protected _groups: OneOrMany<number>;
        public get groups() { return this._groups; };
        public set groups(groups: OneOrMany<number>) { this._groups = groups; };

        constructor(settings: Button.Settings)
        {
            super(settings);
            if (settings.groups == null)
            {
                Log.warn("no group set for ui button");
            }
            this._groups = settings.groups;
        }
    }

    export const button = Flow.declareElement(Button)

    export namespace Button
    {
        export interface Settings extends Flow.Button.Settings
        {
            groups?: OneOrMany<number>;
        }
    }
}
