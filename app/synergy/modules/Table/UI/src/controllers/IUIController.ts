namespace RS.Table.UI
{
    /**
     * Responsible for interfacing the UI with the game logic.
     */
    //tslint:disable-next-line
    export interface IUIController<TSettings extends IUIController.Settings = IUIController.Settings> extends RS.UI.IUIController<TSettings>
    {
        readonly onBetClicked: IEvent;
        readonly onClearClicked: IEvent;
        readonly onUndoClicked: IEvent;

        setChipSelectorVisible(visible: boolean): void;

        setButtonGroup(group: number): void;

        setButtonsEnabled(enabled: boolean): void;

        setAlwaysVisibleButtonsEnabled(enabled: boolean): void;

        /** Creates the UI with the specified settings. */
        create(settings: IUIController.InitSettings): void;

        /** Update abstractUI Label with given props */
        updateLabel(props: Partial<Flow.Label.Settings>): void;
    }

    export const IUIController = Controllers.declare<IUIController, IUIController.Settings>(Strategy.Type.Instance);

    export namespace IUIController
    {
        /**
         * Settings for the UI controller.
         */
        export interface Settings extends RS.UI.IUIController.Settings
        {
            ui: AbstractUI.Settings
        }

        export interface InitSettings extends RS.UI.IUIController.InitSettings
        {
            models: Models;
        }
    }
}
