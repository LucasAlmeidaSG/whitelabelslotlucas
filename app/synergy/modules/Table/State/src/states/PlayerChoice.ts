/// <reference path="../States.ts" />

namespace RS.Table.GameState
{
    /**
     * The "playerChoice" state.
     * Game is waiting for player input during a game (e.g. blackjack decisions)
     */
    @HasCallbacks
    @State.Name(GameState.PlayerChoice)
    export class PlayerChoiceState<T extends Game.Context = Game.Context> extends RS.State.Base<T>
    {
        @AutoDisposeOnSet protected _statusTextHandler: IDisposable;

        public onEnter()
        {
            super.onEnter();
            this._context.game.observables.platformGameState.value = RS.PlatformGameState.Ready;

            this._context.isResuming = false;
        }
    }
}
