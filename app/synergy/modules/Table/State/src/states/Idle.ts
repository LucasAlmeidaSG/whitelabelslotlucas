namespace RS.Table.GameState
{
    /**
     * The "idle" state.
     * The game is closed and is ready to start.
     */
    @HasCallbacks
    @State.Name(RS.GameState.Idle)
    export class IdleState<T extends Game.Context = Game.Context> extends RS.GameState.IdleState<T>
    {
        @AutoDisposeOnSet protected _replaySpinBlock: IDisposable;
        @AutoDisposeOnSet protected _autoBetHandle: IDisposable;
        @AutoDisposeOnSet protected _shouldAutoBetHandler: IDisposable;

        public async onEnter()
        {
            this._context.game.uiController.setChipSelectorVisible(true);
            this._context.game.uiController.setAlwaysVisibleButtonsEnabled(true);

            super.onEnter();
        }

        public onExit()
        {
            this._context.game.uiController.setAlwaysVisibleButtonsEnabled(false);

            super.onExit();
        }

        protected checkAutoBet(): void
        {
            const arbiters = this._context.game.arbiters;
            // Should we spin immediately?
            if (arbiters.shouldAutoBet.value && arbiters.canSpin.value)
            {
                this.autospinWaitTween();

                this._spinOrSkip = arbiters.spinOrSkip.declare("skip");
                this._canSkip = arbiters.canSkip.declare(false);
            }
            else
            {
                this.stayIdle();
            }
        }

        protected doReplay(): void
        {
            const { arbiters, models } = this._context.game;
            // If we're replaying, autospin
            if (models.state.isReplay)
            {
                if (models.state.isReplayComplete)
                {
                    this._replaySpinBlock = arbiters.canSpin.declare(false);
                }
                else
                {
                    this._autoBetHandle = arbiters.shouldAutoBet.declare(true);
                    this._canSpin = arbiters.canSpin.declare(true);
                }
            }
        }

        protected stayIdle()
        {
            super.stayIdle();

            this._shouldAutoBetHandler = this._context.game.arbiters.shouldAutoBet.onChanged(this.handleAutoBetArbiterUpdated);
            this._canSpinHandler = this._context.game.arbiters.canSpin.onChanged(this.handleAutoBetArbiterUpdated);
        }

        @Callback
        protected handleAutoBetArbiterUpdated(): void
        {
            const arbiters = this._context.game.arbiters;

            if (arbiters.shouldAutoBet.value && arbiters.canSpin.value)
            {
                this._canSpin = arbiters.canSpin.declare(false);
                this.autospinWaitTween();
            }
            else if (!arbiters.shouldAutoBet.value && this._autospinWaitTween != null)
            {
                // If the player has cancelled autoplay while we're in the 1s timer, dispose it here to prevent another spin happening
                this._autospinWaitTween = null;
                this._canSpin = arbiters.canSpin.declare(true);
            }
        }

        @Callback
        protected async handleSpinRequested()
        {
            if (!IChips2D.get().exceedsMinBets(true) || !IChips3D.get().exceedsMinBets(true))
            {
                return;
            }
            
            super.handleSpinRequested();
        }

        /**
         * Gets the bet breakdown message to show.
         * return null to show no bet breakdown.
         */
        protected getBetBreakdown(): Localisation.LocalisableString | null
        {
            return "Place your bet";
        }

        protected getResultsModel(models: Models): RS.Models.GameResults
        {
            return models.gameResults;
        }
    }
}
