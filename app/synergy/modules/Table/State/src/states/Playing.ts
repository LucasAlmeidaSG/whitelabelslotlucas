namespace RS.Table.GameState
{
    /**
     * A play has been requested and a response received.
     * Any in-reel features (that occur during a spin) should be handled here.
     * Then it should spin down the reels.
     */
    @HasCallbacks
    @State.Name(RS.GameState.Playing)
    export class PlayingState<T extends Game.Context = Game.Context> extends RS.GameState.PlayingState<T>
    {
        @AutoDisposeOnSet protected _statusTextHandler: IDisposable;

        public onEnter()
        {
            super.onEnter();
            this.doAnimations();
        }

        protected async doAnimations()
        {
            //override for animations and next states

            this.transitionToNextState(RS.GameState.WinRendering);
        }

        protected getResultsModel(models: Models): RS.Models.GameResults
        {
            return models.gameResults;
        }
    }
}
