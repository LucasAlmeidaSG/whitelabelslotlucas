namespace RS.Table.GameState
{
    /**
     * The current spin has finished and we're ready to close the game and move back to ReadyToSpin.
     */
    @HasCallbacks
    @State.Name(RS.GameState.PlayFinished)
    export class PlayFinishedState<T extends Game.Context = Game.Context> extends RS.GameState.PlayFinishedState<T>
    {
        protected getResultsModel(models: Models): RS.Models.GameResults
        {
            return models.gameResults;
        }
    }
}
