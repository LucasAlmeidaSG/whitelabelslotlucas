namespace RS.Table.GameState
{
    @State.Name(RS.GameState.Loading)
    export class LoadingState<T extends Game.Context = Game.Context> extends RS.GameState.LoadingState<T>
    {
        protected createUIController()
        {
            this._context.game.uiController.create({
                models: this._context.game.models,
                locale: this._context.game.locale,
                currencyFormatter: this._context.game.currencyFormatter
            });
        }

        protected setStakes(): void
        {
            const models = this._context.game.models;
            const override = models.stake.override;
            if (override != null)
            {
                this._context.game.uiController.setTotalBet(override.totalBet);
                this._context.game.observables.totalBet.value = override.totalBet;
            }
            else
            {
                this._context.game.uiController.setTotalBet(0);
                this._context.game.observables.totalBet.value = 0;
            }
        }

        protected async doRestore(): Promise<boolean>
        {
            if (this._context.game.models.state.state !== Models.State.Type.Closed)
            {
                this.transitionToState(RS.GameState.Resuming);
                return true;
            }
            return false;
        }

        protected getResultsModel(models: Models): RS.Models.GameResults
        {
            return models.gameResults;
        }
    }
}
