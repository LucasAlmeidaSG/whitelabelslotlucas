namespace RS.Table.GameState
{
    /**
     * The game has been resumed and is waiting for user input to progress.
     */
    @State.Name(RS.GameState.Resuming)
    export class ResumingState<T extends Game.Context = Game.Context> extends RS.GameState.ResumingState<T>
    {
        protected getResultsModel(models: Models): RS.Models.GameResults
        {
            return models.gameResults;
        }
    }
}
