namespace RS.Table.GameState
{
    /**
     * It's time to render any wins.
     * It could be that there are no wins to render, in which case we move on immediately.
     */
    @HasCallbacks
    @State.Name(RS.GameState.WinRendering)
    export class WinRenderingState<T extends Game.Context = Game.Context> extends RS.GameState.WinRenderingState<T>
    {
        @AutoDisposeOnSet protected _statusTextHandler: IDisposable;
    }
}
