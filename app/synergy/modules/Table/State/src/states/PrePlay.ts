namespace RS.Table.GameState
{
    /**
     * A play has been requested but no response received yet.
     */
    @HasCallbacks
    @State.Name(RS.GameState.PrePlay)
    export class PrePlayState<T extends Game.Context = Game.Context> extends RS.GameState.PrePlayState<T>
    {
        public onEnter()
        {
            this._context.game.uiController.setChipSelectorVisible(false);

            super.onEnter();
        }

        protected updateUI(): void
        {
            // Update total bet observable with newly parsed total bet for edgecases where total bet may have been edited externally, eg. via Charles
            const models = this._context.game.models;
            if (models.stake.override != null)
            {
                this._context.game.uiController.setTotalBet(models.stake.override.totalBet);
                this._context.game.observables.totalBet.value = models.stake.override.totalBet;
            }
        }

        protected getResultsModel(models: Models): RS.Models.GameResults
        {
            return models.gameResults;
        }
    }
}
