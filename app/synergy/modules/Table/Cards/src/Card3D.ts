namespace RS.Table
{
    export class Card3D
    {
        protected _settings: Card3D.Settings;
        protected _stage: RS.Rendering.IStage;

        protected _visual: Rendering.Cards.Card3D;
        public get visual()
        {
            return this._visual;
        }

        constructor(settings: Card3D.Settings, stage: RS.Rendering.IStage)
        {
            this._settings = settings;
            this._stage = stage;

            this.createVisual();
        }

        /**
         * Start the reveal animation
         */
        public async reveal(suit: string, value: number, delay: number = 0, instant: boolean = false, direction: Rendering.Cards.Card3D.FlipDirection = Rendering.Cards.Card3D.FlipDirection.LeftToRight)
        {
            await this._visual.reveal(suit, value, delay, instant, direction);
        }

        /**
         * Start the hide animation
         */
        public async hide(delay: number = 0, instant: boolean = false, direction: Rendering.Cards.Card3D.FlipDirection = Rendering.Cards.Card3D.FlipDirection.LeftToRight)
        {
            await this._visual.hide(delay, instant, direction);
        }

        public async peek()
        {
            await this._visual.peek();
        }

        public reset()
        {
            this._visual.setInitialSettings();
        }

        /**
         * Creates the visual part of the card
         */
        protected createVisual()
        {
            this._visual = new Rendering.Cards.Card3D(this._settings.visualSettings, this._stage);
        }
    }

    export namespace Card3D
    {
        export interface Settings
        {
            visualSettings: Rendering.Cards.Card3D.Settings;
        }
    }
}
