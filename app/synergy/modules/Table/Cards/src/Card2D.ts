namespace RS.Table
{
    export class Card2D
    {
        protected _settings: Card2D.Settings;

        protected _visual: Rendering.Cards.Card2D;
        public get visual()
        {
            return this._visual;
        }

        constructor(settings: Card2D.Settings)
        {
            this._settings = settings;

            this.createVisual();
        }

        /**
         * Start the reveal animation
         * @param currentScreenDimensions This is either the screen dimensions OR the size of the projection containers filter area, if using one, and it's bigger.
         * @param worldPosOffset This should normally be x:0,y:0. This is used when using a projection container filter area that is larger than the screen dimensions.
         */
        public async reveal(type: string, index: number, currentScreenDimensions: Math.Size2D, delay: number = 0, instant: boolean = false, worldPosOffset: RS.Math.Vector2D = { x: 0, y: 0 })
        {
            await this._visual.reveal(type, index, currentScreenDimensions, delay, instant, worldPosOffset);
        }

        /**
         * Start the hide animation
         * @param currentScreenDimensions This is either the screen dimensions OR the size of the projection containers filter area, if using one, and it's bigger.
         * @param worldPosOffset This should normally be x:0,y:0. This is used when using a projection container filter area that is larger than the screen dimensions.
         */
        public async hide(currentScreenDimensions: Math.Size2D, delay: number = 0, instant: boolean = false, worldPosOffset: RS.Math.Vector2D = { x: 0, y: 0 })
        {
            await this._visual.hide(currentScreenDimensions, delay, instant, worldPosOffset)
        }

        public reset()
        {
            this._visual.setInitialSettings();
        }

        /**
         * Creates the visual part of the card
         */
        protected createVisual()
        {
            this._visual = new Rendering.Cards.Card2D(this._settings.visualSettings);
        }
    }

    export namespace Card2D
    {
        export interface Settings
        {
            visualSettings: Rendering.Cards.Card2D.Settings
        }
    }
}
