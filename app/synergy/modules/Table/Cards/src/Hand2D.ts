namespace RS.Table
{
    export class Hand2D
    {
        protected _visual: Rendering.Cards.Hand2D;
        public get visual() { return this._visual; }

        protected _cards: Card2D[] = [];
        public get cards() { return this._cards; }

        public set stacked(value: boolean) { this._visual.stacked = value; }
        public get stacked() { return this._visual.stacked; }

        constructor(protected readonly _settings: Hand2D.Settings, protected readonly _context: Game.Context, dealDiscardRelativeTo?: RS.Rendering.IDisplayObject)
        {
            this._visual = new Rendering.Cards.Hand2D(this._settings.visual, this._context, dealDiscardRelativeTo);
        }

        public async dealCard(suit?: string, value?: number): Promise<void>
        {
            const card = new Card2D(this._settings.card);
            this._cards.push(card);
            await this._visual.dealCard(card.visual, suit, value);
        }

        public async revealCard(currentScreenDimensions: RS.Math.Size2D, worldPosOffset: RS.Math.Vector2D, suit: string, value: number, cardIndex: number = this._cards.length - 1): Promise<void>
        {
            await this._cards[cardIndex].reveal(suit, value, currentScreenDimensions, 0, false, worldPosOffset);
        }

        public async hideCard(currentScreenDimensions: RS.Math.Size2D, worldPosOffset: RS.Math.Vector2D, cardIndex: number = this._cards.length - 1): Promise<void>
        {
            await this._cards[cardIndex].hide(currentScreenDimensions, 0, false, worldPosOffset);
        }

        public async separateCards(instant: boolean = false): Promise<void>
        {
            if (!this.stacked) { return; }
            this.stacked = false;

            await this._visual.changeStackType(instant ? 0 : this._settings.visual.separateTweenTime);
        }

        public async stackCards(instant: boolean = false): Promise<void>
        {
            if (this.stacked) { return; }
            this.stacked = true;

            await this._visual.changeStackType(instant ? 0 : this._settings.visual.stackTweenTime);
        }

        public async centerCards(tweenTime?: number, numCards?: number): Promise<void>
        {
            await this._visual.centerCards(tweenTime, numCards);
        }

        public async peek(): Promise<void>
        {
            await this._visual.peek();
        }

        public async clear(): Promise<void>
        {
            await this._visual.clear();
            this._cards = [];
        }
    }

    export namespace Hand2D
    {
        export interface Settings
        {
            card: Card2D.Settings,
            visual: Rendering.Cards.Hand2D.Settings
        }
    }
}
