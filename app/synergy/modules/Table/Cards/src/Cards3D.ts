namespace RS.Table
{
    export class Cards3D<TSettings extends Cards3D.Settings = Cards3D.Settings, TRuntimeData extends Cards3D.RuntimeData = Cards3D.RuntimeData>
    {
        protected readonly _settings: TSettings;
        protected readonly _runtimeData: TRuntimeData;

        protected _visual: Rendering.Cards.Cards3D;
        public get visual() { return this._visual; }

        protected _cards: Card3D[] = [];
        public get cards() { return this._cards; }

        public set stacked(value: boolean) { this._visual.stacked = value; }
        public get stacked() { return this._visual.stacked; }

        constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            this._settings = settings;
            this._runtimeData = runtimeData;

            this._visual = new Rendering.Cards.Cards3D(this._settings.visual, this._runtimeData.overlayContainer);
        }

        public async dealCard(card: Models.Card, instant?: boolean): Promise<void>;
        public async dealCard(suit?: Models.Card.Suit, value?: number, instant?: boolean): Promise<void>;
        public async dealCard(p1?: Models.Card | Models.Card.Suit, p2?: boolean | number, p3?: boolean): Promise<void>
        {
            //resolve params
            const suit = Is.object(p1) ? p1.suit : p1;
            const value = Is.object(p1) ? p1.value : Is.number(p2) ? p2 : null;
            const instant = Is.boolean(p2) ? p2 : p3;

            const card = new Card3D(this._settings.card, this._runtimeData.stage);
            this._cards.push(card);

            await this._visual.dealCard(card.visual, suit, value, instant);
        }

        public async revealCard(card: Models.Card, cardIndex?: number, instant?: boolean): Promise<void>;
        public async revealCard(suit: Models.Card.Suit, value: number, cardIndex?: number, instant?: boolean): Promise<void>;
        public async revealCard(p1?: Models.Card | Models.Card.Suit, p2?: number, p3?: boolean | number, p4?: boolean): Promise<void>
        {
            //resolve params
            const suit = Is.object(p1) ? p1.suit : p1;
            const value = Is.object(p1) ? p1.value : p2;
            const cardIndex = Is.number(p3) ? p3 : p2;
            const instant = Is.boolean(p3) ? p3 : p4;

            await this._cards[cardIndex].reveal(suit, value, 0, instant, this._settings.visual.revealFlipDirection);
        }

        public getCard(cardIndex: number): Card3D
        {
            const card = this._cards.splice(cardIndex, 1)[0];
            this._visual.removeCard(card.visual);
            return card;
        }

        public addCard(card: Card3D): void
        {
            this._cards.push(card);
            this._visual.addCard(card.visual);
        }

        public async hideCard(cardIndex: number = this._cards.length - 1): Promise<void>
        {
            await this._cards[cardIndex].hide(0, false, this._settings.visual.hideFlipDirection);
        }

        public async separateCards(instant: boolean = false): Promise<void>
        {
            if (!this.stacked) { return; }
            this.stacked = false;

            await this._visual.changeStackType(instant ? 0 : this._settings.visual.separateTweenTime);
        }

        public async stackCards(instant: boolean = false): Promise<void>
        {
            if (this.stacked) { return; }
            this.stacked = true;

            await this._visual.changeStackType(instant ? 0 : this._settings.visual.stackTweenTime);
        }

        public async centerCards(tweenTime?: number, numCards?: number): Promise<void>
        {
            await this._visual.centerCards(tweenTime, numCards);
        }

        public async peek(): Promise<void>
        {
            await this._visual.peek();
        }

        public async discardCards(keepWin: boolean = false, instant: boolean = false): Promise<void>
        {
            await this._visual.discardCards(keepWin, instant);
            this._cards = [];
        }

        public async showTally(show: boolean = true)
        {
            await this._visual.tally.showTally(show);
        }

        public async moveTally(active: boolean = true)
        {
            await this._visual.tally.moveTally(active);
        }

        public async hideResult()
        {
            await this._visual.winDisplay.hideResult();
        }

        public async showResult(win?: boolean, instant?: boolean)
        {
            await this._visual.winDisplay.showResult(win, instant);
        }
    }

    export namespace Cards3D
    {
        export interface Settings
        {
            card: Card3D.Settings;
            visual: Rendering.Cards.Cards3D.Settings;
        }

        export interface RuntimeData
        {
            stage: RS.Rendering.IStage;
            overlayContainer: Flow.Container;
        }
    }
}
