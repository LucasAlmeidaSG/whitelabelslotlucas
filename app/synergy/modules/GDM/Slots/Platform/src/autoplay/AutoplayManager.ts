namespace RS.GDM.Environment.Slots
{
    @RS.HasCallbacks
    export class AutoplayManager
    {
        public readonly settings: AutoplayManager.Settings;
        public readonly runtimeData: AutoplayManager.RuntimeData;

        /** Spins remaining observable. Can be mutated externally. */
        public readonly spinsRemaining: RS.IObservable<number>;

        protected __container: RS.Rendering.IContainer;
        public get container() { return this.__container; }
        public set container(container)
        {
            if (this.__container === container) { return; }
            if (this.__countPromise)
            {
                container.addChild(this.__control);
                this.__control.invalidateLayout();
            }
            this.__container = container;
        }

        @RS.AutoDisposeOnSet
        private __control: RS.Slots.Autoplay.Dialog;
        protected get _control() { return this.__control; }

        private __countPromise: PromiseLike<number> | null = null;

        // Tracking info
        private __singleWinLimit: number | null = null;
        private __totalLossLimit: number | null = null;
        private __netEarnings: number | null = 0;

        public constructor(runtimeData: AutoplayManager.RuntimeData);
        public constructor(settings: AutoplayManager.Settings, runtimeData: AutoplayManager.RuntimeData);
        public constructor(p1: AutoplayManager.Settings | AutoplayManager.RuntimeData, p2?: AutoplayManager.RuntimeData)
        {
            if (p2)
            {
                this.settings = { ...AutoplayManager.defaultSettings, ...p1 as AutoplayManager.Settings };
                this.runtimeData = p2;
            }
            else
            {
                this.settings = { ...AutoplayManager.defaultSettings };
                this.runtimeData = p1 as AutoplayManager.RuntimeData;
            }

            this.spinsRemaining = new RS.Observable(0);
            this.spinsRemaining.onChanged(this.onSpinCountChanged);
        }

        /** Prompts the user to select autoplay options. Return a promise for the number of autoplays to play. */
        public prompt(stake: number): PromiseLike<number>;
        public prompt(stake: number)
        {
            // We need to lazy instantiate the control because we won't have the assets when we're constructed
            // Also we want to lose state from the last time the prompt was open
            try
            {
                this.__control = this.createControl(stake);
            }
            catch (err)
            {
                throw new RS.NestedError(`Failed to create autoplay control`, err);
            }
            const control = this.__control;

            // Auto-play already in progress?
            if (this.spinsRemaining.value > 0)
            {
                logger.warn(`Autoplay already in progress`);
                return Promise.resolve(0);
            }

            // User already being prompted for auto-play?
            if (this.__countPromise)
            {
                logger.warn(`Autoplay prompt already in progress`);
                return this.__countPromise;
            }

            return this.__countPromise = new Promise<number>((resolve) =>
            {
                // Setup response handlers
                const submitHandle = control.onStartClicked(() =>
                {
                    const { spinCount } = control;
                    submitHandle.dispose();
                    cancelHandle.dispose();
                    this.__countPromise = null;
                    this.__control = null;
                    resolve(spinCount);
                });
                const cancelHandle = control.onCancelClicked(() =>
                {
                    submitHandle.dispose();
                    cancelHandle.dispose();
                    this.__countPromise = null;
                    this.__control = null;
                    resolve(0);
                });

                // Show control
                if (this.__container)
                {
                    this.__container.addChild(control);
                    control.invalidateLayout();
                }
                else
                {
                    logger.warn("No container for autoplay control");
                }
            });
        }

        /** To be called after each spin. Returns whether or not auto-play finished by this call. */
        public handleSpinFinished({ stake, win }: { stake: number, win: number }): boolean
        {
            // Auto-play already finished?
            if (this.spinsRemaining.value === 0) { return false; }

            // Update our earnings so far for this campaign.
            this.__netEarnings += win - stake;

            // Have we hit the total loss limit?
            if (this.__totalLossLimit != null && this.__netEarnings <= -this.__totalLossLimit)
            {
                this.spinsRemaining.value = 0;
                return true;
            }

            // Have we hit the single-win limit?
            if (this.__singleWinLimit != null && this.__singleWinLimit <= win)
            {
                this.spinsRemaining.value = 0;
                return true;
            }

            // Decrement spin counter
            return --this.spinsRemaining.value === 0;
        }

        protected createControl(stake: number)
        {
            const control = RS.Slots.Autoplay.dialog.create(this.settings.control,
            {
                totalBetObservable: new RS.Observable(stake),
                maxSpinCount: this.runtimeData.autoplayLimit,
                requireLossLimit: this.runtimeData.requireTotalLossLimit
            });
            control.onStartClicked(this.onSubmitRequested);
            return control;
        }

        @RS.Callback
        protected onSpinCountChanged(count: number)
        {
            if (count > 0)
            {
                if (this.__totalLossLimit == null && this.runtimeData.requireTotalLossLimit)
                {
                    throw new Error("Loss limit required");
                }
            }
            else
            {
                // Stop
                this.__singleWinLimit = null;
                this.__totalLossLimit = null;
                this.__netEarnings = null;
            }
        }

        @RS.Callback
        protected onSubmitRequested()
        {
            const totalLossLimit = this.__control.lossLimit * this.__control.runtimeData.totalBetObservable.value;
            const singleWinLimit = this.__control.winLimit * this.__control.runtimeData.totalBetObservable.value;
            const count = this.__control.spinCount;

            // Set up total loss limit
            if (totalLossLimit != null)
            {
                this.__totalLossLimit = totalLossLimit || null;
            }

            // Set up single win limit
            if (singleWinLimit != null)
            {
                this.__singleWinLimit = singleWinLimit || null;
            }

            // Reset net earnings
            this.__netEarnings = 0;

            // Start auto-play with count spins
            this.spinsRemaining.value = count;

            if (count > this.runtimeData.autoplayLimit)
            {
                logger.warn(`Autoplay limit exceeded`);
                this.spinsRemaining.value = this.runtimeData.autoplayLimit;
            }
        }
    }

    export namespace AutoplayManager
    {
        export interface Settings
        {
            control?: RS.Slots.Autoplay.Dialog.Settings;
        }

        export interface RuntimeData
        {
            requireTotalLossLimit: boolean;
            autoplayLimit: number;
        }

        export type OptionValue = number | "NEVER";
        export type Option = "count" | "totalLossLimit" | "singleWinLimit";

        export const defaultSettings: Settings =
        {
            control: RS.Slots.Autoplay.Dialog.defaultSettings
        };
    }
}