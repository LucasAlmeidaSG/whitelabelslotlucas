namespace RS.GDM.Environment
{
    import ISlotsPlatform = RS.Slots.IPlatform;
    import PlatformGameState = RS.Slots.PlatformGameState;

    @RS.HasCallbacks
    export class SlotsPlatform implements ISlotsPlatform
    {
        //#region IPlatform.properties

        /** @deprecated Unused (TODO: remove from IPlatform) */
        public readonly sessionInfo: IPlatform.SessionInfo = null;

        public get supportsAutoplay()
        {
            const maxAutoplays = this.platform.getAPIExt("SET_MAX_AUTOPLAYS").value;
            return maxAutoplays ? RS.Slots.PlatformAutoplaySupport.GameProvides : RS.Slots.PlatformAutoplaySupport.None;
        }

        public get onBalanceUpdatedExternally() { return this.platform.onBalanceUpdatedExternally; }

        public get autoplayController() { return this.__autoplayController; }
        public set autoplayController(value)
        {
            if (this.__autoplayController === value) { return; }
            if (value)
            {
                this.__autoplayControllerStartedHandler = value.onAutoplayStarted((num) =>
                {
                    if (!this.__autoplayManager)
                    {
                        logger.warn("No autoplay manager");
                        return;
                    }

                    this.__autoplayManager.spinsRemaining.value = num;
                });
                this.__autoplayControllerStoppedHandler = value.onAutoplayStopped(() =>
                {
                    if (!this.__autoplayManager)
                    {
                        logger.warn("No autoplay manager");
                        return;
                    }

                    this.__autoplayManager.spinsRemaining.value = 0;
                });
            }
            else
            {
                this.__autoplayControllerStartedHandler = null;
                this.__autoplayControllerStoppedHandler = null;
            }
            this.__autoplayController = value || null;
        }
        private __autoplayController: RS.Slots.Autoplay.IController | null = null;
        @RS.AutoDisposeOnSet
        private __autoplayControllerStartedHandler: RS.IDisposable | null = null;
        @RS.AutoDisposeOnSet
        private __autoplayControllerStoppedHandler: RS.IDisposable | null = null;

        //#endregion

        private __settings: Readonly<ISlotsPlatform.Settings>;
        protected get settings() { return this.__settings; }

        private __autoplayManager: Slots.AutoplayManager | null = null;
        @RS.AutoDisposeOnSet
        private __autoplayOptionsHandle: RS.IDisposable = null;
        @RS.AutoDisposeOnSet
        private __autoplayPauseHandler: RS.IDisposable = null;

        protected get platform(): Platform { return RS.IPlatform.get() as Platform; }
        protected get partnerAdapter(): OGS.GDM.IWrapper { return this.platform.adapter; }

        constructor()
        {
            this.scheduleAutoplayInitialisation();
        }

        //#region IPlatform.methods

        public init(settings: ISlotsPlatform.Settings)
        {
            this.__settings = settings;

            //#region Bindings

            // Bind moneys
            this.platform.bindValueToObservable("BALANCE", settings.balanceObservable);
            this.platform.bindValueToObservable("TOTAL_WIN", settings.winObservable);
            this.platform.bindValueToObservable("TOTAL_BET", settings.stakeObservable);

            // Bind game state
            this.platform.bindValueToObservable("ROUND", settings.gameStateObservable.map((state) =>
            {
                switch (state)
                {
                    case PlatformGameState.InPlay:
                    case PlatformGameState.InPlayPostWinRendering:
                    case PlatformGameState.Resuming:
                        return OGS.GDM.IGame.RoundState.Started;

                    case PlatformGameState.NotReady:
                    case PlatformGameState.Ready:
                        return OGS.GDM.IGame.RoundState.Stopped;
                }
            }));

            if (settings.paytableVisible)
            {
                this.platform.bindValueToObservable("PAYTABLE", settings.paytableVisible);
            }
            else
            {
                logger.warn(`paytableVisible arbiter not set`);
            }

            //#endregion

            //#region State Transitions

            const sm = new RS.ObservableStateMachine(settings.gameStateObservable);

            sm.setStateExitHandler(PlatformGameState.Uninitialised, () =>
            {
                this.platform.onGameInitialised();
            });

            sm.setStateExitHandler(PlatformGameState.InPlayPostWinRendering, () =>
            {
                if (this.__autoplayManager)
                {
                    this.__autoplayManager.handleSpinFinished({ stake: this.__settings.stakeObservable.value, win: this.__settings.winObservable.value });
                }
            });

            //#endregion
        }

        public async requestAutoplay()
        {
            if (!this.__autoplayController)
            {
                logger.warn(`No autoplay controller`);
                return;
            }

            if (!this.__autoplayManager)
            {
                logger.warn(`No autoplay manager`);
                return;
            }

            const handle = this.__settings.interactionEnabledArbiter.declare(false);
            try
            {
                const spinCount = await this.__autoplayManager.prompt(this.__settings.stakeObservable.value);
                if (spinCount === 0) { return; }
                this.__autoplayController.start(spinCount);
            }
            finally
            {
                handle.dispose();
            }
        }

        public requestAutoplayStop(): void
        {
            if (!this.__autoplayManager)
            {
                logger.warn("No autoplay manager");
                return;
            }

            this.__autoplayManager.spinsRemaining.value = 0;
        }

        /** @deprecated Unused (TODO: remove from IPlatform) */
        public networkResponseReceived(request: XMLHttpRequest): void
        {
            RS.Log.warn("This method is deprecated and should not be called");
        }

        //#endregion

        protected async scheduleAutoplayInitialisation()
        {
            const autoplayOptions = this.platform.getAPIExt("SET_AUTOPLAY_STOP_OPTIONS");
            const autoplayLimit = this.platform.getAPIExt("SET_MAX_AUTOPLAYS");
            if (autoplayOptions.value != null && autoplayLimit.value != null)
            {
                this.useAutoplayOptions(autoplayOptions.value, autoplayLimit.value);
            }
            else
            {
                // Wait for options?
                if (autoplayOptions.value == null)
                {
                    await autoplayOptions.onCalled;
                }
                // Wait for limit?
                if (autoplayLimit.value == null)
                {
                    await autoplayLimit.onCalled;
                }
                // Use
                this.useAutoplayOptions(autoplayOptions.value, autoplayLimit.value);
            }
        }

        @RS.Validate.Method({})
        protected useAutoplayOptions(
            @RS.ArgIs(RS.Validate.NotNull) stopOptions: OGS.GDM.IGame.AutoplayStopOptions,
            @RS.ArgIs(RS.Validate.NotNull) autoplayLimit: number)
        {
            if (this.__autoplayManager)
            {
                throw new Error(`Internal error: autoplay manager already exists`);
            }

            this.__autoplayOptionsHandle = null;

            switch (stopOptions)
            {
                case OGS.GDM.IGame.AutoplayStopOptions.None:
                    this.__autoplayManager = new Slots.AutoplayManager({ requireTotalLossLimit: false, autoplayLimit });
                    break;

                case OGS.GDM.IGame.AutoplayStopOptions.LimitLossAndSingleWin:
                    this.__autoplayManager = new Slots.AutoplayManager({ requireTotalLossLimit: true, autoplayLimit });
                    break;

                default:
                    throw new Error(`Invalid or unsupported autoplay stop options value ${RS.Dump.stringify(stopOptions)}`);
            }

            this.__autoplayManager.container = this.__settings.uiContainer;
            this.__autoplayManager.spinsRemaining.onChanged((value) =>
            {
                if (value === 0)
                {
                    this.__autoplayController.stop();
                }
            });

            const pauseExt = this.platform.getAPIExt("PAUSE_AUTOPLAY");
            this.__autoplayPauseHandler = pauseExt.onCalled((pause) =>
            {
                if (pause)
                {
                    this.__autoplayManager.spinsRemaining.value = 0;
                }
            });
        }
    }

    ISlotsPlatform.register(SlotsPlatform);
}
