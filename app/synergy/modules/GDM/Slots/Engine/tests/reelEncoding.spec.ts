namespace RS.GDM.Engine.Slots.ReelEncoding.Tests
{
    const { expect } = chai;

    describe("Encoding.ts", function ()
    {
        interface DataPair
        {
            decoded: number[];
            encoded: string;
        }

        const dataSet: DataPair[] =
        [
            {
                decoded: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                encoded: "plzwt;1fdzcl;255ysd"
            },
            {
                decoded: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
                encoded: "plzwt;1fdzcl;2cm3"
            },
            {
                decoded: [],
                encoded: ""
            }
        ];

        describe("encode", function ()
        {
            for (const pair of dataSet)
            {
                it(`should encode [${pair.decoded}] to "${pair.encoded}"`, function ()
                {
                    expect(encode(pair.decoded)).to.equal(pair.encoded);
                });
            }
        });

        describe("decode", function ()
        {
            for (const pair of dataSet)
            {
                it(`should decode "${pair.encoded}" to [${pair.decoded}]`, function ()
                {
                    expect(decode(pair.encoded)).to.deep.equal(pair.decoded);
                });
            }
        });
    });
}