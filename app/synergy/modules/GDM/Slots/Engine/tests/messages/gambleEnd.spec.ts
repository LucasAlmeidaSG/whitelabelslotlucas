namespace RS.GDM.Engine.Messages.Slots.Tests
{
    const { expect } = chai;

    describe("BetMessage.ts", function ()
    {
        let models: RS.Slots.Models;
        beforeEach(function ()
        {
            models = {} as any;
            RS.Slots.Models.clear(models);

            models.config.reelSets =
            [
                new RS.Slots.ReelSet(
                [
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                ])
            ];
        });

        afterEach(function ()
        {
            models = null;
        });

        describe("BetRequest", function ()
        {
            it("should correctly serialise a client GAMBLE_END message", function ()
            {
                const request = new Messages.BetRequest(models, { isRecovering: false });
                request.type = MessageTypes.Slots.Gamble.End;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "GAMBLE_END",
                    "REC": "0"
                });
            });
        });
    });

    describe("BaseMessage.ts", function ()
    {
        let models: RS.Slots.Models;
        beforeEach(function ()
        {
            models = {} as any;
            RS.Slots.Models.clear(models);

            models.config.reelSets =
            [
                new RS.Slots.ReelSet(
                [
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                ])
            ];
        });

        afterEach(function ()
        {
            models = null;
        });

        describe("Response", function ()
        {
            it("should correctly deserialise a server GAMBLE_END message", function ()
            {
                const responseString =
                [
                    "&MSGID=GAMBLE_END&B=103384&AB=103384&VER=2.6.3-2.4.17-4-4",
                    "&GA=1&SID=SESSION00000&"
                ].join("");
                const response = new Response(models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base game message testing */
                expect(response.type).to.equal(MessageTypes.Slots.Gamble.End);
                expect(response.accountBalance).to.equal(103384);
                expect(response.version).to.equal("2.6.3-2.4.17-4-4");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(true);
            });
        });
    });
}