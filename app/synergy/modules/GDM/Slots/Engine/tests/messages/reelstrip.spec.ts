namespace RS.GDM.Engine.Messages.Slots.Tests
{
    const { expect } = chai;

    describe("ReelStripMessage.ts", function ()
    {
        let models: RS.Slots.Models;
        beforeEach(function ()
        {
            models = {} as any;
            RS.Slots.Models.clear(models);

            models.config.reelSets =
            [
                new RS.Slots.ReelSet(
                [
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                ])
            ];
        });

        afterEach(function ()
        {
            models = null;
        });

        describe("ReelStripRequest", function ()
        {
            it("should correctly serialise a client REELSTRIP message", function ()
            {
                const request = new ReelStripRequest(models, { isRecovering: true });
                request.type = MessageTypes.Slots.ReelStrip;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "REELSTRIP",
                    "REC": "1"
                });
            });
        });

        describe("ReelStripResponse", function ()
        {
            it("should correctly deserialise a server REELSTRIP message", function ()
            {
                const responseString =
                [
                    "&MSGID=REELSTRIP&B=100475&AB=100475&VER=2.6.3-2.4.17-4-11&ENC=0",
                    "&RSIDS=1|0|3|2|",
                    "&RC=5",
                    "&RST_0=0>5;7;2;10;5;11;3;6;5;8;4;6;5;8;12;9;0;11;5;9;3;8;4;9;1;8;2;9;1;10|1>7;2;9;11;4;9;11;4;9;2;11;9;4;7;3;9;12;8;10;3;9;4;11;5;6;1;9;0;6;5|2>10;11;4;6;8;2;7;8;3;7;10;5;8;7;1;6;8;1;10;9;12;10;8;0;10;8;5;10;7;8|3>9;6;3;7;1;8;5;11;7;5;11;0;7;6;1;7;2;11;7;4;6;2;10;12;9;5;11;4;10;3|4>12;8;4;9;1;8;5;10;2;9;3;7;5;8;2;7;0;9;2;6;1;11;4;9;2;6;4;11;1;10;3;6;5;10;4;8;3;1;10;5;3;6;2;7|",
                    "&RST_1=4>12;8;10;9;7;8;11;10;6;9;8;7;11;8;9;7;0;9;8;6;7;11;4;9;7;6;10;11;8;10;3;6;11;10;4;8;9;1;10;5;11;6;2;7|",
                    "&RSC_1=0>6:8;22:10;|1>14:11;23:9;28:11;|2>6:11;13:11;17:3;|3>2:10;9:8;14:10;18:10;19:7;20:4;21:10;22:7;24:10;27:7;|",
                    "&RSC_3=1>4:1;5:2;|2>11:1;28:2;|3>22:11;|&RSC_2=0>5:8;24:5;|2>11:1;|3>0:11;|",
                    "&GA=0&SID=SESSION00000&"
                ].join("");
                const response = new ReelStripResponse({ defaultRowCount: 3 }, models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.Slots.ReelStrip);
                expect(response.accountBalance).to.equal(100475);
                expect(response.version).to.equal("2.6.3-2.4.17-4-11");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(false);

                /** Message specific testing */
                expect(response.reelEncoding).to.equal(false);
                expect(response.reelCount).to.equal(5);
                expect(response.reelSetIDs).to.deep.equal([1, 0, 3, 2]);
                expect(response.reelSets).to.deep.equal(
                [
                    [
                        [ 5, 7, 2, 10, 5, 11, 3, 6, 5, 8, 4, 6, 5, 8, 12, 9, 0, 11, 5, 9, 3, 8, 4, 9, 1, 8, 2, 9, 1, 10 ],
                        [ 7, 2, 9, 11, 4, 9, 11, 4, 9, 2, 11, 9, 4, 7, 3, 9, 12, 8, 10, 3, 9, 4, 11, 5, 6, 1, 9, 0, 6, 5 ],
                        [ 10, 11, 4, 6, 8, 2, 7, 8, 3, 7, 10, 5, 8, 7, 1, 6, 8, 1, 10, 9, 12, 10, 8, 0, 10, 8, 5, 10, 7, 8 ],
                        [ 9, 6, 3, 7, 1, 8, 5, 11, 7, 5, 11, 0, 7, 6, 1, 7, 2, 11, 7, 4, 6, 2, 10, 12, 9, 5, 11, 4, 10, 3 ],
                        [ 12, 8, 4, 9, 1, 8, 5, 10, 2, 9, 3, 7, 5, 8, 2, 7, 0, 9, 2, 6, 1, 11, 4, 9, 2, 6, 4, 11, 1, 10, 3, 6, 5, 10, 4, 8, 3, 1, 10, 5, 3, 6, 2, 7 ]
                    ],
                    [
                        ,,,,
                        [ 12, 8, 10, 9, 7, 8, 11, 10, 6, 9, 8, 7, 11, 8, 9, 7, 0, 9, 8, 6, 7, 11, 4, 9, 7, 6, 10, 11, 8, 10, 3, 6, 11, 10, 4, 8, 9, 1,10, 5, 11, 6, 2, 7 ]
                    ]
                ]);
                expect(response.reelSetChanges).to.deep.equal(
                [
                    ,
                    [
                        { "6": 8, "22": 10 },
                        { "14": 11, "23": 9, "28": 11 },
                        { "6": 11, "13": 11, "17": 3 },
                        { "2": 10, "9": 8, "14": 10, "18": 10, "19": 7, "20": 4, "21": 10, "22": 7, "24": 10, "27": 7 }
                    ],
                    [
                        { "5": 8, "24": 5 },
                        ,
                        { "11": 1 }, { "0": 11 }
                    ],
                    [
                        ,
                        { "4": 1, "5": 2 },
                        { "11": 1, "28": 2 },
                        { "22": 11 }
                    ]
                ]);
            });

            it("should correctly deserialise a server REELSTRIP message with encoded reels", function ()
            {
                const responseString =
                [
                    "&MSGID=REELSTRIP&B=100475&AB=100475&VER=2.6.3-2.4.17-4-11&ENC=1",
                    "&RSIDS=1|0|3|2|",
                    "&RC=5",
                    "&RST_0=0>qjk8d;14rjh9;1p7fad;19s765;6f40t;1e56it|1>lql31;bexcd;g3h3x;gl1sd;ustvx;pwqvh|2>14wuol;zgvmd;64k7p;1aikvp;1dzfj9;153hol|3>63ost;q49xh;5yqn1;l4gn1;1avd4l;gkca5|4>6f42t;19b70l;b9gkd;ublml;bexal;1e5jjp;lkaal;qjd51;13pr0|",
                    "&RST_1=4>10ey5h;19xpad;1a93d1;v9b99;10dwsd;1f8wmt;llc7x;qjebp;13pss|",
                    "&RSC_1=0>6:8;22:10;|1>14:11;23:9;28:11;|2>6:11;13:11;17:3;|3>2:10;9:8;14:10;18:10;19:7;20:4;21:10;22:7;24:10;27:7;|",
                    "&RSC_3=1>4:1;5:2;|2>11:1;28:2;|3>22:11;|&RSC_2=0>5:8;24:5;|2>11:1;|3>0:11;|",
                    "&GA=0&SID=SESSION00000&"
                ].join("");
                const response = new ReelStripResponse({ defaultRowCount: 3 }, models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.Slots.ReelStrip);
                expect(response.accountBalance).to.equal(100475);
                expect(response.version).to.equal("2.6.3-2.4.17-4-11");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(false);

                /** Message specific testing */
                expect(response.reelEncoding).to.equal(true);
                expect(response.reelCount).to.equal(5);
                expect(response.reelSetIDs).to.deep.equal([1, 0, 3, 2]);
                expect(response.reelSets).to.deep.equal(
                [
                    [
                        [ 5, 7, 2, 10, 5, 11, 3, 6, 5, 8, 4, 6, 5, 8, 12, 9, 0, 11, 5, 9, 3, 8, 4, 9, 1, 8, 2, 9, 1, 10 ],
                        [ 7, 2, 9, 11, 4, 9, 11, 4, 9, 2, 11, 9, 4, 7, 3, 9, 12, 8, 10, 3, 9, 4, 11, 5, 6, 1, 9, 0, 6, 5 ],
                        [ 10, 11, 4, 6, 8, 2, 7, 8, 3, 7, 10, 5, 8, 7, 1, 6, 8, 1, 10, 9, 12, 10, 8, 0, 10, 8, 5, 10, 7, 8 ],
                        [ 9, 6, 3, 7, 1, 8, 5, 11, 7, 5, 11, 0, 7, 6, 1, 7, 2, 11, 7, 4, 6, 2, 10, 12, 9, 5, 11, 4, 10, 3 ],
                        [ 12, 8, 4, 9, 1, 8, 5, 10, 2, 9, 3, 7, 5, 8, 2, 7, 0, 9, 2, 6, 1, 11, 4, 9, 2, 6, 4, 11, 1, 10, 3, 6, 5, 10, 4, 8, 3, 1, 10, 5, 3, 6, 2, 7 ]
                    ],
                    [
                        ,,,,
                        [ 12, 8, 10, 9, 7, 8, 11, 10, 6, 9, 8, 7, 11, 8, 9, 7, 0, 9, 8, 6, 7, 11, 4, 9, 7, 6, 10, 11, 8, 10, 3, 6, 11, 10, 4, 8, 9, 1,10, 5, 11, 6, 2, 7 ]
                    ]
                ]);
                expect(response.reelSetChanges).to.deep.equal(
                [
                    ,
                    [
                        { "6": 8, "22": 10 },
                        { "14": 11, "23": 9, "28": 11 },
                        { "6": 11, "13": 11, "17": 3 },
                        { "2": 10, "9": 8, "14": 10, "18": 10, "19": 7, "20": 4, "21": 10, "22": 7, "24": 10, "27": 7 }
                    ],
                    [
                        { "5": 8, "24": 5 },
                        ,
                        { "11": 1 }, { "0": 11 }
                    ],
                    [
                        ,
                        { "4": 1, "5": 2 },
                        { "11": 1, "28": 2 },
                        { "22": 11 }
                    ]
                ]);
            });
        });
    });
}