namespace RS.GDM.Engine.Messages.Slots.Tests
{
    const { expect } = chai;

    describe("BetMessage.ts", function ()
    {
        describe("BetLogic", function ()
        {
            let models: RS.Slots.Models;
            beforeEach(function ()
            {
                models = {} as any;
                RS.Slots.Models.clear(models);

                models.config.reelSets =
                [
                    new RS.Slots.ReelSet(
                    [
                        [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                        [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                        [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                        [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                        [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                    ])
                ];
            });

            afterEach(function ()
            {
                models = null;
            });

            function setRequiredProperties(response: BetResponse<RS.Slots.Models>, properties: BetResponse<RS.Slots.Models>)
            {
                for (const p in properties)
                {
                    response[p] = properties[p];
                }
            }

            it("should populate models using a response symbolsInView", function ()
            {
                const response = new BetResponse(
                {
                    symbolMapping:
                    {
                        "H1": 0,
                        "H2": 1,
                        "H3": 2,
                        "H4": 3,
                        "GH5": 4,
                        "Q": 5,
                        "A": 6,
                        "K": 7
                    }
                }, models);

                // setRequiredProperties(response,
                // {
                //     stopIndices: [71, 98, 54, 45, 13, 0],
                //     reelSetIndex: 0,
                //     nextReelSetIndex: 0,

                //     isFreeGames: false,
                //     freeGamesTriggered: 0,
                //     freeGamesRemaining: 0,
                // })

                response.stopIndices = [71, 98, 54, 45, 13, 0];
                response.reelSetIndex = 0;

                response.symbolsInView = ["H4", "GH5", "Q", "A", "K", "Q", "H2", "H3", "K", "GH5", "GH5", "K", "H1", "H4", "A"];
                response.populateModels(models);

                const reelSet = models.spinResults[0].reels.symbolsInView.toNative();
                expect(reelSet).to.deep.equal(
                [
                    [3, 5, 4],
                    [4, 1, 7],
                    [5, 2, 0],
                    [6, 7, 3],
                    [7, 4, 6]
                ]);
            });

            it("should throw if the response symbolsInView do not match the reelset reel count", function ()
            {
                const response = new BetResponse(
                {
                    symbolMapping:
                    {
                        "H1": 0,
                        "H2": 1,
                        "H3": 2,
                        "H4": 3,
                        "GH5": 4,
                        "Q": 5,
                        "A": 6,
                        "K": 7
                    }
                }, models);

                // setRequiredProperties(response,
                // {
                //     stopIndices: [71, 98, 54, 45, 13, 0],
                //     reelSetIndex: 0,
                //     nextReelSetIndex: 0,

                //     isFreeGames: false,
                //     freeGamesTriggered: 0,
                //     freeGamesRemaining: 0,
                // })

                response.stopIndices = [71, 98, 54, 45, 13, 0];
                response.reelSetIndex = 0;

                response.symbolsInView = ["H4", "GH5", "Q", "A", "K", "Q", "H2", "H3", "K", "GH5", "GH5", "K", "H1"];
                expect(() => response.populateModels(models)).to.throw();
            });
        });
    });
}