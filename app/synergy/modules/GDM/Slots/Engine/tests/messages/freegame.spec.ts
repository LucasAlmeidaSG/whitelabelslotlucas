namespace RS.GDM.Engine.Messages.Slots.Tests
{
    const { expect } = chai;

    describe("BetMessage.ts", function ()
    {
        let models: RS.Slots.Models;
        beforeEach(function ()
        {
            models = {} as any;
            RS.Slots.Models.clear(models);

            models.config.reelSets =
            [
                new RS.Slots.ReelSet(
                [
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                ])
            ];
        });

        afterEach(function ()
        {
            models = null;
        });

        describe("BetRequest", function ()
        {
            it("should correctly serialise a client FREE_GAME message", function ()
            {
                const request = new BetRequest(models, { isRecovering: true, betAmount: { type: RS.Slots.Models.Stake.Type.PerLine, value: 1 }, paylineCount: 25 });
                request.type = MessageTypes.Slots.FreeGame;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "FREE_GAME",
                    "REC": "1",
                    "BPL": "1",
                    "LB": "25"
                });
            });
        });

        describe("BetResponse", function ()
        {
            it("should correctly deserialise a server FREE_GAME message", function ()
            {
                const responseString =
                [
                    "&MSGID=FREE_GAME&B=103884&AB=103884&VER=2.6.3-2.4.17-4-4",
                    "&RID=1&NRID=1&BPL=1&LB=25&RS=12|23|11|10|32|&TW=500&WC=3|2|1|",
                    "&CW=0&NFG=4&TFG=5&CFGG=1&FGTW=0&IFG=1&FID=4|&MUL=10&SUB=0",
                    "&GSD=FT~0&GA=0&SID=SESSION00000&"
                ].join("");
                const response = new Messages.Slots.BetResponse({}, models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.Slots.FreeGame);
                expect(response.accountBalance).to.equal(103884);
                expect(response.version).to.equal("2.6.3-2.4.17-4-4");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(false);

                /** Common game message testing */
                expect(response.totalWin).to.equal(500);
                expect(response.isFreeGames).to.equal(true);
                expect(response.gameSpecificData).to.be.an.instanceOf(Messages.GameSpecifcData);

                /** Common free game message testing */
                expect(response.freeGamesRemaining).to.equal(4);
                expect(response.totalFreeGames).to.equal(5);
                expect(response.freeGamesPlayed).to.equal(1);
                expect(response.freeGamesTotalWin).to.equal(0);

                /** Message specific testing */
                expect(response.currentWin).to.equal(0);
                expect(response.winMultiplier).to.equal(10);
                expect(response.substituteWin).to.equal(false);

                expect(response.reelSetIndex).to.equal(1);
                expect(response.nextReelSetIndex).to.equal(1);
                expect(response.betPerLine).to.equal(1);
                expect(response.linesBet).to.equal(25);

                expect(response.stopIndices).to.deep.equal([ 12, 23, 11, 10, 32]);

                expect(response.winCounts).to.be.an.instanceOf(Messages.Slots.WinCounts);
                expect(response.winCounts.lineCount).to.equal(3);
                expect(response.winCounts.scatterCount).to.equal(2);
                expect(response.winCounts.triggerCount).to.equal(1);

                expect(response.featureIDs).to.deep.equal([4]);
            });
        });
    });
}