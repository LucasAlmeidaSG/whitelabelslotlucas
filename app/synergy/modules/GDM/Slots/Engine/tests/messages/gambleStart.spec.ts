namespace RS.GDM.Engine.Messages.Slots.Gamble.Tests
{
    const { expect } = chai;

    describe("BetMessage.ts", function ()
    {
        let models: RS.Slots.Models;
        beforeEach(function ()
        {
            models = {} as any;
            RS.Slots.Models.clear(models);

            models.config.reelSets =
            [
                new RS.Slots.ReelSet(
                [
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                ])
            ];
        });

        afterEach(function ()
        {
            models = null;
        });

        describe("BetRequest", function ()
        {
            it("should correctly serialise a client GAMBLE_START message", function ()
            {
                const request = new Messages.BetRequest(models, { isRecovering: false });
                request.type = MessageTypes.Slots.Gamble.Start;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "GAMBLE_START",
                    "REC": "0"
                });
            });
        });
    });

    describe("GambleMessage.ts", function ()
    {
        let models: RS.Slots.Models;
        beforeEach(function ()
        {
            models = {} as any;
            RS.Slots.Models.clear(models);

            models.config.reelSets =
            [
                new RS.Slots.ReelSet(
                [
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0],
                    [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0]
                ])
            ];
        });

        afterEach(function ()
        {
            models = null;
        });

        describe("BetResponse", function ()
        {
            it("should correctly deserialise a server GAMBLE_START message", function ()
            {
                const responseString =
                [
                    "&MSGID=GAMBLE_START&B=103384&AB=103384&VER=2.6.3-2.4.17-4-4&TW=50",
                    "&GAM=50&GC=0&GH=-1;-1;-1|-1;-1;-1|-1;-1;-1|-1;-1;-1|-1;-1;-1",
                    "&GPW=100|200|&GA=1&SID=SESSION00000&GSD=FT~0&"
                ].join("");
                const response = new BetResponse({}, models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.Slots.Gamble.Start);
                expect(response.accountBalance).to.equal(103384);
                expect(response.version).to.equal("2.6.3-2.4.17-4-4");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(true);

                /** Common game message testing */
                expect(response.totalWin).to.equal(50);
                expect(response.gameSpecificData).to.be.an.instanceOf(Messages.GameSpecifcData);

                /** Message specific testing */
                expect(response.gambleAmount).to.equal(50);

                expect(response.cardHistory).to.be.an("array");
                expect(response.cardHistory.length).to.equal(5);
                for (const entry of response.cardHistory)
                {
                    expect(entry).to.be.an.instanceOf(CardHistoryEntry);
                    expect(entry.card).to.equal(-1);
                    expect(entry.color).to.equal(-1);
                    expect(entry.suit).to.equal(-1);
                }

                expect(response.cardPossibleWins).to.be.an.instanceOf(CardPossibleWins);
                expect(response.cardPossibleWins.colorWin).to.equal(100);
                expect(response.cardPossibleWins.suitWin).to.equal(200);
            });
        });
    });
}