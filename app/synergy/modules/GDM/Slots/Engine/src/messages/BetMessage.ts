namespace RS.GDM.Engine.MessageTypes.Slots
{
    export const FreeGame = "FREE_GAME";
}

namespace RS.GDM.Engine.Messages.Slots
{
    /**
     * A client bet message. Used for normal spin of a game.
     * E.g.
     * &MSGID=BET
     * &BPL=1
     * &LB=25
     * &GN=dummygame
     * &PID=TestPlayer
     * &REC=1&
     */
    export class BetRequest<TModels extends RS.Slots.Models, TPayload extends SlotsEngine.SpinMessagePayload> extends Messages.BetRequest<TModels, TPayload>
    {
        /** Bet per line (BPL). */
        @Serialisation.Property({ key: "BPL", type: Serialisation.Integer })
        public betPerLine?: number;

        /** Number of lines (LB) to play. */
        @Serialisation.Property({ key: "LB", type: Serialisation.Integer })
        public linesBet?: number;

        /** Current bet multiplier (BPR). */
        @Serialisation.Property({ key: "BPR", type: Serialisation.Integer })
        public betMultiplier?: number;

        /** Reel bet mode (RB) to play. */
        @Serialisation.Property({ key: "RB", type: Serialisation.Integer })
        public reelBetMode?: number;

        constructor(models: TModels, payload: TPayload)
        {
            super(models, payload);

            if (payload.paylineCount)
            {
                this.linesBet = payload.paylineCount;
                this.betPerLine = payload.betAmount.type === RS.Slots.Models.Stake.Type.PerLine
                    ? payload.betAmount.value
                    : RS.Slots.Models.Stake.toTotal(models.stake, payload.betAmount).value / payload.paylineCount;
            }
        }
    }

    /** Number of pay win lines, scatter wins and triggers for current game
     *
     * WC = w|ws|t
     *
     * w = number of winning lines
     * ws = number of winning scatter symbols
     * t = number of triggers
     */
    export class WinCounts
    {
        /** Number of winning lines (#0). */
        @Serialisation.Property({ key: "0", type: Serialisation.Number, required: true })
        public lineCount: number;

        /** Number of winning scatter symbols (#1). */
        @Serialisation.Property({ key: "1", type: Serialisation.Number, required: true })
        public scatterCount: number;

        /** Number of triggers (#2). */
        @Serialisation.Property({ key: "2", type: Serialisation.Number, required: true })
        public triggerCount: number;
    }

    /**
     * A server bet message
     * E.g.
     * &MSGID=BET
     * &B=99981
     * &VER=2.6.3-2.4.17-4-11
     * &RID=0
     * &NRID=0
     * &BPL=1
     * &LB=25
     * &RS=5|22|5|2|21|
     * &TW=6
     * &WC=3|0|0|
     * &WS=0;2;1;1;-1;-1;-1;|15;2;1;1;-1;-1;-1;|16;2;1;1;-1;-1;-1;|
     * &IFG=0
     * &MUL=1
     * &SUB=0
     * &GSD=FT~0
     * &GA=1
     * &SID=SESSION00000
     */
    export class BetResponse<TModels extends RS.Slots.Models> extends Messages.BetResponse<TModels>
    {
        //#region Stake

        /** Bet per line (BPL). */
        @Serialisation.Property({ key: "BPL", type: Serialisation.Integer })
        public betPerLine?: number;

        /** Number of lines (LB) in play. */
        @Serialisation.Property({ key: "LB", type: Serialisation.Integer })
        public linesBet?: number;

        /** Current bet multiplier (BPR). */
        @Serialisation.Property({ key: "BPR", type: Serialisation.Number })
        public betMultiplier?: number;

        /** Reel bet mode (RB) in play. */
        @Serialisation.Property({ key: "RB", type: Serialisation.Number })
        public reelBetMode?: number;

        //#endregion

        //#region Reels

        /** Current reel-set index (RID). */
        @Serialisation.Property({ key: "RID", type: Serialisation.Integer })
        public reelSetIndex?: number;

        /** Next spin reel-set index (NRID). */
        @Serialisation.Property({ key: "NRID", type: Serialisation.Integer })
        public nextReelSetIndex?: number;

        /** Current reel stop indices (RS). */
        @Serialisation.Property({ key: "RS", type: Serialisation.ArrayOf(Serialisation.Integer) })
        public stopIndices?: number[];

        /**
         * Current symbols in view as an array of symbol names (VA). \
         * You'll need to map these to numeric IDs. \
         * Not yet in the GDM server core, so check your game supports this before using it.
         */
        @Serialisation.Property({ key: "VA", type: Serialisation.ArrayOf(Serialisation.String) })
        public symbolsInView?: string[];

        //#endregion

        //#region Free-spins

        /** Whether the player is in free games or not (IFG). */
        @Serialisation.Property({ key: "IFG", type: Serialisation.Boolean, defaultValue: false })
        public isFreeGames: boolean;

        /** Current number of remaining free games (NFG). */
        @Serialisation.Property({ key: "NFG", type: Serialisation.Integer, defaultValue: 0 })
        public freeGamesRemaining: number;

        /** Number of free games that have been triggered (FGT). */
        @Serialisation.Property({ key: "FGT", type: Serialisation.Integer, defaultValue: 0 })
        public freeGamesTriggered: number;

        /** Number of free games that have been triggered, plus all free games won from retriggers (TFG). */
        @Serialisation.Property({ key: "TFG", type: Serialisation.Integer, defaultValue: 0 })
        public totalFreeGames: number;

        /** Number of free games played (CFGG). */
        @Serialisation.Property({ key: "CFGG", type: Serialisation.Integer, defaultValue: 0 })
        public freeGamesPlayed: number;

        /** Total win for free games (excluding triggering spin) (FGTW). */
        @Serialisation.Property({ key: "FGTW", type: Serialisation.Integer, defaultValue: 0 })
        public freeGamesTotalWin: number;

        //#endregion

        //#region Wins

        /**
         * Number of paying win-lines, scatter wins and triggers for current game (WC).
         *
         * WC = w|ws|t
         *
         * w = number of winning lines
         * ws = number of winning scatter symbols
         * t = number of triggers
         */
        @Serialisation.Property({ key: "WC", type: Serialisation.Tuple(WinCounts) })
        public winCounts?: WinCounts;

        /**
         * All winning symbols/win combinations for the current game (WS).
         *
         * WS = wt;wa;r1;r2;;r3;r4;r5
         *
         * wt = win type: Serialisation.0 -> N for win lines, -1 for scatters, -2 for feature triggers
         * wa = win amount
         * rn = indicates which row symbol appears on for reel
         */
        @Serialisation.Property({ key: "WS", type: Serialisation.ArrayOf(
            // inner array
            Serialisation.ArrayOf(Serialisation.Number, { delimiter: ";"}),
            // outer array
            { delimiter: "|" })
        })
        public winLines?: number[][];

        /** Multiplier for all wins in the current game (MUL). */
        @Serialisation.Property({ key: "MUL", type: Serialisation.Number })
        public winMultiplier?: number;

        /** Indicates whether current game has a substitute win or not (SUB). */
        @Serialisation.Property({ key: "SUB", type: Serialisation.Boolean })
        public substituteWin?: boolean;

        //#endregion

        constructor(public readonly settings: BetResponse.Settings, public readonly models: TModels)
        {
            super(models);
        }

        public populateModels(models: TModels)
        {
            // Reset spin results
            RS.Slots.Models.SpinResults.clear(models.spinResults);
            RS.Slots.Models.FreeSpins.clear(models.freeSpins);

            models.state.isMaxWin = this.isMaxWin;

            // We're now open
            models.state.state = RS.Slots.Models.State.Type.Open;

            this.populateFreeSpinsModel(models.freeSpins);
            this.updateStakeModel(models.stake);

            // Separate wins by type.
            let spinWinAmount = 0;
            let scatterWinAmount = 0;
            const featureWins: number[][] = [];
            const paylineWins: number[][] = [];
            const scatterWins: number[][] = [];

            if (this.winLines)
            {
                for (const line of this.winLines)
                {
                    const [type, win] = line;
                    if (type === -1)
                    {
                        scatterWinAmount += win;
                        scatterWins.push(line);
                    }
                    else if (type >= 0)
                    {
                        spinWinAmount += win;
                        paylineWins.push(line);
                    }
                    else
                    {
                        featureWins.push(line);
                    }
                }
            }

            if (this.stopIndices != null && this.reelSetIndex != null)
            {
                // Get total win amounts
                const currentWin = this.currentWin;
                const accumulatedWin = this.accumulatedWin;
                logger.debug(`Spin win: ${currentWin} / ${accumulatedWin}`);

                // Get balance chronology
                const balanceBefore = this.balanceBefore;
                const balanceAfter = this.balanceAfter;
                logger.debug(`Spin balance: ${balanceBefore} -> ${balanceAfter}`);

                // Translate stop indices
                const stopIndices = this.toSlotStops(this.stopIndices);
                logger.debug(`Spin stops: ${this.stopIndices} => ${stopIndices}`);

                // Get reel-set
                const reelSetIndex = this.toSlotReelSetIndex(this.reelSetIndex);
                if (!models.config.reelSets) { throw new Error(`No reel-sets defined`); }
                const reelSet = models.config.reelSets[reelSetIndex];
                if (!reelSet) { throw new Error(`No reel-set for index ${this.reelSetIndex} (mapped index ${reelSetIndex})`); }

                // Get symbols in view
                const symbolsInView = this.getSymbolsInView(stopIndices, reelSet);

                /** Spin Results Model */
                const spinResult: RS.Slots.Models.SpinResult =
                {
                    index: 0,
                    reels:
                    {
                        stopIndices: stopIndices,
                        currentReelSetIndex: reelSetIndex,
                        symbolsInView: symbolsInView
                    },
                    win:
                    {
                        totalWin: currentWin,
                        accumulatedWin: accumulatedWin,
                        winAmounts: { "total": spinWinAmount, "scatter": scatterWinAmount },
                        paylineWins: paylineWins.map<RS.Slots.Models.PaylineWin>(([lineType, amountWon, ...winRows], index) =>
                        {
                            const positions = this.rowsToReelSetPositions(winRows);
                            return {
                                lineIndex: lineType,
                                reverse: this.isPaylineReversed(index),
                                multiplier: this.getLineStakeMultiplier(amountWon),
                                totalWin: amountWon,
                                positions: positions,
                                symbolID: this.getLineSymbolID(positions, symbolsInView)
                            };
                        }),
                        scatterWins: scatterWins.map<RS.Slots.Models.ScatterWin>(([lineType, amountWon, ...winRows]) =>
                        ({
                            totalWin: amountWon,
                            positions: this.rowsToReelSetPositions(winRows)
                        }))
                    },
                    balanceBefore: RS.Models.Balances(balanceBefore),
                    balanceAfter: RS.Models.Balances(balanceAfter)
                };
                this.setWinAmounts(spinResult.win.winAmounts);
                models.spinResults.push(spinResult);
            }

            // Feature-line hook
            for (const [lineType, amountWon, ...winRows] of featureWins)
            {
                this.populateWithFeatureLine(models, lineType, amountWon, this.rowsToReelSetPositions(winRows));
            }
        }

        protected hasPendingFeatureRequests(): boolean
        {
            return this.freeGamesRemaining > 0 || super.hasPendingFeatureRequests();
        }

        /** Returns a view-capture reel-set. */
        protected getSymbolsInView(stopIndices: ReadonlyArray<number>, reelSet: RS.Slots.ReelSet): RS.Slots.ReelSet
        {
            if (this.settings.symbolMapping && this.symbolsInView)
            {
                const engineView = this.readResponseSymbolView(this.symbolsInView, reelSet.reelCount);
                const slotView = this.toSlotReelSet(engineView);
                return new RS.Slots.ReelSet(slotView);
            }
            else
            {
                const rowCount = this.getRowCount();
                return reelSet.extractSubset(stopIndices, rowCount);
            }
        }

        /** Reads a symbol view array. */
        protected readResponseSymbolView(view: string[], reelCount: number)
        {
            const data: number[][] = [];
            if (view.length % reelCount)
            {
                throw new Error(`symbolsInView length ${view.length} is invalid for reelCount ${reelCount}`);
            }

            for (let i = 0; i < view.length; i++)
            {
                const row = Math.floor(i / reelCount);
                const reel = i % reelCount;

                const name = view[i];
                if (!name) { throw new Error(`Missing symbol at ${reel}, ${row}`); }

                const id = this.settings.symbolMapping[name];
                if (!RS.Is.integer(id)) { throw new Error(`Missing symbol ID for symbol '${name}'`); }

                if (!data[reel]) { data[reel] = []; }
                data[reel][row] = id;
            }
            return data;
        }

        /** Maps an external engine reel-set to an internal reel-set. */
        protected toSlotReelSet(reelSet: ReadonlyArray<ReadonlyArray<number>>): number[][]
        {
            return reelSet.map((reel) =>
                        reel.map((row) =>
                            this.toSlotSymbolID(row)));
        }

        /** Maps an external engine symbol ID to an internal slot symbol ID. */
        protected toSlotSymbolID(engineSymbolID: number): number
        {
            return engineSymbolID;
        }

        /** Populates the models using the given feature trigger line. */
        protected populateWithFeatureLine(models: TModels, type: number, winAmount: number, positions: ReadonlyArray<Readonly<RS.Slots.ReelSetPosition>>): void
        {
            return;
        }

        /** Sets additional win amounts on the given map. */
        protected setWinAmounts(amounts: RS.Map<number>)
        {
            return;
        }

        /** Returns whether or not the given payline is reversed. */
        protected isPaylineReversed(paylineIndex: number): boolean
        {
            return false;
        }

        /** Returns the stake multiplier for a given win amount. */
        protected getLineStakeMultiplier(winAmount: number): number
        {
            const base = RS.Slots.Models.Stake.getCurrentBetAmount(this.models.stake, RS.Slots.Models.Stake.Type.PerLine);
            return winAmount / base;
        }

        /** Returns the symbol ID for the given win line. */
        protected getLineSymbolID(positions: ReadonlyArray<Readonly<RS.Slots.ReelSetPosition>>, symbolsInView: RS.Slots.ReelSet<number>): number
        {
            return symbolsInView.get(positions[0].reelIndex, positions[0].rowIndex);
        }

        /** Sets the stake model using the given bet response. */
        protected updateStakeModel(model: RS.Slots.Models.Stake): void
        {
            if (this.linesBet != null)
            {
                const paylineIndex = model.paylineOptions.indexOf(this.linesBet);
                if (paylineIndex === -1)
                {
                    logger.warn(`Could not find payline option '${this.linesBet}' in stake model`);
                }
                else
                {
                    model.currentPaylineIndex = paylineIndex;
                }
            }

            if (this.betPerLine != null)
            {
                RS.Slots.Models.Stake.setCurrentBetAmount(model, RS.Slots.Models.Stake.Type.PerLine, this.betPerLine);
            }

            if (this.betMultiplier != null)
            {
                RS.Slots.Models.Stake.setCurrentBetAmount(model, RS.Slots.Models.Stake.Type.CreditMultiplier, this.betMultiplier);
            }
        }

        /** Converts a reel-indexed array of rows to an array of reel-set positions. */
        protected rowsToReelSetPositions(rows: ReadonlyArray<number>): RS.Slots.ReelSetPosition[]
        {
            return rows.map((rowIndex, reelIndex) => rowIndex === -1 ? null : { reelIndex, rowIndex })
                       // We need to filter after because otherwise reelIndex may be wrong
                       .filter((pos) => pos != null);
        }

        /** Populates the FreeSpins model using a given response. */
        protected populateFreeSpinsModel(model: RS.Slots.Models.FreeSpins)
        {
            model.isFreeSpins = this.isFreeGames;
            model.used = this.freeGamesPlayed;
            model.remaining = this.freeGamesRemaining;
            model.winAmount = this.freeGamesTotalWin;
            model.extraSpinsAwarded = this.freeGamesTriggered;
        }

        /** Translates the given GDM stop index array into an internal stop index array. */
        protected toSlotStops(stopIndices: ReadonlyArray<number>): number[]
        {
            return [...stopIndices];
        }

        /** Maps a GDM reel-set index to an internal config model reel-set index. */
        protected toSlotReelSetIndex(responseReelSetIndex: number): number
        {
            return responseReelSetIndex;
        }

        /** Returns current row count to use. This may be fixed, change as a result of a response, or be based on model state. */
        protected getRowCount(): number
        {
            return this.settings.defaultRowCount == null ? 3 : this.settings.defaultRowCount;
        }
    }

    export namespace BetResponse
    {
        export interface Settings
        {
            /** Default row count. Defaults to 3. */
            defaultRowCount?: number;
            /** Mapping of symbol names (e.g. H4, H2) to numeric IDs. Required to enable handling of the serverside symbolsInView (VA). */
            symbolMapping?: RS.Map<number>;
        }
    }

    export class BetSequence<TModels extends RS.Slots.Models = RS.Slots.Models, TPayload extends SlotsEngine.SpinMessagePayload = SlotsEngine.SpinMessagePayload> extends Messages.BetSequence<TModels, TPayload>
    {
        public readonly settings: BetSequence.Settings<TModels, TPayload>;

        constructor(settings: BetSequence.Settings<TModels, TPayload>)
        {
            super(settings);
        }

        protected inferMessageType(models: TModels, payload: TPayload): string
        {
            if (models.freeSpins.remaining > 0 && !models.state.isMaxWin)
            {
                return MessageTypes.Slots.FreeGame;
            }

            return super.inferMessageType(models, payload);
        }

        protected createRequest(models: TModels, payload: TPayload)
        {
            const ctor = this.settings.request || BetRequest;
            return new ctor(models, payload);
        }
    }

    export namespace BetSequence
    {
        export interface Settings<TModels extends RS.Slots.Models, TPayload extends SlotsEngine.SpinMessagePayload> extends Messages.BetSequence.Settings<TModels>
        {
            request?: RS.Constructor2A<BetRequest<TModels, TPayload>, TModels, TPayload>;
        }
    }
}