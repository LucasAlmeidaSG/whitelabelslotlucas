/// <reference path="../helpers/Encoding.ts"/>
namespace RS.GDM.Engine.MessageTypes.Slots
{
    /** ReelStrip messageType */
    export const ReelStrip = "REELSTRIP";
}

const createArray = Array;

namespace RS.GDM.Engine.Messages.Slots
{
    /**
     * A client reelstrip message. Can only be sent after INIT message.
     * E.g.
     * &MSGID=REELSTRIP
     * &GN=dummygame
     * &PID=TestPlayer
     * &REC=1&
     */
    export class ReelStripRequest<TModels extends RS.Slots.Models, TPayload extends CommonMessagePayload> extends Request<TModels, TPayload>
    {
        // Nothing special about this
    }

    /**
     * A server reelstrip message.
     * E.g.
     * &MSGID=REELSTRIP
     * &B=100475
     * &VER=2.6.3-2.4.17-4-11
     * &ENC=0
     * &RSIDS=1|0|3|2|
     * &RC=5
     * &RST_0=
            0>5;7;2;10;5;11;3;6;5;8;4;6;5;8;12;9;0;11;5;9;3;8;4;9;1;8;2;9;1;10|
            1>7;2;9;11;4;9;11;4;9;2;11;9;4;7;3;9;12;8;10;3;9;4;11;5;6;1;9;0;6;5|
            2>10;11;4;6;8;2;7;8;3;7;10;5;8;7;1;6;8;1;10;9;12;10;8;0;10;8;5;10;7;8|
            3>9;6;3;7;1;8;5;11;7;5;11;0;7;6;1;7;2;11;7;4;6;2;10;12;9;5;11;4;10;3|
            4>12;8;4;9;1;8;5;10;2;9;3;7;5;8;2;7;0;9;2;6;1;11;4;9;2;6;4;11;1;10;3;6;5;10;4;8;3;1;10;5;3;6;2;7|
        &RST_1=4>12;8;10;9;7;8;11;10;6;9;8;7;11;8;9;7;0;9;8;6;7;11;4;9;7;6;10;11;8;10;3;6;11
            ;10;4;8;9;1;10;5;11;6;2;7|
     *  &RSC_1=0>6:8;22:10;|1>14:11;23:9;28:11;|2>6:
            11;13:11;17:3;|3>2:10;9:8;14:10;18:10;19:7;20:4;21:10;22:7;24:10;27:7;|
        &RSC_3=1>4:1;5:2;|2>11:1;28:2;|3>22:11;|
        &RSC_2=0>5:8;24:5;|2>11:1;|3>0:11;|
     * &GA=0
     * &SID=SESSION00000
     */
    export class ReelStripResponse<TModels extends RS.Slots.Models> extends Response<TModels>
    {
        /** Indicates whether the reel strip encoding is on or off (ENC). */
        @Serialisation.Property({ key: "ENC", type: Serialisation.Boolean, defaultValue: false })
        public reelEncoding: boolean;

        /** Indices for each reel-set (RSIDS). */
        @Serialisation.Property({ key: "RSIDS", type: Serialisation.ArrayOf(Serialisation.Integer) })
        public reelSetIDs?: number[];

        /** Number of reels (RC). */
        @Serialisation.Property({ key: "RC", type: Serialisation.Integer })
        public reelCount?: number;

        /**
         * Specifies unencoded reel-sets i.e. complete sets of reel strips (RST).
         *
         * NOTE: you probably want to use reelSets.
         *
         * RST_<rid> = reel>s;s;s;s;|reel>s;s;s;s
         *
         * rid = reel strip id
         * reel = reel index
         * s = symbol value
         *
         * Example: RST_0 = 0>5;7;2;10;5;11;3;6;5;8;4;6;5;8|1>3;6;4;7;9;1;2;3;1
         */
        @Serialisation.Property({ key: "RST_{#}", multiKey: true, silent: true, type:
            Serialisation.ArrayOf(
                // inner array
                Serialisation.ArrayOf(Serialisation.Number, { delimiter: ";", delimitStrategy: Serialisation.Array.DelimitStrategy.Infix }),
                // outer array
                { delimiter: "|", indexed: true, indexDelimiter: ">" })
            })
        public unEncodedReelSets?: number[][][];

        /**
         * Specifies encoded reel-sets i.e. complete sets of reel strips (RST). \
         * See Encoding.ts for the encoding/decoding algorithm.
         *
         * NOTE: you probably want to use reelSets.
         *
         * RST_<rid> = reel>encoding|reel>encoding
         *
         * rid = reel strip id
         * reel = reel index
         * s = symbol value
         *
         * Example: RST_0 = 0>qjk8d;14rjh9;1p7fad;19s765;6f40t;1e56it|1>lql31;bexcd;g3h3x;gl1sd;ustvx;pwqvh
         */
        @Serialisation.Property({ key: "RST_{#}", multiKey: true, silent: true, type:
            Serialisation.ArrayOf(
                // inner array
                Serialisation.Slots.EncodedReel,
                // outer array
                { delimiter: "|", indexed: true, indexDelimiter: ">" })
            })
        public encodedReelSets?: number[][][];

        /**
         * Outlines changes between these reels and base reels (RSC). \
         * Specifies the positions of each reel that has changed.
         *
         * RSC_rid = reel>p:s;p:s;|reel>
         *
         * rid = reel strip id
         * reel = reel index
         * p = position of change
         * s = symbol value
         *
         * Example: RSC_2=0>5:8;24:5;2>11:1;3>0:11&
         */
        @Serialisation.Property({ key: "RSC_{#}", multiKey: true, type:
            Serialisation.ArrayOf(
                // inner array
                Serialisation.Map(Serialisation.Number, { keyValueDelimiter: ";", keyValueSeparator: ":" }),
                // outer array
                { delimiter: "|", indexed: true, indexDelimiter: ">" })
            })
        public reelSetChanges?: { [row: number]: number }[][];

        /** Specifies reel-sets i.e. complete sets of reel strips (RST). */
        public get reelSets()
        {
            return this.reelEncoding ? this.encodedReelSets : this.unEncodedReelSets;
        }
        public set reelSets(value)
        {
            if (this.reelEncoding)
            {
                this.encodedReelSets = value;
            }
            else
            {
                this.unEncodedReelSets = value;
            }
        }

        constructor(public readonly settings: ReelStripResponse.Settings, models: TModels)
        {
            super(models);
        }

        public populateModels(models: TModels)
        {
            this.setReelSets(models);
            this.setDefaultReels(models);
        }

        /** Reads unmapped reel-sets from the given response. */
        protected readReelSets(): number[][][]
        {
            const responseReelSets = this.reelSets;
            if (responseReelSets.length === 0) { throw new Error(`Reel-sets missing from response`); }

            const baseReelSet = responseReelSets[0];

            const reelSets = [baseReelSet];
            reelSets.length = responseReelSets.length;

            for (let i = 1; i < responseReelSets.length; i++)
            {
                // Clone base reel-set
                const reelSet = [...responseReelSets[i]];

                const reelSetChanges = this.reelSetChanges[i];
                if (reelSetChanges)
                {
                    // Apply reel-set changes
                    for (const reel in reelSetChanges)
                    {
                        const baseReelStrip = baseReelSet[reel];
                        const reelStripChanges = reelSetChanges[reel];

                        reelSet[reel] = [...baseReelStrip];
                        for (const row in reelStripChanges)
                        {
                            const symbol = reelStripChanges[row];
                            reelSet[reel][row] = symbol;
                        }
                    }
                }

                reelSets[i] = reelSet;
            }

            return reelSets;
        }

        /** Maps an external engine reel-set to an internal reel-set. */
        protected toSlotReelSet(reelSet: ReadonlyArray<ReadonlyArray<number>>, index: number): number[][]
        {
            return reelSet.map((reel) =>
                        reel.map((symbol) =>
                            this.toSlotSymbolID(symbol)));
        }

        /** Maps an external engine symbol ID to an internal slot symbol ID. */
        protected toSlotSymbolID(engineSymbolID: number): number
        {
            return engineSymbolID;
        }

        /** Sets the config model reel-sets using the given response. */
        protected setReelSets(models: TModels)
        {
            const reelSets = this.readReelSets()
                .map((reelSet, index) => this.toSlotReelSet(reelSet, index));

            models.config.reelSets = [];
            for (let i = 0; i < reelSets.length; i++)
            {
                const reelSetData = reelSets[i];
                try
                {
                    models.config.reelSets[i] = this.createReelSet(reelSetData, i);
                }
                catch (err)
                {
                    throw new RS.NestedError(`Failed to create reel-set ${i}`, err);
                }
            }
        }

        /** Sets up the default reels. */
        protected setDefaultReels(models: TModels)
        {
            const defaultReelSetIndex = this.settings.defaultReelSet || 0;
            const defaultReelSet = models.config.reelSets[defaultReelSetIndex];
            const defaultStopIndices = createArray<number>(defaultReelSet.reelCount);

            for (let i = 0, l = defaultStopIndices.length; i < l; ++i)
            {
                defaultStopIndices[i] = this.settings.defaultStopIndices && this.settings.defaultStopIndices[i] || 0;
            }

            models.config.defaultReels =
            {
                currentReelSetIndex: defaultReelSetIndex,
                stopIndices: defaultStopIndices,
                symbolsInView: defaultReelSet.extractSubset(defaultStopIndices, this.settings.defaultRowCount)
            };
        }

        /** Creates a new ReelSet instance using the given reel-set data. */
        protected createReelSet(data: ReadonlyArray<ReadonlyArray<number>>, index: number)
        {
            return new RS.Slots.ReelSet(data);
        }
    }

    export namespace ReelStripResponse
    {
        export interface Settings
        {
            /** Stop indices for the default reels. Defaults to 0s. */
            defaultStopIndices?: number[];
            /** Reel-set index for the default reels. Defaults to 0. */
            defaultReelSet?: number;
            /** Row count for the default reels. */
            defaultRowCount: number;
        }
    }

    export class ReelStripSequence<TModels extends RS.Slots.Models = RS.Slots.Models>
        extends SimpleMessageSequence<TModels, CommonMessagePayload, ReelStripResponse<TModels>>
    {
        constructor(public readonly settings: ReelStripSequence.Settings<TModels>)
        {
            super();
        }

        protected getMessageType()
        {
            return MessageTypes.Slots.ReelStrip;
        }

        protected createRequest(models: TModels, payload: CommonMessagePayload)
        {
            return new ReelStripRequest(models, payload);
        }

        protected createResponse(models: TModels)
        {
            return new this.settings.reelStripResponse(models);
        }
    }

    export namespace ReelStripSequence
    {
        export interface Settings<TModels extends RS.Slots.Models>
        {
            reelStripResponse?: RS.Constructor1A<ReelStripResponse<TModels>, TModels>;
        }
    }
}