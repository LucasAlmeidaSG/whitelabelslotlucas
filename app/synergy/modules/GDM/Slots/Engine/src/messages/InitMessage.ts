namespace RS.GDM.Engine.Messages.Slots
{
    export class InitResponse<TModels extends RS.Slots.Models> extends Messages.InitResponse<TModels>
    {
        /** Reel bet modes (RBM). */
        @Serialisation.Property({ key: "RBM", type: Serialisation.ArrayOf(Serialisation.Integer) })
        public reelBetModes?: number[];

        constructor(public readonly settings: InitResponse.Settings, models: TModels)
        {
            super(models);
        }

        public populateModels(models: TModels)
        {
            super.populateModels(models);

            if (this.maxWinAmount != null)
            {
                models.config.maxWinAmount = this.maxWinAmount;
                const platform = RS.IPlatform.get();
                if (platform instanceof GDM.Environment.Platform)
                {
                    platform.maxWinMode = RS.PlatformMaxWinMode.GreaterThanOrEqual;
                }
            }

            let defaultBetFound = false;
            for (let i = 0; i < this.betDenominations.length; i++)
            {
                const denom = this.betDenominations[i];

                models.stake.betOptions.push({
                    type: this.settings.stakeType,
                    value: denom
                });

                if (this.defaultBet === denom)
                {
                    defaultBetFound = true;
                    models.stake.defaultBetIndex = i;
                    models.stake.currentBetIndex = i;
                }
            }

            if (!defaultBetFound)
            {
                logger.warn(`Default bet '${this.defaultBet}' not found in stakes list`);
                models.stake.defaultBetIndex = 0;
                models.stake.currentBetIndex = 0;
            }
        }
    }

    export namespace InitResponse
    {
        export interface Settings
        {
            /** The stake type of the bet denominations in the init response. */
            stakeType: RS.Slots.Models.Stake.Type;
        }
    }

    export class InitSequence<TModels extends RS.Slots.Models>
        extends Messages.InitSequence<TModels>
    {
        public readonly settings: InitSequence.Settings<TModels>;

        constructor(settings: InitSequence.Settings<TModels>)
        {
            super(settings);

            this.settings = { ...settings };
        }

        protected async loadAdditionalData(models: TModels, payload: CommonMessagePayload, format: Serialisation.Format)
        {
            if (!await super.loadAdditionalData(models, payload, format)) { return false; }
            const sequence = this.settings.reelStripSequence;
            if (!sequence) { throw new Error("reelStripSequence cannot be null"); }
            sequence.serverConnector = this.serverConnector;
            return await sequence.execute(models, payload, format);
        }
    }

    export namespace InitSequence
    {
        export interface Settings<TModels extends RS.Slots.Models> extends Messages.InitSequence.Settings<TModels>
        {
            reelStripSequence: Engine.Messages.Slots.ReelStripSequence<TModels>;
        }
    }
}