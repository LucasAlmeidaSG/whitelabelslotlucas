namespace RS.GDM.Engine.MessageTypes.Slots.Gamble
{
    export const End = "GAMBLE_END";
    export const Start = "GAMBLE_START";
}

namespace RS.GDM.Engine.Messages.Slots.Gamble
{
    /** Possible card-related wins the player can win in a gamble. */
    export class CardPossibleWins
    {
        /** Possible color wins the player can win (#0). */
        @Serialisation.Property({ key: "0", type: Serialisation.Integer, required: true })
        public colorWin: number;

        /** Possible suit wins the player can win (#1). */
        @Serialisation.Property({ key: "1", type: Serialisation.Integer, required: true })
        public suitWin: number;
    }

    /** Gamble card suit. */
    export enum CardSuit
    {
        Heart,
        Diamond,
        Club,
        Spade
    }

    /** Gamble card color. */
    export enum CardColor
    {
        Red,
        Black
    }

    /** Gamble card history. */
    export class CardHistoryEntry
    {
        /** Card index from 0 to 51 (#0). */
        @Serialisation.Property({ key: "0", type: Serialisation.Integer, required: true })
        public card: number;
        /** Card suit (#1). */
        @Serialisation.Property({ key: "1", type: Serialisation.Integer, required: true })
        public suit: CardSuit;
        /** Card color (#2). */
        @Serialisation.Property({ key: "2", type: Serialisation.Integer, required: true })
        public color: CardColor;
    }

    /** A gamble-compatible bet response. */
    export class BetResponse<TModels extends RS.Slots.Models> extends Slots.BetResponse<TModels>
    {
        /** Amount being gambled (GAM). */
        @Serialisation.Property({ key: "GAM", type: Serialisation.Integer })
        public gambleAmount?: number;

        /**
         * History of cards that have been revealed for 5 possible gambles (GH).
         *
         * GH=cd;s;clr;|cd;s;clr;|
         *
         * cd = card (0-51)
         * s = suit (0=heart, 1=diamond, 2=club, 3=spade)
         * clr = color (0=red, 1=black)
         */
        @Serialisation.Property({ key: "GH", type: Serialisation.ArrayOf(
            // inner array
            Serialisation.Tuple(CardHistoryEntry, { delimiter: ";", delimitStrategy: Serialisation.Array.DelimitStrategy.Infix }),
            // outer array
            { delimiter: "|", delimitStrategy: Serialisation.Array.DelimitStrategy.Infix })
        })
        public cardHistory?: CardHistoryEntry[];

        /**
         * Possible color and suit wins the player can win (GPW).
         *
         * GPW=cw|sw|
         *
         * cw = color win
         * sw = suit win
         */
        @Serialisation.Property({ key: "GPW", type: Serialisation.Tuple(CardPossibleWins, { delimiter: "|" })})
        public cardPossibleWins?: CardPossibleWins;

        public populateModels(models: TModels)
        {
            if (this.gambleAmount == null)
            {
                super.populateModels(models);
            }
            else
            {
                /*
                    Gamble response is pretty much totally different:
                    "&MSGID=GAMBLE_START&B=103384&VER=2.6.3-2.4.17-4-4&TW=50",
                    "&GAM=50&GC=0&GH=-1;-1;-1|-1;-1;-1|-1;-1;-1|-1;-1;-1|-1;-1;-1",
                    "&GPW=100|200|&GA=1&SID=SESSION00000&GSD=FT~0&"

                    "&MSGID=GAMBLE_END&B=103384&VER=2.6.3-2.4.17-4-4",
                    "&GA=1&SID=SESSION00000&"
                 */
            }
        }
    }
}