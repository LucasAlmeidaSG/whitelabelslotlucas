/// <reference path="./messages/InitMessage.ts"/>
/// <reference path="./messages/ReelStripMessage.ts"/>
/// <reference path="./messages/BetMessage.ts"/>

namespace RS.GDM
{
    type ConstructionSettings<TModels extends RS.Slots.Models, T extends SlotsEngine.Settings<TModels>> =
        Partial<SlotsEngine.Settings<TModels>> &
        RS.Omit<T, keyof SlotsEngine.Settings<TModels>>;

    export class SlotsEngine<TModels extends RS.Slots.Models, TSettings extends SlotsEngine.Settings<TModels>>
        extends GDM.Engine.Base<TModels, TSettings>
        implements RS.Slots.Engine.IEngine<TModels, TSettings>
    {
        //#region IEngine.properties
        public readonly hasReelSets = true;
        //#endregion

        public get onSpinResponseReceived() { return this.__onBetResponseReceived.public; }

        constructor(settings: ConstructionSettings<TModels, TSettings>, models: TModels)
        {
            super({ ...SlotsEngine.defaultSettings, ...settings as object } as TSettings, models);
        }

        //#region IEngine.methods

        public async bet(payload: RS.Slots.Engine.ISpinRequest.Payload): Promise<RS.Engine.IResponse>
        {
            // Consume next force
            if (!await this.consumeNextForce()) { return { isError: true, request: null }; }

            // Message sequence
            const sequence = this.settings.spinSequence;
            const messagePayload = this.generateSpinMessagePayload(payload);
            const success = await this.executeSequence(sequence, messagePayload);

            // Response handling
            const response: RS.Engine.IResponse = { isError: !success, request: null };
            if (success)
            {
                this.__onBetResponseReceived.publish(response);
            }
            return response;
        }

        public async spin(payload: RS.Slots.Engine.ISpinRequest.Payload): Promise<RS.Engine.IResponse>
        {
            return await this.bet(payload);
        }

        protected toForceValues(forceResult: RS.Slots.Engine.ForceResult): ReadonlyArray<number>
        {
            return forceResult.stopIndices ? this.toEngineStops(forceResult.stopIndices) : [];
        }

        /** Converts the given slot stops to GDM engine stops for forcing. */
        protected toEngineStops(slotStops: ReadonlyArray<number>): ReadonlyArray<number>
        {
            return slotStops;
        }

        //#endregion

        /** Generates payload for spin message */
        protected generateSpinMessagePayload(payload: RS.Slots.Engine.ISpinRequest.Payload): SlotsEngine.SpinMessagePayload
        {
            const totalBet = RS.Slots.Models.Stake.toTotal(this.models.stake, payload.betAmount);
            const common = this.generateCommonMessagePayload();
            return {
                ...common,
                betAmount: totalBet,
                paylineCount: payload.paylineCount
            };
        }
    }

    export namespace SlotsEngine
    {
        export type SpinMessagePayload = Engine.CommonMessagePayload & RS.Slots.Engine.ISpinRequest.Payload;

        export interface Settings<TModels extends RS.Slots.Models> extends Engine.Settings<TModels>
        {
            spinSequence: Engine.IMessageSequence<TModels, SpinMessagePayload>;
        }

        import InitResponse = Engine.Messages.Slots.InitResponse;
        import InitSequence = Engine.Messages.Slots.InitSequence;

        import ReelStripResponse = Engine.Messages.Slots.ReelStripResponse;
        import ReelStripSequence = Engine.Messages.Slots.ReelStripSequence;

        import BetResponse = Engine.Messages.Slots.BetResponse;
        import BetSequence = Engine.Messages.Slots.BetSequence;

        export const defaultBetResponse = Engine.Messages.Message.bind(Engine.Messages.Slots.BetResponse, {});
        export const defaultInitResponse = Engine.Messages.Message.bind(Engine.Messages.Slots.InitResponse, { stakeType: RS.Slots.Models.Stake.Type.Total });
        export const defaultReelStripResponse = Engine.Messages.Message.bind(Engine.Messages.Slots.ReelStripResponse, { defaultRowCount: 3 })

        export const defaultSettings: Settings<RS.Slots.Models> =
        {
            initSequence: new InitSequence(
            {
                betResponse: BetResponse.bind(BetResponse, {}),
                initResponse: InitResponse.bind(InitResponse, { stakeType: RS.Slots.Models.Stake.Type.Total }),
                reelStripSequence: new ReelStripSequence(
                {
                    reelStripResponse: ReelStripResponse.bind(ReelStripResponse, { defaultRowCount: 3 })
                })
            }),
            spinSequence: new BetSequence(
            {
                betResponse: BetResponse.bind(BetResponse, {})
            })
        };
    }
}