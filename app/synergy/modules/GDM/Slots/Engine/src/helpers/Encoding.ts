namespace RS.GDM.Engine
{
    export namespace Slots.ReelEncoding
    {
        // This is the format the GDM engine uses when reel encoding is turned on.

        // This is the bit string format:
        // |symbol4|symbol3|symbol2|symbol1|symbol0|count|
        // Each symbol is 5 bits, the count is 3.

        // Symbols are grouped into words of up to 5 symbols separated by semi-colons.
        // The encoding algorithm was ported directly from GDM source.
        // The decoding algorithm was then reverse-engineered so that we can support it.

        //#region Parameters

        /** Max symbols per word. In conjunction with symbolSize prevents integer overflow. */
        const wordSize = 5;
        /** Number of bits per symbol. In conjunction with wordSize prevents integer overflow. */
        const symbolSize = 5;
        /** Numeric base (radix) for string representation. */
        const radix = 36;

        //#endregion

        //#region Calculated consts

        /** Number of bits for the count. Calculated as the fewest number of bits required to represent the maximum word size. */
        const countSize = Math.ceil(RS.Math.log2(wordSize));
        /** Highest possible count according to countSize. Used for the bitmask when decoding to get the count. */
        const maxCount = 2 ** countSize - 1;
        /** Highest possible symbol ID according to symbolSize. Used for the bitmask when decoding to get the symbol ID. */
        const maxSymbolID = 2 ** symbolSize - 1;

        //#endregion

        /** Encodes a reel-strip to reduce the amount of text required. */
        export function encode(reelStrip: ReadonlyArray<number>): string
        {
            let encodedStrip = "";

            let count = 0;
            let newSymbolID = 0;
            for (const symbolID of reelStrip)
            {
                // Store symbolID in the next 5 bits, leaving the 3 lowest bits reserved for the count
                newSymbolID |= symbolID << (count * symbolSize + countSize);
                count++;

                if (count === wordSize)
                {
                    // Store 'count' in the lowest 3 bits
                    newSymbolID |= count;
                    // Write encoded value in base-36
                    encodedStrip += `${newSymbolID.toString(radix)};`;

                    // Reset all to 0
                    count = 0;
                    newSymbolID = 0;
                }
            }

            // Process 'left over' data from loop
            if (count > 0)
            {
                newSymbolID |= count;
                encodedStrip += `${newSymbolID.toString(radix)};`;
            }

            // Remove trailing ;
            if (encodedStrip.charAt(encodedStrip.length - 1) === ";")
            {
                encodedStrip = encodedStrip.substr(0, encodedStrip.length - 1);
            }

            return encodedStrip;
        }

        /** Decodes an encoded reel-strip. Tbh I mostly wrote this for a bit of fun. */
        export function decode(encodedStrip: string): number[]
        {
            const reelStrip: number[] = [];

            let word: string = "";
            for (let charIndex = 0; charIndex < encodedStrip.length; charIndex++)
            {
                const char = encodedStrip[charIndex];
                if (char === ";" || charIndex === encodedStrip.length - 1)
                {
                    if (charIndex === encodedStrip.length - 1)
                    {
                        word += char;
                    }

                    // Get the numeric value
                    const newSymbolID = parseInt(word, radix);

                    // Get the 3 right-most bits.
                    const count = newSymbolID & maxCount;

                    // Read 'count' symbol IDs
                    for (let i = 0; i < count; i++)
                    {
                        const offset = i * symbolSize + countSize;
                        const symbolID = (newSymbolID >> offset) & maxSymbolID;
                        reelStrip.push(symbolID);
                    }

                    word = "";
                }
                else
                {
                    word += char;
                }
            }

            return reelStrip;
        }
    }

    export namespace Serialisation.Slots
    {
        /** Specifies that the property is to be serialised and deserialised as an encoded array of numbers as per the GDM reel encoding. */
        export const EncodedReel = Type<number[]>(
            function (arr)
            {
                if (!RS.Is.arrayOf(arr, RS.Is.number))
                {
                    throw new Error(`Not an array of numbers`);
                }

                return Engine.Slots.ReelEncoding.encode(arr);
            },
            function (str)
            {
                return Engine.Slots.ReelEncoding.decode(str);
            }
        );
    }
}