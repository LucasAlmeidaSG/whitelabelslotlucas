namespace RS.GDM.Environment.Tests
{
    const { expect } = chai;

    describe("APIExt.ts", function ()
    {
        describe("APIExt", function ()
        {
            describe("event publishing", function ()
            {
                it("should be published when onAPIExt is called", function ()
                {
                    const apiExt = new APIExt(true);
                    const spy = sinon.spy();
                    apiExt.onCalled(spy);

                    apiExt.onAPIExt(true);
                    expect(spy.callCount).to.equal(1);
                });
            });

            describe("value", function ()
            {
                it("should be initially equal to the initial value if one was set", function ()
                {
                    const apiExt = new APIExt(true);
                    expect(apiExt.value).to.equal(true);
                });

                it("should be initially equal to undefined if no initial value was set", function ()
                {
                    const apiExt = new APIExt();
                    expect(apiExt.value).to.equal(undefined);
                });

                it("should be equal to the value of the last argument passed", function ()
                {
                    const apiExt = new APIExt(true);
                    apiExt.onAPIExt(false);
                    expect(apiExt.value).to.equal(false);
                });
            });

            describe("result delegation", function ()
            {
                it("should return undefined with no result delegate", function ()
                {
                    const apiExt = new APIExt();
                    expect(apiExt.onAPIExt(null)).to.equal(undefined);
                });

                it("should pass the argument to the result delegate", function ()
                {
                    const apiExt = new APIExt();

                    const spy = sinon.spy();
                    apiExt.setResultDelegate(spy);

                    const arg = "hi";
                    apiExt.onAPIExt(arg);

                    expect(spy.callCount, "delegate should be called once").to.equal(1);
                    expect(spy.lastCall.args[0]).to.equal(arg);
                });

                it("should return the result of the result delegate", function ()
                {
                    const apiExt = new APIExt();

                    const retVal = "hi";
                    apiExt.setResultDelegate(() => retVal);

                    expect(apiExt.onAPIExt(null)).to.equal(retVal);
                });
            });

            describe("arbiter binding", function ()
            {
                it("should declare initial value on the arbiter if one was set", function ()
                {
                    const apiExt = new APIExt(true);
                    const arbiter = new RS.Arbiter(RS.Arbiter.OrResolver, false);

                    const spy = sinon.spy(arbiter, "declare");
                    apiExt.bindArbiter(arbiter);

                    expect(spy.callCount, "declare should be called").to.equal(1);
                    expect(spy.lastCall.args[0], "declare should be called with initial value").to.equal(true);
                });

                it("should declare previous value if one was set", function ()
                {
                    const apiExt = new APIExt();
                    const arbiter = new RS.Arbiter(RS.Arbiter.OrResolver, false);
                    apiExt.onAPIExt(false);

                    const spy = sinon.spy(arbiter, "declare");
                    apiExt.bindArbiter(arbiter);

                    expect(spy.callCount, "declare should be called").to.equal(1);
                    expect(spy.lastCall.args[0], "declare should be called with previous value").to.equal(false);
                });

                it("should not declare any value on the arbiter if none was set", function ()
                {
                    const apiExt = new APIExt();
                    const arbiter = new RS.Arbiter(RS.Arbiter.OrResolver, false);

                    const spy = sinon.spy(arbiter, "declare");
                    apiExt.bindArbiter(arbiter);

                    expect(spy.called, "declare should not be called on arbiter").to.equal(false);
                });

                it("should declare undefined if it was explicitly passed as the initial value", function ()
                {
                    const apiExt = new APIExt(undefined);
                    const arbiter = new RS.Arbiter((a, b) => b, false);

                    const spy = sinon.spy(arbiter, "declare");
                    apiExt.bindArbiter(arbiter);

                    expect(spy.callCount, "declare should be called").to.equal(1);
                    expect(spy.lastCall.args[0], "declare should be called with undefined").to.equal(undefined);
                });

                it("should be possible to unbind an arbiter once bound", function ()
                {
                    const apiExt = new APIExt();
                    const arbiter = new RS.Arbiter(RS.Arbiter.OrResolver, false);

                    const spy = sinon.spy(arbiter, "undeclare");
                    apiExt.bindArbiter(arbiter);
                    apiExt.onAPIExt(true);
                    apiExt.unbindArbiter(arbiter);

                    expect(spy.callCount, "undeclare should be called once").to.equal(1);
                });

                it("should declare a new value on the arbiter when passed one", function ()
                {
                    const apiExt = new APIExt(false);
                    const arbiter = new RS.Arbiter(RS.Arbiter.OrResolver, false);

                    const spy = sinon.spy(arbiter, "declare");
                    apiExt.bindArbiter(arbiter);
                    apiExt.onAPIExt(true);

                    expect(spy.callCount, "declare should be called for initial value and later update").to.equal(2);
                    expect(spy.lastCall.args[0], "declare should be called with passed value").to.equal(true);
                });

                it("should undeclare on the arbiter before re-declaring", function ()
                {
                    const apiExt = new APIExt();
                    const arbiter = new RS.Arbiter(RS.Arbiter.OrResolver, false);

                    const spy = sinon.spy(arbiter, "undeclare");
                    apiExt.bindArbiter(arbiter);

                    apiExt.onAPIExt(true);
                    apiExt.onAPIExt(false);

                    expect(spy.callCount, "undeclare should be called once").to.equal(1);
                });
            });
        });
    });

    describe("APIExtDispatcher.ts", function ()
    {
        describe("APIExtDispatcher", function ()
        {
            it("should initialise APIExts with explicit values passed in the constructor", function ()
            {
                const dispatcher = new APIExtDispatcher({ ENABLE_ALL_UI: false });
                expect(dispatcher.getAPIExt("ENABLE_ALL_UI").value).to.equal(false);
            });

            it("should return an uninitialised APIExt if not given a value in the constructor", function ()
            {
                const dispatcher = new APIExtDispatcher({});
                expect(dispatcher.getAPIExt("ENABLE_ALL_UI").value).to.equal(undefined);
            });

            it("should consider values passed via onAPIExt even without any previous getAPIExt calls", function ()
            {
                const dispatcher = new APIExtDispatcher();
                dispatcher.onAPIExt("ENABLE_ALL_UI", true);
                const apiExt = dispatcher.getAPIExt("ENABLE_ALL_UI");
                expect(apiExt.value).to.equal(true);
            });
        });
    });
}