namespace RS.GDM.Environment.Tests
{
    const { expect } = chai;

    describe("APIGlobals.ts", function ()
    {
        describe("APIGlobals", function ()
        {
            interface MockGlobals
            {
                mockGlobal: boolean;
                mockOtherGlobal: boolean;
            }

            const windowWithGlobals = window as Partial<MockGlobals>;

            let globals: APIGlobals<MockGlobals>;
            beforeEach(function ()
            {
                globals = new APIGlobals();
            });

            afterEach(function ()
            {
                delete windowWithGlobals.mockGlobal;
                delete windowWithGlobals.mockOtherGlobal;
            });

            describe("set", function ()
            {
                it("should set a global on the window", function ()
                {
                    globals.set("mockGlobal", true);
                    expect(windowWithGlobals.mockGlobal).to.equal(true);
                });

                it("should allow overriding a previously-set global", function ()
                {
                    globals.set("mockGlobal", true);
                    globals.set("mockGlobal", false);
                    expect(windowWithGlobals.mockGlobal).to.equal(false);
                });

                it("should not allow overriding an externally-set global", function ()
                {
                    windowWithGlobals.mockGlobal = true;
                    expect(() => globals.set("mockGlobal", true)).to.throw();
                });
            });

            describe("unset", function ()
            {
                it("should unset a previously set global", function ()
                {
                    globals.set("mockGlobal", true);
                    globals.unset("mockGlobal");
                    expect(windowWithGlobals).not.to.have.ownProperty("mockGlobal");
                });

                it("should not allow unsetting an externally-set global", function ()
                {
                    windowWithGlobals.mockGlobal = true;
                    globals.unset("mockGlobal");
                    expect(windowWithGlobals.mockGlobal).to.equal(true);
                });

                it("should return true if a change was made", function ()
                {
                    globals.set("mockGlobal", true);
                    expect(globals.unset("mockGlobal")).to.equal(true);
                });

                it("should return false if no change was made", function ()
                {
                    expect(globals.unset("mockGlobal")).to.equal(false);
                });

                it("should return false if no change was made due to an externally-set global", function ()
                {
                    windowWithGlobals.mockGlobal = true;
                    expect(globals.unset("mockGlobal")).to.equal(false);
                });
            });

            describe("get", function ()
            {
                it("should get a previously set global", function ()
                {
                    globals.set("mockGlobal", true);
                    expect(globals.get("mockGlobal")).to.equal(true);
                });

                it("should return null for unset globals", function ()
                {
                    expect(globals.get("mockGlobal")).to.equal(null);
                })

                it("should return null for externally-set globals", function ()
                {
                    windowWithGlobals.mockGlobal = true;
                    expect(globals.get("mockGlobal")).to.equal(null);
                });
            });

            describe("setAll", function ()
            {
                it("should set all globals", function ()
                {
                    globals.setAll(
                    {
                        mockGlobal: true,
                        mockOtherGlobal: true
                    });
                    expect(windowWithGlobals.mockGlobal).to.equal(true);
                    expect(windowWithGlobals.mockOtherGlobal).to.equal(true);
                });
            });

            describe("unsetAll", function ()
            {
                it("should unset all previously set globals", function ()
                {
                    globals.set("mockGlobal", true);
                    globals.unsetAll();
                    expect(windowWithGlobals).not.to.have.ownProperty("mockGlobal");
                });

                it("should not allow unsetting an externally-set global", function ()
                {
                    windowWithGlobals.mockGlobal = true;
                    globals.unsetAll();
                    expect(windowWithGlobals.mockGlobal).to.equal(true);
                });
            })

            describe("map", function ()
            {
                it("should return all set globals", function ()
                {
                    globals.set("mockGlobal", true);
                    expect(globals.map).to.deep.equal({ mockGlobal: true });
                });
            });
        });
    });
}