namespace RS.GDM.Environment
{
    const { expect } = chai;

    describe("MessageQueue.ts", function ()
    {
        describe("MessageQueue", function ()
        {
            let queue: MessageQueue, spy: sinon.SinonSpy;
            beforeEach(function ()
            {
                spy = sinon.spy();
                queue = new MessageQueue(spy);
            });

            describe("enqueueing", function ()
            {
                it("should invoke the request delegate immediately when the queue is empty", function ()
                {
                    const mockRequest = "request";

                    queue.enqueue(mockRequest);

                    expect(spy.callCount, "request delegate called once").to.equal(1);
                    expect(spy.lastCall.args[0], "request delegate passed request").to.equal(mockRequest);
                });

                it("should not invoke the request delegate if there is another message in the queue", function ()
                {
                    const mockFirstRequest = "request1";
                    const mockSecondRequest = "request2";

                    queue.enqueue(mockFirstRequest);
                    queue.enqueue(mockSecondRequest);

                    expect(spy.callCount, "request delegate called once").to.equal(1);
                    expect(spy.lastCall.args[0], "request delegate passed first request").to.equal(mockFirstRequest);
                });

                it("should return a Promise which resolves when a response is passed to the queue when the queue is empty", async function ()
                {
                    const mockRequest = "request";
                    const mockResponse = "response";

                    const response = queue.enqueue(mockRequest);
                    queue.onResponse(mockResponse);

                    expect(await response).to.equal(mockResponse);
                });

                it("should return a Promise which resolves after the first message is resolved and another message is passed to the queue if a message is ahead in the queue", async function ()
                {
                    const mockFirstRequest = "request1";
                    const mockFirstResponse = "response1";
                    const mockSecondRequest = "request2";
                    const mockSecondResponse = "response2";

                    queue.enqueue(mockFirstRequest);
                    const response = queue.enqueue(mockSecondRequest);
                    queue.onResponse(mockFirstResponse);
                    queue.onResponse(mockSecondResponse);

                    expect(await response).to.equal(mockSecondResponse);
                });

                it("should return a Promise which is rejected if rejectNext is called", async function ()
                {
                    const mockRequest = "request";
                    const rejectReason = "error";

                    const response = queue.enqueue(mockRequest);
                    queue.rejectNext(rejectReason);

                    try
                    {
                        await response;
                    }
                    catch (err)
                    {
                        expect(err).to.equal(rejectReason);
                        return;
                    }

                    expect(false, "should be rejected").to.equal(true);
                });

                it("should return a Promise which is rejected if rejectAll is called", async function ()
                {
                    const mockRequest = "request";
                    const rejectReason = "error";

                    const response = queue.enqueue(mockRequest);
                    queue.rejectAll(rejectReason);

                    try
                    {
                        await response;
                    }
                    catch (err)
                    {
                        expect(err).to.equal(rejectReason);
                        return;
                    }

                    expect(false, "should be rejected").to.equal(true);
                });

                it("should return a Promise which is resolved with the response if rejectNext is called and there is a message ahead in the queue", async function ()
                {
                    const mockFirstRequest = "request1";
                    const mockSecondRequest = "request2";
                    const rejectReason = "error";
                    const mockSecondResponse = "response2";

                    queue.enqueue(mockFirstRequest);
                    const response = queue.enqueue(mockSecondRequest);
                    queue.rejectNext(rejectReason);
                    queue.onResponse(mockSecondResponse);

                    expect(await response).to.equal(mockSecondResponse);
                });

                it("should return a Promise which is rejected if rejectAll is called and there is a message ahead in the queue", async function ()
                {
                    const mockFirstRequest = "request1";
                    const mockSecondRequest = "request2";
                    const rejectReason = "error";

                    queue.enqueue(mockFirstRequest);
                    const response = queue.enqueue(mockSecondRequest);
                    queue.rejectAll(rejectReason);

                    try
                    {
                        await response;
                    }
                    catch (err)
                    {
                        expect(err).to.equal(rejectReason);
                        return;
                    }

                    expect(false, "should be rejected").to.equal(true);
                });
            });

            describe("onResponse", function ()
            {
                it("should throw if no messages are queued", function ()
                {
                    expect(() => queue.onResponse("response")).to.throw();
                });
            });

            describe("rejectNext", function ()
            {
                it("should throw if no messages are queued", function ()
                {
                    expect(() => queue.rejectNext("response")).to.throw();
                });
            });

            describe("rejectAll", function ()
            {
                it("should do nothing if no messages are queued", function ()
                {
                    expect(() => queue.rejectAll("response")).not.to.throw();
                });
            });

            describe("constructor", function ()
            {
                it("should throw if no request delegate is passed", function ()
                {
                    expect(() => new MessageQueue(null)).to.throw();
                });
            });

            describe("pending", function ()
            {
                it("should be 0 for a newly created queue", function ()
                {
                    expect(queue.pending).to.equal(0);
                });

                it("should be 1 for a queue with a message in progress", function ()
                {
                    queue.enqueue("message");
                    expect(queue.pending).to.equal(1);
                });

                it("should be 2 for a queue with a message in progress and one queued message", function ()
                {
                    queue.enqueue("message1");
                    queue.enqueue("message2");
                    expect(queue.pending).to.equal(2);
                });
            });
        });
    });
}