namespace RS.GDM.Environment.Tests
{
    const { expect } = chai;
    describe("ValueChangeProxy.ts", function ()
    {
        describe("ValueChangeProxy", function ()
        {
            class MockValueChangeable implements ValueChangeProxy.IValueChangeable
            {
                public readonly valueChanged = sinon.spy();
            }

            let mockTarget: MockValueChangeable, mockProxy: ValueChangeProxy;
            beforeEach(function ()
            {
                mockTarget = new MockValueChangeable();
                mockProxy = new ValueChangeProxy(mockTarget);
            });

            it("should not invoke valueChanged on the target when deferred is true", function ()
            {
                mockProxy.deferred = true;
                mockProxy.valueChanged("BALANCE", 50);
                expect(mockTarget.valueChanged.called).to.equal(false);
            });

            it("should invoke valueChanged on the target when deferred is false", function ()
            {
                mockProxy.deferred = false;
                mockProxy.valueChanged("BALANCE", 50);
                expect(mockTarget.valueChanged.callCount, "target valueChanged should be called once").to.equal(1);
            });

            it("should pass withheld values to the target when deferred is set to false", function ()
            {
                mockProxy.deferred = true;
                mockProxy.valueChanged("BALANCE", 50);
                mockProxy.deferred = false;

                expect(mockTarget.valueChanged.callCount, "target valueChanged should be called once").to.equal(1);
                expect(mockTarget.valueChanged.lastCall.args[0], "key should match that of the proxy call").to.equal("BALANCE");
                expect(mockTarget.valueChanged.lastCall.args[1], "value should match that of the proxy call").to.equal(50);
            });

            it("should not pass previously-passed values to the target when deferred is set to false twice", function ()
            {
                mockProxy.deferred = true;
                mockProxy.valueChanged("BALANCE", 50);
                mockProxy.deferred = false;
                mockProxy.deferred = true;
                mockProxy.deferred = false;
                expect(mockTarget.valueChanged.callCount, "target valueChanged should be called once").to.equal(1);
            });

            it("should not pass previously-passed values to the target when valueChanged is called twice and deferred is true", function ()
            {
                mockProxy.deferred = true;
                mockProxy.valueChanged("BALANCE", 25);
                mockProxy.valueChanged("BALANCE", 50);
                mockProxy.deferred = false;
                expect(mockTarget.valueChanged.callCount, "target valueChanged should be called once").to.equal(1);
                expect(mockTarget.valueChanged.lastCall.args[0], "key should match that of the last proxy call").to.equal("BALANCE");
                expect(mockTarget.valueChanged.lastCall.args[1], "value should match that of the last proxy call").to.equal(50);
            });
        });
    });
}