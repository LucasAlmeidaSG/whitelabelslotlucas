namespace RS.GDM.Environment.Tests
{
    const { expect } = chai;

    describe("Utils.ts", function ()
    {
        describe("isMock", function ()
        {
            it("should return true for numeric opids under 100", function ()
            {
                for (let i = 1; i < 100; i++)
                {
                    const opid = i;
                    expect(isMock(opid)).to.equal(true);
                }
            });

            it("should return true for string opids under 100", function ()
            {
                for (let i = 1; i < 100; i++)
                {
                    const opid = `${i}`;
                    expect(isMock(opid)).to.equal(true);
                }
            });

            it("should return false for numeric opids 100 and over", function ()
            {
                for (let i = 100; i < 150; i++)
                {
                    const opid = i;
                    expect(isMock(opid)).to.equal(false);
                }
            });

            it("should return false for string opids 100 and over", function ()
            {
                for (let i = 100; i < 150; i++)
                {
                    const opid = `${i}`;
                    expect(isMock(opid)).to.equal(false);
                }
            });

            it("should return false for null", function ()
            {
                expect(isMock(null)).to.equal(false);
            });

            it("should return false for undefined", function ()
            {
                expect(isMock(undefined)).to.equal(false);
            });

            it("should return false for empty string", function ()
            {
                expect(isMock("")).to.equal(false);
            });

            it("should return false for spacy string", function ()
            {
                expect(isMock("   ")).to.equal(false);
            });

            it("should return false for non-numeric string", function ()
            {
                expect(isMock("opid")).to.equal(false);
                // parseInt et al won't suffice for this, use Number()
                expect(isMock("1opid")).to.equal(false);
                expect(isMock("opid1")).to.equal(false);
            });
        });
    });

}