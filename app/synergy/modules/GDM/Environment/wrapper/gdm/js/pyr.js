
// the following object may or may not exist, we have this here as one of our games tests for it and customizes the game if it can detect where it is being played from
// do not rely on the following to exist
    window.urlParameters = {
        lobby: 'https://gaming.williamhill.com/launch/return'
    };

// custom PYR function called as part of gameReady
	function environmentGameReady() {
	// show topbar volume button
		document.getElementById('topbarVolume').style.display = 'inline-block';

	// tell the progress percentage to hide
        progressComplete();
    // set the balance to the value stored in the INIT response
        if ( typeof balance === 'number' ) { updateField(fields.BALANCE, balance); }

    // enable the lock button, this will be used to show whether the game is busy or not
        document.getElementById('topbarLock').style.display = 'inline-block';
        toggleLock();

    // we send a showRTP apiExt to indicate whether to display RTP or not
        if (!params.showRtp) {
            apiCall("SHOW_RTP", false);
        }else{
            apiCall("SHOW_RTP", true);
        }
    }

// internal use only
// emulate reducing the bet if we are on PYR
// note that in places we have found this to cause issues, for example if a game requires balance to be reduced by more than stake
// (if buying into the bonus game) we do not yet have enough data to determine if this will be a problem on live PYR and so this
// code is still here
    function deductStake() {
        topbarText.balance.innerHTML = (Number(topbarText.balance.innerHTML) - Number(topbarText.bet.innerHTML)).toFixed(2);
    // the PYR wrapper sends a SET_BALANCE apiExt when stake is deducted, this caused an issue in one of our games at one point so this is now
    // replicated here. If you're here because balance is getting weird then I recommend you ignore SET_BALANCE calls during spins
        apiCall("SET_BALANCE", Number(topbarText.balance.innerHTML) * 100);
    }

// called by the game to set certain values
    function valueChanged(name, value) {
        if (params.debug) { console.log("%c Wrapper received " + name + ", " + value + " to valueChanged ", "color: white; background: black"); }
        switch (name) {
            case "TOTAL_BET":
                updateField(fields.TOTAL_BET, value);
                updateField(fields.WINNINGS, 0);
            break;
            case "ROUND":
                value = Number(value);
                if (value === 0) {
                    updateField(fields.WINNINGS, apiCall("GET_ROUND_TOTAL_WIN"));
                    updateField(fields.BALANCE, balance);
                    if (gameActive === Boolean(value)) {
                        internalError('ROUND set to ' + Number(value) + ' when it was already ' + Number(gameActive));
                    }else{
                        gameActive = Boolean(value);
                    }
                    onCloseGame();
                } else {
                    updateField(fields.WINNINGS, 0);
                    if (gameActive === Boolean(value)) {
                        internalError('ROUND set to ' + Number(value) + ' when it was already ' + Number(gameActive));
                    }else{
                        gameActive = Boolean(value);
                    }
                }
                toggleLock();
            break;
        }
    }

// returns true if you should enable the ingame sound button and false if not
    function audioBtnVisibility () {
        return false;
    }

// internal use only, called by the topbar volume button
    function toggleVolume () {
    // send the appropriate apiExt
        if (document.getElementById('topbarVolume').style.display === 'inline-block') {
            if (document.getElementById('topbarVolume').getElementsByTagName('img')[0].src.indexOf('on.png') > -1) {
                apiCall("SET_MUTE", true);
                document.getElementById('topbarVolume').getElementsByTagName('img')[0].src = 'gdm/img/vol-off.png';
            }else{
                apiCall("SET_MUTE", false);
                document.getElementById('topbarVolume').getElementsByTagName('img')[0].src = 'gdm/img/vol-on.png';
            }
        }
    }

// to be called to update the loading bar in the wrapper
    function updateProgress (count, total) {
        document.getElementById('topbarProgress').innerHTML = Math.round((count/total) * 100) + '%';
    }

// function the game calls to tell the wrapper that autoplay has begun
    function autoPlayNotification ( autoplays, losslimit, maxwinlimit) {
        if (typeof autoplays !== 'number' || typeof losslimit !== 'number' || typeof maxwinlimit !== 'number' ||
            isNaN(autoplays) || isNaN(losslimit) || isNaN(maxwinlimit)) {
            internalError('Invalid autoPlayNotification arguments: '+autoplays + ' ' + losslimit + ' ' + maxwinlimit);
        }else{
            document.getElementById('autoplayEnabled').style.display = 'inline-block';
        }
    }

// call this if it exists rather than showing the error yourself
// note that if you set unrecoverable then the message will be a generic one
    function delegatedErrorHandling (code, msg, unrecoverable) {
        if (unrecoverable) {
            showRestartError();
            return;
        }
        switch (code) {
            case 'ERROR_INSUFFICIENTFUNDS':
                showError(
                    msg, 
                    'Add 1000', 
                    'Cancel', 
                    function () { setBalanceTo(1000); hideError();}.bind(this),
                    hideError.bind(this),
                    'blue',
                    'white'
                );
                break;
            default:
                var func;
                showError(msg, 'Okay', '', hideError.bind(window));
                break;
        }
    }