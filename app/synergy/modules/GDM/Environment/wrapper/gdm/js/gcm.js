// custom GCM function called as part of gameReady
	function environmentGameReady() {
		document.getElementById('topbarVolume').style.display = 'inline-block';

	    enableAllUI(true); // happens on live
	    gameiframe.style.height = '';
	// this is just a test because a game once crashed when it received an apiExt that it wasn't expecting
	    apiCall("TEST_THAT_WE_ACCEPT_UNEXPECTED_APIEXTS", true);
	    document.getElementById('topbarAbout').style.display = 'inline-block';
	    document.getElementById('topbarSettings').style.display = 'inline-block';
	    document.getElementById('topbarLock').style.display = 'inline-block';
	// set the alt-text of the image to the game name
	    document.getElementById('logoImage').title = apiCall('GET_GAME_NAME');

    // enable the lock button, this will be used to show whether the game is busy or not
        document.getElementById('topbarLock').style.display = 'inline-block';
        toggleLock();

    // we send a showRTP apiExt to indicate whether to display RTP or not
        if (!params.showRtp) {
            apiCall("SHOW_RTP", false);
        }else{
            apiCall("SHOW_RTP", true);
        }
	}

// called when the about button is clicked in the topbar
    function showAbout () {
        if (document.getElementById('topbarAbout').style.display === 'inline-block') {
            apiCall("SHOW_GAME_INFO");
        }
    }

// called when the settings button is clicked in the topbar
    function showSettings () {
        if (document.getElementById('topbarSettings').style.display === 'inline-block') {
            apiCall("SHOW_SETTING");
        }
    }

// called by the game to set certain values
    function valueChanged(name, value) {
        if (params.debug) { console.log("%c Wrapper received " + name + ", " + value + " to valueChanged ", "color: white; background: black"); }
        switch (name) {
            case "ROUND":
            	value = Boolean(Number(value));
                if (gameActive === value) {
                    internalError('ROUND set to ' + Number(value) + ' when it was already ' + Number(gameActive));
                }else{
                    gameActive = value;
                    if (gameActive === false) { onCloseGame(); }
                }
                toggleLock();
            break;
            case "TOTAL_BET":
                updateField(fields.TOTAL_BET, value);
            break;
            case "TOTAL_WIN":
                updateField(fields.WINNINGS, value);
            break;
            case "BALANCE":
                updateField(fields.BALANCE, value);
            break;
            case "MUTE":
                document.getElementById('topbarVolume').getElementsByTagName('img')[0].src = (value) ? 'gdm/img/vol-off.png' : 'gdm/img/vol-on.png';
            break;
            case 'ABOUT':
            break;
            case 'SETTINGS':
            break;
        }
    }

// returns true if you should enable the ingame sound button and false if not
    function audioBtnVisibility () {
        return true;
    }

// to be called to update the loading bar in the wrapper
    function updateProgress (count, total) {
        document.getElementById('topbarProgress').innerHTML = Math.round((count/total) * 100) + '%';
        if (Math.round((count/total) * 100) === 100) {
            progressComplete();
        }
    }

// function the game calls to tell the wrapper that autoplay has begun
    function autoPlayNotification ( autoplays, losslimit, maxwinlimit) {
        if (typeof autoplays !== 'number' || typeof losslimit !== 'number' || typeof maxwinlimit !== 'number' ||
            isNaN(autoplays) || isNaN(losslimit) || isNaN(maxwinlimit)) {
            internalError('Invalid autoPlayNotification arguments: '+autoplays + ' ' + losslimit + ' ' + maxwinlimit);
        }else{
            document.getElementById('autoplayEnabled').style.display = 'inline-block';
        }
    }

// call this if it exists rather than showing the error yourself
// note that if you set unrecoverable then the message will be a generic one
    function delegatedErrorHandling (code, msg, unrecoverable) {
        if (unrecoverable) {
            showRestartError();
            return;
        }
        switch (code) {
            case 'ERROR_INSUFFICIENTFUNDS':
                showError(
                    apiCall('GET_ERROR_MESSAGE', code),
                    'Add 1000', 
                    'Cancel', 
                    function () { setBalanceTo(1000); hideError();}.bind(this),
                    hideError.bind(this),
                    'blue',
                    'white'
                );
                break;
            default:
                showError(apiCall('GET_ERROR_MESSAGE', code), 'Okay', '', hideError.bind(window));
                break;
        }
    }

