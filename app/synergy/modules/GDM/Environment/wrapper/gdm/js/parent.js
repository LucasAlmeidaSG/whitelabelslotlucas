// check our environment on shefbox, we do not use cross-origin on dev but we ensure the setup is the same
    var environments = {
        'DEV':  0,
        'DEMO': 1,
        'QA':   2
    };

    var environment = environments.DEMO;
    if (document.location.pathname.indexOf('/dev/') > -1) {
        environment = environments.DEV;
    }else if (document.location.pathname.indexOf('/qa/') > -1) {
        environment = environments.QA;
    }

// function to call when the child iframe sends us a cross-origin compatible message
    function messageParse (event) {
        switch (event.data.key) {
            case 'goToUrl':
                goToUrl(event.data.value); break;
            case 'refreshPage':
                refreshPage(); break;
            case 'setTitle':
                setTitle(event.data.value); break;
        }
    }

    function setTitle (title) {
        document.title = title;
    }

    function refreshPage () {
        location.reload();
    }

    function goToUrl (url) {
        if (url === 'home') {
            window.open(location.href.substring(0, location.href.lastIndexOf("/wrapper.htm") + 1), '_self');
        }else{
            window.open(url, '_self');
        }
    }

// this is called when we're on dev only. it defines a bunch of variables in the top context that we can use
// as shortcuts
    function setGlobals () {
        window.g = window.wrapper.contentWindow.g
        window.game = window.wrapper.contentWindow.game;
        if (window.g && window.game) {
            window.clearInterval(window.globalInterval);
        }
    }

    function onLoad () {
        window.scrollTo(0,0);

        window.addEventListener('message', messageParse);

        window.wrapper = document.getElementById("wrapperiframe");

        window.addEventListener('resize', setiFrameSize);
        setiFrameSize();

        var address;
    // set address to either the ip or the server name
    // don't set it to https if we're using the ip as the https is registered to shefbox.co.uk
        if (window.location.hostname.toLowerCase().indexOf('shefbox') > -1 && environment === environments.QA && navigator.userAgent.indexOf('Edge') === -1) {
            address = 'http://109.228.54.244';
        } else if (window.location.hostname.toLowerCase().indexOf('109.228.54.244') > -1 && environment !== environments.DEV && navigator.userAgent.indexOf('Edge') === -1) {
            address = 'http://shefbox.co.uk';
        }else {
            address = window.location.origin;
        }
        if (environment === environments.DEV) {
            window.globalInterval = window.setInterval(setGlobals, 250);
        }
    // add the correct subdirectory (dev, demo etc) and point to wrapper.htm
        address = address + window.location.pathname.replace('wrapper.htm', 'wrapper_gdm.htm') + window.location.search;
        
    // add the same parameters as this page
        window.wrapper.src = address;
    };

// the following is a duplicate of code found in mobile.js
// ensures the same setup
    var iosVer = parseFloat(
        ('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0,''])[1])
        .replace('undefined', '3_2').replace('_', '.').replace('_', '')
        ) || -1;

    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (navigator.userAgent.match(/iPhone/i)|| ((/Android/i.test(navigator.userAgent.toLowerCase())) && (/chrome/i.test(navigator.userAgent.toLowerCase())))) {
        if (viewportmeta /*&& !iphone_uiwebview*/) {
            if (window.navigator.standalone && iosVer >= 8.3 && iosVer < 9 && navigator.userAgent.match(/iPhone/i)) {
            } else {
                viewportmeta.content = 'width=device-width, initial-scale=0.5, maximum-scale=0.5, user-scalable=no';
            }
        }
    }

    var userAgent       = navigator.userAgent.toLowerCase();
    var androidversion  = 0;
                
    if( userAgent.indexOf("android") >= 0 )
    {
        androidversion = parseFloat(userAgent.slice(userAgent.indexOf("android")+8)); 
    }           
                
    var ios = (/iphone|ipad|ipod/i.test(userAgent));
    var android = (/Android/i.test(userAgent));
    var chrome  = (/chrome/i.test(userAgent));
    var iphone = (/iphone/i.test(userAgent));
    var ios_uiwebview = /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent) && !window.navigator.standalone;
    var iphone_uiwebview = (/(iphone).*AppleWebKit(?!.*Safari)/i.test(userAgent)) && !window.navigator.standalone;
                                     
    var iosVer = parseFloat(
        ('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0,''])[1])
        .replace('undefined', '3_2').replace('_', '.').replace('_', '')
        ) || -1;
                
    var ios7URLAppear = true;

    function setiFrameSize()
    {
        var iframeHeightOffset = 0;
        if(ios)
        {
            var fullscreen = window.navigator.standalone;
            var iosHeight = 0;
            if (iphone) {
                if (document.documentElement.clientWidth <= 568) {
                    
                    window.wrapper.width = document.documentElement.clientWidth * 2; //screen.width;
                    if (!fullscreen && document.documentElement.clientHeight != 320) {
                        if (iosVer >= 7) {
                            iosHeight = document.documentElement.clientHeight*2;
                        } else {
                            iosHeight = document.documentElement.clientHeight*2 + 120;
                        }
                    } 
                    else {
                        iosHeight = document.documentElement.clientHeight*2; 
                    }
                } else {
                    window.wrapper.width = document.documentElement.clientWidth; //screen.width;
                    if (!fullscreen  && document.documentElement.clientHeight != 640) {
                        iosHeight = document.documentElement.clientHeight + 120; 
                        if (iosVer >= 7) {
                            if (document.documentElement.clientHeight == 552) {
                                iosHeight = 640;
                            } else {
                                iosHeight = document.documentElement.clientHeight;      
                            }
                        }
                    }
                    else {
                        iosHeight = document.documentElement.clientHeight; 
                    }
                }
                window.wrapper.height = iosHeight+2;
            } else {
                window.wrapper.width  = document.documentElement.clientWidth; //screen.width;
                window.wrapper.height = document.documentElement.clientHeight; 
            }
            if (window.navigator.standalone && iosVer >= 8.3 && iosVer < 9 && navigator.userAgent.match(/iPhone/i)) {
                if (Number(window.wrapper.height) > Number(window.wrapper.width)) {
                    window.wrapper.height = Number(window.wrapper.height)/2;
                    window.wrapper.width  = Number(window.wrapper.width)/2;
                } else {
                    window.wrapper.width  = Number(window.wrapper.width);
                    window.wrapper.height = Number(window.wrapper.height);
                }
            }
        }
        else if(android) { 
            if(androidversion < 4.0) {
                window.wrapper.width  = screen.width;
                if(window.devicePixelRatio == 1) {
                    window.wrapper.height = window.outerHeight * 2;  //45 is the top bar height of android device;
                }
                else {
                    window.wrapper.height = window.outerHeight * 1.5  - window.screenTop * 1.5;
                }
            }
            else {
                window.wrapper.width  = document.documentElement.clientWidth; 
                if (chrome || androidversion > 4.1) {
                    //Tested on Samsung S4
                    window.wrapper.height = document.documentElement.clientHeight+1;
                } else {
                    window.wrapper.height = screen.height - window.screenTop;
                }
            }
        }
        else {
            window.wrapper.width  = document.documentElement.clientWidth; 
            window.wrapper.height = document.documentElement.clientHeight; 
            window.wrapper.height -= iframeHeightOffset; //Iframe height
        }
    }