// these params are for internal use only, do not attempt to access them from your game
// note that two variables from here are shared at the window level for your access
    window.params = {
        serverAddress:  gup('server'),                                                  // The IP address and port of the GDM game server
        playerName:     gup('pid'),                                                     // The player ID
        gameName:       gup('gname'),                                                   // Name of the game to load
        gamePath:       gup('gpath') || gameName + '-gdm',                              // Path to the game, if not included will match the game name
        delay:          Number(gup('delay')) || 0,                                      // All comms will be delayed by the amount of milliseconds provided here.
        languageCode:   gup('lcode') === "" ? "en_US" : gup('lcode'),                   // Tells the wrapper which language to request the game in
        cur:            gup('currency'),                                                // three-character currency code, defaults to GBP if not provided.
        home:           (gup('home') == 'true' || gup('home') == ''),                   // toggle the home button on and off, defaults to on
        cashier:        (gup('cashier') == 'true' || gup('cashier') == ''),             // toggle the cashier button on and off, defaults to on
        auto:           gup('auto') || '1',                                             // enable autoplay
        maxAuto:        (gup('autonum') === '') ? 100 : Number(gup('autonum')) || 0,    // set maximum autoplay spins, defaults to 100
        showRtp:        (gup('showRtp') == 'true' || gup('showRtp') == ''),             // show rtp in paytable, default to true
        wrapperLevel:   (gup('wrapperLevel') === '') ? 1 : Number(gup('wrapperLevel')), // set level of features
        quality:        gup('quality') || 'hq',                                         // this is set to hq for desktop and sq for mobile, at Betdigital we ignore this variable
        debug:          (gup('debug').toLowerCase() === 'true') ? true : false          // this sets the wrapper sending out console logs as it does it's thing
    }

// this boolean is used to toggle off any features that run the risk of breaking if you are running on a different environment
// if you want this up and running on a different environment to shefbox then enable this, though in the long run you may be
// better off disabling it and then seperately commenting out the chunks of code that don't run for you
    window.safeMode = true;

// a key for topbar fields, internal use only
    window.fields = {
        'TOTAL_BET': 0,
        'WINNINGS':  1,
        'BALANCE':   2
    };

// a key for params.wrapperLevel, internal use only
    window.wrapperLevels = {
        'BASIC': 0,
        'PYR':   1,
        'GCM':   2
    };

// This is in regards to the three Betdigital environments, of no use outside of varying js by location
    window.environments = {
        'DEV':  0,
        'DEMO': 1,
        'QA':   2
    };
    window.environment = environments.DEMO;
    if (document.location.pathname.indexOf('/dev/') > -1) {
        environment = environments.DEV;
    }else if (document.location.pathname.indexOf('/qa/') > -1) {
        environment = environments.QA;
    }

// do not attempt to access this from your game, internal variable to track the game state
    window.gameActive = false;
    window.errorOnRequest = false;
    window.holdOffOnComms = true;

// the following two variables are available in the wrapper at window level should you wish to access them
// you can rely on these under any wrapper environment
// note that window.languageCode will be overwritten in setLanguageCode(), the params version will always reflect the address bar
    window.languageCode = params.languageCode;
    window.quality = params.quality;


// we set the iphone shortcut icon to the image in the root of the game folder
    document.getElementById('icon').setAttribute('href', 'games/' + params.gamePath + '/apple-touch-icon-iphone4.png');

    $(document).ready(function () {
    // we refer to components.game but mobile.js needs a gameiframe variable
        gameiframe = document.getElementById("gameiframe");
    // ensure the screen is at the top of the document
        window.scrollTo(0,0);

    // set the sidebar arrow to not be draggable and not be a location from which the game can be scrolled
        var img = document.getElementById('sidebarArrow');
        img.draggable = false;
        img.ontouchmove = function (e) {
            e.preventDefault();
        };

    // a variable that defines the width (in vw) of the sidebar
    // this value can be overriden by the sidebar itself
        window.sideBarScale = 18;

    // attempt to load the lcmapping.json file from the root of the game folder
    // this is used to determine the games language
        $.ajax({
            url: 'games/' + params.gamePath + '/lcmapping.json',
            type: 'GET',
            success: function (data) {
                if (typeof data === 'string') { data = JSON.parse(data); }
                setLanguageCode(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                errorThrown = errorHelper(errorThrown, 'lcmapping.json');
                setTimeout(serverError.bind(this, errorThrown), params.delay);
            }
            // error: internalError.bind(window, 'Unable to load lcmapping.json')
        });

    /*
        Here we assign functions depending on the wrapper level
        Basic has no optional functions.
        Note that some GCM functions are identical to their PYR equivilents and some differ considerably.
    */
        if (params.wrapperLevel === wrapperLevels.PYR) {
            loadScript( 'gdm/js/pyr.js');
        }else if (params.wrapperLevel === wrapperLevels.GCM) {
            loadScript( 'gdm/js/gcm.js');
            createGCMProgressScreen();
        }else if (params.wrapperLevel === wrapperLevels.BASIC) {
            loadScript( 'gdm/js/basic.js');
        }

    // for internal use only, do not attempt to access any of the elements inside components or topbarText
        window.components = {
            errorbacking:   document.getElementById('errorBacking'), 
            game:           document.getElementById("gameiframe"),
            sidebar:        document.getElementById("sidebar"),
            topbar:         document.getElementById("topbar"),
            topbartab:      document.getElementById("topbarTab"),
            timebar:        document.getElementById("time"),
            sidebararrow:   document.getElementById("arrowContainer")
        };

    // disable pinch to zoom on the wrapper elements
        components.topbar.addEventListener('gesturestart', function (e) {
            e.preventDefault();
        });
        components.timebar.addEventListener('gesturestart', function (e) {
            e.preventDefault();
        });
        components.sidebararrow.addEventListener('gesturestart', function (e) {
            e.preventDefault();
        });
        components.sidebar.addEventListener('gesturestart', function (e) {
            e.preventDefault();
        });
        

        window.topbarText = {
            "bet":          document.getElementById("topbarBet"),
            "winnings":     document.getElementById("topbarWinnings"),
            "balance":      document.getElementById("topbarBalance")
        };


    // we define the sidebars width and the backings visible now so that they always have an inline style value
        components.sidebar.style.width = '0vw';
        components.errorbacking.style.display = 'none';

    // set the src of the game's iframe
        components.game.src = 'games/' + params.gamePath + '/index.htm';

    // this is a function within mobile.js that sets up the iframe size etc
        initIframe();
    
    // this function is called on a loop until it completes successfully. only useful for Betdigital games, please disregard
        window.globalInterval = window.setInterval(setGlobals, 250);

    // call once a second to tell the time bar to update
        window.setInterval(setTime, 1000);

    // set the topbar to mobile mode if mobile (different css)
        if (ios || android) { // variables created in mobile.js
            components.topbar.className = 'topbar mobile hidden';
        }else{
            components.topbartab.className = 'tab hidden';
        }

    // there are a few files that we like to check the existance of before making ajax requests for
    // we check for a sidebar.htm file within the game folder before defaulting to the one in the gdm folder and we check for
    // a {pid}.script file. This PHP file returns a JSON letting us know which of those files exist
        if (!safeMode) {
            $.ajax({
                url: 'gdm/fileCheck.php',
                type: 'POST',
                data: { gpath: params.gamePath, pid: params.playerName },
                success: function (data) {
                    try {
                        data = JSON.parse(data);
                    } catch (e) {
                        internalError('Unable to load fileCheck JSON');
                        data = {"complete": true, "game": true, "sidebar": false, "script": false};
                    }
                    gameInfoLoaded(data);
                }
            });
        }else{
    // in safemode we don't expect the PHP to run (its all domain specific) so we will enable comms immediately and skip
    // the sidebar and scripting stuff. We will also hide the menu and log buttons.
            window.holdOffOnComms = false;
            document.getElementById('topbarMenu').style.display = 'none';
        }

    });

// this is the function to call from the game when the home button is pressed
// this function will take the customer back to the lobby and away from your game
    function homeButtonPressed() {
        if (window.parent !== window){ // we're using the cross-origin wrapper
            window.parent.postMessage({key: 'goToUrl', value: 'home'}, '*');
        }else{
            window.open(location.href.substring(0, location.href.lastIndexOf("/wrapper_gdm.htm") + 1), '_self');
        }
        return;
    }

// this is the function to call when the customer presses the cashier button from the game
// window.parent.gameButtonPressed('CASHIER');
// this is the only button currently supported by gameButtonPressed at the moment
    function gameButtonPressed(buttonID) {
        if (buttonID == "CASHIER") {
            apiCall("MSG_POP_UP", "", "Cashier button pressed");
        }else{
            internalError('Invalid parameter "' + buttonID + '" sent to gameButtonPressed');
        }
    }

// this function is called once the game is finished loading assets and is ready to display
    function gameReady() {
    // bring up a full screen prompt if supported
        if (window.android && window.chrome) {
            showError(
                'Go fullscreen?', 
                'Yes', 
                'No', 
                function () {
                    fullscreenYes();
                    hideError();
                }, 
                hideError
            );
        }
    // set the window title to the game iframe title
        document.title = components.game.contentWindow.document.title;
        if (window.parent !== window) { // if cross-origin
            window.parent.postMessage({key: 'setTitle', value: document.title}, '*');
        }

    // this button is only available outside of the dev environment for Betdigital
    // outside of dev we hide our console logs but use this button to display them if needs be (for QA)
        if (environment !== environments.DEV && !safeMode) {
            document.getElementById('topbarLog').style.display = 'inline-block';
        }
        
    // set maximum autoplays (we always send 100 before the actual number because thats what the live wrapper does (i know, right?!))
        apiCall("SET_MAX_AUTOPLAYS", 100);

    // tell the game whether to enable autoplay with UKGC limitations
        apiCall("SET_AUTOPLAY_STOP_OPTIONS", Number(params.auto));

        if (params.maxAuto !== 100) {
            apiCall("SET_MAX_AUTOPLAYS", params.maxAuto);
        }

    // set the home button (frontend defaults to visible)
        if (!params.home) {
            apiCall("SHOW_HOME_BUTTON", false);
        }

    // set the cashier button (frontend defaults to hidden)
        if (params.cashier) {
            apiCall("SHOW_CASHIER_BUTTON", true);
        }

    // run the gameReady function specific to environment (see gcm.js, pyr.js and basic.js)
        environmentGameReady();

    // call the gameReady function in mobile.js
        nggGameReady();
    }

// this function returns the working game size, note that this will be smaller than any way of working it out from the game
// itself as the timebar etc sits over the top of the gameiframe and the game may be slightly covered
    function getSize() {
        var size = nggGetSize();
        size.w = String(size.w - components.sidebar.clientWidth);
        if (android || ios) {
            size.h = String(size.h - components.timebar.clientHeight); // topbar sits over game on mobile
        }else{
            size.h = String(size.h - components.timebar.clientHeight - topbar.clientHeight);
        }
        // if we have set the iframe height ot 0, send 0 to the game
        // this is a test, as the game should not be setting its canvas size to 0 (messes a lot up)
        if (components.game.style.height !== '') {
            size.h = 0;
        }
        return size;
    }

// this function is to be called when any request is to be made to the game server
    function sendMsgToServer(gameMsg) {

    // we stop comms until we confirm whether we have a script to work from
        if (window.holdOffOnComms) {
            if (params.debug) { console.log("%c Wrapper delaying comms ", "color: white; background: black"); }
            window.setTimeout(sendMsgToServer.bind(this, gameMsg), 500);
            return;
        }

    // if we are sending responses via a script (see parseScript()) then cancel the request and send a response
        if (typeof scriptResponses === 'object' && scriptResponses.length > 0) {
            window.setTimeout(processServerMsg.bind(window, scriptResponses.shift()), 200);
            if (params.debug) { console.log("%c " + scriptResponses.length + " script responses remaining ", "color: white; background: black"); }
            return;
        }else if (typeof scriptResponses === 'object' && scriptResponses.length === 0) {
            showError('The script has now ended',
                    'OK', 
                    'Reload', 
                    hideError.bind(this),
                    refreshPage.bind(this),
                    'white',
                    'blue'
            );
            return;
        }
    // if we're asking for messages to fail then fake a server error
        if (errorOnRequest) {
            if (params.debug) { console.log("%c Wrapper faking error ", "color: white; background: black"); }
            window.setTimeout(function () {
                components.game.contentWindow.processServerMsg('&MSGID=ERROR&EID=ERROR_COMMUNICATIONS&AB=0&SID=SESSION00000&');
            }.bind(this), 1000);
            return;
        }

    // our tomcat server has a bug in it where it can't accept pipe characters. We swap out | for %7C so that it will accept them, Alex's server code
    // knows to convert them back however that change isn't in other engines. We check the client isn't sending decoded pipes (will break on production
    // servers) and then we convert them so it runs on shefbox alright 
        if (gameMsg.indexOf('%7C') > -1) {
            internalError('game message includes a web-decoded pipe: ' + gameMsg);
            return;
        }
        gameMsg = gameMsg.replace(/\|/g, '%7C');

    // undeclared parameters break production environments but not our dev ones, check for them here and error if the request contains them
    // examples would be '?MSGID=BET&BPL=10&PRESET='
        if (containsUndefinedVariables(gameMsg)) {
            internalError('game message includes undefined variables: ' + gameMsg);
            return;
        }

    // this one isn't GDM specific, but # is used to seperate GSD values and so theres a good chance at some point someone will send a gameMsg
    // with a hash in it. This will break the comms as a hash is used to indicate a fragment identifier.
    // %23 should be used instead.
        if (gameMsg.indexOf('#') > -1) {
            internalError('game message includes a hash character: ' + gameMsg);
            return;
        }

    // hide the autoplay notification if ap=false sent in the message
        if (gameMsg.indexOf('AP=false') > -1) {
            document.getElementById('autoplayEnabled').style.display = 'none';
        }

    // if this is a bet, add spinforce
        if (gameMsg.indexOf('MSGID=BET') > -1 && typeof window.spinForce !== 'undefined') {
            gameMsg += '&PRESET='+ window.spinForce;
        }

    // if this is a freespin add freespinforce
        if (gameMsg.indexOf('MSGID=FREE_GAME') > -1 && typeof window.freeSpinForce !== 'undefined') {
            gameMsg += '&PRESET='+ window.freeSpinForce;
        }
    // store the request so we can log it later
    //NOTE: Timestamp is appended to the message for iOS 6 as it will cache identical requests.
        window.latestRequest = "?GN=" + params.gameName + "&PID=" + params.playerName + gameMsg + "&timeStamp=" + Date.now();

        var msg = "http://" + params.serverAddress + window.latestRequest;

        $.ajax({
            url: msg,
            type: "get",
            timeout: (8000 - params.delay),
            success: function (data) {
                setTimeout(function () {
                    window.processServerMsg(data);
                }, params.delay);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                errorThrown = errorHelper(errorThrown);
                setTimeout(serverError.bind(this, errorThrown), params.delay);
            }
        });
    }

    function refreshPage() {
        if (window.parent !== window) {
            window.parent.postMessage({key: 'refreshPage', value: ''}, '*');
        }else{
            location.reload();
        }
    }