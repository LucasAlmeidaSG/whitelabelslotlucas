// load a js file and add it to the dom (this ensures it exists in the source part of the dev tools)
    function loadScript(url, callback) {
        var script = document.createElement('script');
        script.onload = function () {
        };
        script.src = url;

        document.head.appendChild(script);
    }

// a function to make apiExt calls, they all get wrapped around a check that it exists and add an error if it does not
    function apiCall() {
        if (typeof components.game.contentWindow.apiExt === 'function') {
            if (params.debug) {
                var text = 'Wrapper sending apiExt ';
                for (var i = 0; i < arguments.length; i++) {
                    text += '' + arguments[i];
                    if (i < arguments.length - 1) { text += ', '; } // theres more to come
                }
                console.log("%c " + text + " ", "color: white; background: black");
            }
            return components.game.contentWindow.apiExt.apply(null, arguments);
        }else{
            internalError('Attempted to call apiExt with ' + arguments[0] + ' but it does not exist in the game window level');
        }
    }
// This function is specifically for Betdigital games, you may wish to stop this from being ran for non-Betdigital games
// this defines some window level variables that get at variables that exist within the iframe level, its just a shorthand
    function setGlobals() {
        try {
            window.g = components.game.contentWindow.game.state.states.game;
        } catch (e) { }
        try {  
            window.game = components.game.contentWindow.game;
        } catch (e) { }
        if (window.g && window.game) {
            window.clearInterval(window.globalInterval);
        }
    }

// function to change the time in the timebar
// is called on a setInterval from within document.ready
    function setTime() {
        var date = new Date();
        components.timebar.innerHTML =  String('0' + date.getHours()).slice(-2) + ':' +
                                        String('0' + date.getMinutes()).slice(-2) + ':' +
                                        String('0' + date.getSeconds()).slice(-2);
    }

// once lcmapping.json has been collected, we determine the language to set here
    function setLanguageCode(obj) {
        for (var lan in obj.info.OGS.mapping) {
            if (obj.info.OGS.mapping.hasOwnProperty(lan)) {
                if (obj.info.OGS.mapping[lan].key.toLowerCase() === languageCode.toLowerCase()) {
                    languageCode = obj.info.OGS.mapping[lan].value;
                    return;
                }
            }
        }
        internalError('unsupported language: \'' + languageCode + '\' so defaulting to \'' + obj.info.OGS.defaultLanguage.key + '\'');
        languageCode = obj.info.OGS.defaultLanguage.value;
    }

// internal use only, when a game is closed it tells the game to reactivate the buttons and that autoplay may continue
    function onCloseGame() {
        enableAllUI(true);
        // tempPauseAutoplay(false);
    }

// called once the fileCheck response has returned
// sets the src for the sidebar iframe and if there is a script for replaying comms, queues it up to be processed
    function gameInfoLoaded(data) {
        if (data.complete) {
            if (!data.game) {
                serverError(errorHelper('Not Found', 'index.htm'));
            }
            if (data.sidebar) {
                components.sidebar.src = 'games/' + params.gamePath + '/sidebar.htm';
            }else{
                components.sidebar.src = 'gdm/sidebar.htm';
            }
        // make the arrow visible
            components.sidebararrow.className = 'arrow';

            if (data.script) {
                $.ajax({
                    url: 'games/' + params.gamePath + '/' + params.playerName + '.script',
                    type: 'GET',
                    success: function (data) {
                        parseScript(data);
                    },
                    error: internalError.bind(window, 'Unable to load the player script file')
                });
                return; // return now so you don't set comms to false
            }
        }
        window.holdOffOnComms = false;
    }

// our wrapper topbar displays a yellow icon in the top right when certain errors occur
// this can be used to flag non-fatal things that go wrong, or sometimes just to let
// qa know there is something to report

// an array of internal issues that the wrapper may want to alert the dev to
    window.mostRecentError = [];

// adds an issue to the list
    function internalError(msg) {
        console.error(msg);
        window.mostRecentError.push(msg);
        document.getElementById('errorThrown').style.display = 'inline-block';
    }

// pops the most resent error from the list and shows an alert with the text
    function displayError() {
        if (window.mostRecentError.length > 0) {
            alert(window.mostRecentError.pop());
            if (window.mostRecentError.length === 0) {
                document.getElementById('errorThrown').style.display = 'none';
            }
        }
    }

// my version of gup from mobile.js
    function getParams(params) {
        params = params.substring(1).split('&');
        var obj = {};
        for (var i = 0; i < params.length; i++) {
            params[i] = params[i].split('=');
            if (params[i].length > 1) {
                obj[params[i][0]] = params[i].splice(1)[0];
            }
        }
        return obj;
    }

// Although the dev system can handle 'msg=BET&BPL=20&PRESET=' the production system can't
// So we ensure there are no definitions in the server message that would contain one
    function containsUndefinedVariables(params) {
        var obj = getParams(params);

        for (var key in obj) {
             if (obj.hasOwnProperty(key) && typeof obj[key].length === 'number' && obj[key].length === 0) {
                return true;
             }
        }
        return false;
    }

// this wrapper has generated an error contacting the server
// if we have more information about the error it is included here
    function serverError(optionalMsg) {
        if (typeof delegatedErrorHandling === 'function') {
            showRestartError(optionalMsg);
        }else{
            if (optionalMsg) { internalError(optionalMsg); }
            apiCall('SERVER_ERROR', window.refreshPage.bind(this));
        }
    }

// we store all requests and responses in a db
// unlike the others, we haven't made this an absolute url as you can't access the data without internal shefbox access
    function logRequestResponsePair(request, response) {
        if (!safeMode) {
            $.ajax({
                url: '../tools/logRequest.php',
                data: { pid: params.playerName, request: request, response: response},
                type: "POST",
                timeout: 8000
            });
        }
    }

// build a script array for replaying past games
    function parseScript(data) {
        window.scriptResponses = data.trim().split('\n');
        if (params.debug) { console.log("%c Script Responses: ", "color: white; background: black"); console.log(scriptResponses); }
        document.getElementById('scriptTag').style.display = 'inline-block';
        window.holdOffOnComms = false;
    }

// deal with a server response
    function processServerMsg(data) {
    // don't log a response to the db if we aren't running on shefbox or if we're playing through scripted comms
        if (typeof scriptResponses === 'undefined' && data && data.indexOf && (params.serverAddress.indexOf('shefbox') > -1 || params.serverAddress.indexOf('109.228.54.244') > -1)) {
            logRequestResponsePair(latestRequest, data);
        } 

    // emulate reducing the bet if we are on PYR
    // note that in places we have found this to cause issues, for example if a game requires balance to be reduced by more than stake
    // (if buying into the bonus game) we do not yet have enough data to determine if this will be a problem on live PYR and so this
    // code is still here
        if (params.wrapperLevel === wrapperLevels.PYR && data.indexOf('MSGID=BET') > -1 && data.indexOf('IFR=1') === -1) {
            deductStake();
        }

        if (data.indexOf('&MSGID=ERROR&EID=ERROR_INSUFFICIENTFUNDS') > -1) {
            internalError('A bet request was made even though there wasn\'t enough balance to cover this.');
        }
    // we iterate through the comms, if we find a CUR element, we replace it with the value from currencies.js
    // if we get the AB value, we store it to emulate PYR balance topbar
    // if FRMSG is not present, we force a value in, this is because Camilla mostly doesn't include an FRMSG
        var freeRoundsMessagePresent = false;
        var pairs = data.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split("=");
            switch (pair[0]) {
                case "CUR":
                    if (params.cur !== "") {
                        var cur = getCurrencyInfo(params.cur);
                        pairs[i] = pair[0] + '=' + decodeURI(cur);
                    }
                    break;
                case "AB":
                    balance = Number(pair[1]);
                    break;
                case "FRMSG":
                    freeRoundsMessagePresent = true;
                    break;
            }
        }
        data = pairs.join('&');

    // fake an FRMSG so we can test they work
        if (freeRoundsMessagePresent === false) {
            data = data+'FRMSG='+encodeURIComponent('HI <a href="http://shenandoahsentinel.weebly.com/news/mahanoy-city-police-searching-for-wanted-person">LEWIS</a>!').replace('%20', '+') + '&';
        }

    // alert our sidebar to a completed spin response so it can disable any forces that are set to cancel
        if (typeof components.sidebar.contentWindow.cancelSets === 'function') {
            components.sidebar.contentWindow.cancelSets((data.indexOf('FREE_GAME') > -1)); // sends true for a freespin
        }

    // pass the game the server message
        components.game.contentWindow.processServerMsg(data);

    }

    function showSideBar() {
        components.game.origWidth = components.game.style.width;
        if (android || ios) {
            components.sidebar.style.width = '100vw';
            components.game.style.width = '0vw';
        } else {
            components.sidebar.style.width = window.sideBarScale + 'vw';
            components.game.style.width = (100 - window.sideBarScale) + 'vw';
            document.getElementById('arrowContainer').style.left = (window.sideBarScale + 1) + 'vw';
        }

        document.getElementById('sidebarArrow').style.transform = 'scale(1, 1)';
    }

    function hideSideBar() {
        components.sidebar.style.width = '0vw';
        components.game.style.width = components.game.origWidth;
        setiFrameSize();
        document.getElementById('sidebarArrow').style.transform = 'scale(-1, 1)';
        document.getElementById('arrowContainer').style.left = '1vw';
    }

    function toggleSideBar() {
        if (!components.sidebar) { return; }
        if (components.sidebar.style.width === '0vw') {
            showSideBar();
        } else {
            hideSideBar();
        }
    }

// get the currency string from currencies.js for the appropriate currency string, for example GBP or EUR
    function getCurrencyInfo(cur) {
        if (cur && typeof cur === 'string') {
            if (typeof currencyData[cur.toUpperCase()] === 'string') {
                return currencyData[cur.toUpperCase()];
            }
        }
        return cur;
    }

// toggle whether the lock icon is lit or not
// this is to signify round 0 or round 1 being set
    function toggleLock () {
        var lock = document.getElementById('topbarLock');
        if (lock.style.display === 'inline-block') {
            lock.className = (gameActive) ? 'lock button' : 'unlock button';
        }
    }

// a function that hides the loading progress info and sets up the topbar to display the three elements
    function  progressComplete () {
        hideProgressData();
        document.getElementById('topbarInfoHousing').style.display = 'flex';
        updateField(fields.WINNINGS, 0);
    }

// subfunction used above but called directly by the showError function (if in GCM)
    function hideProgressData () {
        document.getElementById('topbarProgress').style.display = 'none';
        if (document.getElementById('progressParent')) {
            document.getElementById('progressParent').style.display = 'none';
        }
    }

    function fullscreenYes() {
    // we add a class to the tobar so we can scale down some larger elements when fullscreen
    // we can't comfortably remove it again as far as i'm aware
        components.topbar.classList.add("fullscreen");
        var goFullscreen = document.documentElement.requestFullscreen ||
            document.documentElement.mozRequestFullScreen ||
            document.documentElement.webkitRequestFullScreen ||
            document.documentElement.msRequestFullscreen;

        var exitFullscreen = document.exitFullscreen ||
            document.mozCancelFullScreen ||
            document.webkitExitFullscreen ||
            document.msExitFullscreen;

        if (!document.fullscreenElement &&
            !document.mozFullScreenElement &&
            !document.webkitFullscreenElement &&
            !document.msFullscreenElement) {

            goFullscreen.call(document.documentElement);
        } else {
            exitFullscreen.call(document);
        }
    }

// our internal function to change values in the topbar
    function updateField(field, value) {
        switch (field) {
            case fields.TOTAL_BET: 
                field = topbarText.bet;
            break;
            case fields.WINNINGS: 
                field = topbarText.winnings;
            break;
            case fields.BALANCE:
                field = topbarText.balance;
            // overwrite the value with the requested value on PYR
                if (params.wrapperLevel === wrapperLevels.PYR) {
                    value = balance || 0;
                }
            break;
        }
        field.innerHTML = (Number(value) / 100).toFixed(2);
    }

// internal use only, called by the topbar volume button on click
    function toggleVolume () {
    // send the appropriate apiExt
        if (document.getElementById('topbarVolume').style.display === 'inline-block') {
            if (document.getElementById('topbarVolume').getElementsByTagName('img')[0].src.indexOf('on.png') > -1) {
                apiCall("SET_MUTE", true);
                document.getElementById('topbarVolume').getElementsByTagName('img')[0].src = 'gdm/img/vol-off.png';
            }else{
                apiCall("SET_MUTE", false);
                document.getElementById('topbarVolume').getElementsByTagName('img')[0].src = 'gdm/img/vol-on.png';
            }
        }
    }

// called when the menu button is clicked in the topbar
    function goToEditPage() {
        var url = encodeURI(window.location.search);
        if (window.parent !== window){
            window.parent.postMessage({key: 'goToUrl', value: 'gdm/edit.php' + url}, '*');
        }else{
            window.open('gdm/edit.php' + url, '_self');
        }
    }

// this is called from the sidebar to reset an account
// game parameter is optional, only required if the game name is different from the request to tomcat
// (this has only meant multihand videopoker so far)
    function resetAccount(game) {
        setBalance(1000);
        window.setTimeout(function () {
            $.ajax({
                url: 'http://shefbox.co.uk/tools/deleteState.php',
                type: 'POST',
                data: { game: game || params.gameName, account: params.playerName },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.complete) {
                        refreshPage();
                    } else {
                        internalError(data.error);
                    }
                }
            });
        }, 800);
    }

// the function called by the sidebar to set the balance outside of the game
    function setBalanceTo(num) {
        num = Number(num);
        if (isNaN(num)) {
            alert('requested balance is not a number');
            return;
        }
        setBalance(num);
        window.fakeBalanceUpdate(num * 100);
        apiCall("SET_BALANCE", num * 100);
    }

// contacts our db and changes the balance of the requested pid
    function setBalance(number) {
        $.ajax({
            url: 'http://shefbox.co.uk/tools/setBalance.php',
            data: { balance: number, player_id: params.playerName },
            type: "POST",
            timeout: 8000,
            success: function (data) {
                data = JSON.parse(data);
                if (data.complete) {
                    refreshPage();
                } else {
                    internalError(data.error);
                }
            }
        });
    }

// used when the sidebar updates the balance, we set the topbar balance (if available) to the requested amount
    function fakeBalanceUpdate (value) {
        topbarText.balance.innerHTML = (Number(value) / 100).toFixed(2);
    }

// our error message handler, used where delegatedErrorHandling is called
    function showError (errorMessage, bttn1Text, bttn2Text, bttn1Callback, bttn2Callback, bttn1Color, bttn2Color) {
        if (params.wrapperLevel === wrapperLevels.GCM) {
            hideProgressData();
        }
        document.getElementById('errorBacking').style.display = 'inline-block';
        document.getElementById('errorBoxText').textContent = errorMessage;
        if (bttn1Color) { bttn1Color = bttn1Color.toLowerCase().trim(); }
        if (bttn2Color) { bttn2Color = bttn2Color.toLowerCase().trim(); }
        if (bttn1Text == '' && bttn2Text == '') { console.error('error \'' + errorMessage + '\' appeared with no way to progress'); }
        if (bttn1Text == '' || bttn2Text == '') {
            document.getElementById('errorBoxFlex').style.justifyContent = 'center';
        } else {
            document.getElementById('errorBoxFlex').style.justifyContent = 'space-between';
        }
        if (typeof bttn1Text === 'string' && bttn1Text !== '') {
            document.getElementById('errorBoxButtonLeft').style.display = 'inline-block';
            document.getElementById('errorBoxButtonLeft').textContent = bttn1Text;
            if (bttn1Callback) {
                window.buttonLeftCallback = bttn1Callback;
            } else {
                window.buttonLeftCallback = function () { };
            }

            if (bttn1Color === 'red' || bttn1Color === 'green' || bttn1Color === 'blue' || bttn1Color === 'black') {
                document.getElementById('errorBoxButtonLeft').className = 'button ' + bttn1Color;
            } else {
                document.getElementById('errorBoxButtonLeft').className = 'button white';
            }
        } else {
            document.getElementById('errorBoxButtonLeft').style.display = 'none';
        }
        if (typeof bttn2Text === 'string' && bttn2Text !== '') {
            document.getElementById('errorBoxButtonRight').style.display = 'inline-block';
            document.getElementById('errorBoxButtonRight').textContent = bttn2Text;
            if (bttn2Callback) {
                window.buttonRightCallback = bttn2Callback;
            } else {
                window.buttonRightCallback = function () { };
            }
            if (bttn2Color === 'red' || bttn2Color === 'green' || bttn2Color === 'blue' || bttn2Color === 'black') {
                document.getElementById('errorBoxButtonRight').className = 'button ' + bttn2Color;
            } else {
                document.getElementById('errorBoxButtonRight').className = 'button white';
            }
        } else {
            document.getElementById('errorBoxButtonRight').style.display = 'none';
        }
    }

    function buttonClicked (button) {
        switch (button) {
            case 'left':
                buttonLeftCallback();
                break;
            case 'right':
                buttonRightCallback();
                break;
            default:
                console.error('not expecting ' + button + ' in buttonClicked function');
        }
    }

    function hideError () {
        document.getElementById('errorBacking').style.display = 'none';
    }

    function showRestartError (optionalMsg) {
        showError(
            optionalMsg || 'We\'re sorry, this game has been closed due to an error',
            'Okay', 
            '', 
            refreshPage.bind(window),
            function () {},
            'blue'
        );
    }

    function errorHelper (error, file) {
        switch (error) {
            case 'Internal Server Error':
                return 'Internal Server Error: please check the game name or alert the engine developer';
            case 'Not Found':
                return 'Unable to find ' + file + ': please check that the file exists in ./games/' + params.gamePath + '/';
        }
    }

// our internal function to handle calls to the game (used by the sidebar)
    function tempPauseAutoplay (paused) {
        apiCall("TEMP_PAUSE_AUTOPLAY", Boolean(paused));
    }

// our internal function to handle calls to the game (used by the sidebar)
    function enableAllUI (enabled) {
        apiCall("ENABLE_ALL_UI", Boolean(enabled));
    }

// this is in internal rather than gcm.js because of the order things are loaded
    function createGCMProgressScreen () {
        var topbar = document.getElementById('topbarProgress');
        topbar.parentNode.removeChild(topbar);
        var d = document.createElement('div');
        d.id = 'progressParent';
        d.className = 'progressGCM';
        document.body.appendChild(d);
        var i = document.createElement('img');
        i.src = 'gdm/img/betdigital.png';
        d.appendChild(i);
        var t = document.createElement('p');
        t.id = 'topbarProgress';
        t.innerText = '0%';
        d.appendChild(t);
    // as per GCM, in desktop, we set the game iframe height to 0 otherwise we just cover the game iframe with a div
        if (!ios && !android) {
            gameiframe.style.height = '0';
        } else {
            var d = document.getElementById('progressParent');
            d.className = 'progressGCM black';
        }
    }

// this is called from the topbar and shows the console in environments where the console is disabled
    function logButton() {
        if (document.getElementById('logScreen')) {
            clearLogScreen();
        }else{
            generateOutputLogs();
        }
    }

    function generateOutputLogs() {
        if (!document.getElementById('logScreen')) {
            let log = components.game.contentWindow.BDLog;
            if (log) {
                let el = document.createElement('div');
                el.id = 'logScreen';
                el.className = 'logScreen';
                for (let i = 0; i < log.length; i++) {
                    let inner = document.createElement('div');
                    if (log[i].length === 1 && typeof log[i][0] === 'string') {
                        inner.innerHTML += '&#9679; ' +log[i];
                    }else if (log[i].length > 1 && typeof log[i][0] === 'string' && log[i][0].indexOf('%c') > -1) {
                        inner.innerHTML = '&#9679; ';
                        let str = log[i][0].split('%c');
                        for (let j = 1; j < str.length; j++) {
                            let fragment = document.createElement('span');
                            fragment.style = log[i][j];
                            fragment.innerText += str[j];
                            inner.appendChild(fragment);
                        }
                    }else if (log[i].length > 0 && typeof log[i][0] === 'object') {
                        let a = document.createElement('a');
                        a.id = 'obj' + i;

                        a.onclick = function (json) {
                            var obj = document.getElementById('obj'+i);
                            if (obj.innerText == '[object Object]') {
                                obj.innerHTML = json;
                                obj.parentNode.getElementsByTagName('span')[0].innerHTML = '&#9660; ';
                            }else{
                                obj.innerText = '[object Object]';
                                obj.parentNode.getElementsByTagName('span')[0].innerHTML = '&#9654; ';
                            }
                        }.bind(window, JSONOutput(log[i][0]));

                        a.innerText = '[object Object]';
                        inner.innerHTML = '<span>&#9654; </span>';
                        inner.appendChild(a);
                    }
                    el.appendChild(inner);
                }
                document.body.appendChild(el);
            }
        }
    }

    function clearLogScreen() {
        document.body.removeChild(document.getElementById('logScreen'));
    }

    function JSONOutput (json) {
        window.cache = [];
        var output = JSON.stringify(json, function(key, value) {
            if (typeof value === 'object' && value !== null) {
                if (window.cache.indexOf(value) !== -1) {
                    // Circular reference found, discard key
                    return;
                }
                // Store value in our collection
                window.cache.push(value);
            }
            return value;
        }, 2);
        window.cache = null; // Enable garbage collection
        return output.replace(/\n/g, '<br>').replace(/\s/g, '&nbsp;');
    }

// used on mobile, to change the css of the topbar
    function toggleTopbar() {
        if (topbar.className.indexOf('hidden') > -1) {
            topbar.classList.remove('hidden');
            document.getElementById('topbarTab').classList.add('active');
        }else{
            topbar.classList.add('hidden');
            document.getElementById('topbarTab').classList.remove('active');
        }
    }

// called from the sidebar to test the pause_autoplay apiExt
    function stopAutoplay() {
        apiCall("PAUSE_AUTOPLAY", true);
    }
