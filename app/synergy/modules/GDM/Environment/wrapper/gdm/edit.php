<!doctype html>
<html>

<?php

    $iOS = stripos($_SERVER['HTTP_USER_AGENT'],"iPod") || stripos($_SERVER['HTTP_USER_AGENT'],"iPhone") || stripos($_SERVER['HTTP_USER_AGENT'],"iPad");

    $iExplorer = strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.') !== false;

// ios does not support data lists so we construct them as dropdowns
    function createDataList($name, $arr) { 
        global $iOS, $iExplorer;
        if ($iOS || $iExplorer) {
            echo "<select name='{$name}' id='{$name}'>";
            foreach ($arr as $key => $value) {
                echo "<option value='{$key}'>{$value}</option>";
            }
            echo "</select>";
        }else{
            echo "<input name='{$name}' id='{$name}' list='{$name}List'/><br>";
            echo "<datalist id='{$name}List'>";
            foreach ($arr as $key => $value) {
                echo "<option value='{$key}'>{$value}</option>";
            }
            echo "</datalist>";
        }
    }
?>

<head>
    <meta charset="UTF-8" />
    <title>Game Loader</title>

    <script>
        window.onload = function () {

            // define defaults
            window.data = {
                server: {
                    value: 'shefbox.co.uk/tomcat/',
                    input: document.getElementById('server')},
                pid: {
                    value: 'betdigital1',
                    input: document.getElementById('pid')},
                gname: {
                    value: 'examplegame',
                    input: document.getElementById('gname')},
                gpath: {
                    value: '',
                    input: document.getElementById('gpath')},
                delay: {
                    value: '0',
                    input: document.getElementById('delay')},
                lcode: {
                    value: 'en_US',
                    input: document.getElementById('lcode')},
                currency: {
                    value: 'GBP',
                    input: document.getElementById('currency')},
                home: {
                    value: 'true',
                    input: document.getElementById('home')},
                cashier: {
                    value: 'true',
                    input: document.getElementById('cashier')},
                auto: {
                    value: '2',
                    input: document.getElementById('auto')},
                autonum: {
                    value: '100',
                    input: document.getElementById('autonum')},
                showRtp: {
                    value: 'true',
                    input: document.getElementById('showRtp')},
                wrapperLevel: {
                    value: '1',
                    input: document.getElementById('wrapperLevel')},
                debug: {
                    value: 'false',
                    input: document.getElementById('debug')},               

            };  

        // override defaults with data sent in the url
            var str = window.location.search.substring(1);
            str = str.split('&');
            for (var i = 0; i < str.length; i++) {
                var s = str[i].split('=');
                if (typeof window.data[s[0]] !== 'undefined') {
                    window.data[s[0]].value = s[1];
                }
            }

        // print the object for convenience
            console.log(window.data);

        // set the values of the inputs to the current values
            for(var index in window.data) { 
                if (window.data.hasOwnProperty(index)) {
                    var item = window.data[index].input;
                    if (item.type === 'checkbox') {
                        item.checked = (window.data[index].value === 'true');
                    }else if (item.type === 'number') {
                        item.value = Number(window.data[index].value);
                    }else if (index === 'currency' || index === 'lcode') {
                        if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
                            item.value = window.data[index].value;
                        }else if (item.tagName === 'SELECT') {
                            for (var i = 0; i < item.options.length; i++) {
                                if (item.options[i].value.toUpperCase().indexOf(window.data[index].value.toUpperCase()) > -1) {
                                    item.value = item.options[i].value;
                                    break;
                                }
                            }
                        }else{
                            for (var i = 0; i < item.list.options.length; i++) {
                                if (item.list.options[i].value.toUpperCase().indexOf(window.data[index].value.toUpperCase()) > -1) {
                                    item.value = item.list.options[i].value;
                                    break;
                                }
                            }
                        }

                    }else{
                        item.value = window.data[index].value;
                    }
                }
            }
        }

        function submit () {
        // set all of the current values to window.data so it is up to date
            for(var index in window.data) { 
                if (window.data.hasOwnProperty(index)) {
                    var item = window.data[index].input;
                    if (item.type === 'checkbox') {
                        window.data[index].value = item.checked;
                    }else if (index === 'currency' || index === 'lcode') {
                        var line = item.value.replace(/.*\((.*)\).*/g, '$1');
                        if (line == '') {
                            line = item.value.trim();
                        }
                        window.data[index].value = line;
                    }else{
                        window.data[index].value = item.value;
                    }
                }
            }

            var url = window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf('gdm/edit.php'));
            url += 'wrapper.htm?server=' + window.data.server.value;
            url += '&pid=' + window.data.pid.value;
            url += '&gname=' + window.data.gname.value;
            if (window.data.gpath) { url += '&gpath=' + window.data.gpath.value; }
            if (window.data.delay) { url += '&delay=' + window.data.delay.value; }
            url += '&lcode=' + window.data.lcode.value;
            url += '&currency=' + window.data.currency.value;
            url += '&home=' + window.data.home.value;
            url += '&cashier=' + window.data.cashier.value;
            url += '&auto=' + window.data.auto.value;
            url += '&autonum=' + window.data.autonum.value;
            url += '&showRtp=' + window.data.showRtp.value;
            url += '&wrapperLevel=' + window.data.wrapperLevel.value;
            url += '&debug=' + window.data.debug.value;

            window.open(url, '_self');
        }

    </script>

    <style>
        html {

        }
        body {
            display: flex;
            justify-content: center;
            background: #ddd;
            font-family: sans-serif;
            margin: 0;
            overflow: auto;
            color: white;
            font-size: 1.2em;
        }

        h1 {
            margin-bottom: 0;
        }

        div {

        }

        input {
            font-size: 1.4em;
        }

        select {
            font-size: 1.4em;
        }

        label {
            font-size: 1.8em;
        }
        .notes {
            color: #888;
            max-width: 70vw;
        }
        .container {
            background: #000;
            background-size: 100%;
            min-width: 75vw;
/*            max-width: 1440px;*/
            padding: 50px 100px;
        }

        .container > div {
            padding-bottom: 1em;
        }

        [type=checkbox] {
            width: 40px;
            height: 40px;
            background: #333;

            border-radius: 50px;
            position: relative;
        }

        [type=button] {
            position:fixed;
            right:5%;
            top:5%;
            background: #022fb5;
            color: #fff;
            padding: 15px;
            border-radius: 5px;
            font-weight: bold;
        }

    </style>

</head>

<body>
    <div class='container'>
        <div>
            <h1>Custom Game Rules</h1>

        </div>

        <div>
            <input type='button' onclick='submit()' value='Load Game'/>
        </div>

        <div>
            <label for='server'>Server Location</label><br>
            <?php
                $serverList = array(
                    "camillagdm.nextgengaming.com/gdm-release/GDMServer" =>   "camillagdm.nextgengaming.com/gdm-release/GDMServer", 
                    "camillagdm.nextgengaming.com/gdm-stateless/GDMServer" => "camillagdm.nextgengaming.com/gdm-stateless/GDMServer", 
                    "camillagdm.nextgengaming.com/GDM/GDMServer" =>           "camillagdm.nextgengaming.com/GDM/GDMServer", 
                    "shefbox.co.uk/tomcat/" =>                                "shefbox.co.uk/tomcat/"
                );
                createDataList('server', $serverList);
            ?>

            <div class='notes'>The server that the game will communicate with, usually this should point to shefbox.co.uk/tomcat/ however you may want to change this to test Free Spins etc</div>
        </div>

        <div>
            <label for='wrapperLevel'>Wrapper functionality level</label><br>
            <select name='wrapperLevel' id='wrapperLevel'>
                <option value="0">Basic (0)</option>
                <option value="1" selected>PYR (1)</option>
                <option value="2" selected>GCM4 (2)</option>
            </select><br>

            <div class='notes'>Determines the level of optional functionality wihtin the wrapper. Under basic, a lot of optional functionality is missing, PYR fudges a lot of information to have a working topbar and GCM4 relies on the game letting the topbar know when to change information in the topbar</div>
        </div>

        <div>
            <label for='pid'>Player ID</label><br>
            <input name='pid' id='pid'/><br>
            <div class='notes'>The id for the player, this can be any value and if the user account does not exist it will be created with 1000.00 balance</div>
        </div>

        <div>
            <label for='gname'>Game Name</label><br>
            <input name='gname' id='gname' type='text'/><br>

            <div class='notes'>The name of the game, this will be used for server requests and must match the name of the game there. If no game path is provided, it will use this valu for that also.</div>
        </div>

        <div>
            <label for='gpath'>Game Path</label><br>

            <?php
                $gPaths = include('gamelist.php');
                createDataList('gpath', $gPaths);
            ?>
            <div class='notes'>This can be left blank unless you want to load the game from an alternate path. Not required for testing, but useful for devs.</div>
        </div>

        <div>
            <label for='delay'>Delay</label><br>
            <input name='delay' id='delay' type='number'/><br>

            <div class='notes'>Delay can be used to add time before any communication is received to emulate a shaky connection. Time is in milliseconds. A value of 8000 or over should trigger all comms to fail. It is not recommended that you use this if able, the Chrome Dev tools are a much better </div>
        </div>

        <div>
            <label for='lcode'>Language</label><br>

            <?php
                $lans = include('lanlist.php');
                createDataList('lcode', $lans);
            ?>
            <div class='notes'>The language the game will load in, note that this list is for each language a game may be translated to and to check specific game support.</div>
        </div>

        <div>
            <label for='currency'>Currency</label><br>
            <input name='currency' id='currency' list='curr'/><br>
            
            <?php
                include 'currlist.php';
           ?>
            <div class='notes'>Choose the currency the game will display in.</div>
        </div>

        <div>
            <label for='home'>Home Button</label><br>
            <input name='home' id='home' type='checkbox'/><br>

            <div class='notes'>Determines whether the home button is available from the menu in game.</div>
        </div>

        <div>
            <label for='cashier'>Cashier Button</label><br>
            <input name='cashier' id='cashier' type='checkbox'/><br>

            <div class='notes'>Determines whether the cashier button is available from the menu in game.</div>
        </div>

        <div>
            <label for='auto'>Autoplay Type</label><br>
            <select name='auto' id='auto'>
                <option value="0">Non-UKGC (0)</option>
                <option value="1">Debug (1)</option>
                <option value="2" selected>UKGC (2)</option>
            </select><br>

            <div class='notes'>Determines whether autoplay should enable UKGC restrictions</div>
        </div>

        <div>
            <label for='autonum'>Maximum number of autoplays</label><br>
            <input name='autonum' id='autonum' type='number'/><br>

            <div class='notes'>When using autoplay, the maximum number of autoplays available at once should not exceed this number.</div>
        </div>

        <div>
            <label for='showRtp'>Show RTP information in the paytable</label><br>
            <input name='showRtp' id='showRtp' type='checkbox'/><br>

            <div class='notes'>Tells the game whether to display RTP information in the paytable.</div>
        </div>

        <div>
            <label for='debug'>Enable Debug Wrapper Info</label><br>
            <input name='debug' id='debug' type='checkbox'/><br>

            <div class='notes'>If enabled, the wrapper will output console logs for various things</div>
        </div>
    </div>
</body>

</html>