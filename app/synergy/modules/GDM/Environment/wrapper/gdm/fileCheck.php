<?php
    
    if (isset($_POST['gpath'])) {
        $path = $_POST['gpath'];
    }else{
        exit('{"complete": false, "error": "No Game Path"}');
    }

    if (isset($_POST['pid'])) {
        $pid = $_POST['pid'];
    }else{
        exit('{"complete": false, "error": "No Game Path"}');
    }

    $game = (file_exists('../games/'.$path.'/index.htm'));
    $sidebar = (file_exists('../games/'.$path.'/sidebar.htm'));
    $script = (file_exists('../games/'.$path.'/'.$pid.'.script'));

    echo '{"complete": true, "game": '.($game === true ? 'true':'false').', "sidebar": '.($sidebar === true ? 'true':'false').', "script": '.($script === true ? 'true':'false').'}';
?>