<?php
    $list = [];
    if ($handle = opendir('../games')) {
        chdir('../games');
        while (false !== ($entry = readdir($handle))) {
            if ($entry != '.' && $entry != '..' && is_dir($entry)) {
                $list[$entry] = $entry;
            }   
        }
        closedir($handle);
    }
    return $list;
?>
