declare namespace GCM
{
    export type JackpotInstanceID = string;

    /** Represents a jackpot query. */
    export interface Jackpot
    {
        /** Fixed jackpot name. */
        id: string;
        /** Jackpot instance UUID. Changes when jackpot drops. */
        instance: JackpotInstanceID;
        /** Current jackpot balance amount in the user's currency. */
        balanceAmountInRequestedCurrency: number;
        /** Current jackpot balance amount in the jackpot's currency. */
        balanceAmountInJackpotCurrency: number;
        /** User currency code. */
        requestedCurrency: string;
        /** Jackpot currency code. */
        jackpotCurrency: string;
    }

    export interface JackpotQueryError
    {
        code: string;
        message: string;
    }

    export interface JackpotQueryFailure
    {
        errors: JackpotQueryError[];
    }
}