type Unknown = boolean | number | string | object | ((...args: Unknown[]) => Unknown) | undefined;
type Action<T> = (arg: T) => void;

declare namespace OGS
{
    export namespace GDM
    {
        /** Represents a GDM compatible game. */
        export interface IGame
        {
            readonly apiExt: IGame.APIExtHandler;
            readonly changeOrientation: IGame.OrientationChangeHandler;
            readonly processServerMsg: IGame.ServerMessageHandler;

            readonly jackpotBalancesUpdate?: IGame.JackpotUpdateHandler;
            readonly onJackpotsUpdateFailed?: IGame.JackpotUpdateErrorHandler;

            readonly game?: IGame.GameSettings;
        }

        export namespace IGame
        {
            export interface GameSettings
            {
                /** If the game has a help page that requires the page to scroll, then the game should have a “game” object with a “showRules” property, and the value of “showRules” should indicate if the game is currently showing the help page.  When this value is true it lets the wrapper know not to show the “swipe up” overlay on iOS because otherwise it would interfere with scrolling the help page. */
                readonly showRules: boolean;
            }

            export type APIExtCall<TArg, TResult = void> = { arg: TArg; result: TResult; };

            export interface APIExtCalls
            {
                "SET_BALANCE": APIExtCall<number>;

                "SHOW_HOME_BUTTON": APIExtCall<boolean>;
                "SHOW_CASHIER_BUTTON": APIExtCall<boolean>;
                "SHOW_RTP": APIExtCall<boolean>;
                "ENABLE_ALL_UI": APIExtCall<boolean>;
                "SET_MUTE": APIExtCall<boolean>;
                "SHOW_GAME_INFO": APIExtCall<boolean>;
                "SHOW_SETTINGS": APIExtCall<boolean>;

                // Autoplay
                "SET_AUTOPLAY_STOP_OPTIONS": APIExtCall<AutoplayStopOptions>;
                "SET_MAX_AUTOPLAYS": APIExtCall<number>;
                "PAUSE_AUTOPLAY": APIExtCall<boolean>;
                "TEMP_PAUSE_AUTOPLAY": APIExtCall<boolean>;

                "GET_ROUND_TOTAL_WIN": APIExtCall<void, number>;
                "GET_ERROR_MESSAGE": APIExtCall<void, string>;

            }

            export type APIExtHandler = <T extends keyof APIExtCalls>(key: T, arg: APIExtCalls[T]["arg"]) => APIExtCalls[T]["result"];

            export type JackpotUpdateHandler = Action<ReadonlyArray<GCM.Jackpot>>;
            export type JackpotUpdateErrorHandler = Action<GCM.JackpotQueryFailure>;

            /**
             * Should be set in window.changeOrientation.
             *
             * This lets the game know any time there is a change of orientation or a window resize.
             *
             * Typically the game will call the getSize() API in response to this call to get the new window size.
             *
             * Possible values of window.orientation are -90, 0, 90, and 180. Positive values are clockwise; negative values are counterclockwise.
             */
            export type OrientationChangeHandler = (orientation: Orientation) => void;

            /**
             * Should be set in window.processServerMsg.
             *
             * The wrapper will call this function in the game anytime it receives a message from the GDM server.
             *
             * The message passed to the game is the entire XML string containing the GDM string.
             *
             * The game must be able to extract the information it needs from this string.
             */
            export type ServerMessageHandler = (message: string) => void;

            export type Orientation = -90 | 0 | 90 | 180;

            /**
             * GDM games must handle autoplay themselves. \
             * These are the requirements that should be applied whilst doing so.
             */
            export namespace AutoplayStopOptions
            {
                /** No automatic autoplay stop options are required. */
                export type None = 0;
                /** The player must configure “loss limit” and “single win limit”. Only used for UK jurisdiction. */
                export type LimitLossAndSingleWin = 2;
            }
            export type AutoplayStopOptions = AutoplayStopOptions.None | AutoplayStopOptions.LimitLossAndSingleWin;
        }

        /** Represents the GDM wrapper. */
        export interface IWrapper
        {
            //#region Properties

            /** Game quality, e.g. hq */
            readonly quality: string;
            /** Global language code variable e.g. en_GB */
            readonly languageCode: string;

            //#endregion

            //#region Getter methods

            /** Provides clients the ability to know if the game is in free or real play. Returns “TRUE” if the game is in free play mode. */
            isFreeMode(): boolean;
            /** Gets custom game settings. */
            getCustomSettings(): Readonly<IWrapper.CustomSettings>;
            /** Get the size the game should be. The game should not get this any other way. */
            getSize(): Readonly<{ w: number, h: number }>;
            /** Gets a map of feature client objects. */
            getGCMFeatures(): Readonly<IWrapper.FeatureMap>;
            /** Gets whether or not the ingame audio options should be shown. */
            audioBtnVisibility(): boolean;

            //#endregion

            //#region Lifecycle methods

            /**
             * Game clients should invoke this method on GCM if they need to be notified for any changes in the balances of any OGS jackpot(s).
             * Note that pollJackpots method must be invoked after GCM initialization completes (i.e. after callback configReady(ogsParams) has been called by GCM), and every time the game receives new jackpot instance ids from the game server (dependent on the game server’s integration protocol).
             * @param currency The currency to which the balances will be denominated (typically the player’s currency) in ISO-4217 three-letter code.
             * @param jackpots An array with the list of jackpot instance identifiers for which the balances will be retrieved.
             *                 Game clients are expected to receive the active identifiers of the jackpots applicable to the game at any given time,
             *                 from the game server. If null, will try to use the active jackpots for the current game.
             * @param pollJackpotsCallback A callback passed the result of each poll. If not set, the result will be passed to Game.jackpotBalancesUpdate() instead.
             * @param pollJackpotsErrorCallback A callback passed the result of each poll. If not set, the result will be passed to Game.onJackpotsUpdateFailed() instead.
             */
            pollJackpots(currency: string, jackpots?: ReadonlyArray<GCM.JackpotInstanceID>, pollJackpotsCallback?: IGame.JackpotUpdateHandler, pollJackpotsErrorCallback?: IGame.JackpotUpdateErrorHandler): void;

            /** Update the SG loading screen */
            updateProgress(value: number, max: number): void;
            /** Send a message to the GDM backend */
            sendMsgToServer(message: string): void;
            /** Notifies the wrapper that your game has loaded all assets and is ready to be played. This should be called after all updateProgress() calls. */
            gameReady(): void;

            /** Sets a value for a game property */
            valueChanged(key: "ROUND", value: IWrapper.RoundState): void;
            valueChanged(key: IWrapper.NumericValueKey, value: number): void;
            valueChanged(key: IWrapper.BooleanValueKey, value: boolean): void;
            valueChanged(key: IWrapper.ValueKey, value: number | boolean): void;

            /** To be called when processServerMessage is an error message */
            delegatedErrorHandling(errorType: string, errorMsg: string, server: boolean): void;

            /** To be called when the user presses an in-game button relying on GCM API. */
            gameButtonPressed(buttonID: "CASHIER"): void;
            /** To be called when the user presses an in-game home button. */
            homeButtonPressed(): void;

            /** To be called when game enters UKGC autoplay, supply -1 for values not defined/limited */
            autoPlayNotification(spins: number, losslimit: number, singlewinlimit: number): void;

            //#endregion
        }

        export type OperatorID = string | number;

        export namespace IWrapper
        {
            export interface CustomSettings
            {
                /** Operator-specific id. */
                opid: OperatorID;
                /** Jurisdiction code, e.g. “uk”, “se”, “lt”, */
                jurisdiction: string;
                /** e.g. “foxinwinshq” */
                gameName: string;
                /** Same as global languageCode */
                languageCode: string;
                /** true - Quick Stop can be enabled. false - Quick Stop must be disabled.*/
                quickStop: boolean;
                /** Minimum spin time in seconds. */
                minSpinTime: number;
                /** true - Buy Pass can be enabled. false - Buy Pass must be disabled. */
                buyPass: boolean;
                /** If the game has the Gamble Feature then this is the max number of gambles */
                gambleCount: number;
            }

            /** Numeric GDM valueChanged keys. */
            export type NumericValueKey =
                "ROUND"
                | "TOTAL_BET"
                | "TOTAL_WIN"
                | "BALANCE";

            /** Boolean GDM valueChanged keys. */
            export type BooleanValueKey =
                "MUTE"
                | "PAYTABLE";

            /** All valid GDM valueChanged keys. */
            export type ValueKey = NumericValueKey | BooleanValueKey;

            export namespace RoundState
            {
                export type Started = 0;
                export type Stopped = 1;
            }
            export type RoundState = RoundState.Started | RoundState.Stopped;

            export type FeatureMap = { [name: string]: object };
        }
    }
}