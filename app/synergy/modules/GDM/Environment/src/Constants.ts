/// <reference path="./helpers/APIGlobals.ts"/>

namespace OGS.GDM.IGame
{
    // Give the .d.ts 'enum types' actual runtime values

    enum AutoplayStopOptionsEnum
    {
        None = 0,
        LimitLossAndSingleWin = 2
    }

    enum RoundStateEnum
    {
        Stopped = 0,
        Started = 1
    }

    export const AutoplayStopOptions: typeof AutoplayStopOptionsEnum = { ...AutoplayStopOptionsEnum };
    export const RoundState: typeof RoundStateEnum = { ...RoundStateEnum };
}

namespace RS.GDM.Environment
{
    /** Exported globals used by the GDM wrapper to interface with the game. */
    export const globals = new APIGlobals<OGS.GDM.IGame>();

    /**
     * A map of error codes to localisable strings.
     *
     * Generated from the GDM server API: https://docs.google.com/document/d/1zoujB2cvOQwV1YlUFOml4q_lwHSjhPirOSwY1skoumg
     */
    export const errorMessages: Readonly<RS.Map<RS.Localisation.LocalisableString>> =
    {
        // User errors
        "ERROR_INSUFFICIENT_FUNDS": "Insufficient funds to place this bet",
        "ERROR_INSUFFICIENTFUNDS": "Insufficient funds to place this bet",
        "ERROR_REALITY_CHECK": "Reality check",
        "ERROR_GAMING_LIMITS": "Gaming limits reached",
        "ERROR_ACCOUNT_BLOCKED": "Player account blocked",
        "ERROR_MAXWIN": "Maximum win amount achieved",

        // Generic errors
        "GENERIC_ERROR": "Generic error",
        "ERROR_UNKNOWN": "Unknown error",
        "SERVLET_ERROR": "Servlet error",
        "ERROR_ACCOUNT": "Account error",

        // Session errors
        "ERROR_INVALID_SESSION": "Invalid session",
        "ERROR_INVALID_LB_TOKEN": "Invalid load balancer token",
        "ERROR_INVALID_SECURITY_TOKEN": "Invalid security token",
        "ERROR_GEOLOCATION_FAILURE": "Geolocation error",
        "ERROR_AUTHENTICATION_FAILED": "Authentication failed",
        "ERROR_WAGER_NOT_FOUND": "Wager not found",

        // Message flow errors
        "ERROR_INITIALISATION": "Initialisation error",
        "ERROR_COMMUNICATIONS": "Communications error",
        "ERROR_PROTOCOL_SEQUENCE": "Incorrect message sequence",
        "ERROR_PROTOCOL": "Unknown protocol message",
        "ERROR_STARTGAME": "Error trying to start the game",
        "ERROR_ENDGAME": "Error trying to end the game",
        "ERROR_JACKPOT_WON": "Jackpot already won",

        // Message parameter errors
        "ERROR_UNKNOWN_PARAMETER": "Unknown parameter in protocol",
        "ERROR_MISSING_PARAMETER": "Missing parameter in protocol",
        "ERROR_GDM_PARAMETERS": "Incorrect GDM parameters",
        "ERROR_PARAMETER_VALUE": "Incorrect parameter values",
        "ERROR_FEATURE_PARAMETERS": "Incorrect feature parameters",
        "ERROR_PARAMETERS_REQUIRED": "Incomplete parameters",

        // Validation errors
        "ERROR_BET_DISCRETE": "Invalid bet amount",
        "ERROR_BET_LIMITS": "Bet amount out of limits",
        "ERROR_LINES": "Incorrect line number",
        "ERROR_UNKNOWN_CURRENCY": "Invalid currency",

        // Configuration errors
        "ERROR_GAMENOTSUPPORTED": "Game not supported",

        // Feature errors
        "ERROR_FEATURE": "Feature error",
        "ERROR_FEATURE_FEATUREID": "Feature error",
        "ERROR_FEATURE_TRIGGER": "Feature error",
        "ERROR_FEATURE_TERMINATE": "Feature error",
        "ERROR_FEATURE_PLAY": "Feature error",
        "ERROR_FEATURE_SETSTATE": "Feature error",

        // Free games errors
        "ERROR_FREE_GAMES": "Free games error",
        "ERROR_FREE_GAMES_FEATUREID": "Free games error",
        "ERROR_FREE_GAMES_TRIGGER": "Free games error",
        "ERROR_FREE_GAMES_TERMINATE": "Free games error",
        "ERROR_FREE_GAMES_PLAY": "Free games error",
        "ERROR_FREE_GAMES_SETSTATE": "Free games error",

        // Table errors
        "ERROR_PRESPIN": "Pre-spin error",
        "ERROR_ANTEBET": "Ante bet error",
        "ERROR_SIDEBET": "Invalid side bet",

        // Slot errors
        "ERROR_LOGICALSLOT": "Slot error",
        "ERROR_GAMBLE": "Gamble error"
    };
}