namespace RS.GDM
{
    export function prompt(key: string, message: string, defaultValue?: string): string;

    export function prompt(key: string, message: string, validator: RegExp, defaultValue?: string): string;

    /** Displays a prompt to the user which uses WebStorage to load the previous response. */
    export function prompt(key: string, message: string, p3?: RegExp | string, p4?: string): string
    {
        let validator: RegExp, defaultValue: string;
        if (RS.Is.string(p3))
        {
            validator = null;
            defaultValue = p3;
        }
        else
        {
            validator = p3;
            defaultValue = p4;
        }

        const storageKey = `GDM/PromptHistory/${key}`;
        defaultValue = RS.Storage.load(storageKey, "") || defaultValue;

        const maxLen = 1000;
        const placeholder = "<truncated>";
        let displayedDefault = (defaultValue && defaultValue.length > maxLen) ? placeholder : defaultValue;

        let result: string = null;
        while (result == null)
        {
            const promptResult = useDefault(window.prompt(message, displayedDefault), displayedDefault);
            if (promptResult == null) { return ""; }
            if (promptResult === displayedDefault || !validator || validator.test(promptResult))
            {
                result = promptResult;
            }
            else
            {
                message = `Invalid value; submit again to override.\n${message}`;
                displayedDefault = promptResult;
            }
        }

        if (result === placeholder)
        {
            result = defaultValue;
        }

        RS.Storage.save(storageKey, result);
        return result;
    }

    function useDefault(str: string | null, defaultStr: string): string | null
    {
        if (str === "") { return defaultStr; }
        return str;
    }
}