namespace RS.GDM.Environment
{

    /** Withholds value-change calls. */
    export class ValueChangeProxy implements ValueChangeProxy.IValueChangeable
    {
        private readonly __target: ValueChangeProxy.IValueChangeable;
        private readonly __values: { [P in OGS.GDM.IWrapper.ValueKey]?: number | boolean } = {};

        private __defer = true;
        /** Whether or not value-change calls should be withheld. When set false, releases all held values and all future valueChanged calls will update target directly. */
        public get deferred() { return this.__defer; }
        public set deferred(value)
        {
            if (this.__defer === value) { return; }
            this.__defer = value;
            if (!value)
            {
                this.dispatchDeferredValues();
            }
        }

        constructor(target: ValueChangeProxy.IValueChangeable)
        {
            this.__target = target;
        }

        public valueChanged(key: "ROUND", value: OGS.GDM.IWrapper.RoundState): void;
        public valueChanged(key: OGS.GDM.IWrapper.NumericValueKey, value: number): void;
        public valueChanged(key: OGS.GDM.IWrapper.BooleanValueKey, value: boolean): void;
        public valueChanged(key: OGS.GDM.IWrapper.ValueKey, value: number | boolean): void;
        public valueChanged(key: OGS.GDM.IWrapper.ValueKey, value: number | boolean): void
        {
            if (this.__defer)
            {
                this.__values[key] = value;
            }
            else
            {
                this.__target.valueChanged(key, value);
            }
        }

        protected dispatchDeferredValues()
        {
            for (const key in this.__values)
            {
                const value = this.__values[key];
                this.__target.valueChanged(key as OGS.GDM.IWrapper.ValueKey, value);
                delete this.__values[key];
            }
        }
    }

    export namespace ValueChangeProxy
    {
        export type IValueChangeable = Pick<OGS.GDM.IWrapper, "valueChanged">;
    }
}