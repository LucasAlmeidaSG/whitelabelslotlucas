namespace RS.GDM.Environment
{
    const _globals = window;

    /**
     * Typed API globals object.
     *
     * Exists purely because using the global context and typing global variables as part of an API is a royal pain in the a
     */
    export class APIGlobals<T extends object>
    {
        private readonly _map: Partial<T> = {};

        /**
         * Gets a map of all globals.
         *
         * Useful to get a single typed object containing the globals.
         *
         * May not include all typed globals if setAll was not used.
         */
        public get map(): Readonly<Partial<T>> { return this._map; }

        /**
         * Sets a single global.
         *
         * Throws if the global was already set by another object.
         */
        public set<TKey extends keyof T>(name: TKey, value: T[TKey]): void
        {
            if (!(name in this._map) && name in _globals)
            {
                throw new Error(`Global value '${value}' already set`);
            }

            this._map[name] = value;
            _globals[name as string] = value;
        }

        /**
         * Sets globals using a map.
         *
         * Useful to ensure that all desired globals are set.
         */
        public setAll(map: Readonly<T>): void
        {
            for (const key in map)
            {
                this.set(key, map[key]);
            }
        }

        /**
         * Deletes a global from the map and global context.
         *
         * Will not alter global context if the global was not set by this object.
         *
         * Returns whether or not any change was made.
         */
        public unset<TKey extends keyof T>(name: TKey): boolean
        {
            if (name in this._map)
            {
                delete _globals[name as string];
                delete this._map[name];
                return true;
            }

            return false;
        }

        /**
         * Deletes all globals set via this object from the map and global context.
         *
         * Will not alter global context for keys not set by this object.
         */
        public unsetAll(): void
        {
            for (const key in this._map)
            {
                this.unset(key);
            }
        }

        /**
         * Gets a single global.
         *
         * Returns null if the global does not exist or was not set by this object.
         */
        public get<TKey extends keyof T>(name: TKey): T[TKey] | null
        {
            if (name in this._map)
            {
                return _globals[name as string];
            }
            else
            {
                return null;
            }
        }
    }
}