namespace RS.GDM
{
    export function promptFile(message: string, accept?: string): PromiseLike<File>
    {
        const div = document.createElement("div");
        div.style.position = "fixed";
        div.style.zIndex = "999";
        div.style.left = "0";
        div.style.right = "0";
        div.style.textAlign = "center";

        const prompt = document.createElement("div");
        prompt.style.display = "inline-block";
        prompt.style.backgroundColor = "white";
        prompt.style.textAlign = "left";
        prompt.style.padding = "20px";
        prompt.style.borderRadius = "3px";
        prompt.style.fontFamily = "Sans-Serif";
        prompt.style.fontSize = "11pt";
        prompt.style.color = "#666";
        prompt.style.boxShadow = "0px 2px 5px #000";
        prompt.innerText = message;
        div.appendChild(prompt);

        const form = document.createElement("form");
        form.id = "rs-prompt-upload";
        form.style.textAlign = "right";
        form.style.marginTop = "10px";
        prompt.appendChild(form);

        const upload = document.createElement("input");
        upload.style.display = "block";
        upload.type = "file";
        upload.accept = accept;
        upload.name = "file";
        form.appendChild(upload);

        const submit = document.createElement("input");
        submit.style.marginTop = "20px";
        submit.style.padding = "8px 15px";
        submit.value = "OK";
        submit.type = "submit";
        form.appendChild(submit);

        const cancel = document.createElement("input");
        cancel.style.marginTop = "20px";
        cancel.style.marginLeft = "8px";
        cancel.style.padding = "8px 15px";
        cancel.value = "Cancel";
        cancel.type = "button";
        form.appendChild(cancel);

        document.body.appendChild(div);

        // Disable submit unless file
        submit.disabled = true;
        upload.onchange = function (ev)
        {
            submit.disabled = upload.files.length === 0;
        };

        form.focus();

        return new Promise<File>((resolve, reject) =>
        {
            function close()
            {
                div.remove();
            }

            function doCancel()
            {
                close();
                resolve(null);
            }

            function doCancelOnEsc(ev: KeyboardEvent)
            {
                if (ev.key === "Escape")
                {
                    doCancel();
                }
            }

            function doSubmit(ev: Event)
            {
                ev.preventDefault();

                const file = upload.files[0];
                if (!file) { return; }

                close();
                resolve(file);
            }

            cancel.onclick = doCancel;
            form.onkeyup = doCancelOnEsc;
            form.onsubmit = doSubmit;
        });
    }
}