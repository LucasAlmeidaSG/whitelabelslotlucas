namespace RS.GDM
{
    /**
     * GDM games deal with a lot of objects from other frames.
     *
     * Objects passed in from other frames aren't actually Objects, so we need to recreate them in order for simple objects & arrays to be considered what they are.
     */
    export function objectify(val: any)
    {
        if (RS.Is.object(val))
        {
            const obj = {};
            objectifyObject(val, obj);
            return obj;
        }
        if (RS.Is.array(val))
        {
            const arr = new Array(val.length);
            objectifyObject(val, arr);
            return arr;
        }
        return val;
    }

    function objectifyObject(obj: object, out: object)
    {
        for (const key in obj)
        {
            try
            {
                out[key] = objectify(obj[key]);
            }
            catch (err)
            {
                Environment.logger.error(`objectify: failed to read property '${key}'`, err);
                continue;
            }
        }
    }
}