namespace RS.GDM.Environment
{
    export interface IAPIExt<T>
    {
        /** The current value of this APIExt. */
        readonly value: T | undefined;
        /** Published when this APIExt is invoked. */
        readonly onCalled: RS.IEvent<T>;

        /**
         * Binds an arbiter to this APIExt.
         * @param nullValue If set, will not declare this value.
         */
        bindArbiter(arbiter: RS.IArbiter<T>, nullValue?: T): void;
        /**
         * Binds a differently-typed arbiter to this APIExt using a map function.
         * @param nullValue If set, will not declare this value.
         */
        bindArbiter<U>(arbiter: RS.IArbiter<U>, mapFunc: (value: T) => U, nullValue?: U): void;
        /** Unbinds an arbiter from this APIExt. */
        unbindArbiter(arbiter: RS.IArbiter<any>): void;
    }

    export interface IAPIExtWithResult<T, TResult> extends IAPIExt<T>
    {
        /** Sets the delegate used to evaluate the return value for this APIExt. */
        setResultDelegate(callback: IAPIExt.ResultDelegate<T, TResult>): void;
    }

    export namespace IAPIExt
    {
        export type ResultDelegate<T, TResult> = (arg: T) => TResult;
    }
}