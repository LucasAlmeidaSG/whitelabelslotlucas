namespace RS.GDM.Environment
{
    /** Implements value handling deferral as well as binding and eventing for apiExt calls. */
    export class APIExt<T, TResult> implements IAPIExt<T>, IAPIExtWithResult<T, TResult>
    {
        public get value() { return this.__value; }
        public get onCalled()
        {
            return this.__onCalled
                ? this.__onCalled.public
                : (this.__onCalled = RS.createEvent()).public;
        }

        private __set: boolean;
        private __value: T;
        private __onCalled: RS.IPrivateEvent<T> | void;
        private __delegate: IAPIExt.ResultDelegate<T, TResult> | void;
        private __arbiterBindings: (APIExt.ArbiterBinding<T> | APIExt.MappedArbiterBinding<any, T>)[] | void;

        constructor(initialValue?: T)
        {
            this.__value = initialValue;
            this.__set = arguments.length > 0;
        }

        public setResultDelegate(callback: IAPIExt.ResultDelegate<T, TResult>)
        {
            if (this.__delegate) { logger.warn("Overrode previously set result delegate"); }
            this.__delegate = callback;
        }

        public onAPIExt(arg: T): TResult | void
        {
            if (arg !== this.__value && this.__arbiterBindings)
            {
                for (const binding of this.__arbiterBindings)
                {
                    if (binding.handle)
                    {
                        binding.handle.dispose();
                        binding.handle = null;
                    }

                    // TypeScript doesn't recognise the implicit boolean for type narrowing
                    // So we need == true for it to compile
                    if (binding.mapped == true)
                    {
                        const mappedArg = binding.mapFunc(arg);
                        if (mappedArg !== binding.nullValue)
                        {
                            binding.handle = binding.arbiter.declare(mappedArg);
                        }
                    }
                    else
                    {
                        if (arg !== binding.nullValue)
                        {
                            binding.handle = binding.arbiter.declare(arg);
                        }
                    }
                }
            }

            if (this.__onCalled)
            {
                this.__onCalled.publish(arg);
            }

            this.__value = arg;
            this.__set = true;

            if (this.__delegate)
            {
                return this.__delegate(arg);
            }

            return;
        }

        public bindArbiter(arbiter: RS.IArbiter<T>, nullValue?: T): void;
        public bindArbiter<U>(arbiter: RS.IArbiter<U>, mapFunc: (value: T) => U, nullValue?: U): void;
        public bindArbiter<U>(arbiter: RS.IArbiter<U> | RS.IArbiter<T>, p2?: T | ((val: T) => U), p3?: U): void
        {
            let mapFunc: (value: T) => U, nullValue: U | T;
            if (RS.Is.func(p2))
            {
                mapFunc = p2;
                nullValue = p3;
            }
            else
            {
                nullValue = p2;
            }

            if (!this.__arbiterBindings) { this.__arbiterBindings = []; }
            if (this.__arbiterBindings.some((b) => b.arbiter === arbiter))
            {
                logger.warn("Arbiter already bound");
                return;
            }

            if (mapFunc)
            {
                arbiter = arbiter as RS.IArbiter<U>;
                const handle = this.__set ? arbiter.declare(mapFunc(this.__value)) : null;
                this.__arbiterBindings.push({ arbiter, handle, mapFunc, mapped: true, nullValue });
            }
            else
            {
                arbiter = arbiter as RS.IArbiter<T>;
                const handle = this.__set ? arbiter.declare(this.__value) : null;
                this.__arbiterBindings.push({ arbiter, handle, mapped: false, nullValue: nullValue as T });
            }
        }

        public unbindArbiter(arbiter: RS.IArbiter<any>)
        {
            if (!this.__arbiterBindings) { return; }
            for (let i = 0; i < this.__arbiterBindings.length; i++)
            {
                const binding = this.__arbiterBindings[i];
                if (binding.arbiter === arbiter)
                {
                    if (binding.handle)
                    {
                        binding.handle.dispose();
                        binding.handle = null;
                    }

                    this.__arbiterBindings.splice(i, 1);
                    break;
                }
            }
        }
    }

    export namespace APIExt
    {
        interface BaseArbiterBinding<T>
        {
            arbiter: RS.IArbiter<T>;
            handle: RS.IArbiterHandle<T> | undefined;
            nullValue?: T;
        }

        export interface ArbiterBinding<T> extends BaseArbiterBinding<T>
        {
            mapped: false;
        }

        export interface MappedArbiterBinding<U, T> extends BaseArbiterBinding<U>
        {
            mapped: true;
            mapFunc: (val: T) => U;
        }
    }
}