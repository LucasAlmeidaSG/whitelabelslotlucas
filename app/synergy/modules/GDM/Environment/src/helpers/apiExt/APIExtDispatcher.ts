namespace RS.GDM.Environment
{
    import APIExtCalls = OGS.GDM.IGame.APIExtCalls;

    export type ReturnedAPIExt<T extends keyof APIExtCalls> =
        APIExtCalls[T]["result"] extends void
            ? IAPIExt<APIExtCalls[T]["arg"]>
            : IAPIExtWithResult<APIExtCalls[T]["arg"], APIExtCalls[T]["result"]>;

    /** Dispatches platform apiExt calls to internal handlers, with support for deferred handling due to potential time gap between call and handler registration. */
    export class APIExtDispatcher
    {
        private readonly __map: { [key: string]: APIExt<any, any>; };

        constructor(initialValues?: APIExtDispatcher.InitialValues)
        {
            this.__map = {};
            if (initialValues)
            {
                for (const apiKey in initialValues)
                {
                    this.__map[apiKey] = new APIExt(initialValues[apiKey]);
                }
            }
        }

        public getAPIExt<T extends keyof APIExtCalls>(key: T): ReturnedAPIExt<T>;
        public getAPIExt(key: string): APIExt<any, any>
        {
            return this.getAPIExtInternal(key);
        }

        public onAPIExt<T extends keyof APIExtCalls>(key: T, arg: APIExtCalls[T]["arg"]): APIExtCalls[T]["result"]
        {
            const apiExt = this.getAPIExtInternal(key);
            return apiExt.onAPIExt(arg);
        }

        protected getAPIExtInternal(key: string): APIExt<any, any>
        {
            if (!this.__map[key]) { this.__map[key] = new APIExt() as any; }
            return this.__map[key];
        }
    }

    export namespace APIExtDispatcher
    {
        export type InitialValues =
        {
            [P in keyof APIExtCalls]?: APIExtCalls[P]["arg"];
        };
    }
}