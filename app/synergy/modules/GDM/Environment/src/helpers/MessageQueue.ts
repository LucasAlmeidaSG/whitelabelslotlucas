namespace RS.GDM.Environment
{
    export class MessageQueue
    {
        private readonly __queue: QueuedMessage[] = [];
        private readonly __requestDelegate: MessageQueue.RequestDelegate;

        /** The number of pending messages. */
        public get pending() { return this.__queue.length; }

        constructor(requestDelegate: MessageQueue.RequestDelegate)
        {
            if (!requestDelegate) { throw new Error("requestDelegate cannot be null"); }
            this.__requestDelegate = requestDelegate;
        }

        /** Resolves the next message in the queue with the given response. Throws if there is no message to reject. */
        public onResponse(response: string): void
        {
            this.settleHead(true, response);
        }

        /** Rejects the next message in the queue with the given reason. Throws if there is no message to reject. */
        public rejectNext(reason?: any)
        {
            this.settleHead(false, reason);
        }

        /** Rejects all messages in the queue with the given reason (environment invalidation, etc.). */
        public rejectAll(reason?: any)
        {
            for (const msg of this.__queue)
            {
                msg.reject(reason);
            }
            this.__queue.length = 0;
        }

        /**
         * Enqueues the given request and returns a Promise that resolves
         * when there are no messages ahead of it in the queue and a
         * response is received.
         */
        public enqueue(request: string): Promise<string>
        {
            return new Promise<string>((resolve, reject) =>
            {
                this.__queue.push({ request, resolve, reject });

                // Process immediately if we are the only one in the queue
                if (this.__queue.length === 1)
                {
                    this.processQueue();
                }
            });
        }

        protected settleHead(resolve: true, response: string): void;
        protected settleHead(resolve: false, reason: any): void;
        protected settleHead(resolve: boolean, arg: any): void
        {
            if (this.__queue.length === 0)
            {
                throw new Error("No message in queue");
            }

            const msg = this.__queue.shift();
            if (resolve)
            {
                msg.resolve(arg);
            }
            else
            {
                msg.reject(arg);
            }

            // Process next queue item
            if (this.__queue.length > 0)
            {
                this.processQueue();
            }
        }

        /** Executes the next request in the queue. */
        protected processQueue()
        {
            const head = this.__queue[0];
            this.__requestDelegate(head.request);
        }
    }

    export namespace MessageQueue
    {
        export type RequestDelegate = (message: string) => void;
    }

    interface QueuedMessage
    {
        request: string;
        resolve: (response: string) => void;
        reject: (reason?: any) => void;
    }
}