namespace RS.GDM.Environment
{
    /** Returns whether the given opid represents a mock operator or not. */
    export function isMock(opid: string | number)
    {
        const num = Number(opid);
        if (!RS.Is.number(num)) { return false; }
        return 0 < num && num < 100;
    }
}