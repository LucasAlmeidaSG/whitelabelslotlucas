namespace RS.GDM.Environment
{
    // Initialise on ready
    RS.DOM.Document.readyState.for(true).then(() => RS.IPlatform.get());

    const ntPartnerCodes =
    [
        "1285"
    ];

    /** GDM context logger. */
    export const logger = RS.Logging.getContext("GDM");

    /** Server message connector. */
    export interface IServerConnector
    {
        /** Sends the given serialised message to the game server and returns a promise of its response. */
        sendServerMessage(string: string): PromiseLike<string>;
        /** Notifies the platform of a server error. */
        notifyError(code: string, message?: string): void;
    }

    export interface IError
    {
        code: string;
        message: RS.Localisation.LocalisableString;
    }

    import APIExtCalls = OGS.GDM.IGame.APIExtCalls;

    /** Implements common GDM integration with games */
    @RS.HasCallbacks
    export class Platform implements RS.IPlatform, IServerConnector
    {
        /** Platform logger */
        public readonly logger = logger;

        /** Whether or not we are currently in a mock environment. */
        public readonly inMockEnvironment: boolean;

        //#region IPlatform.properties

        // TODO Synergy 1.2: remove engineEndpointOverride from RS.IPlatform
        public readonly engineEndpointOverride: string = null;

        public readonly assetsRoot: string = "assets";
        public readonly translationsRoot: string = "translations";

        //#region Region

        public locale: RS.Localisation.Locale = null;
        public get localeCode()
        {
            // NOTE: this is mapped from GDM's en_gb format to Synergy's en_GB format using lcmapping.json, which is generated at build-time.
            // Ergo, no conversion is needed here.
            return this.__gcmWrapper.languageCode;
        }

        public get countryCode()
        {
            const gcmJurisdiction = this.__gcmWrapper.getCustomSettings().jurisdiction;
            const uppercased = gcmJurisdiction.toUpperCase();
            return uppercased in jurisdictionMap
                ? jurisdictionMap[uppercased]
                : uppercased;
        }

        public get partnerCode(): string | null
        {
            return `${this.__gcmWrapper.getCustomSettings().opid}` || null;
        }

        /**
         * Gets if the game needs to load its NT variant
         */
        public get isNT()
        {
            if (!this.__localStorageNTLoaded)
            {
                this.__ntOverride = RS.Storage.load("isNT", false, true);
                this.__localStorageNTLoaded = true;
            }

            return this.__ntOverride ? this.__ntOverride : ntPartnerCodes.indexOf(this.partnerCode) !== -1;
        }

        //#endregion

        //#region Modes

        public readonly localeMode = RS.PlatformLocaleMode.ProvidedByGame;

        /** Gets how max win will be handled. Set by the engine after the first init response. */
        public maxWinMode = RS.PlatformMaxWinMode.None;

        public get networkErrorHandlingMode()
        {
            return this.__lastError
                ? RS.PlatformMessageHandlingMode.PassToPlatform
                : RS.PlatformMessageHandlingMode.Display;
        }
        public readonly generalErrorHandlingMode = RS.PlatformMessageHandlingMode.Display;
        public readonly recoveryMessageHandlingMode = RS.PlatformMessageHandlingMode.Display;
        public readonly generalMessageHandlingMode = RS.PlatformMessageHandlingMode.Display;
        public readonly connectionLostHandlingMode = RS.PlatformConnectionLostMode.PauseWithMessage;
        public readonly settingsHandlingMode = RS.PlatformSettingsHandlingMode.ProvidedByGame;

        //#endregion

        //#region Capabilities

        public get canNavigateToLobby() { return this.getAPIExt("SHOW_HOME_BUTTON").value; }
        public get canAccessSettings() { return this.getAPIExt("SHOW_SETTINGS").value; }
        public readonly canNavigateToExternalHelp = true;

        //#endregion

        //#region Switches

        public get isFreePlay() { return this.__gcmWrapper.isFreeMode(); }
        public get shouldDisplayDevTools() { return /*this.isFreePlay ||*/ this.inMockEnvironment; }
        public get shouldDisplayDemoTool() { return /*this.isFreePlay ||*/ this.inMockEnvironment; }
        public get shouldDisplayGaffingTool() { return /*this.isFreePlay ||*/ this.inMockEnvironment; }

        //#endregion

        //#region Settings

        public get minWagerDuration()
        {
            return (this.__gcmWrapper.getCustomSettings().minSpinTime || 3) * 1000;
        }

        public readonly turboModeEnabled = true;
        public readonly bigBetEnabled = true;

        //#endregion

        //#region Preloader

        public preloaderVisible: boolean = true;

        public get preloaderProgress() { return this.__preloaderProgress; }
        public set preloaderProgress(value: number)
        {
            if (this.__preloaderProgress === value) { return; }
            value = RS.Math.clamp(value, 0, 1);
            this.__preloaderProgress = value;

            // updateProgress doesn't exist on "wrapper3" so we check for that here before calling
            if (Is.func(this.__gcmWrapper.updateProgress))
            {
                this.__gcmWrapper.updateProgress(value, 1);
            }
        }
        private __preloaderProgress: number = 0.0;

        public get loaderProgress() { return this.__loaderProgress; }
        public set loaderProgress(value: number)
        {
            if (this.__loaderProgress === value) { return; }
            if (value < this.__loaderProgress) { logger.warn("Loader progress decreased"); }
            value = RS.Math.clamp(value, 0, 1);
            this.__loaderProgress = value;
            if (value === 1)
            {
                this.handleGameReady();
            }
        }
        private __loaderProgress: number = 0.0;

        //#endregion

        /** Returns the GCM wrapper. */
        public get adapter() { return this.__gcmWrapper; }

        //#endregion

        /** Published when the GDM wrapper calls apiExt with key "SET_BALANCE". */
        public get onBalanceUpdatedExternally() { return this.getAPIExt("SET_BALANCE").onCalled; }

        private readonly __messageQueue: MessageQueue;

        public get onJackpotUpdate() { return this.__onJackpotUpdate.public; }
        private readonly __onJackpotUpdate = RS.createEvent<ReadonlyArray<GCM.Jackpot>>();

        public get onJackpotError() { return this.__onJackpotError.public; }
        private readonly __onJackpotError = RS.createEvent<GCM.JackpotQueryFailure>();

        /** Jackpots which have been passed to initialiseJackpots(). */
        private readonly __initialisedJackpots: string[] = [];

        /** Partial map of valueChanged keys to observable bindings. */
        private readonly __valueChangedBindings: ValueChangedBindingMap = {};
        /**
         * Withholds valueChanged calls from the partner until it is ready to receive them. \
         * This is to help translate the Synergy game lifecycle to the GDM game lifecycle.
         */
        private readonly __valueChangedProxy: ValueChangeProxy;

        /** Fallback currency code to use in case currencyCode is not set. */
        private __defaultCurrencyCode: string = "EUR";
        private __currencyCode: string = null;
        /** Player's currency code e.g. GBP or EUR. To be set only once when the currency code is received by the Engine. */
        public get currencyCode() { return this.__currencyCode; }
        public set currencyCode(code)
        {
            if (this.__currencyCode === code) { return; }
            if (this.__currencyCode)
            {
                // This shouldn't happen, we can't really rely on this
                throw new Error(`Attempted to change currencyCode from ${this.__currencyCode} to ${code}`);
            }
            else
            {
                this.__currencyCode = code;
            }
        }

        /** Dispatches platform apiExt calls to internal handlers, with support for deferred handling due to potential time gap between call and handler registration. */
        private readonly __apiExtDispatcher: APIExtDispatcher;

        private __settings: Readonly<RS.IPlatform.Settings>;
        protected get settings() { return this.__settings; }

        /** GCM wrapper which serves as the third-party platform environment akin to the RGS partner adapter. */
        private readonly __gcmWrapper: OGS.GDM.IWrapper;

        /** User-set mute handle. */
        @RS.AutoDisposeOnSet
        private __muteHandle: RS.IDisposable;

        private __lastError: IError = null;
        /** The last error reported to the platform. */
        public get lastError(): Readonly<IError> { return this.__lastError; }

        private __lastJackpotQuery: ReadonlyArray<GCM.Jackpot> | GCM.JackpotQueryFailure = null;

        private __ntOverride: boolean = false;
        private __localStorageNTLoaded: boolean = false;

        public constructor()
        {
            // Notify only the first success and the first error so we don't spam things up
            this.onJackpotError.once(this.logOnJackpotError);
            this.onJackpotUpdate.once(this.logOnJackpotSuccess);

            this.onJackpotError(this.handleJackpotQueryResponse);
            this.onJackpotUpdate(this.handleJackpotQueryResponse);
            const dump = RS.IDump.get();
            dump.onPreSave(this.handleDumpPreSave);

            if (window.parent == window)
            {
                this.__gcmWrapper = new DummyWrapper();
            }
            else
            {
                this.__gcmWrapper = window.parent as any;
                // Enable debug logging to help all our OGS friends
                this.logger.logLevel = RS.Logging.LogLevel.Debug;
            }

            // This serves to implement a request-response queue for server messages
            // It combines sendMsgToServer and processServerMsg into a promise-based API
            // It also ensures two request-response sequences never overlap
            this.__messageQueue = new MessageQueue((message) =>
            {
                this.dumpServerMessage("request", message);
                this.__gcmWrapper.sendMsgToServer(message);
            });

            // Defer value changes until we and the platform are ready to receive them because the wrapper uses them to initialise
            // (Yes, this is a massive pain in the ass, but this environment is extremely sensitive about the timing of its calls.)
            this.__valueChangedProxy = new ValueChangeProxy(this.__gcmWrapper);
            this.__valueChangedProxy.deferred = true;

            // The dispatcher mostly serves to bridge the potential time-gap between platform apiExt calls and internal apiExt linkages
            // Sort of like valueChangedProxy but in reverse
            // If the platform manages to call apiExt before we've actually attached handling, it will be deferred until we decide to handle it
            this.__apiExtDispatcher = new APIExtDispatcher(
            {
                "ENABLE_ALL_UI": false,
                "SHOW_HOME_BUTTON": true,       // Home button should be shown by default
                "SHOW_SETTINGS": true,
                "SHOW_CASHIER_BUTTON": false    // Cashier button should be hidden by default
            });

            // Setting some globals; this is our half of the GDM API
            globals.setAll(
            {
                apiExt: this.onAPIExt,
                processServerMsg: this.onServerMessage,
                changeOrientation: this.onOrientationChange
            });

            // If we're in a mock environment, enable dev tools
            const env = this.__gcmWrapper.getCustomSettings().opid;
            this.inMockEnvironment = isMock(env);

            this.launch();
        }

        //#region GDM.methods

        @RS.Callback
        public onAPIExt<T extends keyof APIExtCalls>(key: T, value?: APIExtCalls[T]["arg"]): APIExtCalls[T]["result"]
        {
            this.dumpWrapperCall(key.toLowerCase(), "in", value);
            return this.__apiExtDispatcher.onAPIExt(key, value);
        }

        @RS.Callback
        public onServerMessage(message: string)
        {
            this.dumpServerMessage("response", message);
            this.__messageQueue.onResponse(message);
        }

        @RS.Callback
        @RS.DebugLog(logger, "onOrientationChange (unused)", true)
        public onOrientationChange(orientation: OGS.GDM.IGame.Orientation)
        {
            return;
        }

        //#endregion

        public sendServerMessage(message: string): PromiseLike<string>
        {
            return this.__messageQueue.enqueue(message);
        }

        /**
         * Binds a numeric GDM value to a numeric observable.
         * @param key Numeric value key to bind.
         * @param observable Observable to bind to. If null, will remove any existing binding.
         * @returns Disposable binding handle, or null if observable was null.
         */
        public bindValueToObservable(key: "ROUND", observable: RS.IReadonlyObservable<OGS.GDM.IWrapper.RoundState>): RS.IDisposable | null;
        /**
         * Binds a numeric GDM value to a numeric observable.
         * @param key Numeric value key to bind.
         * @param observable Observable to bind to. If null, will remove any existing binding.
         * @returns Disposable binding handle, or null if observable was null.
         */
        public bindValueToObservable(key: OGS.GDM.IWrapper.NumericValueKey, observable: RS.IReadonlyObservable<number>): RS.IDisposable | null;
        /**
         * Binds a numeric GDM value to a numeric observable.
         * @param key Numeric value key to bind.
         * @param observable Observable to bind to. If null, will remove any existing binding.
         * @returns Disposable binding handle, or null if observable was null.
         */
        public bindValueToObservable(key: OGS.GDM.IWrapper.NumericValueKey, observable: RS.IReadonlyObservable<number>): RS.IDisposable | null;
        /**
         * Binds a boolean GDM value to a boolean observable.
         * @param key Boolean value key to bind.
         * @param observable Observable to bind to. If null, will remove any existing binding.
         * @returns Disposable binding handle, or null if observable was null.
         */
        public bindValueToObservable(key: OGS.GDM.IWrapper.BooleanValueKey, observable: RS.IReadonlyObservable<boolean>): RS.IDisposable | null;
        public bindValueToObservable(key: OGS.GDM.IWrapper.ValueKey, observable: RS.IReadonlyObservable<boolean | number>): RS.IDisposable | null
        {
            if (this.__valueChangedBindings[key])
            {
                this.__valueChangedBindings[key].dispose();
                this.__valueChangedBindings[key] = null;
            }

            if (observable)
            {
                this.doValueChange(key, observable.value);
                const handle = observable.onChanged((value) => { this.doValueChange(key, value); });
                this.__valueChangedBindings[key] = handle;
            }

            return this.__valueChangedBindings[key];
        }

        /**
         * Returns an APIExt for the given GDM apiExt key.
         * @param key A supported GDM apiExt key
         */
        public getAPIExt<T extends keyof APIExtCalls>(key: T)
        {
            return this.__apiExtDispatcher.getAPIExt(key);
        }

        /**
         * Releases held values. To be called after Engine has inited and observables have been set.
         */
        public onGameInitialised()
        {
            this.__valueChangedProxy.deferred = false;
        }

        //#region IPlatform.methods

        /**
         * Initialises the platform.
         * This is called when the game class has been initialised.
         */
        public init(settings: RS.IPlatform.Settings): void
        {
            this.__settings = settings;

            // Link mute
            this.bindValueToObservable("MUTE", settings.muteArbiter);
            this.getAPIExt("SET_MUTE").bindArbiter(settings.muteArbiter, false);

            // Link UI enabled arbiter
            this.getAPIExt("ENABLE_ALL_UI").bindArbiter(settings.uiEnabledArbiter, true);
        }

        /**
         * Handles a network error.
         */
        public async handleNetworkError(error: Error | string)
        {
            return this.delegateError(error, true);
        }

        /**
         * Handles a general error.
         */
        public async handleGeneralError(error: Error | string)
        {
            logger.warn("General errors should not be passed to the GDM platform");
            logger.error(`${error}`);
        }

        /**
         * Handles displaying the recovery message.
         */
        public async handleRecoveryMessage() { return; }

        /**
         * Handles displaying a general message.
         */
        public async handleGeneralMessage(message: string, title?: string) { return; }

        /**
         * Directs the platform to navigate to the lobby.
         */
        public navigateToLobby(): void
        {
            if (!this.canNavigateToLobby) { return logger.warn("Cannot navigate to lobby"); }
            this.adapter.homeButtonPressed();
        }

        public close(): void
        {
            this.adapter.homeButtonPressed();
        }

        public reload(): void
        {
            location.reload();
        }

        /**
         * Directs the platform to open the settings menu.
         */
        public async openSettingsMenu()
        {
            if (!this.canAccessSettings) { return logger.warn("Cannot access settings"); }

            const handle = this.settings.uiEnabledArbiter.declare(false);
            const dialog = Settings.dialog.create(Settings.Dialog.defaultSettings, this.__settings.uiContainer);

            const mute = dialog.addSwitch(Translations.Settings.Mute, false);
            mute.onChanged((v) =>
            {
                this.__muteHandle = v ? this.__settings.muteArbiter.declare(true) : null;
            });

            if (this.getAPIExt("SHOW_CASHIER_BUTTON").value)
            {
                const deposit = dialog.addButton(Translations.Settings.Deposit);
                deposit.onClicked(() =>
                {
                    this.__gcmWrapper.gameButtonPressed("CASHIER");
                });
            }

            await dialog.onCloseRequested;

            dialog.dispose();
            handle.dispose();
        }

        /**
         * Directs the platform to navigate to the external help page.
         */
        public navigateToExternalHelp(externalHelpData: RS.Map<string> = {}): void
        {
            if (!this.canNavigateToExternalHelp) { return logger.warn("Cannot navigate to external help"); }

            const analytics = RS.Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Game",
                    action: "OpenExternalHelp"
                });
            }

            const path = RS.Path.combine("help", `${this.localeCode}.html`);
            const countryCode = this.countryCode;
            const isNT = false;

            const url = RS.URL.setParameters({  isNT, countryCode, ...externalHelpData }, path);
            window.open(url, "", "resizable=1, scrollbars, width=750, height=500");
        }

        //#endregion

        /** Returns a named feature client e.g. JackpotWars. */
        public getFeature(name: string): object | null
        {
            return this.__gcmWrapper.getGCMFeatures()[name];
        }

        /** Initialises the given jackpots with the given currency code. Returns a Promise which resolves on the first success or rejects on the first error. */
        public initialiseJackpots(jackpotInstances?: ReadonlyArray<GCM.JackpotInstanceID>)
        {
            if (!this.__currencyCode)
            {
                logger.warn(`initialiseJackpots called with no currencyCode set, defaulting to '${this.__defaultCurrencyCode}'`);
            }
            const currencyCode = this.__currencyCode || this.__defaultCurrencyCode;

            if (this.__initialisedJackpots.length > 0)
            {
                if (jackpotInstances)
                {
                    for (const instance of jackpotInstances)
                    {
                        if (this.__initialisedJackpots.indexOf(instance) === -1)
                        {
                            throw new Error(`Cannot initialise new jackpot with instance ${instance}`);
                        }
                    }
                }

                logger.warn(`Cannot initialise jackpots twice`);
                return;
            }

            const onUpdate = this.__onJackpotUpdate.publish.bind(this.__onJackpotUpdate);
            const onError = this.__onJackpotError.publish.bind(this.__onJackpotError);

            const promise = new Promise<GCM.Jackpot[]>((resolve, reject) =>
            {
                const settle = (result: any, err: boolean) =>
                {
                    updateHandle.dispose();
                    errHandle.dispose();
                    if (err)
                    {
                        reject(result);
                    }
                    else
                    {
                        resolve(result);
                    }
                };

                const updateHandle = this.__onJackpotUpdate((data) => settle(data, false));
                const errHandle = this.__onJackpotError((err) => settle(err, true));
            });

            this.__gcmWrapper.pollJackpots(currencyCode, jackpotInstances, onUpdate, onError);

            return promise;
        }

        /** Notifies the platform of a (generally server) error to be delegated to the platform. */
        public notifyError(code: string, message: RS.Localisation.LocalisableString = code): void
        {
            this.__lastError = { code, message };
        }

        /** Mutes the game using the platform class. This is so we can keep the platform and internal mute buttons in sync  */
        @RS.DebugLog(logger, "internalMute", true)
        public internalMute(mute: boolean): void
        {
            this.__muteHandle = mute ? this.__settings.muteArbiter.declare(true) : null;
        }

        /** Delegates error to partner. Returns a promise that resolves when apiExt(ENABLE_ALL_UI) is called. */
        protected async delegateError(error: string | Error, server: boolean, sendMessage: boolean = false)
        {
            if (!this.__lastError) { logger.warn(`delegateError called with no lastError`); }

            const code = this.__lastError ? this.__lastError.code : "GENERIC_ERROR";

            let localisedMessage = null;
            if (sendMessage)
            {
                const message = errorMessages[code] || (this.__lastError ? this.__lastError.message : `${error}`);
                localisedMessage = RS.Localisation.resolveLocalisableString(this.locale, message);
            }

            this.__lastError = null;

            this.adapter.delegatedErrorHandling(code, localisedMessage, server);

            await this.getAPIExt("ENABLE_ALL_UI").onCalled;
        }

        /** Starts the game. */
        protected launch()
        {
            // Enqueue game launch
            setTimeout(RS.triggerInits, 0);
        }

        /** Performs a valueChanged call. */
        protected doValueChange(key: OGS.GDM.IWrapper.ValueKey, value: number | boolean)
        {
            this.dumpWrapperCall(key.toLowerCase(), "out", value);
            this.__valueChangedProxy.valueChanged(key, value);
        }

        //#region Jackpot debugging

        @RS.Callback
        @RS.DebugLog(logger, "onJackpotSuccess", true)
        protected logOnJackpotSuccess(jackpots: ReadonlyArray<GCM.Jackpot>) { return; }

        @RS.Callback
        @RS.DebugLog(logger, "onJackpotError", true)
        protected logOnJackpotError(failure: GCM.JackpotQueryFailure) { return; }

        @RS.Callback
        protected handleDumpPreSave()
        {
            if (!this.__lastJackpotQuery) { return; }
            const dump = RS.IDump.get();
            if (!dump.enabled) { return; }
            const subDump = dump.getSubDump("GCMJackpot");
            subDump.clear();

            if (RS.Util.hasOwnKeys(this.__lastJackpotQuery, { errors: true }))
            {
                this.__lastJackpotQuery.errors.forEach((err) => subDump.add(objectify(err)), true);
            }
            else
            {
                this.__lastJackpotQuery.forEach((jp) => subDump.add(objectify(jp)));
            }
        }

        @RS.Callback
        protected handleJackpotQueryResponse(response: ReadonlyArray<GCM.Jackpot> | GCM.JackpotQueryFailure)
        {
            this.__lastJackpotQuery = response;
        }

        //#endregion

        protected handleGameReady()
        {
            const dump = RS.IDump.get();
            if (dump.enabled)
            {
                const subDump = dump.getSubDump("GDM");
                subDump.add(this.getDumpObject("gameReady"));
            }
            this.__gcmWrapper.gameReady();
        }

        /**
         * Adds a GDM server message to the dump.
         * @param type High level type of message (e.g. "request" or "response").
         * @param message Message text.
         */
        protected dumpServerMessage(type: string, message: string)
        {
            const dump = RS.IDump.get();
            if (!dump.enabled) { return; }
            const subDump = dump.getSubDump("GDM");
            subDump.add({ ...this.getDumpObject(type), "message": message });
        }

        /**
         * Adds a GDM wrapper API call to the dump.
         * @param type High level type of message (e.g. "request" or "response").
         * @param message Message text.
         */
        protected dumpWrapperCall(type: string, direction: "in" | "out", data?: number | boolean | string | void | null | undefined)
        {
            const dump = RS.IDump.get();
            if (!dump.enabled) { return; }
            const subDump = dump.getSubDump("GDM");
            subDump.add({ ...this.getDumpObject(type), "direction": direction, "data": data });
        }

        protected getDumpObject(type: string)
        {
            const time = new Date();
            const dateTime = RS.Dump.getTimestamp(time);
            const stamp = time.getTime();
            return { "type": type, "datetime": dateTime, "timestamp": stamp };
        }
    }

    RS.IPlatform.register(Platform);

    type ValueChangedBindingMap = { [P in OGS.GDM.IWrapper.ValueKey]?: RS.IDisposable; };

    /** Map of special GDM jurisdictions. */
    const jurisdictionMap: { [gdmJurisdiction: string]: string } =
    {
        "UK": "GB"
    };
}
