/// <reference path="../generated/Translations.ts"/>
/// <reference path="ToggleButton.ts" />

namespace RS.GDM.Environment.Settings
{
    import Flow = RS.Flow;
    import Localisation = RS.Localisation;

    export class Dialog extends Flow.BaseElement<Dialog.Settings>
    {
        protected _onCloseRequested = RS.createSimpleEvent();
        public get onCloseRequested() { return this._onCloseRequested.public; }

        constructor(settings: Dialog.Settings)
        {
            super(settings, null);

            Flow.background.create(settings.background, this);
            Flow.label.create(settings.title, this);

            const closeButton = Flow.button.create(settings.closeButton, this);
            closeButton.onClicked(() => this._onCloseRequested.publish());
        }

        public addSwitch(name: Localisation.LocalisableString, defaultState: boolean): Dialog.ISetting<boolean>
        {
            const el = toggleButton.create(this.settings.toggleButton, { defaultState });
            this.addSetting(name, el);
            return el;
        }

        public addButton(name: Localisation.LocalisableString): RS.Flow.Button
        {
            const el = RS.Flow.button.create(this.settings.clickButton, this);
            if (el.label) { el.label.text = name; }
            this.invalidateLayout({ reason: "addButton" });
            return el;
        }

        protected addSetting(name: Localisation.LocalisableString, element: Flow.GenericElement)
        {
            const title = Flow.titled.create(this.settings.settingTitle, element, this);
            title.label.text = name;
            this.invalidateLayout({ reason: "addSetting" });
        }
    }

    export namespace Dialog
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            title: Flow.Label.Settings;
            settingTitle: Flow.Titled.Settings;
            toggleButton: ToggleButton.Settings;
            clickButton: Flow.Button.Settings;
            background: Flow.Background.Settings;
            closeButton: Flow.Button.Settings;
        }

        export interface ISetting<T>
        {
            readonly onChanged: RS.IEvent<T>;
        }

        export const defaultSettings: Settings =
        {
            background: Flow.Background.defaultSettings,

            closeButton:
            {
                ...Flow.Button.defaultSettings,
                textLabel:
                {
                    ...Flow.Label.defaultSettings,

                    dock: Flow.Dock.Fill,
                    sizeToContents: true,
                    expand: Flow.Expand.Allowed,
                    text: Translations.Settings.Close
                },

                sizeToContents: true,
                dock: RS.Flow.Dock.Bottom,
                expand: Flow.Expand.HorizontalOnly
            },

            title:
            {
                ...Flow.Label.defaultSettings,
                text: Translations.Settings.Title,
                fontSize: 32,

                sizeToContents: true,
                dock: Flow.Dock.Top,
                expand: Flow.Expand.HorizontalOnly
            },

            toggleButton:
            {
                ...ToggleButton.defaultSettings,

                sizeToContents: true,
                dock: Flow.Dock.Right,
                expand: Flow.Expand.HorizontalOnly
            },
            clickButton:
            {
                ...RS.Flow.Button.defaultSettings,

                sizeToContents: true,
                dock: Flow.Dock.Top,
                expand: Flow.Expand.HorizontalOnly
            },

            settingTitle:
            {
                title:
                {
                    ...Flow.Label.defaultSettings,
                    fontSize: 24,

                    sizeToContents: true,
                    expand: Flow.Expand.HorizontalOnly
                },
                dock: Flow.Dock.Top,
                side: Flow.Titled.Side.Left,
                sizeToContents: true,
                spacing: Flow.Spacing.horizontal(5)
            },

            spacing: Flow.Spacing.all(10),

            sizeToContents: true,
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },

            scaleFactor: RS.Device.isMobileDevice ? 2 : 1
        };
    }

    export const dialog = Flow.declareElement(Dialog, Dialog.defaultSettings);
}