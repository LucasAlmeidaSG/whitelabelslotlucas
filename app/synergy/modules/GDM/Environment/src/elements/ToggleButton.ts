/// <reference path="../generated/Translations.ts"/>

namespace RS.GDM.Environment.Settings
{
    import Flow = RS.Flow;
    import Localisation = RS.Localisation;

    export class ToggleButton extends Flow.BaseElement<ToggleButton.Settings, ToggleButton.RuntimeData> implements Dialog.ISetting<boolean>
    {
        protected _onChanged = RS.createEvent<boolean>();
        public get onChanged() { return this._onChanged.public; }

        protected _activeButton: Flow.Button;
        protected _inactiveButton: Flow.Button;

        private __active: boolean;
        public get active() { return this.__active; }
        public set active(value)
        {
            if (this.__active === value) { return; }
            this.__active = value;
            this._activeButton.enabled = this._activeButton.visible = value;
            this._inactiveButton.enabled = this._inactiveButton.visible = !value;
            this._onChanged.publish(value);
        }

        public get enabled() { return this.__active ? this._activeButton.enabled : this._inactiveButton.enabled; }
        public set enabled(enabled)
        {
            if (enabled)
            {
                this._activeButton.enabled = this.__active;
                this._inactiveButton.enabled = !this.__active;
            }
            else
            {
                this._activeButton.enabled = false;
                this._inactiveButton.enabled = false;
            }
        }

        constructor(settings: ToggleButton.Settings, runtimeData: ToggleButton.RuntimeData)
        {
            super(settings, runtimeData);

            this._activeButton = Flow.button.create(settings.active, this);
            this._inactiveButton = Flow.button.create(settings.inactive, this);

            this._activeButton.onClicked(() => { this.active = false; })
            this._inactiveButton.onClicked(() => { this.active = true; })

            this.active = runtimeData.defaultState;
        }
    }

    export namespace ToggleButton
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            inactive: Flow.Button.Settings;
            active: Flow.Button.Settings;
        }

        export interface RuntimeData
        {
            defaultState: boolean;
        }

        export const defaultSettings: Settings =
        {
            inactive:
            {
                ...Flow.Button.defaultSettings,
                textLabel:
                {
                    ...Flow.Label.defaultSettings,
                    text: Translations.Settings.Inactive,

                    dock: Flow.Dock.Fill,
                    expand: Flow.Expand.Allowed
                },

                sizeToContents: true,
                dock: RS.Flow.Dock.Fill
            },
            active:
            {
                ...Flow.Button.defaultSettings,
                textLabel:
                {
                    ...Flow.Label.defaultSettings,
                    text: Translations.Settings.Active,

                    dock: Flow.Dock.Fill,
                    expand: Flow.Expand.Allowed
                },

                sizeToContents: true,
                dock: RS.Flow.Dock.Fill
            }
        };
    }

    export const toggleButton = Flow.declareElement(ToggleButton, true);
}