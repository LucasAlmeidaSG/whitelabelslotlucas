
namespace RS.GDM.Environment
{
    const locales: string[] =
    [
        "bg_BG",
        "ca_ES",
        "cs_CZ",
        "da_DK",
        "de_DE",
        "el_GR",
        "en_GB",
        "es_ES",
        "et_EE",
        "fi_FI",
        "fr_FR",
        "hr_HR",
        "hu_HU",
        "it_IT",
        "lt_LT",
        "lv_LV",
        "nl_NL",
        "no_NO",
        "pl_PL",
        "pt_PT",
        "ro_RO",
        "ru_RU",
        "sk_SK",
        "sl_SI",
        "sv_SE",
        "tr_TR"
    ];

    const context = Logging.getContext("GDM", "DummyWrapper");
    const DebugLog = RS.DebugLog(context);
    const storageKey = "GDM/DummyWrapper/";

    interface IDumpEntry
    {
        "type": string;
        "timestamp": number;
        "message": string;
    }

    interface IDumpFile extends Map<any[]>
    {
        "GDM"?: IDumpEntry[];
    }

    @HasCallbacks
    export class DummyWrapper implements OGS.GDM.IWrapper
    {
        public readonly jackpotService = new DummyJackpotService();
        public get dumpFile(): PromiseLike<Map<any[]>> { return this._dumpFile; }

        private readonly _gcmFeatures: { [featureName: string]: DummyWrapper.IFeature } = {};

        // GDM playback data
        private _dumpFile: PromiseLike<IDumpFile>;
        private _replayMessages: PromiseLike<string[]>;
        private _replayIndex: number = -1;

        private __initialised = false;
        private __balance: number;
        private __win: number;

        private readonly __game = Environment.globals.map;

        private __pollJackpotsCallback?: OGS.GDM.IGame.JackpotUpdateHandler;
        private __pollJackpotsErrorCallback?: OGS.GDM.IGame.JackpotUpdateErrorHandler;
        private __pollCurrency: string;

        //#region IWrapper.properties

        public get languageCode(): string
        {
            return URL.getParameter("locale", "en_GB", this.decodeLocale);
        }

        public get quality()
        {
            return "hq";
        }

        constructor()
        {
            this._dumpFile = this.loadDumpFile();
            this._replayMessages = this.loadDumpReplay();
        }

        @DebugLog
        public updateProgress(value: number, max: number): void
        {
            return;
        }

        @DebugLog
        public async sendMsgToServer(request: string)
        {
            // Find message type for response memory
            const typeMatch = request.match(/(?:[&"]MSGID(?:=|":\s*"))([A-Za-z_\-0-9]+)/);
            const type = typeMatch && typeMatch[1] || "DEFAULT";
            if (type === "DEFAULT") { logger.warn(`Failed to resolve type for response memory`); }

            if (this._replayMessages)
            {
                const messages = await this._replayMessages;
                if (++this._replayIndex == messages.length)
                {
                    logger.debug("Dump playback finished");
                    this._replayMessages = null;
                    this._replayIndex = -1;
                }
                else
                {
                    const playbackResponse = messages[this._replayIndex];
                    this.__game.processServerMsg(playbackResponse);
                    return;
                }
            }

            const response = prompt(`Wrapper/PromptHistory/${type}`, `Message received: ${request}\nEnter response:`);
            this.__game.processServerMsg(response);
        }

        @DebugLog
        public getGCMFeatures(): Readonly<OGS.GDM.IWrapper.FeatureMap>
        {
            return this._gcmFeatures;
        }

        //#endregion

        //#region IWrapper.methods

        public isFreeMode()
        {
            return URL.getParameterAsBool("freeplay", false);
        }

        @DebugLog
        public pollJackpots(
            currency: string, jackpots?: ReadonlyArray<GCM.JackpotInstanceID>,
            pollJackpotsCallback?: OGS.GDM.IGame.JackpotUpdateHandler, pollJackpotsErrorCallback?: OGS.GDM.IGame.JackpotUpdateErrorHandler): void
        {
            this.__pollJackpotsCallback = pollJackpotsCallback;
            this.__pollJackpotsErrorCallback = pollJackpotsErrorCallback;
            this.__pollCurrency = currency;

            if (jackpots)
            {
                for (const id of jackpots)
                {
                    this.jackpotService.registerJackpot(id);
                }
            }

            Ticker.registerTickers(this);
        }

        @DebugLog
        public getCustomSettings(): Readonly<OGS.GDM.IWrapper.CustomSettings>
        {
            return {
                opid: URL.getParameter("opid", "5"),
                jurisdiction: URL.getParameter("jurisdiction", "uk"),
                gameName: URL.getParameter("gamename", "mygame"),
                languageCode: this.languageCode,
                quickStop: URL.getParameterAsBool("quickstop", true),
                minSpinTime: URL.getParameterAsNumber("spintime"),
                buyPass: URL.getParameterAsBool("buypass", true),
                gambleCount: URL.getParameterAsNumber("gamblecount", 999999)
            };
        }

        /** Notifies the wrapper that your game has loaded all assets and is ready to be played. This should be called after all updateProgress() calls. */
        @DebugLog
        public gameReady(): void
        {
            this.__game.apiExt("TEMP_PAUSE_AUTOPLAY", false);
            this.__game.apiExt("SHOW_HOME_BUTTON", false);
            this.__game.apiExt("SHOW_CASHIER_BUTTON", URL.getParameter("depositurl") != null);
            this.__game.apiExt("SET_AUTOPLAY_STOP_OPTIONS", URL.getParameterAsEnumVal(OGS.GDM.IGame.AutoplayStopOptions, "gdmautoplay", OGS.GDM.IGame.AutoplayStopOptions.LimitLossAndSingleWin));
            this.__game.apiExt("SET_MAX_AUTOPLAYS", URL.getParameterAsNumber("gdmmaxautoplays", 100));
        }

        /** Get the size the game should be (Game should not get this any other way) */
        @DebugLog
        public getSize(): Readonly<{ w: number, h: number }>
        {
            // return RS.Viewport.dimensions;
            return { w: 0, h: 0 };
        }

        /** Sets a value for a game property */
        public valueChanged(key: OGS.GDM.IWrapper.NumericValueKey, value: number): void;
        public valueChanged(key: OGS.GDM.IWrapper.BooleanValueKey, value: boolean): void;
        @DebugLog
        public valueChanged(key: OGS.GDM.IWrapper.ValueKey, value: number | boolean): void
        {
            switch (key)
            {
                case "MUTE":
                {
                    Storage.save(Path.combine(storageKey, "Muted"), value);
                    return;
                }
                case "BALANCE":
                {
                    if (!this.__initialised) { this.initialise(); }
                    this.__balance = value as number;
                    return;
                }
                case "TOTAL_WIN":
                {
                    this.__win = value as number;
                    return;
                }
            }
        }

        /** PokerStars only, gets if the ingame audio options should be shown. */
        @DebugLog
        public audioBtnVisibility(): boolean
        {
            return true;
        }

        /** Called when game enters UKGC autoplay, supply -1 for values not defined/limited */
        @DebugLog
        public autoPlayNotification(spins: number, losslimit: number, singlewinlimit: number): void
        {
            return;
        }

        /** To be called when processServerMessage is an error message */
        @DebugLog
        public delegatedErrorHandling(errorType: string, errorMsg: string, server: boolean): void
        {
            return;
        }

        @DebugLog
        public gameButtonPressed(buttonID: "CASHIER"): void
        {
            return;
        }

        @DebugLog
        public homeButtonPressed(): void
        {
            return;
        }

        //#endregion

        /** Register a new dummy GCM feature. */
        public registerGCMFeature(name: string, value: DummyWrapper.IFeature)
        {
            if (!name) { throw new Error(`Cannot register "${name}" as a GCM feature`); }
            if (!value) { throw new Error(`Cannot register '${value}' as a GCM feature`); }
            if (this._gcmFeatures[name]) { throw new Error(`Feature '${name}' already registered`); }
            this._gcmFeatures[name] = value;
        }

        @Tick({ kind: Ticker.Kind.FixedStep, data: 1000 })
        public tick(): void
        {
            const query = this.jackpotService.queryJackpots(this.__pollCurrency);
            if (query.length === 0)
            {
                this.__pollJackpotsErrorCallback({ "errors": [{ "code": "NoJackpotsFound", "message": "No jackpots found" }] });
            }
            else
            {
                this.__pollJackpotsCallback(query);
            }
        }

        private loadDumpFile(url?: string): PromiseLike<IDumpFile> | null
        {
            url = url || URL.getParameter("gdmdumpfile");
            if (!url) { return null; }

            if (Read.boolean(url, false))
            {
                return promptFile("Provide a dump file")
                    .then<IDumpFile>((file) => new Promise((resolve, reject) =>
                    {
                        const reader = new FileReader()
                        reader.onerror = reject;
                        reader.onload = function (ev)
                        {
                            try
                            {
                                if (!Is.string(ev.target.result)) { throw new Error("File must be text-type"); }
                                resolve(JSON.parse(ev.target.result));
                            }
                            catch (err)
                            {
                                reject(err);
                            }
                        };
                        reader.readAsText(file)
                    }));
            }

            return Request
                .ajax(
                {
                    url,
                    method: "GET"
                }, Request.AJAXResponseType.JSON) as PromiseLike<IDumpFile>;
        }

        private loadDumpReplay(): PromiseLike<string[]>
        {
            if (!this._dumpFile) { return null; }
            return this._dumpFile.then((dumpFile) =>
            {
                const key = "GDM";
                if (dumpFile && Is.array(dumpFile[key]))
                {
                    return dumpFile[key].filter((x) => x.type === "response").map((x) => x.message);
                }
                else
                {
                    return [];
                }
            });
        }

        private initialise()
        {
            this.__initialised = true;

            this.__balance = 0;
            this.__win = 0;

            for (const name in this._gcmFeatures)
            {
                this._gcmFeatures[name].initialise();
            }

            ITicker.get().after(() =>
            {
                // Reload mute state
                const muted = Storage.load(Path.combine(storageKey, "Muted"), false);
                this.__game.apiExt("SET_MUTE", muted);

                // Activate UI straight away
                this.__game.apiExt("ENABLE_ALL_UI", true);
            }, 0);
        }

        @Callback
        private decodeLocale(locale: string): string
        {
            if (locale === "ra_ND")
            {
                const index: number = Math.generateRandomInt(0, locales.length);
                const randomLocale: string = locales[index];

                Log.info(`Random locale - ${randomLocale}`);

                return randomLocale;
            }

            return locale;
        }
    }

    export namespace DummyWrapper
    {
        export interface IFeature
        {
            initialise(): void;
        }
    }
}