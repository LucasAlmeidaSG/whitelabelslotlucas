namespace RS.GDM.Environment
{
    export interface IDummyJackpot
    {
        active: boolean;
        query(currency: string, exchangeRate: number): GCM.Jackpot;
    }

    export namespace IDummyJackpot
    {
        export function is(x: any): x is IDummyJackpot
        {
            return RS.Is.object(x) && RS.Is.func(x.query) && RS.Is.boolean(x.active);
        }
    }

    export class DummyJackpotService
    {
        private readonly __jackpots: RS.Map<IDummyJackpot> = {};

        /** Registers a new dummy jackpot. */
        public registerJackpot(id: string, jackpot: IDummyJackpot): void;
        /** Registers a new dummy jackpot. */
        public registerJackpot(id: string, settings?: Partial<DummyJackpot.Settings>): void;
        public registerJackpot(id: string, p2?: IDummyJackpot | Partial<DummyJackpot.Settings>): void
        {
            if (this.__jackpots[id]) { return logger.warn(`Jackpot '${id}' already exists`); }
            const jackpot = IDummyJackpot.is(p2) ? p2 : new DummyJackpot(id, p2);
            this.__jackpots[id] = jackpot;
            jackpot.active = true;
        }

        /** Query a dummy jackpot. */
        public queryJackpot(id: string, currency?: string, exchangeRate?: number)
        {
            const jackpot = this.__jackpots[id];
            if (!jackpot)
            {
                throw new Error(`Unrecognised jackpot '${id}'`)
            }
            return jackpot.query(currency, exchangeRate);
        }

        public queryJackpots(currency?: string, exchangeRate?: number): GCM.Jackpot[]
        {
            const jackpots: GCM.Jackpot[] = [];
            for (const id in this.__jackpots)
            {
                const jackpot = this.__jackpots[id];
                jackpots.push(jackpot.query(currency, exchangeRate));
            }
            return jackpots;
        }
    }

    export class DummyJackpot
    {
        private readonly __contributionFunction: DummyJackpot.ContributionFunction;

        private __active: boolean;
        public get active() { return this.__active; }
        public set active(active)
        {
            if (this.__active === active) { return;}
            if (active)
            {
                logger.debug(`DummyJackpot '${this.__id}' active`);
                RS.Ticker.registerTickers(this);
            }
            else
            {
                logger.debug(`DummyJackpot '${this.__id}' inactive`);
                RS.Ticker.unregisterTickers(this);
            }
            this.__active = active;
        }

        private __balance: number;
        private __id: string;
        private __instance: string;
        private __currency: string;

        private __dropValue: number;
        private __seedValue: number;

        constructor(id: string, settings?: Partial<DummyJackpot.Settings>)
        {
            settings = settings ? {...DummyJackpot.defaultSettings, ...settings } : DummyJackpot.defaultSettings;

            this.__id = id || "";
            this.__seedValue = settings.seedValue || 1;
            this.__dropValue = settings.dropValue || Infinity;
            this.__balance = settings.initialValue || this.__seedValue;
            this.__currency = settings.currency || DummyJackpot.defaultSettings.currency,
            this.__instance = settings.instance || this.generateInstance();
            this.__contributionFunction = settings.contributionFunction || this.getDefautContributionFunction(settings as DummyJackpot.Settings);
            this.__active = false;
        }

        public query(currency: string = this.__currency, exchangeRate: number = 1): GCM.Jackpot
        {
            const balance = this.__balance / 100;
            return {
                id: this.__id,
                instance: this.__instance,

                balanceAmountInJackpotCurrency: balance,
                jackpotCurrency: this.__currency,

                balanceAmountInRequestedCurrency: balance * exchangeRate,
                requestedCurrency: currency
            };
        }

        public contribute(valueInJackpotCurrency: number)
        {
            if (!RS.Is.number(valueInJackpotCurrency) || valueInJackpotCurrency < 0)
            {
                throw new Error(`Invalid contribution amount '${valueInJackpotCurrency}'`);
            }

            valueInJackpotCurrency = Math.round(valueInJackpotCurrency);
            this.__balance += valueInJackpotCurrency;
            if (this.__balance > this.__dropValue)
            {
                this.trigger();
            }
        }

        public trigger()
        {
            // Generate new instance
            logger.debug(`DummyJackpot '${this.__id}' triggered`);
            this.__instance = this.generateInstance();
            this.__balance = this.__seedValue;
        }

        public tick(delta: number): void;
        public tick(event: RS.Ticker.Event): void;

        @RS.Tick({ kind: RS.Ticker.Kind.FixedStep, data: 1000 })
        public tick(p1: number | RS.Ticker.Event): void
        {
            const delta = RS.Is.number(p1) ? p1 : p1.delta;
            this.onTick(delta);
        }

        protected onTick(delta: number)
        {
            if (this.__contributionFunction == null)
            {
                throw new Error(`Contribution function missing`);
            }
            const contribution = this.__contributionFunction(delta);
            this.contribute(contribution);
        }

        protected getDefautContributionFunction(settings: DummyJackpot.Settings)
        {
            const scale = settings.dropValue / 100000;
            return DummyJackpot.getContributionFunction(5, 100 * scale, 100 * scale);
        }

        protected generateInstance(): string
        {
            return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (char)
            {
                // Thanks https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
                const random = Math.random() * 16 | 0;
                const value = char === "x" ? random : (random & 0x3 | 0x8);
                return value.toString(16);
            });
        }
    }

    export namespace DummyJackpot
    {
        export type ContributionFunction = (millis: number) => number;

        export interface Settings
        {
            currency: string;

            initialValue: number;
            seedValue: number;
            dropValue: number;

            contributionFunction?: ContributionFunction;
            instance?: string;
        }

        export function getContributionFunction(wagersPerSecond = 5, averageWagerStake = 1000, maxVariancePerSecond = 100): ContributionFunction
        {
            return function (millis)
            {
                const flip = Math.random() < 0.5 ? -1 : 1;
                const varianceValue = flip * Math.random() * (maxVariancePerSecond / 1000);
                return Math.max(0, averageWagerStake * (wagersPerSecond / 1000) + varianceValue * millis)
            };
        }

        export const defaultSettings: Settings =
        {
            currency: "GBP",
            dropValue: 100000,
            initialValue: 0,
            seedValue: 0
        };
    }
}