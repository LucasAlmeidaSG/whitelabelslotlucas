namespace RS.GDM.Engine
{
    export const forceToolURLs: RS.Map<string> =
    {
        "https://ogs-jackpots-qa-3-gcm.casino.openbet.com": "https://ogs-jackpots-qa-3-gdm.casino.openbet.com/gdmforcetool",
        "https://ogs-gcm-eu-stage.nyxop.net": "https://ogs-gdm-aws-2pp-int-generic-0.nyxaws.net/forcetool"
    };
}