/** Contains functions for dealing with server response data from a GDM engine. */
namespace RS.GDM.Engine.Serialisation
{
    const keyArraySymbol = "{#}";

    //#region SerialiserPair

    /** Converts the given object to a string. The object can be null. */
    export type Serialiser<T> = (object: T) => string;
    /** Converts the given string to an object. */
    export type Deserialiser<T> = (string: string) => T;

    export type SerialiserPair<T> = { serialiser: Serialiser<T>; deserialiser: Deserialiser<T>; };

    /** Constructs a new type serialiser pair using the given serialiser and deserialiser. */
    export function Type<T>(serialiser: Serialiser<T>, deserialiser: Deserialiser<T>): SerialiserPair<T>
    {
        return { serialiser, deserialiser };
    }

    //#endregion

    //#region Decorator

    export interface PropertyData<T>
    {
        /** The GDM key for this property. */
        key: string;
        /** The parser for this property, e.g. Serialisation.Number. */
        type: SerialiserPair<T>;
        /**
         * If true, will search the key for an index marker {#} and substitute for an array index.
         *
         * E.g. RSC_{#} will cause the values of the keys RSC_1 and RSC_3 to be combined into a single array of values with the indices 1 and 3 (skipping 0).
         *
         * If you ever need more than one index marker... then all hope is lost.
         */
        multiKey?: boolean;
        /**
         * Default value to use if not present.
         *
         * Note that the property may still be missing if an error is encountered during deserialisation or serialisation. \
         * Set 'required' true to ensure the value is always present.
         */
        defaultValue?: T;
        /**
         * If true, causes an error to be thrown if the property is not found in the object (during serialisation) or the string (during deserialisation). \
         * For array types, causes an error to be thrown if the array is null or has no members. \
         * Also causes serialisation and deserialisation to fail completely if any other errors are encountered whilst parsing it, rather than just skipping this property.
         *
         * Does not throw if 'defaultValue' is set and the property is simply missing.
         *
         * Defaults to false.
         */
        required?: boolean;
        /**
         * If true, causes any errors encountered whilst serialising or deserialising this property not to be logged to the console.
         *
         * Handy when defining a property multiple times with different typing.
         */
        silent?: boolean;
    }

    export interface PropertyDataIsNotKeyArray<T> extends PropertyData<T>
    {
        multiKey?: false;
    }

    export interface PropertyDataIsKeyArray<T> extends PropertyData<T>
    {
        multiKey: true;
    }

    const property = RS.Decorators.Tag.create<PropertyData<any>>(RS.Decorators.TagKind.Property);
    // Validate only once
    property.onApplied(({ val }) =>
    {
        if (!val.key)
        {
            throw new Error(`Invalid GDM key ${RS.Dump.stringify(val.key)}`);
        }

        if (!val.type || !RS.Is.func(val.type.deserialiser) || !RS.Is.func(val.type.serialiser))
        {
            throw new Error(`Property "${val.key}" has invalid type ${RS.Dump.stringify(val.type)}`);
        }
    });

    /** A strictly-typed PropertyDecorator. */
    export type PropertyDecorator<P> = <T extends Partial<Record<K, P>>, K extends string>(target: T, prop: K) => void;

    /** The type of Property. */
    export type PropertyTag =
    {
        <T>(data: PropertyDataIsNotKeyArray<T>): PropertyDecorator<T>;
        <T>(data: PropertyDataIsKeyArray<T>): PropertyDecorator<T[]>;
    }
    /** Declares a property to be serialised and deserialised. */
    export const Property: PropertyTag = property;

    //#endregion

    //#region Serialisation

    /** Serialisation format to use. */
    export enum Format
    {
        /** GDM's beautifully crafted bespoke message format. Used by professional developers. */
        Message,
        /** Boring old JuvenileScript Object Notation. Used by amateur script kiddies. */
        JSON,

        /** The default format is Message. */
        Default = Message
    }

    /** Converts an object to a GDM API string, optionally using an explicitly specified format. */
    export function serialise<T extends object>(obj: T, format = Format.Default): string
    {
        const stringMap: RS.Map<string> = {};
        serialiseToStringMap(obj, stringMap);

        switch (format)
        {
            case Format.JSON:
                return JSON.stringify(stringMap);

            case Format.Message:
                return MessageParser.jsonToMessage(stringMap);

            default:
                throw new Error(`Unrecognised format '${format}'`);
        }
    }

    /** Converts a GDM API string to an object, optionally using an explicitly specified format. */
    export function deserialise<T extends object>(obj: T, data: string, format?: Format): void
    {
        const stringMap = deserialiseToStringMap(data, format);
        deserialiseWithStringMap(obj, stringMap);
    }

    /** Serialises the given object into an intermediate string map. Counterpart of deserialiseWithStringMap. */
    function serialiseToStringMap(obj: object, map: RS.Map<string>): void
    {
        const props = property.get(obj, RS.Decorators.MemberType.Instance);
        for (const propName in props)
        {
            const propData = props[propName];
            if (!propData)
            {
                continue;
            }

            try
            {
                let propValue = obj[propName];
                if (propValue == null)
                {
                    if (propData.defaultValue != null)
                    {
                        propValue = propData.defaultValue;
                    }
                    else if (propData.required)
                    {
                        throw new Error("Property not set");
                    }
                    else
                    {
                        continue;
                    }
                }

                const key = propData.key;
                if (propData.multiKey)
                {
                    if (propValue == null)
                    {
                        continue;
                    }

                    if (!RS.Is.array(propValue))
                    {
                        throw new Error(`A property with a declared ${keyArraySymbol} must be an array`);
                    }

                    for (const propValueKey in propValue)
                    {
                        const thisKey = key.replace(keyArraySymbol, propValueKey);
                        try
                        {
                            map[thisKey] = propData.type.serialiser(propValue[propValueKey]);
                        }
                        catch (err)
                        {
                            const val = propValue[propValueKey];
                            const str = RS.Is.primitive(val) || RS.Is.object(val) ? RS.Dump.stringify(val) : `${val}`;
                            throw new RS.NestedError(`Could not serialise '${str}'`, err);
                        }
                    }
                }
                else
                {
                    try
                    {
                        map[key] = propData.type.serialiser(propValue);
                    }
                    catch (err)
                    {
                        const val = propValue;
                        const str = RS.Is.primitive(val) || RS.Is.object(val) ? RS.Dump.stringify(val) : `${val}`;
                        throw new RS.NestedError(`Could not serialise '${str}'`, err);
                    }
                }
            }
            catch (err)
            {
                const msg = getErrorSummary(propName, propData, false);
                if (propData.required)
                {
                    throw new RS.NestedError(msg, err);
                }
                else if (!propData.silent)
                {
                    logger.error(msg, err);
                }
            }
        }
    }

    /** Deserialises the given intermediate string map into the given object. Counterpart of serialiseToStringMap. */
    function deserialiseWithStringMap(obj: object, map: RS.Map<string>)
    {
        const props = property.get(obj, RS.Decorators.MemberType.Instance);
        for (const propName in props)
        {
            const propData = props[propName];
            if (!propData)
            {
                continue;
            }

            try
            {
                const key = propData.key;
                if (propData.multiKey)
                {
                    const keyArraySymbolIdx = key.indexOf(keyArraySymbol);
                    const before = key.substr(0, keyArraySymbolIdx);
                    const after = key.substr(keyArraySymbolIdx + keyArraySymbol.length);

                    const arr = obj[propName] = [];
                    for (const mapKey in map)
                    {
                        const mapKeyBefore = mapKey.substr(0, keyArraySymbolIdx);
                        if (mapKeyBefore !== before) { continue; }

                        const mapKeyIdx = parseInt(mapKey.substr(keyArraySymbolIdx));
                        if (!RS.Is.number(mapKeyIdx)) { continue; }

                        const mapKeyAfter = mapKey.substr(keyArraySymbolIdx + `${mapKeyIdx}`.length);
                        if (mapKeyAfter !== after) { continue; }

                        const strValue = map[mapKey];
                        try
                        {
                            arr[mapKeyIdx] = propData.type.deserialiser(strValue);
                        }
                        catch (err)
                        {
                            throw new RS.NestedError(`Could not deserialise '${strValue}'`, err);
                        }
                    }

                    if (propData.required && arr.length === 0)
                    {
                        throw new Error("Property not found in message");
                    }
                }
                else
                {
                    if (key in map)
                    {
                        const strValue = map[key];
                        try
                        {
                            obj[propName] = propData.type.deserialiser(strValue);
                        }
                        catch (err)
                        {
                            throw new RS.NestedError(`Could not deserialise '${strValue}'`, err);
                        }
                    }
                    else
                    {
                        if (propData.defaultValue != null)
                        {
                            obj[propName] = propData.defaultValue;
                        }
                        else if (propData.required)
                        {
                            throw new Error("Property not found in message");
                        }
                        else
                        {
                            obj[propName] = null;
                        }
                    }
                }
            }
            catch (err)
            {
                const msg = getErrorSummary(propName, propData, true);
                if (propData.required)
                {
                    throw new RS.NestedError(msg, err);
                }
                else if (!propData.silent)
                {
                    logger.error(msg, err);
                    obj[propName] = null;
                }
            }
        }
    }

    function getErrorSummary(propName: string, propData: PropertyData<any>, deserialisation: boolean)
    {
        const prop = propData.required ? "required property" : "property";
        const verb = deserialisation ? "deserialise" : "serialise";
        return `Failed to ${verb} ${prop} '${propName}' (key '${propData.key}')`;
    }

    /** Deserialises a given GDM API string to an intermediate string map, optionally using an explicitly specified format. */
    function deserialiseToStringMap(data: string, format: Format = inferFormat(data)): RS.Map<string>
    {
        switch (format)
        {
            case Format.JSON:
                const result = JSON.parse(data);
                if (!RS.Is.object(result)) { throw new Error(`Invalid JSON message '${result}'`); }
                return result;

            case Format.Message:
                return MessageParser.messageToJSON(data);

            default:
                throw new Error(`Unrecognised format '${format}'`);
        }
    }

    /** Infers the format of a given GDM API string. */
    function inferFormat(data: string): Format
    {
        if (data[0] === "{")
        {
            return Format.JSON;
        }

        return Format.Message;
    }

    //#endregion

    //#region Types

    function createTypeError(val: any, type: string)
    {
        const str = RS.Is.primitive(val) || RS.Is.object(val) ? RS.Dump.stringify(val) : `${val}`;
        return new TypeError(`${str} is not a valid ${type}`);
    }

    function createParseError(str: string, type: string)
    {
        return new Error(`Failed to read "${str}" as ${type}`);
    }

    /** Specifies that the property is to be serialised and deserialised as a string. */
    export const String = Type<string>((str) => `${str}`, (str) => str);

    /** Specifies that the property is to be serialised and deserialised as a number. */
    export const Number = Type<number>(
        function (num)
        {
            if (!RS.Is.number(num))
            {
                throw createTypeError(num, "number");
            }

            return `${num}`;
        },
        function (str)
        {
            if (str.trim() === "")
            {
                throw createParseError(str, "number");
            }

            const num = RS.Read.number(str, null);
            if (!RS.Is.number(num))
            {
                throw createParseError(str, "number");
            }

            return num;
        });

    /** Specifies that the property is to be serialised and deserialised as an integer. */
    export const Integer = Type<number>(
        function (num)
        {
            if (!RS.Is.integer(num))
            {
                throw createTypeError(num, "integer");
            }

            return `${num}`;
        },
        function (str)
        {
            if (str.trim() === "")
            {
                throw createParseError(str, "integer");
            }

            const int = RS.Read.integer(str, null);
            if (int == null)
            {
                throw createParseError(str, "integer");
            }

            return int;
        });

    /**
     * Specifies that the property is to be serialised and deserialised as a boolean.
     * More customisable than the basic Boolean.
     */
    export function BooleanEx(settings?: Partial<BooleanEx.Settings>): SerialiserPair<boolean>
    {
        settings = settings ? { ...BooleanEx.defaultSettings, ...settings } : BooleanEx.defaultSettings;

        // Ensure comparisons are only done between lower-case strings
        settings.acceptedAsFalse = settings.acceptedAsFalse.map((str) => str.toLowerCase());
        settings.acceptedAsTrue = settings.acceptedAsTrue.map((str) => str.toLowerCase());

        return {
            serialiser: (bool) =>
            {
                if (bool === true) { return settings.serialiseTrueAs; }
                if (bool === false) { return settings.serialiseFalseAs; }

                throw createTypeError(bool, "boolean");
            },
            deserialiser: (str) =>
            {
                const num = RS.Read.number(str, null);
                if (num != null) { return num !== 0; }

                const flat = str.toLowerCase();
                if (settings.acceptedAsTrue.indexOf(flat) !== -1) { return true; }
                if (settings.acceptedAsFalse.indexOf(flat) !== -1) { return false; }

                throw createParseError(str, "boolean");
            }
        };
    }

    export namespace BooleanEx
    {
        export interface Settings
        {
            serialiseTrueAs: string;
            serialiseFalseAs: string;
            acceptedAsTrue: string[];
            acceptedAsFalse: string[];
        }

        export const defaultSettings: Settings =
        {
            serialiseTrueAs: "1",
            serialiseFalseAs: "0",
            acceptedAsTrue: [ "true", "y", "yes" ],
            acceptedAsFalse: [ "false", "n", "no" ]
        };
    }

    /** Specifies that the property is to be serialised and deserialised as a boolean. */
    export const Boolean = BooleanEx();

    /** Specifies that the property is to be serialised and deserialised as an array of another type. */
    export function ArrayOf<T>(inner: SerialiserPair<T>, settings?: Partial<Array.Settings>): SerialiserPair<T[]>
    {
        const fullSettings: Array.Settings = settings ? { ...Array.defaultSettings, ...settings } : Array.defaultSettings;
        return {
            serialiser: (arr) =>
            {
                if (!RS.Is.array(arr))
                {
                    throw createTypeError(arr, "boolean");
                }

                const strVals = arr.map((val) => inner.serialiser(val));
                return toArrayStr(strVals, fullSettings);
            },
            deserialiser: (str) =>
            {
                const strVals = toArrayStrVals(str, fullSettings);
                return strVals.map((val) => inner.deserialiser(val));
            }
        };
    }

    /** Specifies that the property is to be serialised from and deserialised into an object in tuple format (i.e. a fixed-length variable-type array). */
    export function Tuple<T extends object>(ctor: RS.Constructor<T>, settings?: Partial<Array.Settings>): SerialiserPair<T>
    {
        const fullSettings: Array.Settings = settings ? { ...Array.defaultSettings, ...settings } : Array.defaultSettings;
        return {
            serialiser: (obj) =>
            {
                if (!RS.Is.object(obj))
                {
                    throw createTypeError(obj, "object");
                }

                const strVals: string[] = [];
                serialiseToStringMap(obj, strVals as any);
                return toArrayStr(strVals, fullSettings);
            },
            deserialiser: (str) =>
            {
                const obj = new ctor();
                const strVals = toArrayStrVals(str, fullSettings);
                deserialiseWithStringMap(obj, strVals as any);
                return obj;
            }
        };
    }

    function toArrayStr(strVals: string[], settings: Array.Settings)
    {
        return join(withIndex(strVals, settings), settings);
    }

    function toArrayStrVals(str: string, settings: Array.Settings)
    {
        return reIndexed(split(str, settings), settings);
    }

    /** Joins strings using the given Array.Settings. */
    function join(strings: string[], settings: Array.Settings): string
    {
        switch (settings.delimitStrategy)
        {
            case Array.DelimitStrategy.Infix:
                return strings.join(settings.delimiter);

            case Array.DelimitStrategy.Prefix:
                return strings.map((x) => `${settings.delimiter}${x}`).join("");

            case Array.DelimitStrategy.Postfix:
                return strings.map((x) => `${x}${settings.delimiter}`).join("");

            default:
                throw new Error(`Invalid strategy ${settings.delimitStrategy}`);
        }
    }

    /** Splits strings using the given Array.Settings. */
    function split(str: string, settings: Array.Settings): string[]
    {
        const vals = str.split(settings.delimiter);
        switch (settings.delimitStrategy)
        {
            case Array.DelimitStrategy.Infix:
                return vals;

            case Array.DelimitStrategy.Prefix:
                vals.shift();
                return vals;

            case Array.DelimitStrategy.Postfix:
                vals.pop();
                return vals;

            default:
                throw new Error(`Invalid strategy ${settings.delimitStrategy}`);
        }
    }

    /** Re-indexes an array of indexed strings using the given Array.Settings */
    function reIndexed(strings: string[], settings: Array.Settings): string[]
    {
        if (!settings.indexed) { return strings; }
        const result: string[] = [];
        for (const str of strings)
        {
            const idx = str.indexOf(settings.indexDelimiter);

            if (idx === -1)
            {
                const index = strings.indexOf(str);
                logger.warn(`Index missing from supposedly indexed string ${str}; using existing index ${index}`);
                result[index] = str;
                continue;
            }

            const newIndex = str.substr(0, idx);
            result[newIndex] = str.substr(idx + settings.indexDelimiter.length);
        }
        return result;
    }

    /** Prepends index to an array of indexed strings */
    function withIndex(strings: string[], settings: Array.Settings): string[]
    {
        if (!settings.indexed) { return strings; }
        return strings.map((str, idx) => `${idx}${settings.indexDelimiter}${str}`);
    }

    export namespace Array
    {
        export interface Settings
        {
            /** Item delimiter. Defaults to |. */
            delimiter: string;
            /** Item delimiting strategy. Variety is the spice of life (and message formats). */
            delimitStrategy: DelimitStrategy;
            /** Does it have an index at the start of each element for extra parsing challenge? */
            indexed: boolean;
            /** Index delimiter. Defaults to >. */
            indexDelimiter: string;
        }

        export enum DelimitStrategy
        {
            /** The delimiter is placed between pairs of elements. Used by inferior standard serialisation formats. */
            Infix,
            /** The delimiter is placed after each element, including the last. Used in supreme GDM format (usually). */
            Postfix,
            /** The delimiter is placed before each element, including the first. */
            Prefix
        }

        export const defaultSettings: Settings =
        {
            delimiter: "|",
            delimitStrategy: DelimitStrategy.Postfix,

            // Based on RST values
            indexDelimiter: ">",
            indexed: false
        };
    }

    /** Specifies that the property is to be serialised and deserialised as a map of keys to homogenous values (values of a common type). */
    export function Map<T>(inner: SerialiserPair<T>, settings: Map.Settings): SerialiserPair<RS.Map<T>>
    {
        return {
            serialiser: function (obj)
            {
                if (!RS.Is.object(obj))
                {
                    throw createTypeError(obj, "object");
                }

                const map: RS.Map<string> = {};
                for (const key in obj)
                {
                    map[key] = inner.serialiser(obj[key]);
                }
                return MapParser.mapToString(map, settings);
            },
            deserialiser: function (str)
            {
                const obj: { [key: string]: T } = {};
                const map = MapParser.stringToMap(str, settings);
                for (const key in map)
                {
                    obj[key] = inner.deserialiser(map[key]);
                }
                return obj;
            }
        };
    }

    export namespace Map
    {
        export type Settings = MapParser.Settings;
    }

    /** Specifies that the property is to be serialised and deserialised as an object. */
    export function Object<T extends object>(ctor: RS.Constructor<T>, settings: Object.Settings): SerialiserPair<T>
    {
        return {
            serialiser: function (obj)
            {
                if (!RS.Is.object(obj))
                {
                    throw createTypeError(obj, "object");
                }

                const map: RS.Map<string> = {};
                serialiseToStringMap(obj, map);
                return MapParser.mapToString(map, settings);
            },
            deserialiser: function (str)
            {
                const obj = new ctor();
                const map = MapParser.stringToMap(str, settings);
                deserialiseWithStringMap(obj, map);
                return obj;
            }
        };
    }

    export namespace Object
    {
        export type Settings = MapParser.Settings;
    }

    const windowString = (window as any).String as StringConstructor;
    /** Specifies that the property is to be serialised and deserialised as a character code array. */
    export function charArray(settings: Partial<Array.Settings>): SerialiserPair<string>
    {
        const inner = ArrayOf(String, settings);
        return {
            serialiser: function (str)
            {
                const strVals = `${str}`.split("").map((c) => `${c.charCodeAt(0)}`);
                return inner.serialiser(strVals);
            },
            deserialiser: function (str)
            {
                const strVals = inner.deserialiser(str);
                let result: string = "";
                for (const char of strVals)
                {
                    const num = parseInt(char);
                    if (num >= 0)
                    {
                        result += windowString.fromCharCode(num);
                    }
                    else
                    {
                        result += char;
                    }
                }
                return result;
            }
        }
    };
    /** Specifies that the property is to be serialised and deserialised as a character code array using the default array settings. */
    export const CharArray: typeof charArray & SerialiserPair<string> = charArray as any;
    const defaultCharArray = charArray({});
    CharArray.serialiser = defaultCharArray.serialiser;
    CharArray.deserialiser = defaultCharArray.deserialiser;

    //#endregion
}