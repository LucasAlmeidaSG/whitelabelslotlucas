/** Contains functions for dealing with &GDM=messages& */
namespace RS.GDM.Engine.Serialisation.MessageParser
{
    const mapParserSettings: MapParser.Settings =
    {
        keyValueDelimiter: "&",
        keyValueSeparator: "=",
        start: "&",
        end: "&"
    }

    /** Converts a GDM message into a string map. */
    export function messageToJSON(message: string): RS.Map<string>
    {
        return MapParser.stringToMap(message, mapParserSettings);
    }

    /** Converts a string map into a GDM message. */
    export function jsonToMessage(map: Readonly<RS.Map<string>>): string
    {
        return MapParser.mapToString(map, mapParserSettings);
    }
}