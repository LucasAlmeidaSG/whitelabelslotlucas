/** Contains functions for dealing with arbitrary simple key-value strings. */
namespace RS.GDM.Engine.Serialisation.MapParser
{
    enum ParserState
    {
        ExpectKeyValueDelimiter,
        ExpectKeyValueSeparator
    }

    /** Converts a GDM message into a string map. */
    export function stringToMap(message: string, settings: Settings): RS.Map<string>
    {
        const map: RS.Map<string> = {};

        const start = settings.start ? settings.start.length : 0;
        const end = settings.end ? message.length - settings.end.length : message.length;

        let key: string, buffer = "", state = ParserState.ExpectKeyValueSeparator;
        for (let i = start; i < end; i++)
        {
            const char = message[i];
            switch (state)
            {
                case ParserState.ExpectKeyValueDelimiter:
                {
                    if (char !== settings.keyValueDelimiter)
                    {
                        buffer += char;
                        continue;
                    }

                    if (key) { map[key] = buffer; }
                    key = null;
                    buffer = "";
                    state = ParserState.ExpectKeyValueSeparator;
                    break;
                }

                case ParserState.ExpectKeyValueSeparator:
                {
                    if (char !== settings.keyValueSeparator)
                    {
                        buffer += char;
                        continue;
                    }

                    key = buffer;
                    buffer = "";
                    state = ParserState.ExpectKeyValueDelimiter;
                    break;
                }
            }
        }

        // Consume last key
        if (key) { map[key] = buffer; }

        return map;
    }

    /** Converts a string map into a GDM message. */
    export function mapToString(map: Readonly<RS.Map<string>>, settings: Settings): string
    {
        let message: string = "";

        for (const key in map)
        {
            const value = map[key];

            // TODO: are there escape sequences or some such for these characters?
            if (key.match(settings.keyValueDelimiter) || key.match(settings.keyValueSeparator)) { throw new Error(`Unserialisable JSON key "${key}"`); }
            if (value.match(settings.keyValueDelimiter) || value.match(settings.keyValueSeparator)) { throw new Error(`Unserialisable JSON value "${value}"`); }

            const prefix = message.length === 0
                ? (settings.start || "")
                : settings.keyValueDelimiter;

            message += `${prefix}${key}${settings.keyValueSeparator}${value}`;
        }

        if (settings.end)
        {
            message += settings.end;
        }

        return message;
    }

    export interface Settings
    {
        /** Separates a key and value within a key-value pair. */
        keyValueSeparator: string;
        /** Separates sets of key-value pairs. */
        keyValueDelimiter: string;
        /** Optional initial (head) string. */
        start?: string;
        /** Optional final (tail) string. */
        end?: string;
    }
}