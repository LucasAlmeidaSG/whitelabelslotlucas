namespace RS.GDM.Engine
{
    export const logger = RS.Logging.getContext("GDM", "Engine");

    /** Represents an abstract GDM engine. */
    @RS.HasCallbacks
    export abstract class Base<TModels extends RS.Models, TSettings extends Settings<TModels>> implements RS.Engine.IEngine<TModels, TSettings>
    {
        public readonly settings: TSettings;
        public readonly models: TModels;

        /** TODO can we support the replay tools? */
        public readonly supportsReplay = false;

        // TODO not sure about these events' continued existence?
        /** Published when an init response has been received and fully processed. */
        protected readonly __onInitResponseReceived = RS.createEvent<RS.Engine.IResponse>();
        public get onInitResponseReceived() { return this.__onInitResponseReceived.public; }

        /** Published when a bet response has been received and fully processed. */
        protected readonly __onBetResponseReceived = RS.createEvent<RS.Engine.IResponse>();
        public get onBetResponseReceived() { return this.__onBetResponseReceived.public; }

        /** Published when a close response has been received and fully processed. */
        protected readonly __onCloseResponseReceived = RS.createEvent<RS.Engine.IResponse>();
        public get onCloseResponseReceived() { return this.__onCloseResponseReceived.public; }

        /** The GDM platform to use. */
        protected get platform() { return RS.IPlatform.get() as GDM.Environment.Platform; }

        private __version: RS.Version = null;
        /** Version of the GDM endpoint. */
        public get version() { return this.__version; }

        /** Are we currently recovering? State model is currently slots specific, otherwise /probably/ wouldn't be needed. */
        private __isRecovering = false;

        /** URL for the force tool. */
        private __forceToolURL: string | null = null;
        /** Session ID. */
        private __sessionID: string | null = null;

        private readonly __forceQueue: RS.Engine.ForceResult[] = [];

        constructor(settings: TSettings, models: TModels)
        {
            if (settings == null) { throw new Error("settings cannot be null"); }
            if (models == null) { throw new Error("models cannot be null"); }

            this.settings = { format: Serialisation.Format.Default, ...settings };
            this.models = models;
        }

        /** Initialises the engine and sends an init request. */
        public async init(): Promise<RS.Engine.IResponse>
        {
            const sequence = this.settings.initSequence;
            const payload = this.generateCommonMessagePayload();

            // Wait for currency code
            const currencyCodeHandler = sequence.onCurrencyCodeReceived.once(this.onCurrencyCodeReceived);

            const success = await this.executeSequence(sequence, payload);
            if (success == null) { logger.warn("Sequence did not return a boolean"); }
            if (!success) { this.makeFatal(); }

            // Stop respecting sequence currency code event, in case an error occurred before the one-time listener could be published
            currencyCodeHandler.dispose();
            if (this.platform.currencyCode == null) { logger.warn(`Init message sequence did not publish onCurrencyCodeReceived`); }

            const response: RS.Engine.IResponse = { isError: !success, request: null };
            if (success)
            {
                this.__onInitResponseReceived.publish(response);
            }

            return response;
        }

        public async bet(payload: RS.Engine.ILogicRequest.Payload): Promise<RS.Engine.IResponse>
        {
            // Consume next force
            if (!await this.consumeNextForce()) { return { isError: true, request: null }; }

            // Message sequence
            const sequence = this.settings.betSequence;
            const messagePayload = this.generateBetMessagePayload(payload);
            const success = await this.executeSequence(sequence, messagePayload);

            // Response handling
            const response: RS.Engine.IResponse = { isError: !success, request: null };
            if (success)
            {
                this.__onBetResponseReceived.publish(response);
            }

            return response;
        }

        public async close(): Promise<RS.Engine.IResponse>
        {
            // Emulate close request for consistency with other engines.
            this.models.state.state = RS.Models.State.Type.Closed;
            const response: RS.Engine.IResponse = { isError: false, request: null };
            this.__onCloseResponseReceived.publish(response);
            return response;
        }

        public force(forceResult: RS.OneOrMany<RS.Engine.ForceResult>)
        {
            if (!forceResult) { throw new Error(`Force result cannot be null`); }
            if (RS.Is.array(forceResult))
            {
                this.__forceQueue.length = forceResult.length;
                for (let i = 0; i < forceResult.length; i++)
                {
                    const thisResult = forceResult[i];
                    if (!thisResult) { throw new Error(`Force result ${i} cannot be null`); }
                    this.__forceQueue[i] = thisResult;
                }
            }
            else
            {
                this.__forceQueue.length = 1;
                this.__forceQueue[0] = forceResult;
            }
        }

        // TODO make below methods optional?

        public getReplayData(): PromiseLike<RS.Map<any>>
        {
            throw new Error("Unsupported");
        }

        public replay(data: any): void
        {
            throw new Error("Unsupported");
        }

        public getGREGData(): PromiseLike<RS.Map<RS.Engine.GREGData>>
        {
            throw new Error("Unsupported");
        }

        /** Generates payload for spin message */
        protected generateBetMessagePayload(payload: RS.Engine.ILogicRequest.Payload): Engine.BetMessagePayload
        {
            const totalBet = this.getTotalBet(payload.betAmount);
            const common = this.generateCommonMessagePayload();
            return {
                ...common,
                betAmount: totalBet
            };
        }

        protected getTotalBet(betAmount: RS.Models.StakeAmount): RS.Models.StakeAmount
        {
            return betAmount;
        }

        /** Consumes the next force in the queue and returns success. */
        protected async consumeNextForce(): Promise<boolean>
        {
            if (this.__forceQueue.length === 0) { return true; }
            const next = this.__forceQueue.shift();
            const values = this.toForceValues(next);
            return await this.sendForceMessage(values);
        }

        /** Converts the given force result to an array of force values. */
        protected abstract toForceValues(forceResult: RS.Engine.ForceResult): ReadonlyArray<number>;

        /** Sends a force message to the GDM server. */
        protected async sendForceMessage(values: ReadonlyArray<number>): Promise<boolean>
        {
            // TODO: use wrapper when forcing is implemented.
            try
            {
                // Temporary forcing solution until we have something proper in the wrapper
                if (this.__forceToolURL == null)
                {
                    const message = "Input the force tool URL.\nNote: this is the engine path (found in the nyxroot URL param) with /nextgen replaced with /forcetool, e.g. https://ogs-gdm-aws-2pp-int-generic-0.nyxaws.net/forcetool.";
                    this.__forceToolURL = prompt("ForceToolURL", message, this.inferForceToolURL() || undefined);
                }

                if (this.__sessionID == null)
                {
                    logger.warn("No message sequence published session ID!");
                    this.__sessionID = prompt("SessionID", `Input the session ID.`);
                }

                const url = this.__forceToolURL;
                const sessionID = this.__sessionID;
                const gameName = this.platform.adapter.getCustomSettings().gameName;

                let data = window.prompt("Modify force request?", values.join(";"));
                while (data != null && !/^([0-9]+;?)+$/.test(data))
                {
                    data = window.prompt("Invalid force request", data);
                }

                if (data == null)
                {
                    logger.warn("Force request cancelled");
                    return;
                }

                /*
                * sessionid: asdf
                * gameid: asdf
                * values: 1;2;3;4;5
                */

                const postData = Utils.formDataToQueryString({ "sessionid": sessionID, "gameid": gameName, "values": data });
                const headers = { "Content-Type": "application/x-www-form-urlencoded" };

                const dump = RS.IDump.get();
                const subDump = dump.getSubDump("GDM");

                const time = new Date();
                const dateTime = RS.Dump.getTimestamp(time);
                const stamp = time.getTime();

                subDump.add({ "type": "force", "datetime": dateTime, "timestamp": stamp, "message": data });
                await RS.Request.ajax({ url, method: "POST", postData, headers }, RS.Request.AJAXResponseType.Text);
                return true;
            }
            catch (err)
            {
                this.onForceError(err);
                return false;
            }
        }

        /** Gets pre-filled force tool URL. */
        protected inferForceToolURL(): string
        {
            const { referrer } = document;
            const [, configURL] = referrer.match(/(?:configurl=)([^&]*)/) || [null, null];
            if (!configURL) { return null; }

            for (const key in forceToolURLs)
            {
                if (configURL.substr(0, key.length) === key)
                {
                    return forceToolURLs[key];
                }
            }

            return null;
        }

        /** Generates payload common to all messages. */
        protected generateCommonMessagePayload(): CommonMessagePayload
        {
            return { isRecovering: this.__isRecovering };
        }

        /** Makes the previous error fatal. */
        protected makeFatal()
        {
            const model = this.models.error;
            switch (model.type)
            {
                case RS.Models.Error.Type.Server:
                    this.models.error.options = [ { type: RS.Models.Error.OptionType.Close, text: "OK" } ];
                    break;

                case RS.Models.Error.Type.Client:
                    this.models.error.options = [ { type: RS.Models.Error.OptionType.Refresh, text: "OK" } ];
                    break;

                case RS.Models.Error.Type.None:
                default:
                    logger.warn("Attempted to make the last error fatal, but none was set");
                    return;
            }
            this.models.error.fatal = true;
        }

        /**
         * Executes the given message sequence.
         */
        protected async executeSequence<TPayload extends CommonMessagePayload>(sequence: IMessageSequence<TModels, TPayload>, payload: TPayload)
        {
            if (sequence == null) { throw new Error("sequence cannot be null"); }
            if (payload == null) { throw new Error("payload cannot be null"); }

            // Check for connection loss.
            if (!RS.Connection.connected.value)
            {
                this.onConnectionLost();
                return false;
            }

            // Clear error model.
            RS.Models.Error.clear(this.models.error);

            const handles: RS.IDisposable[] = [];
            try
            {
                // Set up the sequence. We need to catch propery access as its implementation is out of our control (could theoretically throw).
                this.attachListenerIfEventPresent(sequence.onRecoveryStart, this.onRecoveryStart, handles);
                this.attachListenerIfEventPresent(sequence.onRecoveryEnd, this.onRecoveryEnd, handles);
                this.attachListenerIfEventPresent(sequence.onVersionReceived, this.onVersionReceived, handles);
                this.attachListenerIfEventPresent(sequence.onSessionReceived, this.onSessionReceived, handles);

                sequence.serverConnector = this.platform;

                // Go for it!
                return await sequence.execute(this.models, payload, this.settings.format);
            }
            catch (err)
            {
                this.onSequenceError(err);
                return false;
            }
            finally
            {
                for (const handle of handles)
                {
                    handle.dispose();
                }
            }
        }

        protected onForceError(err: any)
        {
            logger.error("Force failed", err);
            const { error } = this.models;
            error.type = RS.Models.Error.Type.Client;
            error.title = "CLIENT ERROR";
            error.message = (err && `${err}`) || "Unknown error";
            error.options =
            [
                { type: RS.Models.Error.OptionType.Continue, text: "OK" }
            ];
        }

        /** Handles message sequence throwing an error. */
        protected onSequenceError(err: any)
        {
            // Drop force queue
            this.__forceQueue.length = 0;

            logger.error("Message sequence failed", err);
            const { error } = this.models;
            error.type = RS.Models.Error.Type.Client;
            error.title = "CLIENT ERROR";
            error.message = (err && `${err}`) || "Unknown error";
            error.options =
            [
                { type: RS.Models.Error.OptionType.Continue, text: "OK" }
            ];
        }

        /** Handles connection lost. */
        protected onConnectionLost()
        {
            const { error } = this.models;
            error.type = RS.Models.Error.Type.Client;
            // TODO localisations
            error.title = "Translations.ConnectionError.Title";
            error.message = "Translations.ConnectionError.Message";
            error.options =
            [
                { type: RS.Models.Error.OptionType.Refresh, text: "Translations.ConnectionError.OK" }
            ];
        }

        /** If event exists, attaches listener and adds it to handlesArray. */
        private attachListenerIfEventPresent<T>(event: RS.IEvent<T> | void, listener: RS.EventHandlerCallback<T>, handlesArray: RS.IDisposable[])
        {
            if (!event) { return; }
            const handle = event(listener);
            handlesArray.push(handle);
        }

        /** Passes currency code from the Engine to the platform. */
        @RS.Callback
        private onCurrencyCodeReceived(code: string)
        {
            this.platform.currencyCode = code;
        }

        /** Handles message sequence reporting recovery start. */
        @RS.Callback
        private onRecoveryStart()
        {
            this.__isRecovering = true;
        }

        /** Handles message sequence reporting recovery end. */
        @RS.Callback
        private onRecoveryEnd()
        {
            this.__isRecovering = false;
        }

        /** Handles message sequence reporting GDM version. */
        @RS.Callback
        private onVersionReceived(version: string)
        {
            try
            {
                this.__version = Utils.extractServerVersion(version);
            }
            catch (err)
            {
                logger.error(`Failed to parse server version`, err);
            }
        }

        /** Handles message sequence reporting GDM session ID. */
        @RS.Callback
        private onSessionReceived(session: string)
        {
            this.__sessionID = session;
        }
    }

    export interface Settings<TModels extends object>
    {
        /** The GDM serialisation format to use. If not specified, uses Format.Default. */
        format?: Serialisation.Format;
        initSequence: IInitSequence<TModels, CommonMessagePayload>;
        betSequence?: Engine.IMessageSequence<TModels, BetMessagePayload>;
    }

    export type BetMessagePayload = Engine.CommonMessagePayload & RS.Engine.ILogicRequest.Payload;

    /** Message payload common to all messages. */
    export interface CommonMessagePayload
    {
        isRecovering: boolean;
    }

    /** Handles the external API communications for the initialising message sequence. */
    export interface IInitSequence<TModels extends object, TPayload extends CommonMessagePayload> extends IMessageSequence<TModels, TPayload>
    {
        /** Published when the currency code is received. */
        readonly onCurrencyCodeReceived: RS.IEvent<string>;
    }

    /** Handles the external API communications for a particular message sequence. */
    export interface IMessageSequence<TModels extends object, TPayload extends CommonMessagePayload>
    {
        /**
         * Published when recovery starts, if it starts as part of this message sequence.
         *
         * May be omitted if recovery cannot start as part of this message sequence.
         */
        readonly onRecoveryStart?: RS.IEvent;

        /**
         * Published when recovery starts, if it starts as part of this message sequence.
         *
         * May be omitted if recovery cannot end as part of this message sequence.
         */
        readonly onRecoveryEnd?: RS.IEvent;

        /**
         * Published when the GDM endpoint version is received.
         *
         * May be omitted if the version cannot be ascertained as part of this message sequence.
         */
        readonly onVersionReceived?: RS.IEvent<string>;

        // TODO: remove when GDM wrapper forcing is implemented.
        /**
         * Published when the GDM session ID is received.
         *
         * May be omitted if the version cannot be ascertained as part of this message sequence.
         *
         * NOTE: Part of a stop-gap forcing solution. Do NOT use this outside of the core GDM.Engine; it will be removed when wrapper forcing is implemented.
         */
        readonly onSessionReceived?: RS.IEvent<string>;

        /** Server message connector e.g. GDM platform. */
        serverConnector: Environment.IServerConnector;

        /** Executes the message sequence and returns success. */
        execute(models: TModels, payload: TPayload, format: Serialisation.Format): boolean | PromiseLike<boolean>;
    }
}