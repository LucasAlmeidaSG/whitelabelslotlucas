/// <reference path="BaseMessage.ts" />

namespace RS.GDM.Engine.MessageTypes
{
    export const Bet = "BET";
    export const FeatureEnd = "FEATURE_END";
    export const FeaturePick = "FEATURE_PICK";
    export const FeatureStart = "FEATURE_START";
}

namespace RS.GDM.Engine.Messages
{
    /**
     * Contains game-specific data (GSD).
     *
     * GSD = field1~values#field2~values
     *
     * field1 = field name for game specific data (name will vary from game to game)
     * field2 = field name for game specific data
     * values = values for field (format will vary from game to game)
     */
    export class GameSpecifcData
    {
        /** Whether or not max win has been triggered this spin (GCT). */
        @Serialisation.Property({ key: "GCT", type: Serialisation.Boolean })
        public isMaxWin?: boolean;
    }

    /**
     * Represents a decision made as part of a feature (GSD).
     *
     * Required in FEATURE_PICK requests.
     */
    export class FeaturePick
    {
        /** The round number for this pick (#0). */
        @Serialisation.Property({ key: "0", type: Serialisation.Integer, required: true })
        public round: number;

        /** The pick choice for this pick (#1). */
        @Serialisation.Property({ key: "1", type: Serialisation.Integer, required: true })
        public pick: number;

        /** The pick value for this pick (#2). */
        @Serialisation.Property({ key: "2", type: Serialisation.Integer, required: true })
        public value: number;
    }

    export class BetRequest<TModels extends RS.Models, TPayload extends CommonMessagePayload> extends Request<TModels, TPayload>
    {
        /** Feature ID of the current feature (CFG). */
        @Serialisation.Property({ key: "CFG", type: Serialisation.Integer })
        public currentFeature?: number;

        /** Picks for the current feature (FP). */
        @Serialisation.Property({ key: "FP", type: Serialisation.Tuple(FeaturePick, { delimitStrategy: Serialisation.Array.DelimitStrategy.Infix, delimiter: "|" }) })
        public featurePick?: FeaturePick;
    }

    /** Contains common game message properties */
    export class BetResponse<TModels extends RS.Models> extends Response<TModels>
    {
        /** Total win for the wager (TW). */
        @Serialisation.Property({ key: "TW", type: Serialisation.Integer, required: true })
        public totalWin: number;

        /** Win amount for the current free game (CW). */
        @Serialisation.Property({ key: "CW", type: Serialisation.Number })
        public currentServerWin?: number;

        /** Contains game-specific data (GSD). */
        @Serialisation.Property({ key: "GSD", type: Serialisation.Object(GameSpecifcData, { keyValueSeparator: "~", keyValueDelimiter: "#" }) })
        public gameSpecificData?: GameSpecifcData;

        /** Total win for the wager. */
        public get accumulatedWin(): number
        {
            return this.totalWin;
        }

        /** Whether or not this bet response resulted in the win limit being reached. */
        public get isMaxWin(): boolean
        {
            return this.gameSpecificData && this.gameSpecificData.isMaxWin || false;
        }

        /** Win amount for this response, not including the win for any earlier responses from the same wager. */
        public get currentWin(): number
        {
            return this.currentServerWin == null ? this.totalWin : this.currentServerWin;
        }

        public get balanceBefore(): number
        {
            if (this.isFinal)
            {
                return this.accountBalance - this.accumulatedWin;
            }
            else
            {
                return this.accountBalance;
            }
        }

        public get balanceAfter(): number
        {
            if (this.isFinal)
            {
                return this.accountBalance;
            }
            else
            {
                return this.balanceBefore;
            }
        }

        public get isFinal(): boolean
        {
            if (this.isMaxWin) { return true; }
            return !this.hasPendingFeatureRequests();
        }

        //#region Feature

        /**
         * IDs of current features and free games. First value is current feature, other values are the next features (FID).
         *
         * FID = id1|id2|id3
         * id1 = current feature/FG ID
         * id2 = next feature/FG ID
         * id3 = 3rd feature ID
         */
        @Serialisation.Property({ key: "FID", type: Serialisation.ArrayOf(Serialisation.Integer) })
        public featureIDs?: number[];

        /** Feature ID of the current feature (CFG). */
        @Serialisation.Property({ key: "CFG", type: Serialisation.Integer })
        public currentFeature?: number;

        /**
         * Indicates whether the server has recieved a feature start message from client (FS).
         *
         * FS_fid = n
         * fid = feature ID
         * n = 0 (false), 1 (true)
         */
        @Serialisation.Property({ key: "FS_{#}", multiKey: true, type: Serialisation.Boolean })
        public featureStarted?: boolean[];

        /**
         * Number of rounds in the feature (NFR).
         * NFR_fid = n
         * fid = feature ID
         * n = number of rounds
         */
        @Serialisation.Property({ key: "NFR_{#}", multiKey: true, type: Serialisation.Integer })
        public numberOfFeatureRounds?: number[];

        /**
         * Number of rounds in the feature (FPM).
         *
         * FPM_fid = p1;p2;pn|
         * p1 = pick value for pick 1
         * p2 = pick value for pick 2
         * pn = pick value for pick n
         */
        @Serialisation.Property({ key: "FPM_{#}", multiKey: true, type:
            Serialisation.ArrayOf(
                // inner array
                Serialisation.ArrayOf(Serialisation.Number, { delimiter: ";" }),
                // outer array
                { delimiter: "|" })
            })
        public pickValues: number[][][];

        /**
         * Current round for the feature (CFR).
         *
         * CFR_fid = n
         * fid = feature ID
         * n = current round
         */
        @Serialisation.Property({ key: "CFR_{#}", multiKey: true, type: Serialisation.Integer })
        public currentRound: number[];

        /**
         * Current pick for the feature (CFP).
         *
         * CFP_fid = n
         * fid = feature ID
         * n = current pick
         */
        @Serialisation.Property({ key: "CFP_{#}", multiKey: true, type: Serialisation.Integer })
        public currentPick: number[];

        //#endregion

        protected hasPendingFeatureRequests(): boolean
        {
            return false;
        }
    }

    export abstract class BetSequence<TModels extends RS.Models, TPayload extends CommonMessagePayload> extends SimpleMessageSequence<TModels, TPayload, Response<TModels>>
    {
        public readonly settings: BetSequence.Settings<TModels>;

        constructor(settings: BetSequence.Settings<TModels>)
        {
            super();

            this.settings = { ...settings };
        }

        protected getMessageType(models: TModels, payload: TPayload): string
        {
            return this.settings.messageType || this.inferMessageType(models, payload);
        }

        protected inferMessageType(models: TModels, payload: TPayload): string
        {
            return MessageTypes.Bet;
        }

        protected createResponse(models: TModels)
        {
            return new this.settings.betResponse(models);
        }
    }

    export namespace BetSequence
    {
        export interface Settings<TModels extends RS.Models>
        {
            betResponse: RS.Constructor1A<BetResponse<TModels>, TModels>;
            messageType?: string;
        }
    }
}
