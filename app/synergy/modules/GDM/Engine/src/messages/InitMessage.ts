/// <reference path="BaseMessage.ts" />

namespace RS.GDM.Engine.MessageTypes
{
    export const Init = "INIT";
}

namespace RS.GDM.Engine.Messages
{
    /** Contains formatting information for the player's currency (CUR). */
    export class CurrencyData
    {
        /** GDM currency code e.g. "ISO:GBP" (#0). */
        @Serialisation.Property({ key: "0", type: Serialisation.String, required: true })
        public gdmCode: string;

        /** 3-letter ISO currency code. */
        public get isoCode()
        {
            const gdmCode = this.gdmCode;
            if (!gdmCode)
            {
                throw new Error(`No GDM data to resolve ISO currency code from`);
            }

            const match = gdmCode.match(/(?:ISO:)([A-Z]{3})/);
            if (!match)
            {
                throw new Error(`Failed to resolve ISO currency code from "${gdmCode}"`);
            }

            return match[1];
        }

        /** Thousands grouping separator (#1). */
        @Serialisation.Property({ key: "1", type: Serialisation.String, required: true })
        public groupingSeparator: string;

        /** Decimal fraction separator (#2). */
        @Serialisation.Property({ key: "2", type: Serialisation.String, required: true })
        public fractionalSeparator: string;

        /** Integer symbol e.g. the £ in £34 (#3). */
        @Serialisation.Property({ key: "3", type: Serialisation.CharArray({ delimiter: ";" }), required: true })
        public integerSymbol: string;

        /** Which side of the number the integer symbol appears on (#4). */
        @Serialisation.Property({ key: "4", type: Serialisation.String, required: true })
        public integerSymbolPos: string;

        /** Fractional symbol e.g. the p in 34p (#5). */
        @Serialisation.Property({ key: "5", type: Serialisation.CharArray({ delimiter: ";" }), required: true })
        public fractionalSymbol: string;

        /** Which side of the number the fractional symbol appears on (#6). */
        @Serialisation.Property({ key: "6", type: Serialisation.String, required: true })
        public fractionalSymbolPos: string;
    }

    export class InitRequest<TModels extends RS.Models, TPayload extends CommonMessagePayload> extends Request<TModels, TPayload>
    {
        // Nothing special here
    }

    /**
     * A server init message.
     * E.g. &MSGID=INIT&B=100000&VER=2.6.3-2.4.17-4-11&LIM=1|100000|&BD=1|5|10|20|50|100|200|500|1000|&BDD=10&RSTM=1;0|&UGB=1&CUR=ISO:GBP|,|.|163;|L|112;|R&GA=0&SID=SESSION00000&
     */
    export class InitResponse<TModels extends RS.Models> extends Response<TModels>
    {
        /** Min and max bet limits for bet per line and side bet (LIM). */
        @Serialisation.Property({ key: "LIM", type: Serialisation.ArrayOf(Serialisation.Integer) })
        public betLimits?: number[];

        /** Bet denominations i.e. stake levels (BD). */
        @Serialisation.Property({ key: "BD", type: Serialisation.ArrayOf(Serialisation.Integer), required: true })
        public betDenominations: number[];

        /** Default bet denomination i.e. default stake level (BDD). */
        @Serialisation.Property({ key: "BDD", type: Serialisation.Integer, required: true })
        public defaultBet: number;

        /**
         * Formatting information for the player currency (CUR).
         *
         * CUR=cc|ts|ds|wcs|wcp|fcs|fcp
         *
         * cc = currency code \
         * ts = thousands separator \
         * ds = decimal separator \
         * wcs = whole currency symbols ASCII characters separated by ; \
         * wcp = whole currency symbols position (L=left, R=right) \
         * fcs = fractional currency symbols ASCII charactes separated by ; \
         * fcp = fractional currency symbols position (L=left, R=right)
         *
         * Example: "CUR=ISO:GBP|,|.|71;66;80;|L|112;|R" should display currency like so: GBP34,087.00
         */
        @Serialisation.Property({ key: "CUR", type: Serialisation.Tuple(CurrencyData, { delimitStrategy: Serialisation.Array.DelimitStrategy.Infix }), required: true })
        public currencyData: CurrencyData;

        // Gambling

        /** Indicates whether to display and enable the gamble button (UGB). */
        @Serialisation.Property({ key: "UGB", type: Serialisation.Boolean })
        public useGambleButton?: boolean;

        /** Indicates whether or not we are in recovery (R). */
        @Serialisation.Property({ key: "R", type: Serialisation.Boolean, defaultValue: false })
        public isRecovering: boolean;

        /** Current max win amount. */
        @Serialisation.Property({ key: "CAP", type: Serialisation.Integer })
        public maxWinAmount?: number;

        /** Populates models using an InitResponse object. */
        public populateModels(models: TModels)
        {
            this.populateWithCurrencyData(models);
        }

        /** Populates models using a CurrencyData object. */
        protected populateWithCurrencyData(models: TModels)
        {
            const currencyData = this.currencyData;

            const model = models.customer;
            const currencyMultiplier = 1.0;
            const shouldDisplaySymbol = !!currencyData.integerSymbol;

            model.currencyCode = currencyData.isoCode;
            model.currencyMultiplier = currencyMultiplier;
            model.currencySettings =
            {
                name: currencyData.isoCode,
                symbol: currencyData.integerSymbol,
                currencyMultiplier: currencyMultiplier,

                currencyCodeBeforeAmount: currencyData.integerSymbolPos === "L",
                currencyCodeSpacing: 0,

                currencySymbolBeforeAmount: currencyData.integerSymbolPos === "L",
                currencySymbolSpacing: 0,

                groupingSeparator: currencyData.groupingSeparator,
                grouping: 3,

                fractionalSeparator: currencyData.fractionalSeparator,
                fractionalDigits: 2,

                displayCode: !shouldDisplaySymbol,
                displaySymbol: shouldDisplaySymbol
            };
        }
    }

    /** Sequence for a single init request-response sequence. */
    export class InitSequence<TModels extends RS.Models>
        extends SimpleMessageSequence<TModels, CommonMessagePayload, InitResponse<TModels>>
        implements IInitSequence<TModels, CommonMessagePayload>
    {
        protected readonly _onCurrencyCodeReceived = RS.createEvent<string>();
        public get onCurrencyCodeReceived() { return this._onCurrencyCodeReceived.public; }

        protected readonly _onRecoveryStart = RS.createSimpleEvent();
        public get onRecoveryStart() { return this._onRecoveryStart.public; }

        constructor(public readonly settings: InitSequence.Settings<TModels>)
        {
            super();
        }

        protected async handleResponseReceived(responseString: string, models: TModels, payload: CommonMessagePayload, format: Serialisation.Format)
        {
            // Do normal handling
            if (!await super.handleResponseReceived(responseString, models, payload, format)) { return false; }

            // Determine recovery & currency code
            const initResponse = new InitResponse(models);
            Serialisation.deserialise(initResponse, responseString);
            if (initResponse.isRecovering) { this._onRecoveryStart.publish(); }
            this._onCurrencyCodeReceived.publish(initResponse.currencyData.isoCode);

            // Use updated isRecovering value
            if (!await this.loadAdditionalData(models, { ...payload, isRecovering: initResponse.isRecovering }, format)) { return false; }

            // Recovery data is merged in with init data; handle it
            if (initResponse.isRecovering)
            {
                const recoveryResponse = new this.settings.betResponse(models);
                Serialisation.deserialise(recoveryResponse, responseString);
                recoveryResponse.populateModels(models);
            }

            return true;
        }

        /** Override to load data after the init response is processed and before the recovery response is processed. */
        protected async loadAdditionalData(models: TModels, payload: CommonMessagePayload, format: Serialisation.Format)
        {
            return true;
        }

        /** Creates an empty InitResponse object to deserialise into. */
        protected createResponse(models: TModels)
        {
            return new this.settings.initResponse(models);
        }

        protected createRequest(models: TModels, payload: CommonMessagePayload)
        {
            const ctor = this.settings.request || Request;
            return new ctor(models, payload);
        }

        protected getMessageType()
        {
            return MessageTypes.Init;
        }
    }

    export namespace InitSequence
    {
        export interface Settings<TModels extends RS.Models>
        {
            request?: RS.Constructor2A<Request<TModels, CommonMessagePayload>, TModels, CommonMessagePayload>;
            initResponse: RS.Constructor1A<InitResponse<TModels>, TModels>;
            betResponse: RS.Constructor1A<BetResponse<TModels>, TModels>;
        }
    }

    /** Bet response handling delegate to enable consistent extensible handling of both standard bet and init recovery response data. */
    export interface IBetLogic<TModels extends RS.Models, TResponse extends object>
    {
        /** Creates the bet response object to deserialise a bet response into. */
        createResponse(): TResponse;
        /** Populates models using the filled object returned from createResponse(). */
        populateModels(models: TModels, response: TResponse): void;
    }
}