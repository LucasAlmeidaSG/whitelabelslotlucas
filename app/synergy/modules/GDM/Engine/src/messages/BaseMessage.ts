/// <reference path="../serialisation/Serialisation.ts" />

/**
 * Contains some GDM message types e.g. INIT or REELSTRIP.
 * Note that, in general, the type is the same for a response as for the request that preceded it.
 */
namespace RS.GDM.Engine.MessageTypes
{
    export const Error = "ERROR";
}

namespace RS.GDM.Engine.Messages
{
    //#region Serialisation

    /** A base message. */
    export class Message<TModels extends RS.Models>
    {
        /**
         * Message type e.g. INIT or REELSTRIP (MSGID).
         * Note that, in general, the type is the same for a response as for the request that preceded it.
         */
        @Serialisation.Property({ key: "MSGID", type: Serialisation.String, required: true })
        public type: string;

        constructor(public readonly models: RS.Models)
        {

        }

        /**
         * Binds a message constructor requiring settings to the given settings.
         */
        public static bind<TSettings extends object, TModels extends RS.Models, TMessage extends Message<TModels>, T extends RS.Constructor2A<TMessage, TSettings, TModels>>(cl: T, settings: TSettings): RS.Constructor<TMessage>
        {
            return Function.bind.call(cl, undefined, settings) as RS.Constructor<TMessage>;
        }
    }

    /** A base client message. */
    export class Request<TModels extends RS.Models, TPayload extends CommonMessagePayload> extends Message<TModels>
    {
        /** Whether or not the this request resumes an unclosed wager (REC). */
        @Serialisation.Property({ key: "REC", type: Serialisation.Boolean })
        public isRecovering: boolean;

        constructor(models: TModels, public readonly payload: TPayload)
        {
            super(models);

            this.isRecovering = payload.isRecovering;
        }
    }

    /** An error message. */
    export class ErrorResponse<TModels extends RS.Models> extends Message<TModels>
    {
        /** Error code, if this response is an error (EID). */
        @Serialisation.Property({ key: "EID", type: Serialisation.String })
        public errorCode: string;

        public populateModels(models: TModels)
        {
            models.error.type = RS.Models.Error.Type.Server;
            models.error.title = "SERVER ERROR";
            models.error.message = this.errorCode;
            models.error.options = [ { type: RS.Models.Error.OptionType.Continue, text: "OK" } ];
        }
    }

    /** A base server message. */
    export class Response<TModels extends RS.Models> extends Message<TModels>
    {
        /** Server version (VER). */
        @Serialisation.Property({ key: "VER", type: Serialisation.String })
        public version?: string;

        /** Calculated player balance (AB + FRBAL). */
        public get balance() { return this.serverBalance + this.freeRoundsBalance; }

        /** Player balance (B). */
        @Serialisation.Property({ key: "B", type: Serialisation.Integer })
        public serverBalance?: number;

        /** Player cash balance (total - bonus). */
        public get cashBalance() { return this.accountBalance - this.freeRoundsBalance; }

        /** Player's total balance (cash + bonus (free rounds / promotional)). */
        @Serialisation.Property({ key: "AB", type: Serialisation.Integer, required: true })
        public accountBalance: number;

        /** Player's free rounds (promotional) balance (FRBAL). */
        @Serialisation.Property({ key: "FRBAL", type: Serialisation.Integer, defaultValue: 0 })
        public freeRoundsBalance: number;

        /** Current session token (SID). */
        @Serialisation.Property({ key: "SID", type: Serialisation.String })
        public sessionID?: string;

        /** Indicates whether the player can gamble in the current game (GA). */
        @Serialisation.Property({ key: "GA", type: Serialisation.Boolean })
        public canGamble?: boolean;

        public populateModels(models: TModels)
        {
            this.updateBalance(models.customer.finalBalance);
        }

        protected updateBalance(balances: RS.Models.Balances)
        {
            balances.primary = this.balance;
            balances.named[RS.Models.Balances.Cash] = this.serverBalance;
            balances.named[RS.Models.Balances.Promotional] = this.freeRoundsBalance;
        }
    }

    //#endregion
    //#region Sequencing

    /**
     * A base class for a single request-response message sequence.
     *
     * More complicated (multi-message) sequences should implement IMessageSequence on their own and call execute on child sequences.
     */
    export abstract class MessageSequence<TModels extends RS.Models, TPayload extends CommonMessagePayload> implements IMessageSequence<TModels, TPayload>
    {
        public serverConnector: Environment.IServerConnector | null = null;

        protected readonly _onVersionReceived = RS.createEvent<string>();
        public get onVersionReceived() { return this._onVersionReceived.public; }

        protected readonly _onSessionReceived = RS.createEvent<string>();
        public get onSessionReceived() { return this._onSessionReceived.public; }

        /** Sends a message using the given message IMessageSequence and payload in the format configured in the settings (unless overridden). */
        public async execute(models: TModels, payload: TPayload, format: Serialisation.Format)
        {
            if (!this.serverConnector) { throw new Error(`No server connector set`); }
            const requestString = this.serialiseMessage(models, payload, format);
            const responseString = await this.serverConnector.sendServerMessage(requestString);
            return await this.handleResponseReceived(responseString, models, payload, format);
        }

        protected async handleResponseReceived(responseString: string, models: TModels, payload: TPayload, format: Serialisation.Format)
        {
            // Handle type
            const message = new Message(models);
            Serialisation.deserialise(message, responseString);

            const expectedType = this.getMessageType(models, payload);
            switch (message.type)
            {
                case MessageTypes.Error:
                {
                    const error = new ErrorResponse(models);
                    Serialisation.deserialise(error, responseString);

                    this.serverConnector.notifyError(error.errorCode);
                    error.populateModels(models);
                    return false;
                }

                case expectedType:
                {
                    const response = new Response(models);
                    Serialisation.deserialise(response, responseString);

                    if (response.version)
                    {
                        this._onVersionReceived.publish(response.version);
                    }

                    if (response.sessionID)
                    {
                        this._onSessionReceived.publish(response.sessionID);
                    }

                    // Update models
                    response.populateModels(models);

                    // Pass through to specific response handlers
                    this.populateModels(models, responseString);
                    return true;
                }

                default:
                {
                    // Sequence failure
                    throw new Error(`Expected type '${expectedType}', got '${message.type}'`);
                }
            }
        }

        /** Handles a successful server response. */
        protected abstract populateModels(models: TModels, responseStr: string);

        /** Creates a request object. */
        protected abstract createRequest(models: TModels, payload: TPayload): Request<TModels, TPayload>;

        /** Returns the message type string. */
        protected abstract getMessageType(models: TModels, payload: TPayload): string;

        /** Produces a message string using the given IMessageSequence and payload in the format configured in the settings (unless overridden). */
        protected serialiseMessage(models: TModels, payload: TPayload, format: Serialisation.Format)
        {
            const request = this.createRequest(models, payload);
            if (!request) { throw new Error("Request object missing"); }

            request.type = this.getMessageType(models, payload);
            return Serialisation.serialise(request, format);
        }
    }

    /** A simple single-response sequence. */
    export abstract class SimpleMessageSequence<TModels extends RS.Models, TPayload extends CommonMessagePayload, TResponse extends Response<TModels>>
        extends MessageSequence<TModels, TPayload>
    {
        protected populateModels(models: TModels, responseStr: string)
        {
            const response = this.createResponse(models);
            Serialisation.deserialise(response, responseStr);
            response.populateModels(models);
        }

        protected abstract createResponse(models: TModels): TResponse;
    }

    /** A sequence consisting of multiple inner message sequences. */
    export abstract class MultiMessageSequence<TModels extends RS.Models, TPayload extends CommonMessagePayload> implements IMessageSequence<TModels, TPayload>
    {
        public serverConnector: Environment.IServerConnector | null = null;

        public get onSessionReceived() { return this._onSessionReceived.public; }
        public get onVersionReceived() { return this._onVersionReceived.public; }
        public get onRecoveryStart() { return this._onRecoveryStart.public; }
        public get onRecoveryEnd() { return this._onRecoveryEnd.public; }

        protected readonly _onSessionReceived = RS.createEvent<string>();
        protected readonly _onVersionReceived = RS.createEvent<string>();
        protected readonly _onRecoveryStart = RS.createSimpleEvent();
        protected readonly _onRecoveryEnd = RS.createSimpleEvent();

        public abstract execute(models: TModels, payload: TPayload, format: Serialisation.Format): PromiseLike<boolean>;

        /** Executes the given sequence as part of this multi-message sequence. */
        protected async executeSequence(sequence: IMessageSequence<TModels, TPayload>, models: TModels, payload: TPayload, format: Serialisation.Format)
        {
            if (sequence == null) { throw new Error("sequence cannot be null"); }
            if (models == null) { throw new Error("models cannot be null"); }
            if (payload == null) { throw new Error("payload cannot be null"); }
            if (format == null) { throw new Error("format cannot be null"); }

            if (!this.serverConnector) { throw new Error(`No server connector set`); }

            const handles: RS.IDisposable[] = [];
            try
            {
                // Set up the sequence. We need to catch propery access as its implementation is out of our control (could theoretically throw).
                this.attachListenerIfEventPresent(sequence.onSessionReceived, (ver) => this._onSessionReceived.publish(ver), handles);
                this.attachListenerIfEventPresent(sequence.onVersionReceived, (ver) => this._onVersionReceived.publish(ver), handles);
                this.attachListenerIfEventPresent(sequence.onRecoveryStart, () => this._onRecoveryStart.publish(), handles);
                this.attachListenerIfEventPresent(sequence.onRecoveryEnd, () => this._onRecoveryEnd.publish(), handles);

                sequence.serverConnector = this.serverConnector;

                // Go for it!
                return await sequence.execute(models, payload, format);
            }
            finally
            {
                for (const handle of handles)
                {
                    handle.dispose();
                }
            }
        }

        /** Executes the given array of sequences as part of this multi-message sequence. */
        protected async executeSequences(sequences: ReadonlyArray<IMessageSequence<TModels, TPayload>>, models: TModels, payload: TPayload, format: Serialisation.Format)
        {
            for (const sequence of sequences)
            {
                if (!await this.executeSequence(sequence, models, payload, format))
                {
                    return false;
                }
            }
            return true;
        }

        /** If event exists, attaches listener and adds it to handlesArray. */
        private attachListenerIfEventPresent<T>(event: RS.IEvent<T> | void, listener: RS.EventHandlerCallback<T>, handlesArray: RS.IDisposable[])
        {
            if (!event) { return; }
            const handle = event(listener);
            handlesArray.push(handle);
        }
    }

    /** A multi-message sequence which executes a simple array of sequences in order. */
    export class ArrayMultiMessageSequence<TModels extends RS.Models, TPayload extends CommonMessagePayload> extends MultiMessageSequence<TModels, TPayload>
    {
        private readonly __sequences: IMessageSequence<TModels, TPayload>[];
        protected get _sequences(): ReadonlyArray<IMessageSequence<TModels, TPayload>> { return this.__sequences; }

        constructor(sequences: IMessageSequence<TModels, TPayload>[])
        {
            super();

            this.__sequences = sequences;
        }

        public execute(models: TModels, payload: TPayload, format: Serialisation.Format): Promise<boolean>
        {
            return this.executeSequences(this.__sequences, models, payload, format);
        }
    }

    //#endregion
}