namespace RS.GDM.Engine.Utils
{
    /** Extracts the relevant server version from a given GDM version string e.g. 2.6.3-2.4.17-4-11 -> 2.6.3. */
    export function extractServerVersion(versionString: string): RS.Version
    {
        // example: 2.6.3-2.4.17-4-11
        return RS.Version.fromString(versionString);
    }

    /** Converts FormData to a query string. */
    export function formDataToQueryString(formData: Readonly<RS.Map<string>>): string
    {
        let str = "";
        for (const key in formData)
        {
            const value = formData[key];
            if (str.length > 0) { str += "&"; }
            if (RS.Is.string(value))
            {
                str += `${encodeFormString(key)}=${encodeFormString(value)}`;
            }
            else
            {
                throw new Error(`Files are not allowed with FormDataFormat.QueryString`);
            }
        }
        return str;
    }

    function encodeFormString(str: string)
    {
        return encodeURI(str).replace("%20", "+");
    }
}