namespace RS.GDM.Engine.Serialisation.MessageParser.Tests
{
    const { expect } = chai;

    describe("MessageParser.ts", function ()
    {
        describe("messageToJSON", function ()
        {
            function testPair(pair: SerialisationPair)
            {
                const response = pair.message;
                const json = messageToJSON(response);
                const expectedJSON = pair.json;

                for (const key in expectedJSON)
                {
                    expect(json, `json should contain key "${key}"`).to.haveOwnProperty(key);
                }

                for (const key in json)
                {
                    const value = json[key];
                    const expectedValue = expectedJSON[key];
                    expect(value, `json[${key}] should equal "${expectedValue}"`).to.equal(expectedValue);
                }
            }

            it("should convert a GDM message into a JSON map", function ()
            {
                for (const pair of pairs)
                {
                    testPair(pair)
                }
            });

            // Deserialiser should be more robust to oddities because we can't control what we're sent
            it("should handle keys containing &", function ()
            {
                testPair({
                    message: "&HAS&AMPERSAND&=WEIRD_KEY&",
                    json: { "HAS&AMPERSAND&": "WEIRD_KEY" }
                });
            });

            it("should handle values containing =", function ()
            {
                testPair({
                    message: "&WEIRD_VAL=HAS=EQUALS=&",
                    json: { "WEIRD_VAL": "HAS=EQUALS=" }
                });
            });
        });

        describe("jsonToMessage", function ()
        {
            it("should convert a JSON map into a GDM message", function ()
            {
                for (const pair of pairs)
                {
                    const json = pair.json;
                    const message = jsonToMessage(json);
                    const expectedMessage = pair.message;
                    expect(message).to.equal(expectedMessage);
                }
            });

            it("should throw if a key contains =", function ()
            {
                const invalidObj = { "mykey=": "yes" };
                expect(() => jsonToMessage(invalidObj)).to.throw();
            });

            it("should throw if a key contains &", function ()
            {
                const invalidObj = { "mykey&": "yes" };
                expect(() => jsonToMessage(invalidObj)).to.throw();
            });

            it("should throw if a value contains &", function ()
            {
                const invalidObj = { "mykey": "yes&" };
                expect(() => jsonToMessage(invalidObj)).to.throw();
            });

            it("should throw if a value contains =", function ()
            {
                const invalidObj = { "mykey": "yes=" };
                expect(() => jsonToMessage(invalidObj)).to.throw();
            });
        });
    });

    // Test data

    interface SerialisationPair
    {
        json: RS.Map<string>;
        message: string;
    }

    const pairs: SerialisationPair[] =
    [
        {
            message: "&",
            json: {}
        },
        {
            message: "&EMPTY=&MSGID=INIT&B=100000&VER=2.6.3-2.4.17-4-11&LIM=1|100000| &BD=1|5|10|20|50|100|200|500|1000|&BDD=10&RSTM=1;0|&UGB=1 &CUR=ISO:GBP|,|.|163;|L|112;|R&GA=0&SID=SESSION00000&",
            json:
            {
                "EMPTY": "",
                "MSGID": "INIT",
                "B": "100000",
                "VER": "2.6.3-2.4.17-4-11",
                "LIM": "1|100000| ",
                "BD": "1|5|10|20|50|100|200|500|1000|",
                "BDD": "10",
                "RSTM": "1;0|",
                "UGB": "1 ",
                "CUR": "ISO:GBP|,|.|163;|L|112;|R",
                "GA": "0",
                "SID": "SESSION00000"
            }
        }
    ];
}