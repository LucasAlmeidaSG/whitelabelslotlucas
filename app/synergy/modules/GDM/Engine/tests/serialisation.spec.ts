namespace RS.GDM.Engine.Serialisation.Tests
{
    const { expect } = chai;

    type StringifyFunc = (obj: Readonly<RS.Map<string>>) => string;

    describe("Serialisation.ts", function ()
    {
        describe("decorator", function ()
        {
            it("should throw an error if the key is null", function ()
            {
                expect(() =>
                {
                    class MyClass
                    {
                        @Property({ key: null, type: Boolean })
                        public myProp: boolean;
                    }
                }).to.throw();
            });

            it("should throw an error if the key is an empty string", function ()
            {
                expect(() =>
                {
                    class MyClass
                    {
                        @Property({ key: "", type: Boolean })
                        public myProp: boolean;
                    }
                }).to.throw();
            });

            it("should throw an error if type is null", function ()
            {
                expect(() =>
                {
                    class MyClass
                    {
                        @Property({ key: "K", type: null })
                        public myProp: boolean;
                    }
                }).to.throw();
            });

            it("should throw an error if type is invalid", function ()
            {
                expect(() =>
                {
                    class MyClass
                    {
                        @Property({ key: "K", type: { serialiser: null, deserialiser: null } })
                        public myProp: boolean;
                    }
                }).to.throw();
            });
        });

        describe("deserialise", function ()
        {
            interface TestPair<T>
            {
                input: string;
                output: T;
            }

            function TestPair<T>(input: string, output: T): TestPair<T>
            {
                return { input, output };
            }

            function getTestFn(stringifyFn: StringifyFunc)
            {
                return function <T>(prop: SerialiserPair<T>, pairs: TestPair<T>[])
                {
                    class MockObject
                    {
                        @Property({ key: "P", type: prop })
                        public prop: T;
                    }

                    for (const pair of pairs)
                    {
                        const obj = new MockObject();
                        deserialise(obj, stringifyFn({ P: pair.input }));
                        expect(obj.prop).to.deep.equal(pair.output);
                    }
                };
            }

            function testFormat(format: Format, stringifyFn: StringifyFunc)
            {
                const test = getTestFn(stringifyFn);
                describe(`Format.${Format[format]}`, function ()
                {
                    it("should use a default value for a null property if defaultValue is set", function ()
                    {
                        const defaultValue = "hi";

                        class MockObject
                        {
                            @Property({ key: "P", type: String, defaultValue })
                            public prop: string;
                        }

                        const obj = new MockObject();
                        deserialise(obj, stringifyFn({ P: defaultValue }), format);
                        expect(obj.prop).to.equal(defaultValue);
                    });

                    it("should throw if a property is missing and required is true", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "P", type: String, required: true })
                            public prop: string;
                        }

                        const obj = new MockObject();
                        expect(() => deserialise(obj, stringifyFn({}), format)).to.throw();
                    });

                    it("should use a default value for a null property if defaultValue is set and required is true", function ()
                    {
                        const defaultValue = "hi";

                        class MockObject
                        {
                            @Property({ key: "P", type: String, defaultValue, required: true })
                            public prop: string;
                        }

                        const obj = new MockObject();
                        deserialise(obj, stringifyFn({ P: defaultValue }), format);
                        expect(obj.prop).to.equal(defaultValue);
                    });

                    it("should deserialise a string", function ()
                    {
                        const strs = ["my arbitrary string", "", " ", "  ", " my bad  str   "];
                        test(String, strs.map((str) => TestPair(str, str)));
                    });

                    it("should deserialise a number", function ()
                    {
                        test(Number,
                        [
                            TestPair("2.5", 2.5),
                            TestPair("2", 2),

                            // Invalid values
                            TestPair("|", null),
                            TestPair("a0", null),
                            TestPair("a0.3", null),
                            TestPair("a", null),
                            TestPair("", null),
                            TestPair(" ", null)
                        ]);
                    });

                    it("should deserialise an integer", function ()
                    {
                        test(Integer,
                        [
                            TestPair("2", 2),

                            // Invalid values
                            TestPair("2.5", null),
                            TestPair("2.4", null),
                            TestPair("|", null),
                            TestPair("a0", null),
                            TestPair("a", null),
                            TestPair("", null),
                            TestPair(" ", null)
                        ]);
                    });

                    it("should deserialise a boolean", function ()
                    {
                        const truthy = ["tRuE", "Y", "yes", "1", "2.2", "-2"];
                        const falsey = ["fAlSe", "N", "no", "0"];
                        const invalid = ["", "invalid"];
                        test(Boolean,
                        [
                            ...truthy.map((v) => TestPair(v, true)),
                            ...falsey.map((v) => TestPair(v, false)),
                            ...invalid.map((v) => TestPair(v, null))
                        ]);
                    });

                    it("should deserialise a number array", function ()
                    {
                        test(ArrayOf(Number),
                        [
                            TestPair("", []),
                            TestPair("3|", [3]),
                            TestPair("1|5|10|20|50|100|200|500|1000|", [1, 5, 10, 20, 50, 100, 200, 500, 1000])
                        ]);
                    });

                    it("should deserialise an indexed number array", function ()
                    {
                        test(ArrayOf(Number, { indexed: true }),
                        [
                            TestPair("", []),
                            TestPair("0>3|", [3]),
                            TestPair("0>1|1>5|2>10|3>20|4>50|5>100|6>200|7>500|8>1000|", [1, 5, 10, 20, 50, 100, 200, 500, 1000])
                        ]);
                    });

                    it("should deserialise a character array", function ()
                    {
                        test(CharArray,
                        [
                            TestPair("163|", "£"),
                            TestPair("112|", "p"),
                            TestPair("65|", "A"),
                            TestPair("97|", "a"),
                            TestPair("|", ""),

                            TestPair("a|", "a"),
                            TestPair("abc|", "abc"),

                            TestPair("-1|", "-1"),
                            TestPair("", "")
                        ]);
                    });

                    it("should deserialise a tuple", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "0", type: Number })
                            public propA: number;
                            @Property({ key: "1", type: Boolean })
                            public propB: boolean;
                            @Property({ key: "2", type: String })
                            public propC: string;
                        }

                        const obj = new MockObject();
                        obj.propA = 3;
                        obj.propB = true;
                        obj.propC = "0";

                        test(Tuple(MockObject),
                        [
                            TestPair("3|1|0|", obj)
                        ]);
                    });

                    it("should deserialise a map", function ()
                    {
                        test(Map(Number, { keyValueDelimiter: ",", keyValueSeparator: ":" }),
                        [
                            TestPair("a:0,b:1,c:2", { a: 0, b: 1, c: 2 }),
                            TestPair("", {})
                        ]);
                    });

                    it("should deserialise an inner object", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "PA", type: Boolean })
                            public propA: boolean;
                            @Property({ key: "PB", type: String })
                            public propB: string;
                        }

                        const complete = new MockObject();
                        complete.propA = true;
                        complete.propB = "hi";

                        const partial = new MockObject();
                        partial.propB = "hi";
                        partial.propA = null;

                        const nothing = new MockObject();
                        nothing.propA = null;
                        nothing.propB = null;

                        test(Object(MockObject, { keyValueDelimiter: ",", keyValueSeparator: ":" }),
                        [
                            TestPair("PA:1,PB:hi", complete),
                            TestPair("PB:hi", partial),
                            TestPair("", nothing),
                            TestPair("invalid", nothing),
                            TestPair("missing:1", nothing)
                        ]);
                    });

                    describe("key arrays", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "P_{#}", type: String, multiKey: true })
                            public prop: string[];
                        }

                        it("should parse a 1-dimensional key array", function ()
                        {
                            const obj = new MockObject();
                            deserialise(obj, stringifyFn({ P_0: "A", P_1: "B" }));
                            expect(obj.prop).to.deep.equal(["A", "B"]);
                        });

                        it("should create an empty array if the data does not include the key", function ()
                        {
                            const obj = new MockObject();
                            deserialise(obj, stringifyFn({}));
                            expect(obj.prop).to.deep.equal([]);
                        });
                    });
                });
            }

            testFormat(Format.Message, MessageParser.jsonToMessage);
            testFormat(Format.JSON, JSON.stringify);
        });

        describe("serialise", function ()
        {
            interface TestPair<T>
            {
                input: T;
                output: string;
            }

            function TestPair<T>(input: T, output: string): TestPair<T>
            {
                return { input, output };
            }

            function getTestFn(stringifyFn: StringifyFunc, format: Format)
            {
                return function <T>(prop: SerialiserPair<T>, pairs: TestPair<T>[])
                {
                    class MockObject
                    {
                        @Property({ key: "P", type: prop })
                        public prop: T;
                    }

                    for (const pair of pairs)
                    {
                        const obj = new MockObject();
                        obj.prop = pair.input;
                        const serialised = serialise(obj, format);

                        const str = pair.output == null ? stringifyFn({}) : stringifyFn({ P: pair.output });
                        expect(serialised).to.equal(str);
                    }
                };
            }

            function testFormat(format: Format, stringifyFn: StringifyFunc)
            {
                const test = getTestFn(stringifyFn, format);
                describe(`Format.${Format[format]}`, function ()
                {
                    it("should use a default value for a null property if defaultValue is set", function ()
                    {
                        const defaultValue = "hi";

                        class MockObject
                        {
                            @Property({ key: "P", type: String, defaultValue })
                            public prop: string;
                        }

                        const obj = new MockObject();
                        const serialised = serialise(obj, format);
                        expect(serialised).to.equal(stringifyFn({ P: defaultValue }));
                    });

                    it("should throw if a property is missing and required is true", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "P", type: String, required: true })
                            public prop: string;
                        }

                        const obj = new MockObject();
                        expect(() => serialise(obj, format)).to.throw();
                    });

                    it("should use a default value for a null property if defaultValue is set and required is true", function ()
                    {
                        const defaultValue = "hi";

                        class MockObject
                        {
                            @Property({ key: "P", type: String, defaultValue, required: true })
                            public prop: string;
                        }

                        const obj = new MockObject();
                        const serialised = serialise(obj, format);
                        expect(serialised).to.equal(stringifyFn({ P: defaultValue }));
                    });

                    it("should serialise a string", function ()
                    {
                        const strs = ["my arbitrary string", "", " ", "  ", " my bad  str   "];
                        test(String,
                        [
                            ...strs.map((str) => TestPair(str, str)),

                            TestPair(true, "true"),
                            TestPair(false, "false"),
                            TestPair({}, "[object Object]"),
                        ]);
                    });

                    it("should serialise a number", function ()
                    {
                        test(Number,
                        [
                            TestPair(2.5, "2.5"),
                            TestPair(2, "2")
                        ]);
                    });

                    it("should serialise an integer", function ()
                    {
                        test(Integer,
                        [
                            TestPair(2, "2"),

                            // Invalid values
                            TestPair(undefined, null),
                            TestPair(null, null),
                            TestPair(2.5, null),
                            TestPair(2.4, null)
                        ]);
                    });

                    it("should serialise a boolean", function ()
                    {
                        test(Boolean,
                        [
                            TestPair(true, "1"),
                            TestPair(false, "0"),

                            // Invalid values
                            TestPair(undefined, null),
                            TestPair(null, null)
                        ]);
                    });

                    it("should serialise a number array", function ()
                    {
                        test(ArrayOf(Number),
                        [
                            TestPair([], ""),
                            TestPair([3], "3|"),
                            TestPair([1, 5, 10, 20, 50, 100, 200, 500, 1000], "1|5|10|20|50|100|200|500|1000|")
                        ]);
                    });

                    it("should serialise an indexed number array", function ()
                    {
                        test(ArrayOf(Number, { indexed: true }),
                        [
                            TestPair([], ""),
                            TestPair([3], "0>3|"),
                            TestPair([1, 5, 10, 20, 50, 100, 200, 500, 1000], "0>1|1>5|2>10|3>20|4>50|5>100|6>200|7>500|8>1000|")
                        ]);
                    });

                    it("should serialise a character array", function ()
                    {
                        test(CharArray,
                        [
                            TestPair("£", "163|"),
                            TestPair("p", "112|"),
                            TestPair("abc", "97|98|99|")
                        ]);
                    });

                    it("should serialise a tuple", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "0", type: Number })
                            public propA: number;
                            @Property({ key: "1", type: Boolean })
                            public propB: boolean;
                            @Property({ key: "2", type: String })
                            public propC: string;
                        }

                        const complete = new MockObject();
                        complete.propA = 3;
                        complete.propB = true;
                        complete.propC = "0";

                        const partial = new MockObject();
                        partial.propA = 3;
                        partial.propC = "0";

                        const empty = new MockObject();

                        test(Tuple(MockObject),
                        [
                            TestPair(complete, "3|1|0|"),
                            TestPair(partial, "3|0|"),
                            TestPair(empty, "")
                        ]);
                    });

                    it("should serialise a map", function ()
                    {
                        test(Map(Number, { keyValueDelimiter: ",", keyValueSeparator: ":" }),
                        [
                            TestPair({ a: 0, b: 1, c: 2 }, "a:0,b:1,c:2"),
                            TestPair({}, "")
                        ]);
                    });

                    it("should serialise an inner object", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "PA", type: Boolean })
                            public propA: boolean;
                            @Property({ key: "PB", type: String })
                            public propB: string;
                        }

                        const complete = new MockObject();
                        complete.propA = true;
                        complete.propB = "hi";

                        const partial = new MockObject();
                        partial.propB = "hi";
                        partial.propA = null;

                        const nothing = new MockObject();
                        nothing.propA = null;
                        nothing.propB = null;

                        test(Object(MockObject, { keyValueDelimiter: ",", keyValueSeparator: ":" }),
                        [
                            TestPair(complete, "PA:1,PB:hi"),
                            TestPair(partial, "PB:hi"),
                            TestPair(nothing, "")
                        ]);
                    });

                    describe("key arrays", function ()
                    {
                        class MockObject
                        {
                            @Property({ key: "P_{#}", type: String, multiKey: true })
                            public prop: string[];
                        }

                        it("should serialise a 1-dimensional key array", function ()
                        {
                            const obj = new MockObject();
                            obj.prop = ["A", "B"];
                            const serialised = serialise(obj, format);
                            expect(serialised).to.equal(stringifyFn({ P_0: "A", P_1: "B" }));
                        });
                    });
                });
            }

            testFormat(Format.Message, MessageParser.jsonToMessage);
            testFormat(Format.JSON, JSON.stringify);
        });
    });
}