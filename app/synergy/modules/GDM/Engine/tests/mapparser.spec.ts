namespace RS.GDM.Engine.Serialisation.MapParser.Tests
{
    const { expect } = chai;

    describe("MapParser.ts", function ()
    {
        it("should deserialise an arbitrary simple key-value string", function ()
        {
            const map = MapParser.stringToMap("PropertyA:ValueA,PropertyB:ValueB", { keyValueDelimiter: ",", keyValueSeparator: ":" });
            expect(map).to.deep.equal({ "PropertyA": "ValueA", "PropertyB": "ValueB" });
        });

        it("should deserialise an arbitrary simple key-value string with a start and end string", function ()
        {
            const map = MapParser.stringToMap("--PropertyA:ValueA,PropertyB:ValueB++", { keyValueDelimiter: ",", keyValueSeparator: ":", start: "--", end: "++" });
            expect(map).to.deep.equal({ "PropertyA": "ValueA", "PropertyB": "ValueB" });
        });

        it("should serialise an arbitrary simple key-value string", function ()
        {
            const string = MapParser.mapToString({ "PropertyA": "ValueA", "PropertyB": "ValueB" }, { keyValueDelimiter: ",", keyValueSeparator: ":" });
            expect(string).to.equal("PropertyA:ValueA,PropertyB:ValueB");
        });

        it("should serialise an arbitrary simple key-value string with a start and end character", function ()
        {
            const string = MapParser.mapToString({ "PropertyA": "ValueA", "PropertyB": "ValueB" }, { keyValueDelimiter: ",", keyValueSeparator: ":", start: "--", end: "++" });
            expect(string).to.equal("--PropertyA:ValueA,PropertyB:ValueB++");
        });
    })
}