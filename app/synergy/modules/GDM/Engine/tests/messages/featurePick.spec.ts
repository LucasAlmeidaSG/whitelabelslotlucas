namespace RS.GDM.Engine.Messages.Tests
{
    const { expect } = chai;

    describe("BetMessage.ts", function ()
    {
        describe("BetRequest", function ()
        {
            it("should correctly serialise a client FEATURE_PICK message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const request = new Messages.BetRequest(models, { isRecovering: true });
                request.type = MessageTypes.FeaturePick;
                request.currentFeature = 0;
                request.featurePick = new FeaturePick();
                request.featurePick.pick = 1;
                request.featurePick.round = 1;
                request.featurePick.value = 1;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "FEATURE_PICK",
                    "REC": "1",
                    "CFG": "0",
                    "FP": "1|1|1"
                });
            });
        });

        describe("BetResponse", function ()
        {
            it("should correctly deserialise a server FEATURE_PICK message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const responseString =
                [
                    "&MSGID=FEATURE_PICK&B=103884&AB=103884&VER=2.6.3-2.4.17-4-4",
                    "&FID=0|&CFG=0&FS_0=1&NFR_0=1&FPM_0=4;|&CFR_0=1&CFP_0=1",
                    "&IFG=0&TW=500&GSD=FT~0&GA=0&SID=SESSION00000&"
                ].join("");
                const response = new Messages.BetResponse(models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.FeaturePick);
                expect(response.accountBalance).to.equal(103884);
                expect(response.version).to.equal("2.6.3-2.4.17-4-4");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(false);

                /** Common game message testing */
                expect(response.totalWin).to.equal(500);
                expect(response.gameSpecificData).to.be.an.instanceOf(Messages.GameSpecifcData);

                /** Message specific testing */
                expect(response.featureIDs).to.deep.equal([0]);
                expect(response.currentFeature).to.equal(0);
                expect(response.featureStarted).to.deep.equal([true]);
                expect(response.numberOfFeatureRounds).to.deep.equal([1]);

                expect(response.pickValues).to.deep.equal(
                    [
                        [
                            [ 4 ]
                        ]
                    ]);
                expect(response.currentRound).to.deep.equal([1]);
                expect(response.currentPick).to.deep.equal([1]);
            });
        });
    });
}