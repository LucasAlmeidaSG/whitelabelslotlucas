namespace RS.GDM.Engine.Messages.Tests
{
    const { expect } = chai;

    describe("InitMessage.ts", function ()
    {
        describe("InitRequest", function ()
        {
            it("should correctly serialise a client INIT message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const request = new Messages.InitRequest(models, { isRecovering: true });
                request.type = MessageTypes.Init;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "INIT",
                    "REC": "1"
                });
            });
        });

        describe("InitResponse", function ()
        {
            it("should correctly deserialise a server INIT message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const responseString =
                [
                    "&MSGID=INIT&B=100000&AB=100000&VER=2.6.3-2.4.17-4-11&LIM=1|100000|",
                    "&BD=1|5|10|20|50|100|200|500|1000|&BDD=10&RSTM=1;0|",
                    "&UGB=1&CUR=ISO:GBP|,|.|163;|L|112;|R&GA=0&SID=SESSION00000&"
                ].join("");
                const response = new Messages.InitResponse(models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.Init);
                expect(response.accountBalance).to.equal(100000);
                expect(response.version).to.equal("2.6.3-2.4.17-4-11");
                expect(response.canGamble).to.equal(false);
                expect(response.sessionID).to.equal("SESSION00000");

                /** Message specific testing */
                expect(response.betLimits).to.deep.equal([1, 100000]);
                expect(response.betDenominations).to.deep.equal([1, 5, 10, 20, 50, 100, 200, 500, 1000]);
                expect(response.useGambleButton).to.equal(true);
                expect(response.isRecovering).to.equal(false);

                expect(response.currencyData).to.be.an.instanceOf(Messages.CurrencyData);
                expect(response.currencyData.gdmCode).to.equal("ISO:GBP");
                expect(response.currencyData.isoCode).to.equal("GBP");

                expect(response.currencyData.groupingSeparator).to.equal(",");
                expect(response.currencyData.fractionalSeparator).to.equal(".");

                expect(response.currencyData.integerSymbol).to.equal("£");
                expect(response.currencyData.integerSymbolPos).to.equal("L");

                expect(response.currencyData.fractionalSymbol).to.equal("p");
                expect(response.currencyData.fractionalSymbolPos).to.equal("R");
            });
        });

        describe("InitSequence", function ()
        {
            class MockRecoveryResponse
            {
                @Serialisation.Property({ key: "MD", type: Serialisation.Boolean })
                public mockData: boolean;
            }

            class MockBetLogic implements IBetLogic<RS.Models, MockRecoveryResponse>
            {
                public createResponse()
                {
                    return new MockRecoveryResponse();
                }

                public populateModels(models: RS.Models, response: MockRecoveryResponse)
                {
                    return;
                }
            }

            it("should execute the init request-response sequence", async function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const responseString =
                [
                    "&MSGID=INIT&B=100000&AB=100000&VER=2.6.3-2.4.17-4-11&LIM=1|100000|",
                    "&BD=1|5|10|20|50|100|200|500|1000|&BDD=10&RSTM=1;0|",
                    "&UGB=1&CUR=ISO:GBP|,|.|163;|L|112;|R&GA=0&SID=SESSION00000&"
                ].join("");
                const server = new MockServerConnector(responseString);

                const sequence = new InitSequence({ initResponse: InitResponse, betResponse: BetResponse });
                sequence.serverConnector = server;
                await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);

                expect(models.customer.finalBalance.primary).to.equal(100000);

                expect(models.customer.currencyCode).to.equal("GBP");

                expect(models.customer.currencySettings.name).to.equal("GBP");
                expect(models.customer.currencySettings.symbol).to.equal("£");

                expect(models.customer.currencySettings.currencySymbolBeforeAmount).to.equal(true);
                expect(models.customer.currencySettings.currencyCodeBeforeAmount).to.equal(true);

                expect(models.customer.currencySettings.displaySymbol).to.equal(true);
                expect(models.customer.currencySettings.displayCode).to.equal(false);

                // Hard codings
                expect(models.customer.currencyMultiplier).to.equal(1.0);
                expect(models.customer.currencySettings.grouping).to.equal(3);
                expect(models.customer.currencySettings.fractionalDigits).to.equal(2);
                expect(models.customer.currencySettings.currencyCodeSpacing).to.equal(0);
                expect(models.customer.currencySettings.currencySymbolSpacing).to.equal(0);
            });

            it("should not publish onRecoveryStart if the init response does not include R=1", async function ()
            {
                const responseString =
                [
                    "&MSGID=INIT&B=100000&AB=100000&VER=2.6.3-2.4.17-4-11&LIM=1|100000|",
                    "&BD=1|5|10|20|50|100|200|500|1000|&BDD=10&RSTM=1;0|",
                    "&UGB=1&CUR=ISO:GBP|,|.|163;|L|112;|R&GA=0&SID=SESSION00000&"
                ].join("");
                const server = new MockServerConnector(responseString);

                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                let recovering = false;
                const sequence = new InitSequence({ initResponse: InitResponse, betResponse: BetResponse });
                sequence.onRecoveryStart.once(() => recovering = true);
                sequence.serverConnector = server;
                await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);
                expect(recovering).to.equal(false);
            });

            it("should publish onRecoveryStart if the init response includes R=1", async function ()
            {
                const responseString =
                [
                    "&MSGID=INIT&B=100000&AB=100000&VER=2.6.3-2.4.17-4-11&LIM=1|100000|",
                    "&BD=1|5|10|20|50|100|200|500|1000|&BDD=10&RSTM=1;0|&TW=100",
                    "&UGB=1&CUR=ISO:GBP|,|.|163;|L|112;|R&GA=0&SID=SESSION00000&R=1&"
                ].join("");
                const server = new MockServerConnector(responseString);

                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                let recovering = false;
                const sequence = new InitSequence({ initResponse: InitResponse, betResponse: BetResponse });
                sequence.onRecoveryStart.once(() => recovering = true);
                sequence.serverConnector = server;
                await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);
                expect(recovering).to.equal(true);
            });
        });
    });
}