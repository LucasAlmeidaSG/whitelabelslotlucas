namespace RS.GDM.Engine.Messages.Tests
{
    const { expect } = chai;

    describe("BetMessage.ts", function ()
    {
        describe("BetRequest", function ()
        {
            it("should correctly serialise a client FEATURE_START message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const request = new Messages.BetRequest(models, { isRecovering: true });
                request.type = MessageTypes.FeatureStart;
                request.currentFeature = 0;

                const json = JSON.parse(Serialisation.serialise(request, Serialisation.Format.JSON));
                expect(json).to.deep.equal(
                {
                    "MSGID": "FEATURE_START",
                    "REC": "1",
                    "CFG": "0"
                });
            });
        });

        describe("BetResponse", function ()
        {
            it("should correctly deserialise a server FEATURE_START message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const responseString =
                [
                    "&MSGID=FEATURE_START&B=103884&AB=103884&VER=2.6.3-2.4.17-4-4",
                    "&IFG=0&TW=500&GSD=FT~0&GA=0&SID=SESSION00000&"
                ].join("");
                const response = new Messages.BetResponse(models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                /** Base message testing */
                expect(response.type).to.equal(MessageTypes.FeatureStart);
                expect(response.accountBalance).to.equal(103884);
                expect(response.version).to.equal("2.6.3-2.4.17-4-4");
                expect(response.sessionID).to.equal("SESSION00000");
                expect(response.canGamble).to.equal(false);

                /** Common game message testing */
                expect(response.totalWin).to.equal(500);
                expect(response.gameSpecificData).to.be.an.instanceOf(Messages.GameSpecifcData);
            });
        });
    });
}