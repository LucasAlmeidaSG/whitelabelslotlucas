namespace RS.GDM.Engine.Messages.Tests
{
    const { expect } = chai;

    describe("BaseMessage.ts", function ()
    {
        describe("ErrorResponse", function ()
        {
            it("should correctly deserialise from an error message", function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const responseString = "&MSGID=ERROR&EID=ERROR_COMMUNICATIONS&AB=18562556&FRBAL=0&SID=SESSION00000&RNG=-1&RNG_RANGE=-1&";
                const response = new Messages.ErrorResponse(models);
                Serialisation.deserialise(response, responseString, Serialisation.Format.Message);

                expect(response.type).to.equal(MessageTypes.Error);
                expect(response.errorCode).to.equal("ERROR_COMMUNICATIONS");
            });
        });

        describe("MessageSequence", function ()
        {
            class MockSequence<TModels extends RS.Models> extends MessageSequence<TModels, CommonMessagePayload>
            {
                protected createRequest(models: TModels, payload: CommonMessagePayload)
                {
                    return new Request(models, payload);
                }

                protected createResponse(models: TModels)
                {
                    return new Response(models);
                }

                protected getMessageType()
                {
                    return "MOCK";
                }

                protected populateModels(models: TModels, responseStr: string)
                {
                    models.customer.finalBalance.primary = 1000;
                }
            }

            it("should populate the models with a successful response", async function ()
            {
                const responseString = "&MSGID=MOCK&B=1000&AB=1000";
                const server = new MockServerConnector(responseString);

                const models: RS.Models = {} as any;
                RS.Models.clear(models);
                expect(models.customer.finalBalance.primary, "should set up models correctly").not.to.equal(1000);

                const sequence = new MockSequence();
                sequence.serverConnector = server;
                await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);

                expect(models.customer.finalBalance.primary).to.equal(1000);
            });

            it("should handle an error response", async function ()
            {
                const responseString = "&MSGID=ERROR&EID=ERROR_COMMUNICATIONS&AB=18562556&FRBAL=0&SID=SESSION00000&RNG=-1&RNG_RANGE=-1&";
                const server = new MockServerConnector(responseString);

                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const sequence = new MockSequence();
                sequence.serverConnector = server;
                await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);

                expect(models.error.type).to.equal(RS.Models.Error.Type.Server);

                expect(server.lastError.code).to.equal("ERROR_COMMUNICATIONS");
                expect(server.lastError.message).to.be.oneOf([undefined, "ERROR_COMMUNICATIONS"]);
            });

            it("should throw if execute is called with no server connector defined", async function ()
            {
                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const sequence = new MockSequence();
                try
                {
                    await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);
                    expect.fail(null, null, "expected sequence.execute to throw an error");
                }
                catch (err)
                {
                    // Success
                }
            });

            it("should throw if the server returns a response with the wrong MSGID", async function ()
            {
                const responseString = "&MSGID=NOTMOCK&DATA=HERE&YES=OK&";
                const server = new MockServerConnector(responseString);

                const models: RS.Models = {} as any;
                RS.Models.clear(models);

                const sequence = new MockSequence();
                sequence.serverConnector = server;

                try
                {
                    await sequence.execute(models, { isRecovering: false }, Serialisation.Format.JSON);
                    expect.fail(null, null, "expected sequence.execute to throw an error");
                }
                catch (err)
                {
                    // Success
                }
            });
        });
    });

    /** Mock server communicator for testing sequence classes. */
    export class MockServerConnector implements Environment.IServerConnector
    {
        private readonly __promise: Promise<string>;

        private __lastError: GDM.Environment.IError;
        public get lastError(): Readonly<GDM.Environment.IError> { return this.__lastError; }

        constructor(response: string)
        {
            this.__promise = Promise.resolve(response);
        }

        public sendServerMessage(message: string): PromiseLike<string>
        {
            return this.__promise;
        }

        public notifyError(code: string, message?: string)
        {
            this.__lastError = { code, message };
        }
    }
}