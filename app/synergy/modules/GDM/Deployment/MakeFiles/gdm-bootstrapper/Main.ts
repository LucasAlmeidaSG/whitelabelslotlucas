interface FetchInfo
{
    files: FileInfo[];
    version?: string;
}

interface FileInfo
{
    path: string;
    sha256?: string;
}

const req = new XMLHttpRequest();
const cacheFactor = 1000 * 60;
const cache = Math.floor(Date.now() / cacheFactor);
req.open("GET", `scripts.json?cache_bust=${cache}`);
req.send();
req.addEventListener("load", () =>
{
    if (req.status !== 200)
    {
        throw new Error(`Failed to fetch bootstrapping information`);
    }

    try
    {
        let json: FetchInfo;
        if (typeof req.response === "string")
        {
            json = JSON.parse(req.response) as FetchInfo;
        }
        else
        {
            json = req.response as FetchInfo;
        }

        const version = json.version;
        const files = json.files;

        for (const { path, sha256 } of files)
        {
            const tag = document.createElement("script");
            tag.type = "text/javascript";
            if (version)
            {
                tag.src = `${path}?gamever=${version}`;
            }
            if (sha256)
            {
                tag.integrity = `sha256-${sha256}`;
            }
            tag.crossOrigin = "anonymous";
            tag.async = true;
            tag.defer = true;
            document.body.appendChild(tag);
        }

        const el = document.getElementById("bootstrapper");
        el.remove();
    }
    catch (err)
    {
        throw new Error(`Bootstrapping failed: ${err}`);
    }
});