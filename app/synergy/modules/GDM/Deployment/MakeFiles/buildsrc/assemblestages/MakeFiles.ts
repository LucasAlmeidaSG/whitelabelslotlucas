namespace RS.GDM.AssembleStages
{
    import Path = RS.Path;
    import FileSystem = RS.FileSystem;

    /**
     * Responsible for outputting a valid GDM file structure in the assemble directory.
     */
    export class MakeFiles extends RS.AssembleStage.Base
    {
        public async execute(settings: RS.AssembleStage.AssembleSettings, moduleList: RS.List<RS.Module.Base>)
        {
            await this.prepareFetchInfo(settings);
            await this.preparePage(settings);
            await this.prepareLocaleMapping(settings);
        }

        public async executeModule(settings: RS.AssembleStage.AssembleSettings, module: RS.Module.Base) { throw new Error("Unused"); }

        protected async preparePage(settings: RS.AssembleStage.AssembleSettings)
        {
            const moduleName = "GDM.MakeFiles";
            const module = RS.Module.getModule(moduleName);
            if (!module) { throw new Error(`Module ${moduleName} not found`); }

            const htmlPath = Path.combine(module.path, "html", "index.htm");
            const templateText = await FileSystem.readFile(htmlPath);

            const bootstrapperPath = Path.combine(module.path, "build", "bootstrapper.js");
            const bootstrapper = await FileSystem.readFile(bootstrapperPath);

            const htmlText = Util.replaceAll(templateText,
            {
                "{ ClientVersion }": EnvArgs.version.value,
                "{ BootstrapScript }": `<script id="bootstrapper" type="text/javascript">${bootstrapper}</script>`
            });
            const outputPath = Path.combine(settings.outputDir, "index.htm");
            await FileSystem.writeFile(outputPath, htmlText);
        }

        protected async prepareLocaleMapping(settings: RS.AssembleStage.AssembleSettings)
        {
            const mapping = await this.generateLocaleMapping(settings);
            await FileSystem.writeFile(Path.combine(settings.outputDir, "lcmapping.json"), JSON.stringify(mapping));
        }

        protected async generateLocaleMapping(settings: RS.AssembleStage.AssembleSettings): Promise<GDM.LCMapping>
        {
            const mapping: GDM.LCMapping =
            {
                info:
                {
                    OGS:
                    {
                        mapping: await this.getLocaleMappingArray(settings),
                        defaultLanguage: this.generateKeyValuePair(RS.Localisation.Constants.referenceLocale)
                    }
                }
            };
            return mapping;
        }

        protected async prepareFetchInfo(settings: RS.AssembleStage.AssembleSettings)
        {
            const info: FetchInfo = { files: [], version: EnvArgs.version.value };
            const files = ["thirdparty", "app"];
            for (const file of files)
            {
                const path = Path.combine("js", Path.replaceExtension(file, ".js"));
                const scriptText = await FileSystem.readFile(Path.combine(settings.outputDir, path));
                const sha256 = await Checksum.sha256(scriptText);
                info.files.push({ path, sha256 });
            }
            await FileSystem.writeFile(Path.combine(settings.outputDir, "scripts.json"), JSON.stringify(info));
        }

        protected async getLocaleMappingArray(settings: RS.AssembleStage.AssembleSettings): Promise<GDM.KeyValuePair[]>
        {
            const arr: GDM.KeyValuePair[] = [];
            const localeFiles = await FileSystem.readDirectory(Path.combine(settings.outputDir, "translations"), false);
            const mappedLocales: { [locale: string]: boolean } = {};

            // Apply simple mappings
            for (const localeFile of localeFiles)
            {
                const baseName = Path.baseName(localeFile);
                const ext = Path.extension(localeFile);
                const localeCode = baseName.substr(0, baseName.length - ext.length);

                const kv = this.generateKeyValuePair(localeCode);
                arr.push(kv);
                mappedLocales[kv.key] = true;
            }

            // Apply fallback mappings
            for (const ogsLocaleCode of knownLocales)
            {
                if (mappedLocales[ogsLocaleCode]) { continue; }
                // Key won't exist, set to explicitly false
                mappedLocales[ogsLocaleCode] = false;

                const [languageCode] = ogsLocaleCode.split("_");
                for (const localeFile of localeFiles)
                {
                    const baseName = Path.baseName(localeFile);
                    const ext = Path.extension(localeFile);
                    const localeCode = baseName.substr(0, baseName.length - ext.length);

                    const [thisLanguageCode] = localeCode.split("_");
                    if (thisLanguageCode === languageCode)
                    {
                        const kv: KeyValuePair = { key: ogsLocaleCode, value: localeCode };
                        arr.push(kv);
                        mappedLocales[ogsLocaleCode] = true;
                        break;
                    }
                }
            }

            return arr;
        }

        protected generateKeyValuePair(localeCode: string): KeyValuePair
        {
            const ogsLocaleCode = localeCode.toLowerCase();
            return { key: ogsLocaleCode, value: localeCode };
        }
    }

    export const makeFiles = new MakeFiles();
    RS.AssembleStage.register({ after: [ RS.AssembleStages.code ], last: true }, makeFiles);
}