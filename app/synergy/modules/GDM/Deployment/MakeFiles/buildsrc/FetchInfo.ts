namespace RS.GDM
{
    export interface FetchInfo
    {
        files: FileInfo[];
        version: string;
    }

    export interface FileInfo
    {
        path: string;
        sha256?: string;
    }
}