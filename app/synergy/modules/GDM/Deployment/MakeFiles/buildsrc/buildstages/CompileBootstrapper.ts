namespace RS.GDM.BuildStages
{
    export class CompileBootstrapper extends BuildStage.Base
    {
        public async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            if (!await module.hasCode(CompilationUnits.bootstrapperPath)) { return { workDone: false, errorCount: 0 }; }
            const unit = module.addCompilationUnit(CompilationUnits.bootstrapper, CompilationUnits.bootstrapperPath);
            const result = await Build.compileUnit(unit);
            if (result.workDone)
            {
                await module.saveChecksums();
            }
            return result;
        }
    }

    export const compileBootstrapper = new CompileBootstrapper();
    BuildStage.register({}, compileBootstrapper);
}