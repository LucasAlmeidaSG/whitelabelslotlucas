declare namespace RS.GDM
{
    /** A locale mapping file. */
    export interface LCMapping
    {
        info:
        {
            OGS:
            {
                mapping: KeyValuePair[];
                defaultLanguage: KeyValuePair;
            }
        }
    }

    export interface KeyValuePair
    {
        key: string;
        value: string;
    }
}