<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>desktop</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>4096</int>
                    <key>height</key>
                    <int>4096</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>mobile</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>2048</int>
                    <key>height</key>
                    <int>2048</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string>lowperf</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>1024</int>
                    <key>height</key>
                    <int>1024</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>red7</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <true/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>{v}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">raw_ui/autoplay_button/autoplay_open_button.png</key>
            <key type="filename">raw_ui/autoplay_button/autoplay_open_button_press.png</key>
            <key type="filename">raw_ui/autoplay_button/autoplay_open_disabled.png</key>
            <key type="filename">raw_ui/autoplay_button/autoplay_open_hover.png</key>
            <key type="filename">raw_ui/stake_button/stake_close_button.png</key>
            <key type="filename">raw_ui/stake_button/stake_close_button_disabled.png</key>
            <key type="filename">raw_ui/stake_button/stake_close_button_hover.png</key>
            <key type="filename">raw_ui/stake_button/stake_close_button_press.png</key>
            <key type="filename">raw_ui/stake_button/stake_open_button.png</key>
            <key type="filename">raw_ui/stake_button/stake_open_button_disabled.png</key>
            <key type="filename">raw_ui/stake_button/stake_open_button_hover.png</key>
            <key type="filename">raw_ui/stake_button/stake_open_button_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,45,91,91</rect>
                <key>scale9Paddings</key>
                <rect>45,45,91,91</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/back_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,23,23</rect>
                <key>scale9Paddings</key>
                <rect>11,11,23,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/button_panel_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,16,61,31</rect>
                <key>scale9Paddings</key>
                <rect>30,16,61,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/clock_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,23,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,23,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/help_button/help_button.png</key>
            <key type="filename">raw_ui/help_button/help_button_disabled.png</key>
            <key type="filename">raw_ui/help_button/help_button_hover.png</key>
            <key type="filename">raw_ui/help_button/help_button_press.png</key>
            <key type="filename">raw_ui/left_hand_button/left_hand_button.png</key>
            <key type="filename">raw_ui/left_hand_button/left_hand_button_disabled.png</key>
            <key type="filename">raw_ui/left_hand_button/left_hand_button_hover.png</key>
            <key type="filename">raw_ui/left_hand_button/left_hand_button_press.png</key>
            <key type="filename">raw_ui/settings_button/settings_button.png</key>
            <key type="filename">raw_ui/settings_button/settings_button_disabled.png</key>
            <key type="filename">raw_ui/settings_button/settings_button_hover.png</key>
            <key type="filename">raw_ui/settings_button/settings_button_press.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_off.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_off_disabled.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_off_hover.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_off_press.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_up.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_up_disabled.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_up_hover.png</key>
            <key type="filename">raw_ui/volume_button/button_volume_up_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,39,79,79</rect>
                <key>scale9Paddings</key>
                <rect>39,39,79,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/message_bar_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>242,10,484,19</rect>
                <key>scale9Paddings</key>
                <rect>242,10,484,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/meter_panel_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>169,30,337,61</rect>
                <key>scale9Paddings</key>
                <rect>169,30,337,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/settings_button/settings_close.png</key>
            <key type="filename">raw_ui/settings_button/settings_close_disabled.png</key>
            <key type="filename">raw_ui/settings_button/settings_close_hover.png</key>
            <key type="filename">raw_ui/settings_button/settings_close_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,39,77,77</rect>
                <key>scale9Paddings</key>
                <rect>39,39,77,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/spin_button/play_button.png</key>
            <key type="filename">raw_ui/spin_button/play_button_disabled.png</key>
            <key type="filename">raw_ui/spin_button/play_button_hover.png</key>
            <key type="filename">raw_ui/spin_button/play_button_press.png</key>
            <key type="filename">raw_ui/stop_button/button_stop.png</key>
            <key type="filename">raw_ui/stop_button/button_stop_disabled.png</key>
            <key type="filename">raw_ui/stop_button/button_stop_hover.png</key>
            <key type="filename">raw_ui/stop_button/button_stop_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>78,78,156,156</rect>
                <key>scale9Paddings</key>
                <rect>78,78,156,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/stake_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>82,25,164,49</rect>
                <key>scale9Paddings</key>
                <rect>82,25,164,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/stake_button/minus_button.png</key>
            <key type="filename">raw_ui/stake_button/minus_button_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,2,15,3</rect>
                <key>scale9Paddings</key>
                <rect>8,2,15,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/stake_button/plus_button.png</key>
            <key type="filename">raw_ui/stake_button/plus_button_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw_ui/stake_button/stake_minus_button.png</key>
            <key type="filename">raw_ui/stake_button/stake_minus_button_disabled.png</key>
            <key type="filename">raw_ui/stake_button/stake_minus_button_hover.png</key>
            <key type="filename">raw_ui/stake_button/stake_minus_button_press.png</key>
            <key type="filename">raw_ui/stake_button/stake_plus_button.png</key>
            <key type="filename">raw_ui/stake_button/stake_plus_button_disabled.png</key>
            <key type="filename">raw_ui/stake_button/stake_plus_button_hover.png</key>
            <key type="filename">raw_ui/stake_button/stake_plus_button_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,35,90,70</rect>
                <key>scale9Paddings</key>
                <rect>45,35,90,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>raw_ui/autoplay_button/autoplay_open_button.png</filename>
            <filename>raw_ui/autoplay_button/autoplay_open_button_press.png</filename>
            <filename>raw_ui/autoplay_button/autoplay_open_disabled.png</filename>
            <filename>raw_ui/autoplay_button/autoplay_open_hover.png</filename>
            <filename>raw_ui/back_icon.png</filename>
            <filename>raw_ui/button_panel_background.png</filename>
            <filename>raw_ui/stop_button/button_stop.png</filename>
            <filename>raw_ui/stop_button/button_stop_disabled.png</filename>
            <filename>raw_ui/stop_button/button_stop_hover.png</filename>
            <filename>raw_ui/stop_button/button_stop_press.png</filename>
            <filename>raw_ui/volume_button/button_volume_off.png</filename>
            <filename>raw_ui/volume_button/button_volume_off_disabled.png</filename>
            <filename>raw_ui/volume_button/button_volume_off_hover.png</filename>
            <filename>raw_ui/volume_button/button_volume_off_press.png</filename>
            <filename>raw_ui/volume_button/button_volume_up.png</filename>
            <filename>raw_ui/volume_button/button_volume_up_disabled.png</filename>
            <filename>raw_ui/volume_button/button_volume_up_hover.png</filename>
            <filename>raw_ui/volume_button/button_volume_up_press.png</filename>
            <filename>raw_ui/clock_icon.png</filename>
            <filename>raw_ui/help_button/help_button.png</filename>
            <filename>raw_ui/help_button/help_button_disabled.png</filename>
            <filename>raw_ui/help_button/help_button_hover.png</filename>
            <filename>raw_ui/help_button/help_button_press.png</filename>
            <filename>raw_ui/left_hand_button/left_hand_button.png</filename>
            <filename>raw_ui/left_hand_button/left_hand_button_disabled.png</filename>
            <filename>raw_ui/left_hand_button/left_hand_button_hover.png</filename>
            <filename>raw_ui/left_hand_button/left_hand_button_press.png</filename>
            <filename>raw_ui/message_bar_background.png</filename>
            <filename>raw_ui/meter_panel_background.png</filename>
            <filename>raw_ui/stake_button/minus_button.png</filename>
            <filename>raw_ui/stake_button/minus_button_hover.png</filename>
            <filename>raw_ui/stake_button/plus_button.png</filename>
            <filename>raw_ui/stake_button/plus_button_hover.png</filename>
            <filename>raw_ui/stake_button/stake_close_button.png</filename>
            <filename>raw_ui/stake_button/stake_close_button_disabled.png</filename>
            <filename>raw_ui/stake_button/stake_close_button_hover.png</filename>
            <filename>raw_ui/stake_button/stake_close_button_press.png</filename>
            <filename>raw_ui/stake_button/stake_minus_button.png</filename>
            <filename>raw_ui/stake_button/stake_minus_button_disabled.png</filename>
            <filename>raw_ui/stake_button/stake_minus_button_hover.png</filename>
            <filename>raw_ui/stake_button/stake_minus_button_press.png</filename>
            <filename>raw_ui/stake_button/stake_open_button.png</filename>
            <filename>raw_ui/stake_button/stake_open_button_disabled.png</filename>
            <filename>raw_ui/stake_button/stake_open_button_hover.png</filename>
            <filename>raw_ui/stake_button/stake_open_button_press.png</filename>
            <filename>raw_ui/stake_button/stake_plus_button.png</filename>
            <filename>raw_ui/stake_button/stake_plus_button_disabled.png</filename>
            <filename>raw_ui/stake_button/stake_plus_button_hover.png</filename>
            <filename>raw_ui/stake_button/stake_plus_button_press.png</filename>
            <filename>raw_ui/spin_button/play_button.png</filename>
            <filename>raw_ui/spin_button/play_button_disabled.png</filename>
            <filename>raw_ui/spin_button/play_button_hover.png</filename>
            <filename>raw_ui/spin_button/play_button_press.png</filename>
            <filename>raw_ui/settings_button/settings_button.png</filename>
            <filename>raw_ui/settings_button/settings_button_disabled.png</filename>
            <filename>raw_ui/settings_button/settings_button_hover.png</filename>
            <filename>raw_ui/settings_button/settings_button_press.png</filename>
            <filename>raw_ui/settings_button/settings_close.png</filename>
            <filename>raw_ui/settings_button/settings_close_disabled.png</filename>
            <filename>raw_ui/settings_button/settings_close_hover.png</filename>
            <filename>raw_ui/settings_button/settings_close_press.png</filename>
            <filename>raw_ui/stake_background.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>large-max-texture-size</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties">
            <key>plain::bool-property</key>
            <struct type="ExporterProperty">
                <key>value</key>
                <string>false</string>
            </struct>
            <key>plain::string-property</key>
            <struct type="ExporterProperty">
                <key>value</key>
                <string>hello world</string>
            </struct>
        </map>
    </struct>
</data>
