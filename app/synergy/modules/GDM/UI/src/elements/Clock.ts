/// <reference path="../generated/Assets.ts" />

namespace RS.GDM.UI
{
    const timePrefix = [ "00", "0", "" ];

    @HasCallbacks
    export class Clock extends Flow.BaseElement<Clock.Settings>
    {
        protected _label: Flow.Label;
        protected _icon: Flow.Image;
        protected _timer: number;
        protected _list: Flow.List;

        public constructor(settings: Clock.Settings)
        {
            super(settings, undefined);

            // Setup label
            this._list = Flow.list.create(this.settings.list, this);
            this._icon = Flow.image.create(this.settings.icon, this._list);
            this._label = Flow.label.create(settings.label, this._list);

            // Setup timer
            this._timer = setInterval(this.update, 1000) as any;
            this.update();
        }

        /**
         * Disposes this element.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }
            clearInterval(this._timer);
            super.dispose();
        }

        @Callback
        protected update()
        {
            const date = new Date(Date.now());
            const hours = date.getHours().toString();
            const minutes = date.getMinutes().toString();
            const time = `${timePrefix[hours.length]}${hours}:${timePrefix[minutes.length]}${minutes}`;
            this._label.text = time;
        }
    }

    export const clock = Flow.declareElement(Clock);

    export namespace Clock
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            label: Flow.Label.Settings;
            icon: Flow.Image.Settings;
            list: Flow.List.Settings;
        }

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Clock",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: -300, y: 50 },
            sizeToContents: true,
            label:
            {
                ...Flow.Label.defaultSettings,
                name: "Clock Label",
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                text: "",
                textColor: Util.Colors.white,
                dock: Flow.Dock.Fill
            },
            icon:
            {
                name: "Clock Icon",
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Assets.UI.Elements,
                animationName: Assets.UI.Elements.clock_icon,
                dock: Flow.Dock.Fill,
                sizeToContents: false,
                size: { w: 20, h: 20 }
            },
            list:
            {
                name: "Clock List",
                direction: Flow.List.Direction.LeftToRight,
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                expand: Flow.Expand.Allowed,
                spacing: Flow.Spacing.horizontal(5),
                spreadItems: true,
                evenlySpaceItems: true
            }
        };

        export let defaultPortraitSettings: Settings =
        {
            ...defaultSettings,
            floatOffset: { x: -300, y: 1015 }
        }

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            dockAlignment: { x: 0.0, y: 0.0 },
            floatPosition: { x: 0.0, y: 0.0 },
            floatOffset: { x: 60, y: 15 },
            label:
            {
                ...defaultSettings.label,
                fontSize: 45
            },
            icon:
            {
                ...defaultSettings.icon,
                size: { w: 45, h: 45 }
            }
        };
    }
}