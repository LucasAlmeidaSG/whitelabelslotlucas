/// <reference path="UISettings.ts" />

namespace RS.GDM.UI
{
    export namespace UISettings
    {
        /** Set true to use UISettings.settingsNew instead of UISettings.settings */
        export let UseNewSettings: boolean = false;

        //TODO 1.2 - change to defaultLandscape, export this and deprecate
        export const defaultLandscapeNew: LayoutSettings =
        {
            meterPanel:
            {
                ...MeterPanel.defaultSettingsNew
            },
            messageBar:
            {
                ...MessageBar.defaultSettings
            },
            backButton:
            {
                ...BackButton.defaultSettingsNew
            },
            clock:
            {
                ...Clock.defaultSettingsNew
            },
            spinButton:
            {
                ...SpinButton.defaultSettingsNew,
                left:
                {
                    ...SpinButton.defaultLeftSettingsNew
                },
                right:
                {
                    ...SpinButton.defaultSettingsNew
                }
            },
            autoplayButton:
            {
                ...AutoplayButton.defaultSettingsNew,
                left:
                {
                    ...AutoplayButton.defaultLeftSettingsNew
                },
                right:
                {
                    ...AutoplayButton.defaultSettingsNew
                }
            },
            stakeButton:
            {
                ...StakeButton.defaultSettingsNew,
                left:
                {
                    ...StakeButton.defaultLeftSettingsNew
                },
                right:
                {
                    ...StakeButton.defaultSettingsNew
                }
            },
            settingsPanel:
            {
                ...SettingsPanel.defaultSettingsNew,
                left:
                {
                    ...SettingsPanel.defaultLeftSettingsNew
                },
                right:
                {
                    ...SettingsPanel.defaultSettingsNew
                }
            },
            turboToggle:
            {
                ...TurboToggle.defaultSettingsNew,
                left:
                {
                    ...TurboToggle.defaultLeftSettingsNew
                },
                right:
                {
                    ...TurboToggle.defaultSettingsNew
                }
            }
        };

        //TODO 1.2 - change to defaultPortrait, export this and deprecate
        export const defaultPortraitNew: LayoutSettings =
        {
            meterPanel:
            {
                ...MeterPanel.defaultPortraitSettingsNew
            },
            messageBar:
            {
                ...MessageBar.defaultPortraitSettings
            },
            backButton:
            {
                ...BackButton.defaultSettingsNew
            },
            clock:
            {
                ...Clock.defaultSettingsNew
            },
            spinButton:
            {
                ...SpinButton.defaultPortraitSettingsNew
            },
            autoplayButton:
            {
                ...AutoplayButton.defaultPortraitSettingsNew
            },
            stakeButton:
            {
                ...StakeButton.defaultPortraitSettingsNew
            },
            settingsPanel:
            {
                ...SettingsPanel.defaultPortraitSettingsNew
            },
            turboToggle:
            {
                ...TurboToggle.defaultSettings,
                left:
                {
                    floatOffset: { x: -TurboToggle.defaultSettings.floatOffset.x, y: 1680 }
                },
                right:
                {
                    floatOffset: { x: TurboToggle.defaultSettings.floatOffset.x, y: 1680 }
                }
            }
        };

        //TODO 1.2 - change to settings, export this and deprecate
        export let settingsNew: Settings =
        {
            desktop:
            {
                landscape: defaultLandscapeNew,
                portrait: defaultPortraitNew
            },
            mobile:
            {
                landscape: defaultLandscapeNew,
                portrait: defaultPortraitNew
            }
        };
    }
}