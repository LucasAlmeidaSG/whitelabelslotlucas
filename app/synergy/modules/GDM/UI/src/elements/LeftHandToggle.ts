/// <reference path="../generated/Assets.ts" />

/// <reference path="ToggleSwitch.ts"/>

namespace RS.GDM.UI
{
    @HasCallbacks
    export class LeftHandToggle extends Flow.BaseElement<LeftHandToggle.Settings, LeftHandToggle.RuntimeData>
    {
        @AutoDispose protected _buttonLeft: Flow.Button;
        @AutoDispose protected _buttonRight: Flow.Button;

        /** Published when the back button has been clicked. */
        public get onButtonLeftClicked() { return this._buttonLeft.onClicked; }
        /** Published when the back button has been clicked. */
        public get onButtonRightClicked() { return this._buttonRight.onClicked; }

        public constructor(settings: LeftHandToggle.Settings, runtimeData: LeftHandToggle.RuntimeData)
        {
            super(settings, runtimeData);

            this._buttonLeft = Flow.button.create(this.settings.buttonLeft, this);
            this._buttonRight = Flow.button.create(this.settings.buttonRight, this);

            if (runtimeData.enableInteractionArbiter)
            {
                this._buttonRight.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._buttonLeft.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
            }

            this.toggle(false);
        }

        public toggle(isLeft: boolean)
        {
            if (isLeft)
            {
                this._buttonLeft.visible = false;
                this._buttonLeft.enabled = false;
                this._buttonRight.visible = true;
                this._buttonRight.enabled = true;
            }
            else
            {
                this._buttonLeft.visible = true;
                this._buttonLeft.enabled = true;
                this._buttonRight.visible = false;
                this._buttonRight.enabled = false;
            }
        }
    }

    export const leftHandToggle = Flow.declareElement(LeftHandToggle, true);

    export namespace LeftHandToggle
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            buttonLeft: Flow.Button.Settings;
            buttonRight: Flow.Button.Settings;
            list: Flow.List.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Allowed
        };

        const defaultButtonSettings: Flow.Button.Settings =
        {
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            textLabel: null,
            spacing: Flow.Spacing.none
        };

        export let defaultSettings: Settings =
        {
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            buttonLeft:
            {
                ...defaultButtonSettings,
                name: "Left Hand Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.left_hand_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.left_hand_button_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.left_hand_button_disabled },
                seleniumId: "LeftHandButton"
            },
            buttonRight:
            {
                ...defaultButtonSettings,
                name: "Right Hand Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.left_hand_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.left_hand_button_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.left_hand_button_disabled },
                seleniumId: "RightHandButton"
            },
            list:
            {
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.RightToLeft,
                expand: Flow.Expand.Allowed,
                spacing: Flow.Spacing.horizontal(5)
            }
        };
    }
}