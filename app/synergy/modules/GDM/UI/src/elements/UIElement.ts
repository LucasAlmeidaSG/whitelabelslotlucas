namespace RS.GDM.UI
{
    export type UIElement<T extends Partial<Flow.ElementProperties>> = T &
    {
        left?: Partial<T>;
        right?: Partial<T>;
    };

    export namespace UIElement
    {
        export function apply<T extends Partial<Flow.ElementProperties>>(element: Flow.GenericElement, settings: UIElement<T>, isLeft: boolean)
        {
            if (isLeft)
            {
                if (settings.left)
                {
                    element.apply(settings.left);
                }
            }
            else
            {
                if (settings.right)
                {
                    element.apply(settings.right);
                }
            }
        }
    }
}