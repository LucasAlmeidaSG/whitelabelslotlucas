/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/Translations.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class AutoplayButton extends Flow.BaseElement<AutoplayButton.Settings, AutoplayButton.RuntimeData>
    {
        protected _value: number;

        @AutoDispose protected _startButton: Flow.Button;
        @AutoDispose protected _stopButton: Flow.Button;
        @AutoDispose protected _meter: Flow.Meter;
        @AutoDispose protected _list: Flow.List;

        /** Gets or sets the win amount to display. */
        public get value() { return this._value; }
        public set value(value)
        {
            if (value === this._value) { return; }
            this._value = value;
        }

        /** Published when the autoplay button has been clicked. */
        public get onAutoplayClicked() { return this._startButton.onClicked; }

        /** Published when the autoplay button has been clicked. */
        public get onAutoplayStopClicked() { return this._stopButton.onClicked; }

        public constructor(settings: AutoplayButton.Settings, runtimeData: AutoplayButton.RuntimeData)
        {
            super(settings, runtimeData);

            this._list = Flow.list.create(this.settings.list, this);

            this._startButton = Flow.button.create(this.settings.playButton, this._list);
            this._stopButton = Flow.button.create(this.settings.stopButton, this._list);
            this._meter = Flow.meter.create(this.settings.meter, this._list);

            if (runtimeData.enableInteractionArbiter && runtimeData.canAutoplayObservable)
            {
                const enableAutoplay = Observable.reduce([runtimeData.enableInteractionArbiter, runtimeData.canAutoplayObservable], Arbiter.AndResolver);
                Disposable.bind(enableAutoplay, this);
                this._startButton.bindToObservables({ enabled: enableAutoplay });
            }
            if (runtimeData.autoplaysRemainingObservable)
            {
                this._meter.bindToObservables({ value: runtimeData.autoplaysRemainingObservable });
            }
            if (runtimeData.autoplaysRemainingObservable)
            {
                const autoplaysActive = runtimeData.autoplaysRemainingObservable.map((autoplaysRemaining) => autoplaysRemaining > 0);
                const autoplaysNotActive = runtimeData.autoplaysRemainingObservable.map((autoplaysRemaining) => autoplaysRemaining === 0);
                Disposable.bind(autoplaysActive, this);
                Disposable.bind(autoplaysNotActive, this);

                this._startButton.bindToObservables({ visible: autoplaysNotActive });
                this._stopButton.bindToObservables({ visible: autoplaysActive });
            }
        }

        public setVisible(value: boolean)
        {
            this._startButton.visible = value;
        }
    }

    export const autoplayButton = Flow.declareElement(AutoplayButton, true);

    export namespace AutoplayButton
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            playButton: Flow.Button.Settings;
            stopButton: Flow.Button.Settings;
            meter: Flow.Meter.Settings;
            list: Flow.List.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
            autoplaysRemainingObservable: IReadonlyObservable<number>;
            canAutoplayObservable?: IObservable<boolean>;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        const defaultButtonSettings: Flow.Button.Settings =
        {
            dock: Flow.Dock.Fill,
            spacing: Flow.Spacing.none,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            textLabel: null
        };

        /** @deprecated use newDefaultSettings */
        export let defaultSettings: Settings =
        {
            name: "Autoplay Button",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: 585, y: 455 },
            playButton:
            {
                ...defaultButtonSettings,
                name: "Autoplay Play Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.autoplay_open_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.autoplay_open_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.autoplay_open_button_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.autoplay_open_disabled },
                seleniumId: "Autoplay Play Button"
            },
            stopButton:
            {
                ...defaultButtonSettings,
                name: "Autoplay Stop Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop_disabled },
                seleniumId: "Autoplay Stop Button"
            },
            meter:
            {
                dock: Flow.Dock.Fill,
                innerPadding: { top: 2.25, bottom: 0, left: 0, right: 0 },
                nameLabel:
                {
                    font: Fonts.Frutiger.CondensedMedium,
                    fontSize: 22,
                    text: "",
                    sizeToContents: true,
                    dock: Flow.Dock.Fill,
                    textColor: Util.Colors.black,
                    expand: Flow.Expand.Allowed
                },
                valueLabel:
                {
                    font: Fonts.Frutiger.CondensedMedium,
                    fontSize: 22,
                    text: "",
                    sizeToContents: true,
                    dock: Flow.Dock.Fill,
                    textColor: Util.Colors.black,
                    expand: Flow.Expand.Allowed
                },
                isCurrency: false,
                background:
                {
                    kind: Flow.Background.Kind.ImageFrame,
                    dock: Flow.Dock.Fill,
                    asset: Assets.UI.Elements,
                    frame: Assets.UI.Elements.button_panel_background,
                    sizeToContents: true,
                    expand: Flow.Expand.Disallowed,
                    scaleFactor: 0.56 //new asset is bigger, scale original down
                }
            },
            list:
            {
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.TopToBottom,
                expand: Flow.Expand.Allowed,
                spacing: Flow.Spacing.vertical(1)
            },
            sizeToContents: true
        };

        export let defaultLeftSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: defaultSettings.floatOffset.y }
        };
        export let defaultRightSettings: Partial<Settings> =
        {
            floatOffset: defaultSettings.floatOffset
        };
        export let defaultLeftPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: 1380 }
        };
        export let defaultRightPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: defaultSettings.floatOffset.x, y: 1380 }
        };

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            floatPosition: { x: 1.0, y: 0.5 },
            dockAlignment: { x: 1.0, y: 0.5 },
            floatOffset: { x: -115, y: 0 },
            scaleFactor: 0.6,
            stopButton:
            {
                ...defaultSettings.stopButton,
                scaleFactor: 0.58
            },
            meter:
            {
                ...defaultSettings.meter,
                background:
                {
                    ...defaultSettings.meter.background,
                    scaleFactor: 1
                },
                nameLabel:
                {
                    ...defaultSettings.meter.nameLabel,
                    font: Fonts.Frutiger.Bold,
                    fontSize: 42
                },
                valueLabel:
                {
                    ...defaultSettings.meter.valueLabel,
                    font: Fonts.Frutiger.Bold,
                    fontSize: 42
                }
            }
        };

        //TODO 1.2 - change to defaultLeftSettings, export this and deprecate
        export let defaultLeftSettingsNew: Partial<Settings> =
        {
            floatPosition: { x: 0.0, y: 0.5 },
            dockAlignment: { x: 0.0, y: 0.5 },
            floatOffset: { x: 115, y: 0 }
        };

        //TODO 1.2 - change to defaultPortraitSettings, export this and deprecate
        export let defaultPortraitSettingsNew: Settings =
        {
            ...defaultSettings,
            scaleFactor: 1.0,
            floatPosition: { x: 0.5, y: 1.0 },
            dockAlignment: { x: 0.5, y: 1.0 },
            floatOffset: { x: 350, y: -90 }
        };
    }
}