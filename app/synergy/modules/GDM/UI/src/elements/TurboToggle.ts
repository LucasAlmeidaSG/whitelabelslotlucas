/// <reference path="../generated/Assets.ts" />

/// <reference path="ToggleSwitch.ts"/>

namespace RS.GDM.UI
{
    @HasCallbacks
    export class TurboToggle extends Flow.BaseElement<TurboToggle.Settings, TurboToggle.RuntimeData>
    {
        @AutoDispose protected _icon: Flow.Image;
        @AutoDispose protected _toggleSwitch: UI.ToggleSwitch;

        protected _turboOn: boolean = null;

        /** Published when the back button has been clicked. */
        public get onTurboToggleOn() { return this._toggleSwitch.toggledOn; }
        public get onTurboToggleOff() { return this._toggleSwitch.toggledOff; }
        public get isTurboOn() { return this._turboOn; }
        public set isTurboOn(turboOn: boolean)
        {
            this._toggleSwitch.toggle(turboOn);
            this._turboOn = turboOn;
        }

        public constructor(settings: TurboToggle.Settings, runtimeData: TurboToggle.RuntimeData)
        {
            super(settings, runtimeData);

            this._toggleSwitch = UI.toggleSwitch.create(this.settings.toggleSwitch,
            {
                enableInteractionArbiter: this.runtimeData.enableInteractionArbiter
            });
            this.addChild(this._toggleSwitch);

            this._icon = Flow.image.create(this.settings.icon, this);
        }

        public toggle(value: boolean)
        {
            this._toggleSwitch.toggle(value);
        }
    }

    export const turboToggle = Flow.declareElement(TurboToggle, true);

    export namespace TurboToggle
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            icon: Flow.Image.Settings;
            toggleSwitch: UI.ToggleSwitch.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
        }

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Turbo Toggle",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: -585, y: 765 },
            spacing: Flow.Spacing.horizontal(5),
            icon:
            {
                name: "Lightning Icon",
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Assets.UI.Toggle,
                animationName: Assets.UI.Toggle.lightning_icon,
                dock: Flow.Dock.Left,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed
            },
            toggleSwitch:
            {
                ...UI.ToggleSwitch.defaultSettings,
                dock: Flow.Dock.Right
            },
            sizeToContents: true
        };

        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            floatPosition: { x: 1.0, y: 0.5 },
            dockAlignment: { x: 1.0, y: 0.5 },
            floatOffset: { x: -120, y: 380 },
            scaleFactor: 0.8,
        }

        export let defaultLeftSettingsNew: Partial<Settings> =
        {
            floatPosition: { x: 0.0, y: 0.5 },
            dockAlignment: { x: 0.0, y: 0.5 },
            floatOffset: { x: 120, y: 380 },
        }

        export let defaultPortraitSettings: Settings =
        {
            ...defaultSettingsNew,
            scaleFactor: 1.0,
            floatPosition: { x: 0.5, y: 1.0 },
            dockAlignment: { x: 0.5, y: 1.0 },
            floatOffset: { x: -25, y: -20 },
        }
    }
}