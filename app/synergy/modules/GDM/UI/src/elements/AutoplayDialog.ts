/// <reference path="RadioButton.ts" />
/// <reference path="../generated/Translations.ts"/>

namespace RS.GDM.UI
{
    /**
     * A dialog for selecting autoplay options.
     */
    @HasCallbacks
    export class AutoplayDialog extends Flow.BaseElement<AutoplayDialog.Settings, AutoplayDialog.RuntimeData>
    {
        @AutoDisposeOnSet protected _cancelButton: Flow.Button;
        @AutoDisposeOnSet protected _startButton: Flow.Button;
        @AutoDisposeOnSet protected _closeButton: Flow.Button;

        @AutoDispose protected _background: Flow.Background;
        @AutoDispose protected _topContainer: Flow.Container;
        @AutoDispose protected _title: Flow.Label;
        @AutoDispose protected _subtitle: Flow.Label;

        @AutoDisposeOnSet protected _spinsButtonList: Flow.Button[];
        @AutoDisposeOnSet protected _spinsButtonGrid: Flow.Grid;
        @AutoDisposeOnSet protected _buttonList: Flow.List;

        @AutoDisposeOnSet protected _advancedButtonOpen: Flow.Button;
        @AutoDisposeOnSet protected _advancedButtonClose: Flow.Button;
        @AutoDisposeOnSet protected _advancedButtonLabel: Flow.Label;

        @AutoDisposeOnSet protected _winExceedsRadio: UI.RadioButton;
        @AutoDisposeOnSet protected _cashDecreasedRadio: UI.RadioButton;

        @AutoDisposeOnSet protected _radioOptionsList: UI.RadioButton[];
        @AutoDisposeOnSet protected _inputList: Flow.TextBox[];

        @AutoDisposeOnSet protected readonly _spinCount: IObservable<number> = new Observable(0);
        @AutoDisposeOnSet protected readonly _lossLimit: IObservable<number> = new Observable(0);
        @AutoDisposeOnSet protected readonly _winLimit: IObservable<number> = new Observable(0);

        @AutoDisposeOnSet protected _stakeObservableHandler: IDisposable;

        protected _advancedSettingsIsOpen: Observable<boolean> = new Observable(false);
        protected _advancedSettingsIsClosed: Observable<boolean> = new Observable(true);

        /** Gets the current selected spin count. */
        public get spinCount() { return this._spinCount.value; }

        /** Gets the current selected loss limit (as a multiplier of total stake). */
        public get lossLimit() { return this._lossLimit.value; }

        /** Gets the current selected win limit (as a multiplier of total stake). */
        public get winLimit() { return this._winLimit.value; }

        /** Published when the cancel button has been clicked. */
        public get onCancelClicked() { return this._cancelButton.onClicked; }

        /** Published when the start button has been clicked. */
        public get onStartClicked() { return this._startButton.onClicked; }

        /** Published when the x button has been clicked. */
        public get onCloseClicked() { return this._closeButton.onClicked; }

        public constructor(settings: AutoplayDialog.Settings, runtimeData: AutoplayDialog.RuntimeData)
        {
            super(settings, runtimeData);

            this._background = Flow.background.create(settings.background, this);

            const list = Flow.list.create({
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.TopToBottom,
                expand: Flow.Expand.Allowed,
                spacing: Flow.Spacing.vertical(18)
            }, this);

            this._topContainer = Flow.container.create(settings.topContainer, list);
            this._title = Flow.label.create(settings.title, this._topContainer);
            this._subtitle = Flow.label.create(settings.subtitle, list);
            this._closeButton = Flow.button.create(settings.closeButton, this._topContainer);

            this._spinsButtonList = [];
            this._spinsButtonGrid = Flow.grid.create(settings.spinButtonGrid, list);
            for (let i = 0; i < settings.spinCountOptions.length; i++)
            {
                const button = Flow.button.create(settings.spinButton, this._spinsButtonGrid);
                button.label.text = settings.spinCountOptions[i].toString();
                button.onClicked(() => this.handleSpinsButtonClicked(i));
                this._spinsButtonList.push(button);
            }

            this._buttonList = Flow.list.create(settings.buttonList, list);

            this._cancelButton = Flow.button.create(settings.cancelButton, this._buttonList);
            this._startButton = Flow.button.create(settings.startButton, this._buttonList);

            const advancedContainer = Flow.container.create(
            {
                ...settings.topContainer,
                dock: Flow.Dock.Fill,
                spacing: Flow.Spacing.vertical(5)
            }, list);
            this._advancedButtonLabel = Flow.label.create(settings.advancedButtonTextLabel, advancedContainer);
            this._advancedButtonOpen = Flow.button.create(settings.advancedButtonOpen, advancedContainer);
            this._advancedButtonOpen.onClicked(this.openAdvancedSettings);
            this._advancedButtonOpen.bindToObservables({ enabled: this._advancedSettingsIsClosed, visible: this._advancedSettingsIsClosed });
            this._advancedButtonClose = Flow.button.create(settings.advancedButtonClose, advancedContainer);
            this._advancedButtonClose.onClicked(this.closeAdvancedSettings);
            this._advancedButtonClose.bindToObservables({ enabled: this._advancedSettingsIsOpen, visible: this._advancedSettingsIsOpen });

            //List for advanced settings
            const inputList = Flow.list.create({
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.TopToBottom,
                expand: Flow.Expand.Disallowed,
                contentAlignment: { x: 0.0, y: 0.5 },
                spacing: Flow.Spacing.vertical(4)
            }, list);

            inputList.bindToObservables({ visible: this._advancedSettingsIsOpen });

            //Radio Button creation
            this._radioOptionsList = [];
            this._winExceedsRadio = UI.radioButton.create(settings.winExceedsRadio);
            this._cashDecreasedRadio = UI.radioButton.create(settings.cashDecreasedRadio);
            this._radioOptionsList.push(this._winExceedsRadio, this._cashDecreasedRadio);

            this._inputList = [];
            for (let i = 0; i < this._radioOptionsList.length; i++)
            {
                const container = Flow.container.create({
                    ...settings.topContainer,
                    dock: Flow.Dock.Fill,
                    expand: Flow.Expand.Disallowed
                }, inputList);

                this._radioOptionsList[i].onClickedOn(() => this.radioOptionClickedOn(i));
                this._radioOptionsList[i].onClickedOff(() => this.radioOptionClickedOff(i));
                container.addChild(this._radioOptionsList[i]);

                const input = Flow.textBox.create(settings.textInput);
                container.addChild(input);
                this._inputList.push(input);
                input.onChanged(() => this.handleInputs(i));
                input.enabled = false;
            }

            const hasSpins = this._spinCount.map((val) => val > 0);
            Disposable.bind(hasSpins, this);
            if (runtimeData.requireLossLimit)
            {
                const hasLossLimit = this._lossLimit.map((val) => val > 0);
                Disposable.bind(hasLossLimit, this);

                const hasBoth = Observable.reduce(
                [
                    hasSpins,
                    this._lossLimit.map((val) => val > 0)
                ], Arbiter.AndResolver);
                Disposable.bind(hasBoth, this);

                this._startButton.bindToObservables({ enabled: hasBoth });
            }
            else
            {
                this._startButton.bindToObservables({ enabled: hasSpins });
            }

            this.onParentChanged(this.updateInputCurrency);
        }

        protected onFlowParentChanged(): void
        {
            if (this.isDisposed) { return; }
            if (this.flowParent != null)
            {
                this.updateInputCurrency();
            }
            super.onFlowParentChanged();
        }

        protected updateInputCurrency()
        {
            if (this.isDisposed) { return; }
            const cf = this.currencyFormatter;
            for (let i = 0; i  < this._inputList.length; i++)
            {
                this._inputList[i].label.text = cf.settings.symbol;
            }
        }

        @Callback
        protected openAdvancedSettings()
        {
            this._advancedSettingsIsOpen.value = true;
            this._advancedSettingsIsClosed.value = false;
            this._advancedButtonLabel.text = Localisation.resolveLocalisableString(this.locale, Translations.Autoplay.Prompt.CloseAdvancedSettings);
        }

        @Callback
        protected closeAdvancedSettings()
        {
            this._advancedSettingsIsOpen.value = false;
            this._advancedSettingsIsClosed.value = true;
            this._advancedButtonLabel.text = Localisation.resolveLocalisableString(this.locale, Translations.Autoplay.Prompt.AdvancedSettings);
        }

        @Callback
        protected radioOptionClickedOn(value: number)
        {
            this._inputList[value].enabled = true;
            if (this._inputList[value].text.length > 0)
            {
                this.handleInputs(value);
            }
        }

        @Callback
        protected radioOptionClickedOff(value: number)
        {
            this._inputList[value].enabled = false;
            if (this._inputList[value].text.length > 0)
            {
                this.clearInput(value);
            }
        }

        @Callback
        protected handleInputs(value: number)
        {
            if (Number(this._inputList[value].text))
            {
                switch (value)
                {
                    case AutoplayDialog.InputOptions.WinLimit:
                        this._winLimit.value = Number(this._inputList[value].text);
                        break;
                    case AutoplayDialog.InputOptions.LossLimit:
                        this._lossLimit.value = Number(this._inputList[value].text);
                        break;
                    default:
                        Log.warn("Value is not a valid");
                }
            }
            else
            {
                Log.warn("Not a number, the inputted value will not be used");
            }
        }

        protected clearInput(value: number)
        {
            switch (value)
            {
                case AutoplayDialog.InputOptions.WinLimit:
                    this._winLimit.value = 0;
                    break;
                case AutoplayDialog.InputOptions.LossLimit:
                    this._lossLimit.value = 0;
                    break;
                default:
                    Log.warn("Value is not a valid");
            }
        }

        @Callback
        protected handleSpinsButtonClicked(value: number)
        {
            if (this._spinsButtonGrid && this._spinsButtonList.length > 0)
            {
                this._spinsButtonList[value].enabled = false;
                this._spinCount.value = this.settings.spinCountOptions[value];
                for (let i = 0; i < this._spinsButtonList.length; i++)
                {
                    if (i != value)
                    {
                        if (!this._spinsButtonList[i].enabled)
                        {
                            this._spinsButtonList[i].enabled = true;
                        }
                    }
                }
            }
        }
    }

    export namespace AutoplayDialog
    {
        export enum InputOptions
        {
            WinLimit = 0,
            LossLimit = 1
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            background: Flow.Background.Settings;
            title: Flow.Label.Settings;
            subtitle: Flow.Label.Settings;
            closeButton: Flow.Button.Settings;
            topContainer: Flow.Container.Settings;

            spinButtonGrid: Flow.Grid.Settings;
            spinButton: Flow.Button.Settings;

            cancelButton: Flow.Button.Settings;
            startButton: Flow.Button.Settings;
            buttonList: Flow.List.Settings;

            advancedButtonOpen: Flow.Button.Settings;
            advancedButtonClose: Flow.Button.Settings;
            advancedButtonTextLabel: Flow.Label.Settings;

            radioGrid: Flow.Grid.Settings;
            winExceedsRadio: UI.RadioButton.Settings;
            cashDecreasedRadio: UI.RadioButton.Settings;

            textInput: Flow.TextBox.Settings;

            spinCountOptions: number[];
        }

        export interface RuntimeData
        {
            totalBetObservable: IReadonlyObservable<number>;
            requireLossLimit?: boolean;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Autoplay,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        export const defaultSettings: Settings =
        {
            sizeToContents: true,
            spacing: Flow.Spacing.all(5),
            background:
            {
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Autoplay,
                frame: Assets.UI.Autoplay.autoplay_background
            },
            title:
            {
                dock: Flow.Dock.Fill,
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 32,
                textColor: Util.Colors.black,
                sizeToContents: true,
                text: Translations.Autoplay.Prompt.Title
            },
            subtitle:
            {
                dock: Flow.Dock.Bottom,
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 20,
                textColor: Util.Colors.black,
                sizeToContents: true,
                text: Translations.Autoplay.Prompt.NoOfSpins
            },
            closeButton:
            {
                name: "Close",
                background: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                dock: Flow.Dock.Right,
                scaleFactor: 0.7,
                seleniumId: "ExitAutoplayButton"
            },
            topContainer:
            {
                dock: Flow.Dock.Top,
                sizeToContents: true,
                expand: Flow.Expand.Allowed,
                spacing: Flow.Spacing.all(4)
            },
            buttonList:
            {
                sizeToContents: true,
                direction: Flow.List.Direction.LeftToRight,
                expand: Flow.Expand.Disallowed,
                spacing: Flow.Spacing.horizontal(25),
                dock: Flow.Dock.Bottom
            },
            spinButtonGrid:
            {
                name: "Spin Button Grid",
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                spacing: Flow.Spacing.all(4),
                expand: Flow.Expand.Disallowed,
                order: Flow.Grid.Order.RowFirst,
                rowCount: 2,
                colCount: 4
            },
            spinButton:
            {
                dock: Flow.Dock.Fill,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    text: "",
                    textColor: Util.Colors.black
                },
                background: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.button_hover },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.button_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                scaleFactor: 0.9
            },
            advancedButtonOpen:
            {
                dock: Flow.Dock.Fill,
                background: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.down_arrow },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.down_arrow },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.down_arrow },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.down_arrow },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null
            },
            advancedButtonClose:
            {
                dock: Flow.Dock.Fill,
                background: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.up_arrow },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.up_arrow },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.up_arrow },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.up_arrow },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null
            },
            advancedButtonTextLabel:
            {
                ...Flow.Button.defaultSettings.textLabel,
                dock: Flow.Dock.Top,
                text: Translations.Autoplay.Prompt.AdvancedSettings,
                textColor: Util.Colors.black,
                font: Fonts.Frutiger.CondensedMedium
            },
            radioGrid:
            {
                name: "Radio Button Grid",
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                spacing: Flow.Spacing.vertical(4),
                expand: Flow.Expand.Disallowed,
                order: Flow.Grid.Order.ColumnFirst,
                rowCount: 2,
                colCount: 2
            },
            winExceedsRadio:
            {
                ...UI.RadioButton.defaultSettings,
                dock: Flow.Dock.Left,
                label:
                {
                    ...UI.RadioButton.defaultSettings.label,
                    text: Translations.Autoplay.Prompt.WinLimitTitle
                }
            },
            cashDecreasedRadio:
            {
                ...UI.RadioButton.defaultSettings,
                dock: Flow.Dock.Left,
                label:
                {
                    ...UI.RadioButton.defaultSettings.label,
                    text: Translations.Autoplay.Prompt.LossLimitTitle
                }
            },
            textInput:
            {
                ...Flow.TextBox.defaultSettings,
                spacing: Flow.Spacing.horizontal(4),
                label:
                {
                    ...Flow.TextBox.defaultSettings.label,
                    dock: Flow.Dock.Left,
                    font: Fonts.Frutiger.CondensedMedium,
                    fontSize: 20,
                    textColor: Util.Colors.black,
                    expand: Flow.Expand.Disallowed,
                    text: ""
                },
                background:
                {
                    kind: Flow.Background.Kind.ImageFrame,
                    dock: Flow.Dock.Fill,
                    asset: Assets.UI.Autoplay,
                    frame: Assets.UI.Autoplay.input_box,
                    sizeToContents: true,
                    expand: Flow.Expand.Disallowed
                },
                input:
                {
                    ...Flow.TextBox.defaultSettings.input,
                    dock: Flow.Dock.Fill,
                    sizeToContents: false,
                    size: { w: 0, h: 0 },
                    expand: Flow.Expand.Allowed,
                    text:
                    {
                        ...Flow.TextBox.defaultSettings.input.text,
                        textColor: Util.Colors.black,
                        fontSize: 20,
                        dock: Flow.Dock.Fill
                    },
                    expression: RegExp('[0-9]+\.?[0-9]*'),
                    caretFlashInterval: 200
                }
            },
            startButton:
            {
                dock: Flow.Dock.Fill,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    text: Translations.Autoplay.Prompt.Start,
                    textColor: Util.Colors.black,
                    font: Fonts.Frutiger.CondensedMedium
                },
                background: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button_hover },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed
            },
            cancelButton:
            {
                dock: Flow.Dock.Fill,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    text: Translations.Autoplay.Prompt.Cancel,
                    textColor: Util.Colors.black,
                    font: Fonts.Frutiger.CondensedMedium
                },
                background: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button_hover },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Autoplay.menu_button_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed
            },
            spinCountOptions: [ 10, 25, 50, 75, 250, 500, 750, 1000 ],

            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            dockAlignment: { x: 0.5, y: 0.5 }
        };
    }

    /**
     * A dialog for selecting autoplay options.
     */
    export const autoplayDialog = Flow.declareElement(AutoplayDialog, true, AutoplayDialog.defaultSettings);
}