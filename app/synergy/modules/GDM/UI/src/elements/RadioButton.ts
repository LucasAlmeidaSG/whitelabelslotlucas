/// <reference path="../generated/Assets.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class RadioButton extends Flow.BaseElement<RadioButton.Settings>
    {
        @AutoDispose protected _onButton: Flow.Button;
        @AutoDispose protected _offButton: Flow.Button;

        @AutoDispose protected _list: Flow.List;
        @AutoDispose protected _label?: Flow.Label;

        protected _isOn: boolean;

        protected _currentButton: Flow.Button;

        public get isOn() { return this._isOn; }
        public set isOn(value: boolean)
        {
            if (this._isOn === value) { return; }
            if (value)
            {
                this._currentButton = this._onButton;
                this._isOn = true;
                this._onButton.visible = true;
                this._onButton.enabled = true;
                this._offButton.visible = false;
                this._offButton.enabled = false;
            }
            else
            {
                this._currentButton = this._offButton;
                this._isOn = false;
                this._onButton.visible = false;
                this._onButton.enabled = false;
                this._offButton.visible = true;
                this._offButton.enabled = true;
            }
        }

        /** Whether or not the user may us this radio button. */
        public get enabled() { return this._currentButton.enabled; }
        public set enabled(value: boolean)
        {
            if (this._currentButton.enabled === value) { return; }
            this._currentButton.enabled = value;
        }

        /** Published when the radio button has been clicked on. */
        public get onClickedOn()
        {
            return this._offButton.onClicked;
        }

        /** Published when the radio button has been clicked off. */
        public get onClickedOff()
        {
            return this._onButton.onClicked;
        }

        public constructor(settings: RadioButton.Settings)
        {
            super(settings, null);

            this._list = Flow.list.create(settings.list, this);
            this._onButton = Flow.button.create(this.settings.buttonOn, this._list);
            this._onButton.onClicked(() => { this.isOn = false; });
            this._offButton = Flow.button.create(this.settings.buttonOff, this._list);
            this._offButton.onClicked(() => { this.isOn = true; });

            this.isOn = false;

            if (settings.label)
            {
                this._label = Flow.label.create(settings.label, this._list);
            }
        }
    }

    export const radioButton = Flow.declareElement(RadioButton);

    export namespace RadioButton
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            buttonOn: Flow.Button.Settings;
            buttonOff: Flow.Button.Settings;
            list: Flow.List.Settings;
            label?: Flow.Label.Settings;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.Image,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.RadioOn,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        export let defaultSettings: Settings =
        {
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            buttonOn:
            {
                name: "Back Button",
                dock: Flow.Dock.Left,
                background: { ...defaultButtonBackground },
                hoverbackground: { ...defaultButtonBackground },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "RadioButton"
            },
            buttonOff:
            {
                name: "Back Button",
                dock: Flow.Dock.Left,
                background: { ...defaultButtonBackground, asset: Assets.UI.RadioOff },
                hoverbackground: { ...defaultButtonBackground, asset: Assets.UI.RadioOff },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, asset: Assets.UI.RadioOff },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "RadioButton"
            },
            list:
            {
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.LeftToRight,
                expand: Flow.Expand.Disallowed,
                spacing: Flow.Spacing.horizontal(15)
            },
            label:
            {
                dock: Flow.Dock.Fill,
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 20,
                textColor: Util.Colors.black,
                sizeToContents: true,
                text: ""
            }
        };
    }
}