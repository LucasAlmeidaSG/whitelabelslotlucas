/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/Translations.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class MeterPanel extends Flow.BaseElement<MeterPanel.Settings>
    {
        protected _balance: number;
        protected _hideBalance: boolean;
        protected _winAmount: number;
        protected _replayMode: boolean = false;

        protected _panel: Flow.Container;
        protected _list: Flow.List;
        protected _winMeter: Flow.Meter;
        protected _balanceMeter: Flow.Meter;
        protected _background: Flow.Background;

        /** Gets or sets the balance to display. */
        public get balance() { return this._balance; }
        public set balance(value)
        {
            if (value === this._balance) { return; }
            this._balance = value;
            this.updateBalanceTextOverride();
            this._balanceMeter.value = value;
        }

        /** Whether or not to show the current balance */
        public get replayMode() { return this._replayMode; }
        public set replayMode(value)
        {
            if (value === this._replayMode) { return; }
            this._replayMode = value;
            this.updateBalanceTextOverride();
        }

        /** Gets or sets the win amount to display. */
        public get winAmount() { return this._winAmount; }
        public set winAmount(value)
        {
            if (value === this._winAmount) { return; }
            this._winAmount = value;
            this._winMeter.textOverride = this.settings.winMeterBlank && value === 0 ? "" : null;
            this._winMeter.value = value;
        }

        public constructor(settings: MeterPanel.Settings)
        {
            super(settings, undefined);

            // Setup meters
            this._panel = Flow.container.create(this.settings.panel, this);
            this._background = Flow.background.create(this.settings.background, this._panel);
            this._list = Flow.list.create(this.settings.list, this._panel);
            this._balanceMeter = Flow.meter.create(this.settings.balanceMeter, this._list);
            this._winMeter = Flow.meter.create(this.settings.winMeter, this._list);
        }

        protected updateBalanceTextOverride(): void
        {
            if (this._replayMode)
            {
                this._balanceMeter.textOverride = this.settings.balanceReplayText;
            }
            else if (this.settings.balanceMeterBlank && this._balance === 0)
            {
                this._balanceMeter.textOverride = "";
            }
            else
            {
                this._balanceMeter.textOverride = null;
            }
        }
    }

    export const meterPanel = Flow.declareElement(MeterPanel);

    export namespace MeterPanel
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            panel: Flow.Container.Settings;
            list: Flow.List.Settings;
            winMeter: Flow.Meter.Settings;
            balanceMeter: Flow.Meter.Settings;
            background: Flow.Background.Settings;
            balanceReplayText: Localisation.LocalisableString;
            /** Whether or not to show blank space instead of 0.00 for the balance meter */
            balanceMeterBlank?: boolean;
            /** Whether or not to show blank space instead of 0.00 for the win meter */
            winMeterBlank?: boolean;
        }

        const defaultMeterSettings: Flow.Meter.Settings =
        {
            dock: Flow.Dock.Left,
            nameLabel:
            {
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 22,
                text: "",
                sizeToContents: true,
                dock: Flow.Dock.Bottom,
                align: Rendering.TextOptions.Align.Middle,
                textColor: Util.Colors.black,
                expand: Flow.Expand.Allowed
            },
            valueLabel:
            {
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 22,
                text: "",
                sizeToContents: true,
                dock: Flow.Dock.Top,
                align: Rendering.TextOptions.Align.Middle,
                textColor: Util.Colors.black,
                expand: Flow.Expand.Allowed
            },
            spacing: { left: 5, right: 5, top: 20, bottom: 10 },
            isCurrency: true,
            background: null
        };

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Meter Panel",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: 0, y: 10 },
            sizeToContents: true,
            panel:
            {
                name: "Inner Panel",
                dock: Flow.Dock.Fill,
                sizeToContents: false,
                expand: Flow.Expand.Allowed,
                size: { w: 400, h: 90 }
            },
            list:
            {
                name: "Meter List",
                direction: Flow.List.Direction.LeftToRight,
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                expand: Flow.Expand.Allowed,
                spacing: Flow.Spacing.horizontal(10),
                spreadItems: true,
                evenlySpaceItems: true
            },
            background:
            {
                name: "Meter Background",
                kind: Flow.Background.Kind.ImageFrame,
                dock: Flow.Dock.Fill,
                asset: Assets.UI.Elements,
                frame: Assets.UI.Elements.meter_panel_background,
                expand: Flow.Expand.Allowed
            },
            balanceMeter:
            {
                ...defaultMeterSettings,
                name: "Balance Value",
                nameLabel:
                {
                    ...defaultMeterSettings.nameLabel,
                    text:  Translations.UI.Balance
                }
            },
            balanceReplayText: Translations.UI.Replay,
            winMeter:
            {
                ...defaultMeterSettings,
                name: "Win Value",
                nameLabel:
                {
                    ...defaultMeterSettings.nameLabel,
                    text:  Translations.UI.Win
                },
                dock: Flow.Dock.Right
            },
            winMeterBlank: true
        };

        export let defaultPortraitSettings: Settings =
        {
            ...defaultSettings,
            floatOffset: { x: 0, y: 975 }
        };

        const defaultMeterSettingsNew: Flow.Meter.Settings =
        {
            ...defaultMeterSettings,
            dock: Flow.Dock.Left,
            nameLabel:
            {
                ...defaultMeterSettings.nameLabel,
                font: Fonts.Frutiger.Bold,
                fontSize: 30,
                align: Rendering.TextOptions.Align.Middle
            },
            valueLabel:
            {
                ...defaultMeterSettings.valueLabel,
                font: Fonts.Frutiger.Bold,
                fontSize: 30,
                align: Rendering.TextOptions.Align.Middle
            },
            spacing: { left: 5, right: 5, top: 25, bottom: 10 }
        };

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            floatOffset: { x: 0, y: 15 },
            scaleFactor: 0.9,
            panel:
            {
                ...defaultSettings.panel,
                size: { w: 674, h: 121 }
            },
            background:
            {
                ...defaultSettings.background,
                sizeToContents: true
            },
            balanceMeter:
            {
                ...defaultMeterSettingsNew,
                name: "Balance Value",
                nameLabel:
                {
                    ...defaultMeterSettingsNew.nameLabel,
                    text:  Translations.UI.Balance
                },
                spacing: { left: 50, right: 5, top: 25, bottom: 15 }
            },
            winMeter:
            {
                ...defaultMeterSettingsNew,
                name: "Win Value",
                nameLabel:
                {
                    ...defaultMeterSettings.nameLabel,
                    text:  Translations.UI.Win
                },
                spacing: { left: 5, right: 50, top: 25, bottom: 15 },
                dock: Flow.Dock.Right
            }
        }

        export const defaultPortraitSettingsNew: Settings =
        {
            ...defaultSettingsNew,
            scaleFactor: 1.0,
            floatOffset: { x: 0, y: 720 }
        }
    }
}