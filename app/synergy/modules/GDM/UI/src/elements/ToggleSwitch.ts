/// <reference path="../generated/Assets.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class ToggleSwitch extends Flow.BaseElement<ToggleSwitch.Settings, ToggleSwitch.RuntimeData>
    {
        //The toggle background for when the toggle is off - contains toggleBackgroundOn button
        @AutoDispose protected _toggleBackgroundOff: Flow.Background;
        //The toggle background for when the toggle is on - contains toggleBackgroundOff button
        @AutoDispose protected _toggleBackgroundOn: Flow.Background;
        //Clicking this will turn the toggle off
        @AutoDispose protected _toggleButtonIsOn: Flow.Button;
        //Clicking this will turn the toggle on
        @AutoDispose protected _toggleButtonIsOff: Flow.Button;

        public get toggledOn() { return this._toggleBackgroundOff.onClicked; }
        public get toggledOff() { return this._toggleBackgroundOn.onClicked; }

        public constructor(settings: ToggleSwitch.Settings, runtimeData: ToggleSwitch.RuntimeData)
        {
            super(settings, runtimeData);

            this._toggleBackgroundOn = Flow.background.create(this.settings.toggleBackgroundOn, this);
            this._toggleBackgroundOff = Flow.background.create(this.settings.toggleBackgroundOff, this);
            this._toggleButtonIsOff = Flow.button.create(this.settings.toggleButtonIsOff, this._toggleBackgroundOff);
            this._toggleButtonIsOn = Flow.button.create(this.settings.toggleButtonIsOn, this._toggleBackgroundOn);

            if (runtimeData.enableInteractionArbiter)
            {
                this._toggleButtonIsOff.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._toggleButtonIsOn.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
            }

            this.toggle(false);
        }

        public toggle(value: boolean)
        {
            if (value)
            {

                this._toggleButtonIsOff.enabled = false;
                this._toggleBackgroundOff.visible = false;
                this._toggleButtonIsOn.enabled = true;
                this._toggleBackgroundOn.visible = true;
            }
            else
            {
                this._toggleButtonIsOff.enabled = true;
                this._toggleBackgroundOff.visible = true;
                this._toggleButtonIsOn.enabled = false;
                this._toggleBackgroundOn.visible = false;
            }
        }
    }

    export const toggleSwitch = Flow.declareElement(ToggleSwitch, true);

    export namespace ToggleSwitch
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            toggleButtonIsOff: Flow.Button.Settings;
            toggleButtonIsOn: Flow.Button.Settings;
            toggleBackgroundOn: Flow.Background.Settings;
            toggleBackgroundOff: Flow.Background.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Allowed
        };

        const defaultToggleButton: Flow.Button.Settings =
        {
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            textLabel: null,
            spacing: Flow.Spacing.none,
            background: { ...defaultButtonBackground, asset: Assets.UI.Toggle, frame: Assets.UI.Toggle.toggle_button },
            hoverbackground: { ...defaultButtonBackground, asset: Assets.UI.Toggle, frame: Assets.UI.Toggle.toggle_button_hover },
            pressbackground: null,
            disabledbackground: { ...defaultButtonBackground, asset: Assets.UI.Toggle, frame: Assets.UI.Toggle.toggle_button }
        };

        export let defaultSettings: Settings =
        {
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            toggleButtonIsOff:
            {
                ...defaultToggleButton,
                name: "Toggle On Button",
                dock: Flow.Dock.Left,
                seleniumId: "ToggleOnButton"
            },
            toggleButtonIsOn:
            {
                ...defaultToggleButton,
                name: "Toggle Off Button",
                dock: Flow.Dock.Right,
                seleniumId: "ToggleOffButton"
            },
            toggleBackgroundOn:
            {
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Toggle,
                frame: Assets.UI.Toggle.toggle_background_on,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Fill,
                spacing: { left: 0, right: 5, top: 0, bottom: 0 }
            },
            toggleBackgroundOff:
            {
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Toggle,
                frame: Assets.UI.Toggle.toggle_background_off,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Fill,
                spacing: { left: 5, right: 0, top: 0, bottom: 0 }
            }
        };
    }
}