/// <reference path="../generated/Assets.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class SpinButton extends Flow.BaseElement<SpinButton.Settings, SpinButton.RuntimeData>
    {
        @AutoDispose protected _spinButton: Flow.Button;
        @AutoDispose protected _stopButton: Flow.Button;

        /** Published when the spin button has been clicked. */
        public get onSpinClicked() { return this._spinButton.onClicked; }

        /** Published when the stop button has been clicked. */
        public get onStopClicked() { return this._stopButton.onClicked; }

        public constructor(settings: SpinButton.Settings, runtimeData: SpinButton.RuntimeData)
        {
            super(settings, runtimeData);

            this._spinButton = Flow.button.create(this.settings.spinButton, this);
            this._stopButton = Flow.button.create(this.settings.stopButton, this);

            const canSpin = Observable.reduce([runtimeData.canSpinArbiter, runtimeData.enableInteractionArbiter], Arbiter.AndResolver);
            Disposable.bind(canSpin, this);
            const canSkipAndAllowQuickSpin = Observable.reduce([runtimeData.canSkipArbiter, runtimeData.canQuickSpinArbiter], Arbiter.AndResolver);
            const canSkipAndAllowQuickSpinOrCanSpin = Observable.reduce([canSpin, canSkipAndAllowQuickSpin], Arbiter.OrResolver);
            Disposable.bind(canSkipAndAllowQuickSpin, this);
            Disposable.bind(canSkipAndAllowQuickSpinOrCanSpin, this);

            this._spinButton.bindToObservables({ enabled: canSkipAndAllowQuickSpinOrCanSpin });
            this._stopButton.bindToObservables({ enabled: runtimeData.canSkipArbiter });

            const spinOrSkipHandler = runtimeData.spinOrSkipArbiter.onChanged(this.handleSpinOrSkipChanged);
            const quickSpinHandler = runtimeData.canQuickSpinArbiter.onChanged(this.handleSpinOrSkipChanged);
            Disposable.bind(spinOrSkipHandler, this);
            Disposable.bind(quickSpinHandler, this);

            this.handleSpinOrSkipChanged();
        }

        @Callback
        protected handleSpinOrSkipChanged(): void
        {
            if (this.runtimeData.spinOrSkipArbiter.value === "spin" || this.runtimeData.canQuickSpinArbiter.value)
            {
                this._spinButton.visible = true;
                this._stopButton.visible = false;
            }
            else
            {
                this._spinButton.visible = false;
                this._stopButton.visible = true;
            }
        }
    }

    export const spinButton = Flow.declareElement(SpinButton, true);

    export namespace SpinButton
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            spinButton: Flow.Button.Settings;
            stopButton: Flow.Button.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
            spinOrSkipArbiter: IArbiter<"spin"|"skip">;
            canSpinArbiter: IArbiter<boolean>;
            canSkipArbiter: IArbiter<boolean>;
            canQuickSpinArbiter: IArbiter<boolean>;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Spin/Stop Button",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: 585, y: 235 },
            spinButton:
            {
                name: "Spin Button",
                dock: Flow.Dock.Fill,
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.play_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.play_button_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.play_button_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "SpinButton"
            },
            stopButton:
            {
                name: "Stop Button",
                dock: Flow.Dock.Fill,
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_stop_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "StopSpinButton"
            },
            sizeToContents: true
        };

        export let defaultLeftSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: defaultSettings.floatOffset.y }
        };
        export let defaultRightSettings: Partial<Settings> =
        {
            floatOffset: defaultSettings.floatOffset
        };
        export let defaultLeftPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: 1160 }
        };
        export let defaultRightPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: defaultSettings.floatOffset.x, y: 1160 }
        };

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            floatPosition: { x: 1.0, y: 0.5 },
            dockAlignment: { x: 1.0, y: 0.5 },
            floatOffset: { x: -45, y: 220 },
            scaleFactor: 0.8
        };

        export let defaultLeftSettingsNew: Partial<Settings> =
        {
            floatPosition: { x: 0.0, y: 0.5 },
            dockAlignment: { x: 0.0, y: 0.5 },
            floatOffset: { x: 45, y: 220 },
        };

        export let defaultPortraitSettingsNew: Settings =
        {
            ...defaultSettingsNew,
            scaleFactor: 1.0,
            dockAlignment: { x: 0.5, y: 1.0 },
            floatPosition: { x: 0.5, y: 1.0 },
            floatOffset: {x: 0, y: -100}
        };
    }
}