namespace RS.GDM.UI
{
    export namespace UISettings
    {
        export type OrientationSettings<T> =
        {
            landscape: T;
            portrait: T;
        };

        export interface Settings
        {
            desktop: OrientationSettings<LayoutSettings>;
            mobile: OrientationSettings<LayoutSettings>;
        }

        export interface LayoutSettings
        {
            meterPanel: UIElement<MeterPanel.Settings>;
            messageBar: UIElement<MessageBar.Settings>;
            backButton: UIElement<BackButton.Settings>;
            clock: UIElement<Clock.Settings>;
            spinButton: UIElement<SpinButton.Settings>;
            autoplayButton: UIElement<AutoplayButton.Settings>;
            stakeButton: UIElement<StakeButton.Settings>;
            settingsPanel: UIElement<SettingsPanel.Settings>;
            turboToggle: UIElement<TurboToggle.Settings>;
        }

        /** @deprecated use defaultLandscapeNew */
        export const defaultLandscape: LayoutSettings =
        {
            meterPanel:
            {
                ...MeterPanel.defaultSettings
            },
            messageBar:
            {
                ...MessageBar.defaultSettings
            },
            backButton:
            {
                ...BackButton.defaultSettings
            },
            clock:
            {
                ...Clock.defaultSettings
            },
            spinButton:
            {
                ...SpinButton.defaultSettings,
                left: SpinButton.defaultLeftSettings,
                right: SpinButton.defaultRightSettings
            },
            autoplayButton:
            {
                ...AutoplayButton.defaultSettings,
                left: AutoplayButton.defaultLeftSettings,
                right: AutoplayButton.defaultRightSettings
            },
            stakeButton:
            {
                ...StakeButton.defaultSettings,
                left: StakeButton.defaultLeftSettings,
                right: StakeButton.defaultRightSettings
            },
            settingsPanel:
            {
                ...SettingsPanel.defaultSettings,
                left: SettingsPanel.defaultLeftSettings,
                right: SettingsPanel.defaultRightSettings
            },
            turboToggle:
            {
                ...TurboToggle.defaultSettings,
                left:
                {
                    floatOffset: { x: -TurboToggle.defaultSettings.floatOffset.x, y: TurboToggle.defaultSettings.floatOffset.y }
                },
                right:
                {
                    floatOffset: TurboToggle.defaultSettings.floatOffset
                }
            }
        };

        /** @deprecated use defaultPortraitNew */
        export const defaultPortrait: LayoutSettings =
        {
            meterPanel:
            {
                ...MeterPanel.defaultPortraitSettings
            },
            messageBar:
            {
                ...MessageBar.defaultPortraitSettings
            },
            backButton:
            {
                ...BackButton.defaultPortraitSettings
            },
            clock:
            {
                ...Clock.defaultPortraitSettings
            },
            spinButton:
            {
                ...SpinButton.defaultSettings,
                left: SpinButton.defaultLeftPortraitSettings,
                right: SpinButton.defaultRightPortraitSettings
            },
            autoplayButton:
            {
                ...AutoplayButton.defaultSettings,
                left: AutoplayButton.defaultLeftPortraitSettings,
                right: AutoplayButton.defaultRightPortraitSettings
            },
            stakeButton:
            {
                ...StakeButton.defaultSettings,
                left: StakeButton.defaultLeftPortraitSettings,
                right: StakeButton.defaultRightPortraitSettings
            },
            settingsPanel:
            {
                ...SettingsPanel.defaultSettings,
                left: SettingsPanel.defaultLeftPortraitSettings,
                right: SettingsPanel.defaultRightPortraitSettings
            },
            turboToggle:
            {
                ...TurboToggle.defaultSettings,
                left:
                {
                    floatOffset: { x: -TurboToggle.defaultSettings.floatOffset.x, y: 1680 }
                },
                right:
                {
                    floatOffset: { x: TurboToggle.defaultSettings.floatOffset.x, y: 1680 }
                }
            }
        };

        /** @deprecated use settingsNew */
        export let settings: Settings =
        {
            desktop:
            {
                landscape: defaultLandscape,
                portrait: defaultPortrait
            },
            mobile:
            {
                landscape: defaultLandscape,
                portrait: defaultPortrait
            }
        };
    }
}