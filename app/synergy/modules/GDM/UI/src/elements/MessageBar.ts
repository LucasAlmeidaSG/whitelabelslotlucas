/// <reference path="../generated/Assets.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class MessageBar extends Flow.BaseElement<MessageBar.Settings>
    {
        protected _isDisplayed: boolean;
        protected _messageText: Localisation.LocalisableString;

        @AutoDisposeOnSet protected _background: Flow.Background;
        @AutoDisposeOnSet protected _messageLabel: Flow.Label;
        @AutoDisposeOnSet protected _messageMask: Rendering.IGraphics;

        @AutoDisposeOnSet protected _scrollTween: Tween<Flow.Label>;
        @AutoDisposeOnSet protected _visibilityTween: Tween<MessageBar>;

        /** Gets or sets the message to display. */
        public get messageText() { return this._messageText; }
        public set messageText(value)
        {
            if(value === null || value === this._messageText) { return; }
            this._messageText = value;
            this._messageLabel.text = value;
            this.scrollOrStatic();
        }

        /** Gets or sets the visibility of this element. */
        public get isDisplayed() { return this._isDisplayed; }
        public set isDisplayed(value)
        {
            if (value === this._isDisplayed) { return; }

            this._isDisplayed = value;
            this.changeVisibility();
        }

        public constructor(settings: MessageBar.Settings)
        {
            // Create parent element with fixed size
            super({ ...settings, size: settings.messageBarSize }, undefined);

            // Setup message bar with fixes size
            this._background = Flow.background.create({ ...this.settings.background, size: settings.messageBarSize }, this);
            this._messageLabel = Flow.label.create(this.settings.messageLabel, this);
            this.createMask();

            // Set visibility according to arbiter instantly
            this.changeVisibility(true);
        }

        protected createMask()
        {
            // Calculate mask sizing
            const padding = this.settings.maskPadding;
            const rect = new RS.Rendering.Rectangle(padding.left, padding.top, +this.settings.messageBarSize.w - padding.left - padding.right, +this.settings.messageBarSize.h - padding.top - padding.bottom);

            // Create and attach
            this._messageMask = new Rendering.Graphics().beginFill(Util.Colors.black).drawRect(rect).endFill();
            this.addChild(this._messageMask);
            this._messageLabel.mask = this._messageMask;
        }

        /** Decides how to display the message based of length */
        @Callback
        protected scrollOrStatic()
        {
            this.invalidateLayout();
            if (this._messageLabel.width > this._background.width)
            {
                this.scrollLabel();
            }
            else
            {
                this.centerLabel();
            }
            this.invalidateLayout();
        }

        /** Scrolls the label across the message bar */
        @Callback
        protected scrollLabel()
        {
            this._messageLabel.apply(MessageBar.Right);
            this._scrollTween = Tween.get(this._messageLabel, { loop: true }).wait(2000).to(MessageBar.Left, this.settings.messageScrollSpeed).wait(4000).to(MessageBar.Right, this.settings.messageScrollSpeed);
        }

        /** Centers the label in the message bar */
        @Callback
        protected centerLabel()
        {
            this._scrollTween = null;
            this._messageLabel.apply(MessageBar.Centre);
        }

        @Callback
        protected async changeVisibility(instant: boolean = false)
        {
            // Get desired settings
            const settings = this.isDisplayed ? this.settings.animationInProperties : this.settings.animationOutProperties;

            // Either just toggle visibility or animate
            if (instant || settings == null) { this.visible = this.isDisplayed; }

            if (this.isDisplayed)
            {
                // if animate in, set visible before animation so it doesn't just appear
                this.visible = this.isDisplayed;
            }

            if (settings != null)
            {
                if (instant)
                {
                    this.apply(settings.properties as Partial<RS.Flow.ElementProperties>);
                    if (settings.properties.postTransform)
                    {
                        if (settings.properties.postTransform.scaleX) { this.postTransform.scaleX = settings.properties.postTransform.scaleX; }
                        if (settings.properties.postTransform.scaleY) { this.postTransform.scaleY = settings.properties.postTransform.scaleY; }
                        if (settings.properties.postTransform.x) { this.postTransform.x = settings.properties.postTransform.x; }
                        if (settings.properties.postTransform.y) { this.postTransform.y = settings.properties.postTransform.y; }
                    }
                }
                else
                {
                    this._visibilityTween = Tween.get(this as MessageBar).to(settings.properties, settings.time, settings.ease);
                    try
                    {
                        await this._visibilityTween;
                    }
                    catch (e)
                    {
                        if (!(e instanceof Tween.DisposedRejection)) { throw e; }
                    }
                }
            }

            // if animate out, set visible after animation so it doesn't just disappear if theres an animtion
            if (!this.isDisplayed)
            {
                this.visible = this.isDisplayed;
            }
        }
    }

    export const messageBar = Flow.declareElement(MessageBar);

    export namespace MessageBar
    {
        export const Left: Partial<Flow.ElementProperties> = { dockAlignment: { x: 1.0, y: 0.5 }, floatPosition: { x: 1.0, y: 0.5 }, floatOffset: { x: -10, y: 0 } };
        export const Right: Partial<Flow.ElementProperties> = { dockAlignment: { x: 0.0, y: 0.5 }, floatPosition: { x: 0.0, y: 0.5 }, floatOffset: { x: 10, y: 0 } };
        export const Centre: Partial<Flow.ElementProperties> = { dockAlignment: { x: 0.5, y: 0.5 }, floatPosition: { x: 0.5, y: 0.5 }, floatOffset: { x: 0, y: 0 } };

        export interface AnimationProperties
        {
            properties: Partial<Flow.ElementProperties> & { postTransform?: Flow.Transform };
            time: number;
            ease?: RS.EaseFunction;
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            background: Flow.Background.Settings;
            messageLabel: Flow.Label.Settings;

            /** Must be set for the element to work correctly, a fixed size is how the mask and scrolling of longer strings is calculated */
            messageBarSize: RS.Flow.Size;

            /** How long (ms) it takes for the text to scroll from left to right on strings longer than the bar */
            messageScrollSpeed: number;

            /** How much padding around the mask there is, useful for background images with thick borders */
            maskPadding: RS.Flow.Spacing;

            /** Settings to define how the bar will tween when animating in and out (will only animate if defined) */
            animationInProperties?: AnimationProperties;
            animationOutProperties?: AnimationProperties;
        }

        export const defaultSettings: Settings =
        {
            name: "Message Bar",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: 0, y: 160 },
            sizeToContents: false,
            background:
            {
                name: "Background",
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Elements,
                frame: Assets.UI.Elements.message_bar_background,
                sizeToContents: false,
                expand: Flow.Expand.Disallowed
            },
            messageLabel:
            {
                name: "Message Label",
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 },
                dockAlignment: { x: 0.5, y: 0.5 },
                sizeToContents: true,
                font: Fonts.Frutiger.CondensedMedium,
                textColor: Util.Colors.black,
                fontSize: 25,
                text: "",
            },

            maskPadding: { left: 10, right: 10, top: 0, bottom: 0 },
            messageBarSize: { w: 968, h: 39 },
            messageScrollSpeed: 4000,

            animationInProperties:
            {
                properties: { postTransform: { x: 0, y: 0, scaleX: 1, scaleY: 1 } },
                time: 500,
                ease: RS.Ease.quadOut
            },
            animationOutProperties:
            {
                properties: { postTransform: { x: 0, y: -150, scaleX: 0, scaleY: 0, } },
                time: 500,
                ease: RS.Ease.quadIn
            }
        }

        export const defaultPortraitSettings: Settings =
        {
            ...defaultSettings,
            floatOffset: { x: 0, y: 980 }
        }
    }
}