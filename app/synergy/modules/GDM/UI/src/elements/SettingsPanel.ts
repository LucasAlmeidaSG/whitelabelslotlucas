/// <reference path="../generated/Assets.ts" />
/// <reference path="LeftHandToggle.ts"/>

namespace RS.GDM.UI
{
    @HasCallbacks
    export class SettingsPanel extends Flow.BaseElement<SettingsPanel.Settings, SettingsPanel.RuntimeData>
    {
        @AutoDispose protected _settingsButtonsList: Flow.List;
        @AutoDispose protected _mainButtonList: Flow.List;
        @AutoDispose protected _settingsButton: Flow.Button;
        @AutoDispose protected _exitButton: Flow.Button;
        @AutoDispose protected _soundIsOffButton: Flow.Button;
        @AutoDispose protected _soundIsOnButton: Flow.Button;
        @AutoDispose protected _leftHandToggle: UI.LeftHandToggle;
        @AutoDispose protected _helpButton: Flow.Button;

        @AutoDisposeOnSet protected _muteHandle: IDisposable;

        protected _hideMute: boolean = false;

        protected _isOpen: boolean = false;
        public get isOpen() { return this._isOpen; }

        /** Published when the settings button has been clicked. */
        public get onSettingsClicked() { return this._settingsButton.onClicked; }
        /** Published when the exit button has been clicked. */
        public get onExitClicked() { return this._exitButton.onClicked; }
        /** Published when the help buttonhas been clicked. */
        public get onHelpClicked() { return this._helpButton.onClicked; }

        public get onLeftHandButtonClicked() { return this._leftHandToggle.onButtonLeftClicked; }
        public get onRightHandButtonClicked() { return this._leftHandToggle.onButtonRightClicked; }

        public constructor(settings: SettingsPanel.Settings, runtimeData: SettingsPanel.RuntimeData)
        {
            super(settings, runtimeData);

            this._mainButtonList = Flow.list.create(this.settings.mainButtonList, this);
            this._settingsButtonsList = Flow.list.create(this.settings.settingsButtonsList, this);

            this._settingsButton = Flow.button.create(this.settings.settingsButton);
            this._exitButton = Flow.button.create(this.settings.exitButton);

            this._soundIsOffButton = Flow.button.create(this.settings.soundIsOffButton);
            this._soundIsOffButton.onClicked(this.handleMuteClicked.bind(this, false));
            this._soundIsOnButton = Flow.button.create(this.settings.soundIsOnButton);
            this._soundIsOnButton.onClicked(this.handleMuteClicked.bind(this, true));

            this._leftHandToggle = UI.leftHandToggle.create(this.settings.leftHandToggle,
            {
                enableInteractionArbiter: this.runtimeData.enableInteractionArbiter
            });

            this._helpButton = Flow.button.create(this.settings.helpButton);

            if (runtimeData.enableInteractionArbiter)
            {
                this._settingsButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._exitButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._soundIsOffButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._soundIsOnButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });

                const enabledArbiter = Observable.reduce([ runtimeData.enableInteractionArbiter, runtimeData.paytableEnabledArbiter ], Arbiter.AndResolver);
                this._helpButton.bindToObservables({ enabled: enabledArbiter });
            }

            this.setupList();

            this.checkPartnerMute();

            //set init state and bind to arbiter to allow external control
            this.toggleSound(this.runtimeData.muteArbiter.value);
            const onMuteChanged = this.runtimeData.muteArbiter.onChanged(this.toggleSound);
            Disposable.bind(onMuteChanged, this);
        }

        public toggleSettings(expandList: boolean)
        {
            this._isOpen = expandList;
            //If list is closed, open the required elements
            if (expandList)
            {
                if (this.runtimeData.enableInteractionArbiter.value === true)
                {
                    this._settingsButton.enabled = false;
                    this._exitButton.enabled = true;
                }
                this._settingsButton.visible = false;
                this._exitButton.visible = true;

                this.toggleSound(this.runtimeData.muteArbiter.value);

                if (this.settings.showLeftHandedUI)
                {
                    this._leftHandToggle.visible = true;
                }
                this._helpButton.visible = true;
            }
            //If the list is open, close the list
            else
            {
                if (this.runtimeData.enableInteractionArbiter.value === true)
                {
                    this._settingsButton.enabled = true;
                    this._exitButton.enabled = false;
                }
                this._settingsButton.visible = true;
                this._exitButton.visible = false;

                this.toggleSound(this.runtimeData.muteArbiter.value);

                if (this.settings.showLeftHandedUI)
                {
                    this._leftHandToggle.visible = false;
                }
                this._helpButton.visible = false;
            }
        }

        @Callback
        public toggleSound(muted: boolean)
        {
            if (this._hideMute)
            {
                this._soundIsOnButton.visible = false;
                this._soundIsOffButton.visible = false;
                return;
            }

            this._soundIsOnButton.enabled = !muted;
            this._soundIsOffButton.enabled = muted;

            if (this._isOpen)
            {
                this._soundIsOnButton.visible = !muted;
                this._soundIsOffButton.visible = muted;
            }
            else
            {
                this._soundIsOnButton.visible = false;
                this._soundIsOffButton.visible = false;
            }
        }

        public toggleLeftHandSwitch(value: boolean)
        {
            this._leftHandToggle.toggle(value);
        }

        // Apply left hand mode settings to the settingsButtonList
        public applyLeftHandMode(value: boolean)
        {
            if (value && this.settings.settingsButtonsListLeft)
            {
                this._settingsButtonsList.apply(this.settings.settingsButtonsListLeft);
            }
            else if (!value && this.settings.settingsButtonsListRight)
            {
                this._settingsButtonsList.apply(this.settings.settingsButtonsListRight);
            }
        }

        protected setupList(): void
        {
            this._mainButtonList.addChild(this._settingsButton);
            this._mainButtonList.addChild(this._exitButton);
            this._settingsButtonsList.addChild(this._soundIsOnButton);
            this._settingsButtonsList.addChild(this._soundIsOffButton);

            if (this.settings.showLeftHandedUI)
            {
                this._settingsButtonsList.addChild(this._leftHandToggle);
            }

            this._settingsButtonsList.addChild(this._helpButton);

            this._exitButton.visible = false;
            this._soundIsOffButton.visible = false;
            this._soundIsOnButton.visible = false;

            if (this.settings.showLeftHandedUI)
            {
                this._leftHandToggle.visible = false;
            }
            this._helpButton.visible = false;
        }

        /** Check if partner supports the platform mute setting. If not we shouldn't show the mute button in game */
        protected checkPartnerMute()
        {
            // As far as I can tell the PokerStars PartnerAdapter just checks isDesktop and hides the platform mute setting if true
            // Unfortunately this check is done every time the settings menu is opened so we can't just check if the element is visible if we don't use the partner settings menu.
            // So we're stuck with checking partnercode + isDesktop for now
            const isPKS = IPlatform.get().partnerCode === "nyx_pokerstars" && Device.isDesktopDevice;
            this._hideMute = isPKS;
        }

        @Callback
        protected handleMuteClicked(muted: boolean)
        {
            // Need to broadcast this to partners to keep their mute button in sync with the one in game.
            // Can't find an API call for this, each partner just has their own menu implementation.
            // And so we click the mute button in a hidden menu somewhere...
            // Tried with sky, openbet and gamesys so the ID is at least consistent between these 3, hopefully other partners are the same
            // ¯\_(ツ)_/¯
            // This triggers both the internal menu listener and the partner listener,
            // so both internal and external buttons get updated together
            const checkbox = document.getElementById("menu-checkbox-sound");
            if (checkbox)
            {
                if (Device.isMobileDevice)
                {
                    // have to do it like this for 1.0 - window.Event doesn't exist yet in ts2.9.2
                    const Event = "Event";
                    checkbox.dispatchEvent(new window[Event]("touchend"));
                }
                else
                {
                    //same as dispatching Event("click")
                    checkbox.click();
                }
            }
            else
            {
                // Log in case the element isn't found so we know where to investigate
                Log.debug("element with id 'menu-checkbox-sound' not found, can't toggle partner mute button");
            }

            // Force platform to declare mute in case the callback doesn't get triggered:
            // - The checkbox doesn't exist (local build)
            // - Mobile, where the element exists but doesn't seem to do anything
            // We have to use platform instead of declaring our own handle to keep both in sync
            IPlatform.get().internalMute(muted);
        }
    }

    export const settingsPanel = Flow.declareElement(SettingsPanel, true);

    export namespace SettingsPanel
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            settingsButtonsList: Flow.List.Settings;
            settingsButtonsListLeft?: Flow.List.Settings;
            settingsButtonsListRight?: Flow.List.Settings;
            mainButtonList: Flow.List.Settings;
            settingsButton: Flow.Button.Settings;
            exitButton: Flow.Button.Settings;
            soundIsOffButton: Flow.Button.Settings;
            soundIsOnButton: Flow.Button.Settings;
            leftHandToggle: UI.LeftHandToggle.Settings;
            helpButton: Flow.Button.Settings;
            showLeftHandedUI: boolean;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
            paytableEnabledArbiter: IArbiter<boolean>;
            muteArbiter: IArbiter<boolean>;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        const defaultButtonSettings: Flow.Button.Settings =
        {
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            textLabel: null,
            spacing: Flow.Spacing.none
        };

        const defaultListSettings: Flow.List.Settings =
        {
            name: "Main Button List",
            direction: Flow.List.Direction.BottomToTop,
            spacing: Flow.Spacing.vertical(10),
            dock: Flow.Dock.Fill,
            sizeToContents: true,
            expand: Flow.Expand.Allowed,
            spreadItems: true,
            evenlySpaceItems: true
        };

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Settings Panel",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 1.0 },
            floatOffset: { x: -585, y: 735 },
            sizeToContents: true,
            showLeftHandedUI: true,
            mainButtonList:
            {
                ...defaultListSettings,
                name: "Button List",
                dock: Flow.Dock.Bottom,
                direction: Flow.List.Direction.BottomToTop
            },
            settingsButtonsList:
            {
                ...defaultListSettings,
                name: "Button List",
                dock: Flow.Dock.Top,
                direction: Flow.List.Direction.BottomToTop
            },
            settingsButton:
            {
                ...defaultButtonSettings,
                dock: Flow.Dock.Bottom,
                name: "Settings Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_button_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_button_disabled },
                seleniumId: "SettingsButton"
            },
            exitButton:
            {
                ...defaultButtonSettings,
                dock: Flow.Dock.Bottom,
                name: "Exit Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close_disabled },
                seleniumId: "ExitButton"
            },
            soundIsOffButton:
            {
                ...defaultButtonSettings,
                name: "Sound Off Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_off },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_off_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_up_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_off_disabled },
                seleniumId: "SoundOffButton"
            },
            soundIsOnButton:
            {
                ...defaultButtonSettings,
                name: "Sound On Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_up },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_up_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_up_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.button_volume_up_disabled },
                seleniumId: "SoundOnButton"
            },
            leftHandToggle: UI.LeftHandToggle.defaultSettings,
            helpButton:
            {
                ...defaultButtonSettings,
                name: "Help Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.help_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.help_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.help_button_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.help_button_disabled },
                seleniumId: "HelpButton"
            }
        };

        export let defaultLeftSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: defaultSettings.floatOffset.y }
        };
        export let defaultRightSettings: Partial<Settings> =
        {
            floatOffset: defaultSettings.floatOffset
        };
        export let defaultLeftPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: 1650 }
        };
        export let defaultRightPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: defaultSettings.floatOffset.x, y: 1650 }
        };

        const defaultListSettingsLandscapeNew: Flow.List.Settings =
        {
            ...defaultListSettings,
            name: "Settings Button List",
            dock: Flow.Dock.Float,
            direction: Flow.List.Direction.LeftToRight,
            spacing: Flow.Spacing.horizontal(-5),
        };

        const defaultListSettingsPortraitNew: Flow.List.Settings =
        {
            ...defaultListSettings,
            name: "Settings Button List",
            dock: Flow.Dock.Float,
            dockAlignment: { x: 0.5, y: 0.0 },
            floatPosition: { x: 0.5, y: 0.0 },
            floatOffset: { x: 0, y: 270 }
        };

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            floatPosition: { x: 1.0, y: 0.5 },
            dockAlignment: { x: 1.0, y: 0.5 },
            floatOffset: { x: -113, y: -300 },
            scaleFactor: 0.725,
            mainButtonList:
            {
                ...defaultListSettings,
                dock: Flow.Dock.Right
            },
            settingsButtonsList:
            {
                ...defaultListSettingsLandscapeNew,
                dockAlignment: { x: 1.0, y: 0.5 },
                floatPosition: { x: 1.0, y: 0.5 },
                floatOffset: { x: -150, y: 0 }
            }
        };

        export let defaultLeftSettingsNew: Partial<Settings> =
        {
            floatPosition: { x: 0.0, y: 0.5 },
            dockAlignment: { x: 0.0, y: 0.5 },
            floatOffset: { x: 113, y: -300 },
            settingsButtonsList:
            {
                direction: Flow.List.Direction.RightToLeft,
                dockAlignment: { x: 0.0, y: 0.5 },
                floatPosition: { x: 0.0, y: 0.5 },
                floatOffset: { x: 150, y: 0 }
            }
        };

        export let defaultPortraitSettingsNew: Settings =
        {
            ...defaultSettingsNew,
            floatOffset: {x: -30, y: -900},
            scaleFactor: 1.0,
            mainButtonList: defaultListSettings,
            settingsButtonsList: defaultListSettingsPortraitNew,
            showLeftHandedUI: false
        };
    }
}