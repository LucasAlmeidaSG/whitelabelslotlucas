/// <reference path="../generated/Assets.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class BackButton extends Flow.BaseElement<BackButton.Settings, BackButton.RuntimeData>
    {
        @AutoDispose protected _button: Flow.Button;

        /** Published when the back button has been clicked. */
        public get onBackClicked() { return this._button.onClicked; }

        public constructor(settings: BackButton.Settings, runtimeData: BackButton.RuntimeData)
        {
            super(settings, runtimeData);

            this._button = Flow.button.create(this.settings.button);
            if (runtimeData.enableInteractionArbiter)
            {
                this._button.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
            }
            if (runtimeData.showBackButton) { this.addChild(this._button); }
        }
    }

    export const backButton = Flow.declareElement(BackButton, true);

    export namespace BackButton
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            button: Flow.Button.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
            showBackButton: boolean;
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Back Button",
            dock: Flow.Dock.Float,
            dockAlignment: { x: 0.5, y: 0.0 },
            floatPosition: { x: 0.5, y: 0.0 },
            floatOffset: { x: -380, y: 50 },
            sizeToContents: true,
            button:
            {
                name: "Back Icon",
                dock: Flow.Dock.Fill,
                scaleFactor: 0.8,
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.back_icon },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.back_icon },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.back_icon },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.back_icon },
                spacing: Flow.Spacing.none,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "BackButton",
                //new asset too big, set fixed size for old settings
                sizeToContents: false,
                size: { w: 27, h: 26 }
            }
        };

        export let defaultPortraitSettings: Settings =
        {
            ...defaultSettings,
            floatOffset: { x: -380, y: 1015 }
        }

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            dockAlignment: { x: 0.0, y: 0.0 },
            floatPosition: { x: 0.0, y: 0.0 },
            floatOffset: { x: 15, y: 20 },
            scaleFactor: 0.8,
            button:
            {
                ...defaultSettings.button,
                scaleFactor: 1.0,
                sizeToContents: true
            }
        };
    }
}