/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/Translations.ts" />

namespace RS.GDM.UI
{
    @HasCallbacks
    export class StakeButton extends Flow.BaseElement<StakeButton.Settings, StakeButton.RuntimeData>
    {
        public readonly onInputOptionChanged = createEvent<{ inputOption: number; optionIndex: number; }>();

        @AutoDispose protected _button: Flow.Button;
        @AutoDispose protected _closeButton: Flow.Button;

        protected _isOpen: boolean;
        protected _stakeAmount: number;
        protected _spinnerList: Flow.List;
        protected _spinners: Flow.Spinner[];

        @AutoDispose protected readonly _canChangeBet: IObservable<boolean>;

        /** Published when the stake button has been clicked. */
        public get onStakeClicked() { return this._button.onClicked; }

        /** Published when the close button has been clicked. */
        public get onCloseClicked() { return this._closeButton.onClicked; }

        public get isOpen() { return this._isOpen; }
        public set isOpen(value: boolean)
        {
            if (this._isOpen === value) { return; }
            this._isOpen = value;
            //If bets can be changed their states can change freely
            if (this._canChangeBet.value)
            {
                this._closeButton.enabled = value;
                this._button.enabled = !value;
            }
            this._button.visible = !value;
            this._closeButton.visible = value;

            for (const spinner of this._spinners)
            {
                for (const child of spinner.children)
                {
                    if (child instanceof Flow.Button)
                    {
                        child.visible = value;
                    }
                }
            }
        }

        public constructor(settings: StakeButton.Settings, runtimeData: StakeButton.RuntimeData)
        {
            super(settings, runtimeData);

            this._canChangeBet = Observable.reduce([runtimeData.canChangeBetArbiter, runtimeData.enableInteractionArbiter], Arbiter.AndResolver);

            this._spinnerList = Flow.list.create(settings.spinnerList, this);

            this._spinners = [];
            if (runtimeData.spinners)
            {
                for (let i = 0, l = runtimeData.spinners.length; i < l; ++i)
                {
                    const spinnerSettings = runtimeData.spinners[i];
                    const spinner = Flow.spinner.create(spinnerSettings.spinner);
                    spinner.bindToObservables({ enabled: this._canChangeBet });
                    spinner.onValueChanged((newValue) =>
                    {
                        const idx = spinner.options.indexOf(newValue);
                        this.onInputOptionChanged.publish({ inputOption: i, optionIndex: idx });
                    });
                    this._spinnerList.addChild(spinner);
                    this._spinners.push(spinner);
                    for (const child of spinner.children)
                    {
                        if (child instanceof Flow.Container)
                        {
                            spinner.moveToBottom(child);
                        }
                        if (child instanceof Flow.Button)
                        {
                            child.visible = false;
                        }
                    }
                }
            }
            this._button = Flow.button.create(this.settings.button, this);
            this._closeButton = Flow.button.create(this.settings.closeButton, this);
            this._closeButton.visible = false;

            this._button.bindToObservables({ enabled: this._canChangeBet });
            this._closeButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
        }

        public getSpinner(index: number)
        {
            return this._spinners[index];
        }
    }

    export const stakeButton = Flow.declareElement(StakeButton, true);

    export namespace StakeButton
    {
        export interface SpinnerSettings
        {
            spinner: Flow.Spinner.Settings;
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            button: Flow.Button.Settings;
            closeButton: Flow.Button.Settings;
            spinnerList: Flow.List.Settings;
            container: Flow.Container.Settings;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: IReadonlyObservable<boolean>;
            canChangeBetArbiter: IArbiter<boolean>;

            spinners: SpinnerSettings[];
        }

        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Elements,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        export let defaultSpinnerSettings: Flow.Spinner.Settings =
        {
            dock: Flow.Dock.Fill,
            decrementButton:
            {
                ...Flow.Spinner.defaultSettings.decrementButton,
                dock: Flow.Dock.Left,
                name: "Decrement",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_minus_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_minus_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_minus_button_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_minus_button_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null
            },
            incrementButton:
            {
                ...Flow.Spinner.defaultSettings.incrementButton,
                name: "Increment",
                dock: Flow.Dock.Right,
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_plus_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_plus_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_plus_button_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_plus_button_disabled },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null
            },
            background:
            {
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Elements,
                frame: Assets.UI.Elements.button_panel_background,
                sizeToContents: true,
                expand: Flow.Expand.Allowed,
                scaleFactor: 0.56 //new asset is bigger, scale original down
            },
            label:
            {
                ...Flow.Spinner.defaultSettings.label,
                font: Fonts.Frutiger.CondensedMedium,
                textColor: Util.Colors.black,
                fontSize: 22
            },
            container:
            {
                ...Flow.Spinner.defaultSettings.container,
                innerPadding: { top: 2.25, bottom: 0, left: 0, right: 0 },
                dock: Flow.Dock.Bottom,
                expand: Flow.Expand.Disallowed,
                overflowMode: Flow.OverflowMode.Shrink,
                spacing: Flow.Spacing.horizontal(5)
            },
            size: { w: 250, h: 130 },
            expand: Flow.Expand.Allowed,
            options:
            {
                type: Flow.Spinner.Type.Options,
                options: [0]
            }
        };

        /** @deprecated use getSpinnerSettingsNew */
        export function getSpinnerSettings(incSelID?: string, decSelID?: string): SpinnerSettings
        {
            return {
                spinner:
                {
                    ...defaultSpinnerSettings,
                    incrementButton:
                    {
                        ...defaultSpinnerSettings.incrementButton,
                        seleniumId: incSelID
                    },
                    decrementButton:
                    {
                        ...defaultSpinnerSettings.decrementButton,
                        seleniumId: decSelID
                    }
                }
            };
        }

        const defaultButtonSettings: Flow.Button.Settings =
        {
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.375 },
            spacing: Flow.Spacing.none,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            textLabel: null
        };

        /** @deprecated use defaultSettingsNew */
        export let defaultSettings: Settings =
        {
            name: "Stake Button",
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.0 },
            dockAlignment: { x: 0.5, y: 0.0 },
            floatOffset: { x: 585, y: 605 },
            button:
            {
                ...defaultButtonSettings,
                name: "Stake Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_open_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_open_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_open_button_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.stake_open_button_disabled },
                seleniumId: "Stake Button"
            },
            closeButton:
            {
                ...defaultButtonSettings,
                name: "Close Button",
                background: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close_press },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Elements.settings_close_disabled },
                seleniumId: "Stake Button Close"
            },
            container:
            {
                name: "Stake Button Panel",
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed
            },
            spinnerList:
            {
                dock: Flow.Dock.Fill,
                size: { w: 260, h: 0 },
                direction: Flow.List.Direction.TopToBottom,
                contentAlignment: { x: 0.5, y: 0.5 }
            },
            sizeToContents: true,
            expand: Flow.Expand.Allowed
        };

        export let defaultLeftSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: defaultSettings.floatOffset.y }
        };
        export let defaultRightSettings: Partial<Settings> =
        {
            floatOffset: defaultSettings.floatOffset
        };
        export let defaultLeftPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: -defaultSettings.floatOffset.x, y: 1550 }
        };
        export let defaultRightPortraitSettings: Partial<Settings> =
        {
            floatOffset: { x: defaultSettings.floatOffset.x, y: 1550 }
        };

        export let defaultSpinnerSettingsNew: Flow.Spinner.Settings =
        {
            ...defaultSpinnerSettings,
            size: { w: 328, h: 250 },
            label:
            {
                ...defaultSpinnerSettings.label,
                expand: Flow.Expand.Allowed,
                fontSize: 42
            },
            background:
            {
                ...defaultSpinnerSettings.background,
                scaleFactor: 1
            }
        };

        //TODO 1.2 - change to getSpinnerSettings, export this and deprecate
        export function getSpinnerSettingsNew(incSelID?: string, decSelID?: string): SpinnerSettings
        {
            return {
                spinner:
                {
                    ...defaultSpinnerSettingsNew,
                    incrementButton:
                    {
                        ...defaultSpinnerSettingsNew.incrementButton,
                        seleniumId: incSelID
                    },
                    decrementButton:
                    {
                        ...defaultSpinnerSettingsNew.decrementButton,
                        seleniumId: decSelID
                    }
                }
            };
        }

        //TODO 1.2 - change to defaultSettings, export this and deprecate
        export let defaultSettingsNew: Settings =
        {
            ...defaultSettings,
            floatPosition: { x: 1.0, y: 0.5 },
            dockAlignment: { x: 1.0, y: 0.5 },
            floatOffset: { x: -72, y: -160 },
            scaleFactor: 0.6,
            closeButton:
            {
                ...defaultSettings.closeButton,
                scaleFactor: 1.2
            },
            spinnerList:
            {
                ...defaultSettings.spinnerList,
                size: { w: 328, h: 0 }
            }
        };

        export let defaultLeftSettingsNew: Partial<Settings> =
        {
            floatPosition: { x: 0.0, y: 0.5 },
            dockAlignment: { x: 0.0, y: 0.5 },
            floatOffset: { x: 72, y: -160 },
        };

        export let defaultPortraitSettingsNew: Settings =
        {
            ...defaultSettingsNew,
            scaleFactor: 1.0,
            floatPosition: { x: 0.5, y: 1.0 },
            dockAlignment: { x: 0.5, y: 1.0 },
            floatOffset: { x: -350, y: -90 },
        };
    }
}