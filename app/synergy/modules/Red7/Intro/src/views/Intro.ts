namespace Red7.Views
{
    /**
     * The Intro view.
     */
    @RS.HasCallbacks
    // @RS.View.RequiresAssets([ ]) // TODO: Add any required asset groups here
    @RS.View.ScreenName("Intro")
    export class Intro extends RS.View.Base<Intro.Settings, RS.Game.Context>
    {
        /** Published when the intro video has finished. */
        public readonly onFinished = RS.createSimpleEvent();

        @RS.AutoDispose protected _video: RS.Rendering.IVideo;
        @RS.AutoDispose protected _clickCapture: RS.View.IClickCapture;
        @RS.AutoDispose protected _bg: RS.Flow.Background;
        @RS.AutoDispose protected _skipLabel: RS.Flow.Label;
        @RS.AutoDispose protected _skipLabelDOM: RS.DOM.Component;

        /**
         * Called when this view has been opened.
         */
        public onOpened(): void
        {
            super.onOpened();

            /* setup locale */
            this.locale = this.context.game.locale;

            this._bg = RS.Flow.background.create({
                dock: RS.Flow.Dock.Fill,
                kind: RS.Flow.Background.Kind.SolidColor,
                borderSize: 0,
                borderColor: RS.Util.Colors.black,
                color: RS.Util.Colors.black
            }, this);

            this._video = new RS.Rendering.Video({
                ...(this.settings.videoSettings || {}),
                cssContainer: this.context.game.autoscaleContainer
            });
            this._video.onFinished(this.handleVideoFinished);

            if (this.settings.videoSize)
            {
                this._video.pivot.set(this.settings.videoSize.w * 0.5, this.settings.videoSize.h * 0.5);
                this._video.x = this.controller.width * 0.5;
                this._video.y = this.controller.height * 0.5;
            }

            this.addChild(this._video);

            // add a label to display "Tap / Click to skip" information
            if (this._video.tag.parentElement != null)
            {
                this._skipLabelDOM = new RS.DOM.Component("rs-skip");
                this._skipLabelDOM.text = RS.Localisation.resolveLocalisableString(this.locale, (this.settings.skipLabel || Intro.defaultSkipLabelSettings).text);
                this.context.game.autoscaleContainer.addChild(this._skipLabelDOM);
            }
            else
            {
                this._skipLabel = RS.Flow.label.create(this.settings.skipLabel || Intro.defaultSkipLabelSettings, this);
            }

            this._video.play({ src: this.settings.videoAsset });

            this._clickCapture = this.controller.captureFullScreenClicks();
            this._clickCapture.onClicked(this.cancel);
            this._video.onClicked(this.cancel);
        }

        /**
         * Prematurely cancels the video.
         */
        @RS.Callback
        public cancel(): void
        {
            this._video.pause();
            this.onFinished.publish();
        }

        /**
         * Called when this view has been closed.
         */
        public onClosed(): void
        {
            super.onClosed();
        }

        @RS.Callback
        protected async handleVideoFinished()
        {
            this._clickCapture.dispose();
            this._clickCapture = null;

            const duration = 1000;

            // fade video and background to view
            await RS.Timeline.get([
                RS.Tween.get(this._bg)
                    .to({ alpha: 0.0 }, duration),
                RS.Tween.get(this._video)
                    .to({ alpha: 0.0 }, duration)
            ]);

            this.onFinished.publish();
        }
    }

    export namespace Intro
    {
        export const defaultSkipLabelSettings: RS.Flow.Label.Settings =
        {
            ...RS.Flow.Label.defaultSettings,
            text: Translations.Intro.SkipText,
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.9 },
            dockAlignment: { x: 0.5, y: 1.0 },
        };

        export const defaultSettings: Settings =
        {
            videoAsset: null,
            videoSettings: null,
            videoSize: null,
            skipLabel: defaultSkipLabelSettings
        };

        export interface Settings extends RS.View.Base.Settings
        {
            videoAsset: RS.Asset.VideoReference;
            videoSettings?: Partial<RS.Rendering.Video.Settings>;
            videoSize?: RS.Math.Size2D;
            skipLabel?: RS.Flow.Label.Settings;
        }
    }
}
