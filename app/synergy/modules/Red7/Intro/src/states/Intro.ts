/// <reference path="../views/Intro.ts" />

namespace Red7.States
{
    export const Intro = "intro";

    /**
     * The Intro state.
     */
    @RS.HasCallbacks
    @RS.State.Name(Intro)
    export class IntroState extends RS.State.Base<RS.Game.Context>
    {
        protected _view: Views.Intro|null = null;
        @RS.AutoDispose protected _handleOrientationChanged: RS.IDisposable;

        public onEnter(): void
        {
            super.onEnter();
            
            if (!this.shouldPlayVideo())
            {
                this.transitionToNextState("");
                return;
            }

            this._handleOrientationChanged = RS.Orientation.onChanged(this.handleOrientationChanged);

            this.doIntro();
        }

        protected shouldPlayVideo(): boolean
        {
            if (RS.IOS.iPhoneVersion === "iPhone 6+") { return false; }
            if (!this.orientationIsSupported(RS.Orientation.currentOrientation)) { return false; }
            return true;
        }

        @RS.Callback
        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation)
        {
            if (this._view == null) { return; }
            if (!this.orientationIsSupported(newOrientation))
            {
                this._view.cancel();
            }
        }

        protected async doIntro()
        {
            this._view = await this._context.game.viewController.open(Views.Intro);
            await this._view.onFinished;
            this._view = null;
            await this._context.game.viewController.close(Views.Intro);
            this.transitionToNextState("");
        }

        protected orientationIsSupported(orientation: RS.DeviceOrientation): boolean
        {
            return orientation === RS.DeviceOrientation.Landscape;
        }
    }
}