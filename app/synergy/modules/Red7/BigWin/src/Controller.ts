/// <reference path="View.ts"/>

namespace Red7.BigWin
{
    /**
     * Controller class which maintains a Big Win renderer instances and processes the events starting or stopping it.
     */
    @RS.HasCallbacks
    export class Controller implements RS.BigWin.IController
    {
        private _bigWinComponent: View;
        private _volume: number = 1.0;
        private _isDisposed: boolean = false;
        private readonly _isAnimating = new RS.Observable(false);

        private _runtimeData: RS.BigWin.IController.RuntimeData;

        public get isDisposed() { return this._isDisposed; }

        /** Initialises this controller. */
        public constructor(public readonly settings: Controller.Settings)
        {
            
        }

        /**
         * Initialises the big win controller.
         * @param runtimeData
         */
        public initialise(runtimeData: RS.BigWin.IController.RuntimeData): void
        {
            this._runtimeData = runtimeData;
            runtimeData.assetController.setGroupPriority(Assets.Groups.bigWin, RS.Asset.LoadPriority.Low);
        }

        /**
         * Gets if the specified stake and payout constitutes a big win.
         * @param totalStake
         * @param totalPayout
         */
        public isBigWin(totalStake: number, totalPayout: number): boolean
        {
            return this.identifyTier(totalStake, totalPayout) > Tier.None;
        }

        /**
         * Starts animating big win.
         * @param totalStake
         * @param totalPayout
         */
        public async do(totalStake: number, totalPayout: number)
        {
            if (this._isAnimating.value) { throw new Error("Tried to start big win when it was already started"); }
            await this._runtimeData.assetController.loadGroup(Assets.Groups.bigWin).complete.for(true);
            this._isAnimating.value = true;
            const viewSettings = {...View.defaultSettings, ...this.settings.viewSettings};
            this._bigWinComponent = await this._runtimeData.viewController.open(View, viewSettings);
            this._bigWinComponent.globalVolume = this.settings.volume;
            const bigWinSettings: BigWinSettings =
            {
                totalPayout, totalStake,
                tierMultiplier: 1.0,
                bonusMode: false,
                currencyFormatter: this._runtimeData.currencyFormatter,
                locale: this._runtimeData.locale,
                tierReached: this.identifyTier(totalStake, totalPayout),
                viewSettings: viewSettings
            };
            await this._bigWinComponent.doAnimation(bigWinSettings);
            await this._runtimeData.viewController.close(View);
            this._bigWinComponent = null;
            this._isAnimating.value = false;
        }

        /**
         * Skips any currently animating big win.
         */
        public async skip()
        {
            if (!this._isAnimating.value) { return; }
            this._bigWinComponent.onSkip();
            await this._isAnimating.for(false);
        }

        public dispose()
        {
            if (this.isDisposed) { return; }
            if (this._isAnimating.value)
            {
                throw new Error("Tried to dispose big win controller whilst it was animating");
            }
            this._isDisposed = true;
        }

        /**
         * Gets the tier reached for the specified stake and payout.
         * @param totalStake
         * @param totalPayout
         */
        protected identifyTier(totalStake: number, totalPayout: number): Tier
        {
            const multiplier = totalPayout / totalStake;
            
            for (let tier = Tier.Epic; tier >= Tier.None; --tier)
            {
                let threshold: number | undefined;
                switch (tier)
                {
                    case Tier.None: threshold = 0; break;
                    case Tier.Big: threshold = this.settings.thresholds.big; break;
                    case Tier.Super: threshold = this.settings.thresholds.super; break;
                    case Tier.Mega: threshold = this.settings.thresholds.mega; break;
                    case Tier.Epic: threshold = this.settings.thresholds.epic; break;
                }
                if (threshold != null && multiplier >= threshold) { return tier; }
            }
            return Tier.None;
        }
    }

    export namespace Controller
    {
        export interface Settings extends RS.BigWin.IController.Settings
        {
            viewSettings: View.Settings;
            volume: number;
            thresholds:
            {
                big?: number;
                super?: number;
                mega?: number;
                epic?: number;
            };
        }
    }
}
