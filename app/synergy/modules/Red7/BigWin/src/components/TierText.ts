/// <reference path="tiertexts/BigWin.ts" />
/// <reference path="tiertexts/SuperWin.ts" />
/// <reference path="tiertexts/MegaWin.ts" />
/// <reference path="tiertexts/EpicWin.ts" />

namespace Red7.BigWin
{
    /**
     * Contains and controls which tier of text is shown during the Big Win, handling upgrades.
     * Tiers are in the order BIG, SUPER, MEGA and EPIC.
    **/
    export class TierText extends RS.Rendering.DisplayObjectContainer implements IBigWinComponent
    {
        protected static tierTextClasses = [ TierTexts.BigWin, TierTexts.SuperWin, TierTexts.MegaWin, TierTexts.EpicWin ];

        protected _currentTier: Tier;
        protected _currentText: TierTexts.Base|null = null;
        protected _bonusMode: boolean = false;

        public constructor(private masterComponent: IBigWinComponent)
        {
            super("Big Win Tier Text");
        }

        /**
         * Starts the tier text animation.
        **/
        public start(bonusMode: boolean): void
        {
            this._bonusMode = bonusMode;

            this._currentTier = Tier.Big;
            this.enterNewTierText(this._currentTier);
        }

        /**
         * Stops the tier text animation.
        **/
        public stop(): void
        {
            return;
        }

        /**
         * Called when an upgrade is predicted to happen soon.
        **/
        public onPreUpgrade(tier: Tier, after: number): void
        {
            if (this._currentText)
            {
                RS.Tween.get<TierText>(this)
                    .wait(after - this._currentText.exitTime)
                    .call((t) => { this._currentText.state = TierTexts.State.Exiting; });
            }
        }

        /**
         * Called when an upgrade just happened.
        **/
        public onUpgrade(tier: Tier): void
        {
            if (this._currentTier === tier) { return; }
            RS.Tween.removeTweens<TierText>(this);
            this._currentTier = tier;
            this.enterNewTierText(this._currentTier);
        }

        /**
         * Called when the countup has finished.
        **/
        public onFinished(): void
        {
            RS.Tween.removeTweens<TierText>(this);
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }
        public onSkip(): void { return; }

        protected enterNewTierText(tier: Tier): void
        {
            // Remove old tier text
            if (this._currentText)
            {
                this._currentText.state = TierTexts.State.None;
                this.removeChild(this._currentText);
                this._currentText = null;
            }

            // Create new tier text
            const typ = TierText.tierTextClasses[this._currentTier - 1];
            if (!typ) { return; }
            const text: TierTexts.Base = this._currentText = new typ();
            text.bonusMode = this._bonusMode;
            const bounds = text.estimatedBounds;
            text.pivot.set(bounds.x + bounds.w * 0.5, bounds.y + bounds.h * 0.5);
            this.addChild(text);

            // Animate it in
            text.state = TierTexts.State.Entering;
        }
    }
}