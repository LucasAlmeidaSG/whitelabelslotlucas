namespace Red7.BigWin
{
    /**
     * Frame around the Big Win with animating lights.
     */
    export class Frame extends RS.Rendering.DisplayObjectContainer implements IBigWinComponent
    {
        /**
         * Background bitmap.
         */
        private _background: RS.Rendering.IBitmap;
        /**
         * Frame bitmap.
         */
        private _frame: RS.Rendering.IBitmap;
        /**
         * Lights which go around the frame.
         */
        private _frameLights: RS.Rendering.IAnimatedSprite[] = [];

        public constructor(private masterComponent: IBigWinComponent)
        {
            super("Big Win Frame");

            this._frame = RS.Factory.bitmap(Assets.BigWin.Frame, {
                anchor: { x: 0.5, y: 0.5 }
            });

            this._background = RS.Factory.bitmap(Assets.BigWin.BG, {
                anchor: { x: 0.5, y: 0.5 },
                scale: { x: 1.05, y: 1.05 }
            });

            const frameLights1 = RS.Factory.sprite(Assets.BigWin.FrameLights);
            frameLights1.gotoAndStop(Assets.BigWin.FrameLights.lightsB);
            frameLights1.framerate = View.spriteFramerate;
            frameLights1.x = -5;
            frameLights1.y = 5;
            frameLights1.blendMode = RS.Rendering.BlendMode.Add;

            const frameLights2 = RS.Factory.sprite(Assets.BigWin.FrameLights);
            frameLights2.gotoAndStop(Assets.BigWin.FrameLights.lightsB);
            frameLights2.framerate = View.spriteFramerate;
            frameLights2.x = -5;
            frameLights2.y = 5;
            frameLights2.blendMode = RS.Rendering.BlendMode.Add;

            this._frameLights.push(frameLights1, frameLights2);

            this.addChildren(this._background, this._frame, frameLights1, frameLights2);
        }

        /**
         * Starts the frame lights animating.
         */
        public start(): void
        {
            this._frameLights[0].gotoAndPlay(Assets.BigWin.FrameLights.lightsB);
            const frameLights2 = this._frameLights[1];
            frameLights2.gotoAndPlay(Assets.BigWin.FrameLights.lightsB);
            frameLights2.currentAnimationFrame = Math.round(frameLights2.spriteSheet.getAnimation(Assets.BigWin.FrameLights.lightsB).frames.length / 2);
        }

        /**
         * Stops the frame lights animating.
         */
        public stop(): void
        {
            this._frameLights[0].stop();
            this._frameLights[1].stop();
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onPreUpgrade(tier: Tier, after: number): void { return; }
        public onUpgrade(tier: Tier, skipped: boolean): void { return; }
        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }
        public onSkip(): void { return; }
        public onFinished(): void { return; }
    }
}
