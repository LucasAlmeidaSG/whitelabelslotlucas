namespace Red7.BigWin
{
    /**
     * Two stars which appear either side of the Big Win and spin.
     */
    export class Stars extends RS.Rendering.DisplayObjectContainer implements IBigWinComponent
    {
        /**
         * Left star instance.
         */
        private _leftStar: RS.Rendering.IAnimatedSprite;
        /**
         * Right star instance.
         */
        private _rightStar: RS.Rendering.IAnimatedSprite;

        public constructor(private masterComponent: IBigWinComponent)
        {
            super("Big Win Stars");

            this._leftStar = RS.Factory.sprite(Assets.BigWin.Star);
            this._leftStar.gotoAndStop(Assets.BigWin.Star.star);
            this._leftStar.framerate = View.spriteFramerate;
            this._leftStar.x = -400;
            this._leftStar.y = 30;

            this._rightStar = RS.Factory.sprite(Assets.BigWin.Star);
            this._rightStar.gotoAndStop(Assets.BigWin.Star.star);
            this._rightStar.framerate = View.spriteFramerate;
            this._rightStar.x = 400;
            this._rightStar.y = 30;
            this._rightStar.scaleX = -1;

            this.addChildren(this._rightStar, this._leftStar);
        }

        /**
         * Starts the stars animating.
         */
        public start(): void
        {
            this._leftStar.play();
            this._rightStar.play();
        }

        /**
         * Stops the stars animating.
         */
        public stop(): void
        {
            this._leftStar.stop();
            this._rightStar.stop();
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onPreUpgrade(tier: Tier, after: number): void { return; }
        public onUpgrade(tier: Tier): void { return; }
        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }
        public onSkip(): void { return; }
        public onFinished(): void { return; }
    }
}
