namespace Red7.BigWin
{
    /**
     * Spotlights on the Big Win which rotate back and forth.
     */
    export class Spotlights extends RS.Rendering.DisplayObjectContainer implements IBigWinComponent
    {
        /**
         * Distance from center to position spotlights.
         */
        private static spotlightDistancing = 300;
        /**
         * Amount each spotlight rotates each side.
         */
        private static spotlightRotationFrom = RS.Math.degreesToRadians(25);
        private static spotlightRotationTo = RS.Math.degreesToRadians(-25);
        /**
         * Left spotlight instance.
         */
        private _leftSpotlight: RS.Rendering.IDisplayObject;
        /**
         * Right spotlight instance.
         */
        private _rightSpotlight: RS.Rendering.IDisplayObject;

        /**
         * @param referenceDimensions - reference dimensions, same as BigWinRenderer property.
         */
        public constructor(private masterComponent: IBigWinComponent, referenceDimensions: RS.Dimensions)
        {
            super("Big Win Spotlights");

            this._leftSpotlight = this.createSpotlight(referenceDimensions);
            this._leftSpotlight.x = -Spotlights.spotlightDistancing;

            this._rightSpotlight = this.createSpotlight(referenceDimensions);
            this._rightSpotlight.x = Spotlights.spotlightDistancing;

            this.addChildren(this._leftSpotlight, this._rightSpotlight);
        }

        /**
         * Starts the spotlights moving.
         */
        public start(): void
        {
            this._leftSpotlight.rotation = Spotlights.spotlightRotationFrom;

            const tweenTime = 2000;
            const ease = RS.Ease.quadInOut;

            RS.Tween.get(this._leftSpotlight, { override: true, loop: true })
                .to({ rotation: Spotlights.spotlightRotationTo }, tweenTime, ease)
                .to({ rotation: Spotlights.spotlightRotationFrom }, tweenTime, ease);

            this._rightSpotlight.rotation = Spotlights.spotlightRotationTo;
            const tween = RS.Tween.get(this._rightSpotlight, { override: true, loop: true })
                .to({ rotation: Spotlights.spotlightRotationFrom }, tweenTime, ease)
                .to({ rotation: Spotlights.spotlightRotationTo }, tweenTime, ease);
            tween.setPosition(500, RS.StepBehaviour.Skip);
        }

        /**
         * Stops the spotlights moving.
         */
        public stop(): void
        {
            RS.Tween.removeTweens(this._leftSpotlight);
            RS.Tween.removeTweens(this._rightSpotlight);
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onPreUpgrade(tier: Tier, after: number): void { return; }
        public onUpgrade(tier: Tier): void { return; }
        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }
        public onSkip(): void { return; }
        public onFinished(): void { return; }

        /**
         * Creates a spotlight.
         * @param referenceDimensions - reference dimensions, same as BigWinRenderer property.
         */
        private createSpotlight(referenceDimensions: RS.Dimensions): RS.Rendering.IDisplayObject
        {
            const spotlightContainer = new RS.Rendering.DisplayObjectContainer();

            const spotlight = new RS.Rendering.Graphics();

            const spotlightHeight = referenceDimensions.h * 1.5;
            const spotlightWidth = 250;
            const spotlightInset = Math.round(spotlightWidth * 0.3);

            spotlight.beginFill(new RS.Util.Color("#7f3a6b"), 0.75);
            spotlight.moveTo(0, 0);
            spotlight.lineTo(spotlightWidth, 0);
            spotlight.lineTo(spotlightWidth - spotlightInset, spotlightHeight);
            spotlight.lineTo(spotlightInset, spotlightHeight);
            spotlight.endFill();
            spotlight.blendMode = RS.Rendering.BlendMode.Screen;// PIXI.BLEND_MODES.SCREEN;
            spotlight.pivot.x = spotlightWidth * 0.5;
            spotlight.pivot.y = spotlightHeight;

            //const faderWidth = spotlightWidth * 0.1;
            //let leftSideFader = new RS.Create.GradientFader(faderWidth, -top, RS.LayoutOrientation.RightToLeft);
            //let rightSideFader = new RS.Create.GradientFader(faderWidth, -top, RS.LayoutOrientation.LeftToRight);
            //leftSideFader.x = 0;
            //rightSideFader.x = spotlightWidth - faderWidth;

            // TODO: Sort this out properly
            //spotlightContainer.addChild(spotlight, leftSideFader, rightSideFader);
            spotlightContainer.addChild(spotlight);

            // rotate faders to align with the spotlights.
            // let requiredAngle = RS.MathUtils.radiansToDegrees(Math.atan(spotlightInset / spotlightHeight));

            //leftSideFader.rotation = -requiredAngle;
            //rightSideFader.rotation = requiredAngle;
            return spotlightContainer;
        }
    }
}
