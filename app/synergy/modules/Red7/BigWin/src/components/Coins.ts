/// <reference path="../generated/Assets.ts" />

namespace Red7.BigWin
{
    /**
     * Coins which spit out from the bottom of the Big Win.
     */
    export class Coins implements IBigWinComponent
    {
        /**
         * Gets the container holding coins behind the frame.
         */
        public get rearContainer(): RS.Rendering.IContainer
        {
            return this._rearContainer;
        }

        /**
         * Gets the container holding coins in front of the frame.
         */
        public get frontContainer(): RS.Rendering.IContainer
        {
            return this._frontContainer;
        }

        protected _settings: Coins.Settings;

        /**
         * Pool of coin sprites to use.
         */
        protected _coinSprites: RS.Rendering.IAnimatedSprite[] = [];
        /**
         * Next index to use for a coin sprite.
         */
        protected _coinPoolIndex = 0;
        /**
         * Container holding coins which should be behind the frame.
         */
        protected _rearContainer: RS.Rendering.IContainer = new RS.Rendering.DisplayObjectContainer();
        /**
         * Container holding coins which should be in front of the frame.
         */
        protected _frontContainer: RS.Rendering.IContainer = new RS.Rendering.DisplayObjectContainer();
        /**
         * Current tier reached in the big win.
         */
        protected _tierReached: Tier;
        /**
         * If coins have started
         */
        protected _rendering: boolean;

        public constructor(protected masterComponent: IBigWinComponent, settings?: Coins.Settings)
        {
            this._rendering = true;
            this._settings = settings || Coins.defaultSettings;
            // create pool of coins
            for (let i = 0; i < this._settings.coinSpritePoolSize; i++)
            {
                const assetName = this._settings.coinSpriteAssetIDs[i % this._settings.coinSpriteAssetIDs.length];
                const coinSprite = RS.Factory.sprite(assetName);
                coinSprite.gotoAndStop(this._settings.coinAnimationNames[i % this._settings.coinAnimationNames.length]);
                coinSprite.framerate = View.spriteFramerate;
                coinSprite.y = this._settings.coinStartY;
                this._coinSprites.push(coinSprite);
            }
        }

        /**
         * Starts the coins looping.
         */
        public start(): void
        {
            this._rendering = true;
            this._tierReached = Tier.Big;
            this.coinLoop();
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onPreUpgrade(tier: number, after: number): void { return; }

        /**
         * Called when the big win is upgraded.
         */
        public onUpgrade(tier: Tier): void
        {
            this._tierReached = tier;
        }

        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }
        public onFinished(): void { return; }
        public onSkip(): void { return; }

        /**
         * Stop generating coins.
         */
        public stop(): void
        {
            this._rendering = false;
            RS.Tween.removeTweens<Coins>(this);
        }

        /**
         * Does a tween loop which generates coins continually.
         */
        protected async coinLoop()
        {
            const nextCoin = this._coinSprites[this._coinPoolIndex];
            const coinDirection = this._coinPoolIndex % this._settings.coinXVariance.length;
            const scaleFactor = this._settings.coinScaleVariance == null ? 1.0 : RS.Math.generateRandomNumber(this._settings.coinScaleVariance.min, this._settings.coinScaleVariance.max);

            const flipDirection = Math.random() < 0.5;
            const coinXVariance = this._settings.coinXVariance[coinDirection];
            let tweenToX = RS.Math.generateRandomNumber(coinXVariance.min, coinXVariance.max);

            // randomly flip direction so it goes the other way
            if (flipDirection)
            {
                nextCoin.scaleX = -0.5 * scaleFactor;
                tweenToX *= -1;
            }
            else
            {
                nextCoin.scaleX = 0.5 * scaleFactor;
            }

            const coinYVariance = this._settings.coinYVariance[this._tierReached - 1];
            const tweenToY = nextCoin.y - RS.Math.generateRandomNumber(coinYVariance.min, coinYVariance.max);

            nextCoin.scaleY = 0.5 * scaleFactor;

            const halfTweenTime = this._settings.coinTweenYTime;
            const totalTweenTime = halfTweenTime * 2;

            nextCoin.play();

            // scale coin to maximum (zoom in towards screen)
            RS.Tween.get(nextCoin)
                .to({scaleX:nextCoin.scaleX * 2.0, scaleY: nextCoin.scaleY * 2.0}, totalTweenTime);

            // tween coin to its maximum height, switch to front container, then tween back down
            RS.Tween.get(nextCoin)
                .to({ y: tweenToY }, halfTweenTime, RS.Ease.quadOut)
                .call(() =>
                {
                    this.rearContainer.removeChild(nextCoin);
                    this.frontContainer.addChild(nextCoin, 0);
                })
                .to({ y: this._settings.coinStartY }, halfTweenTime, RS.Ease.quadIn);
            // tween x position
            RS.Tween.get(nextCoin)
                .to({ x: tweenToX }, totalTweenTime)
                .call(() =>
                {
                    RS.Tween.removeTweens(nextCoin);
                    nextCoin.parent.removeChild(nextCoin);
                    nextCoin.gotoAndStop(this._settings.coinAnimationNames[coinDirection]);
                    nextCoin.x = 0;
                });

            this.rearContainer.addChild(nextCoin, 0);

            this._coinPoolIndex++;
            if (this._coinPoolIndex >= this._coinSprites.length)
            {
                this._coinPoolIndex = 0;
            }

            // wait then generate a new coin
            await RS.Tween.get<Coins>(this)
                .wait(this._settings.coinDeltas[this._tierReached - 1]);
            if (this._rendering) { this.coinLoop(); }
        }
    }

    export namespace Coins
    {
        export interface Settings
        {
            /** Number of coin sprites to pool. Must be a multiple of 3. */
            coinSpritePoolSize: number;

            coinSpriteAssetIDs: RS.Asset.RSSpriteSheetReference[];
            coinAnimationNames: string[];
            /** Time between new coins being thrown up, per tier. */
            coinDeltas: number[];
            /** Range of random final x position for different coin types. */
            coinXVariance: { min: number, max: number }[];
            /** Height reachable depending on tier (currently all the same). */
            coinYVariance: { min: number, max: number }[];
            /** Range of scales for the coins. */
            coinScaleVariance?: { min: number, max: number };
            /** Time it takes for a coin to reach max height. */
            coinTweenYTime: number;
            /** y position coins start at. */
            coinStartY: number;
        }

        export let defaultSettings: Coins.Settings =
        {
            coinSpritePoolSize: 30,
            coinSpriteAssetIDs: [Assets.BigWin.Coins, Assets.BigWin.Coins, Assets.BigWin.Coins],
            coinAnimationNames: [Assets.BigWin.Coins.coin1_coinA, Assets.BigWin.Coins.coin2_coinB, Assets.BigWin.Coins.coin3_coinC],
            coinDeltas: [200, 150, 110, 70],
            coinXVariance: [{ min: 0, max: 150 }, { min: 150, max: 400 }, { min: 400, max: 600 }],
            coinYVariance: [{ min: 260, max: 300 }, { min: 260, max: 300 }, { min: 260, max: 300 }, { min: 260, max: 300 }],
            coinTweenYTime: 600,
            coinStartY: 500
        };
    }
}
