namespace Red7.BigWin
{
    /**
     * A bitmap text instances which counts up to the Big Win payout.
     */
    export class Countup extends RS.Rendering.BitmapText implements IBigWinComponent
    {
        /**
         * Tween to this value.
         */
        protected _toValue: number;
        /**
         * Tweened current value.
         */
        protected _currentValue: number;

        public get currentValue() { return this._currentValue; }
        public set currentValue(value) { this._currentValue = value; }
        /**
         * Currency code to format the countup with.
         */
        protected _currencyFormatter: RS.Localisation.CurrencyFormatter;
        /**
         * Whether or not this countup has been skipped.
         */
        protected _skipped: boolean;
        /**
         * Current tier scale.
         */
        protected _curScaleFactor: number = 1;
        /**
         * The final scale factor.
         */
        protected _finalScaleFactor: number = 1;
        /**
         * Scale modifier to apply (to compensate for currencies with larger width)
         */
        protected _scaleModifier: number = 1;

        protected _curTier: number = 0;
        protected _finalTier: number = 0;
        protected _tierMultiplier: number = 1;
        protected _betAmount: number = 0;

        protected _bigwinSettings: BigWinSettings;
        protected _settings: Countup.Settings;

        public constructor(protected masterComponent: IBigWinComponent, settings?: Countup.Settings)
        {
            super(RS.Asset.Store.getAsset(settings ? settings.font : Countup.defaultSettings.font) as RS.Asset.IBitmapFontAsset);
            this._settings = settings || Countup.defaultSettings;

            this.anchorX = this.anchorY = 0.5;

            this.y = 100;
        }

        /**
         * Starts the big win countup to the given value and with the given currency code.
         */
        public start(settings: BigWinSettings): void
        {
            // Store data from event
            this._toValue = settings.totalPayout;
            this._currencyFormatter = settings.currencyFormatter;
            this._betAmount = settings.totalStake;
            this._tierMultiplier = settings.tierMultiplier;
            this._finalTier = settings.tierReached;
            this._bigwinSettings = settings;

            if (this._settings.offset != null)
            {
                this.x += this._settings.offset.x;
                this.y += this._settings.offset.y;
            }

            this.doCountup();

            // // work out the scale of the text
            // // let scale = RS.Device.isMobileDevice ? 2 : 1;
            // const scale = 1;
            // //this.letterSpacing = -35 / scale;
            // //this.spaceWidth = 25;
            // this.text = CurrencyUtils.formatCurrencyString(this._currencyCode, event.payout);
            // const scaledWidth = this.getBounds().width * scale * (1 + this._settings.scalePerTier * tierReached);
            // if (scaledWidth > this._settings.maxWidth)
            // {
            //     this._scaleModifier = this._settings.maxWidth / scaledWidth;
            // }
            // this.scale.x = this.scale.y = scale * this._scaleModifier;

            // this.onValueChanged();

            // // tween which will publish an upgrade event at set intervals (if required)
            // let intervalTween = RS.Tween.get(this);

            // // extra delay for first tier
            // if (tierReached > 1)
            // {
            //     intervalTween.wait(intervalTime);
            // }

            // // if reached the highest tier, set to maximum time
            // if (tierReached === Utils.numTiers)
            // {
            //     totalTime = Utils.MAXIMUM_BIG_WIN_TIME;
            //     for (let tier = 1; tier < Utils.numTiers; tier++)
            //     {
            //         intervalTween.wait(intervalTime)
            //             .call(() => this.publishUpgradeEvent());
            //     }
            // }
            // else
            // {
            //     for (let tier = 1; tier < tierReached; tier++)
            //     {
            //         intervalTween.wait(intervalTime)
            //             .call(() => this.publishUpgradeEvent());
            //     }

            //     let breachedMultiple = Utils.BIG_WIN_MULTIPLES[tierReached - 1] * event.tierMultiplier;
            //     let totalMultiple = event.payout / event.totalBet - breachedMultiple;
            //     let nextMultiple = Utils.BIG_WIN_MULTIPLES[tierReached] * event.tierMultiplier - breachedMultiple;
            //     let distanceToNextMultiple = totalMultiple / nextMultiple;

            //     totalTime = intervalTime * tierReached + (distanceToNextMultiple * intervalTime);
            // }

            // // start countup tween
            // RS.Tween.get(this, { onChange: () => this.onValueChanged() })
            //     .to({ _currentValue: this._toValue }, totalTime)
            //     .call(t =>
            //     {
            //         this.onComplete();
            //     });

            this._skipped = false;
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onPreUpgrade(tier: number, after: number): void { return; }
        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }

        /**
         * Skips the Big Win.
         */
        public onSkip(): void
        {
            if (this._skipped)
            {
                return;
            }

            RS.Tween.removeTweens<Countup>(this);

            this._skipped = true;

            const newScale = this._scaleModifier * this._finalScaleFactor;
            RS.Tween.get<Countup>(this, { override: true })
                .to({ scaleX: this.scaleX * 0.5, y: this.scaleY * 0.5 }, 100, RS.Ease.quadIn)
                .to({ scaleX: newScale * 1.1, y: newScale * 1.1 }, 200, RS.Ease.quadOut)
                .to({ scaleX: newScale, y: newScale }, 500, RS.Ease.quadInOut);

            this.onComplete();
        }

        /**
         * Called when the big win is upgraded into a new tier.
         */
        public onUpgrade(tier: number, skipped: boolean): void
        {
            this._curScaleFactor += this._settings.scalePerTier;
            const newScale = this._scaleModifier * this._curScaleFactor;
            RS.Tween.get<Countup>(this)
                .to({ scaleX: this.scaleX * 0.5, y: this.scaleY * 0.5 }, 100, RS.Ease.quadIn)
                .to({ scaleX: newScale * 1.1, y: newScale * 1.1 }, 200, RS.Ease.quadOut)
                .to({ scaleX: newScale, y: newScale }, 500, RS.Ease.quadInOut);
        }

        /**
         * Called when the big win has faded out completely.
         */
        public onFinished(): void
        {
            this.text = " ";
        }

        protected async doCountup()
        {
            this._curTier = Tier.Big;
            const tierReached = this._finalTier;

            // Work out interval timing
            const intervals = new Array<number>(tierReached + 1);
            for (let i = this._curTier; i < intervals.length; i++) { intervals[i] = this._bigwinSettings.viewSettings.tierInterval; }
            if (tierReached > 0) { intervals[this._curTier] += this._bigwinSettings.viewSettings.tierInterval; }
            if (tierReached === Tier.Epic)
            {
                const sum = intervals.reduce((a, b) => a + b);
                const normFactor = this._bigwinSettings.viewSettings.maximumBigWinTime / sum;
                for (let i = this._curTier; i < intervals.length; i++) { intervals[i] *= normFactor; }
            }
            const totalTime = intervals.reduce((a, b) => a + b);
            const intervalValues = new Array<number>(tierReached + 1);
            {
                let sum = 0;
                for (let i = this._curTier; i < intervalValues.length; i++)
                {
                    sum += intervals[i];
                    const delta = sum / totalTime;
                    intervalValues[i] = delta * this._toValue;
                }
            }
            
            // Work out text scaling
            this._currentValue = 0;
            this._curScaleFactor = 1;
            this._finalScaleFactor = 1 + this._settings.scalePerTier * tierReached;

            // Countup each segment
            this.onValueChanged();
            for (let i = this._curTier; i < intervals.length; i++)
            {
                if (i > Tier.Big)
                {
                    // Upgrade tier
                    this.publishUpgradeEvent();
                }

                const n = 1.0 - this._bigwinSettings.viewSettings.teaseTime / intervals[i];

                // Tween this segment
                if (this._curTier < this._finalTier)
                {
                    this.masterComponent.onPreUpgrade(this._curTier + 1, intervals[i]);
                }
                try
                {
                    await RS.Tween.get<Countup>(this, { onChange: () => this.onValueChanged()})
                        .to({ currentValue: intervalValues[i] }, intervals[i], RS.Ease.getTease(n));
                }
                catch (err)
                {
                    // This means the tween got cancelled, so let's exit out (as we're likely skipping)
                    return;
                }
                if (this._skipped) { return; }
            }

            // Done
            this.onComplete();
        }

        /**
         * Called when the tween has finished.
         */
        protected onComplete(): void
        {
            this._curTier = this._finalTier;
            // if (this._skipped)
            // {
            //     this.masterComponent.onUpgrade(this._curTier, this._skipped);
            // }
            this._skipped = true;
            this._currentValue = this._toValue;
            this.onValueChanged();
            this.masterComponent.onCountupComplete();
        }

        /**
         * Called every time the value changes.
         */
        protected onValueChanged(): void
        {
            const value = Math.round(this._currentValue);

            this.masterComponent.onCountupProgress(value);

            this.text = this._currencyFormatter.format(value);
            //     .replace("'", ",");
            // this._text = value.toString();
            //const bounds = this.getLocalBounds();
            //this.pivot.set(bounds.x + bounds.width * 0.5, bounds.y + bounds.height * 0.5);
        }

        /**
         * Publishes an upgrade big win event.
         */
        protected publishUpgradeEvent(): void
        {
            this._curTier++;
            this.masterComponent.onUpgrade(this._curTier, false);
        }
    }

    export namespace Countup
    {
        export interface Settings
        {
            font: RS.Asset.BitmapFontReference;
            offset?: RS.Math.Vector2D;
            maxWidth: number;
            /** How much extra scale to gain per tier. */
            scalePerTier: number;
        }

        export let defaultSettings: Settings =
        {
            font: Assets.BigWin.CurrencyFont,
            offset: RS.Math.Vector2D(0, 100),
            maxWidth: 900,
            scalePerTier: 0.1
        };
    }
}
