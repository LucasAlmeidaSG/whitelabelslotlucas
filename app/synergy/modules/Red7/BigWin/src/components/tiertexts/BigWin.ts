/// <reference path="Base.ts" />

namespace Red7.BigWin.TierTexts
{
    export class BigWin extends Base
    {
        protected _big: RS.Rendering.IBitmap;
        protected _bonus: RS.Rendering.IBitmap;
        protected _win: RS.Rendering.IBitmap;

        protected _scaleFactor: number = 1.2;

        /**
         * Gets how long this tier text will take to play the exit animation.
        **/
        public get exitTime() { return 500; }

        /**
         * Gets the estimated total frame of this tier text.
         * Useful for centering.
        **/
        public get estimatedBounds()
        {
            const rect = new RS.Rendering.Rectangle();
            if (this._bonusMode)
            {
                rect.x = -this._bonus.texture.width;
                rect.w = this._bonus.texture.width + this._win.texture.width;
                rect.h = Math.max(this._bonus.texture.height, this._win.texture.height) + this._big.texture.height;
                rect.y = rect.h * -0.5;
            }
            else
            {
                rect.x = -this._big.texture.width;
                rect.w = this._big.texture.width + this._win.texture.width;
                rect.h = Math.max(this._big.texture.height, this._win.texture.height);
                rect.y = rect.h * -0.5;
            }
            return rect;
        }

        public constructor()
        {
            super();
            
            this._big = RS.Factory.bitmap(Assets.BigWin.TierText.Big); 
            this.addChild(this._big);

            this._bonus = RS.Factory.bitmap(Assets.BigWin.TierText.Bonus);
            this._bonus.visible = false;
            this.addChild(this._bonus);

            this._win = RS.Factory.bitmap(Assets.BigWin.TierText.Win);
            this.addChild(this._win);

            this.state = State.None;
        }

        /** Updates the animation state. */
        protected async onStateChanged()
        {
            const sf = this._scaleFactor, fd = sf - 0.1, hd = sf - 0.05;

            switch (this._state)
            {
                case State.None:
                {
                    RS.Tween.removeTweens(this._big);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._win);

                    this._big.x = this._big.y = 0;
                    this._big.anchorX = 1;
                    this._big.anchorY = 0.5;
                    this._big.scaleX = this._big.scaleY = 0;

                    this._bonus.x = this._bonus.y = 0;
                    this._bonus.anchorX = 1;
                    this._bonus.anchorY = 0.5;
                    this._bonus.scaleX = this._bonus.scaleY = 0;

                    this._win.x = this._win.y = 0;
                    this._win.anchorX = 0;
                    this._win.anchorY = 0.5;
                    this._win.scaleX = this._win.scaleY = 0;

                    this._scaleFactor = this._bonusMode ? 1.2 : 1.1;

                    break;
                }
                case State.Entering:
                {
                    if (this._bonusMode)
                    {
                        this._bonus.anchorX = 1;
                        this._bonus.anchorY = 0.5;
                        this._bonus.x = 0;
                        this._bonus.visible = true;

                        const bounds = this.estimatedBounds;

                        this._big.anchorX = 0.5;
                        this._big.anchorY = 1.5;
                        this._big.x = bounds.x + bounds.w * 0.5;
                    }
                    else
                    {
                        this._big.anchorX = 1;
                        this._big.anchorY = 0.5;
                        this._big.x = 0;
                    }
                    this._win.anchorX = 0;
                    this._win.anchorY = 0.5;
                    this._win.x = 0;

                    await RS.Timeline.get([
                        RS.Tween.get(this._bonus)
                            .wait(this._bonusMode ? 200 : 0)
                            .to({scaleX: sf, scaleY: sf}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._big)
                            .to({scaleX: sf, scaleY: sf}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._win)
                            .wait(this._bonusMode ? 400 : 200)
                            .to({scaleX: sf, scaleY: sf}, 750, RS.Ease.backOut)
                    ]);
                    this.state = State.Looping;

                    break;
                }
                case State.Looping:
                {
                    if (this._bonusMode)
                    {
                        this._bonus.anchorX = this._bonus.anchorY = 0.5;
                        this._bonus.x = this._bonus.localBounds.w * -0.5 * sf;

                        this._big.anchorX = this._big.anchorY = 0.5;
                        this._big.y = this._big.height * -1.0 * sf;
                    }
                    else
                    {
                        this._big.anchorX = this._big.anchorY = 0.5;
                        this._big.x = this._big.localBounds.w * -0.5 * sf;
                    }
                    this._win.anchorX = this._win.anchorY = 0.5;
                    this._win.x = this._win.localBounds.w * 0.5 * sf;

                    if (this._bonusMode)
                    {
                        RS.Tween.get(this._bonus, {loop: true})
                            .to({scaleX: fd, scaleY: fd}, 800, RS.Ease.quadIn)
                            .to({scaleX: hd, scaleY: hd}, 400, RS.Ease.quadInOut)
                            .to({scaleX: fd, scaleY: fd}, 400, RS.Ease.quadInOut)
                            .to({scaleX: sf, scaleY: sf}, 800, RS.Ease.quadOut);    
                    }

                    RS.Tween.get(this._big, {loop: true})
                        .to({scaleX: fd, scaleY: fd}, 800, RS.Ease.quadIn)
                        .to({scaleX: hd, scaleY: hd}, 400, RS.Ease.quadInOut)
                        .to({scaleX: fd, scaleY: fd}, 400, RS.Ease.quadInOut)
                        .to({scaleX: sf, scaleY: sf}, 800, RS.Ease.quadOut);

                    RS.Tween.get(this._win, {loop: true})
                        .to({scaleX: fd, scaleY: fd}, 800, RS.Ease.quadIn)
                        .to({scaleX: hd, scaleY: hd}, 400, RS.Ease.quadInOut)
                        .to({scaleX: fd, scaleY: fd}, 400, RS.Ease.quadInOut)
                        .to({scaleX: sf, scaleY: sf}, 800, RS.Ease.quadOut);
                    
                    break;
                }
                case State.Exiting:
                {
                    RS.Tween.removeTweens(this._big);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._win);

                    RS.Tween.get(this._big)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._bonus)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._win)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);

                    break;
                }
            }
        }
    }
}