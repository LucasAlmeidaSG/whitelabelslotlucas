/// <reference path="Base.ts" />
/// <reference path="../../shaders/BulgeShader.ts" />

namespace Red7.BigWin.TierTexts
{
    const emptyVec = RS.Math.Vector2D(0.0, 0.0);
    const tmpVec = RS.Math.Vector2D();

    export class MegaWin extends Base
    {
        protected _mega:       RS.Rendering.IBitmap;
        protected _megaGlow:   RS.Rendering.IBitmap;
        protected _bonus:      RS.Rendering.IBitmap;
        protected _bonusGlow:  RS.Rendering.IBitmap;
        protected _win:        RS.Rendering.IBitmap;
        protected _winGlow:    RS.Rendering.IBitmap;

        protected _glowReveal: RS.Rendering.IBitmap;

        protected _bulgeShaderProgram: Shaders.BulgeShaderProgram;
        protected _bulgeShader: Shaders.BulgeShader;

        protected _maskShaderProgram: RS.Rendering.Shaders.MaskByLinesShaderProgram;
        protected _maskShader: RS.Rendering.Shaders.MaskByLinesShader;

        /**
         * Gets how long this tier text will take to play the exit animation.
        **/
        public get exitTime() { return 500; }

        /**
         * Gets the estimated total frame of this tier text.
         * Useful for centering.
        **/
        public get estimatedBounds()
        {
            const rect = new RS.Rendering.Rectangle();
            if (this._bonusMode)
            {
                rect.x = -this._bonus.texture.width;
                rect.w = this._bonus.texture.width + this._win.texture.width;
                rect.h = Math.max(this._bonus.texture.height, this._win.texture.height) + this._mega.texture.height;
                rect.y = rect.h * -0.5;
            }
            else
            {
                rect.x = -this._mega.texture.width;
                rect.w = this._mega.texture.width + this._win.texture.width;
                rect.h = Math.max(this._mega.texture.height, this._win.texture.height);
                rect.y = rect.h * -0.5;
            }
            return rect;
        }

        public constructor()
        {
            super();
            
            this._mega = RS.Factory.bitmap(Assets.BigWin.TierText.Mega);
            this._mega.name = "Mega";
            this.addChild(this._mega);

            this._megaGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Mega.Glow);
            this._megaGlow.name = "Mega Glow";
            this._megaGlow.blendMode = RS.Rendering.BlendMode.Add;
            this._mega.addChild(this._megaGlow);

            this._bonus = RS.Factory.bitmap(Assets.BigWin.TierText.Bonus);
            this._bonus.name = "Bonus";
            this._bonus.visible = false;
            this.addChild(this._bonus);

            this._bonusGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Bonus.Glow);
            this._bonusGlow.name = "Bonus Glow";
            this._bonusGlow.blendMode =  RS.Rendering.BlendMode.Add;
            this._bonus.addChild(this._bonusGlow);

            this._win = RS.Factory.bitmap(Assets.BigWin.TierText.Win);
            this._win.name = "Win";
            this.addChild(this._win);

            this._winGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Win.Glow);
            this._winGlow.name = "Win Glow";
            this._winGlow.blendMode =  RS.Rendering.BlendMode.Add;
            this._win.addChild(this._winGlow);

            this._glowReveal = RS.Factory.bitmap(Assets.BigWin.Glow);
            this._glowReveal.name = "Glow Reveal";
            this._glowReveal.blendMode =  RS.Rendering.BlendMode.Add;
            this.addChild(this._glowReveal);

            this._bulgeShaderProgram = new Shaders.BulgeShaderProgram();
            this._bulgeShader = this._bulgeShaderProgram.createShader();

            this._maskShaderProgram = new RS.Rendering.Shaders.MaskByLinesShaderProgram(1);
            this._maskShader = this._maskShaderProgram.createShader();

            this.state = State.None;
        }

        protected updateBulge(): void
        {
            // const pt = this._glowReveal.toGlobal(tmpPoint);

            if (this._bulgeShader)
            {
                //const localPt = new RS.Rendering.Point2D(this._glowReveal.x, this._glowReveal.y);
                //localPt.x *= 0.5;
                //if (this._bonusMode) { localPt.y = -50; }
                this._bulgeShader.bulgePoint = this._glowReveal.toGlobal(emptyVec, tmpVec);
            }

            if (this._maskShader)
            {
                // const localPt = pt.clone();
                const bounds = this.localBounds;
                
                const localPt = new RS.Rendering.Point2D(this._glowReveal.x - bounds.x, this._glowReveal.y - bounds.y);
                this._maskShader.setLineByDir(1, localPt, {x: 0, y: 1});
            }
        }

        /** Updates the animation state. */
        protected async onStateChanged()
        {
            switch (this._state)
            {
                case State.None:
                {
                    RS.Tween.removeTweens(this._mega);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._win);

                    this._mega.x = this._mega.y = 0;
                    this._mega.anchorX = 1.0;
                    this._mega.anchorY = 0.5;
                    this._mega.scaleX = this._mega.scaleY = 1.0;
                    
                    this._megaGlow.x = this._megaGlow.y = 0;
                    this._megaGlow.anchorX = 1.0;
                    this._megaGlow.anchorY = 0.5;
                    this._megaGlow.alpha = 0.0;
                    
                    this._bonus.x = this._bonus.y = 0;
                    this._bonus.anchorX = 1.0;
                    this._bonus.anchorY = 0.5;
                    this._bonus.scaleX = this._bonus.scaleY = 1.0;

                    this._bonusGlow.x = this._bonusGlow.y = 0;
                    this._bonusGlow.anchorX = 1.0;
                    this._bonusGlow.anchorY = 0.5;
                    this._bonusGlow.alpha = 0.0;

                    this._win.x = this._win.y = 0;
                    this._win.anchorX = 0.0;
                    this._win.anchorY = 0.5;
                    this._win.scaleX = this._win.scaleY = 1.0;

                    this._winGlow.x = this._winGlow.y = 0;
                    this._winGlow.anchorX = 0.0;
                    this._winGlow.anchorY = 0.5;
                    this._winGlow.alpha = 0.0;

                    this._glowReveal.x = this._glowReveal.y = 0;
                    this._glowReveal.anchorX = 0.5;
                    this._glowReveal.anchorY = 0.5;
                    this._glowReveal.alpha = 0.0;
                    
                    break;
                }
                case State.Entering:
                {
                    this.filters = [ new RS.Rendering.Filter(this._bulgeShader) ];

                    this._bulgeShader.bulgeRadius = 200;
                    this._bulgeShader.bulgeAmount = 20;

                    if (this._bonusMode)
                    {
                        this._bonus.anchorX = 1.0;
                        this._bonus.anchorY = 0.5;
                        this._bonus.shader = this._maskShader;
                        this._bonus.x = 0;
                        this._bonus.visible = true;

                        const bounds = this.estimatedBounds;

                        this._mega.anchorX = 0.5;
                        this._mega.anchorY = 1.5;
                        this._mega.shader = this._maskShader;
                        this._mega.x = bounds.x + bounds.w * 0.5;

                        this._mega.removeChild(this._megaGlow);
                        this._megaGlow.dispose();
                        this._megaGlow = null;

                        this._megaGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Mega.Glow2);
                        this._megaGlow.blendMode = RS.Rendering.BlendMode.Add;
                        this._megaGlow.alpha = 0.0;
                        this._mega.addChild(this._megaGlow);
                    }
                    else
                    {
                        this._mega.anchorX = 1.0;
                        this._mega.anchorY = 0.5;
                        this._mega.shader = this._maskShader;
                        this._mega.x = 0;
                    }

                    this._win.anchorX = 0.0;
                    this._win.anchorY = 0.5;
                    this._win.shader = this._maskShader;
                    this._win.x = 0;

                    this._glowReveal.x = -350;

                    await RS.Timeline.get([
                        RS.Tween.get(this._glowReveal)
                            .to({alpha: 1.0}, 200, RS.Ease.quadInOut)
                            .wait(600)
                            .to({alpha: 0.0}, 200, RS.Ease.quadInOut),
                        RS.Tween.get(this._glowReveal, {onChange: () => this.updateBulge()})
                            .to({x: 350}, 1250, RS.Ease.quadInOut),
                        RS.Tween.get(this._bulgeShader)
                            .wait(1250 - 400)
                            .to({bulgeAmount: 0}, 400, RS.Ease.linear)
                    ]);
                    this.state = State.Looping;

                    break;
                }
                case State.Looping:
                {
                    if (this._bulgeShader)
                    {
                        this.filters = null;
                        this._bulgeShader.dispose();
                        this._bulgeShader = null;
                    }
                    if (this._maskShader)
                    {
                        this._mega.shader = null;
                        this._win.shader = null;
                        if (this._bonus) { this._bonus.shader = null; }
                        this._maskShader.dispose();
                        this._maskShader = null;
                    }

                    if (this._bonusMode)
                    {
                        this._bonus.anchorX = this._bonus.anchorY = 0.5;
                        this._bonus.x = this._bonus.texture.width * -0.5;

                        this._bonusGlow.x = this._bonus.texture.width * 0.5;

                        this._mega.anchorX = this._mega.anchorY = 0.5;
                        this._mega.y = this._mega.height * -1.0;

                        this._megaGlow.anchorX = 0.5;
                        this._megaGlow.anchorY = 1.0;
                        this._megaGlow.y = this._mega.height * 0.5;
                    }
                    else
                    {
                        this._mega.x = this._mega.texture.width * -0.5;
                        this._mega.anchorX = this._mega.anchorY = 0.5;

                        this._megaGlow.x = this._mega.texture.width * 0.5;
                    }

                    this._win.x = this._win.texture.width * 0.5;
                    this._win.anchorX = this._win.anchorY = 0.5;

                    this._winGlow.x = this._win.texture.width * -0.5;

                    RS.Tween.get(this._mega, {loop: true})
                        .wait(this._bonusMode ? 500 : 750)
                        .to({scaleX: 1.1, scaleY: 1.1}, 500, RS.Ease.quadInOut)
                        .to({scaleX: 1.0, scaleY: 1.0}, 500, RS.Ease.quadInOut)
                        .wait(this._bonusMode ? 500 : 250);
                    
                    RS.Tween.get(this._megaGlow, {loop: true})
                        .wait(this._bonusMode ? 750 : 500)
                        .to({alpha: 1.0}, 500, RS.Ease.quadInOut)
                        .to({alpha: 0.0}, 500, RS.Ease.quadInOut)
                        .wait(this._bonusMode ? 250 : 500);
                    
                    RS.Tween.get(this._bonus, {loop: true})
                        .wait(750)
                        .to({scaleX: 1.1, scaleY: 1.1}, 500, RS.Ease.quadInOut)
                        .to({scaleX: 1.0, scaleY: 1.0}, 500, RS.Ease.quadInOut)
                        .wait(250);
                    
                    RS.Tween.get(this._bonusGlow, {loop: true})
                        .wait(750)
                        .to({alpha: 1.0}, 500, RS.Ease.quadInOut)
                        .to({alpha: 0.0}, 500, RS.Ease.quadInOut)
                        .wait(250);

                    RS.Tween.get(this._win, {loop: true})
                        .wait(this._bonusMode ? 1000 : 750)
                        .to({scaleX: 1.1, scaleY: 1.1}, 500, RS.Ease.quadInOut)
                        .to({scaleX: 1.0, scaleY: 1.0}, 500, RS.Ease.quadInOut)
                        .wait(this._bonusMode ? 0 : 250);
                    
                    RS.Tween.get(this._winGlow, {loop: true})
                        .wait(this._bonusMode ? 1000 : 750)
                        .to({alpha: 1.0}, 500, RS.Ease.quadInOut)
                        .to({alpha: 0.0}, 500, RS.Ease.quadInOut)
                        .wait(this._bonusMode ? 0 : 250);
                    
                    break;
                }
                case State.Exiting:
                {
                    if (this._bulgeShader)
                    {
                        this.filters = null;
                        this._bulgeShader.dispose();
                        this._bulgeShader = null;
                    }
                    if (this._maskShader)
                    {
                        this._mega.shader = null;
                        this._win.shader = null;
                        if (this._bonusMode) { this._bonus.shader = null; }
                        this._maskShader.dispose();
                        this._maskShader = null;
                    }

                    RS.Tween.removeTweens(this._mega);
                    RS.Tween.removeTweens(this._megaGlow);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._bonusGlow);
                    RS.Tween.removeTweens(this._win);
                    RS.Tween.removeTweens(this._winGlow);

                    RS.Tween.get(this._mega)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._megaGlow)
                        .to({alpha: 0.0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._bonus)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._bonusGlow)
                        .to({alpha: 0.0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._win)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._winGlow)
                        .to({alpha: 0.0}, 250, RS.Ease.quadIn);

                    break;
                }
            }
        }
    }
}