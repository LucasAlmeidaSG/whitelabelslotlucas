namespace Red7.BigWin.TierTexts
{
    export enum State { None, Entering, Looping, Exiting }

    /**
     * Base tier text animation.
    **/
    export abstract class Base extends RS.Rendering.DisplayObjectContainer
    {
        protected _state: State;
        protected _bonusMode: boolean;

        /**
         * Gets or sets the current animation state of this tier text.
        **/
        public get state() { return this._state; }
        public set state(value)
        {
            if (value === this._state) { return; }
            this._state = value;
            this.onStateChanged();
        }

        /**
         * Gets or sets if this tier text should be in bonus mode or not.
        **/
        public get bonusMode() { return this._bonusMode; }
        public set bonusMode(value) { this._bonusMode = value; }

        /**
         * Gets how long this tier text will take to play the exit animation.
        **/
        public get exitTime() { return 0; }

        /**
         * Gets the estimated total frame of this tier text.
         * Useful for centering.
        **/
        public get estimatedBounds() { return new RS.Rendering.Rectangle(0, 0, 0, 0); }

        /** Updates the animation state. */
        protected abstract onStateChanged(): void;
        
    }

}