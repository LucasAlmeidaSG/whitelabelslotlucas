/// <reference path="Base.ts" />
/// <reference path="../../shaders/WiggleShader.ts" />

namespace Red7.BigWin.TierTexts
{
    export class EpicWin extends Base
    {
        protected _epic: RS.Rendering.IAnimatedSprite;
        protected _bonus: RS.Rendering.IAnimatedSprite;
        protected _win: RS.Rendering.IAnimatedSprite;

        protected _wiggleShaderProgram: Shaders.WiggleShaderProgram;
        protected _wiggleShader: Shaders.WiggleShader;

        /**
         * Gets how long this tier text will take to play the exit animation.
        **/
        public get exitTime() { return 500; }

        /**
         * Gets the estimated total frame of this tier text.
         * Useful for centering.
        **/
        public get estimatedBounds()
        {
            const rect = new RS.Rendering.Rectangle();

            const epicFrame = this._epic.spriteSheet.getFrame(0);
            const winFrame = this._win.spriteSheet.getFrame(0);

            if (this._bonusMode)
            {
                const bonusFrame = this._bonus.spriteSheet.getFrame(0);

                rect.x = -960;
                rect.w = bonusFrame.untrimmedSize.w + winFrame.untrimmedSize.w;
                rect.h = Math.max(bonusFrame.untrimmedSize.h, winFrame.untrimmedSize.h) + epicFrame.untrimmedSize.h;
                rect.y = rect.h * -0.5 - 50;
            }
            else
            {
                rect.x = -epicFrame.untrimmedSize.w;
                rect.w = epicFrame.untrimmedSize.w + winFrame.untrimmedSize.w;
                rect.h = Math.max(epicFrame.untrimmedSize.h, winFrame.untrimmedSize.h);
                rect.y = rect.h * -0.5 - 50;
            }
            
            return rect;
        }

        public constructor()
        {
            super();
            
            this._epic = RS.Factory.sprite(Assets.BigWin.TierText.Epic.Fire);
            this._epic.framerate = 24;
            this.addChild(this._epic);

            this._bonus = RS.Factory.sprite(Assets.BigWin.TierText.Bonus.Fire);
            this._bonus.framerate = 24;
            this._bonus.visible = false;
            this.addChild(this._bonus);

            this._win = RS.Factory.sprite(Assets.BigWin.TierText.Win.Fire);
            this._win.framerate = 24;
            this.addChild(this._win);

            this._wiggleShaderProgram = new Shaders.WiggleShaderProgram();
            this._wiggleShader = this._wiggleShaderProgram.createShader();

            this.state = State.None;
        }

        /** Updates the animation state. */
        protected async onStateChanged()
        {
            switch (this._state)
            {
                case State.None:
                {
                    RS.Tween.removeTweens(this._epic);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._win);

                    this._bonus.x = this._bonus.y = 0;
                    this._bonus.scaleX = this._bonus.scaleY = 0;

                    this._epic.x = 0;
                    this._epic.y = -50;
                    this._epic.scaleX = this._epic.scaleY = 0;

                    this._win.x = 0;
                    this._win.y = -50;
                    this._win.scaleX = this._win.scaleY = 0;

                    break;
                }
                case State.Entering:
                {
                    this.filters = [new RS.Rendering.Filter(this._wiggleShader)];
                    this._wiggleShader.distortAmount = 0;
                    this._wiggleShader.distortScale = { x: 0.005, y: 0.0025 };
                    this._wiggleShader.distortSpeed = 0.5;

                    if (this._bonusMode)
                    {
                        this._epic.x = 90;
                        this._epic.y = -150;

                        this._bonus.x = -280;
                        this._bonus.y = 0;

                        this._win.x = 0;
                        this._win.y = 0;
                    }

                    this._epic.gotoAndPlay(Assets.BigWin.TierText.Epic.Fire.EpicFire);
                    if (this._bonusMode)
                    {
                        this._bonus.visible = true;
                        this._bonus.gotoAndPlay(Assets.BigWin.TierText.Bonus.Fire.EPIC_WIN);
                    }
                    this._win.gotoAndPlay(Assets.BigWin.TierText.Win.Fire.EPIC_WIN);

                    await RS.Timeline.get([
                        RS.Tween.get(this._epic)
                            .to({scaleX: 1.0, scaleY: 1.0}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._bonus)
                            .wait(200)
                            .to({scaleX: 1.0, scaleY: 1.0}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._win)
                            .wait(this._bonusMode ? 400 : 200)
                            .to({scaleX: 1.0, scaleY: 1.0}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._wiggleShader)
                            .to({distortAmount: this._bonusMode ? 3 : 5}, 750, RS.Ease.quadInOut)
                    ]);
                    this.state = State.Looping;

                    break;
                }
                case State.Looping:
                {                    
                    break;
                }
                case State.Exiting:
                {
                    RS.Tween.removeTweens(this._epic);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._win);

                    RS.Tween.get(this._epic)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._bonus)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._win)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);

                    if (this._wiggleShader)
                    {
                        await RS.Tween.get(this._wiggleShader)
                            .to({distortAmount: 0}, 750, RS.Ease.quadInOut);

                        this.filters.length = null;
                    }

                    break;
                }
            }
        }
    }
}