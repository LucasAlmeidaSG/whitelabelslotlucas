/// <reference path="Base.ts" />

namespace Red7.BigWin.TierTexts
{
    export class SuperWin extends Base
    {
        protected _super:     RS.Rendering.IBitmap;
        protected _superGlow: RS.Rendering.IBitmap;
        protected _bonus:     RS.Rendering.IBitmap;
        protected _bonusGlow: RS.Rendering.IBitmap;
        protected _win:       RS.Rendering.IBitmap;
        protected _winGlow:   RS.Rendering.IBitmap;

        /**
         * Gets how long this tier text will take to play the exit animation.
        **/
        public get exitTime() { return 500; }

        /**
         * Gets the estimated total frame of this tier text.
         * Useful for centering.
        **/
        public get estimatedBounds()
        {
            const rect = new RS.Rendering.Rectangle();
            if (this._bonusMode)
            {
                rect.x = -this._bonus.texture.width;
                rect.w = this._bonus.texture.width + this._win.texture.width;
                rect.h = Math.max(this._bonus.texture.height, this._win.texture.height) + this._super.texture.height;
                rect.y = rect.h * -0.5;
            }
            else
            {
                rect.x = -this._super.texture.width;
                rect.w = this._super.texture.width + this._win.texture.width;
                rect.h = Math.max(this._super.texture.height, this._win.texture.height);
                rect.y = rect.h * -0.5;
            }
            return rect;
        }

        public constructor()
        {
            super();
            
            this._super = RS.Factory.bitmap(Assets.BigWin.TierText.Super);
            this.addChild(this._super);

            this._superGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Super.Glow);
            this._superGlow.blendMode = RS.Rendering.BlendMode.Add;
            this._super.addChild(this._superGlow);

            this._bonus = RS.Factory.bitmap(Assets.BigWin.TierText.Bonus);
            this._bonus.visible = false;
            this.addChild(this._bonus);

            this._bonusGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Bonus.Glow);
            this._bonusGlow.blendMode = RS.Rendering.BlendMode.Add;
            this._bonus.addChild(this._bonusGlow);

            this._win = RS.Factory.bitmap(Assets.BigWin.TierText.Win);
            this.addChild(this._win);

            this._winGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Win.Glow);
            this._winGlow.blendMode = RS.Rendering.BlendMode.Add;
            this._win.addChild(this._winGlow);

            this.state = State.None;
        }

        /** Updates the animation state. */
        protected async onStateChanged()
        {
            switch (this._state)
            {
                case State.None:
                {
                    RS.Tween.removeTweens(this._super);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._win);

                    this._super.x = this._super.y = 0;
                    this._super.anchorX = 1.0;
                    this._super.anchorY = 0.5;
                    this._super.scaleX = this._super.scaleY = 0;

                    this._superGlow.x = this._superGlow.y = 0;
                    this._superGlow.anchorX = 1.0;
                    this._superGlow.anchorY = 0.5;
                    this._superGlow.alpha = 0.0;

                    this._bonus.x = this._bonus.y = 0;
                    this._bonus.anchorX = 1.0;
                    this._bonus.anchorY = 0.5;
                    this._bonus.scaleX = this._bonus.scaleY = 0;

                    this._bonusGlow.x = this._bonusGlow.y = 0;
                    this._bonusGlow.anchorX = 1.0;
                    this._bonusGlow.anchorY = 0.5;

                    this._win.x = this._win.y = 0;
                    this._win.anchorX = 0.0;
                    this._win.anchorY = 0.5;
                    this._win.scaleX = this._win.scaleY = 0;

                    this._winGlow.x = this._winGlow.y = 0;
                    this._winGlow.anchorX = 0.0;
                    this._winGlow.anchorY = 0.5;
                    this._winGlow.alpha = 0.0;

                    break;
                }
                case State.Entering:
                {
                    if (this._bonusMode)
                    {
                        this._bonus.anchorX = 1.0;
                        this._bonus.anchorY = 0.5;
                        this._bonus.x = 0;
                        this._bonus.visible = true;

                        const bounds = this.estimatedBounds;

                        this._super.anchorX = 0.5;
                        this._super.anchorY = 1.5;
                        this._super.x = bounds.x + bounds.w * 0.5;

                        this._super.removeChild(this._superGlow);
                        this._superGlow.dispose();
                        this._superGlow = null;

                        this._superGlow = RS.Factory.bitmap(Assets.BigWin.TierText.Super.Glow2);
                        this._superGlow.blendMode = RS.Rendering.BlendMode.Add;
                        this._super.addChild(this._superGlow);

                        // this._superGlow.x = this._super.getLocalBounds().width * 0.5;
                        this._superGlow.y = this._super.height * -0.5;
                        this._superGlow.anchorX = 0.5;
                        this._superGlow.scaleY = 1.0;
                        this._superGlow.alpha = 0.0;
                    }
                    else
                    {
                        this._super.anchorX = 1.0;
                        this._super.anchorY = 0.5;
                        this._super.x = 0;
                    }

                    this._win.anchorX = 0.0;
                    this._win.anchorY = 0.5;
                    this._win.x = 0;

                    await RS.Timeline.get([
                        RS.Tween.get(this._bonus)
                            .wait(this._bonusMode ? 200 : 0)
                            .to({scaleX: 1.1, scaleY: 1.1}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._bonusGlow)
                            .to({alpha: 1.0}, 750, RS.Ease.quadInOut),
                        RS.Tween.get(this._super)
                            .to({scaleX: 1.1, scaleY: 1.1}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._superGlow)
                            .to({alpha: 1.0}, 750, RS.Ease.quadInOut),
                        RS.Tween.get(this._win)
                            .wait(200)
                            .to({scaleX: 1.1, scaleY: 1.1}, 750, RS.Ease.backOut),
                        RS.Tween.get(this._winGlow)
                            .wait(200)
                            .to({alpha: 1.0}, 750, RS.Ease.quadInOut)
                    ]);
                    this.state = State.Looping;

                    break;
                }
                case State.Looping:
                {
                    if (this._bonusMode)
                    {
                        this._bonus.scaleX = this._bonus.scaleY = 0.5;
                        this._bonus.x = this._bonus.localBounds.w * -0.5 * 1.1;

                        this._bonusGlow.x = this._bonus.localBounds.w * 0.5;

                        // this._super.scaleX = this._super.scaleY = 0.5;
                        this._super.y = this._super.localBounds.h * -1.0 * 1.1;

                        this._superGlow.y = this._super.localBounds.h * 0.5;
                    }
                    else
                    {
                        // this._super.scaleX = this._super.scaleY = 0.5;
                        // this._super.x = this._super.localBounds.w * -0.5 * 1.1;

                        // this._superGlow.x = this._super.localBounds.w * 0.5;
                    }

                    // this._win.scaleX = this._win.scaleY = 0.5;
                    // this._win.x = this._win.localBounds.w * 0.5 * 1.1;

                    // this._winGlow.x = this._win.localBounds.w * -0.5;

                    if (this._bonusMode)
                    {
                        RS.Tween.get(this._bonus, {loop: true})
                            .to({scaleX: 1.0, scaleY: 1.0}, 800, RS.Ease.quadInOut)
                            .to({scaleX: 1.1, scaleY: 1.1}, 800, RS.Ease.quadInOut);
                        
                        RS.Tween.get(this._bonusGlow, {loop: true})
                            .to({alpha: 0.0}, 800, RS.Ease.quadInOut)
                            .to({alpha: 1.0}, 800, RS.Ease.quadInOut);    
                    }

                    RS.Tween.get(this._super, {loop: true})
                        .to({scaleX: 1.0, scaleY: 1.0}, 800, RS.Ease.quadInOut)
                        .to({scaleX: 1.1, scaleY: 1.1}, 800, RS.Ease.quadInOut);
                    
                    RS.Tween.get(this._superGlow, {loop: true})
                        .to({alpha: 0.0}, 800, RS.Ease.quadInOut)
                        .to({alpha: 1.0}, 800, RS.Ease.quadInOut);

                    RS.Tween.get(this._win, {loop: true})
                        .to({scaleX: 1.0, scaleY: 1.0}, 800, RS.Ease.quadInOut)
                        .to({scaleX: 1.1, scaleY: 1.1}, 800, RS.Ease.quadInOut);

                    RS.Tween.get(this._winGlow, {loop: true})
                        .to({alpha: 0.0}, 800, RS.Ease.quadInOut)
                        .to({alpha: 1.0}, 800, RS.Ease.quadInOut);
                    
                    break;
                }
                case State.Exiting:
                {
                    RS.Tween.removeTweens(this._super);
                    RS.Tween.removeTweens(this._superGlow);
                    RS.Tween.removeTweens(this._bonus);
                    RS.Tween.removeTweens(this._bonusGlow);
                    RS.Tween.removeTweens(this._win);
                    RS.Tween.removeTweens(this._winGlow);

                    RS.Tween.get(this._super)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._superGlow)
                        .to({alpha: 0.0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._bonus)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._bonusGlow)
                        .to({alpha: 0.0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._win)
                        .to({scaleX: 0, scaleY: 0}, 250, RS.Ease.quadIn);
                    RS.Tween.get(this._winGlow)
                        .to({alpha: 0.0}, 250, RS.Ease.quadIn);

                    break;
                }
            }
        }
    }
}