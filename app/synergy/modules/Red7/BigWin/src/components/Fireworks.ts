namespace Red7.BigWin
{
    /**
     * Fireworks generator and holder.
     */
    export class Fireworks extends RS.Rendering.DisplayObjectContainer implements IBigWinComponent
    {
        protected _settings: Fireworks.Settings;
        /**
         * Pool of fireworks to pull from.
         */
        protected _fireworksPool: RS.Rendering.IAnimatedSprite[] = [];
        /**
         * Next index to use for a firework.
         */
        protected _nextFireworkPoolIndex = 0;
        /**
         * Tier the big win has reached.
         */
        protected _tierReached: number;

        /**
         * @param spread - spread of the fireworks as a distance from the center of the screen.
         */
        public constructor(protected masterComponent: IBigWinComponent, settings?: Fireworks.Settings)
        {
            super("Big Win Fireworks");

            this._settings = settings || Fireworks.defaultSettings;

            for (let i = 0; i < this._settings.fireworkPoolSize; i++)
            {
                const coinSprite = RS.Factory.sprite(Assets.BigWin.Firework);
                coinSprite.gotoAndStop(Assets.BigWin.Firework.Firework);
                coinSprite.framerate = View.spriteFramerate;
                this._fireworksPool.push(coinSprite);
            }
        }

        /**
         * Starts the fireworks off.
         */
        public start(): void
        {
            this._tierReached = Tier.Big;
            this.fireworksLoop();
        }

        public onStarted(settings: BigWinSettings): void { return; }
        public onPreUpgrade(tier: Tier, after: number): void { return; }

        /**
         * Called when the big win is upgraded.
         */
        public onUpgrade(tier: Tier): void
        {
            this._tierReached = tier;
        }

        public onCountupProgress(value: number): void { return; }
        public onCountupComplete(): void { return; }
        public onFinished(): void { return; }
        public onSkip(): void { return; }

        /**
         * Stops generating fireworks.
         */
        public stop(): void
        {
            RS.Tween.removeTweens<Fireworks>(this);
        }

        /**
         * Generates a new firework, waits, then loops.
         */
        protected fireworksLoop(): void
        {
            const nextFirework = this._fireworksPool[this._nextFireworkPoolIndex];

            const variance: number = 250;

            let xVariance: number = variance;
            let yVariance: number = variance;

            if (this._settings.spread != null)
            {
                xVariance = this._settings.spread.w;
                yVariance = this._settings.spread.h;
            }

            nextFirework.x = RS.Math.generateRandomInt(-xVariance, xVariance);
            nextFirework.y = RS.Math.generateRandomInt(-yVariance, yVariance);

            this.addChild(nextFirework);
            nextFirework.gotoAndPlay(Assets.BigWin.Firework.Firework);

            const handler = () =>
            {
                nextFirework.onAnimationEnded.off(handler);
                //nextFirework.off("animationend", handler);
                this.removeChild(nextFirework);
            };
            nextFirework.onAnimationEnded(handler);
            //nextFirework.on("animationend", handler);

            this._nextFireworkPoolIndex++;
            if (this._nextFireworkPoolIndex >= this._settings.fireworkPoolSize)
            {
                this._nextFireworkPoolIndex = 0;
            }

            RS.Tween.get<Fireworks>(this)
                .wait(this._settings.fireworksDeltas[this._tierReached - 1])
                .call(() =>
                {
                    this.fireworksLoop();
                });
        }
    }

    export namespace Fireworks
    {
        export interface Settings
        {
            asset: RS.Asset.SpriteSheetReference | RS.Asset.RSSpriteSheetReference;
            animation: string;
            /** Number of firework sprites to pool. */
            fireworkPoolSize: number;
            /** Delta between fireworks being let off per tier */
            fireworksDeltas: number[];
            /** Spread (radius) from center of the screen to generate fireworks in. */
            spread?: RS.Dimensions;
        }

        export let defaultSettings: Settings =
        {
            asset: Assets.BigWin.Firework,
            animation: Assets.BigWin.Firework.Firework,
            fireworkPoolSize: 10,
            fireworksDeltas: [500, 400, 300, 200]
        };
    }
}
