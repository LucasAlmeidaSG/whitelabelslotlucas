namespace Red7.BigWin
{
    export enum Tier
    {
        None,
        Big,
        Super,
        Mega,
        Epic
    }

    export interface BigWinSettings
    {
        totalPayout: number;
        totalStake: number;
        tierMultiplier: number;
        bonusMode: boolean;
        currencyFormatter: RS.Localisation.CurrencyFormatter;
        locale: RS.Localisation.Locale;
        tierReached: Tier;
        viewSettings: View.Settings;
    }
}