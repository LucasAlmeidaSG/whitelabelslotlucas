namespace Red7.BigWin
{
    export interface IBigWinComponent
    {
        onStarted(settings: BigWinSettings): void;
        onPreUpgrade(tier: Tier, after: number): void;
        onUpgrade(tier: Tier, skipped: boolean): void;
        onCountupProgress(value: number): void;
        onCountupComplete(): void;
        onSkip(): void;
        onFinished(): void;
    }
}