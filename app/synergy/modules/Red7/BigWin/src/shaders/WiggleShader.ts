/// <reference path="../generated/Assets.ts" />

namespace Red7.BigWin.Shaders
{
    export class WiggleShaderProgram extends RS.Rendering.ShaderProgram
    {
        public constructor()
        {
            super({ fragmentSource: Assets.BigWin.Shaders.EpicWinWiggle.Fragment });
        }

        public createShader(): WiggleShader
        {
            return new WiggleShader(this);
        }
    }

    export class WiggleShader extends RS.Rendering.Shader
    {
        protected _distortAmount: number = 0;

        protected _uDistortAmount: RS.Rendering.ShaderUniform.Vec2;
        protected _uDistortScale: RS.Rendering.ShaderUniform.Vec2;
        protected _uDistortSpeed: RS.Rendering.ShaderUniform.Float;
        protected _uTime: RS.Rendering.ShaderUniform.Float;

        /**
         * Gets or sets the distort scale.
         */
        public get distortScale(): Readonly<RS.Math.Vector2D> { return this._uDistortScale.value; }
        public set distortScale(value)
        {
            this._uDistortScale.value = value;
        }

        /**
         * Gets or sets the distort amount.
         */
        public get distortAmount() { return this._distortAmount; }
        public set distortAmount(value)
        {
            this._distortAmount = value;
            RS.Math.Vector2D.set(this._uDistortAmount.value, value);
        }

        /**
         * Gets or sets the distort amount.
         */
        public get distortSpeed() { return this._uDistortSpeed.value; }
        public set distortSpeed(value)
        {
            this._uDistortSpeed.value = value;
        }

        public constructor(shaderProgram: WiggleShaderProgram)
        {
            super(shaderProgram);

            this._uDistortAmount = this.uniforms["uDistortAmount"] as RS.Rendering.ShaderUniform.Vec2;
            this._uDistortScale = this.uniforms["uDistortScale"] as RS.Rendering.ShaderUniform.Vec2;
            this._uDistortSpeed = this.uniforms["uDistortSpeed"] as RS.Rendering.ShaderUniform.Float;
            this._uTime = this.uniforms["uTime"] as RS.Rendering.ShaderUniform.Float;

            this._uDistortAmount.inputSizeOp = RS.Rendering.ShaderUniform.Op.Divide;

            RS.Ticker.registerTickers(this);
        }

        public dispose()
        {
            if (this.isDisposed) { return; }
            RS.Ticker.unregisterTickers(this);
            super.dispose();
        }

        @RS.Tick({ kind: RS.Ticker.Kind.Render })
        protected tick(ev: RS.Ticker.Event)
        {
            this._uTime.value = ev.runTime / 1000;
        }
    }
}