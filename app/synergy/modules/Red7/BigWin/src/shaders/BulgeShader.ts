/// <reference path="../generated/Assets.ts" />

namespace Red7.BigWin.Shaders
{
    export class BulgeShaderProgram extends RS.Rendering.ShaderProgram
    {
        public constructor()
        {
            super({ fragmentSource: Assets.BigWin.Shaders.MegaWinBulge.Fragment });
        }

        public createShader(): BulgeShader
        {
            return new BulgeShader(this);
        }
    }

    export class BulgeShader extends RS.Rendering.Shader
    {
        protected _bulgeAmount: number = 0;

        protected _uPoint: RS.Rendering.ShaderUniform.Vec2;
        protected _uBulgeRadius: RS.Rendering.ShaderUniform.Float;
        protected _uBulgeAmount: RS.Rendering.ShaderUniform.Vec2;

        /**
         * Gets or sets the bulge point position in world space.
         */
        public get bulgePoint(): Readonly<RS.Math.Vector2D> { return this._uPoint.value; }
        public set bulgePoint(value)
        {
            this._uPoint.value = value;
        }

        /**
         * Gets or sets the bulge radius.
         */
        public get bulgeRadius() { return this._uBulgeRadius.value; }
        public set bulgeRadius(value)
        {
            this._uBulgeRadius.value = value;
        }

        /**
         * Gets or sets the bulge amount.
         */
        public get bulgeAmount() { return this._bulgeAmount; }
        public set bulgeAmount(value)
        {
            this._bulgeAmount = value;
            RS.Math.Vector2D.set(this._uBulgeAmount.value, value, value);
        }

        public constructor(shaderProgram: BulgeShaderProgram)
        {
            super(shaderProgram);

            this._uPoint = this.uniforms["uPoint"] as RS.Rendering.ShaderUniform.Vec2;
            this._uBulgeRadius = this.uniforms["uBulgeRadius"] as RS.Rendering.ShaderUniform.Float;
            this._uBulgeAmount = this.uniforms["uBulgeAmount"] as RS.Rendering.ShaderUniform.Vec2;

            this._uPoint.targetSizeOp = RS.Rendering.ShaderUniform.Op.InverseY;
            this._uBulgeAmount.inputSizeOp = RS.Rendering.ShaderUniform.Op.Divide;
        }
    }
}