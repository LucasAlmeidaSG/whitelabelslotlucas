namespace Red7.BigWin
{
    @RS.HasCallbacks
    export class View extends RS.View.Base<View.Settings> implements IBigWinComponent
    {
        /**
         * Framerate for all sprites to play at.
         */
        public static readonly spriteFramerate: number = 24;

        /**
         * How long to keep the tick sound at full volume for.
         */
        private static readonly tickSoundFullVolumeTime = 5000;
        /**
         * How long to fade the tick sound down to low volume over.
         */
        private static readonly tickSoundFadeToLowTime = 8000;
        /**
         * Volume of the tick sound when in the background.
         */
        private static readonly tickSoundLowVolume = 0.3;

        private _innerContainer: RS.Rendering.IContainer;
        private _bigWinSettings: BigWinSettings;

        private readonly _onFinished = RS.createSimpleEvent();

        private _components: IBigWinComponent[] = [];

        /**
         * Countup (payout) text.
         */
        private _countupText: Countup;
        /**
         * Coin sprites.
         */
        private _coins: Coins;
        /**
         * Fades out the stuff behind the big win.
         */
        private _fadeBG: RS.Rendering.IGraphics;
        /**
         * Spotlight shapes.
         */
        private _spotlights: Spotlights;
        /**
         * Fireworks.
         */
        private _fireworks: Fireworks;
        /**
         * Current tier reached in big win.
         */
        private _tierText: TierText;
        /**
         * Stars instance.
         */
        private _stars: Stars;
        /**
         * Frame instance.
         */
        private _frame: Frame;
        /**
         * Container for particles behind the frame.
         */
        private _particlesContainerBack: RS.Rendering.IContainer = new RS.Rendering.DisplayObjectContainer("Particles Container Back");
        /**
         * Container for particles in front of the frame.
         */
        private _particlesContainerFront: RS.Rendering.IContainer = new RS.Rendering.DisplayObjectContainer("Particles Container Front");
        /**
         * Container for text.
         */
        private _textContainer: RS.Rendering.IContainer = new RS.Rendering.DisplayObjectContainer("Text Container");
        /**
         * Full screen button which allows the player to skip the big win.
         */
        private _skipButton: RS.DOM.Button;
        /**
         * Whether or not skip has already been requested.
         */
        private _skipped: boolean = true;

        private _globalVolume: number = 1.0;
        public get globalVolume() { return this._globalVolume; }
        public set globalVolume(value)
        {
            this._globalVolume = value;
            this.musicVolume = this.musicVolume;
            this.endSoundVolume = this.endSoundVolume;
            this.tickSoundVolume = this.tickSoundVolume;
        }

        /**
         * Music which plays during the big win.
         */
        private _bigWinMusic: RS.Audio.ISound;
        private _bigWinMusicVolume: number = 1.0;
        public get musicVolume() { return this._bigWinMusicVolume; }
        public set musicVolume(value)
        {
            this._bigWinMusicVolume = value;
            if (this._bigWinMusic)
            {
                this._bigWinMusic.volume = value * this._globalVolume;
            }
        }

        /**
         * Music which plays once the countup has completed.
         */
        private _bigWinEndSound: RS.Audio.ISound;
        private _bigWinEndSoundVolume: number = 1.0;
        public get endSoundVolume() { return this._bigWinEndSoundVolume; }
        public set endSoundVolume(value)
        {
            this._bigWinEndSoundVolume = value;
            if (this._bigWinEndSound)
            {
                this._bigWinEndSound.volume = value * this._globalVolume;
            }
        }

        /**
         * Tick up sound effect.
         */
        private _bigWinTickSound: RS.Audio.ISound;
        private _bigWinTickSoundVolume: number = 1.0;
        public get tickSoundVolume() { return this._bigWinTickSoundVolume; }
        public set tickSoundVolume(value)
        {
            this._bigWinTickSoundVolume = value;
            if (this._bigWinTickSound)
            {
                this._bigWinTickSound.volume = value * this._globalVolume;
            }
        }

        /**
         * The final tier we will reach when done.
         */
        private _finalTier: number;

        /** Called when this view has been opened. */
        public onOpened(): void
        {
            return;
        }

        /**
         * Skips the big win, going straight to the payout.
         */
        @RS.Callback
        public onSkip(): void
        {
            if (RS.ITicker.get().paused) { return; }
            if (this._skipped) { return; }
            this._skipped = true;

            for (const c of this._components)
            {
                c.onSkip();
            }

            this.onUpgrade(this._finalTier, true);
        }

        public onStarted(settings: BigWinSettings)
        {
            for (const c of this._components)
            {
                c.onStarted(settings);
            }
        }

        public onPreUpgrade(tier: Tier, after: number)
        {
            RS.Log.debug(`Big Win: onPreUpgrade(${Tier[tier]}, ${after})`);
            for (const c of this._components)
            {
                c.onPreUpgrade(tier, after);
            }
        }

        public onUpgrade(tier: Tier, skipped: boolean)
        {
            RS.Log.debug(`Big Win: onUpgrade(${Tier[tier]}, ${skipped})`);
            for (const c of this._components)
            {
                c.onUpgrade(tier, skipped);
            }
        }

        public onCountupProgress(value: number)
        {
            for (const c of this._components)
            {
                c.onCountupProgress(value);
            }
        }

        public onCountupComplete()
        {
            for (const c of this._components)
            {
                c.onCountupComplete();
            }
            this.onComplete();
        }

        public onFinished()
        {
            for (const c of this._components)
            {
                c.onFinished();
            }
        }

        /** Cleans up any components used by this view. */
        public dispose(): void
        {
            super.dispose();
            RS.Tween.removeTweens<View>(this);
        }

        public async doAnimation(bigWinSettings: BigWinSettings)
        {
            this._bigWinSettings = bigWinSettings;

            this.createStars();
            this.createTiers();
            this.createCoins();
            this.createFireworks();
            this.createCountupText();
            this.createFrame();
            this.createFade();
            this.createSpotlights();
            this.createSkipButton();

            this._bigWinMusic = RS.Audio.play(Assets.BigWin.Music);
            this.musicVolume = 1.0;

            this._bigWinTickSound = RS.Audio.play(Assets.BigWin.Tick);
            this._bigWinTickSound.loop = true;
            this._bigWinTickSound.volume = 0.0;
            RS.Tween.get<View>(this)
                .to({ tickSoundVolume: 1.0 }, this._bigWinSettings.viewSettings.fadeTime)
                .wait(View.tickSoundFullVolumeTime)
                .to({ tickSoundVolume: View.tickSoundLowVolume }, View.tickSoundFadeToLowTime);

            this.alpha = 0;

            this._innerContainer = new RS.Rendering.DisplayObjectContainer();
            this._innerContainer.addChildren(this._particlesContainerBack, this._frame, this._stars, this._particlesContainerFront, this._textContainer);
            this.addChildren(this._fadeBG, this._innerContainer, this._spotlights);

            this._innerContainer.scaleX = this._innerContainer.scaleY = this.settings.scaleFactor;

            this.onStageResized();

            this._tierText.start(bigWinSettings.bonusMode);
            this._coins.start();
            this._fireworks.start();
            this._stars.start();
            this._spotlights.start();
            this._frame.start();

            this._finalTier = bigWinSettings.tierReached;

            RS.Tween.get<View>(this)
                .to({ alpha: 1 }, this._bigWinSettings.viewSettings.fadeTime)
                .call(() =>
                {
                    this._countupText.start(bigWinSettings);
                    this._skipped = false;
                    this._skipButton.addToWrapper();
                });

            await this._onFinished;
        }

        // @RS.EventHandler(StageResizedEvent)
        @RS.Callback
        private onStageResized(): void
        {
            this.x = this.controller.width * 0.5;
            this.y = this.controller.height * 0.5;
        }

        /**
         * Called when the countup has completed.
         */
        // @RS.EventHandler(Events.CountupComplete)
        @RS.Callback
        private onComplete(): void
        {
            this._fireworks.stop();
            this._coins.stop();
            this._skipped = true;

            this._skipButton.parent = null;

            RS.Tween.get<View>(this, { override: true })
                .to({ tickSoundVolume: 0 }, this._bigWinSettings.viewSettings.fadeTime)
                .call((t) =>
                {
                    this._bigWinTickSound.dispose();
                    this._bigWinTickSound = null;
                });

            RS.Tween.get<View>(this)
                .wait(this._bigWinSettings.viewSettings.endWaitTime)
                .to({ alpha: 0 }, this._bigWinSettings.viewSettings.fadeTime)
                .call(() =>
                {
                    this._spotlights.stop();
                    this._stars.stop();
                    this._tierText.stop();
                    this._frame.stop();

                    this._onFinished.publish();
                    this.onFinished();
                });

            // end music
            RS.Tween.get<View>(this)
                .to({ musicVolume: 0 }, 500)
                .call(() =>
                {
                    this._bigWinMusic.dispose();
                    this._bigWinMusic = null;
                });

            this._bigWinEndSound = RS.Audio.play(Assets.BigWin.EndMusic);
            this.endSoundVolume = 1.0;
        }

        /**
         * Creates the two star sprites.
         */
        private createStars(): void
        {
            this._stars = new Stars(this);

            this._components.push(this._stars);
        }

        /**
         * Creates the tier text sprites.
         */
        private createTiers(): void
        {
            this._tierText = new TierText(this);
            this._tierText.y = -120;
            this._textContainer.addChild(this._tierText);

            this._components.push(this._tierText);
        }

        /**
         * Populates the coin pool.
         */
        private createCoins(): void
        {
            this._coins = new Coins(this);
            this._particlesContainerFront.addChild(this._coins.frontContainer);
            this._particlesContainerBack.addChild(this._coins.rearContainer);

            this._components.push(this._coins);
        }

        /**
         * Creates fireworks pool and component.
         */
        private createFireworks(): void
        {
            this._fireworks = new Fireworks(this, {...Fireworks.defaultSettings,
                spread:
                {
                    w: this.settings.dimensions.w / 4,
                    h: this.settings.dimensions.h / 4
                }
            });
            this._particlesContainerFront.addChild(this._fireworks);

            this._components.push(this._fireworks);
        }

        /**
         * Creates the countup text.
         */
        private createCountupText(): void
        {
            this._countupText = new Countup(this);
            this._textContainer.addChild(this._countupText);

            this._components.push(this._countupText);
        }

        /**
         * Creates the frame and the lights which go around it.
         */
        private createFrame(): void
        {
            this._frame = new Frame(this);

            this._components.push(this._frame);
        }

        /**
         * Creates a fade as big as the stage.
         */
        private createFade(): void
        {
            if (this._fadeBG)
            {
                return;
            }

            const canvasWidth = this.settings.dimensions.w;
            const canvasHeight = this.settings.dimensions.h;

            const graphics = this._fadeBG = new RS.Rendering.Graphics();
            graphics.beginFill(RS.Util.Colors.black, 0.75);
            graphics.drawRect(canvasWidth * -0.5, canvasHeight * -0.5, canvasWidth, canvasHeight);
            graphics.endFill();
        }

        /**
         * Creates the spotlights which scan the stage.
         */
        private createSpotlights(): void
        {
            if (this._spotlights !== undefined)
            {
                return;
            }

            this._spotlights = new Spotlights(this, this.settings.dimensions);
            this._spotlights.y = this.settings.dimensions.h / 1.8;

            this._components.push(this._spotlights);
        }

        /**
         * Creates the skip button to skip the big win.
         */
        private createSkipButton(): void
        {
            this._skipButton = new RS.DOM.Button();
            this._skipButton.addClass("rs-fullscreen");
            this._skipButton.onClicked(this.onSkip);
        }
    }

    export namespace View
    {
        export interface Settings extends RS.View.Base.Settings
        {
            dimensions: RS.Math.Size2D;
            scaleFactor: number;

            /** Time it takes to fade the big win in or out. */
            fadeTime?: number;

            /** Time to show the big win for before fading out. */
            endWaitTime?: number;

            /** Time between big win tiers, in milliseconds. After this time the big win will upgrade. */
            tierInterval: number;

            /** Maximum amount of time the big win can take in milliseconds. */
            maximumBigWinTime: number;

            /** Time to "tease" the next tier at end of each tier interval. */
            teaseTime: number;
        }

        export const defaultSettings: Settings =
        {
            dimensions: { w: 1024, h: 768 },
            scaleFactor: 1.0,
            fadeTime: 500,
            endWaitTime: 4000,
            tierInterval: 5000,
            maximumBigWinTime: 30000,
            teaseTime: 3000
        };
    }
}
