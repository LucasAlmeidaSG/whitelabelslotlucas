/// <reference path="TestXML.ts" />

namespace RS.Tests
{
    const { expect } = chai;

    describe("InitResponse.ts", function ()
    {
        it("should correctly parse a response", function ()
        {
            const xmlResponse = new DOMParser().parseFromString(glmMegaDropInitResponse, "text/xml");
            const response = new Red7.MegaDrop.GLM.InitResponse(null);
            RS.Serialisation.XML.deserialise(response, xmlResponse);

            expect(response).to.exist;
            expect(response.jackpotData).to.exist;

            const observed = JSON.parse(JSON.stringify(response.jackpotData));
            const expected = JSON.parse(glmMegaDropInitResponseParsed_JackpotData);

            expect(observed).to.deep.equal(expected);
        });

    });
}
