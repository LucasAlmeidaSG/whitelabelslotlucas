/// <reference path="TestXML.ts" />

namespace RS.Tests
{
    const { expect } = chai;

    describe("SpinRequest.ts", function ()
    {
        const mockJackpotData: Red7.MegaDrop.GLM.JackpotInstanceData[] = [];
        const mockJackpot1 = new Red7.MegaDrop.GLM.JackpotInstanceData();
        mockJackpot1.index = "1";
        mockJackpotData.push(mockJackpot1);
        const mockJackpot2 = new Red7.MegaDrop.GLM.JackpotInstanceData();
        mockJackpot2.index = "2345-23d4-786d-345g-a23h-jkdj";
        mockJackpotData.push(mockJackpot2);
        const mockJackpot3 = new Red7.MegaDrop.GLM.JackpotInstanceData();
        mockJackpot3.index = "3";
        mockJackpotData.push(mockJackpot3);

        it("should correctly serialise a request", function ()
        {
            const request = new Red7.MegaDrop.GLM.SpinRequest.Payload();
            request.jackpots = mockJackpotData;

            const doc = RS.Serialisation.XML.serialise(request);
            const observed = new XMLSerializer().serializeToString(doc);

            expect(observed).to.equal(glmMegaDropSpinRequest);
        });

        it("should correctly serialise a request with force data", function ()
        {
            const forceData = new Red7.MegaDrop.GLM.SpinRequest.ForceData();
            forceData.jackpots = [ new Red7.MegaDrop.GLM.PotData("MockJackpot1") ];

            const request = new Red7.MegaDrop.GLM.SpinRequest.Payload();
            request.jackpots = mockJackpotData;
            request.forceData = forceData;

            const doc = RS.Serialisation.XML.serialise(request);
            const observed = new XMLSerializer().serializeToString(doc);

            expect(observed).to.equal(glmMegaDropSpinRequestWithForceData);
        });

    });
}
