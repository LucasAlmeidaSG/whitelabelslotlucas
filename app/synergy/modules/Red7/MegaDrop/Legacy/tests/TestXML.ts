namespace RS.Tests
{
    export const glmMegaDropInitResponse: string =
`<GameResponse type="Init">
<AccountData>
    <init>
        <message>Success</message>
        <code>200</code>
        <session_id></session_id>
        <node_id></node_id>
        <pubsubtoken></pubsubtoken>
        <progressive>
            <id></id>
            <status>Running</status>
            <levels>
                <id>Jackpot1</id>
                <index>1</index>
                <seedpct></seedpct>
                <description></description>
                <currency>EUR</currency>
                <name></name>
                <contributepct></contributepct>
                <minbet></minbet>
            </levels>
            <levels>
                <id>Jackpot2</id>
                <index>2345-23d4-786d-345g-a23h-jkdj</index>
                <seedpct></seedpct>
                <description></description>
                <currency>EUR</currency>
                <name></name>
                <contributepct></contributepct>
                <minbet></minbet>
            </levels>
            <levels>
                <id>Jackpot3</id>
                <index>3</index>
                <seedpct></seedpct>
                <description></description>
                <currency>EUR</currency>
                <name></name>
                <contributepct></contributepct>
                <minbet></minbet>
            </levels>
            <description>xyz</description>
            <name>xyz</name>
            <kind>ACTIVE</kind>
            <currency/>
        </progressive>
    </init>
</AccountData>
</GameResponse>`;

    export const glmMegaDropInitResponseParsed_JackpotData: string = `{"instanceData":[{"id":"Jackpot1","index":"1","currency":"EUR"},{"id":"Jackpot2","index":"2345-23d4-786d-345g-a23h-jkdj","currency":"EUR"},{"id":"Jackpot3","index":"3","currency":"EUR"}],"status":"Running","kind":"ACTIVE","name":"xyz"}`;
    export const glmMegaDropSpinRequest: string = `<GameRequest type="Logic"><progressive><levels><index>1</index></levels><levels><index>2345-23d4-786d-345g-a23h-jkdj</index></levels><levels><index>3</index></levels></progressive><Stake total="undefined"/></GameRequest>`;

    export const glmMegaDropSpinRequestWithForceData: string = `<GameRequest type="Logic"><progressive><levels><index>1</index></levels><levels><index>2345-23d4-786d-345g-a23h-jkdj</index></levels><levels><index>3</index></levels></progressive><FORCE><AwardJackpots><Pot name="MockJackpot1"/></AwardJackpots></FORCE><Stake total="undefined"/></GameRequest>`;
}
