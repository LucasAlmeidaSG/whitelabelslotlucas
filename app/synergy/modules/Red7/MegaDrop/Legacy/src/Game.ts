namespace Red7.MegaDrop
{
    export class Game extends RS.Slots.Game
    {
        public readonly settings: Game.Settings;

        protected _models: Models;

        protected _context: Game.Context;
        protected _megaDropController: Red7.MegaDrop.IController;

        /** Gets the models for this game. */
        public get models() { return this._models; }

        /** Gets the context */
        public get context() { return this._context }

        /** Gets the megadrop controller for this game */
        public get megaDropController() { return this._megaDropController; }

        public constructor(settings: Game.Settings)
        {
            super(settings);
        }

    }

    export namespace Game
    {
        export interface Context extends RS.Slots.Game.Context
        {
            game: Game;
        }

        export interface Settings extends RS.Slots.Game.Settings
        {
            megaDropController: Red7.MegaDrop.IController.Settings;
        }
    }
}
