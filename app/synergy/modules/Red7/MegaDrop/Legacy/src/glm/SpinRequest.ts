/// <reference path="InitResponse.ts" />

namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export interface SpinRequestPayload extends SG.GLM.SpinRequestPayload
    {
        jackpotData: JackpotData;
        forceResult?: MegaDrop.Legacy.ForceResult;
    }

    export class SpinRequest extends SG.GLM.SpinRequest
    {
        protected createResponse(): SpinResponse
        {
            return new SpinResponse(this);
        }

        protected createRequest(payload: SpinRequestPayload): SpinRequest.Payload
        {
            const request = this.createResponseObject();
            request.header = this.createHeader();
            request.totalStake = payload.betAmount.value;
            request.paylineCount = payload.paylineCount;
            if (payload.forceResult)
            {
                request.forceData = this.createForceData();
                this.fillForceData(request.forceData, payload.forceResult);
            }

            if (payload.replay)
            {
                request.replayData = this.createReplayData();
                this.fillReplayData(request.replayData, payload.replay);
            }

            if (payload.currencyMultiplier != null)
            {
                request.accountData = new SG.GLM.Core.AccountData();
                request.accountData.currencyMultiplier = payload.currencyMultiplier;
            }

            if (payload.jackpotData)
            {
                request.jackpots = payload.jackpotData.instanceData.map((data) =>
                {
                    const newData = new JackpotInstanceData();
                    newData.index = data.index;
                    return newData;
                });
            }

            return request;
        }

        protected fillForceData(forceData: SpinRequest.ForceData, forceResult: MegaDrop.Legacy.ForceResult)
        {
            super.fillForceData(forceData, forceResult);

            if (forceResult && forceData)
            {
                if (forceResult.awardJackpots)
                {
                    forceData.jackpots = forceResult.awardJackpots.map((name) => new PotData(name));
                }

                if (forceResult.suppressAward || forceResult.suppressContrib)
                {
                    if (forceResult.suppressAward) { forceData.allowJackpots = false; }
                    if (forceResult.suppressContrib) { forceData.allowContribution = false; }
                }
            }
        }

        protected createResponseObject()
        {
            return new SpinRequest.Payload();
        }

        protected createForceData(): SpinRequest.ForceData
        {
            return new SpinRequest.ForceData();
        }
    }

    export namespace SpinRequest
    {
        export class ForceData extends SG.GLM.SpinRequest.ForceData
        {
            @XML.Property({ path: "AwardJackpots", type: XML.Array(XML.Object), ignoreIfNull: true })
            public jackpots?: PotData[];

            @XML.Property({ path: "jackpot .allowJackpots", type: XML.BooleanEx({ serialiseTrueAs: "1", serialiseFalseAs: "0" }), ignoreIfNull: true })
            public allowJackpots?: boolean;

            @XML.Property({ path: "jackpot .allowContribution", type: XML.BooleanEx({ serialiseTrueAs: "1", serialiseFalseAs: "0" }), ignoreIfNull: true })
            public allowContribution?: boolean;
        }

        @XML.Type("GameRequest")
        export class Payload extends SG.GLM.SpinRequest.Payload
        {
            @XML.Property({ path: "progressive", type: XML.Array(XML.ExplicitObject(JackpotInstanceData)) })
            public jackpots?: JackpotInstanceData[];

            @XML.Property({ path: "FORCE", type: XML.ExplicitObject(ForceData)})
            public forceData?: ForceData;
        }
    }
}
