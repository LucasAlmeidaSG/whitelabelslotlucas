/// <reference path="InitResponse.ts" />

namespace Red7.MegaDrop.GLM
{
    export class InitRequest extends SG.GLM.InitRequest
    {
        protected createResponse(): InitResponse
        {
            return new InitResponse(this);
        }
    }
}
