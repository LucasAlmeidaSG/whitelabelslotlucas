namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export class InitResponse extends SG.GLM.InitResponse
    {
        @XML.Property({ path: "AccountData init progressive", type: XML.ExplicitObject(JackpotData) })
        public jackpotData?: JackpotData;

        @XML.Property({ path: "GameInfo.jackpotRTP", type: XML.Number })
        public jackpotRTP?: number;

        @XML.Property({ path: "GameInfo.totalRTPWithJackpot", type: XML.Number })
        public totalRTPWithJackpot?: number;

        @XML.Property({ path: "JackpotThresholds", type: XML.ExplicitObject(JackpotThresholds) })
        public jackpotThresholds?: JackpotThresholds;

        @XML.Property({ path: "JackpotThresholds", type: XML.ExplicitObject(JackpotThresholdsLegacy) })
        public jackpotThresholdsLegacy?: JackpotThresholdsLegacy;

        @XML.Property({ path: "JackpotRecovery", type: XML.EmptyTag })
        public jackpotRecovery?: string;
    }
}
