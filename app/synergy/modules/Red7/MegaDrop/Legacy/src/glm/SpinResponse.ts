namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export class SpinResponse extends SG.GLM.SpinResponse
    {
        @XML.Property({ path: "AccountData Progressive", type: XML.ExplicitObject(BetJackpotData) })
        public betJackpotData?: BetJackpotData;
    }
}
