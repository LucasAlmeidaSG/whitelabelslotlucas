namespace Red7.MegaDrop.GLM
{
    /**
     * Encapsulates a slots engine interface for a GLM.
     */
    @RS.HasCallbacks
    export class Engine<TModels extends Models = Models> extends SG.GLM.Engine<TModels>
    {
        public get onMegaDropModelUpdated() { return this._megaDropComponent.onMegaDropModelUpdated; }
        public get onCurrencyCodeObtained() { return this._megaDropComponent.onCurrencyCodeObtained; }

        protected _recoveredReplay: boolean = false;

        protected _megaDropComponent: Component;

        public constructor(public readonly settings: Engine.Settings, public readonly models: TModels)
        {
            super(settings, models);
        }

        public async init()
        {
            const response = await super.init() as InitResponse;

            if (!response.isError &&
                (/**response.linkedData || */response.jackpotRecovery) &&
                this.models.state.state === RS.Models.State.Type.Closed &&
                !this.models.state.isReplay)
            {
                await this.hackyRecover();
            }
            if (!response.isError && this.models.state.isReplay)
            {
                const replayResponse = await this.spin({
                    betAmount: this.models.stake.betOptions[this.models.stake.currentBetIndex],
                    paylineCount: this.models.stake.paylineOptions[this.models.stake.currentPaylineIndex]
                });
                this.models.state.state = RS.Models.State.Type.Closed;
                this.models.state.isReplayComplete = false;
                this._recoveredReplay = true;
                return replayResponse;
            }
            return response;
        }

        public async spin(payload: RS.Slots.Engine.ISpinRequest.Payload)
        {
            // If it's the first spin of a recovered replay, skip it
            if (this._recoveredReplay)
            {
                this._recoveredReplay = false;
                this.models.state.state = RS.Models.State.Type.Open;
                this.models.state.isReplayComplete = this._replayEngine.replay.isComplete;
                return { ...SG.GLM.Core.errorResponse, isError: false };
            }

            // Are we in free spins?
            if (this.models.freeSpins.isFreeSpins && this._megaDropComponent.jackpotIsAwarded && !this.models.state.isMaxWin)
            {
                // Special case for free spins games: winning the jackpot closes the game...
                // so, we need to specifically send a close here and open a new session with a 1p stake
                // ...yeah, I know :(
                if (this.models.state.state !== RS.Slots.Models.State.Type.Closed)
                {
                    await this.close();
                }
                payload.betAmount = { type: RS.Slots.Models.Stake.Type.Total, value: 1 };
            }

            return await super.spin(payload);
        }

        protected createComponents(arr: RS.Engine.IComponent[])
        {
            this._megaDropComponent = new Component();
            arr.push(this._megaDropComponent);
        }

        /**
         * Parses an error.
         * If the jackpot has been claimed by another player, we parse the new instance IDs here.
         * @param error
         */
        protected parseError(error: SG.GLM.Error)
        {
            this._megaDropComponent.parseError(error, this.models);

            //save and restore, jackpot object gets mistaken for sessionID in core
            const sessionIDCache = this._userSettings.sessionID;

            super.parseError(error);

            this._userSettings.sessionID = sessionIDCache;
        }

        /**
         * Performs the hacky 1p wager recover.
         */
        protected async hackyRecover()
        {
            const response = await this.spin({
                betAmount: { type: RS.Slots.Models.Stake.Type.Total, value: 1 },
                paylineCount: this.models.stake.paylineOptions[this.models.stake.currentPaylineIndex]
            });
            return response;
        }

        /**
         * Returns whether or not an actual close request should be sent.
         */
        protected shouldMakeCloseRequest(): boolean
        {
            return super.shouldMakeCloseRequest() || (this._megaDropComponent.jackpotIsAwarded && this.models.freeSpins.isFreeSpins);
        }

        protected isEngineBalanceFrozen(gameResult: SG.GLM.GameResult): boolean
        {
            // Balance is frozen when not in real-money mode.
            if (this._isFreePlay) { return false; }

            // Balance frozen when max win hit.
            if (this.models.state.isMaxWin) { return false; }

            // Balance frozen during freespins UNLESS jackpot awarded
            if (this.models.freeSpins.remaining > 0) { return !this._megaDropComponent.jackpotIsAwarded; }

            // GLM balance has been updated
            return false;
        }
    }

    export namespace Engine
    {
        export interface Settings extends SG.GLM.Engine.Settings
        {
            initRequest: { new(networkSettings: SG.GLM.Engine.NetworkSettings, userSettings: SG.GLM.Engine.UserSettings): InitRequest; };
            spinRequest: { new(settings: SG.GLM.Engine.NetworkSettings, userSettings: SG.GLM.Engine.UserSettings): SpinRequest; };
        }
    }
}
