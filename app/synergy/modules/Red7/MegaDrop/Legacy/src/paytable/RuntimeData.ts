namespace Red7.MegaDrop.Legacy.Paytable
{
    export interface RuntimeData extends RS.Slots.Paytable.Pages.Base.RuntimeData
    {
        megaDropController: IController;
        megaDropConfig: Models.MegaDropConfig;
        exchangeRate: number;
        totalBet: number;
    }
}
