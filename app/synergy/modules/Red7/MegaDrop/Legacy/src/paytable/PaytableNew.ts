namespace Red7.MegaDrop.Legacy.Views
{
    @RS.HasCallbacks
    export class PaytableNew<TSettings extends PaytableNew.Settings = PaytableNew.Settings, TContext extends Game.Context = Game.Context> extends MegaDrop.Views.PaytableNew<TSettings, TContext>
    {
        protected getRuntimeData(): Legacy.Paytable.RuntimeData
        {
            return {
                ...super.getRuntimeData(),
                configModel: this.context.game.models.config,
                stakeModel: this.context.game.models.stake,
                totalBet: RS.Slots.Models.Stake.toTotal(this.context.game.models.stake, this.context.game.models.stake.betOptions[this.context.game.models.stake.currentBetIndex]).value
            };
        }
    }

    export namespace PaytableNew
    {
        export import Settings = MegaDrop.Views.PaytableNew.Settings;
    }
}
