namespace Red7.MegaDrop.Legacy.Views
{
    @RS.HasCallbacks
    export class Paytable<TSettings extends Paytable.Settings = Paytable.Settings, TContext extends Game.Context = Game.Context> extends MegaDrop.Views.Paytable<TSettings, TContext>
    {
        protected getRuntimeData(): Legacy.Paytable.RuntimeData
        {
            return {
                ...super.getRuntimeData(),
                configModel: this.context.game.models.config,
                stakeModel: this.context.game.models.stake,
                totalBet: RS.Slots.Models.Stake.toTotal(this.context.game.models.stake, this.context.game.models.stake.betOptions[this.context.game.models.stake.currentBetIndex]).value
            };
        }
    }

    export namespace Paytable
    {
        export import Settings = MegaDrop.Views.Paytable.Settings;
    }
}
