namespace Red7.MegaDrop.Legacy
{
    export interface ForceResult extends RS.Slots.Engine.ForceResult
    {
        awardJackpots?: string[];
        suppressAward?: boolean;
        suppressContrib?: boolean;
    }

    export type ForceData = RS.Map<RS.DevTools.Helpers.ItemSelector.Entry<RS.OneOrMany<ForceResult>>>;

    export import commonForceData = Red7.MegaDrop.commonForceData;
    export import commonForceDataLow = Red7.MegaDrop.commonForceDataLow;
}
