namespace Red7.MegaDrop
{
    export interface Models extends RS.Slots.Models
    {
        megaDropWin: number;
        megaDrop: Models.MegaDropJackpots;
        megaDropConfig: Models.MegaDropConfig;
    }

    export namespace Models
    {
        export function clear(models: Models)
        {
            RS.Slots.Models.clear(models);

            models.megaDrop = models.megaDrop || [] as Models.MegaDropJackpots;
            Models.MegaDropJackpots.clear(models.megaDrop);

            models.megaDropConfig = models.megaDropConfig || {} as Models.MegaDropConfig;
            Models.MegaDropConfig.clear(models.megaDropConfig);

            models.megaDropWin = 0;
        }
    }
}
