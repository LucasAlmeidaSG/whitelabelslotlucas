/// <reference path="models/MegaDropJackpots.ts" />
/// <reference path="elements/JackpotPanel.ts" />
/// <reference path="suspense/Suspense.ts" />

namespace Red7.MegaDrop
{
    import Flow = RS.Flow;

    export type StandardUIMode = LayoutDelegate;
    /**
     * Represents how the standard jackpot UI should be layouted.
     */
    export namespace StandardUIMode
    {
        /** Dock right, list top to bottom with background. */
        export const VerticalRight: LayoutDelegate = (container, panel) =>
        {
            container.dock = Flow.Dock.Right;
            container.contentAlignment = { x: 0.5, y: 0.0 };
            container.expand = Flow.Expand.VerticalOnly;
            panel.applyOptions(Elements.JackpotPanel.defaultVerticalPanelOptions);
        };

        /** Dock left, list top to bottom with background. */
        export const VerticalLeft: LayoutDelegate = (container, panel) =>
        {
            container.dock = Flow.Dock.Left;
            container.contentAlignment = { x: 0.5, y: 0.0 };
            container.expand = Flow.Expand.VerticalOnly;
            panel.applyOptions(Elements.JackpotPanel.defaultVerticalPanelOptions);
        };

        /** Dock top, list in triangle format with no background. */
        export const TriangleTop: LayoutDelegate = (container, panel) =>
        {
            container.dock = Flow.Dock.Top;
            container.contentAlignment = { x: 0.5, y: 0.5 };
            container.expand = Flow.Expand.Allowed;
            panel.applyOptions(
            {
                ...Elements.JackpotPanel.defaultVerticalPanelOptions,
                scaleFactor: 1.5,
                backgroundMode: false,
                triangleMode: true,
                horizontalLogo: true,
            });
        };

        /** Dock top, list top to bottom with no background */
        export const VerticalTop: LayoutDelegate = (container, panel) =>
        {
            container.dock = Flow.Dock.Top;
            container.contentAlignment = { x: 0.5, y: 0.5 };
            container.expand = Flow.Expand.Allowed;
            panel.applyOptions(
            {
                ...Elements.JackpotPanel.defaultVerticalPanelOptions,
                horizontalLogo: true,
                backgroundMode: false,
                scaleStagger: true
            });
        };

        /** Dock top, list left to righ with no background */
        export const HorizontalTop: LayoutDelegate = (container, panel) =>
        {
            container.dock = Flow.Dock.Top;
            container.contentAlignment = { x: 0.5, y: 0.5 };
            container.expand = Flow.Expand.Allowed;
            panel.applyOptions(Elements.JackpotPanel.defaultHorizontalPanelOptions);
        };

        /** Dock bottom, list left to righ with no background */
        export const HorizontalBottom: LayoutDelegate = (container, panel) =>
        {
            container.dock = Flow.Dock.Bottom;
            container.contentAlignment = { x: 0.5, y: 0.5 };
            container.expand = Flow.Expand.Allowed;
            panel.applyOptions(Elements.JackpotPanel.defaultHorizontalPanelOptions);
        };

        /** Sets jackpot panel visibility to false */
        export const Hidden: LayoutDelegate = (container, panel) =>
        {
            panel.visible = false;
        };
    }

    /**
     * Responsible for controlling the Mega Drop jackpot UI.
     */
    export interface IController<TSettings extends IController.Settings = IController.Settings> extends RS.Controllers.IController<TSettings>
    {
        readonly settings: TSettings;

        /** Published when the paytable has been requested by the user. */
        readonly onMegaDropAnimationStarted: RS.IEvent;
        /** Published once the megadrop currency code has been obtained. */
        readonly onMegaDropCurrencyCodeUpdated: RS.IEvent;

        /** Gets or sets the current mode of the standard jackpot UI. */
        standardUIMode: LayoutDelegate;

        /**
         * Gets or sets the container the win animation will render in.
         * If no container is provided the animation will render on the win animation view.
         */
        winAnimationContainer: RS.Flow.Container | null;

        /** Gets the currency formatter used to render the jackpots and drop by functions in EUR. */
        megaDropCurrencyFormatter: RS.Localisation.CurrencyFormatter;

        /** Gets whether or not the currency formatter is set or not */
        megaDropCurrencyFormatterSet: boolean;

        /**
         * Attaches the standard jackpot UI to the specified container.
         * @param container
         */
        attach(container: RS.Flow.GenericElement): void;

        /**
         * Removes the standard jackpot UI from any container.
         */
        detach(): void;

        /**
         * Attaches the suspense component to the specified container.
         * @param container
         */
        attachSuspense(container: RS.Flow.GenericElement): void;

        /**
         * Removes the suspense component from any container.
         */
        detachSuspense(): void;

        /**
         * Start suspense symbol drop
         * @param index
         */
        dropSuspenseSymbol(index: number): void;

        setupSuspense(betID: number, isWin: boolean, forceTease?: boolean): void;

        /**
         * Check if suspense rendering is finished
         */
        isSuspenseComplete(): Promise<void>;

        /**
         * Updates the jackpot UI from the specified model.
         * @param model
         */
        update(model: Models.MegaDropJackpots): void;

        /**
         * Recreates the background with given settings
         * @param settings
         */
        updatePanelBackground(settings: RS.Flow.Background.Settings): void;

        /**
         * Creates a jackpot UI element for the specified tier.
         * This can be used for custom animations involving a jackpot UI element.
         * The element will be automatically bound to the models.
         * @param overrideSettings
         */
        createJackpotElement(tier: Models.MegaDropJackpotTier, overrideSettings?: Partial<Elements.JackpotElement.Settings>): Elements.JackpotElement;

        /**
         * Creates and plays the winning view for Mega Drop.
         */
        playWinAnimation(jackpotName: string, jackpotWinnings: number): Promise<void>;

        /**
         * Calculate odds of winning a certain jackpot tier where 1 = 100%
         * @param tier
         * @param exchangeRate
         * @param stake
         */
        getJackpotOdds(tier: Models.MegaDropJackpotTier, exchangeRate: number, stake: number): number;
    }

    export type LayoutDelegate = (container: RS.Flow.Container, panel: Elements.JackpotPanel) => void;

    export const IController = RS.Controllers.declare<IController, IController.Settings>(RS.Strategy.Type.Instance);

    export namespace IController
    {
        export interface Settings
        {
            jackpotPanel: Elements.JackpotPanel.Settings;
            stateModel: RS.Models.State;
            winObservable: RS.Observable<number>;
            viewController: RS.View.IController;
            musicVolumeArbiter: RS.Arbiter<number>;
            JackpotWinTier: { [tier: string]: number };
            suspense: Suspense.Settings;
        }
    }

    export const defaultControllerSettings: IController.Settings =
    {
        jackpotPanel: Elements.JackpotPanel.defaultSettings,
        stateModel: null,
        winObservable: null,
        viewController: null,
        musicVolumeArbiter: null,
        JackpotWinTier:
        {
            "MegaJackpotEpic": 0,
            "MegaJackpotMajor": 1,
            "MegaJackpotMinor": 2,
            "MegaJackpotEpicLow": 0,
            "MegaJackpotMajorLow": 1,
            "MegaJackpotMinorLow": 2
        },
        suspense: Settings.Suspense.defaultSettings
    };
}
