namespace Red7.MegaDrop.Models
{
    export type Jackpots = { [type: number]: number };

    export interface MegaDropConfig
    {
        jackpotRTP: number;
        totalRTPWithJackpot: number;
        thresholds: Jackpots;
        thresholdsInRequestedExchangeRate: Jackpots;
        seedContributions: Jackpots;
        reseedContributions: Jackpots;
        contributionPerBet: number;
        currencyCode: string;
        baseToRequestedExchangeRate: number;
    }

    export namespace MegaDropConfig
    {
        export function clear(model: MegaDropConfig)
        {
            model.jackpotRTP = undefined;
            model.totalRTPWithJackpot = undefined;
            model.thresholds = model.thresholds || {} as Jackpots;
            model.thresholdsInRequestedExchangeRate = model.thresholdsInRequestedExchangeRate || {} as Jackpots;
            clearJackpots(model.thresholds);
            model.seedContributions = model.seedContributions || {} as Jackpots;
            clearJackpots(model.seedContributions);
            model.reseedContributions = model.reseedContributions || {} as Jackpots;
            clearJackpots(model.reseedContributions);
            model.contributionPerBet = undefined;
            model.baseToRequestedExchangeRate = undefined;
        }

        export function clearJackpots(jackpots: Jackpots)
        {
            for (const type in jackpots)
            {
                delete jackpots[type];
            }
        }
    }
}
