namespace Red7.MegaDrop
{
    export interface IModels extends RS.Models
    {
        megaDropWin: number;
        megaDropConfig: Models.MegaDropConfig;
        megaDrop: Models.MegaDropJackpots;
    }
}
