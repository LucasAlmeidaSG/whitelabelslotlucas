namespace Red7.MegaDrop.Models
{
    /**
     * Encapsulates a single mega drop jackpot.
     */
    export interface MegaDropJackpot
    {
        /** The name of the jackpot. */
        name: string;

        /** The index this jackpot relates to in the GCM. */
        index: string | number;

        /** The currency amount that the jackpot is initialised at. */
        seedValue: number;

        /** The currency amount that the jackpot is currently at. */
        currentValue: number;

        /** The currency amount at which the jackpot is guaranteed to drop in the base jackpot currency. */
        dropValue: number;

        /** The currency amount at which the jackpot is guaranteed to drop in the requested jackpot currency */
        dropValueInRequestedExchangeRate: number;

        /** If this jackpot has been won, set to true. */
        awarded: boolean;

        /** The actual value sent by the GLM when this jackpot was won. */
        actualWinValue: number;
    }

    export namespace MegaDropJackpot
    {
        export function clear(model: MegaDropJackpot)
        {
            model.name = undefined;
            model.index = "";
            model.seedValue = 0.0;
            model.currentValue = 0.0;
            model.dropValue = 0.0;
            model.dropValueInRequestedExchangeRate = 0.0;
            model.awarded = false;
            model.actualWinValue = 0.0;
        }

        export function clearWinData(model: MegaDropJackpot)
        {
            model.awarded = false;
            model.actualWinValue = 0.0;
        }
    }
}
