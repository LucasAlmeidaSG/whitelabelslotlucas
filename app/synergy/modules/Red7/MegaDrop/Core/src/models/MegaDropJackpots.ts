namespace Red7.MegaDrop.Models
{
    export enum MegaDropJackpotTier
    {
        Epic,
        Major,
        Minor
    }

    const numJackpots = Object.keys(MegaDropJackpotTier).length >> 1;

    /**
     * Encapsulates all three mega drop jackpots.
     */
    export type MegaDropJackpots = MegaDropJackpot[];

    export namespace MegaDropJackpots
    {
        export function clear(model: MegaDropJackpots)
        {
            model.length = numJackpots;
            for (let i = 0, l = numJackpots; i < l; ++i)
            {
                model[i] = model[i] || {} as MegaDropJackpot;
                MegaDropJackpot.clear(model[i])
            }
        }
    }
}