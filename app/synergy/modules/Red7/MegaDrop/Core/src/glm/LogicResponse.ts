/// <reference path="CommonData.ts" />

namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export class PotAwarded
    {
        @XML.Property({ path: ".potIdx", type: XML.Number })
        public potIndex: number;

        @XML.Property({ path: ".name", type: XML.String })
        public name: string;

        @XML.Property({ path: ".amount", type: XML.Number })
        public winAmount: number;
    }

    @XML.Type("Progressive")
    export class BetJackpotData extends JackpotData
    {
        @XML.Property({ path: "PotAwarded", type: XML.ExplicitObject(PotAwarded) })
        public results?: PotAwarded;
    }

    export interface ILogicResponse
    {
        betJackpotData: BetJackpotData;
    }
}
