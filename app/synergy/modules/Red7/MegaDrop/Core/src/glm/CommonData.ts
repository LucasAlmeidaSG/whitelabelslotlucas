namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export enum JackpotStatus
    {
        Running = "Running"
    }

    export enum JackpotKind
    {
        Active = "ACTIVE"
    }

    @XML.Type("levels")
    export class JackpotInstanceData
    {
        @XML.Property({ path: "id", type: XML.String, ignoreIfNull: true})
        public id?: string;

        @XML.Property({ path: "index", type: XML.String, ignoreIfNull: true })
        public index?: string;

        @XML.Property({ path: "currency", type: XML.String, ignoreIfNull: true })
        public currency?: string;
    }

    export class JackpotData
    {
        @XML.Property({ path: "*levels", type: XML.Array(XML.ExplicitObject(JackpotInstanceData)) })
        public instanceData: JackpotInstanceData[];

        @XML.Property({ path: "status", type: XML.String })
        public status: JackpotStatus;

        @XML.Property({ path: "kind", type: XML.String })
        public kind: JackpotKind;

        @XML.Property({ path: "name", type: XML.String })
        public name: string;
    }

    // #region Reference Response

    // Inside AccountData:
    /* #region
    <init>
        <message>Success</message>
        <code>200</code>
        <session_id></session_id>
        <node_id></node_id>
        <pubsubtoken></pubsubtoken>
        <progressive>
            <id></id>
            <status>Running</status>
            <levels>
                <id>Jackpot1</id>
                <index>1</index>
                <seedpct></seedpct>
                <description></description>
                <currency>EUR</currency>
                <name></name>
                <contributepct></contributepct>
                <minbet></minbet>
                <syndication>
                    <id>846f79e2-7a86-42f0-8564-d34dfdd4b0b9</id>
                    <name>TheWinningTeam1</name>
                    <threshold>900</threshold>
                </syndication>
            </levels>
            <levels>
                <id>Jackpot2</id>
                <index>2</index>
                <seedpct></seedpct>
                <description></description>
                <currency>EUR</currency>
                <name></name>
                <contributepct></contributepct>
                <minbet></minbet>
                <syndication>
                    <id>846f79e2-7a86-42f0-8564-d34dfdd4b0b9</id>
                    <name>TheWinningTeam1</name>
                    <threshold>900</threshold>
                </syndication>
            </levels>
            <levels>
                <id>Jackpot3</id>
                <index>3</index>
                <seedpct></seedpct>
                <description></description>
                <currency>EUR</currency>
                <name></name>
                <contributepct></contributepct>
                <minbet></minbet>
                <syndication>
                    <id>846f79e2-7a86-42f0-8564-d34dfdd4b0b9</id>
                    <name>TheWinningTeam1</name>
                    <threshold>900</threshold>
                </syndication>
            </levels>
            <description>xyz</description>
            <name>xyz</name>
            <kind>ACTIVE</kind>
            <currency/>
        </progressive>
    </init>
    */

    // #endregion
}
