/// <reference path="CommonData.ts" />

namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export class JackpotThresholds
    {
        @XML.Property({ path: ".values", type: XML.DelimitedArray(XML.Number, "|") })
        public values: number[];

        @XML.Property({ path: ".ids", type: XML.DelimitedArray(XML.String, "|") })
        public ids: string[];
    }

    export class JackpotThresholdsLegacy
    {
        @XML.Property({ path: ".epic", type: XML.Number })
        public epic: number;

        @XML.Property({ path: ".major", type: XML.Number })
        public major: number;

        @XML.Property({ path: ".minor", type: XML.Number })
        public minor: number;
    }

    export interface IInitResponse
    {
        //base
        accountData: SG.GLM.Core.AccountData;
        isError: boolean;

        //megadrop
        jackpotData: JackpotData;
        jackpotRTP: number;
        totalRTPWithJackpot: number;
        jackpotThresholds: JackpotThresholds;
        jackpotThresholdsLegacy: JackpotThresholdsLegacy;
        jackpotRecovery: string;
    }
}
