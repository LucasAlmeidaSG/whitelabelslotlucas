/// <reference path="CommonData.ts" />

namespace Red7.MegaDrop.GLM
{
    import XML = RS.Serialisation.XML;

    export interface LogicRequestPayload extends SG.GLM.Core.LogicRequestPayload
    {
        jackpotData: JackpotData;
        forceResult?: ForceResult;
    }

    @XML.Type("Pot")
    export class PotData
    {
        @XML.Property({ path: ".name", type: XML.String })
        public name: string;

        public constructor(name: string) { this.name = name; }
    }

    export class ForceData extends SG.GLM.Core.LogicRequest.ForceData
    {
        @XML.Property({ path: "AwardJackpots", type: XML.Array(XML.Object), ignoreIfNull: true })
        public jackpots?: PotData[];

        @XML.Property({ path: "jackpot .allowJackpots", type: XML.BooleanEx({ serialiseTrueAs: "1", serialiseFalseAs: "0" }), ignoreIfNull: true })
        public allowJackpots?: boolean;

        @XML.Property({ path: "jackpot .allowContribution", type: XML.BooleanEx({ serialiseTrueAs: "1", serialiseFalseAs: "0" }), ignoreIfNull: true })
        public allowContribution?: boolean;
    }

    @XML.Type("GameRequest")
    export class LogicRequest extends SG.GLM.Core.LogicRequest.Payload
    {
        @XML.Property({ path: "progressive*levels", type: XML.Array(XML.ExplicitObject(JackpotInstanceData)) })
        public jackpots?: JackpotInstanceData[];

        @XML.Property({ path: "FORCE", type: XML.ExplicitObject(ForceData)})
        public forceData: ForceData;
    }
}
