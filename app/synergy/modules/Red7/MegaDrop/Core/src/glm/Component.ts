/// <reference path="InitResponse.ts" />
/// <reference path="LogicRequest.ts" />
/// <reference path="LogicResponse.ts" />

namespace Red7.MegaDrop.GLM
{
    interface ErrorJackpotData
    {
        [jackpot: string]: { instanceId: string; }
    }

    const externalToInternalJackpotNameMap: RS.Map<string> =
    {
        "MEGAJACKPOTEPICLOW": "MegaJackpotEpic",
        "MEGAJACKPOTMAJORLOW": "MegaJackpotMajor",
        "MEGAJACKPOTMINORLOW": "MegaJackpotMinor",
        "MEGAJACKPOTEPIC": "MegaJackpotEpic",
        "MEGAJACKPOTMAJOR": "MegaJackpotMajor",
        "MEGAJACKPOTMINOR": "MegaJackpotMinor"
    };

    export class Component<
        TInitPayload extends object = {}, TInitResponse extends IInitResponse = IInitResponse,
        TLogicPayload extends LogicRequestPayload = LogicRequestPayload, TLogicResponse extends ILogicResponse = ILogicResponse,
        TClosePayload extends object = {}, TCloseResponse extends object = {},
        TModels extends IModels = IModels>
    implements RS.Engine.IComponent<
        TInitPayload, TInitResponse,
        TLogicPayload, TLogicResponse,
        TClosePayload, TCloseResponse,
        TModels>
    {
        protected _jackpotData: JackpotData | null = null;
        protected _jackpotIDs: (string | number)[] | null = null;
        protected _currencyInformation: SG.GLM.Core.CurrencyInformation;
        protected _jackpotIsAwarded: boolean = false;
        public get jackpotIsAwarded() { return this._jackpotIsAwarded; }

        private readonly _onMegaDropModelUpdated = RS.createSimpleEvent();
        public get onMegaDropModelUpdated() { return this._onMegaDropModelUpdated.public; }

        private readonly _onCurrencyCodeObtained = RS.createEvent<string>();
        public get onCurrencyCodeObtained() { return this._onCurrencyCodeObtained.public; }

        private _isDisposed: boolean;
        public get isDisposed() { return this._isDisposed; }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }

        public populateInitPayload(payload: TInitPayload): void
        {
            // nothing to add
        }

        public populateInitModels(response: TInitResponse, models: TModels): void
        {
            if (response.jackpotData)
            {
                this._jackpotData = response.jackpotData;
                try
                {
                    this.parseJackpotData(response.jackpotData, models);
                }
                catch (err)
                {
                    if (!response.isError)
                    {
                        models.error.fatal = true;
                        models.error.type = RS.Models.Error.Type.Client;
                        models.error.title = "Jackpots";
                        models.error.message = `${err}`;
                        models.error.options = [ { type: RS.Models.Error.OptionType.Refresh, text: "OK" } ];
                        throw err;
                    }
                }
            }

            if (response.jackpotThresholds &&
                response.jackpotThresholds.ids != undefined &&
                response.jackpotThresholds.values != undefined)
            {
                this.parseJackpotThreshold(response.jackpotThresholds, models);
            }
            else if (response.jackpotThresholdsLegacy &&
                response.jackpotThresholdsLegacy.epic != undefined &&
                response.jackpotThresholdsLegacy.major != undefined &&
                response.jackpotThresholdsLegacy.minor != undefined)
            {
                this.parseLegacyJackpotThreshold(response.jackpotThresholdsLegacy, models);
            }

            if (models.megaDrop.length > 0)
            {
                // Sort jackpots in order of jackpot threshold descending
                models.megaDrop.sort((a, b) => b.dropValue - a.dropValue);
                models.megaDropConfig.thresholds = models.megaDrop.map((m) => m.dropValue);
            }

            if (response.jackpotRTP != undefined)
            {
                models.megaDropConfig.jackpotRTP = response.jackpotRTP;
            }
            if (response.totalRTPWithJackpot != undefined)
            {
                models.megaDropConfig.totalRTPWithJackpot = response.totalRTPWithJackpot;
            }

            this.parseContributions(models);

            if (this._jackpotIDs)
            {
                const platform = RS.IPlatform.get();
                if (platform instanceof SG.Environment.Platform)
                {
                    if (response.accountData && response.accountData.currencyInformation)
                    {
                        this._currencyInformation = response.accountData.currencyInformation;
                        platform.initialiseJackpots(response.accountData.currencyInformation.currency, this._jackpotIDs);
                    }
                    else
                    {
                        // Probably free play
                        platform.initialiseJackpots("GBP", this._jackpotIDs);
                    }
                    const jackpotsHandler = platform.onJackpotsUpdated((data: SGI.JackpotData[]) => this.handleJackpotsUpdated(data, models));
                    RS.Disposable.bind(jackpotsHandler, this);
                    const jackpotsOnceHandler = platform.onJackpotsUpdated.once((data: SGI.JackpotData[]) => this.handleFirstJackpotUpdate(data, models));
                    RS.Disposable.bind(jackpotsOnceHandler, this);
                }
            }
        }

        public populateLogicPayload(payload: TLogicPayload): void
        {
            payload.jackpotData = this._jackpotData;
        }

        public populateLogicModels(response: TLogicResponse, models: TModels): void
        {
            this.clearJackpots(models);
            models.megaDropWin = 0;

            // Parse account data for progressive jackpot
            this._jackpotIsAwarded = false;
            if (response.betJackpotData)
            {
                // First, parse the win so we set the correct jackpots awarded to true
                // Second, parse new instance ID(s) from same logic response
                // The jackpot shouldn't need to have a reference to it's old ID to complete the win. the awarded boolean on it should do that job.
                if (response.betJackpotData.results)
                {
                    this._jackpotIsAwarded = true;
                    this.parseJackpotWin(response.betJackpotData.results, models);
                }

                if (!models.state.isReplay)
                {
                    this.updateJackpotData(response.betJackpotData, models);
                }
            }
        }

        public populateClosePayload(payload: TClosePayload): void
        {
            //
        }

        public populateCloseModels(response: TCloseResponse, models: TModels): void
        {
            //
        }

        public parseError(error: SG.GLM.Core.Error, models: TModels)
        {
            const jackpots = error.message.split("|")[3];
            if (jackpots != null)
            {
                const parsedData = this.parseErrorObject(jackpots);
                if (Object.keys(parsedData).length > 0)
                {
                    this.updateJackpotDataFromError(parsedData, models);
                }
            }
        }

        /**
         * Takes an external jackpot name and returns the internal synergy name
         * @param externalJackpotName
         */
        protected convertExternalJackpotName(externalJackpotName: string): string
        {
            const upperExternalJackpotName = externalJackpotName.toUpperCase();
            if (externalToInternalJackpotNameMap[upperExternalJackpotName])
            {
                return externalToInternalJackpotNameMap[upperExternalJackpotName];
            }
            else
            {
                RS.Log.warn("External jackpot name " + externalJackpotName + " is unsupported.");
                return externalJackpotName;
            }
        }

        protected parseErrorObject(str: string): ErrorJackpotData
        {
            const out: Partial<ErrorJackpotData> = {};

            // match anything not "{" "," or ":",
            // then match ":",
            // then match anything not "," or "}"
            const regExp = /([^:{,]+):([^,}:]+)(:([^,}:]+))?/g;
            for (let match = regExp.exec(str); match != null; match = regExp.exec(str))
            {
                const [, key, instance] = match;
                out[key] = { instanceId: instance };
            }
            return out as ErrorJackpotData;
        }

        protected updateJackpotDataFromError(errorData: ErrorJackpotData, models: TModels)
        {
            const curJackpotData = this._jackpotData;

            for (const jackpot of curJackpotData.instanceData)
            {
                jackpot.index = errorData[jackpot.id].instanceId;
            }

            this.updateJackpotData(curJackpotData, models);
        }

        protected updateJackpotData(jackpotData: JackpotData, models: TModels)
        {
            this._jackpotData = jackpotData;
            this._jackpotIDs = [];

            //check to see if the specified tier in the jackpot model has the same name ID as the specified new data
            //if so, update it's index to the new data's index
            for (const newData of jackpotData.instanceData)
            {
                this._jackpotIDs.push(newData.index);
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    const internalJackpotName = this.convertExternalJackpotName(newData.id);
                    if (internalJackpotName == models.megaDrop[tier].name)
                    {
                        models.megaDrop[tier].index = newData.index;
                        break;
                    }
                }
            }

            const platform = RS.IPlatform.get();
            if (platform instanceof SG.Environment.Platform)
            {
                platform.initialiseJackpots(this._currencyInformation.currency, this._jackpotIDs);
            }

            this._onMegaDropModelUpdated.publish();
        }

        protected parseContributions(models: TModels)
        {
            // Hard coded jackpot contributions, as we’re not sent this data from the GLM
            models.megaDropConfig.seedContributions[Models.MegaDropJackpotTier.Epic] = 0.300;
            models.megaDropConfig.seedContributions[Models.MegaDropJackpotTier.Major] = 0.525;
            models.megaDropConfig.seedContributions[Models.MegaDropJackpotTier.Minor] = 0.675;

            models.megaDropConfig.reseedContributions[Models.MegaDropJackpotTier.Epic] = 0.100;
            models.megaDropConfig.reseedContributions[Models.MegaDropJackpotTier.Major] = 0.175;
            models.megaDropConfig.reseedContributions[Models.MegaDropJackpotTier.Minor] = 0.225;

            models.megaDropConfig.contributionPerBet = 2.00;
        }

        protected parseJackpotThreshold(data: JackpotThresholds, models: TModels)
        {
            /* New jackpot ID related adding */
            for (let i = 0; i < models.megaDrop.length; ++i)
            {
                if (data.ids && data.values)
                {
                    const jackpotName = models.megaDrop[i].name;
                    for (let j = 0; j < data.ids.length; ++j)
                    {
                        const internalJackpotID = this.convertExternalJackpotName(data.ids[j]);
                        if (internalJackpotID == jackpotName)
                        {
                            models.megaDropConfig.thresholds[i] = data.values[j];
                            models.megaDrop[i].dropValue = data.values[j];
                            break;
                        }
                    }
                }
            }
        }

        protected parseLegacyJackpotThreshold(data: JackpotThresholdsLegacy, models: TModels)
        {
            /* Legacy search for attribs */
            models.megaDropConfig.thresholds[Models.MegaDropJackpotTier.Epic] = data.epic;
            models.megaDropConfig.thresholds[Models.MegaDropJackpotTier.Major] = data.major;
            models.megaDropConfig.thresholds[Models.MegaDropJackpotTier.Minor] = data.minor;

            for (let i = 0, l = models.megaDrop.length; i < l; ++i)
            {
                if (Constants.EpicJPNames.indexOf(models.megaDrop[i].name) > -1)
                {
                    models.megaDrop[i].dropValue = data.epic;
                }
                else if (Constants.MajorJPNames.indexOf(models.megaDrop[i].name) > -1)
                {
                    models.megaDrop[i].dropValue = data.major;
                }
                else if (Constants.MinorJPNames.indexOf(models.megaDrop[i].name) > -1)
                {
                    models.megaDrop[i].dropValue = data.minor;
                }
            }
        }

        protected parseJackpotData(data: JackpotData, models: TModels): void
        {
            if (data.kind !== JackpotKind.Active)
            {
                throw new Error(`Unexpected jackpot kind (got '${data.kind}', expected '${JackpotKind.Active}')`);
            }
            if (data.status !== JackpotStatus.Running)
            {
                throw new Error(`Unexpected jackpot kind (got '${data.status}', expected '${JackpotStatus.Running}')`);
            }
            if (data.instanceData.length !== 3)
            {
                throw new Error(`Unexpected jackpot data (got ${data.instanceData.length} jackpot instances, expected 3)`);
            }
            this._jackpotIDs = [];

            for (let tier = 0; tier < data.instanceData.length; tier++)
            {
                // if (this.models.customer.currencyCode !== "" && instanceData.currency !== this.models.customer.currencyCode)
                // {
                //     throw new Error(`Jackpot was a different currency to the current session (got '${instanceData.currency}', expected '${this.models.customer.currencyCode})'`);
                // }
                const internalJackpotName = this.convertExternalJackpotName(data.instanceData[tier].id);
                models.megaDrop[tier].name = internalJackpotName;
                models.megaDrop[tier].index = data.instanceData[tier].index;
                this._jackpotIDs.push(data.instanceData[tier].index);
            }
            this._onMegaDropModelUpdated.publish();
        }

        protected parseJackpotWin(potAwarded: PotAwarded, models: TModels, shouldMultiply: boolean = true)
        {
            //this number gets sent already with the decimal point where the currency would put it, eg 12534.24911414
            //to offset this so that our own currency formatter doesn't do this we multiply it by 100. above example would be 1253424.911414
            //jackpotWin in the GLM sends this normally though so we dont change it's format if we're using that
            const multiplier = shouldMultiply ? 100 : 1;

            const win = RS.Math.floor(RS.Math.shiftDecimal(potAwarded.winAmount, 2));
            models.megaDropWin = win;

            //if we're in replay the mega drop model won't be populated, but it'll still exist
            //therefore we can fill index 0 with the name and value awarded for the state to display
            if (models.state.isReplay)
            {
                const internalJackpotName = this.convertExternalJackpotName(potAwarded.name);
                models.megaDrop[potAwarded.potIndex].name = internalJackpotName;
                models.megaDrop[potAwarded.potIndex].awarded = true;
                models.megaDrop[potAwarded.potIndex].actualWinValue = win;
            }
            else
            {
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    const internalJackpotName = this.convertExternalJackpotName(potAwarded.name);
                    if (models.megaDrop[tier].name == internalJackpotName)
                    {
                        models.megaDrop[tier].awarded = true;
                        models.megaDrop[tier].actualWinValue = win;
                    }
                }
            }
        }

        @RS.Callback
        protected handleJackpotsUpdated(data: SGI.JackpotData[], models: TModels): void
        {
            // Loop through each new updated piece of data, find the model index that matches, and update it's current value
            // Model order and data order should be the exact same but just incase it isn't, this method ensures the correct model is updated
            for (const jackpotData of data)
            {
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    //stringify jackpot data in order to compare it to the string parsed model info
                    if (`${jackpotData.instance}` === models.megaDrop[tier].index)
                    {
                        models.megaDrop[tier].currentValue = RS.Math.floor(jackpotData.balanceAmountInRequestedCurrency * 100);
                        break;
                    }
                }
            }
            //this.models.megadropConfig.jackpotCurrency = data[0].jackpotCurrency;
            this._onMegaDropModelUpdated.publish();
        }

        @RS.Callback
        protected handleFirstJackpotUpdate(data: SGI.JackpotData[], models: TModels): void
        {
            //Set the currency code, only needs to be done once at the start of the game.
            const currencyCode = data[0].requestedCurrency;
            models.megaDropConfig.currencyCode = currencyCode;

            //Index 2 for Epic Jackpot because it is seeded and is never 0
            models.megaDropConfig.baseToRequestedExchangeRate = data[2].balanceAmountInRequestedCurrency / data[2].balanceAmountInJackpotCurrency;

            //Set up jackpot data for threshold in requested exchange rate
            for (const jackpotData of data)
            {
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    //stringify jackpot data in order to compare it to the string parsed model info
                    if (`${jackpotData.instance}` === models.megaDrop[tier].index)
                    {
                        models.megaDrop[tier].dropValueInRequestedExchangeRate = models.megaDropConfig.thresholds[tier] * models.megaDropConfig.baseToRequestedExchangeRate;
                        models.megaDropConfig.thresholdsInRequestedExchangeRate[tier] = models.megaDrop[tier].dropValueInRequestedExchangeRate;
                        break;
                    }
                }
            }

            this._onCurrencyCodeObtained.publish(currencyCode);
        }

        protected clearJackpots(models: TModels)
        {
            for (const jackpot of models.megaDrop)
            {
                Models.MegaDropJackpot.clearWinData(jackpot)
            }
        }
    }

    export namespace Component
    {
        export interface RuntimeData<TStakeModel extends RS.Models.Stake = RS.Models.Stake>
        {
            error: RS.Models.Error;
            stake: TStakeModel;
            state: RS.Models.State;
        }
    }
}
