namespace Red7.MegaDrop.Emitters
{
    /**
     * A particle emitter that emits particles inside a flow element's layout area.
     */
    export class Flow<TParticle extends RS.Particles.Base = RS.Particles.Base> extends RS.Particles.Emitter<TParticle, Flow.Settings<TParticle>>
    {
        protected _target: RS.Flow.GenericElement;

        /** Gets or sets the target flow element. */
        public get target() { return this._target; }
        public set target(value) { this._target = value; }

        public constructor(public readonly system: RS.Particles.System<TParticle>, public readonly settings: Flow.Settings<TParticle>, target: RS.Flow.GenericElement)
        {
            super(system, settings);
            this._target = target;
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): Flow
        {
            return new Flow(this.system, this.settings, this._target);
        }

        /**
         * Generates a new particle object.
         */
        protected generate(): TParticle
        {
            const p = super.generate();
            let localise = false;
            switch (this.settings.kind)
            {
                case Flow.Kind.Area:
                {
                    const b = this.settings.border || RS.Flow.Spacing.none;
                    const x = b.left, y = b.top, w = this._target.layoutSize.w - (b.left + b.right), h = this._target.layoutSize.h - (b.top + b.bottom);
                    p.x = this._rng.randomRange(x, x + w);
                    p.y = this._rng.randomRange(y, y + h);
                    localise = true;
                    break;
                }
                case Flow.Kind.Border:
                {
                    const side = this._rng.randomIntRange(0, 4);
                    const b = this.settings.border;
                    const x = 0, y = 0, w = this._target.layoutSize.w, h = this._target.layoutSize.h;
                    if (side === 0)
                    {
                        // Left
                        p.x = this._rng.randomRange(x, x + b.left);
                        p.y = this._rng.randomRange(y, y + h);
                    }
                    else if (side === 1)
                    {
                        // Top
                        p.x = this._rng.randomRange(x, x + w);
                        p.y = this._rng.randomRange(y, y + b.top);
                    }
                    else if (side === 2)
                    {
                        // Right
                        p.x = this._rng.randomRange(x + w - b.right, x + w);
                        p.y = this._rng.randomRange(y, y + h);
                    }
                    else if (side === 3)
                    {
                        // Bottom
                        p.x = this._rng.randomRange(x, x + w);
                        p.y = this._rng.randomRange(y + h - b.bottom, y + h);
                    }
                    localise = true;
                    break;
                }
            }
            if (localise)
            {
                const tmp = this.system.toLocal(p, this._target);
                p.x = tmp.x;
                p.y = tmp.y;
            }
            return p;
        }
    }

    export namespace Flow
    {
        export enum Kind { Area, Border }

        export interface BaseSettings<TKind extends Kind>
        {
            kind: TKind;
        }

        export interface AreaSettings extends BaseSettings<Kind.Area>
        {
            /** How much space around the edge of the layout area to exclude from particle emission. */
            border?: RS.Flow.Spacing;
        }

        export interface BorderSettings extends BaseSettings<Kind.Border>
        {
            /** The size of the border in which to emit particles. */
            border: RS.Flow.Spacing;
        }

        /**
         * Settings for a box emitter.
         */
        export type Settings<TParticle extends RS.Particles.Base> = RS.Particles.Emitter.Settings<TParticle> & (AreaSettings | BorderSettings);
    }
}
