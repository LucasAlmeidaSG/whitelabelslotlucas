/// <reference path="ScaleIncreaser.ts" />
namespace Red7.MegaDrop.JackpotWin
{
    export const TopParticles = RS.Particles.template()
        .addBehaviour(new RS.Particles.Behaviours.LinearMover())
        .addBehaviour(new RS.Particles.Behaviours.Tweener({
            propertyMap:
            {
                alpha: [
                    { fromAge: 0, fromValue: 1.0, toAge: 900, toValue: 0.0 },
                ]
            }
        }))
        .setInitialParticle({
            scaleX: 1.0, scaleY: 1.0,
            rotation: 0.0, rotVel: 0.0,
            alpha: 0.0,
            age: 0, maxAge: 900,
            regX: 0.5, regY: 0.5
        })
        .addParticleInitialiser((p) =>
        {
            p.asset = Assets.MegaDrop.Particles.TopCircle;
            p.yVel = RS.Math.linearInterpolate(150, 375, Math.random());
            p.scaleX = p.scaleY = RS.Math.linearInterpolate(0.1, 0.8, Math.random());
        });

    export const TopParticleEmitter: RS.Particles.Emitters.Box.Settings<RS.Particles.Base> =
    {
        emitCount: 18,
        emitTime: 100,
        rect: new RS.Rendering.Rectangle(0, 0, 2048, -50)
    };

    const ExplosionAnimationNames: string[] =
    [
        Assets.MegaDrop.Particles.ExplosionParticles.ChunkyParticle,
        Assets.MegaDrop.Particles.ExplosionParticles.RoundParticle
    ]

    const DefaultExplosionParticleSettings: RS.Particles.System.Settings<ScaleIncreaser.Particle> =
    {
        initialParticle: 
        {
            scaleX: 1.0, scaleY: 1.0,
            age: 0, maxAge: 1600,
            alpha: 1.0,
            regX: 0.5, regY: 0.5,
        },
        maxParticleCount: 300,
        initialisers:[],
        behaviours:
        [
            new RS.Particles.Behaviours.QuadraticCurveMover(),
            new ScaleIncreaser({
                fromAge: 0,
                toAge: 1600,
                scaleAdditioner: 0.8
            })
        ]
    }

    const particleAge = RS.Math.Range(1000, 1700);

    export const ExplosionParticleSystems: RS.Particles.System.Settings<ScaleIncreaser.Particle>[] =
    [
        {
            ...DefaultExplosionParticleSettings,
            initialisers:
            [
                (p) =>
                {
                    const endPointX = RS.Math.Range(-1100, -600);
                    const endPointY = RS.Math.Range(300, 900);
                    const controlPointY = RS.Math.Range(350, 1500);
                    p = InitialiseExplosionParticle(p, endPointX, endPointY, controlPointY);
                }
            ]
        },
        {
            ...DefaultExplosionParticleSettings,
            initialisers:
            [
                (p) =>
                {   
                    const endPointX = RS.Math.Range(-600, 600);
                    const endPointY = RS.Math.Range(300, 900);
                    const controlPointY = RS.Math.Range(300, 1700);
                    p = InitialiseExplosionParticle(p, endPointX, endPointY, controlPointY);
                }
            ]
        },
        {
            ...DefaultExplosionParticleSettings,
            initialisers:
            [
                (p) =>
                {   
                    const endPointX = RS.Math.Range(1100, 600);
                    const endPointY = RS.Math.Range(300, 900);
                    const controlPointY = RS.Math.Range(350, 1500);
                    p = InitialiseExplosionParticle(p, endPointX, endPointY, controlPointY);
                }
            ]
        }
    ]
    export const ExplosionParticleEmitters: RS.Particles.Emitters.Box.Settings<RS.Particles.Base>[] =
    [
        {
            emitCount: 300,
            rect: new RS.Rendering.Rectangle(50, 700, 500, 100)
        },
        {
            emitCount: 320,
            rect: new RS.Rendering.Rectangle(250, 700, 600, 100)
        },
        {
            emitCount: 300,
            rect: new RS.Rendering.Rectangle(450, 700, 500, 100)
        }
    ];

    function InitialiseExplosionParticle(p: ScaleIncreaser.Particle, endPointX: RS.Math.Range.MinMaxRange, endPointY: RS.Math.Range.MinMaxRange, controlPointY: RS.Math.Range.MinMaxRange): ScaleIncreaser.Particle
    {
        p.asset = Assets.MegaDrop.Particles.ExplosionParticles;
        p.animationName = ExplosionAnimationNames[RS.Math.floor(RS.Math.random() * ExplosionAnimationNames.length)];
        p.startPt = {x: p.x, y: p.y};
        p.endPt = {x: p.startPt.x + (RS.Math.random() * (endPointX.max - endPointX.min) + endPointX.min), y: p.startPt.y + (RS.Math.random() * (endPointY.max - endPointY.min) + endPointY.min)};
        p.controlPt = {x: (p.startPt.x + p.endPt.x)/2, y: p.startPt.y - (RS.Math.random() * (controlPointY.max - controlPointY.min) + controlPointY.min)}
        p.startScale = RS.Math.linearInterpolate(0.01, 0.4, Math.random());
        p.rotation = RS.Math.random() * RS.Math.Angles.fullCircle;
        p.maxAge = RS.Math.random() * (particleAge.max - particleAge.min) + particleAge.min;

        return p;
    }
}