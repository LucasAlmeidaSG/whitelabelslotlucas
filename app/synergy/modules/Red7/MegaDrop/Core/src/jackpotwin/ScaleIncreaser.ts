namespace Red7.MegaDrop.JackpotWin
{
    /**
     * A behaviour that moves particles based on cartesian velocity properties.
     */
    export class ScaleIncreaser extends RS.Particles.Behaviour<RS.Particles.Base>
    {
        @RS.AutoDispose protected _settings: ScaleIncreaser.Settings;

        public constructor(settings: ScaleIncreaser.Settings)
        {
            super(settings);
            this._settings = settings;
        }

        /**
         * Returns a copy of this behaviour
         */
        public clone(): ScaleIncreaser
        {
            return new ScaleIncreaser(this._settings);
        }

        /**
         * Initialises the specified particle.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public setupParticle(p: ScaleIncreaser.Particle): RS.Particles.Action
        {
            // No action
            return RS.Particles.Action.None;
        }

        /**
         * Updates the specified particle over the specified timeframe.
         * @param p        The particle to initialise.
         * @returns        The action to take.
         */
        public updateParticle(p: ScaleIncreaser.Particle, deltaTime: number): RS.Particles.Action
        {
            const endScale = p.startScale + this._settings.scaleAdditioner;

            const ageRange = this._settings.toAge - this._settings.fromAge;
            const valueRange = endScale - p.startScale;

            if (p.age >= this._settings.fromAge && p.age <= this._settings.toAge)
            {
                let delta = (p.age - this._settings.fromAge) / ageRange;
                delta = delta < 0 ? 0 : delta > 1 ? 1 : delta;

                p.scaleX = valueRange * delta + p.startScale;
                p.scaleY = valueRange * delta + p.startScale;
            }

            // No action
            return RS.Particles.Action.None;
        }
    }
    export namespace ScaleIncreaser
    {
        export interface Settings extends RS.Particles.Behaviour.Settings
        {
            fromAge: number;
            toAge: number;
            scaleAdditioner: number;
        }

        export interface Particle extends RS.Particles.Behaviours.QuadraticCurveMover.Particle
        {
            startScale: number;
        }
    }
}
