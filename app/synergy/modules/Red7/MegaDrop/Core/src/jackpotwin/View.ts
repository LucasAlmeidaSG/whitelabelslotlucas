namespace Red7.MegaDrop.JackpotWin
{
    enum WeightState
    {
        Waiting,
        Dropping,
        Dropped
    }

    export class View extends RS.View.Base<View.Settings, RS.Game.Context>
    {
        @RS.AutoDispose protected _musicHandle: RS.Audio.ISound;
        protected _musicVolume: number = 0.7;

        @RS.AutoDispose protected _topParticleSystem: RS.Particles.System<RS.Particles.Base>;
        @RS.AutoDispose protected _topParticleEmitter: RS.Particles.Emitter;
        @RS.AutoDispose protected _explosionParticleSystems: RS.Particles.System<RS.Particles.Base>[];
        @RS.AutoDispose protected _explosionParticleEmitters: RS.Particles.Emitter[];

        protected _mainContainer: RS.Flow.Container = null;
        protected _viewController: RS.View.IController;
        @RS.AutoDispose protected _flareContainer: RS.Flow.Container;

        @RS.AutoDispose protected _skipButton: RS.DOM.Button;
        @RS.AutoDispose protected _countupText: JackpotWin.Countup;
        @RS.AutoDispose protected _weight: RS.Flow.Image;
        protected _weightState: WeightState = WeightState.Waiting;
        @RS.AutoDispose protected _tierText: RS.Flow.Label;
        @RS.AutoDispose protected _lightFlares: RS.Flow.Image[];

        protected _jackpotWinSettings: View.JackpotWinSettings;

        protected _skipped: boolean = true;
        protected readonly _onFinished = RS.createSimpleEvent();

        protected _isEpic: boolean = false;

        @RS.AutoDispose protected _drumsSound: RS.IDisposable;
        @RS.AutoDispose protected _chimes: RS.IDisposable;
        @RS.AutoDispose protected _choir: RS.IDisposable;
        @RS.AutoDispose protected _epicSound: RS.IDisposable;
        @RS.AutoDispose protected _countUpEndSound: RS.IDisposable;

        public onOpened(): void
        {
            super.onOpened();
            this.currencyFormatter = this.context.game.currencyFormatter;
            this.locale = this.context.game.locale;
        }

        /**
         * Creates the Jackpot Win.
         */
        public setUpComponents(winSettings: View.JackpotWinSettings, mainContainer?: RS.Flow.Container): void
        {
            this._jackpotWinSettings = winSettings;
            if (mainContainer) { this._mainContainer = mainContainer };

            this.createFlareContainer();
            this.createTopParticles();
            this.createLightFlares();

            this.createWeight();
            this.createExplosionParticles();
            this.createJackpotTierText();
            this.createCountupText();

            this.handleOrientationChanged(RS.Orientation.currentOrientation);
        }

        public async start(viewController: RS.View.IController)
        {
            this._skipped = false;
            this._viewController = viewController;

            this.invalidateLayout();
            if (this._mainContainer) { this._mainContainer.invalidateLayout(); }

            if (!this._jackpotWinSettings.isReplay)
            {
                this.createSkipButton();
            }

            RS.Ticker.registerTickers(this);

            this.playIntro();

            await this._onFinished;
        }

        /**
         * Skips the jackpot win, going straight to the payout.
         */
        @RS.Callback
        public onSkip(): void
        {
            if (this._skipped) { return; }
            this._skipped = true;

            if (this._flareContainer && !this._flareContainer.isDisposed)
            {
                RS.Tween.removeTweens(this._flareContainer);
            }
            if (this._topParticleSystem && !this._topParticleSystem.isDisposed)
            {
                RS.Tween.removeTweens(this._topParticleSystem)
            }
            if (this._weight && !this._weight.isDisposed)
            {
                RS.Tween.removeTweens(this._weight);
            }
            if (this._tierText && !this._tierText.isDisposed)
            {
                RS.Tween.removeTweens(this._tierText);
            }

            this._weight.floatPosition = { x: 0.5, y: this.settings.weightSettings.endFloatY }
            this._weightState = WeightState.Dropped;
            this._tierText.scaleFactor = 1;

            this._flareContainer.alpha = 1;
            this._topParticleSystem.alpha = 1;

            for (const view of this._viewController.viewStack)
            {
                view.screenShakeIntensity = 0;
            }

            this.onComplete();
            if (this._countupText != null)
            {
                this._countupText.onSkip();
            }
        }

        public onFinished(): void
        {
            if (this._countupText != null)
            {
                this._countupText.onFinished();
            }
        }

        protected async playIntro()
        {
            if (this._skipped) { return; }

            this._drumsSound = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.WinDrums);

            RS.Tween.get(this._flareContainer)
                .to({ alpha: 1 }, this.settings.topEffectsFadeInTime)
            RS.Tween.get(this._topParticleSystem)
                .to({ alpha: 1 }, this.settings.topEffectsFadeInTime)

            await RS.Tween.get(this._weight)
                .wait(this.settings.chimesDelay)
                .call( () => { this._chimes = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.WinChimes); })
                .wait(this.settings.choirDelay)
                .call( () => { this._choir = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.WinChoir); })
                .wait(this.settings.introPauseTime)
                .call( () => { this._weightState = WeightState.Dropping; })
                .to({ floatPosition: { x: 0.5, y: this.settings.weightSettings.endFloatY } }, this.settings.weightSettings.tweenTime)
                .call( () =>
                {
                    this.performScreenShake();
                    for (const emitter of this._explosionParticleEmitters)
                    {
                        emitter.emit(emitter.emitCount);
                    }

                    this._weightState = WeightState.Dropped;
                })
                .wait(this.settings.weightPauseTime)

            if (this._skipped) { return; }

            this.playEpicSound();

            await RS.Tween.get(this._tierText)
                .to({ scaleFactor: 1}, this.settings.tierTweenInTime, RS.Ease.getBackOut(3))
                .call(() =>
                {
                    if (this._countupText != null)
                    {
                        this._countupText.startCountup();
                    }
                });
        }

        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation): void
        {
            super.handleOrientationChanged(newOrientation);
            if (newOrientation === RS.DeviceOrientation.Portrait)
            {
                this.setupOrientation(this.settings.portraitSettings);
            }
            else
            {
                this.setupOrientation(this.settings.landscapeSettings);
            }
        }

        protected setupOrientation(settings: View.OrientationSettings): void
        {
            if (this._weightState == WeightState.Waiting)
            {
                this._weight.floatPosition = settings.weightStartPos;
            }
            else if (this._weightState == WeightState.Dropping)
            {
                RS.Tween.removeTweens(this._weight);
                this._weight.floatPosition = {x: 0.5, y: this.settings.weightSettings.endFloatY};
                this.performScreenShake();
            }

            this._flareContainer.floatPosition = settings.lightFlarePos;
        }

        /**
         * Called when the countup has completed.
         */
        @RS.Callback
        protected onComplete(): void
        {
            this._skipped = true;

            if (this._skipButton)
            {
                this._skipButton.parent = null;
            }

            RS.Tween.get(this._flareContainer)
                .wait(this.settings.endWaitTime)
                .to({ alpha: 0 }, this.settings.endTweenOutTime)
            RS.Tween.get(this._topParticleSystem)
                .wait(this.settings.endWaitTime)
                .to({ alpha: 0 }, this.settings.endTweenOutTime)

            RS.Tween.get(this._weight)
                .wait(this.settings.endWaitTime)
                .to({ scaleFactor: 0 }, this.settings.endTweenOutTime, RS.Ease.backIn)
                .call(() =>
                {
                    RS.Ticker.unregisterTickers(this);
                    this._onFinished.publish();
                    this.onFinished();
                });

            if (this._drumsSound)
            {
                this._drumsSound.dispose();
            }

            if (this._chimes)
            {
                this._chimes.dispose();
            }

            if (this._choir)
            {
                this._choir.dispose();
            }
        }

        protected createSkipButton()
        {
            this._skipButton = new RS.DOM.Button();
            this._skipButton.addClass("rs-fullscreen");
            this._skipButton.onClicked(this.onSkip);
            this._skipButton.addToWrapper();
        }

        protected createFlareContainer()
        {
            this._flareContainer = new RS.Flow.Container({
                dock: RS.Flow.Dock.Float,
                floatPosition: this.settings.landscapeSettings.lightFlarePos,
                sizeToContents: true
            });
            this._flareContainer.alpha = 0;
            if (this._mainContainer)
            {
                this._mainContainer.addChild(this._flareContainer);
            }
            else
            {
                this.addChild(this._flareContainer);
            }
        }

        protected createTopParticles()
        {
            this._topParticleSystem = Red7.MegaDrop.JackpotWin.TopParticles.create();
            this._topParticleSystem.name = "Top Particles";
            this._topParticleSystem.blendMode = RS.Rendering.BlendMode.Add;
            this._topParticleSystem.alpha = 0;

            this.addChild(this._topParticleSystem);

            this._topParticleEmitter = new RS.Particles.Emitters.Box(this._topParticleSystem, Red7.MegaDrop.JackpotWin.TopParticleEmitter);
            this._topParticleEmitter.enabled = true;
        }

        protected createExplosionParticles()
        {
            this._explosionParticleSystems = [];
            this._explosionParticleEmitters = [];

            for (let i = 0; i < JackpotWin.ExplosionParticleSystems.length; i++)
            {
                const newSys = new RS.Particles.System(JackpotWin.ExplosionParticleSystems[i])
                newSys.name = "Explosion Particles";
                newSys.blendMode = RS.Rendering.BlendMode.Add;

                this._explosionParticleSystems.push(newSys);
                this._weight.addChild(newSys);

                const newEmitter = new RS.Particles.Emitters.Box(newSys, Red7.MegaDrop.JackpotWin.ExplosionParticleEmitters[i]);
                newEmitter.enabled = true;
                this._explosionParticleEmitters.push(newEmitter);
            }
        }

        protected createLightFlares()
        {
            const flareSettings = this.settings.lightFlareSettings;
            this._lightFlares = [];

            for (let i = 0; i < flareSettings.flareAmount; i++)
            {
                const newFlare = new RS.Flow.Image(flareSettings.lightFlareImage);
                newFlare.rotation = flareSettings.flareRotateOffset * i;
                newFlare.innerSprite.blendMode = RS.Rendering.BlendMode.Screen;
                newFlare.alpha = flareSettings.flareBaseAlpha - (flareSettings.flareAlphaOffset * i);

                this._flareContainer.addChild(newFlare);
                this._lightFlares.push(newFlare);
            }
        }

        @RS.Tick({ kind: RS.Ticker.Kind.Default })
        protected rotateLightFlares(ev: RS.Ticker.Event)
        {
            const deltaTime = ev.delta / 1000;

            for (let i = 0; i < this._lightFlares.length; i++)
            {
                this._lightFlares[i].rotation += (this.settings.lightFlareSettings.rotateSpeed + (this.settings.lightFlareSettings.flareRotateOffset * i)) * deltaTime;
            }
        }

        protected createWeight()
        {
            this._weight = new RS.Flow.Image(this.settings.weightSettings.weight);
            this._weight.name = "Weight";

            if (this._mainContainer)
            {
                this._mainContainer.addChild(this._weight);
            }
            else
            {
                this.addChild(this._weight);
            }
        }

        protected createCountupText()
        {
            this._countupText = new JackpotWin.Countup(this.settings.countupText, this.settings.countupTextSettings, {
                countupTime: this.settings.countupTotalTime,
                totalWin: this._jackpotWinSettings.totalWin
            }, this._jackpotWinSettings.winObservable);
            this._countupText.onComplete(this.onComplete)

            this._weight.addChild(this._countupText);
        }

        protected createJackpotTierText()
        {
            const tier = this._jackpotWinSettings.jackpotTier == null ? 0 : this._jackpotWinSettings.jackpotTier;
            this._tierText = new RS.Flow.Label(this.settings.jackpotTierText[tier]);
            this._tierText.scaleFactor = 0;
            this._weight.addChild(this._tierText);

            this._isEpic = (this._jackpotWinSettings.jackpotTier === MegaDrop.Models.MegaDropJackpotTier.Epic);
        }

        protected performScreenShake()
        {
            const shakeSettings = this.settings.screenShakeSettings;
            const minIntensity = 0;

            for (const view of this._viewController.viewStack)
            {
                view.screenShakeIntensity = minIntensity;
                RS.Tween.get(view)
                    .to({screenShakeIntensity: shakeSettings.maxIntensity}, shakeSettings.shakeUpTime)
                    .wait(shakeSettings.shakeWaitTime)
                    .to({screenShakeIntensity: minIntensity}, shakeSettings.shakeDownTime)
            }
        }

        protected playEpicSound()
        {
            if (this._isEpic)
            {
                this._epicSound = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.EpicLand);
            }
        }
    }

    export namespace View
    {
        export interface JackpotWinSettings
        {
            isReplay: boolean;
            totalWin: number;
            jackpotTier: Red7.MegaDrop.Models.MegaDropJackpotTier;
            winObservable: RS.Observable<number>;
        }
        export interface Settings extends RS.View.Base.Settings
        {
            /** Time it takes for the effects at the top of the screen to fade in. */
            topEffectsFadeInTime: number;
            /** Time to wait before chimes sounds start playing */
            chimesDelay: number;
            /** Time to wait before choir sound starts playing */
            choirDelay: number;
            /** Time to wait for before the weight drops. */
            introPauseTime: number,
            /** Time to wait for after the weight drops and before the tier text tweens in. */
            weightPauseTime: number;
            /** How long it should take for the tier text to tween in. */
            tierTweenInTime: number;
            /** The total time that the countup should take to complete. */
            countupTotalTime: number;
            /** Time to wait for after the animation is complete. */
            endWaitTime: number;
            /** Time it takes for the weight + text to shrink and effects to fade. */
            endTweenOutTime: number;

            /** Time that we should wait for in the case of a replay, as we aren't showing a jackpot win */
            replayWaitTime: number;

            countupText: RS.Flow.Label.Settings;
            countupTextSettings: Countup.Settings;
            jackpotTierText: RS.Flow.Label.Settings[];
            weightSettings: WeightSettings;
            screenShakeSettings: ScreenShakeSettings;
            lightFlareSettings: LightFlareSettings;
            landscapeSettings: OrientationSettings;
            portraitSettings: OrientationSettings;
        }

        export interface OrientationSettings
        {
            weightStartPos: RS.Math.Vector2D;
            lightFlarePos: RS.Math.Vector2D;
        }

        export interface LightFlareSettings
        {
            lightFlareImage: RS.Flow.Image.Settings;
            flareAmount: number;
            /** How much to offset each new flare's rotation by during tick rotation, to prevent overlap. */
            flareRotateOffset: number;
            rotateSpeed: number;
            flareBaseAlpha: number;
            /** How much to offset each new flare's alpha by, to prevent overlap. */
            flareAlphaOffset: number;
        }

        export interface WeightSettings
        {
            weight: RS.Flow.Image.Settings;
            /** How quickly the weight falls down from the top of the screen. */
            tweenTime: number;
            /** The end position the weight should sit at. */
            endFloatY: number;
        }

        export interface ScreenShakeSettings
        {
            maxIntensity: number;
            /** How long it takes for the screen to go from 0 to max intensity. */
            shakeUpTime: number;
            /** Time we should have the screen shake at max intensity for. */
            shakeWaitTime: number;
            /** How long it takes for the screen to go from max intensity to 0. */
            shakeDownTime: number;
        }
    }
}
