namespace Red7.MegaDrop.JackpotWin
{
    export class Countup extends RS.Flow.Label
    {
        public get currentValue() { return this._currentValue; }
        public set currentValue(value) { this._currentValue = value; }

        @RS.AutoDispose public readonly onComplete = RS.createSimpleEvent();
        @RS.AutoDispose public readonly onUpgrade = RS.createSimpleEvent();
        
        protected _winObservable: RS.IObservable<number>;

        protected _countupSettings: Countup.Settings;

        protected _countupTime: number;
        protected _startingValue: number;
        protected _currentValue: number;
        protected _toValue: number;
        protected _skipped: boolean = false;
        protected _isSyndicated: boolean = false;

        @RS.AutoDispose protected _countUpSound: RS.IDisposable;
        @RS.AutoDispose protected _countUpEndSound: RS.IDisposable;

        constructor(settings: RS.Flow.Label.Settings, countupSettings: Countup.Settings, jpData: Countup.JackpotData, winObservable: RS.IObservable<number>)
        {
            super(settings);
            
            this._countupSettings = countupSettings;

            this._countupTime = jpData.countupTime;
            this._toValue = jpData.totalWin;

            this._winObservable = winObservable;
            this._startingValue = this._winObservable.value;

            this.scaleFactor = 0;
        }
        
        public async startCountup(syndicated: boolean = false)
        {
            this._isSyndicated = syndicated;
            // Skip win count up
            if (!this._isSyndicated)
            {
                this._currentValue = 0;
                this.onValueChanged();

                this._countUpSound = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.WinCountUp);

                const idleScaleUpTime = RS.Math.max(this._countupTime - this._countupSettings.tweenInTime, 0);

                //Tween animations
                RS.Tween.get<Countup>(this)
                    .to({ scaleFactor: 1 }, this._countupSettings.tweenInTime, RS.Ease.backOut)
                    .to({ scaleFactor: this._countupSettings.maxScale }, idleScaleUpTime);

                //Tween value
                await RS.Tween.get<Countup>(this, { onChange: () => this.onValueChanged()})
                    .to({ currentValue: this._toValue }, this._countupTime);

                const curScale = this.scaleFactor;
                
                RS.Tween.get<Countup>(this)
                    .to({ scaleFactor: curScale + this._countupSettings.endScaleAmount }, this._countupSettings.finishTweenTime / 2)
                    .to({ scaleFactor: 1 }, this._countupSettings.finishTweenTime / 2);
            }
            // Done
            this.finishCountup();
        }

        public onSkip()
        {
            if (this._skipped)
            {
                return;
            }

            RS.Tween.removeTweens<Countup>(this);
            if (!this._isSyndicated)
            {
                this.scaleFactor = 1;
            }
            this._skipped = true;

            this.finishCountup();
        }
        
        public onFinished()
        {
            if (!this.isDisposed)
            {
                this.text = "";
            }
        }

        protected onValueChanged(): void
        {   
            const value = Math.round(this._currentValue);
            if (this._winObservable) { this._winObservable.value = this._startingValue + value; }

            if (this.currencyFormatter)
            {
                this.text = this.currencyFormatter.format(value);
            }
            else
            {
                this.text = value.toString();
            }
        }

        protected finishCountup()
        {   
            if (!this._isSyndicated)
            {
                this._currentValue = this._toValue;
                this.onValueChanged();
            }

            if (this._countUpSound)
            {
                this._countUpSound.dispose();
            }

            this._countUpEndSound = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.WinAmount);

            this.onComplete.publish();
        }
    }

    export namespace Countup
    {
        export interface Settings
        {
            tweenInTime: number; /* should be less than countup time */
            maxScale: number;
            finishTweenTime: number; /* used for the scale up and down at the end */
            endScaleAmount: number;
        }
        export interface JackpotData 
        {
            totalWin: number;
            countupTime: number;
        }
    }
}