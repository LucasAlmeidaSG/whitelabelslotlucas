/// <reference path="../settings/Suspense.ts" />

namespace Red7.MegaDrop
{
    import Flow = RS.Flow;

    export class Suspense extends RS.Flow.BaseElement<Suspense.Settings>
    {
        public readonly _onFinished = RS.createSimpleEvent();
        public _isPlaying: boolean;

        public settings: Suspense.Settings;

        @RS.AutoDisposeOnSet protected _background: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _symbolsContainer: RS.Flow.Container;

        @RS.AutoDisposeOnSet protected _symbols: RS.Flow.Image[];
        @RS.AutoDisposeOnSet protected _suspenseSprite: RS.Flow.Image;

        @RS.AutoDisposeOnSet protected _mask: RS.Rendering.IGraphics;

        @RS.AutoDisposeOnSet protected _rng: RS.Math.MersenneTwister;

        private _symbolsToDrop: number;
        private _isTease: boolean;

        constructor(settings: Suspense.Settings)
        {
            super(settings);

            this.showBackground(false);
            this.createSymbols();
            this.alpha = 0;

            this._isTease = true;
            this._symbolsToDrop = 0;
        }

        /**
         * Determines if suspense win/tease is played
         * @param betID
         * @param isWin 
         */
        public setup(seed: number, isWin: boolean, forceTease: boolean = false)
        {
            this._rng = new RS.Math.MersenneTwister(seed);

            if (!isWin)
            {
                // Check for random tease
                this._isTease =  this._rng.randomIntRange(1, this.settings.teaseSettings.teaseChanceEverySpin + 1) <= 1 || forceTease;

                if (!this._isTease) { return; }
            }

            this.reset();

            // Setup animation
            if (isWin)
            {
                // Drop All symbols
                this._symbolsToDrop = this._symbols.length;
                this._isPlaying = true;
                this._isTease = false;
            }
            else if (this._isTease)
            {
                // Generate random number of symbols to drop
                const dropCountChance = this._rng.randomIntRange(0, 100);

                if (dropCountChance < 20)
                {
                    this._symbolsToDrop = 5;
                }
                else if (dropCountChance < 50)
                {
                    this._symbolsToDrop = 3;
                }
                else
                {
                    this._symbolsToDrop = 2;
                }

                this._isPlaying = true;
            }
        }

        public async drop(index: number)
        {
            if (!this._isPlaying) { return; }

            if (index >= this._symbols.length) 
            {
                return;
            }

            this.showBackground();

            // Allow megadrop suspense to be "closed" before the end of the spin
            if (this.settings.endTeaseEarly && this._isTease && index >= this._symbolsToDrop)
            {
                await RS.Tween.wait(this.settings.endTeaseDelay? this.settings.endTeaseDelay : 250);
                await this.close();
            }
            // Check for the last symbol (Drop Symbol)
            else if (index >= this._symbols.length - 1)
            {
                if (this._symbolsToDrop >= index)
                {
                    // Suspense for the last symbol
                    await this.startSuspense();
                }
                else { await RS.Tween.wait(500); }

                if (!this._isTease)
                {
                    await this.dropSymbol(index);
                }
                await this.close();
            }
            else if ( index < this._symbolsToDrop)
            {
                this.dropSymbol(index);
            }
        }

        protected async close()
        {
            // Fade Out
            await RS.Tween.get(this as Suspense).to({alpha: 0.0 }, 250);
            this.alpha = 0;
            this._isPlaying = false;
            this._onFinished.publish();
        }

        protected showBackground(show: boolean = true)
        {
            if (!this.settings.background) { return; }

            if (!this._background)
            {
                this._background = RS.Flow.background.create(this.settings.background, this);
                this._background.alpha = 0.0;
            }

            if (show)
            {
                // Fade In
                RS.Tween.get(this._background).to({ alpha: 1.0 }, 200);
            }
            else
            {
                // Fade Out
                RS.Tween.get(this._background).to({ alpha: 0.0 }, 200);
            }
        }
        
        protected async startSuspense()
        {
            if (!this._suspenseSprite)
            {
                this._suspenseSprite = RS.Flow.image.create(this.settings.suspenseSprite, this);
                this.moveToBottom(this._suspenseSprite);
                this.invalidateLayout();
            }
            else
            {
                this.moveToBottom(this._suspenseSprite);
            }

            if (this.settings.suspenseSound)
            {
                RS.Audio.play(this.settings.suspenseSound);
            }

            if (this.settings.tablePeekSuspense)
            {
                this._suspenseSprite.alpha = 1;
                const tween =
                RS.Tween.get(this._suspenseSprite)
                .wait(500)
                .to({ floatOffset: { x: 0, y: 100 } }, 1500, RS.Ease.getElasticOut(1, 0.5))
                .to({ floatOffset: { x: 0, y: 0 } }, 1500, RS.Ease.elasticIn)
                .wait(500);

                await tween;
            }
            else
            {
                // Fade In
                this._suspenseSprite.alpha = 0;
                this._suspenseSprite.gotoAndPlay(this.settings.suspenseSprite.animationName);
                RS.Tween.get(this._suspenseSprite).to({ alpha: 1.0 }, 250);

                // Delay
                await RS.Tween.wait(this.settings.suspenseDuration);

                // Fade Out
                await RS.Tween.get(this._suspenseSprite)
                .to({ alpha: 0.0 }, 250);
                this._suspenseSprite.gotoAndStop(this.settings.suspenseSprite.animationName);
            }
            
        }

        protected createSymbols(): void
        {
            this._symbols = [];

            this._mask = new RS.Rendering.Graphics();
            this._mask.name = "Suspense Mask";
            this._mask.drawRect(-(this.settings.maskExpandWidth * 0.5), 0, this.layoutSize.w + this.settings.maskExpandWidth,this._layoutSize.h);
            this.addChild(this._mask);

            if (this.settings.symbols)
            {
                for(const symbolSetting of this.settings.symbols)
                {
                    const symbol = RS.Flow.image.create(symbolSetting, this);
                    symbol.mask = this._mask;
                    this._symbols.push(symbol);
                }
            }

            if (!this._suspenseSprite)
            {
                this._suspenseSprite = RS.Flow.image.create(this.settings.suspenseSprite, this);
                this._suspenseSprite.gotoAndStop(this.settings.suspenseSprite.animationName);
                this._suspenseSprite.alpha = 0;
                this.moveToBottom(this._suspenseSprite);
                this.invalidateLayout();
            }
        }

        /**
         * Drop Symbol Tween
         * @param index
         */
        protected async dropSymbol(index: number)
        {
            const symbol = this._symbols[index];
            const animationName = this.settings.symbols[index].animationName;

            await RS.Tween.get(symbol.postTransform).to({ x: symbol.postTransform.x, y: 0}, 250);

            if (index < this.settings.landSounds.length)
            {
                RS.Audio.play(this.settings.landSounds[index]);
            }

            await symbol.gotoAndPlay(animationName);
        }

        /** Reset symbol positions */
        protected reset(): void
        {
            this.showBackground(false);

            for (let i = 0; i < this._symbols.length; ++i)
            {
                const symbol = this._symbols[i];
                symbol.gotoAndStop(this.settings.symbols[i].animationName);
                symbol.postTransform.x = this.settings.symbolXSpacing * i;
                symbol.postTransform.y = -(this.settings.size.h as number);
            }

            this.alpha = 1.0;
        }
    }

    export namespace Suspense
    {
        export interface TeaseSettings
        {
            /**
             * Tease chance for every 1 in x spins
             */
            teaseChanceEverySpin: number;
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            teaseSettings: TeaseSettings;

            background?: RS.Flow.Background.Settings;

            suspenseDuration: number;
            suspenseSprite: RS.Flow.Image.SpriteSettings;

            symbolXSpacing: number;
            symbols: RS.Flow.Image.SpriteSettings[];
            maskExpandWidth: number;

            landSounds: RS.Asset.SoundReference[];
            suspenseSound: RS.Asset.SoundReference;

            tablePeekSuspense?: boolean;
            /**
             * Whether or not suspense can end mid-spin
             */
            endTeaseEarly?: boolean;
            /**
             * How long suspense lives on the reel once ended prematurely
             */
            endTeaseDelay?: number;
        }
    }

    /** A progress bar. */
    export const suspense = Flow.declareElement(Suspense, Settings.Suspense.defaultSettings);
}