/// <reference path="SplitFlapDigit.ts" />

namespace Red7.MegaDrop.Elements
{
    import Math = RS.Math;
    import Flow = RS.Flow;

    /**
     * A split-flap display for a string, built from split-flap digits.
     */
    export class SplitFlapMeter extends RS.Flow.BaseElement<SplitFlapMeter.Settings>
    {
        protected _text: string = "";

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _list: Flow.List | null = null;

        @RS.AutoDisposeOnSet protected _digits: SplitFlapDigit[] | null = null;

        /** Gets or sets the current text to display on the meter. */
        public get text() { return this._text; }
        public set text(value)
        {
            this._text = value;
            this.updateDigits();
        }

        public constructor(settings: SplitFlapMeter.Settings)
        {
            super(settings, undefined);

            if (settings.background)
            {
                this._bg = Flow.background.create(settings.background, this);
            }

            this._list = Flow.list.create({
                dock: Flow.Dock.Fill,
                direction: Flow.List.Direction.LeftToRight,
                spacing: settings.digitSpacing,
                legacy: false
            }, this);

            let groupAccum = (settings.digitCount - 1) % settings.groupSize;
            const digits = new Array<SplitFlapDigit>(settings.digitCount);
            for (let i = 0, l = settings.digitCount; i < l; ++i)
            {
                digits[i] = splitFlapDigit.create(settings.digitSettings, this._list);
                if (settings.groupSize != null && settings.groupSeperator != null && groupAccum <= 0 && i < l - 1)
                {
                    groupAccum = this.settings.groupSize;
                    if (SplitFlapDigit.isSpriteSheetSource(settings.digitSettings.digitSource))
                    {
                        for (const chr of settings.groupSeperator)
                        {
                            const spr = new RS.Rendering.Bitmap(null);
                            digits[i].loadDigit(spr, chr);
                            const container = Flow.container.create({
                                sizeToContents: false,
                                size: { w: spr.texture.width, h: spr.texture.height },
                                expand: Flow.Expand.Disallowed
                            }, this._list);
                            spr.position.set(spr.texture.width * 0.5, spr.texture.height * 0.5);
                            container.addChild(spr);
                        }
                        
                    }
                    else if (SplitFlapDigit.isTextSource(settings.digitSettings.digitSource))
                    {
                        const txt = new RS.Rendering.Text(settings.digitSettings.digitSource.textSettings, settings.groupSeperator);
                        const lb = txt.getLocalBounds();
                        const container = Flow.container.create({
                            sizeToContents: false,
                            size: lb,
                            expand: Flow.Expand.Disallowed
                        }, this._list);
                        txt.position.set(-lb.x, -lb.y);
                        container.addChild(txt);
                    }
                }
                --groupAccum;
            }
            this._digits = digits;

            this.updateDigits();
        }

        protected updateDigits(): void
        {
            const indexOffset = this._text.length - this._digits.length;
            for (let i = this._digits.length - 1; i >= 0; --i)
            {
                const chr = i + indexOffset;
                if (chr >= 0)
                {
                    this._digits[i].digit = this._text.charAt(chr);
                }
                else
                {
                    this._digits[i].digit = this.settings.blankDigit || " ";
                }
            }
        }
    }

    export namespace SplitFlapMeter
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            /** Background for the meter. */
            background?: Flow.Background.Settings;

            /** The settings for the split flap digits. */
            digitSettings: SplitFlapDigit.Settings;

            /** Spacing around each digit in the list. */
            digitSpacing: Flow.Spacing;

            /** Total number of digits. */
            digitCount: number;

            /** Digit to use when there are more digits than the string length. */
            blankDigit?: string;

            /** How many digits per group. */
            groupSize?: number;

            /** Seperator string between digit groups. */
            groupSeperator?: string;
            
        }
    }

    /**
     * A split-flap meter built out of split-flap digits.
     */
    export const splitFlapMeter = RS.Flow.declareElement(SplitFlapMeter);
}