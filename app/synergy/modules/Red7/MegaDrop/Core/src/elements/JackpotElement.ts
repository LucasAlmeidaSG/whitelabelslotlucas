/// <reference path="../generated/Translations.ts" />

/// <reference path="SplitFlapMeter.ts" />
/// <reference path="ProgressBar.ts" />

namespace Red7.MegaDrop.Elements
{
    import Flow = RS.Flow;

    /**
     * Encapsulates the UI for a single jackpot.
     */
    @RS.HasCallbacks
    export class JackpotElement extends Flow.BaseElement<JackpotElement.Settings, JackpotElement.RuntimeData>
    {
        protected _backgroundMode: boolean = true;

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _list: Flow.List | null = null;

        @RS.AutoDisposeOnSet protected _valueMeter: Flow.Meter | null = null;
        // @RS.AutoDisposeOnSet protected _creditsMeter: SplitFlapMeter | null = null;
        @RS.AutoDisposeOnSet protected _creditsBar: ProgressBar | null = null;
        @RS.AutoDisposeOnSet protected _dropsLabel: Flow.Label | null = null;

        @RS.AutoDisposeOnSet protected _particles: RS.Particles.System<RS.Particles.Base> | null = null;
        @RS.AutoDisposeOnSet protected _emitters: RS.Particles.Emitter[] | null = null;

        public get backgroundMode() { return this._backgroundMode; }
        public set backgroundMode(value)
        {
            this._backgroundMode = value;
            if (this._bg) { this._bg.visible = value; }
        }

        public get showLabel() { return this._dropsLabel.visible; }
        public set showLabel(show: boolean) { this._dropsLabel.visible = show; }

        public constructor(public readonly settings: JackpotElement.Settings, public readonly runtimeData: JackpotElement.RuntimeData)
        {
            super(settings, runtimeData);

            if (settings.background)
            {
                this._bg = Flow.background.create(settings.background, this);
            }

            if (settings.hitArea)
            {
                this.hitArea = settings.hitArea;
            }

            this._list = Flow.list.create({
                dock: Flow.Dock.Fill,
                direction: Flow.List.Direction.TopToBottom,
                expand: Flow.Expand.Allowed,
                legacy: false
            }, this);

            this._valueMeter = Flow.meter.create(settings.valueMeter, this._list);
            this._valueMeter.bindToObservables({
                value: runtimeData.currentValue
            });

            // this._creditsMeter = splitFlapMeter.create(settings.creditsMeter, this._list);
            // this._creditsMeter.bindToObservables({
            //     text: runtimeData.currentCredits.map((val) => `${val}`)
            // });

            this._creditsBar = progressBar.create(settings.creditsBar, this._list);
            this._creditsBar.bindToObservables({
                progress: RS.Observable.reduce([ runtimeData.currentValue, runtimeData.dropValueInRequestedCurrency ], (a, b) => a / b)
            });

            this._dropsLabel = Flow.label.create(settings.dropsLabel, this._list);
            this._dropsLabel.bindToObservables({
                text: runtimeData.dropValueInRequestedCurrency
                    .map((value) => this.currencyFormatter ? this.currencyFormatter.format(value) : `${value}`)
                    .map((value) => RS.Localisation.bind(settings.dropsLabelText, { amount: value }))
            });

            if (settings.effect)
            {
                this._particles = settings.effect.template.create();
                const emitters: RS.Particles.Emitter[] = [];
                emitters.push(new Emitters.Flow(this._particles, {
                    ...settings.effect.emitterSettings,
                    kind: Emitters.Flow.Kind.Border,
                    border: RS.Flow.Spacing.all(4)
                }, this._valueMeter));
                for (const emitter of emitters)
                {
                    emitter.enabled = true;
                }
                this._emitters = emitters;
                this.addChild(this._particles);
            }

            // Set as a button
            this.buttonMode = true;
        }

        public apply(settings: JackpotElement.Settings)
        {
            if (this._bg && settings.background) { this._bg.apply(settings.background); }
            if (this._valueMeter && settings.valueMeter) { this._valueMeter.apply(settings.valueMeter); }
            if (this._creditsBar && settings.creditsBar) { this._creditsBar.apply(settings.creditsBar); }
            if (this._dropsLabel && settings.dropsLabel) { this._dropsLabel.apply(settings.dropsLabel); }

            super.apply(settings);
        }

        protected onFlowParentChanged(): void
        {
            super.onFlowParentChanged();

            // work around issue where meter doesn't update if we have a new parent (causing "NO CURRENCY FORMATTER")
            this._valueMeter.value = this._valueMeter.value;
        }
    }

    export const jackpotElement = RS.Flow.declareElement(JackpotElement, true);

    export namespace JackpotElement
    {
        export interface EffectSettings<TParticle extends RS.Particles.Base>
        {
            template: RS.Particles.ITemplate<TParticle>;
            emitterSettings: RS.Particles.Emitter.Settings<RS.Particles.Base>;
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            /** The overall background. */
            background?: Flow.Background.Settings;

            /** Hit area for user interaction */
            hitArea?: RS.Rendering.ReadonlyShape;

            /** The meter containing the name and award value of the jackpot. */
            valueMeter: Flow.Meter.Settings;

            /** The split-flap meter showing the current credits value. */
            // creditsMeter: SplitFlapMeter.Settings;

            /** The progress bar showing progress towards total credits. */
            creditsBar: ProgressBar.Settings;

            /** The "drops at N credits" label. */
            dropsLabel: Flow.Label.Settings;

            /** The "drops at N credits" text. */
            dropsLabelText: RS.Localisation.ComplexTranslationReference<{ amount: string }>;

            /** Click sound when element is interacted with. */
            clickSound?: RS.Asset.SoundReference;

            /** Optional particle effect. */
            effect?: EffectSettings<RS.Particles.Base>;
        }

        export interface RuntimeData
        {
            /** The currency amount that the jackpot is initialised at. */
            seedValue: RS.IReadonlyObservable<number>;

            /** The currency amount that the jackpot is currently at. */
            currentValue: RS.IReadonlyObservable<number>;

            /** The currency amount at which the jackpot is guaranteed to drop. */
            dropValue: RS.IReadonlyObservable<number>;

            /** The currency amount at which the jackpot is guaranteed to drop. */
            dropValueInRequestedCurrency: RS.IReadonlyObservable<number>;

            /** Name of the jackpot. Used for sorting jackpots. */
            jackpotName: string;
        }

        export const defaultSettings: Settings =
        {
            name: "Jackpot Element",
            background:
            {
                ignoreParentSpacing: true,
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.SolidColor,
                color: new RS.Util.Color(0x46474b),
                borderSize: 0
            },
            valueMeter:
            {
                ...Flow.Meter.defaultSettings,
                sizeToContents: true,
                expand: Flow.Expand.HorizontalOnly,
                dock: Flow.Dock.None,
                spacing: Flow.Spacing.all(10),
                nameLabel:
                {
                    dock: Flow.Dock.Float,
                    floatPosition: { x: 0.5, y: 0.0 },
                    dockAlignment: { x: 0.5, y: 0.75 },
                    sizeToContents: false,
                    size: { w: 350, h: 100 },
                    legacy: false,
                    font: RS.Fonts.FrutigerWorld.Bold,
                    fontSize: 32,
                    textColor: RS.Util.Colors.white,
                    text: "MINOR/MAJOR/EPIC"
                },
                valueLabel:
                {
                    dock: Flow.Dock.Fill,
                    sizeToContents: true,
                    legacy: false,
                    font: RS.Fonts.FrutigerWorld.Bold,
                    fontSize: 32,
                    textColor: RS.Util.Colors.white,
                    text: ""
                },
                isCurrency: true
            },
            // creditsMeter:
            // {
            //     dock: RS.Flow.Dock.Float,
            //     floatPosition: { x: 0.5, y: 0.5 },
            //     sizeToContents: true,
            //     digitSettings:
            //     {
            //         sizeToContents: true,
            //         flapAsset: Red7.MegaDrop.Assets.MegaDrop.Jackpots,
            //         flapFrame: Red7.MegaDrop.Assets.MegaDrop.Jackpots.flipper,
            //         digitSource:
            //         {
            //             asset: Red7.MegaDrop.Assets.MegaDrop.Jackpots,
            //             animationName: Red7.MegaDrop.Assets.MegaDrop.Jackpots.digit,
            //             digitFrameMap:
            //             {
            //                 ["0"]: 0, ["1"]: 1, ["2"]: 2, ["3"]: 3, ["4"]: 4, ["5"]: 5, ["6"]: 6, ["7"]: 7, ["8"]: 8, ["9"]: 9,
            //                 [","]: Red7.MegaDrop.Assets.MegaDrop.Jackpots.digit_comma
            //             }
            //         },
            //         flipTime: 500
            //     },
            //     digitSpacing: RS.Flow.Spacing.all(1),
            //     digitCount: 9,
            //     groupSize: 3,
            //     groupSeperator: ",",
            //     blankDigit: "0"
            // },
            creditsBar:
            {
                ...ProgressBar.defaultSettings,
                sizeToContents: false,
                expand: Flow.Expand.HorizontalOnly,
                size: { w: 0, h: 30 },
                divisor: 1.0
            },
            dropsLabel:
            {
                sizeToContents: true,
                legacy: false,
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 24,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        offset: { x: 0.00, y: 0.00 },
                        size: 4
                    },
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        offset: { x: 0.00, y: 0.00 },
                        color: RS.Util.Colors.white
                    }
                ],
                text: ""
            },
            dropsLabelText: Translations.MegaDrop.DropsAt,
            sizeToContents: false,
            size: { w: 354, h: 197 },
            dock: Flow.Dock.None,
            expand: Flow.Expand.Disallowed,
            padding: { left: 20, top: 40, right: 20, bottom: 20 },
            clickSound: null
        };
    }
}
