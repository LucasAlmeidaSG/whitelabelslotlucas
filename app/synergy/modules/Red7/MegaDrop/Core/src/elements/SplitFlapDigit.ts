namespace Red7.MegaDrop.Elements
{
    import Math = RS.Math;

    /**
     * A split-flap display for a single digit.
     * The digit can be text (from a font) or a frame (from a sprite sheet).
     */
    export class SplitFlapDigit extends RS.Flow.BaseElement<SplitFlapDigit.Settings>
    {
        protected _currentDigit: string = " ";
        protected _queuedDigit: string | null = null;
        protected _flipping: boolean = false;

        @RS.AutoDisposeOnSet protected _baseContainer: RS.Rendering.IContainer | null = null;
        @RS.AutoDisposeOnSet protected _flippingContainer: RS.Rendering.IContainer | null = null;

        @RS.AutoDisposeOnSet protected _baseDigit: RS.Rendering.IBitmap | RS.Rendering.IText | null = null;
        @RS.AutoDisposeOnSet protected _flipDigit: RS.Rendering.IBitmap | RS.Rendering.IText | null = null;
        @RS.AutoDisposeOnSet protected _underDigit: RS.Rendering.IBitmap | RS.Rendering.IText | null = null;

        @RS.AutoDisposeOnSet protected _baseFlipper: RS.Rendering.IBitmap | null = null;
        @RS.AutoDisposeOnSet protected _flippingFlipper: RS.Rendering.IBitmap | null = null;

        @RS.AutoDisposeOnSet protected _topMask: RS.Rendering.IGraphics | null = null;
        @RS.AutoDisposeOnSet protected _botMask: RS.Rendering.IGraphics | null = null;

        /** Gets or sets the currently displayed digit. */
        public get digit() { return this._currentDigit; }
        public set digit(value)
        {
            if (value.length !== 1) { throw new Error("Digit must be 1 character"); }
            if (value === this._currentDigit || value === this._queuedDigit) { return; }
            if (this._flipping)
            {
                this._queuedDigit = value;
            }
            else
            {
                this.animateFlip(this._currentDigit, value);
                this._currentDigit = value;
            }
        }

        public constructor(settings: SplitFlapDigit.Settings)
        {
            super(settings, undefined);

            this._baseContainer = new RS.Rendering.Container("Base Container");
            this.addChild(this._baseContainer);

            this._flippingContainer = new RS.Rendering.Container("Flipping Container");
            this._flippingContainer.visible = false;
            this.addChild(this._flippingContainer);

            this._topMask = new RS.Rendering.Graphics();
            this._topMask.name = "Top Mask";
            this._topMask.visible = false;
            this.addChild(this._topMask);

            this._botMask = new RS.Rendering.Graphics();
            this._botMask.name = "Bottom Mask";
            this._botMask.visible = false;
            this.addChild(this._botMask);

            switch (settings.flapAsset.type)
            {
                case "image":
                {
                    const img = RS.Asset.Store.getAsset(this.settings.flapAsset).as(RS.Asset.ImageAsset);

                    this._baseFlipper = new RS.Rendering.Bitmap(new RS.Rendering.SubTexture(img.asset));
                    this._baseFlipper.name = "Base Flipper";
                    this._baseFlipper.pivot.set(img.asset.width / 2, img.asset.height / 2);
                    this._baseContainer.addChild(this._baseFlipper);

                    this._flippingFlipper = new RS.Rendering.Bitmap(new RS.Rendering.SubTexture(img.asset, new RS.Rendering.Rectangle(0, 0, img.asset.width, img.asset.height / 2)));
                    this._baseFlipper.name = "Flipping Flipper";
                    this._flippingFlipper.pivot.copy(this._baseFlipper.pivot);
                    this._flippingContainer.addChild(this._flippingFlipper);

                    break;
                }
                case "rsspritesheet":
                case "spritesheet":
                {
                    const spritesheet = RS.Asset.Store.getAsset(this.settings.flapAsset).asset as RS.Rendering.ISpriteSheet;
                    if (this.settings.flapFrame == null) { throw new Error("Split-flap digit using sprite sheet must have a frame"); }
                    const frame = RS.Is.string(this.settings.flapFrame) ? spritesheet.getFrame(this.settings.flapFrame, 0) : spritesheet.getFrame(this.settings.flapFrame);
                    
                    this._baseFlipper = new RS.Rendering.Bitmap(frame.imageData);
                    this._baseFlipper.name = "Base Flipper";
                    this._baseFlipper.pivot.set(frame.reg.x, frame.reg.y);
                    this._baseContainer.addChild(this._baseFlipper);

                    this._flippingFlipper = new RS.Rendering.Bitmap(frame.imageData);
                    this._baseFlipper.name = "Flipping Flipper";
                    this._flippingFlipper.pivot.copy(this._flippingFlipper.pivot);
                    this._flippingContainer.addChild(this._flippingFlipper);

                    break;
                }
            }

            if (SplitFlapDigit.isTextSource(settings.digitSource))
            {
                this._underDigit = new RS.Rendering.Text(settings.digitSource.textSettings);
                this._underDigit.visible = false;
                this._underDigit.name = "Under Digit";
                this._baseContainer.addChild(this._underDigit);

                this._baseDigit = new RS.Rendering.Text(settings.digitSource.textSettings);
                this._baseDigit.name = "Base Digit";
                this._baseContainer.addChild(this._baseDigit);

                this._flipDigit = new RS.Rendering.Text(settings.digitSource.textSettings);
                this._flipDigit.name = "Flip Digit";
                this._flippingContainer.addChild(this._flipDigit);
            }
            else if (SplitFlapDigit.isSpriteSheetSource(this.settings.digitSource))
            {
                this._underDigit = new RS.Rendering.Bitmap(null);
                this._underDigit.visible = false;
                this._underDigit.name = "Under Digit";
                this._baseContainer.addChild(this._underDigit);

                this._baseDigit = new RS.Rendering.Bitmap(null);
                this._baseDigit.name = "Base Digit";
                this._baseContainer.addChild(this._baseDigit);

                this._flipDigit = new RS.Rendering.Bitmap(null);
                this._flipDigit.name = "Flip Digit";
                this._flippingContainer.addChild(this._flipDigit);
            }

            this.loadDigit(this._baseDigit, this._currentDigit);
        }

        /**
         * Loads a digit into the specified text or bitmap based on this element's digit source.
         * @param target 
         * @param digit 
         */
        public loadDigit(target: RS.Rendering.IText | RS.Rendering.IBitmap, digit: string)
        {
            const digitSource = this.settings.digitSource;
            if (SplitFlapDigit.isTextSource(digitSource) && target instanceof RS.Rendering.Text)
            {
                target.text = digit;
            }
            else if (SplitFlapDigit.isSpriteSheetSource(digitSource) && target instanceof RS.Rendering.Bitmap)
            {
                if (digit === "" || digit === " ")
                {
                    target.texture = null;
                    target.visible = false;
                    return;
                }
                const asset = RS.Asset.Store.getAsset(digitSource.asset);
                if (asset == null) { throw new Error("Asset is missing"); }
                const spritesheet = asset.asset as RS.Rendering.ISpriteSheet;
                if (spritesheet == null) { throw new Error("Asset is present but not loaded"); }
                let resolvedAnimName: string | null = digitSource.animationName || null;
                let resolvedFrameIndex: number | null = null;
                if (digitSource.digitFrameMap)
                {
                    const result = digitSource.digitFrameMap[digit];
                    if (RS.Is.number(result))
                    {
                        resolvedFrameIndex = result;
                    }
                    else if (RS.Is.string(result))
                    {
                        resolvedAnimName = result;
                        resolvedFrameIndex = 0;
                    }
                }
                if (resolvedFrameIndex == null)
                {
                    resolvedFrameIndex = digit.charCodeAt(0) - (digitSource.asciiStart || 0);
                }
                const frame = resolvedAnimName != null ? spritesheet.getFrame(resolvedAnimName, resolvedFrameIndex) : spritesheet.getFrame(resolvedFrameIndex);
                if (frame == null)
                {
                    RS.Log.warn(`Unable to find frame for digit '${digit}'`);
                    target.visible = false;
                    return;
                }
                target.texture = frame.imageData;
                target.pivot.copy(frame.reg);
                target.visible = true;
            }
            else
            {
                throw new Error("Target and digit source are not compatible");
            }
        }

        protected async animateFlip(fromDigit: string, toDigit: string)
        {
            if (this._flipping) { return; }
            this._flipping = true;

            this._flippingContainer.scale.set(1.0, 1.0);
            this._flippingContainer.visible = true;
            this._flippingContainer.mask = this._topMask;
            this._underDigit.mask = this._topMask;
            this._baseDigit.mask = this._botMask;
            this.loadDigit(this._baseDigit, fromDigit);
            this.loadDigit(this._underDigit, toDigit);
            this.loadDigit(this._flipDigit, fromDigit);

            await RS.Tween.get(this._flippingContainer)
                .to({scaleY: 0.0}, this.settings.flipTime * 0.5, RS.Ease.circIn);

            this.loadDigit(this._flipDigit, toDigit);
            this._flippingContainer.mask = this._botMask;
            this._baseDigit.visible = false;

            this._underDigit.mask = null;
            this.loadDigit(this._underDigit, toDigit);
                
            await RS.Tween.get(this._flippingContainer)
                .to({scaleY: 1.0}, this.settings.flipTime * 0.5, RS.Ease.circOut);
            
            this._flippingContainer.visible = false;
            this._underDigit.visible = false;

            this._baseDigit.visible = true;
            this._baseDigit.mask = null;
            this.loadDigit(this._baseDigit, toDigit);

            this._flipping = false;
            if (this._queuedDigit)
            {
                const newDigit = this._queuedDigit;
                this._queuedDigit = null;
                this.digit = newDigit;
            }
        }

        protected calculateContentsSize(out?: Math.Size2D): Math.Size2D
        {
            out = super.calculateContentsSize(out);
            switch (this.settings.flapAsset.type)
            {
                case "image":
                {
                    const img = RS.Asset.Store.getAsset(this.settings.flapAsset).as(RS.Asset.ImageAsset);
                    if (img == null || img.asset == null) { return out; }
                    out.w = Math.max(out.w, img.asset.width);
                    out.h = Math.max(out.h, img.asset.height);
                    break;
                }
                case "rsspritesheet":
                case "spritesheet":
                {
                    const spritesheet = RS.Asset.Store.getAsset(this.settings.flapAsset).asset as RS.Rendering.ISpriteSheet;
                    if (spritesheet == null) { return out; }
                    if (this.settings.flapFrame == null) { return out; }
                    const frame = RS.Is.string(this.settings.flapFrame) ? spritesheet.getFrame(this.settings.flapFrame, 0) : spritesheet.getFrame(this.settings.flapFrame);
                    out.w = Math.max(out.w, frame.untrimmedSize.w);
                    out.h = Math.max(out.h, frame.untrimmedSize.h);
                    break;
                }
            }
            return out;
        }

        protected performLayout(): void
        {
            super.performLayout();

            if (!this._baseContainer || !this._flippingContainer) { return; }

            this._baseContainer.position.set(this._layoutSize.w / 2, this._layoutSize.h / 2);
            this._baseContainer.pivot.copy(this._baseContainer.position);
            this._flippingContainer.position.set(this._layoutSize.w / 2, this._layoutSize.h / 2);
            this._flippingContainer.pivot.copy(this._flippingContainer.position);

            this._baseFlipper.position.set(this._baseFlipper.pivot.x, this._baseFlipper.pivot.y);
            this._flippingFlipper.position.set(this._flippingFlipper.pivot.x, this._flippingFlipper.pivot.y);

            this._underDigit.position.set(this._layoutSize.w / 2, this._layoutSize.h / 2);
            this._baseDigit.position.set(this._layoutSize.w / 2, this._layoutSize.h / 2);
            this._flipDigit.position.set(this._layoutSize.w / 2, this._layoutSize.h / 2);

            this._topMask.clear();
            this._topMask.drawRect(0, 0, this._layoutSize.w, this._layoutSize.h * 0.5);

            this._botMask.clear();
            this._botMask.drawRect(0, this._layoutSize.h * 0.5, this._layoutSize.w, this._layoutSize.h * 0.5);
        }

    }

    export namespace SplitFlapDigit
    {
        export namespace DigitSource
        {
            /**
             * Use the text rendering engine to source digits.
             */
            export interface Text
            {
                textSettings: RS.Rendering.TextOptions.Settings;
            }

            /**
             * Use a sprite sheet to source digits.
             * 
             * The digitFrameMap specifies how to translate digits into frame indices.
             * If animationName is present, the map will be relative to that animation, otherwise it will be absolute.
             * If no digitFrameMap is present, the frame index used will be the ASCII code of the character instead, using asciiStart as an starting point if present.
             * If digitFrameMap resolves to a string for a particular digit, frame 0 of the animation of that name will be used.
             */
            export interface SpriteSheet
            {
                asset: RS.Asset.SpriteSheetReference | RS.Asset.RSSpriteSheetReference;
                asciiStart?: number;
                animationName?: string;
                digitFrameMap?: RS.Map<number | string>;
            }
        }

        export type DigitSource = DigitSource.Text | DigitSource.SpriteSheet;

        export function isTextSource(src: DigitSource): src is DigitSource.Text
        {
            return RS.Is.object(src) && RS.Is.object((src as DigitSource.Text).textSettings);
        }

        export function isSpriteSheetSource(src: DigitSource): src is DigitSource.SpriteSheet
        {
            return RS.Is.object(src) && RS.Is.assetReference((src as DigitSource.SpriteSheet).asset);
        }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            /** The asset to use for the flap itself. */
            flapAsset: RS.Asset.ImageReference | RS.Asset.SpriteSheetReference | RS.Asset.RSSpriteSheetReference;

            /** If the flap asset is a sprite sheet, use this frame (animation name or absolute index). */
            flapFrame?: string | number;

            /** Where to source digits from (text rendering engine or sprite sheet). */
            digitSource: DigitSource;

            /** How long a single flip should take. */
            flipTime: number;

            /** Sound to play when flipping. */
            flipSound?: RS.Asset.SoundReference;

            /** Settings of sound to play when flipping. */
            flipSoundSettings?: RS.Audio.PlaySettings;
        }
    }

    /**
     * A split-flap display for a single digit.
     * The digit can be text (from a font) or a frame (from a sprite sheet).
     */
    export const splitFlapDigit = RS.Flow.declareElement(SplitFlapDigit);
}