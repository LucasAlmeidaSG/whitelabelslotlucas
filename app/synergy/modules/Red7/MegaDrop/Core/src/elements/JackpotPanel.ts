/// <reference path="../generated/Translations.ts" />
/// <reference path="../generated/Assets.ts" />

/// <reference path="JackpotElement.ts" />

/// <reference path="../settings/Epic.ts" />
/// <reference path="../settings/Major.ts" />
/// <reference path="../settings/Minor.ts" />

namespace Red7.MegaDrop.Elements
{
    import Flow = RS.Flow;

    /**
     * Encapsulates a number jackpot elements.
     */
    export class JackpotPanel extends Flow.BaseElement<JackpotPanel.Settings, JackpotPanel.RuntimeData>
    {
        protected _backgroundMode: boolean = true;
        protected _triangleMode: boolean = false;
        protected _horizontalLogo: boolean = false;

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _list: Flow.List | null = null;
        @RS.AutoDisposeOnSet protected _lrlist: Flow.List | null = null;

        @RS.AutoDisposeOnSet protected _logo: Flow.Image | null = null;
        @RS.AutoDisposeOnSet protected _logoH: Flow.Image | null = null;
        @RS.AutoDisposeOnSet protected _jackpotElements: JackpotElement[] | null = null;

        public get backgroundMode() { return this._backgroundMode; }
        public set backgroundMode(value)
        {
            this._backgroundMode = value;
            if (this._bg) { this._bg.visible = value; }
            this._logo.visible = value;
            for (const element of this._jackpotElements)
            {
                element.backgroundMode = value;
            }
        }

        public get triangleMode() { return this._triangleMode; }
        public set triangleMode(value)
        {
            if (this._triangleMode === value) { return; }
            this._triangleMode = value;
            this._logoH.visible = value;
            if (value)
            {
                this._list.removeChild(this._jackpotElements[1]);
                this._list.removeChild(this._jackpotElements[2]);
                this._list.addChild(this._lrlist);
                this._lrlist.addChild(this._jackpotElements[1]);
                this._lrlist.addChild(this._jackpotElements[2]);
                this._list.spacing = Flow.Spacing.none;
            }
            else
            {
                this._lrlist.removeAllChildren();
                this._list.removeChild(this._lrlist);
                this._list.addChild(this._jackpotElements[1]);
                this._list.addChild(this._jackpotElements[2]);
                this._list.spacing = this.settings.listSpacing;
            }
            this.invalidateLayout();
        }

        public get horizontalLogo() { return this._horizontalLogo; }
        public set horizontalLogo(value)
        {
            this._horizontalLogo = value;
            this._logo.visible = !value;
            this._logoH.visible = value
        }

        public set listDirection(value: Flow.List.Direction)
        {
            this._list.direction = value;
        }

        public set scaleStagger(value: boolean)
        {
            if (value)
            {
                this._jackpotElements[0].scaleFactor = 1.4;
                this._jackpotElements[1].scaleFactor = 1.2;
                this._jackpotElements[2].scaleFactor = 1.0;
            }
            else
            {
                this._jackpotElements[0].scaleFactor = 1.0;
                this._jackpotElements[1].scaleFactor = 1.0;
                this._jackpotElements[2].scaleFactor = 1.0;
            }
        }

        public constructor(public readonly settings: JackpotPanel.Settings, public readonly runtimeData: JackpotPanel.RuntimeData)
        {
            super(settings, runtimeData);

            this.suppressLayouting();

            if (settings.background)
            {
                this._bg = Flow.background.create(settings.background, this);
            }

            this._list = Flow.list.create({
                dock: Flow.Dock.Fill,
                direction: Flow.List.Direction.TopToBottom,
                legacy: false,
                sizeToContents: true,
                spacing: settings.listSpacing
            }, this);

            this._lrlist = Flow.list.create({
                direction: Flow.List.Direction.LeftToRight,
                legacy: false,
                sizeToContents: true,
                spacing: settings.listSpacing
            });

            this._logo = Flow.image.create(settings.logoImage, this._list);
            this._logoH = Flow.image.create(settings.horizLogoImage, this._list);
            this._logoH.visible = false;

            const jpRuntimeData = runtimeData.jackpots;

            const jackpotElements = new Array<JackpotElement>(settings.jackpots.length);
            for (let i = 0, l = runtimeData.jackpots.length; i < l; ++i)
            {
                jackpotElements[i] = jackpotElement.create({
                    ...settings.jackpots[i],
                    currencyFormatter: this.currencyFormatter
                }, jpRuntimeData[i] ? jpRuntimeData[i] : runtimeData.jackpots[i], this._list);
            }
            this._jackpotElements = jackpotElements;

            this.restoreLayouting(false);
        }

        public updateBackground(settings: RS.Flow.Background.Settings)
        {
            const currentIndex = this._bg ? this.children.indexOf(this._bg) : 0;

            this._bg = RS.Flow.background.create(settings);
            this.addChild(this._bg, currentIndex);
        }

        public applyOptions(options: JackpotPanel.Options)
        {
            this.scaleFactor = options.scaleFactor;
            this.backgroundMode = options.backgroundMode;
            this.triangleMode = options.triangleMode;
            this.horizontalLogo = options.horizontalLogo;
            this.visible = options.visible;
            this.listDirection = options.listDirection;
            this.scaleStagger = options.scaleStagger;
        }
    }

    export const jackpotPanel = RS.Flow.declareElement(JackpotPanel, true);

    export namespace JackpotPanel
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            /** The overall background. */
            background?: Flow.Background.Settings;

            /** The logo image. */
            logoImage: Flow.Image.Settings;

            /** The horizontal logo image.  */
            horizLogoImage: Flow.Image.Settings;

            /** The individual jackpots. */
            jackpots: JackpotElement.Settings[];

            listSpacing: Flow.Spacing;
        }

        export interface RuntimeData
        {
            jackpots: JackpotElement.RuntimeData[];
        }

        export interface Options
        {
            scaleFactor: number,
            backgroundMode: boolean;
            triangleMode: boolean;
            horizontalLogo: boolean;
            visible: boolean;
            listDirection: RS.Flow.List.Direction;
            scaleStagger: boolean;
        }

        export const defaultVerticalPanelOptions: Options =
        {
            scaleFactor: 1.0,
            backgroundMode: true,
            triangleMode: false,
            horizontalLogo: false,
            visible: true,
            listDirection: RS.Flow.List.Direction.TopToBottom,
            scaleStagger: false
        }

        export const defaultHorizontalPanelOptions =
        {
            scaleFactor: 0.8,
            backgroundMode: false,
            triangleMode: false,
            horizontalLogo: false,
            visible: true,
            listDirection: RS.Flow.List.Direction.LeftToRight,
            scaleStagger: false
        }

        export const defaultSettings: Settings =
        {
            background:
            {
                ignoreParentSpacing: true,
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.SolidColor,
                color: new RS.Util.Color(0x2b2b2b),
                borderSize: 0
            },
            logoImage:
            {
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Assets.MegaDrop.Jackpots,
                animationName: Assets.MegaDrop.Jackpots.logo,
                sizeToContents: true
            },
            horizLogoImage:
            {
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Assets.MegaDrop.Jackpots,
                animationName: Assets.MegaDrop.Jackpots.logo_horiz,
                sizeToContents: true
            },
            jackpots:
            [
                Red7.MegaDrop.Settings.EpicJackpotElement,
                Red7.MegaDrop.Settings.MajorJackpotElement,
                Red7.MegaDrop.Settings.MinorJackpotElement
            ],
            sizeToContents: true,
            dock: Flow.Dock.Fill,
            expand: Flow.Expand.Allowed,
            contentAlignment: { x: 0.5, y: 0.4 },
            listSpacing: Flow.Spacing.vertical(10),
            padding: Flow.Spacing.axis(20, 10)
        };
    }
}
