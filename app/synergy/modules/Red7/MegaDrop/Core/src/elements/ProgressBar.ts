namespace Red7.MegaDrop.Elements
{
    import Flow = RS.Flow;

    /** A progress bar. */
    export class ProgressBar extends Flow.BaseElement<ProgressBar.Settings>
    {
        protected _progress: number = 0;
        protected _glowState: ProgressBar.GlowState = ProgressBar.GlowState.None;

        protected _activeSettings: ProgressBar.Settings;

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _bar: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _glowBG: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _glowBGTween: RS.IDisposable | null = null;

        /**
         * Gets or sets the progress of this bar.
         */
        @RS.CheckDisposed
        public get progress() { return this._progress; }
        public set progress(value)
        {
            //temporary fix. need to adjust bar to hide or something if below a set val
            if (value < this._activeSettings.minProgress) { value = this._activeSettings.minProgress; }

            if (value >= this.settings.thresholds.high)
            {
                this.updateGlowState(ProgressBar.GlowState.High);
            }
            else if (value >= this.settings.thresholds.medium)
            {
                this.updateGlowState(ProgressBar.GlowState.Medium);
            }
            else if (value >= this.settings.thresholds.low)
            {
                this.updateGlowState(ProgressBar.GlowState.Low);
            }
            else
            {
                this.updateGlowState(ProgressBar.GlowState.None);
            }

            this._progress = value;
            this.invalidateLayout();
        }

        public constructor(settings: ProgressBar.Settings)
        {
            super(settings, undefined);
            this._activeSettings = settings;

            if (settings.background)
            {
                this._bg = Flow.background.create(settings.background, this);
            }
            this._bar = Flow.background.create({
                ...settings.bar,
                sizeToContents: false,
                expand: Flow.Expand.VerticalOnly,
                size: { w: { relative: true, get: this.resolveBarWidth }, h: 0 }
            }, this);
        }

        public apply(settings: ProgressBar.Settings)
        {
            this._activeSettings = settings;

            if (this._bg && settings.background) { this._bg.apply(settings.background); }
            if (this._bar && settings.bar)
            {
                const barIndex = this.children.indexOf(this._bar);
                this._bar = Flow.background.create({
                    ...settings.bar,
                    sizeToContents: false,
                    expand: Flow.Expand.VerticalOnly,
                    size: { w: { relative: true, get: this.resolveBarWidth }, h: 0 }
                });
                this.addChild(this._bar, barIndex);
            }

            let activeGlowSettings: Flow.Background.Settings;
            switch (this._glowState)
            {
                case ProgressBar.GlowState.High:
                    activeGlowSettings = settings.glowHighBG;
                    break;
                case ProgressBar.GlowState.Medium:
                    activeGlowSettings = settings.glowMediumBG;
                    break;
                case ProgressBar.GlowState.Low:
                    activeGlowSettings = settings.glowLowBG;
                    break;
            }
            if (this._glowBG && activeGlowSettings) { this._glowBG.apply(activeGlowSettings); }

            super.apply(settings);
        }

        @RS.Callback
        protected resolveBarWidth(): number
        {
            const p = this.flowParent;
            if (p == null) { return 0.0; }
            return p.layoutSize.w * this._progress / this.settings.divisor;
        }

        protected setGlowState(background: Flow.Background.Settings, pulseSpeed: number, pulseAlpha: number)
        {
            this._glowBG = RS.Flow.background.create(background, this);
            this._glowBG.alpha = 0.0;
            this.moveToTop(this._glowBG);

            this._glowBGTween = RS.Tween.get(this._glowBG, { loop: true })
                .to({ alpha: pulseAlpha }, pulseSpeed, RS.Ease.getPowIn(1))
                .to({ alpha: 0.0 }, pulseSpeed, RS.Ease.getPowIn(1));
        }

        protected updateGlowState(glowState: ProgressBar.GlowState)
        {
            // Update only when the glow state has changed
            if (this._glowState !== glowState)
            {
                this._glowState = glowState;
                // Change State
                this._glowBG = null;
                this._glowBGTween = null;

                switch (glowState)
                {
                    case ProgressBar.GlowState.Low:
                    {
                        this.setGlowState(this._activeSettings.glowLowBG, this._activeSettings.pulseSpeed.low, this._activeSettings.pulseAlpha.low);
                        break;
                    }
                    case ProgressBar.GlowState.Medium:
                    {
                        this.setGlowState(this._activeSettings.glowMediumBG, this._activeSettings.pulseSpeed.medium, this._activeSettings.pulseAlpha.medium);
                        break;
                    }
                    case ProgressBar.GlowState.High:
                    {
                        this.setGlowState(this._activeSettings.glowHighBG, this._activeSettings.pulseSpeed.high, this._activeSettings.pulseAlpha.high);
                        break;
                    }
                    default:
                    {
                        this._glowState = ProgressBar.GlowState.None;
                        break;
                    }
                }
            }
        }
    }

    export namespace ProgressBar
    {
        export enum GlowState
        {
            None,
            Low,
            Medium,
            High
        }

        export interface Thresholds
        {
            low: number;
            medium: number;
            high: number;
        }

        /** Settings for the progress bar element. */
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            divisor: number;

            glowLowBG: Flow.Background.Settings;
            glowMediumBG: Flow.Background.Settings;
            glowHighBG: Flow.Background.Settings;

            /* Progress required to active pulse */
            thresholds: ProgressBar.Thresholds;
            /* Pulse speed for each threshold */
            pulseSpeed: ProgressBar.Thresholds;
            /* Max alpha for each threshold */
            pulseAlpha: ProgressBar.Thresholds;
            /* Min value progress can be to avoid ninepatch clipping*/
            minProgress: number;

            background?: Flow.Background.Settings;
            bar: Flow.Background.Settings;
        }

        export const defaultGlowBGSettings: Flow.Background.Settings =
        {
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            kind: Flow.Background.Kind.NinePatch,
            asset: Assets.MegaDrop.Jackpots,
            frame: Assets.MegaDrop.Jackpots.bar_glow_yellow,
            borderSize: { left: 25, top: 25, right: 25, bottom: 25 },
            size: { w: 350, h: 80 },
            sizeToContents: false,
            blendMode: RS.Rendering.BlendMode.Add,
            expand: Flow.Expand.Allowed
        }

        /** Default settings for the label element. */
        export let defaultSettings: Settings =
        {
            divisor: 100.0,
            bar: Flow.Background.defaultSettings,
            contentAlignment: { x: 0.0, y: 0.5 },
            glowLowBG: defaultGlowBGSettings,
            glowMediumBG:
            {
                ...defaultGlowBGSettings,
                frame: Assets.MegaDrop.Jackpots.bar_glow_amber,
            },
            glowHighBG:
            {
                ...defaultGlowBGSettings,
                frame: Assets.MegaDrop.Jackpots.bar_glow_red,
            },
            thresholds:
            {
                low: 0.7,
                medium: 0.8,
                high: 0.9
            },
            pulseSpeed:
            {
                low: 1800,
                medium: 1350,
                high: 1000
            },
            pulseAlpha:
            {
                low: 0.7,
                medium: 0.9,
                high: 1.0
            },
            minProgress: 0.08
        };
    }

    /** A progress bar. */
    export const progressBar = Flow.declareElement(ProgressBar, ProgressBar.defaultSettings);
}
