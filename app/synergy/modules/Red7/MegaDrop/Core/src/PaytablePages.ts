namespace Red7.MegaDrop.Settings.Paytable
{
    const jackpotsLogo: RS.Flow.Image.Settings =
    {
        dock: RS.Flow.Dock.Float,
        floatPosition: { x: 0.5, y: 0.5 },
        scaleFactor: 1.5,
        kind: RS.Flow.Image.Kind.SpriteFrame,
        asset: Assets.MegaDrop.Jackpots,
        animationName: Assets.MegaDrop.Jackpots.logo
    }

    export function page1(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.MegaDrop.Paytable.Text1,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.MegaDrop.Paytable.Text2,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.MegaDrop.Paytable.Text3,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        }
    };

    export function page2(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.MegaDrop.Paytable.Text4Alt,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.MegaDrop.Paytable.Text5,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.MegaDrop.Paytable.Text6,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        }
    };

    export function page3(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.MegaDrop.Paytable.Text7,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.MegaDrop.Paytable.Text8,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Red7.MegaDrop.Translations.MegaDrop.Paytable.Text9,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        }
    };

    export function legal(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): SG.CommonUI.Views.Paytable.PageInfo
    {
        return {
            title: RS.Translations.Paytable.Disclaimer.Title,
            element: MegaDrop.Paytable.megaDropLegalPage.bind(MegaDrop.Paytable.MegaDropLegalPage.getDefaultSettings(textSettings))
        }
    };

    export function odds(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): SG.CommonUI.Views.Paytable.PageInfo
    {
        return {
            title: MegaDrop.Translations.MegaDrop.Paytable.Title,
            element: MegaDrop.Paytable.megaDropOddsPage.bind(MegaDrop.Paytable.MegaDropOddsPage.getDefaultSettings(textSettings))
        }
    }

    export function megaDropSection(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): SG.CommonUI.Views.Paytable.SectionInfo
    {
        return {
            title: Red7.MegaDrop.Translations.MegaDrop.Paytable.Title,
            pages:
            [
                {
                    title: Translations.MegaDrop.Paytable.Title,
                    element: RS.Paytable.Pages.textPage.bind(page1(textSettings))
                },
                {
                    title: Translations.MegaDrop.Paytable.Title,
                    element: RS.Paytable.Pages.textPage.bind(page2(textSettings))
                },
                {
                    title: Translations.MegaDrop.Paytable.Title,
                    element: RS.Paytable.Pages.textPage.bind(page3(textSettings))
                },
                {
                    title: Translations.MegaDrop.Paytable.Title,
                    element: MegaDrop.Paytable.megaDropAwardsPage.bind(MegaDrop.Paytable.MegaDropAwardsPage.getDefaultSettings(textSettings))
                }
            ]
        }
    }
}
