namespace Red7.MegaDrop.Constants
{
    export const JackpotModifierThresholds: Readonly<{ [value: number]: number }> =
    {
        //default
        20000000:   19900000,
        2000000:    1950000,
        150000:     140000,

        //lowered (150000 is the same)
        500000:     475000,
        1500000:    1450000
    };

    export const JackpotWeights: ReadonlyArray<number> = [ 0.2, 0.35, 0.45 ];

    export const JackpotMaxStake: number = 30000; //called max stake in PAR sheet but is constant
    export const JackpotFixedWeight: number = 0.004;

    // For local builds / demo mode
    export const DefaultDropValues: ReadonlyArray<number> = [ 20000000, 2000000, 150000 ];

    export const EpicJPNames: ReadonlyArray<string> =
    [
        "MegaJackpotEpic",
        "MegaJackpotEpicLow"
    ];

    export const MajorJPNames: ReadonlyArray<string> =
    [
        "MegaJackpotMajor",
        "MegaJackpotMajorLow"
    ];

    export const MinorJPNames: ReadonlyArray<string> =
    [
        "MegaJackpotMinor",
        "MegaJackpotMinorLow"
    ];
}
