/// <reference path="../generated/Translations.ts" />
/// <reference path="../generated/Assets.ts" />

namespace Red7.MegaDrop.Settings
{
    import Flow = RS.Flow;
    import JackpotElement = Elements.JackpotElement;

    export const MinorJackpotElement: JackpotElement.Settings =
    {
        ...JackpotElement.defaultSettings,
        valueMeter:
        {
            ...JackpotElement.defaultSettings.valueMeter,
            background:
            {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.NinePatch,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.minor_award_bg, /* 40 x 40 */
                borderSize: { left: 19, top: 19, right: 19, bottom: 19 },
                sizeToContents: false,
                size: { w: 51, h: 81 },
                expand: Flow.Expand.Allowed
            },
            nameLabel:
            {
                ...JackpotElement.defaultSettings.valueMeter.nameLabel,
                text: Translations.MegaDrop.Minor,
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0 },
                floatOffset: { x: 0, y: 10 },
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 52,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0x31412),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 2.00, y: 2.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0x87ffea)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x7c7b)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0x195958),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 0.8125,
                        offset: { x: 0.00, y: 0.00 }
                    }
                ]
            },
            valueLabel:
            {
                ...JackpotElement.defaultSettings.valueMeter.valueLabel,
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 42,
                layers:
                    [
                        {
                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                            color: new RS.Util.Color(0x4ef7bf),
                            layerType: RS.Rendering.TextOptions.LayerType.Border,
                            offset: { x: 0.00, y: 0.00 },
                            size: 2
                        },
                        {
                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                            color: new RS.Util.Color(0x87ffeb),
                            layerType: RS.Rendering.TextOptions.LayerType.Fill,
                            offset: { x: 0.00, y: 0.00 }
                        }
                    ]
            }
        },
        // creditsMeter:
        // {
        //     ...JackpotElement.defaultSettings.creditsMeter,
        //     padding: Flow.Spacing.all(4),
        //     expand: Flow.Expand.Disallowed,
        //     background:
        //     {
        //         dock: Flow.Dock.Fill,
        //         ignoreParentSpacing: true,
        //         kind: Flow.Background.Kind.NinePatch,
        //         asset: Assets.MegaDrop.Jackpots,
        //         frame: Assets.MegaDrop.Jackpots.minor_credit_bg, /* 15 x 12 */
        //         borderSize: { left: 6, top: 5, right: 7, bottom: 5 }
        //     },
        //     digitCount: 6
        // },
        creditsBar:
        {
            ...JackpotElement.defaultSettings.creditsBar,
            background:
            {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.NinePatch,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.minor_bar_bg, /* 34 x 33 */
                borderSize: { left: 16, top: 16, right: 16, bottom: 16 }
            },
            bar:
            {
                kind: Flow.Background.Kind.NinePatch,
                ignoreParentSpacing: false,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.minor_bar, /* 29 x 27 */
                borderSize: { left: 14, top: 13, right: 14, bottom: 13 }
            },
            padding: Flow.Spacing.all(2),
            expand: Flow.Expand.HorizontalOnly,
            sizeToContents: false,
            size: { w: 34, h: 33 }
        }
    };
}