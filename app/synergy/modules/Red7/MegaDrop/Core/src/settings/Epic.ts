/// <reference path="../generated/Translations.ts" />
/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/shaders/ScreenSpaceOverlay.ts" />

/// <reference path="SparkleParticles.ts" />

namespace Red7.MegaDrop.Settings
{
    import Flow = RS.Flow;
    import JackpotElement = Elements.JackpotElement;

    const overlayShaderProg = new Shaders.ScreenSpaceOverlayShaderProgram();
    const overlayShader = overlayShaderProg.createShader();
    overlayShader.overlayTexture = Assets.MegaDrop.EpicBarPattern;
    overlayShader.scale = RS.Math.Vector2D(32.0, 32.0);

    export const EpicJackpotElement: JackpotElement.Settings =
    {
        ...JackpotElement.defaultSettings,
        valueMeter:
        {
            ...JackpotElement.defaultSettings.valueMeter,
            background:
            {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.NinePatch,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.epic_award_bg, /* 51 x 81 */
                borderSize: { left: 25, top: 40, right: 25, bottom: 40 },
                sizeToContents: false,
                size: { w: 51, h: 81 },
                expand: Flow.Expand.Allowed
            },
            nameLabel:
            {
                ...JackpotElement.defaultSettings.valueMeter.nameLabel,
                text: Translations.MegaDrop.Epic,
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 60,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 4.00, y: 4.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0xf0b738)
                                },
                                {
                                    offset: 0.1,
                                    color: new RS.Util.Color(0xdea82e)
                                },
                                {
                                    offset: 0.54,
                                    color: new RS.Util.Color(0xfdffc6)
                                },
                                {
                                    offset: 0.55,
                                    color: new RS.Util.Color(0x915f11)
                                },
                                {
                                    offset: 0.8,
                                    color: new RS.Util.Color(0xfdc43f)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x885a0d)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0xf7bf3b)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0x5e3d0b)
                                },
                                {
                                    offset: 0.75,
                                    color: new RS.Util.Color(0xf5f4a0)
                                },
                                {
                                    offset: 0.8,
                                    color: new RS.Util.Color(0x503713)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0xfffb7e)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 2,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xfff377),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 1,
                        offset: { x: 0.00, y: 0.00 }
                    }
                ]
            },
            valueLabel:
            {
                ...JackpotElement.defaultSettings.valueMeter.valueLabel,
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 42,
                size: { w: 290, h: 50 },
                sizeToContents: false,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xe6bd51),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        offset: { x: 0.00, y: 0.00 },
                        size: 2
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xf9e951),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    }
                ]
            }
        },
        // creditsMeter:
        // {
        //     ...JackpotElement.defaultSettings.creditsMeter,
        //     padding: Flow.Spacing.all(4),
        //     expand: Flow.Expand.Disallowed,
        //     background:
        //     {
        //         dock: Flow.Dock.Fill,
        //         ignoreParentSpacing: true,
        //         kind: Flow.Background.Kind.NinePatch,
        //         asset: Assets.MegaDrop.Jackpots,
        //         frame: Assets.MegaDrop.Jackpots.epic_credit_bg, /* 17 x 47 */
        //         borderSize: { left: 8, top: 8, right: 8, bottom: 8 }
        //     },
        //     digitCount: 9
        // },
        creditsBar:
        {
            ...JackpotElement.defaultSettings.creditsBar,
            background:
            {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.NinePatch,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.epic_bar_bg, /* 35 x 38 */
                borderSize: { left: 17, top: 18, right: 17, bottom: 18 }
            },
            bar:
            {
                kind: Flow.Background.Kind.NinePatch,
                ignoreParentSpacing: false,
                asset: Assets.MegaDrop.Jackpots,
                frame: RS.Capabilities.WebGL.get().available ? Assets.MegaDrop.Jackpots.epic_bar_desat : Assets.MegaDrop.Jackpots.epic_bar, /* 28 x 27 */
                borderSize: { left: 13, top: 13, right: 13, bottom: 13 },
                shader: RS.Capabilities.WebGL.get().available ? overlayShader : null
            },
            padding: Flow.Spacing.all(6),
            expand: Flow.Expand.HorizontalOnly,
            sizeToContents: false,
            size: { w: 35, h: 38 }
        },
        effect:
        {
            template: sparkleParticles,
            emitterSettings:
            {
                emitTime: 2500,
                emitCount: 1
            }
        }
    };
}