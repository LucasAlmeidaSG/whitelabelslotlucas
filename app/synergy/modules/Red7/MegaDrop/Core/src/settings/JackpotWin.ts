
/// <reference path="../generated/Assets.ts" />

namespace Red7.MegaDrop.Settings
{
    export const GenericJackpotTextSettings: RS.Flow.Label.Settings =
    {
        font: RS.Fonts.FrutigerWorld.Black,
        fontSize: 216,
        text: "",
        textColor: RS.Util.Colors.white,
        dock: RS.Flow.Dock.Float,
        floatPosition: {x: 0.5, y: 0.15},
        sizeToContents: true
    }

    export const JackpotWin: JackpotWin.View.Settings =
    {
        chimesDelay: 500,
        choirDelay: 2000,
        introPauseTime: 2500,
        topEffectsFadeInTime: 750,
        weightPauseTime: 1500,
        tierTweenInTime: 800,
        countupTotalTime: 5000,
        endWaitTime: 3000,
        endTweenOutTime: 500,
        replayWaitTime: 1000,
        landscapeSettings:
        {
            weightStartPos: {x: 0.5, y: -0.4},
            lightFlarePos: {x: 0.5, y: -0.4}
        },
        portraitSettings:
        {
            weightStartPos: {x: 0.5, y: -1.2},
            lightFlarePos: {x: 0.5, y: -1.2}
        },
        countupTextSettings:
        {
            tweenInTime: 500,
            maxScale: 1.3,
            finishTweenTime: 400,
            endScaleAmount: 0.1
        },
        countupText:
        {
            text: "",
            font: RS.Fonts.FrutigerWorld.Black,
            fontSize: 156,
            align: RS.Rendering.TextAlign.Middle,
            textColor: RS.Util.Colors.white,
            layers:
            [
                {
                    fillType: RS.Rendering.TextOptions.FillType.Gradient,
                    gradient:
                    {
                        type: RS.Rendering.Gradient.Type.Vertical,
                        stops:
                        [
                            {
                                color: new RS.Util.Color(0xfefd38),
                                offset: 0
                            },
                            {
                                color: new RS.Util.Color(0xfffc58),
                                offset: 0.6
                            },
                            {
                                color: new RS.Util.Color(0xf99e11),
                                offset: 1
                            }
                        ]
                    },
                    layerType: RS.Rendering.TextOptions.LayerType.Fill,
                    offset: { x: 0.00, y: 0.00 }
                },
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: new RS.Util.Color(0x7b3e0d),
                    layerType: RS.Rendering.TextOptions.LayerType.Border,
                    offset: { x: 0.00, y: 0.00 },
                    size: 3
                }
            ],
            size: {w: 600, h: 600},
            dock: RS.Flow.Dock.Float,
            floatPosition: {x: 0.5, y: 0.9},
            sizeToContents: false,
        },
        weightSettings:
        {
            weight:
            {
                kind: RS.Flow.Image.Kind.Bitmap,
                asset: Assets.MegaDrop.Weight,
                dock: RS.Flow.Dock.Float,
                floatPosition: {x: 0.5, y: -0.4},
                sizeToContents: true
            },
            endFloatY: 0.5,
            tweenTime: 200
        },
        jackpotTierText:
        [
            {
                ...GenericJackpotTextSettings,
                text: Translations.MegaDrop.Epic,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0x2f1a01),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 9.00, y: 9.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0xf0b738)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0xdea82e)
                                },
                                {
                                    offset: 0.48,
                                    color: new RS.Util.Color(0xeeeee7)
                                },
                                {
                                    offset: 0.49,
                                    color: new RS.Util.Color(0x915f11)
                                },
                                {
                                    offset: 0.6,
                                    color: new RS.Util.Color(0xfdc43f)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x885a0d)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0xf7bf3b)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0x5e3d0b)
                                },
                                {
                                    offset: 0.75,
                                    color: new RS.Util.Color(0xf5f4a0)
                                },
                                {
                                    offset: 0.8,
                                    color: new RS.Util.Color(0x503713)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0xfffb7e)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 3,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xfaffac),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 1,
                        offset: { x: 1.00, y: 1.00 }
                    }
                ]
            },
            {
                ...GenericJackpotTextSettings,
                text: Translations.MegaDrop.Major,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xc2b09),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 18.00, y: 18.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0x93e903)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0x8bd848)
                                },
                                {
                                    offset: 0.48,
                                    color: new RS.Util.Color(0xeeeee7)
                                },
                                {
                                    offset: 0.49,
                                    color: new RS.Util.Color(0x578b12)
                                },
                                {
                                    offset: 0.6,
                                    color: new RS.Util.Color(0x97f73e)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x4b8316)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0x9bf135)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0x265a00)
                                },
                                {
                                    offset: 0.75,
                                    color: new RS.Util.Color(0x87f88e)
                                },
                                {
                                    offset: 0.8,
                                    color: new RS.Util.Color(0x2f4d19)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x7dff7b)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 6,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xa7ffad),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 2,
                        offset: { x: 2.00, y: 2.00 }
                    }
                ]
            },
            {
                ...GenericJackpotTextSettings,
                text: Translations.MegaDrop.Minor,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0x1a1a2c),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 18.00, y: 18.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0x3085ee)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0x448cdc)
                                },
                                {
                                    offset: 0.48,
                                    color: new RS.Util.Color(0xeeeee7)
                                },
                                {
                                    offset: 0.49,
                                    color: new RS.Util.Color(0x305a8e)
                                },
                                {
                                    offset: 0.6,
                                    color: new RS.Util.Color(0x5c9efc)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x345c85)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0x47acf5)
                                },
                                {
                                    offset: 0.2,
                                    color: new RS.Util.Color(0x15395b)
                                },
                                {
                                    offset: 0.75,
                                    color: new RS.Util.Color(0x95adfb)
                                },
                                {
                                    offset: 0.8,
                                    color: new RS.Util.Color(0x1c324e)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x7780ff)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 6,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xa7b4ff),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 2,
                        offset: { x: 2.00, y: 2.00 }
                    }
                ]
            }
        ],
        lightFlareSettings:
        {
            lightFlareImage:
            {
                kind: RS.Flow.Image.Kind.Bitmap,
                asset: Assets.MegaDrop.Flare,
                dock: RS.Flow.Dock.Float,
                floatPosition: {x: 0.5, y: 0.5},
                sizeToContents: true
            },
            flareAmount: 2,
            flareRotateOffset: 0.3,
            flareBaseAlpha: 0.7,
            flareAlphaOffset: 0.25,
            rotateSpeed: 0.1
        },
        screenShakeSettings:
        {
            maxIntensity: 0.75,
            shakeUpTime: 100,
            shakeWaitTime: 100,
            shakeDownTime: 1200
        }
    }
}