/// <reference path="../generated/Translations.ts" />
/// <reference path="../generated/Assets.ts" />

namespace Red7.MegaDrop.Settings
{
    import Flow = RS.Flow;
    import JackpotElement = Elements.JackpotElement;

    export const MajorJackpotElement: JackpotElement.Settings =
    {
        ...JackpotElement.defaultSettings,
        valueMeter:
        {
            ...JackpotElement.defaultSettings.valueMeter,
            background:
            {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.NinePatch,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.major_award_bg, /* 43 x 39 */
                borderSize: { left: 21, top: 18, right: 21, bottom: 18 },
                sizeToContents: false,
                size: { w: 51, h: 81 },
                expand: Flow.Expand.Allowed
            },
            nameLabel:
            {
                ...JackpotElement.defaultSettings.valueMeter.nameLabel,
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0 },
                floatOffset: { x: 0, y: 10 },
                text: Translations.MegaDrop.Major,
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 52,
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0x31412),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 2.00, y: 2.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.Gradient,
                        gradient:
                        {
                            type: RS.Rendering.Gradient.Type.Vertical,
                            stops:
                            [
                                {
                                    offset: 0,
                                    color: new RS.Util.Color(0x40f856)
                                },
                                {
                                    offset: 1,
                                    color: new RS.Util.Color(0x379c42)
                                }
                            ]
                        },
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0x5313),
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 0.8125,
                        offset: { x: 0.00, y: 0.00 }
                    }
                ]
            },
            valueLabel:
            {
                ...JackpotElement.defaultSettings.valueMeter.valueLabel,
                font: RS.Fonts.FrutigerWorld.Black,
                fontSize: 42,
                layers:
                    [
                        {
                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                            color: new RS.Util.Color(0x72fead),
                            layerType: RS.Rendering.TextOptions.LayerType.Border,
                            offset: { x: 0.00, y: 0.00 },
                            size: 2
                        },
                        {
                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                            color: new RS.Util.Color(0x87ffc3),
                            layerType: RS.Rendering.TextOptions.LayerType.Fill,
                            offset: { x: 0.00, y: 0.00 }
                        }
                    ]
            }
        },
        // creditsMeter:
        // {
        //     ...JackpotElement.defaultSettings.creditsMeter,
        //     padding: Flow.Spacing.all(4),
        //     expand: Flow.Expand.Disallowed,
        //     background:
        //     {
        //         dock: Flow.Dock.Fill,
        //         ignoreParentSpacing: true,
        //         kind: Flow.Background.Kind.NinePatch,
        //         asset: Assets.MegaDrop.Jackpots,
        //         frame: Assets.MegaDrop.Jackpots.major_credit_bg, /* 15 x 12 */
        //         borderSize: { left: 6, top: 5, right: 7, bottom: 5 }
        //     },
        //     digitCount: 8
        // },
        creditsBar:
        {
            ...JackpotElement.defaultSettings.creditsBar,
            background:
            {
                dock: Flow.Dock.Fill,
                kind: Flow.Background.Kind.NinePatch,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.major_bar_bg, /* 35 x 32 */
                borderSize: { left: 17, top: 15, right: 17, bottom: 15 }
            },
            bar:
            {
                kind: Flow.Background.Kind.NinePatch,
                ignoreParentSpacing: false,
                asset: Assets.MegaDrop.Jackpots,
                frame: Assets.MegaDrop.Jackpots.major_bar, /* 26 x 27 */
                borderSize: { left: 12, top: 13, right: 12, bottom: 13 }
            },
            padding: Flow.Spacing.all(2),
            expand: Flow.Expand.HorizontalOnly,
            sizeToContents: false,
            size: { w: 35, h: 32 }
        }
    };
}