/// <reference path="../generated/Assets.ts" />

namespace Red7.MegaDrop.Settings
{
    export const sparkleParticles = RS.Particles.template()
        .addBehaviour(new RS.Particles.Behaviours.Tweener({
            propertyMap:
            {
                scaleX:
                    [
                        { fromAge: 0, toAge: 750, fromValue: 0.25, toValue: 1.0 },
                        { fromAge: 1250, toAge: 2000, fromValue: 1.0, toValue: 0.25 }
                    ],
                scaleY:
                    [
                        { fromAge: 0, toAge: 750, fromValue: 0.25, toValue: 1.0 },
                        { fromAge: 1250, toAge: 2000, fromValue: 1.0, toValue: 0.25 }
                    ],
                alpha:
                    [
                        { fromAge: 0, toAge: 500, fromValue: 0.0, toValue: 1.0 },
                        { fromAge: 1500, toAge: 2000, fromValue: 1.0, toValue: 0.0 }
                    ],
                rotation:
                    [
                        { fromAge: 0, toAge: 2000, fromValue: RS.Math.Angles.circle(-0.1), toValue: RS.Math.Angles.circle(0.1) }
                    ]
            }
        }))
        .setInitialParticle({
            age: 0,
            maxAge: 2000,
            asset: Assets.MegaDrop.Particles.Sparkle
        });
}