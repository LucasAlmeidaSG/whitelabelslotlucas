namespace Red7.MegaDrop.Settings
{
    export namespace Suspense
    {
        export const defaultSymbolSetting: RS.Flow.Image.Settings =
        {
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.0, y: 0.5 },
            floatOffset: { x: 100, y: 0 },
            kind: RS.Flow.Image.Kind.Sprite,
            asset: Assets.MegaDrop.SymbolDrop,
            animationName: Assets.MegaDrop.SymbolDrop.drop,
            playSettings: { loop: false, fps: 24},
            autoplay: false,
            sizeToContents: true
        };

        export const SymbolM: RS.Flow.Image.SpriteSettings =
        {
            ...defaultSymbolSetting,
            name: "Suspense Symbol M",
            asset: Assets.MegaDrop.SymbolM,
            animationName: Assets.MegaDrop.SymbolM.m
        }

        export const SymbolE: RS.Flow.Image.SpriteSettings =
        {
            ...defaultSymbolSetting,
            name: "Suspense Symbol E",
            asset: Assets.MegaDrop.SymbolE,
            animationName: Assets.MegaDrop.SymbolE.e
        }

        export const SymbolG: RS.Flow.Image.SpriteSettings =
        {
            ...defaultSymbolSetting,
            name: "Suspense Symbol G",
            asset: Assets.MegaDrop.SymbolG,
            animationName: Assets.MegaDrop.SymbolG.g
        }

        export const SymbolA: RS.Flow.Image.SpriteSettings =
        {
            ...defaultSymbolSetting,
            name: "Suspense Symbol A",
            asset: Assets.MegaDrop.SymbolA,
            animationName: Assets.MegaDrop.SymbolA.a
        }

        export const SymbolDrop: RS.Flow.Image.SpriteSettings =
        {
            ...defaultSymbolSetting,
            name: "Suspense Symbol Drop",
            asset: Assets.MegaDrop.SymbolDrop,
            animationName: Assets.MegaDrop.SymbolDrop.drop
        }

        export const SuspenseSprite: RS.Flow.Image.SpriteSettings =
        {
            name: "Suspense sprite",
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.9, y: 0.5 },
            kind: RS.Flow.Image.Kind.Sprite,
            asset: Assets.MegaDrop.Suspense,
            animationName: Assets.MegaDrop.Suspense.suspense,
            playSettings: { loop: true, fps: 24 },
            autoplay: true,
            sizeToContents: true,
        }

        export const defaultSettings: Red7.MegaDrop.Suspense.Settings =
        {
            name: "Megadrop Suspense",
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            size: { w: 1145, h: 676 },
            sizeToContents: false,

            teaseSettings:
            {
                teaseChanceEverySpin: 90
            },

            suspenseDuration: 1000,
            suspenseSprite: SuspenseSprite,
            symbolXSpacing: 229,
            symbols: 
            [
                SymbolM,
                SymbolE,
                SymbolG,
                SymbolA,
                SymbolDrop
            ],
            maskExpandWidth: 100,
            landSounds:
            [
                Assets.MegaDrop.Sounds.Suspense.LandM,
                Assets.MegaDrop.Sounds.Suspense.LandE,
                Assets.MegaDrop.Sounds.Suspense.LandG,
                Assets.MegaDrop.Sounds.Suspense.LandA
            ],
            suspenseSound: Assets.MegaDrop.Sounds.Suspense.Drop
        }
    }
}