/// <reference path="IController.ts" />

namespace Red7.MegaDrop
{
    import Flow = RS.Flow;

    @RS.HasCallbacks
    export class Controller<TSettings extends IController.Settings = IController.Settings> implements IController<TSettings>
    {
        /** Published when the Mega Drop animation has begun. */
        @RS.AutoDispose public readonly onMegaDropAnimationStarted = RS.createSimpleEvent();
        @RS.AutoDispose public readonly onMegaDropCurrencyCodeUpdated = RS.createSimpleEvent();

        public winAnimationContainer: Flow.Container | null = null;

        protected _jackpotWinView: JackpotWin.View;
        protected _currencyFormatter: RS.Localisation.CurrencyFormatter;
        protected _settings: TSettings;
        protected _megaDropModel: Models.MegaDropJackpots;

        protected _suspense: MegaDrop.Suspense;

        private _isDisposed: boolean = false;
        private _mode: LayoutDelegate = StandardUIMode.VerticalRight;

        @RS.AutoDisposeOnSet private _jackpotPanelContainer: Flow.Container | null = null;

        private _jackpotPanelData: Elements.JackpotPanel.RuntimeData;
        @RS.AutoDisposeOnSet private _jackpotPanel: Elements.JackpotPanel | null = null;

        @RS.AutoDisposeOnSet private _seedValueObservables: RS.Observable<number>[] | null = null;
        @RS.AutoDisposeOnSet private _currentValueObservables: RS.Observable<number>[] | null = null;
        @RS.AutoDisposeOnSet private _dropValueObservables: RS.Observable<number>[] | null = null;
        @RS.AutoDisposeOnSet private _dropValueInRequestedCurrencyObservables: RS.Observable<number>[] | null = null;

        public get isDisposed() { return this._isDisposed; }

        public get settings() { return this._settings; }

        public get standardUIMode() { return this._mode; }
        public set standardUIMode(value)
        {
            if (this._mode === value) { return; }
            if (this._jackpotPanelContainer == null) { return; }
            this._mode = value;
            this.updateStandardUIMode();
        }

        // no idea why its called this but don't wanna break games
        public get megaDropCurrencyFormatter() { return this._currencyFormatter; }
        public set megaDropCurrencyFormatter(currencyFormatter: RS.Localisation.CurrencyFormatter)
        {
            this._currencyFormatter = currencyFormatter;
            this.onMegaDropCurrencyCodeUpdated.publish();
        }

        public get megaDropCurrencyFormatterSet(): boolean { return this._currencyFormatter != null }

        public initialise(settings: TSettings): void
        {
            this._settings = settings;

            this._jackpotPanelData =
            {
                jackpots: []
            };

            this._seedValueObservables = new Array<RS.Observable<number>>(settings.jackpotPanel.jackpots.length);
            this._currentValueObservables = new Array<RS.Observable<number>>(settings.jackpotPanel.jackpots.length);
            this._dropValueObservables = new Array<RS.Observable<number>>(settings.jackpotPanel.jackpots.length);
            this._dropValueInRequestedCurrencyObservables = new Array<RS.Observable<number>>(settings.jackpotPanel.jackpots.length);
            for (let i = 0, l = settings.jackpotPanel.jackpots.length; i < l; ++i)
            {
                this._jackpotPanelData.jackpots.push({
                    seedValue: this._seedValueObservables[i] = new RS.Observable(0),
                    currentValue: this._currentValueObservables[i] = new RS.Observable(0),
                    dropValue: this._dropValueObservables[i] = new RS.Observable(0),
                    dropValueInRequestedCurrency: this._dropValueInRequestedCurrencyObservables[i] = new RS.Observable(0),
                    jackpotName: null
                });
            }
        }

        public attach(container: RS.Flow.GenericElement): void
        {
            if (this._jackpotPanelContainer == null)
            {
                this._jackpotPanelContainer = RS.Flow.container.create({
                    sizeToContents: true
                });
            }
            if (this._jackpotPanel == null)
            {
                this._jackpotPanel = Elements.jackpotPanel.create({
                    ...this.settings.jackpotPanel,
                    currencyFormatter: this._currencyFormatter
                }, this._jackpotPanelData, this._jackpotPanelContainer);
                this.updateStandardUIMode();
            }
            container.addChild(this._jackpotPanelContainer);
            container.invalidateLayout();
        }

        public detach(): void
        {
            if (this._jackpotPanelContainer == null) { return; }
            this._jackpotPanelContainer.parent.removeChild(this._jackpotPanelContainer);
        }

        public attachSuspense(container: RS.Flow.GenericElement): void
        {
            if (this._suspense == null)
            {
                this._suspense = MegaDrop.suspense.create(this.settings.suspense);
            }

            container.addChild(this._suspense);
            container.invalidateLayout();
        }

        public detachSuspense(): void
        {
            if (this._suspense == null) { return; }
            this._suspense.parent.removeChild(this._suspense);
        }

        public setupSuspense(betID: number, isWin: boolean, forceTease?: boolean): void
        {
            if (this._suspense)
            {
                this._suspense.setup(betID, isWin, forceTease);
            }
        }

        @RS.Callback
        public async dropSuspenseSymbol(index: number)
        {
            if (this._suspense)
            {
                await this._suspense.drop(index);
            }
        }

        public async isSuspenseComplete()
        {
            if (this._suspense && this._suspense._isPlaying)
            {
                return await this._suspense._onFinished
            }
            else
            {
                return;
            }
        }

        public update(model: Models.MegaDropJackpots): void
        {
            for (let i = 0, l = model.length; i < l; ++i)
            {
                this._seedValueObservables[i].value = model[i].seedValue;
                this._currentValueObservables[i].value = model[i].currentValue;
                this._dropValueObservables[i].value = model[i].dropValue;
                this._dropValueInRequestedCurrencyObservables[i].value = model[i].dropValueInRequestedExchangeRate;

                if (!this._jackpotPanelData.jackpots[i].jackpotName)
                {
                    this._jackpotPanelData.jackpots[i].jackpotName = model[i].name;
                }
            }
            this._megaDropModel = model;
        }

        public updatePanelBackground(settings: RS.Flow.Background.Settings)
        {
            if (this._jackpotPanel)
            {
                this._jackpotPanel.updateBackground(settings);
            }
        }

        public createJackpotElement(tier: Models.MegaDropJackpotTier, overrideSettings?: Partial<Elements.JackpotElement.Settings>): Elements.JackpotElement
        {
            return Elements.jackpotElement.create(
                {
                    ...this.settings.jackpotPanel.jackpots[tier],
                    ...overrideSettings
                },
                this._jackpotPanelData.jackpots[tier]
            );
        }

        public async playWinAnimation(jackpotName: string, jackpotWinnings: number)
        {
            const musicHandle = this.settings.musicVolumeArbiter.declare(0);
            const viewSettings = Red7.MegaDrop.Settings.JackpotWin;
            const jackpotWinSettings: JackpotWin.View.JackpotWinSettings =
            {
                isReplay: this._settings.stateModel.isReplay,
                totalWin: jackpotWinnings,
                jackpotTier: this.settings.JackpotWinTier[jackpotName],
                winObservable: this._settings.winObservable
            };

            this.onMegaDropAnimationStarted.publish();
            this._jackpotWinView = await this._settings.viewController.open(JackpotWin.View, viewSettings);

            this._jackpotWinView.setUpComponents(jackpotWinSettings, this.winAnimationContainer);
            this.onMegaDropAnimationStarted.publish();
            await this._jackpotWinView.start(this._settings.viewController);
            await this._settings.viewController.close(JackpotWin.View);
            musicHandle.dispose();
        }

        public getJackpotOdds(tier: Models.MegaDropJackpotTier, exchangeRate: number, stake: number): number
        {
            let jackpotDropValue = 0;
            let currentJackpot = 0;
            if (this._megaDropModel)
            {
                currentJackpot = this._megaDropModel[tier].currentValue;
                jackpotDropValue = this._megaDropModel[tier].dropValue;
            }
            else
            {
                // set default values for demo mode to avoid divide by 0
                jackpotDropValue = Constants.DefaultDropValues[tier];
            }

            const modifierThreshold = Constants.JackpotModifierThresholds[jackpotDropValue];
            const jackpotWeight = Constants.JackpotWeights[tier];
            const stakeEUR = stake / exchangeRate;

            // Formula from MegaDrop PAR sheet
            let triggerProbability = jackpotWeight * (stakeEUR) * Constants.JackpotFixedWeight / jackpotDropValue;
            if (currentJackpot > modifierThreshold)
            {
                triggerProbability += 0.2 * (stakeEUR / Constants.JackpotMaxStake) * RS.Math.pow(1 - (jackpotDropValue - currentJackpot) / (jackpotDropValue - modifierThreshold), 4);
            }

            return triggerProbability;
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }

            this._isDisposed = true;
        }

        protected updateStandardUIMode(): void
        {
            this._jackpotPanelContainer.suppressLayouting();
            this._mode(this._jackpotPanelContainer, this._jackpotPanel);
            this._jackpotPanelContainer.restoreLayouting();
        }
    }

    IController.register(Controller);
}
