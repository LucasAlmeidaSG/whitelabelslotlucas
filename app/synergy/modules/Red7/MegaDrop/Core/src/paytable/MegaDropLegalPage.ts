namespace Red7.MegaDrop.Paytable
{
    @RS.HasCallbacks
    export class MegaDropLegalPage extends RS.Paytable.Pages.TextPage<MegaDropLegalPage.Settings, RuntimeData>
    {
        protected addElements(elements: RS.Paytable.Pages.TextPage.Element[], parent: RS.Flow.GenericElement): void
        {
            super.addElements(elements, parent);

            RS.Flow.label.create(
            {
                ...this.settings.textSettings,
                text: Translations.MegaDrop.Paytable.Legal.Text1
            }, this._contentList);

            RS.Flow.label.create(
            {
                ...this.settings.textSettings,
                text: Translations.MegaDrop.Paytable.Legal.Text2
            }, this._contentList);

            if (RS.IPlatform.get().maxWinMode != RS.PlatformMaxWinMode.None)
            {
                RS.Flow.label.create(
                {
                    ...this.settings.textSettings,
                    text: Translations.MegaDrop.Paytable.Legal.Text3
                }, this._contentList);
            }

            RS.Flow.label.create(
            {
                ...this.settings.textSettings,
                text: Translations.MegaDrop.Paytable.Legal.Text4
            }, this._contentList);

            this.addRTPWithMegaDrop(parent);
            this.addRTP(parent);

            RS.Flow.label.create(
                {
                    ...this.settings.textSettings,
                    text: Translations.MegaDrop.Paytable.Legal.Text7
                }, this._contentList);
        }

        protected addRTPWithMegaDrop(parent: RS.Flow.GenericElement)
        {
            const rtpWithMegaDrop = this.runtimeData.megaDropConfig.totalRTPWithJackpot;
            let resolvedText = Translations.MegaDrop.Paytable.Legal.Text5.get(this.locale, { RTPWithMegaDrop: rtpWithMegaDrop.toFixed(2) });
            this.addParagraph(this.settings.textSettings, resolvedText, parent);
        }

        protected addRTP(parent: RS.Flow.GenericElement)
        {
            let resolvedText = Translations.MegaDrop.Paytable.Legal.Text6.get(this.locale, { RTP: this.runtimeData.configModel.maxRTP.toFixed(2) });
            this.addParagraph(this.settings.textSettings, resolvedText, parent);
        }
    }

    export const megaDropLegalPage = RS.Flow.declareElement(MegaDropLegalPage, true);

    export namespace MegaDropLegalPage
    {
        export interface Settings extends RS.Paytable.Pages.TextPage.Settings
        {
            textSettings?: RS.Flow.Label.Settings;
            complexTextSettings: RS.Paytable.Pages.TextPage.ComplexTextSettings;
        }

        export function getDefaultSettings(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): MegaDropLegalPage.Settings
        {
            return {
                ...RS.Paytable.Pages.TextPage.defaultSettings,
                textSettings: textSettings,
                complexTextSettings:
                {
                    locale: new RS.Localisation.Locale("en", "GB"),
                    translation: null,
                    substitutions: { },
                    textSettings: { ...textSettings, canWrap: false },
                    listSettings:
                    {
                        direction: RS.Flow.List.Direction.LeftToRight,
                        overflowMode: RS.Flow.OverflowMode.Shrink,
                        sizeToContents: true
                    },
                    kind: RS.Paytable.Pages.TextPage.ElementKind.ComplexText
                },
                elements: [],
            };
        }
    }
}
