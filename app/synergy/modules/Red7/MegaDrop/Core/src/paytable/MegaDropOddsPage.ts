namespace Red7.MegaDrop.Paytable
{
    @RS.HasCallbacks
    export class MegaDropOddsPage extends RS.Paytable.Pages.TextPage<MegaDropOddsPage.Settings, Paytable.RuntimeData>
    {
        protected addElements(elements: RS.Paytable.Pages.TextPage.Element[], parent: RS.Flow.GenericElement): void
        {
            super.addElements(elements, parent);

            this.addOdds();
        }

        protected addOdds()
        {
            RS.Flow.label.create(this.settings.oddsGridTitle, this._contentList);

            const oddsGrid = RS.Flow.grid.create(this.settings.oddsGrid, this._contentList);

            const defaultSettings: RS.Flow.Label.Settings =
            {
                ...this.settings.textSettings,
                dock: RS.Flow.Dock.Fill
            };

            // Add Jackpot Names
            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: Translations.MegaDrop.Paytable.Jackpot
            }, oddsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: Translations.MegaDrop.Minor
            }, oddsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: Translations.MegaDrop.Major
            }, oddsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: Translations.MegaDrop.Epic
            }, oddsGrid);

            // Add Jackpot Odds
            const currentBet = this.runtimeData.totalBet;
            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: Translations.MegaDrop.Paytable.Odds
            }, oddsGrid);

            const jpOdds =
            [
                this.runtimeData.megaDropController.getJackpotOdds(Models.MegaDropJackpotTier.Minor, this.runtimeData.exchangeRate, currentBet),
                this.runtimeData.megaDropController.getJackpotOdds(Models.MegaDropJackpotTier.Major, this.runtimeData.exchangeRate, currentBet),
                this.runtimeData.megaDropController.getJackpotOdds(Models.MegaDropJackpotTier.Epic, this.runtimeData.exchangeRate, currentBet)
            ];
            for (const odds of jpOdds)
            {
                const formattedOdds = this.runtimeData.megaDropController.megaDropCurrencyFormatter.formatDecimal(RS.Math.round(RS.Math.shiftDecimal(1/odds, 2)), false, 0);
                const text = Translations.MegaDrop.Paytable.OddsValue.get(this.locale, {odds: formattedOdds} );
                RS.Flow.label.create(
                {
                    ...defaultSettings,
                    text: text
                }, oddsGrid);
            }
        }
    }

    export const megaDropOddsPage = RS.Flow.declareElement(MegaDropOddsPage, true);

    export namespace MegaDropOddsPage
    {
        export interface Settings extends RS.Paytable.Pages.TextPage.Settings
        {
            textSettings?: RS.Flow.Label.Settings;
            oddsGridTitle: RS.Flow.Label.Settings;
            oddsGrid: RS.Flow.Grid.Settings;
        }

        export function getDefaultSettings(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): MegaDropOddsPage.Settings
        {
            return {
                ...RS.Paytable.Pages.TextPage.defaultSettings,
                textSettings: textSettings,
                elements: [],
                oddsGrid:
                {
                    order: RS.Flow.Grid.Order.ColumnFirst,
                    colCount: 2,
                    rowCount: 4,
                    borderSize: 2,
                    size: { w: 600, h: 150},
                    borderColor: RS.Util.Colors.white,
                    expand: RS.Flow.Expand.VerticalOnly,
                    sizeToContents: false,
                },
                oddsGridTitle:
                {
                    ...textSettings,
                    text: Translations.MegaDrop.Paytable.OddsLine
                }
            }
        };
    }
}
