namespace Red7.MegaDrop.Paytable
{
    export interface Game extends RS.Game
    {
        models: IModels;
        megaDropController: IController;
    }

    export interface Context extends RS.Game.Context
    {
        game: Game;
    }

    export interface RuntimeData extends RS.Paytable.Pages.Base.RuntimeData
    {
        megaDropController: IController;
        megaDropConfig: Models.MegaDropConfig;
        exchangeRate: number;
        totalBet: number;
    }
}
