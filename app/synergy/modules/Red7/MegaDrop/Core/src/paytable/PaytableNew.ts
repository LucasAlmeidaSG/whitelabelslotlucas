namespace Red7.MegaDrop.Views
{
    @RS.HasCallbacks
    export class PaytableNew<TSettings extends PaytableNew.Settings = PaytableNew.Settings, TContext extends MegaDrop.Paytable.Context = MegaDrop.Paytable.Context> extends SG.CommonUI.Views.PaytableNew<TSettings, TContext>
    {
        /** The paytable sections, which contain the pages. */
        protected _sections: Paytable.Section[];

        protected buildPage(pageInfo: PaytableNew.PageInfo)
        {
            let pages = pageInfo.element.create(this.getRuntimeData());

            if (!RS.Is.array(pages))
            {
                pages = [pages];
            }

            return pages;
        }

        protected getRuntimeData(): MegaDrop.Paytable.RuntimeData
        {
            return {
                configModel: this.context.game.models.config,
                stakeModel: this.context.game.models.stake,
                megaDropConfig: this.context.game.models.megaDropConfig,
                megaDropController: this.context.game.megaDropController,
                exchangeRate: this.context.game.models.customer.currencySettings.exchangeRate,
                totalBet: RS.Models.Stake.getCurrentBet(this.context.game.models.stake).value
            };
        }
    }

    export namespace PaytableNew
    {
        export interface SectionInfo
        {
            title: RS.Localisation.LocalisableString;
            pages: PageInfo[];
        }

        export interface PageInfo extends SG.CommonUI.Views.Paytable.PageInfo
        {
            element: RS.Paytable.IPageFactory<RS.Paytable.Pages.Base.Settings, Red7.MegaDrop.Paytable.RuntimeData>;
        }

        export interface Settings extends SG.CommonUI.Views.PaytableNew.Settings
        {
            sections: SectionInfo[];
        }
    }
}
