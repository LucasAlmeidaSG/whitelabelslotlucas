namespace Red7.MegaDrop.Views
{
    @RS.HasCallbacks
    export class Paytable<TSettings extends Paytable.Settings = Paytable.Settings, TContext extends MegaDrop.Paytable.Context = MegaDrop.Paytable.Context> extends SG.CommonUI.Views.Paytable<TSettings, TContext>
    {
        /** The paytable sections, which contain the pages. */
        protected _sections: Paytable.Section[];

        protected buildSections(): void
        {
            this._sections = [];
            for (const section of this.settings.sections)
            {
                const s: Paytable.Section = { pages: [] };
                for (const pageInfo of section.pages)
                {
                    let pages = pageInfo.element.create(this.getRuntimeData());

                    if (!RS.Is.array(pages))
                    {
                        pages = [pages];
                    }

                    for (const pageElement of pages)
                    {
                        s.pages.push(
                        {
                            title: pageInfo.title,
                            element: pageElement
                        });
                    }
                }
                this._sections.push(s);
            }

            if (this._sections.length === 0)
            {
                const tmpLabel: RS.Flow.Label.Settings =
                {
                    dock: RS.Flow.Dock.Fill,
                    text: "Add some paytable pages to your game!",
                    font: SG.Fonts.Frutiger.CondensedMedium,
                    fontSize: 32,
                    textColor: RS.Util.Colors.white
                };
                this._body.page = RS.Flow.label.create(tmpLabel);
            }
        }

        protected getRuntimeData(): MegaDrop.Paytable.RuntimeData
        {
            return {
                configModel: this.context.game.models.config,
                stakeModel: this.context.game.models.stake,
                megaDropConfig: this.context.game.models.megaDropConfig,
                megaDropController: this.context.game.megaDropController,
                exchangeRate: this.context.game.models.customer.currencySettings.exchangeRate,
                totalBet: RS.Models.Stake.getCurrentBet(this.context.game.models.stake).value
            };
        }
    }

    export namespace Paytable
    {
        export interface Section
        {
            pages:
            {
                title: RS.Localisation.LocalisableString;
                element: RS.Flow.GenericElement;
            }[];
        }

        export interface SectionInfo
        {
            title: RS.Localisation.LocalisableString;
            pages: PageInfo[];
        }

        export interface PageInfo extends SG.CommonUI.Views.Paytable.PageInfo
        {
            element: RS.Paytable.IPageFactory<RS.Paytable.Pages.Base.Settings, MegaDrop.Paytable.RuntimeData>;
        }

        export interface Settings extends SG.CommonUI.Views.Paytable.Settings
        {
            sections: SectionInfo[];
        }
    }
}
