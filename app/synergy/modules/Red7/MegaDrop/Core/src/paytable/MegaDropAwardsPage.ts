namespace Red7.MegaDrop.Paytable
{
    @RS.HasCallbacks
    export class MegaDropAwardsPage extends RS.Paytable.Pages.TextPage<MegaDropAwardsPage.Settings, RuntimeData>
    {
        protected addElements(elements: RS.Paytable.Pages.TextPage.Element[], parent: RS.Flow.GenericElement): void
        {
            super.addElements(elements, parent);

            this.addJackpotAwards();
            this.addContributions();
            this.addContributionPerBet();
        }

        protected addJackpotAwards()
        {
            RS.Flow.label.create(this.settings.awardsGridTitle, this._contentList);

            if (!this.settings.awardsGrid) { return; }

            const awardsGrid = RS.Flow.grid.create(this.settings.awardsGrid, this._contentList);

            const defaultSettings: RS.Flow.Label.Settings =
            {
                ...this.settings.textSettings,
                dock: RS.Flow.Dock.Fill
            };

            // Add Jackpot Titles
            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: this.settings.epicTitle
            }, awardsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: this.settings.majorTitle
            }, awardsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: this.settings.minorTitle
            }, awardsGrid);

            // Max Jackpot values
            // We check for NaN here incase we have no jackpots and set it to 0 if so
            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: this.runtimeData.megaDropController.megaDropCurrencyFormatter.format(RS.as(this.runtimeData.megaDropConfig.thresholdsInRequestedExchangeRate[Models.MegaDropJackpotTier.Epic], RS.Is.number, 0), true)
            }, awardsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: this.runtimeData.megaDropController.megaDropCurrencyFormatter.format(RS.as(this.runtimeData.megaDropConfig.thresholdsInRequestedExchangeRate[Models.MegaDropJackpotTier.Major], RS.Is.number, 0), true)
            }, awardsGrid);

            RS.Flow.label.create(
            {
                ...defaultSettings,
                text: this.runtimeData.megaDropController.megaDropCurrencyFormatter.format(RS.as(this.runtimeData.megaDropConfig.thresholdsInRequestedExchangeRate[Models.MegaDropJackpotTier.Minor], RS.Is.number, 0), true)
            }, awardsGrid);
        }

        protected addContributions()
        {
            // Title
            RS.Flow.label.create(this.settings.contributionsGridTitle, this._contentList);

            if (!this.settings.contributionsGrid) { return; }

            const contributionsGrid = RS.Flow.grid.create(this.settings.contributionsGrid, this._contentList);

            const defaultTextSettings: RS.Flow.Label.Settings =
            {
                ...this.settings.textSettings,
                expand: RS.Flow.Expand.Disallowed,
                dock: RS.Flow.Dock.Fill,
                sizeToContents: false,
                size: { w: 190, h: 60 },
            };

            // Jackpots
            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: Translations.MegaDrop.Paytable.Jackpot
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.settings.minorTitle
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.settings.majorTitle
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.settings.epicTitle
            }, contributionsGrid);

            // Contributions Seed
            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: Translations.MegaDrop.Paytable.ContributionCurrent
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.runtimeData.megaDropConfig.seedContributions[Models.MegaDropJackpotTier.Minor].toString()
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.runtimeData.megaDropConfig.seedContributions[Models.MegaDropJackpotTier.Major].toString()
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.runtimeData.megaDropConfig.seedContributions[Models.MegaDropJackpotTier.Epic].toString()
            }, contributionsGrid);

            // Contributions Reseed
            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: Translations.MegaDrop.Paytable.ContributionReseed
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.runtimeData.megaDropConfig.reseedContributions[Models.MegaDropJackpotTier.Minor].toString()
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.runtimeData.megaDropConfig.reseedContributions[Models.MegaDropJackpotTier.Major].toString()
            }, contributionsGrid);

            RS.Flow.label.create(
            {
                ...defaultTextSettings,
                text: this.runtimeData.megaDropConfig.reseedContributions[Models.MegaDropJackpotTier.Epic].toString()
            }, contributionsGrid);
        }

        protected addContributionPerBet()
        {
            const string = Translations.MegaDrop.Paytable.ContributionPerBet.get(this.locale, { contributionPerBet: this.runtimeData.megaDropConfig.contributionPerBet });

            RS.Flow.label.create(
            {
                ...this.settings.textSettings,
                dock: RS.Flow.Dock.Fill,
                text: string
            }, this._contentList);
        }
    }

    export const megaDropAwardsPage = RS.Flow.declareElement(MegaDropAwardsPage, true);

    export namespace MegaDropAwardsPage
    {
        export interface Settings extends RS.Paytable.Pages.TextPage.Settings
        {
            textSettings?: RS.Flow.Label.Settings;
            complexTextSettings: RS.Paytable.Pages.TextPage.ComplexTextSettings;
            awardsGridTitle: RS.Flow.Label.Settings;
            awardsGrid: RS.Flow.Grid.Settings;

            contributionsGridTitle: RS.Flow.Label.Settings;
            contributionsGrid: RS.Flow.Grid.Settings;

            epicTitle: RS.Localisation.SimpleTranslationReference;
            majorTitle: RS.Localisation.SimpleTranslationReference;
            minorTitle: RS.Localisation.SimpleTranslationReference;
        }

        export function getDefaultSettings(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): MegaDropAwardsPage.Settings
        {
            return {
                ...RS.Paytable.Pages.TextPage.defaultSettings,
                textSettings: textSettings,
                complexTextSettings:
                {
                    locale: new RS.Localisation.Locale("en", "GB"),
                    translation: null,
                    substitutions: { },
                    textSettings: { ...textSettings, canWrap: false },
                    listSettings:
                    {
                        direction: RS.Flow.List.Direction.LeftToRight,
                        overflowMode: RS.Flow.OverflowMode.Shrink,
                        sizeToContents: true
                    },
                    kind: RS.Paytable.Pages.TextPage.ElementKind.ComplexText
                },
                elements: [],
                awardsGrid:
                {
                    order: RS.Flow.Grid.Order.ColumnFirst,
                    colCount: 2,
                    rowCount: 3,
                    borderSize: 2,
                    size: { w: 600, h: 150},
                    borderColor: RS.Util.Colors.white,
                    expand: RS.Flow.Expand.VerticalOnly,
                    sizeToContents: false,
                },
                awardsGridTitle:
                {
                    ...textSettings,
                    text: Translations.MegaDrop.Paytable.MaxJackpotAwards
                },
                contributionsGrid:
                {
                    order: RS.Flow.Grid.Order.ColumnFirst,
                    colCount: 3,
                    rowCount: 4,
                    sizeToContents: false,
                    size: { w: 600, h: 300},
                    borderSize: 2,
                    borderColor: RS.Util.Colors.white,
                    expand: RS.Flow.Expand.VerticalOnly
                },
                contributionsGridTitle:
                {
                    ...textSettings,
                    text: Translations.MegaDrop.Paytable.ProportionOfBet
                },
                epicTitle: Translations.MegaDrop.Epic,
                majorTitle: Translations.MegaDrop.Major,
                minorTitle: Translations.MegaDrop.Minor
            }
        };
    }
}
