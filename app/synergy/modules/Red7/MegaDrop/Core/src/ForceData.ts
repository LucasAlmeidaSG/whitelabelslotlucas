namespace Red7.MegaDrop
{
    export interface ForceResult extends RS.Engine.ForceResult
    {
        awardJackpots?: string[];
        suppressAward?: boolean;
        suppressContrib?: boolean;
    }

    export type ForceData = RS.Map<RS.DevTools.Helpers.ItemSelector.Entry<RS.OneOrMany<ForceResult>>>;

    export const commonForceData: ForceData =
    {
        "Mega Drop":
        {
            isCategory: true,
            additive: true,
            items:
            {
                "Minor": { awardJackpots: [ "MegaJackpotMinor" ] },
                "Major": { awardJackpots: [ "MegaJackpotMajor" ] },
                "Epic": { awardJackpots: [ "MegaJackpotEpic" ] },
                "Suppress Win": { suppressAward: true },
                "Suppress Contrib": { suppressContrib: true },
                "Suppress Both": { suppressAward: true, suppressContrib: true }
            }
        }
    };

    export const commonForceDataLow: ForceData =
    {
        "Mega Drop Low":
        {
            isCategory: true,
            additive: true,
            items:
            {
                "Minor": { awardJackpots: [ "MegaJackpotMinorLow" ] },
                "Major": { awardJackpots: [ "MegaJackpotMajorLow" ] },
                "Epic": { awardJackpots: [ "MegaJackpotEpicLow" ] },
                "Suppress Win": { suppressAward: true },
                "Suppress Contrib": { suppressContrib: true },
                "Suppress Both": { suppressAward: true, suppressContrib: true }
            }
        }
    };
}
