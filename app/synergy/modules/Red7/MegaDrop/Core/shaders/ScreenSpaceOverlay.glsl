precision lowp float;

#pragma meta private
uniform sampler2D uSampler;

#pragma meta property=overlayTexture
uniform sampler2D uTex;

#pragma meta default=1.0 targetsizeop=Divide
uniform vec2 uScale;

varying vec2 vTextureCoord;
varying vec4 vColor;
varying float vTextureId;

void OverlayFragment()
{
    vec4 baseColor;
     float textureId = floor(vTextureId + 0.5);
    if (textureId == 0.0)
    {
        baseColor = texture2D(uSampler, vTextureCoord);
    }
    else
    {
        baseColor = texture2D(uSampler, vTextureCoord);
    }
    vec4 overlayColor = texture2D(uTex, gl_FragCoord.xy * uScale);
    gl_FragColor = baseColor * overlayColor * 2.0 * vColor;
}

// A shader that modulates the base texture with a screen-aligned overlay texture
#pragma export ScreenSpaceOverlay Fragment=OverlayFragment