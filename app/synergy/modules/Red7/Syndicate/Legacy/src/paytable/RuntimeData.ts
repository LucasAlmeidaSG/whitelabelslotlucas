namespace Red7.Syndicate.Legacy.Paytable
{
    export interface RuntimeData extends MegaDrop.Legacy.Paytable.RuntimeData
    {
        megaDropController: IController;
    }
}
