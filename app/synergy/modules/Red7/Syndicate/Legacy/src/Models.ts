namespace Red7.Syndicate
{
    export interface Models extends MegaDrop.Models
    {
        megaDropWin: number;
        megaDrop: Models.MegaDropJackpots;
        megaDropConfig: MegaDrop.Models.MegaDropConfig;
        syndicate?: Models.Syndicate;
    }

    export namespace Models
    {
        export function clear(models: Models)
        {
            MegaDrop.Models.clear(models);

            models.megaDrop = models.megaDrop || [] as Models.MegaDropJackpots;
            Models.MegaDropJackpots.clear(models.megaDrop);

            models.syndicate = models.syndicate || {} as Models.Syndicate;
            Models.Syndicate.clear(models.syndicate);

            models.megaDropWin = 0;
        }
    }
}
