namespace Red7.Syndicate
{
    export class Game extends Red7.MegaDrop.Game
    {
        public readonly settings: Game.Settings;

        protected _context: Game.Context;
        protected _syndicateController: Red7.Syndicate.IController;
        protected _models: Models;

        /** Gets the syndicate controller for this game */
        public get syndicateController() { return this._syndicateController; }

        /** Gets the models for this game. */
        public get models() { return this._models; }

        public constructor(settings: Game.Settings)
        {
            super(settings);
        }

        protected populateExternalHelpData(data: { [name: string]: any }): void
        {
            super.populateExternalHelpData(data);

            const gameModeKey = "gamemode";
            data[gameModeKey] = this.syndicateController.gameMode;
        }
    }

    export namespace Game
    {
        export interface Context extends Red7.MegaDrop.Game.Context
        {
            game: Red7.Syndicate.Game;
        }

        export interface Settings extends Red7.MegaDrop.Game.Settings
        {
            syndicateController: Red7.Syndicate.IController.Settings;
        }
    }
}
