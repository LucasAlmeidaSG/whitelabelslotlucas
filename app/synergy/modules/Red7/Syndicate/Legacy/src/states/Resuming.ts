namespace Red7.Syndicate.States
{
    @RS.HasCallbacks
    export class ResumingState<TContext extends Game.Context = Game.Context> extends RS.Slots.GameState.ResumingState<TContext>
    {
        protected async onDialogAccepted(): Promise<void>
        {
            await super.onDialogAccepted();

            if (this._context.game.syndicateController)
            {
                this._context.game.syndicateController.setupSuspense(this._context.game.models.seed, this._context.game.models.megaDropWin > 0);
            }
        }
    }
}