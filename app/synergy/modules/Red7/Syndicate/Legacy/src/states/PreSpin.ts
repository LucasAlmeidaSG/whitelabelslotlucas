namespace Red7.Syndicate.States
{
    @RS.HasCallbacks
    export class PreSpinState<TContext extends Game.Context = Game.Context> extends RS.Slots.GameState.PreSpinState<TContext>
    {
        @RS.AutoDisposeOnSet protected _showButtonAreaHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _winDialogChangeHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _uiEnabledHandle: RS.IDisposable;

        protected onLogicResponseReceived(response: RS.Engine.IResponse)
        {
            super.onLogicResponseReceived(response);
            
            if (this._context.game.syndicateController)
            {
                this._context.game.syndicateController.setupSuspense(this._context.game.models.seed, this._context.game.models.megaDropWin > 0);
            }
        }

        /**
         * Client will handle 1051 error and show a custom dialog box.
         */
        protected onRecoverFromError()
        {
            super.onRecoverFromError();
            if (this._context.game.syndicateController.gameMode !== GameMode.None &&
                this._context.game.models.syndicate.teamWin)
            {
                // Disable buttons
                this._context.game.syndicateController.buttonEnabled = false;
                this._showButtonAreaHandle = this._context.game.arbiters.showButtonArea.declare(false);
                this._uiEnabledHandle = this._context.game.arbiters.uiEnabled.declare(false);
                this._winDialogChangeHandle = this._context.game.syndicateController.winDialogOpened.onChanged((isOpen) =>
                {
                    if (!isOpen)
                    {
                        this._context.game.syndicateController.buttonEnabled = true;
                        if (this._showButtonAreaHandle) { this._showButtonAreaHandle.dispose(); }
                        if (this._winDialogChangeHandle) { this._winDialogChangeHandle.dispose(); }
                        if (this._uiEnabledHandle) { this._uiEnabledHandle.dispose(); }
                    }
                });

                // Open 1051 dialog
                this._context.game.syndicateController.winDialog();
                this._context.game.models.syndicate.teamWin = false;
            }
        }
    }
}