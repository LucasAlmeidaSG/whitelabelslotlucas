namespace Red7.Syndicate.GLM
{
    @RS.HasCallbacks
    export class Engine<TModels extends Models = Models> extends MegaDrop.GLM.Engine<TModels>
    {
        public get onSyndicateGameModeObtained() { return this._megaDropComponent.onSyndicateGameModeObtained; }

        protected _megaDropComponent: Component;

        public constructor(public readonly settings: Engine.Settings, public readonly models: TModels)
        {
            super(settings, models);
        }

        protected createComponents(arr: RS.Engine.IComponent[])
        {
            this._megaDropComponent = new Component(this.settings.defaultGameMode, this.settings.forceGameMode);
            arr.push(this._megaDropComponent);
        }

        protected calculateBalances(gameResult: SG.GLM.GameResult)
        {
            const endBalance = this.models.customer.finalBalance;
            const shouldSubtractWin: boolean = !this.isEngineBalanceFrozen(gameResult);

            const resultsModel = this.getResultsModel(this.models);

            let lastBalance = RS.Models.Balances.clone(endBalance);
            // Start at the final balance, and work backwards through each spin result
            for (let i = this.models.spinResults.length - 1; i >= 0; --i)
            {
                const result = resultsModel[i];
                result.balanceBefore = lastBalance;
                result.balanceAfter = lastBalance;

                if (shouldSubtractWin)
                {
                    let totalWin = 0;
                    if (i == 0)
                    {
                        totalWin = gameResult.bgInfo.totalWagerWin;
                        result.balanceBefore = RS.Models.Balances.clone(endBalance);
                    }
                    else
                    {
                        totalWin = result.win.totalWin;
                        result.balanceBefore = RS.Models.Balances.clone(lastBalance);
                    }

                    for (const tier of this.models.megaDrop)
                    {
                        if (tier.awarded && !tier.syndicated)
                        {
                            totalWin += tier.actualWinValue;
                        }
                    }

                    this.fudgeWinFromBalances(result.balanceBefore, totalWin);
                    lastBalance = result.balanceBefore;
                }
            }
        }

        protected fillRequestPayload(payload: RS.Slots.Engine.ISpinRequest.Payload): SpinRequestPayload
        {
            const superData = super.fillRequestPayload(payload) as SpinRequestPayload;

            return {
                ...superData,
                selectedSyndicateTeam: this.models.syndicate.selectedTeamId,
                selectedSyndicateTeamName: this.models.syndicate.selectedTeamName
            }
        }
    }

    export namespace Engine
    {
        export interface Settings extends MegaDrop.GLM.Engine.Settings
        {
            initRequest: { new(networkSettings: SG.GLM.Engine.NetworkSettings, userSettings: SG.GLM.Engine.UserSettings): InitRequest; };
            spinRequest: { new(settings: SG.GLM.Engine.NetworkSettings, userSettings: SG.GLM.Engine.UserSettings): SpinRequest; };
            defaultGameMode?: GameMode;
            forceGameMode?: boolean;
        }
    }
}
