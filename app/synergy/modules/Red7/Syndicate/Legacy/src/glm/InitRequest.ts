/// <reference path="InitResponse.ts" />

namespace Red7.Syndicate.GLM
{
    export class InitRequest extends Red7.MegaDrop.GLM.InitRequest
    {
        protected createResponse(): InitResponse
        {
            return new InitResponse(this);
        }
    }
}
