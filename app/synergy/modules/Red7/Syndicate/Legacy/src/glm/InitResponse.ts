namespace Red7.Syndicate.GLM
{
    import XML = RS.Serialisation.XML;

    export class InitResponse extends Red7.MegaDrop.GLM.InitResponse
    {
        @XML.Property({ path: "AccountData init progressive", type: XML.ExplicitObject(JackpotData) })
        public jackpotData?: JackpotData;

        /**
         * 0 - off
         * 1 - megaDrop only
         * 2 - full syndicate
         */
        @XML.Property({ path: "AccountData BetConfiguration blob MDQ", type: XML.Number, ignoreIfNull: true })
        public syndicateMode?: number;
    }
}
