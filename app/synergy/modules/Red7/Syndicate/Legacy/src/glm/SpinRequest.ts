namespace Red7.Syndicate.GLM
{
    import XML = RS.Serialisation.XML;

    export interface SpinRequestPayload extends MegaDrop.GLM.SpinRequestPayload
    {
        selectedSyndicateTeam?: string;
        selectedSyndicateTeamName?: string;
    }

    export class SpinRequest extends Red7.MegaDrop.GLM.SpinRequest
    {
        protected createResponse(): SpinResponse
        {
            return new SpinResponse(this);
        }

        protected createResponseObject()
        {
            return new SpinRequest.Payload();
        }

        protected createRequest(payload: SpinRequestPayload): SpinRequest.Payload
        {
            const request = super.createRequest(payload) as SpinRequest.Payload;
            request.syndicationId = payload.selectedSyndicateTeam;
            request.syndicationName = payload.selectedSyndicateTeamName;
            return request;
        }
    }

    export namespace SpinRequest
    {
        @XML.Type("GameRequest")
        export class Payload extends Red7.MegaDrop.GLM.SpinRequest.Payload
        {
            @XML.Property({ path: "progressive", type: XML.Array(XML.ExplicitObject(JackpotInstanceData)) })
            public jackpots?: JackpotInstanceData[];

            @XML.Property({ path: "progressive SyndicationId", type: XML.String, ignoreIfNull: true })
            public syndicationId?: string;

            @XML.Property({ path: "progressive SyndicationName", type: XML.String, ignoreIfNull: true })
            public syndicationName?: string;
        }
    }
}
