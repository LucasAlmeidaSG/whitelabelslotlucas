namespace Red7.Syndicate.GLM
{
    import XML = RS.Serialisation.XML;

    export class SpinResponse extends Red7.MegaDrop.GLM.SpinResponse
    {
        @XML.Property({ path: "AccountData Progressive", type: XML.ExplicitObject(BetJackpotData) })
        public betJackpotData?: BetJackpotData;

        /**
         * 0 - off
         * 1 - megaDrop only
         * 2 - full syndicate
         */
        @XML.Property({ path: "GameResult MDQInfo .value", type: XML.Number })
        public syndicateMode?: number;

        @XML.Property({ path: "GameResult BGInfo .syndicate", type: XML.Boolean })
        public syndicate?: boolean;
    }
}
