namespace Red7.Syndicate.JackpotWin
{
    export class View extends MegaDrop.JackpotWin.View
    {
        /**
         * Creates the Jackpot Win.
         */
        public setUpComponents(winSettings: View.JackpotWinSettings, mainContainer?: RS.Flow.Container, syndicated: boolean = false): void
        {
            this._jackpotWinSettings = winSettings;
            if (mainContainer) { this._mainContainer = mainContainer };

            this.createFlareContainer();
            this.createTopParticles();
            this.createLightFlares();

            this.createWeight();
            this.createExplosionParticles();
            this.createJackpotTierText();
            if (!syndicated)
            {
                this.createCountupText();
            }

            this.handleOrientationChanged(RS.Orientation.currentOrientation);
        }

        protected async playIntro()
        {
            if (this._skipped) { return; }

            await super.playIntro();

            if (!this._countupText)
            {
                await RS.Tween.wait(this.settings.countupTotalTime);
                this._countUpEndSound = RS.Audio.play(MegaDrop.Assets.MegaDrop.Sounds.WinAmount);
                this.onComplete();
            }
        }
    }

    export namespace View
    {
        export import JackpotWinSettings = MegaDrop.JackpotWin.View.JackpotWinSettings;
        export import Settings = MegaDrop.JackpotWin.View.Settings;
        export import OrientationSettings = MegaDrop.JackpotWin.View.OrientationSettings;
        export import LightFlareSettings = MegaDrop.JackpotWin.View.LightFlareSettings;
        export import WeightSettings = MegaDrop.JackpotWin.View.WeightSettings;
        export import ScreenShakeSettings = MegaDrop.JackpotWin.View.ScreenShakeSettings;
    }
}
