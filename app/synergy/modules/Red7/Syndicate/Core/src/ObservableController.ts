namespace Red7.Syndicate
{
    export class ObservableController
    {
        @RS.AutoDisposeOnSet private _seedValueObservables: RS.Observable<number>[] | null = null;
        public get seedValueObservables(): number[]
        {
            const value = [];
            this._seedValueObservables.forEach((v) => value.push(v.value));
            return value;
        }

        @RS.AutoDisposeOnSet private _currentValueObservables: RS.Observable<number>[] | null = null;
        public get currentValueObservables(): number[]
        {
            const value = [];
            this._currentValueObservables.forEach((v) => value.push(v.value));
            return value;
        }

        @RS.AutoDisposeOnSet private _dropValueObservables: RS.Observable<number>[] | null = null;
        public get dropValueObservables(): number[]
        {
            const value = [];
            this._dropValueObservables.forEach((v) => value.push(v.value));
            return value;
        }

        @RS.AutoDisposeOnSet private _dropValueInRequestedCurrencyObservables: RS.Observable<number>[] | null = null;
        public get dropValueInRequestedCurrencyObservables() { return this._dropValueInRequestedCurrencyObservables; }

        @RS.AutoDisposeOnSet private _playerCountValueObservables: RS.Observable<number>[] | null = null;
        public get playerCountValueObservables() { return this._playerCountValueObservables; }

        @RS.AutoDisposeOnSet private _winEligibilityPeriodsObservables: RS.Observable<number>[] | null = null;
        public get winEligibilityPeriodsObservables() { return this._winEligibilityPeriodsObservables; }

        public constructor(jackpotCount: number)
        {
            this.initialiseObservables(jackpotCount);
        }

        public updateObservables(index: number, newData: Models.MegaDropJackpot)
        {
            this._seedValueObservables[index].value = newData.seedValue;
            this._currentValueObservables[index].value = newData.currentValue;
            this._dropValueObservables[index].value = newData.dropValue;
            this._dropValueInRequestedCurrencyObservables[index].value = newData.dropValueInRequestedExchangeRate;

            if (newData.syndication)
                {
                    if (newData.syndication.playerCount != null)
                    {
                        this._playerCountValueObservables[index].value = newData.syndication.playerCount;
                    }

                    if (newData.syndication.threshold != null)
                    {
                        this._winEligibilityPeriodsObservables[index].value = newData.syndication.threshold;
                    }
                }
        }

        public newJackpotPanelData(index: number): MegaDrop.Elements.JackpotElement.RuntimeData
        {
            const runtimeData =
            {
                seedValue: this._seedValueObservables[index] = new RS.Observable(0),
                currentValue: this._currentValueObservables[index] = new RS.Observable(0),
                dropValue: this._dropValueObservables[index] = new RS.Observable(0),
                dropValueInRequestedCurrency: this._dropValueInRequestedCurrencyObservables[index] = new RS.Observable(0),
                jackpotName: index.toString()
            };

            this._playerCountValueObservables[index] = new RS.Observable(0);
            this._winEligibilityPeriodsObservables[index] = new RS.Observable(0);

            return runtimeData;
        }

        protected initialiseObservables(jackpotCount: number)
        {
            this._seedValueObservables = new Array<RS.Observable<number>>(jackpotCount);
            this._currentValueObservables = new Array<RS.Observable<number>>(jackpotCount);
            this._dropValueObservables = new Array<RS.Observable<number>>(jackpotCount);
            this._playerCountValueObservables = new Array<RS.Observable<number>>(jackpotCount);
            this._winEligibilityPeriodsObservables = new Array<RS.Observable<number>>(jackpotCount);
            this._dropValueInRequestedCurrencyObservables = new Array<RS.Observable<number>>(jackpotCount);
        }
    }
}
