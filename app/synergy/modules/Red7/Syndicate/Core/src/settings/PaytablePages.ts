namespace Red7.Syndicate.Settings.Paytable
{
    const jackpotsLogo: RS.Flow.Image.Settings =
    {
        dock: RS.Flow.Dock.Float,
        floatPosition: { x: 0.5, y: 0.5 },
        scaleFactor: 1.5,
        kind: RS.Flow.Image.Kind.SpriteFrame,
        asset: Red7.MegaDrop.Assets.MegaDrop.Jackpots,
        animationName: Red7.MegaDrop.Assets.MegaDrop.Jackpots.logo
    };

    export function page1(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line1,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.Syndicate.Paytable.Line2,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page2(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line3,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.Syndicate.Paytable.Line4,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page3(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line5,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page4(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line6,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page5(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line7,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.Syndicate.Paytable.Line8,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page6(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line9,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.Syndicate.Paytable.Line10,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page7(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line11,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page8(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line12,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.Syndicate.Paytable.Line13,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                },
                {
                    text: Translations.Syndicate.Paytable.Line14,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };

    export function page9(textSettings: RS.Flow.Label.Settings): RS.Paytable.Pages.TextPage.Settings
    {
        return {
            ...RS.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                paragraphSettings: textSettings
            },
            elements:
            [
                {
                    image: jackpotsLogo,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.Image
                },
                {
                    text: Translations.Syndicate.Paytable.Line15,
                    kind: RS.Paytable.Pages.TextPage.ElementKind.DefaultText
                }
            ]
        };
    };
}
