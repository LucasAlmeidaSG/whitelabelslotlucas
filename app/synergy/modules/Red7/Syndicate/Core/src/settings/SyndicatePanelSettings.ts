/// <reference path="../generated/Translations.ts" />
/// <reference path="../generated/Assets.ts" />
/// <reference path="../models/Syndicate.ts" />

namespace Red7.Syndicate.Settings.SyndicatePanel
{
    import Flow = RS.Flow;

    export const defaultTextSettings: Flow.Label.Settings =
    {
        dock: Flow.Dock.Fill,
        text: "",
        font: RS.Rendering.Assets.Fonts.MyriadPro.Bold,
        fontSize: 32,
        canWrap: true,
        align: RS.Rendering.TextAlign.Middle,
        size: { w: 400, h: 70 },
        textColor: RS.Util.Colors.lightgrey,
        expand: Flow.Expand.VerticalOnly,
        sizeToContents: false
    };

    export const greenGradientPanelBackgroundSettings: Flow.Background.Settings =
    {
        name: "Syndicate Panel Background",
        ignoreParentSpacing: true,
        dock: Flow.Dock.Fill,
        kind: Flow.Background.Kind.Gradient,
        gradient:
        {
            type: RS.Rendering.Gradient.Type.Vertical,
            stops:
            [
                {
                    color: new RS.Util.Color(0.03529411764705882, 0.17254901960784313, 0.09803921568627451, 1),
                    offset: 0
                },
                {
                    color: new RS.Util.Color(0.03529411764705882, 0.17254901960784313, 0.09803921568627451, 1),
                    offset: 0.65
                },
                {
                    color: new RS.Util.Color(0.027450980392156862, 0.4470588235294118, 0.25882352941176473, 1),
                    offset: 1
                }
            ]
        },
        textureSize: { w: 1, h: 1 }
    };

    export const defaultJackpotSettings: MegaDrop.Elements.JackpotElement.Settings[] =
    [
        {
            ...MegaDrop.Settings.EpicJackpotElement,
            dock: Flow.Dock.Top,
            scaleFactor: 1.0,
            padding: { top: 50, left: 20, right: 20, bottom: 20 }
        },
        {
            ...MegaDrop.Settings.MajorJackpotElement,
            dock: Flow.Dock.Top,
            scaleFactor: 1.0
        },
        {
            ...MegaDrop.Settings.MinorJackpotElement,
            dock: Flow.Dock.Top,
            scaleFactor: 1.0
        }
    ];

    export const defaultButtonImage: RS.Flow.Image.SpritesheetFrameSettings =
    {
        dock: Flow.Dock.Float,
        floatPosition: { x: 0.5, y: 0.5 },
        kind: Flow.Image.Kind.SpriteFrame,
        asset: Assets.Syndicate.Button,
        animationName: Assets.Syndicate.Button.up,
        expand: RS.Flow.Expand.Allowed,
        scaleFactor: 0.8,
        sizeToContents: true
    };

    export const defaultCurrentTeamTextElement: Elements.SyndicatePanel.CurrentTeamText =
    {
        kind: Elements.SyndicatePanel.ElementKind.CurrentTeamText,
        label: defaultTextSettings
    };

    export const defaultPlayerCountTextElement: Elements.SyndicatePanel.PlayerCountText =
    {
        kind: Elements.SyndicatePanel.ElementKind.PlayerCountText,
        label:
        {
            ...defaultTextSettings,
            fontSize: 48,
            textColor: RS.Util.Colors.white
        }
    };

    export const defaultCurrentTeamJackpotElement: Elements.SyndicatePanel.CurrentTeamJackpot =
    {
        kind: Elements.SyndicatePanel.ElementKind.CurrentTeamJackpot,
        container:
        {
            name: "Current Team Container",
            dock: Flow.Dock.Top,
            sizeToContents: true
        }
    };

    export const defaultOtherTeamJackpotElement: Elements.SyndicatePanel.OtherTeamsJackpots =
    {
        kind: Elements.SyndicatePanel.ElementKind.OtherTeamsJackpots,
        list:
        {
            name: "Other Teams List Landscape",
            dock: Flow.Dock.Top,
            direction: Flow.List.Direction.TopToBottom,
            scaleFactor: 0.6,
            sizeToContents: true
        }
    };

    export const defaultMegaDropElement: Elements.SyndicatePanel.MegaDrop =
    {
        kind: Elements.SyndicatePanel.ElementKind.MegaDrop,
        listSettings:
        {
            name: "Mega Drop List",
            dock: Flow.Dock.Fill,
            direction: Flow.List.Direction.TopToBottom
        }
    };

    export const defaultChangeTeamButtonElement: Elements.SyndicatePanel.ChangeTeamButton =
    {
        kind: Elements.SyndicatePanel.ElementKind.ChangeTeamButton,
        button:
        {
            name: "Change Team Button",
            dock: RS.Flow.Dock.Top,
            image: defaultButtonImage,
            pressImage:
            {
                ...defaultButtonImage,
                animationName: Assets.Syndicate.Button.down,
            },
            hoverImage:
            {
                ...defaultButtonImage,
                animationName: Assets.Syndicate.Button.hover,
            },
            disabledImage:
            {
                ...defaultButtonImage,
                animationName: Assets.Syndicate.Button.disabled
            },
            textLabel:
            {
                dock: RS.Flow.Dock.Fill,
                font: RS.Rendering.Assets.Fonts.MyriadPro.Black,
                fontSize: 48,
                text: Translations.Syndicate.ChangeTeam,
                sizeToContents: false,
                size: { w: 310, h: 100 }
            },
            sizeToContents: false,
            hitArea: new RS.Rendering.RoundedRectangle(25, 0, 352, 96, 20),
            seleniumId: "ChangeSelectTeamButton"
        }
    };

    export const winDialog: RS.Flow.Dialog.Settings =
    {
        name: "Congratulations Dialog",
        dock: RS.Flow.Dock.Float,
        floatPosition: { x: 0.5 , y: 0.5 },
        background:
        {
            name: "Congratulations Background",
            dock: RS.Flow.Dock.Fill,
            kind: RS.Flow.Background.Kind.Image,
            asset: Assets.Syndicate.WinDialogBackground
        },
        titleLabel:
        {
            name: "Congratulations Title",
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.35 },
            text: Translations.Syndicate.TeamWin,
            font: RS.Rendering.Assets.Fonts.MyriadPro.Regular,
            fontSize: 75,
            align: RS.Rendering.TextAlign.Middle,
            size: { w: 750, h: 200 },
            sizeToContents: false,
            layers: MegaDrop.Settings.EpicJackpotElement.valueMeter.nameLabel.layers
        },
        messageLabel:
        {
            name: "Congratulations Message",
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.55 },
            text: Translations.Syndicate.Share,
            font: RS.Rendering.Assets.Fonts.MyriadPro.Bold,
            fontSize: 40,
            canWrap: true,
            align: RS.Rendering.TextAlign.Middle,
            size: { w: 750, h: 100 },
            sizeToContents: false
        },
        list:
        {
            name: "Congratulations Button List",
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.8 },
            direction: RS.Flow.List.Direction.TopToBottom,
            sizeToContents: true
        },
        buttons:
        [
            {
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 },
                ignoreParentSpacing: true,
                textLabel:
                {
                    dock: RS.Flow.Dock.Float,
                    floatPosition: { x: 0.5, y: 0.5 },
                    text: Translations.Syndicate.Okay,
                    font: RS.Rendering.Assets.Fonts.MyriadPro.Bold,
                    fontSize: 60,
                    align: RS.Rendering.TextAlign.Middle,
                    size: { w: 240, h: 80 },
                    sizeToContents: false
                },
                background:
                {
                    dock: RS.Flow.Dock.Fill,
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.Syndicate.Button,
                    frame: Assets.Syndicate.Button.up,
                    sizeToContents: true
                },
                hoverbackground:
                {
                    dock: RS.Flow.Dock.Fill,
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.Syndicate.Button,
                    frame: Assets.Syndicate.Button.hover,
                    sizeToContents: true
                },
                pressbackground:
                {
                    dock: RS.Flow.Dock.Fill,
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.Syndicate.Button,
                    frame: Assets.Syndicate.Button.down,
                    sizeToContents: true
                },
                disabledbackground:
                {
                    dock: RS.Flow.Dock.Fill,
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.Syndicate.Button,
                    frame: Assets.Syndicate.Button.disabled,
                    sizeToContents: true
                },
                scaleFactor: 0.8,
                hitArea: new RS.Rendering.RoundedRectangle(50, 55, 450, 130, 20)
            }
        ],
        sizeToContents: false,
        size: { w: 1000, h: 711 },
        enforceMinimumSize: true
    };

    export const defaultQualifyingTimerElement: Elements.SyndicatePanel.QualifyingTimerText =
    {
        kind: Elements.SyndicatePanel.ElementKind.QualifyingTimerText,
        label:
        {
            ...defaultTextSettings,
            textColor: RS.Util.Colors.white,
            sizeToContents: true
        }
    };

    export const defaultSettings: Elements.SyndicatePanel.Settings =
    {
        name: "Syndicate Panel",
        logoImage:
        {
            kind: Flow.Image.Kind.SpriteFrame,
            asset: MegaDrop.Assets.MegaDrop.Jackpots,
            animationName: MegaDrop.Assets.MegaDrop.Jackpots.logo,
            sizeToContents: true
        },
        jackpots: defaultJackpotSettings,
        showJackpotBackground: false,
        sizeToContents: true,
        dock: Flow.Dock.Fill,
        expand: Flow.Expand.Allowed,
        contentAlignment: { x: 0.5, y: 0.4 },
        listSettings:
        {
            name: "Syndicate Panel List",
            dock: Flow.Dock.Top,
            direction: Flow.List.Direction.TopToBottom,
            spacing: { top: 0, left: 5, right: 5, bottom: 10 },
            evenlySpaceItems: false,
            legacy: false,
            sizeToContents: false,
            size: { w: 600, h: 500 },
            overflowMode: RS.Flow.OverflowMode.Overflow
        },
        otherTeamsJackpotList:
        {
            name: "Syndicate Panel List",
            dock: Flow.Dock.Top,
            spacing: { top: 0, left: 5, right: 5, bottom: 10 },
            evenlySpaceItems: false,
            legacy: false,
            sizeToContents: true,
            direction: Flow.List.Direction.TopToBottom
        },
        padding: Flow.Spacing.axis(10, 5),
        elements:
        [
            {
                kind: Elements.SyndicatePanel.ElementKind.Background,
                background: greenGradientPanelBackgroundSettings
            },
            defaultCurrentTeamTextElement,
            defaultCurrentTeamJackpotElement,
            defaultQualifyingTimerElement,
            defaultPlayerCountTextElement,
            defaultOtherTeamJackpotElement,
            defaultChangeTeamButtonElement
        ],
        megaDropElements:
        [
            {
                kind: Elements.SyndicatePanel.ElementKind.Background,
                background: greenGradientPanelBackgroundSettings
            },
            defaultMegaDropElement,
            {
                ...defaultChangeTeamButtonElement,
                button:
                {
                    ...defaultChangeTeamButtonElement.button,
                    hitArea: new RS.Rendering.RoundedRectangle(20, 0, 352, 96, 20)
                }
            }
        ],
        teamTranslations:
        {
            [Models.Teams.Epic]: Translations.Syndicate.TeamEpic,
            [Models.Teams.Major]: Translations.Syndicate.TeamMajor,
            [Models.Teams.Minor]: Translations.Syndicate.TeamMinor,
            [Models.Teams.Alone]: Translations.Syndicate.PlayAlone
        },
        jackpotTranslations:
        {
            [Models.Teams.Epic]: Translations.Syndicate.EpicJackpot,
            [Models.Teams.Major]: Translations.Syndicate.MajorJackpot,
            [Models.Teams.Minor]: Translations.Syndicate.MinorJackpot,
            [Models.Teams.Alone]: Translations.Syndicate.PlayAlone
        }
    };

    export const defaultLandscapeSettings: Elements.SyndicatePanel.Settings =
    {
        ...defaultSettings,
        dock: RS.Flow.Dock.Right,
        listSettings:
        {
            ...defaultSettings.listSettings,
            size: { w: 400, h: 500 },
        }
    };

    export const defaultPortraitSettings: Elements.SyndicatePanel.Settings =
    {
        ...defaultSettings,
        dock: RS.Flow.Dock.Top,
        showJackpotBackground: false,
        elements:
        [
            {
                kind: Elements.SyndicatePanel.ElementKind.Background,
                background:
                {
                    dock: Flow.Dock.Float,
                    dockAlignment: { x: 0.5, y: 0.1 },
                    floatPosition: { x: 0.5, y: 0 },
                    kind: Flow.Background.Kind.Image,
                    asset: Assets.Syndicate.ShadowBackground,
                    sizeToContents: false,
                    size: { w: 1800, h: 1200 }
                }
            },
            defaultCurrentTeamJackpotElement,
            defaultCurrentTeamTextElement,
            defaultQualifyingTimerElement,
            defaultPlayerCountTextElement,
            {
                ...defaultOtherTeamJackpotElement,
                list:
                {
                    ...defaultOtherTeamJackpotElement.list,
                    name: "Other Teams List Portrait",
                    direction: Flow.List.Direction.LeftToRight
                }
            },
            {
                ...defaultChangeTeamButtonElement,
                button:
                {
                    ...defaultChangeTeamButtonElement.button,
                    hitArea: new RS.Rendering.RoundedRectangle(120, 0, 352, 96, 20)
                }
            }
        ],
        megaDropElements:
        [
            {
                kind: Elements.SyndicatePanel.ElementKind.Background,
                background:
                {
                    dock: Flow.Dock.Float,
                    floatPosition: { x: 0.5, y: 0.55 },
                    kind: Flow.Background.Kind.Image,
                    asset: Assets.Syndicate.ShadowBackground,
                    scaleFactor: 3,
                    sizeToContents: true
                }
            },
            defaultMegaDropElement,
            {
                ...defaultChangeTeamButtonElement,
                button:
                {
                    ...defaultChangeTeamButtonElement.button,
                    hitArea: new RS.Rendering.RoundedRectangle(120, 0, 352, 96, 20)
                }
            }
        ],
        otherTeamsJackpotList:
        {
            ...defaultSettings.otherTeamsJackpotList,
            direction: Flow.List.Direction.LeftToRight
        }
    };

    export const defaultMobileLandscapeSettings: Elements.SyndicatePanel.Settings =
    {
        ...defaultSettings,
        dock: Flow.Dock.Left,
        listSettings:
        {
            ...defaultSettings.listSettings,
            direction: Flow.List.Direction.TopToBottom,
            spacing: Flow.Spacing.vertical(1),
            overflowMode: RS.Flow.OverflowMode.Shrink,
            size: { w: 400, h: 690 }
        },
        logoImage: null,
        size: { w: 360, h: 100 },
        sizeToContents: false,
        elements:
        [
            {
                kind: Elements.SyndicatePanel.ElementKind.Background,
                background: greenGradientPanelBackgroundSettings
            },
            defaultCurrentTeamJackpotElement,
            defaultCurrentTeamTextElement,
            defaultQualifyingTimerElement,
            defaultPlayerCountTextElement,
            defaultOtherTeamJackpotElement,
            {
                ...defaultChangeTeamButtonElement,
                button:
                {
                    ...defaultChangeTeamButtonElement.button,
                    hitArea: new RS.Rendering.RoundedRectangle(-10, 0, 352, 96, 20)
                }
            }
        ],
        megaDropElements:
        [
            {
                kind: Elements.SyndicatePanel.ElementKind.Background,
                background: greenGradientPanelBackgroundSettings
            },
            defaultMegaDropElement,
            defaultMegaDropElement,
            {
                ...defaultChangeTeamButtonElement,
                button:
                {
                    ...defaultChangeTeamButtonElement.button,
                    hitArea: new RS.Rendering.RoundedRectangle(-5, 0, 352, 96, 20)
                }
            }
        ]
    };

    export const defaultMobilePortraitSettings: Elements.SyndicatePanel.Settings =
    {
        ...defaultPortraitSettings
    };
}
