namespace Red7.Syndicate.Settings.TeamSelection
{
    import Flow = RS.Flow;

    const defaultButtonImage: Flow.Image.SpritesheetFrameSettings =
    {
        dock: Flow.Dock.Fill,
        kind: Flow.Image.Kind.SpriteFrame,
        asset: Assets.Syndicate.TeamSelection,
        animationName: Assets.Syndicate.TeamSelection.button_up,
        sizeToContents: true
    };

    export const defaultJackpotBackground: Flow.Image.SpritesheetFrameSettings =
    {
        name: "Jackpot Card Background",
        dock: RS.Flow.Dock.Fill,
        kind: Flow.Image.Kind.SpriteFrame,
        asset: Assets.Syndicate.TeamSelection,
        animationName: Assets.Syndicate.TeamSelection.background_epic,
        sizeToContents: true,
        border: { top: 14, bottom: 54, left: 34, right: 34 }
    }

    export const defaultCardButton: Flow.Button.Settings =
    {
        name: "Jackpot Card Button",
        textLabel: null,
        image: defaultJackpotBackground,
        pressImage: defaultJackpotBackground,
        hoverImage: defaultJackpotBackground,
        hitArea: new RS.Rendering.ReadonlyRoundedRectangle(10, 50, 370, 330, 10),
        sizeToContents: true
    }

    export const defaultSettings: Red7.Syndicate.Elements.TeamSelection.Settings =
    {
        dock: RS.Flow.Dock.Top,
        spacing: { top: 100, left: 0, bottom: 0, right: 0 },
        sizeToContents: true,
        title:
        {
            name: "Team Selection Title Label",
            dock: RS.Flow.Dock.Bottom,
            font: RS.Rendering.Assets.Fonts.MyriadPro.Black,
            fontSize: 64,
            text: Translations.Syndicate.ChooseTeam,
            size: { w: 1200, h: 70 },
            sizeToContents: false,
            layers:
            [
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.white,
                    layerType: RS.Rendering.TextOptions.LayerType.Fill,
                    offset: { x: 0.00, y: 0.00 }
                },
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.black,
                    layerType: RS.Rendering.TextOptions.LayerType.Border,
                    size: 1
                }
            ]
        },
        description:
        {
            name: "Team Selection Description Label",
            dock: RS.Flow.Dock.Bottom,
            font: RS.Rendering.Assets.Fonts.MyriadPro.Black,
            fontSize: 32,
            canWrap: true,
            align: RS.Rendering.TextAlign.Middle,
            text: Translations.Syndicate.PlayInATeam,
            size: { w: 1200, h: 70 },
            sizeToContents: false,
            layers:
            [
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.white,
                    layerType: RS.Rendering.TextOptions.LayerType.Fill,
                    offset: { x: 0.00, y: 0.00 }
                },
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.black,
                    layerType: RS.Rendering.TextOptions.LayerType.Border,
                    size: 1
                }
            ]
        },
        jackpots:
        [
            {
                ...MegaDrop.Settings.EpicJackpotElement,
                background: null
            },
            {
                ...MegaDrop.Settings.MajorJackpotElement,
                background: null
            },
            {
                ...MegaDrop.Settings.MinorJackpotElement,
                background: null
            }
        ],
        jackpotCardButton:
        [
            {
                name: "Jackpot Card Button",
                textLabel: null,
                image:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.GoldBackground,
                },
                pressImage:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.GoldBackground,
                },
                hoverImage:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.GoldBackground,
                },
                hitArea: new RS.Rendering.ReadonlyRoundedRectangle(-10, 25, 370, 375, 35),
                seleniumId: "TeamEpicCardButton",
                sizeToContents: true
            },
            {
                name: "Jackpot Card Button",
                textLabel: null,
                image:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.GreenBackground,
                },
                pressImage:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.GreenBackground,
                },
                hoverImage:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.GreenBackground,
                },
                hitArea: new RS.Rendering.ReadonlyRoundedRectangle(-10, 25, 370, 375, 35),
                seleniumId: "TeamMajorCardButton",
                sizeToContents: true
            },
            {
                name: "Jackpot Card Button",
                textLabel: null,
                image:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.BlueBackground,
                },
                pressImage:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.BlueBackground,
                },
                hoverImage:
                {
                    ...defaultJackpotBackground,
                    kind: RS.Flow.Image.Kind.Bitmap,
                    asset: Assets.Syndicate.BlueBackground,
                },
                hitArea: new RS.Rendering.ReadonlyRoundedRectangle(-10, 25, 370, 375, 35),
                seleniumId: "TeamMinorCardButton",
                sizeToContents: true
            }
        ],
        playAloneButton:
        {
            name: "Play Alone Button",
            dock: RS.Flow.Dock.Bottom,
            image:
            {
                ...defaultButtonImage,
                asset: Assets.Syndicate.Button.Long,
                animationName: Assets.Syndicate.Button.Long.long_up,
            },
            pressImage:
            {
                ...defaultButtonImage,
                asset: Assets.Syndicate.Button.Long,
                animationName: Assets.Syndicate.Button.Long.long_down,
            },
            hoverImage:
            {
                ...defaultButtonImage,
                asset: Assets.Syndicate.Button.Long,
                animationName: Assets.Syndicate.Button.Long.long_hover
            },
            disabledImage:
            {
                ...defaultButtonImage,
                asset: Assets.Syndicate.Button.Long,
                animationName: Assets.Syndicate.Button.Long.long_disabled
            },
            textLabel:
            {
                dock: RS.Flow.Dock.Fill,
                font: RS.Rendering.Assets.Fonts.MyriadPro.Bold,
                fontSize: 60,
                text: Translations.Syndicate.PlayAlone,
                sizeToContents: false,
                size: { w: 700, h: 200 },
            },
            scaleFactor: 0.7,
            expand: RS.Flow.Expand.Disallowed,
            hitArea: new RS.Rendering.RoundedRectangle(75, 155, 820, 145, 50),
            spacing: { left: 10, right: 10, top: 90, bottom: 10 },
            seleniumId: "TeamPlayAloneButton"
        },
        background:
        {
            dock: Flow.Dock.Fill,
            kind: Flow.Background.Kind.SolidColor,
            color: RS.Util.Colors.black,
            borderSize: 0,
            alpha: 0.75
        },
        listSettings:
        {
            name: "Team Selection List",
            dock: Flow.Dock.Bottom,
            direction: Flow.List.Direction.TopToBottom,
            enforceMinimumSize: true,
            sizeToContents: false,
            size: { w: 1300, h: 950 }
        },
        jackpotsListSettings:
        {
            name: "Jackpots List",
            dock: Flow.Dock.Bottom,
            direction: Flow.List.Direction.RightToLeft,
            spacing: Flow.Spacing.all(40),
            sizeToContents: false,
            size: { w: 1300, h: 450 }
        },
        jackpotListSettings:
        {
            name: "Jackpot List",
            dock: Flow.Dock.Fill,
            direction: Flow.List.Direction.TopToBottom
        },
        playerCountLabelSettings:
        {
            dock: RS.Flow.Dock.Fill,
            font: RS.Rendering.Assets.Fonts.MyriadPro.Bold,
            fontSize: 48,
            text: Translations.Syndicate.PlayAlone,
            size: { w: 300, h: 80 },
            spacing: RS.Flow.Spacing.horizontal(20),
            sizeToContents: false,
            layers:
            [
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.white,
                    layerType: RS.Rendering.TextOptions.LayerType.Fill,
                    offset: { x: 0.00, y: 0.00 }
                },
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.black,
                    layerType: RS.Rendering.TextOptions.LayerType.Border,
                    size: 1
                }
            ]
        },
        tweenSettings: 
        {
            defaultScale: 1.0,
            hoverScale: 1.2,
            pulseRate: 1,
            pulseScale: 1.05
        }
    };

    export const alternateSettings: Syndicate.Elements.TeamSelection.Settings =
    {
        ...defaultSettings,
        jackpotCardButton:
        [
            { 
                ...defaultCardButton,
                seleniumId: "TeamEpicCardButton",
                image: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_epic },
                pressImage: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_epic },
                hoverImage: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_epic }
            },
            { 
                ...defaultCardButton,
                seleniumId: "TeamMajorCardButton",
                image: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_major },
                pressImage: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_major },
                hoverImage: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_major }
            },
            { 
                ...defaultCardButton,
                seleniumId: "TeamMinorCardButton",
                image: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_minor },
                pressImage: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_minor },
                hoverImage: { ...defaultJackpotBackground, animationName: Assets.Syndicate.TeamSelection.background_minor }
            }
        ],
        playAloneButton:
        {
            name: "Play Alone Button",
            dock: RS.Flow.Dock.Bottom,
            image: defaultButtonImage,
            pressImage:
            {
                ...defaultButtonImage,
                animationName: Assets.Syndicate.TeamSelection.button_down,
            },
            hoverImage:
            {
                ...defaultButtonImage,
                animationName: Assets.Syndicate.TeamSelection.button_hover
            },
            disabledImage:
            {
                ...defaultButtonImage,
                animationName: Assets.Syndicate.TeamSelection.button_disabled
            },
            textLabel:
            {
                dock: RS.Flow.Dock.Fill,
                font: RS.Rendering.Assets.Fonts.MyriadPro.Bold,
                fontSize: 60,
                text: Translations.Syndicate.PlayAlone,
                sizeToContents: false,
                size: { w: 700, h: 200 },
            },
            scaleFactor: 0.7,
            expand: RS.Flow.Expand.Disallowed,
            hitArea: new RS.Rendering.RoundedRectangle(75, 155, 820, 145, 50),
            spacing: { left: 10, right: 10, top: 90, bottom: 10 },
            seleniumId: "TeamPlayAloneButton"
        },
        jackpotsListSettings:
        {
            name: "Jackpots List",
            dock: Flow.Dock.Bottom,
            direction: Flow.List.Direction.RightToLeft,
            spacing: Flow.Spacing.all(22),
            sizeToContents: false,
            size: { w: 1300, h: 450 }
        }
    }
}