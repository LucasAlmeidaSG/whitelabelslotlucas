namespace Red7.Syndicate.Views
{
    @RS.HasCallbacks
    export class PaytableNew<TSettings extends PaytableNew.Settings = PaytableNew.Settings, TContext extends Syndicate.Paytable.Context = Syndicate.Paytable.Context> extends MegaDrop.Views.PaytableNew<TSettings, TContext>
    {
        protected getRuntimeData(): Syndicate.Paytable.RuntimeData
        {
            return {
                ...super.getRuntimeData(),
                megaDropController: this.context.game.syndicateController
            };
        }
    }

    export namespace PaytableNew
    {
        export import Settings = MegaDrop.Views.PaytableNew.Settings;
    }
}
