namespace Red7.Syndicate.Paytable
{
    export interface Game extends MegaDrop.Paytable.Game
    {
        models: IModels;
        syndicateController: IController;
    }

    export interface Context extends MegaDrop.Paytable.Context
    {
        game: Game;
    }

    export interface RuntimeData extends MegaDrop.Paytable.RuntimeData
    {
        megaDropController: IController;
    }
}
