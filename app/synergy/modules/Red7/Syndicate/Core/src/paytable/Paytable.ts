namespace Red7.Syndicate.Views
{
    @RS.HasCallbacks
    export class Paytable<TSettings extends Paytable.Settings = Paytable.Settings, TContext extends Syndicate.Paytable.Context = Syndicate.Paytable.Context> extends MegaDrop.Views.Paytable<TSettings, TContext>
    {
        protected getRuntimeData(): Syndicate.Paytable.RuntimeData
        {
            return {
                ...super.getRuntimeData(),
                megaDropController: this.context.game.syndicateController
            };
        }
    }

    export namespace Paytable
    {
        export import Settings = MegaDrop.Views.Paytable.Settings;
    }
}
