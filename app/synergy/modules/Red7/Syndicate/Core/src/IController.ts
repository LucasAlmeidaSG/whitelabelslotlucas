/// <reference path="elements/SyndicatePanel.ts" />
/// <reference path="settings/SyndicatePanelSettings.ts" />
/// <reference  path="elements/TeamSelection.ts" />
/// <reference path="settings/TeamSelection.ts" />

namespace Red7.Syndicate
{
    export interface IController<TSettings extends IController.Settings = IController.Settings> extends MegaDrop.IController<TSettings>
    {
        readonly onTeamSelected: RS.IEvent<string>;
        readonly onTeamSelectionOpened: RS.IEvent;

        readonly winDialogOpened: RS.IReadonlyObservable<boolean>;

        /** gets/sets syndicate game mode. Setter does nothing if value isn't null to prevent accidental changes while playing */
        gameMode: GameMode;

        /** The syndicate panel visibility */
        showPanel: boolean;
        isTeamSelectOpen: boolean;
        idle: boolean;

        /** Whether the change team button is enabled or not */
        buttonEnabled: boolean;

        /** The currency formatter used to render the jackpots and drop by functions in EUR. */
        currencyFormatter: RS.Localisation.CurrencyFormatter;

        update(megadropModel: MegaDrop.Models.MegaDropJackpots, syndicateModel?: Models.Syndicate): void;

        updatePanel(settings: Elements.SyndicatePanel.Settings, rebuild: boolean): void;

        openTeamSelection(): void;

        resetTimer(): void;

        winDialog(): void;

        /**
         * Creates and plays the winning view for Mega Drop.
         */
        playWinAnimation(jackpotName: string, jackpotWinnings: number, syndicated?: boolean): Promise<void>;
    }

    export const IController = RS.Controllers.declare<IController, IController.Settings>(RS.Strategy.Type.Instance);

    export enum GameMode
    {
        None,
        Alone,
        Syndicate
    }

    export namespace IController
    {
        export interface Settings extends MegaDrop.IController.Settings
        {
            panel: Elements.SyndicatePanel.Settings;
            teamSelection: Elements.TeamSelection.Settings;
        }
    }

    export const defaultControllerSettings: IController.Settings =
    {
        ...MegaDrop.defaultControllerSettings,
        panel: Settings.SyndicatePanel.defaultSettings,
        teamSelection: Settings.TeamSelection.defaultSettings
    };
}
