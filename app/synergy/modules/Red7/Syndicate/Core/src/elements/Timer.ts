namespace Red7.Syndicate.Elements
{
    export class Timer extends RS.Flow.BaseElement<Timer.Settings, SyndicatePanel.RuntimeData>
    {
        public settings: Timer.Settings;
        @RS.AutoDispose public onTimerEnd = RS.createSimpleEvent();
        @RS.AutoDispose public timer: RS.Observable<string> = new RS.Observable<string>("");

        protected _jackpotIndex: number;
        public get jackpotIndex() { return this._jackpotIndex; }

        protected _active: boolean;
        public get isActive() { return this._active; }

        private _qualifyingStartTime: number[] | null = null;
        private _internalTimer: any;
        private _timerReady: boolean = false;
        private _timerSwitched: boolean = false;
        private timePrefix = ["00", "0", ""];

        public constructor (settings: Timer.Settings, runtimeData: SyndicatePanel.RuntimeData, jackpotIndex: number)
        {
            super(settings, runtimeData);

            this.settings = settings;
            this._jackpotIndex = jackpotIndex;
            this._qualifyingStartTime = new Array<number>(runtimeData.jackpots.length);
            this.create();
        }

        /** Starts a timer after it has been reset to it's starting value. Resumes an existing timer. */
        public startTimer()
        {
            if (this._active) { return; }
            if (!this._timerReady)
            {
                this.timer.value = "";
                return;
            }
            this._active = true;
            this._timerSwitched = true;
        }

        /** Reset timer to starting value. Can be started automatically with oprional parameter or can be started
         *  manually afterwards using startTimer().
         */
        public resetTimer(autoStart: boolean = false)
        {
            this._qualifyingStartTime[this._jackpotIndex] = Date.now();

            const threshold = this.runtimeData.winEligibilityPeriods[this._jackpotIndex].value * 1000;

            const date = new Date(threshold);
            const minutes = date.getUTCMinutes().toString();
            const seconds = date.getUTCSeconds().toString();

            const time = `${this.timePrefix[minutes.length]}${minutes}:${this.timePrefix[seconds.length]}${seconds}`;
            this.timer.value = time;

            this._active = autoStart;
            this._timerReady = true;
        }

        /** Stops / Pauses the update loop of the timer. */
        public stopTimer()
        {
            this._active = false;
        }

        /** Update loop for timer. Will only change value when individual timer is set to active. */
        @RS.Callback
        protected update()
        {
            if (this._active)
            {
                if (this.runtimeData.winEligibilityPeriods[this._jackpotIndex].value <= 0) { return; }

                const threshold = this.runtimeData.winEligibilityPeriods[this._jackpotIndex].value * 1000;
                const diff = Date.now() - this._qualifyingStartTime[this._jackpotIndex];

                const date = new Date(threshold - diff);
                const minutes = date.getUTCMinutes().toString();
                const seconds = date.getUTCSeconds().toString();

                const time = `${this.timePrefix[minutes.length]}${minutes}:${this.timePrefix[seconds.length]}${seconds}`;
                this.timer.value = time;

                if (threshold - diff <= 0)
                {
                    if (!this._timerSwitched) { this.onTimerEnd.publish(); }
                    this._active = false;
                    this.timer.value = "";
                    this._timerReady = false;
                }
                this._timerSwitched = false;
            }
        }

        protected create()
        {
            this._internalTimer = setInterval(this.update, this.settings.updateFrequency);
            this.update();
        }
    }

    namespace Timer
    {
        export interface Settings
        {
            updateFrequency: number;
        }
    }
}
