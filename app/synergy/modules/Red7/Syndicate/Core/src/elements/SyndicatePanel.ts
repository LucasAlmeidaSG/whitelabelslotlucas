/// <reference path="../generated/Translations.ts" />
/// <reference path="../generated/Assets.ts" />

namespace Red7.Syndicate.Elements
{
    import Flow = RS.Flow;

    /**
     * Encapsulates a number jackpot elements.
     */
    @RS.HasCallbacks
    export class SyndicatePanel extends Flow.BaseElement<SyndicatePanel.Settings, SyndicatePanel.RuntimeData>
    {
        @RS.AutoDispose protected readonly _onTeamChanged = RS.createSimpleEvent();
        public get onTeamChanged() { return this._onTeamChanged.public; }

        @RS.AutoDispose protected readonly _newTimerText = RS.createSimpleEvent();
        public get newTimerText() { return this._newTimerText.public; }

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _list: Flow.List | null = null;

        @RS.AutoDisposeOnSet protected _currentTeamText: Flow.Label | null = null;
        @RS.AutoDisposeOnSet protected _playerCountText: Flow.Label | null = null;
        @RS.AutoDisposeOnSet protected _timerText: Flow.Label | null = null;
        public get timerText() { return this._timerText; }

        @RS.AutoDisposeOnSet protected _logo: Flow.Image | null = null;

        @RS.AutoDisposeOnSet protected _jackpotElements: MegaDrop.Elements.JackpotElement[] | null = null;
        @RS.AutoDisposeOnSet protected _changeTeamButton: Flow.Button | null = null;
        @RS.AutoDisposeOnSet protected _isChangeTeamButtonEnabled: boolean | null = null;

        @RS.AutoDisposeOnSet protected _teamTranslations: RS.Map<RS.Localisation.SimpleTranslationReference> | null;
        @RS.AutoDisposeOnSet protected _jackpotTranslations: RS.Map<RS.Localisation.SimpleTranslationReference> | null;

        private _currentTeam: string | null = null;
        private _currentTeamType: Models.Teams | null = null;
        @RS.AutoDisposeOnSet private _currentElements: RS.Flow.GenericElement[] = [];

        public get currentTeam(): string
        {
            return this._currentTeam;
        }

        /**
        *   Update panel to show current team or change to original Mega Drop layout
        *   @param newTeam
        */
        public set currentTeam(team: string)
        {
            this._currentTeam = team;
            this._currentTeamType = this.getTeamIndex(team);
            this.rebuildPanel();
        }

        public set currentTeamType(team: Models.Teams)
        {
            this._currentTeamType = team;
        }

        public get currentTeamType(): Models.Teams
        {
            return this._currentTeamType;
        }

        public get isChangeTeamButtonEnabled(): boolean
        {
            return this._isChangeTeamButtonEnabled;
        }

        public set isChangeTeamButtonEnabled(value)
        {
            this._isChangeTeamButtonEnabled = value;
            this._changeTeamButton.enabled = value;
            this._changeTeamButton.alpha = value ? 1 : 0;
        }

        public constructor(settings: SyndicatePanel.Settings, public readonly runtimeData: SyndicatePanel.RuntimeData)
        {
            super(settings, runtimeData);
            this.create();
            this.currentTeam = null;

            this._teamTranslations = settings.teamTranslations;
            this._jackpotTranslations = settings.jackpotTranslations;
        }

        public updateBackground(settings: RS.Flow.Background.Settings)
        {
            const currentIndex = this._bg ? this.children.indexOf(this._bg) : 0;

            this._bg = RS.Flow.background.create(settings);
            this.addChild(this._bg, currentIndex);
        }

        /**
        * Apply new settings to flow elements
        * @param settings
        * @param rebuild Remove and create elements again
        */
        public apply(settings: SyndicatePanel.Settings, rebuild: boolean = false)
        {
            super.apply(settings);

            this.settings.background = settings.background;
            this.settings.logoImage = settings.logoImage;
            this.settings.jackpots = settings.jackpots;
            this.settings.showJackpotBackground = settings.showJackpotBackground;
            this.settings.currentTeamJackpotContainer = settings.currentTeamJackpotContainer;
            this.settings.otherTeamsJackpotList = settings.otherTeamsJackpotList;
            this.settings.currentTeamText = settings.currentTeamText;
            this.settings.playerCountText = settings.playerCountText;
            this.settings.changeTeamButton = settings.changeTeamButton;
            this.settings.elements = settings.elements;
            this.settings.megaDropElements = settings.megaDropElements;
            this.settings.listSettings = settings.listSettings;
            this.settings.timerUpdateFrequency = settings.timerUpdateFrequency;
            this.settings.teamTranslations = settings.teamTranslations;
            this.settings.jackpotTranslations = settings.jackpotTranslations;

            if (settings.logoImage)
            {
                if (!this._logo || rebuild)
                {
                    this._logo = Flow.image.create(settings.logoImage, this);
                }
                else
                {
                    this._logo.apply(settings.logoImage);
                }
            }
            else if (this._logo)
            {
                this._logo = null;
            }

            if (this._list && settings.listSettings)
            {
                this._list.apply(settings.listSettings);
                this.moveToTop(this._list);
            }

            if (this._jackpotElements && settings.jackpots)
            {
                for (let i = 0; i < settings.jackpots.length; i++)
                {
                    if (this._jackpotElements[i] && settings.jackpots[i])
                    {
                        this._jackpotElements[i].apply(settings.jackpots[i]);
                    }
                }
            }

            if (rebuild)
            {
                this.rebuildPanel();
            }

            if (this._changeTeamButton)
            {
                if (settings.changeTeamButton)
                {
                    this._changeTeamButton.apply(settings.changeTeamButton);
                }
                this.updateChangeTeamButton(this._currentTeamType === Models.Teams.Alone);
                this._changeTeamButton.enabled = this._isChangeTeamButtonEnabled;
                this._changeTeamButton.alpha = this._isChangeTeamButtonEnabled ? 1 : 0;
            }
        }

        /** Returns jackpot index using team name. Returns null if no team selected*/
        public getTeamIndex(teamName: string): Models.Teams
        {
            if (!teamName) { return Models.Teams.Alone; }

            for (let i = 0; i < this._jackpotElements.length; i++)
            {
                if (this._jackpotElements[i].runtimeData.jackpotName === teamName)
                {
                    return i;
                }
            }
            return Models.Teams.Alone;
        }

        public updateChangeTeamButton(alone: boolean)
        {
            this._changeTeamButton.label.text = alone ? Translations.Syndicate.SelectTeam : Translations.Syndicate.ChangeTeam;
        }

        protected create()
        {
            this.suppressLayouting();

            const jpRuntimeData = this.runtimeData.jackpots;

            const jackpotElements = new Array<MegaDrop.Elements.JackpotElement>(this.settings.jackpots.length);
            for (let i = 0, l = this.runtimeData.jackpots.length; i < l; ++i)
            {
                jackpotElements[i] = MegaDrop.Elements.jackpotElement.create({
                    ...this.settings.jackpots[i],
                    background: this.settings.jackpots[i].background,
                    currencyFormatter: this.currencyFormatter
                }, jpRuntimeData[i] ? jpRuntimeData[i] : this.runtimeData.jackpots[i]);
            }
            this._jackpotElements = jackpotElements;

            // Add Elements
            if (this.settings.background)
            {
                this._bg = Flow.background.create(this.settings.background, this);
            }

            if (this.settings.logoImage)
            {
                this._logo = Flow.image.create(this.settings.logoImage, this);
            }

            this._list = Flow.list.create(this.settings.listSettings, this);
            this.rebuild(this.settings.megaDropElements);

            this.restoreLayouting(false);
        }

        /** Rebuild panel elements */
        protected rebuild(elements: Elements.SyndicatePanel.Element[])
        {
            if (this._bg)
            {
                this.removeChild(this._bg);
            }

            if (this._list && elements)
            {
                this._list.removeAllChildren();
                for (const jackpot of this._jackpotElements)
                {
                    if (jackpot.parent)
                    {
                        jackpot.parent.removeChild(jackpot);
                    }
                }

                this._currentElements = [];
                this.removeAllChildren();
                if (this._logo)
                {
                    this.addChild(this._logo);
                }
                this.addChild(this._list);

                this.addElements(elements);
            }
        }

        /** Rebuild entire panel */
        protected rebuildPanel()
        {
            const teamIndex = this.getTeamIndex(this._currentTeam);

            if (teamIndex === Models.Teams.Alone)
            {
                this.rebuild(this.settings.megaDropElements);
                this._jackpotElements.forEach((je) => je.showLabel = true);
            }
            else
            {
                this.rebuild(this.settings.elements);
            }

            this.updateText(teamIndex);
            this.invalidateLayout();
        }

        @RS.Callback
        protected handleChangeTeam(): void
        {
            this._onTeamChanged.publish();
        }

        protected addBackground(settings: Flow.Background.Settings)
        {
            this._bg = Flow.background.create(settings, this);
            this.moveToBottom(this._bg);
        }

        protected addLogo()
        {
            if (this.settings.logoImage)
            {
                this._logo = Flow.image.create(this.settings.logoImage, this);
            }
        }

        protected addElements(elements: SyndicatePanel.Element[]): void
        {
            for (let i = 0; i < elements.length; i++)
            {
                this.addElement(elements[i]);
            }
        }

        //TODO upgrade to degates
        protected addElement(element: SyndicatePanel.Element): void
        {
            const parent = element.ignoreList ? this : this._list;

            switch (element.kind)
            {
                case SyndicatePanel.ElementKind.Background:
                    this.addBackground(element.background);
                    break;

                case SyndicatePanel.ElementKind.Image:
                    this.addImage(element.image, parent);
                    break;

                case SyndicatePanel.ElementKind.CurrentTeamText:
                    this.addCurrentTeamText(element.label, parent);
                    break;

                case SyndicatePanel.ElementKind.PlayerCountText:
                    this.addPlayerCountText(element.label, parent);
                    break;

                case SyndicatePanel.ElementKind.CurrentTeamJackpot:
                    this.addCurrentTeamJackpot(element.container, parent);
                    break;

                case SyndicatePanel.ElementKind.OtherTeamsJackpots:
                    this.addOtherTeamsJackpot(element.list, parent);
                    break;

                case SyndicatePanel.ElementKind.MegaDrop:
                    this.addStandardMegaDrop(element.listSettings, parent);
                    break;

                case SyndicatePanel.ElementKind.ChangeTeamButton:
                    this.addChangeTeamButton(element.button, parent);
                    break;

                case SyndicatePanel.ElementKind.QualifyingTimerText:
                    this.addTimer(element.label, parent);
                    break;
            }
        }

        protected addText(settings: Flow.Label.Settings, parent: Flow.GenericElement)
        {
            this._currentElements.push(Flow.label.create(settings, parent));
        }

        protected addImage(settings: Flow.Image.Settings, parent: Flow.GenericElement)
        {
            this._currentElements.push(Flow.image.create(settings, parent));
        }

        protected addCurrentTeamText(settings: Flow.Label.Settings, parent: Flow.GenericElement)
        {
            this._currentTeamText = Flow.label.create(settings, parent);
        }

        protected addPlayerCountText(settings: Flow.Label.Settings, parent: Flow.GenericElement)
        {
            this._playerCountText = Flow.label.create(settings, parent);
        }

        protected addTimer(settings: Flow.Label.Settings, parent: Flow.GenericElement)
        {
            this._timerText = Flow.label.create(settings, parent);
            this._newTimerText.publish();
        }

        protected addCurrentTeamJackpot(settings: Flow.Container.Settings, parent: Flow.GenericElement)
        {
            const container = Flow.container.create(settings, parent);

            const teamIndex = this.getTeamIndex(this._currentTeam);
            if (teamIndex !== Models.Teams.Alone)
            {
                for (let i = 0; i < this._jackpotElements.length; i++)
                {
                    if (i === teamIndex)
                    {
                        container.addChild(this._jackpotElements[i]);
                        this._jackpotElements[i].showLabel = true;
                    }
                }
            }
            this._currentElements.push(container);
        }

        protected addOtherTeamsJackpot(settings: Flow.List.Settings, parent: Flow.GenericElement)
        {
            const list = Flow.list.create(settings, parent);

            const teamIndex = this.getTeamIndex(this._currentTeam);
            if (teamIndex !== Models.Teams.Alone)
            {
                for (let i = 0; i < this._jackpotElements.length; i++)
                {
                    if (i !== teamIndex)
                    {
                        list.addChild(this._jackpotElements[i]);
                        this._jackpotElements[i].showLabel = false;
                    }
                }
            }
            this._currentElements.push(list);
        }

        protected addStandardMegaDrop(settings: Flow.List.Settings, parent: Flow.GenericElement)
        {
            const list = Flow.list.create(settings, parent);
            for (let i = 0; i < this._jackpotElements.length; i++)
            {
                if (i === 0)
                {
                    const index = parent === this._list ? 0 : null
                    parent.addChild(this._jackpotElements[i], index);
                }
                else
                {
                    list.addChild(this._jackpotElements[i]);
                }
                this._jackpotElements[i].backgroundMode = this.settings.showJackpotBackground;
            }
            this._currentElements.push(list);
        }

        protected addChangeTeamButton(settings: Flow.Button.Settings, parent: Flow.GenericElement)
        {
            this._changeTeamButton = Flow.button.create(settings, parent);
            this._changeTeamButton.enabled = this._isChangeTeamButtonEnabled;
            this._changeTeamButton.alpha = this._isChangeTeamButtonEnabled ? 1 : 0;
            this._changeTeamButton.onClicked(this.handleChangeTeam);
        }

        protected getPlayerTeamString(tier: Models.Teams): string
        {
            return this._teamTranslations[tier].get(this.locale);
        }

        protected getJackpotNameString(tier: Models.Teams): string
        {
            return this._jackpotTranslations[tier].get(this.locale);
        }

        protected updateText(tier: Models.Teams)
        {
            if (this._currentTeamText)
            {
                const teamString = this.getPlayerTeamString(this.currentTeamType);
                const jackpotString = this.getJackpotNameString(this.currentTeamType);

                this._currentTeamText.text = Translations.Syndicate.CurrentTeam.get(this.locale, { currentTeam: teamString, jackpotName: jackpotString });
            }

            if (this._playerCountText && tier != Models.Teams.Alone && this._currentTeam)
            {
                const count = this.runtimeData.playerCountObservables[this.getTeamIndex(this._currentTeam)];
                this._playerCountText.bindToObservables({
                    text: count
                        .map((value) => value.toString())
                        .map((value) => RS.Localisation.bind(Translations.Syndicate.PlayerCount, { playerCount: value}))
                });
            }
        }
    }

    export const syndicatePanel = RS.Flow.declareElement(SyndicatePanel, true);

    export namespace SyndicatePanel
    {
        export enum ElementKind
        {
            Background,
            Image,
            Text,
            CurrentTeamText,
            PlayerCountText,
            Jackpots,
            MegaDrop,
            CurrentTeamJackpot,
            OtherTeamsJackpots,
            ChangeTeamButton,
            QualifyingTimerText
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            /** The overall background. */
            background?: Flow.Background.Settings;
            /** The logo image. */
            logoImage?: Flow.Image.Settings;
            /** The individual jackpots. */
            jackpots: MegaDrop.Elements.JackpotElement.Settings[];
            showJackpotBackground?: boolean;
            currentTeamJackpotContainer?: Flow.Container.Settings;
            otherTeamsJackpotList?: Flow.List.Settings;
            currentTeamText?: Flow.Label.Settings;
            playerCountText?: Flow.Label.Settings;
            changeTeamButton?: Flow.Button.Settings;
            elements: Element[];
            megaDropElements: Element[];
            listSettings: Flow.List.Settings;
            timerUpdateFrequency?: number;
            teamTranslations: RS.Map<RS.Localisation.SimpleTranslationReference>;
            jackpotTranslations: RS.Map<RS.Localisation.SimpleTranslationReference>;
        }

        export interface RuntimeData extends MegaDrop.Elements.JackpotPanel.RuntimeData
        {
            playerCountObservables: RS.IReadonlyObservable<number>[];
            winEligibilityPeriods: RS.IReadonlyObservable<number>[];
        }

        export interface ElementSettings<TKind extends ElementKind>
        {
            kind: TKind;
            /** true to add to panel instead of list */
            ignoreList?: boolean;
        }

        export interface Background extends ElementSettings<ElementKind.Background>
        {
            background: Flow.Background.Settings;
        }

        export interface Image extends ElementSettings<ElementKind.Image>
        {
            image: Flow.Image.Settings;
        }

        export interface Text<TKind extends ElementKind> extends ElementSettings<TKind>
        {
            kind: TKind;
            label: Flow.Label.Settings;
        }

        export type CurrentTeamText = Text<ElementKind.CurrentTeamText>;

        export type PlayerCountText = Text<ElementKind.PlayerCountText>;

        export type QualifyingTimerText = Text<ElementKind.QualifyingTimerText>;

        /*
        * Original Mega Drop Panel Layout
        */
        export interface MegaDrop extends ElementSettings<ElementKind.MegaDrop>
        {
            listSettings: Flow.List.Settings;
        }

        // Contains the current team jackpot
        export interface CurrentTeamJackpot extends ElementSettings<ElementKind.CurrentTeamJackpot>
        {
            container: Flow.Container.Settings;
        }

        // Contains other team jackpots
        export interface OtherTeamsJackpots extends ElementSettings<ElementKind.OtherTeamsJackpots>
        {
            list: Flow.List.Settings;
        }

        export interface ChangeTeamButton extends ElementSettings<ElementKind.ChangeTeamButton>
        {
            button: Flow.Button.Settings;
        }

        export type Element = Background | Image | CurrentTeamText | PlayerCountText | MegaDrop | CurrentTeamJackpot | OtherTeamsJackpots | ChangeTeamButton | QualifyingTimerText;
    }
}
