/// <reference path="../generated/Translations.ts" />

namespace Red7.Syndicate.Elements
{
    import Flow = RS.Flow;
    import MegaDrop = Red7.MegaDrop;

    export class TeamSelection extends Flow.BaseElement<TeamSelection.Settings, TeamSelection.RuntimeData>
    {
        @RS.AutoDispose public readonly onSelected = RS.createEvent<string | null>();

        @RS.AutoDisposeOnSet protected _list: Flow.List | null = null;
        @RS.AutoDisposeOnSet protected _jackpotList: Flow.List | null = null;
        @RS.AutoDisposeOnSet protected _jackpotElements: MegaDrop.Elements.JackpotElement[] | null = null;

        @RS.AutoDisposeOnSet protected _bg: Flow.Background | null = null;
        @RS.AutoDisposeOnSet protected _title: Flow.Label | null = null;
        @RS.AutoDisposeOnSet protected _description: Flow.Label | null = null;
        @RS.AutoDisposeOnSet protected _playAloneButton: Flow.Button | null = null;
        @RS.AutoDisposeOnSet protected _playerCountObservable: RS.Flow.ObservablePropertyMap<Flow.Label> | null = null;

        @RS.AutoDisposeOnSet protected _cards: RS.Flow.Button[];

        public constructor(settings: TeamSelection.Settings, runtimeData: TeamSelection.RuntimeData)
        {
            super(settings, runtimeData);

            this.addBackground();
            this.addListElements();
            this.addJackpots();
            this.addPlayAlone();

            this.invalidateLayout();
        }

        /** Fde team selection in and out */
        public async fadeTeamSelection(fadeIn: boolean, time: number = 250)
        {
            this.interactiveChildren = fadeIn;
            RS.Tween.removeTweens(this as TeamSelection);
            await RS.Tween.get(this as TeamSelection).to({ alpha: fadeIn ? 1.0 : 0.0 }, time);
        }

        /** Start jackpot idle animation */
        public startJackpotTweens()
        {
            this._cards.forEach((el)=>
            {
                RS.Tween.removeTweens(el);
                this.animateIdle(el);
            })
        }

        /** Add background element to team selection */
        protected addBackground()
        {
            if (this.settings.background)
            {
                this._bg = Flow.background.create(this.settings.background, this);
            }
        }

        /** Add list elements above background */
        protected addListElements()
        {  
            this._list = Flow.list.create(this.settings.listSettings, this);

            if (this.settings.title)
            {
                this._title = Flow.label.create(this.settings.title, this._list);
            }
            if (this.settings.description)
            {
                this._description = Flow.label.create(this.settings.description, this._list);
            }
        }

        /** Add jackpot list elements */
        protected addJackpots()
        {
            const jpRuntimeData = this.runtimeData.jackpots;
            this._jackpotList = Flow.list.create(this.settings.jackpotsListSettings, this._list);
            this._jackpotElements = new Array<MegaDrop.Elements.JackpotElement>(this.settings.jackpots.length);
            this._cards = [];

            for (let i = 0, l = this.runtimeData.jackpots.length; i < l; ++i)
            {
                // Create jackpot element
                const cardBackground = Flow.button.create(this.settings.jackpotCardButton[i], this._jackpotList);
                const list = Flow.list.create(this.settings.jackpotListSettings, cardBackground);
                this._cards.push(cardBackground);

                this._jackpotElements[i] = MegaDrop.Elements.jackpotElement.create(
                {
                    ...this.settings.jackpots[i], 
                    currencyFormatter: this.runtimeData.megaDropCurrencyFormatter,
                }, jpRuntimeData[i] ? jpRuntimeData[i] : this.runtimeData.jackpots[i], list);

                //this._jackpotElements[i].interactive = false;
                   
                this.addPlayerCounts(i, list);
                this.addClickEvents(cardBackground, this._jackpotElements[i]);
            }

            this.invalidateLayout();
            this.startJackpotTweens();
        }

        /** Create player counts for jackpot element */
        protected addPlayerCounts(index: number, container: Flow.GenericElement)
        {
            const count = this.runtimeData.playerCountObservables[index];
            const label = Flow.label.create(this.settings.playerCountLabelSettings, container);
            this._playerCountObservable =
            {
                text: count
                .map((value) => RS.Localisation.bind(Translations.Syndicate.PlayerCount, { playerCount: value.toString()}))
            };
            label.bindToObservables(this._playerCountObservable);
        }

        /** Create click events for jackpot element */
        protected addClickEvents(card: RS.Flow.Button, jackpotElement: MegaDrop.Elements.JackpotElement)
        {
            // Mouse over
            card.onOver(async () =>
            {
                this.animateHoverOver(card);
                this._cards.forEach((el) => { if (el !== card) { this.animateHoverHide(el); } });
            });

            // Mouse out
            card.onOut(async () =>
            {
                await Promise.all(this._cards.map((el) => this.animateHoverOut(el)));
            });

            // Mouse down / device touch
            card.onClicked(async () =>
            {
                this.animatePressed(card);
                await this.fadeTeamSelection(false);
                this.onSelected.publish(jackpotElement.runtimeData.jackpotName);
            });
        }

        /** Add play alone button */
        protected addPlayAlone() 
        {
            if (this.settings.playAloneButton)
            {
                this._playAloneButton = Flow.button.create(this.settings.playAloneButton, this._list);
                this._playAloneButton.onClicked(async () =>
                {
                    RS.Tween.removeTweens(this._playAloneButton);
                    RS.Tween.get(this._playAloneButton)
                        .to({ scaleFactor: this.settings.playAloneButton.scaleFactor - 0.05 }, 50)
                        .to({ scaleFactor: this.settings.playAloneButton.scaleFactor }, 50);
                    
                    await this.fadeTeamSelection(false);
                    this.onSelected.publish(null);
                });
                this._playAloneButton.onOver(async () =>
                {
                    RS.Tween.removeTweens(this._playAloneButton);
                    RS.Tween.get(this._playAloneButton).to({ scaleFactor: this.settings.playAloneButton.scaleFactor + 0.05 }, 300, RS.Ease.backOut);
                });
                this._playAloneButton.onOut(async () =>
                {
                    RS.Tween.removeTweens(this._playAloneButton);
                    RS.Tween.get(this._playAloneButton).to({ scaleFactor: this.settings.playAloneButton.scaleFactor }, 300, RS.Ease.backOut);
                });
            }
        }

        @RS.Callback
        protected async animateIdle(card: RS.Flow.Button)
        {
            RS.Tween.removeTweens(card.postTransform);
            RS.Tween.get(card.postTransform, { loop: true })
                .set({ scaleX: this.settings.tweenSettings.defaultScale, scaleY: this.settings.tweenSettings.defaultScale })
                .to({ scaleX: this.settings.tweenSettings.pulseScale, scaleY: this.settings.tweenSettings.pulseScale }, (1000 / this.settings.tweenSettings.pulseRate))
                .to({ scaleX: this.settings.tweenSettings.defaultScale, scaleY: this.settings.tweenSettings.defaultScale }, (1000 / this.settings.tweenSettings.pulseRate));
        }

        @RS.Callback
        protected async animateHoverOver(card: RS.Flow.Button)
        {
            RS.Tween.removeTweens(card.postTransform);
            await RS.Tween.get(card.postTransform)
                .to({ scaleX: this.settings.tweenSettings.hoverScale, scaleY: this.settings.tweenSettings.hoverScale }, 400, RS.Ease.backOut);
        }

        @RS.Callback
        protected async animateHoverHide(card: RS.Flow.Button)
        {
            RS.Tween.removeTweens(card.postTransform);
            await RS.Tween.get(card.postTransform)
                .to({ scaleX: this.settings.tweenSettings.defaultScale - 0.1, scaleY: this.settings.tweenSettings.defaultScale - 0.1 }, 400, RS.Ease.backOut);
        }

        @RS.Callback
        protected async animateHoverOut(card: RS.Flow.Button)
        {
            RS.Tween.removeTweens(card.postTransform);
            await RS.Tween.get(card.postTransform).wait(100)
                .to({ scaleX: this.settings.tweenSettings.defaultScale, scaleY: this.settings.tweenSettings.defaultScale }, 400, RS.Ease.backOut)
                .call(() => { this.animateIdle(card); });         
        }

        @RS.Callback
        protected async animatePressed(card: RS.Flow.Button)
        {
            if (this.settings.clickSound) { RS.Audio.play(this.settings.clickSound); }

            RS.Tween.removeTweens(card);
            RS.Tween.removeTweens(card.postTransform);
            await RS.Tween.get(card.postTransform)
                .to({ scaleX: this.scaleX - 0.1, scaleY: this.scaleY - 0.1 }, 50)
                .to({ scaleX: this.scaleX, scaleY: this.scaleY, }, 50);
        }
    }

    export namespace TeamSelection
    {
        export interface TweenSettings
        {
            /** Default resting scale */
            defaultScale: number;

            /** Scale when mouse over */
            hoverScale: number; 

            /** Scale when pulsing */
            pulseScale: number;

            /** Rate to pulse (pulse/sec, default: 1) */
            pulseRate: number;
        }
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            title: Flow.Label.Settings;
            description: Flow.Label.Settings;
            jackpots: MegaDrop.Elements.JackpotElement.Settings[];
            jackpotCardButton?: Flow.Button.Settings[];
            playAloneButton: Flow.Button.Settings;
            background: Flow.Background.Settings;
            listSettings: Flow.List.Settings;
            jackpotsListSettings: Flow.List.Settings;
            jackpotListSettings: Flow.List.Settings;
            playerCountLabelSettings: Flow.Label.Settings;

            /** The idle and hover tween settings. */
            tweenSettings: TweenSettings;

            /** Click sound when element is interacted with. */
            clickSound?: RS.Asset.SoundReference;
        }

        export interface RuntimeData
        {
            jackpots: MegaDrop.Elements.JackpotElement.RuntimeData[];
            playerCountObservables: RS.Observable<number>[];
            megaDropCurrencyFormatter: RS.Localisation.CurrencyFormatter;
        }
    };
}