/// <reference path="IController.ts" />
/// <reference path="elements/TeamSelection.ts" />
/// <reference path="elements/SyndicatePanel.ts" />

namespace Red7.Syndicate
{
    import Flow = RS.Flow;

    @RS.HasCallbacks
    export class Controller<TSettings extends IController.Settings = IController.Settings> extends MegaDrop.Controller<TSettings> implements IController<TSettings>
    {
        protected readonly _onTeamSelected = RS.createEvent<string>();
        public get onTeamSelected() { return this._onTeamSelected.public; }

        protected readonly _onTeamSelectionOpened = RS.createSimpleEvent();
        public get onTeamSelectionOpened() { return this._onTeamSelectionOpened.public; }

        protected _isTeamSelectOpen: boolean = false;
        public get isTeamSelectOpen() { return this._isTeamSelectOpen; }

        protected readonly _winDialogOpened: RS.Observable<boolean> = new RS.Observable(false);
        public get winDialogOpened() { return this._winDialogOpened; }

        @RS.AutoDispose protected readonly _onTeamTimerEnd = RS.createSimpleEvent();
        public get onTeamTimerEnd() { return this._onTeamTimerEnd.public; }

        protected _syndicateModel: Models.Syndicate;
        protected _megaDropModel: Models.MegaDropJackpots;
        protected _jackpotWinView: JackpotWin.View;

        private _gameMode: GameMode = null;
        public get gameMode() { return this._gameMode; }
        public set gameMode(val: GameMode)
        {
            if (this._gameMode === null)
            {
                this._gameMode = val;
            }
            else
            {
                RS.Log.warn("[Red7.Syndicate.Controller] Tried to set gamemode when one was already set.");
            }
        }

        @RS.AutoDisposeOnSet private _teamSelection: Elements.TeamSelection | null = null;

        @RS.AutoDisposeOnSet private _panel: Elements.SyndicatePanel;
        @RS.AutoDisposeOnSet private _syndicatePanelData: Elements.SyndicatePanel.RuntimeData;

        @RS.AutoDispose private readonly _isIdle: RS.Observable<boolean> = new RS.Observable(false);
        public get idle() { return this._isIdle.value; }
        public set idle(value) { this._isIdle.value = value; }

        private _observableController: ObservableController | null;

        private _container: Flow.GenericElement | null = null;
        @RS.AutoDispose private readonly _handlers: RS.IDisposable[] = [];

        @RS.AutoDisposeOnSet private _timers: Elements.Timer[] = [];
        @RS.AutoDispose private readonly _timerHandlers: RS.IDisposable[] = [];

        public set showPanel(visible: boolean) { this._panel.visible = visible; }
        public get showPanel() { return this._panel.visible; }

        public set buttonEnabled(enabled: boolean) { this._panel.isChangeTeamButtonEnabled = enabled; }
        public get buttonEnabledl() { return this._panel.isChangeTeamButtonEnabled; }

        public get currencyFormatter() { return this._currencyFormatter; }
        public set currencyFormatter(currencyFormatter: RS.Localisation.CurrencyFormatter)
        {
            // trigger setter of parent controller
            this.megaDropCurrencyFormatter = currencyFormatter;
        }

        public initialise(settings: TSettings)
        {
            this._settings = settings;

            this._syndicatePanelData =
            {
                jackpots: [],
                playerCountObservables: [],
                winEligibilityPeriods: []
            };

            // Setup Jackpot Runtime data
            const jackpotSettings = settings.panel.jackpots;
            this._observableController = new ObservableController(jackpotSettings.length);

            for (let i = 0, l = jackpotSettings.length; i < l; ++i)
            {
                this._syndicatePanelData.jackpots.push(this._observableController.newJackpotPanelData(i));
            }

            this._syndicatePanelData.playerCountObservables = this._observableController.playerCountValueObservables;
            this._syndicatePanelData.winEligibilityPeriods = this._observableController.winEligibilityPeriodsObservables;
        }

        public attach(container: Flow.GenericElement): void
        {
            if (this.gameMode === GameMode.None) { return; }
            this._container = container;

            if (this._panel == null)
            {
                this._panel = Elements.syndicatePanel.create(
                {
                    ...this.settings.panel,
                    currencyFormatter: this._currencyFormatter
                }, this._syndicatePanelData, container);

                this.createTimers();

                this._handlers.push(this._panel.onTeamChanged(this.openTeamSelection));
                this._handlers.push(this.onTeamTimerEnd(this.openTeamSelection));
                this._handlers.push(this._panel.newTimerText(this.newTimerText));
            }
            else if (this._panel.parent == null)
            {
                container.addChild(this._panel);
            }

            container.invalidateLayout();
        }

        public detach(): void
        {
            if (this._teamSelection != null && this._teamSelection.parent != null)
            {
                this._teamSelection.parent.removeChild(this._teamSelection);
            }

            if (this._panel != null)
            {
                this._panel.parent.removeChild(this._panel);
            }
        }

        public update(megadropModel: Models.MegaDropJackpots, syndicateModel?: Models.Syndicate): void
        {
            this._megaDropModel = megadropModel;
            this._syndicateModel = syndicateModel;

            for (let i = 0; i < this._megaDropModel.length; ++i)
            {
                this._observableController.updateObservables(i, this._megaDropModel[i]);

                if (this._megaDropModel[i].name != null)
                {
                    this._syndicatePanelData.jackpots[i].jackpotName = this._megaDropModel[i].name;
                }
            }
        }

        public updatePanel(settings: Elements.SyndicatePanel.Settings, rebuild: boolean = false)
        {
            this.settings.panel = settings;
            this._panel.apply(settings, rebuild);
        }

        @RS.Callback
        public async openTeamSelection()
        {
            if (this.gameMode === GameMode.None) { return; }
            if (this.gameMode === GameMode.Alone)
            {
                this.handleTeamSelected(null);
                return;
            }

            if (this._container == null) { return; }

            await this._isIdle.for(true);

            this._isTeamSelectOpen = true;

            this._onTeamSelectionOpened.publish();

            this._panel.isChangeTeamButtonEnabled = false;

            if (this._teamSelection == null)
            {
                this._teamSelection = new Elements.TeamSelection(this.settings.teamSelection,
                {
                    jackpots: this._syndicatePanelData.jackpots,
                    playerCountObservables: this._observableController.playerCountValueObservables,
                    megaDropCurrencyFormatter: this._currencyFormatter
                });
                this._teamSelection.onSelected(this.handleTeamSelected);
            }
            else
            {
                this._teamSelection.startJackpotTweens();
            }

            this._container.addChild(this._teamSelection);
            this._container.invalidateLayout();

            this._teamSelection.fadeTeamSelection(true);
        }

        @RS.Callback
        public async winDialog()
        {
            this._winDialogOpened.value = true;

            const container = RS.Flow.container.create(
                {
                    dock: RS.Flow.Dock.Fill,
                    sizeToContents: true
                }
            );
            const dialog = RS.Flow.dialog.create(Settings.SyndicatePanel.winDialog);

            container.addChild(dialog);
            this._container.addChild(container);
            this._container.invalidateLayout();
            await dialog.onOptionSelected;
            this._container.removeChild(container);

            this._winDialogOpened.value = false;

            container.dispose();
        }

        public resetTimer()
        {
            this.resetQualifyingPeriod();
        }

        public async playWinAnimation(jackpotName: string, jackpotWinnings: number, syndicated: boolean = false)
        {
            const musicHandle = this.settings.musicVolumeArbiter.declare(0);
            const viewSettings = Red7.MegaDrop.Settings.JackpotWin;
            const jackpotWinSettings: JackpotWin.View.JackpotWinSettings =
            {
                isReplay: this._settings.stateModel.isReplay,
                totalWin: jackpotWinnings,
                jackpotTier: this.settings.JackpotWinTier[jackpotName],
                winObservable: this._settings.winObservable
            };

            this.onMegaDropAnimationStarted.publish();
            this._jackpotWinView = await this._settings.viewController.open(JackpotWin.View, viewSettings);

            this._jackpotWinView.setUpComponents(jackpotWinSettings, this.winAnimationContainer, syndicated);
            this.onMegaDropAnimationStarted.publish();
            await this._jackpotWinView.start(this._settings.viewController);
            await this._settings.viewController.close(JackpotWin.View);
            musicHandle.dispose();
        }

        protected resetQualifyingPeriod()
        {
            if (this._panel.getTeamIndex(this._panel.currentTeam) === Models.Teams.Alone) { return; }
            this._timers[this._panel.getTeamIndex(this._panel.currentTeam)].resetTimer(true);
        }

        protected setActiveTimer()
        {
            this._timers.forEach((t) => t.stopTimer());

            if (this._panel.getTeamIndex(this._panel.currentTeam) === Models.Teams.Alone) { return; }
            this._timers[this._panel.getTeamIndex(this._panel.currentTeam)].startTimer();
        }

        protected createTimers()
        {
            for (let i = 0; i < this._panel.runtimeData.jackpots.length; i++)
            {
                const timer = new Elements.Timer
                (
                    {
                        updateFrequency: this._panel.settings.timerUpdateFrequency ? this._panel.settings.timerUpdateFrequency : 500
                    },
                    this._panel.runtimeData,
                    i
                );
                this._timers.push(timer);

                const handler = timer.onTimerEnd(() =>
                {
                    if (timer.jackpotIndex === this._panel.getTeamIndex(this._panel.currentTeam))
                    {
                        this._onTeamTimerEnd.publish();
                    }
                });
                this._timerHandlers.push(handler);
            }
        }

        @RS.Callback
        protected newTimerText()
        {
            this._timers.forEach((t) =>
                {
                    const handler = t.timer.onChanged(() =>
                    {
                        if (t.timer.value === "")
                        {
                            this._panel.timerText.text = "";
                        }
                        else
                        {
                            this._panel.timerText.text = Translations.Syndicate.QualifyingPeriod.get(this._panel.locale, { remainingTime: t.timer.value });
                        }
                    });
                    this._timerHandlers.push(handler);
                })
        }

        @RS.Callback
        protected async handleTeamSelected(teamName: string)
        {
            this._isTeamSelectOpen = false;

            if (this.gameMode === GameMode.Syndicate)
            {
                this._teamSelection.parent.removeChild(this._teamSelection);
                this._panel.isChangeTeamButtonEnabled = true;
                this.setActiveTimer();
            }

            this._panel.currentTeam = teamName;
            this._onTeamSelected.publish(teamName);

            if (this._megaDropModel != null)
            {
                for (const jackpot of this._megaDropModel)
                {
                    if (jackpot.name === teamName)
                    {
                        this._syndicateModel.selectedTeamId = jackpot.syndication.id;
                        this._syndicateModel.selectedTeamName = jackpot.syndication.name;
                        this._panel.updateChangeTeamButton(false);
                        return;
                    }
                }
            }
            // Playing alone
            if (this._syndicateModel != null)
            {
                this._syndicateModel.selectedTeamId = null;
                this._syndicateModel.selectedTeamName = null;
            }

            this._panel.updateChangeTeamButton(teamName === null);
        }
    }

    IController.register(Controller);
}
