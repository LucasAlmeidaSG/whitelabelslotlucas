/// <reference path="CommonData.ts" />

namespace Red7.Syndicate.GLM
{
    import XML = RS.Serialisation.XML;

    export class PotAwarded extends MegaDrop.GLM.PotAwarded
    {
        @XML.Property({ path: ".syndication", type: XML.Boolean, ignoreIfNull: true })
        public syndication?: boolean;
    }

    export class BetJackpotData extends JackpotData
    {
        @XML.Property({ path: "PotAwarded", type: XML.ExplicitObject(PotAwarded) })
        public results?: PotAwarded;
    }

    export interface ILogicResponse extends MegaDrop.GLM.ILogicResponse
    {
        //base
        header: SG.GLM.Core.Header;
        isError: boolean;

        betJackpotData: BetJackpotData;
        syndicateMode: number;
        syndicate: boolean;
    }
}
