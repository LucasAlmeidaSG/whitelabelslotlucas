/// <reference path="CommonData.ts" />

namespace Red7.Syndicate.GLM
{
    import XML = RS.Serialisation.XML;

    export interface LogicRequestPayload extends MegaDrop.GLM.LogicRequestPayload
    {
        selectedSyndicateTeam?: string;
        selectedSyndicateTeamName?: string;
    }

    export class LogicRequest extends MegaDrop.GLM.LogicRequest
    {
        @XML.Property({ path: "progressive SyndicationId", type: XML.String, ignoreIfNull: true })
        public syndicationId?: string;

        @XML.Property({ path: "progressive SyndicationName", type: XML.String, ignoreIfNull: true })
        public syndicationName?: string;
    }
}
