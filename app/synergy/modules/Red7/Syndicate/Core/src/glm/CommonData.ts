namespace Red7.Syndicate.GLM
{
    import XML = RS.Serialisation.XML;

    export class SyndicationId
    {
        @XML.Property({ path: "syndicationId", type: XML.String })
        public syndicationId: string;
    }

    export class Syndication
    {
        @XML.Property({ path: "id", type: XML.String })
        public id: string;

        @XML.Property({ path: "name", type: XML.String })
        public name: string;

        @XML.Property({ path: "threshold", type: XML.Number })
        public threshold: number;
    }

    @XML.Type("levels")
    export class JackpotInstanceData extends MegaDrop.GLM.JackpotInstanceData
    {
        @XML.Property({ path:"syndication", type: XML.ExplicitObject(Syndication), ignoreIfNull: true })
        public syndication?: Syndication;
    }

    export class JackpotData extends MegaDrop.GLM.JackpotData
    {
        @XML.Property({ path: "*levels", type: XML.Array(XML.ExplicitObject(JackpotInstanceData)) })
        public instanceData: JackpotInstanceData[];
    }
}
