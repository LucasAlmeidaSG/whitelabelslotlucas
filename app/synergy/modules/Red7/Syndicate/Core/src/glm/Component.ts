namespace Red7.Syndicate.GLM
{
    interface ErrorJackpotData
    {
        [jackpot: string]:
        {
            instanceId: string;
            syndicationId: string;
        }
    }

    export class Component<
        TInitPayload extends object = {}, TInitResponse extends IInitResponse = IInitResponse,
        TLogicPayload extends LogicRequestPayload = LogicRequestPayload, TLogicResponse extends ILogicResponse = ILogicResponse,
        TClosePayload extends object = {}, TCloseResponse extends object = {},
        TModels extends IModels = IModels>
    extends MegaDrop.GLM.Component<
        TInitPayload, TInitResponse,
        TLogicPayload, TLogicResponse,
        TClosePayload, TCloseResponse,
        TModels>
    {
        protected _jackpotData: JackpotData | null = null;
        protected _syndicationId: SyndicationId | null = null;

        protected _forceGameMode: boolean;
        protected _defaultGameMode: GameMode;
        protected _gameMode: GameMode;

        protected _onSyndicateGameModeObtained = RS.createSimpleEvent();
        public get onSyndicateGameModeObtained() { return this._onSyndicateGameModeObtained.public; }

        constructor(defaultGameMode?: GameMode, forceGameMode?: boolean)
        {
            super();
            this._forceGameMode = forceGameMode;
            this._defaultGameMode = defaultGameMode != undefined ? defaultGameMode : GameMode.Syndicate;
        }

        public populateInitPayload(payload: TInitPayload): void
        {
            super.populateInitPayload(payload);
        }

        public populateInitModels(response: TInitResponse, models: TModels): void
        {
            super.populateInitModels(response, models);

            //only parse in init if not recovery and not replay
            if (!response.header.isRecovering && !models.state.isReplay)
            {
                this.parseSyndicateGameMode(models, response.syndicateMode);
            }
        }

        public populateLogicPayload(payload: TLogicPayload): void
        {
            super.populateLogicPayload(payload);
        }

        public populateLogicModels(response: TLogicResponse, models: TModels): void
        {
            super.populateLogicModels(response, models);

            //only parse in logic if recovery or replay
            if (!response.isError && (response.header.isRecovering || models.state.isReplay))
            {
                this.parseSyndicateGameMode(models, response.syndicateMode);
            }

            models.syndicate.syndicate = response.syndicate;
        }

        public populateClosePayload(payload: TClosePayload): void
        {
            super.populateClosePayload(payload);
        }

        public populateCloseModels(response: TCloseResponse, models: TModels): void
        {
            super.populateCloseModels(response, models);
        }

        public parseError(error: SG.GLM.Core.Error, models: TModels)
        {
            super.parseError(error, models);

            // If Jackpot won by a Syndicate
            if (error.message.search("code:1051") >= 0)
            {
                models.syndicate.teamWin = true;
            }
        }

        protected parseSyndicateGameMode(models: TModels, newGameMode: number): boolean
        {
            if (models.syndicate.gameMode != undefined) { return; }

            if (this._forceGameMode)
            {
                this._gameMode = this._defaultGameMode;
            }
            else
            {
                const urlOverride = RS.URL.getParameterAsEnumVal(
                    GameMode,
                    "gamemode",
                    this._defaultGameMode);
    
                this._gameMode = newGameMode != undefined ? newGameMode : urlOverride;
            }

            models.syndicate.gameMode = this._gameMode;
            this._onSyndicateGameModeObtained.publish();
        }

        protected parseErrorObject(str: string): ErrorJackpotData
        {
            const out: Partial<ErrorJackpotData> = {};

            // match anything not "{" "," or ":",
            // then match ":",
            // then match anything not "," or "}"
            const regExp = /([^:{,]+):([^,}:]+)(:([^,}:]+))?/g;
            for (let match = regExp.exec(str); match != null; match = regExp.exec(str))
            {
                const [, key, instance, , syndication] = match;
                out[key] = { instanceId: instance, syndicationId: syndication };
            }
            return out as ErrorJackpotData;
        }

        protected parseJackpotData(data: JackpotData, models: TModels)
        {
            for (let tier = 0; tier < data.instanceData.length; tier++)
            {
                if (data.instanceData[tier].syndication)
                {
                    models.megaDrop[tier].syndication = data.instanceData[tier].syndication;
                }
            }
            super.parseJackpotData(data, models);
        }

        protected updateJackpotDataFromError(errorData: ErrorJackpotData, models: TModels)
        {
            const curJackpotData = this._jackpotData;
            for (const jackpot of curJackpotData.instanceData)
            {
                //play alone doesn't have syndication
                if (jackpot.syndication)
                {
                    jackpot.syndication.id = errorData[jackpot.id].syndicationId;
                }
            }
            super.updateJackpotDataFromError(errorData, models);
        }

        protected updateJackpotData(jackpotData: JackpotData, models: TModels)
        {
            super.updateJackpotData(jackpotData, models);

            for (const newData of jackpotData.instanceData)
            {
                this._jackpotIDs.push(newData.index);
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    if (newData.id == models.megaDrop[tier].name)
                    {
                        if (newData.syndication)
                        {
                            models.megaDrop[tier].syndication = newData.syndication;
                        }
                        break;
                    }
                }
            }
        }

        protected parseJackpotWin(potAwarded: PotAwarded, models: TModels, shouldMultiply: boolean = true)
        {
            super.parseJackpotWin(potAwarded, models, shouldMultiply);

            //if we're in replay the mega drop model won't be populated, but it'll still exist
            //therefore we can fill index 0 with the name and value awarded for the state to display
            if (models.state.isReplay)
            {
                models.megaDrop[potAwarded.potIndex].syndicated = potAwarded.syndication;
            }
            else
            {
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    const internalJackpotName = this.convertExternalJackpotName(potAwarded.name);
                    if (models.megaDrop[tier].name == internalJackpotName)
                    {
                        models.megaDrop[tier].syndicated = potAwarded.syndication;
                    }
                }
            }

            const platform = RS.IPlatform.get();
            if (platform instanceof SG.Environment.Platform)
            {
                platform.syndicatedJackpotAwarded = potAwarded.syndication;
            }
        }

        protected handleJackpotsUpdated(data: SGI.JackpotData[], models: TModels): void
        {
            // Loop through each new updated piece of data, find the model index that matches, and update it's current value
            // Model order and data order should be the exact same but just incase it isn't, this method ensures the correct model is updated
            for (const jackpotData of data)
            {
                for (let tier = 0; tier < models.megaDrop.length; ++tier)
                {
                    //stringify jackpot data in order to compare it to the string parsed model info
                    if (`${jackpotData.instance}` === models.megaDrop[tier].index)
                    {
                        models.megaDrop[tier].currentValue = RS.Math.floor(jackpotData.balanceAmountInJackpotCurrency * 100);

                        if (jackpotData.syndication)
                        {
                            models.megaDrop[tier].syndication.id = jackpotData.syndication.id;
                            models.megaDrop[tier].syndication.name = jackpotData.syndication.name;
                            models.megaDrop[tier].syndication.playerCount = jackpotData.syndication.playerCount;
                            models.megaDrop[tier].syndication.threshold = jackpotData.syndication.winEligibilityPeriod;
                        }
                        break;
                    }
                }
            }
            super.handleJackpotsUpdated(data, models);
        }
    }
}
