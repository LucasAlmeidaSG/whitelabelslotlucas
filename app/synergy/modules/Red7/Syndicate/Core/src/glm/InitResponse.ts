/// <reference path="CommonData.ts" />

namespace Red7.Syndicate.GLM
{
    export interface IInitResponse extends MegaDrop.GLM.IInitResponse
    {
        header: SG.GLM.Core.Header;

        jackpotData: JackpotData;
        syndicateMode: number;
    }
}
