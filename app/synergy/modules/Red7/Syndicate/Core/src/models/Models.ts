namespace Red7.Syndicate
{
    export interface IModels extends MegaDrop.IModels
    {
        megaDrop: Models.MegaDropJackpots;
        syndicate?: Models.Syndicate;
    }
}
