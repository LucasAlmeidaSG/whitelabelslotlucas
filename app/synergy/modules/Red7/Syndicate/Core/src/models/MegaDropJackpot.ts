namespace Red7.Syndicate.Models
{
    /**
     * Encapsulates a syndicate jackpot team
     * Syndication is absent in non-syndicated jackpots
     * @param id UUID
     * @param name Human readable name
     * @param threshold Period before a win for a player to be eligible for winnning the jackpot. (seconds)
     * */
    export interface Syndication
    {
        id: string;
        name: string;
        threshold?: number;
        playerCount?: number;
    }

    export interface MegaDropJackpot extends MegaDrop.Models.MegaDropJackpot
    {
        /**
         * A syndicate jackpot team.
         * Absent for non-syndicated jackpots
         */
        syndication?: Syndication;

        /**
         * Awarded jackpot is syndicated.
         */
        syndicated?: boolean;

        /**
         * Wager is part of a syndicate
         */
        syndicate?: boolean;
    }

    export namespace MegaDropJackpot
    {
        export function clear(model: MegaDropJackpot)
        {
            MegaDrop.Models.MegaDropJackpot.clear(model);
            model.syndication = { id: null, name: null };
            model.syndicate = false;
        }
    }
}
