/// <reference path="../IController.ts" />

namespace Red7.Syndicate.Models
{
    export enum Teams
    {
        Epic = 0,
        Major = 1,
        Minor = 2,
        Alone = 3
    }

    export interface Syndicate
    {
        /**
         * Syndicate team selected by the player. Non-syndicated jackpots if null
         */
        selectedTeamId?: string;

        /**
         * Needs to be sent in the request to determine if wager is part of a syndicate
         */
        selectedTeamName?: string;

        /**
         * Has another syndicated team won the jackpot?
         */
        teamWin?: boolean;

        /**
         * Wager is part of a syndicate team
         */
        syndicate?: boolean;

        /**
         * Betconfig game mode that came from init response
         */
        gameMode?: GameMode;
    }

    export namespace Syndicate
    {
        export function clear(model: Syndicate)
        {
            model.selectedTeamId = null;
            model.selectedTeamName = null;
            model.teamWin = null;
            model.syndicate = null;
            model.gameMode = null;
        }
    }
}
