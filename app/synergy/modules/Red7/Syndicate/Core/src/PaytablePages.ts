namespace Red7.Syndicate.Settings.Paytable
{
    export function legal(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): SG.CommonUI.Views.Paytable.PageInfo
    {
        return {
            title: RS.Translations.Paytable.Disclaimer.Title,
            element: MegaDrop.Paytable.megaDropLegalPage.bind(MegaDrop.Paytable.MegaDropLegalPage.getDefaultSettings(textSettings))
        };
    };

    export function odds(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): SG.CommonUI.Views.Paytable.PageInfo
    {
        return {
            title: Red7.MegaDrop.Translations.MegaDrop.Paytable.Title,
            element: MegaDrop.Paytable.megaDropOddsPage.bind(MegaDrop.Paytable.MegaDropOddsPage.getDefaultSettings(textSettings))
        };
    }

    export function megaDropSection(textSettings: RS.Flow.Label.Settings = RS.Paytable.Pages.TextPage.defaultPageSettings.paragraphSettings): SG.CommonUI.Views.Paytable.SectionInfo
    {
        return {
            title: MegaDrop.Translations.MegaDrop.Paytable.Title,
            pages:
            [
                {
                    title: MegaDrop.Translations.MegaDrop.Paytable.Title,
                    element: RS.Paytable.Pages.textPage.bind(page1(textSettings))
                },
                {
                    title: MegaDrop.Translations.MegaDrop.Paytable.Title,
                    element: RS.Paytable.Pages.textPage.bind(page2(textSettings))
                },
                {
                    title: MegaDrop.Translations.MegaDrop.Paytable.Title,
                    element: RS.Paytable.Pages.textPage.bind(page3(textSettings))
                },
                {
                    title: MegaDrop.Translations.MegaDrop.Paytable.Title,
                    element: MegaDrop.Paytable.megaDropAwardsPage.bind(MegaDrop.Paytable.MegaDropAwardsPage.getDefaultSettings(textSettings))
                }
            ]
        };
    }
}
