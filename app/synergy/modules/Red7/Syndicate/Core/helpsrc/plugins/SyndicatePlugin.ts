namespace Red7.Syndicate.Plugins
{
    /**
     * Uses gamemode to hide or show jackpot elements
     */
    export class SyndicatePlugin extends RS.ExternalHelp.VisitorPlugin
    {
        private readonly _gameMode: GameMode;

        public constructor(page: RS.ExternalHelp.IPage)
        {
            super(page);
            
            this._gameMode = RS.URL.getParameterAsEnumVal(GameMode, "gamemode", GameMode.None);
        }

        public onElementLoaded(element: HTMLElement)
        {
            switch (this._gameMode)
            {
                case GameMode.Alone:
                {
                    this.hideSyndicateElement(element);
                    this.hideNoneOnlyElement(element);
                    break;
                }
                case GameMode.Syndicate:
                {
                    this.hideAloneOnlyElement(element);
                    this.hideNoneOnlyElement(element);
                    break;
                }
                default:
                {
                    this.hideSyndicateElement(element);
                    this.hideAloneElement(element);
                    this.hideAloneOnlyElement(element);
                }
            }
        }

        protected hideSyndicateElement(element: HTMLElement)
        {
            if (element.classList.contains("rs-jackpot-syndicate"))
            {
                RS.ExternalHelp.hideElement(element)
            }
        }

        protected hideAloneElement(element: HTMLElement)
        {
            if (element.classList.contains("rs-jackpot-alone"))
            {
                RS.ExternalHelp.hideElement(element)
            }
        }

        protected hideAloneOnlyElement(element: HTMLElement)
        {
            if (element.classList.contains("rs-jackpot-alone-only"))
            {
                RS.ExternalHelp.hideElement(element)
            }
        }

        protected hideNoneOnlyElement(element: HTMLElement)
        {
            if (element.classList.contains("rs-jackpot-none-only"))
            {
                RS.ExternalHelp.hideElement(element)
            }
        }
    }

    export enum GameMode
    {
        None,
        Alone,
        Syndicate
    }

    RS.ExternalHelp.pluginRegistry.register(SyndicatePlugin);
}