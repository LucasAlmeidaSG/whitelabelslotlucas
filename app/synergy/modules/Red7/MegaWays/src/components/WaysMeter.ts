namespace Red7.MegaWays.Components
{
    export class WaysMeter extends RS.Flow.Meter
    {
        @RS.AutoDisposeOnSet protected _tween;

        public get value() { return this._value; }
        public set value(value)
        {
            this._value = value;
            if (value > 0)
            {
                this.updateText();
                this._tween = RS.Tween.get(this._valueLabel, { override: true })
                    .to({ alpha: 1 }, this.settings.fadeTime);
            }
            else if (value === 0)
            {
                this._tween = RS.Tween.get(this._valueLabel, { override: true })
                    .to({ alpha: 0 }, this.settings.fadeTime)
            }
        }

        constructor(public readonly settings: WaysMeter.Settings)
        {
            super(settings);
            this._valueLabel.alpha = 0;
            this.value = 0;
        }
    }

    export const waysMeter = RS.Flow.declareElement(WaysMeter);

    export namespace WaysMeter
    {
        export interface Settings extends RS.Flow.Meter.Settings
        {
            fadeTime: number;
        }

        export const defaultSettings: Settings =
        {
            ...RS.Flow.Meter.defaultSettings,
            valueLabel:
            {
                text: "",
                bitmapFont: Assets.Fonts.MegaWays.Small,
                font: null,
                fontSize: 45,
                dock: RS.Flow.Dock.Float,
                expand: RS.Flow.Expand.Allowed,
                align: RS.Rendering.TextOptions.Align.Middle,
                floatPosition: { x: 0.5, y: 0.5 }
            },
            nameLabel:
            {
                text: "",
                font: RS.Fonts.FrutigerWorld,
                fontSize: 0
            },
            fadeTime: 500,
            sizeToContents: true
        }
    }
}