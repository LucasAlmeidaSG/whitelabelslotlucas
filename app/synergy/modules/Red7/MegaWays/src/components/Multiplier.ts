/// <reference path="../generated/Assets.ts" />
namespace Red7.MegaWays.Components
{
    /** Animates a multiplier change. */
    export class Multiplier extends RS.Flow.BaseElement<Multiplier.Settings, Multiplier.RuntimeData>
    {
        @RS.AutoDisposeOnSet private _innerList: RS.Flow.List;
        @RS.AutoDisposeOnSet private _multiplyLabel: RS.Flow.Label;
        @RS.AutoDisposeOnSet private _numberLabels: RS.Flow.Label[];
        @RS.AutoDisposeOnSet private _tweens: RS.Tween<object>[]

        private _onAnimationFinished: RS.ISimplePrivateEvent = RS.createSimpleEvent();
        private flyTweenProxy: { t: number };

        /** Published when the animation sequence finishes. */
        public get onAnimationFinished() { return this._onAnimationFinished.public; }

        constructor(settings: Multiplier.Settings, runtime: Multiplier.RuntimeData)
        {
            super(settings, runtime);
            this._innerList = RS.Flow.list.create(this.settings.list, this);
            this._innerList.alpha = 0;
            this._multiplyLabel = RS.Flow.label.create({ ...this.settings.labels, text: "×" }, this._innerList);
        }

        public async do(mult: number)
        {
            if (mult <= 0 || (mult % 1) > 0) { throw new Error(`Bad multiplier: ${mult}`); }
            if (mult == 1) { return; } // It's resetting, so don't animate

            this._numberLabels = [];
            const valAsStr = mult.toString();
            for (let i = 0; i < valAsStr.length; i++)
            {
                this._numberLabels.push(RS.Flow.label.create({ ...this.settings.labels, text: valAsStr[i] }, this._innerList));
                this._numberLabels[i].alpha = 0;
            }

            this.invalidateLayout();

            this._tweens = [];
            this._innerList.alpha = 1;

            await this.landAnimation();

            await this.flyAnimation();

            this._innerList.postTransform = { x: 0, y: 0, scaleX: 1, scaleY: 1 };
            this._innerList.alpha = 0;
            this._onAnimationFinished.publish();
        }

        /** Each character slams down one by one */
        protected async landAnimation()
        {
            const anim = this.settings.animation;
            const labelsToAnimate = [this._multiplyLabel, ...this._numberLabels];
            RS.Audio.play(this.settings.impactSound);
            for (let i = 0; i < labelsToAnimate.length; i++)
            {
                const tween = RS.Tween.get(labelsToAnimate[i].postTransform)
                    .set({ scaleX: anim.startScale, scaleY: anim.startScale })
                    .to({ scaleX: anim.endScale, scaleY: anim.endScale }, anim.time, RS.Ease.circIn);
                this._tweens.push(tween);
                labelsToAnimate[i].alpha = 1;
                await tween;
            }

            const showTween = RS.Tween.wait(anim.showTime);
            this._tweens.push(showTween);
            await showTween;
        }

        // Tween the innerlist to the location of the multiplier meter
        protected async flyAnimation()
        {
            this.flyTweenProxy = { t: 0 };
            const flyTween = RS.Tween.get(this.flyTweenProxy)
                .to({ t: 1 }, this.settings.animation.flyTime);

            flyTween.onPositionChanged(() =>
            {
                let transformPoint = this.toLocal(this.runtimeData.animationTarget.offset, this.runtimeData.animationTarget.element);
                transformPoint = RS.Math.Vector2D.subtract(transformPoint, RS.Math.Vector2D(this.localBounds.w, this.localBounds.h));
                transformPoint = RS.Math.Vector2D.linearInterpolate(RS.Math.Vector2D.zero, transformPoint, this.flyTweenProxy.t);
                this._innerList.postTransform.x = transformPoint.x;
                this._innerList.postTransform.y = transformPoint.y;

                this._innerList.postTransform.scaleY = this._innerList.postTransform.scaleX = RS.Math.linearInterpolate(1, 0, this.flyTweenProxy.t);
            })

            await flyTween;
            flyTween.dispose();
        }
    }

    export const multiplier = RS.Flow.declareElement(Multiplier, true);

    export namespace Multiplier
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            labels: RS.Flow.Label.Settings;
            list: RS.Flow.List.Settings;
            animation:
            {
                startScale: number;
                endScale: number;
                time: number;
                showTime: number;
                flyTime: number;
            };
            impactSound: RS.Asset.SoundReference;
        }
        export interface RuntimeData
        {
            animationTarget:
            {
                element: RS.Flow.BaseElement<any, any>;
                offset: RS.Math.Vector2D;
            }
        }

        export const defaultSettings: Settings =
        {
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            sizeToContents: true,
            labels:
            {
                font: null,
                bitmapFont: MegaWays.Assets.Fonts.MegaWays.Large,
                fontSize: 202,
                text: ""
            },
            list:
            {
                direction: RS.Flow.List.Direction.LeftToRight,
                sizeToContents: true,
                dock: RS.Flow.Dock.Top
            },
            animation:
            {
                startScale: 1.5,
                endScale: 1.0,
                time: 50,
                showTime: 500,
                flyTime: 200
            },
            impactSound: MegaWays.Assets.SFX.Multiplier
        }
    }
}