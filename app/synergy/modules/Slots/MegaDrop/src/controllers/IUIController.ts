namespace RS.Slots.MegaDrop
{
    export interface IUIController extends RS.Slots.UI.IUIController
    {
        attachBackUIToContainer(
            container: RS.Flow.Container,
            showButtonArbiter: RS.Arbiter<boolean>,
            mobilePanelSettings?: SG.CommonUI.MobileButtonPanel.Settings,
            mobileMenuSettings?: SG.CommonUI.MobileMenuHolder.Settings);
    }

    export const IUIController = Controllers.declare<IUIController, RS.Slots.UI.IUIController.Settings>(Strategy.Type.Instance);
}
