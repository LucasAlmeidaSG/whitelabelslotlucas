namespace RS.Slots.Util
{
    /**
     * Generates an array of colors across the entire hue spectrum, suitable for winlines.
     * @param count Number of colors to generate.
     * @param saturation Saturation to use for each color (0-1).
     * @param value Value to use for each color (0-1).
     */
    export function generateWinlineColors(count: number, saturation: number = 0.8, value: number = 0.8): RS.Util.Color[]
    {
        const result = new Array<RS.Util.Color>(count);
        for (let i = 0; i < count; ++i)
        {
            const color = new RS.Util.Color();
            color.hsv = { h: Math.linearInterpolate(0, 360, i / count), s: saturation, v: value };
            result[i] = color;
        }
        return result;
    }
}