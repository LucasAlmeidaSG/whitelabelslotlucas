namespace RS.Slots
{
    export type ReadonlyArray2D<T> = ReadonlyArray<ReadonlyArray<T>>;

    /**
     * Encapsulates a position on a reel set.
     */
    export interface ReelSetPosition
    {
        reelIndex: number;
        rowIndex: number;
    }

    /**
     * Encapsulates symbols or other arbitrary data for a set of reels.
     */
    export class ReelSet<T = number>
    {
        protected _data: T[][];
        protected _rowCount: number;
        protected _type: ReelSet.Type;

        /**
         * Gets or sets the type of this reel set.
         * Changing from variable to fixed will resize all reels to match the largest reel's size.
         */
        public get type() { return this._type; }
        public set type(value)
        {
            if (this._type === value) { return; }
            switch (this._type = value)
            {
                case ReelSet.Type.Fixed:
                    this.rowCount = this._data
                        .map((r) => r.length)
                        .reduce((a, b) => Math.max(a, b), 0);
                    break;
            }
        }

        /**
         * Gets or sets the number of reels in this reel set.
         * Increasing the value will create new empty reels with the size of the rowCount property of this reel set.
         */
        public get reelCount() { return this._data.length; }
        public set reelCount(value)
        {
            if (value > this._data.length)
            {
                const rowCount = this.rowCount;
                for (let i = 0, l = value - this._data.length; i < l; ++i)
                {
                    this._data.push(new Array<T>(rowCount));
                }
            }
        }

        /**
         * Gets or sets the number of rows in this reel set.
         * If the reel set type is variable, getting will retrieve the largest contained reel.
         * Setting will always resize all reels to match the size.
         */
        public get rowCount()
        {
            switch (this._type)
            {
                case ReelSet.Type.Fixed:
                    return this._rowCount;
                case ReelSet.Type.Variable:
                    return this._data
                        .map((r) => r.length)
                        .reduce((a, b) => Math.max(a, b), 0);
                default:
                    return 0;
            }
        }
        public set rowCount(value)
        {
            this._rowCount = value;
            for (const reel of this._data)
            {
                reel.length = value;
            }
        }

        /**
         * Initialises an empty reel set.
         */
        public constructor();

        /**
         * Initialises a reel set with the specified reel and row count.
         */
        public constructor(reelCount: number, rowCount: number);

        /**
         * Initialises a reel set with the specified reel and row count, and default item.
         */
        public constructor(reelCount: number, rowCount: number, defaultItem: T);

        /**
         * Initialises a reel set with the specified reel and row count, default item, and type.
         */
        public constructor(reelCount: number, rowCount: number, defaultItem: T, type: ReelSet.Type);

        /**
         * Initialises a reel set from the specified data.
         * Passed in data will be cloned.
         */
        public constructor(data: number[][], transpose?: boolean);

        /**
         * Initialises a reel set from the specified data.
         * Passed in data will be cloned.
         */
        public constructor(data: ReadonlyArray2D<T>, transpose?: boolean);

        /**
         * Initialises a reel set from the specified existing reel set.
         * Passed in data will be cloned.
         */
        public constructor(data: ReelSet<T>);

        /**
         * Initialises a reel set from the specified existing reel set.
         * Passed in data will be cloned.
         */
        public constructor(p1?: number|ReadonlyArray2D<T>|ReelSet<T>, p2?: number|boolean, p3?: T, p4?: ReelSet.Type)
        {
            // Resolve overload
            if (Is.readonlyArray(p1))
            {
                if (p2 === true)
                {
                    this._data = RS.Util.transpose2DArray(p1);
                }
                else
                {
                    this._data = p1.map((a) => [...a]);
                }
                if (this._data.length === 0)
                {
                    this._type = ReelSet.Type.Fixed;
                    this._rowCount = 0;
                }
                else
                {
                    this._type = ReelSet.Type.Fixed;
                    const reel0 = this._data[0];
                    if (!reel0) { throw new Error("Reel 0 missing"); }
                    this._rowCount = reel0.length;
                    for (let i = 1, l = this._data.length; i < l; ++i)
                    {
                        const data = this._data[i];
                        if (!data) { throw new Error(`Reel ${i} missing`); }
                        if (data.length !== this._rowCount)
                        {
                            this._type = ReelSet.Type.Variable;
                            this._rowCount = 0;
                            break;
                        }
                    }
                    this._rowCount = this.rowCount;
                }
                return;
            }
            else if (p1 instanceof ReelSet)
            {
                this._data = p1._data.map((a) => [...a]);
                this._type = p1._type;
                this._rowCount = p1._rowCount;
            }
            else
            {
                this._data = [];
                this._type = p4 != null ? p4 : ReelSet.Type.Fixed;
                this._rowCount = Is.number(p2) ? p2 : 0;
                this.reelCount = p1;
                for (const reel of this._data)
                {
                    for (let i = 0, l = reel.length; i < l; ++i)
                    {
                        reel[i] = p3;
                    }
                }
            }
        }

        /**
         * Creates a deep copy of this reel set.
         */
        public clone(): ReelSet<T>
        {
            return new ReelSet(this);
        }

//ReelSetPosition

        /**
         * Gets if the specified position is valid.
         * @param reelIndex
         * @param rowIndex
         */
        public isPositionValid(reelIndex: number, rowIndex: number): boolean;

        /**
         * Gets if the specified position is valid.
         * @param position
         */
        public isPositionValid(position: ReelSetPosition): boolean;

        /**
         * Gets if the specified position is valid.
         * @param reelIndex
         * @param rowIndex
         */
        public isPositionValid(p1: number | ReelSetPosition, p2?: number): boolean
        {
            const reelIndex = Is.number(p1) ? p1 : p1.reelIndex;
            const rowIndex = Is.number(p1) ? p2 : p1.rowIndex;

            if (reelIndex < 0 || reelIndex >= this._data.length) { return false; }
            const reel = this._data[reelIndex];
            return rowIndex >= 0 && rowIndex < reel.length;
        }

        /**
         * Gets the item at the specified position.
         * Throws if the position is invalid.
         * @param reelIndex
         * @param rowIndex
         * @param wrapped If true, wrap row indices
         */
        public get(reelIndex: number, rowIndex: number, wrapped?: boolean): T;

        /**
         * Gets the item at the specified position.
         * Throws if the position is invalid.
         * @param position
         * @param wrapped If true, wrap row indices
         */
        public get(position: ReelSetPosition, wrapped?: boolean): T;

        /**
         * Gets the item at the specified position.
         * Throws if the position is invalid.
         * @param reelIndex
         * @param rowIndex
         * @param wrapped If true, wrap row indices
         */
        public get(p1: number | ReelSetPosition, p2: number | boolean, p3?: boolean): T
        {
            const reelIndex = Is.number(p1) ? p1 : p1.reelIndex;
            let rowIndex = Is.number(p1) ? p2 as number : p1.rowIndex;
            const wrapped = Is.boolean(p2) ? p2 : p3;

            if (reelIndex < 0 || reelIndex >= this._data.length)
            {
                throw new Error(`Position invalid (*reel index = ${reelIndex}*, row index = ${rowIndex})`);
            }
            const reel = this._data[reelIndex];
            if (rowIndex < 0 || rowIndex >= reel.length)
            {
                if (wrapped)
                {
                    rowIndex = Math.wrap(rowIndex, 0, reel.length);
                }
                else
                {
                    throw new Error(`Position invalid (reel index = ${reelIndex}, *row index = ${rowIndex}*)`);
                }
            }
            return reel[rowIndex];
        }

        /**
         * Gets a copy of the specified reel.
         * Throws if the position is invalid.
         * @param reelIndex
         */
        public getReel(reelIndex: number): T[]
        {
            if (reelIndex < 0 || reelIndex >= this._data.length)
            {
                throw new Error(`Position invalid (reel index = ${reelIndex})`);
            }
            return [...this._data[reelIndex]];
        }

        /**
         * Gets a copy of the specified row.
         * Throws if the position is invalid.
         * @param rowIndex
         */
        public getRow(rowIndex: number): T[]
        {
            if (rowIndex < 0 || rowIndex >= this.rowCount)
            {
                throw new Error(`Position invalid (row index = ${rowIndex})`);
            }
            const result: T[] = [];
            for (let reelIndex = 0, l = this._data.length; reelIndex < l; ++reelIndex)
            {
                result[reelIndex] = this._data[reelIndex][rowIndex];
            }
            return result;
        }

        /**
         * Gets the number of rows of the specified reel.
         * Throws if the position is invalid.
         * @param reelIndex
         */
        public getRowCount(reelIndex: number): number
        {
            if (reelIndex < 0 || reelIndex >= this._data.length)
            {
                throw new Error(`Position invalid (reel index = ${reelIndex})`);
            }
            return this._data[reelIndex].length;
        }

        /**
         * Sets the item at the specified position.
         * Throws if the position is invalid.
         * @param reelIndex
         * @param rowIndex
         * @param item
         */
        public set(reelIndex: number, rowIndex: number, item: T): void;

        /**
         * Sets the item at the specified position.
         * Throws if the position is invalid.
         * @param reelIndex
         * @param rowIndex
         * @param item
         */
        public set(position: ReelSetPosition, item: T): void;

        /**
         * Sets the item at the specified position.
         * Throws if the position is invalid.
         * @param reelIndex
         * @param rowIndex
         * @param item
         */
        public set(p1: number | ReelSetPosition, p2: number | T, p3?: T): void
        {
            const reelIndex = Is.number(p1) ? p1 : p1.reelIndex;
            const rowIndex = Is.number(p1) ? p2 as number : p1.rowIndex;
            const item = Is.number(p1) ? p3 as T : p2 as T;
            if (!this.isPositionValid(reelIndex, rowIndex))
            {
                throw new Error(`Position invalid (reel index = ${reelIndex}, row index = ${rowIndex})`);
            }
            this._data[reelIndex][rowIndex] = item;
        }

        /**
         * Sets the content of the specified reel.
         * Throws if the position is invalid, or the length of the reel doesn't match.
         * Undefined items will be ignored.
         * @param reelIndex
         */
        public setReel(reelIndex: number, data: ReadonlyArray<T>): void
        {
            if (reelIndex < 0 || reelIndex >= this._data.length)
            {
                throw new Error(`Position invalid (reel index = ${reelIndex})`);
            }
            const reel = this._data[reelIndex];
            if (reel.length !== data.length)
            {
                if (this._type === ReelSet.Type.Fixed)
                {
                    throw new Error("Reel size mismatch");
                }
                else
                {
                    reel.length = data.length;
                }
            }
            for (let i = 0, l = reel.length; i < l; ++i)
            {
                const item = data[i];
                if (item !== undefined)
                {
                    reel[i] = item;
                }
            }
        }

        /**
         * Sets the content of the specified row.
         * Throws if the position is invalid, or the data contains an item on an invalid position (e.g. when using variable length reels).
         * Undefined items will be ignored.
         * @param rowIndex
         */
        public setRow(rowIndex: number, data: ReadonlyArray<T>)
        {
            if (rowIndex < 0 || rowIndex >= this.rowCount)
            {
                throw new Error(`Position invalid (row index = ${rowIndex})`);
            }
            for (let reelIndex = 0, l = this._data.length; reelIndex < l; ++reelIndex)
            {
                const reel = this._data[reelIndex];
                const item = data[reelIndex];
                if (item !== undefined)
                {
                    if (rowIndex >= reel.length)
                    {
                        throw new Error(`Position invalid (reel index = ${reelIndex}, *row index = ${rowIndex}*)`);
                    }
                    reel[rowIndex] = item;
                }
            }
        }

        /**
         * Creates a copy of this ReelSet with the reels and rows transposed.
         * For example, 5x3 reels will become 3x5.
         */
        public transpose(): ReelSet<T>
        {
            return new ReelSet<T>(RS.Util.transpose2DArray<T>(this._data));
        }

        /**
         * Gets if the content of this reel set is a perfect match of the other.
         * @param other
         */
        public equals(other: ReelSet<T>): boolean
        {
            if (this.reelCount !== other.reelCount) { return false; }
            for (let i = 0, l = this.reelCount; i < l; ++i)
            {
                const myReel = this._data[i];
                const otherReel = other._data[i];
                if (myReel.length !== otherReel.length) { return false; }
                for (let j = 0, l2 = myReel.length; j < l2; ++j)
                {
                    if (myReel[j] !== otherReel[j]) { return false; }
                }
            }
            return true;
        }

        /**
         * Iterates all items within this reel set.
         * If the iterator returns a new item, this will be written back to this reel set.
         * @param iterator
         */
        public iterate(iterator: (reelIndex: number, rowIndex: number, item: T) => T|undefined): void;

        /**
         * Iterates all items within this reel set.
         * If the iterator returns a new item, this will be written back to this reel set.
         * @param iterator
         */
        public iterate(iterator: (reelIndex: number, rowIndex: number, item: T) => void): void;

        public iterate(iterator: (reelIndex: number, rowIndex: number, item: T) => T|undefined|void): void
        {
            const reelCount = this.reelCount;
            for (let reelIndex = 0; reelIndex < reelCount; ++reelIndex)
            {
                const reel = this._data[reelIndex];
                const rowCount = reel.length;
                for (let rowIndex = 0; rowIndex < rowCount; ++rowIndex)
                {
                    const item = reel[rowIndex];
                    const newItem = iterator(reelIndex, rowIndex, item);
                    if (newItem !== undefined) // it could be null and we want to accept that
                    {
                        reel[rowIndex] = newItem as T;
                    }
                }
            }
        }

        /**
         * Selects all positions where the specified predicate passes.
         * @param predicate
         */
        public selectPositions(predicate: (reelIndex: number, rowIndex: number, item: T) => boolean): ReelSetPosition[]
        {
            const result: ReelSetPosition[] = [];
            this.iterate((reelIndex, rowIndex, item) =>
            {
                if (predicate(reelIndex, rowIndex, item))
                {
                    result.push({ reelIndex, rowIndex });
                }
            });
            return result;
        }

        /**
         * Extracts a subset of this reel set.
         * Extracting past the length of a reel will wrap around.
         * @param indices The row indices to extract from.
         * @param sizes The sizes of each reel to extract.
         */
        public extractSubset(offsets: ReadonlyArray<number>, sizes: ReadonlyArray<number>): ReelSet<T>;

        /**
         * Extracts a subset of this reel set.
         * Extracting past the length of a reel will wrap around.
         * @param indices The row indices to extract from.
         * @param size The size of each reel to extract.
         */
        public extractSubset(offsets: ReadonlyArray<number>, size: number): ReelSet<T>;

        public extractSubset(offsets: ReadonlyArray<number>, sizes: number|ReadonlyArray<number>): ReelSet<T>
        {
            if (offsets.length > this.reelCount)
            {
                throw new Error("Tried to extract too many reels");
            }
            const newReelSet = new ReelSet<T>(offsets.length, Is.readonlyArray(sizes) ? 0 : sizes);
            if (Is.array(sizes))
            {
                if (sizes.length !== offsets.length)
                {
                    throw new Error("Sizes mismatch offsets");
                }
                newReelSet.type = ReelSet.Type.Variable;
            }
            for (let reelIndex = 0, l = offsets.length; reelIndex < l; ++reelIndex)
            {
                const newReel: T[] = [];
                for (let rowIndex = offsets[reelIndex], l2 = offsets[reelIndex] + (Is.array(sizes) ? sizes[reelIndex] : sizes); rowIndex < l2; ++rowIndex)
                {
                    newReel.push(this.get(reelIndex, rowIndex, true));
                }
                newReelSet.setReel(reelIndex, newReel);
            }
            return newReelSet;
        }

        /**
         * Converts this reel set to a native 2D array.
         */
        public toNative(): T[][]
        {
            return this.clone()._data;
        }
    }

    export namespace ReelSet
    {
        /** Represents different types of reel set. */
        export enum Type
        {
            /** The reel set contains reels of fixed length. */
            Fixed,

            /** The reel set contains reels of variable length. */
            Variable
        }
    }
}