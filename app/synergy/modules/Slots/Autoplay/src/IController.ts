namespace RS.Slots.Autoplay
{
    /**
     * Responsible for managing autoplay state.
     */
    export interface IController extends Controllers.IController<IController.Settings>
    {
        /** Gets or sets the locale to use. */
        locale: Localisation.Locale;

        /** Gets if autoplays are currently active. */
        readonly isAutoplaying: boolean;

        /** Published when autoplays have started */
        readonly onAutoplayStarted: IEvent<number>;

        /** Published when autoplays have stopped */
        readonly onAutoplayStopped: IEvent;

        /**
         * Initiates the specified number of autoplays.
         * @param autoplayCount 
         */
        start(autoplayCount: number): void;

        /**
         * Stops autoplaying.
         */
        stop(): void;

        /**
         * Signals that a single autoplay is complete.
         */
        consume(): void;
    }

    export const IController = Controllers.declare<IController, IController.Settings>(Strategy.Type.Singleton);

    export namespace IController
    {
        export interface Settings
        {
            autospinArbiter: IArbiter<boolean>;
            uiEnabledArbiter: IArbiter<boolean>;
            messageTextArbiter: IArbiter<Localisation.LocalisableString>;
            balanceObservable: IReadonlyObservable<number>;
            totalBetObservable: IReadonlyObservable<number>;
            autoplaysRemainingObservable: IObservable<number>;
        }
    }
}