/// <reference path="./generated/Translations.ts"/>
namespace RS.Slots.Autoplay
{
    /**
     * A dialog for selecting autoplay options.
     */
    @HasCallbacks
    export class Dialog extends RS.Flow.BaseElement<Dialog.Settings, Dialog.RuntimeData>
    {
        @AutoDisposeOnSet protected _spinCountSpinner: Flow.Spinner;
        @AutoDisposeOnSet protected _winLimitSpinner: Flow.Spinner;
        @AutoDisposeOnSet protected _lossLimitSpinner: Flow.Spinner;

        @AutoDisposeOnSet protected _cancelButton: Flow.Button;
        @AutoDisposeOnSet protected _startButton: Flow.Button;

        @AutoDispose protected readonly _spinCount: IObservable<number> = new Observable(0);
        @AutoDispose protected readonly _lossLimit: IObservable<number> = new Observable(0);
        @AutoDispose protected readonly _winLimit: IObservable<number> = new Observable(0);

        @AutoDisposeOnSet protected _stakeObservableHandler: IDisposable;

        /** Gets the current selected spin count. */
        public get spinCount() { return this._spinCount.value; }

        /** Gets the current selected loss limit (as a multiplier of total stake). */
        public get lossLimit() { return this._lossLimit.value; }

        /** Gets the current selected win limit (as a multiplier of total stake). */
        public get winLimit() { return this._winLimit.value; }

        /** Published when the cancel button has been clicked. */
        public get onCancelClicked() { return this._cancelButton.onClicked; }

        /** Published when the start button has been clicked. */
        public get onStartClicked() { return this._startButton.onClicked; }

        private readonly _spinCountOptions: ReadonlyArray<number>;
        private readonly _lossLimitOptions: ReadonlyArray<number>;
        private readonly _winLimitOptions: ReadonlyArray<number>;

        public constructor(settings: Dialog.Settings, runtimeData: Dialog.RuntimeData)
        {
            super(settings, runtimeData);

            this._spinCountOptions = this.getSpinCountOptions();
            this._lossLimitOptions = this.getLossLimitOptions();
            this._winLimitOptions = settings.winLimitOptions;

            Flow.background.create(settings.background, this);

            const list = Flow.list.create({
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                direction: Flow.List.Direction.TopToBottom,
                expand: Flow.Expand.Allowed,
                evenlySpaceItems: true,
                spacing: Flow.Spacing.all(settings.listSpacing),
                enforceMinimumSize: true
            }, this);

            Flow.label.create(settings.title, list);

            this._spinCountSpinner = Flow.spinner.create({
                ...settings.spinCountSpinner,
                options:
                {
                    type: Flow.BaseSpinner.Type.Options,
                    options: this._spinCountOptions.map((opt) => `${opt}`)
                }
            });
            this._spinCountSpinner.onValueChanged(this.handleSpinCountValueChanged);
            Flow.titled.create({
                title: settings.spinCountLabel,
                side: Flow.Titled.Side.Top,
                sizeToContents: true,
                spacing: Flow.Spacing.all(settings.spinnerSpacing)
            }, this._spinCountSpinner, list);

            this._lossLimitSpinner = Flow.spinner.create({
                ...settings.lossLimitSpinner,
                options:
                {
                    type: Flow.BaseSpinner.Type.Options,
                    options: this._lossLimitOptions.map((opt) => `${opt}`)
                }
            });
            this._lossLimitSpinner.onValueChanged(this.handleLossLimitValueChanged);
            Flow.titled.create({
                title: settings.lossLimitLabel,
                side: Flow.Titled.Side.Top,
                sizeToContents: true,
                spacing: Flow.Spacing.all(settings.spinnerSpacing)
            }, this._lossLimitSpinner, list);

            this._winLimitSpinner = Flow.spinner.create({
                ...settings.winLimitSpinner,
                options:
                {
                    type: Flow.BaseSpinner.Type.Options,
                    options: this._winLimitOptions.map((opt) => `${opt}`)
                }
            });
            this._winLimitSpinner.onValueChanged(this.handleWinLimitValueChanged);
            Flow.titled.create({
                title: settings.winLimitLabel,
                side: Flow.Titled.Side.Top,
                sizeToContents: true,
                spacing: Flow.Spacing.all(settings.spinnerSpacing)
            }, this._winLimitSpinner, list);

            const buttonList = Flow.list.create({
                sizeToContents: true,
                direction: Flow.List.Direction.LeftToRight,
                expand: Flow.Expand.Allowed,
                evenlySpaceItems: true,
                spreadItems: false
            }, list);

            this._cancelButton = Flow.button.create(settings.cancelButton, buttonList);
            this._startButton = Flow.button.create(settings.startButton, buttonList);

            const hasSpins = this._spinCount.map((val) => val > 0);
            RS.Disposable.bind(hasSpins, this);
            if (runtimeData.requireLossLimit)
            {
                const hasLossLimit = this._lossLimit.map((val) => val > 0);
                RS.Disposable.bind(hasLossLimit, this);

                const hasBoth = Observable.reduce(
                [
                    hasSpins,
                    this._lossLimit.map((val) => val > 0)
                ], Arbiter.AndResolver);
                RS.Disposable.bind(hasBoth, this);

                this._startButton.bindToObservables({ enabled: hasBoth });
            }
            else
            {
                this._startButton.bindToObservables({ enabled: hasSpins });
            }

            this.onParentChanged(this.updateSpinnerOptions);
            this._stakeObservableHandler = runtimeData.totalBetObservable.onChanged(this.updateSpinnerOptions);
            this.updateSpinnerOptions();
        }

        protected getSpinCountOptions(): number[]
        {
            const { maxSpinCount } = this.runtimeData;
            if (maxSpinCount == null)
            {
                return this.settings.spinCountOptions;
            }
            else
            {
                const opts = this.settings.spinCountOptions.filter((opt) => opt < maxSpinCount);
                if (opts[opts.length - 1] !== maxSpinCount)
                {
                    opts.push(maxSpinCount);
                }
                return opts;
            }
        }

        protected getLossLimitOptions(): number[]
        {
            const { requireLossLimit } = this.runtimeData;
            if (requireLossLimit)
            {
                return this.settings.lossLimitOptions.filter((x) => x > 0);
            }
            else
            {
                return this.settings.lossLimitOptions;
            }
        }

        protected onFlowParentChanged(): void
        {
            if (this.isDisposed) { return; }
            if (this.flowParent != null)
            {
                this.updateSpinnerOptions();
            }
            super.onFlowParentChanged();
        }

        @Callback
        protected updateSpinnerOptions(): void
        {
            if (this.isDisposed) { return; }
            this._spinCountSpinner.options = this._spinCountOptions.map((opt) => `${opt}`);
            this._winLimitSpinner.options = this._winLimitOptions.map((opt) => this.formatLimitOption(opt));
            this._lossLimitSpinner.options = this._lossLimitOptions.map((opt) => this.formatLimitOption(opt));
        }

        protected formatLimitOption(value: number): string | number
        {
            const cf = this.currencyFormatter;
            return value === 0
                ? Localisation.resolveLocalisableString(this.locale, Translations.Autoplay.Prompt.Never)
                : cf == null
                    ? value * this.runtimeData.totalBetObservable.value
                    : cf.format(value * this.runtimeData.totalBetObservable.value);
        }

        @Callback
        protected handleSpinCountValueChanged(): void
        {
            this._spinCount.value = this._spinCountOptions[this._spinCountSpinner.optionIndex];
        }

        @Callback
        protected handleWinLimitValueChanged(): void
        {
            this._winLimit.value = this._winLimitOptions[this._winLimitSpinner.optionIndex];
        }

        @Callback
        protected handleLossLimitValueChanged(): void
        {
            this._lossLimit.value = this._lossLimitOptions[this._lossLimitSpinner.optionIndex];
        }
    }

    export namespace Dialog
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            background: Flow.Background.Settings;
            title: Flow.Label.Settings;
            listSpacing: number;
            spinnerSpacing: number;

            spinCountLabel: Flow.Label.Settings;
            spinCountSpinner: Flow.Spinner.Settings;
            winLimitLabel: Flow.Label.Settings;
            winLimitSpinner: Flow.Spinner.Settings;
            lossLimitLabel: Flow.Label.Settings;
            lossLimitSpinner: Flow.Spinner.Settings;

            cancelButton: Flow.Button.Settings;
            startButton: Flow.Button.Settings;

            spinCountOptions: number[];
            winLimitOptions: number[];
            lossLimitOptions: number[];
        }

        export interface RuntimeData
        {
            /** Maximum spin count. */
            maxSpinCount?: number;
            requireLossLimit?: boolean;

            totalBetObservable: IReadonlyObservable<number>;
        }

        export const defaultSettings: Settings =
        {
            legacy: false,
            background: Flow.Background.defaultSettings,
            title:
            {
                ...Flow.Label.defaultSettings,

                text: Translations.Autoplay.Prompt.Title,
                fontSize: 32,

                expand: RS.Flow.Expand.Allowed,
                canWrap: true
            },
            sizeToContents: false,
            size: { w: 500, h: 500 },
            spacing: Flow.Spacing.all(10),
            listSpacing: 10,
            spinnerSpacing: 10,
            spinCountLabel:
            {
                ...Flow.Label.defaultSettings,

                text: Translations.Autoplay.Prompt.SpinCountTitle,
                fontSize: 24,

                expand: RS.Flow.Expand.Allowed,
                canWrap: false
            },
            spinCountSpinner:
            {
                ...Flow.Spinner.defaultSettings,
                sizeToContents: true
            },
            winLimitLabel:
            {
                ...Flow.Label.defaultSettings,

                text: Translations.Autoplay.Prompt.WinLimitTitle,
                fontSize: 24,

                expand: RS.Flow.Expand.Allowed,
                canWrap: false
            },
            winLimitSpinner:
            {
                ...Flow.Spinner.defaultSettings,
                sizeToContents: true
            },
            lossLimitLabel:
            {
                ...Flow.Label.defaultSettings,

                text: Translations.Autoplay.Prompt.LossLimitTitle,
                fontSize: 24,

                expand: RS.Flow.Expand.Allowed,
                canWrap: false
            },
            lossLimitSpinner:
            {
                ...Flow.Spinner.defaultSettings,
                sizeToContents: true
            },

            startButton:
            {
                ...Flow.Button.defaultSettings,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    text: Translations.Autoplay.Prompt.Start
                }
            },
            cancelButton:
            {
                ...Flow.Button.defaultSettings,
                textLabel:
                {
                    ...Flow.Button.defaultSettings.textLabel,
                    text: Translations.Autoplay.Prompt.Cancel
                }
            },

            spinCountOptions: [ 5, 25, 50, 100 ],
            lossLimitOptions: [ 0, 5, 25, 50, 100 ],
            winLimitOptions: [ 0, 5, 10, 20 ],

            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },

            scaleFactor: Device.isMobileDevice ? 2 : 1
        };
    }

    /**
     * A dialog for selecting autoplay options.
     */
    export const dialog = RS.Flow.declareElement(Dialog, true, Dialog.defaultSettings);
}