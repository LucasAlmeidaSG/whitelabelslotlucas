/// <reference path="IController.ts" />

namespace RS.Slots.Autoplay
{
    class Controller implements IController
    {
        public readonly onAutoplayStarted = createEvent<number>();
        public readonly onAutoplayStopped = createSimpleEvent();

        private _settings: IController.Settings;
        private _isDisposed: boolean = false;
        private _isAutoplaying: boolean = false;
        private _locale: Localisation.Locale | null = null;
        @AutoDispose private _autospinHandle: IDisposable | null = null;
        @AutoDispose private _uiEnabledHandle: IDisposable | null = null;
        @AutoDispose private _statusTextHandle: IDisposable | null = null;

        public get settings() { return this._settings; }

        public get isDisposed() { return this._isDisposed; }

        public get isAutoplaying() { return this._isAutoplaying; }

        public get locale() { return this._locale; }
        public set locale(value) { this._locale = value; }

        public initialise(settings: IController.Settings): void
        {
            this._settings = settings;
        }

        public start(autoplayCount: number): void
        {
            this._settings.autoplaysRemainingObservable.value = autoplayCount;
            if (this._isAutoplaying) { return; }
            this._isAutoplaying = true;
            this._autospinHandle = this._settings.autospinArbiter.declare(true);
            this._uiEnabledHandle = this._settings.uiEnabledArbiter.declare(false);
            if (this._locale)
            {
                this._statusTextHandle = this._settings.messageTextArbiter.declare(Translations.Autoplay.StatusText.get(this._locale, { count: autoplayCount }));
            }
            this.onAutoplayStarted.publish(autoplayCount);
        }

        public stop(): void
        {
            this._settings.autoplaysRemainingObservable.value = 0;
            if (!this._isAutoplaying) { return; }
            this._isAutoplaying = false;
            if (this._autospinHandle)
            {
                this._autospinHandle.dispose();
                this._autospinHandle = null;
            }
            if (this._uiEnabledHandle)
            {
                this._uiEnabledHandle.dispose();
                this._uiEnabledHandle = null;
            }
            if (this._statusTextHandle)
            {
                this._statusTextHandle.dispose();
                this._statusTextHandle = null;
            }
            this.onAutoplayStopped.publish();
        }

        public consume(): void
        {
            if (this._statusTextHandle)
            {
                this._statusTextHandle.dispose();
                this._statusTextHandle = null;
            }
            --this._settings.autoplaysRemainingObservable.value;
            if (this._locale)
            {
                this._statusTextHandle = this._settings.messageTextArbiter.declare(Translations.Autoplay.StatusText.get(this._locale, { count: this._settings.autoplaysRemainingObservable.value }));
            }
        }
        
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            
            this._isDisposed = true;
        }
    }

    IController.register(Controller);
}