namespace RS.Slots.Paytable.Pages
{
    /**
     * Base paytable page.
     */
    export abstract class Base<TSettings extends Base.Settings, TRuntimeData extends Base.RuntimeData> extends RS.Paytable.Pages.Base<TSettings, TRuntimeData>
    {
        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);
        }
    }

    export namespace Base
    {
        // tslint:disable-next-line:no-empty-interface
        export interface DefaultSettings extends RS.Paytable.Pages.Base.DefaultSettings
        {

        }

        // tslint:disable-next-line:no-empty-interface
        export interface Settings extends RS.Paytable.Pages.Base.Settings
        {

        }

        export interface RuntimeData extends RS.Paytable.Pages.Base.RuntimeData
        {
            configModel: Models.Config;
            stakeModel: Models.Stake;
        }
    }
}
