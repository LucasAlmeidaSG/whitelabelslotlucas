/// <reference path="Base.ts" />

namespace RS.Slots.Paytable.Pages
{
    /**
     * Standard winlines page.
     */
    export class Winlines<TSettings extends Winlines.Settings = Winlines.Settings, TRuntimeData extends Base.RuntimeData = Base.RuntimeData> extends Base<TSettings, TRuntimeData>
    {
        @AutoDisposeOnSet protected _grid: Flow.Grid | void;
        @AutoDisposeOnSet protected _contentList: Flow.List;
        @AutoDisposeOnSet protected _winLines: Elements.WinLine[] | void;

        protected get grid(): Flow.Grid
        {
            if (!this._grid) { this.createGrid(); }
            return this._grid as Flow.Grid;
        }

        protected get winLines(): Elements.WinLine[]
        {
            if (!this._winLines) { this.createWinLines(); }
            return this._winLines as Elements.WinLine[];
        }

        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);

            this._contentList = RS.Flow.list.create(this.settings.listSettings, this);

            if (this.settings.topText)
            {
                this.addText(this.settings.topText);
            }

            this.createGrid();

            if (this.settings.bottomText)
            {
                this.addText(this.settings.bottomText);
            }
        }

        protected createGrid(): void
        {
            this._grid = Flow.grid.create(this.settings.gridSettings, this._contentList);

            for (const winline of this.winLines)
            {
                this._grid.addChild(winline);
            }
        }

        protected addText(texts: RS.Flow.Label.Settings | RS.Flow.Label.Settings[])
        {
            if (!texts) { return; }
            if (!RS.Is.array(texts))
            {
                RS.Flow.label.create(texts, this._contentList);
            }
            else
            {
                for (const text of texts)
                {
                    RS.Flow.label.create(text, this._contentList);
                }
            }
        }

        protected createWinLines(): void
        {
            // Setup all the winlines
            const winLines: Models.Winline[] = this.runtimeData.configModel.winlines;

            const rangeStart = (this.settings.range && this.settings.range.start) || 0;
            const rangeSize = (this.settings.range && this.settings.range.length) || winLines.length;
            const rangeEnd = Math.min(rangeStart + rangeSize, winLines.length);

            this._winLines = [];
            for (let i = rangeStart; i < rangeEnd; i++)
            {
                const winlineDef = winLines[i];
                const runtimeData: Elements.WinLine.RuntimeData =
                {
                    index: i,
                    winLine: winlineDef
                };
                const winline = Elements.winLine.create(this.settings.winlineSettings, runtimeData);
                winline.name = (i + 1).toString();
                this._winLines.push(winline);
            }
        }
    }

    export const winlines = Flow.declareElement(Winlines, true);

    export namespace Winlines
    {
        export interface DefaultSettings extends Base.DefaultSettings
        {
            gridSettings: Flow.Grid.Settings;
        }

        export interface Settings extends DefaultSettings
        {
            listSettings: RS.Flow.List.Settings;
            winlineSettings: Elements.WinLine.Settings;
            range?: { start: number; length: number; };
            topText?: RS.Flow.Label.Settings | RS.Flow.Label.Settings[];
            bottomText?: RS.Flow.Label.Settings | RS.Flow.Label.Settings[];
        }

        export const defaultSettings: DefaultSettings =
        {
            gridSettings:
            {
                order: Flow.Grid.Order.RowFirst,
                dock: Flow.Dock.Fill,
                sizeToContents: true,
                spacing: RS.Flow.Spacing.all(8)
            }
        };
    }
}
