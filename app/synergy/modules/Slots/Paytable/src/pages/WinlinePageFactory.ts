namespace RS.Slots.Paytable.Pages
{
    export class WinlinePageFactory implements RS.Paytable.IPageFactory<Winlines.Settings, Base.RuntimeData>
    {
        constructor(protected readonly ctor: new (settings: Winlines.Settings, runtimeData: Base.RuntimeData) => Winlines, protected readonly settings: WinlinePageFactory.Settings)
        {

        }

        public create(overrideSettings: Partial<Winlines.Settings>, runtimeData: Base.RuntimeData): Winlines[];

        public create(runtimeData: Base.RuntimeData): Winlines[];

        public create(overrideSettingsOrRuntimeData: Partial<Winlines.Settings> | Base.RuntimeData, runtimeData?: Base.RuntimeData): Winlines[]
        {
            let overrideSettings: Partial<Winlines.Settings> = {};
            if (runtimeData)
            {
                overrideSettings = overrideSettingsOrRuntimeData;
            }
            else
            {
                runtimeData = overrideSettingsOrRuntimeData as Base.RuntimeData;
            }

            const winlines: Models.Winline[] = runtimeData.configModel.winlines;
            const rangeSize: number = this.settings.winLinesPerPage;

            const pages: Winlines[] = [];
            for (let rangeStart = 0; rangeStart < winlines.length; rangeStart += rangeSize)
            {
                const page = new this.ctor(
                {
                    ...this.settings.pageSettings,
                    ...overrideSettings,
                    range: { start: rangeStart, length: rangeSize }
                }, runtimeData);
                pages.push(page);
            }

            return pages;
        }
    }

    export namespace WinlinePageFactory
    {
        export interface Settings
        {
            pageSettings: Winlines.Settings;
            winLinesPerPage: number;
        }
    }
}
