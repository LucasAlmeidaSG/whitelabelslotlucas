namespace RS.Slots.Paytable.Pages
{

    export class TextPage<TSettings extends TextPage.Settings = TextPage.Settings, TRuntimeData extends Base.RuntimeData = Base.RuntimeData> extends RS.Paytable.Pages.TextPage<TSettings, TRuntimeData>
    {
        protected addMaxBetText(element: TextPage.MaxBetTextSettings, parent: RS.Flow.GenericElement, stake: Models.Stake): void
        {
            const maxStake = RS.Find.highest((stake.betOptions), (opt) => opt.value);
            const totalMax = Models.Stake.toTotal(stake, maxStake);

            const resolvedText = element.text.get(this.locale, { maxBet: this.currencyFormatter.format(totalMax.value, true) });
            this.addParagraph(element.paragraphSettings, resolvedText, parent);
        }
    }

    export namespace TextPage
    {
        /**
         * TODO: remove this
         * @deprecated
         */
        export import ElementKind = RS.Paytable.Pages.TextPage.ElementKind;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Settings = RS.Paytable.Pages.TextPage.Settings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import PageSettings = RS.Paytable.Pages.TextPage.PageSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import defaultPageSettings = RS.Paytable.Pages.TextPage.defaultPageSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import defaultSettings = RS.Paytable.Pages.TextPage.defaultSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ElementSettings = RS.Paytable.Pages.TextPage.ElementSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ElementGroup = RS.Paytable.Pages.TextPage.ElementGroup;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import BaseTextSettings = RS.Paytable.Pages.TextPage.BaseTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import DefaultText = RS.Paytable.Pages.TextPage.DefaultText;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import DefaultTextSettings = RS.Paytable.Pages.TextPage.DefaultTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import RtpTextSettings = RS.Paytable.Pages.TextPage.RtpTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import SingleRtpTextSettings = RS.Paytable.Pages.TextPage.SingleRtpTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import MaxWinTextSettings = RS.Paytable.Pages.TextPage.MaxWinTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import MaxBetTextSettings = RS.Paytable.Pages.TextPage.MaxBetTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import GameVersionTextSettings = RS.Paytable.Pages.TextPage.GameVersionTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import YearTextSettings = RS.Paytable.Pages.TextPage.YearTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ParagraphSettings = RS.Paytable.Pages.TextPage.ParagraphSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ImageSettings = RS.Paytable.Pages.TextPage.ImageSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ImageListSettings = RS.Paytable.Pages.TextPage.ImageListSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ComplexTextSettings = RS.Paytable.Pages.TextPage.ComplexTextSettings;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Element = RS.Paytable.Pages.TextPage.Element;
    }

    export const textPage = Flow.declareElement(TextPage, true);
}
