/// <reference path="Base.ts" />

namespace RS.Slots.Paytable.Pages
{
    /**
     * Standard symbol payout page.
     */
    export class SymbolPayouts<TSettings extends SymbolPayouts.Settings = SymbolPayouts.Settings, TRuntimeData extends Base.RuntimeData = Base.RuntimeData> extends Base<TSettings, TRuntimeData>
    {
        @AutoDisposeOnSet protected _grid: Flow.Grid;
        @AutoDisposeOnSet protected _gridList: Flow.List;
        @AutoDisposeOnSet protected _contentList: Flow.List;
        @AutoDisposeOnSet protected _textBackground: Flow.Background;
        @AutoDisposeOnSet protected _botContainer: Flow.Container;

        @AutoDisposeOnSet protected _animatedPayouts: Elements.BaseSymbolPayout<Flow.Image, Elements.BaseSymbolPayout.Settings, Elements.BaseSymbolPayout.RuntimeData>[] = [];

        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);

            this._gridList = Flow.list.create(settings.listSettings, this);

            this._textBackground = settings.textBackground ? Flow.background.create(settings.textBackground, this) : null;

            if (settings.textBackground)
            {
                this._contentList = Flow.list.create(settings.listSettings, this._textBackground);
            }
            else
            {
                this._botContainer = Flow.container.create(
                    {
                        name: "Text Container - Bottom",
                        dock: Flow.Dock.Top,
                        sizeToContents: true
                    }
                );
                this.addChildAbove(this._botContainer, this._gridList);
                this._contentList = Flow.list.create(settings.listSettings, this._botContainer)
            }

            this.createPayouts();
            this.addText();

            if (Is.boolean(settings.addAfterPayouts) && settings.addAfterPayouts === false)
            {
                if (this._textBackground)
                {
                    this.swapChildren(this._textBackground, this._gridList);
                }
                else
                {
                    this.swapChildren(this._botContainer, this._gridList);
                }
            }
        }

        public onAdded()
        {
            super.onAdded();

            if (this.settings.isAnimated) 
            {
                this.animatePayouts();
            }
        }

        public onRemoved()
        {
            super.onRemoved();
            
            if (this.settings.isAnimated)
            {
                for (let i = 0; i < this._animatedPayouts.length; i++)
                {
                    if (this._animatedPayouts)
                    {
                        this._animatedPayouts[i].stopSymbol();
                    }
                }
            }
        }

        @Callback
        protected async animatePayouts()
        {
            for (let i = 0; i < this._animatedPayouts.length; i++)
            {
                if (this._animatedPayouts)
                {
                    await this._animatedPayouts[i].playSymbol();
                }
            }
            if (this.parent && this._animatedPayouts)
            {
                this.animatePayouts();
            }
        }

        protected addText()
        {
            if (!this.settings.text) { return; }
            if (!Is.array(this.settings.text))
            {
                Flow.label.create(this.settings.text, this._contentList);
            }
            else
            {
                for (const text of this.settings.text)
                {
                    Flow.label.create(text, this._contentList);
                }
            }
            this.invalidateLayout();
        }

        protected createPayouts()
        {

            const gridSettings: Flow.Grid.Settings =
            {
                order: Flow.Grid.Order.RowFirst,
                dock: Flow.Dock.Fill
            };
            RS.Util.assign(this.settings.gridSettings, gridSettings, false);
            this._grid = Flow.grid.create(gridSettings, this._gridList);

            // Setup all the payouts
            for (const symbolDef of this.settings.symbols)
            {
                // Find the right binding
                let binding: Reels.Symbols.Binding | null = null;
                for (const b of this.settings.bindingSet)
                {
                    if (b.definition === symbolDef)
                    {
                        binding = b;
                        break;
                    }
                }
                if (binding)
                {
                    const payout = this.createSymbolPayout(binding);
                    this._grid.addChild(payout);

                    if (this.settings.isAnimated) {this._animatedPayouts.push(payout as Elements.BaseSymbolPayout<Flow.Image, Elements.BaseSymbolPayout.Settings, Elements.BaseSymbolPayout.RuntimeData>)}
                }
            }
        }

        protected createSymbolPayout(binding: Reels.Symbols.Binding): Elements.GenericSymbolPayout | Elements.BaseSymbolPayout<Flow.Image, Elements.BaseSymbolPayout.Settings, Elements.BaseSymbolPayout.RuntimeData>
        {
            if (this.settings.isAnimated && this.settings.isAnimated == true)
            {
                const payoutTableID = this.settings.payoutTableID || 0;

                const runtimeData: Elements.BaseSymbolPayout.RuntimeDataAnimated =
                {
                    kind: Elements.BaseSymbolPayout.SettingsType.Animated,
                    payouts: this.runtimeData.configModel.symbolPayoutTables[payoutTableID][binding.id],
                    stakeModel: this.runtimeData.stakeModel,
                    definition: binding.definition
                };

                return Elements.symbolPayoutAnim.create(this.settings.baseSymbolPayout, runtimeData);
            }
            else
            {
                const payoutTableID = this.settings.payoutTableID || 0;
                const runtimeData: Elements.SymbolPayout.RuntimeData =
                {
                    kind: Elements.BaseSymbolPayout.SettingsType.Static,
                    texture: this.settings.database.getRenderInfo(binding.definition).texture,
                    payouts: this.runtimeData.configModel.symbolPayoutTables[payoutTableID][binding.id],
                    stakeModel: this.runtimeData.stakeModel
                };
                return Elements.symbolPayout.create(this.settings.baseSymbolPayout, runtimeData);
            }
        }
    }

    export const symbolPayouts = Flow.declareElement(SymbolPayouts, true);

    export namespace SymbolPayouts
    {
        export interface Settings extends Base.Settings
        {
            bindingSet: Reels.Symbols.BindingSet;
            database: Reels.Symbols.IDatabase;
            symbols: Reels.Symbols.Definition[];
            payoutTableID?: number;

            baseSymbolPayout: Elements.SymbolPayout.Settings;

            gridSettings?: Partial<Flow.Grid.Settings>;
            listSettings: Flow.List.Settings;
            text?: Flow.Label.Settings | Flow.Label.Settings[];
            addAfterPayouts?: boolean;
            textBackground?: Flow.Background.Settings;
            isAnimated?: boolean;
        }
    }
}