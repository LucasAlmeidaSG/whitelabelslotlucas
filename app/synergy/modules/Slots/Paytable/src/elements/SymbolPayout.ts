/// <reference path="BaseSymbolPayout.ts" />

namespace RS.Slots.Paytable.Elements
{
    const defaultSymbolSettings: Flow.Image.Settings =
    {
        name: "Symbol",
        kind: Flow.Image.Kind.Texture,
        texture: null,
        dock: Flow.Dock.Top,
        sizeToContents: false,
        expand: Flow.Expand.Disallowed,
        scaleMode: Math.ScaleMode.Contain
    };

    const defaultPayoutsListSettings: Flow.List.Settings =
    {
        name: "Payouts List",
        direction: Flow.List.Direction.TopToBottom,
        dock: Flow.Dock.Fill,
        sizeToContents: true,
        expand: Flow.Expand.Disallowed,
        overflowMode: RS.Flow.OverflowMode.Shrink
    };

    const defaultPayoutsContainerSettings: Flow.Container.Settings =
    {
        name: "Payouts Container",
        dock: Flow.Dock.Top,
        sizeToContents: true
    };

    /**
     * The SymbolPayout element.
     */
    export class SymbolPayout extends BaseSymbolPayout<Flow.Image, SymbolPayout.Settings, SymbolPayout.RuntimeData>
    {
        protected createInner(): void
        {
            const symbolSettings = this.settings.symbolSettings ||
            {
                ...defaultSymbolSettings,
                size: this.settings.symbolSize,
                texture: this.runtimeData.texture
            }
            this._symbol = Flow.image.create(symbolSettings, this._list);

            const payoutsContainerSettings = this.settings.payoutsContainer ? this.settings.payoutsContainer : defaultPayoutsContainerSettings;
            this._payoutsContainer = Flow.container.create(payoutsContainerSettings, this._list);

            this._payoutsBG = this.settings.payoutsBackground ? Flow.background.create(this.settings.payoutsBackground, this._payoutsContainer) : null;

            const payoutsListSettings = this.settings.payoutsList ? this.settings.payoutsList : defaultPayoutsListSettings;
            this._payoutsList = Flow.list.create(payoutsListSettings, this._payoutsContainer);

            for (let i = this.runtimeData.payouts.length - 1; i >= 0; --i)
            {
                const payout = this.runtimeData.payouts[i];
                if (payout > 0)
                {
                    const row = this.createPayoutRow(i, payout);
                    this._payoutsList.addChild(row);
                }
            }
        }
    }

    export class SymbolPayoutAnim extends BaseSymbolPayout<Flow.Image, SymbolPayout.Settings, BaseSymbolPayout.RuntimeDataAnimated>
    {
        protected createInner():void { /* DO NOTHING */ }
    }

    export const symbolPayout = Flow.declareElement(SymbolPayout, true);
    export const symbolPayoutAnim = Flow.declareElement(SymbolPayoutAnim, true);

    export namespace SymbolPayout
    {
        export interface Settings extends BaseSymbolPayout.Settings
        {
            symbolSettings?: Flow.Image.Settings;
        }

        export interface RuntimeData extends BaseSymbolPayout.RuntimeDataStatic
        {
            texture: Rendering.ISubTexture;
        }
    }

    export namespace SymbolPayoutAnim
    {
        export type Settings = BaseSymbolPayout.Settings;
    }
}
