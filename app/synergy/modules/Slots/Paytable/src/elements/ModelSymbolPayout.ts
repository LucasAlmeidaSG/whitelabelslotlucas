/// <reference path="BaseSymbolPayout.ts" />

namespace RS.Slots.Paytable.Elements
{
    import Scene = RS.Rendering.Scene;

    const initialCameraOffset = Math.Vector3D(1.0, 1.0, 1.0);
    Math.Vector3D.normalise(initialCameraOffset);
    const negZ = Math.Vector3D(0.0, 0.0, -1.0);
    const tmpVec = Math.Vector3D();

    /**
     * The ModelSymbolPayout element.
     */
    @RS.HasCallbacks
    export class ModelSymbolPayout extends BaseSymbolPayout<Flow.SceneView, ModelSymbolPayout.Settings, ModelSymbolPayout.RuntimeData>
    {
        @RS.AutoDisposeOnSet protected _sceneRoot: Scene.ISceneObject | null;
        @RS.AutoDisposeOnSet protected _object: Scene.IMeshObject | null;
        @RS.AutoDisposeOnSet protected _ambientLight: Scene.IAmbientLightSource | null;
        @RS.AutoDisposeOnSet protected _camera: Scene.ICamera | null;

        protected _yaw: number;

        /** @inheritdoc */
        public dispose(): void
        {
            if (this.isDisposed) { return; }
            RS.Ticker.unregisterTickers(this);
            super.dispose();
        }

        public get meshObject()
        {
            return this._object;
        }

        protected createInner(): void
        {
            this._yaw = this.settings.cameraInitialYaw;

            this._sceneRoot = new Scene.SceneObject();

            this._camera = new Scene.Camera();
            this._camera.setPerspective(this.settings.cameraFov, 1.0);
            Math.Quaternion.fromEuler(this.settings.cameraPitch, this._yaw, 0.0, this._camera.rotation);
            Math.Quaternion.transform(this._camera.rotation, negZ, tmpVec);
            Math.Vector3D.multiply(tmpVec, this.settings.cameraOrbitDistance, this._camera.position);
            this._camera.backgroundColor = this.settings.cameraBackgroundColor;
            this._sceneRoot.addChild(this._camera);

            this._ambientLight = new Scene.AmbientLightSource(this.settings.ambientLightColor, this.settings.ambientLightIntensity);
            this._sceneRoot.addChild(this._ambientLight);

            const geometry = Is.assetReference(this.runtimeData.geometry) ? Asset.Store.getAsset(this.runtimeData.geometry).asset as Scene.IReadonlyGeometry : this.runtimeData.geometry;
            this._object = new Scene.MeshObject(geometry, this.runtimeData.materials);
            this._sceneRoot.addChild(this._object);

            this._symbol = Flow.sceneView.create(
                {
                    dock: Flow.Dock.Top,
                    sizeToContents: false,
                    size: this.settings.symbolSize,
                    expand: Flow.Expand.Disallowed
                }, {
                    camera: this._camera
                }, this._list);
            this._symbol.onLayouted(this.handleInnerLayouted);

            RS.Ticker.registerTickers(this);
        }

        @RS.Callback
        protected handleInnerLayouted(): void
        {
            this._camera.setPerspective(this.settings.cameraFov, this._symbol.layoutSize.w / this._symbol.layoutSize.h);
        }

        @RS.Tick({ kind: Ticker.Kind.Render, priority: Ticker.Priority.PreRender })
        protected tick(ev: RS.Ticker.Event): void
        {
            if (!this._sceneRoot) { return; }
            this._yaw += (ev.delta / 1000) * this.settings.cameraOrbitSpeed;
            Math.Quaternion.fromEuler(this.settings.cameraPitch, this._yaw, 0.0, this._camera.rotation);
            Math.Quaternion.transform(this._camera.rotation, negZ, tmpVec);
            Math.Vector3D.multiply(tmpVec, this.settings.cameraOrbitDistance, this._camera.position);
        }
    }

    export const modelSymbolPayout = Flow.declareElement(ModelSymbolPayout, true);

    export namespace ModelSymbolPayout
    {
        export interface Settings extends BaseSymbolPayout.Settings
        {
            cameraOrbitDistance: number;
            cameraOrbitSpeed: number;
            cameraPitch: number;
            cameraInitialYaw: number;
            cameraFov: number;
            cameraBackgroundColor: RS.Util.Color;

            ambientLightColor: RS.Util.Color;
            ambientLightIntensity: number;
        }

        export interface RuntimeData extends BaseSymbolPayout.RuntimeDataStatic
        {
            geometry: Asset.ModelReference | Rendering.Scene.IReadonlyGeometry;
            materials: Rendering.Scene.Material[];
        }
    }
}