namespace RS.Slots.Paytable.Elements
{
    /**
     * A reel grid with coloured blocks and a line to display a win line.
     */
    export class WinLineGrid extends RS.Flow.BaseElement<WinLineGrid.Settings, WinLineGrid.RuntimeData>
    {
        protected _isLayouted = false;
        protected _blockSize: RS.Math.Size2D | void;
        public get blockSize(): RS.Math.Size2D
        {
            if (!this._blockSize) { this._blockSize = { w: 0, h: 0 }; this.updateBlockSize(); }
            return this._blockSize;
        }

        protected _offset: RS.Math.Vector2D | void;
        protected get offset(): RS.Math.Vector2D
        {
            if (!this._offset) {this._offset = { x: 0, y: 0 }; this.updatePositionOffset(); }
            return this._offset;
        }

        protected calculateContentsSize(): Math.Size2D
        {
            return {
                w: this.settings.blockSize * this.settings.reelCount,
                h: this.settings.blockSize * this.settings.rowCount
            };
        }

        protected onLayout()
        {
            // this.cacheAsBitmap = false;

            super.onLayout();

            if (!this._isLayouted)
            {
                this._isLayouted = true;
                this.removeAllChildren();

                this.updateBlockSize();
                this.updatePositionOffset();
    
                this.createBackground();
                this.createBlocks();
                if (this.settings.gridColor)
                {
                    this.createGrid();
                }
    
                if (this.settings.lineColor)
                {
                    this.createLine();
                }
            }
            
            // this.cacheAsBitmap = true;
        }

        protected get gridHeight(): number
        {
            return this.settings.blockSize * this.settings.rowCount;
        }

        protected get gridWidth(): number
        {
            return this.settings.blockSize * this.settings.reelCount;
        }

        protected updateBlockSize(): void
        {
            Math.Size2D.set(this.blockSize, this.settings.blockSize, this.settings.blockSize);
        }

        protected updatePositionOffset(): void
        {
            // const transform = this.worldTransform;
            // const offsX = transform.tx - Math.floor(transform.tx);
            // const offsY = transform.ty - Math.floor(transform.ty);
            // Math.Vector2D.set(this.offset, offsX, offsY)
            Math.Vector2D.set(this.offset, 0, 0);
        }

        /**
         * Creates the background of the reel grid
         */
        protected createBackground(): void
        {
            const settings = this.settings;
            if (settings.bgColor)
            {
                const background = new RS.Rendering.Graphics();

                background.beginFill(settings.bgColor)
                    .drawRect(this.offset.x, this.offset.y, this.gridWidth, this.gridHeight);
                // background.setBounds(0, 0, settings.width, settings.height);

                this.addChild(background);
            }
            else if (settings.bgBitmap)
            {
                const background = Factory.bitmap(settings.bgBitmap);
                if (settings.bgBitmapOffset)
                {
                    background.setTransform(settings.bgBitmapOffset.x, settings.bgBitmapOffset.y);
                }
                this.addChild(background);
            }
        }

        /**
         * Creates the blocks of the win line at the specified symbol positions
         */
        protected createBlocks(): void
        {
            const settings = this.settings;
            const winLine = this.runtimeData.winLine;
            const block = new RS.Rendering.Graphics();
            let row = 0;
            let blockX = 0;
            let blockY = 0;

            for (let reel = 0; reel < settings.reelCount; reel++)
            {
                row = winLine[reel].rowIndex;
                blockX = reel * this.blockSize.w;
                blockY = row * this.blockSize.h;
                block.moveTo(blockX, blockY)
                              .beginFill(settings.blockColor)
                              .drawRect(blockX, blockY, this.blockSize.w, this.blockSize.h)
                              .endFill();
                // block.setBounds(blockX, blockY, blockWidth, blockHeight);
                this.addChild(block);
            }
        }

        /**
         * Creates the grid lines of the reels grid
         */
        protected createGrid(): void
        {
            const settings = this.settings;
            const grid = new RS.Rendering.Graphics();
            grid.lineStyle(settings.gridThickness, settings.gridColor);
            let gridLineX = this.offset.x;
            let gridLineY = this.offset.y;

            for (let verCount = 0; verCount <= settings.reelCount; verCount++)
            {
                grid.moveTo(gridLineX, this.offset.y);
                grid.lineTo(gridLineX, this.gridHeight);
                gridLineX += this.blockSize.w;
            }

            for (let horCount = 0; horCount <= settings.rowCount; horCount++)
            {
                grid.moveTo(this.offset.x, gridLineY);
                grid.lineTo(this.gridWidth, gridLineY);
                gridLineY += this.blockSize.h;
            }

            this.addChild(grid);
        }

        /**
         * Creates the win line
         */
        protected createLine(): void
        {
            const settings = this.settings;
            const blockCenterX = this.blockSize.w / 2;
            const blockCenterY = this.blockSize.h / 2;
            const winLine = this.runtimeData.winLine;
            const startX = -blockCenterX / 2;
            const startY = (winLine[0].rowIndex * this.blockSize.h) + blockCenterY;
            const endX = ((settings.reelCount - 1) * this.blockSize.w) + this.blockSize.w + blockCenterX / 2;
            const endY = (winLine[settings.reelCount - 1].rowIndex * this.blockSize.h) + blockCenterY;

            const outLine = new RS.Rendering.Graphics();
            outLine
                .lineStyle(settings.lineWidth + settings.lineOutlineWidth, settings.lineOutlineColor)
                .moveTo(startX, startY)
                .lineTo(blockCenterX, startY);

            const lineColor = RS.Is.array(settings.lineColor) ? settings.lineColor[this.runtimeData.index % settings.lineColor.length] : settings.lineColor;
            const line = new RS.Rendering.Graphics();
            line
                .lineStyle(settings.lineWidth, lineColor)
                .moveTo(startX, startY)
                .lineTo(blockCenterX, startY);

            for (let i = 1; i < settings.reelCount; i++)
            {
                line.lineTo((i * this.blockSize.w) + blockCenterX, (winLine[i].rowIndex * this.blockSize.h) + blockCenterY);
                outLine.lineTo((i * this.blockSize.w) + blockCenterX, (winLine[i].rowIndex * this.blockSize.h) + blockCenterY);
            }

            line.lineTo(endX, endY);
            outLine.lineTo(endX, endY);

            this.addChild(outLine);
            this.addChild(line);
        }
    }

    export const winLineGrid = RS.Flow.declareElement(WinLineGrid, true);

    export namespace WinLineGrid
    {
        /** Interface for the default settings. */
        export interface DefaultSettings extends Partial<Flow.ElementProperties>
        {
            /** Minimum size of a symbol block. */
            blockSize: number;

            /** The background colour of the grid */
            bgColor?: RS.Util.Color;

            /** The background picture of the grid */
            bgBitmap?: Asset.ImageReference;

            /** The background picture offset */
            bgBitmapOffset?: Math.Vector2D;

            /** The colour of the grid lines */
            gridColor?: RS.Util.Color

            /** The width of the grid cell borders. */
            gridThickness?: number;

            /** The colour of the symbol blocks */
            blockColor: RS.Util.Color;

            /** The colour of the win line */
            lineColor?: OneOrMany<RS.Util.Color>;

            /** The colour of the win line outline */
            lineOutlineColor?: RS.Util.Color;

            /** The width of the win line outline. */
            lineOutlineWidth?: number;

            /** The width of the win line */
            lineWidth?: number;
        }

        /** Settings for the WinLine component. */
        export interface Settings extends DefaultSettings
        {
            /** The number of reels to display in the grid */
            reelCount: number;

            /** The number of rows to display in the grid */
            rowCount: number;
        }

        /** Default settings for the WinLine component. */
        export const defaultSettings: DefaultSettings =
        {
            blockSize: 20,
            blockColor: RS.Util.Colors.white,
            gridColor: RS.Util.Colors.black,
            gridThickness: 1,

            lineColor: RS.Util.Colors.yellow,
            lineWidth: 8,
            lineOutlineColor: RS.Util.Colors.black,
            lineOutlineWidth: 4,
            
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export interface RuntimeData
        {
            /** The index of the winline */
            index?: number;
            /** WinLine data object. */
            winLine: Models.Winline;
        }
    }
}