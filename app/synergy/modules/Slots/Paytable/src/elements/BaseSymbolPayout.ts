namespace RS.Slots.Paytable.Elements
{
    const defaultListSettings: Flow.List.Settings =
    {
        name: "Element List",
        dock: Flow.Dock.Fill,
        direction: Flow.List.Direction.LeftToRight,
        overflowMode: RS.Flow.OverflowMode.Shrink,
    };

    /**
     * The SymbolPayout element.
     */
    export abstract class BaseSymbolPayout<
        TInner extends Flow.GenericElement,
        TSettings extends BaseSymbolPayout.Settings,
        TRuntimeData extends BaseSymbolPayout.RuntimeData
    > extends Flow.BaseElement<TSettings, TRuntimeData>
    {
        @AutoDisposeOnSet protected _bg: Flow.Background | void;
        @AutoDisposeOnSet protected _list: Flow.List;
        @AutoDisposeOnSet protected _symbol: TInner;
        @AutoDisposeOnSet protected _payoutsContainer: Flow.Container;
        @AutoDisposeOnSet protected _payoutsBG: Flow.Background | void;
        @AutoDisposeOnSet protected _payoutsList: Flow.List;

        @AutoDisposeOnSet protected _symbolSprite: Rendering.IAnimatedSprite | Rendering.ISpine;

        public constructor(settings: TSettings, runtimeData: TRuntimeData)
        {
            super(settings, runtimeData);
            this.createElements();
        }

        public async playSymbol()
        {
            if (this._symbolSprite instanceof Rendering.Spine)
            {
                this._symbolSprite.gotoAndPlay();
                await this._symbolSprite.onSpineComplete;
                this._symbolSprite.gotoAndStop();
            }
            else if (this._symbolSprite instanceof Rendering.AnimatedSprite)
            {
                this._symbolSprite.gotoAndPlay(this._symbolSprite.currentAnimation);
                await this._symbolSprite.onAnimationEnded;
                this._symbolSprite.gotoAndStop(this._symbolSprite.currentAnimation);
            }
        }

        public async stopSymbol()
        {
            if (this._symbolSprite && !this._symbolSprite.isDisposed)
            {
                if (this._symbolSprite instanceof Rendering.Spine)
                {
                    this._symbolSprite.gotoAndStop();
                }
                else if (this._symbolSprite instanceof Rendering.AnimatedSprite)
                {
                    this._symbolSprite.gotoAndStop(this._symbolSprite.currentAnimation);
                }
            }
        }

        protected createElements(): void
        {
            this._bg = this.settings.background ? Flow.background.create(this.settings.background, this) : null;

            const listSettings = { ...defaultListSettings, ...this.settings.listSettings };
            this._list = Flow.list.create(listSettings, this);

            this.createInner();

            const isAnimated = (data: BaseSymbolPayout.RuntimeData): data is BaseSymbolPayout.RuntimeDataAnimated => this.runtimeData.kind === BaseSymbolPayout.SettingsType.Animated;

            if (isAnimated(this.runtimeData))
            {
                const def = this.runtimeData.definition;

                if (def.spineAnimation != null)
                {
                    if (def.spineAnimation.scale == null) { def.spineAnimation.scale = 1; }
                    const spineAsset = Asset.Store.getAsset(def.spineAnimation.spineAsset) as object as Asset.ISpineAsset;
                    const spine = new Rendering.Spine(def.spineAnimation.scale, spineAsset);
                    if (def.spineAnimation.animationName != null)
                    {
                        spine.gotoAndStop(def.spineAnimation.animationName);
                        if (def.spineAnimation.animationDuration == null) { def.spineAnimation.animationDuration = spine.getAnimationLength(); }
                    }
                    this._symbolSprite = spine;
                }
                else if (def.spriteAsset)
                {
                    switch (def.spriteAsset.type)
                    {
                        case "spritesheet":
                        case "rsspritesheet":
                        {
                            const spriteSheet = Asset.Store.getAsset(def.spriteAsset).asset as Rendering.ISpriteSheet;
                            const spr = new Rendering.AnimatedSprite(spriteSheet);
                            spr.gotoAndStop(0);
                            if (def.animation != null && def.animation.framerate != null)
                            {
                                spr.framerate = def.animation.framerate;
                            }
                            if (def.animation != null && def.animation.name != null)
                            {
                                spr.gotoAndStop(def.animation.name);
                            }
                            this._symbolSprite = spr;
                            break;
                        }
                        default:
                        {
                            throw new Error(`Asset type ${def.spriteAsset.type} of the animated symbol payout sprite is not valid`);
                        }
                    }
                }

                this._bg ? this._bg.addChild(this._symbolSprite) : this.addChild(this._symbolSprite);

                if (this.settings.spriteTransPosX || this.settings.spriteTransPosY || this.settings.spriteTransScaleX || this.settings.spriteTransScaleY)
                {
                    this._symbolSprite.setTransform(this.settings.spriteTransPosX, this.settings.spriteTransPosY, this.settings.spriteTransScaleX, this.settings.spriteTransScaleY);
                }
                else
                {
                    this._symbolSprite.setTransform(95, 80, 0.7, 0.7);
                }
            }

            this._payoutsContainer = Flow.container.create(this.settings.payoutsContainer ? this.settings.payoutsContainer : BaseSymbolPayout.defaultSettings.payoutsContainer, this._list);

            this._payoutsBG = this.settings.payoutsBackground ? Flow.background.create(this.settings.payoutsBackground, this._payoutsContainer) : null;

            this._payoutsList = Flow.list.create(this.settings.payoutsList ? this.settings.payoutsList : BaseSymbolPayout.defaultSettings.payoutsList, this._payoutsContainer);

            for (let i = this.runtimeData.payouts.length - 1; i >= 0; --i)
            {
                const payout = this.runtimeData.payouts[i];
                if (payout > 0)
                {
                    const row = this.createPayoutRow(i, payout);
                    this._payoutsList.addChild(row);
                }
            }
        }

        protected abstract createInner(): void;

        protected createPayoutRow(index: number, amount: number): Flow.Container
        {
            const container = Flow.container.create(
            {
                sizeToContents: true,
                spacing: this.settings.payoutSpacing
            });

            if (this.settings.itemBackground)
            {
                Flow.background.create(this.settings.itemBackground, container);
            }

            const indexLabel = Flow.label.create(
            {
                ...this.settings.indexLabel,
                text: `${index}`,
                sizeToContents: true,
                dock: Flow.Dock.Left
            }, container);

            if (this.settings.payoutsAsMultipliers)
            {
                const valueLabel = Flow.label.create(
                {
                    ...this.settings.valueLabel,
                    text: `${amount}x`,
                    dock: Flow.Dock.Left,
                    contentAlignment: { x: 0.0, y: 0.5 }
                });
                container.addChild(valueLabel);
            }
            else
            {
                const valueLabel = Flow.currencyLabel.create(
                {
                    ...this.settings.valueLabel,
                    text: null,
                    sizeToContents: true,
                    dock: Flow.Dock.Fill
                });
                const currentBetOption = this.runtimeData.stakeModel.betOptions[this.runtimeData.stakeModel.currentBetIndex];

                let lineBet = null;
                if (this.settings.useTotalStakeForPayouts)
                {
                    lineBet = this.runtimeData.stakeModel.override != null ? this.runtimeData.stakeModel.override.totalBet : Slots.Models.Stake.toTotal(this.runtimeData.stakeModel, currentBetOption).value;
                }
                else
                {
                    lineBet = this.runtimeData.stakeModel.override != null ? this.runtimeData.stakeModel.override.betPerLine : Slots.Models.Stake.toPerLine(this.runtimeData.stakeModel, currentBetOption).value;
                }

                valueLabel.currencyValue = (amount * lineBet);
                container.addChild(valueLabel);
            }

            return container;
        }
    }

    export type GenericSymbolPayout = BaseSymbolPayout<Flow.GenericElement, BaseSymbolPayout.Settings, BaseSymbolPayout.RuntimeData>;

    export namespace BaseSymbolPayout
    {
        export interface Settings extends Partial<Flow.ElementProperties>
        {
            payoutsAsMultipliers: boolean;
            useTotalStakeForPayouts?: boolean;
            symbolSize: Math.Size2D;
            indexLabel: Flow.Label.Settings;
            valueLabel: Flow.Label.Settings;
            payoutSpacing: Flow.Spacing;
            listSettings?: Flow.List.Settings;

            payoutsContainer?: Flow.Container.Settings;
            payoutsList?: Flow.List.Settings;

            /** Background for the whole component */
            background?: Flow.Background.Settings;
            /** Background for the list of payout lines, excluding the symbol */
            payoutsBackground?: Flow.Background.Settings;

            spriteTransPosX?: number;
            spriteTransPosY?: number;
            spriteTransScaleX?: number;
            spriteTransScaleY?: number;

            /** Background for individual payout lines */
            itemBackground?: Flow.Background.Settings;
        }

        export enum SettingsType { Static, Animated }

        export interface RuntimeDataBase
        {
            payouts: number[];
            stakeModel: Slots.Models.Stake;
        }
        export interface RuntimeDataStatic extends RuntimeDataBase
        {
            kind?: SettingsType.Static;
        }

        export interface RuntimeDataAnimated extends RuntimeDataBase
        {
            kind: SettingsType.Animated;
            definition: Reels.Symbols.Definition;
        }

        export type RuntimeData = RuntimeDataStatic | RuntimeDataAnimated;

        export const defaultSettings: BaseSymbolPayout.Settings =
        {
            payoutsAsMultipliers: false,
            symbolSize: { w: 135, h: 135 },
            payoutsContainer:
            {
                dock: Flow.Dock.Right,
                sizeToContents: true,
                spacing: { left: 0, right: 35, top: 0, bottom: 0 },
            },
            indexLabel:
            {
                expand: Flow.Expand.Allowed,
                legacy: false,
                text: "",
                font: Rendering.Assets.Fonts.MyriadPro.Regular,
                fontSize: 22,
                dock: Flow.Dock.Top,
                align: Rendering.TextAlign.Left,
                size: { w: 30, h: 30 },
                sizeToContents: false,
                canWrap: false,
                renderSettings: { ...Rendering.TextOptions.defaultCanvasRenderSettings, padding: 10 }
            },
            valueLabel:
            {
                expand: Flow.Expand.Allowed,
                legacy: false,
                text: "",
                font: Rendering.Assets.Fonts.MyriadPro.Regular,
                fontSize: 22,
                dock: Flow.Dock.Top,
                align: Rendering.TextAlign.Right,
                sizeToContents: false,
                size: { w: 125, h: 30 },
                canWrap: false,
                renderSettings: { ...Rendering.TextOptions.defaultCanvasRenderSettings, padding: 10 }
            },
            payoutSpacing: Flow.Spacing.axis(7, 8),
            listSettings:
            {
                dock: Flow.Dock.Right,
                direction: Flow.List.Direction.LeftToRight,
                spacing: { left: 10, top: 5, right: 10, bottom: 5 },
                expandItems: false
            },
            payoutsList:
            {
                direction: Flow.List.Direction.TopToBottom,
                dock: Flow.Dock.Right,
                sizeToContents: false,
                size: { w: 155, h: 130 },
                contentAlignment: { x: 0.5, y: 0.5 },
                dockAlignment: { x: 0.5, y: 0.5},
                evenlySpaceItems: true
            }
        }
    }
}