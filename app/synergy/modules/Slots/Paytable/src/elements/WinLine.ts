/// <reference path="WinLineGrid.ts" />

namespace RS.Slots.Paytable.Elements
{
    /**
     * A reel grid with coloured blocks and a line to display a win line.
     */
    export class WinLine extends RS.Flow.BaseElement<WinLine.Settings, WinLine.RuntimeData>
    {
        protected _grid: WinLineGrid | void;
        protected get grid(): WinLineGrid
        {
            if (!this._grid) { this.createGrid(); }
            return this._grid as WinLineGrid;
        }

        protected _indexLabel: Flow.Label | void;
        protected get indexLabel(): Flow.Label
        {
            if (!this._indexLabel) { this.createIndexLabel(); }
            return this._indexLabel as Flow.Label;
        }
        
        public constructor(settings: WinLine.Settings, runtimeData: WinLine.RuntimeData)
        {
            super(settings, runtimeData);

            this.addChild(this.grid);
            this.addChild(this.indexLabel);
        }

        protected createGrid(): void
        {
            this._grid = winLineGrid.create(this.settings.gridSettings, this.runtimeData);
        }

        protected createIndexLabel(): void
        {
            this._indexLabel = Flow.label.create({ ...this.settings.label, text: this.runtimeData.index + 1 + "" });
        }
    }

    export const winLine = RS.Flow.declareElement(WinLine, true);

    export namespace WinLine
    {
        /** Interface for the default settings. */
        export interface DefaultSettings extends Partial<Flow.ElementProperties>
        {
            gridSettings: WinLineGrid.DefaultSettings;
            label: RS.Flow.Label.Settings;
        }

        /** Settings for the WinLine component. */
        export interface Settings extends DefaultSettings
        {
            gridSettings: WinLineGrid.Settings;
        }

        /** Default settings for the WinLine component. */
        export const defaultSettings: DefaultSettings =
        {
            dock: RS.Flow.Dock.Fill,
            sizeToContents: true,
            gridSettings:
            {
                ...WinLineGrid.defaultSettings,
                dock: RS.Flow.Dock.Top,
            },
            label:
            {
                ...RS.Flow.Label.defaultSettings,
                dock: RS.Flow.Dock.Top,
                fontSize: 32,
                layers:
                [
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        size: 5,
                        color: RS.Util.Colors.black
                    },
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.white
                    }
                ]
            }
        };

        export interface RuntimeData extends WinLineGrid.RuntimeData
        {
            /** Line index. */
            index: number;
        }
    }
}