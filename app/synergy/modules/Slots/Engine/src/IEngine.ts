/// <reference path="ISpinRequest.ts" />

namespace RS.Slots.Engine
{
    /**
     * Data for a "force" or "rig".
     */
    export interface ForceResult extends RS.Engine.ForceResult
    {
        stopIndices?: ReadonlyArray<number>;
        multiStopIndices?: Array<ReadonlyArray<number>>;
        reelSetIndex?: number;
        featureIndex?: number;
    }

    /**
     * Encapsulates a slots engine interface.
     */
    /* tslint:disable-next-line:no-empty-interface */
    export interface IEngine<
        TModels extends Models,
        TSettings extends object,
        TReplayData extends object = {},
        TForceResult extends ForceResult = ForceResult
    > extends RS.Engine.IEngine<TModels, TSettings, TReplayData, TForceResult>
    {
        /** Published when a spin response has been received and fully processed. */
        readonly onSpinResponseReceived: IEvent<IResponse>;

        /** Gets if this engine has reel sets. */
        readonly hasReelSets: boolean;

        spin(payload: ISpinRequest.Payload): PromiseLike<RS.Engine.IResponse>;
    }

    export type EngineType<
        TModels extends Models,
        TSettings extends object,
        TReplayData extends object = {},
        TForceResult extends ForceResult = ForceResult
    > = { new(settings: TSettings, models: TModels): IEngine<TModels, TSettings, TReplayData, TForceResult>; };

    /**
     * TODO: remove this
     * @deprecated
     */
    export import IResponse = RS.Engine.IResponse;
}
