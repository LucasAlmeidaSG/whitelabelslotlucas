namespace RS.Slots.Engine
{
    /**
     * Encapsulates a request to spin the reels.
     */
    export interface ISpinRequest<TPayload extends ISpinRequest.Payload = ISpinRequest.Payload, TResponse extends RS.Engine.IResponse = RS.Engine.IResponse> extends RS.Engine.IRequest<TPayload, TResponse> { }

    export namespace ISpinRequest
    {
        export interface Payload extends RS.Engine.ILogicRequest.Payload
        {
            betAmount: Models.StakeAmount;
            paylineCount: number;
        }
    }
}
