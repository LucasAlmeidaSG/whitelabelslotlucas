namespace RS.Tests.Slots.Models
{
    const { expect } = chai;
    import Stake = RS.Slots.Models.Stake;

    describe("Stake.ts", function ()
    {
        let model: Stake;
        beforeEach(function ()
        {
            model =
            {
                betOptions:
                [
                    { type: Stake.Type.CreditMultiplier, value: 2 },
                    { type: Stake.Type.Total, value: 300 },
                    { type: Stake.Type.PerLine, value: 40 },
                    { type: Stake.Type.Total, value: 493 },
                    { type: Stake.Type.PerLine, value: 60 }
                ],
                defaultBetIndex: 1,
                currentBetIndex: 1,
                paylineOptions: [10],
                defaultPaylineIndex: 0,
                currentPaylineIndex: 0,
                baseCreditValue: 88
            };
        });

        describe("toTotal", function ()
        {
            it("should return the same value when passed total bet", function ()
            {
                expect(Stake.toTotal(model, { value: 498, type: Stake.Type.Total }).value).to.equal(498);
            });
            it("should return the total bet when passed bet per line", function ()
            {
                expect(Stake.toTotal(model, { value: 22, type: Stake.Type.PerLine }).value).to.equal(220);
            });
            it("should return the total bet when passed credit multiplier", function ()
            {
                expect(Stake.toTotal(model, { value: 10, type: Stake.Type.CreditMultiplier }).value).to.equal(880);
            });
        });

        describe("toPerLine", function ()
        {
            it("should return the same value when passed bet per line", function ()
            {
                expect(Stake.toPerLine(model, { value: 302, type: Stake.Type.PerLine }).value).to.equal(302);
            });
            it("should return the bet per line when passed total bet", function ()
            {
                expect(Stake.toPerLine(model, { value: 420, type: Stake.Type.Total }).value).to.equal(42);
            });
            it("should return the bet per line when passed credit multiplier", function ()
            {
                expect(Stake.toPerLine(model, { value: 30, type: Stake.Type.CreditMultiplier }).value).to.equal(264);
            });
        });

        describe("toCreditMultiplier", function ()
        {
            it("should return the same value when passed credit multiplier", function ()
            {
                expect(Stake.toCreditMultiplier(model, { value: 8, type: Stake.Type.CreditMultiplier }).value).to.equal(8);
            });
            it("should return the credit multiplier when passed total bet", function ()
            {
                expect(Stake.toCreditMultiplier(model, { value: 264, type: Stake.Type.Total }).value).to.equal(3);
            });
            it("should return the credit multiplier when passed bet per line", function ()
            {
                expect(Stake.toCreditMultiplier(model, { value: 44, type: Stake.Type.PerLine }).value).to.equal(5);
            });
        });

        describe("getCurrentBet", function ()
        {
            it("should return the current bet", function ()
            {
                expect(Stake.getCurrentBet(model)).to.deep.equal({ type: Stake.Type.Total, value: 300 });

                model.currentBetIndex = 2;
                expect(Stake.getCurrentBet(model)).to.deep.equal({ type: Stake.Type.PerLine, value: 40 });
            });

            it("should return the overridden bet when present", function ()
            {
                model.override =
                {
                    totalBet: 200,
                    betPerLine: 20
                };
                expect(Stake.getCurrentBet(model)).to.deep.equal({ type: Stake.Type.Total, value: 200 });
            });
        });
    });
}