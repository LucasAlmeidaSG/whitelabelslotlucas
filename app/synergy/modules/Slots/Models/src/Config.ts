/// <reference path="Reels.ts" />

namespace RS.Slots.Models
{
    export type Winline = RS.Slots.ReelSetPosition[];

    export namespace Winline
    {
        /** Returns whether or not two given win-lines are equivalent. */
        export function equals(a: Winline, b: Winline): boolean
        {
            return RS.Util.arraysEqual(a, b, (posA, posB) => posA.reelIndex === posB.reelIndex && posA.rowIndex === posB.rowIndex);
        }
    }

    /**
     * Model for general slot game settings.
     */
    export interface Config extends RS.Models.Config
    {
        /** Complete reel sets for each reel set index. May not be present across all engines. */
        reelSets?: ReelSet[];

        /** Name of each symbol. */
        symbolNames: Localisation.LocalisableString[];

        /** Payouts for each symbol. */
        symbolPayouts: number[][];

        /** All symbol payout tables. */
        symbolPayoutTables: number[][][];

        /** Winlines for the primary reel set. */
        winlines?: Winline[];

        /** Winlines for other reel sets. */
        extraWinlines?: Winline[][];

        /** Whether or not the player can select paylines. */
        selectablePaylines: boolean;

        /** Default reels state. */
        defaultReels: Reels;

        /** Whether the big bet functionality is enabled. */
        bigBetEnabled: boolean;
    }

    export namespace Config
    {
        /** Populates the config model with default values. */
        export function clear(config: Config): void
        {
            RS.Models.Config.clear(config);
            config.reelSets = null;
            config.symbolNames = [];
            config.symbolPayouts = null;
            config.winlines = null;
            config.extraWinlines = null;
            config.defaultReels = {} as Reels;
            Reels.clear(config.defaultReels);
            config.bigBetEnabled = true;
        }
    }
}
