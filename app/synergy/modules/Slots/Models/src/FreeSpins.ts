namespace RS.Slots.Models
{
    /**
     * Model for free spins data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     */
    export interface FreeSpins
    {
        /** Whether the game is in free spins or not. */
        isFreeSpins: boolean;

        /** The number of free spins used. */
        used?: number;

        /** The number of free spins remaining. */
        remaining?: number;

        /** How much has been won from free spins so far. */
        winAmount?: number;

        /** How many free spins have been awarded in this spin. */
        extraSpinsAwarded?: number;
    }

    export namespace FreeSpins
    {
        /** Populates the customer model with default values. */
        export function clear(freeSpins: FreeSpins): void
        {
            freeSpins.isFreeSpins = false;
            freeSpins.used = null;
            freeSpins.remaining = null;
            freeSpins.winAmount = null;
            freeSpins.extraSpinsAwarded = null;
        }
    }
}