namespace RS.Slots.Models
{
    /**
     * Model for free spins data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     */
    export interface Jackpot
    {
        value: number;
    }

    export type Jackpots = { [name: string]: Jackpot };

    export namespace Jackpots
    {
        /** Populates the customer model with default values. */
        export function clear(jackpots: Jackpots): void
        {
            for (const key in jackpots)
            {
                delete jackpots[key];
            }
        }
    }
}