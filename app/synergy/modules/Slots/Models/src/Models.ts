/// <reference path="Config.ts" />
/// <reference path="FreeSpins.ts" />
/// <reference path="Reels.ts" />
/// <reference path="SpinResult.ts" />
/// <reference path="Stake.ts" />
/// <reference path="SpinResult.ts" />

namespace RS.Slots
{
    /**
     * Encapsulate all models common to a slot game.
     */
    export interface Models extends RS.Models
    {
        config: Models.Config;
        stake: Models.Stake;
        freeSpins: Models.FreeSpins;
        spinResults: Models.SpinResults;
        jackpots: Models.Jackpots;
    }

    export namespace Models
    {
        /** Populates the models with default values. */
        export function clear(models: Models): void
        {
            RS.Models.clear(models);

            models.config = models.config || {} as Models.Config;
            Models.Config.clear(models.config);

            models.stake = models.stake || {} as Models.Stake;
            Models.Stake.clear(models.stake);

            models.freeSpins = models.freeSpins || {} as Models.FreeSpins;
            Models.FreeSpins.clear(models.freeSpins);

            models.spinResults = models.spinResults || [];
            models.spinResults.length = 0;

            models.jackpots = models.jackpots || {} as Models.Jackpots;
            Models.Jackpots.clear(models.jackpots);
        }

        export namespace State
        {
            /**
             * TODO: remove this
             * @deprecated
             */
            export import Type = RS.Models.State.Type;
        }
    }
}
