/// <reference path="Win.ts" />
/// <reference path="Reels.ts" />

namespace RS.Slots.Models
{
    /**
     * Model for the result of a spin.
     */
    export interface SpinResult extends RS.Models.GameResult
    {
        /** State of the reels for this spin. */
        reels: Reels;

        /** Win data for this spin. */
        win: Win;
    }

    export namespace SpinResult
    {
        /** Populates the spin result model with default values. */
        export function clear(spinResult: SpinResult): void
        {
            RS.Models.GameResult.clear(spinResult);
            spinResult.reels = {} as Reels;
            Reels.clear(spinResult.reels);
        }

        /** Creates a deep clone of the spin result model. */
        export function clone(spinResult: SpinResult): SpinResult
        {
            const superClone = RS.Models.GameResult.clone(spinResult);
            return {
                ...superClone,
                reels:
                {
                    stopIndices: spinResult.reels.stopIndices.slice(),
                    symbolsInView: spinResult.reels.symbolsInView.clone(),
                    currentReelSetIndex: spinResult.reels.currentReelSetIndex
                },
                win:
                {
                    ...superClone.win,
                    winAmounts: { ...spinResult.win.winAmounts },
                    paylineWins: spinResult.win.paylineWins && spinResult.win.paylineWins.slice(),
                    scatterWins: spinResult.win.scatterWins && spinResult.win.scatterWins.slice(),
                    accumulatedWin: spinResult.win.accumulatedWin
                },
            };
        }
    }

    /**
     * Multiple spin results.
     */
    export type SpinResults = SpinResult[];

    export namespace SpinResults
    {
        /** Populates the spin results model with default values. */
        export function clear(spinResults: SpinResults): void
        {
            spinResults.length = 0;
        }

        /** Creates a deep clone of the spin results model. */
        export function clone(spinResults: SpinResults): SpinResults
        {
            return spinResults.map((sr) => SpinResult.clone(sr));
        }
    }
}
