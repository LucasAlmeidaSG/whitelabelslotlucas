namespace RS.Slots.Models
{
    export interface StakeOverride extends RS.Models.StakeOverride
    {
        betPerLine?: number;
    }

    export interface StakeAmount<T extends Stake.Type = Stake.Type> extends RS.Models.StakeAmount
    {
        type: T;
    }

    /**
     * Model for stake data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     */
    export interface Stake extends RS.Models.Stake
    {
        /** Bet options for the primary bet kind. */
        betOptions: StakeAmount[];

        /** Overridden stake data. */
        override?: StakeOverride

        /** Payline count options. */
        paylineOptions: number[];

        /** Index of the default payline count option. */
        defaultPaylineIndex: number;

        /** Index of the current selected payline count option. */
        currentPaylineIndex: number;

        /** If the stake calculation method is CreditMultiplier, the base stake amount. */
        baseCreditValue?: number;

        /** Number of extra paylines to add as the play bet amount */
        extraPayLines?: number;
    }

    export namespace Stake
    {
        /**
         * Converts the specified stake amount to a total stake.
         * @param model
         * @param value
         */
        export function toTotal(model: Stake, value: StakeAmount): StakeAmount<Type.Total>
        {
            switch (value.type)
            {
                case Type.Total:
                    return { type: Type.Total, value: value.value };
                case Type.PerLine:
                    return { type: Type.Total, value: value.value * (model.paylineOptions[model.currentPaylineIndex] + (model.extraPayLines || 0))};
                case Type.CreditMultiplier:
                    if (model.baseCreditValue == null) { throw new Error("Stake type was CreditMultiplier but no baseCreditValue is present"); }
                    return { type: Type.Total, value: value.value * model.baseCreditValue };
                default:
                    throw new Error(`Unknown stake type '${value.type}'`);
            }
        }

        /**
         * Converts the specified stake amount to a per-line stake.
         * @param model
         * @param value
         */
        export function toPerLine(model: Stake, value: StakeAmount): StakeAmount<Type.PerLine>
        {
            switch (value.type)
            {
                case Type.Total:
                    return { type: Type.PerLine, value: value.value / (model.paylineOptions[model.currentPaylineIndex] + (model.extraPayLines || 0)) };
                case Type.PerLine:
                    return { type: Type.PerLine, value: value.value };
                case Type.CreditMultiplier:
                    if (model.baseCreditValue == null) { throw new Error("Stake type was CreditMultiplier but no baseCreditValue is present"); }
                    return { type: Type.PerLine, value: value.value * model.baseCreditValue / model.paylineOptions[model.currentPaylineIndex] };
                default:
                    throw new Error(`Unknown stake type '${value.type}'`);
            }
        }

        /**
         * Converts the specified stake amount to a credit multiplier stake.
         * @param model
         * @param value
         */
        export function toCreditMultiplier(model: Stake, value: StakeAmount): StakeAmount<Type.CreditMultiplier>
        {
            if (model.baseCreditValue == null) { throw new Error("Stake type was CreditMultiplier but no baseCreditValue is present"); }
            switch (value.type)
            {
                case Type.Total:
                    return { type: Type.CreditMultiplier, value: value.value / model.baseCreditValue };
                case Type.PerLine:
                    return { type: Type.CreditMultiplier, value: (value.value * (model.paylineOptions[model.currentPaylineIndex] + (model.extraPayLines || 0))) / model.baseCreditValue };
                case Type.CreditMultiplier:
                    return { type: Type.CreditMultiplier, value: value.value };
                default:
                    throw new Error(`Unknown stake type '${value.type}'`);
            }
        }

        /**
         * Gets the currently selected bet.
         * @param model
         */
        export function getCurrentBet(model: Stake): StakeAmount
        {
            return model.override ?
            model.override.totalBet ? { type: Type.Total, value: model.override.totalBet } : {type: Type.PerLine, value: model.override.betPerLine} :
            model.betOptions[model.currentBetIndex];
        }

        /**
         * Gets the current bet value in the given type.
         * @param model
         * @param type
         */
        export function getCurrentBetAmount(model: Stake, type: Type): number
        {
            switch (type)
            {
                case Type.Total:
                    return toTotal(model, getCurrentBet(model)).value;
                case Type.PerLine:
                    return toPerLine(model, getCurrentBet(model)).value;
                case Type.CreditMultiplier:
                    return toCreditMultiplier(model, getCurrentBet(model)).value;
                default:
                    throw new Error(`Unknown stake type '${type}'`);
            }
        }

        /**
         * Sets the current bet value in the given type
         * @param model
         * @param type
         * @param value
         */
        export function setCurrentBetAmount(model: Stake, type: Type, value: number): void
        {
            for (let i = 0; i < model.betOptions.length; i++)
            {
                const betOption = model.betOptions[i];
                switch (type)
                {
                    case Type.Total:
                        const total = toTotal(model, betOption);
                        if (total.value === value)
                        {
                            model.currentBetIndex = i;
                            return;
                        }
                        break;

                    case Type.PerLine:
                        const perLine = toPerLine(model, betOption);
                        if (perLine.value === value)
                        {
                            model.currentBetIndex = i;
                            return;
                        }
                        break;

                    case Type.CreditMultiplier:
                        const multiplier = toCreditMultiplier(model, betOption);
                        if (multiplier.value === value)
                        {
                            model.currentBetIndex = i;
                            return;
                        }
                        break;

                    default:
                        throw new Error(`Unknown stake type '${type}'`);
                }
            }

            model.override = { totalBet: toTotal(model, { type, value }).value, betPerLine: toPerLine(model, { type, value }).value };
        }

        /** Represents the type of a given stake. */
        export enum Type
        {
            Total,
            PerLine,
            CreditMultiplier
        }

        /** Populates the stake model with default values. */
        export function clear(stake: Stake): void
        {
            stake.paylineOptions = [];
            stake.currentPaylineIndex = 0;
            stake.extraPayLines = 0;   // Added this, if set it will add extra paylines when dividing to give a true per payline value
            stake.baseCreditValue = null;
            RS.Models.Stake.clear(stake);
        }
    }
}
