namespace RS.Slots.Models
{
    /**
     * Information about a winning payline.
     */
    export interface PaylineWin
    {
        /** Index of the payline. */
        lineIndex: number;

        /** Whether the payline is reverse direction or not. */
        reverse: boolean;

        /** Win amount of this payline, as a multiplier of the stake per line. */
        multiplier: number;

        /** Win amount of this payline, as a sum of currency. */
        totalWin: number;

        /** Winning positions of the payline. */
        positions: { reelIndex: number, rowIndex: number }[];

        /** The winning symbol ID. */
        symbolID: number;
    }

    /**
     * Information about a scatter trigger win.
     */
    export interface ScatterWin
    {
        /** Win amount of this scatter trigger, as a sum of currency. */
        totalWin: number;

        /** Winning positions of the scatter trigger. */
        positions: { reelIndex: number, rowIndex: number }[];
    }

    /**
     * Model for win data.
     * All currency values should be in the lowest denomination of the currency (e.g. pennies).
     * There may be more than one of these present per spin.
     */
    export interface Win extends RS.Models.Win
    {
        /** Winning paylines. */
        paylineWins?: PaylineWin[];

        /** Winning scatters. */
        scatterWins?: ScatterWin[];

        /** Breakdown of win amounts (e.g. per feature). */
        winAmounts?: Map<number>;
    }

    export namespace Win
    {
        /** Populates the win model with default values. */
        export function clear(win: Win): void
        {
            RS.Models.Win.clear(win);
            win.paylineWins = null;
            win.winAmounts = null;
        }
    }
}
