namespace RS.Slots.Models
{
    /**
     * Model for reel state data.
     * There may be more than one of these present per spin.
     */
    export interface Reels
    {
        /** Index of the current reel set. */
        currentReelSetIndex: number;

        /** Current offsets into the current reel set. */
        stopIndices: number[];

        /** Current symbols to display on the reels. */
        symbolsInView: ReelSet | null;
    }

    export namespace Reels
    {
        /** Populates the reels model with default values. */
        export function clear(reels: Reels): void
        {
            reels.currentReelSetIndex = 0;
            reels.stopIndices = [];
            reels.symbolsInView = null;
        }

        /**
         * Computes a hash of the specified reels model.
         * @param reels 
         */
        export function hash(reels: Reels): number
        {
            return Math.hash([ reels.currentReelSetIndex, ...reels.stopIndices ]);
        }
    }
}