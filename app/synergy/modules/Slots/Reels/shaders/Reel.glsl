precision lowp float;

// If the symbols should utilise motion blur
#pragma meta default="1"
#pragma switch USE_MOTION_BLUR

// If the symbols should be masked
#pragma meta default="1"
#pragma switch USE_MASKING

varying vec2 vTextureCoord;
varying vec4 vColor;
varying float vTextureId;

#pragma meta private
uniform sampler2D uSampler;

// The blur texture page
uniform sampler2D uBlurTexture;

// How much to blur the symbol by
uniform float uBlurFactor;

// The clipping region (top, bottom)
#pragma meta targetsizeop=InverseYAll targetsizeop:rt=None
uniform vec2 uClipRegion;

// The fade size (top, bottom)
#pragma meta targetsizeop=InverseYAll targetsizeop:rt=None
uniform vec2 uFadeSize;

// Indicates whether the fade should be applied
#pragma meta default="1"
#pragma switch USE_FADE

void ReelFragment(void)
{
    vec4 c;
    float textureId = floor(vTextureId+0.5);
#ifdef USE_MOTION_BLUR

    if (textureId == 0.0)
    {
        c = mix(texture2D(uSampler, vTextureCoord), texture2D(uBlurTexture, vTextureCoord), uBlurFactor);
    }

#else

    if (textureId == 0.0)
    {
        c = texture2D(uSampler, vTextureCoord);
    }

#endif

    // Compute mask
#ifdef USE_MASKING

    float mask = 1.0;

#ifdef USE_FADE

    mask = smoothstep(uClipRegion.y, uFadeSize.y, gl_FragCoord.y) - smoothstep(uFadeSize.x, uClipRegion.x, gl_FragCoord.y);

#else

    if (gl_FragCoord.y > uClipRegion.y || gl_FragCoord.y < uFadeSize.x)
    {
        mask = 0.0;
    }

#endif

#else

    float mask = 1.0;

#endif

    // Combine
    gl_FragColor = c * mask * vColor;
}

#pragma export Shaders.Symbol Fragment=ReelFragment