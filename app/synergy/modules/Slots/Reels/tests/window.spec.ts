namespace RS.Reels.Tests
{
    const { expect } = chai;

    describe("Window.ts", function ()
    {
        describe("Window", function ()
        {
            const defaultStripRef = new RS.Reels.Symbols.Strip([ 1, 3, 2, 3, 2, 1, 3, 1, 2, 1, 3, 3, 3, 2 ]); // 14 long
            const defaultStrip = new RS.Reels.Symbols.Strip(defaultStripRef);
            const defaultBindingSet: RS.Reels.Symbols.BindingSet =
            [
                { id: 1, definition: { name: "Mock 1" }, selectionChance: 1 },
                { id: 2, definition: { name: "Mock 2" }, selectionChance: 1 },
                { id: 3, definition: { name: "Mock 3" }, selectionChance: 1 }
            ];

            function checkThatStripDidntChange(strip = defaultStrip, stripRef = defaultStripRef): void
            {
                expect(strip.equals(stripRef), "strip was changed").to.be.true;
            }

            describe("constructor", function ()
            {
                it("should create a window with no border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    expect(window.borderSize).to.equal(0);
                    expect(window.size).to.equal(3);
                    expect(window.bindingSet).to.equal(defaultBindingSet);
                    expect(window.strip).to.equal(defaultStrip);
                    expect(window["_symbols"]).to.have.lengthOf(3);
                    expect(window.isClear).to.be.true;
                    expect(window.position).to.equal(0);
                    expect(window.randomSymbolFunction).to.not.be.null;
                    checkThatStripDidntChange();
                });

                it("should create a window with a border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 1, defaultStrip);
                    expect(window.borderSize).to.equal(1);
                    expect(window.size).to.equal(3);
                    expect(window.bindingSet).to.equal(defaultBindingSet);
                    expect(window.strip).to.equal(defaultStrip);
                    expect(window["_symbols"]).to.have.lengthOf(5);
                    expect(window.isClear).to.be.true;
                    expect(window.position).to.equal(0);
                    expect(window.randomSymbolFunction).to.not.be.null;
                    checkThatStripDidntChange();
                });
            });

            describe("toStripIndex", function ()
            {
                it("should consider window position", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.position = 5;
                    expect(window.toStripIndex(0)).to.equal(5);
                    expect(window.toStripIndex(1)).to.equal(6);
                    expect(window.toStripIndex(2)).to.equal(7);
                    checkThatStripDidntChange();
                });

                it("should consider window border size", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 2, defaultStrip);
                    window.position = 5;
                    expect(window.toStripIndex(-2)).to.equal(3);
                    expect(window.toStripIndex(-1)).to.equal(4);
                    expect(window.toStripIndex(0)).to.equal(5);
                    expect(window.toStripIndex(1)).to.equal(6);
                    expect(window.toStripIndex(2)).to.equal(7);
                    expect(window.toStripIndex(3)).to.equal(8);
                    expect(window.toStripIndex(4)).to.equal(9);
                    checkThatStripDidntChange();
                });
            });

            describe("fromStripIndex", function ()
            {
                it("should consider window position", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.position = 5;
                    expect(window.fromStripIndex(5)).to.equal(0);
                    expect(window.fromStripIndex(6)).to.equal(1);
                    expect(window.fromStripIndex(7)).to.equal(2);
                    checkThatStripDidntChange();
                });

                it("should consider window border size", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 2, defaultStrip);
                    window.position = 5;
                    expect(window.fromStripIndex(3)).to.equal(-2);
                    expect(window.fromStripIndex(4)).to.equal(-1);
                    expect(window.fromStripIndex(5)).to.equal(0);
                    expect(window.fromStripIndex(6)).to.equal(1);
                    expect(window.fromStripIndex(7)).to.equal(2);
                    expect(window.fromStripIndex(8)).to.equal(3);
                    expect(window.fromStripIndex(9)).to.equal(4);
                    checkThatStripDidntChange();
                });
            });

            describe("toArrayIndex", function ()
            {
                it("should not alter row index if window has no border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    expect(window.toArrayIndex(0)).to.equal(0);
                    expect(window.toArrayIndex(1)).to.equal(1);
                    expect(window.toArrayIndex(2)).to.equal(2);
                });

                it("should consider border size if window has a border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 2, defaultStrip);
                    expect(window.toArrayIndex(-2)).to.equal(0);
                    expect(window.toArrayIndex(-1)).to.equal(1);
                    expect(window.toArrayIndex(0)).to.equal(2);
                    expect(window.toArrayIndex(1)).to.equal(3);
                    expect(window.toArrayIndex(2)).to.equal(4);
                    expect(window.toArrayIndex(3)).to.equal(5);
                    expect(window.toArrayIndex(4)).to.equal(6);
                });
            });

            describe("fromArrayIndex", function ()
            {
                it("should not alter row index if window has no border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    expect(window.fromArrayIndex(0)).to.equal(0);
                    expect(window.fromArrayIndex(1)).to.equal(1);
                    expect(window.fromArrayIndex(2)).to.equal(2);
                });

                it("should consider border size if window has a border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 2, defaultStrip);
                    expect(window.fromArrayIndex(0)).to.equal(-2);
                    expect(window.fromArrayIndex(1)).to.equal(-1);
                    expect(window.fromArrayIndex(2)).to.equal(0);
                    expect(window.fromArrayIndex(3)).to.equal(1);
                    expect(window.fromArrayIndex(4)).to.equal(2);
                    expect(window.fromArrayIndex(5)).to.equal(3);
                    expect(window.fromArrayIndex(6)).to.equal(4);
                });
            });

            describe("get", function ()
            {
                it("should get from the strip when the window position is empty", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.position = 3; // 3, 2, 1
                    expect(window.get(0).symbolID).to.equal(3);
                    expect(window.get(1).symbolID).to.equal(2);
                    expect(window.get(2).symbolID).to.equal(1);
                    window.set(1, 3);
                    expect(window.get(0).symbolID).to.equal(3);
                    expect(window.get(1).symbolID).to.equal(3);
                    expect(window.get(2).symbolID).to.equal(1);
                    checkThatStripDidntChange();
                });

                it("should get from a row index", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.position = 3; // 3, 2, 1
                    window.set(1, 4);
                    expect(window.get(1).symbolID).to.equal(4, "normal case");
                    expect(window.get(1 + window.strip.length).symbolID).to.equal(4, "+ strip length");
                    expect(window.get(1 - window.strip.length).symbolID).to.equal(4, "- strip length");
                    checkThatStripDidntChange();
                });
            });

            describe("isOpaque", function ()
            {
                it("should return true after setting", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    expect(window.isOpaque(0)).to.be.false;
                    expect(window.isOpaque(1)).to.be.false;
                    expect(window.isOpaque(2)).to.be.false;
                    window.set(1, 1);
                    expect(window.isOpaque(1)).to.be.true;
                    checkThatStripDidntChange();
                });
            });

            describe("set", function ()
            {
                it("should write a single symbol to a window with no border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    function testSet(index: number): void
                    {
                        window.set(index, 1);
                        expect(window.get(index).symbolID).to.equal(1);
                        window.set(index, 2, 0.5, RS.Util.Colors.blue);
                        expect(window.get(index)).to.deep.equal({ symbolID: 2, opacity: 0.5, tint: RS.Util.Colors.blue });
                    }
                    testSet(0);
                    testSet(1);
                    testSet(2);
                    checkThatStripDidntChange();
                });

                it("should write a single symbol to a window with a border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 2, defaultStrip);
                    function testSet(index: number): void
                    {
                        window.set(index, 1);
                        expect(window.get(index).symbolID).to.equal(1);
                        window.set(index, 2, 0.5, RS.Util.Colors.blue);
                        expect(window.get(index)).to.deep.equal({ symbolID: 2, opacity: 0.5, tint: RS.Util.Colors.blue });
                    }
                    testSet(-2);
                    testSet(-1);
                    testSet(0);
                    testSet(1);
                    testSet(2);
                    testSet(3);
                    testSet(4);
                    checkThatStripDidntChange();
                });

                it("should write multiple symbols to a window", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.set(0, [ 1, 2, 3 ]);
                    expect(window.get(0).symbolID).to.equal(1);
                    expect(window.get(1).symbolID).to.equal(2);
                    expect(window.get(2).symbolID).to.equal(3);
                    checkThatStripDidntChange();
                });

                it("should not do anything when setting with out of bounds index", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    let didError = false;
                    try
                    {
                        window.set(-1, 1);
                    }
                    catch
                    {
                        didError = true;
                    }
                    expect(didError).to.be.false;
                    checkThatStripDidntChange();
                });
            });

            describe("capture", function ()
            {
                it("should copy the strip at the window's current position, when window has no border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.position = 4;
                    const oldSymbolIDs: number[] = [];
                    for (let i = 0; i < 3; ++i)
                    {
                        expect(window.isOpaque(i)).to.be.false;
                        oldSymbolIDs[i] = window.get(i).symbolID;
                    }
                    window.capture();
                    for (let i = 0; i < 3; ++i)
                    {
                        expect(window.isOpaque(i)).to.be.true;
                        expect(window.get(i).symbolID).to.equal(oldSymbolIDs[i]);
                    }
                    checkThatStripDidntChange();
                });

                it("should copy the strip at the window's current position, when window has a border", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 2, defaultStrip);
                    window.position = 4;
                    const oldSymbolIDs: number[] = [];
                    for (let i = 0; i < 7; ++i)
                    {
                        expect(window.isOpaque(i - 2)).to.be.false;
                        oldSymbolIDs[i] = window.get(i - 2).symbolID;
                    }
                    window.capture();
                    for (let i = 0; i < 7; ++i)
                    {
                        expect(window.isOpaque(i - 2)).to.be.true;
                        expect(window.get(i - 2).symbolID).to.equal(oldSymbolIDs[i]);
                    }
                    checkThatStripDidntChange();
                });
            });

            describe("findExtent", function ()
            {
                function testExtent(windowData: number[], windowPos: number, expectedExtents: number[], direction: RS.Reels.Symbols.SearchDirection, message?: string): void
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.position = windowPos;
                    for (let i = 0, l = windowData.length; i < l; ++i)
                    {
                        window.set(i, windowData[i]);
                    }
                    for (let i = 0, l = expectedExtents.length; i < l; ++i)
                    {
                        expect(window.findExtent(window.fromStripIndex(i), direction)).to.equal(expectedExtents[i], `${message ? `${message}: ` : ""}${i}`);
                    }
                }
                it("should find the forwards extent", function ()
                {
                    testExtent([ 1, 1, 1 ], 0, [ 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 2, 1, 1 ], Reels.Symbols.SearchDirection.Forwards, "case 1"); // [1, 1, 1], 3, 2, 1, 3, 1, 2, 1, 3, 3, 3, 2
                    testExtent([ 3, 3, 3 ], -1, [ 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 5, 4, 3 ], Reels.Symbols.SearchDirection.Forwards, "case 2"); // 3, 3], 2, 3, 2, 1, 3, 1, 2, 1, 3, 3, 3, [3
                    testExtent([ 2, 2, 3 ], 3, [ 1, 1, 3, 2, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1 ], Reels.Symbols.SearchDirection.Forwards, "case 3"); // 1, 3, 2, [2, 2, 3], 3, 1, 2, 1, 3, 3, 3, 2
                    checkThatStripDidntChange();
                });
                it("should find the backwards extent", function ()
                {
                    testExtent([ 1, 1, 1 ], 0, [ 1, 2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 1 ], Reels.Symbols.SearchDirection.Backwards, "case 1"); // [1, 1, 1], 3, 2, 1, 3, 1, 2, 1, 3, 3, 3, 2
                    testExtent([ 3, 3, 3 ], -1, [ 5, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 4 ], Reels.Symbols.SearchDirection.Backwards, "case 2"); // 3, 3], 2, 3, 2, 1, 3, 1, 2, 1, 3, 3, 3, [3
                    testExtent([ 2, 2, 3 ], 3, [ 1, 1, 1, 2, 3, 1, 2, 1, 1, 1, 1, 2, 3, 1 ], Reels.Symbols.SearchDirection.Backwards, "case 3"); // 1, 3, 2, [2, 2, 3], 3, 1, 2, 1, 3, 3, 3, 2
                    checkThatStripDidntChange();
                });
            });

            describe("clear", function ()
            {
                it("should clear the window", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    window.set(0, 3);
                    window.set(1, 3);
                    window.set(2, 3);
                    expect(window.isOpaque(0)).to.be.true;
                    expect(window.isOpaque(1)).to.be.true;
                    expect(window.isOpaque(2)).to.be.true;
                    expect(window.isClear).to.be.false;
                    window.clear();
                    expect(window.isOpaque(0)).to.be.false;
                    expect(window.isOpaque(1)).to.be.false;
                    expect(window.isOpaque(2)).to.be.false;
                    expect(window.isClear).to.be.true;
                    checkThatStripDidntChange();
                });
            });

            describe("dispose", function ()
            {
                it("should dispose the window", function ()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 0, defaultStrip);
                    expect(window.isDisposed).to.be.false;
                    window.dispose();
                    expect(window.isDisposed).to.be.true;
                    checkThatStripDidntChange();
                });
            });

            describe("maintainSymbolSpanIntegrity", function ()
            {
                it("should modify the window border to maintain symbol span integrity", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 5, 5, 5, 2, 4, 4, 2, 1, 2, 3, 1, 5, 5, 5, 4, 4, 1, 4, 4, 3, 2, 1, 2, 5, 5, 5, 5, 5, 5, 1, 4, 4, 3, 1, 4, 4 ]); // 36
                    const bindingSet: RS.Reels.Symbols.BindingSet =
                    [
                        { id: 1, definition: { name: "Mock 1 - No Span" }, selectionChance: 1 },
                        { id: 2, definition: { name: "Mock 2 - No Span" }, selectionChance: 1 },
                        { id: 3, definition: { name: "Mock 3 - No Span" }, selectionChance: 1 },
                        { id: 4, definition: { name: "Mock 4 - Span 2", span: 2 }, selectionChance: 1 },
                        { id: 5, definition: { name: "Mock 5 - Span 3", span: 3 }, selectionChance: 1 }
                    ];
                    const testStops: number[][] =
                    [
                        [ 1, 2, 1 ],
                        [ 5, 1, 1 ],
                        [ 5, 5, 2 ],
                        [ 5, 5, 5 ],
                        [ 2, 5, 5 ],
                        [ 1, 2, 5 ],
                        [ 4, 1, 2 ],
                        [ 4, 4, 3 ],
                        [ 4, 4, 4 ],
                        [ 2, 4, 4 ],
                        [ 1, 3, 4 ],
                        [ 5, 3, 4 ],
                        [ 5, 5, 4 ],
                        [ 5, 4, 4 ],
                        [ 4, 2, 5 ],
                        [ 4, 4, 5 ],
                        [ 4, 5, 5 ]
                    ];
                    function dump(): void
                    {
                        const segments: string[] = [];
                        for (let i = 0, l = strip.length; i < l; ++i)
                        {
                            const rowIndex = window.fromStripIndex(i);
                            const symbolID = window.get(rowIndex).symbolID;
                            if (i > 0) { segments.push(", "); }
                            if (rowIndex === 0) { segments.push("["); }
                            segments.push(`${symbolID}`);
                            if (rowIndex === 2) { segments.push("]"); }
                        }
                        Log.debug(segments.join(""));
                    }
                    function checkSpanIntegrity(message: string): void
                    {
                        try
                        {
                            for (let i = 0, l = strip.length; i < l; ++i)
                            {
                                const rowIndex = window.fromStripIndex(i);
                                const symbolID = window.get(rowIndex).symbolID;
                                expect(symbolID).to.be.a("number", `${message}, i=${i}`);
                                expect(symbolID).to.be.within(1, 5, `${message}, i=${i}`);
                                const span = bindingSet[symbolID - 1].definition.span || 1;
                                const downExtent = window.findExtent(rowIndex, RS.Reels.Symbols.SearchDirection.Forwards);
                                const upExtent = window.findExtent(rowIndex, RS.Reels.Symbols.SearchDirection.Backwards);
                                const extent = upExtent + downExtent - 1;
                                expect(extent % span).to.equal(0, `${message}, i=${i}`);
                                i += (downExtent - 1);
                            }
                        }
                        catch (err)
                        {
                            dump();
                            throw err;
                        }
                    }
                    const window = new RS.Reels.Symbols.Window(bindingSet, 3, 4, strip);
                    checkSpanIntegrity(`control`);
                    for (let i = 0, l = strip.length; i < l; ++i)
                    {
                        window.position = i;
                        for (let j = 0, l2 = testStops.length; j < l2; ++j)
                        {
                            window.clear();
                            window.set(0, testStops[j]);
                            checkSpanIntegrity(`p=${i}, s=${j}`);
                        }
                    }
                });
            });

            describe("matchBorderSymbols", function()
            {
                it ("should update border symbols to match reel strip", function()
                {
                    const window = new RS.Reels.Symbols.Window(defaultBindingSet, 3, 3, defaultStrip, true);
                    window.stopIndex = 4;
                    window.set(0, [2,1,3]);
                    // Top border symbols
                    expect(window.get(-3).symbolID).to.equal(3, "case -3");
                    expect(window.get(-2).symbolID).to.equal(2, "case -2");
                    expect(window.get(-1).symbolID).to.equal(3, "case -1");

                    // View symbols
                    expect(window.get(0).symbolID).to.equal(2, "case 0");
                    expect(window.get(1).symbolID).to.equal(1, "case 1");
                    expect(window.get(2).symbolID).to.equal(3, "case 2");

                    // Bottom border symbols
                    expect(window.get(3).symbolID).to.equal(1, "case 3");
                    expect(window.get(4).symbolID).to.equal(2, "case 4");
                    expect(window.get(5).symbolID).to.equal(1, "case 5");

                    checkThatStripDidntChange();
                });
            });
        });
    });
}