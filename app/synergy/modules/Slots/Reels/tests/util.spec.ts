namespace RS.Reels.Tests
{
    const { expect } = chai;

    describe("Util.ts", function ()
    {
        describe("randomSelectSymbol", function ()
        {
            const mockDefinition: RS.Reels.Symbols.Definition =
            {
                name: "Mock Symbol A"
            };

            const mockDefinitionSpan2: RS.Reels.Symbols.Definition =
            {
                name: "Mock Symbol B",
                span: 2
            };

            it("should select a symbol from weighted selection chances", function ()
            {
                const mockBindingSet: RS.Reels.Symbols.BindingSet =
                [
                    { definition: mockDefinition, id: 0, selectionChance: 3 },
                    { definition: mockDefinition, id: 1, selectionChance: 5 },
                    { definition: mockDefinition, id: 2, selectionChance: 2 }
                ];
                const counts: { [id: number]: number } = {};
                counts[0] = counts[1] = counts[2] = 0;
                for (let i = 0; i < 10000; ++i)
                {
                    ++counts[RS.Reels.Symbols.randomSelectSymbol(mockBindingSet)];
                }
                expect(counts[0]).to.be.approximately(3000, 500, "counts[0]");
                expect(counts[1]).to.be.approximately(5000, 500, "counts[1]");
                expect(counts[2]).to.be.approximately(2000, 500, "counts[2]");
            });

            it("should not select a symbol with selection chance of 0", function ()
            {
                const mockBindingSet: RS.Reels.Symbols.BindingSet =
                [
                    { definition: mockDefinition, id: 0, selectionChance: 0 },
                    { definition: mockDefinition, id: 1, selectionChance: 10 },
                    { definition: mockDefinition, id: 2, selectionChance: 20 }
                ];
                for (let i = 0; i < 10000; ++i)
                {
                    expect(RS.Reels.Symbols.randomSelectSymbol(mockBindingSet)).to.not.equal(0);
                }
            });

            it("should not select a symbol which is excluded via maxSpan", function ()
            {
                const mockBindingSet: RS.Reels.Symbols.BindingSet =
                [
                    { definition: mockDefinition, id: 0, selectionChance: 1 },
                    { definition: mockDefinitionSpan2, id: 1, selectionChance: 1 },
                    { definition: mockDefinition, id: 2, selectionChance: 1 }
                ];
                for (let i = 0; i < 10000; ++i)
                {
                    expect(RS.Reels.Symbols.randomSelectSymbol(mockBindingSet, 1)).to.not.equal(1);
                }
            });

            it("should not select a symbol which is excluded via predicate", function ()
            {
                const mockBindingSet: RS.Reels.Symbols.BindingSet =
                [
                    { definition: mockDefinition, id: 0, selectionChance: 1 },
                    { definition: mockDefinition, id: 1, selectionChance: 1 },
                    { definition: mockDefinition, id: 2, selectionChance: 1 }
                ];
                for (let i = 0; i < 10000; ++i)
                {
                    expect(RS.Reels.Symbols.randomSelectSymbol(mockBindingSet, undefined, [ 1 ])).to.not.equal(1);
                }
            });
        });
    });
}