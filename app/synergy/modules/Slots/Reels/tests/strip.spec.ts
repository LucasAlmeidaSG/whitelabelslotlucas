namespace RS.Reels.Tests
{
    const { expect } = chai;

    describe("Strip.ts", function ()
    {
        describe("Strip", function ()
        {
            describe("constructor", function ()
            {
                it("should create a strip from an array", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    expect(strip.length).to.equal(3);
                    expect(strip.get(0)).to.equal(1);
                    expect(strip.get(1)).to.equal(5);
                    expect(strip.get(2)).to.equal(3);
                });

                it("should create a strip from a size and symbol ID", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(3, 5);
                    expect(strip.length).to.equal(3);
                    expect(strip.get(0)).to.equal(5);
                    expect(strip.get(1)).to.equal(5);
                    expect(strip.get(2)).to.equal(5);
                });

                it("should create a strip from another strip", function ()
                {
                    const baseStrip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    const strip = new RS.Reels.Symbols.Strip(baseStrip);
                    expect(strip.length).to.equal(3);
                    expect(strip.get(0)).to.equal(1);
                    expect(strip.get(1)).to.equal(5);
                    expect(strip.get(2)).to.equal(3);
                });

                it("should create a strip from a subset of another strip", function ()
                {
                    const baseStrip = new RS.Reels.Symbols.Strip([ 1, 5, 3, 8, 9 ]);
                    const stripA = new RS.Reels.Symbols.Strip(baseStrip, 2);
                    expect(stripA.length).to.equal(3);
                    expect(stripA.get(0)).to.equal(3);
                    expect(stripA.get(1)).to.equal(8);
                    expect(stripA.get(2)).to.equal(9);
                    const stripB = new RS.Reels.Symbols.Strip(baseStrip, 1, 3);
                    expect(stripB.length).to.equal(3);
                    expect(stripB.get(0)).to.equal(5);
                    expect(stripB.get(1)).to.equal(3);
                    expect(stripB.get(2)).to.equal(8);
                });

                it("should create a strip from a reelset", function ()
                {
                    const reelSet = new RS.Slots.ReelSet([
                        [ 1, 5, 3, 8, 9 ],
                        [ 6, 7, 2, 5, 1 ]
                    ]);

                    const stripA = new RS.Reels.Symbols.Strip(reelSet, 0);
                    expect(stripA.length).to.equal(5);
                    expect(stripA.get(0)).to.equal(1);
                    expect(stripA.get(1)).to.equal(5);
                    expect(stripA.get(2)).to.equal(3);
                    expect(stripA.get(3)).to.equal(8);
                    expect(stripA.get(4)).to.equal(9);

                    const stripB = new RS.Reels.Symbols.Strip(reelSet, 1, 2);
                    expect(stripB.length).to.equal(3);
                    expect(stripB.get(0)).to.equal(2);
                    expect(stripB.get(1)).to.equal(5);
                    expect(stripB.get(2)).to.equal(1);
                });
            });

            describe("setFrom", function ()
            {
                it("should populate a strip from an array", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([]);
                    strip.setFrom([ 1, 5, 3 ]);
                    expect(strip.length).to.equal(3);
                    expect(strip.get(0)).to.equal(1);
                    expect(strip.get(1)).to.equal(5);
                    expect(strip.get(2)).to.equal(3);
                });

                it("should populate a strip from a size and symbol ID", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([]);
                    strip.setFrom(3, 5);
                    expect(strip.length).to.equal(3);
                    expect(strip.get(0)).to.equal(5);
                    expect(strip.get(1)).to.equal(5);
                    expect(strip.get(2)).to.equal(5);
                });

                it("should populate a strip from another strip", function ()
                {
                    const baseStrip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    const strip = new RS.Reels.Symbols.Strip([]);
                    strip.setFrom(baseStrip);
                    expect(strip.length).to.equal(3);
                    expect(strip.get(0)).to.equal(1);
                    expect(strip.get(1)).to.equal(5);
                    expect(strip.get(2)).to.equal(3);
                });

                it("should populate a strip from a subset of another strip", function ()
                {
                    const baseStrip = new RS.Reels.Symbols.Strip([ 1, 5, 3, 8, 9 ]);
                    const stripA = new RS.Reels.Symbols.Strip([]);
                    stripA.setFrom(baseStrip, 2);
                    expect(stripA.length).to.equal(3);
                    expect(stripA.get(0)).to.equal(3);
                    expect(stripA.get(1)).to.equal(8);
                    expect(stripA.get(2)).to.equal(9);
                    const stripB = new RS.Reels.Symbols.Strip([]);
                    stripB.setFrom(baseStrip, 1, 3);
                    expect(stripB.length).to.equal(3);
                    expect(stripB.get(0)).to.equal(5);
                    expect(stripB.get(1)).to.equal(3);
                    expect(stripB.get(2)).to.equal(8);
                });

                it("should populate a strip from a reelset", function ()
                {
                    const reelSet = new RS.Slots.ReelSet([
                        [ 1, 5, 3, 8, 9 ],
                        [ 6, 7, 2, 5, 1 ]
                    ]);

                    const stripA = new RS.Reels.Symbols.Strip([]);
                    stripA.setFrom(reelSet, 0);
                    expect(stripA.length).to.equal(5);
                    expect(stripA.get(0)).to.equal(1);
                    expect(stripA.get(1)).to.equal(5);
                    expect(stripA.get(2)).to.equal(3);
                    expect(stripA.get(3)).to.equal(8);
                    expect(stripA.get(4)).to.equal(9);

                    const stripB = new RS.Reels.Symbols.Strip([]);
                    stripB.setFrom(reelSet, 1, 2);
                    expect(stripB.length).to.equal(3);
                    expect(stripB.get(0)).to.equal(2);
                    expect(stripB.get(1)).to.equal(5);
                    expect(stripB.get(2)).to.equal(1);
                });
            });

            describe("populateRandomly", function ()
            {
                it("should fill a strip with random values", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(12, 0);
                    const bindings: RS.Reels.Symbols.BindingSet =
                    [
                        { id: 1, definition: { name: "Mock 1" }, selectionChance: 1 },
                        { id: 2, definition: { name: "Mock 2" }, selectionChance: 1 },
                        { id: 3, definition: { name: "Mock 3" }, selectionChance: 1 }
                    ];
                    strip.populateRandomly(bindings);
                    for (let i = 0, l = strip.length; i < l; ++i)
                    {
                        expect(strip.get(i)).to.not.equal(0);
                    }
                });
            });

            describe("populateEvenly", function ()
            {
                it("should fill a strip with evenly distributed random values", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(12, 0);
                    const bindings: RS.Reels.Symbols.BindingSet =
                    [
                        { id: 1, definition: { name: "Mock 1" }, selectionChance: 1 },
                        { id: 2, definition: { name: "Mock 2" }, selectionChance: 1 },
                        { id: 3, definition: { name: "Mock 3" }, selectionChance: 1 }
                    ];
                    strip.populateEvenly(bindings);
                    const freqTable: { [key: number]: number } = {};
                    for (let i = 0, l = strip.length; i < l; ++i)
                    {
                        const id = strip.get(i);
                        expect(id).to.not.equal(0);
                        freqTable[id] = (freqTable[id] || 0) + 1;
                    }
                    expect(freqTable[1]).to.equal(4);
                    expect(freqTable[2]).to.equal(4);
                    expect(freqTable[3]).to.equal(4);
                });
            });

            describe("wrapRowIndex", function ()
            {
                it("should not affect an in-range value", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(12, 0);
                    expect(strip.wrapRowIndex(0)).to.equal(0);
                    expect(strip.wrapRowIndex(5)).to.equal(5);
                    expect(strip.wrapRowIndex(11)).to.equal(11);
                });

                it("should wrap an out-range value", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(12, 0);
                    expect(strip.wrapRowIndex(-1)).to.equal(11);
                    expect(strip.wrapRowIndex(12)).to.equal(0);
                    expect(strip.wrapRowIndex(-4)).to.equal(8);
                    expect(strip.wrapRowIndex(15)).to.equal(3);
                });
            });

            describe("clampRowIndex", function ()
            {
                it("should not affect an in-range value", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(12, 0);
                    expect(strip.clampRowIndex(0)).to.equal(0);
                    expect(strip.clampRowIndex(5)).to.equal(5);
                    expect(strip.clampRowIndex(11)).to.equal(11);
                });

                it("should clamp an out-range value", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip(12, 0);
                    expect(strip.clampRowIndex(-1)).to.equal(0);
                    expect(strip.clampRowIndex(12)).to.equal(11);
                    expect(strip.clampRowIndex(-4)).to.equal(0);
                    expect(strip.clampRowIndex(15)).to.equal(11);
                });
            });

            describe("get", function ()
            {
                it("should return a value based on the wrapped index", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 4, 7, 2, 1, 6 ]);
                    const testArr = [ 4, 7, 2, 1, 6, 4, 7, 2, 1, 6, 4, 7, 2, 1, 6 ]
                    for (let i = 0, l = testArr.length; i < l; ++i)
                    {
                        const idx = i - 5;
                        expect(strip.get(idx)).to.equal(testArr[i]);
                    }
                });
            });

            describe("set", function ()
            {
                it("should change a value on the strip", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    expect(strip.get(1)).to.equal(5);
                    strip.set(1, 7);
                    expect(strip.get(1)).to.equal(7);
                });

                it("should publish onSymbolChanged if the value changed", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    let calledCount = 0;
                    let calledWithData: RS.Reels.Symbols.Strip.SymbolChangeData | null = null;
                    strip.onSymbolChanged((data) =>
                    {
                        ++calledCount;
                        calledWithData = data;
                    });
                    strip.set(1, 7);
                    expect(calledCount).to.equal(1);
                    expect(calledWithData).to.deep.equal({ rowIndex: 1, oldSymbolID: 5, newSymbolID: 7 });
                });

                it("should not publish onSymbolChanged if the value didn't change", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    let calledCount = 0;
                    strip.onSymbolChanged((data) =>
                    {
                        ++calledCount;
                    });
                    strip.set(1, 5);
                    expect(calledCount).to.equal(0);
                });
            });

            describe("findExtent", function ()
            {
                it("should find following consecutive symbol IDs, without wrapping", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 5, 1, 5, 5, 5, 3, 3 ]);
                    const spans = [ 1, 1, 3, 2, 1, 2, 1 ];
                    for (let i = 0, l = spans.length; i < l; ++i)
                    {
                        expect(strip.findExtent(i, Reels.Symbols.SearchDirection.Forwards, false)).to.equal(spans[i]);
                    }
                });

                it("should find preceding consecutive symbol IDs, without wrapping", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 5, 1, 5, 5, 5, 3, 3 ]);
                    const spans = [ 1, 1, 1, 2, 3, 1, 2 ];
                    for (let i = 0, l = spans.length; i < l; ++i)
                    {
                        expect(strip.findExtent(i, Reels.Symbols.SearchDirection.Backwards, false)).to.equal(spans[i]);
                    }
                });

                it("should find following consecutive symbol IDs, with wrapping", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 5, 3, 3, 5, 5 ]);
                    const spans = [ 1, 2, 1, 3, 2 ];
                    for (let i = 0, l = spans.length; i < l; ++i)
                    {
                        expect(strip.findExtent(i, Reels.Symbols.SearchDirection.Forwards, true)).to.equal(spans[i]);
                    }
                });

                it("should find preceding consecutive symbol IDs, with wrapping", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 5, 3, 3, 5, 5 ]);
                    const spans = [ 3, 1, 2, 1, 2 ];
                    for (let i = 0, l = spans.length; i < l; ++i)
                    {
                        expect(strip.findExtent(i, Reels.Symbols.SearchDirection.Backwards, true)).to.equal(spans[i]);
                    }
                });
            });

            describe("equals", function ()
            {
                it("should return true for an equal strip", function ()
                {
                    const stripA = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    const stripB = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    expect(stripA.equals(stripB)).to.be.true;
                    expect(stripB.equals(stripA)).to.be.true;
                });

                it("should return true for an equal array", function ()
                {
                    const stripA = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    expect(stripA.equals([ 1, 5, 3 ])).to.be.true;
                });

                it("should return false for a non-equal strip", function ()
                {
                    const stripA = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    const stripB = new RS.Reels.Symbols.Strip([ 1, 5, 4 ]);
                    const stripC = new RS.Reels.Symbols.Strip([ 1, 5, 3, 1 ]);
                    expect(stripA.equals(stripB)).to.be.false;
                    expect(stripB.equals(stripA)).to.be.false;
                    expect(stripA.equals(stripC)).to.be.false;
                    expect(stripC.equals(stripA)).to.be.false;
                });

                it("should return false for a non-equal array", function ()
                {
                    const stripA = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    expect(stripA.equals([ 1, 5, 4 ])).to.be.false;
                    expect(stripA.equals([ 1, 5, 3, 1 ])).to.be.false;
                });
            });

            describe("clone", function ()
            {
                it("should return a copy of a strip", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    const cloned = strip.clone();
                    expect(strip).to.not.equal(cloned);
                    expect(strip.equals(cloned)).to.be.true;
                });
            });

            describe("dispose", function ()
            {
                it("should dispose a strip", function ()
                {
                    const strip = new RS.Reels.Symbols.Strip([ 1, 5, 3 ]);
                    expect(strip.isDisposed).to.be.false;
                    strip.dispose();
                    expect(strip.isDisposed).to.be.true;
                });
            });
        });
    });
}