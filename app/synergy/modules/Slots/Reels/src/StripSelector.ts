namespace RS.Reels
{
    /**
     * A tool for selecting a symbol from the whole reel strip.
     */
    @HasCallbacks
    export class StripSelector extends RS.Flow.BaseElement<StripSelector.Settings, StripSelector.RuntimeData>
    {
        public readonly onSymbolSelected = createEvent<number>();

        @AutoDispose protected readonly _grid: Flow.Grid;
        @AutoDispose protected readonly _backgrounds: Flow.Background[] = [];

        public constructor(settings: StripSelector.Settings, runtimeData: StripSelector.RuntimeData)
        {
            super(settings, runtimeData);

            const background = Flow.background.create(settings.backgroundSettings, this);

            this._grid = Flow.grid.create(this.settings.gridSettings, this);

            for (let i = 0; i < runtimeData.reelStrip.length; i++)
            {
                const isTopWindow = this.isTopWindowIndex(i);
                const isRestWindow = this.isOtherWindowIndex(i);
                const isWindow = isTopWindow || isRestWindow;

                const id = runtimeData.reelStrip[i];
                const definition = runtimeData.reel.getSymbolDefinition(id);
                const texture = Symbols.IDatabase.get().getRenderInfo(definition).texture;
                const cell: Flow.Container = Flow.container.create({ dock: Flow.Dock.Fill }, this._grid);

                let color: RS.Util.Color;
                if (isTopWindow)
                {
                    color = this.settings.currentIndexColor;
                }
                else if (isRestWindow)
                {
                    color = this.settings.windowColor;
                }
                else if (definition.isScatter)
                {
                    color = this.settings.scatterColor;
                }
                else
                {
                    color = this.settings.overColor;
                }

                const back: Flow.Background = Flow.background.create(
                {
                    kind: Flow.Background.Kind.SolidColor,
                    color: color,
                    borderSize: 0,
                    dock: Flow.Dock.Fill,
                }, cell);
                back.alpha = (isWindow || definition.isScatter) ? settings.selectAlpha : 0.0;
                this._backgrounds.push(back);
                const symbol = Flow.image.create(
                {
                    dock: Flow.Dock.Fill,
                    kind: RS.Flow.Image.Kind.Texture,
                    texture: texture,
                    name: "Symbol Sprite",
                    ignoreParentSpacing: false,
                    scaleMode: RS.Math.ScaleMode.Contain
                }, cell);
                const idLabel: Flow.Label = Flow.label.create({
                    ...settings.labelSettings,
                    text: id.toString()
                }, cell);

                cell.onDown(this.onCellDown.bind(this, i));
                if (!isWindow)
                {
                    cell.onOver(this.onCellOver.bind(this, i));
                    cell.onOut(this.onCellOut.bind(this, i));
                }
            }
        }

        protected isTopWindowIndex(index: number): boolean
        {
            return index === this.runtimeData.currentIndex;
        }

        protected isOtherWindowIndex(index: number): boolean
        {
            return (this.runtimeData.currentIndex + this.runtimeData.stopSymbolCount > this.runtimeData.reelStrip.length)
                ? (index > this.runtimeData.currentIndex || index < Math.mod(this.runtimeData.currentIndex + this.runtimeData.stopSymbolCount, this.runtimeData.reelStrip.length))
                : (index > this.runtimeData.currentIndex && index < this.runtimeData.currentIndex + this.runtimeData.stopSymbolCount);
        }

        protected isScatter(index: number): boolean
        {
            const id = this.runtimeData.reelStrip[index];
            const definition = this.runtimeData.reel.getSymbolDefinition(id);
            return definition.isScatter;
        }

        protected onCellDown(index: number): void
        {
            this.onSymbolSelected.publish(index);
        }

        protected onCellOver(index: number): void
        {
            this._backgrounds[index].alpha = this.settings.selectAlpha;
        }

        protected onCellOut(index: number): void
        {
            if (!this.isScatter(index))
            {
                this._backgrounds[index].alpha = 0.0;
            }
        }
    }

    export namespace StripSelector
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            backgroundSettings: Flow.Background.Settings;
            gridSettings: Flow.Grid.Settings;
            selectAlpha: number;
            labelSettings: Flow.Label.Settings;
            currentIndexColor?: Util.Color;
            windowColor?: Util.Color;
            overColor?: Util.Color;
            scatterColor?: Util.Color;
        }

        export interface RuntimeData
        {
            reel: IGenericReel;
            reelStrip: number[];
            currentIndex: number;
            stopSymbolCount: number;
        }

        export let defaultSettings: Settings =
        {
            dock: Flow.Dock.Fill,
            backgroundSettings:
            {
                ...Flow.Background.defaultSettings,
                dock: Flow.Dock.Fill,
                alpha: 0.8
            },
            gridSettings:
            {
                dock: Flow.Dock.Fill,
                order: Flow.Grid.Order.ColumnFirst
            },
            selectAlpha: 0.7,
            labelSettings:
            {
                text: "",
                font: RS.Fonts.FrutigerWorld,
                fontSize: 20,
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.75, y: 0.35 },
                layers:
                [
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black,
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        offset: { x: 0.00, y: 0.00 },
                        size: 2
                    },
                    {
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: new RS.Util.Color(0xfefdff),
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        offset: { x: 0.00, y: 0.00 }
                    }
                ]
            },
            currentIndexColor: Util.Colors.red,
            windowColor: Util.Colors.orange,
            overColor: Util.Colors.darkturquoise,
            scatterColor: Util.Colors.white
        }
    }
}