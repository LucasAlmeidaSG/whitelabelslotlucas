/// <reference path="IReel.ts" />

namespace RS.Reels
{
    /**
     * A reels component.
     * Manages a set of individual reels.
     */
    export interface IComponent<TReelSettings extends IReel.Settings, TSettings extends IComponent.Settings<TReelSettings>> extends Flow.BaseElement<TSettings, IComponent.RuntimeData>, IDisposable
    {
        /** Gets the settings for the reels. */
        readonly settings: TSettings;

        /** Gets or sets the current state of the reels. */
        readonly state: Observable<IReel.State>;

        /** Gets or sets if the reels are enabled. */
        enabled: boolean;

        /** Gets or sets if the reels can be skipped using the skip arbiter. */
        skipEnabled: boolean;

        /** Gets if the reels can do a standard stop. */
        readonly canStopNormally: boolean;

        /** Gets if the reels can do a quick stop. */
        readonly canStopQuickly: boolean;

        /** Gets or sets the tint of the reels. */
        tint: Util.Color;

        /** Gets the individual reels. */
        readonly reels: ReadonlyArray<IReel<TReelSettings>>;

        /** Gets the current stop indices. */
        readonly stopIndices: ReadonlyArray<number> | null;

        /** Gets the reels mask. */
        readonly maskShape: RS.Rendering.IGraphics;

        /** Gets or sets if the rig tool is visible. */
        rigToolVisible: boolean;

        /** Published when all the reels have stopped. */
        readonly onStopped: IEvent;

        /** Published when a single reel has stopped. */
        readonly onReelStopped: IEvent<number>;

        /** Published when a symbol has landed. */
        readonly onSymbolLanded: IEvent<Slots.ReelSetPosition>;

        /** Published when a scatter has landed on a reel. */
        readonly onScatterLanded: IEvent<IReel.ScatterLandedData & { reelIndex: number; }>;

        /** Published when suspense has started on a reel. */
        readonly onSuspenseStarted: IEvent<number>;

        /** Published when suspense has ended on a reel. */
        readonly onSuspenseEnded: IEvent<number>;

        /** Published when this reel has been clicked to skip. */
        readonly onSkipRequested: IEvent;

        /** Gets the symbol info for the specified ID, based on the specified reel's symbol bindings. */
        getSymbolDefinition(id: number, reelIndex: number): Symbols.Definition;

        /** Gets the symbol ID for the specified definition, based on the specified reel's symbol bindings. */
        getSymbolID(definition: Symbols.Definition, reelIndex: number): number | null;

        /** Starts spinning the reels. */
        startReels(type: IReel.StartType): void;

        /**
         * Stops spinning the reels.
         * @param type Stop type, which determines behaviours for skip, suspense etc.
         * @param noSuspense Whether or not to skip suspense. Defaults to false for Standard and Immediate stops, and true for Quick stops.
         */
        stopReels(type: IReel.StopType, noSuspense?: boolean): void;

        /** Starts spinning an individual reel. */
        startReel(index: number, type: IReel.StartType): void;

        /** Stops spinning an individual reel. */
        stopReel(index: number, type: IReel.StopType): void;

        /** Sets a symbol in the visible window. */
        setSymbol(reelIndex: number, rowIndex: number, symbolID: number): void;

        /** Gets a symbol from the visible window. */
        getSymbol(reelIndex: number, rowIndex: number): number;

        /**
         * Sets the content of the reels with the given reel set.
         * Only affects the visible window.
         */
        setReels(reelSet: Slots.ReelSet): void;

        /**
         * Sets the content of the reels with the given reel stops.
         * Reel set must be set (via setReelSet) for this to work.
         * Only affects the visible window.
         */
        setReels(stopPositions: number[]): void;

        /**
         * Sets the content of the reels with the given reel set.
         * Only affects the visible window.
         */
        setReels(reelSet: Slots.ReelSet, stopPositions: number[]): void;

        /**
         * Sets the reel set that the reels references in to.
         * If set, this is used to populate the reels that spin by, and when shifting the reels via rig tool.
         * If not set, the reels that spin by are generated randomly using selectionChance and shifting via rig tool will be non-functional.
         */
        setReelSet(reelSet: Slots.ReelSet): void;

        /** Hides the symbol at the given location. */
        hideSymbol(reelIndex: number, rowIndex: number): void;

        /** Hides the symbol at the given location. */
        showSymbol(reelIndex: number, rowIndex: number): void;

        /** Sets the opacity of the symbol at the given location. */
        setSymbolOpacity(reelIndex: number, rowIndex: number, opacity: number): void;

        /** Sets the color of the symbol at the given location. */
        setSymbolColor(reelIndex: number, rowIndex: number, color: Util.Color): void;

        /** Hides all symbols on the reels. */
        hideAllSymbols(): void;

        /** Shows all symbols on the reels. */
        showAllSymbols(): void;

        /**
         * Given the index of the specified symbol (in stop symbol coordinates), finds the row index that STARTS the symbol.
         * If the symbol's span is 1, this function will just return the passed in row.
         * If the symbol's span is > 1, this function will find the top-most point of the symbol.
         * Searches the entire symbol strip including the window overlay.
         */
        findSymbolStart(reelIndex: number, rowIndex: number): number;

        /**
         * Given the index of the specified symbol (in stop symbol coordinates), finds the row index in the CENTER of the symbol.
         * If the symbol's span is 1, this function will just return the passed in row.
         * If the symbol's span is > 1, this function will find the middle-most point of the symbol.
         * Searches the entire symbol strip including the window overlay.
         */
        findSymbolCenter(reelIndex: number, rowIndex: number): number;

        /**
         * Returns a new sprite/spine positioned at the correct location (relative to the reels stage) above an existing symbol.
         * If no symbol ID is provided, the current reels are queried for the symbol at the given reel and row.
         */
        getSpriteForPosition(reelIndex: number, rowIndex: number, symbolID?: number): RS.Rendering.IBitmap | RS.Rendering.IAnimatedSprite | RS.Rendering.ISpine;

        /** Gets the centered position of a symbol on the reels. */
        getSymbolPosition(reelIndex: number, rowIndex: number): Math.Vector2D;

        /**
         * Nudges the position of the given reel up or down by a single symbol.
         * Async.
        **/
        nudgeReel(reelIndex: number, direction: "up" | "down", duration: number, ease?: (num: number) => number): PromiseLike<void>;

        /** Hides the specified reels. */
        hideReels(...reels: number[]): void;

        /** Unhides all reels. */
        showAllReels(): void;

        /** Holds the specified positions. They won't spin with the rest of the reels. */
        holdPositions(positions: ReadonlyArray<Slots.ReelSetPosition>): void;

        /** Holds all positions. */
        holdAllPositions(): void;

        /** Unholds the specified positions. */
        unholdPositions(positions: ReadonlyArray<Slots.ReelSetPosition>): void;

        /** Unholds all positions. */
        unholdAllPositions(): void;

        /** Locks the specified reels so that they do not spin. */
        lockReels(...reels: number[]): void;

        /** Unlocks the specified reels so that they can spin again. */
        unlockReels(...reels: number[]): void;

        /** Unlocks all reels so that they can spin again. */
        unlockAllReels(): void;

        /**
         * Slaves the specified reel to another.
         * The slaved reel will spin in sync with the master reel.
         */
        slaveReel(slaveReelIndex: number, masterReelIndex: number);

        /** Unslaves the specified reel. */
        unslaveReel(reelIndex: number);

        /** Unslaves all reels. */
        unslaveAllReels();

        /** Adds the specified symbol land animator to the reels. */
        addSymbolLandAnimator(symbolDef: Symbols.Definition, animator: ISymbolLandAnimator): void;

        /** Removes the specified symbol land animator from the reels. */
        removeSymbolLandAnimator(symbolDef: Symbols.Definition, animator: ISymbolLandAnimator): void;

        /** Removes all symbol land animator for the specified symbol from the reels. */
        removeSymbolLandAnimator(symbolDef: Symbols.Definition): void;

        /** Removes the specified symbol land animator instance from the reels. */
        removeSymbolLandAnimator(animator: ISymbolLandAnimator): void;

        /** Removes the specified symbol land animator instance from the reels. */
        removeAllSymbolLandAnimators(): void;
    }

    export type IGenericComponent = IComponent<IReel.Settings, IComponent.Settings<IReel.Settings>>;

    export namespace IComponent
    {
        export type SymbolSortFunction = (a: Rendering.StripSymbol, b: Rendering.StripSymbol) => number;

        /**
         * Basic settings for a reels component.
         */
        export interface Settings<TReelSettings extends IReel.Settings = IReel.Settings> extends Partial<Flow.ElementProperties>
        {
            /** Class to use for the individual reels. */
            reelClass: ReelConstructor<TReelSettings>;

            /** Settings for each reel. */
            reels: TReelSettings[];

            /** Minimum amount of time to elapse before the reels can be stopped (ms). */
            minimumStopTime: number;

            /** Time between reels being started individually (ms). */
            reelStartDelta: number;

            /** Time between reels being stopped individually (ms) from when stop is requested. */
            reelStopDelta: number;

            /** Time between reels being stopped individually (ms) from when stop is requested when turbo mode is active. */
            turboStopDelta?: number;

            /** Minimum number of scatters needed to win a scatter bonus. */
            scatterWinCount?: number | { [scatterGroup: number]: number };

            /** How to sort symbols. */
            sortFunc?: SymbolSortFunction;

            /** RigTool settings. */
            rigToolSettings?: RigTool.Settings;

            stripSelectorSettings?: StripSelector.Settings;
        }

        /**
         * Configuration for the reels' initial stop symbols.
         */
        export interface InitialConfiguration
        {
            /** Reel-set to start with. */
            reelSet: Slots.ReelSet;

            /** Visible stop symbols. */
            symbolsInView: Slots.ReelSet;

            /** Initial stop indices. */
            stopIndices: number[];
        }

        /**
         * Runtime data for a reels component.
         */
        export interface RuntimeData
        {
            /** An arbiter that drives whether or not the reels are skippable via clicking. */
            skipArbiter?: IReadonlyObservable<boolean>;

            /** Configuration for the reels' initial stop symbols. */
            initialConfiguration?: InitialConfiguration;

            /** The root scene object to attach 3D symbols to. */
            sceneRoot?: RS.Rendering.Scene.ISceneObject;
        }
    }

    export namespace Sort
    {
        export const ByGlobalZIndex: IComponent.SymbolSortFunction = (a, b) => a.globalZIndex - b.globalZIndex;
        export const ByYCoordAsc: IComponent.SymbolSortFunction = (a, b) => a.y - b.y;
        export const ByYCoordDes: IComponent.SymbolSortFunction = (a, b) => b.y - a.y;

        /**
         * Combines multiple sorts into one.
         * The first sort will take priority - if it sorts as equal, it will fall back to the second etc.
         * @param sorts
         */
        export function combine(...sorts: IComponent.SymbolSortFunction[]): IComponent.SymbolSortFunction
        {
            return function(a, b)
            {
                for (const sort of sorts)
                {
                    const result = sort(a, b);
                    if (result !== 0) { return result; }
                }
                return 0;
            };
        }
    }
}
