namespace RS.Reels.LandAnimators
{
    class SpriteAnimation implements ISymbolLandAnimation
    {
        private _isDisposed: boolean = false;
        private _container = new RS.Rendering.Container();
        @AutoDispose private _sprite: RS.Rendering.IAnimatedSprite;

        private _tween: RS.Tween<RS.Rendering.IAnimatedSprite>;

        private _onDisposed = createSimpleEvent();

        public get isDisposed() { return this._isDisposed; }

        public constructor(
            public readonly settings: Sprite.Settings,
            public readonly reels: IGenericComponent,
            public readonly reelIndex: number,
            public readonly rowIndex: number
        )
        {
            reels.addChild(this._container);
            if (this.settings.useReelsMask)
            {
                this._container.addChild(reels.maskShape);
                this._container.mask = reels.maskShape;
            }
        }

        public async animate()
        {
            const symbolID = this.reels.getSymbol(this.reelIndex, this.rowIndex);
            const symbolDef = this.reels.getSymbolDefinition(symbolID, this.reelIndex);
            if (!symbolDef.animation && !this.settings.animName) { return; }

            const renderInfo = Reels.Symbols.IDatabase.get().getRenderInfo(symbolDef);

            if (symbolDef.spriteAsset.type === "image") { return; }

            const symbolSprite = this._sprite = this.reels.getSpriteForPosition(this.reelIndex, this.rowIndex, symbolID) as RS.Rendering.IAnimatedSprite;
            const pos = this.reels.getSymbolPosition(this.reelIndex, this.rowIndex);
            symbolSprite.x = pos.x;
            symbolSprite.y = pos.y;
            // symbolSprite.pivot.set(renderInfo.pivot.x, renderInfo.pivot.y);
            this._container.addChild(symbolSprite);

            this.reels.hideSymbol(this.reelIndex, this.rowIndex);

            const animName = symbolDef.spineAnimation != null ?
                this.settings.animName || symbolDef.spineAnimation.animationName :
                this.settings.animName || symbolDef.animation.name;
            symbolSprite.gotoAndPlay(animName);
            await Promise.race([ symbolSprite.onAnimationEnded, this._onDisposed ]);

            this.reels.showSymbol(this.reelIndex, this.rowIndex);

            if (this._isDisposed) { return; }

            this._container.removeChild(symbolSprite);

        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }

            if (this._tween)
            {
                this._tween.finish();
                this._tween = null;
            }

            this.reels.removeChild(this._container);
            this._container = null;

            this._onDisposed.publish();

            this._isDisposed = true;
        }
    }

    export class Sprite implements ISymbolLandAnimator
    {
        protected _animateChecker: AnimateCheckHandler = defaultScatterAnimateChecker;
        public set animateChecker(callback: AnimateCheckHandler) { this._animateChecker = callback; }
        public get animateChecker(): AnimateCheckHandler { return this._animateChecker; }

        protected _landedCount: number = -1;

        public constructor(
            public readonly settings: Sprite.Settings
        )
        {
            if (settings.animateChecker)
            {
                this._animateChecker = settings.animateChecker;
            }
        }

        public animateSymbol(reels: IComponent<IReel.Settings, IComponent.Settings<IReel.Settings>>, reelIndex: number, rowIndex: number): ISymbolLandAnimation
        {
            this._landedCount++;
            if (this.settings.checkBeforeAnimate && !this._animateChecker(reels, reelIndex, rowIndex, this._landedCount))
            {
                return null;
            }
            return new SpriteAnimation(this.settings, reels, reelIndex, rowIndex);
        }

        public reset(): void
        {
            this._landedCount = -1;
        }
    }

    export namespace Sprite
    {
        export interface Settings extends SymbolLandAnimator.Settings
        {
            animName?: string;
        }
    }
}
