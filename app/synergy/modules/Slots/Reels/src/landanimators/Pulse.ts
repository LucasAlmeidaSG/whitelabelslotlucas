namespace RS.Reels.LandAnimators
{
    class PulseAnimation implements ISymbolLandAnimation
    {
        private _isDisposed: boolean = false;
        private _container = new RS.Rendering.Container();

        private _tween: RS.Tween<RS.Rendering.IBitmap>;

        public get isDisposed() { return this._isDisposed; }

        public constructor(
            public readonly settings: Pulse.Settings,
            public readonly reels: IGenericComponent,
            public readonly reelIndex: number,
            public readonly rowIndex: number
        )
        {
            reels.addChild(this._container);
            if (this.settings.useReelsMask)
            {
                this._container.addChild(reels.maskShape);
                this._container.mask = reels.maskShape;
            }
        }

        public async animate()
        {
            const symbolID = this.reels.getSymbol(this.reelIndex, this.rowIndex);
            const symbolDef = this.reels.getSymbolDefinition(symbolID, this.reelIndex);

            const renderInfo = Reels.Symbols.IDatabase.get().getRenderInfo(symbolDef);

            const symbolSprite = new RS.Rendering.Bitmap(renderInfo.texture);
            const pos = this.reels.getSymbolPosition(this.reelIndex, this.rowIndex);
            symbolSprite.x = pos.x + (symbolDef.offset ? symbolDef.offset.x : 0);
            symbolSprite.y = pos.y + (symbolDef.offset ? symbolDef.offset.y : 0);
            symbolSprite.pivot.set(renderInfo.pivot.x, renderInfo.pivot.y);
            this._container.addChild(symbolSprite);

            this._tween = RS.Tween.get(symbolSprite)
                .to({scaleX: this.settings.scaleFactor, scaleY: this.settings.scaleFactor}, this.settings.pulseDuration / 2, this.settings.ease || RS.Ease.quadInOut)
                .to({scaleX: 1.0, scaleY: 1.0}, this.settings.pulseDuration / 2, this.settings.ease || RS.Ease.quadInOut);

            this.reels.hideSymbol(this.reelIndex, this.rowIndex);

            await this._tween;
            this._tween = null;

            this.reels.showSymbol(this.reelIndex, this.rowIndex);

            if (this._isDisposed) { return; }

            this._container.removeChild(symbolSprite);
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }

            if (this._tween)
            {
                this._tween.finish();
                this._tween = null;
            }

            this.reels.removeChild(this._container);
            this._container = null;

            this._isDisposed = true;
        }
    }

    export class Pulse implements ISymbolLandAnimator
    {
        protected _animateChecker: AnimateCheckHandler = defaultScatterAnimateChecker;
        public set animateChecker(callback: AnimateCheckHandler) { this._animateChecker = callback; }
        public get animateChecker(): AnimateCheckHandler { return this._animateChecker; }

        protected _landedCount: number = -1;

        public constructor(
            public readonly settings: Pulse.Settings
        )
        {
            if (settings.animateChecker)
            {
                this._animateChecker = settings.animateChecker;
            }
        }

        public animateSymbol(reels: IComponent<IReel.Settings, IComponent.Settings<IReel.Settings>>, reelIndex: number, rowIndex: number): ISymbolLandAnimation
        {
            this._landedCount++;
            if (this.settings.checkBeforeAnimate && !this._animateChecker(reels, reelIndex, rowIndex, this._landedCount))
            {
                return null;
            }
            return new PulseAnimation(this.settings, reels, reelIndex, rowIndex);
        }

        public reset(): void
        {
            this._landedCount = -1;
        }
    }

    export namespace Pulse
    {
        export interface Settings extends SymbolLandAnimator.Settings
        {
            scaleFactor: number;
            pulseDuration: number;
            ease?: RS.EaseFunction;
        }
    }
}
