namespace RS.Reels
{
    /** Function determining whether or not a landing symbol should be animated. */
    export type AnimateCheckHandler = (reels: IGenericComponent, reelIndex: number, rowIndex?: number, count?: number) => boolean;

    /** Checker comparing the remaining reels and symbols with the scatter win count, like for suspense. */
    export const defaultScatterAnimateChecker: AnimateCheckHandler = (reels: IGenericComponent, reelIndex: number, rowIndex?: number, count?: number) =>
    {
        return (reels.reels.length - reelIndex + count >= reels.settings.scatterWinCount);
    };

    /**
     * Encapsulates a behaviour that animates symbols as they land.
     */
    export interface ISymbolLandAnimator
    {
        /** Land scatter animation checker function. */
        animateChecker?: AnimateCheckHandler;

        /** Creates an ISymbolLandAnimation for the specified symbol. */
        animateSymbol(reels: IGenericComponent, reelIndex: number, rowIndex: number): ISymbolLandAnimation;

        /** Reset attributes. */
        reset(): void;
    }

    /**
     * Encapsulates the animation of a single symbol animation.
     */
    export interface ISymbolLandAnimation extends IDisposable
    {
        /** Performs animation. */
        animate(): PromiseLike<void>;
    }

    export namespace SymbolLandAnimator
    {
        export interface Settings
        {
            checkBeforeAnimate?: boolean;
            animateChecker?: AnimateCheckHandler;
            useReelsMask?: boolean;
        }
    }
}