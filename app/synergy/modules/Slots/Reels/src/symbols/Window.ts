namespace RS.Reels.Symbols
{
    const compareFunc: CompareFunction<Window.SymbolData> = (obj1, obj2) =>
    {
        for (const elem in obj1)
        {
            if (obj2[elem] == null || obj1[elem] != obj2[elem])
            {
                return false;
            }
        }
        return true;
    };

    /**
     * Encapsulates a set of symbols that overlay a certain position on a symbol strip.
     * Typically this is used to hold the visible symbols of a reel from the engine.
     * The window has a fixed sized with a border.
     * The border is usually transparent but can be used to resolve symbol span integrity errors.
     *
     * For example, given size=3 and borderSize=2, the window would look like the following:
     *
     * rowIndex | arrayIndex | stripIndex | B/W
     *
     *       -2 |          0 |    pos - 2 |   B
     *       -1 |          1 |    pos - 1 |   B
     *        0 |          2 |        pos |   W (top)
     *        1 |          3 |    pos + 1 |   W (middle)
     *        2 |          4 |    pos + 2 |   W (bottom)
     *        3 |          5 |    pos + 3 |   B
     *        4 |          6 |    pos + 4 |   B
     *
     * Note that the terminology used here will be consistent with variable names throughout this class.
     *  - rowIndex: Offset relative to the top of the window excluding border
     *  - arrayIndex: Offset relative to the top of the window including border
     *  - stripIndex: Offset in the underlying strip
     *  - position: The stripIndex of rowIndex 0
     *
     * The appropiate conversion functions should be used to move through the different indices wherever possible.
     */
    export class Window implements IDisposable
    {
        protected _size: number;
        protected _borderSize: number;
        protected _strip: Strip;
        protected _symbols: Window.SymbolData[];
        protected _position: number = 0;
        protected _bindingSet: BindingSet;
        protected _isDisposed: boolean = false;
        protected _symbolsByID: BindingSet;
        protected _randomSymbolFunction: Window.RandomSymbolFunction;
        protected _matchBorderSymbols: boolean;
        protected _stopIndex: number;

        /** The lowest span of any selectable symbol in the current binding set. */
        protected _lowestSelectableSpan: number;

        /** Gets the size of this window. */
        public get size() { return this._size; }

        /** Gets the border size of this window. */
        public get borderSize() { return this._borderSize; }

        /** Gets or sets the position of the top of this window, as on offset to the underlying strip. */
        public get position() { return this._position; }
        public set position(value) { this._position = value; }

        /** Gets or sets the underlying strip of this window. */
        public get strip() { return this._strip; }
        public set strip(value) { this._strip = value; }

        /** Gets if this window has been disposed or not. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets or sets the symbols binding set. */
        public get bindingSet(): BindingSet { return this._bindingSet; }
        public set bindingSet(bindingSet: BindingSet)
        {
            this._symbolsByID = constructSymbolsByID(bindingSet);
            this._lowestSelectableSpan = findLowestSelectableSpan(bindingSet);
            this._bindingSet = bindingSet;
        }

        /** Gets or sets the function used to generate random symbols when covering up something on the strip. */
        public get randomSymbolFunction() { return this._randomSymbolFunction; }
        public set randomSymbolFunction(value) { this._randomSymbolFunction = value; }

        /** Gets if this window is completely transparent. */
        public get isClear(): boolean
        {
            return !this._symbols.some((s) => s.symbolID != null);
        }

        public get stopIndex() { return this._stopIndex; }
        public set stopIndex(stopIndex: number)
        {
            this._stopIndex = stopIndex;
        }

        public constructor(bindingSet: BindingSet, size: number, borderSize: number, underlyingStrip?: Strip, matchBorderSymbols?: boolean)
        {
            this._size = size;
            this._borderSize = borderSize;
            this._strip = underlyingStrip;
            this._symbols = new Array<Window.SymbolData>(size + borderSize * 2);
            this._matchBorderSymbols = matchBorderSymbols;
            for (let i = 0, l = this._symbols.length; i < l; ++i)
            {
                this._symbols[i] = { symbolID: null, opacity: 1.0, tint: Util.Colors.white };
            }
            this._bindingSet = bindingSet;
            this._symbolsByID = constructSymbolsByID(bindingSet);
            this._lowestSelectableSpan = findLowestSelectableSpan(bindingSet);
            this._randomSymbolFunction = Window.defaultRandomSymbolFunction;
        }

        /**
         * Transforms the specified row index into a strip index.
         * @param rowIndex row index relative to the top of the window
         */
        public toStripIndex(rowIndex: number): number
        {
            return rowIndex + this._position;
        }

        /**
         * Transforms the specified strip index into a row index.
         * @param stripIndex
         */
        public fromStripIndex(stripIndex: number): number
        {
            return stripIndex - this._position;
        }

        /**
         * Transforms the specified row index into an array index.
         * @param rowIndex row index relative to the top of the window
         */
        public toArrayIndex(rowIndex: number): number
        {
            return Math.wrap(rowIndex + this._borderSize, 0, this._strip.length);
        }

        /**
         * Transforms the specified array index into an row index.
         * @param arrayIndex
         */
        public fromArrayIndex(arrayIndex: number): number
        {
            return arrayIndex - this._borderSize;
        }

        /**
         * Gets a symbol from this window using the given row index.
         * If the symbol is "transparent" or the row is outside of the window, will sample the underlying strip.
         * @param rowIndex Row index relative to the top of the window.
         */
        public get(rowIndex: number, out?: Window.SymbolData): Window.SymbolData
        {
            out = out || { symbolID: null, opacity: 1.0, tint: Util.Colors.white };
            const windowSymbolData = this._symbols[this.toArrayIndex(rowIndex)];
            if (windowSymbolData == null || windowSymbolData.symbolID == null)
            {
                out.symbolID = this._strip.get(this.toStripIndex(rowIndex));
                out.opacity = 1.0;
                out.tint = Util.Colors.white;
            }
            else
            {
                out.symbolID = windowSymbolData.symbolID;
                out.opacity = windowSymbolData.opacity;
                out.tint = windowSymbolData.tint;
            }
            return out;
        }

        /**
         * Gets if the symbol at the given row index within this window is "opaque", that is, set explicitly (thus a "get" will not sample the strip).
         * @param rowIndex Row index relative to the top of the window.
         */
        public isOpaque(rowIndex: number): boolean
        {
            const windowSymbolData = this._symbols[this.toArrayIndex(rowIndex)];
            return windowSymbolData != null && windowSymbolData.symbolID != null;
        }

        /**
         * Sets a symbol from this window using the given row index.
         * Will NOT write to the underlying strip.
         * @param rowIndex Row index relative to the top of the window.
         * @param symbolID
         */
        public set(rowIndex: number, symbolID: number | null, opacity?: number, tint?: Util.Color): void;

        /**
         * Sets a symbol from this window using the given row index.
         * Will NOT write to the underlying strip.
         * @param rowIndex Row index relative to the top of the window.
         * @param symbolID
         */
        public set(rowIndex: number, symbolIDs: (number | null)[], opacity?: number, tint?: Util.Color): void;

        public set(rowIndex: number, symbolID: number | (number | null)[] | null, opacity: number = 1.0, tint: Util.Color = Util.Colors.white): void
        {
            if (Is.array(symbolID))
            {
                for (let i = 0, l = symbolID.length; i < l; ++i)
                {
                    const idx = this.toArrayIndex(rowIndex + i);
                    if (idx >= 0 && idx < this._symbols.length)
                    {
                        this._symbols[idx].symbolID = symbolID[i];
                        this._symbols[idx].opacity = opacity;
                        this._symbols[idx].tint = tint;
                    }
                }
            }
            else
            {
                const idx = this.toArrayIndex(rowIndex);
                if (idx >= 0 && idx < this._symbols.length)
                {
                    this._symbols[idx].symbolID = symbolID;
                    this._symbols[idx].opacity = opacity;
                    this._symbols[idx].tint = tint;
                }
            }

            // Border symbols set to match reel strip
            if (this._matchBorderSymbols && this._stopIndex >= 0)
            {
                this.updateBorderSymbols();
            }

            this.maintainSymbolSpanIntegrity();
        }

        /**
         * Sets all symbols in the window from the underlying strip.
         */
        public capture(): void
        {
            for (let i = -this._borderSize, l = this._size + this._borderSize; i < l; ++i)
            {
                this.set(i, this.get(i).symbolID);
            }
        }

        /**
         * Finds the extent of the block of equal values in this window at the specified row index.
         * Can search forwards or backwards.
         * @param rowIndex The row index to start searching at
         * @param direction The direction to search in (1 = forwards, -1 = backwards)
         */
        public findExtent(rowIndex: number = 0, direction: SearchDirection = SearchDirection.Forwards): number
        {
            if (direction === 0) { throw new Error("Infinite loop avoided"); }
            const val = this.get(rowIndex).symbolID;
            let accum = 0;
            while (val === this.get(rowIndex).symbolID)
            {
                rowIndex += direction;
                accum++;
                if (accum >= this._strip.length) { break; }
            }
            return accum;
        }

        /**
         * Makes the window fully "transparent".
         */
        public clear(): void
        {
            for (let i = 0, l = this._symbols.length; i < l; ++i)
            {
                this._symbols[i].symbolID = null;
                this._symbols[i].opacity = 1.0;
                this._symbols[i].tint = Util.Colors.white;
            }
        }

        /** Disposes this window. */
        public dispose()
        {
            if (this._isDisposed) { return; }
            this._symbols.length = 0;
            this._symbols = null;
            this._isDisposed = true;
        }

        protected fillWithRandomSymbols(startRowIndex: number, count: number): void
        {
            for (let i = 0; i < count; ++i)
            {
                const rowIndex = this._borderSize + startRowIndex + i;
                if (this._symbols[rowIndex].symbolID != null) { continue; }

                // Find max span.
                let maxSpan = 1;
                for (let blockEnd = i + 1; blockEnd < count; ++blockEnd)
                {
                    const extentRowIndex = this._borderSize + startRowIndex + blockEnd;
                    if (this._symbols[extentRowIndex].symbolID != null) { break; }
                    ++maxSpan;
                }

                if (maxSpan < this._lowestSelectableSpan)
                {
                    Log.warn(`No selectable symbol exists with a span less than or equal to ${maxSpan}`);
                    continue;
                }

                // Delegate symbolID generation.
                const stripIndex = this.toStripIndex(startRowIndex + i);
                const symbolID = this._randomSymbolFunction(this._bindingSet, this._strip, stripIndex, maxSpan);

                const binding = this._symbolsByID[symbolID];
                if (!binding) { throw new Error(`Random symbol function returned invalid or unknown symbol ${symbolID}`); }

                const def = binding.definition;
                const span = def.span || 1;
                if (span > maxSpan) { throw new Error(`Random symbol function returned overspan symbol ${symbolID}`); }

                for (let fillRow = rowIndex, fillEnd = fillRow + span; fillRow < fillEnd; ++fillRow)
                {
                    this._symbols[fillRow].symbolID = symbolID;
                }

                i += span - 1;
            }
        }

        protected fillWithSymbol(startRowIndex: number, count: number, symbolID: number): void
        {
            for (let i = 0; i < count; ++i)
            {
                const idx = this.toArrayIndex(startRowIndex + i);
                this._symbols[idx].symbolID = symbolID;
            }
        }

        /**
         * Covers up the symbol at the specified strip index by filling in the border with random symbols.
         * Intended for use on spanning symbols.
         * @param stripIndex
         * @param minSpan only do anything if the span of the symbol at stripIndex >= minSpan
         * @returns true if work was done, false if no changes made
         */
        protected coverUpSymbol(stripIndex: number, minSpan: number = 2): boolean
        {
            const symbolID = this._strip.get(stripIndex);
            const symbolDef = this._symbolsByID[symbolID].definition;
            const span = symbolDef.span || 1;
            if (span < minSpan) { return false; }

            // Find how high the symbol goes
            const upwardsExtent = (this._strip.findExtent(stripIndex, SearchDirection.Backwards, true) - 1) % span;

            // Find where stripIndex is inside this window
            const baseRowIndex = this.fromStripIndex(stripIndex);

            // If upwardsExtent is how much of the spanning symbol falls ABOVE this position
            // If it's 0, this position is the TOP of the spanning symbol
            this.fillWithRandomSymbols(baseRowIndex - upwardsExtent, span);

            return true;
        }

        /**
         * Updates the borders as needed to ensure there are no "broken" multi-spanning symbols.
         * Broken multi-spanning symbols can happen if the edge of the window intersects a multi-spanning symbol on the underlying strip.
         */
        protected maintainSymbolSpanIntegrity(): void
        {
            // Cache some values for easy access
            const border = this._borderSize;
            const visible = this._size;

            let opaqueStartTop = 0; // as rowIndex
            let opaqueEndBottom = visible - 1; // as rowIndex

            // Identify multi-spanning symbols within the visible portion of the window and extend them as needed
            // e.g. if we have a symbol that spans 3 rows, and we can only see 1 symbol ID belonging to it at the very top of the window, then we need to expand it upwards into the border.

            // Top
            {
                const symbolID = this.get(0).symbolID;
                const symbolDef = this._symbolsByID[symbolID].definition;
                const span = symbolDef.span || 1;
                const extentDown = findExtent(this._symbols, border, visible + border, SearchDirection.Forwards, false, compareFunc);
                const projectUp = extentDown % span > 0 ? span - (extentDown % span) : 0;
                if (projectUp > 0)
                {
                    opaqueStartTop = -projectUp;
                    this.fillWithSymbol(-projectUp, projectUp, symbolID);
                }
            }
            // Bottom
            {
                const symbolID = this.get(visible - 1).symbolID;
                const symbolDef = this._symbolsByID[symbolID].definition;
                const span = symbolDef.span || 1;
                const extentUp = findExtent(this._symbols, visible + border - 1, visible + border, SearchDirection.Backwards, false, compareFunc);
                const projectDown = extentUp % span > 0 ? span - (extentUp % span) : 0;
                if (projectDown > 0)
                {
                    opaqueEndBottom = visible - 1 + projectDown;
                    this.fillWithSymbol(visible, projectDown, symbolID);
                }
            }

            // Clean up window edges make sure no spanning symbols are being cut off in the strip
            // We do this by identifying spanning symbols on the strip that intersect opaque symbols on the border and overlaying them with random symbol IDs

            // Top
            this.coverUpSymbol(this.toStripIndex(opaqueStartTop));

            // Bottom
            this.coverUpSymbol(this.toStripIndex(opaqueEndBottom));
        }

        /** Updates border symbols match the reel strip */
        protected updateBorderSymbols()
        {
            // Update top border symbols
            for (let rowIndex = -this._borderSize; rowIndex < 0; rowIndex++)
            {
                this.updateBorderSymbol(rowIndex);
            }

            // Update bottom border symbols
            for (let rowIndex = this._symbols.length - (this._borderSize * 2); rowIndex < this._symbols.length - this._borderSize; rowIndex++)
            {
                this.updateBorderSymbol(rowIndex);
            }
        }

        /** Set symbol to match reel strip */
        protected updateBorderSymbol(rowIndex: number)
        {
            const arrayIndex = this.toArrayIndex(rowIndex);
            const symbolId = this._strip.get(this.stopIndex + rowIndex);
            this._symbols[arrayIndex].symbolID = symbolId;
        }
    }

    export namespace Window
    {
        export type RandomSymbolFunction = (bindingSet: Symbols.BindingSet, strip: Strip, stripIndex: number, maxSpan: number) => number;

        export const defaultRandomSymbolFunction: RandomSymbolFunction = (bindings, strip, stripIndex, maxSpan) =>
        {
            return bindings[randomSelectSymbol(bindings, maxSpan)].id;
        };

        export interface SymbolData
        {
            symbolID: number | null;
            opacity: number;
            tint: Util.Color;
        }
    }
}
