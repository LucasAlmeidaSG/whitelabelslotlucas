/// <reference path="IDatabase.ts" />

namespace RS.Reels.Symbols
{
    interface Entry
    {
        guid: GUID;
        definition: Definition;
        renderInfo: RenderInfo | null;
    }

    interface SpriteData
    {
        width  : number;
        height : number;
        anchorX: number;
        anchorY: number;
    }

    /** Size of the texture page. */
    const pageSize: Dimensions = { w: 2048, h: 2048 };

    /** How many blur passes to perform (each one is vertical gaussian blur r=6) */
    const blurPasses = 15;

    /** How much to expand the vertical bounds of each symbol by (overall) */
    const expansion = 0.25;

    class Database implements IDatabase
    {
        private _symbols: Entry[] = [];
        private _isDisposed: boolean = false;
        private _dirty: boolean = true;

        @AutoDispose private _mainSymbolTexture: RS.Rendering.ITexture | null = null;
        @AutoDispose private _blurSymbolTexture: RS.Rendering.ITexture | null = null;

        public get mainSymbolTexture() { return this._mainSymbolTexture;  }

        public get blurSymbolTexture() { return this._blurSymbolTexture; }

        /** Gets if this database has been disposed or not. */
        public get isDisposed() { return this._isDisposed; }

        /**
         * Registers a symbol with the database.
         */
        public registerSymbol(definition: Definition): GUID
        {
            const guid = this._symbols.length;
            this._symbols.push({ guid, definition, renderInfo: null });
            this._dirty = true;
            return guid;
        }

        /**
         * Removes a symbol from the database.
         */
        public unregisterSymbol(definition: Definition): void
        {
            const guid = this.get(definition);
            if (guid == null)
            {
                Log.warn(`[Symbols.Database] Attempted to unregister definition ${definition.name}, but it was not registered.`);
                return;
            }
            this._symbols.splice(guid, 1);
            this._dirty = true;
        }

        /**
         * Finds a symbol definition using the given function.
         */
        public find(predicate: (definition: Definition) => boolean): Definition | null
        {
            for (const symbol of this._symbols)
            {
                if (predicate(symbol.definition))
                {
                    return symbol.definition;
                }
            }
            return null;
        }

        /**
         * Gets the symbol from the specified GUID.
         */
        public get(guid: GUID): Definition | null;

        /**
         * Gets the GUID from the specified definition.
         */
        public get(definition: Definition): GUID | null;

        public get(p1: GUID | Definition): Definition | GUID | null
        {
            if (Is.number(p1))
            {
                const entry = this._symbols[p1];
                return entry && entry.definition || null;
            }
            else
            {
                for (let i = 0, l = this._symbols.length; i < l; ++i)
                {
                    const entry = this._symbols[i];
                    if (entry.definition === p1)
                    {
                        return i;
                    }
                }
                return null;
            }
        }

        public set(guid: GUID, definition: Definition): void
        {
            const entry = this._symbols[guid] || (this._symbols[guid] = { guid, definition, renderInfo: null })
            entry.definition = definition;
            this._dirty = true;
        }

        /**
         * Gets render info for the specified symbol definition.
         */
        public getRenderInfo(definition: Definition): RenderInfo | null;

        /**
         * Gets render info for the specified GUID.
         */
        public getRenderInfo(guid: GUID): RenderInfo | null;

        public getRenderInfo(p1: GUID | Definition): RenderInfo | null
        {
            const id = Is.number(p1) ? p1 : this.get(p1);
            const entry = this._symbols[id];
            return entry && entry.renderInfo || null;
        }

        /**
         * Rebuilds the symbol textures.
         */
        public rebuild(viewController: View.IController, forceRebuild: boolean = false, res = 1): void
        {
            if (!this._dirty && !forceRebuild) { return; }
            if (this._mainSymbolTexture)
            {
                this._mainSymbolTexture.dispose();
                this._mainSymbolTexture = null;
            }
            if (this._blurSymbolTexture)
            {
                this._blurSymbolTexture.dispose();
                this._blurSymbolTexture = null;
            }

            const pageHeight = pageSize.h / res;

            let x = 0;
            let y = 0;
            let width = 0;
            let maxWidth = 0;
            let maxHeight = 0;
            const container = new RS.Rendering.DisplayObjectContainer();

            const clips: (RS.Rendering.Rectangle | null)[] = [];
            const regs: (Math.Vector2D | null)[] = [];
            const sprites: RS.Rendering.IBitmap[] = [];

            let verticalBlurNeeded = false;
            let horizontalBlurNeeded = false;

            for (const entry of this._symbols)
            {
                const def = entry.definition;
                if (!horizontalBlurNeeded && def.horizontalBlur)
                {
                    horizontalBlurNeeded = true;
                }
                if (!verticalBlurNeeded && !def.horizontalBlur)
                {
                    verticalBlurNeeded = true;
                }
                let spr: RS.Rendering.IBitmap;

                // data used to calculate regs
                const spriteData: SpriteData = { width: 0, height: 0, anchorX: 0, anchorY: 0 };

                // use texture directly if provided
                if (def.spriteTexture != null)
                {
                    // definition with texture
                    spr = this.getBitmapFromTexture(def.spriteTexture, def, spriteData);
                }
                else if (def.spriteAsset != null)
                {
                    // definition with asset
                    spr = this.getBitmapFromAsset(def, spriteData);
                }
                sprites.push(spr);

                if (spr == null)
                {
                    clips.push(null);
                    regs.push(null);
                    continue;
                }

                const ch = Math.ceil(spriteData.height * (1 + expansion));

                if (y + ch >= pageHeight)
                {
                    y = 0;
                    x += width + 1;
                    width = 0;
                }
                const clip = new RS.Rendering.Rectangle(x, y, spriteData.width, ch);

                spr.x = x + spriteData.anchorX;
                spr.y = y + spriteData.anchorY + Math.floor((spriteData.height * expansion) / 2);

                container.addChild(spr);

                y += ch;

                if (spriteData.width > width)
                {
                    width = spriteData.width;
                }

                if (y > maxHeight)
                {
                    maxHeight = y;
                }

                clips.push(clip);
                regs.push({ x: spriteData.anchorX, y: spriteData.anchorY + Math.floor((spriteData.height * expansion) / 2) });
            }

            if (container.children.length === 0)
            {
                this._dirty = false;
                return;
            }

            maxWidth = x + width;
            width = Math.nextPowerOf2(maxWidth);

            const renderer = viewController.stage;

            // Render base texture
            const baseRT = new RS.Rendering.RenderTexture(width, pageHeight, res);
            for (let i = 0, l = clips.length; i < l; ++i)
            {
                const entry = this._symbols[i];
                const clip = clips[i];
                if (clip)
                {
                    const renderInfo: RenderInfo =
                    {
                        definition: entry.definition,
                        texture: new RS.Rendering.SubTexture(baseRT, clip),
                        blurTexture: null,
                        pivot: { x: regs[i].x, y: regs[i].y }
                    };
                    entry.renderInfo = renderInfo;
                }
                else
                {
                    entry.renderInfo = null;
                }
            }
            baseRT.render(renderer, container);
            this._mainSymbolTexture = baseRT;

            // Render blur texture
            const blurRT = new RS.Rendering.RenderTexture(width, pageHeight, res);
            const blurShaderProgram = verticalBlurNeeded ? new RS.Rendering.Shaders.BlurShaderProgram({
                direction: Math.Vector2D(0.0, 1.0),
                radius: 6,
                dependentTextureReads: true
            }) : null;
            const horizontalBlurShaderProgram = horizontalBlurNeeded ? new RS.Rendering.Shaders.BlurShaderProgram({
                direction: Math.Vector2D(1.0, 0.0),
                radius: 6,
                dependentTextureReads: true
            }) : null;
            const verticalFilters: RS.Rendering.IFilter[] = [];
            const horizontalFilters: RS.Rendering.IFilter[] = [];
            for (let i = 0; i < blurPasses; ++i)
            {
                if (verticalBlurNeeded)
                {
                    verticalFilters.push(new RS.Rendering.Filter(blurShaderProgram.createShader()));
                }
                if (horizontalBlurNeeded)
                {
                    horizontalFilters.push(new RS.Rendering.Filter(horizontalBlurShaderProgram.createShader()));
                }
            }
            for (let i = 0, l = clips.length; i < l; ++i)
            {
                const entry = this._symbols[i];
                const spr = sprites[i];
                if (spr)
                {
                    spr.filters = entry.definition.horizontalBlur ? horizontalFilters : verticalFilters;
                }
            }
            blurRT.render(renderer, container);
            this._blurSymbolTexture = blurRT;

            for (let i = 0, l = clips.length; i < l; ++i)
            {
                const info = this._symbols[i];
                const clip = clips[i];
                if (clip && info.renderInfo)
                {
                    info.renderInfo.blurTexture = new RS.Rendering.SubTexture(blurRT, clip);
                }
            }

            this._dirty = false;
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._blurSymbolTexture = null;
            this._symbols.length = 0;
            this._symbols = null;
            this._isDisposed = true;
        }

        private getBitmapFromTexture(texture: RS.Rendering.ISubTexture, def: Definition, spriteData: SpriteData): RS.Rendering.IBitmap
        {
            const spr = new RS.Rendering.Bitmap(texture);
            spriteData.width = spr.width;
            spriteData.height = spr.height;
            spriteData.anchorX = Math.floor(spriteData.width / 2);
            spriteData.anchorY = Math.floor(spriteData.height / 2);
            spr.pivot.set(spriteData.anchorX, spriteData.anchorY);
            return spr;
        }

        private getBitmapFromAsset(def: Definition, spriteData: SpriteData): RS.Rendering.IBitmap
        {
            let spr: RS.Rendering.IBitmap;
            const asset = Asset.Store.getAsset(def.spriteAsset);

            switch (def.spriteAsset.type)
            {
                case "image":
                {
                    const rawAsset = asset.asset as RS.Rendering.ITexture;
                    if (rawAsset == null)
                    {
                        Log.error(`Symbol '${def.name}' refers to invalid asset (wrong type)`);
                        return;
                    }

                    spr = this.getBitmapFromTexture(new RS.Rendering.SubTexture(rawAsset), def, spriteData);
                    break;
                }
                case "spritesheet":
                case "rsspritesheet":
                {
                    const rawAsset = asset.asset as RS.Rendering.ISpriteSheet;
                    if (rawAsset == null)
                    {
                        Log.error(`Symbol '${def.name}' refers to invalid asset (wrong type)`);
                        return;
                    }

                    let frame = def.spriteFrame != null ? def.spriteFrame : 0;
                    if (def.animation != null && def.animation.name != null)
                    {
                        const anim = rawAsset.getAnimation(def.animation.name);
                        if (anim)
                        {
                            if (frame < 0)
                            {
                                frame += anim.frames.length;
                            }
                            frame = anim.frames[frame];
                        }
                    }
                    else if (frame < 0)
                    {
                        frame += rawAsset.getNumFrames();
                    }

                    spr = Factory.bitmap(def.spriteAsset, frame);
                    const frameData = rawAsset.getFrame(frame);
                    spriteData.width = frameData.imageRect.w;
                    spriteData.height = frameData.imageRect.h;
                    spriteData.anchorX = frameData.reg.x;
                    spriteData.anchorY = frameData.reg.y;

                    break;
                }
            }

            return spr;
        }
    }

    IDatabase.register(Database);
}
