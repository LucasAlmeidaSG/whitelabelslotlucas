namespace RS.Reels.Symbols.WindowPositionValidators
{
    const logger = Logging.ILogger.get().getContext("Reels");

    function getSpanPattern(symbolIDs: number[], maxSpan: number = 0): number[]
    {
        const pattern = [];
        let cumulative = 0;
        let lastSymbolID = symbolIDs[0];
        //for span symbol chains
        let innerPattern = [];
        let first = true;

        const addToPattern = () =>
        {
            if (innerPattern.length > 0)
            {
                first ?
                    innerPattern.unshift(cumulative) :
                    innerPattern.push(cumulative);
                pattern.push(...innerPattern);
            }
            else
            {
                pattern.push(cumulative);
            }
        }

        for (const id of symbolIDs)
        {
            if (lastSymbolID === id)
            {
                if (maxSpan && cumulative + 1 > maxSpan)
                {
                    first ?
                        innerPattern.unshift(cumulative) :
                        innerPattern.push(cumulative);

                    cumulative = 1;
                }
                else
                {
                    cumulative++;
                }
            }
            else
            {
                addToPattern();
                innerPattern = [];
                first = false;
                cumulative = 1;
                lastSymbolID = id;
            }
        }

        // last symbolID doesn't hit the equality check so add it here
        addToPattern();

        logger.debug(`[WindowPositionValidator MatchSpanPattern] ${symbolIDs} -> ${pattern}`);
        return pattern;
    }

    /**
     * Makes sure the window stops in the right place so that all symbols before gets rendered properly.
     * @param maxSpan number - optional max span param so its easier to find a pattern match. Helps with super long reelsets (500+ symbols)
     */
    export function MatchSpanPattern(maxSpan?: number): IReel.WindowPositionValidator
    {
        return (symbolStrip: Strip, stripIndex: number, stopSymbols: number[]): boolean =>
        {
            logger.debug(`[WindowPositionValidator MatchSpanPattern] stripIndex: ${stripIndex}`);
            //check span pattern of stop symbols
            const stopPattern = getSpanPattern(stopSymbols, maxSpan);

            //compare against the pattern in symbolStrip.get(stripIndex) + stopsymbols.length
            const stripStopSymbols = [];
            for (let i = 0; i < stopSymbols.length; i++)
            {
                stripStopSymbols.push(symbolStrip.get(stripIndex + i));
            }
            const stripPattern = getSpanPattern(stripStopSymbols, maxSpan);

            return Util.arraysEqual(stopPattern, stripPattern);
        }
    }
}