namespace RS.Reels.Symbols
{
    /**
     * Encapsulates a 1D array of symbol IDs.
     */
    export class Strip implements IDisposable
    {
        /** Published when a symbol on this strip has changed. */
        public readonly onSymbolChanged = createEvent<Strip.SymbolChangeData>();

        protected _data: number[];
        protected _isDisposed: boolean = false;

        /**
         * Gets the number of symbols in this strip.
         */
        @CheckDisposed
        public get length() { return this._data.length; }

        /**
         * Gets if this strip has been disposed.
         */
        public get isDisposed() { return this._isDisposed; }

        public constructor(symbols: number[]);
        
        public constructor(size: number, symbolID: number);

        public constructor(strip: Strip, offset?: number, size?: number);

        public constructor(reelSet: Slots.ReelSet, reelIndex: number, offset?: number, size?: number);

        public constructor(p1: number | number[] | Strip | Slots.ReelSet, p2?: number, p3?: number, p4?: number)
        {
            this.setFromInternal(p1, p2, p3, p4);
        }

        /**
         * Sets the contents of this strip from the specified array.
         * @param symbols 
         */
        public setFrom(symbols: number[]): void;

        /**
         * Initialises this strip to the specified size, populated by the specified symbol ID.
         * @param size 
         * @param symbolID 
         */
        public setFrom(size: number, symbolID: number): void;

        /**
         * Sets the contents of this strip from another, optionally with a different offset and size.
         * @param strip 
         * @param offset 
         * @param size 
         */
        public setFrom(strip: Strip, offset?: number, size?: number): void;

        /**
         * Sets the contents of this strip from a reel of a reel set.
         * @param reelSet 
         * @param reelIndex 
         * @param offset 
         * @param size 
         */
        public setFrom(reelSet: Slots.ReelSet, reelIndex: number, offset?: number, size?: number): void;

        public setFrom(p1: number | number[] | Strip | Slots.ReelSet, p2?: number, p3?: number, p4?: number): void
        {
            this.setFromInternal(p1, p2, p3, p4);
        }

        /**
         * Populates the strip randomly using the specified binding set.
         * @param bindingSet 
         */
        @CheckDisposed
        public populateRandomly(bindingSet: BindingSet): void
        {
            for (let i = 0, l = this._data.length; i < l; ++i)
            {
                const remaining = l - i;
                const bindingID = randomSelectSymbol(bindingSet, remaining, null);
                const span = bindingSet[bindingID].definition.span || 1;
                for (let j = 0; j < span; ++j)
                {
                    this.set(i + j, bindingSet[bindingID].id);
                }
                i += (span - 1);
            }
        }

        /**
         * Populates the strip randomly but evenly using the specified binding set.
         * @param bindingSet 
         */
        public populateEvenly(bindingSet: BindingSet): void
        {
            const symbols = constructSymbolsByID(bindingSet);
            const selectionChanceSum = symbols.map((s) => s.selectionChance).reduce((a, b) => a + b, 0);
            
            // Build array of heuristic counts for each symbol type
            const hCounts = new Array<number>(symbols.length);
            let targetSymbolCount = this._data.length;
            let overflow = 0;
            let counter = 0;
            do
            {
                targetSymbolCount -= overflow;
                let totalSize = 0;
                for (let i = 0; i < symbols.length; i++)
                {
                    if (symbols[i])
                    {
                        const span = symbols[i].definition.span || 1;
                        const cnt = hCounts[i] = ((symbols[i].selectionChance / selectionChanceSum) * targetSymbolCount) | 0;
                        totalSize += cnt * span;
                    }
                    else
                    {
                        hCounts[i] = 0;
                    }
                }
                counter++;
                if (counter > 100 || targetSymbolCount < 0)
                {
                    throw new Error("Infinite loop detected");
                }
                overflow = totalSize - this._data.length;
            }
            while (overflow > 0);

            // Construct collapsed slice array
            const cSlice: number[] = [];
            for (let i = 0; i < hCounts.length; i++)
            {
                for (let j = 0; j < hCounts[i]; j++)
                {
                    cSlice.push(i);
                }
            }
            RS.Math.shuffle(cSlice);

            // Fill in
            let ptr = 0;
            for (let i = 0; i < cSlice.length; i++)
            {
                const symbolID = cSlice[i];
                let span = symbols[symbolID].definition.span || 1;
                while (span-- > 0)
                {
                    this._data[ptr++] = symbolID;
                }
            }

            // Fix any underflow
            fillWithRandomSymbols(this._data, ptr, this._data.length - ptr, bindingSet);
        }

        /**
         * Wraps a row index to within bounds of this strip.
         * @param rowIndex 
         */
        @CheckDisposed
        public wrapRowIndex(rowIndex: number): number
        {
            return Math.wrap(rowIndex, 0, this.length);
        }

        /**
         * Clamps a row index to within bounds of this strip.
         * @param rowIndex 
         */
        @CheckDisposed
        public clampRowIndex(rowIndex: number): number
        {
            return Math.clamp(rowIndex, 0, this.length - 1);
        }

        /**
         * Gets a symbol from this strip.
         * @param rowIndex Row index of the symbol, will be wrapped.
         */
        @CheckDisposed
        public get(rowIndex: number): number
        {
            return this._data[this.wrapRowIndex(rowIndex)];
        }

        /**
         * Sets a symbol on this strip.
         * @param rowIndex 
         * @param symbolID 
         */
        @CheckDisposed
        public set(rowIndex: number, symbolID: number): void
        {
            const existing = this.get(rowIndex);
            if (existing === symbolID) { return; }
            this._data[this.wrapRowIndex(rowIndex)] = symbolID;
            this.onSymbolChanged.publish({ rowIndex, oldSymbolID: existing, newSymbolID: symbolID });
        }

        /**
         * Finds the extent of the block of equal values in the specified array at the specified index.
         * Can search forwards or backwards.
         * @param arr       The array to search
         * @param index     The index to start searching at
         * @param direction The direction to search in (1 = forwards, -1 = backwards)
         * @param wrap
         */
        public findExtent(index: number, direction: SearchDirection = SearchDirection.Forwards, wrap: boolean = true): number
        {
            return findExtent(this._data, index, this._data.length, direction, wrap);
        }

        /**
         * Creates a clone of this strip.
         */
        @CheckDisposed
        public clone(): Strip
        {
            return new Strip(this);
        }

        /**
         * Gets if this strip is equal to the other.
         * @param other 
         */
        public equals(other: Strip): boolean;

        /**
         * Gets if this strip is equal to the specified array.
         * @param other
         */
        public equals(other: ReadonlyArray<number>): boolean;

        public equals(other: Strip | ReadonlyArray<number>): boolean
        {
            if (this.length !== other.length) { return false; }
            if (other instanceof Strip)
            {
                for (let i = 0, l = this.length; i < l; ++i)
                {
                    if (this.get(i) !== other.get(i)) { return false; }
                }
            }
            else
            {
                for (let i = 0, l = this.length; i < l; ++i)
                {
                    if (this.get(i) !== other[i]) { return false; }
                }
            }
            return true;
        }

        /**
         * Disposes this symbol strip.
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }
            this._data.length = 0;
            this._data = null;
            this._isDisposed = true;
        }

        protected setFromInternal(p1: number | number[] | Strip | Slots.ReelSet, p2?: number, p3?: number, p4?: number): void
        {
            if (Is.array(p1))
            {
                this._data = p1.slice();
            }
            else if (Is.number(p1) && p2 != null)
            {
                this._data = new Array<number>(p1);
                for (let i = 0; i < p1; ++i)
                {
                    this._data[i] = p2;
                }
            }
            else if (p1 instanceof Strip)
            {
                if (p2 != null)
                {
                    if (p3 != null)
                    {
                        this._data = p1._data.slice(p2, p2 + p3);
                    }
                    else
                    {
                        this._data = p1._data.slice(p2);
                    }
                }
                else
                {
                    this._data = p1._data.slice();
                }
            }
            else if (p1 instanceof Slots.ReelSet && p2 != null)
            {
                if (p3 != null)
                {
                    if (p4 != null)
                    {
                        this._data = p1.getReel(p2).slice(p3, p3 + p4);
                    }
                    else
                    {
                        this._data = p1.getReel(p2).slice(p3);
                    }
                }
                else
                {
                    this._data = p1.getReel(p2).slice();
                }
            }
            else
            {
                this._data = [];
            }
        }
    }

    export namespace Strip
    {
        export interface SymbolChangeData
        {
            rowIndex: number;
            oldSymbolID: number;
            newSymbolID: number;
        }
    }
}