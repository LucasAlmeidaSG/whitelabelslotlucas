/// <reference path="Definition.ts" />

namespace RS.Reels.Symbols
{
    /**
     * A unique identifier for a symbol.
     * This is NOT the same as a symbol ID used in models and by the engine!
     */
    export type GUID = number;

    /**
     * Encapsulates a database of symbols.
     */
    export interface IDatabase extends IDisposable
    {
        /**
         * Primary texture page.
         */
        readonly mainSymbolTexture: RS.Rendering.ITexture;

        /**
         * Motion blurred texture page.
         */
        readonly blurSymbolTexture: RS.Rendering.ITexture;

        /**
         * Registers a symbol with the database.
         */
        registerSymbol(definition: Definition): GUID;

        /**
         * Removes a symbol from the database.
         */
        unregisterSymbol(definition: Definition): void;

        /**
         * Finds a symbol definition using the given function.
         */
        find(predicate: (definition: Definition) => boolean): Definition | null;

        /**
         * Gets the symbol from the specified GUID.
         */
        get(guid: GUID): Definition | null;

        /**
         * Gets the GUID from the specified definition.
         */
        get(definition: Definition): GUID;

        /**
         * Sets the definition for the specified GUID.
         */
        set(guid: GUID, definition: Definition): void;

        /**
         * Gets render info for the specified symbol definition.
         */
        getRenderInfo(definition: Definition): RenderInfo | null;

        /**
         * Gets render info for the specified GUID.
         */
        getRenderInfo(guid: GUID): RenderInfo | null;

        /**
         * Rebuilds render info for all registered symbols.
         * @param forceRebuild Whether or not to rebuild even if no known changes have been made.
         * @param resolution Texture resolution. Does not affect apparent symbol size.
         */
        rebuild(viewController: View.IController, forceRebuild?: boolean, resolution?: number): void;
    }

    export const IDatabase = Strategy.declare<IDatabase>(Strategy.Type.Singleton);

    /**
     * Creates a debug display of the declared symbol database's texture, optionally with the given render parent.
     * @param blur Whether or not to display the blur texture, instead of the main texture.
     */
    export function debugDraw(blur: boolean, parent?: RS.Rendering.IContainer): RS.Rendering.IDisplayObject;
    /**
     * Creates a debug display of the given symbol database's texture, optionally with the given render parent.
     * @param blur Whether or not to display the blur texture, instead of the main texture.
     */
    export function debugDraw(blur: boolean, database: IDatabase, parent?: RS.Rendering.IContainer): RS.Rendering.IDisplayObject;
    export function debugDraw(blur: boolean, p1?: IDatabase | RS.Rendering.IContainer, p2?: RS.Rendering.IContainer): RS.Rendering.IDisplayObject
    {
        let database: IDatabase, parent: RS.Rendering.IContainer;
        if (p1 == null || p1 instanceof RS.Rendering.Container)
        {
            database = IDatabase.get();
            parent = p1 as RS.Rendering.IContainer;
        }
        else
        {
            database = p1;
            parent = p2;
        }

        // Bitmap with symbol texture on.
        const subTex = new RS.Rendering.SubTexture(blur ? database.blurSymbolTexture : database.mainSymbolTexture);
        const bitmap = new RS.Rendering.Bitmap(subTex);

        // Graphics background to show bounds.
        const gfx = new RS.Rendering.Graphics();
        gfx.beginFill(Util.Colors.white).drawRect(0, 0, bitmap.localBounds.w, bitmap.localBounds.h);

        // Container to keep both together.
        const container = new RS.Rendering.Container();
        container.addChild(gfx);
        container.addChild(bitmap);

        // Everything is ready, add child and return.
        if (parent) { parent.addChild(container); } 
        return container;
    }
}