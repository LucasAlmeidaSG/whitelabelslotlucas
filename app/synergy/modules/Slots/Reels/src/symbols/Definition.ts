namespace RS.Reels.Symbols
{
    /**
     * Defines a single symbol.
     */
    export interface Definition
    {
        /** Name to used to identify this symbol. */
        name: string;

        /** The asset to use for displaying the symbol. */
        spriteAsset?: Asset.ImageReference | Asset.SpriteSheetReference | Asset.RSSpriteSheetReference;

        /** Use a texture directly if provided. */
        spriteTexture?: RS.Rendering.ISubTexture;

        /** If the asset is a sprite sheet, the frame to use when this symbol is static. */
        spriteFrame?: number;

        /** The model asset to use if the reel is rendered in 3D. */
        modelAsset?: Asset.ModelReference;

        /** Use a 3D geometry directly if provided. */
        modelGeometry?: RS.Rendering.Scene.IReadonlyGeometry;

        /** A scale to apply to the model if the reel is rendered in 3D. */
        modelScale?: RS.Math.ReadonlyVector3D;

        /** An offset to apply to the model if the reel is rendered in 3D. */
        modelOffset?: RS.Math.ReadonlyVector3D;

        /** Materials to apply to the model if the reel is rendered in 3D. */
        modelMaterials?: RS.Rendering.Scene.Material[];

        /** Animation details for the symbol. */
        animation?:
        {
            /** Framerate to play at. Defaults to the sprite sheet's default. */
            framerate?: number;

            /** Animation name. */
            name?: string;
        };

        /** Used when animating the symbol if present. A symbol asset is still required for displaying the symbol normally */
        spineAnimation?:
        {
            spineAsset: Asset.SpineReference;
            animationName: string;
            /** Scale to use when creating the spine */
            scale?: number;
            /** Duration to use to compute the win rendering time. Calculated if not specified. */
            animationDuration?: number;
        }

        /** The x/y offset to use when drawing this symbol. */
        offset?: Math.Vector2D;

        /** controls z-index on the reel (higher numbers will be above symbols with lower numbers). */
        zIndex?: number;

        /** The height span of the symbol, for multi spanning symbols (default is 1). */
        span?: number;

        /** Should a multi-spanning symbol render 1 sprite per symbol location? (default is false, meaning render 1 sprite to represent the entire span). */
        spanSplit?: boolean;

        /** Is the symbol a scatter (will cause suspense). */
        isScatter?: boolean;

        /** Symbol scatter group. Only scatters sharing a group trigger suspense. Defaults to 0. */
        scatterGroup?: number;

        /** should the symbol blur texture be horizontal? defaults to vertical */
        horizontalBlur?: boolean;
    }

    /**
     * Defines additional data for a symbol.
     */
    export interface BaseSymbolData
    {
        /** Definition for the symbol. */
        definition: Definition;
    }

    /**
     * Data for symbol rendering.
     */
    export interface RenderInfo extends BaseSymbolData
    {
        /** Texture for the symbol. */
        texture: RS.Rendering.ISubTexture | null;

        /** Motion blurred texture for the symbol. */
        blurTexture: RS.Rendering.ISubTexture | null;

        /** Pivot for the symbol. */
        pivot: Math.Vector2D;
    }

    /**
     * Describes a relationship between a symbol and a reel.
     */
    export interface Binding extends BaseSymbolData
    {
        /**
         * Chance that the symbol crops up on this reel.
         * On a scale of 0-N, where N is the sum of selection chances across every symbol binding for this reel.
         * Defaults to 1.
         */
        selectionChance?: number;

        /**
         * The ID mapping of this symbol.
         */
        id: number;
    }

    /**
     * A set of symbol bindings.
     */
    export type BindingSet = Binding[];
}
