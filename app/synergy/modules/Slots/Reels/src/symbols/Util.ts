namespace RS.Reels.Symbols
{
    export enum SearchDirection { Forwards = 1, Backwards = -1 }

    export type CompareFunction<T> = (a: T, b: T) => boolean;

    /**
     * Finds the extent of the block of equal values in the specified array at the specified index.
     * Can search forwards or backwards.
     * @param arr       The array to search
     * @param index     The index to start searching at
     * @param direction The direction to search in (1 = forwards, -1 = backwards)
     * @param wrap
     */
    export function findExtent<T>(arr: ArrayLike<T>, index: number = 0, length?: number, direction: SearchDirection = SearchDirection.Forwards, wrap: boolean = true, compareFunction?: CompareFunction<T>): number
    {
        if (direction === 0) { throw new Error("Infinite loop avoided"); }
        if (length == null) { length = arr.length; }
        if (wrap) { index = Math.wrap(index, 0, length); }
        const initialIndex = index;
        const val = arr[index];
        let accum = 0;
        const compare = compareFunction == null ? (a, b) => a == b : compareFunction;
        while (compare(val, arr[index]))
        {
            index += direction;
            accum++;
            if (wrap) { index = Math.wrap(index, 0, length); }
            if (index === initialIndex) { return length; }
            if (index < 0 || index >= length) { break; }
        }
        return accum;
    }

    export function constructSymbolsByID(bindingSet: BindingSet): BindingSet
    {
        const symbolsByID: BindingSet = [];
        for (let i = 0, l = bindingSet.length; i < l; ++i)
        {
            const binding = bindingSet[i];
            symbolsByID[binding.id] = binding;
        }
        return symbolsByID;
    }

    /**
     * Returns the lowest span of any selectable symbol in the given binding set.
     * Returns 1 if the binding set is empty.
     */
    export function findLowestSelectableSpan(bindingSet: BindingSet): number
    {
        if (bindingSet.length === 0) { return 1; }

        let result: number;
        for (const binding of bindingSet)
        {
            if (!binding || binding.selectionChance <= 0) { continue; }
            const span = binding.definition.span || 1;
            result = result ? Math.min(result, span) : span;
        }

        return result;
    }

    /**
     * Given a set of symbols, selects one at random while considering their selection chance.
     * @param symbols       The symbols to select from.
     * @param maxSpan       The maximum span value to allow.
     * @param exclude       Array of symbol IDs to exclude.
     * @returns             The index of the selected symbol within the binding set.
     */
    export function randomSelectSymbol(bindingSet: BindingSet, maxSpan?: number, exclude?: ReadonlyArray<number>): number
    {
        const excludePredicate = (id: number) => (bindingSet[id] == null) || (maxSpan != null ? (bindingSet[id].definition.span||1) > maxSpan : false) || (exclude != null ? exclude.indexOf(id) != -1 : false);
        let totalChance = 0;
        for (let i = 0; i < bindingSet.length; i++)
        {
            if (!excludePredicate(i))
            {
                totalChance += bindingSet[i].selectionChance != null ? bindingSet[i].selectionChance : 1;
            }
        }
        const randomSelection = Math.random() * totalChance;
        let chanceTally = 0;
        for (let i = 0; i < bindingSet.length; i++)
        {
            if (!excludePredicate(i))
            {
                chanceTally += bindingSet[i].selectionChance != null ? bindingSet[i].selectionChance : 1;
                if (chanceTally > randomSelection) { return i; }
            }
        }

        if (maxSpan != null)
        {
            throw new Error(`Could not find a random symbol to use; no symbol exists with a span less than or equal to ${maxSpan}.`);
        }
        else
        {
            throw new Error("Could not find a random symbol to use; something weird happened.");
        }
    }

    /**
     * Fills the specified slice of array with random symbols
     */
    export function fillWithRandomSymbols(arr: (number | null)[], idx: number, sz: number, bindingSet: BindingSet, onlyEmpty = false): void
    {
        let prevI = idx;
        for (let i = idx; i < idx + sz;)
        {
            if (arr[i] == null || !onlyEmpty)
            {
                let maxSpan: number;
                const totalSpace = idx + sz - i;
                if (onlyEmpty)
                {
                    for (maxSpan = 1; maxSpan < totalSpace; ++maxSpan)
                    {
                        if (arr[i + maxSpan] != null) { break; }
                    }
                }
                else
                {
                    maxSpan = totalSpace;
                }

                const bindingIndex = randomSelectSymbol(bindingSet, maxSpan);
                const symbolID = bindingSet[bindingIndex].id;
                const span = bindingSet[bindingIndex].definition.span || 1;
                const endPoint = i + span;
                while (i < endPoint)
                {
                    if (arr[i] == null || !onlyEmpty)
                    {
                        arr[i] = symbolID;
                    }
                    ++i;
                }
            }
            else
            {
                ++i;
            }
            if (i === prevI)
            {
                throw new Error("infinite loop detected");
            }
            prevI = i;
        }
    }
}
