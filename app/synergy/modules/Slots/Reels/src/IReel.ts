namespace RS.Reels
{
    /**
     * Encapsulates a single reel.
     */
    export interface IReel<TSettings extends IReel.Settings> extends IDisposable
    {
        /** Settings for the reel. */
        readonly settings: TSettings;

        /**
         * Gets or sets the "master" reel for this reel.
         * This reel will spin in sync with the master reel, as if locked to it.
         */
        masterReel: IGenericReel;

        /** Gets or sets the current spinning speed of this reel. */
        speed: number;

        /** Gets or sets the current spinning position of this reel. */
        delta: number;

        /** Gets or sets if this reel is visible. */
        visible: boolean;

        /** Gets or sets the tint of this reel. */
        tint: Util.Color;

        /** Gets or sets the x position of this reel. */
        x: number;

        /** Gets or sets the y position of this reel. */
        y: number;

        /** Gets or sets the current stop index */
        stopIndex?: number;

        /** Gets or sets the current state of this reel. */
        readonly state: IReadonlyObservable<IReel.State>;

        /** Published when this reel has been stopped. */
        readonly onStopped: IEvent<IReel.StopType>;

        /** Published when a symbol has landed. */
        readonly onSymbolLanded: IEvent<number>;

        /** Published when a scatter has landed on this reel. */
        readonly onScatterLanded: IEvent<IReel.ScatterLandedData>;

        /** Published when suspense has started for this reel. */
        readonly onSuspenseStarted: IEvent;

        /** Published when suspense has ended for this reel. */
        readonly onSuspenseEnded: IEvent;

        /** Published when the reel has actually begun to spin down. */
        readonly onDecelerateStarted: IEvent;

        /** Gets or creates a debug view for this reel. */
        readonly debugView: Rendering.DebugView;

        /** Gets how long this reel will take to spin down (ms). */
        readonly spinDownTime: number;

        /** Gets if this reel is in suspense or not. */
        readonly isSuspensing: boolean;

        /**
         * Sets the array of symbol bindings
         */
        setSymbolBindings(bindings: Symbols.Binding[]): void;

        /**
         * Gets the symbol definition for the specified ID, based on this reel's symbol bindings.
         */
        getSymbolDefinition(id: number): Symbols.Definition | null;

        /**
         * Gets the symbol ID for the specified definition, based on this reel's symbol bindings.
         */
        getSymbolID(definition: Symbols.Definition): number | null;

        /**
         * Updates the mask state for this reel.
         */
        updateMask(): void;

        /**
         * Sets the current stop symbols on this reel.
         */
        setStopSymbols(symbolsInView: number[], defer?: boolean, stopIndex?: number): void;

        /**
         * Sets a symbol in the visible window.
         * Will NOT consider any intersections or resolve multi-spanning symbols.
         */
        setStopSymbol(rowIndex: number, symbolID: number): void;

        /**
         * Sets the current strip of the reel.
         */
        setStrip(strip: number[]): void;

        /**
         * Requests that this reel spins up.
         */
        spinUp(startType: IReel.StartType): void;

        /**
         * Performs a tick on this reel.
         */
        tick(delta: number): void;

        /**
         * Requests that this reel spins down.
         * @param quick If true, will execute a quick stop (but not instant).
         */
        spinDown(stopType: IReel.StopType): void;

        /**
         * Hides a stop symbol at the given row index.
         * @param rowIndex Row index of the symbol, relative to the window position.
         */
        hideSymbol(rowIndex: number): void;

        /**
         * Shows a stop symbol at the given row index.
         * @param rowIndex Row index of the symbol, relative to the window position.
         */
        showSymbol(rowIndex: number): void;

        /**
         * Sets the opacity of the symbol at the given location.
         * @param rowIndex Row index of the symbol, relative to the window position.
         * @param opacity Opacity of the symbol (0-1).
         */
        setSymbolOpacity(rowIndex: number, opacity: number): void;

        /**
         * Sets the color of the symbol at the given location.
         * @param rowIndex Row index of the symbol, relative to the window position.
         * @param color Color of the symbol.
         */
        setSymbolColor(rowIndex: number, color: Util.Color): void;

        /**
         * Holds the specified row. It won't spin with the rest of the reels.
         * @param row
         */
        holdRow(row: number): void;

        /**
         * Holds the specified rows.
         * @param rows
         */
        holdRows(rows: ReadonlyArray<number>): void;

        /**
         * Holds all rows.
         */
        holdAllRows(): void;

        /**
         * Unholds the specified row.
         * @param row
         */
        unholdRow(row: number): void;

        /**
         * Unholds the specified rows.
         * @param rows
         */
        unholdRows(rows: ReadonlyArray<number>): void;

        /**
         * Unholds all rows.
         */
        unholdAllRows(): void;

        /**
         * Gets the position (center) of where a symbol would be drawn given the provided row, in reel space (where 0,0 is the top left of the reel).
         * @param rowIndex Row index of the symbol, relative to the window position.
         */
        getSymbolPosition(rowIndex: number): Math.Vector2D;

        /**
         * Given the index of the specified symbol (in stop symbol coordinates), finds the row index that STARTS the symbol.
         * If the symbol's span is 1, this function will just return the passed in row.
         * If the symbol's span is > 1, this function will find the top-most point of the symbol.
         * Searches the entire symbol strip including the window overlay.
         * @param rowIndex Row index of the symbol, relative to the window position.
         * @returns         The row of the symbol's start, in stop symbol coordinates
         */
        findSymbolStart(rowIndex: number): number;

        /**
         * Nudges the position of the reel up or down by a single symbol.
         * Async, resolves when nudge has finished.
         */
        nudge(direction: "up" | "down", duration: number, ease?: (num: number) => number): PromiseLike<void>;

        /**
         * Nudges the position of the reel up or down by a specified amount of spaces.
         * Negative spaces will nudge the reel up, while positive spaces will nudge it down.
         * Async, resolves when nudge has finished.
         */
        nudgeBy?(spaces: number,  duration: number, ease?: (num: number) => number): PromiseLike<void>;

        /**
         * Repositions the window to align with the reel.
         */
        realignWindow(): void;

        /**
         * Begins suspense rendering on this reel.
         */
        beginSuspense(): void;

        /**
         * Ends suspense rendering on this reel.
         */
        endSuspense(): void;
    }

    export type IGenericReel = IReel<IReel.Settings>;

    /**
     * A constructor for a reel.
     */
    export type ReelConstructor<TSettings extends IReel.Settings> = { new(settings: TSettings, symbolContainer: RS.Rendering.IContainer, sceneRoot?: RS.Rendering.Scene.ISceneObject): IReel<TSettings>; };

    export type GenericReelConstructor = ReelConstructor<IReel.Settings>;

    export namespace IReel
    {
        /**
         * Describes the current spin state of a reel.
         */
        export enum State
        {
            /** Not moving */
            Stopped,

            /** Spinning up, not yet reached full speed */
            SpinningUp,

            /** Full speed */
            Spinning,

            /** Slowing to a stop position */
            SlowingDown
        }

        /**
         * Describes how the reels should or did start.
         */
        export enum StartType
        {
            /** Normal start type, reels start at set intervals and speed up to top speed with optional windup. */
            Standard,

            /** Reels start at full speed. */
            Immediate
        }

        /**
         * Describes how a reel should or did stop.
         */
        export enum StopType
        {
            /** Normal stop type. */
            Standard,

            /** Spins down in an accelerated manner. */
            Quick,

            /** Turbo mode stop type */
            Turbo,

            /** Stop immediately with no spin down (hard reset). */
            Immediate
        }

        /**
         * Describes a scatter landed event.
         */
        export interface ScatterLandedData
        {
            rowIndex: number;
            stopType: StopType;
        }

        /**
         * Encapsulates a function that validates a candidate position of the window on the strip.
         * @param strip the strip that will be overlaid by the window
         * @param stripIndex the candidate position which the top of the window would align with
         * @param stopIndex the stop position
         * @param stopSymbols the symbols that will be displayed
         */
        export type WindowPositionValidator = (strip: Symbols.Strip, stripIndex: number, stopSymbols: number[]) => boolean;

        /**
         * Basic settings for a reel.
         */
        export interface Settings
        {
            /** Symbols to use. */
            symbols: Symbols.BindingSet;

            /** Symbol database to use. */
            database: Symbols.IDatabase;

            /** Number of symbols to show when the reel is stopped (rows). */
            stopSymbolCount: number;

            /** Sound to play when the reel stops. */
            stopSound?: Asset.SoundReference;

            /** Spacing between symbols (height) in pixels. */
            symbolSpacing: number;

            /** Whether or not to use motion blur effect. */
            useMotionBlur: boolean;

            /** Size of the symbol "strip". */
            symbolsPerStrip: number;

            /** Effective width of the reel. Used to center symbols. */
            width: number;

            /** Amount to multiply/ enhance blur effect by. Must be present if useMotionBlur is true. */
            blurMultiplier?: number;

            /** Speed at which reels start to blur from. Must be present if useMotionBlur is true. */
            minimumBlurSpeed?: number;

            /** Should this reel exhibit suspense? */
            shouldSuspense?: boolean;

            /** How long extra should this reel take to stop if exhibiting suspense (ms)? */
            suspenseTime?: number;

            /** Additional time to delay the reel when spinning down (ms). */
            spinDownDelay?: number;

            /** Additional time to delay the reel when spinning down after suspense. */
            suspenseSpinDownDelay?: number;

            /** Maximum width of a symbol. */
            maxSymbolWidth?: number;

            /** Whether or not the reel has symbols larger than a row height */
            hasLongSymbols?: boolean;

            /** Distance from the edge the reels start to fade out. */
            fadeDistance?: number;

            /** How much to expand the mask upwards by. */
            maskExpandTop?: number;

            /** How much to expand the mask downwards by. */
            maskExpandBottom?: number;

            /** Whether or not to use the shader-based approach to reels rendering. */
            useShader?: boolean;

            /** Defines an offset for the reel. */
            offset?: RS.Math.Vector2D;

            /** Defines an offset for 3D symbols in the reel. */
            sceneOffset?: RS.Math.Vector3D;

            /** Defines how much spacing should be between each 3D symbol in the reel. */
            sceneSpacing?: RS.Math.Vector3D;

            /** Defines a z-index the reel */
            zIndex?: number;

            /** Specifies a custom strip symbol class to use. */
            stripSymbolClass?: { new(database: Symbols.IDatabase, definition?: Symbols.Definition): Rendering.StripSymbol; };

            /** Specifies a custom strip model class to use. */
            stripModelClass?: { new(database: Symbols.IDatabase, definition?: Symbols.Definition): Rendering.StripModel; };

            /** Whether or not to ignore master reel's symbols */
            ignoreMasterReelSymbols?: boolean;

            /** Function which restricts where the stop symbols can be placed on the underlying strip. */
            windowPositionValidator?: WindowPositionValidator;

            /** Function which selects random symbols for specific positions when resolving spanning symbol intersections on the window border.  */
            windowRandomSymbolFunction?: Symbols.Window.RandomSymbolFunction;
        }
    }
}
