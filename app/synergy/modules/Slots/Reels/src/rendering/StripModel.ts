/// <reference path="../symbols/IDatabase.ts" />

namespace RS.Reels.Rendering
{
    /**
     * Represents a single drawable symbol in 3D.
     */
    export class StripModel extends RS.Rendering.Scene.SceneObject
    {
        protected _symbolDef: Symbols.Definition | null;
        protected _database: Symbols.IDatabase;
        @RS.AutoDisposeOnSet protected _mesh: RS.Rendering.Scene.IMeshObject | null = null;

        /** Gets the symbol that this strip symbol displays. */
        public get symbolDefinition() { return this._symbolDef; }

        /** Gets the symbol database for this strip symbol. */
        public get database() { return this._database; }

        constructor(database: Symbols.IDatabase, definition: Symbols.Definition | null = null)
        {
            super();
            this._database = database;
            this._mesh = new RS.Rendering.Scene.MeshObject();
            this.addChild(this._mesh);
            if (definition != null)
            {
                this.setSymbol(definition, false, 0);
            }
            else
            {
                this.clearSymbol();
            }
        }

        /**
         * Updates the definition that this strip symbol should display.
         * @param symbolDef 
         * @param inWindow true if this symbol represents a symbol on the window
         * @param index if inWindow is true, the row index, otherwise the strip index
         */
        public setSymbol(symbolDef: Symbols.Definition, inWindow: boolean, index: number): void
        {
            this._symbolDef = symbolDef;
            if (symbolDef.modelAsset)
            {
                this._mesh.geometry = symbolDef.modelGeometry || Asset.Store.getAsset(symbolDef.modelAsset).asset as RS.Rendering.Scene.IReadonlyGeometry;
                if (symbolDef.modelMaterials)
                {
                    this._mesh.materials = symbolDef.modelMaterials;
                }
                if (symbolDef.modelScale)
                {
                    Math.Vector3D.copy(symbolDef.modelScale, this._mesh.scale);
                }
                if (symbolDef.modelOffset)
                {
                    Math.Vector3D.copy(symbolDef.modelOffset, this._mesh.position);
                }
            }
        }

        /**
         * Clears the definition on this strip symbol.
         */
        public clearSymbol(): void
        {
            this._mesh.geometry = null;
            this._mesh.materials = null;
            this._mesh.scale.set(1.0, 1.0, 1.0);
        }

    }
}
