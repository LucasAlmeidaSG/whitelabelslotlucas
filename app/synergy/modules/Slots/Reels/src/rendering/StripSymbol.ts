/// <reference path="../symbols/IDatabase.ts" />

namespace RS.Reels.Rendering
{
    /**
     * Represents a single drawable symbol.
     */
    export class StripSymbol extends RS.Rendering.Bitmap
    {
        protected _symbolDef: Symbols.Definition | null;
        protected _database: Symbols.IDatabase;
        protected _reelZIndex: number = 0;

        /** Gets or sets the reel z-index */
        public get reelZIndex(): number { return this._reelZIndex; }
        public set reelZIndex(value) { this._reelZIndex = value; }

        /** Gets the global z-index */
        public get globalZIndex() { return (this.zIndex+this.reelZIndex); }

        /** Gets the symbol that this strip symbol displays. */
        public get symbolDefinition() { return this._symbolDef; }

        /** Gets the symbol database for this strip symbol. */
        public get database() { return this._database; }

        /** Gets the z index of this strip symbol. */
        public get zIndex() { return (this._symbolDef && this._symbolDef.zIndex) || 0; }

        constructor(database: Symbols.IDatabase, definition: Symbols.Definition | null = null)
        {
            super(null);
            this._database = database;
            if (definition != null)
            {
                this.setSymbol(definition, false, 0);
            }
            else
            {
                this.clearSymbol();
            }
        }

        /**
         * Sets the current blur factor for this strip symbol.
         * @param blurFactor
         */
        public setBlurFactor(blurFactor: number)
        {
            const shader = this.shader;
            if (shader && shader instanceof SymbolShader)
            {
                shader.blurFactor = blurFactor;
            }
        }

        /**
         * Updates the definition that this strip symbol should display.
         * @param symbolDef
         * @param inWindow true if this symbol represents a symbol on the window
         * @param index if inWindow is true, the row index, otherwise the strip index
         */
        public setSymbol(symbolDef: Symbols.Definition, inWindow: boolean, index: number): void
        {
            this._symbolDef = symbolDef;
            
            const renderInfo = this._database.getRenderInfo(symbolDef);
            if (!renderInfo) { throw new Error(`Render info not found for symbol definition ${Dump.stringify(symbolDef)}`); }

            this.texture = renderInfo.texture;
            this.pivot.copy(renderInfo.pivot);
        }

        /**
         * Clears the definition on this strip symbol.
         */
        public clearSymbol(): void
        {
            this.texture = null;
            this.pivot.set(0, 0);
        }

    }
}
