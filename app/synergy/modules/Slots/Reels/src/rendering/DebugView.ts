namespace RS.Reels.Rendering
{
    interface Unit
    {
        graphics: RS.Rendering.IGraphics;
        text: RS.Rendering.IText;
    }

    const unitDisplaySize = 20;
    const unitPadding = 2;
    const tmpColor = new RS.Util.Color();

    @HasCallbacks
    export class DebugView extends RS.Rendering.DisplayObjectContainer
    {
        private _units: Unit[];
        private _windowRect: RS.Rendering.IGraphics;
        private _symbolsByID: Symbols.BindingSet;
        private _settings: DebugView.Settings;
        private _unitContainer: RS.Rendering.IContainer;
        private _windowIsVisible: boolean;
        private _delta: number;

        public get delta() { return this._delta; }
        public set delta(value) { this._delta = value; }

        public get settings() { return this._settings; }
        public set settings(value)
        {
            this._settings = value;
            this.rebuild();
        }

        public get windowIsVisible() { return this._windowIsVisible; }
        public set windowIsVisible(value) { this._windowIsVisible = value; }

        public constructor(settings: DebugView.Settings)
        {
            super();

            this._unitContainer = new RS.Rendering.Container();
            this.addChild(this._unitContainer);

            this._windowRect = new RS.Rendering.Graphics();
            this.addChild(this._windowRect);

            this.settings = settings;

            Ticker.registerTickers(this);
        }

        public dispose()
        {
            if (this.isDisposed) { return; }
            Ticker.unregisterTickers(this);
            super.dispose();
        }

        protected rebuild(): void
        {
            this._unitContainer.removeAllChildren();

            this._symbolsByID = Symbols.constructSymbolsByID(this._settings.bindingSet);

            this._units = new Array<Unit>(this._settings.strip.length + this._settings.window.size);
            for (let i = 0, l = this._units.length; i < l; ++i)
            {
                const container = new RS.Rendering.DisplayObjectContainer();
                container.x = unitPadding;
                container.y = unitPadding + (unitDisplaySize + unitPadding) * i;

                const graphics = new RS.Rendering.Graphics();
                container.addChild(graphics);

                const text = new RS.Rendering.Text({
                    font: Fonts.FrutigerWorld,
                    fontSize: 16,
                    color: Util.Colors.black,
                    baseline: RS.Rendering.TextBaseline.Bottom
                });
                container.addChild(text);

                this._units[i] = { graphics, text };

                this._unitContainer.addChild(container);
            }
        }

        private updateUnit(index: number, color: Util.Color, text: string): void
        {
            const unit = this._units[index];
            const g = unit.graphics;

            g.clear();
            g.beginFill(color, 1.0);
            g.drawRect(0, 0, unitDisplaySize, unitDisplaySize);
            g.endFill();

            unit.text.text = text;
            const textBounds = unit.text.getLocalBounds();
            unit.text.x = unitDisplaySize * 0.5;
            unit.text.y = unitDisplaySize;
            unit.text.pivot.set(textBounds.w * 0.5, 0);
        }

        @Tick({ })
        private tick(): void
        {
            const deltaWrapped = Math.wrap(-this._delta, 0, this.settings.unitSize * this.settings.strip.length);
            const unitPos = deltaWrapped / this.settings.unitSize;
            const windowTop = Math.wrap(this.settings.window.position, 0, this.settings.strip.length);
            const windowBot = windowTop + this.settings.window.size;
            const windowBorderTop = windowTop - this.settings.window.borderSize;
            const windowBorderBot = windowBot + this.settings.window.borderSize;
            const tmp = {} as Symbols.Window.SymbolData;
            for (let i = 0, l = this._units.length; i < l; ++i)
            {
                const windowIdx = i - windowTop;

                let color: Util.Color;
                if (i >= windowTop && i < windowBot)
                {
                    if (!this._settings.window.isOpaque(windowIdx))
                    {
                        color = Util.Colors.lightsalmon;
                    }
                    else if (this._windowIsVisible)
                    {
                        color = Util.Colors.lightgreen;
                    }
                    else
                    {
                        color = Util.Colors.lightgoldenrodyellow;
                    }
                }
                else if (i >= windowBorderTop && i < windowBorderBot)
                {
                    if (!this._settings.window.isOpaque(windowIdx))
                    {
                        color = tmpColor;
                        tmpColor.lerp(Util.Colors.lightsalmon, Util.Colors.lightgrey, 0.5);
                    }
                    else if (this._windowIsVisible)
                    {
                        color = tmpColor;
                        tmpColor.lerp(Util.Colors.lightgreen, Util.Colors.lightgrey, 0.5);
                    }
                    else
                    {
                        color = tmpColor;
                        tmpColor.lerp(Util.Colors.lightgoldenrodyellow, Util.Colors.lightgrey, 0.5);
                    }
                }
                else
                {
                    color = Util.Colors.lightgrey;
                }

                const symbolID = this.settings.window.get(windowIdx, tmp).symbolID;
                const symbolDef = this._symbolsByID[symbolID].definition;
                const txt = symbolDef.name[0];

                this.updateUnit(i, color, txt);
            }

            this._windowRect.clear();
            this._windowRect.beginFill(Util.Colors.red, 0.5);
            this._windowRect.drawRect(0, unitPos * (unitDisplaySize + unitPadding), unitDisplaySize + unitPadding * 2, unitDisplaySize * this.settings.window.size + unitPadding * 2);
            this._windowRect.endFill();
        }
    }

    export namespace DebugView
    {
        export interface Settings
        {
            /** The symbol strip to view. */
            strip: Symbols.Strip;

            /** The symbol window to view. */
            window: Symbols.Window;

            /** The symbol binding set. */
            bindingSet: Symbols.BindingSet;

            /** The size of a single "unit" (symbol). */
            unitSize: number;
        }
    }
}