namespace RS.Reels
{
    /**
     * A tool for rigging a single reel by shifting the stop position.
     */
    @HasCallbacks
    export class RigTool extends RS.Flow.BaseElement<RigTool.Settings, RigTool.RuntimeData> implements SGI.Selenium.ISeleniumComponent
    {
        public seleniumId: string;

        @AutoDispose public readonly stopPosition = new Observable(0);
        @AutoDispose public readonly onStripButtonClicked = createSimpleEvent();

        @AutoDispose protected readonly _upButton: RS.Flow.Button;
        @AutoDispose protected readonly _downButton: RS.Flow.Button;
        @AutoDispose protected readonly _valueButton: RS.Flow.Button;
        @AutoDispose protected readonly _stripButton: RS.Flow.Button;

        @AutoDisposeOnSet protected _holdTween: RS.Tween<RigTool>;

        public constructor(settings: RigTool.Settings, runtimeData: RigTool.RuntimeData)
        {
            super(settings, runtimeData);

            this._upButton = Flow.button.create(settings.upButton, this);
            this._upButton.onPressed(this.onDecDown);
            this._upButton.onReleased(this.onButtonUp);

            this._downButton = Flow.button.create(settings.downButton, this);
            this._downButton.onPressed(this.onIncDown);
            this._downButton.onReleased(this.onButtonUp);

            this._valueButton = Flow.button.create(settings.valueButton, this);
            this._valueButton.onClicked(this.onValueClicked);

            if (settings.stripButtonActivated)
            {
                this._stripButton = Flow.button.create(settings.stripButton, this);
                this._stripButton.onClicked(() => this.onStripButtonClicked.publish());
            }

            this.stopPosition.onChanged((val) => this._valueButton.label.text = `${val}`);
        }

        @Callback
        protected onIncDown(): void
        {
            this.stopPosition.value = Math.wrap((this.stopPosition.value + 1), 0, this.runtimeData.symbolCount);
            this.startShift(1);
        }

        @Callback
        protected onDecDown(): void
        {
            this.stopPosition.value = Math.wrap((this.stopPosition.value - 1), 0, this.runtimeData.symbolCount);
            this.startShift(-1);
        }

        @Callback
        protected onButtonUp(): void
        {
            this._holdTween = null;
        }

        @Callback
        protected async startShift(delta: number)
        {
            try
            {
                await (this._holdTween = Tween.wait<RigTool>(this.settings.holdDelay, this, { ignoreGlobalPause: true, realTime: true }));
                this._holdTween = Tween.get<RigTool>(this, { loop: true, ignoreGlobalPause: true, realTime: true })
                    .wait(this.settings.holdInterval)
                    .call(() => { this.stopPosition.value = Math.wrap((this.stopPosition.value + delta), 0, this.runtimeData.symbolCount); });
            }
            catch (e)
            {
                if (e instanceof Tween.DisposedRejection) { return; }
                throw e;
            }
        }

        @Callback
        protected onValueClicked(): void
        {
            const explanation = "Enter an index for the reel";
            const textString = window.prompt(explanation, "");
            if (textString != null)
            {
                const parsedIndex = parseInt(textString);
                if (Is.number(parsedIndex))
                {
                    this.stopPosition.value = parsedIndex;
                }
            }
        }
    }

    export namespace RigTool
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            upButton: RS.Flow.Button.Settings;
            downButton: RS.Flow.Button.Settings;
            valueButton: RS.Flow.Button.Settings;
            stripButton: RS.Flow.Button.Settings;

            stripButtonActivated?: boolean;

            /** Delay before rapid movement begins after mouse button down. */
            holdDelay: number;
            /** Time between each movement during rapid movement. */
            holdInterval: number;
        }

        export interface RuntimeData
        {
            reel: IGenericReel;
            symbolCount: number;
        }

        export const defaultSettings: Settings =
        {
            holdDelay: 500,
            holdInterval: 50,
            stripButtonActivated: true,

            upButton:
            {
                background: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red },
                hoverbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red.brighter(0.1) },
                pressbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red.darker(0.1) },
                disabledbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red.darker(0.25) },
                textLabel: { dock: Flow.Dock.Fill, sizeToContents: true, font: Fonts.FrutigerWorld, fontSize: 24, text: "↑" },
                sizeToContents: true,
                spacing: Flow.Spacing.all(5),
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Top,

                seleniumId: "RigUp"
            },
            downButton:
            {
                background: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red },
                hoverbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red.brighter(0.1) },
                pressbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red.darker(0.1) },
                disabledbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.red.darker(0.25) },
                textLabel: { dock: Flow.Dock.Fill, sizeToContents: true, font: Fonts.FrutigerWorld, fontSize: 24, text: "↓" },
                sizeToContents: true,
                spacing: Flow.Spacing.all(5),
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Bottom,

                seleniumId: "RigDown"
            },
            valueButton:
            {
                background: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray },
                hoverbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray.brighter(0.1) },
                pressbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray.darker(0.1) },
                disabledbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray.darker(0.25) },
                textLabel: { dock: Flow.Dock.Fill, sizeToContents: true, font: Fonts.FrutigerWorld, fontSize: 24, text: "0" },
                sizeToContents: true,
                spacing: Flow.Spacing.all(5),
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.47 },

                seleniumId: "RigValue"
            },
            stripButton:
            {
                background: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray },
                hoverbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray.brighter(0.1) },
                pressbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray.darker(0.1) },
                disabledbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: Util.Colors.black, borderSize: 2, color: Util.Colors.gray.darker(0.25) },
                textLabel: { dock: Flow.Dock.Fill, sizeToContents: true, font: Fonts.FrutigerWorld, fontSize: 24, text: "strip" },
                sizeToContents: true,
                spacing: Flow.Spacing.all(5),
                expand: Flow.Expand.Disallowed,
                dock: Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.53 }
            }
        };
    }
}