namespace RS.Reels.Cascade
{
    const tmpPoint = new RS.Rendering.Point2D();
    const tmpColor1 = new Util.Color();
    const tmpColor2 = new Util.Color();

    const defaultSymbolOffset = Math.Vector2D();

    export class Reel<TSettings extends Reel.Settings = Reel.Settings> implements IReel<TSettings>
    {
        protected static _shaderProgram: Rendering.SymbolShaderProgram | null;

        public readonly state = new Observable(IReel.State.Stopped);

        /** Published when this reel has been stopped. */
        public readonly onStopped = createEvent<IReel.StopType>();

        /** Published when a symbol has landed. */
        public readonly onSymbolLanded = createEvent<number>();

        /** Published when a scatter has landed on this reel. */
        public readonly onScatterLanded = createEvent<IReel.ScatterLandedData>();

        /** Published when suspense has started for this reel. */
        public readonly onSuspenseStarted = createSimpleEvent();

        /** Published when suspense has ended for this reel. */
        public readonly onSuspenseEnded = createSimpleEvent();

        /** Published when the reel has actually begun to spin down. */
        public readonly onDecelerateStarted = createSimpleEvent();

        public readonly position = new RS.Rendering.Point2D(0, 0);

        protected _isDisposed: boolean = false;

        /** Gets if this reel has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        protected _masterReel: Reel|null = null;

        /** Gets or sets the "master" reel of this reel. */
        @CheckDisposed
        public get masterReel() { return this._masterReel; }
        public set masterReel(value) { this._masterReel = value; }

        protected _symbolsByID: Symbols.Binding[];

        protected _pendingStopSymbols: number[] | null = null;

        protected _oldWindow: Symbols.Window.SymbolData[] = [];
        protected _window: Symbols.Window.SymbolData[] = [];
        protected _landedSymbols: boolean[] = [];
        protected _hiddenSymbols: boolean[] = [];
        protected _heldSymbols: boolean[] = [];

        protected _symbolContainer: RS.Rendering.IContainer;

        protected _windowSymbols: Rendering.StripSymbol[];

        protected _delta: number = 0;
        protected _speed: number = 0;

        protected _tallestSymbol: number;
        protected _maxZIndex: number;
        protected _reelZIndex: number;

        protected _inSuspense: boolean;

        protected _scatterSymbolIDs: number[];

        protected _visible: boolean = true;

        protected _useShaderForSymbols: boolean;
        @AutoDispose protected _shader: Rendering.SymbolShader;

        protected _debugView: Rendering.DebugView;

        protected _tint: Util.Color = Util.Colors.white;

        /** Gets or creates a debug view for this reel. */
        public get debugView()
        {
            if (this._debugView) { return this._debugView; }

            // this._debugView = new Rendering.DebugView({
            //     strip: this._symbolStrip,
            //     window: this._window,
            //     bindingSet: this.settings.symbols,
            //     unitSize: this.settings.symbolSpacing
            // });

            return this._debugView;
        }

        /** Gets or sets the current spin speed of this reel. */
        @CheckDisposed
        public get speed() { return this._speed; }
        public set speed(value) { this._speed = value; }

        /** Gets or sets the current position of this reel. */
        @CheckDisposed
        public get delta() { return this._delta; }
        public set delta(value) { this._delta = value; }

        /** Gets or sets if this reel is visible. */
        @CheckDisposed
        public get visible() { return this._visible; }
        public set visible(value)
        {
            if (value === this._visible) { return; }
            this._visible = value;
            this.updateElements();
        }

        /** Gets or sets the tint color for this reel. */
        @CheckDisposed
        public get tint() { return this._tint; }
        public set tint(value)
        {
            if (value === this._tint) { return; }
            this._tint = value;
            this.updateElements();
        }

        /** Gets or sets the x position of this reel. */
        public get x() { return this.position.x; }
        public set x(value) { this.position.x = value; }

        /** Gets or sets the y position of this reel. */
        public get y() { return this.position.y; }
        public set y(value) { this.position.y = value; }

        /** Gets how long this reel will take to spin down (ms). */
        public get spinDownTime()
        {
            const distance = this.settings.symbolSpacing * (this.settings.stopSymbolCount + 0.5);

            return (Math.Mechanics.solveTime({
                initialSpeed: this.settings.terminalVelocity,
                finalSpeed: this.settings.terminalVelocity,
                displacement: distance
            }) - this.settings.timeOffsetByRow * this.settings.stopSymbolCount)*1000;
        }

        /** Gets how long this reel will take to spin up (ms). */
        public get spinUpTime()
        {
            const distance = this.settings.symbolSpacing * (this.settings.stopSymbolCount + 0.5);

            // Find how long it takes to reach terminal velocity
            const timeUntilTerminalVelocity = Math.Mechanics.solveTime({
                acceleration: this.settings.fallAcceleration,
                initialSpeed: 0,
                finalSpeed: this.settings.terminalVelocity
            });

            const distanceAtTerminalVelocity = Math.Mechanics.solveDisplacement({
                acceleration: this.settings.fallAcceleration,
                initialSpeed: 0,
                time: timeUntilTerminalVelocity
            });

            if (distanceAtTerminalVelocity > distance)
            {
                // We don't reach terminal velocity
                return (Math.Mechanics.solveTime({
                    acceleration: this.settings.fallAcceleration,
                    initialSpeed: 0,
                    displacement: distance
                }) - this.settings.timeOffsetByRow * this.settings.stopSymbolCount) * 1000;
            }
            else
            {
                // We reach terminal velocity
                return (timeUntilTerminalVelocity + Math.Mechanics.solveTime({
                    acceleration: 0,
                    initialSpeed: this.settings.terminalVelocity,
                    displacement: distance
                }) - this.settings.timeOffsetByRow * this.settings.stopSymbolCount) * 1000;
            }
        }

        public get isSuspensing() { return false; }

        constructor(public readonly settings: TSettings, symbolContainer: RS.Rendering.IContainer)
        {
            this._useShaderForSymbols = settings.useShader;

            this._symbolContainer = symbolContainer;

            if (this._useShaderForSymbols)
            {
                if (Reel._shaderProgram == null)
                {
                    // When fade distance is not specified, it defaults to 8 in updateMask(), so fade should be enabled
                    Reel._shaderProgram = new Rendering.SymbolShaderProgram({ useFade: settings.fadeDistance == null || settings.fadeDistance > 0 });
                }
                this._shader = Reel._shaderProgram.createShader();
                this._shader.blurTexture = settings.database.blurSymbolTexture;
            }
            else
            {
                this._shader = null;
            }

            this.regenerateReel();

        }

        /**
         * Gets the symbol definition for the specified ID, based on this reel's symbol bindings.
         */
        public getSymbolDefinition(id: number): Symbols.Definition
        {
            return this._symbolsByID[id] ? this._symbolsByID[id].definition : null;
        }

        /**
         * Gets the symbol ID for the specified definition, based on this reel's symbol bindings.
         */
        public getSymbolID(definition: Symbols.Definition): number | null
        {
            for (let symbolID = 0, l = this._symbolsByID.length; symbolID < l; ++symbolID)
            {
                const symbolData = this._symbolsByID[symbolID];
                if (symbolData != null && symbolData.definition === definition)
                {
                    return symbolID;
                }
            }
            return null;
        }

        /**
         * Sets the symbol bindings
         */
        public setSymbolBindings(bindingSet: Symbols.Binding[])
        {
            this._symbolsByID = Symbols.constructSymbolsByID(bindingSet);
        }

        @CheckDisposed
        public updateMask()
        {
            if (!this._useShaderForSymbols) { return; }

            const fadeDist = this.settings.fadeDistance != null ? this.settings.fadeDistance : 8;

            // Compute location of masks in LOCAL space
            const localTopClip = this.position.y - (this.settings.maskExpandTop || 0) - (this.settings.offset ? this.settings.offset.y : 0);
            const localTopFade = localTopClip + fadeDist;
            const localBotClip = this.position.y + (this.settings.maskExpandBottom || 0) - (this.settings.offset ? this.settings.offset.y : 0) + this.settings.stopSymbolCount * this.settings.symbolSpacing;
            const localBotFade = localBotClip - fadeDist;

            // Transform to world space
            tmpPoint.set(0, localTopClip);
            const worldTopClip = this._symbolContainer.toGlobal(tmpPoint).y;
            tmpPoint.set(0, localTopFade);
            const worldTopFade = this._symbolContainer.toGlobal(tmpPoint).y;
            tmpPoint.set(0, localBotClip);
            const worldBotClip = this._symbolContainer.toGlobal(tmpPoint).y;
            tmpPoint.set(0, localBotFade);
            const worldBotFade = this._symbolContainer.toGlobal(tmpPoint).y;

            // Update in shader
            this._shader.clipRegion =
            {
                x: worldBotClip,
                y: worldTopClip
            };
            this._shader.fadeSize =
            {
                x: worldBotFade,
                y: worldTopFade
            };
        }

        public setStopSymbols(symbolsInView: number[], defer?: boolean): void
        {
            const cnt = Math.min(symbolsInView.length, this._window.length);
            for (let i = 0; i < cnt; ++i)
            {
                const symbolID = symbolsInView[i];
                this.setStopSymbol(i, symbolsInView[i]);
            }
        }

        public setStopSymbol(rowIndex: number, symbolID: number): void
        {
            if (!this.getSymbolDefinition(symbolID)) { throw new Error(`Invalid symbol ID ${symbolID}`); }
            this._window[rowIndex].symbolID = symbolID;
        }

        /**
         * Sets the current strip of the reel.
         */
        @CheckDisposed
        public setStrip(strip: number[]): void
        {
            return;
        }

        public spinUp(startType: IReel.StartType): void
        {
            this._speed = 1.0;
            if (this.state.value !== IReel.State.Stopped)
            {
                throw new Error("Can't spin up reels when they aren't stopped");
            }
            this._delta = 0;
            switch (startType)
            {
                case IReel.StartType.Standard:
                    this.state.value = IReel.State.SpinningUp;
                    break;
                case IReel.StartType.Immediate:
                    this.state.value = IReel.State.Spinning;
                    break;
            }
            for (const symbol in this._landedSymbols)
            {
                this._landedSymbols[symbol] = false;
            }
            const rowCount = this._window.length;
            for (let i = 0; i < rowCount; ++i)
            {
                const oldSymbolData = this._oldWindow[i];
                const newSymbolData = this._window[i];

                oldSymbolData.symbolID = newSymbolData.symbolID;
                oldSymbolData.opacity = newSymbolData.opacity;
                oldSymbolData.tint = newSymbolData.tint;

                if (this._pendingStopSymbols) { newSymbolData.symbolID = this._pendingStopSymbols[i]; }
            }
        }

        public tick(delta: number): void
        {
            // What state are we in?
            switch (this.state.value)
            {
                case IReel.State.Stopped:
                case IReel.State.Spinning:
                    this._delta = 0;
                    break;

                case IReel.State.SpinningUp:
                    this._delta += this._speed * (delta / 1000);
                    if (this._delta > this.spinUpTime/1000)
                    {
                        this.state.value = IReel.State.Spinning;
                    }

                    break;
                case IReel.State.SlowingDown:
                    this._delta += this._speed * (delta / 1000);
                    if (this._delta > this.spinDownTime/1000)
                    {
                        this.state.value = IReel.State.Stopped;
                        this.onStopped.publish(IReel.StopType.Standard);
                    }

                    break;
            }

            this.updateElements();
        }

        public spinDown(stopType: IReel.StopType): void
        {
            for (const symbol in this._landedSymbols)
            {
                this._landedSymbols[symbol] = false;
            }
            // Drop any held rows
            let heldCount = 0;
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                if (this._heldSymbols[i]) { ++heldCount; }
            }
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this._heldSymbols[i] = (l - i) <= heldCount;
            }

            this._speed = 1.0;
            switch (stopType)
            {
                case IReel.StopType.Standard:
                case IReel.StopType.Quick:
                case IReel.StopType.Turbo:

                    if (this.state.value !== IReel.State.Spinning &&
                        this.state.value !== IReel.State.SlowingDown)
                    {
                        throw new Error("Can't spin down reels when they aren't spinning");
                    }

                    this._delta = 0;
                    this.state.value = IReel.State.SlowingDown;

                    this.onDecelerateStarted.publish();

                    break;

                case IReel.StopType.Immediate:

                    this._delta = 0;
                    this.state.value = IReel.State.Stopped;
                    this.onStopped.publish(IReel.StopType.Immediate);

                    break;
            }
        }

        public hideSymbol(rowIndex: number): void
        {
            this._hiddenSymbols[rowIndex] = true;
        }
        public showSymbol(rowIndex: number): void
        {
            this._hiddenSymbols[rowIndex] = false;
        }
        public setSymbolOpacity(rowIndex: number, opacity: number): void
        {
            this._oldWindow[rowIndex].opacity = this._window[rowIndex].opacity = opacity;
        }
        public setSymbolColor(rowIndex: number, color: Util.Color): void
        {
            this._oldWindow[rowIndex].tint = this._window[rowIndex].tint = color;
        }

        /**
         * Holds the specified row. It won't spin with the rest of the reels.
         * @param row
         */
        public holdRow(row: number): void
        {
            this._heldSymbols[row] = true;
        }

        /**
         * Holds the specified rows. They won't spin with the rest of the reels.
         * @param rows
         */
        public holdRows(rows: ReadonlyArray<number>): void
        {
            for (const row of rows)
            {
                this.holdRow(row);
            }
        }

        /** Holds all positions. */
        public holdAllRows(): void
        {
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this.holdRow(i);
            }
        }

        /**
         * Unholds the specified row.
         * @param row
         */
        public unholdRow(row: number): void
        {
            this._heldSymbols[row] = false;
        }

        /**
         * Unholds the specified rows.
         * @param rows
         */
        public unholdRows(rows: ReadonlyArray<number>): void
        {
            for (const row of rows)
            {
                this.unholdRow(row);
            }
        }

        /**
         * Unholds all rows.
         */
        public unholdAllRows(): void
        {
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this.unholdRow(i);
            }
        }

        public getSymbolPosition(rowIndex: number): Math.Vector2D
        {
            return {
                x: Math.round(this.settings.width / 2),
                y: Math.round((rowIndex * this.settings.symbolSpacing) + this.settings.symbolSpacing / 2)
            };
        }
        public findSymbolStart(rowIndex: number): number
        {
            return rowIndex;
        }
        public nudge(direction: "up" | "down", duration: number, ease?: (num: number) => number): PromiseLike<void>
        {
            throw new Error("Method not implemented.");
        }
        public nudgeBy(spaces: number, duration: number, ease?: (num: number) => number): PromiseLike<void>
        {
            throw new Error("Method not implemented.");
        }
        public realignWindow(): void
        {
            throw new Error("Method not implemented.");
        }
        public beginSuspense(): void
        {
            throw new Error("Method not implemented.");
        }
        public endSuspense(): void
        {
            throw new Error("Method not implemented.");
        }

        /**
         * Destroys this reel.
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }

            RS.Tween.removeTweens<Reel>(this);

            for (let i = 0; i < this._windowSymbols.length; i++)
            {
                const symbol = this._windowSymbols[i];
                if (symbol.visible)
                {
                    this._symbolContainer.removeChild(symbol);
                }
            }
            this._windowSymbols.length = 0;
            this._windowSymbols = null;

            this._pendingStopSymbols = null;

            this._isDisposed = true;
        }

        protected regenerateReel(): void
        {
            // Delete existing symbols
            if (this._windowSymbols)
            {
                for (const sym of this._windowSymbols)
                {
                    this._symbolContainer.removeChild(sym);
                }
                this._windowSymbols.length = 0;
                this._windowSymbols = null;
            }

            this._pendingStopSymbols = null;
            if (this.settings.offset)
            {
                this.position.set(this.settings.offset.x, this.settings.offset.y);
            }

            // Compute metrics
            this._tallestSymbol = 0;
            this._maxZIndex = 0;
            this._scatterSymbolIDs = [];
            for (const binding of this.settings.symbols)
            {
                const selectionChance = binding.selectionChance != null ? binding.selectionChance : 1;
                this._tallestSymbol = Math.max(this._tallestSymbol, binding.definition.span || 1);
                this._maxZIndex = Math.max(this._maxZIndex, binding.definition.zIndex || 0);

                if (binding.definition.isScatter)
                {
                    this._scatterSymbolIDs.push(binding.id);
                }
            }
            this._symbolsByID = Symbols.constructSymbolsByID(this.settings.symbols);

            // Create new ones
            const windowSymbolCount = this.settings.stopSymbolCount * 2;
            this._windowSymbols = new Array<Rendering.StripSymbol>(windowSymbolCount);
            this._landedSymbols = new Array<boolean>(windowSymbolCount);
            for (let i = 0; i < windowSymbolCount; ++i)
            {
                const stripSymbol = new (this.settings.stripSymbolClass || Rendering.StripSymbol)(this.settings.database);
                stripSymbol.visible = false;
                if (this._useShaderForSymbols) { stripSymbol.shader = this._shader; }
                this._symbolContainer.addChild(stripSymbol);
                this._windowSymbols[i] = stripSymbol;
                this._landedSymbols[i] = false;
            }

            // Set window size(s)
            const stopSymbolCount = this.settings.stopSymbolCount;
            this._window.length = stopSymbolCount;
            this._oldWindow.length = stopSymbolCount;
            this._heldSymbols.length = stopSymbolCount;
            for (let i = 0; i < stopSymbolCount; ++i)
            {
                this._window[i] = { symbolID: 0, tint: Util.Colors.white, opacity: 1.0 };
                this._oldWindow[i] = { symbolID: 0, tint: Util.Colors.white, opacity: 1.0 };
                this._heldSymbols[i] = false;
            }

            this.updateElements();
        }

        protected updateElements(): void
        {
            // If we're not visible, hide everything and early-out
            if (!this._visible)
            {
                for (const sym of this._windowSymbols)
                {
                    sym.visible = false;
                }
                return;
            }

            // Iterate all symbols
            for (let i = 0, l = this._windowSymbols.length; i < l; ++i)
            {
                const sym = this._windowSymbols[i];
                sym.tint = this._tint;

                // The window symbols are split into two halves, top and bottom
                // Top half drops in whilst bottom half drops out
                const isTopHalf = i < this.settings.stopSymbolCount;

                // What state are we in?
                switch (this.state.value)
                {
                    // Reels are stopped, top half is showing
                    case IReel.State.Stopped:
                    {
                        if (isTopHalf)
                        {
                            const rowIndex = i;
                            const symbolData = this._window[rowIndex];
                            const symbolDef = this.getSymbolDefinition(symbolData.symbolID);
                            const x = this.settings.width * 0.5;
                            const y = this.settings.symbolSpacing * (rowIndex + 0.5);
                            const offset = symbolDef.offset || defaultSymbolOffset;
                            sym.x = this.position.x + offset.x + x;
                            sym.y = this.position.y + offset.y + y;
                            sym.alpha = symbolData.opacity;
                            sym.tint = symbolData.tint;
                            sym.visible = !this._hiddenSymbols[rowIndex];
                            sym.setSymbol(symbolDef, true, i);
                        }
                        else
                        {
                            sym.visible = false;
                        }

                        break;
                    }

                    // Old symbols (bottom half) are dropping out
                    case IReel.State.SpinningUp:
                    {
                        if (isTopHalf)
                        {
                            sym.visible = false;
                        }
                        else
                        {
                            const rowIndex = i - this.settings.stopSymbolCount;
                            const timeOffset = this._delta + this.settings.timeOffsetByRow * (this.settings.stopSymbolCount - rowIndex - 1);
                            const symbolData = this._window[rowIndex];
                            const symbolDef = this.getSymbolDefinition(symbolData.symbolID);
                            const isHeld = this._heldSymbols[rowIndex];

                            let fallOffset = this.getFallOffset(Math.max(0.0, timeOffset));
                            if (isHeld)
                            {
                                let fallRows = 0;
                                for (let j = rowIndex + 1; j < this.settings.stopSymbolCount; ++j)
                                {
                                    if (!this._heldSymbols[j]) { ++fallRows; }
                                }
                                fallOffset = Math.min(fallOffset, fallRows * this.settings.symbolSpacing);
                                // check for landed symbol
                                if (fallOffset === fallRows * this.settings.symbolSpacing
                                    && fallRows > 0
                                    && !this._hiddenSymbols[rowIndex]
                                    && !this._landedSymbols[rowIndex])
                                {
                                    this._landedSymbols[rowIndex] = true;
                                    this.onSymbolLanded.publish(rowIndex + fallRows);
                                    if (this._scatterSymbolIDs.indexOf(symbolData.symbolID) !== -1)
                                    {
                                        this.onScatterLanded.publish({rowIndex: rowIndex + fallRows, stopType: 0});
                                    }
                                }
                            }

                            const x = this.settings.width * 0.5;
                            const y = this.settings.symbolSpacing * (rowIndex + 0.5) + fallOffset;
                            const offset = symbolDef.offset || defaultSymbolOffset;
                            sym.x = this.position.x + offset.x + x;
                            sym.y = this.position.y + offset.y + y;
                            sym.alpha = symbolData.opacity;
                            sym.tint = symbolData.tint;
                            sym.visible = !this._hiddenSymbols[rowIndex];
                            sym.setSymbol(symbolDef, true, rowIndex);
                        }
                        break;
                    }

                    // Nothing is happening
                    case IReel.State.Spinning:
                    {
                        if (isTopHalf)
                        {
                            sym.visible = false;
                        }
                        else
                        {
                            const rowIndex = i - this.settings.stopSymbolCount;
                            const isHeld = this._heldSymbols[rowIndex];

                            if (isHeld)
                            {
                                const symbolData = this._oldWindow[rowIndex];
                                const symbolDef = this.getSymbolDefinition(symbolData.symbolID);

                                let fallRows = 0;
                                for (let j = rowIndex + 1; j < this.settings.stopSymbolCount; ++j)
                                {
                                    if (!this._heldSymbols[j]) { ++fallRows; }
                                }

                                const x = this.settings.width * 0.5;
                                const y = this.settings.symbolSpacing * (rowIndex + 0.5) + fallRows * this.settings.symbolSpacing;
                                const offset = symbolDef.offset || defaultSymbolOffset;
                                sym.x = this.position.x + offset.x + x;
                                sym.y = this.position.y + offset.y + y;
                                sym.alpha = symbolData.opacity;
                                sym.tint = symbolData.tint;
                                sym.visible = !this._hiddenSymbols[rowIndex];
                                sym.setSymbol(symbolDef, true, rowIndex);
                            }
                            else
                            {
                                sym.visible = false;
                            }
                        }
                        break;
                    }

                    // New symbols (top half) are dropping in
                    case IReel.State.SlowingDown:
                    {
                        if (isTopHalf)
                        {
                            const rowIndex = i;
                            const isHeld = this._heldSymbols[rowIndex];
                            const symbolData = this._window[rowIndex];

                            let offset: number;
                            if (isHeld)
                            {
                                offset = 0;
                            }
                            else
                            {
                                const timeOffset = this._delta + this.settings.timeOffsetByRow * (this.settings.stopSymbolCount - rowIndex - 1);
                                const initialOffset = this.settings.symbolSpacing * (this.settings.stopSymbolCount + 0.5) * -1;
                                offset = Math.min(0, initialOffset + timeOffset * this.settings.terminalVelocity);
                                // check for landed symbol
                                if (offset === 0 && !this._landedSymbols[rowIndex])
                                {
                                    this._landedSymbols[rowIndex] = true;
                                    this.onSymbolLanded.publish(rowIndex);
                                    if (this._scatterSymbolIDs.indexOf(symbolData.symbolID) !== -1)
                                    {
                                        this.onScatterLanded.publish({rowIndex: rowIndex, stopType: 0});
                                    }
                                }
                            }

                            const symbolDef = this.getSymbolDefinition(symbolData.symbolID);
                            const symbolOffset = symbolDef.offset || defaultSymbolOffset;
                            const x = this.settings.width * 0.5;
                            const y = this.settings.symbolSpacing * (rowIndex + 0.5) + offset;
                            sym.x = this.position.x + symbolOffset.x + x;
                            sym.y = this.position.y + symbolOffset.y + y;
                            sym.alpha = symbolData.opacity;
                            sym.tint = symbolData.tint;
                            sym.visible = !this._hiddenSymbols[rowIndex];
                            sym.setSymbol(symbolDef, true, rowIndex);
                        }
                        else
                        {
                            sym.visible = false;
                        }

                        break;
                    }
                }
            }
        }

        protected getFallOffset(time: number): number
        {
            // Find how long it takes to reach terminal velocity
            const timeUntilTerminalVelocity = Math.Mechanics.solveTime({
                acceleration: this.settings.fallAcceleration,
                initialSpeed: 0,
                finalSpeed: this.settings.terminalVelocity
            });

            if (timeUntilTerminalVelocity < time)
            {
                // We reach terminal velocity
                return Math.Mechanics.solveDisplacement({
                    initialSpeed: 0,
                    finalSpeed: this.settings.terminalVelocity,
                    acceleration: this.settings.fallAcceleration
                }) + Math.Mechanics.solveDisplacement({
                    initialSpeed: this.settings.terminalVelocity,
                    finalSpeed: this.settings.terminalVelocity,
                    time: time - timeUntilTerminalVelocity
                });
            }
            else
            {
                // We don't reach terminal velocity
                return Math.Mechanics.solveDisplacement({
                    initialSpeed: 0,
                    acceleration: this.settings.fallAcceleration,
                    time
                });
            }
        }
    }

    export namespace Reel
    {
        export interface Settings extends IReel.Settings
        {
            /** How much to accelerate a symbol due to "gravity" (pixels per second). */
            fallAcceleration: number;

            /** Terminal velocity of a falling symbol. */
            terminalVelocity: number;

            /** How much time to add to the delta per row. This causes symbols to fall away one after another rather than all at the same time. */
            timeOffsetByRow: number;
        }
    }
}
