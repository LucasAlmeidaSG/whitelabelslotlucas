/// <reference path="IReel.ts" />

namespace RS.Reels
{
    const tmpPoint = new RS.Rendering.Point2D();
    const tmpColor1 = new Util.Color();
    const tmpColor2 = new Util.Color();

    const defaultSymbolOffset = Math.Vector2D();

    export class Reel<TSettings extends Reel.Settings = Reel.Settings> implements IReel<TSettings>
    {
        protected static _shaderProgram: Rendering.SymbolShaderProgram | null;

        public readonly state = new Observable(IReel.State.Stopped);

        /** Published when this reel has been stopped. */
        public readonly onStopped = createEvent<IReel.StopType>();

        /** Published when a symbol has landed. */
        public readonly onSymbolLanded = createEvent<number>();

        /** Published when a scatter has landed on this reel. */
        public readonly onScatterLanded = createEvent<IReel.ScatterLandedData>();

        /** Published when suspense has started for this reel. */
        public readonly onSuspenseStarted = createSimpleEvent();

        /** Published when suspense has ended for this reel. */
        public readonly onSuspenseEnded = createSimpleEvent();

        /** Published when the reel has actually begun to spin down. */
        public readonly onDecelerateStarted = createSimpleEvent();

        public readonly position = new RS.Rendering.Point2D(0, 0);

        protected _isDisposed: boolean = false;

        /** Gets if this reel has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        protected _stopIndex: number | null;

        protected _masterReel: Reel | null = null;

        /**
         * Returns the correct border size for the reels making sure that it's always large enough for any spanning symbols.
         * Also allow there to be no border for instant reveal games. This gets set in the constructor, and takes the bordersize setting into account.
         */
        protected _borderSize: number;

        /** Gets or sets the "master" reel of this reel. */
        @CheckDisposed
        public get masterReel() { return this._masterReel; }
        public set masterReel(value)
        {
            this._masterReel = value;
            if (value == null) { return; }
            this._delta = value._delta;
            this._window.position = value._window.position;
        }

        protected _symbolsByID: Symbols.Binding[];
        protected _selectionChanceSum: number;

        @AutoDispose protected _symbolStrip: Symbols.Strip;
        @AutoDispose protected _window: Symbols.Window;

        protected _pendingStopSymbols: number[] | null = null;
        protected _expectedStopSymbols: number[] | null = null;
        protected _stopSymbolsCommitted: boolean = false;

        protected readonly _isStripValid = new Observable(true);
        protected _pendingStrip: number[] | null = null;
        protected _stripData: number[] | null = null;

        protected _cleanNeeded: boolean;

        protected _currentStripIndex: number = 0;
        protected _delta: number = 0;
        protected _endDelta: number = 0;
        protected _autoDelta: boolean = false;
        protected _motionBlur: number = 0;
        protected _speed: number = 0;
        protected _windowVisible: boolean = false;

        protected _symbolContainer: RS.Rendering.IContainer;
        protected _windowSymbols: Rendering.StripSymbol[];

        protected _sceneRoot: RS.Rendering.Scene.ISceneObject;
        protected _sceneSymbols: Rendering.StripModel[];

        protected _tallestSymbol: number;
        protected _maxZIndex: number;
        protected _reelZIndex: number;

        protected _inSuspense: boolean;

        protected _scatterSymbolIDs: number[];

        protected _visible: boolean = true;

        protected _useShaderForSymbols: boolean;

        @AutoDispose protected _shader: Rendering.SymbolShader;

        @AutoDispose protected _debugView: Rendering.DebugView;

        protected _tint: Util.Color = Util.Colors.white;

        protected get logger() { return Logging.ILogger.get().getContext("Reels"); }

        /** Gets or creates a debug view for this reel. */
        public get debugView()
        {
            if (this._debugView) { return this._debugView; }

            this._debugView = new Rendering.DebugView({
                strip: this._symbolStrip,
                window: this._window,
                bindingSet: this.settings.symbols,
                unitSize: this.settings.symbolSpacing
            });

            return this._debugView;
        }

        /** Gets or sets the current spin speed of this reel. */
        @CheckDisposed
        public get speed() { return this._speed; }
        public set speed(value) { this._speed = value; }

        /** Gets or sets the current position of this reel. */
        @CheckDisposed
        public get delta() { return this._delta; }
        public set delta(value) { this._delta = value; }

        /** Gets or sets if this reel is visible. */
        @CheckDisposed
        public get visible() { return this._visible; }
        public set visible(value)
        {
            if (value === this._visible) { return; }
            this._visible = value;
            this.updateElements();
        }

        /** Gets or sets the tint color for this reel. */
        @CheckDisposed
        public get tint() { return this._tint; }
        public set tint(tint) { this._tint = tint; }

        /** Gets or sets the x position of this reel. */
        public get x() { return this.position.x; }
        public set x(value) { this.position.x = value; }

        /** Gets or sets the y position of this reel. */
        public get y() { return this.position.y; }
        public set y(value) { this.position.y = value; }

        /** Gets how long this reel will take to spin down (ms). */
        public get spinDownTime()
        {
            return (this.settings.deceleration.time || 0) + (this.settings.overhang.time || 0);
        }

        /** Gets if this reel is in suspense or not. */
        public get isSuspensing() { return this._inSuspense; }

        constructor(public readonly settings: TSettings, symbolContainer: RS.Rendering.IContainer, sceneRoot?: RS.Rendering.Scene.ISceneObject)
        {
            this._useShaderForSymbols = settings.useShader;

            this._symbolContainer = symbolContainer;
            this._sceneRoot = sceneRoot;

            if (this._useShaderForSymbols)
            {
                if (Reel._shaderProgram == null)
                {
                    // When fade distance is not specified, it defaults to 8 in updateMask(), so fade should be enabled
                    Reel._shaderProgram = new Rendering.SymbolShaderProgram({ useFade: settings.fadeDistance == null || settings.fadeDistance > 0 });
                }
                this._shader = Reel._shaderProgram.createShader();
                this._shader.blurTexture = settings.database.blurSymbolTexture;
            }
            else
            {
                this._shader = null;
            }

            this.regenerateReel();
        }

        /**
         * Gets the symbol definition for the specified ID, based on this reel's symbol bindings.
         */
        public getSymbolDefinition(id: number): Symbols.Definition
        {
            return this._symbolsByID[id] ? this._symbolsByID[id].definition : null;
        }

        /**
         * Gets the symbol ID for the specified definition, based on this reel's symbol bindings.
         */
        public getSymbolID(definition: Symbols.Definition): number | null
        {
            for (let symbolID = 0, l = this._symbolsByID.length; symbolID < l; ++symbolID)
            {
                const symbolData = this._symbolsByID[symbolID];
                if (symbolData != null && symbolData.definition === definition)
                {
                    return symbolID;
                }
            }
            return null;
        }

        /**
         * Sets the symbol bindings
         */
        public setSymbolBindings(bindingSet: Symbols.Binding[])
        {
            this._symbolsByID = Symbols.constructSymbolsByID(bindingSet);
            this._window.bindingSet = bindingSet;
        }

        @CheckDisposed
        public updateMask()
        {
            if (!this._useShaderForSymbols) { return; }

            const fadeDist = this.settings.fadeDistance != null ? this.settings.fadeDistance : 8;

            // Compute location of masks in LOCAL space
            const localTopClip = this.position.y - (this.settings.maskExpandTop || 0) - (this.settings.offset ? this.settings.offset.y : 0);
            const localTopFade = localTopClip + fadeDist;
            const localBotClip = this.position.y + (this.settings.maskExpandBottom || 0) - (this.settings.offset ? this.settings.offset.y : 0) + this.settings.stopSymbolCount * this.settings.symbolSpacing;
            const localBotFade = localBotClip - fadeDist;

            // Transform to world space
            tmpPoint.set(0, localTopClip);
            const worldTopClip = this._symbolContainer.toGlobal(tmpPoint, null, true).y;
            tmpPoint.set(0, localTopFade);
            const worldTopFade = this._symbolContainer.toGlobal(tmpPoint, null, true).y;
            tmpPoint.set(0, localBotClip);
            const worldBotClip = this._symbolContainer.toGlobal(tmpPoint, null, true).y;
            tmpPoint.set(0, localBotFade);
            const worldBotFade = this._symbolContainer.toGlobal(tmpPoint, null, true).y;

            // Update in shader
            this._shader.clipRegion =
                {
                    x: worldBotClip,
                    y: worldTopClip
                };
            this._shader.fadeSize =
                {
                    x: worldBotFade,
                    y: worldTopFade
                };
        }

        /**
         * Sets the stop symbols.
         * Should only be called when the reels are not moving.
         * Defer flag can be used to defer the setting, needed to make suspense work with quick skipping
         * @param symbolsInView
         * @param defer
         */
        @CheckDisposed
        public setStopSymbols(symbolsInView: number[], defer?: boolean, stopIndex?: number): void
        {
            this._stopIndex = stopIndex;

            // Validate
            for (const symbolID of symbolsInView)
            {
                if (!this.getSymbolDefinition(symbolID))
                {
                    throw new Error(`Reel.setStopSymbols: symbol ID '${symbolID}' invalid`);
                }
            }

            // Clone & modify
            symbolsInView = symbolsInView.slice(0);

            // Set the pending stop symbols, their positions to be determined when stop is called
            this._pendingStopSymbols = symbolsInView;
            this._expectedStopSymbols = symbolsInView;

            if (this.state.value === IReel.State.Stopped && !defer)
            {
                // Reels stopped, immediately set symbols
                this.commitPendingStopSymbols(this._currentStripIndex);
                this._pendingStopSymbols = null;
            }

            this._stopSymbolsCommitted = false;
        }

        /**
         * Sets a symbol in the visible window.
         * Will NOT consider any intersections or resolve multi-spanning symbols.
         * @param rowIndex
         * @param symbolID
         */
        @CheckDisposed
        public setStopSymbol(rowIndex: number, symbolID: number): void
        {
            if (rowIndex < -this._window.borderSize || rowIndex >= this._window.size + this._window.borderSize)
            {
                throw new Error("Can't set a symbol outside of the window!");
            }
            this._window.set(rowIndex, symbolID);
        }

        /**
         * Sets the current strip of the reel.
         */
        @CheckDisposed
        public setStrip(strip: number[]): void
        {
            if (this._symbolStrip.equals(strip)) { return; }
            if (this.state.value !== IReel.State.Stopped)
            {
                if (!this.getWindowIsVisible() && !this._stopSymbolsCommitted)
                {
                    this.transitionToStrip(strip);
                    return;
                }

                this._pendingStrip = strip;
                this._isStripValid.value = false;
                return;
            }
            this._pendingStrip = null;
            this.internalSetStrip(strip);
        }

        /**
         * Starts the reel spinning.
         */
        @CheckDisposed
        public async spinUp(startType: IReel.StartType)
        {
            if (startType === IReel.StartType.Immediate)
            {
                this.quickStart();
                return;
            }
            if (this.state.value !== IReel.State.Stopped)
            {
                throw new Error("Cannot start a reel which hasn't stopped");
            }

            this._motionBlur = 0;
            this._endDelta = this._delta;

            this.state.value = IReel.State.SpinningUp;
            this._speed = this.settings.windup.distance;
            this._autoDelta = true;
            this._cleanNeeded = true;
            this._inSuspense = false;

            // accelerate
            await RS.Tween.get<Reel>(this)
                .to({ speed: this.settings.topSpeed.speed }, this.settings.topSpeed.time, this.settings.topSpeed.ease);

            if (this.state.value === IReel.State.SpinningUp)
            {
                this.state.value = IReel.State.Spinning;
                if (this._pendingStrip)
                {
                    this.setStrip(this._pendingStrip);
                }
            }
        }

        /**
         * Performs a tick on this reel.
         * @param delta     Duration of the tick in ms.
         */
        @CheckDisposed
        public tick(delta: number): void
        {
            // Check window visibility
            const windowVisible = this.getWindowIsVisible();
            if (this._windowVisible && !windowVisible)
            {
                this._windowVisible = false;
                this.handleWindowExitView();
            }
            else if (windowVisible && !this._windowVisible)
            {
                this._windowVisible = true;
                this.handleWindowEnterView();
            }

            const deltaTime = delta / 1000;
            // If we're not stopped, integrate
            if (this._autoDelta)
            {
                this._delta += this._speed * deltaTime;
            }

            // Calculate motion blur
            if (this.settings.useMotionBlur)
            {
                if (this._speed >= this.settings.minimumBlurSpeed)
                {
                    this._motionBlur = Math.clamp(((this._speed - this.settings.minimumBlurSpeed) / this.settings.topSpeed.speed) * this.settings.blurMultiplier, 0, 1);
                }
                else
                {
                    this._motionBlur = 0.0;
                }
            }

            // this.updateMask(0, this._reelProperties.overrideHeight);
            this.updateElements();

            if (this._debugView)
            {
                this._debugView.delta = this._delta;
            }
        }

        /**
         * Requests that this reel spins down. Returns a promise that resolves when the reel is ready to start stopping, if it wasn't at the time the method was called.
         * @param quickstop - if true, the reel will quickstop (will not wait before stopping)
         */
        @CheckDisposed
        public async spinDown(stopType: IReel.StopType): Promise<void>
        {
            if (stopType === IReel.StopType.Immediate)
            {
                this.stopNow();
                return;
            }
            if (this.state.value !== IReel.State.Spinning) { return; }
            const logger = this.logger;
            logger.queue("Waiting for valid strip.");

            // Wait for pending strip to be committed before trying to stop.
            await this._isStripValid.for(true);
            if (this.state.value !== IReel.State.Spinning) { return; }
            const quickStop = (stopType === IReel.StopType.Quick || stopType === IReel.StopType.Turbo);
            logger.queue("Spin down started. quickStop:", quickStop);

            const spinDownDelay = this._inSuspense ? (this.settings.suspenseSpinDownDelay || 0) : (this.settings.spinDownDelay || 0);
            // Calculate the stopping distance using kinematics (d = ((v0 + v1) / 2) * t)
            let distanceAfterStop = Math.round((this._speed / 2 * (this.settings.deceleration.time / 1000)));
            distanceAfterStop = quickStop ? distanceAfterStop : distanceAfterStop + (this._speed * (spinDownDelay / 1000));
            // Where we would stop, according to the above
            const stopPosition = this._delta + distanceAfterStop;

            /** If we stopped now, how much would the symbol be offset on the reel after stopping? */
            const symbolOffset = Math.wrap(stopPosition, 0, this.settings.symbolSpacing);
            const timeToWait = quickStop ? 0 : Math.round((symbolOffset / this._speed) * 1000) + spinDownDelay; // how long do we keep the reel spinning to negate that offset

            // the position if we were to start stopping right now
            this._endDelta = stopPosition - symbolOffset;
            logger.queue("Stop position calculated. stoppingDistance:", distanceAfterStop, "symboloffset:", symbolOffset, "timeToWait:", timeToWait, "endDelta:", this._endDelta);

            // set stop symbols at stop index
            let extraTimeToWait = 0;
            if (this._pendingStopSymbols !== null)
            {
                const stopSymbolIndex = this.wrapStopSymbolIndex(this.deltaToStripIndex(this._endDelta));
                const validPos = this.discoverNextValidWindowPosition(stopSymbolIndex, this._pendingStopSymbols);
                /** Symbol count between current valid pos and stop index. */
                let aheadAmount: number;
                if (validPos <= stopSymbolIndex)
                {
                    aheadAmount = stopSymbolIndex - validPos;
                }
                else
                {
                    aheadAmount = this._symbolStrip.length - (validPos - stopSymbolIndex);
                }
                // Time until the reel has reached the stop index.
                extraTimeToWait = (aheadAmount * this.settings.symbolSpacing) / this._speed;
                // Pixels to travel from now to stop index.
                this._endDelta += aheadAmount * this.settings.symbolSpacing;

                this.commitPendingStopSymbols(validPos);
                logger.queue("Pending stop symbols committed. validPos:", validPos, "aheadAmount:", aheadAmount, "extraTimeToWait:", extraTimeToWait, "endDelta:", this._endDelta);
            }

            const overhangDelta = this._endDelta + this.settings.overhang.distance;

            logger.queue("Waiting to decelerate. Time:", timeToWait + extraTimeToWait * 1000);
            RS.Tween.get<Reel>(this, { override: true })
                .wait(timeToWait + extraTimeToWait * 1000)
                .call(() =>
                {
                    if (this.state.value !== IReel.State.Spinning) { return; }
                    logger.queue("Starting deceleration.");
                    this.state.value = IReel.State.SlowingDown;
                    this.onDecelerateStarted.publish();
                    this._autoDelta = false;
                    RS.Tween.get<Reel>(this, { override: true })
                        .to({ delta: overhangDelta, speed: 0 }, this.settings.deceleration.time, this.settings.deceleration.ease)
                        .to({ delta: this._endDelta }, this.settings.overhang.time, this.settings.overhang.ease)
                        .call(() =>
                        {
                            this.onStop(quickStop);
                        });
                });
        }

        /**
         * Hides a stop symbol at the given row index
         * @param row
         */
        @CheckDisposed
        public hideSymbol(rowIndex: number): void
        {
            this.setSymbolOpacity(rowIndex, 0.0);
        }

        /**
         * Unhides a stop symbol at the given row index
         * @param row
         */
        @CheckDisposed
        public showSymbol(rowIndex: number): void
        {
            this.setSymbolOpacity(rowIndex, 1.0);
        }

        /**
         * Sets the opacity of the symbol at the given location.
         * @param rowIndex Row index of the symbol, relative to the window position.
         * @param opacity Opacity of the symbol (0-1).
         */
        public setSymbolOpacity(rowIndex: number, opacity: number): void
        {
            const symbolData = this._window.get(rowIndex);
            const symbolDef = this._symbolsByID[symbolData.symbolID].definition;
            const span = symbolDef.spanSplit ? 1 : (symbolDef.span || 1);
            if (span === 1)
            {
                this._window.set(rowIndex, symbolData.symbolID, opacity, symbolData.tint);
                this.updateElements();
                return;
            }
            const start = this.findSymbolStart(rowIndex);
            const tmp = new Array<number>(span);
            for (let i = 0, l = tmp.length; i < l; ++i)
            {
                tmp[i] = symbolData.symbolID;
            }
            this._window.set(start, tmp, opacity, symbolData.tint);
            this.updateElements();
        }

        /**
         * Sets the color of the symbol at the given location.
         * @param rowIndex Row index of the symbol, relative to the window position.
         * @param color Color of the symbol.
         */
        public setSymbolColor(rowIndex: number, color: Util.Color): void
        {
            const symbolData = this._window.get(rowIndex);
            const symbolDef = this._symbolsByID[symbolData.symbolID].definition;
            const span = symbolDef.spanSplit ? 1 : (symbolDef.span || 1);
            if (span === 1)
            {
                this._window.set(rowIndex, symbolData.symbolID, symbolData.opacity, color);
                this.updateElements();
                return;
            }
            const start = this.findSymbolStart(rowIndex);
            const tmp = new Array<number>(span);
            for (let i = 0, l = tmp.length; i < l; ++i)
            {
                tmp[i] = symbolData.symbolID;
            }
            this._window.set(start, tmp, symbolData.opacity, color);
            this.updateElements();
        }

        /**
         * Holds the specified row. It won't spin with the rest of the reels.
         * @param row
         */
        public holdRow(row: number): void
        {
            throw new Error("Method not implemented.");
        }

        /**
         * Holds the specified rows. They won't spin with the rest of the reels.
         * @param rows
         */
        public holdRows(rows: ReadonlyArray<number>): void
        {
            for (const row of rows)
            {
                this.holdRow(row);
            }
        }

        /** Holds all positions. */
        public holdAllRows(): void
        {
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this.holdRow(i);
            }
        }

        /**
         * Unholds the specified row.
         * @param row
         */
        public unholdRow(row: number): void
        {
            throw new Error("Method not implemented.");
        }

        /**
         * Unholds the specified rows.
         * @param rows
         */
        public unholdRows(rows: ReadonlyArray<number>): void
        {
            for (const row of rows)
            {
                this.unholdRow(row);
            }
        }

        /**
         * Unholds all rows.
         */
        public unholdAllRows(): void
        {
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this.unholdRow(i);
            }
        }

        /**
         * Gets the position (center) of where a symbol would be drawn given the provided row, in reel space (where 0,0
         * is the top left of the reel).
         * @return createjs.Point - x,y coordinates of the symbol on this row, given as its center (e.g. the top symbol
         * on a reel which is 100px wide and 300px tall would be at 50,50)
         */
        @CheckDisposed
        public getSymbolPosition(row: number): Math.Vector2D
        {
            return {
                x: Math.round(this.settings.width / 2),
                y: Math.round((row * this.settings.symbolSpacing) + this.settings.symbolSpacing / 2)
            };
        }

        /**
         * Given the index of the specified symbol (in stop symbol coordinates), finds the row index that STARTS the symbol.
         * If the symbol's span is 1, this function will just return the passed in row.
         * If the symbol's span is > 1, this function will find the top-most point of the symbol.
         * Searches the entire symbol strip including the window overlay.
         * @param row   The row of the symbol to identify
         * @returns     The row of the symbol's start, in stop symbol coordinates
         */
        @CheckDisposed
        public findSymbolStart(rowIndex: number): number
        {
            const stopIndex = this._currentStripIndex + rowIndex;
            const symbolID = this.getSymbolAtIndex(stopIndex).symbolID;
            const symbolDef = this._symbolsByID[symbolID].definition;
            const span = symbolDef.spanSplit ? 1 : (symbolDef.span || 1);
            if (span === 1) { return rowIndex; }
            const upExtent = this._window.findExtent(rowIndex, Symbols.SearchDirection.Backwards);
            const curRelIndex = (upExtent - 1) % span;
            return rowIndex - curRelIndex;
        }

        /**
         * Nudges the position of the reel up or down by a single symbol.
         * Will not adjust the window position or contents.
         * Async.
         */
        @CheckDisposed
        public async nudge(direction: "up" | "down", duration: number, ease?: (num: number) => number)
        {
            if (this.state.value !== IReel.State.Stopped) { return; }

            this._autoDelta = false;
            this._endDelta = this._delta + (direction === "down" ? this.settings.symbolSpacing : -this.settings.symbolSpacing);

            await RS.Tween.get<Reel>(this)
                .to({ delta: this._endDelta }, duration, ease);
        }

        /**
         * Nudges the position of the reel up or down by a specified amount of spaces.
         * Negative spaces will nudge the reel up, while positive spaces will nudge it down.
         * Will not adjust the window position or contents.
         * Async.
         */
        @CheckDisposed
        public async nudgeBy(spaces: number, duration: number, ease?: (num: number) => number)
        {
            if (this.state.value !== IReel.State.Stopped) { return; }

            this._autoDelta = false;
            this._endDelta = this._delta + (this.settings.symbolSpacing * spaces);

            await RS.Tween.get<Reel>(this)
                .to({ delta: this._endDelta }, duration, ease);
        }

        /**
         * Repositions the window to align with the reel.
         */
        @CheckDisposed
        public realignWindow()
        {
            this._currentStripIndex = this.deltaToStripIndex(this._delta);
        }

        @CheckDisposed
        public updateElements()
        {
            const symbolOffset = this._delta % this.settings.symbolSpacing;            // offset of topmost visible symbol on reels
            const containerTop = 0 - this._delta;

            const borderSize = this._borderSize;

            const startY = containerTop + symbolOffset; // where to render from in reel coordinates (symbol space)
            const startIndex = Math.round(startY / this.settings.symbolSpacing) - borderSize;   // index to symbols array which the startY position refers to
            const endY = startY + ((this.settings.stopSymbolCount - 1) * this.settings.symbolSpacing);  // -1 to convert from count -> index
            const endIndex = Math.round(endY / this.settings.symbolSpacing) + borderSize;

            const drawX = Math.round(this.settings.width * 0.5);

            // Hide all the symbol holders
            for (let i = 0; i < this._windowSymbols.length; i++)
            {
                const symbol = this._windowSymbols[i];
                if (symbol.visible)
                {
                    this._symbolContainer.removeChild(symbol);
                }
            }
            if (this._sceneRoot)
            {
                for (let i = 0; i < this._windowSymbols.length; i++)
                {
                    const symbol = this._sceneSymbols[i];
                    symbol.visible = false;
                }
            }

            if (!this._visible) { return; }

            if (this._useShaderForSymbols)
            {
                this._shader.blurFactor = this._inSuspense ? 0.0 : this._motionBlur;
            }

            const is3D = this._sceneRoot != null && this._sceneSymbols != null;

            // const tmp = {} as Symbols.Window.SymbolData;
            for (let j = 0; j <= this._maxZIndex; j++)
            {
                let drawY = Math.round(symbolOffset - (borderSize * this.settings.symbolSpacing) + this.settings.symbolSpacing / 2);   // where to start drawing (in canvas coordinates)
                let viewIndex = 0;
                for (let i = startIndex; i <= endIndex; i++ , viewIndex++)
                {
                    const symbolData = this.getSymbolAtIndex(i);

                    const drawSymbol = (symbolData.opacity > 0.0 || this.state.value === IReel.State.Spinning);
                    if (this._useShaderForSymbols) { this._windowSymbols[viewIndex].shader = this._shader; }

                    let span = 1;

                    const windowSymbol = this._windowSymbols[viewIndex];
                    let forwardExtent = 1;
                    let draw = false;
                    if (drawSymbol)
                    {
                        const symbolBinding = this._symbolsByID[symbolData.symbolID];
                        if (symbolBinding)
                        {
                            const symbolDef = symbolBinding.definition;
                            const zIndex = symbolDef.zIndex || 0;
                            if (zIndex === j)
                            {
                                span = symbolDef.span || 1;

                                const windowIndex = this.stripIndexToWindowIndex(i);
                                forwardExtent = span > 1 ? this._window.findExtent(windowIndex, Symbols.SearchDirection.Forwards) : 1;
                                if (span > 1 && forwardExtent % span > 0)
                                {
                                    draw = false;
                                }
                                else
                                {
                                    forwardExtent = Math.min(forwardExtent, span);
                                    if (span === 1 || forwardExtent % span === 0 || symbolDef.spanSplit)
                                    {
                                        draw = true;
                                        drawY += (forwardExtent - 1) * (this.settings.symbolSpacing / 2);
                                        this.update2DElement(
                                            windowSymbol,
                                            drawX, drawY,
                                            symbolDef, symbolData,
                                            i, windowIndex, viewIndex
                                        );
                                        i += forwardExtent - 1;
                                        viewIndex += forwardExtent - 1;

                                        if (is3D)
                                        {
                                            const stripModel = this._sceneSymbols[viewIndex];
                                            if (symbolDef.modelAsset || symbolDef.modelGeometry)
                                            {
                                                this.update3DElement(
                                                    stripModel,
                                                    drawX, drawY,
                                                    symbolDef, symbolData,
                                                    i, windowIndex, viewIndex
                                                );
                                                stripModel.visible = true;
                                            }
                                            else
                                            {
                                                stripModel.visible = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            windowSymbol.visible = false;
                        }
                    }
                    drawY += draw ? ((forwardExtent + 1) / 2) * this.settings.symbolSpacing : this.settings.symbolSpacing;
                }
            }
        }

        /**
         * Begins suspense rendering on this reel.
         */
        @CheckDisposed
        public beginSuspense(): void
        {
            if (this.state.value !== IReel.State.Spinning)
            {
                return;
            }

            this._inSuspense = true;
            this.state.value = IReel.State.Spinning;
            const suspenseProps = this.settings.suspenseSpeed;
            RS.Tween.get<Reel>(this, { override: true })
                .to({ speed: suspenseProps.speed }, suspenseProps.time, suspenseProps.ease || RS.Ease.linear);
            this.onSuspenseStarted.publish();
        }

        /**
         * Ends suspense rendering on this reel.
         */
        @CheckDisposed
        public endSuspense(): void
        {
            this._inSuspense = false;
            this.onSuspenseEnded.publish();
        }

        /**
         * Destroys this reel.
         */
        public dispose(): void
        {
            if (this._isDisposed) { return; }

            RS.Tween.removeTweens<Reel>(this);

            for (let i = 0; i < this._windowSymbols.length; i++)
            {
                const symbol = this._windowSymbols[i];
                if (symbol.visible)
                {
                    this._symbolContainer.removeChild(symbol);
                }
            }
            this._windowSymbols.length = 0;
            this._windowSymbols = null;

            this._pendingStopSymbols = null;

            this._isDisposed = true;
        }

        /**
         * Sets the correct border size for the reels making sure that it's always large enough for any spanning symbols.
         * Also allow there to be no border for instant reveal games.
         */
        protected calculateBorderSize()
        {
            if (this.settings.borderSize != null)
            {
                // If it is set to 0 in settings, it must be an instant reveal game, so return no border
                if (this.settings.borderSize === 0)
                {
                    this._borderSize = 0;
                }
                else
                {
                    this._borderSize = Math.max(this.settings.borderSize, this._tallestSymbol);
                    if (this.settings.borderSize < this._tallestSymbol)
                    {
                        RS.Log.warn("Border size in settings for reel is smaller than the largest symbol span");
                    }
                }
            }
            else
            {
                this._borderSize = this._tallestSymbol;
            }
        }

        /**
         * Regenerates the reels with the current reel properties.
         */
        protected regenerateReel(): void
        {
            // Clear existing properties
            if (this._window)
            {
                this._window.dispose();
                this._window = null;
            }

            if (this._symbolStrip)
            {
                this._symbolStrip.dispose();
                this._symbolStrip = null;
            }

            if (this.settings.offset)
            {
                this.position.set(this.settings.offset.x, this.settings.offset.y);
            }

            // Compute metrics
            this._tallestSymbol = 0;
            this._maxZIndex = 0;
            this._selectionChanceSum = 0;
            this._scatterSymbolIDs = [];
            for (const binding of this.settings.symbols)
            {
                const selectionChance = binding.selectionChance != null ? binding.selectionChance : 1;
                this._selectionChanceSum += selectionChance;
                this._tallestSymbol = Math.max(this._tallestSymbol, binding.definition.span || 1);
                this._maxZIndex = Math.max(this._maxZIndex, binding.definition.zIndex || 0);

                if (binding.definition.isScatter)
                {
                    this._scatterSymbolIDs.push(binding.id);
                }
            }

            this.calculateBorderSize();

            const borderSize = this._borderSize;
            this._symbolsByID = Symbols.constructSymbolsByID(this.settings.symbols);

            // Initialise strip and window
            this._symbolStrip = new Symbols.Strip(this._stripData ? this._stripData.length : this.settings.symbolsPerStrip, 0);
            this._window = new Symbols.Window(this.settings.symbols, this.settings.stopSymbolCount, borderSize, this._symbolStrip, this.settings.matchBorderSymbols);
            if (this.settings.windowRandomSymbolFunction) { this._window.randomSymbolFunction = this.settings.windowRandomSymbolFunction; }

            if (this._stripData)
            {
                // Populate symbol strip from our set data
                this._symbolStrip.setFrom(this._stripData);
            }
            else
            {
                // Populate symbol strip with random symbols
                this._symbolStrip.populateEvenly(this.settings.symbols);
            }

            // Create a set of symbol sprites to hold the symbols
            this._windowSymbols = new Array<Rendering.StripSymbol>(this._window.size + this._window.borderSize * 2);
            for (let i = 0, l = this._windowSymbols.length; i < l; ++i)
            {
                const stripSymbol = new (this.settings.stripSymbolClass || Rendering.StripSymbol)(this.settings.database);
                stripSymbol.reelZIndex = this.settings.zIndex ? this.settings.zIndex : 0;
                stripSymbol.visible = false;
                if (this._useShaderForSymbols) { stripSymbol.shader = this._shader; }
                this._symbolContainer.addChild(stripSymbol);
                this._windowSymbols[i] = stripSymbol;
            }

            // Create a set of symbol meshes to hold the 3D symbols
            if (this._sceneRoot)
            {
                this._sceneSymbols = new Array<Rendering.StripModel>(this._windowSymbols.length);
                for (let i = 0, l = this._windowSymbols.length; i < l; ++i)
                {
                    const stripModel = new (this.settings.stripModelClass || Rendering.StripModel)(this.settings.database);
                    stripModel.visible = false;
                    this._sceneRoot.addChild(stripModel);
                    this._sceneSymbols[i] = stripModel;
                }
            }

            // Flags
            this._cleanNeeded = false;
            this._stopSymbolsCommitted = false;

            // Update debug view
            if (this._debugView)
            {
                this._debugView.settings = {
                    strip: this._symbolStrip,
                    window: this._window,
                    bindingSet: this.settings.symbols,
                    unitSize: this.settings.symbolSpacing
                };
            }
        }

        protected update2DElement(
            windowSymbol: Rendering.StripSymbol,
            drawX: number, drawY: number,
            symbolDef: Symbols.Definition, symbolData: Symbols.Window.SymbolData,
            stripIndex: number, windowIndex: number, viewIndex: number
        ): void
        {
            const offset = symbolDef.offset || defaultSymbolOffset;
            if (windowIndex >= 0 && windowIndex < this._window.size)
            {
                windowSymbol.setSymbol(symbolDef, true, windowIndex);
            }
            else
            {
                windowSymbol.setSymbol(symbolDef, false, stripIndex);
            }
            windowSymbol.x = drawX + offset.x + this.position.x;
            windowSymbol.y = drawY + offset.y + this.position.y;
            tmpColor1.copy(symbolData.tint);
            tmpColor1.multiply(this._tint);
            windowSymbol.tint = tmpColor1;
            windowSymbol.visible = windowSymbol.texture != null;
            windowSymbol.alpha = symbolData.opacity;
            this._symbolContainer.addChild(windowSymbol);
        }

        protected update3DElement(
            stripModel: Rendering.StripModel,
            drawX: number, drawY: number,
            symbolDef: Symbols.Definition, symbolData: Symbols.Window.SymbolData,
            stripIndex: number, windowIndex: number, viewIndex: number
        ): void
        {
            const sceneOffset = this.settings.sceneOffset || Math.Vector3D.zero;
            const sceneSpacing = this.settings.sceneSpacing || Math.Vector3D.unitY;
            if (windowIndex >= 0 && windowIndex < this._window.size)
            {
                stripModel.setSymbol(symbolDef, true, windowIndex);
            }
            else
            {
                stripModel.setSymbol(symbolDef, false, stripIndex);
            }
            const offset1D = -drawY / this.settings.symbolSpacing;
            stripModel.position.set(
                sceneOffset.x + sceneSpacing.x * offset1D,
                sceneOffset.y + sceneSpacing.y * offset1D,
                sceneOffset.z + sceneSpacing.z * offset1D
            );
        }

        /**
         * Calculates if the window is currently visible.
         */
        protected getWindowIsVisible(): boolean
        {
            // Check if window is clear
            if (this._window.isClear) { return false; }

            // Get visible range
            const topStripIndex = this.deltaToStripIndex(this._delta) - 1;
            const bottomStripIndex = topStripIndex + this.settings.stopSymbolCount + 1;

            // Get window range
            const topWindowIndex = this._window.position - this._window.borderSize;
            const bottomWindowIndex = topWindowIndex + this._window.size + this._window.borderSize * 2;

            // Check if ranges intersect
            return Math.intersect1DWrapped(topStripIndex, bottomStripIndex, topWindowIndex, bottomWindowIndex, 0, this._symbolStrip.length);
        }

        /**
         * Called when the window has become visible (entered the view).
         */
        protected handleWindowEnterView(): void
        {
            // Update debug view
            if (this._debugView) { this._debugView.windowIsVisible = true; }
        }

        /**
         * Called when the window has become non-visible (left the view).
         */
        protected handleWindowExitView(): void
        {
            // If stop symbols have already been committed, we don't want to override them
            if (this._stopSymbolsCommitted) { return; }

            // See if we need to clean it
            if (this._cleanNeeded)
            {
                this._window.clear();
                this._cleanNeeded = false;
                this._stopSymbolsCommitted = false;
            }

            // See if we have a pending strip update
            if (this._pendingStrip)
            {
                this.transitionToStrip(this._pendingStrip);
                this._stopSymbolsCommitted = false;
            }

            // Update debug view
            if (this._debugView) { this._debugView.windowIsVisible = false; }
        }

        protected discoverNextValidWindowPosition(stripIndex: number, stopSymbols: number[]): number
        {
            if (!this.settings.windowPositionValidator) { return stripIndex; }
            let accum = 0;
            while (!this.settings.windowPositionValidator(this._symbolStrip, stripIndex, stopSymbols))
            {
                //-1 cause we spin "backwards"
                stripIndex = this.wrapStopSymbolIndex(stripIndex - 1);
                ++accum;
                if (accum >= this._symbolStrip.length)
                {
                    throw new Error(`windowPositionValidator returned false for every possible position`);
                }
            }
            return stripIndex;
        }

        protected transitionToStrip(strip: number[])
        {
            // Reset the window to be positioned where our view is currently, and copy symbols from the old strip
            // This will "cover up" the transition to the new strip
            this.realignWindow();
            this._window.position = this._currentStripIndex;
            this._window.clear();
            this._window.capture();
            this.internalSetStrip(strip);
            this._pendingStrip = null;

            // When the window exits view (again), clean it
            this._cleanNeeded = true;
        }

        /**
         * Commits any pending stop symbols to the reels immediately.
         */
        protected commitPendingStopSymbols(indexOffset: number): void
        {
            if (!this._pendingStopSymbols) { return; }

            this._window.position = indexOffset;
            this._window.stopIndex = this._stopIndex;
            this._window.clear();
            this._window.set(0, this._pendingStopSymbols);
            // if (this.getWindowIsVisible())
            // {
            //     Log.warn(`[Reel] commitPendingStopSymbols: window was visible right away (not enough time to position it in a non-visible region, increase spinDownDelay?)`);
            // }

            this._cleanNeeded = false;
            this._stopSymbolsCommitted = true;
        }

        /**
         * Wraps a stop symbol index into a valid array index.
         */
        protected wrapStopSymbolIndex(index: number): number
        {
            return Math.round(Math.wrap(index, 0, this._symbolStrip.length));
        }

        /**
         * Returns the symbol at the given index. Handles array wraparound.
         * @param index
         * @param useWindow     Whether or not to read from the window if it overlays the specified index.
         * @returns symbol ID at given index
         */
        protected getSymbolAtIndex(index: number, useWindow: boolean = true, out?: Symbols.Window.SymbolData): Symbols.Window.SymbolData
        {
            if (this._masterReel != null && !this.settings.ignoreMasterReelSymbols)
            {
                return this._masterReel.getSymbolAtIndex(index, useWindow);
            }
            if (useWindow)
            {
                const windowIndex = this.stripIndexToWindowIndex(index);
                return this._window.get(windowIndex, out);
            }
            else
            {
                out = out || { symbolID: null, opacity: 1.0, tint: Util.Colors.white };
                out.symbolID = this._symbolStrip.get(this.wrapStopSymbolIndex(index));
                return out;
            }
        }

        protected stripIndexToWindowIndex(index: number): number
        {
            return Math.round(Math.wrap(index - this._window.position, this._symbolStrip.length * -0.5, this._symbolStrip.length * 0.5));
        }

        protected deltaToStripIndex(delta: number): number
        {
            return -Math.round(this.normaliseDelta(delta) / this.settings.symbolSpacing);
        }

        protected stripIndexToDelta(stripIndex: number): number
        {
            return -stripIndex * this.settings.symbolSpacing;
        }

        protected internalSetStrip(strip: number[]): void
        {
            // Swap the strip
            const oldLength = this._symbolStrip.length;
            this._symbolStrip.setFrom(strip);
            this._stripData = strip;
            const lengthSizeChange = this._symbolStrip.length - oldLength;

            // Delta is relative to the bottom, so if the strip gets longer, we need to increase delta to compensate
            if (this._delta !== 0) { this._delta += lengthSizeChange * this.settings.symbolSpacing; }

            // Update debug view
            if (this._debugView)
            {
                this._debugView.settings = {
                    strip: this._symbolStrip,
                    window: this._window,
                    bindingSet: this.settings.symbols,
                    unitSize: this.settings.symbolSpacing
                };
            }

            // Notify of update
            this._isStripValid.value = true;
        }

        /**
         * Brings the delta to within the bounds of one "strip".
         */
        protected normaliseDelta(): void;

        /**
         * Brings the delta to within the bounds of one "strip".
         */
        protected normaliseDelta(delta: number): number;

        protected normaliseDelta(delta?: number): number | void
        {
            const singleSpinDelta = this._symbolStrip.length * this.settings.symbolSpacing;
            if (delta != null)
            {
                return Math.round(delta % singleSpinDelta);
            }
            else
            {
                this._delta = Math.round(this._delta % singleSpinDelta);
            }
        }

        /**
         * Starts the reel at top speed.
         */
        protected quickStart(): void
        {
            this.state.value = IReel.State.Spinning;
            this._speed = this.settings.topSpeed.speed;
            this._autoDelta = true;
            this._cleanNeeded = true;
        }

        /**
         * Immediately stops the reel. Will consider pending stop symbols.
         */
        protected stopNow()
        {
            const logger = this.logger;
            logger.queue("Instant stop requested.");

            RS.Tween.removeTweens<Reel>(this);

            this._autoDelta = false;
            this._motionBlur = 0;
            this._speed = 0;

            if (this.state.value === IReel.State.SlowingDown)
            {
                this._delta = this._endDelta;
            }

            if (this._inSuspense)
            {
                this.endSuspense();
            }

            if (this._pendingStopSymbols)
            {
                this._currentStripIndex = this.discoverNextValidWindowPosition(0, this._pendingStopSymbols);
                this._delta = this._currentStripIndex * this.settings.symbolSpacing;
                this.commitPendingStopSymbols(this._currentStripIndex);
                this._pendingStopSymbols = null;
            }
            else
            {
                this._currentStripIndex = this.deltaToStripIndex(this._delta);
                this._delta = this.stripIndexToDelta(this._currentStripIndex);

            }
            // Make sure we're showing the correct stop symbols
            this._motionBlur = 0;
            // Don't leave this true if we've committed pending stop symbols, either before or in this method.
            this._stopSymbolsCommitted = false;

            this.state.value = IReel.State.Stopped;

            this.doScatterLandCheck(IReel.StopType.Immediate);

            this.onStopped.publish(IReel.StopType.Immediate);
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this.onSymbolLanded.publish(i);
            }
        }

        /**
         * Called when the reel comes to a complete stop.
         */
        protected onStop(quickstop: boolean)
        {
            this._stopSymbolsCommitted = false;
            this._pendingStopSymbols = null;
            this._autoDelta = false;
            this.state.value = IReel.State.Stopped;

            this.normaliseDelta();
            this.realignWindow();

            this.validateSymbols();
            this._expectedStopSymbols = null;

            if (this._inSuspense) { this.endSuspense(); }
            if (this.settings.stopSound) { Audio.play(this.settings.stopSound); }

            this._motionBlur = 0;

            const stopType = quickstop ? IReel.StopType.Quick : IReel.StopType.Standard;
            this.doScatterLandCheck(stopType);

            this.onStopped.publish(stopType);
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                this.onSymbolLanded.publish(i);
            }

            const logger = this.logger;
            logger.drop();
        }

        /**
         * Checks if a scatter has landed on this reel when it stops, and publishes an event if needed.
         */
        protected doScatterLandCheck(stopReelsType: IReel.StopType): void
        {
            // No scatter symbols in reel properties so don't bother checking
            if (!this._scatterSymbolIDs || this._scatterSymbolIDs.length === 0) { return; }

            // Iterate the window
            const tmp = {} as Symbols.Window.SymbolData;
            for (let i = 0, l = this.settings.stopSymbolCount; i < l; ++i)
            {
                const symbolID = this._window.get(i, tmp).symbolID;
                if (this._scatterSymbolIDs.indexOf(symbolID) !== -1)
                {
                    // It's a scatter
                    this.onScatterLanded.publish({ rowIndex: i, stopType: stopReelsType });
                }
            }
        }

        /** Checks that the stop symbols being displayed are correct. */
        private validateSymbols()
        {
            for (let i = 0; i < this.settings.stopSymbolCount; i++)
            {
                const symbolID = this._window.get(i).symbolID;
                const expectedID = this._expectedStopSymbols[i];
                if (expectedID !== symbolID)
                {
                    this.logValidationError();
                    return;
                }
            }
        }

        private logValidationError()
        {
            const actualStopSymbols: number[] = [];
            for (let i = 0; i < this.settings.stopSymbolCount; i++)
            {
                actualStopSymbols[i] = this._window.get(i).symbolID;
            }

            const logger = this.logger;
            logger.queue(`Incorrect stops detected! Expected ${this._expectedStopSymbols}, got ${actualStopSymbols}. Dump:`);
            logger.dump(Logging.LogLevel.Error);
        }
    }

    export namespace Reel
    {
        /**
         * Describes animation settings for a "stage".
         */
        export interface SpeedDefinition
        {
            /** Time it takes to reach this speed or distance (ms). */
            time: number;

            /** Distance reel should travel in pixels. */
            distance?: number;

            /** Speed to reach in pixels-per-second. */
            speed?: number;

            /** How to ease in to the distance or speed provided. */
            ease?: EaseFunction;
        }

        export interface Settings extends IReel.Settings
        {
            /** Amount of windup (use distance). */
            windup: SpeedDefinition;

            /** Top speed (use speed). */
            topSpeed: SpeedDefinition;

            /** Deceleration (use speed as final speed, usually 0). */
            deceleration: SpeedDefinition;

            /** Speed reels spin when in suspense, and time it takes to get to that from top speed. */
            suspenseSpeed: SpeedDefinition;

            /** Overhang amount (use distance). */
            overhang: SpeedDefinition;

            /** Border size of the reel window. */
            borderSize?: number;

            /** Update border symbols to match the reel strip */
            matchBorderSymbols?: boolean;
        }
    }
}