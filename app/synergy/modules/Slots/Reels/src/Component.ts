/// <reference path="IComponent.ts" />

namespace RS.Reels
{
    import ReelSet = Slots.ReelSet;

    const DebugLog = function (messageIn?: string, messageOut?: string) { return RS.DebugLog("Reels", messageIn, messageOut); }

    /**
     * A reels component.
     * Manages a set of individual reels.
     */
    @HasCallbacks
    export class Component<TReelSettings extends IReel.Settings = IReel.Settings, TSettings extends IComponent.Settings<TReelSettings> = IComponent.Settings<TReelSettings>> extends Flow.BaseElement<TSettings, IComponent.RuntimeData> implements IComponent<TReelSettings, TSettings>
    {
        public readonly state = new Observable(IReel.State.Stopped);

        /** Published when all the reels have stopped. */
        public readonly onStopped = createSimpleEvent();

        /** Published when a single reel has stopped. */
        public readonly onReelStopped = createEvent<number>();

        /** Published when a symbol has landed. */
        public readonly onSymbolLanded = createEvent<Slots.ReelSetPosition>();

        /** Published when a scatter has landed on a reel. */
        public readonly onScatterLanded = createEvent<IReel.ScatterLandedData & { reelIndex: number; }>();

        /** Published when suspense has started on a reel. */
        public readonly onSuspenseStarted = createEvent<number>();

        /** Published when suspense has ended on a reel. */
        public readonly onSuspenseEnded = createEvent<number>();

        /** Published when this reel has been clicked to skip. */
        public readonly onSkipRequested = createSimpleEvent();

        @AutoDispose protected _reels: IReel<TReelSettings>[];
        protected _lockedReels: boolean[];
        protected _lockedReelsCount: number;
        protected _canStopNormally: boolean;
        protected _canQuickstop: boolean;
        protected _stopRequested: boolean;
        protected _quickStopRequested: boolean;
        protected _turboStopRequested: boolean;
        protected _spinState: IReel.State;
        protected _enabled: boolean;
        protected _skipEnabled: boolean = false;
        protected _reelsStopped: number;
        protected _pendingStopSymbols: ReelSet = null;
        protected _suspenseTimes: number[];
        protected _skipNextSuspense = false;
        @AutoDispose protected _symbolContainer: RS.Rendering.IContainer;
        protected _currentReelSet: ReelSet | null = null;
        protected _currentWindow: ReelSet;
        protected _currentStopIndices: number[] | null = null;
        @AutoDispose protected _debugViewContainer: RS.Rendering.IContainer;
        protected _ignoreRigToolStopChanges: boolean = false;
        protected _rigToolVisible: boolean = false;
        protected _rigTools: RigTool[];
        protected _startTween: Tween<Component> | null = null;
        protected _stopTween: Tween<Component> | null = null;
        @AutoDisposeOnSet protected readonly _spinTimer: RS.Ticker.Timer;
        protected _lastRenderStyle: string | null = null;
        protected _hasTickers: boolean = false;
        protected _sceneRoot: RS.Rendering.Scene.ISceneObject | null = null;

        protected _symbolLandAnimators: ISymbolLandAnimator[][] = [];
        protected _symbolLandAnimations: { anim: ISymbolLandAnimation; promise: PromiseLike<void>; }[] = [];

        protected _useShaderForSymbols: boolean;
        protected _maskShape: RS.Rendering.IGraphics;

        protected _tmpPoint1: RS.Rendering.Point2D;
        protected _tmpPoint2: RS.Rendering.Point2D;
        protected _tmpRect: RS.Rendering.Rectangle;
        protected _reelsSize: Math.Vector2D;

        @RS.AutoDisposeOnSet protected _stripSelector: StripSelector;

        /** Gets if the reels can do a standard stop. */
        public get canStopNormally() { return this._canStopNormally; }

        /** Gets if the reels can do a quick stop. */
        public get canStopQuickly() { return this._canQuickstop; }

        /** Gets the individual reels. */
        public get reels() { return this._reels; }

        /** Gets the current stop indices. */
        public get stopIndices() { return this._currentStopIndices; }

        /** Gets the reels mask. */
        public get maskShape() { return this._maskShape; }

        /** Gets or sets if the reels can be skipped using the skip arbiter. */
        public get skipEnabled() { return this._skipEnabled; }
        public set skipEnabled(value)
        {
            this._skipEnabled = value;
            if (this._skipEnabled)
            {
                (this as RS.Reels.Component).bindToObservables({ interactive: this.runtimeData.skipArbiter, buttonMode: this.runtimeData.skipArbiter });
            }
            else
            {
                this.unbindFromObservables();
            }
        }

        /** Gets or sets if the reels are enabled. */
        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            if (value === this._enabled) { return; }
            this._enabled = value;
            if (value)
            {
                if (this._reels)
                {
                    Ticker.registerTickers(this);
                    this._hasTickers = true;
                }
            }
            else
            {
                Ticker.unregisterTickers(this);
                this._hasTickers = false;
                RS.Tween.removeTweens<Component>(this);
                this._startTween = null;
                this._stopTween = null;
            }
        }

        protected _tint: Util.Color = Util.Colors.white;

        /** Gets or sets the tint of the reels. */
        public get tint() { return this._tint; }
        public set tint(value)
        {
            this._tint = value;
            for (let i = 0; i < this._reels.length; i++)
            {
                this._reels[i].tint = value;
            }
        }

        /** Gets or sets if the rig tool is visible. */
        public get rigToolVisible() { return this._rigToolVisible; }
        public set rigToolVisible(value)
        {
            if (this._rigToolVisible === value) { return; }
            this._rigToolVisible = value;
            if (value)
            {
                this.showRigTool();
            }
            else
            {
                this.hideRigTool();
            }
        }

        protected get logger() { return Logging.ILogger.get().getContext("Reels"); }

        public constructor(settings: TSettings, runtimeData: IComponent.RuntimeData)
        {
            super(settings, runtimeData);
            this.name = "Reels Component";
            if (settings.rigToolSettings == null)
            {
                settings.rigToolSettings = RigTool.defaultSettings;
            }

            this._maskShape = new RS.Rendering.Graphics();
            this.state.value = IReel.State.Stopped;
            this._sceneRoot = runtimeData.sceneRoot || null;
            this.enabled = true;

            if (this.runtimeData.skipArbiter)
            {
                this.onClicked(this.trySkip);
                this.skipEnabled = true;
            }

            const width = settings.reels
                .map((r) => r.width)
                .reduce((a, b) => a + b, 0);
            const height = settings.reels
                .map((r) => r.symbolSpacing * r.stopSymbolCount)
                .reduce((a, b) => Math.max(a, b), 0);
            this._reelsSize = { x: width, y: height };

            this.hitArea = new RS.Rendering.Rectangle(0, 0, width, height);
            this.filterArea = new RS.Rendering.Rectangle(0, 0, width, height);
            this._tmpRect = new RS.Rendering.Rectangle();
            this._tmpPoint1 = new RS.Rendering.Point2D(0, 0);
            this._tmpPoint2 = new RS.Rendering.Point2D(0, 0);

            this._spinTimer = new RS.Ticker.Timer();

            this.onStageChanged(this.handleStageChanged);
        }

        /**
         * Starts the reels.
         */
        @DebugLog()
        public startReels(startType: IReel.StartType): void
        {
            if (!this._enabled) { return; }

            if (this.state.value !== IReel.State.Stopped)
            {
                throw new Error("Reels were requested to start when they were not stopped");
            }

            if (startType === IReel.StartType.Immediate)
            {
                this.startReelsImmediately();
            }
            else
            {
                this.spinUpReels();
            }

            this._stopRequested = false;
            this._quickStopRequested = false;
            this._turboStopRequested = false;
        }

        /**
         * Stops the reels in the specified manner.
         * @param noSuspense
         */
        @DebugLog()
        public stopReels(stopType: IReel.StopType, noSuspense?: boolean): void
        {
            switch (stopType)
            {
                case IReel.StopType.Immediate:
                    this.stopImmediately(noSuspense || false);
                    break;
                case IReel.StopType.Standard:
                    this._stopRequested = true;
                    this._skipNextSuspense = noSuspense === true;
                    this.checkForStop();
                    break;
                case IReel.StopType.Quick:
                    this._quickStopRequested = true;
                    this._skipNextSuspense = noSuspense !== false;
                    this.checkForStop();
                    break;
                case IReel.StopType.Turbo:
                    this._turboStopRequested = true;
                    this._quickStopRequested = true;
                    this._skipNextSuspense = noSuspense !== false;
                    this.checkForStop();
                    break;
                default:
                    this._skipNextSuspense = noSuspense === true;
                    this.stop();
                    break;
            }
        }

        /**
         * Gets the symbol info for the specified ID, based on the specified reel's symbol bindings.
         */
        public getSymbolDefinition(id: number, reelIndex: number): Symbols.Definition
        {
            return this._reels[reelIndex].getSymbolDefinition(id);
        }

        /**
         * Gets the symbol ID for the specified definition, based on the specified reel's symbol bindings.
         */
        public getSymbolID(definition: Symbols.Definition, reelIndex: number): number | null
        {
            return this._reels[reelIndex].getSymbolID(definition);
        }

        /**
         * Starts the given reel with a specific start type.
         * @param reelNum       - the reel number to start (zero-indexed)
         * @param startType     - how to start the reel
         */
        @DebugLog()
        public startReel(reelNum: number, startType: IReel.StartType): void
        {
            if (this._reels[reelNum].state.value !== IReel.State.Stopped)
            {
                throw new Error("Tried to start a reel which was not stopped.");
            }

            this._reelsStopped--;

            switch (startType)
            {
                case IReel.StartType.Standard:
                    this._reels[reelNum].spinUp(IReel.StartType.Standard);
                    break;
                case IReel.StartType.Immediate:
                    this._reels[reelNum].spinUp(IReel.StartType.Immediate);
                    break;
            }

            // Also start all slaved reels
            for (let i = 0; i < this._reels.length; i++)
            {
                if (reelNum !== i && this._reels[i].masterReel === this._reels[reelNum])
                {
                    this.startReel(i, startType);
                }
            }

            // Reset animators
            for (const ids of this._symbolLandAnimators)
            {
                if (ids)
                {
                    for (const animator of ids)
                    {
                        animator.reset();
                    }
                }
            }
        }

        /**
         * Stops the given reel with a specific stop type.
         * @param reelNum       - the reel number to stop (zero-indexed)
         * @param stopType      - how to stop the reel
         */
        @DebugLog()
        public stopReel(reelNum: number, stopType: IReel.StopType): void
        {
            if (this._reels[reelNum].state.value === IReel.State.Stopped) { return; }
            
            switch (stopType)
            {
                case IReel.StopType.Standard:
                case IReel.StopType.Quick:
                case IReel.StopType.Turbo:
                    this._reels[reelNum].spinDown(stopType);
                    break;
                case IReel.StopType.Immediate:
                    this._reels[reelNum].spinDown(IReel.StopType.Immediate);
                    if (this._pendingStopSymbols) { this._reels[reelNum].setStopSymbols(this._pendingStopSymbols.getReel(reelNum)); }
                    break;
            }

            // Also stop all slaved reels
            for (let i = 0; i < this._reels.length; i++)
            {
                if (reelNum !== i && this._reels[i].masterReel === this._reels[reelNum])
                {
                    this.stopReel(i, stopType);
                }
            }
        }

        /**
         * Sets a symbol in the visible window.
         * Should only be used when the reels are stopped.
         * Will NOT edit the symbol strip, will instead edit the window.
         * Will NOT consider any intersections or resolve multi-spanning symbols.
         * @param reelIndex     Reel index to set at
         * @param rowIndex      Row index to set at
         * @param symbolID      Symbol ID to set
         */
        public setSymbol(reelIndex: number, rowIndex: number, symbolID: number): void
        {
            if (!this.getSymbolDefinition(symbolID, reelIndex))
            {
                Log.warn(`Symbol ID '${symbolID}' is invalid for reel index ${reelIndex}`);
                return;
            }
            this._reels[reelIndex].setStopSymbol(rowIndex, symbolID);
            this._currentWindow.set(reelIndex, rowIndex, symbolID);
        }

        /**
         * Gets a symbol from the visible window.
         * @param reelIndex
         * @param rowIndex
         */
        public getSymbol(reelIndex: number, rowIndex: number): number
        {
            return this._currentWindow.get(reelIndex, rowIndex);
        }

        /**
         * Sets the content of the reels with the given reel set.
         * Only affects the visible window.
         */
        public setReels(reelSet: Slots.ReelSet): void;

        /**
         * Sets the content of the reels with the given reel stops.
         * Reel set must be set (via setReelSet) for this to work.
         * Only affects the visible window.
         */
        public setReels(stopPositions: number[]): void;

        /**
         * Sets the content of the reels with the given reel set.
         * Only affects the visible window.
         */
        public setReels(reelSet: Slots.ReelSet, stopPositions: number[]): void;

        /**
         * Sets the content of the reels with the given reel set.
         * Only affects the visible window.
         */
        public setReels(p0: ReelSet|number[], p1?: number[]): void
        {
            if (!this._enabled) { return; }

            let reelSet: ReelSet;

            if (Is.array(p0))
            {
                if (this._currentReelSet == null) { throw new Error("Tried to setReels with stopIndices when no reel set is set"); }
                reelSet = this._currentReelSet.extractSubset(p0, this.settings.reels.map((r) => r.stopSymbolCount));
                this._currentStopIndices = p0.slice();
            }
            else
            {
                reelSet = p0;
                if (p1 != null)
                {
                    this._currentStopIndices = p1.slice();
                }
                else
                {
                    this._currentStopIndices = null;
                }
            }
            if (this._rigTools != null && this._currentStopIndices != null)
            {
                this._ignoreRigToolStopChanges = true;
                for (let i = 0, l = this._rigTools.length; i < l; ++i)
                {
                    this._rigTools[i].stopPosition.value = this._currentStopIndices[i];
                }
                this._ignoreRigToolStopChanges = false;
            }
            this._currentWindow = reelSet.clone();
            if (this.state.value === IReel.State.Stopped && this._reels != null)
            {
                for (let i = 0; i < this._reels.length; i++)
                {
                    this._reels[i].setStopSymbols(this._currentWindow.getReel(i), false, this._currentStopIndices ? this._currentStopIndices[i] : null);
                }
            }
            else
            {
                this._pendingStopSymbols = reelSet;
            }
        }

        /**
         * Sets the reel set that the reels references in to.
         * If set, this is used to populate the reels that spin by, and when shifting the reels via rig tool.
         * If not set, the reels that spin by are generated randomly using selectionChance and shifting via rig tool will be non-functional.
         */
        public setReelSet(reelSet: Slots.ReelSet): void
        {
            this._currentReelSet = reelSet;
            for (let i = 0; i < this._reels.length; i++)
            {
                this._reels[i].setStrip(reelSet.getReel(i));
            }
        }

        // #region Symbol Rendering Properties

        /**
         * Hides the symbol at the given location.
         */
        public hideSymbol(reelIndex: number, rowIndex: number): void
        {
            this._reels[reelIndex].hideSymbol(rowIndex);
        }

        /**
         * Hides the symbol at the given location.
         */
        public showSymbol(reelIndex: number, rowIndex: number): void
        {
            this._reels[reelIndex].showSymbol(rowIndex);
        }

        /**
         * Sets the opacity of the symbol at the given location.
         */
        public setSymbolOpacity(reelIndex: number, rowIndex: number, opacity: number): void
        {
            this._reels[reelIndex].setSymbolOpacity(rowIndex, opacity);
        }

        /**
         * Sets the color of the symbol at the given location.
         */
        public setSymbolColor(reelIndex: number, rowIndex: number, color: Util.Color): void
        {
            this._reels[reelIndex].setSymbolColor(rowIndex, color);
        }

        /**
         * Hides all symbols on the reels
         */
        public hideAllSymbols(): void
        {
            for (let reel = 0; reel < this._reels.length; reel++)
            {
                const symbolCount = this.settings.reels[reel].stopSymbolCount;
                for (let row = 0; row < symbolCount; row++)
                {
                    this._reels[reel].hideSymbol(row);
                }
            }
        }

        /**
         * Shows all symbols on the reels
         */
        public showAllSymbols(): void
        {
            for (let reel = 0; reel < this._reels.length; reel++)
            {
                const symbolCount = this.settings.reels[reel].stopSymbolCount;
                for (let row = 0; row < symbolCount; row++)
                {
                    this._reels[reel].showSymbol(row);
                }
            }
        }

        // #endregion

        // #region Symbol Holding

        /**
         * Holds the specified positions. They won't spin with the rest of the reels.
         * @param positions
         */
        public holdPositions(positions: ReadonlyArray<Slots.ReelSetPosition>): void
        {
            for (const pos of positions)
            {
                this._reels[pos.reelIndex].holdRow(pos.rowIndex);
            }
        }

        /** Holds all positions. */
        public holdAllPositions(): void
        {
            for (const reel of this._reels)
            {
                reel.holdAllRows();
            }
        }

        /**
         * Unholds the specified positions.
         * @param positions
         */
        public unholdPositions(positions: ReadonlyArray<Slots.ReelSetPosition>): void
        {
            for (const pos of positions)
            {
                this._reels[pos.reelIndex].unholdRow(pos.rowIndex);
            }
        }

        /**
         * Unholds all positions.
         */
        public unholdAllPositions(): void
        {
            for (const reel of this._reels)
            {
                reel.unholdAllRows();
            }
        }

        // #endregion

        /**
         * Given the index of the specified symbol (in stop symbol coordinates), finds the row index that STARTS the symbol.
         * If the symbol's span is 1, this function will just return the passed in row.
         * If the symbol's span is > 1, this function will find the top-most point of the symbol.
         * Searches the entire symbol strip including the window overlay.
         * @param reel  The reel index to search
         * @param row   The row of the symbol to identify
         * @returns     The row of the symbol's start
         */
        public findSymbolStart(reel: number, row: number): number
        {
            return this._reels[reel].findSymbolStart(row);
        }

        /**
         * Given the index of the specified symbol (in stop symbol coordinates), finds the row index in the CENTER of the symbol.
         * If the symbol's span is 1, this function will just return the passed in row.
         * If the symbol's span is > 1, this function will find the middle-most point of the symbol.
         * Searches the entire symbol strip including the window overlay.
         * @param reel  The reel index to search
         * @param row   The row of the symbol to identify
         * @returns     The row of the symbol's middle
         */
        public findSymbolCenter(reelIndex: number, rowIndex: number): number
        {
            const start = this.findSymbolStart(reelIndex, rowIndex);
            const symbolID = this.getSymbol(reelIndex, rowIndex);
            const symbolDef = this.getSymbolDefinition(symbolID, reelIndex);
            const span = symbolDef.spanSplit ? 1 : (symbolDef.span || 1);
            return start + (span - 1) * 0.5;
        }

        /**
         * Returns a new sprite positioned at the correct location (relative to the reels stage) above an existing symbol.
         * If no symbol ID is provided, the current reels are queried for the symbol at the given reel and row.
         * @param reelIndex
         * @param rowIndex
         * @param symbolID
         */
        public getSpriteForPosition(reelIndex: number, rowIndex: number, symbolID?: number): RS.Rendering.IBitmap | RS.Rendering.IAnimatedSprite | RS.Rendering.ISpine | null
        {
            if (symbolID == null)
            {
                symbolID = this.getSymbol(reelIndex, rowIndex);
            }
            const def = this.getSymbolDefinition(symbolID, reelIndex);

            const asset = Asset.Store.getAsset(def.spriteAsset);
            let sprite: RS.Rendering.IBitmap | RS.Rendering.IAnimatedSprite | RS.Rendering.ISpine | null;

            // Prioritise animating with spine if it exists.
            if (def.spineAnimation != null)
            {
                if (def.spineAnimation.scale == null) { def.spineAnimation.scale = 1; }
                const spineAsset = Asset.Store.getAsset(def.spineAnimation.spineAsset) as object as Asset.ISpineAsset;
                const spine = new RS.Rendering.Spine(def.spineAnimation.scale, spineAsset);
                if (def.spineAnimation.animationName != null)
                {
                    spine.gotoAndStop(def.spineAnimation.animationName);
                    if (def.spineAnimation.animationDuration == null) { def.spineAnimation.animationDuration = spine.getAnimationLength(); }
                }
                sprite = spine;
            }
            else if (def.spriteAsset)
            {
                switch (def.spriteAsset.type)
                {
                    case "spritesheet":
                    case "rsspritesheet":
                    {
                        // const spr = RS.Factory.sprite(def.spriteAsset);
                        const spriteSheet = Asset.Store.getAsset(def.spriteAsset).asset as RS.Rendering.ISpriteSheet;
                        const spr = new RS.Rendering.AnimatedSprite(spriteSheet);
                        spr.gotoAndStop(0);
                        if (def.animation != null && def.animation.framerate != null)
                        {
                            spr.framerate = def.animation.framerate;
                        }
                        if (def.animation != null && def.animation.name != null)
                        {
                            spr.gotoAndStop(def.animation.name);
                        }
                        sprite = spr;
                        break;
                    }
                    case "image":
                    {
                        sprite = RS.Factory.bitmap(def.spriteAsset);
                        sprite.anchorX = sprite.anchorY = 0.5;
                        break;
                    }
                    default:
                    {
                        sprite = null;
                        break;
                    }
                }
            }
            else if (def.spriteTexture)
            {
                sprite = new RS.Rendering.Bitmap(def.spriteTexture);
                sprite.anchorX = sprite.anchorY = 0.5;
            }
            else
            {
                RS.Log.warn(`[Reels] Could not get sprite at position (${reelIndex}, ${rowIndex}) - no spriteAsset or spriteTexture found.`);
            }

            if (sprite == null) { return sprite; }

            const pos = this.getSymbolPosition(reelIndex, rowIndex);
            if (def.offset != null)
            {
                sprite.x = pos.x + def.offset.x;
                sprite.y = pos.y + def.offset.y;
            }
            else
            {
                sprite.x = pos.x;
                sprite.y = pos.y;
            }
            return sprite;
        }

        /**
         * Gets the centered position of a symbol on the reels.
         * @param reelIndex
         * @param rowIndex
         */
        public getSymbolPosition(reelIndex: number, rowIndex: number): Math.Vector2D
        {
            const reelSettings = this.settings.reels[reelIndex];
            const reel = this._reels[reelIndex];
            const relPos = reel.getSymbolPosition(rowIndex);
            return { x: relPos.x + reel.x, y: relPos.y + reel.y };
        }

        /**
         * Nudges the position of the given reel up or down by a single symbol.
         * Calls doneCallback when the nudge has finished.
         */
        public async nudgeReel(reelIndex: number, direction: "up" | "down", duration: number, ease?: (num: number) => number)
        {
            await this._reels[reelIndex].nudge(direction, duration, ease);
        }

        /**
         * Repositions the given window to align with the reel.
         */
        public realignReelWindow(reelIndex: number)
        {
            this._reels[reelIndex].realignWindow();
        }

        // #region Reel Visibility

        public hideReels(...reels: number[])
        {
            for (const reelIndex of reels)
            {
                this._reels[reelIndex].visible = false;
            }
        }

        public showAllReels()
        {
            for (const reel of this._reels)
            {
                reel.visible = true;
            }
        }

        // #endregion

        // #region Reel Locking

        public lockReels(...reels: number[])
        {
            for (let i = 0; i < reels.length; i++)
            {
                this._lockedReels[reels[i]] = true;
            }
            this._lockedReelsCount = this._lockedReels.filter((lock) => lock).length;
        }

        public unlockReels(...reels: number[])
        {
            for (let i = 0; i < reels.length; i++)
            {
                this._lockedReels[reels[i]] = false;
            }
            this._lockedReelsCount = this._lockedReels.filter((lock) => lock).length;
        }

        public unlockAllReels()
        {
            for (let i = 0; i < this._lockedReels.length; i++)
            {
                this._lockedReels[i] = false;
            }
            this._lockedReelsCount = 0;
        }

        // #endregion

        // #region Reel Slaving

        public slaveReel(slaveReelIndex: number, masterReelIndex: number)
        {
            this._reels[slaveReelIndex].masterReel = this._reels[masterReelIndex];
        }

        public unslaveReel(reelIndex: number)
        {
            this._reels[reelIndex].masterReel = null;
        }

        public unslaveAllReels()
        {
            for (let i = 0; i < this._reels.length; i++)
            {
                this.unslaveReel(i);
            }
        }

        // #endregion

        // #region Symbol Land Animation

        public addSymbolLandAnimator(symbolDef: Symbols.Definition, animator: ISymbolLandAnimator): void
        {
            const db = Symbols.IDatabase.get();
            const guid = db.get(symbolDef);
            const arr = (this._symbolLandAnimators[guid] || (this._symbolLandAnimators[guid] = []));
            arr.push(animator);
        }

        public removeSymbolLandAnimator(p1: Symbols.Definition | ISymbolLandAnimator, p2?: ISymbolLandAnimator): void
        {
            const animator: ISymbolLandAnimator|null = Is.func((p1 as ISymbolLandAnimator).animateSymbol) ? p1 as ISymbolLandAnimator : p2;
            const symbolDef: Symbols.Definition|null = Is.func((p1 as ISymbolLandAnimator).animateSymbol) ? null : p1 as Symbols.Definition;

            if (symbolDef)
            {
                const db = Symbols.IDatabase.get();
                const guid = db.get(symbolDef);
                const arr = (this._symbolLandAnimators[guid] || (this._symbolLandAnimators[guid] = []));
                if (animator)
                {
                    const idx = arr.indexOf(animator);
                    if (idx !== -1) { arr.splice(idx, 1); }
                }
                else
                {
                    arr.length = 0;
                }
            }
            else
            {
                for (const arr of this._symbolLandAnimators)
                {
                    if (arr)
                    {
                        const idx = arr.indexOf(animator);
                        if (idx !== -1) { arr.splice(idx, 1); }
                    }
                }
            }
        }

        public removeAllSymbolLandAnimators(): void
        {
            this._symbolLandAnimators.length = 0;
        }

        // #endregion

        public dispose()
        {
            if (this.isDisposed) { return; }
            this.enabled = false;
            this._symbolContainer.parent = null;
            this._symbolContainer.dispose();
            this._symbolContainer = null;
            this._pendingStopSymbols = null;

            // TODO: _symbolLandAnimations

            this.removeAllChildren();
            super.dispose();
        }

        protected showRigTool()
        {
            // Disable our hit area to prevent clipping rig tool interaction
            this.hitArea = null;
            // Show rig tool
            this._rigTools = new Array<RigTool>(this._reels.length);
            for (let i = 0, l = this._rigTools.length; i < l; ++i)
            {
                const rigTool = this._rigTools[i] = new RigTool(this.settings.rigToolSettings,
                {
                    reel: this._reels[i],
                    symbolCount: this._currentReelSet ? this._currentReelSet.getRowCount(i) : 0
                });
                const reelSettings = this.settings.reels[i];
                rigTool.seleniumId = `RigTool${i}`;
                rigTool.x = this._reels[i].x;
                rigTool.y = this._reels[i].y;
                rigTool.size = { w: reelSettings.width, h: reelSettings.symbolSpacing * reelSettings.stopSymbolCount };
                if (this._currentStopIndices)
                {
                    rigTool.stopPosition.value = this._currentStopIndices[i];
                }
                rigTool.stopPosition.onChanged((newStop) =>
                {
                    if (!this._currentStopIndices) { return; }
                    if (this._ignoreRigToolStopChanges) { return; }
                    this._currentStopIndices[i] = newStop;
                    this.setReels(this._currentStopIndices);
                });
                rigTool.onStripButtonClicked(() =>
                {
                    this._stripSelector = new StripSelector(
                    {
                        ...StripSelector.defaultSettings,
                        ...this.settings.stripSelectorSettings
                    },
                    {
                        reel: this._reels[i],
                        reelStrip: this._currentReelSet ? this._currentReelSet.getReel(i) : null,
                        currentIndex: this._currentStopIndices[i],
                        stopSymbolCount: reelSettings.stopSymbolCount
                    });
                    this.addChild(this._stripSelector);
                    this._stripSelector.invalidateLayout();
                    this._stripSelector.onSymbolSelected((index) =>
                    {
                        rigTool.stopPosition.value = index;
                        this._stripSelector = null;
                    });
                });
                this.addChild(this._rigTools[i]);
            }
        }

        protected hideRigTool()
        {
            // Hide rig tool
            for (const rigTool of this._rigTools)
            {
                this.removeChild(rigTool);
            }
            this._stripSelector = null;
            this._rigTools.length = 0;
            this._rigTools = null;
            // Re-enable our hit area
            this.hitArea = new RS.Rendering.Rectangle(0, 0, this._reelsSize.x, this._reelsSize.y);
        }

        @Callback
        protected handleStageChanged(newStage: RS.Rendering.IStage)
        {
            this.log(`Reels component got new stage ${newStage}`);

            if (!newStage) { return; }
            if (this._lastRenderStyle === newStage.renderStyle) { return; }

            this.removeChild(this._maskShape);

            this._useShaderForSymbols = this.stage ? this.stage.renderStyle === RS.Rendering.StageRenderStyle.WEBGL : false;

            if (!this._useShaderForSymbols)
            {
                this.addChild(this._maskShape);
                this.mask = this._maskShape;
            }

            this.regenerateReels();

            const initConfig = this.runtimeData.initialConfiguration;
            if (initConfig && this._lastRenderStyle == null)
            {
                this.setReelSet(initConfig.reelSet);
                this.setReels(initConfig.symbolsInView, initConfig.stopIndices);
            }

            this._lastRenderStyle = newStage.renderStyle;
        }

        protected log(message: string)
        {
            this.logger.log(message, Logging.LogLevel.Debug);
        }

        protected calculateContentsSize(): RS.Dimensions
        {
            return { w: this._reelsSize.x, h: this._reelsSize.y };
        }

        @Callback
        protected trySkip(): void
        {
            if (!this._skipEnabled) { return; }
            const canSkip = this.runtimeData.skipArbiter.value;
            if (!canSkip) { return; }
            this.onSkipRequested.publish();
        }

        /**
         * Regenerates the reels using the current reel properties.
         */
        @DebugLog()
        protected regenerateReels(): void
        {
            if (this._reels)
            {
                for (let i = 0; i < this._reels.length; i++)
                {
                    const reel = this._reels[i];
                    reel.dispose();
                }
            }

            if (this._symbolContainer)
            {
                this.removeChild(this._symbolContainer);
                this._symbolContainer.removeAllChildren();
                this._symbolContainer = null;
            }
            this._symbolContainer = new RS.Rendering.Container();
            this.addChild(this._symbolContainer);

            this._reels = [];
            this._suspenseTimes = [];

            const reelClass: ReelConstructor<TReelSettings> = this.settings.reelClass || Reel as ReelConstructor<TReelSettings>;
            let curX = 0;
            for (let i = 0, l = this.settings.reels.length; i < l; ++i)
            {
                const reelSettings = this.settings.reels[i];
                reelSettings.useShader = this._useShaderForSymbols;
                const reel = new reelClass(reelSettings, this._symbolContainer, this._sceneRoot);
                reel.x += curX;
                reel.onScatterLanded((arg) => this.onScatterLanded.publish({ ...arg, reelIndex: i }));
                reel.state.onChanged((arg) => this.handleReelSpinStateChanged(i, arg));
                reel.onDecelerateStarted(() => this.handleReelDecelerateStarted(i));
                reel.onStopped((arg) => this.handleReelStopped(i, arg));
                reel.onSymbolLanded((arg) => this.handleSymbolLanded(i, arg));
                reel.onSuspenseStarted(() => this.onSuspenseStarted.publish(i));
                reel.onSuspenseEnded(() => this.onSuspenseEnded.publish(i));

                curX += this.settings.reels[i].width;

                this._reels.push(reel);
                this._suspenseTimes.push(0);
            }

            this._lockedReels = this._reels.map((r) => false);
            this._lockedReelsCount = 0;

            this._reelsStopped = this.settings.reels.length;

            if (this._enabled && !this._hasTickers)
            {
                Ticker.registerTickers(this);
                this._hasTickers = true;
            }

            // if (this._debugViewContainer)
            // {
            //     this.removeChild(this._debugViewContainer);
            //     this._debugViewContainer.removeAllChildren();
            //     this._debugViewContainer = null;
            // }
            // this._debugViewContainer = new RS.Rendering.Container();
            // this.addChild(this._debugViewContainer);

            // curX = 50;
            // for (const reel of this._reels)
            // {
            //     const debugView = reel.debugView;
            //     debugView.x = curX;
            //     debugView.y = 50;
            //     curX += 30;
            //     this._debugViewContainer.addChild(debugView);
            // }
        }

        /**
         * Spins each reel up in sequence.
         */
        protected spinUpReels(): void
        {
            if (this.state.value !== IReel.State.Stopped) { return; }

            if (this._startTween)
            {
                if (!this._startTween.isDisposed) { this._startTween.finish(); }
                this._startTween = null;
            }

            const tween = this._startTween = RS.Tween.get<Component>(this);
            let spinDelay = 0;
            for (let i = 0; i < this._reels.length; i++)
            {
                spinDelay += this.settings.reelStartDelta;
                if (!this._lockedReels[i])
                {
                    tween
                        .wait(spinDelay)
                        .call((t) => this.startReel(i, IReel.StartType.Standard));
                    spinDelay = 0;
                }
            }
            tween.call((t) =>
            {
                this._startTween = null;
            });

            this._spinTimer.start();
            this._canStopNormally = false;
            this._canQuickstop = false;
            this.state.value = IReel.State.SpinningUp;
        }

        /**
         * Starts the reels at top speed.
         */
        protected startReelsImmediately(): void
        {
            if (this.state.value !== IReel.State.Stopped) { return; }

            for (let i = 0; i < this._reels.length; i++)
            {
                if (!this._lockedReels[i])
                {
                    this.startReel(i, IReel.StartType.Immediate);
                }
            }

            this._canStopNormally = true;
            this._canQuickstop = true;
            this.state.value = IReel.State.Spinning;
        }

        /**
         * Handles a tick event from the ticker. Checks the spin state of all reels, and if they are all spinning
         * checks that they can stop.
         */
        @RS.Tick({ })
        protected tick(ev: Ticker.Event): void
        {
            if (ev.paused || !this._enabled) { return; }

            if (this.filterArea)
            {
                this._tmpRect.copy(this.filterArea);

                this._tmpPoint1.set(0, 0);
                const pt = this.toLocal(this._tmpPoint1, undefined, this._tmpPoint2);
                this._tmpRect.x = pt.x;
                this._tmpRect.y = pt.y;
                this._tmpPoint1.set(this._reelsSize.x, this._reelsSize.y);
                const pt2 = this.toLocal(this._tmpPoint1, undefined, this._tmpPoint2);
                this._tmpRect.w = pt.x - this._tmpRect.x;
                this._tmpRect.h = pt.y - this._tmpRect.y;

                this.filterArea = this._tmpRect;
            }

            for (const reel of this._reels)
            {
                reel.tick(ev.delta);
            }
            this._symbolContainer.sortChildren(this.settings.sortFunc || Sort.ByGlobalZIndex);

            if (!this._useShaderForSymbols)
            {
                this.updateMask();
            }
            else
            {
                for (const reel of this._reels)
                {
                    reel.updateMask();
                }
            }
        }

        /** Updates the mask shape (only used if the useShaderForSymbols property is false). */
        protected updateMask()
        {
            // Clear mask
            this._maskShape
                .clear()
                .beginFill(Util.Colors.white, 1.0);

            // Iterate each reel
            let curX = 0, commonHeight = 0, heightsEqual = true, commonTop = 0;
            for (let i = 0, l = this._reels.length; i < l; ++i)
            {
                // Draw it to the mask
                const reelProps = this.settings.reels[i];
                const reelHeight = (reelProps.stopSymbolCount * reelProps.symbolSpacing) + reelProps.maskExpandTop + reelProps.maskExpandBottom;
                const top = -reelProps.maskExpandTop;

                if (i === 0)
                {
                    commonHeight = reelHeight;
                    commonTop = top;
                }
                else if (commonHeight !== reelHeight || commonTop !== top)
                {
                    heightsEqual = false;
                }

                this._maskShape.drawRect(curX, top, reelProps.width, reelProps.stopSymbolCount * reelProps.symbolSpacing);
                curX += reelProps.width;
            }

            // Done
            this._maskShape.endFill();

            // If all reels have the same height, we can shortcut and do the whole thing in one rect
            if (heightsEqual)
            {
                this._maskShape
                    .clear()
                    .beginFill(Util.Colors.white, 1.0)
                    .drawRect(0, commonTop, curX, commonHeight)
                    .endFill();
            }
        }

        /**
         * Checks the conditions for stopping the reels when a stop is requested or stopping the reels in certain way
         * becomes true.
         */
        protected checkForStop(): void
        {
            if (!this._enabled) { return; }

            switch (this.state.value)
            {
                case IReel.State.Spinning:
                    if ((this._canStopNormally && this._stopRequested) || (this._canQuickstop && this._quickStopRequested))
                    {
                        this.stop();
                    }
                    break;
                case IReel.State.SlowingDown:
                    if (this._canQuickstop && this._quickStopRequested)
                    {
                        this.stop();
                    }
                    break;
            }
        }

        /**
         * Stops the reels immediately, no questions asked.
         */
        protected stopImmediately(noSuspense: boolean): void
        {
            if (!this._enabled) { return; }

            RS.Tween.removeTweens<Component>(this);
            this._startTween = null;
            this._stopTween = null;

            if (this._pendingStopSymbols)
            {
                this.commitPendingStopSymbols(true);
            }

            // True when all reels are currently stopped
            let allReelsStopped = true;

            if (noSuspense)
            {
                for (let i = 0, l = this._reels.length; i < l; ++i)
                {
                    if (this._reels[i].state.value !== IReel.State.Stopped)
                    {
                        allReelsStopped = false;
                        this.stopReel(i, IReel.StopType.Immediate);
                    }
                }

                // If there is at least one reel which was not stopped, calling stopReel() on it
                // will result in handleAllReelStopped being eventually called.
                // So we only need to trigger handleAllReelsStopped here when all reels were already stopped
                if (allReelsStopped)
                {
                    this.handleAllReelsStopped(IReel.StopType.Immediate);
                }
                return;
            }

            const stopTween = this._stopTween = RS.Tween.get<Component>(this, { override: true });

            this._reelsStopped = 0;
            // Count how many reels aren't spinning
            let reelsStopped = 0;

            for (let i = 0; i < this._reels.length; i++)
            {
                if (this._reels[i].state.value !== IReel.State.Stopped)
                {
                    allReelsStopped = false;
                    if (this._suspenseTimes[i] > 0)
                    {
                        stopTween.call((t) => this._reels[i].beginSuspense())
                                 .wait(this._suspenseTimes[i])
                                 .call((t) => this.stopReel(i, IReel.StopType.Standard));
                    }
                    else
                    {
                        stopTween.call((t) => this.stopReel(i, IReel.StopType.Immediate));
                        reelsStopped ++;
                    }
                }
                else
                {
                    if (this._suspenseTimes[i] > 0)
                    {
                        this._reels[i].spinUp(IReel.StartType.Immediate);
                        stopTween.call((t) => this._reels[i].beginSuspense())
                                 .wait(this._suspenseTimes[i])
                                 .call((t) => this.stopReel(i, IReel.StopType.Standard));
                    }
                    else
                    {
                        reelsStopped ++;
                        this._reelsStopped ++;
                    }
                }
            }

            if (reelsStopped < this.settings.reels.length)
            {
                // this._reelsStopped = reelsStopped;
                stopTween.call((t) => {
                    this.state.value = IReel.State.SlowingDown;
                });
            }
            else
            {
                // this._reelsStopped = reelsStopped;
                stopTween.call((t) => {
                    this._reelsStopped = this.settings.reels.length;
                    this.state.value = IReel.State.Stopped;
                    // Edge case: all reels were in the stopped state already, without suspense
                    // We have to manually call handleAllReelsStopped(), because the onStopped event of reels will not be published.
                    if (allReelsStopped)
                    {
                        this.handleAllReelsStopped(IReel.StopType.Immediate);
                    }
                });
            }

            for (const anim of this._symbolLandAnimations)
            {
                anim.anim.dispose();
            }
        }

        /**
         * Slows the reels down using the requested method.
         */
        protected stop()
        {
            if (!this._enabled) { return; }

            // If we're already slowing down, doing this logic again can cause oddities in specific reel slowing behaviour
            if (this.state.value === IReel.State.SlowingDown)
            {
                if (this._quickStopRequested)
                {

                    // We're already spinning down and the user requested quick-stop
                    // Let any reels which are spinning down keep spinning down

                    // if (this._pendingStopSymbols)
                    // {
                    //     this.commitPendingStopSymbols();
                    // }

                    for (let i = 0; i < this._reels.length; i++)
                    {
                        const reel = this._reels[i];
                        if (reel.state.value !== IReel.State.SlowingDown)
                        {
                            this.stopReel(i, IReel.StopType.Quick);
                        }
                    }

                }
                return;
            }

            this._canStopNormally = this._stopRequested ? false : this._canStopNormally;
            this._canQuickstop = this._quickStopRequested ? false : this._canQuickstop;

            if (this._pendingStopSymbols)
            {
                this.commitPendingStopSymbols();
            }

            this.state.value = IReel.State.SlowingDown;

            if (this._stopTween)
            {
                if (!this._stopTween.isDisposed) { this._stopTween.finish(); }
                this._stopTween = null;
            }

            // const stopTween = this._stopTween = RS.Tween.get<Component>(this, { override: true });
            // let reelHasSuspense = false;
            // let previousReelHasSuspense = false;

            // for (let i = 0; i < this._reels.length; i++)
            // {
            //     reelHasSuspense = reelHasSuspense || this._suspenseTimes[i] > 0;

            //     const reel = this._reels[i];

            //     if (reel.masterReel == null)
            //     {
            //         if (i === 0 || (this._quickStopRequested && !reelHasSuspense))
            //         {
            //             this.stopReel(i, this._quickStopRequested ? IReel.StopType.Quick : IReel.StopType.Standard);
            //         }
            //         else
            //         {
            //             if (this._suspenseTimes[i] > 0)
            //             {
            //                 stopTween
            //                     .wait(this.settings.reelStopDelta + reel.spinDownTime)
            //                     .call((t) =>
            //                     {
            //                         reel.beginSuspense();
            //                     })
            //                     .wait(this._suspenseTimes[i])
            //                     .call((t) =>
            //                     {
            //                         this.stopReel(i, IReel.StopType.Standard);
            //                     });
            //                 previousReelHasSuspense = true;
            //             }
            //             else
            //             {
            //                 stopTween
            //                     .wait(this.settings.reelStopDelta + (previousReelHasSuspense ? reel.spinDownTime : 0))
            //                     .call((t) =>
            //                     {
            //                         this.stopReel(i, IReel.StopType.Standard);
            //                     });
            //             }
            //         }
            //     }
            // }
            // this._stopTween.call((t) =>
            // {
            //     this._stopTween = null;
            // });

            this.stopSequence();
        }

        protected async stopSequence()
        {
            for (let i = 0; i < this._reels.length; i++)
            {
                const reel = this._reels[i];

                if (reel.state.value !== IReel.State.Spinning || reel.masterReel) { continue; }

                if (!this._skipNextSuspense && this._suspenseTimes[i] > 0)
                {
                    reel.beginSuspense();
                    await RS.ITicker.get().after(this._suspenseTimes[i]);
                }

                if (this._quickStopRequested && this._suspenseTimes[i] == 0)
                {
                    this.stopReel(i, IReel.StopType.Quick);
                    //if turbo mode is set up to have a stop delta, do it here
                    if (this._turboStopRequested && this.settings.turboStopDelta)
                    {
                        await RS.ITicker.get().after(this.settings.turboStopDelta);
                    }
                    continue;
                }

                this.stopReel(i, IReel.StopType.Standard);

                await Promise.race([ reel.onDecelerateStarted, reel.onStopped ]);
                // Check that the reel wasn't insta-stopped partway through the sequence, presumably
                if (this.state.value !== IReel.State.SlowingDown) { return; }

                await RS.ITicker.get().after(this.settings.reelStopDelta);
            }
        }

        /**
         * Analyses the stop symbols and looks for reels that should exhibit suspense.
         */
        protected analyseSuspense()
        {
            const scatterCounts: { [groupID: number]: number } = {};
            for (let i = 0; i < this._reels.length; i++)
            {
                const settings = this.settings.reels[i];
                const scatterAchieved = this.checkSuspenseAchieved(i, scatterCounts);
                if (settings.shouldSuspense && scatterAchieved)
                {
                    this._suspenseTimes[i] = settings.suspenseTime || 0;
                }
                else
                {
                    this._suspenseTimes[i] = 0;
                }

                const reel = this._pendingStopSymbols.getReel(i);
                const scatterGroups = [];
                for (const symbolID of reel)
                {
                    if (this.getIsScatter(symbolID, i))
                    {
                        const groupID = this.getScatterGroup(symbolID, i);

                        // Track groups already found.
                        if (scatterGroups.indexOf(groupID) > -1) { continue; }
                        scatterGroups.push(groupID);

                        // Add up scatter total.
                        if (scatterCounts[groupID] == null) { scatterCounts[groupID] = 0; }
                        scatterCounts[groupID]++;
                    }
                }
            }
        }

        protected checkSuspenseAchieved(reelID: number, scatterCounts: { [ groupID: number]: number }): boolean
        {
            for (const scatterGroup in scatterCounts)
            {
                const count = scatterCounts[scatterGroup];
                const winCount = Is.number(this.settings.scatterWinCount) ? this.settings.scatterWinCount : this.settings.scatterWinCount[scatterGroup];
                if (count >= winCount - 1)
                {
                    return true;
                }
            }

            return false;
        }

        protected getIsScatter(id: number, reelIndex: number): boolean
        {
            const symbolInfo = this.getSymbolDefinition(id, reelIndex);
            if (!symbolInfo) { return false; }
            return symbolInfo.isScatter;
        }

        protected getScatterGroup(id: number, reelIndex: number): number
        {
            const symbolInfo = this.getSymbolDefinition(id, reelIndex);
            return (symbolInfo && symbolInfo.scatterGroup) ? symbolInfo.scatterGroup : 0;
        }

        /**
         * Commits pending stop symbols to the reels. Defer flag is used to say don't set even if we're not spinning
         */
        protected commitPendingStopSymbols(defer?: boolean): void
        {
            this.analyseSuspense();
            for (let i = 0; i < this._reels.length; i++)
            {
                this._reels[i].setStopSymbols(this._pendingStopSymbols.getReel(i), (this._suspenseTimes[i] > 0) ? defer : false, this._currentStopIndices[i]);
            }
            this._pendingStopSymbols = null;
        }

        /**
         * Called once the spin state of a reel has changed.
         */
        protected handleReelSpinStateChanged(reelIndex: number, newSpinState: IReel.State): void
        {
            if (this.state.value === IReel.State.SpinningUp)
            {
                const spinningReels = this._reels.filter((r) => r.state.value === IReel.State.Spinning);
                const allReelsSpinning = spinningReels.length === (this._reels.length - this._lockedReelsCount);
                if (allReelsSpinning)
                {
                    this.log("all reels are now spinning");
                    this.state.value = IReel.State.Spinning;
                    if (this._spinTimer.elapsed > this.settings.minimumStopTime)
                    {
                        this.log("we have surpassed minimumStopTime, so canStop and canQuickstop = true");
                        this._canQuickstop = true;
                        this._canStopNormally = true;
                        this.checkForStop();
                    }
                    else
                    {
                        RS.Tween.wait(this.settings.minimumStopTime - this._spinTimer.elapsed, this as Component).call(() =>
                        {
                            this.log("minimumStopTime reached");
                            if (this.state.value === IReel.State.Spinning)
                            {
                                this._canStopNormally = true;
                                this.log("we are spinning, so canStop = true");
                                this.checkForStop();
                            }
                        });
                    }
                }
            }
        }

        /**
         * Called once a reel has begun to spin down.
         * @param reelIndex
         */
        protected handleReelDecelerateStarted(reelIndex: number): void
        {
            //
        }

        /**
         * Called once a reel has stopped.
         */
        protected handleReelStopped(reelIndex: number, stopType: IReel.StopType): void
        {
            if (!this._enabled) { return; }

            if (this._reelsStopped === this.settings.reels.length)
            {
                throw new Error("All reels had already stopped when a reel said it was stopped");
            }

            this._reelsStopped++;
            this.onReelStopped.publish(reelIndex);

            if (stopType !== IReel.StopType.Immediate)
            {
                const reel = this._reels[reelIndex];
                const rowCount = reel.settings.stopSymbolCount;
                const db = Symbols.IDatabase.get();
                for (let i = 0; i < rowCount; ++i)
                {
                    const symbolID = this._currentWindow.get(reelIndex, i);
                    const symbolDef = reel.getSymbolDefinition(symbolID);
                    const guid = db.get(symbolDef);
                    const landSymbolAnimators = this._symbolLandAnimators[guid];
                    if (landSymbolAnimators)
                    {
                        for (const animator of landSymbolAnimators)
                        {
                            const anim = animator.animateSymbol(this, reelIndex, i);

                            // allow the symbol land animator to have conditions for animating
                            if (anim != null)
                            {
                                // allow the symbol land animator to return null.
                                this._symbolLandAnimations.push({
                                    anim,
                                    promise: anim.animate()
                                });
                            }
                        }
                    }
                }
            }

            if (this._reelsStopped === this.settings.reels.length)
            {
                if (this._symbolLandAnimations.length === 0)
                {
                    this.handleAllReelsStopped(stopType);
                }
                else
                {
                    Async.multiple(this._symbolLandAnimations.map((sla) => sla.promise))
                        .then(() => this.handleAllReelsStopped(stopType));
                }
            }
        }

        /**
         * Called once a symbol has landed.
         */
        protected handleSymbolLanded(reelIndex: number, rowIndex: number): void
        {
            if (!this._enabled) { return; }

            this.onSymbolLanded.publish({reelIndex, rowIndex});
        }

        /**
         * Called once all reels have stopped.
         */
        protected handleAllReelsStopped(stopType: IReel.StopType): void
        {
            if (!this._enabled) { return; }

            for (const anim of this._symbolLandAnimations)
            {
                anim.anim.dispose();
            }
            this._symbolLandAnimations.length = 0;

            RS.Tween.removeTweens<Component>(this);
            this._startTween = null;
            this._stopTween = null;
            this._spinTimer.stop();

            this._reelsStopped = this.settings.reels.length;
            this.state.value = IReel.State.Stopped;
            this._skipNextSuspense = false;

            this.onStopped.publish();
        }
    }
}
