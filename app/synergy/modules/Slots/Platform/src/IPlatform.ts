namespace RS.Slots
{
    /**
     * TODO: remove this
     * @deprecated
     */
    export import PlatformGameState = RS.PlatformGameState;

    export enum PlatformAutoplaySupport
    {
        None,
        GameProvides,
        PlatformProvides
    }

    interface BasePlatformWagerUpdate
    {
        /** Type determiner. Denotes the type of wager update this is. */
        type: PlatformWagerUpdate.Type;
    }

    /** Represents a platform request for the stake to be set to the given value. */
    export interface PlatformStakeUpdate extends BasePlatformWagerUpdate
    {
        type: PlatformWagerUpdate.Type.Stake;
        stake: number;
        paylineCount?: number;
    }

    /** Represents a platform request for the stake to be set to the given total bet. */
    export interface PlatformTotalBetUpdate extends BasePlatformWagerUpdate
    {
        type: PlatformWagerUpdate.Type.TotalBet;
        totalBet: number;
    }

    /** Represents a platform request for the stake to be reset to default. */
    export interface PlatformWagerReset extends BasePlatformWagerUpdate
    {
        type: PlatformWagerUpdate.Type.Reset;
        resetType: PlatformWagerReset.Type;
    }

    export namespace PlatformWagerReset
    {
        /**
         * Reserved negative stake values that indicate the stake should be reset in a certain way.
         */
        export enum Type
        {
            Default
            // Previous
        }
    }

    /**
     * Represents a stake update request from the platform.
     * Note that this will not necessarily be applied immediately.
     * It is the responsibility of the game to apply this stake at the soonest appropriate time.
     */
    export type PlatformWagerUpdate = PlatformStakeUpdate | PlatformTotalBetUpdate | PlatformWagerReset;

    export namespace PlatformWagerUpdate
    {
        export enum Type
        {
            /** Request to reset the stake. */
            Reset,
            /** Request to set the stake by total bet. */
            TotalBet,
            /** Request to set the stake by value. */
            Stake
        }

        /**
         * To be thrown on stake request when the platform requests a stake that is not valid.
         */
        export class InvalidStakeError extends BaseError {}
    }

    /**
     * Encapsulates the platform that the game sits upon.
     * This could be a wrapper, integration or environment.
     * This is specialised for slot games.
     */
    export interface IPlatform
    {
        /**
         * Gets generic session data.
         * @deprecated SG (RGS) specific; will be removed from the generic IPlatform interface at some point in the future.
         */
        readonly sessionInfo: RS.IPlatform.SessionInfo;

        /**
         * Gets if autoplay is supported by the platform.
         */
        readonly supportsAutoplay: PlatformAutoplaySupport;

        /**
         * Gets or sets the autoplay controller.
         */
        autoplayController: Autoplay.IController | null;

        /**
         * Initialises the platform.
         * This is called when the game class has been initialised.
         */
        init(settings: IPlatform.Settings): void;

        /**
         * Signals that a response has been received to a network request.
         * This should be used for network requests sent to the engine.
         * @deprecated SG (RGS) specific; will be removed from the generic IPlatform interface at some point in the future.
         */
        networkResponseReceived(request: XMLHttpRequest): void;

        /**
         * Requests that the platform initiates autoplay.
         * Will typically open a menu with autoplay settings.
         */
        requestAutoplay(): void;

        requestAutoplayStop(): void;
    }

    export const IPlatform = Strategy.declare<IPlatform>(Strategy.Type.Singleton, false, true);

    export namespace IPlatform
    {
        export interface Settings
        {
            /** Encapsulates the customer's primary balance. */
            balanceObservable: IObservable<number>;

            /** Encapsulates the customer's balances. */
            balancesObservable: ObservableMap<number>;

            /** Encapsulates the win amount for the current game. */
            winObservable: IObservable<number>;

            /** Encapsulates the total bet amount for the current game. */
            stakeObservable: IObservable<number>;

            /** Encapsulates the state of the game. */
            gameStateObservable: IObservable<PlatformGameState>;

            /** Current number of autoplays remaining. */
            autoplaysRemainingObservable: IObservable<number>;

            /** Whether or not the user can interact with the game. */
            interactionEnabledArbiter: IArbiter<boolean>;

            /** Whether or not the user can change bet. */
            stakeChangeEnabledArbiter?: IArbiter<boolean>;

            /** Whether or not the user can open the paytable. */
            paytableEnabledArbiter?: IArbiter<boolean>;

            /** Whether or not the paytable is currently visible. */
            paytableVisible?: IReadonlyObservable<boolean>;

            /** Container to put dialogs or other UI in. */
            uiContainer: Flow.Container;

            /** Whether or not the user can autoplay. */
            allowAutoplayArbiter: IArbiter<boolean>;

            /**
             * Called when a stake change is requested by the platform. Information is passed regarding the stake update.
             */
            wagerUpdateCallback(update: PlatformWagerUpdate): void | PromiseLike<void>;
        }
    }
}
