namespace RS.Build.Scaffolds
{
    const winRendererTemplate = `namespace {{namespace}}.WinRenderers
{
    import Reels = RS.Reels;
    import Slots = RS.Slots;
    import Models = Slots.Models;
    import Math = RS.Math;

    export class {{className:CamelCase}} extends Slots.WinRenderers.Base<{{className:CamelCase}}.Settings>
    {
        constructor(settings: {{className:CamelCase}}.Settings, reels: RS.Reels.IGenericComponent, container: RS.Rendering.IContainer)
        {
            super(settings, reels, container);

            // TODO
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected async renderWin(ctx: Slots.WinRenderers.WinRenderingContext, win: Models.PaylineWin)
        {
            // TODO
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         */
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            // TODO
            return 0;
        }
    }

    export namespace {{className:CamelCase}}
    {
        /**
         * Settings for the {{className}} win renderer.
         */
        export interface Settings extends Slots.WinRenderers.Base.Settings
        {
            
        }
    }
}`;

    export const winRenderer = new Scaffold();
    winRenderer.addTemplate(winRendererTemplate, "src/winrenderers/{{className}}.ts");
    winRenderer.addParam("className", "Class Name", "MyClass");
    registerScaffold("winrenderer", winRenderer);
}