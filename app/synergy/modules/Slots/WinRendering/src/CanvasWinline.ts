namespace RS.Slots
{
    /**
     * Draws a winline on a canvas ready for display.
     */
    export class CanvasWinline extends RS.Rendering.Graphics
    {
        /**
         * Width of the winline on the canvas.
         */
        public static WINLINE_WIDTH = 12;
        /**
         * Width of the outline/ border around the winline.
         * @type {number}
         */
        public static OUTLINE_WIDTH = 16;
        /**
         * Colour of the winline outline.
         */
        public static OUTLINE_COLOR = RS.Util.Colors.white;

        /**
         * Positions on the reels of this winline. A winline in this sense always passes through all symbols, so this
         * will contain one number for each reel.
         */
        protected _winline: Models.Winline;
        /**
         * Colour of this winline.
         */
        protected _winlineColor: RS.Util.Color;
        /**
         * Width of a symbol in pixels to determine horizontal stop points of the line.
         */
        protected _symbolWidth: number;
        /**
         * Height of a symbol in pixels to determine vertical stop points.
         */
        protected _symbolHeight: number;
        /**
         * Precalculated half symbol width.
         */
        protected _halfSymbolWidth: number;
        /**
         * Precalculated half symbol height.
         */
        protected _halfSymbolHeight: number;

        constructor(winline: Models.Winline, winlineColor: RS.Util.Color, symbolWidth: number, symbolHeight: number)
        {
            super();

            this._winline = winline;
            this._winlineColor = winlineColor;
            this._symbolWidth = symbolWidth;
            this._symbolHeight = symbolHeight;
            this._halfSymbolWidth = this._symbolWidth / 2;
            this._halfSymbolHeight = this._symbolHeight / 2;
            this.buildWinline();
        }

        private buildWinline()
        {
            const firstPos = this._winline[0];
            const lastPos = this._winline[this._winline.length - 1];

            // === Outline === \\
            this.lineStyle(CanvasWinline.OUTLINE_WIDTH, CanvasWinline.OUTLINE_COLOR);
            // horizontal line to the first position
            this.moveTo(
                firstPos.reelIndex * this._symbolWidth,
                firstPos.rowIndex * this._symbolHeight + this._halfSymbolHeight
            );
            // all lines
            for (const pos of this._winline)
            {
                this.lineTo(
                    pos.reelIndex * this._symbolWidth + this._halfSymbolWidth,
                    pos.rowIndex * this._symbolHeight + this._halfSymbolHeight
                );
            }
            // horizontal line from the last position
            this.lineTo(
                (lastPos.reelIndex + 1) * this._symbolWidth,
                lastPos.rowIndex * this._symbolHeight + this._halfSymbolHeight
            );

            // === Filled line === \\
            this.lineStyle(CanvasWinline.WINLINE_WIDTH, this._winlineColor);
            this.moveTo(
                firstPos.reelIndex * this._symbolWidth,
                firstPos.rowIndex * this._symbolHeight + this._halfSymbolHeight
            );
            for (const pos of this._winline)
            {
                this.lineTo(
                    pos.reelIndex * this._symbolWidth + this._halfSymbolWidth,
                    pos.rowIndex * this._symbolHeight + this._halfSymbolHeight
                );
            }
            this.lineTo(
                (lastPos.reelIndex + 1) * this._symbolWidth,
                lastPos.rowIndex * this._symbolHeight + this._halfSymbolHeight
            );
        }
    }
}
