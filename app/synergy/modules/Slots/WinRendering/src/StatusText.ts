/// <reference path="Base.ts" />

namespace RS.Slots.WinRenderers
{
    /**
     * Provides status text that displays information about the currently rendered winline.
     */
    export class StatusText extends Base<StatusText.Settings>
    {
        public constructor(settings: StatusText.Settings)
        {
            settings.allowEarlyTermination = true;
            super(settings, null, null);
        }

        /** Renders the specified batch of wins. */
        public renderWins(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential, totalEstimatedTime: number = 0): Task<void>|null
        {
            // Are they requesting sequential wins
            if (requestedStagger === StaggerTime.Sequential)
            {
                // Early-outs
                if (wins.length === 0) { return null; }
                if (wins.length === 1)
                {
                    return this.createTaskForWin(wins[0], totalEstimatedTime);
                }
                
                // Create a sub-task for each win
                const winTasks: Task<void>[] = wins.map((w) => this.createTaskForWin(w, totalEstimatedTime));
                return new MultiTask(winTasks, false);
            }
            else
            {
                // Create a single task for all wins
                const ctx = new WinRenderingContext();
                ctx.totalEstimatedTime = totalEstimatedTime;
                const task = Task.fromPromiseProvider(() =>
                {
                    const promiseOrTask = this.doRenderWins(ctx, wins);
                    if (promiseOrTask == null) { return null; }
                    if (promiseOrTask instanceof Task)
                    {
                        promiseOrTask.start();
                        task.onCancelled(() => promiseOrTask.cancel());
                    }
                    return promiseOrTask;
                });
                task.name = `NonPerWinDoRenderWinsTask`;
                ctx.task = task;
                task.allowEarlyTermination = this._settings.allowEarlyTermination ? true : false;
                return task;
            }
        }

        /**
         * Renders the specified set of wins in non-per-win mode.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The wins to render.
         */
        protected async doRenderWins(ctx: WinRenderingContext, wins: Models.PaylineWin[])
        {
            if (wins.length === 1)
            {
                return await this.renderWin(ctx, wins[0]);
            }
            
            const str = Translations.statusText.multipleWinlines.get(this._settings.locale, {
                lineCount: wins.length,
                payout: this._settings.currencyFormatter.format(wins.map((win) => win.totalWin).reduce((a, b) => a + b))
            });

            const handle = this._settings.statusTextArbiter.declare(str);
            await ctx.untilCancelled();
            handle.dispose();
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected async renderWin(ctx: WinRenderingContext, win: Models.PaylineWin)
        {
            const str = Translations.statusText.winline.get(this._settings.locale, {
                symbolName: Localisation.resolveLocalisableString(this._settings.locale, this.settings.configModel.symbolNames[win.symbolID]),
                count: win.positions.length,
                lineIndex: win.lineIndex + 1,
                payout: this._settings.currencyFormatter.format(win.totalWin)
            });

            const handle = this._settings.statusTextArbiter.declare(str);
            await ctx.untilCancelled();
            handle.dispose();
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
        **/
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            // We are "endless", e.g. we only stop when told to
            return 0;
        }
    }

    export namespace StatusText
    {
        export interface Settings extends Base.Settings
        {
            configModel: Models.Config;
            locale: Localisation.Locale;
            currencyFormatter: Localisation.CurrencyFormatter;
            statusTextArbiter: Arbiter<Localisation.LocalisableString>;
        }
    }
}