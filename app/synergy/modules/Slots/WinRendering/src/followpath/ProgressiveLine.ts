namespace RS.Slots
{
    /**
     * Draws a progressive line with particles.
     */
    export class ProgressiveLine extends RS.Rendering.Container
    {
        protected _settings: ProgressiveLine.Settings;
        protected _segmentCount: number;
        protected _reelDimensions: RS.Math.Vector2D;

        // containers
        @RS.AutoDispose protected _lineContainer          : RS.Rendering.IContainer; // global container for the winline elements
        @RS.AutoDispose protected _backContainer          : RS.Rendering.IContainer; // container for joints under the winline
        @RS.AutoDispose protected _frontContainer         : RS.Rendering.IContainer; // container for joints on top of the winline
        @RS.AutoDispose protected _particleBackContainer  : RS.Rendering.IContainer; // container for particles under the front joints
        @RS.AutoDispose protected _particleFrontContainer : RS.Rendering.IContainer; // container for particles on top of the front joints
        // mesh segments and attributes for the winline
        @RS.AutoDispose protected _lineMeshes: RS.Rendering.IMesh[] = [];
        @RS.AutoDispose protected _linePoints: RS.Math.Vector2D[] = [];
        
        // graphical elements
        @RS.AutoDispose protected _lineCursor: RS.Rendering.IBitmap;
        @RS.AutoDispose protected _clippingMask: RS.Rendering.IGraphics;

        @RS.AutoDispose protected _trailParticleSystems: Array<RS.Particles.System<RS.Particles.Base>>;
        protected _trailEmitters: Array<RS.Particles.Emitter>;
        @RS.AutoDispose protected _symbolsParticleSystems: Array<RS.Particles.System<RS.Particles.Base>>;
        protected _symbolsParticleEmitters: Array<RS.Particles.Emitter>;

        protected _maskWidth: number;
        protected _jointWidth: number;
        protected _stopParticleEmission: boolean = false;
        protected _directRender: boolean;
        protected _displayParticles: boolean;

        constructor(settings: ProgressiveLine.Settings, reels: Reels.IGenericComponent, directRender: boolean = false, displayParticles: boolean = true)
        {
            super();
            this._settings = settings;
            this._reelDimensions = RS.Math.Vector2D(reels.width, reels.height);
            this._directRender = directRender;
            this._displayParticles = displayParticles && this._settings.displayParticles;
            if (this._displayParticles)
            {
                this.initParticleSystem();
            }
        }

        public async start(ctx: RS.Slots.WinRenderers.WinRenderingContext, points: RS.Math.Vector2D[], winline: RS.Slots.ReelSetPosition[], winPositions: number)
        {
            this._stopParticleEmission = false;
            this._segmentCount = winline.length - 1;

            this._linePoints = points;

            this.createWinLinesComponents(winline);
            if (!this._directRender)
            {
                this.createClippingMask();
            }
            if (this._displayParticles)
            {
                this.createParticleEmitters(points);
            }

            if (this._directRender)
            {
                try
                {
                    for (let i = 0; i < winPositions; i++)
                    {
                        this.displayParticlesOnWinSymbols(winline, i);
                    }
                    await ctx.getTween(this._lineContainer)
                        .wait(this.estimateWinRenderTime())
                        .call(() => {
                            // this._lineContainer.cacheAsBitmap = true;
                        })
                        .to({alpha: 0}, this._settings.fadeOutTime);
                }
                catch { /* Cancelled */ }
                finally
                {
                    for (const mesh of this._lineMeshes)
                    {
                        this._lineContainer.removeChild(mesh);
                        mesh.dispose();
                    }

                    this._lineContainer.removeChild(this._backContainer);
                    this._backContainer.dispose();

                    this._lineContainer.removeChild(this._frontContainer);
                    this._frontContainer.dispose();

                    this.removeChild(this._lineContainer);
                    this._lineContainer.dispose();
                }
            }
            else
            {
                try
                {
                    const totalSegments = this._linePoints.length;
                    const tween1 = ctx.getTween(this._lineCursor, {onChange: this.updateLine.bind(this)});
                    if (this._settings.drawBorderLines)
                    {
                        tween1.to({x: points[1].x, y: points[1].y}, this._settings.duration / totalSegments);
                    }
                    for (let i = 0; i < this._segmentCount; i++)
                    {
                        const index = this._settings.drawBorderLines ? i + 1 : i;
                        tween1
                            .call(() =>
                            {
                                if (i < winPositions)
                                {
                                    this.displayParticlesOnWinSymbols(winline, i);
                                }
                            })
                            .to({x: points[index + 1].x, y: points[index + 1].y}, this._settings.duration / totalSegments);
                    }
                    if (this._settings.drawBorderLines)
                    {
                        tween1.to({x: points[totalSegments - 1].x, y: points[totalSegments - 1].y}, this._settings.duration / totalSegments);
                    }
                    tween1
                        .call(() =>
                        {
                            if (this._settings.drawBorderLines)
                            {
                                this.displayParticlesOnWinSymbols(winline, winPositions - 1);
                            }
                            this._lineContainer.mask = null;
                            this._lineContainer.removeChild(this._clippingMask);
                        })
                        .wait(this._settings.endDelay)
                        .call(() => {
                            this._stopParticleEmission = true;
                        });
                    await tween1;

                    ctx.getTween(this._lineCursor)
                        .to({alpha: 0}, this._settings.fadeOutTime);
                    await ctx.getTween(this._lineContainer)
                        .call(() => {
                            // this._lineContainer.cacheAsBitmap = true;
                        })
                        .to({alpha: 0}, this._settings.fadeOutTime);
                }
                catch { /* Cancelled */ }
                finally
                {
                    for (const mesh of this._lineMeshes)
                    {
                        this._lineContainer.removeChild(mesh);
                        mesh.dispose();
                    }

                    this._lineContainer.removeChild(this._backContainer);
                    this._backContainer.dispose();

                    this._lineContainer.removeChild(this._frontContainer);
                    this._frontContainer.dispose();

                    this.removeChild(this._lineContainer);
                    this._lineContainer.dispose();

                    this.removeChild(this._lineCursor);
                    this._lineCursor.dispose();

                    if (this._clippingMask)
                    {
                        this._clippingMask.dispose();
                        this._clippingMask = null;
                    }

                    if (this._displayParticles)
                    {
                        this.destroyParticles();
                        if (this._particleBackContainer)
                        {
                            this._particleBackContainer.removeAllChildren();
                            this.removeChild(this._particleBackContainer);
                        }
                        if (this._particleFrontContainer)
                        {
                            this._particleFrontContainer.removeAllChildren();
                            this.removeChild(this._particleFrontContainer);
                        }
                    }
                }
            }
        }

        /**
         * Estimates how long the specified win will take to render.
         */
        protected estimateWinRenderTime(): number
        {
            return this._settings.duration + this._settings.fadeOutTime + this._settings.endDelay;
        }

        // ================================================ \\
        // =============== ELEMENTS CREATION ============== \\
        // ================================================ \\

        /**
         * Creates the Particle Systems
         */
        protected initParticleSystem(): void
        {
            if (this._settings.displayParticles)
            {
                if (this._settings.trailParticles)
                {
                    this._trailParticleSystems = [];
                    for (const setting in this._settings.trailParticles)
                    {
                        const trailParticleSystem = this._settings.trailParticles[setting].system.create();
                        trailParticleSystem.blendMode = RS.Rendering.BlendMode.Add;
                        this._trailParticleSystems.push(trailParticleSystem);
                    }
                }

                if (this._settings.symbolParticles)
                {
                    this._symbolsParticleSystems = [];
                    for (const setting in this._settings.symbolParticles)
                    {
                        const symbolsParticleSystem = this._settings.symbolParticles[setting].system.create();
                        symbolsParticleSystem.blendMode = RS.Rendering.BlendMode.Add;
                        this._symbolsParticleSystems.push(symbolsParticleSystem);
                    }
                }
            }
            else
            {
                this.destroyParticles();
            }
        }

        /**
         * Destroys the particle systems and emitters
         */
        protected destroyParticles(): void
        {
            if (this._trailEmitters)
            {
                for (const particles in this._trailParticleSystems)
                {
                    this._trailEmitters[particles].dispose();
                    this._trailParticleSystems[particles].dispose();
                    this._trailEmitters[particles] = null;
                }
            }
            this._trailEmitters = null;
            if (this._symbolsParticleEmitters)
            {
                for (const particles in this._symbolsParticleSystems)
                {
                    this._symbolsParticleEmitters[particles].dispose();
                    this._symbolsParticleSystems[particles].dispose();
                    this._symbolsParticleEmitters[particles] = null;
                }
            }
            this._symbolsParticleEmitters = null;
        }

         /**
         * Creates the Particle Emitters
         */
        protected createParticleEmitters(points: RS.Math.Vector2D[]): void
        {
            if (!this._displayParticles || points.length === 0)
            {
                return;
            }

            if (!this._trailEmitters && this._trailParticleSystems)
            {
                this._trailEmitters = [];
                for (const emitter in this._settings.trailParticles)
                {
                    const trailEmitter = this.createEmitter(this._trailParticleSystems[emitter], this._settings.trailParticles[emitter], points);
                    if (this._settings.trailParticles[emitter].emitter.emitTime != null)
                    {
                        trailEmitter.enabled = true;
                    }
                    this._trailEmitters.push(trailEmitter);
                }
            }

            if (!this._symbolsParticleEmitters && this._symbolsParticleSystems)
            {
                this._symbolsParticleEmitters = [];
                for (const emitter in this._settings.symbolParticles)
                {
                    this._symbolsParticleEmitters.push(this.createEmitter(this._symbolsParticleSystems[emitter], this._settings.symbolParticles[emitter], points));
                }
            }
        }

        protected createEmitter(system: RS.Particles.System<RS.Particles.Base>, settings: ProgressiveLine.ParticleSettings, points: RS.Math.Vector2D[]): RS.Particles.Emitter
        {
            switch (settings.type)
            {
                case "radial":
                    return new RS.Particles.Emitters.Radial(system, {
                        ...settings.emitter as RS.Particles.Emitters.Radial.Settings<RS.Particles.Base>,
                        origin: {x: points[0].x, y: points[0].y}
                    });

                // case "bezier":
                //     return new RS.Particles.Emitters.Bezier(system, {
                //         ...settings.emitter as RS.Particles.Emitters.Bezier.Settings<RS.Particles.Base>
                //     });

                default:
                    return new RS.Particles.Emitters.Box(system, {
                        ...settings.emitter as RS.Particles.Emitters.Box.Settings<RS.Particles.Base>,
                        rect: new RS.Rendering.Rectangle(
                            points[0].x, points[0].y,
                            (settings.emitter as RS.Particles.Emitters.Box.Settings<RS.Particles.Base>).rect.w,
                            (settings.emitter as RS.Particles.Emitters.Box.Settings<RS.Particles.Base>).rect.h)
                    });
            }
        }

        /**
         * Creates the Clipping Mask
         */
        protected createClippingMask(): void
        {
            const offset: number = this._settings.borderLinesOffset | 0;
            const extra: number = Math.max(0, this._jointWidth / 2 - offset);
            this._maskWidth = this._reelDimensions.x + extra * 2;

            this._clippingMask = new RS.Rendering.Graphics();
            this._clippingMask.beginFill(RS.Util.Colors.white);
            this._clippingMask.drawRect(-extra, 0, this._maskWidth, this._reelDimensions.y);
            this._clippingMask.endFill();
            this._clippingMask.scaleX = 0;
            // this._clippingMask.isMask = true;
            this._lineContainer.addChild(this._clippingMask);
            this._lineContainer.mask = this._clippingMask;
        }

        /**
         * Creates the lines Components
         * @param win                   Payline Win Model
         */
        protected createWinLinesComponents(winline: RS.Slots.ReelSetPosition[]): void
        {
            // Main Line Container
            this._lineContainer = new RS.Rendering.Container();
            this._lineContainer.name = "Line Container";
            this.addChild(this._lineContainer);
            this._lineContainer.alpha = 1;

            // Line Body Texture
            const lineTexture = this.getImageFromSetting(this._settings.linePartAsset).texture;
            
            // Background Container
            this._backContainer = new RS.Rendering.Container();
            this._backContainer.name = "Line Background Container";
            this._lineContainer.addChild(this._backContainer);

            // Lines
            this._lineMeshes = this.getWinlineMeshes(winline, lineTexture);
            for (const mesh of this._lineMeshes)
            {
                this._lineContainer.addChild(mesh);
            }

            // Foreground Container
            this._frontContainer = new RS.Rendering.Container();
            this._frontContainer.name = "Line Front Container";
            this._lineContainer.addChild(this._frontContainer);

            // Particle System behind the joints
            if (this._displayParticles && this._trailParticleSystems)
            {
                this._particleBackContainer = new RS.Rendering.Container();
                this.addChild(this._particleBackContainer);
                for (const system in this._trailParticleSystems)
                {
                    this._particleBackContainer.addChild(this._trailParticleSystems[system]);
                }
            }

            // Joints
            const points: RS.Math.Vector2D[] = this._linePoints;
            this._jointWidth = 0;
            for (let i = 0; i < points.length; i++)
            {
                // if not displaying all joints and the direction of the line is
                // the same as the next one, don't display this joint
                const noJoint: boolean =
                    (!this._settings.displayAllJoints && i !== points.length - 1 && i !== 0 &&
                    (points[i+1].y - points[i].y === points[i].y - points[i-1].y));
                if (!noJoint)
                {
                    if (this._settings.backJointAsset)
                    {
                        const circleBack : RS.Rendering.IBitmap = this.getImageFromSetting(this._settings.backJointAsset);
                        circleBack.pivot.x = circleBack.width * 0.5;
                        circleBack.pivot.y = circleBack.height * 0.5;
                        circleBack.x = points[i].x + (this._settings.backJointAsset.offset != null ? this._settings.backJointAsset.offset.x : 0);
                        circleBack.y = points[i].y + (this._settings.backJointAsset.offset != null ? this._settings.backJointAsset.offset.y : 0);
                        if (this._settings.backJointAsset.scale)
                        {
                            circleBack.scaleX = this._settings.backJointAsset.scale.x;
                            circleBack.scaleY = this._settings.backJointAsset.scale.y;
                        }
                        this._backContainer.addChild(circleBack);
                        if (circleBack.texture.width > this._jointWidth)
                        {
                             this._jointWidth = circleBack.texture.width;
                        }
                    }
                    if (this._settings.frontJointAsset)
                    {
                        const circle : RS.Rendering.IBitmap = this.getImageFromSetting(this._settings.frontJointAsset);
                        circle.pivot.x = circle.width * 0.5;
                        circle.pivot.y = circle.height * 0.5;
                        circle.x = points[i].x + (this._settings.frontJointAsset.offset != null ? this._settings.frontJointAsset.offset.x : 0);
                        circle.y = points[i].y + (this._settings.frontJointAsset.offset != null ? this._settings.frontJointAsset.offset.y : 0);
                        if (this._settings.frontJointAsset.scale)
                        {
                            circle.scaleX = this._settings.frontJointAsset.scale.x;
                            circle.scaleY = this._settings.frontJointAsset.scale.y;
                        }
                        this._frontContainer.addChild(circle);
                        if (circle.texture.width > this._jointWidth)
                        {
                             this._jointWidth = circle.texture.width;
                        }
                    }
                }
            }

            // Particle System on symbols
            if (this._displayParticles && this._symbolsParticleSystems)
            {
                this._particleFrontContainer = new RS.Rendering.Container();
                this.addChild(this._particleFrontContainer);
                for (const system in this._symbolsParticleSystems)
                {
                    this._particleFrontContainer.addChild(this._symbolsParticleSystems[system]);
                }
            }

            if (!this._directRender)
            {
                // Winline cursor
                this._lineCursor = this.getImageFromSetting(this._settings.lineCursorAsset);
                this._lineCursor.pivot.x = this._lineCursor.width * 0.5;
                this._lineCursor.pivot.y = this._lineCursor.height * 0.5;
                this._lineCursor.x = points[0].x + (this._settings.lineCursorAsset.offset != null ? this._settings.lineCursorAsset.offset.x : 0);
                this._lineCursor.y = points[0].y + (this._settings.lineCursorAsset.offset != null ? this._settings.lineCursorAsset.offset.y : 0);
                if (this._settings.lineCursorAsset.scale)
                {
                    this._lineCursor.scaleX = this._settings.lineCursorAsset.scale.x;
                    this._lineCursor.scaleY = this._settings.lineCursorAsset.scale.y;
                }
                this._lineCursor.blendMode = RS.Rendering.BlendMode.Add;
                this.addChild(this._lineCursor);
            }
        }

        /**
         * Creates and return the win line meshes
         * @param winline               Reel Positions
         * @param texture               Texture for the mesh
         */
        protected getWinlineMeshes(winline: RS.Slots.ReelSetPosition[], texture: RS.Rendering.ISubTexture): RS.Rendering.IMesh[]
        {
            const lineMeshes: RS.Rendering.IMesh[] = [];

            // TODO: texture wrap mode
            // texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
            for (let i = 0; i < this._linePoints.length - 1; i++)
            {
                const points: RS.Math.Vector2D[] = this._linePoints.slice(i, i+2);
                const vertices: Float32Array = new Float32Array(8);
                const uvs: Float32Array = new Float32Array(8);
                this.calculateVertices(vertices, uvs, points, texture);

                const winlineMesh = new RS.Rendering.Mesh({
                    positions: vertices,
                    texCoords: uvs
                }, texture);
                // texture.requiresUpdate = true;

                lineMeshes.push(winlineMesh);
            }
            // const winlineMesh: PIXI.mesh.Mesh = new PIXI.mesh.Rope(texture, <PIXI.Point[]>this._linePoints);
            return lineMeshes;
        }

        /**
         * Caculates and returns the vertices of the line between points meshes
         * @param vertices              Vertices
         * @param uvs                   Texture coordinates
         * @param points                Start and end point
         * @param texture               Texture for the mesh
         */
        protected calculateVertices(vertices: Float32Array, uvs: Float32Array, points: RS.Math.Vector2D[], texture: RS.Rendering.ISubTexture): void
        {
            const distP1P2: number = RS.Math.Vector2D.distanceBetween(points[1], points[0]);
            const vP1P2 : RS.Math.Vector2D = RS.Math.Vector2D((points[1].x - points[0].x)/distP1P2, (points[1].y - points[0].y)/distP1P2 );
            const nP1P2: RS.Math.Vector2D = RS.Math.Vector2D(vP1P2.y, -vP1P2.x);
            const halfHeight: number = texture.height*0.5;
            // const textureFraction: number = 1;
            const textureFraction: number = distP1P2 / texture.width;

            // v1
            const v1: RS.Math.Vector2D = RS.Math.Vector2D();
            v1.x = points[0].x +(halfHeight * nP1P2.x);
            v1.y = points[0].y +(halfHeight * nP1P2.y);
            // v2
            const v2: RS.Math.Vector2D = RS.Math.Vector2D();
            v2.x = points[1].x +(halfHeight * nP1P2.x);
            v2.y = points[1].y +(halfHeight * nP1P2.y);
            // v3
            const v3: RS.Math.Vector2D = RS.Math.Vector2D();
            v3.x = points[1].x +(halfHeight * -nP1P2.x);
            v3.y = points[1].y +(halfHeight * -nP1P2.y);
            // v4
            const v4: RS.Math.Vector2D = RS.Math.Vector2D();
            v4.x = points[0].x +(halfHeight * -nP1P2.x);
            v4.y = points[0].y +(halfHeight * -nP1P2.y);

            vertices[0] = v2.x;
            vertices[1] = v2.y;
            vertices[2] = v1.x;
            vertices[3] = v1.y;

            vertices[4] = v3.x;
            vertices[5] = v3.y;
            vertices[6] = v4.x;
            vertices[7] = v4.y;

            uvs[0] = 0;
            uvs[1] = 1;
            uvs[2] = textureFraction;
            uvs[3] = 1;
            uvs[4] = textureFraction;
            uvs[5] = 0;
            uvs[6] = 0;
            uvs[7] = 0;
        }

        protected getImageFromSetting(setting: ProgressiveLine.AssetImage): RS.Rendering.IBitmap
        {
            if (setting.frameID == null)
            {
                return RS.Factory.bitmap(setting.assetID as Asset.ImageReference);
            }
            else
            {
                return RS.Factory.bitmap(setting.assetID as Asset.SpriteSheetReference, setting.frameID, setting.frameNumber);
            }
        }

        // ================================================== \\
        // =================== ELEMENTS UPDATE ============== \\
        // ================================================== \\

        /**
         * Emits particles on the winning symbols
         */
        protected displayParticlesOnWinSymbols(winline: RS.Slots.ReelSetPosition[], index: number): void
        {
            if (!this._displayParticles || !this._symbolsParticleEmitters)
            {
                return;
            }

            if (index < winline.length)
            {
                if (this._settings.drawBorderLines)
                {
                    index++;
                }
                for (const emitter in this._symbolsParticleEmitters)
                {
                    switch (this._settings.symbolParticles[emitter].type)
                    {
                        case "radial":
                            (this._symbolsParticleEmitters[emitter] as RS.Particles.Emitters.Radial<RS.Particles.Base>).origin = { x: this._linePoints[index].x, y: this._linePoints[index].y };
                            this._symbolsParticleEmitters[emitter].emit(this._settings.symbolParticles[emitter].emitter.emitCount);
                            break;

                        // case "bezier":
                        //     this._symbolsParticleEmitters[emitter].emit(this._settings.symbolParticles[emitter].emitter.emitCount);
                        //     break;

                        default:
                            const currentRect = (this._symbolsParticleEmitters[emitter] as RS.Particles.Emitters.Box<RS.Particles.Base>).rect;
                            (this._symbolsParticleEmitters[emitter] as RS.Particles.Emitters.Box<RS.Particles.Base>).rect = new RS.Rendering.Rectangle(this._linePoints[index].x, this._linePoints[index].x, currentRect.w, currentRect.h);
                            this._symbolsParticleEmitters[emitter].emit(this._settings.symbolParticles[emitter].emitter.emitCount);
                            break;
                    }
                }
            }
        }

        /**
         * Updates the winline based on the head position
         */
        protected updateLine(): void
        {
            this.updateLineParticles();
            this.updateLineMask();
        }

        /**
         * Updates the winline particles based on the head position
         */
        protected updateLineParticles(): void
        {
            if (!this._displayParticles || this._stopParticleEmission || !this._trailParticleSystems)
            {
                return;
            }

            for (const index in this._trailEmitters)
            {
                const emitParticles: number = this._settings.trailParticles[index].emitter.emitCount;
                switch (this._settings.trailParticles[index].type)
                {
                    case "radial":
                    {
                        const emitter = (this._trailEmitters[index] as RS.Particles.Emitters.Radial<RS.Particles.Base>);
                        emitter.origin = Math.Vector2D(this._lineCursor.x, this._lineCursor.y);
                        break;
                    }

                    default:
                    {
                        const emitter = (this._trailEmitters[index] as RS.Particles.Emitters.Box<RS.Particles.Base>);
                        emitter.rect = new RS.Rendering.Rectangle(this._lineCursor.x, this._lineCursor.y, emitter.rect.w, emitter.rect.h);
                        break;
                    }
                }
                if (this._settings.trailParticles[index].emitter.emitTime == null)
                {
                    this._trailEmitters[index].emit(emitParticles);
                }
            }
        }

        /**
         * Updates the winline mask based on the head position
         */
        protected updateLineMask(): void
        {
            if (this._clippingMask)
            {
                const percentage: number = this._lineCursor.x / this._maskWidth;
                this._clippingMask.scaleX = percentage;
            }
        }
    }

    export namespace ProgressiveLine
    {
        /**
         * Interface defining a sprite to take from a spritesheet frame.
         */
        export interface AssetImage
        {
            assetID: Asset.SpriteSheetReference | Asset.ImageReference;
            frameID: string;
            frameNumber?: number;
            offset?: Math.Vector2D;
            scale?: Math.Vector2D;
        }
        /**
         * Interface defining a single particle system
         */
        export interface ParticleSettings
        {
            type?: "box" | "radial";// | "bezier";
            system: RS.Particles.ITemplate<RS.Particles.Base>;
            emitter: RS.Particles.Emitters.Radial.Settings<RS.Particles.Base> | RS.Particles.Emitters.Box.Settings<RS.Particles.Base>;
        }

        /**
         * Interface defining timings used in the WinlineRenderer class. All times are given in milliseconds.
         */
        export interface Settings extends RS.Slots.WinRenderers.Base.Settings
        {
            /** Whether or not particles should be displayed. */
            displayParticles: boolean;

            /** Whether or not lines before the first position and after the last should be included.
                Default false. */
            drawBorderLines?: boolean;

            /** Offset for the left of the first border line and the right of the last border line.
                Only used if drawBorderLines is true.
                Default 0. */
            borderLinesOffset?: number;

            /** Whether or not joints are added when a line has the same direction as the next one.
                Default true. */
            displayAllJoints?: boolean;

            // ============== TIMING ============= \\

            /** Time it takes for a winline to draw completely. */
            duration: number;

            /** Time it takes for a winline to fade out. */
            fadeOutTime: number;
            
            /** Time a single winline will show for before fading out. */
            endDelay: number;

            // ================ ASSETS ============== \\

             /**
              * Asset setting for the line segment of a progressive winline part.
              * /!\ CAREFUL Only use a single texture, this won't work with a multipack (eg. spritesheet) texture
              */
            linePartAsset: AssetImage;
             /** Asset setting for the tip of the progressive line. */
            lineCursorAsset: AssetImage;
             /** Optional asset setting for the front image of the line segment joint. */
            frontJointAsset?: AssetImage;
             /** Optional asset setting for the back image of the line segment joint. */
            backJointAsset?: AssetImage;

            // ========== PARTICLE SETTINGS ========= \\

            /** Particle settings for the particles following the line path. */
            trailParticles?: Array<ParticleSettings>;

            /** Particle settings for the particles spawning when the cursor reaches a symbol. */
            symbolParticles?: Array<ParticleSettings>;
        }
    }
}