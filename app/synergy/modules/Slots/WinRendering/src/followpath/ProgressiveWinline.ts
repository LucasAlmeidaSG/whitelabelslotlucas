namespace RS.Slots.WinRenderers
{
    /**
     * Renders progressive winline.
     */
    export class ProgressiveWinline extends RS.Slots.WinRenderers.Base<ProgressiveWinline.Settings>
    {
        protected _stagger: StaggerTime;

        constructor(settings: ProgressiveWinline.Settings, reels: Reels.IGenericComponent, container: RS.Rendering.IContainer)
        {
            super(settings, reels, container);
        }

        /**
         * @inheritdoc
         */
        public renderWins(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential, totalEstimatedTime: number = 0): RS.Task<void>|null
        {
            this._stagger = requestedStagger;
            return super.renderWins(wins, requestedStagger, totalEstimatedTime);
        }

        /**
         * @inheritdoc
         */
        protected async renderWin(ctx: RS.Slots.WinRenderers.WinRenderingContext, win: RS.Slots.Models.PaylineWin)
        {
            const winline: RS.Slots.ReelSetPosition[] = this._settings.configModel.winlines[win.lineIndex];
            const count = this._settings.configModel.winlines[win.lineIndex].length;
            const points: RS.Math.Vector2D[] = this.getWinlinePoints(winline, count, this._settings.winlineSettings.drawBorderLines);

            const winlineObject: ProgressiveLine = new ProgressiveLine(this._settings.winlineSettings, this._reels, this.isDirectRender(), this.isRenderingParticles());
            this._container.addChild(winlineObject);

            try
            {
                await winlineObject.start(ctx, points, winline, win.positions.length);
            }
            catch { /* Cancelled */ }
            finally
            {
                this._container.removeChild(winlineObject);
            }
        }

        protected isDirectRender(): boolean
        {
            return (this._stagger === StaggerTime.Parallel &&
                (this._settings.parallelBehaviour === ProgressiveWinline.ParallelBehaviour.DIRECT_SYMBOLPARTICLES
                || this._settings.parallelBehaviour === ProgressiveWinline.ParallelBehaviour.DIRECT_NOPARTICLES));
        }

        protected isRenderingParticles(): boolean
        {
            return (this._stagger === StaggerTime.Sequential ||
                   (this._stagger === StaggerTime.Parallel &&
                       (this._settings.parallelBehaviour === ProgressiveWinline.ParallelBehaviour.NORMAL_PARTICLES
                       || this._settings.parallelBehaviour === ProgressiveWinline.ParallelBehaviour.DIRECT_SYMBOLPARTICLES)));
        }

        /**
         * Returns the points for each win line symbol
         * @param winline  Winline symbol Reel Position
         * @param borders  Whether or not to include border lines
         * @param count    Total number of symbols to create the points for
         */
        protected getWinlinePoints(winline: RS.Slots.ReelSetPosition[], count: number, borders: boolean = false): RS.Math.Vector2D[]
        {
            const reelSettings = this._reels.settings.reels[0] as RS.Reels.IReel.Settings;

            const points: RS.Math.Vector2D[] = [];
            const offset = this._settings.winlineSettings.borderLinesOffset | 0;
            if (borders)
            {
                points.push(RS.Math.Vector2D(
                    offset,
                    (winline[0].rowIndex * reelSettings.symbolSpacing) + (reelSettings.symbolSpacing * 0.5)));
            }
            for (let i = 0; i < count; i++)
            {
                points.push(RS.Math.Vector2D(
                    (winline[i].reelIndex * reelSettings.width) + (reelSettings.width * 0.5),
                    (winline[i].rowIndex * reelSettings.symbolSpacing) + (reelSettings.symbolSpacing * 0.5)));
            }
            if (borders)
            {
                points.push(RS.Math.Vector2D(
                    (winline[count - 1].reelIndex + 1) * reelSettings.width - offset,
                    (winline[count - 1].rowIndex * reelSettings.symbolSpacing) + (reelSettings.symbolSpacing * 0.5)));
            }
            return points;
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected estimateWinRenderTime(win: RS.Slots.Models.PaylineWin): number
        {
            return this._settings.winlineSettings.duration + this._settings.winlineSettings.fadeOutTime + this._settings.winlineSettings.endDelay;
        }
    }

    export namespace ProgressiveWinline
    {
        export enum ParallelBehaviour
        {
            NORMAL_NOPARTICLES,    // progressive render without particles
            NORMAL_PARTICLES,      // progressive render with all particles
            DIRECT_NOPARTICLES,    // direct render without particles
            DIRECT_SYMBOLPARTICLES // direct render with particles on symbols
        }

        /**
         * Interface defining timings used in the WinlineRenderer class. All times are given in milliseconds.
         */
        export interface Settings extends RS.Slots.WinRenderers.Base.Settings
        {
            /** The slot config model. */
            configModel: RS.Slots.Models.Config;

            /** Behavious on parallel drawing (for example during autoplay, freespins, etc.).
                Default normal rendering with no particles. */
            parallelBehaviour?: ParallelBehaviour;

            winlineSettings: ProgressiveLine.Settings;
        }
    }
}
