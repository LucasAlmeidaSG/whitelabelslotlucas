/// <reference path="Base.ts" />

namespace RS.Slots.WinRenderers
{
    /**
     * Encapsulates a number of other win renderers which are executed in parallel.
     */
    @HasCallbacks
    export class Composite extends Base
    {
        @RS.AutoDisposeOnSet protected _perWinSubRenderers: Base[];
        @RS.AutoDisposeOnSet protected _perWinBatchSubRenderers: Base[];
        @RS.AutoDispose protected readonly _enableSkipListeners: IDisposable[] = [];

        protected _skipEnabledThisWin: boolean;

        public constructor(subRenderers: Base[], settings?: Base.Settings)
        {
            super(settings, null, null);
            this._perWinSubRenderers = subRenderers.filter((r) => r.isPerWin);
            this._perWinBatchSubRenderers = subRenderers.filter((r) => !r.isPerWin);
            for (const subRenderer of subRenderers)
            {
                this._enableSkipListeners.push(subRenderer.onSkipEnabled(this.propagateSkip));
            }
        }

        /** Renders the specified batch of wins. */
        public renderWins(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential, totalEstimatedTime: number = 0): Task<void>|null
        {
            // Allow onSkipEnabled to be published
            this._skipEnabledThisWin = false;

            // Early-outs
            if (wins.length === 0) { return null; }
            if (this._perWinSubRenderers.length === 0 && this._perWinBatchSubRenderers.length === 0) { return null; }

            // Determine stagger time
            const staggerTime = requestedStagger === StaggerTime.Sequential ? this._settings.sequentialStagger : this._settings.parallelStagger;

            // What requested stagger?
            const winTasks: RS.Task<void>[] = [];
            if (staggerTime === StaggerTime.Sequential)
            {
                // Render each WIN sequentially
                // Create a sub-task for each win
                if (this._perWinSubRenderers.length > 0)
                {
                    if (wins.length === 1)
                    {
                        const task = this.createTaskForWin(wins[0], totalEstimatedTime);
                        winTasks.push(task);
                    }
                    else
                    {
                        const sequentialTask = new MultiTask(wins.map((w) => this.createTaskForWin(w, totalEstimatedTime)), false);
                        sequentialTask.name = "TaskForSequentialPerWinRendering";
                        winTasks.push(sequentialTask);
                    }
                }
                winTasks.push(...this._perWinBatchSubRenderers.map((r) => this.createTaskForWinBatch(r, wins, undefined, totalEstimatedTime)));
            }
            else if (staggerTime === StaggerTime.Parallel)
            {
                // Create a sub-task for each win renderer
                winTasks.push(...this._perWinSubRenderers.map((r) => this.createTaskForWinBatch(r, wins, staggerTime, totalEstimatedTime)));
                winTasks.push(...this._perWinBatchSubRenderers.map((r) => this.createTaskForWinBatch(r, wins, staggerTime, totalEstimatedTime)));
            }
            else
            {
                if (this._perWinBatchSubRenderers.length > 0)
                {
                    const staggerTask = new StaggeredMultiTask(wins.map((w) => this.createTaskForWin(w, totalEstimatedTime)), staggerTime);
                    staggerTask.name = "TaskForStaggeredPerWinRendering";
                    winTasks.push(staggerTask);
                }
                winTasks.push(...this._perWinBatchSubRenderers.map((r) => this.createTaskForWinBatch(r, wins, undefined, totalEstimatedTime)));
            }
            // Render the stagger task and all per-win batch tasks in parallel
            return winTasks.length === 0 ? null : new MultiTask(winTasks, true);
        }

        /**
         * Estimates how long the specified set of wins will take to render with the requested stagger time.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        public estimateWinsRenderTime(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential)
        {
            const perWinBatchTime = this._perWinBatchSubRenderers
                .map((r) => r.estimateWinsRenderTime(wins, requestedStagger))
                .reduce((a, b) => Math.max(a, b), 0);
            if (requestedStagger === StaggerTime.Sequential)
            {
                const perWinTime = super.estimateWinsRenderTime(wins, requestedStagger);
                return Math.max(perWinTime, perWinBatchTime);
            }
            else
            {
                const perWinTime = this._perWinSubRenderers
                    .map((r) => r.estimateWinsRenderTime(wins, requestedStagger))
                    .reduce((a, b) => Math.max(a, b), 0);
                return Math.max(perWinTime, perWinBatchTime);
            }
        }

        /** Creates a task for rendering the specified win. */
        protected createTaskForWinBatch(renderer: Base, wins: Models.PaylineWin[], requestedStagger?: RS.Slots.WinRenderers.StaggerTime, totalEstimatedTime?: number): Task<void>
        {
            const task = Task.fromPromiseProvider(() =>
            {
                const promiseOrTask = renderer.renderWins(wins, requestedStagger, totalEstimatedTime);
                if (promiseOrTask == null) { return null; }
                promiseOrTask.start();
                task.onCancelled(() => promiseOrTask.cancel());
                return promiseOrTask;
            });
            task.name = `TaskForRendererWithBatchWins`;
            task.allowEarlyTermination = renderer.settings.allowEarlyTermination ? true : false;
            return task;
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected renderWin(ctx: WinRenderingContext, win: Models.PaylineWin): PromiseLike<void> | Task<void> | null
        {
            if (this._perWinSubRenderers.length === 0) { return null; }
            const task = new MultiTask(this._perWinSubRenderers.map((r) => r.renderWins([win], undefined, ctx.totalEstimatedTime)), true);
            task.name = `CompositePerWinMultiTask`;
            task.start();
            return task;
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            // How long will it take all sub renderers to render this win in parallel?
            const totalTime = this._perWinSubRenderers
                .map((r) => r.estimateWinsRenderTime([win], RS.Slots.WinRenderers.StaggerTime.Sequential))
                .reduce((a, b) => Math.max(a, b), 0);

            return totalTime;
        }

        /** Publishes an event which enables winrendering to be skipped once a subrenderer has done the same */
        @Callback
        protected propagateSkip()
        {
            // Only allow onSkipEnabled to be published once per win rendering state
            if (!this._skipEnabledThisWin)
            {
                this._onSkipEnabled.publish();
                this._skipEnabledThisWin = true;
            }
        }
    }
}