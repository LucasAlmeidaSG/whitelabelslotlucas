/// <reference path="Base.ts"/>

namespace RS.Slots.WinRenderers
{
    export interface SymbolAnimDetails
    {
        symbolID: number;
        reelIndex: number;
        rowIndex: number;
        sprite: RS.Rendering.IAnimatedSprite | RS.Rendering.IBitmap | RS.Rendering.ISpine;
        animateTask: Task<void>;
        refCount: number;
    }

    // How long to "animate" non-sprite symbols for
    const staticAnimationTime = 1700;

    /**
     * Win renderer which animates the symbols on a set of reels.
     */
    export class SymbolAnimator extends Base<SymbolAnimator.Settings>
    {
        public readonly onSymbolStartedAnimating = createEvent<SymbolAnimator.SymbolAnimatingData>();
        public readonly onSymbolStoppedAnimating = createEvent<SymbolAnimator.SymbolAnimatingData>();

        protected _symbolsContainer = new RS.Rendering.Container();
        protected _animatingSymbols: SymbolAnimDetails[] = [];

        /** Gets or sets the reels for this animator. */
        public get reels(): Reels.IGenericComponent { return this._reels; }
        public set reels(reels: Reels.IGenericComponent) { this._reels = reels; }

        constructor(reels: Reels.IGenericComponent, container: RS.Rendering.IContainer, settings?: SymbolAnimator.Settings)
        {
            super(reels, container);

            this._settings = {...this._settings, ...SymbolAnimator.defaultSettings, ...settings};

            this._container.addChild(this._symbolsContainer);
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected renderWin(ctx: WinRenderingContext, win: Models.PaylineWin): PromiseLike<void> | Task<void>
        {
            let positions: ReelSetPosition[] = win.positions;
            if (this._settings.useHiddenReels)
            {
                positions = positions.filter((pos) => this._reels.reels[pos.reelIndex].visible);
            }

            if (positions.length > 0)
            {
                const task = new MultiTask(
                    positions.map((pos) =>
                    {
                        const symbolID = this._reels.getSymbol(pos.reelIndex, pos.rowIndex);
                        const subTask = this.renderSymbolAtPosition(pos.reelIndex, this._reels.findSymbolCenter(pos.reelIndex, pos.rowIndex), symbolID);
                        return subTask;
                    }),
                    true
                );
                task.name = `MultiTaskOfSymbolAnimators`;
                return task;
            }
            // No position to render: no task
            return null;
        }

        protected findAnimatingSymbol(reelIndex: number, rowIndex: number): SymbolAnimDetails|null
        {
            for (let i = 0; i < this._animatingSymbols.length; i++)
            {
                const animDetails = this._animatingSymbols[i];
                if (animDetails.reelIndex === reelIndex && animDetails.rowIndex === rowIndex)
                {
                    return animDetails;
                }
            }
            return null;
        }

        /**
         * Animates a symbol at the given position on the reels.
         */
        protected renderSymbolAtPosition(reelIndex: number, rowIndex: number, symbolID: number): Task<void>|null
        {
            // Check if we already have a symbol animating at this position
            // This can happen when multiple winlines animate through a multi-spanning symbol simultaenously
            const existing = this.findAnimatingSymbol(reelIndex, rowIndex);
            if (existing != null)
            {
                // Spawn a new task that waits for the existing task to finish
                // existing.refCount++;
                const newTask = Task.fromPromise(existing.animateTask);
                // newTask.then(v => { existing.refCount--; }, () => { existing.refCount-- });
                return newTask;
            }

            // Start animating it
            const sprite = this.reels.getSpriteForPosition(reelIndex, rowIndex, symbolID);

            if (sprite == null) { return null; }
            // if (!(sprite instanceof RS.Rendering.AnimatedSprite)) { return null; }

            sprite.x += this._settings.symbolOffset.x;
            sprite.y += this._settings.symbolOffset.y;

            this._symbolsContainer.addChild(sprite);
            if (sprite instanceof RS.Rendering.AnimatedSprite)
            {
                sprite.play();
            }
            else if (sprite instanceof RS.Rendering.Spine)
            {
                // Play current set animation
                sprite.gotoAndPlay();
            }

            this.onSymbolStartedAnimating.publish({ symbolID, reelIndex, rowIndex, sprite });

            this.reels.hideSymbol(reelIndex, Math.floor(rowIndex));

            // Create task that ends when the symbol is done
            const task = new Task<void>((innerTask, resolve, reject) =>
            {
                let tween: RS.Tween<any> | null = null;
                const onDone = () =>
                {
                    if (tween) { tween.finish(); }

                    const details = this.findAnimatingSymbol(reelIndex, rowIndex);
                    details.refCount--;
                    if (details.refCount === 0)
                    {
                        this._symbolsContainer.removeChild(sprite);
                        if (sprite instanceof RS.Rendering.AnimatedSprite || sprite instanceof RS.Rendering.Spine)
                        {
                            sprite.stop();
                        }
                        sprite.dispose();

                        this.onSymbolStoppedAnimating.publish({ symbolID, reelIndex, rowIndex, sprite });

                        this._animatingSymbols.splice(this._animatingSymbols.indexOf(details), 1);

                        this.reels.showSymbol(reelIndex, Math.floor(rowIndex));
                    }
                };
                innerTask.onCancelled(onDone);
                if (sprite instanceof RS.Rendering.AnimatedSprite)
                {
                    sprite.onAnimationEnded.once((ev) =>
                    {
                        onDone();
                        resolve();
                    });
                }
                else if (sprite instanceof RS.Rendering.Spine)
                {
                    sprite.onSpineComplete.once((ev) =>
                    {
                        onDone();
                        resolve();
                    })    
                }
                else
                {
                    tween = RS.Tween.wait(staticAnimationTime)
                        .call(() =>
                        {
                            onDone();
                            resolve();
                        });
                }
            });
            task.name = `SymbolAnimation`;

            // Store
            this._animatingSymbols.push({
                symbolID,
                reelIndex: reelIndex,
                rowIndex: rowIndex,
                sprite: sprite,
                animateTask: task,
                refCount: 1
            });

            return task;
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            let animateTime = 0;
            for (const pos of win.positions)
            {
                const symbolID = this._reels.getSymbol(pos.reelIndex, pos.rowIndex);
                animateTime = Math.max(animateTime, this.getSymbolAnimateTime(symbolID, pos.reelIndex));
            }
            return animateTime;
        }

        /**
         * Gets how long symbol animation will last for the specified id.
         */
        protected getSymbolAnimateTime(id: number, reelIndex: number): number
        {
            // Identify symbol definition
            const reelSettings = this._reels.settings.reels[reelIndex] as Reels.IReel.Settings;
            const symbolDef = this._reels.getSymbolDefinition(id, reelIndex);
            if (symbolDef == null)
            {
                Log.warn(`Invalid symbol index '${id}' (definition not found)`);
                return 0;
            }

            // Identify animation
            if (symbolDef.animation == null && symbolDef.spineAnimation == null) { return staticAnimationTime; }
            if (symbolDef.spineAnimation != null && symbolDef.spineAnimation.animationName == null) { return staticAnimationTime; }
            if (symbolDef.animation != null && symbolDef.animation.name == null) { return staticAnimationTime; }
            
            const asset = Asset.Store.getAsset(symbolDef.spineAnimation != null ? symbolDef.spineAnimation.spineAsset : symbolDef.spriteAsset);
            if (asset == null)
            {
                Log.warn(`Invalid symbol index '${id}' (asset not found)`);
                return 0;
            }

            if (asset instanceof RS.Asset.SpineAsset)
            {
                return symbolDef.spineAnimation.animationDuration;
            }
            else if (asset.asset instanceof RS.Rendering.SpriteSheet || asset.asset instanceof RS.Rendering.RSSpriteSheet)
            {
                const spriteSheet = asset.asset;
                const anim = spriteSheet.getAnimation(symbolDef.animation.name);
                if (anim == null)
                {
                    Log.warn(`Invalid symbol index '${id}' (animation not found)`);
                    return 0;
                }

                // Compute duration
                return (anim.frames.length * 1000) / (symbolDef.animation.framerate || spriteSheet.framerate || 24);
            }
            else
            {
                return 0;
            }
        }
    }

    export namespace SymbolAnimator
    {
        /**
         * Published when a symbol has stopped animating.
         */
        export interface SymbolAnimatingData
        {
            /** The animating symbol ID. */
            symbolID: number;

            /** The reel index of the animating symbol. */
            reelIndex: number;

            /** The row index of the animating symbol. */
            rowIndex: number;

            /** The sprite/spine of the animation. */
            sprite: RS.Rendering.IAnimatedSprite | RS.Rendering.IBitmap | RS.Rendering.ISpine;
        }

        /**
         * Settings for the symbol animator.
         */
        export interface Settings extends Base.Settings
        {
            /** Position offset for the symbol sprite . */
            symbolOffset?: Math.Vector2D;

            /** Whether or not symbols should be animating on hidden reels . */
            useHiddenReels?: boolean;
        }

        /**
         * Default settings for the symbol animator.
         */
        export let defaultSettings: Settings =
        {
            symbolOffset: { x: 0, y: 0 },
            useHiddenReels: true
        };
    }
}
