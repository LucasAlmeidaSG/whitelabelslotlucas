/// <reference path="Base.ts" />

namespace RS.Slots.WinRenderers
{
    /**
     * Fades symbols on the reels during win rendering to highlight the winning symbols.
     */
    export class SymbolFade extends Base
    {
        protected _fadeTime: number;

        @RS.AutoDisposeOnSet protected readonly _fadePositions: SymbolFade.FadePosition[];

        /** Gets if this win renderer acts per winline, or per whole batch of winlines. */
        public get isPerWin() { return true; }

        public constructor(reels: Reels.IGenericComponent, fadeTime: number = 500)
        {
            super({ allowEarlyTermination: true }, reels, null);
            this._fadeTime = fadeTime;

            // Assemble all fade positions
            this._fadePositions = [];
            const reelCount = this._reels.settings.reels.length;
            for (let reelIndex = 0; reelIndex < reelCount; ++reelIndex)
            {
                const reelSettings = this._reels.settings.reels[reelIndex];
                const rowCount = reelSettings.stopSymbolCount;
                for (let rowIndex = 0; rowIndex < rowCount; ++rowIndex)
                {
                    this._fadePositions.push(new SymbolFade.FadePosition(this._reels, reelIndex, rowIndex));
                }
                if (reelSettings.hasLongSymbols)
                {
                    // take border symbols into account
                    this._fadePositions.push(new SymbolFade.FadePosition(this._reels, reelIndex, -1));
                    this._fadePositions.push(new SymbolFade.FadePosition(this._reels, reelIndex, rowCount));
                }
            }
        }

        /** Renders the specified batch of wins. */
        public renderWins(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential, totalEstimatedTime: number = 0): Task<void>|null
        {
            Log.debug(`SymbolFade: start renderWins`);
            // Run base tasks
            const baseTask = super.renderWins(wins, requestedStagger, totalEstimatedTime);
            baseTask.then(() =>
            {
                Log.debug(`SymbolFade: end renderWins`);
                for (const pos of this._fadePositions)
                {
                    pos.fadeTo(1.0, this._fadeTime);
                }
            });
            return baseTask;
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected async renderWin(ctx: WinRenderingContext, win: Models.PaylineWin)
        {
            Log.debug(`SymbolFade: start renderWin`);
            for (const pos of this._fadePositions)
            {
                let isInWin = false;
                for (const winPos of win.positions)
                {
                    if (winPos.reelIndex === pos.reelIndex && winPos.rowIndex === pos.rowIndex)
                    {
                        isInWin = true;
                        break;
                    }
                }
                if (isInWin)
                {
                    pos.fadeTo(1.0, this._fadeTime);
                }
                else
                {
                    pos.fadeTo(0.4, this._fadeTime);
                }
            }

            await ctx.untilCancelled();
            this._onSkipEnabled.publish();
            Log.debug(`SymbolFade: end renderWin`);
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            // We are "endless", e.g. we only stop when told to
            return 0;
        }
    }

    export namespace SymbolFade
    {
        export class FadePosition implements IDisposable
        {
            protected _isDisposed = false;

            @RS.AutoDisposeOnSet protected _tween: RS.Tween<FadePosition>;

            protected readonly _color = new RS.Util.Color();
            private _fade = 1.0;

            public get isDisposed() { return this._isDisposed; }

            public get fade() { return this._fade; }
            public set fade(value)
            {
                if (this._fade === value) { return; }
                this._fade = value;
                const tmpCol = this._color;
                tmpCol.r = tmpCol.g = tmpCol.b = value;
                this.reels.setSymbolColor(this.reelIndex, this.rowIndex, tmpCol);
            }

            public constructor(public readonly reels: RS.Reels.IGenericComponent, public readonly reelIndex: number, public readonly rowIndex: number) { }

            public async fadeTo(targetFade: number, duration: number, ease?: RS.EaseFunction)
            {
                try
                {
                    this._tween = RS.Tween.get<FadePosition>(this)
                        .to({ fade: targetFade }, duration, ease);
                    await this._tween;
                    this._tween = null;
                }
                catch (e)
                {
                    if (e instanceof RS.Tween.RejectionError) { return; }
                    throw e;
                }
            }

            public reset(): void
            {
                this._tween = null;
                this.fade = 1.0;
            }

            public dispose(): void
            {
                if (this._isDisposed) { return; }
                this.fade = 1.0;
                this._isDisposed = true;
            }
        }
    }
}
