namespace RS.Slots.WinRenderers
{
    /** Represents an amount of time to stagger wins by. */
    export type StaggerTime = -1 | 0 | number;
    export namespace StaggerTime
    {
        /** Don't stagger, render sequentially. */
        export const Sequential: StaggerTime = -1;

        /** Don't stagger, render in parallel. */
        export const Parallel: StaggerTime = 0;
    }

    /** Unique context for rendering a single win. */
    export class WinRenderingContext
    {
        protected _underlyingTask: Task<void>|null;
        protected _totalEstimatedTime: number = 0;

        /** Gets or sets the task that drives this win render action. */
        public get task() { return this._underlyingTask; }
        public set task(value) { this._underlyingTask = value; }

        /** Gets or sets the total estimated win rendering time. */
        public get totalEstimatedTime() { return this._totalEstimatedTime; }
        public set totalEstimatedTime(value) { this._totalEstimatedTime = value; }

        /** Adds a function callback to be called when win rendering should be cancelled. */
        public onCancelled(func: () => void)
        {
            this._underlyingTask.onCancelled(func);
        }

        /** Adds a function callback to be called when win rendering has finished by any means. */
        public onEnd(func: () => void)
        {
            let funcCalled = false;
            const miniFunc = () =>
            {
                if (funcCalled) { return; }
                func();
                funcCalled = true;
            };
            this.onCancelled(miniFunc);
            this._underlyingTask.then(miniFunc, miniFunc);
        }

        /** Gets a tween that is cancelled when this win render action is cancelled. */
        public getTween<T extends object>(target: T, settings?: TweenSettings): SingleTween<T>
        {
            const tween = RS.Tween.get(target, settings);
            this.onCancelled(() => tween.finish());
            return tween;
        }

        /** Waits for this render action to be cancelled. */
        public untilCancelled(): PromiseLike<void>
        {
            return new Promise<void>((resolve, reject) =>
            {
                this.onCancelled(() => resolve());
            });
        }
    }

    /**
     * Base win renderer.
     * All win renderers should extend this.
     */
    export abstract class Base<TSettings extends Base.Settings = Base.Settings> implements IDisposable
    {
        protected _settings: TSettings;
        protected _isDisposed: boolean = false;
        @RS.AutoDispose protected readonly _onSkipEnabled: RS.ISimplePrivateEvent = RS.createSimpleEvent();

        /** Gets if this win renderer has been disposed. */
        public get isDisposed() { return this._isDisposed; }

        /** Gets or sets the settings for this win renderer. */
        public get settings() { return this._settings; }
        public set settings(value) { this._settings = value; }

        /** Gets if we want to enable skipping of winrendering. */
        public get onSkipEnabled(): RS.IEvent { return this._onSkipEnabled; }

        protected _container: RS.Rendering.IContainer|null;
        protected _reels: Reels.IGenericComponent|null;

        /** Gets if this win renderer acts per winline, or per whole batch of winlines. */
        public get isPerWin() { return true; }

        public constructor(settings: TSettings, reels: Reels.IGenericComponent, container: RS.Rendering.IContainer);

        public constructor(reels: Reels.IGenericComponent, container: RS.Rendering.IContainer);

        public constructor(reels: Reels.IGenericComponent);

        public constructor();

        public constructor(p1?: TSettings|Reels.IGenericComponent, p2?: Reels.IGenericComponent|RS.Rendering.IContainer, p3?: RS.Rendering.IContainer)
        {
            // Resolve overload
            const settings = p1 != null ? (Is.disposable(p1) ? null : p1) : null;
            const reels = p1 != null ? (Is.disposable(p1) ? p1 : p2 instanceof RS.Reels.Component ? p2 : null) : null;
            // Allow settings a 3rd param as a custom container.
            const container = p3 != null ? p3 : p2;

            // Store properties
            this._settings = (settings != null ? {...settings as object} : {}) as TSettings;
            if (this._settings.allowEarlyTermination == null) { this._settings.allowEarlyTermination = false; }
            if (this._settings.parallelStagger == null) { this._settings.parallelStagger = StaggerTime.Parallel; }
            if (this._settings.sequentialStagger == null) { this._settings.sequentialStagger = StaggerTime.Sequential; }
            this._container = container || reels;
            this._reels = reels;
        }

        /** Renders the specified batch of wins. */
        public renderWins(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential, totalEstimatedTime: number = 0): Task<void>|null
        {
            // Are we NOT working per-win?
            if (!this.isPerWin)
            {
                const ctx = new WinRenderingContext();
                ctx.totalEstimatedTime = totalEstimatedTime;
                const task = Task.fromPromiseProvider(() =>
                {
                    const promiseOrTask = this.doRenderWins(ctx, wins);
                    if (promiseOrTask == null) { return null; }
                    if (promiseOrTask instanceof Task)
                    {
                        promiseOrTask.start();
                        task.onCancelled(() => promiseOrTask.cancel());
                    }
                    return promiseOrTask;
                });
                task.name = `NonPerWinDoRenderWinsTask`;
                ctx.task = task;
                task.allowEarlyTermination = this._settings.allowEarlyTermination ? true : false;
                return task;
            }

            // Early-outs
            if (wins.length === 0) { return null; }
            if (wins.length === 1)
            {
                return this.createTaskForWin(wins[0], totalEstimatedTime);
            }

            // Determine stagger time
            const staggerTime = requestedStagger === StaggerTime.Sequential ? this._settings.sequentialStagger : this._settings.parallelStagger;

            // Create a sub-task for each win
            const winTasks = wins.map((w) => this.createTaskForWin(w, totalEstimatedTime));

            // What ordering behaviour?
            if (staggerTime === StaggerTime.Sequential)
            {
                return new MultiTask(winTasks, false);
            }
            else if (staggerTime === StaggerTime.Parallel)
            {
                return new MultiTask(winTasks, true);
            }
            else
            {
                return new StaggeredMultiTask(winTasks, staggerTime);
            }
        }

        /**
         * Estimates how long the specified set of wins will take to render with the requested stagger time.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        public estimateWinsRenderTime(wins: Models.PaylineWin[], requestedStagger: StaggerTime = StaggerTime.Sequential)
        {
            // Determine stagger time
            const staggerTime = requestedStagger === StaggerTime.Sequential ? this._settings.sequentialStagger : this._settings.parallelStagger;

            // Sequential?
            if (staggerTime === StaggerTime.Sequential)
            {
                // Sum time per win
                const totalTime = wins
                    .map((w) => this.estimateWinRenderTime(w))
                    .reduce((a, b) => a + b);
                return totalTime;
            }
            else
            {
                // Consider stagger
                let totalTime = 0;
                for (let i = 0; i < wins.length; i++)
                {
                    const timeToRender = this.estimateWinRenderTime(wins[i]);
                    const endTime = i * staggerTime + timeToRender;
                    totalTime = Math.max(totalTime, endTime);
                }
                return totalTime;
            }
        }

        /**
         * Disposes this win renderer.
         */
        public dispose()
        {
            if (this._isDisposed) { return; }
            this._isDisposed = true;
        }

        /** Creates a task for rendering the specified win. */
        protected createTaskForWin(win: Models.PaylineWin, totalEstimatedTime: number): Task<void>
        {
            const ctx = new WinRenderingContext();
            ctx.totalEstimatedTime = totalEstimatedTime;
            const task = Task.fromPromiseProvider(() =>
            {
                const promiseOrTask = this.renderWin(ctx, win);
                if (promiseOrTask == null) { return null; }
                if (promiseOrTask instanceof Task)
                {
                    promiseOrTask.start();
                    task.onCancelled(() => promiseOrTask.cancel());
                }
                return promiseOrTask;
            });
            task.name = `TaskForWin[${win.symbolID},${win.lineIndex}]`;
            ctx.task = task;
            task.allowEarlyTermination = this._settings.allowEarlyTermination ? true : false;
            return task;
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected abstract renderWin(ctx: WinRenderingContext, win: Models.PaylineWin): PromiseLike<void> | Task<void> | null;

        /**
         * Renders the specified set of wins in non-per-win mode.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The wins to render.
         */
        protected doRenderWins(ctx: WinRenderingContext, wins: Models.PaylineWin[]): PromiseLike<void> | Task<void> | null { return null; }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected abstract estimateWinRenderTime(win: Models.PaylineWin): number;
    }

    export namespace Base
    {
        /**
         * Settings for a win renderer.
         */
        export interface Settings
        {
            /** How much to stagger each win by when sequential win rendering is requested. Defaults to Sequential. */
            sequentialStagger?: StaggerTime;

            /** How much to stagger each win by when parallel win rendering is requested. Defaults to Parallel. */
            parallelStagger?: StaggerTime;

            /** Force using a stagger type. */
            forceStagger?: StaggerTime;

            /** Whether or not to interrupt rendering if there are no other renderers to finish. Defaults to false. */
            allowEarlyTermination?: boolean;
        }
    }
}
