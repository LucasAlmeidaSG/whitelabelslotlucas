/// <reference path="IScatterAnimator.ts" />

namespace RS.Slots.WinRenderers
{
    export interface SymbolPosition
    {
        reelIndex: number;
        rowIndex: number;
    }

    /**
     * Animates scatter symbols.
     * Not strictly a win renderer, but behaves like one.
     */
    export class ScatterAnimator implements IScatterAnimator
    {
        protected _reels: Reels.IGenericComponent;
        protected _container: RS.Rendering.IContainer;

        protected _symbolContainer: RS.Rendering.IContainer;

        protected _isDisposed: boolean = false;

        public get isDisposed() { return this._isDisposed; }

        constructor(reels: Reels.IGenericComponent, container: RS.Rendering.IContainer)
        {
            this._reels = reels;
            this._container = container;

            this._symbolContainer = new RS.Rendering.Container();
            this._container.addChild(this._symbolContainer);
        }

        /**
         * Animates scatter symbols at the specified positions.
         * @param positions
         */
        public async animateScatters(positions: SymbolPosition[]);

        /**
         * Animates scatter symbols within the specified model.
         * @param reelsModel
         * @param groupID
         */
        public async animateScatters(reelsModel: Models.Reels, groupID?: number);

        public async animateScatters(p1: SymbolPosition[] | RS.Slots.Models.Reels, groupID: number = 0)
        {
            // Resolve reels model into array of positions
            if (!Is.array(p1))
            {
                return await this.animateScatters(this.findScatters(p1, groupID));
            }
            if (p1.length === 0) { return; }
            
            // Animate each one, wait for them all to be done
            await RS.Async.multiple(
                p1.map<PromiseLike<void>>(async (pos) =>
                {
                    const spr = this._reels.getSpriteForPosition(pos.reelIndex, pos.rowIndex);
                    this._symbolContainer.addChild(spr);
                    this._reels.hideSymbol(pos.reelIndex, pos.rowIndex);
                    if (spr instanceof RS.Rendering.Spine)
                    {
                        spr.gotoAndPlay();
                        await spr.onSpineComplete;
                        this._reels.showSymbol(pos.reelIndex, pos.rowIndex);
                    }
                    else if (spr instanceof RS.Rendering.AnimatedSprite)
                    {
                        spr.play();
                        await spr.onAnimationEnded;
                        this._reels.showSymbol(pos.reelIndex, pos.rowIndex);
                    }
                    this._symbolContainer.removeChild(spr);
                })
            );
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._container.removeChild(this._symbolContainer);
            this._isDisposed = true;
        }

        protected findScatters(reelsModel: Models.Reels, groupID: number = 0): SymbolPosition[]
        {
            const reels = reelsModel.symbolsInView;
            const result: SymbolPosition[] = [];
            for (let reelIndex = 0, reelCount = reels.reelCount; reelIndex < reelCount; ++reelIndex)
            {
                const reelSettings = this._reels.settings.reels[reelIndex];
                for (let rowIndex = 0, rowCount = reels.getRowCount(reelIndex); rowIndex < rowCount; ++rowIndex)
                {
                    const symbolID = reels.get(reelIndex, rowIndex);
                    const def = this.findDef(reelSettings.symbols, symbolID);
                    if (def != null)
                    {
                        const scatterGroup = def.scatterGroup || 0;
                        if (def.isScatter && scatterGroup === groupID)
                        {
                            result.push({ reelIndex, rowIndex });
                        }
                    }
                }
            }
            return result;
        }

        protected findDef(bindingSet: Reels.Symbols.BindingSet, id: number): Reels.Symbols.Definition | null
        {
            for (const binding of bindingSet)
            {
                if (binding.id === id)
                {
                    return binding.definition;
                }
            }
            return null;
        }
    }
}
