namespace RS.Slots.WinRenderers
{
    /**
     * Animates scatter symbols.
     */
    export interface IScatterAnimator extends IDisposable
    {
        /**
         * Animates scatter symbols at the specified positions.
         * @param positions 
         */
        animateScatters(positions: SymbolPosition[]): PromiseLike<void>;
        
        /**
         * Animates scatter symbols within the specified model.
         * @param reelsModel 
         */
        animateScatters(reelsModel: Models.Reels): PromiseLike<void>;
    }
}