/// <reference path="Base.ts" />

namespace RS.Slots.WinRenderers
{
    /**
     * Win renderer which updates the payout over a set time (like a counter).
     * @deprecated use WinAmountUpdate instead
     */
    export class WinCountup extends Base<WinCountup.Settings>
    {
        /** Gets or sets the settings for this win renderer. */
        public get settings() { return this._settings; }
        public set settings(value) { this._settings = value; }

        /** Gets if this win renderer acts per winline, or per whole batch of winlines. */
        public get isPerWin() { return false; }

        constructor(settings: WinCountup.Settings)
        {
            super(settings, null, null);
            this._settings.allowEarlyTermination = settings.allowEarlyTermination != null ? settings.allowEarlyTermination : true;
            Log.warn("WinCountup has been deprecated. Use WinAmountUpdate instead.");
        }

        /**
         * Renders the specified set of wins in non-per-win mode.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The wins to render.
         */
        protected async doRenderWins(ctx: WinRenderingContext, wins: Models.PaylineWin[])
        {
            const duration = ctx.totalEstimatedTime;
            const winAmountThisSpin = wins
                .map((w) => w.totalWin)
                .reduce((a, b) => a + b);
            
            const final = this.settings.winAmount.value + winAmountThisSpin;

            await ctx.getTween(this.settings.winAmount)
                .to({ value: final }, duration);

            this.settings.winAmount.value = final;
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected renderWin(ctx: WinRenderingContext, win: Models.PaylineWin): PromiseLike<void> | Task<void> { return null; }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            // We are "endless", e.g. we only stop when told to
            return 0;
        }
    }

    export namespace WinCountup
    {
        export interface Settings extends Base.Settings
        {
            winAmount: Observable<number>;
        }
    }
}