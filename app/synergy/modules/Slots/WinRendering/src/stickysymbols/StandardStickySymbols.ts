/// <reference path="IStickySymbols.ts" />

namespace RS.Slots.WinRenderers
{
    export class StandardStickySymbols implements IStickySymbols, IDisposable
    {
        protected _container: RS.Rendering.IContainer;
        protected _reels: Reels.IGenericComponent;

        protected _symbolContainer: RS.Rendering.IContainer;
        protected _symbols: StandardStickySymbols.StickySymbol[] = [];

        protected _isDisposed: boolean = false;

        public get isDisposed() { return this._isDisposed; }

        /** Whether or not the sticky symbols should be displayed. */
        public get visible() { return this._symbolContainer.visible; }
        public set visible(value) { this._symbolContainer.visible = value; }

        public constructor(reels: Reels.IGenericComponent, container: RS.Rendering.IContainer, public readonly settings: StandardStickySymbols.Settings = StandardStickySymbols.defaultSettings)
        {
            this._container = container;
            this._reels = reels;

            this._symbolContainer = new RS.Rendering.Container();
            this._symbolContainer.name = "StandardStickySymbols";
            container.addChild(this._symbolContainer);
        }

        /**
         * Processes the specified set of commands, animating as required.
         * All commands will be executed in parallel.
         * Async.
         */
        public async process(commands: IStickySymbols.Command[])
        {
            if (commands.length === 0) { return; }
            await Async.multiple(commands.map((cmd) => this.executeCommand(cmd)));
        }

        /**
         * Restores the specified sticky symbol states.
         */
        public restore(info: IStickySymbols.RestoreState[]): void
        {
            this._symbolContainer.removeAllChildren();
            this._symbols.length = 0;

            for (const state of info)
            {
                const spr = this._reels.getSpriteForPosition(state.position.reelIndex, state.position.rowIndex, state.symbolID);
                this._symbolContainer.addChild(spr);
                this._symbols.push({ position: state.position, sprite: spr });
            }
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this._container.removeChild(this._symbolContainer);
            this._isDisposed = true;
        }

        protected getSymbolsIndex(pos: IStickySymbols.Position): number
        {
            for (let i = 0, l = this._symbols.length; i < l; ++i)
            {
                const sym = this._symbols[i];
                if (sym.position.reelIndex === pos.reelIndex && sym.position.rowIndex === pos.rowIndex)
                {
                    return i;
                }
            }
            return -1;
        }

        protected async executeCommand(command: IStickySymbols.Command)
        {
            switch (command.type)
            {
                case IStickySymbols.CommandType.Add:
                {
                    const existingIndex = this.getSymbolsIndex(command.position);
                    if (existingIndex !== -1) { return; }
                    const spr = this._reels.getSpriteForPosition(command.position.reelIndex, command.position.rowIndex, command.symbolID);
                    this._symbolContainer.addChild(spr);
                    this._symbols.push({ position: command.position, sprite: spr });
                    spr.alpha = 0.0;
                    await RS.Tween.get(spr)
                        .to({alpha: 1.0}, this.settings.fadeInTime, Ease.quadInOut);
                    break;
                }
                case IStickySymbols.CommandType.Move:
                {
                    const existingIndex = this.getSymbolsIndex(command.oldPosition);
                    if (existingIndex === -1) { return; }
                    const existing = this._symbols[existingIndex];
                    existing.position = command.newPosition;
                    const newPos = this._reels.getSymbolPosition(command.newPosition.reelIndex, command.newPosition.rowIndex);
                    await RS.Tween.get(existing.sprite)
                        .to({x: newPos.x, y: newPos.y}, this.settings.moveTime, Ease.quadInOut);
                    break;
                }
                case IStickySymbols.CommandType.Remove:
                {
                    const existingIndex = this.getSymbolsIndex(command.position);
                    if (existingIndex === -1) { return; }
                    const existing = this._symbols[existingIndex];
                    this._symbols.splice(existingIndex, 1);
                    await RS.Tween.get(existing.sprite)
                        .to({alpha: 0.0}, this.settings.fadeOutTime, Ease.quadInOut);
                    break;
                }
            }
        }
    }

    export namespace StandardStickySymbols
    {
        export interface Settings
        {
            fadeInTime: number;
            fadeOutTime: number;
            moveTime: number;
        }

        export const defaultSettings: Settings =
        {
            fadeInTime: 500,
            fadeOutTime: 500,
            moveTime: 500
        };

        export interface StickySymbol
        {
            position: IStickySymbols.Position;
            sprite: RS.Rendering.IAnimatedSprite | RS.Rendering.IBitmap | RS.Rendering.ISpine;
        }
    }
}