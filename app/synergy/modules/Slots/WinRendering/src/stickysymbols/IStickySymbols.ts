namespace RS.Slots.WinRenderers
{
    /**
     * Renders and animates sticky symbols.
     */
    export interface IStickySymbols
    {
        /** Whether or not the sticky symbols should be displayed. */
        visible: boolean;

        /**
         * Processes the specified set of commands, animating as required.
         * Note that order is important, e.g. [ add, move ] will behave differently to [ move, add ]
         * Async.
         */
        process(commands: IStickySymbols.Command[]): PromiseLike<void>;

        /**
         * Restores the specified sticky symbol states.
         */
        restore(info: IStickySymbols.RestoreState[]): void;
    }

    export namespace IStickySymbols
    {
        export interface Position
        {
            reelIndex: number;
            rowIndex: number;
        }

        export interface RestoreState
        {
            position: Position;
            symbolID: number;
        }

        export enum CommandType { Add, Move, Remove }

        export interface BaseCommand<TType extends CommandType>
        {
            type: TType;
        }

        export interface AddCommand extends BaseCommand<CommandType.Add>
        {
            position: Position;
            symbolID: number;
        }

        export interface MoveCommand extends BaseCommand<CommandType.Move>
        {
            oldPosition: Position;
            newPosition: Position;
        }

        export interface RemoveCommand extends BaseCommand<CommandType.Remove>
        {
            position: Position;
        }

        export type Command = AddCommand | MoveCommand | RemoveCommand;
    }
}