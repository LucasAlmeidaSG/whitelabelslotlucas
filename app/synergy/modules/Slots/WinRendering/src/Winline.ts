/// <reference path="Base.ts"/>

namespace RS.Slots.WinRenderers
{
    /**
     * Renders coloured lines across symbols, showing the winline a win has paid out on.
     */
    export class Winline extends Base
    {
        protected _settings: Winline.Settings;

        /**
         * Array of winlines in use.
         */
        protected _lineArray: CanvasWinline[];
        /**
         * Offset on the stage to position winlines over.
         */
        protected _position: Math.Vector2D;
        /**
         * Array of colours to use for the winlines. There should be one colour for each winline the game uses.
         */
        protected _winlineColors: RS.Util.Color[];

        constructor(settings: Winline.Settings, reels: Reels.IGenericComponent, container: RS.Rendering.IContainer, position?: Math.Vector2D)
        {
            super(settings, reels, container);

            this._position = position != null ? position : { x: 0, y: 0 };
            this._winlineColors = settings.colors || [];
        }

        /**
         * Renders the specified win.
         * @param ctx   Unique context for this render task. Any tweens attached to this context are cancelled if rendering is skipped.
         * @param win   The win to render.
         */
        protected async renderWin(ctx: WinRenderingContext, win: Models.PaylineWin)
        {
            const reelSettings = this._reels.settings.reels[0] as Reels.IReel.Settings;

            const winline = new CanvasWinline(
                this._settings.configModel.winlines[win.lineIndex],
                this._winlineColors[win.lineIndex] || RS.Util.Colors.white,
                reelSettings.width, reelSettings.symbolSpacing
            );
            winline.x = this._position.x;
            winline.y = this._position.y;
            winline.alpha = 0;

            this._container.addChild(winline);

            try
            {
                await ctx.getTween(winline)
                    .to({ alpha: 1 }, this._settings.fadeInOutTime || 350)
                    .wait(this._settings.showTime || 1000)
                    .to({ alpha: 0 }, this._settings.fadeInOutTime || 350);
            }
            catch { /* Cancelled */ }
            finally
            {
                this._container.removeChild(winline);
            }
            this._onSkipEnabled.publish();
        }

        /**
         * Estimates how long the specified win will take to render.
         * @param wins                  The wins that would be rendered.
         * @param requestedStagger      The requested stagger time.
         */
        protected estimateWinRenderTime(win: Models.PaylineWin): number
        {
            return (this._settings.fadeInOutTime || 350) * 2 + (this._settings.showTime || 1000);
        }
    }

    export namespace Winline
    {
        /**
         * Interface defining timings used in the WinlineRenderer class. All times are given in milliseconds.
         */
        export interface Settings extends Base.Settings
        {
            /** Time it takes for a winline to fade in or out. */
            fadeInOutTime?: number;
            
            /** Time a single winline will show for before fading out. */
            showTime?: number;

            /** Colors to use for the winlines. */
            colors?: RS.Util.Color[];

            /** The slot config model. */
            configModel: Models.Config;
        }
    }
}
