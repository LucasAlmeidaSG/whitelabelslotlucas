namespace RS.GDM.UI
{
    interface SpinnerOptionState
    {
        properOptions: number[];
        temporaryOptions: number[] | null;
        temporaryOptionIndex: number | null;
    }

    @HasCallbacks
    export class UIController implements Slots.UI.IUIController
    {
        public settings: Slots.UI.IUIController.Settings;

        /** Published when a spin has been requested by the user. */
        public get onSpinRequested() { return this._onSpinRequested.public; }
        protected readonly _onSpinRequested = createSimpleEvent();

        /** Published when a skip has been requested by the user. */
        public get onSkipRequested() { return this._onSkipRequested.public; }
        protected readonly _onSkipRequested = createSimpleEvent();

        /** Published when the user has changed an input option. In this case, value is the index into options rather than the value itself. */
        public get onInputOptionChanged() { return this._onInputOptionChanged.public; }
        protected readonly _onInputOptionChanged = createEvent<Slots.UI.IUIController.InputOptionChangedData>();

        /** Published when the paytable has been requested by the user. */
        public get onPaytableRequested() { return this._onPaytableRequested.public; }
        protected readonly _onPaytableRequested = createSimpleEvent();

        /** Published when the help page has been requested by the user. */
        public get onHelpRequested() { return this._onHelpRequested.public; }
        protected readonly _onHelpRequested = createSimpleEvent();

        /** Published when the settings control has been requested by the user. */
        public get onSettingsRequested() { return this._onSettingsRequested.public; }
        protected readonly _onSettingsRequested = RS.createSimpleEvent();

        /** Published when the big bet feature has been requested by the user. */
        public get onBigBetRequested() { return this._onBigBetRequested.public; }
        protected readonly _onBigBetRequested = createSimpleEvent();

        /** Published when the autoplay feature has been requested by the user. */
        public get onAutoplayRequested() { return this._onAutoplayRequested.public; }
        protected readonly _onAutoplayRequested = createSimpleEvent();

        /** Published when the autoplay stop feature has been requested by the user. */
        public get onAutoplayStopRequested() { return this._onAutoplayStopRequested.public; }
        protected readonly _onAutoplayStopRequested = createSimpleEvent();

        /** Published when the Turbo feature has been requested by the user. */
        public get onTurboRequested() { return this._onTurboRequested.public; }
        protected readonly _onTurboRequested = createSimpleEvent();

        /** Published when the Turbo feature has been requested to stop by the user. */
        public get onTurboStopRequested() { return this._onTurboStopRequested.public; }
        protected readonly _onTurboStopRequested = createSimpleEvent();

        protected _selectedInputOptions: number[] = [];
        protected _uiSettings: UI.UISettings.Settings;

        protected _initSettings: Slots.UI.IUIController.InitSettings;

        // Handles/listeners
        @AutoDisposeOnSet protected _orientationChangedListener: IDisposable;
        @AutoDisposeOnSet protected _canQuickSpinChangedListener: IDisposable;
        @AutoDisposeOnSet protected _allowSpinHandle: IDisposable;
        @AutoDisposeOnSet protected _allowSkipHandle: IDisposable;
        @AutoDisposeOnSet protected _spinEnableHandle: IArbiterHandle<boolean>;
        @AutoDisposeOnSet protected _enabledStateHandler: IDisposable;

        // UI elements
        @AutoDisposeOnSet protected _clock: UI.Clock;
        @AutoDisposeOnSet protected _backButton: UI.BackButton;
        @AutoDisposeOnSet protected _settingsPanel: UI.SettingsPanel;
        @AutoDisposeOnSet protected _messageBar: UI.MessageBar;
        @AutoDisposeOnSet protected _meterPanel: UI.MeterPanel;
        @AutoDisposeOnSet protected _spinButton: UI.SpinButton;
        @AutoDisposeOnSet protected _stakeButton: UI.StakeButton;
        @AutoDisposeOnSet protected _autoplayButton: UI.AutoplayButton;
        @AutoDisposeOnSet protected _turboToggle: UI.TurboToggle;

        protected _spinQueued: boolean = false;
        protected _leftHandMode: boolean = false;
        protected _stakeOpen: boolean = false;

        protected win: number = 0;
        protected balance: number = 0;

        protected _spinners: UI.StakeButton.SpinnerSettings[];

        /** Gets a rectangle representing how much of the attached container is fully occluded by the UI on all sides. */
        public get enclosedSpace(): Flow.Spacing
        {
            return Flow.Spacing.none;
        }

        /** Number of ignore calls. */
        protected _ignoreRequestCount: number = 0;

        private __linesSpinnerIndex: number | null = null;
        private __totalBetSpinnerIndex: number | null = null;
        private __betPerLineSpinnerIndex: number | null = null;
        private __betMultiplierSpinnerIndex: number | null = null;

        private __spinnerOptions: SpinnerOptionState[];

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this.__isDisposed; }
        private __isDisposed: boolean = false;

        public initialise(settings: Slots.UI.IUIController.Settings): void
        {
            this.settings = settings;
            //TODO remove in 1.2 when we can move settingsNew into settings
            this._uiSettings = UI.UISettings.UseNewSettings ?
                UI.UISettings.settingsNew : UI.UISettings.settings;

            this._orientationChangedListener = settings.orientationObservable
            ? settings.orientationObservable.onChanged(this.handleOrientationChanged)
            : Orientation.onChanged(this.handleOrientationChanged);

            this._canQuickSpinChangedListener = settings.allowQuickSpinArbiter.onChanged(this.handleCanQuickSpinChanged);
        }

        /** Creates the UI with the specified settings. */
        public create(settings: Slots.UI.IUIController.InitSettings): void
        {
            this._initSettings = settings;
            this.__spinnerOptions = [];

            const spinners: UI.StakeButton.SpinnerSettings[] = [];

            // TODO remove in 1.2 when we can remove settingsNew
            const spinnerSettingsFunc = UI.UISettings.UseNewSettings ?
                UI.StakeButton.getSpinnerSettingsNew : UI.StakeButton.getSpinnerSettings;
            for (const inputOption of settings.inputOptions)
            {
                switch (inputOption.type)
                {
                    case Slots.UI.InputOptionType.Lines:
                        this.__linesSpinnerIndex = spinners.length;
                        spinners.push(spinnerSettingsFunc("IncreaseLinesButton", "DecreaseLinesButton"));
                        this.__spinnerOptions[this.__linesSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                    case Slots.UI.InputOptionType.TotalBet:
                        this.__totalBetSpinnerIndex = spinners.length;
                        spinners.push(spinnerSettingsFunc("IncreaseBetButton", "DecreaseBetButton"));
                        this.__spinnerOptions[this.__totalBetSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                    case Slots.UI.InputOptionType.BetPerLine:
                        this.__betPerLineSpinnerIndex = spinners.length;
                        spinners.push(spinnerSettingsFunc("IncreaseBetPerLineButton", "DecreaseBetPerLineButton"));
                        this.__spinnerOptions[this.__betPerLineSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                    case Slots.UI.InputOptionType.BetMultiplier:
                        this.__betMultiplierSpinnerIndex = spinners.length;
                        spinners.push(spinnerSettingsFunc("IncreaseBetMultiplierButton", "DecreaseBetMultiplierButton"));
                        this.__spinnerOptions[this.__betMultiplierSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                }
            }
            this._spinners = spinners;
            this.createUI();

            for (let i = 0, l = settings.inputOptions.length; i < l; ++i)
            {
                this.setInputOptions(settings.inputOptions[i].type, settings.inputOptions[i].options);
            }

            this._enabledStateHandler = this.settings.enableInteractionArbiter.onChanged(this.handleInteractionEnabledChanged);
            this._allowSpinHandle = this.settings.allowSpinArbiter.onChanged(this.handleAllowSpinArbiterValueChanged);
        }

        /** Updates the balance to be displayed on the UI. */
        public setBalance(value: number)
        {
            if (this._meterPanel)
            {
                this._meterPanel.balance = value;
            }
        }

        /** Updates the win amount to be displayed on the UI. */
        public setWin(value: number, final: boolean)
        {
            if (this._meterPanel)
            {
                this._meterPanel.winAmount = value;
            }
        }

        /** Updates the total bet to be displayed on the UI. */
        public setTotalBet(value: number)
        {
            //
        }

        /** Updates the line count to be displayed on the UI. */
        public setLines(value: number)
        {
            //
        }

        public setWays(value: number)
        {
            //
        }

        /** Sets replay mode. */
        public setReplayMode(value: boolean)
        {
            this._meterPanel.replayMode = value;
        }

        /** Overrides the selected input value. */
        public setSelectedInputValue(optionType: Slots.UI.InputOptionType, value: number)
        {
            const spinner = this.getSpinner(optionType);
            if (spinner)
            {
                if (spinner.value === value) { return; }
                // If the value doesn't exist in options, temporarily insert it at the current index
                // When the user changes option, it will ensure correct order and reset the options back so that this value remains unselectable
                const storedOpts = this.getSpinnerOptionState(optionType);
                const idx = storedOpts.properOptions.indexOf(value);
                if (idx === -1)
                {
                    ++this._ignoreRequestCount;

                    storedOpts.temporaryOptionIndex = spinner.optionIndex;
                    storedOpts.temporaryOptions = [...storedOpts.properOptions];
                    storedOpts.temporaryOptions.splice(storedOpts.temporaryOptionIndex, 0, value);
                    const c = this._initSettings.currencyFormatter;
                    spinner.options = this.isCurrency(optionType) ? storedOpts.temporaryOptions.map((o) => c.format(o)) : storedOpts.temporaryOptions;

                    spinner.value = this.isCurrency(optionType) ? this._initSettings.currencyFormatter.format(value) : value;
                    this._onInputOptionChanged.publish({ optionType, optionIndex: -1, userChanged: false });

                    --this._ignoreRequestCount;
                }
                else
                {
                    this.setSelectedInputOption(optionType, idx);
                }
            }
        }

        /** Updates the user's input option selection. */
        public setSelectedInputOption(optionType: Slots.UI.InputOptionType, optionIndex: number)
        {
            const spinner = this.getSpinner(optionType);
            if (spinner)
            {
                if (spinner.optionIndex === optionIndex) { return; }

                ++this._ignoreRequestCount;
                this.resetTemporarySpinnerOptions(optionType);

                spinner.optionIndex = optionIndex;
                this._onInputOptionChanged.publish({ optionType, optionIndex, userChanged: false });

                --this._ignoreRequestCount;
            }

            this._selectedInputOptions[optionType] = optionIndex;
        }

        /** Updates the user's available input options. */
        public setInputOptions(optionType: Slots.UI.InputOptionType, options: number[])
        {
            const spinner = this.getSpinner(optionType);
            if (spinner)
            {
                ++this._ignoreRequestCount;
                const c = this._initSettings.currencyFormatter;
                const optionState = this.getSpinnerOptionState(optionType);
                optionState.properOptions.length = options.length;
                for (let i = 0, l = options.length; i < l; ++i)
                {
                    optionState.properOptions[i] = options[i];
                }
                spinner.options = this.isCurrency(optionType) ? options.map((o) => c.format(o)) : options;
                this._onInputOptionChanged.publish({ optionType, optionIndex: spinner.optionIndex, userChanged: false });
                optionState.temporaryOptions = null;
                optionState.temporaryOptionIndex = null;
                --this._ignoreRequestCount;
            }
        }

        /** Attaches the UI to the specified container. */
        public attach(view: Rendering.IContainer): void
        {
            if (this._clock != null) { view.addChild(this._clock); }
            if (this._backButton != null) { view.addChild(this._backButton); }
            if (this._messageBar != null) { view.addChild(this._messageBar); }
            if (this._meterPanel != null) { view.addChild(this._meterPanel); }
            if (this._settingsPanel != null) { view.addChild(this._settingsPanel); }
            if (this._spinButton != null) { view.addChild(this._spinButton); }
            if (this._stakeButton != null) { view.addChild(this._stakeButton); }
            if (this._autoplayButton != null) { view.addChild(this._autoplayButton); }
            if (this._turboToggle != null) { view.addChild(this._turboToggle); }
            if (Is.flowElement(view))
            {
                view.invalidateLayout();
            }
        }

        /** Detaches the UI from the specified container, if it is attached to that. */
        public detach(view: Rendering.IContainer): void
        {
            const elements =
            [
                this._clock,
                this._backButton,
                this._meterPanel,
                this._spinButton,
                this._stakeButton,
                this._autoplayButton,
                this._settingsPanel,
                this._turboToggle,
                this._messageBar
            ];
            for (const element of elements)
            {
                if (element != null && element.parent === view)
                {
                    view.removeChild(element);
                }
            }
        }

        /**
         * Disposes this controller.
         */
        public dispose(): void
        {
            if (this.__isDisposed) { return; }
            this.__isDisposed = true;
        }

        /**
         * Rebuilds the UI with the specified settings.
         */
        public apply(settings: UI.UISettings.Settings)
        {
            this._uiSettings = settings;
            this.createUI();
        }

        protected createUI()
        {
            const platform = IPlatform.get();
            const uiSettings = this.getOrientationSettings(this.getDeviceSettings(Device.isDesktopDevice), Orientation.currentOrientation);

            this.createMeterPanel(uiSettings.meterPanel);
            this.createMessageBar(uiSettings.messageBar);
            this.createClock(uiSettings.clock);
            this.createBackButton(uiSettings.backButton, platform);
            this.createSpinButton(uiSettings.spinButton);
            this.createSettingsPanel(uiSettings.settingsPanel);
            this.createTurboToggle(uiSettings.turboToggle);
            this.createStakeButton(uiSettings.stakeButton);

            if (Slots.IPlatform.get().supportsAutoplay !== Slots.PlatformAutoplaySupport.None)
            {
                this.createAutoplayButton(uiSettings.autoplayButton);
                this._autoplayButton.onAutoplayClicked(this.handleAutoplayClicked);
                this._autoplayButton.onAutoplayStopClicked(this.handleAutoplayStopClicked);
            }

            //Button Clicks handled
            this._backButton.onBackClicked(this.handleBackClicked);

            //Settings button handler
            this._settingsPanel.onSettingsClicked(this.handleSettingsClicked);
            this._settingsPanel.onExitClicked(this.handleSettingsExitClicked);

            //Set init mute state and bind button to arbiter
            this._settingsPanel.toggleSound(this.settings.muteArbiter.value);
            const externalMute = this.settings.muteArbiter.onChanged((v) => this._settingsPanel.toggleSound(v));
            RS.Disposable.bind(this._settingsPanel, externalMute);

            //Left hand toggle handlers
            this._settingsPanel.onLeftHandButtonClicked(this.handleLeftHandButtonClicked);
            this._settingsPanel.onRightHandButtonClicked(this.handleRightHandButtonClicked);
            this._settingsPanel.onHelpClicked(this.handlePaytableClicked);

            //Spin/Stop button
            this._spinButton.onSpinClicked(this.handleSpinButtonClicked);
            this._spinButton.onStopClicked(this.handleStopButtonClicked);

            //Stake button
            this._stakeButton.onStakeClicked(this.handleStakeButtonClicked);
            this._stakeButton.onCloseClicked(this.handleStakeButtonCloseClicked);
            this._stakeButton.onInputOptionChanged(this.handleInputOptionChanged);

            //Turbo toggle
            this._turboToggle.onTurboToggleOn(this.handleTurboToggledOn);
            this._turboToggle.onTurboToggleOff(this.handleTurboToggledOff);
        }

        @Callback
        protected handleInteractionEnabledChanged(interactionEnabled: boolean)
        {
            //
        }

        protected isCurrency(optionType: Slots.UI.InputOptionType): boolean
        {
            switch (optionType)
            {
                case Slots.UI.InputOptionType.Lines: return false;
                case Slots.UI.InputOptionType.TotalBet: return true;
                case Slots.UI.InputOptionType.BetPerLine: return true;
                case Slots.UI.InputOptionType.BetMultiplier: return false;
            }
        }

        /** Gets the current options for the given option type. */
        protected getSpinnerOptionState(optionType: Slots.UI.InputOptionType): SpinnerOptionState
        {
            switch (optionType)
            {
                case Slots.UI.InputOptionType.Lines:
                    if (this.__linesSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__linesSpinnerIndex];
                    }
                    break;
                case Slots.UI.InputOptionType.TotalBet:
                    if (this.__totalBetSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__totalBetSpinnerIndex];
                    }
                    break;
                case Slots.UI.InputOptionType.BetPerLine:
                    if (this.__betPerLineSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__betPerLineSpinnerIndex];
                    }
                    break;
                case Slots.UI.InputOptionType.BetMultiplier:
                    if (this.__betMultiplierSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__betMultiplierSpinnerIndex];
                    }
                    break;
            }
            return null;
        }

        protected resetTemporarySpinnerOptions(optionType: Slots.UI.InputOptionType): void
        {
            const optionState = this.getSpinnerOptionState(optionType);
            if (optionState && (optionState.temporaryOptions != null || optionState.temporaryOptionIndex != null))
            {
                const spinner = this.getSpinner(optionType);
                if (spinner)
                {
                    ++this._ignoreRequestCount;
                    const c = this._initSettings.currencyFormatter;
                    spinner.options = this.isCurrency(optionType) ? optionState.properOptions.map((o) => c.format(o)) : optionState.properOptions;
                    spinner.optionIndex = this._selectedInputOptions[optionType];
                    --this._ignoreRequestCount;
                }
                optionState.temporaryOptionIndex = null;
                optionState.temporaryOptions = null;
            }
        }

        /** Gets the spinner for the given option type. Returns undefined when not found. */
        protected getSpinner(optionType: Slots.UI.InputOptionType): Flow.Spinner | Flow.ListSpinner | void
        {
            switch (optionType)
            {
                case Slots.UI.InputOptionType.Lines:
                    if (this.__linesSpinnerIndex != null)
                    {
                        return this._stakeButton.getSpinner(this.__linesSpinnerIndex);
                    }
                    break;
                case Slots.UI.InputOptionType.TotalBet:
                    if (this.__totalBetSpinnerIndex != null)
                    {
                        return this._stakeButton.getSpinner(this.__totalBetSpinnerIndex);
                    }
                    break;
                case Slots.UI.InputOptionType.BetPerLine:
                    if (this.__betPerLineSpinnerIndex != null)
                    {
                        return this._stakeButton.getSpinner(this.__betPerLineSpinnerIndex);
                    }
                    break;
                case Slots.UI.InputOptionType.BetMultiplier:
                    if (this.__betMultiplierSpinnerIndex != null)
                    {
                        return this._stakeButton.getSpinner(this.__betMultiplierSpinnerIndex);
                    }
                    break;
            }
        }

        @Callback
        protected handleInputOptionChanged(ev: { inputOption: number; optionIndex: number; })
        {
            if (this._ignoreRequestCount > 0) { return; }
            const optionState = this.getSpinnerOptionState(ev.inputOption);
            const spinner = this.getSpinner(ev.inputOption);
            if (spinner && optionState && optionState.temporaryOptionIndex != null)
            {
                // We're moving off a temporarily inserted value
                // We need to correct the option index to compensate
                if (ev.optionIndex > optionState.temporaryOptionIndex)
                {
                    --ev.optionIndex;
                }
                else
                {
                    ++ev.optionIndex;
                }
            }
            this._selectedInputOptions[ev.inputOption] = ev.optionIndex;
            this.resetTemporarySpinnerOptions(ev.inputOption);
            if (ev.inputOption === this.__linesSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: Slots.UI.InputOptionType.Lines, optionIndex: ev.optionIndex, userChanged: true });
            }
            else if (ev.inputOption === this.__totalBetSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: Slots.UI.InputOptionType.TotalBet, optionIndex: ev.optionIndex, userChanged: true });
            }
            else if (ev.inputOption === this.__betPerLineSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: Slots.UI.InputOptionType.BetPerLine, optionIndex: ev.optionIndex, userChanged: true });
            }
            else if (ev.inputOption === this.__betMultiplierSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: Slots.UI.InputOptionType.BetMultiplier, optionIndex: ev.optionIndex, userChanged: true });
            }
        }

        @Callback
        protected handleSpinButtonClicked()
        {
            if (this._settingsPanel)
            {
                this._settingsPanel.toggleSettings(false);
            }
            if (this._stakeButton)
            {
                this._stakeButton.isOpen = false;
                this._stakeOpen = false;
            }

            if (this.settings.allowQuickSpinArbiter.value)
            {
                this._onSkipRequested.publish();
                this._allowSkipHandle = this.settings.allowSkipArbiter.declare(false);
                this._spinQueued = true;
                Log.debug("quickSpin: Spin now queued");
            }
            else
            {
                this._onSpinRequested.publish();
            }
        }

        @Callback
        protected handleStakeButtonClicked()
        {
            this._stakeButton.isOpen = true;
            this._stakeOpen = true;
        }

        @Callback
        protected handleStakeButtonCloseClicked()
        {
            this._stakeButton.isOpen = false;
            this._stakeOpen = false;
        }

        @Callback
        protected handleCanQuickSpinChanged(canQuickSpin: boolean)
        {
            if (!canQuickSpin && this._spinQueued)
            {
                Log.debug("quickSpin: can no longer quick spin; spin unqueued");
                this._spinQueued = false;

                if (this._allowSkipHandle)
                {
                    this._allowSkipHandle.dispose();
                    this._allowSkipHandle = null;
                }
            }
        }

        @Callback
        protected handleStopButtonClicked()
        {
            this._onSkipRequested.publish();
        }

        @Callback
        protected handleBackClicked()
        {
            IPlatform.get().navigateToLobby();
        }

        @Callback
        protected handleSettingsClicked()
        {
            this._settingsPanel.toggleSettings(true);
        }

        @Callback
        protected handleSettingsExitClicked()
        {
            this._settingsPanel.toggleSettings(false);
        }

        @Callback
        protected handleHelpClicked()
        {
            this._onHelpRequested.publish();
        }

        @Callback
        protected handleBigBetClicked()
        {
            this._onBigBetRequested.publish();
        }

        @Callback
        protected handlePaytableClicked()
        {
            this._onPaytableRequested.publish();
        }

        @Callback
        protected handleAutoplayClicked()
        {
            if (this._settingsPanel)
            {
                this._settingsPanel.toggleSettings(false);
            }

            if (this._stakeButton)
            {
                this._stakeButton.isOpen = false;
                this._stakeOpen = false;
            }

            this._onAutoplayRequested.publish();
        }

        @Callback
        protected handleAutoplayStopClicked()
        {
            this._onAutoplayStopRequested.publish();
        }

        @Callback
        protected handleLeftHandButtonClicked()
        {
            this._leftHandMode = true;
            this.handleOrientationChanged(Orientation.currentOrientation);
        }

        @Callback
        protected handleRightHandButtonClicked()
        {
            this._leftHandMode = false;
            this.handleOrientationChanged(Orientation.currentOrientation);
        }

        @Callback
        protected handleTurboToggledOn()
        {
            this._onTurboRequested.publish();
        }

        @Callback
        protected handleTurboToggledOff()
        {
            this._onTurboStopRequested.publish();
        }

        @Callback
        protected handleAllowSpinArbiterValueChanged(): void
        {
            if (this._spinQueued && this.settings.allowSpinArbiter.value)
            {
                Log.debug("quickSpin: onSpinRequested published, spin no longer queued");
                this._onSpinRequested.publish();
                this._spinQueued = false;

                if (this._allowSkipHandle)
                {
                    this._allowSkipHandle.dispose();
                    this._allowSkipHandle = null;
                }
            }
        }

        @Callback
        protected handleOrientationChanged(newOrientation: DeviceOrientation)
        {
            if (this._initSettings)
            {
                this.createUI();

                if (this._autoplayButton && Is.flowElement(this._autoplayButton.parent))
                {
                    this._autoplayButton.parent.invalidateLayout();
                }
            }
        }

        protected createMessageBar(settings: UI.MessageBar.Settings)
        {
            if(this._messageBar)
            {
                const oldParent = this._messageBar.parent;
                let oldIndex: number;
                if(oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._messageBar);
                    oldParent.removeChild(this._messageBar);
                }
                this._messageBar.dispose();
                this._messageBar = UI.messageBar.create(settings);
                UIElement.apply(this._messageBar, settings, this._leftHandMode);

                if(oldParent)
                {
                    oldParent.addChild(this._messageBar, oldIndex);

                    if(Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._messageBar = UI.messageBar.create(settings);
            }
            UI.UIElement.apply(this._messageBar, settings, this._leftHandMode);
            this._messageBar.bindToObservables({ isDisplayed: this.settings.showMessageBarArbiter });
            this._messageBar.bindToObservables({ messageText: this.settings.messageTextArbiter });
        }

        protected createMeterPanel(settings: UI.MeterPanel.Settings)
        {
            if (this._meterPanel)
            {
                const oldParent = this._meterPanel.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._meterPanel);
                    oldParent.removeChild(this._meterPanel);
                }
                //Get exisiting win and balance values
                this.balance = this._meterPanel.balance;
                this.win = this._meterPanel.winAmount;
                this._meterPanel.dispose();
                this._meterPanel = UI.meterPanel.create(settings);

                //Apply win and balance values to new meters
                this._meterPanel.balance = this.balance;
                this._meterPanel.winAmount = this.win ;
                if (oldParent)
                {
                    oldParent.addChild(this._meterPanel, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._meterPanel = UI.meterPanel.create(settings);
            }
            UI.UIElement.apply(this._meterPanel, settings, this._leftHandMode);
            this._meterPanel.bindToObservables({ visible: this.settings.showMeterPanelArbiter });
        }

        protected createBackButton(settings: UI.BackButton.Settings, platform: IPlatform)
        {
            if (this._backButton)
            {
                const oldParent = this._backButton.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._backButton);
                    oldParent.removeChild(this._backButton);
                }
                this._backButton.dispose();
                this._backButton = UI.backButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    showBackButton: platform.canNavigateToLobby
                });
                if (oldParent)
                {
                    oldParent.addChild(this._backButton, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._backButton = UI.backButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    showBackButton: platform.canNavigateToLobby
                });
            }
            UI.UIElement.apply(this._backButton, settings, this._leftHandMode);
            this._backButton.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
        }

        protected createClock(settings: UI.Clock.Settings)
        {
            if (this._clock)
            {
                const oldParent = this._clock.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._clock);
                    oldParent.removeChild(this._clock);
                }
                this._clock.dispose();
                this._clock = UI.clock.create(settings);
                if (oldParent)
                {
                    oldParent.addChild(this._clock, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._clock = UI.clock.create(settings);
            }
            UI.UIElement.apply(this._clock, settings, this._leftHandMode);

            if (!this.settings.alwaysShowClock)
            {
                this._clock.bindToObservables({ visible: this.settings.showMeterPanelArbiter });
            }
        }

        protected createSpinButton(settings: UI.SpinButton.Settings)
        {
            if (this._spinButton)
            {
                const oldParent = this._spinButton.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._spinButton);
                    oldParent.removeChild(this._spinButton);
                }
                this._spinButton.dispose();
                this._spinButton = UI.spinButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    spinOrSkipArbiter: this.settings.spinOrSkipArbiter,
                    canSpinArbiter: this.settings.allowSpinArbiter,
                    canSkipArbiter: this.settings.allowSkipArbiter,
                    canQuickSpinArbiter: this.settings.allowQuickSpinArbiter
                });
                if (oldParent)
                {
                    oldParent.addChild(this._spinButton, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._spinButton = UI.spinButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    spinOrSkipArbiter: this.settings.spinOrSkipArbiter,
                    canSpinArbiter: this.settings.allowSpinArbiter,
                    canSkipArbiter: this.settings.allowSkipArbiter,
                    canQuickSpinArbiter: this.settings.allowQuickSpinArbiter
                });
            }
            UI.UIElement.apply(this._spinButton, settings, this._leftHandMode);
            this._spinButton.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
        }

        protected createAutoplayButton(settings: UI.AutoplayButton.Settings)
        {
            if (this._autoplayButton)
            {
                const oldParent = this._autoplayButton.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._autoplayButton);
                    oldParent.removeChild(this._autoplayButton);
                }
                this._autoplayButton.dispose();
                this._autoplayButton = UI.autoplayButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    autoplaysRemainingObservable: this.settings.autoplaysRemainingObservable,
                    canAutoplayObservable: this.settings.allowAutoplayArbiter
                });
                if (oldParent)
                {
                    oldParent.addChild(this._autoplayButton, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._autoplayButton = UI.autoplayButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    autoplaysRemainingObservable: this.settings.autoplaysRemainingObservable,
                    canAutoplayObservable: this.settings.allowAutoplayArbiter
                });
            }
            UI.UIElement.apply(this._autoplayButton, settings, this._leftHandMode);
            this._autoplayButton.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
        }

        protected createStakeButton(settings: UI.StakeButton.Settings)
        {
            if (this._stakeButton && this._initSettings)
            {
                //Preserve spinner indices
                const spinnerValues: number[] = [];
                const spinnerIndices: number[] = [];

                if (this.__totalBetSpinnerIndex != null && this._stakeButton.getSpinner(this.__totalBetSpinnerIndex) != null)
                {
                    spinnerIndices.push(this.__totalBetSpinnerIndex);
                    spinnerValues.push(this._stakeButton.getSpinner(this.__totalBetSpinnerIndex).optionIndex);
                }
                if (this.__linesSpinnerIndex != null && this._stakeButton.getSpinner(this.__linesSpinnerIndex) != null)
                {
                    spinnerIndices.push(this.__linesSpinnerIndex);
                    spinnerValues.push(this._stakeButton.getSpinner(this.__linesSpinnerIndex).optionIndex);
                }
                if (this.__betPerLineSpinnerIndex != null && this._stakeButton.getSpinner(this.__betPerLineSpinnerIndex) != null)
                {
                    spinnerIndices.push(this.__betPerLineSpinnerIndex);
                    spinnerValues.push(this._stakeButton.getSpinner(this.__betPerLineSpinnerIndex).optionIndex);
                }
                if (this.__betMultiplierSpinnerIndex != null && this._stakeButton.getSpinner(this.__betMultiplierSpinnerIndex) != null)
                {
                    spinnerIndices.push(this.__betMultiplierSpinnerIndex);
                    spinnerValues.push(this._stakeButton.getSpinner(this.__betMultiplierSpinnerIndex).optionIndex);
                }

                const oldParent = this._stakeButton.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._stakeButton);
                    oldParent.removeChild(this._stakeButton);
                }
                this._stakeButton.dispose();
                this._stakeButton = UI.stakeButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    canChangeBetArbiter: this.settings.allowChangeBetArbiter,
                    spinners: this._spinners
                });
                UI.UIElement.apply(this._stakeButton, settings, this._leftHandMode);
                this._stakeButton.isOpen = this._stakeOpen;

                if (oldParent)
                {
                    oldParent.addChild(this._stakeButton, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
                for (let i = 0, l = this._initSettings.inputOptions.length; i < l; ++i)
                {
                    this.setInputOptions(this._initSettings.inputOptions[i].type, this._initSettings.inputOptions[i].options);
                }

                if (spinnerValues != null && spinnerIndices != null)
                {
                    for (let i = 0; i < spinnerValues.length; i++)
                    {
                        const spinner = this._stakeButton.getSpinner(spinnerIndices[i]);
                        spinner.optionIndex = spinnerValues[i];
                    }
                }
            }
            else
            {
                this._stakeButton = UI.stakeButton.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    canChangeBetArbiter: this.settings.allowChangeBetArbiter,
                    spinners: this._spinners
                });
            }
            UI.UIElement.apply(this._stakeButton, settings, this._leftHandMode);
            this._stakeButton.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
        }

        protected createSettingsPanel(settings: UI.SettingsPanel.Settings)
        {
            if (this._settingsPanel)
            {
                //cache old list state
                const oldListOpen = this._settingsPanel.isOpen;

                const oldParent = this._settingsPanel.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._settingsPanel);
                    oldParent.removeChild(this._settingsPanel);
                }
                this._settingsPanel.dispose();
                this._settingsPanel = UI.settingsPanel.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    paytableEnabledArbiter: this.settings.paytableEnabledArbiter,
                    muteArbiter: this.settings.muteArbiter
                });

                //Restore state of UI
                UI.UIElement.apply(this._settingsPanel, settings, this._leftHandMode);
                this._settingsPanel.toggleSettings(oldListOpen);
                this._settingsPanel.toggleLeftHandSwitch(this._leftHandMode);
                this._settingsPanel.applyLeftHandMode(this._leftHandMode);
                if (oldParent)
                {
                    oldParent.addChild(this._settingsPanel, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._settingsPanel = UI.settingsPanel.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter,
                    paytableEnabledArbiter: this.settings.paytableEnabledArbiter,
                    muteArbiter: this.settings.muteArbiter
                });
            }
            UI.UIElement.apply(this._settingsPanel, settings, this._leftHandMode);
            this._settingsPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
        }

        protected createTurboToggle(settings: UI.TurboToggle.Settings)
        {
            if (this._turboToggle)
            {
                const oldParent = this._turboToggle.parent;
                let oldIndex: number;
                if (oldParent)
                {
                    oldIndex = oldParent.getChildIndex(this._turboToggle);
                    oldParent.removeChild(this._turboToggle);
                }
                this._turboToggle.dispose();
                this._turboToggle = UI.turboToggle.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter
                });
                UI.UIElement.apply(this._turboToggle, settings, this._leftHandMode);

                if (oldParent)
                {
                    oldParent.addChild(this._turboToggle, oldIndex);

                    if (Is.flowElement(oldParent))
                    {
                        oldParent.invalidateLayout();
                    }
                }
            }
            else
            {
                this._turboToggle = UI.turboToggle.create(settings,
                {
                    enableInteractionArbiter: this.settings.enableInteractionArbiter
                });
            }
            UI.UIElement.apply(this._turboToggle, settings, this._leftHandMode);
            this._turboToggle.bindToObservables({ visible: this.settings.showTurboArbiter });
            this._turboToggle.bindToObservables({ isTurboOn: this.settings.turboModeArbiter });
        }

        protected getDeviceSettings(isDesktop: boolean)
        {
            return isDesktop ? this._uiSettings.desktop : this._uiSettings.mobile;
        }

        protected getOrientationSettings<T>(orientationSettings: UI.UISettings.OrientationSettings<T>, orientation: DeviceOrientation): T
        {
            return orientation ? orientationSettings.portrait : orientationSettings.landscape;
        }
    }

    Slots.UI.IUIController.register(UIController);
}