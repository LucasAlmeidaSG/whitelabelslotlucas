/// <reference path="../elements/PaytableNavBar.ts"/>

namespace RS.GDM.UI.Views
{
    @HasCallbacks
    @View.ScreenName("Paytable New")
    @View.RequiresAssets([GDM.UI.Assets.Groups.gdmUI])
    export class Paytable<TSettings extends Paytable.Settings = Paytable.Settings, TContext extends Slots.Game.Context = Slots.Game.Context> extends View.Base<TSettings, TContext>
    {
        @AutoDisposeOnSet protected _background: Flow.Background | void;
        @AutoDisposeOnSet protected _navBar: GDM.UI.PaytableNavBar;
        @AutoDisposeOnSet protected _body: GDM.UI.PaytableBody;

        @AutoDisposeOnSet protected _logo: Flow.Image;

        @AutoDisposeOnSet protected _pipClickedHandle: IDisposable;
        @AutoDisposeOnSet protected _uiEnabledHandle: IDisposable;
        @AutoDisposeOnSet protected _showButtonAreaHandle: IDisposable;
        @AutoDisposeOnSet protected _showMeterPanelHandle: IDisposable;
        @AutoDisposeOnSet protected _showMessageBarHandle: IDisposable;

        protected _suppressionCount: number = 0;

        protected _sections: Paytable.Section[];

        protected _curSectionIndex: number;
        protected _curPageIndex: number;

        protected _changingPage: boolean = false;
        protected _sectionPageSetting: Paytable.PageSetting[][];

        protected get navigationEnabled()
        {
            return this._body.closeButtonEnabled || this._navBar.prevEnabled || this._navBar.nextEnabled;
        }
        protected set navigationEnabled(value: boolean)
        {
            this.suppressLayouting();
            this._body.closeButtonEnabled = value;
            this._navBar.nextEnabled = value;
            this._navBar.prevEnabled = value;
            this._navBar.interactiveChildren = value;
            this.restoreLayouting();
        }

        public suppressLayouting()
        {
            ++this._suppressionCount;
            if (this._suppressionCount === 1)
            {
                return super.suppressLayouting();
            }
            return this;
        }

        public restoreLayouting(forceInvalidate?: boolean)
        {
            if (this._suppressionCount === 0)
            {
                return this;
            }
            --this._suppressionCount;
            if (this._suppressionCount === 0)
            {
                return super.restoreLayouting(forceInvalidate);
            }
            return this;
        }

        public onOpened()
        {
            super.onOpened();

            this.suppressLayouting();

            this.locale = this.context.game.locale;
            this.currencyFormatter = this.context.game.currencyFormatter;

            const currentOrientation: DeviceOrientation = this.context.game.orientation.value;
            const isLandscape: boolean = currentOrientation === DeviceOrientation.Landscape;
            const orientationSettings: Paytable.OrientationSettings = isLandscape ? this.settings.landscape : this.settings.portrait;

            this.initBody();

            this.buildSections();

            this.initBackground();
            this.initLogo(orientationSettings);
            this.initNavBar();

            this._uiEnabledHandle = this.context.game.arbiters.uiEnabled.declare(false);
            this._showButtonAreaHandle = this.context.game.arbiters.showButtonArea.declare(false);
            this._showMeterPanelHandle = this.context.game.arbiters.showMeterPanel.declare(false);
            this._showMessageBarHandle = this.context.game.arbiters.showMessageBar.declare(false);

            this.gotoPage(0, 0);

            this.doLayout(currentOrientation);

            this.restoreLayouting();
        }

        public dispose(): void
        {
            if (this.isDisposed) { return; }
            for (let i = 0; i < this._sections.length; i++)
            {
                for (let j = 0; j < this._sections[i].pages.length; j++)
                {
                    if (this._sections[i].pages[j].element)
                    {
                        this._sections[i].pages[j].element.dispose();
                        this._sections[i].pages[j].element = null;
                    }
                }
            }
            super.dispose();
        }

        //#region Create

        protected initBackground(): void
        {
            if (!this.settings.background) { return; }
            this._background = Flow.background.create(this.settings.background, this.visibleRegionPanel);
            this.visibleRegionPanel.moveToBottom(this._background);
        }

        protected initBody(): void
        {
            this._body = paytableBody.create(this.settings.body, this.visibleRegionPanel);
            this._body.onCloseClicked(this.handleCloseClicked);
        }

        protected initLogo(orientationSettings: Paytable.OrientationSettings): void
        {
            if (orientationSettings.logo != null)
            {
                this._logo = Flow.image.create(orientationSettings.logo, this.visibleRegionPanel);
            }
        }

        protected initNavBar(): void
        {
            this._navBar = paytableNavBar.create(this.settings.navBar, { sections: this._sections.map((s) => s.pages.length) });

            this._pipClickedHandle = this._navBar.onPipClicked(({ section, page }) =>
            {
                this.gotoPage(section, page);
            });
            this._navBar.onPrevClicked(this.handleLeftClicked);
            this._navBar.onNextClicked(this.handleRightClicked);
        }

        protected buildSections(): void
        {
            this._sections = [];
            let sectionIndex = 0;
            this._sectionPageSetting = [];
            for (const section of this.settings.sections)
            {
                const s: Paytable.Section = { pages: [] };
                let pageIndex = 0;
                this._sectionPageSetting[sectionIndex] = [];
                for (const pageInfo of section.pages)
                {
                    let pages = this.buildPage(pageInfo);

                    for (let i = 0; i < pages.length; i++)
                    {
                        this._sectionPageSetting[sectionIndex][pageIndex] =
                        {
                            element: pageInfo,
                            pageIndex: i
                        };
                        pageIndex += 1;
                    }

                    if (!Is.array(pages))
                    {
                        pages = [pages];
                    }

                    for (const pageElement of pages)
                    {
                        s.pages.push(
                        {
                            title: pageInfo.title,
                            element: pageElement
                        });
                    }
                }
                this._sections.push(s);
                sectionIndex += 1;
            }

            if (this._sections.length === 0)
            {
                const tmpLabel: Flow.Label.Settings =
                {
                    dock: Flow.Dock.Fill,
                    text: "Add some paytable pages to your game!",
                    font: Fonts.Frutiger.CondensedMedium,
                    fontSize: 32,
                    textColor: Util.Colors.white
                };
                this._body.page = Flow.label.create(tmpLabel);
            }
        }

        protected buildPage(pageInfo: Paytable.PageInfo)
        {
            let pages = pageInfo.element.create(
            {
                configModel: this.context.game.models.config,
                stakeModel: this.context.game.models.stake
            });
            if (!Is.array(pages))
            {
                pages = [pages];
            }
            return pages;
        }

        //#endregion Create
        //#region Update

        protected gotoPage(sectionIndex: number, pageIndex: number): void
        {
            if (this._changingPage) { return; }
            // only change one page at a time, to avoid errors
            this._changingPage = true;
            this.suppressLayouting();

            this.disableButtonsForDuration();
            const oldSectionIndex = this._curSectionIndex;
            const oldPageIndex = this._curPageIndex;

            this._curSectionIndex = sectionIndex;
            this._curPageIndex = pageIndex;

            //rebuild page if needed
            const section = this._sections[this._curSectionIndex];
            const pageElement = section.pages[this._curPageIndex].element;
            if (!pageElement)
            {
                const pageInfo = this._sectionPageSetting[this._curSectionIndex][this._curPageIndex].element;
                //incase there are multiple pages from this info we now know which one to use
                const elementIndex = this._sectionPageSetting[this._curSectionIndex][this._curPageIndex].pageIndex;
                const builtPages = this.buildPage(pageInfo);
                this._sections[this._curSectionIndex].pages[this._curPageIndex].element = builtPages[elementIndex];
            }

            this.normalisePage();
            const curSection = this._sections[this._curSectionIndex];
            if (curSection == null) { return; }

            this._body.page = curSection.pages[this._curPageIndex].element;
            this._navBar.sectionIndex = this._curSectionIndex;
            this._navBar.pageIndex = this._curPageIndex;
            this._body.pageTitle = curSection.pages[this._curPageIndex].title;

            // Calculate the global page index
            let pageID = 0;
            for (let i = 0; i < sectionIndex; i++)
            {
                pageID += this._sections[i].pages.length;
            }
            pageID += pageIndex;
            // Better way to expose this data?
            SGI.Selenium.SeleniumControl.setText(`${pageID}`);
            this.restoreLayouting();
            this.invalidateLayout();
            // delete old page for performance(memory) increase
            if (oldSectionIndex != null && oldPageIndex != null)
            {
                this._sections[oldSectionIndex].pages[oldPageIndex].element.dispose();
                this._sections[oldSectionIndex].pages[oldPageIndex].element = null;
            }
            this._changingPage = false;
        }

        protected normalisePage(): void
        {
            this._curSectionIndex = Math.clamp(this._curSectionIndex, 0, this._sections.length);
            const curSection = this._sections[this._curSectionIndex];
            if (curSection == null) { return; }
            this._curPageIndex = Math.clamp(this._curPageIndex, 0, curSection.pages.length);
        }

        protected gotoNextPage(): void
        {
            this.normalisePage();
            const curSection = this._sections[this._curSectionIndex];
            let nextPage = this._curPageIndex + 1;
            let nextSection = this._curSectionIndex;
            if (nextPage >= curSection.pages.length)
            {
                nextPage = 0;
                nextSection++;
                if (nextSection >= this._sections.length)
                {
                    nextSection = 0;
                }
            }
            this.gotoPage(nextSection, nextPage);
        }

        protected gotoPreviousPage(): void
        {
            this.normalisePage();
            let nextPage = this._curPageIndex - 1;
            let nextSection = this._curSectionIndex;
            if (nextPage < 0)
            {
                nextSection--;
                if (nextSection < 0)
                {
                    nextSection = this._sections.length - 1;
                }
                nextPage = this._sections[nextSection].pages.length - 1;
            }
            this.gotoPage(nextSection, nextPage);
        }

        protected disableButtonsForDuration()
        {
            this.navigationEnabled = false;
            const duration = this.settings.pageDelay == null ? 250 : this.settings.pageDelay;
            RS.ITicker.get().after(duration).then(() => this.navigationEnabled = true);
        }

        //#endregion Update
        //#region Event

        @Callback
        protected handleCloseClicked(): void
        {
            this.navigationEnabled = false;
            this.controller.close();
        }

        @Callback
        protected handleLeftClicked(): void
        {
            this.gotoPreviousPage();
        }

        @Callback
        protected handleRightClicked(): void
        {
            this.gotoNextPage();
        }

        //#endregion Event

        @Callback
        protected handleOrientationChanged(newOrientation: DeviceOrientation): void
        {
            super.handleOrientationChanged(newOrientation);

            this.doLayout(newOrientation);
        }

        protected doLayout(orientation: DeviceOrientation): void
        {
            this.suppressLayouting();

            const isLandscape: boolean = orientation === DeviceOrientation.Landscape;
            const orientationSettings: Paytable.OrientationSettings = isLandscape ? this.settings.landscape : this.settings.portrait;
            const bottomBarHeight: number = this.context.game.uiController.enclosedSpace.bottom;

            this._body.suppressLayouting();
            this.visibleRegionPanel.removeAllChildren();
            this.visibleRegionPanel.addChild(this._body);
            this._body.restoreLayouting(false);

            this.visibleRegionPanel.innerPadding =
            {
                ...Flow.Spacing.none,
                bottom: bottomBarHeight
            };

            if (orientation === DeviceOrientation.Landscape)
            {
                this._body.mode = GDM.UI.PaytableBody.Mode.Landscape;
            }
            else if (orientation === DeviceOrientation.Portrait)
            {
                this._body.mode = GDM.UI.PaytableBody.Mode.Portrait;
            }

            this._body.body.addChild(this._navBar, 0);

            this.initLogo(orientationSettings);

            if (this._background) { this.visibleRegionPanel.addChild(this._background, 0); }

            this.restoreLayouting();
        }
    }

    export namespace Paytable
    {
        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Paytable,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        const defaultContainerSettings: Flow.Container.Settings =
        {
            dock: Flow.Dock.Top,
            sizeToContents: true,
            expand: Flow.Expand.Allowed
        };

        export interface Section
        {
            pages:
            {
                title: Localisation.LocalisableString;
                element: Flow.GenericElement;
            }[];
        }

        export interface PageInfo
        {
            title: Localisation.LocalisableString;
            element: Slots.Paytable.IPageFactory<Slots.Paytable.Pages.Base.Settings, Slots.Paytable.Pages.Base.RuntimeData>;
        }

        export interface SectionInfo
        {
            title: Localisation.LocalisableString;
            pages: PageInfo[];
        }

        export interface DimensionSettings
        {
            landscapeWidth: number;
            portraitHeight: number;
        }

        export interface OrientationSettings
        {
            buttonList: Flow.List.Settings;
            logo?: Flow.Image.Settings;
        }

        export interface Settings extends View.Base.Settings
        {
            portrait: OrientationSettings;
            landscape: OrientationSettings;

            bottomContainer: Flow.Container.Settings;
            topContainer: Flow.Container.Settings;
            navBar: PaytableNavBar.Settings;
            body: PaytableBody.Settings;
            background?: Flow.Background.Settings;

            sections: Paytable.SectionInfo[];

            /**
             * If set to true, it will add the buttons list as a child before the body, in the visible region panel.
             * The body will therefore shrink to ensure the buttons are always full size
             */
            spacePriorityToButtons?: boolean;

            /**
             * Delay in ms between page changes. Prevents spamming of next/prev buttons that lead to reaching memory limits.
             * Defaults to 250
            */
             pageDelay?: number;
        }

        export let defaultSettings: Settings =
        {
            landscape:
            {
                buttonList:
                {
                    dock: Flow.Dock.Right,
                    sizeToContents: true,
                    direction: Flow.List.Direction.TopToBottom,
                    spacing: Flow.Spacing(0, 20, 50, 20)
                }
            },
            portrait:
            {
                buttonList:
                {
                    dock: Flow.Dock.Bottom,
                    sizeToContents: true,
                    direction: Flow.List.Direction.LeftToRight,
                    spacing: Flow.Spacing(20, 0, 20, 50)
                }
            },
            topContainer:
            {
                ...defaultContainerSettings
            },
            bottomContainer:
            {
                ...defaultContainerSettings,
                dock: Flow.Dock.Bottom
            },
            navBar: PaytableNavBar.defaultSettings,
            body:
            {
                ...PaytableBody.defaultSettings,
                dock: Flow.Dock.Fill
            },
            sections: [],
            pageDelay: 250
        };

        export interface PageSetting
        {
            element: Paytable.PageInfo;
            pageIndex: number;
        }
    }
}