namespace Slots.GDM.Paytable
{
    export interface IPlatform extends RS.IPlatform
    {
        isNT?: boolean;
    }
}