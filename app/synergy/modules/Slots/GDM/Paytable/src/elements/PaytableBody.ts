namespace RS.GDM.UI
{
    export class PaytableBody extends Flow.BaseElement<PaytableBody.Settings>
    {
        @AutoDispose protected _bgLandscape: Flow.Background;
        @AutoDispose protected _bgPortrait: Flow.Background;
        @AutoDispose protected _title: Flow.Label;
        @AutoDispose protected _closeButton: Flow.Button;
        @AutoDispose protected _topContainer: Flow.Container;
        @AutoDispose protected _body: Flow.Container;

        protected _page: Flow.GenericElement | null = null;
        protected _mode: PaytableBody.Mode;

        /** Published when the spin button has been clicked. */
        public get onCloseClicked() { return this._closeButton.onClicked; }
        public get closeButtonEnabled() { return this._closeButton.enabled; }
        public set closeButtonEnabled(value: boolean)
        {
            if (value === this._closeButton.enabled){ return; }
            this._closeButton.enabled = value;
        }

        /** Gets or sets the paytable page to be displayed. */
        public get page() { return this._page; }
        public set page(value)
        {
            if (value === this._page) { return; }
            if (this._page != null)
            {
                this._page.suppressLayouting();
                this._body.removeChild(this._page);
                this._page.restoreLayouting(false);
                this._page = null;
            }
            this._page = value;
            if (this._page != null)
            {
                this._page.suppressLayouting();
                this._body.addChild(this._page);
                this._page.restoreLayouting(false);
            }
            this.invalidateLayout();
        }

        public get body() { return this._body; }

        /** Gets or sets the title of the paytable page to be displayed. */
        public get pageTitle() { return this._title.text; }
        public set pageTitle(value) { this._title.text = value; }

        /** Gets or sets the mode of this nav bar. */
        public get mode() { return this._mode; }
        public set mode(value)
        {
            if (this._mode === value) { return; }
            this._mode = value;
            this.suppressLayouting();
            switch (value)
            {
                case PaytableBody.Mode.Landscape:
                    this._bgLandscape.visible = true;
                    this._bgPortrait.visible = false;
                    break;
                case PaytableBody.Mode.Portrait:
                    this._bgLandscape.visible = false;
                    this._bgPortrait.visible = true;
                    break;
            }
            this.restoreLayouting();
        }

        public constructor(settings: PaytableBody.Settings)
        {
            super(settings, undefined);

            this._bgLandscape = Flow.background.create(settings.backgroundLandscape, this);
            this._bgPortrait = Flow.background.create(settings.backgroundPortrait, this);
            this._topContainer = Flow.container.create(settings.topContainer, this);
            this._title = Flow.label.create(settings.title, this._topContainer);
            this._closeButton = Flow.button.create(settings.closeButton, this._topContainer);
            this._body = Flow.container.create({
                sizeToContents: false,
                dock: Flow.Dock.Fill,
                legacy: false
            }, this);

            this.mode = PaytableBody.Mode.Landscape;
        }
    }

    export const paytableBody = Flow.declareElement(PaytableBody);

    export namespace PaytableBody
    {
        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Paytable,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        export enum Mode
        {
            Landscape,
            Portrait
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            backgroundLandscape: Flow.Background.Settings;
            backgroundPortrait: Flow.Background.Settings;
            title: Flow.Label.Settings;
            closeButton: Flow.Button.Settings;
            topContainer: Flow.Container.Settings;
        }

        export let defaultSettings: Settings =
        {
            backgroundLandscape:
            {
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Paytable,
                frame: Assets.UI.Paytable.background
            },
            backgroundPortrait:
            {
                kind: Flow.Background.Kind.ImageFrame,
                asset: Assets.UI.Paytable,
                frame: Assets.UI.Paytable.background
            },
            title:
            {
                dock: Flow.Dock.Fill,
                font: Fonts.Frutiger.CondensedMedium,
                fontSize: 32,
                textColor: Util.Colors.black,
                sizeToContents: true,
                dockAlignment: { x: 0.5, y: 0.5 },
                text: ""
            },
            closeButton:
            {
                name: "Close",
                background: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.cross },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                dock: Flow.Dock.Right,
                seleniumId: "ExitHelpButton"
            },
            topContainer:
            {
                dock: Flow.Dock.Top,
                sizeToContents: true,
                expand: Flow.Expand.Allowed
            },
            spacing: Flow.Spacing.all(20),
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 0.5 },
            dockAlignment: { x: 0.5, y: 0.5 },
            sizeToContents: false,
            size: {w: 1000, h: 700},
            expand: Flow.Expand.Disallowed
        };
    }
}
