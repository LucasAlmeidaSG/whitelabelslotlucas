namespace RS.GDM.UI
{
    @HasCallbacks
    export class PaytableNavBar extends Flow.BaseElement<PaytableNavBar.Settings, PaytableNavBar.RuntimeData>
    {
        @AutoDisposeOnSet protected readonly _onPipClicked: IPrivateEvent<{ section: number, page: number }> = createEvent<{ section: number, page: number }>();

        @AutoDisposeOnSet protected readonly _bg: Flow.Background;
        @AutoDisposeOnSet protected readonly _list: Flow.List;

        @AutoDisposeOnSet protected _prevButton: Flow.Button;
        @AutoDisposeOnSet protected _nextButton: Flow.Button;
        @AutoDisposeOnSet protected _buttonList: Flow.List;

        @AutoDisposeOnSet protected _sections: PaytableNavBar.Section[][];

        @AutoDisposeOnSet protected _sectionIndex: number = 0;
        @AutoDisposeOnSet protected _pageIndex: number = 0;

        public get onPipClicked() { return this._onPipClicked.public; }
        public get onNextClicked() { return this._nextButton.onClicked; }
        public get onPrevClicked() { return this._prevButton.onClicked; }

        public get nextEnabled() { return this._nextButton.enabled; }
        public set nextEnabled(value: boolean)
        {
            if (this._nextButton.enabled === value) { return; }
            this._nextButton.enabled = value;
        }

        public get prevEnabled() { return this._nextButton.enabled; }
        public set prevEnabled(value: boolean)
        {
            if (this._prevButton.enabled === value) { return; }
            this._prevButton.enabled = value;
        }

        /** Gets or sets the currently selected section index. */
        public get sectionIndex() { return this._sectionIndex; }
        public set sectionIndex(value)
        {
            if (this._sectionIndex == value) { return; }
            this._sectionIndex = value;
            this.update();
        }

        /** Gets or sets the currently selected section index. */
        public get pageIndex() { return this._sectionIndex; }
        public set pageIndex(value)
        {
            if (this._pageIndex == value) { return; }
            this._pageIndex = value;
            this.update();
        }

        public constructor(settings: PaytableNavBar.Settings, runtimeData: PaytableNavBar.RuntimeData)
        {
            super(settings, runtimeData);

            if (settings.background != null)
            {
                this._bg = Flow.background.create(settings.background, this);
            }

            this._buttonList = Flow.list.create(settings.buttonList, this);
            this._prevButton = Flow.button.create(settings.prevButton);
            this._list = Flow.list.create(settings.dotList);
            this._nextButton = Flow.button.create(settings.nextButton);

            const catCount = runtimeData.sections.length;
            this._sections = new Array<PaytableNavBar.Section[]>(catCount);

            for (let catIndex = 0; catIndex < catCount; catIndex++)
            {
                const pageCount: number = runtimeData.sections[catIndex];

                this._sections[catIndex] = new Array<PaytableNavBar.Section>(pageCount);

                for (let pageIndex: number = 0; pageIndex < pageCount; pageIndex++)
                {
                    const inactiveDot = Flow.image.create(settings.inactiveDot, this._list);
                    inactiveDot.onClicked(this.handleClick);
                    inactiveDot.buttonMode = true;

                    const activeDot = Flow.image.create(settings.activeDot, this._list);

                    this._sections[catIndex][pageIndex] = { inactiveDot, activeDot };
                }
            }

            this._buttonList.addChildren(this._prevButton, this._list, this._nextButton);
            this.update();
        }

        protected update(): void
        {
            this.suppressLayouting();

            const catCount = this.runtimeData.sections.length;

            for (let catIndex = 0; catIndex < catCount; catIndex++)
            {
                const pageCount: number = this.runtimeData.sections[catIndex];

                for (let pageIndex: number = 0; pageIndex < pageCount; pageIndex++)
                {
                    const section = this._sections[catIndex][pageIndex];

                    section.activeDot.visible = catIndex === this._sectionIndex && pageIndex === this._pageIndex;
                    section.inactiveDot.visible = !section.activeDot.visible;
                }
            }

            this.restoreLayouting();
        }

        @Callback
        protected handleClick(data: Rendering.InteractionEventData): void
        {
            const index: number = this._list.getChildIndex(data.target);

            if (index > -1)
            {
                let sectionIndex: number = 0;
                let pageIndex: number = 0;
                let runningTotal: number = 0;

                const isInactivePip: boolean = index % 2 === 0;
                const adjustedSectionIndex: number = isInactivePip ? index / 2 : (index - 1) / 2;

                this._sections.forEach((section: PaytableNavBar.Section[], sIndex: number) =>
                {
                    section.forEach((page: PaytableNavBar.Section, pIndex: number) =>
                    {
                        if (runningTotal === adjustedSectionIndex)
                        {
                            sectionIndex = sIndex;
                            pageIndex = pIndex;
                        }

                        runningTotal++;
                    });
                });

                this._onPipClicked.publish({section: sectionIndex, page: pageIndex});

                if (this.settings.clickSound != null)
                {
                    Audio.play(this.settings.clickSound);
                }
            }
        }
    }

    export const paytableNavBar = Flow.declareElement(PaytableNavBar, true);

    export namespace PaytableNavBar
    {
        const defaultButtonBackground: Flow.Background.Settings =
        {
            kind: Flow.Background.Kind.ImageFrame,
            dock: Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.UI.Paytable,
            frame: 0,
            sizeToContents: true,
            expand: Flow.Expand.Disallowed
        };

        export interface Section
        {
            inactiveDot: Flow.Image;
            activeDot: Flow.Image;
        }

        export interface Settings extends Partial<Flow.ElementProperties>
        {
            background?: Flow.Background.Settings;
            dotList: Flow.List.Settings;
            activeDot: Flow.Image.Settings;
            inactiveDot: Flow.Image.Settings;
            clickSound?: Asset.SoundReference;
            nextButton: Flow.Button.Settings;
            prevButton: Flow.Button.Settings;
            buttonList: Flow.List.Settings;
        }

        export interface RuntimeData
        {
            sections: number[];
        }

        export let defaultSettings: Settings =
        {
            activeDot: // 31 x 30
            {
                name: "Page Indicator Active",
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Assets.UI.Paytable,
                animationName: Assets.UI.Paytable.active_circle,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                border: Flow.Spacing.all(4)
            },
            inactiveDot: // 23 x 22
            {
                name: "Page Indicator Inactive",
                kind: Flow.Image.Kind.SpriteFrame,
                asset: Assets.UI.Paytable,
                animationName: Assets.UI.Paytable.inactive_circle,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                border: Flow.Spacing.none
            },
            dotList:
            {
                name: "Page Indicator List",
                direction: Flow.List.Direction.LeftToRight,
                dock: Flow.Dock.Fill,
                contentAlignment: { x: 0.5, y: 0.5 },
                sizeToContents: true,
                spacing: Flow.Spacing.axis(5, 15),
                evenlySpaceItems: true
            },
            buttonList:
            {
                name: "Button List",
                direction: Flow.List.Direction.LeftToRight,
                dock: Flow.Dock.Bottom,
                sizeToContents: true,
                spacing: Flow.Spacing.horizontal(10),
                evenlySpaceItems: true
            },
            prevButton:
            {
                name: "Left",
                background: { ...defaultButtonBackground, frame: Assets.UI.Paytable.left_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.left_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.left_button_hover },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.left_button },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "PreviousHelpButton"
            },
            nextButton:
            {
                name: "Right",
                background: { ...defaultButtonBackground, frame: Assets.UI.Paytable.right_button },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.right_button_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.right_button_hover },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.UI.Paytable.right_button },
                spacing: Flow.Spacing.none,
                sizeToContents: true,
                expand: Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "NextHelpButton"
            },
            sizeToContents: true,
            expand: Flow.Expand.Disallowed,
            dock: Flow.Dock.Bottom,
            spacing: Flow.Spacing.all(10)
        };
    }
}