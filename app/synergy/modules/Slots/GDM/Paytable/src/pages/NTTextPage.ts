
namespace Slots.GDM.Paytable.Pages
{
    export class NTTextPage extends RS.Slots.Paytable.Pages.TextPage<NTTextPage.Settings>
    {
        protected shouldExcludeElement(element: NTTextPage.NTElementSettings): boolean
        {
            const platform: IPlatform = RS.IPlatform.get();
            const isNorskTipping = platform.isNT || false;

            if (super.shouldExcludeElement(element)) { return true; }
            if (element.ntDisplayMode == NTTextPage.NTDisplayMode.NotNT && isNorskTipping) { return true; }
            if (element.ntDisplayMode == NTTextPage.NTDisplayMode.NTOnly && !isNorskTipping) { return true; }
            return false;
        }
    }

    export namespace NTTextPage
    {
        export type NTElementSettings = RS.Slots.Paytable.Pages.TextPage.Element & { ntDisplayMode?: NTDisplayMode };

        export interface Settings extends RS.Slots.Paytable.Pages.TextPage.Settings
        {
            /** List of elements to be added to the page */
            elements: NTElementSettings[];
        }

        export enum NTDisplayMode
        {
            Always,
            NTOnly,
            NotNT
        }
    }

}