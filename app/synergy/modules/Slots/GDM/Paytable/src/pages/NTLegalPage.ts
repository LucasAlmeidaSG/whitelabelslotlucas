/// <reference path="../generated/Translations.ts" />
namespace Slots.GDM.Paytable.Pages
{
    export namespace NTLegalTextPage
    {
        const jackpotLines: (hasJackpots: boolean) => RS.Slots.Paytable.Pages.TextPage.Element[] = (hasJackpots: boolean) =>
        {
            return hasJackpots ? [
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.HasJackpot
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Winnings
                },
            ] :
            [
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.NoJackpot
                }
            ]
        }

        export const Settings: (hasJackpots: boolean) => RS.Slots.Paytable.Pages.TextPage.Settings = (hasJackpots: boolean) =>
        ({
            ...RS.Slots.Paytable.Pages.TextPage.defaultSettings,
            pageSettings:
            {
                ...RS.Slots.Paytable.Pages.TextPage.defaultSettings.pageSettings,
                listSettings:
                {
                    ...RS.Slots.Paytable.Pages.TextPage.defaultSettings.pageSettings.listSettings,
                    spacing: RS.Flow.Spacing.vertical(10),
                },
            },
            elements:
            [
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.MaxWinText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.MaxWin
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.MaxBetText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.MaxBet
                },
                ...jackpotLines(hasJackpots),
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Skills
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.NoInfluence
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Malfunction
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Unfinished
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.UnfinishedExample
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Resuming
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Stored
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Slots.GDM.Paytable.Translations.Paytable.Disclaimer.Timeout
                }
            ]
        });
    }
}