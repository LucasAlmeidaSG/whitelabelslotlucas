
/// <reference path="LegalTextPage.ts" />
/// <reference path="NTLegalPage.ts" />
namespace Slots.GDM.Paytable.Pages
{
    export class LegalPagesFactory implements RS.Slots.Paytable.IPageFactory<NTTextPage.Settings, RS.Slots.Paytable.Pages.Base.RuntimeData>
    {
        constructor(protected readonly ctor: new (settings: NTTextPage.Settings, runtimeData: RS.Slots.Paytable.Pages.Base.RuntimeData) => NTTextPage = NTTextPage, protected readonly settings: LegalPagesFactory.Settings = LegalPagesFactory.defaultSettings)
        {

        }

        public create(overrideSettings: Partial<NTTextPage.Settings>, runtimeData: RS.Slots.Paytable.Pages.Base.RuntimeData): NTTextPage[];

        public create(runtimeData: RS.Slots.Paytable.Pages.Base.RuntimeData): NTTextPage[];

        public create(overrideSettingsOrRuntimeData: Partial<NTTextPage.Settings> | RS.Slots.Paytable.Pages.Base.RuntimeData, runtimeData?: RS.Slots.Paytable.Pages.Base.RuntimeData): NTTextPage[]
        {
            let overrideSettings: Partial<NTTextPage.Settings> = {};
            if (runtimeData)
            {
                overrideSettings = overrideSettingsOrRuntimeData;
            }
            else
            {
                runtimeData = overrideSettingsOrRuntimeData as RS.Slots.Paytable.Pages.Base.RuntimeData;
            }

            const platform: IPlatform = RS.IPlatform.get();
            const isNorskTipping: boolean = platform.isNT || false;
            const pageSettingsList: NTTextPage.Settings[] = [];
            if (runtimeData.configModel.minRTP == runtimeData.configModel.maxRTP)
            {
                pageSettingsList.push(this.settings.singleRTPPage);
            }
            else
            {
                pageSettingsList.push(this.settings.rangeRTPPage);
            }
            if (isNorskTipping)
            {
                pageSettingsList.push(this.settings.ntPage);
            }

            const pages: NTTextPage[] = [];
            for (const pageSettings of pageSettingsList)
            {
                if (!pageSettings) { continue; }
                const page = new this.ctor(
                {
                    ...overrideSettings,
                    ...pageSettings,
                }, runtimeData);
                pages.push(page);
            }

            return pages;
        }
    }

    export namespace LegalPagesFactory
    {
        export interface Settings extends RS.Slots.Paytable.Pages.Base.Settings
        {
            singleRTPPage: RS.Slots.Paytable.Pages.TextPage.Settings;
            rangeRTPPage: RS.Slots.Paytable.Pages.TextPage.Settings;
            ntPage: NTTextPage.Settings;
        }

        export const defaultSettings: Settings =
        {
            singleRTPPage: LegalTextPage.singleRTPSettings,
            rangeRTPPage: LegalTextPage.rangeRTPSettings,
            ntPage: NTLegalTextPage.Settings(false)
        }
    }
}