/// <reference path="../generated/Translations.ts" />
/// <reference path="NTTextPage.ts" />
namespace Slots.GDM.Paytable.Pages
{
    export namespace LegalTextPage
    {
        // We may well be able to set this to ${(new Date()).getFullYear()} but pending legal
        const year = "2019";

        const Settings = (rtpParagraph: RS.Slots.Paytable.Pages.TextPage.RtpTextSettings | RS.Slots.Paytable.Pages.TextPage.SingleRtpTextSettings): NTTextPage.Settings =>
        ({
            ...RS.Slots.Paytable.Pages.TextPage.defaultSettings,
            elements:
            [
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.MaxWinText,
                    text: Translations.Paytable.Disclaimer.MaxWin
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.Paytable.Disclaimer.Independent
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.Paytable.Disclaimer.Outcome
                },
                rtpParagraph,
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: Translations.Paytable.Disclaimer.Malfunction
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.VersionText,
                    text: Translations.Paytable.Disclaimer.Client
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.YearText,
                    text: Translations.Paytable.Disclaimer.Copyright,
                    ntDisplayMode: NTTextPage.NTDisplayMode.NotNT
                },
                {
                    kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                    text: `TM and ©${year} Scientific Games Corp. and its Subsidiaries. All rights reserved.`,
                    languageCodeBlackList: ["en"],
                    ntDisplayMode: NTTextPage.NTDisplayMode.NotNT
                }
            ],
            pageSettings:
            {
                ...RS.Slots.Paytable.Pages.TextPage.defaultPageSettings,
                forceYear: year
            }
        });

        export const singleRTPSettings: RS.Slots.Paytable.Pages.TextPage.Settings =
        {
            ...Settings(
            {
                kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.SingleRTPText,
                text: Translations.Paytable.Disclaimer.SingleRTP
            })
        };

        export const rangeRTPSettings: RS.Slots.Paytable.Pages.TextPage.Settings =
        {
            ...Settings(
            {
                kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.RTPText,
                text: Translations.Paytable.Disclaimer.RTP
            })
        };
    }
}