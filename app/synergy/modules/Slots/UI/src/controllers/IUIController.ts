namespace RS.Slots.UI
{
    /**
     * A type of option that the user can pick.
     */
    export enum InputOptionType
    {
        TotalBet,
        BetMultiplier,
        Lines,
        BetPerLine
    }

    /**
     * Responsible for interfacing the UI with the game logic.
     */
    export interface IUIController<TSettings extends IUIController.Settings = IUIController.Settings> extends RS.UI.IUIController<TSettings>
    {
        /** Published when the big bet feature has been requested by the user. */
        readonly onBigBetRequested: IEvent;

        /** Published when the autoplay feature has been requested by the user. */
        readonly onAutoplayRequested: IEvent;

        /** Published when the autoplay feature has been requested  to stop by the user. */
        readonly onAutoplayStopRequested: IEvent;

        /** Published when the Turbo feature has been requested by the user. */
        readonly onTurboRequested: IEvent;

        /** Published when the Turbo feature has been requested to stop by the user. */
        readonly onTurboStopRequested: IEvent;

        /** Published when the user has changed an input option. */
        readonly onInputOptionChanged: IEvent<IUIController.InputOptionChangedData>;

        /** Creates the UI with the specified settings. */
        create(settings: IUIController.InitSettings): void;

        /** Overrides the selected input value. */
        setSelectedInputValue(optionType: InputOptionType, value: number | string);

        /** Updates the user's input option selection. */
        setSelectedInputOption(optionType: InputOptionType, optionIndex: number): void;

        /** Updates the user's available input options. */
        setInputOptions(optionType: InputOptionType, options: number[]): void;

        /** Updates the line count to be displayed on the UI. */
        setLines(value: number): void;

        /** Updates the ways count to be displayed on the UI. */
        setWays(value: number): void;
    }

    export const IUIController = Controllers.declare<IUIController, IUIController.Settings>(Strategy.Type.Instance);

    export namespace IUIController
    {
        /**
         * Details about an option the user can pick.
         */
        export interface InputOption
        {
            type: InputOptionType;
            options: number[];
        }

        export interface InputOptionChangedData
        {
            optionType: InputOptionType;
            optionIndex: number;
            /** Whether or not the user (player) invoked this change. */
            userChanged?: boolean;
        }

        /**
         * Settings for the UI controller.
         */
        export interface Settings extends RS.UI.IUIController.Settings
        {
            allowAutoplayArbiter: IArbiter<boolean>;
            paytableEnabledArbiter: IArbiter<boolean>;
            allowQuickSpinArbiter: Arbiter<boolean>;
            autoplaysRemainingObservable: IReadonlyObservable<number>;
            muteArbiter: IArbiter<boolean>;
            showMeterPanelArbiter: IArbiter<boolean>;
            showMessageBarArbiter: IArbiter<boolean>;

            /** Set to true to always display the clock */
            alwaysShowClock?: boolean;
        }

        /**
         * Settings for initialising the UI controller.
         * Usually this is stuff not known ahead-of-time (e.g. available bet options).
         */
        export interface InitSettings extends RS.UI.IUIController.InitSettings
        {
            inputOptions: InputOption[];
            showBigBetButton?: boolean;
        }
    }
}
