namespace RS.Slots
{
    /**
     * Identity of a feature.
     */
    export interface FeatureDefinition
    {
        name: string;
    }

    /**
     * Marks the specified class as a feature to be used by the state machine.
     */
    export const Feature = Decorators.Tag.create<FeatureDefinition>(Decorators.TagKind.Class);

    export type FeatureConstructor = { new(context: Game.Context, data: object): IFeature; };

    /**
     * Instantiates a new feature given the specified context.
     */
    export function instantiateFeature(featureName: string, context: Game.Context, data: object): IFeature
    {
        const cls = Feature.classes.filter((c) => Feature.get(c).name === featureName);
        if (cls.length === 0) { return null; }
        if (cls.length > 1)
        {
            Log.warn(`Multiple feature classes lay claim to the feature '${featureName}'`);
        }
        const cl = cls[0] as any as FeatureConstructor;
        return new cl(context, data);
    }

    /**
     * Responsible for creation of features from model data.
     */
    export interface IFeatureFactory
    {
        /** Creates one or more features given the specified game context. */
        create(context: Game.Context): IFeature[] | null;
    }

    export const IFeatureFactory = Strategy.declare<IFeatureFactory>(Strategy.Type.Singleton);
}