namespace RS.Slots.GameState
{
    /**
     * TODO: remove this
     * @deprecated
     */
    export import MaxWinState = RS.GameState.MaxWinState;

    export const BigWin = "bigWin";
    export const PostFeatureBigWin = "postFeatureBigWin";
}

namespace RS.Slots
{
    // Import everything for backwards compatibility
    export namespace State
    {
        /**
         * TODO: remove this
         * @deprecated
         */
        export import Idle = RS.GameState.Idle;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Loading = RS.GameState.Loading;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import MaxWin = RS.GameState.MaxWin;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import PlayFinished = RS.GameState.PlayFinished;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Playing = RS.GameState.Playing;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Spinning = RS.GameState.Playing;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import PreSpin = RS.GameState.PrePlay;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import PrePlay = RS.GameState.PrePlay;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Resuming = RS.GameState.Resuming;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import WinRendering = RS.GameState.WinRendering;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import BigWin = GameState.BigWin;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import PostFeatureBigWin = GameState.PostFeatureBigWin;
    }
}
