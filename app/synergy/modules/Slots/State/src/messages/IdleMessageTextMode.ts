namespace RS.Slots
{
    export type IdleMessageTextMode = (context: Game.Context, totalBet: number) => RS.Localisation.LocalisableString;

    export namespace IdleMessageTextMode
    {
        /**
         * Returns the bet per line message.
         *
         * @param locale The locale
         * @param models The models
         */
        export function BetPerLine(context: Game.Context, totalBet: number): RS.Localisation.LocalisableString
        {
            return Localisation.bind(Translations.MessageTextMode.BetBreakdown,
            {
                lineCount: context.game.models.stake.paylineOptions[context.game.models.stake.currentPaylineIndex],
                betPerLine: context.game.currencyFormatter.format(Models.Stake.toPerLine(context.game.models.stake, { value: totalBet, type: Models.Stake.Type.Total }).value),
                totalBet: context.game.currencyFormatter.format(totalBet)
            });
        }
        /**
         * Returns the bet multiplier message.  Will round to 2 decimal places by default with non divisible numbers
         *
         * @param locale The locale
         * @param models The models
         */
        export function BetMultiplier(context: Game.Context, totalBet: number): RS.Localisation.LocalisableString
        {
            return Localisation.bind(Translations.MessageTextMode.BetMultiplier,
            {
                baseCreditValue: context.game.currencyFormatter.format(context.game.models.stake.baseCreditValue),
                betMultiplier: Models.Stake.toCreditMultiplier(context.game.models.stake, { value: totalBet, type: Models.Stake.Type.Total }).value,
                totalBet: context.game.currencyFormatter.format(totalBet)
            });
        }

        /**
         * TODO: remove this
         * @deprecated
         */
        export import TotalBet = RS.IdleMessageTextMode.TotalBet;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import GameName = RS.IdleMessageTextMode.GameName;

        /**
         * TODO: remove this
         * @deprecated
         */
        export import Empty = RS.IdleMessageTextMode.Empty;
    }
}
