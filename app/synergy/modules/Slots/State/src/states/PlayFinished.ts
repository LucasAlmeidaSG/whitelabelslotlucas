/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    /**
     * The current spin has finished and we're ready to close the game and move back to ReadyToSpin.
     */
    @HasCallbacks
    @RS.State.Name(State.PlayFinished)
    export class PlayFinishedState<T extends Game.Context = Game.Context> extends RS.GameState.PlayFinishedState<T>
    {
        @AutoDisposeOnSet protected _autoplayStopRequestedHandler: IDisposable;
        @AutoDisposeOnSet protected _canQuickSpin: IDisposable;
        @AutoDisposeOnSet protected _handleSkipRequested: IDisposable;

        public onEnter()
        {
            super.onEnter();

            const arbiters = this._context.game.arbiters;
            this._autoplayStopRequestedHandler = this._context.game.uiController.onAutoplayStopRequested(this.handleAutoplayStopRequested);

            if (!arbiters.shouldAutospin.value)
            {
                this._canQuickSpin = arbiters.canQuickSpin.declare(true); // Allow spin button to be used to skip + spin
                this._canSkip = arbiters.canSkip.declare(true); // Allow quickspin by allowing skip, though skip won't actually do anything in this state
                this._handleSkipRequested = this._context.game.uiController.onSkipRequested(this.handleSkip);
            }
        }

        @Callback
        protected handleSkip()
        {
            if (this._canSkip)
            {
                this._canSkip.dispose();
                this._canSkip = null;
            }
            this._canSkip = this._context.game.arbiters.canSkip.declare(false);
        }

        @Callback
        protected handleAutoplayStopRequested()
        {
            IPlatform.get().requestAutoplayStop();
        }

        /**
         * Checks if the game is open, and closes it if needed.
         */
        protected async checkCloseGame()
        {
            if (this._context.game.models.state.state !== Models.State.Type.Closed)
            {
                if (!this._context.playEnded)
                {
                    this._context.playEnded = true;
                    for (const feature of this._context.features)
                    {
                        const stateName = await feature.onEndPlay();
                        if (stateName != null)
                        {
                            this.transitionToState(stateName);
                            return;
                        }
                    }
                }
                if (!await this.doCloseGame()) { return; }
            }
            await this.checkRealityCheck();
            this.transitionToNextState(State.Idle);
        }

        /**
         *
         */
        protected async doCloseGame(response?: Engine.IResponse)
        {
            if (!response)
            {
                response = await this._context.game.engine.close();
            }

            // Was there an error?
            if (response.isError)
            {
                // If we get an error, stop autoplay
                IPlatform.get().requestAutoplayStop();

                this._canQuickSpin = null;
            }

            if (this._context.game.autoplayController.isAutoplaying && this._context.game.observables.autoplaysRemaining.value <= 0)
            {
                this._context.game.autoplayController.stop();
            }
            return await super.doCloseGame(response);
        }

        protected shouldReturnToIdle(): boolean
        {
            const models = this._context.game.models;
            if (models.state.isReplay)
            {
                if (models.freeSpins.isFreeSpins && models.freeSpins.remaining > 0 && !models.state.isMaxWin)
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        protected getResultsModel(models: Models): Models.SpinResults
        {
            return models.spinResults;
        }
    }
}
