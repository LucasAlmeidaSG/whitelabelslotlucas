/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    @RS.State.Name(State.Loading)
    export class LoadingState<T extends Game.Context = Game.Context> extends RS.GameState.LoadingState<T>
    {
        protected initObservables()
        {
            super.initObservables();

            const { models, observables } = this._context.game;

            // Update stake - override taken care of in super call
            if (models.stake.override == null)
            {
                const totalBet = Models.Stake.getCurrentBetAmount(models.stake, Models.Stake.Type.Total);
                observables.totalBet.value = totalBet;
            }

            // Update jackpots
            for (const jackpotName in models.jackpots)
            {
                const jackpotValue = models.jackpots[jackpotName].value;
                observables.jackpots.set(jackpotName, jackpotValue);
            }
        }

        /**
         * Handles post-load logic, after the loading screen has been closed and we're ready to move on.
         * Async.
         */
        protected async postload()
        {
            // Finish timer
            this.finishLoadTimer();

            const game = this._context.game;
            const { models, observables } = game;

            // TODO - remove in 1.2, now created in init()
            // backwards compatibility for games that copy entire loading functions
            if (!game.currencyFormatter)
            {
                game.currencyFormatter = new Localisation.CurrencyFormatter(
                    models.customer.currencySettings ||
                    Localisation.currencyMap[models.customer.currencyCode] ||
                    Localisation.defaultCurrencySettings
                );
            }

            // Initialise UI
            const lineCount = models.stake.paylineOptions[models.stake.currentPaylineIndex];
            this._turboModeHandle = game.arbiters.showTurboUI.declare(models.config.turboModeEnabled);
            game.uiController.create({
                inputOptions:
                [
                    { type: UI.InputOptionType.TotalBet, options: models.stake.betOptions.map((o) => RS.Slots.Models.Stake.toTotal(models.stake, o).value) }
                ],
                locale: game.locale,
                currencyFormatter: game.currencyFormatter,
                showBigBetButton: models.config.bigBetEnabled
            });

            // Initialise big win
            if (game.bigWinController)
            {
                game.bigWinController.initialise({
                    musicVolumeArbiter: game.arbiters.musicVolume,
                    bigWinVolumeArbiter: game.arbiters.bigWinVolume,
                    assetController: game.assetController,
                    viewController: game.viewController,
                    locale: game.locale,
                    currencyFormatter: game.currencyFormatter
                });
            }

            // Initialise autoplay
            game.autoplayController.locale = game.locale;

            game.uiController.setBalance(observables.balance.value);
            game.uiController.setWin(observables.win.value, true);

            game.uiController.setLines(lineCount);
            game.uiController.setSelectedInputOption(UI.InputOptionType.Lines, models.stake.currentPaylineIndex);

            this.setStakes();

            game.uiController.setReplayMode(models.state.isReplay);

            game.initialiseUI();

            // Handle restore
            if (await this.doRestore()) { return; }

            // Not restoring, default behaviour
            await this.openPrimaryView();

            // Go to idle state
            this.transitionToNextState(State.Idle);
        }

        protected setStakes()
        {
            const models = this._context.game.models;
            const override = models.stake.override;
            if (override != null)
            {
                this._context.game.uiController.setTotalBet(override.totalBet);

                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.BetPerLine, models.stake.currentBetIndex);
                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.TotalBet, models.stake.currentBetIndex);
                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.BetMultiplier, models.stake.currentBetIndex);

                this._context.game.uiController.setSelectedInputValue(UI.InputOptionType.BetPerLine, override.betPerLine);
                this._context.game.uiController.setSelectedInputValue(UI.InputOptionType.TotalBet, override.totalBet);

                if (models.stake.baseCreditValue != null)
                {
                    const betMultiplier = Models.Stake.toCreditMultiplier(models.stake, { value: override.totalBet, type: Models.Stake.Type.Total }).value;
                    this._context.game.uiController.setSelectedInputValue(UI.InputOptionType.BetMultiplier, betMultiplier);
                }
            }
            else
            {
                const totalBet = Models.Stake.getCurrentBetAmount(models.stake, Models.Stake.Type.Total);
                this._context.game.uiController.setTotalBet(totalBet);

                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.BetPerLine, models.stake.currentBetIndex);
                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.TotalBet, models.stake.currentBetIndex);
                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.BetMultiplier, models.stake.currentBetIndex);
            }
        }

        protected awaitClickToContinue(view: RS.GameState.LoadingState.IClickToContinueView)
        {
            const { models, observables } =  this._context.game;
            const resuming = models.state.state === Models.State.Type.Open;
            observables.platformGameState.value = resuming ? PlatformGameState.Resuming : PlatformGameState.Ready;
            return super.awaitClickToContinue(view);
        }

        /**
         * Switches to the correct view and state for restoring.
         * Return false if no restoring is in progress.
         * Async.
         */
        protected async doRestore(): Promise<boolean>
        {
            // Instantiate features
            let featureFactory: IFeatureFactory | null = null;
            try
            {
                featureFactory = IFeatureFactory.get();
            }
            catch (err)
            {
                this._context.features = [];
            }
            if (featureFactory != null)
            {
                this._context.features = featureFactory.create(this._context);
            }

            this._context.isResuming = false;

            // Check open state
            if (this._context.game.models.state.state !== Models.State.Type.Closed)
            {
                this.transitionToState(State.Resuming);
                return true;
            }
            else
            {
                // No open state, but let's give any features a chance to resume anyway
                let nextState: string | null = null;
                for (const feature of this._context.features)
                {
                    nextState = nextState || (await feature.onResume());
                }
                if (nextState)
                {
                    this.transitionToState(nextState);
                    return true;
                }
            }
            return false;
        }

        /**
         * Attempts to reduce the starting balance on a local build to the given value by forcing losing spins
         * Errors out if target balance is greater than starting balance, or if no losing force is given in the game settings
         * @param targetBalance - value to aim for
         */
        protected async tweakBalance(targetBalance: number)
        {
            const models = this._context.game.models;
            const initBalance = models.customer.finalBalance.primary;
            if (targetBalance >= initBalance)
            {
                RS.Log.warn("Target balance higher than init balance. Not tweaking");
                return;
            }

            const force = this._context.game.settings.loseForce;
            if (!force)
            {
                RS.Log.warn("Tried to tweak balance without loseForce setting in game settings.");
                return;
            }

            //get bets to place
            let toSubtract = initBalance - targetBalance;
            const bets: RS.Slots.Models.StakeAmount[] = [];
            for (let i = models.stake.betOptions.length - 1; i >= 0; i--)
            {
                const bet = models.stake.betOptions[i];

                const mod = RS.Math.floor(toSubtract / bet.value);
                for (let times = 0; times < mod; times++) { bets.push(bet); }

                toSubtract -= (mod * bet.value);
            }

            for (const bet of bets)
            {
                this._context.game.engine.force(force);
                const spin = await this._context.game.engine.spin({
                    betAmount: bet,
                    paylineCount: models.stake.paylineOptions[models.stake.currentPaylineIndex]
                });

                // Was there an error?
                if (spin.isError)
                {
                    await this._context.game.handleError();
                    break;
                }

                const end = await this._context.game.engine.close();
                // Was there an error?
                if (end.isError)
                {
                    await this._context.game.handleError();
                    break;
                }

                //check if actually decreasing balance
                if (models.customer.finalBalance.primary >= initBalance)
                {
                    RS.Log.warn("Balance didn't go down after first spin, stopping tweak");
                    break;
                }
            }

            //reset back to default bet index
            models.stake.currentBetIndex = models.stake.defaultBetIndex;
        }

        protected getResultsModel(models: Models): Models.SpinResults
        {
            return models.spinResults;
        }
    }
}
