/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    /**
     * If required, performs big win animation.
     */
    @HasCallbacks
    @RS.State.Name(State.BigWin)
    export class BigWinState<T extends Game.Context = Game.Context> extends RS.State.Base<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _handleSkipRequested: IDisposable;
        @AutoDisposeOnSet protected _turboModeHandle: IDisposable;

        public onEnter()
        {
            super.onEnter();

            const { arbiters, models } = this._context.game;

            this._canSpin = arbiters.canSpin.declare(false); // We can't spin

            // Show turbo toggle?
            if (models.config.turboModeEnabled)
            {
                this._turboModeHandle = arbiters.showTurboUI.declare(true);
            }

            this._context.isResuming = false;

            this.doBigWin();
        }

        @Callback
        protected handleSkipRequested(): void
        {
            if (!this._context.game.arbiters.canSkip.value) { return; }

            this._handleSkipRequested.dispose();
            this._handleSkipRequested = null;

            if (this._canSkip)
            {
                this._canSkip.dispose();
                this._canSkip = null;
            }

            this._context.game.bigWinController.skip();
        }

        protected async doBigWin()
        {
            // Do we have a big win controller?
            const bigWinController = this._context.game.bigWinController;
            if (bigWinController)
            {
                const models = this._context.game.models;
                const totalStake = Models.Stake.toTotal(models.stake, Models.Stake.getCurrentBet(models.stake)).value;
                const totalPayout = this.evaluateTotalWin();

                // Do we need to animate?
                if (this.shouldBigWin(totalStake, totalPayout))
                {
                    // Only enable skip if there is a big win.
                    const arbiters = this._context.game.arbiters;
                    this._canSkip = arbiters.canSkip.declare(true); // We can skip
                    this._spinOrSkip = arbiters.spinOrSkip.declare("skip"); // We should now be set to "skip"
                    this._handleSkipRequested = this._context.game.uiController.onSkipRequested(this.handleSkipRequested);

                    // Hide UI
                    const showButtonArea = this._context.game.arbiters.showButtonArea.declare(false);
                    const showMessageBar = this._context.game.arbiters.showMessageBar.declare(false);
                    const showMeterPanel = this._context.game.arbiters.showMeterPanel.declare(false);

                    // Animate
                    await bigWinController.do(totalStake, totalPayout);
                    showButtonArea.dispose();
                    showMessageBar.dispose();
                    showMeterPanel.dispose();
                }
            }

            // Move on
            this.transitionToNextState(State.PlayFinished);
        }

        protected shouldBigWin(totalStake: number, totalPayout: number): boolean
        {
            return this._context.game.bigWinController.isBigWin(totalStake, totalPayout);
        }

        protected evaluateTotalWin(): number
        {
            return this._context.game.models.spinResults.map((sr) => sr.win.totalWin).reduce((a, b) => a + b);
        }
    }
}
