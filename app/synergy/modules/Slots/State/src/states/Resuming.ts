/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    /**
     * The game has been resumed and is waiting for user input to progress.
     */
    @RS.State.Name(State.Resuming)
    export class ResumingState<T extends Game.Context = Game.Context> extends RS.GameState.ResumingState<T>
    {
        @AutoDisposeOnSet protected _canSpin: IDisposable;
        @AutoDisposeOnSet protected _canSkip: IDisposable;
        @AutoDisposeOnSet protected _spinOrSkip: IDisposable;
        @AutoDisposeOnSet protected _musicStateHandle: IDisposable;

        protected _nextState: string | null;

        @AutoDisposeOnSet protected _autoplayStopRequestedHandler: IDisposable;

        public async onEnter()
        {
            const { arbiters, models, observables } = this._context.game;

            this._autoplayStopRequestedHandler = this._context.game.uiController.onAutoplayStopRequested(this.handleAutoplayStopRequested);

            // Restore free spins remaining
            observables.freeSpinsRemaining.value = models.freeSpins.remaining - models.freeSpins.extraSpinsAwarded;

            // Handle resume via features
            this._nextState = null;
            for (const feature of this._context.features)
            {
                this._nextState = this._nextState || (await feature.onResume());
            }
            if (this._nextState)
            {
                this.transitionToState(this._nextState);
                return;
            }

            super.onEnter();
        }

        protected updateWin()
        {
            const { models, observables } = this._context.game;
            const finalWin = models.spinResults[models.spinResults.length - 1].win.accumulatedWin;
            const winThisPlay = models.spinResults.map((s) => s.win.totalWin).reduce((a, b) => a + b, 0);
            observables.win.value = finalWin - winThisPlay;
        }

        @Callback
        protected handleAutoplayStopRequested()
        {
            IPlatform.get().requestAutoplayStop();
        }

        /**
         * Called when the user has clicked OK on the unfinished game dialog.
         */
        protected async onDialogAccepted(): Promise<void>
        {
            // Tell platform we're now in play
            this._context.game.observables.platformGameState.value = PlatformGameState.InPlay;

            for (const feature of this._context.features)
            {
                this._nextState = this._nextState || (await feature.onResumeDialogClosed());
            }

            // Start the reels spinning
            const models = this._context.game.models;
            if (this._context.primaryReels && models.state.state !== Models.State.Type.Closed)
            {
                switch (this._context.primaryReels.state.value)
                {
                    case Reels.IReel.State.Stopped:
                        this._context.primaryReels.startReels(Reels.IReel.StartType.Immediate);
                        break;
                }
            }

            // Go to next state
            if (this._nextState != null)
            {
                this.transitionToState(this._nextState);
            }
            else
            {
                super.onDialogAccepted();
            }
        }

        protected getResultsModel(models: Models): Models.SpinResults
        {
            return models.spinResults;
        }
    }
}
