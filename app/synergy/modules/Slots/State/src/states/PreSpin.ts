/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    /**
     * A play has been requested but no response received yet.
     * The reels should start spinning up.
     */
    @HasCallbacks
    @RS.State.Name(State.PrePlay)
    export class PreSpinState<T extends Game.Context = Game.Context> extends RS.GameState.PrePlayState<T>
    {
        @AutoDispose protected _autoplayStopRequestedHandler: IDisposable;

        public async onEnter()
        {
            if (this._context.game.observables.platformGameState.value === PlatformGameState.Ready)
            {
                // First spin, clear win meter.
                this._context.game.observables.win.value = 0;
            }

            // Reset features
            this._context.features = [];
            this._context.playEnded = false;

            if (this._context.game.autoplayController.isAutoplaying)
            {
                this._context.game.autoplayController.consume();
            }
            this._autoplayStopRequestedHandler = this._context.game.uiController.onAutoplayStopRequested(this.handleAutoplayStopRequested);

            await this.startReels();

            super.onEnter();
        }

        /**
         * TODO: remove this
         * @deprecated
         */
        protected async sendSpinRequest()
        {
            return await this.sendLogicRequest();
        }

        protected async sendLogicRequest()
        {
            this.doAnalytics();

            const response = await this._context.game.engine.spin(this.getEngineParams());

            // Was there an error?
            if (response.isError)
            {
                this.handleEngineError();

                return;
            }

            this.updateUI();

            this.onLogicResponseReceived(response);
        }

        protected getEngineParams()
        {
            const models = this._context.game.models;
            const betAmount = this.getBetAmount();
            return {
                betAmount: betAmount,
                paylineCount: models.stake.paylineOptions[models.stake.currentPaylineIndex]
            }
        }

        protected getBetAmount(): Models.StakeAmount
        {
            const models = this._context.game.models;
            const override = models.stake.override;
            return override != null ? { type: Models.Stake.Type.Total, value: override.totalBet } : Models.Stake.getCurrentBet(models.stake);
        }

        protected updateUI(): void
        {
            // Update total bet observable with newly parsed total bet for edgecases where total bet may have been edited externally, eg. via Charles
            const models = this._context.game.models;
            if (models.stake.override != null)
            {
                this._context.game.uiController.setTotalBet(models.stake.override.totalBet);
                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.TotalBet, models.stake.currentBetIndex);
                this._context.game.uiController.setSelectedInputValue(UI.InputOptionType.TotalBet, models.stake.override.totalBet);
                this._context.game.observables.totalBet.value = models.stake.override.totalBet;
            }
            else
            {
                const currentBetOption = models.stake.betOptions[models.stake.currentBetIndex];
                this._context.game.uiController.setTotalBet(Models.Stake.toTotal(models.stake, currentBetOption).value);
                this._context.game.uiController.setSelectedInputOption(UI.InputOptionType.TotalBet, models.stake.currentBetIndex);
                this._context.game.observables.totalBet.value = RS.Slots.Models.Stake.toTotal(models.stake, currentBetOption).value;
            }
        }

        protected async handleEngineError()
        {
            // If we get an error, stop autoplay
            IPlatform.get().requestAutoplayStop();

            // Reset the spin, don't show any suspense
            const models = this._context.game.models;
            this._context.primaryReels.stopReels(Reels.IReel.StopType.Immediate, true);
            this._context.primaryReels.setReels(models.config.defaultReels.symbolsInView);

            await super.handleEngineError();
        }

        @Callback
        protected handleAutoplayStopRequested()
        {
            IPlatform.get().requestAutoplayStop();
        }

        protected updateBalance()
        {
            const models = this._context.game.models;
            if (!models.freeSpins.isFreeSpins)
            {
                const baseBalance = models.spinResults[0].balanceBefore;
                this._context.game.observables.balance.value = baseBalance.primary;
                this._context.game.observables.balances.value = baseBalance.named;
            }
        }

        protected onLogicResponseReceived(response: Engine.IResponse): void
        {
            this.updateBalance();

            const models = this._context.game.models;
            // Instantiate features
            let featureFactory: IFeatureFactory | null = null;
            try
            {
                featureFactory = IFeatureFactory.get();
            }
            catch (err)
            {
                this._context.features = [];
            }
            if (featureFactory != null)
            {
                this._context.features = featureFactory.create(this._context);
            }

            if (models.freeSpins.isFreeSpins)
            {
                this._context.game.observables.freeSpinsRemaining.value = models.freeSpins.remaining - models.freeSpins.extraSpinsAwarded;
            }

            this.transitionToNextState(State.Playing);
        }

        protected getResultsModel(models: Models): Models.SpinResults
        {
            return models.spinResults;
        }

        /**
         * Start the reels spinning
         */
        protected async startReels(): Promise<void>
        {
            const reelsState = this._context.primaryReels.state.value;

            if (reelsState === Reels.IReel.State.Stopped)
            {
                this._context.primaryReels.startReels(Reels.IReel.StartType.Standard);
            }
        }
    }
}
