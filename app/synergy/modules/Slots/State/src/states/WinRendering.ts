/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    export enum WinRenderingMode
    {
        Sequential,
        Parallel
    }

    export enum WinRenderingSkipMode
    {
        AlwaysEnabled, // Always enable skipping winrendering
        OnEnableSkipPublished // Enable winrendering skip once published
    }

    export let winRenderingSkipMode: WinRenderingSkipMode = WinRenderingSkipMode.AlwaysEnabled;

    /**
     * It's time to render any wins.
     * It could be that there are no wins to render, in which case we move on immediately.
     */
    @HasCallbacks
    @RS.State.Name(State.WinRendering)
    export class WinRenderingState<T extends Game.Context = Game.Context> extends RS.GameState.WinRenderingState<T>
    {
        @AutoDisposeOnSet protected _canQuickSpin: IDisposable;
        @AutoDisposeOnSet protected _skipRequestedHandlerReels: IDisposable;
        @AutoDisposeOnSet protected _autoplayStopRequestedHandler: IDisposable;
        @AutoDisposeOnSet protected readonly _enableSkipListeners: IDisposable[] = [];

        protected _winRenderer: WinRenderers.Base;
        protected _winRendererTask: Task<void>;
        protected _workDone: boolean;

        public async onEnter()
        {
            if (this.shouldQuickSpin())
            {
                this._canQuickSpin = this._context.game.arbiters.canQuickSpin.declare(true); // Allow spin button to be used to skip + spin
            }

            this._skipRequestedHandlerReels = this._context.primaryReels.onSkipRequested(this.handleSkipRequested);

            this._autoplayStopRequestedHandler = this._context.game.uiController.onAutoplayStopRequested(this.handleAutoplayStopRequested);

            await super.onEnter();
        }

        public onExit()
        {
            super.onExit();

            // Error check
            if (this._winRendererTask)
            {
                RS.Log.warn("Exiting win rendering state but win rendering is still in progress!");
            }

            if (winRenderingSkipMode === WinRenderingSkipMode.AlwaysEnabled)
            {
                RS.Log.warn("WinRendering: WinRenderingSkipMode is AlwaysEnabled - this is deprecated as it is a GLI requirement to show the first winline before enabling skip.");
            }
        }

        protected declareCanSkipHandler()
        {
            const arbiters = this._context.game.arbiters;
            if (winRenderingSkipMode === WinRenderingSkipMode.AlwaysEnabled)
            {
                this._canSkip = arbiters.canSkip.declare(true); //We can skip at any point
            }
            else if (winRenderingSkipMode === WinRenderingSkipMode.OnEnableSkipPublished)
            {
                this._canSkip = arbiters.canSkip.declare(false); // We can't skip until the first winline is rendered
            }
        }

        /**
         * Gets if we should be able to quick spin.
         */
        protected shouldQuickSpin(): boolean
        {
            // If there are any features, then no
            if (this._context.features && this._context.features.length > 0) { return false; }

            const models = this._context.game.models;
            const totalStake = Models.Stake.toTotal(models.stake, Models.Stake.getCurrentBet(models.stake)).value;
            const totalPayout = models.spinResults.map((sr) => sr.win.totalWin).reduce((a, b) => a + b);

            // If there will be a big win, then no
            const bigWinController = this._context.game.bigWinController;
            if (bigWinController && bigWinController.isBigWin(totalStake, totalPayout)) { return false; }

            // If we're autoplaying, then no
            if (this._context.game.arbiters.shouldAutospin.value) { return false; }

            // Yes
            return true;
        }

        protected async renderWins()
        {
            await super.renderWins();
            this._workDone = await this.checkWinlines();
        }

        protected async onComplete()
        {
            await this.onWinlinesRendered(!this._workDone);
        }

        protected getWinRenderingMode(): WinRenderingMode
        {
            const { arbiters } = this._context.game;
            return (arbiters.shouldAutospin.value || arbiters.turboMode.value) ? WinRenderingMode.Parallel : WinRenderingMode.Sequential;
        }

        /**
         * Checks if there are any winlines to process, and renders them if so.
         * Async.
         */
        protected async checkWinlines()
        {
            this._skipped = false;

            // Run features
            for (const feature of this._context.features)
            {
                await feature.onPreWinRendering();
            }

            // Check if we have an assigned win renderer
            if (!this._context.winRenderer) { return false; }

            // Get winlines
            const modelStore = this._context.game.models;
            const spinResult = modelStore.spinResults[this._context.spinIndex];
            const winlines = spinResult.win.paylineWins;
            if (winlines == null || winlines.length === 0)
            {
                // Nothing to render
                return false;
            }

            if (Is.array(this._context.winRenderer))
            {
                for (const winrenderer of this._context.winRenderer)
                {
                    if (!this._skipped)
                    {
                        await this.singleWinRenderingTask(winlines, winrenderer);
                    }
                }
            }
            else
            {
                await this.singleWinRenderingTask(winlines, this._context.winRenderer);
            }

            // Success
            this._context.shouldCycle = true;
            return true;
        }

        /**
         * Renders the winlines for one renderer.
         * Async.
         */
        protected async singleWinRenderingTask(winlines: Models.PaylineWin[], winrenderer: WinRenderers.Base)
        {
            if (winRenderingSkipMode === WinRenderingSkipMode.OnEnableSkipPublished)
            {
                this._enableSkipListeners.push(winrenderer.onSkipEnabled(this.onSkipEnabled));
            }
            // Check which type we want
            const stagger: WinRenderers.StaggerTime = winrenderer.settings.forceStagger != null
                ? winrenderer.settings.forceStagger
                : (this.getWinRenderingMode() === WinRenderingMode.Parallel
                    ? WinRenderers.StaggerTime.Parallel
                    : WinRenderers.StaggerTime.Sequential);

            this.startWinRendering(winlines, winrenderer, stagger);

            // Wait for the task to complete
            await this._winRendererTask;
        }

        /**
         * Called when all winlines are rendered.
         */
        protected async onWinlinesRendered(noActivity: boolean = false)
        {
            for (const feature of this._context.features)
            {
                const stateName = await feature.onEndWinRendering();
                if (stateName != null)
                {
                    this.transitionToState(stateName);
                    return;
                }
            }

            // Switch back to play finished... after a short delay
            if (noActivity)
            {
                this.transitionToNextState(State.BigWin);
            }
            else
            {
                RS.Tween.wait(500)
                    .call((t) => this.transitionToNextState(State.BigWin));
            }
        }

        @Callback
        protected handleAutoplayStopRequested()
        {
            IPlatform.get().requestAutoplayStop();
        }

        /**
         * Starts rendering a win.
         */
        protected startWinRendering(winlines: Models.PaylineWin[], winRenderer: WinRenderers.Base, forceStagger?: WinRenderers.StaggerTime)
        {
            this._skipped = false;
            this._winRenderer = winRenderer;

            const totalEstimatedTime = winRenderer.estimateWinsRenderTime(winlines, forceStagger);
            // Log.debug(`Estimated win render time: ${totalEstimatedTime}`);
            this._winRendererTask = winRenderer.renderWins(winlines, forceStagger, totalEstimatedTime);
            this._winRendererTask.then(() =>
            {
                this._winRenderer = null;
                this._winRendererTask = null;
            });
            this._winRendererTask.start();
        }

        /**
         * Skips win rendering.
         * The done callback passed to startWinRender will be called asap.
         */
        protected stopWinRendering(): void
        {
            // Sanity check
            if (this._skipped) { return; }

            super.stopWinRendering();

            // Tell the win renderers to skip
            if (this._winRendererTask)
            {
                this._winRendererTask.cancel();
            }
        }

        /**
         * Allows winrendering to be skipped by user input
         */
        @Callback
        protected onSkipEnabled()
        {
            this._canSkip = this._context.game.arbiters.canSkip.declare(true);
        }
    }
}
