/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{
    /**
     * A play has been requested and a response received.
     * Any in-reel features (that occur during a spin) should be handled here.
     * Then it should spin down the reels.
     */
    @HasCallbacks
    @RS.State.Name(State.Playing)
    export class SpinningState<T extends Game.Context = Game.Context> extends RS.GameState.PlayingState<T>
    {
        @AutoDisposeOnSet protected _skipRequestedHandlerReels: IDisposable;

        @AutoDisposeOnSet protected _reelsStateChangedListener: IDisposable;
        @AutoDisposeOnSet protected _reelStoppedListener: IDisposable;

        @AutoDisposeOnSet protected _autoplayStopRequestedHandler: IDisposable;

        public onEnter()
        {
            this._skipRequestedHandlerReels = this._context.primaryReels.onSkipRequested(this.handleSkipRequested);

            this._autoplayStopRequestedHandler = this._context.game.uiController.onAutoplayStopRequested(this.handleAutoplayStopRequested);

            // Check if we even have reels...
            if (!this._context.primaryReels) { return; }

            // Ensure the reels are spinning
            if (this._context.primaryReels.state.value === Reels.IReel.State.Stopped)
            {
                this._context.primaryReels.startReels(Reels.IReel.StartType.Immediate);
            }

            // If the reels are already spinning, handle that
            if (this._context.primaryReels.state.value === Reels.IReel.State.Spinning)
            {
                this.handleSpinning();
            }

            // Handle when the reels change to spinning or stopped state
            this._reelsStateChangedListener = this._context.primaryReels.state.onChanged((newState) =>
            {
                switch (newState)
                {
                    case Reels.IReel.State.Spinning:
                        this.handleSpinning();
                        break;
                    case Reels.IReel.State.Stopped:
                        this.handleStopped();
                        break;
                }
            });

            super.onEnter();
        }

        protected updateBalance()
        {
            const models = this._context.game.models;
            if (!models.freeSpins.isFreeSpins)
            {
                const spinResult = models.spinResults[this._context.spinIndex];
                this._context.game.observables.balance.value = spinResult.balanceBefore.primary;
                this._context.game.observables.balances.value = spinResult.balanceBefore.named;
            }
        }

        /**
         * Updates the reels state with the current values from the model.
         */
        protected updateReelState(): void
        {
            if (!this._context.primaryReels) { return; }

            const models = this._context.game.models;
            const spinResult = models.spinResults[this._context.spinIndex];
            if (models.config.reelSets != null)
            {
                this._context.primaryReels.setReelSet(models.config.reelSets[spinResult.reels.currentReelSetIndex]);
            }
            this._context.primaryReels.setReels(spinResult.reels.symbolsInView, spinResult.reels.stopIndices);

            for (const feature of this._context.features)
            {
                feature.adjustReels(this._context.primaryReels);
            }
        }

        @Callback
        protected handleAutoplayStopRequested()
        {
            IPlatform.get().requestAutoplayStop();
        }

        /**
         * Handles a skip request.
         */
        protected handleSkip(): void
        {
            super.handleSkip();
            this._context.primaryReels.stopReels(Reels.IReel.StopType.Quick, true);
        }

        protected async handleSpinning()
        {
            // Update stop positions
            this.updateReelState();
            for (const feature of this._context.features)
            {
                const stateName = await feature.onMidSpin();
                if (stateName != null)
                {
                    this.transitionToState(stateName);
                    return;
                }
            }
            if (!this.checkSpinningInReelFeature())
            {
                this.beginSpinDown();
            }
        }

        /**
         * Checks if autoplay should be stopped by the current features.
         */
        protected shouldStopAutoplay(): boolean
        {
            for (const feature of this._context.features)
            {
                // stop autoplay if at least one feature stops autoplay
                if (!feature.isNotStoppingAutoplay)
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * Checks if turbo mode should be stopped by the current features.
         */
        protected shouldStopTurboMode(): boolean
        {
            for (const feature of this._context.features)
            {
                // Stop turbo mode if at least one feature stops turbo mode
                if (feature.stopsTurboMode)
                {
                    return true;
                }
            }
            return false;
        }

        protected async handleStopped()
        {
            if (this.shouldStopAutoplay())
            {
                IPlatform.get().requestAutoplayStop();
            }
            if (this.shouldStopTurboMode())
            {
                this._context.game.resetTurboMode();
            }

            for (const feature of this._context.features)
            {
                const stateName = await feature.onEndSpin();
                if (stateName != null)
                {
                    this.transitionToState(stateName);
                    return;
                }
            }
            if (!this.checkStoppedInReelFeature())
            {
                this.finishInReelFeatures();
            }
        }

        /**
         * Checks if an in-reel feature should be processed while reels are spinning.
         * When handled, call this.beginSpinDown() and return true.
         */
        protected checkSpinningInReelFeature(): boolean
        {
            return false;
        }

        /**
         * Checks if an in-reel feature should be processed when reels have stopped.
         * When handled, call this.finishInReelFeatures() and return true.
         */
        protected checkStoppedInReelFeature(): boolean
        {
            return false;
        }

        /**
         * Begins spinning down.
         */
        protected beginSpinDown(): void
        {
            const stopType = this._context.game.arbiters.turboMode.value ? Reels.IReel.StopType.Turbo : Reels.IReel.StopType.Standard;
            this._context.primaryReels.stopReels(stopType, false);
        }

        /**
         * Called when all in-real features have finished and it's time to move to next state.
         */
        protected finishInReelFeatures(): void
        {
            // Go to win rendering state
            this.transitionToNextState(State.WinRendering);
        }

        protected getResultsModel(models: Models): Models.SpinResults
        {
            return models.spinResults;
        }
    }
}
