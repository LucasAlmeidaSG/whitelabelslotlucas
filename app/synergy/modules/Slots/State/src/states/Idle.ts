/// <reference path="../States.ts" />

namespace RS.Slots.GameState
{

    /**
     * TODO: remove this
     * @deprecated
     */
    export import idleMessageTextMode = RS.GameState.idleMessageTextMode;

    /**
     * The "idle" state.
     * The game is closed and is ready to spin.
     * Could happen in between free spins so we might not always want the player to be able to click spin.
     */
    @HasCallbacks
    @RS.State.Name(State.Idle)
    export class IdleState<T extends Game.Context = Game.Context> extends RS.GameState.IdleState<T>
    {
        @AutoDisposeOnSet protected _inputChangedHandle: IDisposable;
        @AutoDisposeOnSet protected _autoplayRequestedHandler: IDisposable;
        @AutoDisposeOnSet protected _autoplayStopRequestedHandler: IDisposable;
        @AutoDisposeOnSet protected _replaySpinBlock: IDisposable;
        @AutoDisposeOnSet protected _autoSpinHandle: IDisposable;
        @AutoDisposeOnSet protected _shouldAutospinHandler: IDisposable;

        protected _tryingToSpin: boolean;
        protected _cyclingWinlines: boolean;

        protected _skipped: boolean = false;
        protected _winRenderer: WinRenderers.Base;
        protected _winRendererTask: Task<void>;
        protected _autospinned: boolean;

        public onEnter()
        {
            this._cyclingWinlines = false;
            this._tryingToSpin = false;
            this._autospinned = false;

            this._inputChangedHandle = this._context.game.uiController.onInputOptionChanged(this.handleInputOptionChanged);
            this._autoplayRequestedHandler = this._context.game.uiController.onAutoplayRequested(this.handleAutoplayRequested);
            this._autoplayStopRequestedHandler = this._context.game.uiController.onAutoplayStopRequested(this.handleAutoplayStopRequested);

            super.onEnter();
        }

        public onExit()
        {
            super.onExit();

            // Error check
            if (this._winRendererTask)
            {
                RS.Log.warn("Exiting idle state but win rendering is still in progress!");
            }
        }

        protected updateObservables()
        {
            const models = this._context.game.models;
            if (!models.freeSpins.isFreeSpins || models.freeSpins.remaining === 0 || models.state.isMaxWin)
            {
                super.updateObservables();
            }

            this._inputChangedHandle = this._context.game.uiController.onInputOptionChanged(this.handleInputOptionChanged);
        }

        protected stayIdle()
        {
            // Cycle winlines
            this._tryingToSpin = false;
            this._cyclingWinlines = this._context.shouldCycle;
            this.checkCycleWinlines();

            super.stayIdle();

            this.handleAutospinArbiterUpdated();

            this._shouldAutospinHandler = this._context.game.arbiters.shouldAutospin.onChanged(this.handleAutospinArbiterUpdated);
            this._canSpinHandler = this._context.game.arbiters.canSpin.onChanged(this.handleAutospinArbiterUpdated);
        }

        @Callback
        protected handleInputOptionChanged(ev: UI.IUIController.InputOptionChangedData): void
        {
            // Remove old bet breakdown message
            if (this._statusTextHandle)
            {
                this._statusTextHandle = null;
            }

            // Display bet breakdown message
            const messageText = this.getBetBreakdown();
            if (messageText != null)
            {
                this._statusTextHandle = this._context.game.arbiters.messageText.declare(messageText);
            }

            // If we're cycling wins in idle, don't keep showing them with a new stake
            this.stopWinRendering();
        }

        /**
         * Gets the bet breakdown message to show.
         * Will return null to show no bet breakdown.
         */
        protected getBetBreakdown(): RS.Localisation.LocalisableString | null
        {
            const models = this._context.game.models;

            // Don't display a bet breakdown in free spins
            if (models.freeSpins && models.freeSpins.isFreeSpins && models.freeSpins.remaining > 0) { return null; }

            if (RS.GameState.idleMessageTextMode == null)
            {
                RS.GameState.idleMessageTextMode = IdleMessageTextMode.BetPerLine;
            }

            if (models.stake.override != null && this._context.game.observables.totalBet.value === models.stake.override.totalBet)
            {
                return RS.GameState.idleMessageTextMode(this._context, models.stake.override.totalBet);
            }

            const totalBet: number = Models.Stake.toTotal(models.stake, models.stake.betOptions[models.stake.currentBetIndex]).value;

            return RS.GameState.idleMessageTextMode(this._context, totalBet);
        }

        /**
         * Called when either the shouldAutospin or the canSpin arbiters have updated.
         */
        @Callback
        protected handleAutospinArbiterUpdated(): void
        {
            const arbiters = this._context.game.arbiters;

            if (arbiters.shouldAutospin.value && arbiters.canSpin.value)
            {
                this._autospinned = true;

                this.stopWinRendering();
                this._canSpin = arbiters.canSpin.declare(false);
                this.autospinWaitTween();
            }
            else if (!arbiters.shouldAutospin.value && this._autospinWaitTween != null)
            {
                // If the player has cancelled autoplay while we're in the 1s timer, dispose it here to prevent another spin happening
                this._autospinWaitTween = null;
                this._canSpin = arbiters.canSpin.declare(true);
            }
        }

        /**
         * Checks if a game should be resumed.
         * If handled, call this.onWinlinesRendered() when done and return true.
         */
        protected checkCycleWinlines(): boolean
        {
            if (!this._cyclingWinlines) { return false; }
            this._cyclingWinlines = false;

            // Check if we should spin instead
            if (this._tryingToSpin)
            {
                this.handleSpinRequested();
                return false;
            }
            if (this._autospinned) { return false; }

            // Check if we have an assigned win renderer
            if (!this._context.cycledWinRenderer || !this._context.shouldCycle) { return false; }

            // Get winlines
            const spinResult = this._context.game.models.spinResults[this._context.spinIndex];
            if (spinResult == null || spinResult.win == null || spinResult.win.paylineWins == null || spinResult.win.paylineWins.length === 0)
            {
                // Nothing to render
                return false;
            }

            this.startWinRendering(spinResult.win.paylineWins, this._context.cycledWinRenderer);
            this._winRendererTask.then(() => this.checkCycleWinlines());

            this._cyclingWinlines = true;
            return true;
        }

        @Callback
        protected handleSpinRequested(): void
        {
            if (this._cyclingWinlines)
            {
                if (this._canSpin)
                {
                    this._canSpin = this._context.game.arbiters.canSpin.declare(false); // We can no longer spin
                }
                if (this._uiInteractable)
                {
                    this._uiInteractable = null;
                }

                this._tryingToSpin = true;
                this.stopWinRendering();
            }
            else
            {
                super.handleSpinRequested();
            }
        }

        @Callback
        protected handleAutoplayRequested()
        {
            IPlatform.get().requestAutoplay();
        }

        @Callback
        protected handleAutoplayStopRequested()
        {
            IPlatform.get().requestAutoplayStop();
        }

        /**
         * Starts rendering a win.
         */
        protected startWinRendering(winlines: Models.PaylineWin[], winRenderer: WinRenderers.Base, forceStagger?: WinRenderers.StaggerTime)
        {
            this._skipped = false;
            this._winRenderer = winRenderer;

            const totalEstimatedTime = winRenderer.estimateWinsRenderTime(winlines, forceStagger);
            // Log.debug(`Estimated win render time: ${totalEstimatedTime}`);
            this._winRendererTask = winRenderer.renderWins(winlines, forceStagger, totalEstimatedTime);
            this._winRendererTask.then(() =>
            {
                this._winRenderer = null;
                this._winRendererTask = null;
            });
            this._winRendererTask.start();
        }

        /**
         * Skips win rendering.
         * The done callback passed to startWinRender will be called asap.
         */
        protected stopWinRendering(): void
        {
            // Sanity check
            if (this._skipped) { return; }

            // Tell the win renderers to skip
            this._skipped = true;
            this._cyclingWinlines = false;
            if (this._winRendererTask)
            {
                this._winRendererTask.cancel();
            }
        }

        @Callback
        protected handleStopCycledWinRendering(): void
        {
            this.stopWinRendering();
        }

        protected checkAutoBet(): void
        {
            const arbiters = this._context.game.arbiters;

            this._spinOrSkip = arbiters.spinOrSkip.declare("spin");
            this._canSkip = arbiters.canSkip.declare(false);

            // Should we spin immediately?
            if (arbiters.shouldAutospin.value && arbiters.canSpin.value)
            {
                this.autospinWaitTween();
            }
            else
            {
                this.stayIdle();
            }
        }

        protected doReplay(): void
        {
            const { arbiters, models } = this._context.game;
            // If we're replaying, autospin
            if (models.state.isReplay)
            {
                if (models.state.isReplayComplete)
                {
                    this._replaySpinBlock = arbiters.canSpin.declare(false);
                }
                else
                {
                    this._autoSpinHandle = arbiters.shouldAutospin.declare(true);
                }
            }
        }

        protected getResultsModel(models: Models): Models.SpinResults
        {
            return models.spinResults;
        }
    }
}
