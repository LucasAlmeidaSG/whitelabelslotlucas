<?php

$username = 'red7demo';
$password = 'Red7game_';
$server = 'http://52.48.116.188/English/';

# Get all game request headers
if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        if (!is_array($_SERVER))
        {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

# Store cookie headers from game request
$request_headers = getallheaders();
$cookie_header_array = array();
foreach ($request_headers as $key => $value)
{
    if ($key == "Cookie")
    {
        $cookie_header_array[] = $key.": ".$value;
    }
}

# Convert response headers into an associative array
function get_headers_as_array($response_headers)
{
    $headers = array();

    foreach (explode("\r\n", $response_headers) as $i => $line)
    {
        if ($i != 0 && $line != "")
        {
            list ($key, $value) = explode(': ', $line);
            $headers[$key] = $value;
        }
    }

    return $headers;
}

# Get server request URL from GET params
$dest_url = $server . $_GET["url"];

# Setup cURL request and its options
$ch = curl_init($dest_url);
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt( $ch, CURLOPT_HEADER, 1);
curl_setopt( $ch, CURLOPT_USERPWD, $username . ":" . $password );
curl_setopt( $ch, CURLOPT_HTTPHEADER, $cookie_header_array);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

# Add game request POST params to cURL request
if(sizeof($_POST) > 0)
{
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, file_get_contents('php://input'));
}

# Execute CURL request and then close it
$res = curl_exec($ch);

# Store headers and content from the server response
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$headers = substr($res, 0, $header_size);
$body = substr($res, $header_size);
curl_close($ch);

# Apply headers to response
$headers = get_headers_as_array($headers);
foreach ($headers as $key => $value)
{
    if ($key == "Set-Cookie" || $key == "Content-Type")
    {
        header($key.": ".$value);
    }
}


# Echo the body of the response
echo $body;
?>