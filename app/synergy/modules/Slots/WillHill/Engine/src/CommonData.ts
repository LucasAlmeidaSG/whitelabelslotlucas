namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /* tslint:disable:member-ordering */

    /**
     * Contains data about the customer.
     * <Customer balance="0.00" currCode=""/>
     */
    @XML.Type("Customer")
    export class Customer
    {
        @XML.Property({ path: ".balance", type: XML.Number })
        public balance: number;

        @XML.Property({ path: ".currCode", type: XML.String })
        public currencyCode: string;
    }

    /**
     * Contains data about any possible error.
     * <Error type="RECOVER" msg="You have insufficient funds to place this stake. Please deposit more funds into your account."/>
     */
    @XML.Type("Error")
    export class Error
    {
        @XML.Property({ path: ".type", type: XML.String })
        public type: Error.Type;

        @XML.Property({ path: ".msg", type: XML.String })
        public message: string;
    }

    export namespace Error
    {
        export enum Type
        {
            Recover = "RECOVER",
            Fatal = "FATAL",
            RealityCheck = "REALITY_CHECK"
        }
    }

    /**
     * Contains data about the current game state.
     * <GameState state="open" mode="normal"/>
     */
    @XML.Type("GameState")
    export class GameState
    {
        @XML.Property({ path: ".state", type: XML.String })
        public state: GameState.State;

        @XML.Property({ path: ".mode", type: XML.String })
        public mode: GameState.Mode;

        @XML.Property({ path: ".freeSpinsUsed", type: XML.Number })
        public freeSpinsUsed: number;

        @XML.Property({ path: ".freeSpinsRemaining", type: XML.Number })
        public freeSpinsRemaining: number;

        @XML.Property({ path: ".freeSpinWinnings", type: XML.Number })
        public freeSpinWinnings: number;

        @XML.Property({ path: ".freeSpinInfo", type: XML.String })
        public freeSpinInfo: string;

        @XML.Property({ path: ".extraFreeSpinInfo", type: XML.String })
        public extraFreeSpinInfo: string;

        @XML.Property({ path: ".freeSpinFeature", type: XML.String })
        public freeSpinFeature: string;

        @XML.Property({ path: ".freeSpinBet", type: XML.Number })
        public freeSpinBet: number;

        @XML.Property({ path: ".freeSpinLines", type: XML.Number })
        public freeSpinLines: number;
    }

    export namespace GameState
    {
        export enum State
        {
            Open = "open",
            Closed = "closed"
        }

        export enum Mode
        {
            Normal = "normal",
            FreeSpins = "freespin"
        }
    }

    /**
     * Contains data about a single bet.
     * <Bet name="spin" lines="20" value="0.05" />
     */
    @XML.Type("Bet")
    export class Bet
    {
        @XML.Property({ path: ".name", type: XML.String })
        public name: string = "spin";

        @XML.Property({ path: ".lines", type: XML.Number, ignoreIfNull: true })
        public lines: number | null;

        @XML.Property({ path: ".value", type: XML.Number, ignoreIfNull: true })
        public value: number | null;
    }

    /**
     * Contains data about the result of a bet.
     * <BetResult>
     *     <Info totalStake="1.00" totalWinnings="1.25"/>
     *     <Settle name="spin" nextSpinType="NORMAL" winnings="1.25"/>
     * </BetResult>
     */
    @XML.Type("BetResult")
    export class BetResult
    {
        @XML.Property({ path: "Info.totalStake", type: XML.Number })
        public totalStake: number;

        @XML.Property({ path: "Info.totalWinnings", type: XML.Number })
        public totalWinnings: number;

        @XML.Property({ path: "Settle<name=spin>.nextSpinType", type: XML.String })
        public nextSpinType: string;
    }

    /**
     * Contains data about the result of a spin.
     * <Symbols Index="0" stopIndexes="54,14,5,35,42" symbolsInView="8,6,4|3,8,4|0,4,7|6,1,1|9,2,6" reelSetIndex="0"/>
     */
    @XML.Type("Symbols")
    export class SpinResultSymbols
    {
        @XML.Property({ path: ".Index", type: XML.Number })
        public spinIndex: number;

        @XML.Property({ path: ".stopIndexes", type: XML.DelimitedArray(XML.Number, ",") })
        public stopIndices: number[];

        @XML.Property({ path: ".symbolsInView", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
        public symbolsInView: number[][];

        @XML.Property({ path: ".reelSetIndex", type: XML.Number })
        public reelSetIndex: number;
    }

    /**
     * Contains data about a generic feature.
     */
    @XML.Type("Feature")
    export class Feature
    {
        @XML.Property({ path: ".name", type: XML.String })
        public name: string;

        @XML.Property({ path: ".type", type: XML.String })
        public type: string;

        @XML.Property({ path: ".value", type: XML.String })
        public value: string;

        @XML.Property({ path: ".payout", type: XML.Number })
        public payout: number;
    }

    /**
     * Contains data about a single winning winline.
     * <WinLine matchedSymbols="3" containsWild="True" payout="0.25" multiplier="1" symbolName="Jack" symbolIndex="8" lineIndex="7" matchedPositions="0,1,0"/>
     */
    @XML.Type("WinLine")
    export class WinLine
    {
        @XML.Property({ path: ".matchedSymbols", type: XML.Number })
        public matchedSymbols: number;

        @XML.Property({ path: ".containsWild", type: XML.Boolean })
        public containsWild: boolean;

        @XML.Property({ path: ".payout", type: XML.Number })
        public payout: number;

        @XML.Property({ path: ".multiplier", type: XML.Number })
        public multiplier: number;

        @XML.Property({ path: ".symbolName", type: XML.String })
        public symbolName: string;

        @XML.Property({ path: ".symbolIndex", type: XML.Number })
        public symbolIndex: number;

        @XML.Property({ path: ".lineIndex", type: XML.Number })
        public lineIndex: number;

        @XML.Property({ path: ".matchedPositions", type: XML.DelimitedArray(XML.Number, ",") })
        public matchedPositions: number[];

        @XML.Property({ path: ".direction", type: XML.String })
        public direction?: WinLine.Direction;
    }

    export namespace WinLine
    {
        export enum Direction
        {
            LeftToRight = "lr",
            RightToLeft = "rl"
        }
    }

    /**
     * Contains data about a set of winning winlines.
     * <WinLines Index="0" totalWin="1.25" winningLines="0"> ... </WinLines>
     */
    @XML.Type("WinLines")
    export class WinLines
    {
        @XML.Property({ path: ".Index", type: XML.Number })
        public spinIndex: number;

        @XML.Property({ path: ".totalWin", type: XML.Number })
        public totalWin: number;

        @XML.Property({ path: "", type: XML.Array(XML.Object) })
        public winlines: WinLine[];
    }

    /* tslint:enable:member-ordering */
}