namespace RS.Slots.WillHill
{
    /**
     * Encapsulates a slots engine interface for the William Hill engine.
     */
    export class Engine<TModels extends Slots.Models = Slots.Models> implements Slots.Engine.IEngine<TModels, Engine.Settings, Engine.ReplayData>
    {
        /** Published when an init response has been received and fully processed. */
        public readonly onInitResponseReceived = createEvent<RS.Engine.IResponse>();

        /** Published when a bet response has been received and fully processed. */
        public readonly onBetResponseReceived = createEvent<RS.Engine.IResponse>();

        /** Published when a spin response has been received and fully processed. */
        public readonly onSpinResponseReceived = createEvent<RS.Engine.IResponse>();

        /** Published when a close response has been received and fully processed. */
        public readonly onCloseResponseReceived = createEvent<RS.Engine.IResponse>();

        /** Gets if this engine supports replay. */
        public readonly supportsReplay: boolean = false;

        /** Gets if this engine has reel sets. */
        public readonly hasReelSets: boolean = false;

        protected _currencyMultiplier = 100;

        protected _force: Slots.Engine.ForceResult|null = null;
        protected _isFreeSpins: boolean = false;

        public constructor(public readonly settings: Engine.Settings, public readonly models: TModels) { }

        /**
         * Rigs the next spin request.
         */
        public force(forceResult: OneOrMany<Slots.Engine.ForceResult>)
        {
            if (RS.Is.array(forceResult))
            {
                RS.Log.warn("Array forcing not yet implemented for WH engine.");
                forceResult = forceResult[0];
            }

            this._force = forceResult;
        }

        public replay(replayData: {}): void
        {
            Log.warn("Replay not yet implemented for WH engine.");
        }

        /**
         * Initialises the engine and sends an init request.
         * Async.
         */
        public async init(): Promise<RS.Engine.IResponse>
        {
            Cookie.set("GameRoom", `ID=${this.settings.accountID}`);
            Cookie.set("CurrCode", this.settings.currCode);
            const req = new this.settings.initRequest(this.settings.networkSettings);
            let response: InitResponse;
            try
            {
                response = await req.send({});
            }
            catch (err)
            {
                this.handleNetworkError(err);
                return { isError: true, request: req };
            }

            const transformCurrency = (v: number) => v * this._currencyMultiplier;

            this.models.config.reelSets = null;
            this.models.config.symbolNames = response.settings.symbolNames;
            this.models.config.symbolPayouts = response.settings.paytableValues;
            this.models.config.symbolPayoutTables = [response.settings.paytableValues];
            this.models.config.winlines = response.settings.winLines.map((wl) => wl.map<RS.Slots.ReelSetPosition>((rowIndex, reelIndex) =>
            {
                return { reelIndex, rowIndex };
            }));
            this.models.config.extraWinlines = null;
            this.models.config.bigBetEnabled = false;
            this.models.config.defaultReels =
            {
                currentReelSetIndex: 0,
                stopIndices: response.settings.stopIndices,
                symbolsInView: new Slots.ReelSet(response.settings.symbolsInView)
            };

            this.models.stake.betOptions = response.settings.coinsPerLine.map(transformCurrency).map((value) =>
            {
                return { type: RS.Slots.Models.Stake.Type.PerLine, value };
            });
            this.models.stake.defaultBetIndex = response.settings.defaultCoinIndex;
            this.models.stake.currentBetIndex = response.settings.defaultCoinIndex;
            this.models.stake.paylineOptions = [ response.settings.maxLines ];
            this.models.stake.defaultPaylineIndex = 0;
            this.models.stake.currentPaylineIndex = 0;

            this.models.spinResults.length = 0;

            this.parseCustomer(response.customer);
            if (response.gameState) { this.parseState(response.gameState); }
            if (response.bets) { this.parseBets(response.bets); }
            if (response.spinResults) { this.parseSpinResults(response.spinResults); }
            if (response.features) { this.parseFeatures(response.features); }
            if (response.winlines) { this.parseWinlines(response.winlines); }
            if (response.betResult) { this.allocateWins(response.betResult); }
            if (response.error) { this.parseError(response.error); } else { this.models.error.type = RS.Models.Error.Type.None; }
            this.calculateSpinResultBalances(response.betResult);

            this.onInitResponseReceived.publish(response);
            return response;
        }

        /**
         * Sends an spin request.
         * Async.
         */
        public async logic(payload: RS.Slots.Engine.ISpinRequest.Payload): Promise<SpinResponse>
        {
            const errorResponse =
            {
                isError: true,
                request: null,
                customer: null,
                gameState: null,
                bets: null,
                betResult: null,
                spinResults: null,
                features: null,
                winlines: null
            };

            let response: SpinResponse;
            if (this._isFreeSpins)
            {
                const req = new this.settings.freeSpinRequest(this.settings.networkSettings);
                try
                {
                    response = await req.send();
                }
                catch (err)
                {
                    this.handleNetworkError(err);
                    return errorResponse;
                }
            }
            else
            {
                const req = new this.settings.spinRequest(this.settings.networkSettings);
                try
                {
                    const betAmount = Models.Stake.toPerLine(this.models.stake, payload.betAmount);
                    betAmount.value /= this._currencyMultiplier;
                    response = await req.send({
                        betAmount,
                        paylineCount: payload.paylineCount
                    });
                }
                catch (err)
                {
                    this.handleNetworkError(err);
                    return errorResponse;
                }
            }

            return response;
        }

        public async bet(payload: RS.Slots.Engine.ISpinRequest.Payload): Promise<RS.Engine.IResponse>
        {
            return await this.spin(payload);
        }

        public async spin(payload: RS.Slots.Engine.ISpinRequest.Payload): Promise<RS.Engine.IResponse>
        {
            // If we have a pending rig, deal with that first
            if (this._force)
            {
                try
                {
                    await this.rig(this._force);
                }
                catch (err)
                {
                    this.handleNetworkError(err);
                    return { isError: true, request: null };
                }
                this._force = null;
            }

            const response = await this.logic(payload);

            this.parseCustomer(response.customer);
            if (response.gameState) { this.parseState(response.gameState); }
            if (response.bets) { this.parseBets(response.bets); }
            if (response.spinResults) { this.parseSpinResults(response.spinResults); }
            if (response.features) { this.parseFeatures(response.features); }
            if (response.winlines) { this.parseWinlines(response.winlines); }
            if (response.betResult) { this.allocateWins(response.betResult); }
            if (response.error) { this.parseError(response.error); } else { this.models.error.type = RS.Models.Error.Type.None; }
            this.calculateSpinResultBalances(response.betResult);

            this._force = null;
            this.onSpinResponseReceived.publish(response);
            return response;
        }

        /**
         * Sends a close request.
         * Async.
         */
        public async close(): Promise<RS.Engine.IResponse>
        {
            const req = new this.settings.closeRequest(this.settings.networkSettings);

            let response: CloseResponse;
            try
            {
                response = await req.send({});
            }
            catch (err)
            {
                this.handleNetworkError(err);
                return { isError: true, request: req };
            }

            this.parseState(response.gameState);
            if (response.error) { this.parseError(response.error); } else { this.models.error.type = RS.Models.Error.Type.None; }

            this.onCloseResponseReceived.publish(response);
            return response;
        }

        /**
         * Gets an array of all triggerable replays.
         */
        public async getReplayData()
        {
            return {} as Map<Engine.ReplayData>;
        }

        /**
         * Gets an array of all GREG replays.
         */
        public async getGREGData() { return {}; }

        /**
         * Applies the specified rig.
         * @param force
         */
        protected async rig(force: Slots.Engine.ForceResult)
        {
            const numberList: number[] = [];
            if (force.stopIndices != null)
            {
                numberList.push(force.reelSetIndex || 0);
                for (const val of force.stopIndices)
                {
                    numberList.push(val);
                }
            }
            if (force.featureIndex != null && this.settings.featureIndexToBonusNumber != null && force.featureIndex in this.settings.featureIndexToBonusNumber)
            {
                numberList.push(this.settings.featureIndexToBonusNumber[force.featureIndex]);
            }

            const url = `${document.location.origin}/rng/fix_rng.asp?sessionId=${this.settings.accountID}&numberList=${numberList.join(",")}`;
            await RS.Request.ajax({ url }, Request.AJAXResponseType.Text);
        }

        protected parseCustomer(data: Customer)
        {
            RS.Models.Balances.clear(this.models.customer.finalBalance);
            this.models.customer.finalBalance.primary = data.balance * this._currencyMultiplier;
            this.models.customer.finalBalance.named[RS.Models.Balances.Cash] = data.balance * this._currencyMultiplier;
            this.models.customer.currencyCode = data.currencyCode;
        }

        protected parseState(data: GameState)
        {
            switch (data.state)
            {
                case GameState.State.Open:
                    this.models.state.state = Slots.Models.State.Type.Open;
                    break;
                default:
                    this.models.state.state = Slots.Models.State.Type.Closed;
                    break;
            }
            if (data.freeSpinsRemaining != null && data.freeSpinsUsed != null)
            {
                // If the game is closed (e.g. in between free spins) or at least 1 free spin has been used, we are now considered "in free spins"
                this.models.freeSpins.isFreeSpins = data.freeSpinsUsed > 0 || data.state === GameState.State.Closed;
                this.models.freeSpins.used = data.freeSpinsUsed;
                this.models.freeSpins.remaining = data.freeSpinsRemaining;
                this.models.freeSpins.winAmount = data.freeSpinWinnings * this._currencyMultiplier;
            }
            this._isFreeSpins = data.mode === GameState.Mode.FreeSpins && data.freeSpinsRemaining > 0;
        }

        protected parseBets(data: Bet[])
        {
            const betVal = data[0].value * this._currencyMultiplier;
            for (let i = 0, l = this.models.stake.betOptions.length; i < l; ++i)
            {
                if (this.models.stake.betOptions[i].value === betVal)
                {
                    this.models.stake.currentBetIndex = i;
                    break;
                }
            }
            this.models.stake.currentPaylineIndex = this.models.stake.paylineOptions.indexOf(data[0].lines);
        }

        protected parseSpinResults(spinResults: SpinResultSymbols[])
        {
            this.models.spinResults.length = spinResults.length;
            for (let i = 0, l = spinResults.length; i < l; ++i)
            {
                const mdl = this.models.spinResults[i] || (this.models.spinResults[i] = {} as Models.SpinResult);
                const data = spinResults[i];
                mdl.index = data.spinIndex;
                mdl.reels =
                {
                    currentReelSetIndex: data.reelSetIndex,
                    stopIndices: data.stopIndices,
                    symbolsInView: new Slots.ReelSet(data.symbolsInView as Slots.ReadonlyArray2D<number>)
                };
            }
        }

        protected parseFeatures(features: Feature[])
        {
            // Derived class should handle this
        }

        protected parseWinlines(winLines: WinLines[])
        {
            for (let i = 0, l = winLines.length; i < l; ++i)
            {
                const mdl = this.models.spinResults[i];
                if (mdl != null)
                {
                    const data = winLines[i];
                    mdl.win =
                    {
                        paylineWins: (data.winlines || []).map<Models.PaylineWin>((w) =>
                        {
                            let positions: RS.Slots.ReelSetPosition[];
                            if (w.direction === WinLine.Direction.RightToLeft)
                            {
                                // Right-to-left
                                positions = [];
                                for (let reelIndex = w.matchedPositions.length - 1; reelIndex >= 0; --reelIndex)
                                {
                                    const rowIndex = w.matchedPositions[reelIndex];
                                    if (rowIndex === -1) { break; }
                                    positions.push({ reelIndex, rowIndex });
                                }
                            }
                            else
                            {
                                // Left-to-right
                                positions = [];
                                for (let reelIndex = 0, l2 = w.matchedPositions.length; reelIndex < l2; ++reelIndex)
                                {
                                    const rowIndex = w.matchedPositions[reelIndex];
                                    if (rowIndex === -1) { break; }
                                    positions.push({ reelIndex, rowIndex });
                                }
                            }
                            return {
                                lineIndex: w.lineIndex,
                                reverse: w.direction === WinLine.Direction.RightToLeft,
                                symbolID: w.symbolIndex,
                                multiplier: w.multiplier,
                                totalWin: w.payout * this._currencyMultiplier,
                                positions
                            };
                        }),
                        totalWin: data.totalWin * this._currencyMultiplier,
                        accumulatedWin: 0,
                        winAmounts: {}
                    };
                }
            }
        }

        protected allocateWins(data: BetResult): void
        {
            let accumulatedWin: number = data.totalWinnings;
            for (let i: number = this.models.spinResults.length - 1; i >= 0; i--)
            {
                const spinResult = this.models.spinResults[i];
                spinResult.win.accumulatedWin = accumulatedWin;
                accumulatedWin -= spinResult.win.totalWin;
            }
        }

        protected calculateSpinResultBalances(betResult?: BetResult)
        {
            let curBalance = RS.Models.Balances.clone(this.models.customer.finalBalance);
            for (let i = this.models.spinResults.length - 1; i >= 0; --i)
            {
                const spinResult = this.models.spinResults[i];
                const totalWin = spinResult.win.totalWin;
                spinResult.balanceAfter = curBalance;
                spinResult.balanceBefore = RS.Models.Balances.clone(spinResult.balanceAfter);
                this.subtractBalance(spinResult.balanceBefore, totalWin);
            }
            if (betResult)
            {
                curBalance = RS.Models.Balances.clone(this.models.customer.finalBalance);
                this.subtractBalance(curBalance, betResult.totalWinnings * this._currencyMultiplier);
                this.models.spinResults[0].balanceBefore = curBalance;
            }
        }

        protected trySubtractNamedBalance(balances: RS.Models.Balances, name: string, toSubtract: number): number
        {
            if (!(name in balances.named)) { return toSubtract; }
            if (toSubtract <= 0) { return 0; }
            const amount = balances.named[name];
            if (toSubtract > amount)
            {
                balances.named[name] = 0;
                return toSubtract - amount;
            }
            else
            {
                balances.named[name] = amount - toSubtract;
                return 0;
            }
        }

        protected subtractBalance(balances: RS.Models.Balances, toSubtract: number): void
        {
            // Try and take from promotional first
            toSubtract = this.trySubtractNamedBalance(balances, RS.Models.Balances.Promotional, toSubtract);

            // Now try and take from cash
            toSubtract = this.trySubtractNamedBalance(balances, RS.Models.Balances.Cash, toSubtract);

            // Update primary balance
            balances.primary = (balances.named[RS.Models.Balances.Promotional] || 0) + (balances.named[RS.Models.Balances.Cash] || 0);
        }

        protected parseError(error: Error)
        {
            this.models.error.type = RS.Models.Error.Type.Server;
            this.models.error.title = "SERVER ERROR";
            this.models.error.message = error.message;
            switch (error.type)
            {
                case Error.Type.Recover:
                    this.models.error.options =
                    [
                        { type: RS.Models.Error.OptionType.Continue, text: "OK" }
                    ];
                    break;
                case Error.Type.Fatal:
                    this.models.error.options =
                    [
                        { type: RS.Models.Error.OptionType.Refresh, text: "OK" }
                    ];
                    break;
                default:
                    this.models.error.type = RS.Models.Error.Type.None;
                    break;
            }
        }

        protected handleNetworkError(err?: ProgressEvent|Error|string)
        {
            if (err instanceof ProgressEvent)
            {
                err = "No response received from the server";
            }
            else if (err instanceof Error)
            {
                err = err.message;
            }
            else if (!err)
            {
                err = "Unknown server error";
            }
            this.models.error.type = RS.Models.Error.Type.Server;
            this.models.error.title = "NETWORK ERROR";
            this.models.error.message = err;
            this.models.error.options =
            [
                { type: RS.Models.Error.OptionType.Refresh, text: "OK" }
            ];
        }
    }

    export namespace Engine
    {
        export interface NetworkSettings
        {
            endpoint: string;
            contentType: string;
            gameName: string;
        }

        export interface Settings
        {
            networkSettings: NetworkSettings;
            initRequest: { new(settings: NetworkSettings): InitRequest; };
            spinRequest: { new(settings: NetworkSettings): SpinRequest; };
            freeSpinRequest: { new(settings: NetworkSettings): FreeSpinRequest; };
            closeRequest: { new(settings: NetworkSettings): CloseRequest; };
            accountID: string;
            currCode: string;
            featureIndexToBonusNumber?: number[];
        }

        /**
         * Data for triggering a replay.
         */
        export interface ReplayData
        {
            historyServerURL: string;
            betID: string;
        }
    }
}
