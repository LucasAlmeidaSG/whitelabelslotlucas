/// <reference path="../CommonData.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /* tslint:disable:member-ordering */

    export class SpinResponse implements RS.Engine.IResponse
    {
        public constructor(public readonly request: SpinRequest) { }

        @XML.Property({ path: "Customer", type: XML.Object })
        public customer: Customer;

        @XML.Property({ path: "Error", type: XML.Object })
        public error?: Error;

        @XML.Property({ path: "GameState", type: XML.Object })
        public gameState: GameState;

        @XML.Property({ path: "PlaceBet", type: XML.Array(XML.Object) })
        public bets: Bet[];

        @XML.Property({ path: "BetResult", type: XML.Object })
        public betResult: BetResult;

        @XML.Property({ path: "SpinResults", type: XML.Array(XML.Object) })
        public spinResults: SpinResultSymbols[];

        @XML.Property({ path: "Features", type: XML.Array(XML.Object) })
        public features: Feature[];

        @XML.Property({ path: "Winlines", type: XML.Array(XML.Object) })
        public winlines: WinLines[];

        public get isError() { return this.error != null && this.error.type !== Error.Type.RealityCheck; }
    }

    /* tslint:enable:member-ordering */
}