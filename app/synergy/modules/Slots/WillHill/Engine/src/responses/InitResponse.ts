/// <reference path="../CommonData.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /* tslint:disable:member-ordering */

    @XML.Type("GameSettings")
    export class GameSettings
    {
        @XML.Property({ path: ".game", type: XML.String })
        public game: string;

        @XML.Property({ path: "Setting<name=CoinsPerLine>.value", type: XML.DelimitedArray(XML.Number, ",") })
        public coinsPerLine: number[];

        @XML.Property({ path: "Setting<name=MaxLines>.value", type: XML.Number })
        public maxLines: number;

        @XML.Property({ path: "Setting<name=DefaultCoinIndex>.value", type: XML.Number })
        public defaultCoinIndex: number;

        @XML.Property({ path: "Setting<name=SymbolNames>.value", type: XML.DelimitedArray(XML.String, ",") })
        public symbolNames: string[];

        @XML.Property({ path: "Setting<name=PaytableValues>.value", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
        public paytableValues: number[][];

        @XML.Property({ path: "Setting<name=StopIndexes>.value", type: XML.DelimitedArray(XML.Number, ",") })
        public stopIndices: number[];

        @XML.Property({ path: "Setting<name=SymbolsInView>.value", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
        public symbolsInView: number[][];

        @XML.Property({ path: "Setting<name=WinLines>.value", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
        public winLines: number[][];
    }

    export class InitResponse implements RS.Engine.IResponse
    {
        public constructor(public readonly request: InitRequest) { }

        @XML.Property({ path: "Customer", type: XML.Object })
        public customer: Customer;

        @XML.Property({ path: "Error", type: XML.Object })
        public error?: Error;

        @XML.Property({ path: "GameState", type: XML.Object })
        public gameState?: GameState;

        @XML.Property({ path: "PlaceBet", type: XML.Array(XML.Object) })
        public bets?: Bet[];

        @XML.Property({ path: "BetResult", type: XML.Object })
        public betResult?: BetResult;

        @XML.Property({ path: "SpinResults", type: XML.Array(XML.Object) })
        public spinResults?: SpinResultSymbols[];

        @XML.Property({ path: "Features", type: XML.Array(XML.Object) })
        public features?: Feature[];

        @XML.Property({ path: "Winlines", type: XML.Array(XML.Object) })
        public winlines?: WinLines[];

        @XML.Property({ path: "GameSettings", type: XML.Object })
        public settings: GameSettings;

        public get isError() { return this.error != null && this.error.type !== Error.Type.RealityCheck; }
    }

    /* tslint:enable:member-ordering */
}