/// <reference path="../CommonData.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /* tslint:disable:member-ordering */

    export class CloseResponse implements RS.Engine.IResponse
    {
        public constructor(public readonly request: CloseRequest) { }

        @XML.Property({ path: "Customer", type: XML.Object })
        public customer: Customer;

        @XML.Property({ path: "Error", type: XML.Object })
        public error?: Error;

        @XML.Property({ path: "GameState", type: XML.Object })
        public gameState: GameState;

        public get isError() { return this.error != null; }
    }

    /* tslint:enable:member-ordering */
}