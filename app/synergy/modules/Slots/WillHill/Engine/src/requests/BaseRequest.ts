namespace RS.Slots.WillHill
{
    /**
     *  <font face="Arial" size=2>
     *  <p>Microsoft VBScript runtime </font> <font face="Arial" size=2>error '800a000d'</font>
     *  <p>
     *  <font face="Arial" size=2>Type mismatch: 'CDbl'</font>
     *  <p>
     *  <font face="Arial" size=2>/English/AladdinsVacationEngineDev/Config/ValidationFunctions.asp</font><font face="Arial" size=2>, line 71</font>
     */

    /**
     * Base request functionality.
     */
    export abstract class BaseRequest
    {
        public constructor(public readonly networkSettings: Engine.NetworkSettings) { }

        /**
         * Sends the actual network request.
         * @param req
         */
        protected sendNetworkRequest(payload: object, response: object): PromiseLike<void>
        {
            // Serialise payload to XML
            const doc = Serialisation.XML.serialise(payload);
            return new Promise((resolve, reject) =>
            {
                const req = new XMLHttpRequest();
                req.addEventListener("load", (ev) =>
                {
                    RS.Slots.IPlatform.get().networkResponseReceived(req);
                    if (req.responseXML != null)
                    {
                        if (req.responseXML.documentElement instanceof HTMLElement)
                        {
                            reject("Response was a HTML document, probably an asp error message");
                            return;
                        }
                        try
                        {
                            Serialisation.XML.deserialise(response, req.responseXML);
                        }
                        catch (err)
                        {
                            reject(err);
                            return;
                        }
                        resolve();
                    }
                    else if (req.responseText != null)
                    {
                        if (/Microsoft VBScript runtime/g.test(req.responseText))
                        {
                            const matches = RS.Util.matchAll(/<font face="Arial" size=2>([^<]*)<\/font>/g, req.responseText);
                            const errCode = matches[0] && matches[0][1];
                            const errMessage = matches[1] && matches[1][1];
                            const errFile = matches[2] && matches[2][1];
                            const errLine = matches[3] && matches[3][1];
                            
                            if (errCode && errMessage && errFile && errLine)
                            {
                                reject(`${errCode}: ${errMessage} (${errFile}${errLine})`);
                            }
                            else
                            {
                                reject("Unknown ASP error");
                            }
                        }
                        else if (req.responseText.length <= 20)
                        {
                            reject(req.responseText);
                        }
                        else
                        {
                            reject(`${req.responseText.substr(0, 17)}...`);
                        }
                    }
                    else
                    {
                        reject(ev);
                    }
                });
                req.addEventListener("error", (ev) =>
                {
                    RS.Slots.IPlatform.get().networkResponseReceived(req);
                    reject(ev);
                });
                req.addEventListener("abort", (ev) =>
                {
                    RS.Slots.IPlatform.get().networkResponseReceived(req);
                    reject("aborted");
                });
                req.open("POST", this.networkSettings.endpoint, true);
                req.setRequestHeader("Content-type", this.networkSettings.contentType);
                req.send(doc);
            });
        }
    }
}