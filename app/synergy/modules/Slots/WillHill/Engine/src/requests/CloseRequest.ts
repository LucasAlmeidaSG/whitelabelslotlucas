/// <reference path="../responses/SpinResponse.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /*
    <GameRequest name="AladdinsVacation">
        <Close />
    </GameRequest>
    */

    export class CloseRequest extends BaseRequest implements RS.Engine.ICloseRequest<RS.Engine.ICloseRequest.Payload, CloseResponse>
    {
        public async send(payload: RS.Engine.ICloseRequest.Payload)
        {
            const request = new CloseRequest.Payload();
            request.name = this.networkSettings.gameName;
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createResponse(): CloseResponse
        {
            return new CloseResponse(this);
        }
    }

    export namespace CloseRequest
    {
        @XML.Type("GameRequest")
        export class Payload
        {
            @XML.Property({ path: ".name", type: XML.String })
            public name: string;

            @XML.Property({ path: "Close", type: XML.EmptyTag })
            protected close = "Close";
        }
    }
}
