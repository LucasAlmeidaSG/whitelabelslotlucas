/// <reference path="SpinRequest.ts" />
/// <reference path="../responses/SpinResponse.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /* 
    <GameRequest name="AladdinsVacation">
        <PlaceBet>
            <Bet name="freespin" />
        </PlaceBet>
    </GameRequest>
    */

    export class FreeSpinRequest extends SpinRequest
    {
        public async send()
        {
            const request = new SpinRequest.Payload();
            request.name = this.networkSettings.gameName;
            request.bets = [ new Bet() ];
            request.bets[0].name = "freespin";
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }
    }
}