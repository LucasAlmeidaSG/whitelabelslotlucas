/// <reference path="../responses/SpinResponse.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /*
    <GameRequest name="AladdinsVacation">
        <PlaceBet>
            <Bet name="spin" lines="20" value="0.05" />
        </PlaceBet>
    </GameRequest>
    */

    export class SpinRequest extends BaseRequest implements RS.Slots.Engine.ISpinRequest<RS.Slots.Engine.ISpinRequest.Payload, SpinResponse>
    {
        public async send(payload: RS.Slots.Engine.ISpinRequest.Payload)
        {
            const request = new SpinRequest.Payload();
            request.name = this.networkSettings.gameName;
            request.bets = [ new Bet() ];
            request.bets[0].lines = payload.paylineCount;
            request.bets[0].value = payload.betAmount.value;
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createResponse(): SpinResponse
        {
            return new SpinResponse(this);
        }
    }

    export namespace SpinRequest
    {
        @XML.Type("GameRequest")
        export class Payload
        {
            @XML.Property({ path: ".name", type: XML.String })
            public name: string;

            @XML.Property({ path: "PlaceBet", type: XML.Array(XML.Object) })
            public bets: Bet[];
        }
    }
}
