/// <reference path="../responses/InitResponse.ts" />

namespace RS.Slots.WillHill
{
    import XML = Serialisation.XML;

    /*
    <GameRequest name="AladdinsVacation">
        <Init />
    </GameRequest>
    */

    export class InitRequest extends BaseRequest implements RS.Engine.IInitRequest<RS.Engine.IInitRequest.Payload, InitResponse>
    {
        public async send(payload: RS.Engine.IInitRequest.Payload)
        {
            const request = new InitRequest.Payload();
            request.name = this.networkSettings.gameName;
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createResponse(): InitResponse
        {
            return new InitResponse(this);
        }
    }

    export namespace InitRequest
    {
        @XML.Type("GameRequest")
        export class Payload
        {
            @XML.Property({ path: ".name", type: XML.String })
            public name: string;

            @XML.Property({ path: "Init", type: XML.EmptyTag })
            protected init = "Init";
        }
    }
}
