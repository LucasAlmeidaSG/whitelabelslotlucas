namespace SG.GLM.Tests.XML
{
    export const spinResponseText: string =
`<GameResponse type="Logic">
    <Header sessionID="Avd0OYsn/teVLZ06fWIEAfLvJXiu5LoeY5j+RY99Y8UyrVuBWVthXiTc4oR81gRDSo/xl9Zq0xhyI/RklUa7/KWl4Q+QUEHMb35CvxLLeC0=" ccyCode="en_GB" deciSep="." thousandSep="," lang="en_GB" gameID="20240" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
    <AccountData>
        <AccountData>
            <CurrencyMultiplier>1</CurrencyMultiplier>
        </AccountData>
    </AccountData>
    <Balances>
        <Balance name="CASH_BALANCE" value="99820"/>
    </Balances>
    <GameResult stake="200" stakePerLine="4" paylineCount="50" totalWin="20" betID="">
        <ReelResults numSpins="1">
            <ReelSpin spinIndex="0" reelsetIndex="0" winCountPL="1" winCountSC="0" spinWins="20" freeSpin="N" bonusAwarded="N">
                <ReelStops>64|65|128|97|46</ReelStops>
                <PaylineWin index="10" winVal="20" awardIndex="6" awardTableIndex="0">1|2|10</PaylineWin>
            </ReelSpin>
        </ReelResults>
        <BGInfo totalWagerWin="20" bgWinnings="20" isMaxWin="0"/>
    </GameResult>
</GameResponse>`;

    export const spinResponseExpected: Partial<SpinResponse> =
    {
        request: null,
        error: null,
        header:
        {
            affiliate: null,
            ccyCode: "en_GB",
            channel: null,
            freePlay: null,
            gameCodeRGI: null,
            gameID: 20240,
            glsID: null,
            lang: "en_GB",
            promotions: null,
            sessionID: "Avd0OYsn/teVLZ06fWIEAfLvJXiu5LoeY5j+RY99Y8UyrVuBWVthXiTc4oR81gRDSo/xl9Zq0xhyI/RklUa7/KWl4Q+QUEHMb35CvxLLeC0=",
            userID: null,
            userType: null,
            versionID: "1_0",
            fullVersionID: "1.0.-1",
            isRecovering: false
        },
        accountData:
        {
            currencyInformation: null,
            currencyMultiplier: null,
            payload: null
        },
        balances:
        [
            {
                name: "CASH_BALANCE",
                value: 99820
            }
        ],
        gameResult:
        {
            stake: 200,
            stakePerLine: 4,
            paylineCount: 50,
            totalWin: 20,
            betID: "",
            reelSpins:
            [
                {
                    spinIndex: 0,
                    reelsetIndex: 0,
                    winCountPL: 1,
                    winCountSC: 0,
                    spinWins: 20,
                    freeSpin: false,
                    bonusAwarded: false,
                    reelStops: [64, 65, 128, 97, 46],
                    paylineWins:
                    [
                        {
                            index: 10,
                            winVal: 20,
                            awardIndex: 6,
                            awardTableIndex: 0,
                            positions:
                            [
                                1,
                                2,
                                10
                            ]
                        }
                    ],
                    scatterWins: []
                }
            ],
            bgInfo:
            {
                totalWagerWin: 20,
                bgWinnings: 20,
                baseGameSpinsRemaining: null,
                isBigBet: null,
                isMaxWin: false
            },
            fsInfo: null
        }
    }
}