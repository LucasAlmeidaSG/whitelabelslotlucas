namespace SG.GLM.Tests
{
    describe("InitResponse.ts", function ()
    {
        it("should correctly parse a response", function ()
        {
            const xmlResponse = new DOMParser().parseFromString(XML.initResponseText, "text/xml");
            const response = new SG.GLM.InitResponse(null);
            RS.Serialisation.XML.deserialise(response, xmlResponse);
            expectDeepEqual(response, XML.initResponseExpected);
        });
    });
}
