namespace SG.GLM.Tests
{
    const { expect } = chai;
    import Is = RS.Is;

    export function expectDeepEqual(target: object, value: object)
    {
        for (const key in value)
        {
            const expected = value[key];
            if (expected == null || Is.number(expected) || Is.string(expected) || Is.boolean(expected))
            {
                expect(target[key], `${key} should be ${value[key]}`).to.equal(value[key]);
            }
            else
            {
                const expectedType = Is.array(expected) ? "array" : "object";
                expect(target[key], `${key} should be an ${expectedType}`).to.be.an(expectedType);
                expectDeepEqual(target[key], expected);
            }
        }
    }
}