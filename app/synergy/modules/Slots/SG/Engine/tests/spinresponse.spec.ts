namespace SG.GLM.Tests
{
    describe("SpinResponse.ts", function ()
    {
        it("should correctly parse a response", function ()
        {
            const xmlResponse = new DOMParser().parseFromString(XML.spinResponseText, "text/xml");
            const response = new SG.GLM.SpinResponse(null);
            RS.Serialisation.XML.deserialise(response, xmlResponse);
            expectDeepEqual(response, XML.spinResponseExpected);
        });
    });
}
