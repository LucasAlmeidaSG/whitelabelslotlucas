namespace SG.GLM.FreeRoundsUtils
{
    export function createFreeRounds(info: Environment.FreeRoundsInfo): FreeRounds
    {
        const freeRounds = new FreeRounds();
        freeRounds.status = Environment.FreeRoundsInfo.Status.toRaw(info.status);
        freeRounds.activationID = info.activationID;
        freeRounds.campaignID = info.campaignID;
        freeRounds.type = "FREEROUND";
        return freeRounds;
    }

    export function getRequestHeaders(info: Environment.FreeRoundsInfo): string | null
    {
        if (!info) { return null; }
        RS.Log.debug(`[SG.GLM.BaseRequest] Free Rounds status: ${Environment.FreeRoundsInfo.Status[info.status]} (pending rounds: ${info.roundsRemaining || 0})`);
        if (!Environment.FreeRoundsInfo.isOpen(info)) { return null; }
        const freeRoundsInfo = RS.Serialisation.XML.serialise(createFreeRounds(info));
        const rootTag = RS.Serialisation.XML.Type.get(FreeRounds);
        return freeRoundsInfo.getElementsByTagName(rootTag)[0].innerHTML;
    }

    export function setRequestHeaders(info: Environment.FreeRoundsInfo, headers: RS.Map<string>): void
    {
        const text = getRequestHeaders(info);
        if (text)
        {
            if (RS.IPlatform.get().adapter instanceof SG.Environment.DummyPartnerAdapter)
            {
                // Do nothing but debug log so we don't get rejected by CORS
                RS.Log.debug(`[SG.GLM.BaseRequest] Free Rounds header: ${text}`);
            }
            else
            {
                headers["FR-Info"] = text;
            }
        }
    }
}