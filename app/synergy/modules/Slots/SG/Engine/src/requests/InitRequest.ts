/// <reference path="../responses/InitResponse.ts" />

namespace SG.GLM
{
    /**
     * TODO: remove this
     * @deprecated
     */
    export import InitRequestPayload = SG.GLM.Core.InitRequestPayload;

    /*
    <GameRequest type="Init">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="SESSION_ID" userID="1000018628" userType="C" versionID="1_0" />
    </GameRequest>
    */

    export class InitRequest extends Core.InitRequest
    {
        constructor(public readonly networkSettings: Core.Engine.NetworkSettings, public readonly userSettings: Engine.UserSettings)
        {
            super(networkSettings, userSettings);
        }

        protected createResponse(): InitResponse
        {
            return new InitResponse(this);
        }

        protected onComplete(req: XMLHttpRequest)
        {
            return RS.Slots.IPlatform.get().networkResponseReceived(req);
        }

        protected getRequestHeaders(): RS.Map<string>
        {
            const headers = super.getRequestHeaders();
            FreeRoundsUtils.setRequestHeaders(this.userSettings.freeRoundsInfo, headers);
            return headers;
        }
    }

    export namespace InitRequest
    {
        /**
         * TODO: remove this
         * @deprecated
         */
        export import Payload = Core.InitRequest.Payload;
    }
}
