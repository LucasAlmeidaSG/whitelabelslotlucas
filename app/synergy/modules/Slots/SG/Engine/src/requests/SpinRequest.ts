/// <reference path="../responses/SpinResponse.ts" />

namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    /*
    <GameRequest type="Logic">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="Tfxz7fWLLbVKUS9mer9rC/yfzVG2EU8b6EIUzJer56jCVialR8bvCq5E70Zbt3sj" userID="1000018628" userType="C" versionID="1_0" />
        <Stake total="50000"/>

        <FORCE><ReelSpin stopIndices="0|0|0|0|0" bandsetIndex="1"/></FORCE>
    </GameRequest>
    */

    /*
    <GameRequest type="Logic">
        <Header affiliate="703" ccyCode="" channel="I" freePlay="Y" gameCodeRGI="aladdinsvacation" gameID="20232" glsID="65535" lang="en_gb" promotions="N" sessionID="Tfxz7fWLLbVKUS9mer9rC/yfzVG2EU8b6EIUzJer56jCVialR8bvCq5E70Zbt3sj" userID="1000018628" userType="C" versionID="1_0" />
        <AccountData>
            <CurrencyMultiplier>1</CurrencyMultiplier>
        </AccountData>
        <Stake total="50000"/>

        <FORCE>
            <ReelSpin stopIndices="0|0|0|0|0" bandsetIndex="1"/>
            <ReelSpin stopIndices="1|2|3|4|5" bandsetIndex="1"/>
            <ReelSpin stopIndices="6|7|8|9|10" bandsetIndex="1"/>
        </FORCE>

    </GameRequest>
    */

    export interface SpinRequestPayload extends RS.Slots.Engine.ISpinRequest.Payload
    {
        forceResult? : RS.Slots.Engine.ForceResult;
        replay?: SG.Replay.IReplay;
        currencyMultiplier?: number;
    }

    export class SpinRequest extends Core.LogicRequest implements RS.Slots.Engine.ISpinRequest<SpinRequestPayload, SpinResponse>
    {
        constructor(public readonly networkSettings: Core.Engine.NetworkSettings, public readonly userSettings: Engine.UserSettings)
        {
            super(networkSettings, userSettings);
        }

        public async send(payload: SpinRequestPayload)
        {
            const request = this.createRequest(payload);
            const response = this.createResponse();
            await this.sendNetworkRequest(request, response);
            return response;
        }

        protected createRequest(payload: SpinRequestPayload): SpinRequest.Payload
        {
            const request = super.createRequest(payload) as SpinRequest.Payload;
            request.paylineCount = payload.paylineCount;
            return request;
        }

        protected createReelSpin(): SpinRequest.ReelSpin
        {
            return new SpinRequest.ReelSpin();
        }

        protected createPayload(): SpinRequest.Payload
        {
            return new SpinRequest.Payload();
        }

        protected createForceData(): SpinRequest.ForceData
        {
            return new SpinRequest.ForceData();
        }

        protected fillForceData(forceData: SpinRequest.ForceData, forceResult: RS.Slots.Engine.ForceResult): void
        {
            forceData.reelSpin = [];
            if (forceResult.multiStopIndices != null)
            {
                for (const spin in forceResult.multiStopIndices)
                {
                    forceData.reelSpin[spin] = this.createReelSpin();
                    forceData.reelSpin[spin].stopIndices = [...forceResult.multiStopIndices[spin]];
                    if (forceResult.reelSetIndex != null) { forceData.reelSpin[spin].bandsetIndex = forceResult.reelSetIndex; }
                }
            }
            else
            {
                forceData.reelSpin[0] = this.createReelSpin();
                if (forceResult.stopIndices != null) { forceData.reelSpin[0].stopIndices = [...forceResult.stopIndices]; }
                if (forceResult.reelSetIndex != null) { forceData.reelSpin[0].bandsetIndex = forceResult.reelSetIndex; }
            }
        }

        protected createResponse(): SpinResponse
        {
            return new SpinResponse(this);
        }

        protected onComplete(req: XMLHttpRequest)
        {
            return RS.Slots.IPlatform.get().networkResponseReceived(req);
        }

        protected getRequestHeaders(): RS.Map<string>
        {
            const headers = super.getRequestHeaders();
            FreeRoundsUtils.setRequestHeaders(this.userSettings.freeRoundsInfo, headers);
            return headers;
        }
    }

    export namespace SpinRequest
    {
        /* tslint:disable:member-ordering */

        @XML.Type("GameRequest")
        export class Payload extends Core.LogicRequest.Payload
        {
            @XML.Property({ path: "PaylineCount.count", type: XML.Number, ignoreIfNull: true })
            public paylineCount?: number;

            @XML.Property({ path: "FORCE", type: XML.Object })
            public forceData?: ForceData;
        }

        // @XML.Type("ReelSpin")
        export class ReelSpin implements XML.ISerialisableObject
        {
            @XML.Property({ path: ".stopIndices", type: XML.DelimitedArray(XML.Number, "|"), ignoreIfNull: true })
            public stopIndices?: number[];

            @XML.Property({ path: ".bandsetIndex", type: XML.Number, ignoreIfNull: true })
            public bandsetIndex?: number;

            public getTagName(): string { return "ReelSpin"; }
        }

        @XML.Type("FORCE")
        export class ForceData extends Core.LogicRequest.ForceData
        {
            @XML.Property({ path: "", type: XML.Array(XML.Object) })
            public reelSpin: ReelSpin[];
        }

        /* tslint:enable:member-ordering */
    }
}
