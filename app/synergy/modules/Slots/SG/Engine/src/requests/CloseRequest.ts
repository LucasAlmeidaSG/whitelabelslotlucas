namespace SG.GLM
{
    export class CloseRequest extends Core.CloseRequest
    {
        constructor(public readonly networkSettings: Core.Engine.NetworkSettings, public readonly userSettings: Engine.UserSettings)
        {
            super(networkSettings, userSettings);
        }

        protected onComplete(req: XMLHttpRequest)
        {
            return RS.Slots.IPlatform.get().networkResponseReceived(req);
        }

        protected getRequestHeaders(): RS.Map<string>
        {
            const headers = super.getRequestHeaders();
            FreeRoundsUtils.setRequestHeaders(this.userSettings.freeRoundsInfo, headers);
            return headers;
        }
    }
}
