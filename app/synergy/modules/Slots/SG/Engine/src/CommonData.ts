namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    @XML.Type("FREEROUNDS")
    export class FreeRounds
    {
        @XML.Property({ path: "CAMPAIGNID", type: XML.String })
        public campaignID: string;

        @XML.Property({ path: "ACTIVATIONID", type: XML.String })
        public activationID: string;

        @XML.Property({ path: "STATUS", type: XML.String })
        public status: string;

        @XML.Property({ path: "TYPE", type: XML.String })
        public type: string;
    }

    /**
     * Bet configuration information.
     * NOTE: do not use this to restrict standard bets; this is handled by the GLM.
     */
    @XML.Type("BetConfiguration")
    export class BetConfiguration
    {
        @XML.Property({ path: "MaxBetHiR", type: XML.Number, ignoreIfNull: true })
        public maxBigBet: number;
    }

    /**
     * TODO: remove this
     * @deprecated
     */
    export import Header = Core.Header;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import ReplayData = Core.ReplayData;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import CurrencyInformation = Core.CurrencyInformation;

    /**
     * TODO: remove this
     * @deprecated
     */
    export class AccountData extends Core.AccountData
    {
        @XML.Property({ path: "BetConfiguration", type: XML.Object, ignoreIfNull: true })
        public betConfig?: BetConfiguration;
    }

    /**
     * TODO: remove this
     * @deprecated
     */
    export import Balance = Core.Balance;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import Error = Core.Error;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import MapItem = Core.MapItem;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import PayloadItem = Core.PayloadItem;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import Payload = Core.Payload;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import errorResponse = Core.errorResponse;
}
