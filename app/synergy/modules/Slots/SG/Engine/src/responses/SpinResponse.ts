namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    /*
    <GameResponse type="Logic">
        <Header sessionID="Phaa/gibeD2orcQKfny2ZGGaSnI+IAVAta0B6viWzJYw06lRBbmWS9+A3HgQVwld/aEY31msl/a2KbnMh7ZP2eQJfDYzuYmM6AgOj1NfwVg=" ccyCode="" deciSep="." thousandSep="," lang="en_gb" gameID="20232" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
        <AccountData></AccountData>
        <Balances>
            <Balance name="CASH_BALANCE" value="112500"/>
        </Balances>
        <GameResult stake="50000" stakePerLine="2500" paylineCount="20" totalWin="62500" betID="">
            <ReelResults numSpins="1">
                <ReelSpin spinIndex="0" reelsetIndex="0" winCountPL="2" winCountSC="0" spinWins="62500" freeSpin="N" bonusAwarded="N">
                    <ReelStops>15|51|4|14|49</ReelStops>
                    <PaylineWin index="4" winVal="12500" awardIndex="2" awardTableIndex="0">2|6|10</PaylineWin>
                    <PaylineWin index="14" winVal="50000" awardIndex="11" awardTableIndex="0">7|10|11</PaylineWin>
                </ReelSpin>
            </ReelResults>
            <BGInfo totalWagerWin="62500" bgWinnings="62500" baseGameSpinsRemaining="0" isBigBet="0" isMaxWin="0"/>
        </GameResult>
    </GameResponse>
    */

    /* tslint:disable:member-ordering */

    @XML.Type("PaylineWin")
    export class PaylineWin
    {
        @XML.Property({ path: ".index", type: XML.Number })
        public index: number;

        @XML.Property({ path: ".winVal", type: XML.Number })
        public winVal: number;

        @XML.Property({ path: ".awardIndex", type: XML.Number })
        public awardIndex: number;

        @XML.Property({ path: ".awardTableIndex", type: XML.Number })
        public awardTableIndex: number;

        @XML.Property({ path: "", type: XML.DelimitedArray(XML.Number, "|") })
        public positions: number[];
    }

    @XML.Type("ScatterWin")
    export class ScatterWin
    {
        @XML.Property({ path: ".winVal", type: XML.Number })
        public winVal: number;

        @XML.Property({ path: ".awardIndex", type: XML.Number })
        public awardIndex: number;

        @XML.Property({ path: "", type: XML.DelimitedArray(XML.Number, "|") })
        public positions: number[];
    }

    @XML.Type("ReelSpin")
    export class ReelSpin
    {
        @XML.Property({ path: ".spinIndex", type: XML.Number })
        public spinIndex: number;

        @XML.Property({ path: ".reelsetIndex", type: XML.Number })
        public reelsetIndex: number;

        @XML.Property({ path: ".winCountPL", type: XML.Number })
        public winCountPL: number;

        @XML.Property({ path: ".winCountSC", type: XML.Number })
        public winCountSC: number;

        @XML.Property({ path: ".spinWins", type: XML.Number })
        public spinWins: number;

        @XML.Property({ path: ".freeSpin", type: XML.Boolean })
        public freeSpin: boolean;

        @XML.Property({ path: ".bonusAwarded", type: XML.Boolean })
        public bonusAwarded: boolean;

        @XML.Property({ path: "ReelStops", type: XML.DelimitedArray(XML.Number, "|") })
        public reelStops: number[];

        @XML.Property({ path: "", type: XML.Array(XML.Object, "PaylineWin") })
        public paylineWins: PaylineWin[];

        @XML.Property({ path: "", type: XML.Array(XML.Object, "ScatterWin") })
        public scatterWins: ScatterWin[];
    }

    @XML.Type("BGInfo")
    export class BGInfo
    {
        @XML.Property({ path: ".totalWagerWin", type: XML.Number })
        public totalWagerWin: number;

        @XML.Property({ path: ".bgWinnings", type: XML.Number })
        public bgWinnings: number;

        @XML.Property({ path: ".baseGameSpinsRemaining", type: XML.Number })
        public baseGameSpinsRemaining: number;

        @XML.Property({ path: ".isBigBet", type: XML.Boolean })
        public isBigBet: boolean;

        @XML.Property({ path: ".isMaxWin", type: XML.Boolean })
        public isMaxWin: boolean;
    }

    @XML.Type("FSInfo")
    export class FSInfo
    {
        @XML.Property({ path: ".fsWinnings", type: XML.Number })
        public fsWinnings: number;

        @XML.Property({ path: ".isMaxWin", type: XML.Boolean })
        public isMaxWin: boolean;

        @XML.Property({ path: ".freeSpinsTotal", type: XML.Number })
        public freeSpinsTotal?: number;

        @XML.Property({ path: ".freeSpinNumber", type: XML.Number })
        public freeSpinNumber?: number;

        @XML.Property({ path: ".spinsUsed", type: XML.Number })
        public spinsUsed?: number;

        @XML.Property({ path: ".spinsRemaining", type: XML.Number })
        public spinsRemaining?: number;

        @XML.Property({ path: ".extraSpinsAwarded", type: XML.Number })
        public extraSpinsAwarded?: number;
    }

    export class GameResult extends SG.GLM.Core.GameResult
    {
        @XML.Property({ path: ".stakePerLine", type: XML.Number })
        public stakePerLine: number;

        @XML.Property({ path: ".paylineCount", type: XML.Number })
        public paylineCount: number;

        @XML.Property({ path: "ReelResults", type: XML.Array(XML.Object) })
        public reelSpins: ReelSpin[];

        @XML.Property({ path: "BGInfo", type: XML.Object })
        public bgInfo: BGInfo;

        @XML.Property({ path: "FSInfo", type: XML.Object })
        public fsInfo?: FSInfo;

        @XML.Property({ path: "SymbolsInView", type: XML.DelimitedArray(XML.DelimitedArray(XML.Number, ","), "|") })
        public symbolsInView?: number[][];
    }

    export class SpinResponse extends SG.GLM.Core.LogicResponse
    {
        public constructor(public readonly request: SpinRequest) { super(request); }

        @XML.Property({ path: "GameResult", type: XML.ExplicitObject(GameResult) })
        public gameResult: GameResult;

        @XML.Property({ path: "AccountData", type: XML.ExplicitObject(AccountData) })
        public accountData?: AccountData;
    }

    /* tslint:enable:member-ordering */
}
