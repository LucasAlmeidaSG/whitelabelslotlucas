namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    /* tslint:disable:member-ordering */

    export class CloseResponse extends Core.CloseResponse
    {
        public constructor(public readonly request: CloseRequest) { super(request); }

        @XML.Property({ path: "AccountData", type: XML.ExplicitObject(AccountData) })
        public accountData?: AccountData;
    }

    /* tslint:enable:member-ordering */
}
