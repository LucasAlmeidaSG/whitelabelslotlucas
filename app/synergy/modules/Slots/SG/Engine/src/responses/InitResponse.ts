namespace SG.GLM
{
    import XML = RS.Serialisation.XML;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import GameInfo = SG.GLM.Core.GameInfo;

    /*
    <GameResponse type="Init">
        <Header sessionID="Tfxz7fWLLbVKUS9mer9rC/yfzVG2EU8b6EIUzJer56jCVialR8bvCq5E70Zbt3sj" ccyCode="" deciSep="." thousandSep="," lang="en_gb" gameID="20232" versionID="1_0" fullVersionID="1.0.-1" isRecovering="N"/>
        <AccountData>
            <CurrencyInformation>
                <Currency>EUR</Currency>
                <ExchangeRate>1.0000000000</ExchangeRate>
                <CurrencySymbol>€</CurrencySymbol>
                <Grouping>3</Grouping>
                <GroupingSeparator>,</GroupingSeparator>
                <FracDigits>2</FracDigits>
                <DecimalSeparator>.</DecimalSeparator>
                <CurrencyCodeSpacing>1</CurrencyCodeSpacing>
                <CodeBeforeAmount>true</CodeBeforeAmount>
                <CurrencySymbolSpacing>0</CurrencySymbolSpacing>
                <SymbolBeforeAmount>true</SymbolBeforeAmount>
                <CurrencyMultiplier>1</CurrencyMultiplier>
            </CurrencyInformation>
            <CurrencyMultiplier>1</CurrencyMultiplier>
            <Payload>
                <PayloadItem request="init">
                    <Payload />
                </PayloadItem>
            </Payload>
            <BetConfiguration />
        </AccountData>

        <Balances>
            <Balance name="CASH_BALANCE" value="100000"/>
        </Balances>
        <Stakes count="15" defaultIndex="4" type="0">20|40|60|80|100|200|300|400|500|1000|2000|5000|10000|20000|50000</Stakes>
        <AwardsInfo awardTableIndex="0" awardCount="30" maxWin="25000000">
            <Award index="0">8|2|2.000000</Award>
            ...
            <Award index="29">0|3|0.000000</Award>
        </AwardsInfo>
        <Paylines gameMode="0">
            <PaylineInfo paylineCount="20" default="19">
                <Payline index="0" selectable="N">5|6|7|8|9</Payline>
                ...
                <Payline index="19" selectable="Y">0|6|7|8|9</Payline>
            </PaylineInfo>
        </Paylines>
        <ReelInfo numReelSets="6">
            <ReelSet reelSetIndex="0" numReels="5" featIndex="0">
                <Reel reelIndex="0" length="58">9|8|7|5|6|8|4|2|5|6|3|7|3|5|4|9|0|2|8|3|3|5|7|5|0|9|5|6|4|5|8|7|5|8|6|1|1|1|1|9|7|5|4|2|5|3|7|5|2|6|4|9|0|2|8|6|4|5</Reel>
                <Reel reelIndex="1" length="52">3|5|2|8|2|6|5|8|7|6|4|3|8|5|3|8|4|7|6|8|4|5|0|8|4|3|6|2|8|2|6|8|5|7|2|8|7|2|3|8|3|7|4|8|7|1|1|1|8|4|6|5</Reel>
                <Reel reelIndex="2" length="63">9|7|4|5|3|9|0|4|7|2|6|7|8|6|7|3|5|4|7|5|2|7|5|4|6|3|4|9|2|8|1|1|1|1|3|2|7|5|3|6|0|9|7|4|8|7|3|6|8|2|7|4|3|8|7|5|8|3|4|7|6|2|3</Reel>
                <Reel reelIndex="3" length="44">3|8|6|2|7|6|0|8|7|6|8|7|4|5|7|6|4|5|0|8|4|3|5|6|8|5|7|2|6|5|2|3|6|3|5|8|6|1|1|1|4|6|7|0</Reel>
                <Reel reelIndex="4" length="52">9|6|7|8|5|6|4|2|8|7|3|5|3|4|2|6|3|3|7|8|6|0|8|5|4|6|8|7|5|6|1|1|1|1|4|2|3|7|6|2|8|4|9|0|2|6|3|7|5|4|6|5</Reel>
            </ReelSet>
            ...
            <ReelSet reelSetIndex="5" numReels="5" featIndex="0">
                <Reel reelIndex="0" length="36">7|2|6|4|5|8|1|4|3|5|7|2|8|7|6|8|3|5|8|7|4|3|5|7|2|8|7|6|8|3|5|8|6|4|5|7</Reel>
                <Reel reelIndex="1" length="25">7|6|2|6|0|4|1|5|6|3|8|2|5|8|0|7|5|6|4|5|3|6|8|4|6</Reel>
                <Reel reelIndex="2" length="33">4|8|3|6|0|8|2|7|5|1|4|7|6|5|7|0|8|2|6|3|7|5|8|3|7|6|2|7|5|8|6|7|5</Reel>
                <Reel reelIndex="3" length="25">7|6|2|6|0|4|1|5|6|3|8|2|5|8|0|7|5|6|4|5|3|6|8|4|6</Reel>
                <Reel reelIndex="4" length="36">7|2|6|4|5|8|1|4|3|5|7|2|8|7|6|8|3|5|8|7|4|3|5|7|2|8|7|6|8|3|5|8|6|4|5|7</Reel>
            </ReelSet>
        </ReelInfo>
        <GameVariantInfo lowRtp="95.57" highRtp="95.57" bigBetRtp="95.57" bigBetDisabled="1"/>
    </GameResponse>
    */

    /* tslint:disable:member-ordering */

    @XML.Type("Award")
    export class Award
    {
        @XML.Property({ path: ".index", type: XML.Number })
        public index: number;

        @XML.Property({ path: "", type: XML.DelimitedArray(XML.Number, "|") })
        public data: number[];
    }

    @XML.Type("AwardsInfo")
    export class AwardTable
    {
        @XML.Property({ path: ".awardTableIndex", type: XML.Number })
        public index: number;

        @XML.Property({ path: "", type: XML.Array(XML.Object) })
        public awards: Award[];

        @XML.Property({ path: ".maxWin", type: XML.Number })
        public maxWin: number;
    }

    @XML.Type("JackpotInfo")
    export class JackpotInfo
    {
        @XML.Property({ path: ".names", type: XML.DelimitedArray(XML.String, "|") })
        public names: string[];

        @XML.Property({ path: ".values", type: XML.DelimitedArray(XML.Number, "|") })
        public values: number[];
    }

    @XML.Type("Payline")
    export class Payline
    {
        @XML.Property({ path: ".index", type: XML.Number })
        public index: number;

        @XML.Property({ path: ".selectable", type: XML.Boolean })
        public selectable: boolean;

        @XML.Property({ path: "", type: XML.DelimitedArray(XML.Number, "|") })
        public positions: number[];
    }

    @XML.Type("Paylines")
    export class PaylinesTable
    {
        @XML.Property({ path: ".gameMode", type: XML.Number })
        public gamemode?: number;

        @XML.Property({ path: "PaylineInfo.default", type: XML.Number })
        public default?: number;

        @XML.Property({ path: "PaylineInfo.paylineCount", type: XML.Number })
        public paylineCount: number;

        @XML.Property({ path: "PaylineInfo", type: XML.Array(XML.Object) })
        public paylines: Payline[];
    }

    @XML.Type("ReelSet")
    export class ReelSet
    {
        @XML.Property({ path: ".reelSetIndex", type: XML.Number })
        public reelSetIndex: number;

        @XML.Property({ path: ".featIndex", type: XML.Number })
        public featureIndex: number;

        @XML.Property({ path: "", type: XML.Array(XML.DelimitedArray(XML.Number, "|"), "Reel") })
        public reels: number[][];
    }

    export class GameVariantInfo extends SG.GLM.Core.GameVariantInfo
    {
        @XML.Property({ path: ".bigBetRtp", type: XML.Number })
        public bigBetRTP: number;

        @XML.Property({ path: ".bigBetDisabled", type: XML.Boolean })
        public bigBetDisabled: boolean;
    }

    export class InitResponse extends SG.GLM.Core.InitResponse
    {
        public constructor(public readonly request: InitRequest) { super(request); }

        @XML.Property({ path: "", type: XML.Array(XML.Object, "AwardsInfo") })
        public awardTables: AwardTable[];

        @XML.Property({ path: "JackpotInfo", type: XML.Object })
        public jackpotInfo?: JackpotInfo;

        @XML.Property({ path: "", type: XML.Array(XML.Object, "Paylines") })
        public paylineTables: PaylinesTable[];

        @XML.Property({ path: "ReelInfo", type: XML.Array(XML.Object, "ReelSet") })
        public reelSets: ReelSet[];

        @XML.Property({ path: "GameVariantInfo", type: XML.ExplicitObject(GameVariantInfo) })
        public gameVariantInfo?: GameVariantInfo;

        @XML.Property({ path: "AccountData", type: XML.ExplicitObject(AccountData) })
        public accountData?: AccountData;
    }

    /* tslint:enable:member-ordering */
}
