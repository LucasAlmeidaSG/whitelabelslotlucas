namespace SG.GLM
{
    // const balanceNameMap: RS.Map<string> = {};
    // balanceNameMap["CASH_BALANCE"] = RS.Models.Balances.Cash;
    // balanceNameMap["BONUS_BALANCE"] = RS.Models.Balances.Promotional;
    //
    // const primaryBalanceName = "CASH_BALANCE";
    // const GREGEnabled = RS.URL.getParameterAsBool("greg");

    /**
     * Encapsulates a slots engine interface for a GLM.
     */
    export class Engine<TModels extends RS.Slots.Models = RS.Slots.Models, TForceResult extends RS.Slots.Engine.ForceResult = RS.Slots.Engine.ForceResult> extends SG.GLM.Core.Engine<TModels, TForceResult> implements RS.Slots.Engine.IEngine<TModels, Engine.Settings, Core.Engine.ReplayData, TForceResult>
    {
        /** Published when a Spin response has been received and fully processed. */
        public readonly onSpinResponseReceived = RS.createEvent<RS.Engine.IResponse>();

        /** Gets if this engine has reel sets. */
        public readonly hasReelSets: boolean = true;

        protected _isFreeSpins: boolean = false;
        protected _awardTables: AwardTable[];

        protected _userSettings: Engine.UserSettings | null;

        @RS.AutoDispose
        private readonly __onFreeRoundsInfoUpdated: RS.IDisposable;

        public constructor(public readonly settings: Engine.Settings, public readonly models: TModels)
        {
            super(settings, models);

            const platform = RS.IPlatform.get();
            this._userSettings.freeRoundsInfo = platform instanceof Environment.Platform ? platform.freeRoundsInfo.value : null;
            if (platform instanceof Environment.Platform)
            {
                this._userSettings.freeRoundsInfo = platform.freeRoundsInfo.value;
                this.__onFreeRoundsInfoUpdated = platform.freeRoundsInfo.onChanged((info) =>
                {
                    this._userSettings.freeRoundsInfo = info;
                });
            }
        }

        /**
         * Rigs the next spin request.
         */
        public force(forceResults: RS.OneOrMany<TForceResult>)
        {
            if (!RS.Is.array(forceResults))
            {
                forceResults = [forceResults];
            }

            this._force.length = 0;
            for (const forceResult of forceResults)
            {
                const force = RS.Util.clone(forceResult, true);
                if (force.stopIndices != null)
                {
                    const stops = [...force.stopIndices];
                    this.slotStopsToEngineStops(stops, this.settings.rowCount);
                    force.stopIndices = stops;
                }
                else if (force.multiStopIndices != null)
                {
                    for (const index in force.multiStopIndices)
                    {
                        const stops = [...force.multiStopIndices[index]];
                        this.slotStopsToEngineStops(stops, this.settings.rowCount);
                        force.multiStopIndices[index] = stops;
                    }
                }
                this._force.push(force);
            }
        }

        public async spin(payload: RS.Slots.Engine.ISpinRequest.Payload): Promise<SpinResponse>
        {
            // Reset spin results
            this.clearResultsModel(this.models);

            const response = await this.logic(payload) as SpinResponse;
            if (!response.isError)
            {
                this.parseGame(response.gameResult);
                return response;
            }

            this.onSpinResponseReceived.publish(response);
            return response;
        }

        protected getSessionInfo(): RS.IPlatform.SessionInfo
        {
            return RS.Slots.IPlatform.get().sessionInfo;
        }

        protected createGREGData(networkResponse: string, response: SpinResponse, totalBet: number)
        {
            // New wager, create new GREG data
            if (this._currentGREGData == null)
            {
                const ID = response.gameResult.betID ? response.gameResult.betID : this._GREGDataCounter;
                this._currentGREGData =
                {
                    id: ID,
                    networkResponses: [],
                    initResponse: this._initResponse,
                    stake: totalBet,
                    win: 0
                }
                this._GREGDataCounter++;
            }

            // Update win amount (some wagers last multiple spins, so total win amount might change)
            this._currentGREGData.win = this.models.spinResults.map((sr) => sr.win.accumulatedWin).reduce((a, b) => a + b);

            // Add current spin response to the list of responses for the current wager
            this._currentGREGData.networkResponses.push(networkResponse);

            // End of wager, add current GREG data to the list
            if (this.shouldMakeCloseRequest())
            {
                this._GREGData.push(this._currentGREGData);
                if (this._GREGData.length > this._maxGREGCount)
                {
                    this._GREGData.shift(); // Remove old GREG data objects when there are too many
                }
                this._currentGREGData = null;
            }
        }

        protected fillRequestPayload(payload: RS.Slots.Engine.ISpinRequest.Payload): SpinRequestPayload
        {
            const totalBet = RS.Slots.Models.Stake.toTotal(this.models.stake, payload.betAmount);
            totalBet.value /= this._currencyMultiplier;
            return {
                currencyMultiplier: this._userSettings.currencyMultiplier,
                betAmount: totalBet,
                paylineCount: payload.paylineCount,
                forceResult: this._force.shift(),
                replay: this._replayEngine.replay
            }
        }

        /**
         * Handles a successful init request
         */
        protected handleInitSuccess(response: InitResponse): InitResponse
        {
            const superResponse = super.handleInitSuccess(response) as InitResponse;

            this.initReelSets(response);
            this.initSymbolPayouts(response);
            this.initWinLines(response);
            this.initDefaultReels();
            this.initStakePaylineOptions(response);
            this.initJackpots(response);

            // Disable big bet if limit <= 0
            if (response.accountData && response.accountData.betConfig && response.accountData.betConfig.maxBigBet != null && response.accountData.betConfig.maxBigBet <= 0)
            {
                this.models.config.bigBetEnabled = false;
            }

            return superResponse;
        }

        /**
         * Init config reelSets from init request response.
         */
        protected initReelSets(response: InitResponse)
        {
            const reelSets = response.reelSets;
            // Parse reel sets
            this.models.config.reelSets = [];
            for (const reelSet of reelSets)
            {
                const modelReelSet = new RS.Slots.ReelSet();
                modelReelSet.type = RS.Slots.ReelSet.Type.Variable;
                modelReelSet.reelCount = reelSet.reels.length;
                for (let reelIndex = 0, reelCnt = reelSet.reels.length; reelIndex < reelCnt; ++reelIndex)
                {
                    modelReelSet.setReel(reelIndex, reelSet.reels[reelIndex]);
                }
                this.models.config.reelSets[reelSet.reelSetIndex] = modelReelSet;
            }
        }

        /**
         * Init config symbolPayouts from init request response.
         */
        protected initSymbolPayouts(response: InitResponse)
        {
            // Parse symbol payouts
            const awardTables = response.awardTables;
            this._awardTables = awardTables;
            this.models.config.symbolPayouts = [];
            const primaryAwardTable = awardTables[0];
            this.models.config.maxWinAmount = primaryAwardTable.maxWin;
            this.models.config.symbolPayoutTables = [];
            for (const table of awardTables)
            {
                const payoutTable: number[][] = [];
                for (const award of table.awards)
                {
                    const [symbolID, symbolCount, winAmount] = award.data;

                    const payoutsArr = payoutTable[symbolID | 0] || (payoutTable[symbolID | 0] = []);
                    payoutsArr[symbolCount | 0] = winAmount || 0;
                }

                this.models.config.symbolPayoutTables.push(payoutTable);
            }
            this.models.config.symbolPayouts = this.models.config.symbolPayoutTables[0];
            this.models.config.symbolNames = this.models.config.symbolPayouts.map((arr, i) => RS.Util.formatNumber(i, "S0"));
        }

        /**
         * Init config winlines from init request response.
         */
        protected initWinLines(response: InitResponse)
        {
            const reelCount = this.models.config.reelSets[0].reelCount;
            const rowCount = this.settings.rowCount;
            const paylineTables = response.paylineTables;

            // Parse winlines
            this.models.config.extraWinlines = [];
            for (let tableID = 0; tableID < paylineTables.length; tableID++)
            {
                const paylineTable = paylineTables[tableID];

                const tableKey = paylineTable.gamemode == null ? tableID : paylineTable.gamemode;
                const paylines: RS.Slots.ReelSetPosition[][] = this.models.config.extraWinlines[tableKey] = [];
                for (let paylineID = 0, l = paylineTable.paylines.length; paylineID < l; ++paylineID)
                {
                    const srcPayline = paylineTable.paylines[paylineID];
                    const dstPayline = srcPayline.positions.map((wi) => this.positionFromWindowIndex(reelCount, wi));
                    const borderTouch = this.getBorderTouch(dstPayline, reelCount, rowCount);
                    if (borderTouch.right && !borderTouch.left)
                    {
                        this.sortRightToLeft(dstPayline);
                    }
                    else
                    {
                        this.sortLeftToRight(dstPayline);
                    }
                    paylines[srcPayline.index] = dstPayline;
                }
            }
            this.models.config.winlines = this.models.config.extraWinlines[0];
        }

        /**
         * Init config defaultReels from init request response.
         */
        protected initDefaultReels()
        {
            const rowCount = this.settings.rowCount;

            const defaultReelSetIndex = 0;
            const defaultReelSet = this.models.config.reelSets[defaultReelSetIndex];
            const defaultStopIndices = new Array<number>(defaultReelSet.reelCount);

            for (let i = 0, l = defaultStopIndices.length; i < l; ++i)
            {
                defaultStopIndices[i] = 0;
            }

            this.models.config.defaultReels =
            {
                currentReelSetIndex: defaultReelSetIndex,
                stopIndices: defaultStopIndices,
                symbolsInView: this.sampleReelSet(defaultReelSetIndex, defaultStopIndices, rowCount)
            };
        }

        /**
         * Init stake betOptions from init request response.
         */
        protected initStakes(response: InitResponse)
        {
            // Parse stakes
            this.models.stake.betOptions = response.stakes.map((value) =>
            {
                return {
                    type: RS.Slots.Models.Stake.Type.Total,
                    value
                };
            });

            this.models.stake.defaultBetIndex = response.defaultStakeIndex;
            this.models.stake.currentBetIndex = response.defaultStakeIndex;
            this.models.stake.paylineOptions = [];
        }

        /**
         * Init stake paylineOptions from init request response.
         */
        protected initStakePaylineOptions(response: InitResponse)
        {
            const paylineTables = response.paylineTables;
            // Anyways Slots dont have payline information
            if (paylineTables.length > 0)
            {
                for (const payline of paylineTables[0].paylines)
                {
                    if (payline.selectable)
                    {
                        this.models.stake.paylineOptions.push(payline.index + 1);
                    }
                }

                if (this.models.stake.paylineOptions.length > 0)
                {
                    this.models.config.selectablePaylines = true;
                }
                else
                {
                    const paylineCount = paylineTables[0].default || paylineTables[0].paylineCount || this.models.config.winlines.length;
                    this.models.stake.paylineOptions.push(paylineCount);
                    this.models.config.selectablePaylines = false;
                }
            }

            this.models.stake.defaultPaylineIndex = 0;
            this.models.stake.currentPaylineIndex = 0;
        }

        /**
         * Init jackpots from init request response.
         */
        protected initJackpots(response: InitResponse)
        {
            // Configure jackpots.
            if (!response.jackpotInfo)
            {
                return;
            }

            const jackpots = response.jackpotInfo;
            for (let i: number = 0; i < jackpots.names.length; i++)
            {
                const name: string = jackpots.names[i];
                const value: number = jackpots.values[i];
                this.models.jackpots[name] = { value: value };
            }
        }

        /**
         * Converts the specified stop indices from GLM format (indexed at middle) to RSSlots format (indexed at top).
         * @param stopIndices
         * @param rowCount
         */
        protected engineStopsToSlotStops(stopIndices: number[], rowCount: number): void
        {
            const middleRow = (rowCount / 2) | 0;
            for (let i = 0, l = stopIndices.length; i < l; ++i)
            {
                stopIndices[i] -= middleRow;
            }
        }

        /**
         * Converts the specified stop indices from RSSlots format (indexed at top) to GLM format (indexed at middle).
         * @param stopIndices
         * @param rowCount
         */
        protected slotStopsToEngineStops(stopIndices: number[], rowCount: number): void
        {
            const middleRow = (rowCount / 2) | 0;
            for (let i = 0, l = stopIndices.length; i < l; ++i)
            {
                stopIndices[i] += middleRow;
            }
        }

        /**
         * Samples a window from the specified reel set.
         * @param reelSetIndex
         * @param stopIndices
         * @param rowCount
         */
        protected sampleReelSet(reelSetIndex: number, stopIndices: number | number[], rowCount: number): RS.Slots.ReelSet | null
        {
            const reelSet = this.models.config.reelSets[reelSetIndex];
            if (reelSet == null) { return null; }
            if (RS.Is.number(stopIndices))
            {
                const newStopIndices = new Array<number>(reelSet.reelCount);
                for (let i = 0, l = reelSet.reelCount; i < l; ++i)
                {
                    newStopIndices[i] = stopIndices;
                }
                stopIndices = newStopIndices;
            }
            return reelSet.extractSubset(stopIndices, rowCount);
        }

        /**
         * Sends a recovery spin request.
         * Async.
         */
        protected async recover()
        {
            this._isRecovering = true;
            const response = await this.spin({
                betAmount: this.models.stake.betOptions[this.models.stake.currentBetIndex],
                paylineCount: this.models.stake.paylineOptions[this.models.stake.currentPaylineIndex]
            });
            this._isRecovering = false;
            return response;
        }

        protected positionFromWindowIndex(reelCount: number, windowIndex: number): RS.Slots.ReelSetPosition
        {
            return { reelIndex: (windowIndex % reelCount) | 0, rowIndex: (windowIndex / reelCount) | 0 };
        }

        protected positionToWindowIndex(reelCount: number, position: RS.Slots.ReelSetPosition): number
        {
            return position.rowIndex * reelCount + position.reelIndex;
        }

        protected getBorderTouch(position: RS.Slots.ReelSetPosition, reelCount: number, rowCount: number): { left: boolean; top: boolean; right: boolean; bottom: boolean };

        protected getBorderTouch(positions: RS.Slots.ReelSetPosition[], reelCount: number, rowCount: number): { left: boolean; top: boolean; right: boolean; bottom: boolean };

        protected getBorderTouch(p1: RS.Slots.ReelSetPosition | RS.Slots.ReelSetPosition[], reelCount: number, rowCount: number): { left: boolean; top: boolean; right: boolean; bottom: boolean }
        {
            if (RS.Is.array(p1))
            {
                const result = { left: false, right: false, top: false, bottom: false };
                for (const pos of p1)
                {
                    const borderTouch = this.getBorderTouch(pos, reelCount, rowCount);
                    result.left = borderTouch.left || result.left;
                    result.right = borderTouch.right || result.right;
                    result.top = borderTouch.top || result.top;
                    result.bottom = borderTouch.bottom || result.bottom;
                }
                return result;
            }
            else
            {
                return {
                    left: p1.reelIndex === 0,
                    right: p1.reelIndex === reelCount - 1,
                    top: p1.rowIndex === 0,
                    bottom: p1.rowIndex === rowCount - 1
                };
            }
        }

        protected sortLeftToRight(positions: RS.Slots.ReelSetPosition[]): void { positions.sort((a, b) => a.reelIndex - b.reelIndex); }
        protected sortRightToLeft(positions: RS.Slots.ReelSetPosition[]): void { positions.sort((a, b) => b.reelIndex - a.reelIndex); }
        protected sortTopToBottom(positions: RS.Slots.ReelSetPosition[]): void { positions.sort((a, b) => a.rowIndex - b.rowIndex); }
        protected sortBottomToTop(positions: RS.Slots.ReelSetPosition[]): void { positions.sort((a, b) => b.rowIndex - a.rowIndex); }

        /**
         * Returns whether or not an actual close request should be sent.
         */
        protected shouldMakeCloseRequest(): boolean
        {
            if (this.replayEngine.isReplay)
            {
                return false;
            }

            if (!this.settings.closeRequest)
            {
                return false;
            }

            if (this.models.freeSpins.remaining > 0 && !this.models.state.isMaxWin)
            {
                // No close requests should be sent during free spins unless max win has been triggered.
                return false;
            }

            return true;
        }

        protected parseStake(data: GameResult)
        {
            this.models.stake.override = null;

            for (let i = 0, l = this.models.stake.betOptions.length; i < l; ++i)
            {
                const totalBet = RS.Slots.Models.Stake.toTotal(this.models.stake, this.models.stake.betOptions[i]);
                if (totalBet.value === data.stake)
                {
                    this.models.stake.currentBetIndex = i;
                    return;
                }

                const betPerLine = RS.Slots.Models.Stake.toPerLine(this.models.stake, this.models.stake.betOptions[i]);
                if (betPerLine.value === data.stakePerLine)
                {
                    this.models.stake.currentBetIndex = i;
                    return;
                }
            }

            // Bet index not found; could be recovery into spin with no longer available bet option, set override
            this.models.stake.override = { totalBet: data.stake, betPerLine: data.stakePerLine };

            // Set the stake index to the nearest one in our stake list
            const validIndices = Object.keys(this.models.stake.betOptions);
            const nearestIndex = RS.Find.closestToZero(validIndices, (index) => RS.Slots.Models.Stake.toTotal(this.models.stake, this.models.stake.betOptions[index]).value - data.stake);
            this.models.stake.currentBetIndex = parseInt(nearestIndex);

            if (this.models.config.selectablePaylines)
            {
                this.models.stake.currentPaylineIndex = this.models.stake.paylineOptions.indexOf(data.paylineCount);
                if (this.models.stake.currentPaylineIndex === -1)
                {
                    this.models.stake.currentPaylineIndex = this.models.stake.defaultPaylineIndex;
                    RS.Log.warn(`Game result has non selectable payline index`);
                }
            }
            else
            {
                this.models.stake.currentPaylineIndex = this.models.stake.defaultPaylineIndex;
            }
        }

        protected checkMaxWin(data: GameResult): boolean
        {
            return RS.IPlatform.get().maxWinMode !== RS.PlatformMaxWinMode.None && data.bgInfo.isMaxWin;
        }

        protected parseFreeSpins(fsInfo: FSInfo): void
        {
            this.models.freeSpins.used = (fsInfo.freeSpinNumber != null ? fsInfo.freeSpinNumber : fsInfo.spinsUsed) || 0;
            this.models.freeSpins.isFreeSpins = this.models.freeSpins.used > 0;
            this.models.freeSpins.remaining = (fsInfo.freeSpinsTotal != null ? (fsInfo.freeSpinsTotal - this.models.freeSpins.used) : fsInfo.spinsRemaining) || 0;
            this.models.freeSpins.winAmount = fsInfo.fsWinnings;
            this.models.freeSpins.extraSpinsAwarded = fsInfo.extraSpinsAwarded || 0;
        }

        /** @inheritdoc */
        protected parseGame(data: GameResult): void
        {
            this.parseGameResult(data);
            super.parseGame(data);
        }

        /**
         * To maintain backwards compatibility: most games would have overwritten this method
         * TODO: move contents into parseGame and remove this
         * @deprecated
         */
        protected parseGameResult(data: GameResult): void
        {
            this.parseStake(data);

            // Parse free spins
            RS.Slots.Models.FreeSpins.clear(this.models.freeSpins);
            if (data.fsInfo)
            {
                this.parseFreeSpins(data.fsInfo);
            }

            this.parseReelResults(data);
        }

        protected parseReelResults(data: GameResult): void
        {
            for (const reelSpin of data.reelSpins)
            {
                this.parseReelSpin(reelSpin, data.symbolsInView);
            }
        }

        protected allocateWins(data: GameResult): void
        {
            const resultsModel = this.getResultsModel(this.models);
            let accumulatedWin: number = data.bgInfo.totalWagerWin;
            for (let i: number = resultsModel.length - 1; i >= 0; i--)
            {
                const spinResult = resultsModel[i];
                spinResult.win.accumulatedWin = accumulatedWin;
                accumulatedWin -= spinResult.win.totalWin;
            }
        }

        protected parseReelSpin(reelSpin: ReelSpin, expectedSymbolsInView?: number[][]): void
        {
            const betPerLine = RS.Slots.Models.Stake.getCurrentBetAmount(this.models.stake, RS.Slots.Models.Stake.Type.PerLine);
            const stops = [...reelSpin.reelStops];
            this.engineStopsToSlotStops(stops, this.settings.rowCount);

            let symbolsInView;
            if (expectedSymbolsInView && this.settings.useGLMSymbolCheck)
            {
                symbolsInView = new RS.Slots.ReelSet(RS.Util.transpose2DArray(expectedSymbolsInView));
            }
            else
            {
                symbolsInView = this.sampleReelSet(reelSpin.reelsetIndex, stops, this.settings.rowCount);
            }

            const totalScatterWin: number = reelSpin.scatterWins.map((x) => x.winVal).reduce((x, y) => x + y, 0);
            const spinResult: RS.Slots.Models.SpinResult =
            {
                index: reelSpin.spinIndex,
                reels:
                {
                    currentReelSetIndex: reelSpin.reelsetIndex,
                    stopIndices: stops,
                    symbolsInView: symbolsInView
                },
                win:
                {
                    totalWin: reelSpin.spinWins + totalScatterWin,
                    accumulatedWin: 0,
                    winAmounts: { "total": reelSpin.spinWins, "scatter": totalScatterWin },
                    paylineWins: reelSpin.paylineWins.map((pl) =>
                    {
                        const baseWL = this.models.config.winlines[pl.index];
                        return {
                            lineIndex: pl.index,
                            reverse: false,
                            multiplier: (pl.winVal * this._currencyMultiplier) / betPerLine,
                            totalWin: pl.winVal * this._currencyMultiplier,
                            positions: baseWL.filter((pos) => pl.positions.indexOf(this.positionToWindowIndex(this.models.config.reelSets[reelSpin.reelsetIndex].reelCount, pos)) !== -1),
                            symbolID: this._awardTables[pl.awardTableIndex].awards[pl.awardIndex].data[0] | 0
                        };
                    }),
                    scatterWins: reelSpin.scatterWins.map((scat): RS.Slots.Models.ScatterWin =>
                    {
                        return {
                            totalWin: scat.winVal,
                            positions: scat.positions.map((row, reel) =>
                            {
                                return {
                                    reelIndex: reel,
                                    rowIndex: row
                                };
                            })
                        };
                    })
                },
                balanceBefore: null,
                balanceAfter: null
            };
            this.getResultsModel(this.models).push(spinResult);
        }

        /**
         * Gets whether or not the balance from the engine includes the wager win and
         * should therefore have the win subtracted from it to freeze the balance.
         */
        protected isEngineBalanceFrozen(gameResult: GameResult): boolean
        {
            // Balance is never frozen when not in real-money mode.
            if (this._isFreePlay) { return false; }

             // Balance is frozen during free rounds.
            const platform = RS.IPlatform.get();
            if (platform instanceof SG.Environment.Platform)
            {
                const freeRoundsInfo = platform.freeRoundsInfo.value;
                if (freeRoundsInfo && Environment.FreeRoundsInfo.isOpen(freeRoundsInfo)) { return true; }
            }

            // Balance not frozen when max win hit.
            if (this.models.state.isMaxWin) { return false; }

            // Balance frozen during freespins
            if (this.models.freeSpins.remaining > 0) { return true; }

            // Base game spin or last free spin; GLM balance has been updated
            return false;

        }

        /**
         * Updates the before and after balances on all spin results.
         */
        protected calculateBalances(gameResult: GameResult)
        {
            const endBalance = this.models.customer.finalBalance;
            const shouldSubtractWin: boolean = !this.isEngineBalanceFrozen(gameResult);

            const resultsModel = this.getResultsModel(this.models);

            let lastBalance = RS.Models.Balances.clone(endBalance);
            // Start at the final balance, and work backwards through each spin result
            for (let i = resultsModel.length - 1; i >= 0; --i)
            {
                const spinResult = resultsModel[i];
                spinResult.balanceBefore = lastBalance;
                spinResult.balanceAfter = lastBalance;

                if (shouldSubtractWin)
                {
                    let totalWin;
                    if (i == 0)
                    {
                        totalWin = gameResult.bgInfo.totalWagerWin;
                        spinResult.balanceBefore = RS.Models.Balances.clone(endBalance);
                    }
                    else
                    {
                        totalWin = spinResult.win.totalWin;
                        spinResult.balanceBefore = RS.Models.Balances.clone(lastBalance);
                    }

                    this.fudgeWinFromBalances(spinResult.balanceBefore, totalWin);
                    lastBalance = spinResult.balanceBefore;
                }
            }
        }

        /**
         * Returns whether or not the given spin result should result in a balance update.
         */
        protected shouldUpdateBalance(spinResult: RS.Slots.Models.SpinResult, gameResult: GameResult): boolean
        {
            // Free rounds does not update balance until the end.
            const platform = RS.IPlatform.get();
            if (platform instanceof SG.Environment.Platform)
            {
                const freeRoundsInfo = platform.freeRoundsInfo.value;
                if (freeRoundsInfo && Environment.FreeRoundsInfo.isOpen(freeRoundsInfo)) { return false; }
            }

            // Max win spin should always be the last spin.
            if (this.models.state.isMaxWin) { return true; }

            // Balance frozen until last spin.
            if (spinResult.index < this.models.spinResults.length - 1) { return false; }

            // Balance frozen during freespins.
            if (this.models.freeSpins.remaining > 0) { return false; }

            // Base game spin or last free spin; show final balance after
            return true;
        }

        /**
         * Freezes the balances so as to update only at the correct time.
         */
        protected freezeBalances(gameResult: GameResult): void
        {
            const resultsModel = this.getResultsModel(this.models);
            const startBalance = resultsModel[0].balanceBefore;
            for (let i = 0; i < resultsModel.length; i++)
            {
                const spinResult = resultsModel[i];

                if (i > 0)
                {
                    const prevSpinResult = resultsModel[i - 1];
                    spinResult.balanceBefore = prevSpinResult.balanceAfter;
                }

                if (!this.shouldUpdateBalance(spinResult, gameResult))
                {
                    spinResult.balanceAfter = spinResult.balanceBefore;
                }
            }

            this.models.customer.finalBalance = resultsModel[resultsModel.length - 1].balanceAfter;
        }

        protected parseGameVariant(gameVariant: GameVariantInfo)
        {
            super.parseGameVariant(gameVariant);
            this.models.config.bigBetEnabled = this.models.config.bigBetEnabled && !gameVariant.bigBetDisabled;
        }

        protected getResultsModel(models: TModels): RS.Slots.Models.SpinResults
        {
            return models.spinResults;
        }

        protected clearResultsModel(models: TModels)
        {
            RS.Slots.Models.SpinResults.clear(this.getResultsModel(models));
        }
    }

    export namespace Engine
    {

        /**
         * TODO: remove this
         * @deprecated
         */
        export import NetworkSettings = Core.Engine.NetworkSettings;

        export interface UserSettings extends Core.Engine.UserSettings
        {
            freeRoundsInfo?: Environment.FreeRoundsInfo;
        }

        /**
         * TODO: remove this
         * @deprecated
         */
        export import ReplayData = Core.Engine.ReplayData;

        export interface Settings extends SG.GLM.Core.Engine.Settings
        {
            initRequest: { new(networkSettings: Core.Engine.NetworkSettings, userSettings: Core.Engine.UserSettings): InitRequest; };
            // freeSpinRequest: { new(settings: NetworkSettings, userSettings: UserSettings): FreeSpinRequest; };
            spinRequest: { new(settings: Core.Engine.NetworkSettings, userSettings: Core.Engine.UserSettings): SpinRequest; };
            closeRequest?: { new(settings: Core.Engine.NetworkSettings, userSettings: Core.Engine.UserSettings): CloseRequest; };
            rowCount: number;
            /** Will use glm response <SymbolsInView> */
            useGLMSymbolCheck?: boolean;
        }
    }
}
