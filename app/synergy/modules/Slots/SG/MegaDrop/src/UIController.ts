namespace SG.UI.MegaDrop
{
    export class UIController extends SG.UI.UIController implements RS.Slots.MegaDrop.IUIController
    {
        public attachBackUIToContainer(
            container: RS.Flow.Container,
            showButtonArbiter: RS.Arbiter<boolean>,
            mobilePanelSettings?: SG.CommonUI.MobileButtonPanel.Settings,
            mobileMenuSettings?: SG.CommonUI.MobileMenuHolder.Settings)
        {
            if (mobilePanelSettings)
            {
                this.createMobileButtonPanel(mobilePanelSettings);
            }
            container.addChild(this._mobileButtonPanel, 0);
            if (mobileMenuSettings)
            {
                this.createMobileMenu(mobileMenuSettings);
            }
            container.addChild(this._mobileMenu, 0);
            this._mobileMenu.bindToObservables({ visible: showButtonArbiter });
            this._mobileButtonPanel.bindToObservables({ visible: showButtonArbiter });
        }

        protected createMobileMenu(settings: CommonUI.MobileMenuHolder.Settings)
        {
            if (this._mobileMenu)
            {
                this._mobileMenu.apply(settings);
            }
            else
            {
                this._mobileMenu = CommonUI.mobileMenuHolder.create(CommonUI.MobileMenuHolder.defaultSettings,
                {
                    menuData:
                    {
                        showAutoplayButton: RS.Slots.IPlatform.get().supportsAutoplay !== RS.Slots.PlatformAutoplaySupport.None,
                        paytableEnabledArbiter: this.settings.paytableEnabledArbiter
                    },
                    betMenuData: { spinners: this._mobileSpinners }
                });

                this._mobileMenu.locale = settings.locale;
                this._mobileMenu.currencyFormatter = settings.currencyFormatter;
                this._mobileMenu.bindToObservables({ chevronButtonVisible: this.settings.enableInteractionArbiter });
            }

            this._mobileButtonPanel.locale = this._initSettings.locale;
            this._mobileButtonPanel.currencyFormatter = settings.currencyFormatter;
            this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            this._mobileMenu.onAutoplayButtonClicked(this.handleAutoplayClicked);
            this._mobileMenu.onHelpButtonClicked(this.handlePaytableClicked);
            this._mobileMenu.onChevronButtonClicked(this.handleMobileChevronClicked);
            this._mobileMenu.onBetButtonClicked(this.handleBetClicked);
            this._mobileMenu.innerBetMenu.onAcceptClicked(this.handleAcceptMobileBet);
            this._mobileMenu.innerBetMenu.onCancelClicked(this.handleCancelMobileBet);
            if (this._initSettings.showBigBetButton) { this._mobileButtonPanel.bindToObservables({ bigBetButtonVisible: this.settings.enableInteractionArbiter }); }
       }
    }

    RS.Slots.MegaDrop.IUIController.register(UIController);
}