namespace SG.Environment
{
    @RS.HasCallbacks
    export class DummyAutoplayManager implements SGI.IAutoplayManager, RS.IDisposable
    {
        protected _isDisposed: boolean = false;
        protected _settings: DummyAutoplayManager.Settings;
        @RS.AutoDisposeOnSet protected _autoplayDialog: RS.Slots.Autoplay.Dialog | null = null;
        @RS.AutoDisposeOnSet protected _enableInteractionHandler: RS.IDisposable | null = null;

        protected _autoplayInProgress: boolean = false;
        protected _lossLimit: number = 0;
        protected _winLimit: number = 0;
        protected _currentTotalStake: number = 0;
        protected _runningLossAmount: number = 0;

        public get isDisposed() { return this._isDisposed; }

        public constructor(settings: DummyAutoplayManager.Settings)
        {
            this._settings = settings;
        }

        public initPartnerAutoplay(currentTotalStake: number, currentBalance: number, callback: (isEnabled: boolean) => void): void
        {
            this._currentTotalStake = currentTotalStake;
            callback(true);
        }

        public launchPartnerAutoplay(currentTotalStake: number, currentBalance: number, callback: (isEnabled: boolean) => void): void
        {
            this._autoplayDialog = RS.Slots.Autoplay.dialog.create(RS.Slots.Autoplay.Dialog.defaultSettings,
            {
                totalBetObservable: new RS.Observable(currentTotalStake),
                requireLossLimit: true
            }, this._settings.uiContainer);
            this._autoplayDialog.onStartClicked(this.handleAutoplayDialogStartClicked);
            this._autoplayDialog.onCancelClicked(this.handleAutoplayDialogCancelClicked);
            this._autoplayDialog.invalidateLayout();
            this._enableInteractionHandler = this._settings.enableInteractionArbiter.declare(false);
            this._currentTotalStake = currentTotalStake;
            callback(true);
        }

        public updateWinAndStake(win: number, stake: number): void
        {
            RS.Log.debug(`DummyAutoplayManager: updateWinAndStake(${win}, ${stake})`);
            if (this._autoplayInProgress)
            {
                if (win < stake)
                {
                    this._runningLossAmount += (stake - win);
                }
                const sgPlatform = RS.IPlatform.get() as SG.Environment.Platform;
                if (this._winLimit > 0 && win >= this._winLimit)
                {
                    RS.Log.debug(`DummyAutoplayManager: Terminated autoplay as win '${win}' exceeds single win limit of '${this._winLimit}'`);
                    sgPlatform.onAutoplayStopped.publish();
                }
                else if (this._lossLimit > 0 && this._runningLossAmount >= this._lossLimit)
                {
                    RS.Log.debug(`DummyAutoplayManager: Terminated autoplay as total loss '${this._runningLossAmount}' exceeds loss limit of '${this._lossLimit}'`);
                    sgPlatform.onAutoplayStopped.publish();
                }
            }
        }

        public updateAutoPlayStatus(status: string, remainingSpins: number): void
        {
            RS.Log.debug(`DummyAutoplayManager: updateAutoPlayStatus("${status}", ${remainingSpins})`);
            switch (status)
            {
                case "started":
                    this._runningLossAmount = 0;
                    this._autoplayInProgress = true;
                    break;
                case "progress":
                    break;
                case "stopped":
                    this._autoplayInProgress = false;
                    break;
            }
        }

        public updateOnBetChange(totalbet: number): void
        {
            RS.Log.debug(`DummyAutoplayManager: updateOnBetChange(${totalbet})`);
        }

        public dispose(): void
        {
            if (this._isDisposed) { return; }

            this._isDisposed = true;
        }

        @RS.Callback
        protected handleAutoplayDialogStartClicked(): void
        {
            this._winLimit = this._autoplayDialog.winLimit * this._currentTotalStake;
            this._lossLimit = this._autoplayDialog.lossLimit * this._currentTotalStake;
            const sgPlatform = RS.IPlatform.get() as SG.Environment.Platform;
            sgPlatform.onAutoplayStarted.publish(this._autoplayDialog.spinCount);
            this._autoplayDialog = null;
            this._enableInteractionHandler = null;
        }

        @RS.Callback
        protected handleAutoplayDialogCancelClicked(): void
        {
            this._autoplayDialog = null;
            this._enableInteractionHandler = null;
        }
    }

    export namespace DummyAutoplayManager
    {
        export interface Settings
        {
            uiContainer: RS.Flow.Container;
            enableInteractionArbiter: RS.IArbiter<boolean>;
        }
    }
}