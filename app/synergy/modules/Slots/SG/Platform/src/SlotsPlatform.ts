namespace SG.Environment
{
    enum WinUpdateState { None, NextTick, Now }

    @RS.HasCallbacks
    class SlotsPlatform implements RS.Slots.IPlatform
    {
        private _settings: RS.Slots.IPlatform.Settings;
        private _stateMachine: RS.ObservableStateMachine<RS.PlatformGameState>;
        private _balanceUpdateRequired: boolean;
        private _winUpdateRequired: WinUpdateState = WinUpdateState.None;
        private _didUpdateWinThisPlay: boolean;

        private _autoplayEnabled: boolean;
        private _autoplayManager: SGI.IAutoplayManager;
        private _autoplayController: RS.Slots.Autoplay.IController | null;
        private _autoplayStatusStarted: boolean;
        private _autoplayControllerStartedHandler: RS.IDisposable | null;
        private _autoplayControllerStoppedHandler: RS.IDisposable | null;

        @RS.AutoDisposeOnSet private _freeRoundsUpdateHandler: RS.IDisposable | null;
        @RS.AutoDisposeOnSet private _freeRoundsStakeLock: RS.IDisposable | null;
        @RS.AutoDisposeOnSet private _freeRoundsAutoplayLock: RS.IDisposable | null;
        @RS.AutoDisposeOnSet private _freeRoundsPaytableLock: RS.IDisposable | null;
        @RS.AutoDisposeOnSet private _freeRoundsResetPending: boolean = false;

        private get platform(): Platform { return RS.IPlatform.get() as Platform; }
        private get partnerAdapter(): SGI.IPartnerAdapter { return this.platform.adapter; }

        private get cashBalance() { return this._settings.balancesObservable.get(RS.Models.Balances.Cash) || 0; }
        private get freeBetsBalance() { return this._settings.balancesObservable.get(RS.Models.Balances.Promotional) || 0; }
        private get totalBalance() { return this._settings.balanceObservable.value; }

        public get sessionInfo(): RS.IPlatform.SessionInfo
        {
            const partnerAdapter = this.partnerAdapter;
            if (partnerAdapter == null) { return { userName: null, userID: null, sessionID: null, affiliateID: null }; }
            const sessionData = partnerAdapter.getSessionData();
            if (sessionData == null) { return { userName: null, userID: null, sessionID: null, affiliateID: null }; }
            return {
                userName: sessionData.getPlayerID(),
                userID: parseInt(sessionData.getPlayerID()),
                sessionID: sessionData.getSessionID(),
                affiliateID: sessionData.getContextID()
            };
        }

        public get supportsAutoplay()
        {
            if (this.partnerAdapter instanceof DummyPartnerAdapter)
            {
                return RS.Slots.PlatformAutoplaySupport.PlatformProvides;
            }
            else if (this._autoplayEnabled && this._autoplayController != null)
            {
                return RS.Slots.PlatformAutoplaySupport.PlatformProvides;
            }
            else
            {
                return RS.Slots.PlatformAutoplaySupport.None;
            }
        }

        public get autoplayController() { return this._autoplayController; }
        public set autoplayController(value)
        {
            if (this._autoplayControllerStartedHandler)
            {
                this._autoplayControllerStartedHandler.dispose();
                this._autoplayControllerStartedHandler = null;
            }
            if (this._autoplayControllerStoppedHandler)
            {
                this._autoplayControllerStoppedHandler.dispose();
                this._autoplayControllerStoppedHandler = null;
            }
            this._autoplayController = value;
            if (!this._autoplayController) { return; }
            this._autoplayControllerStartedHandler = this._autoplayController.onAutoplayStarted((num) =>
            {
                if (this._autoplayManager) { this._autoplayManager.updateAutoPlayStatus("started", num); }
                this._autoplayStatusStarted = true;
            });
            this._autoplayControllerStoppedHandler = this._autoplayController.onAutoplayStopped(() =>
            {
                if (this._autoplayManager) { this._autoplayManager.updateAutoPlayStatus("finished", 0); }
                this._autoplayStatusStarted = false;
            });
        }

        public init(settings: RS.Slots.IPlatform.Settings): void
        {
            this._settings = settings;
            this._settings.balanceObservable.onChanged(this.handleBalanceChanged);
            this._settings.balancesObservable.onChanged(this.handleNamedBalanceChanged);
            this._settings.winObservable.onChanged(this.handleWinAmountChanged);
            this._settings.stakeObservable.onChanged(this.handleStakeChanged);

            const missingItems = [];
            if (!this._settings.stakeChangeEnabledArbiter)
            {
                missingItems.push("stakeChangeEnabledArbiter");
            }
            if (!this._settings.paytableEnabledArbiter)
            {
                missingItems.push("paytableEnabledArbiter");
            }
            if (missingItems.length > 0)
            {
                RS.Log.warn(`[RS.Slots.IPlatform] Free Rounds will not work correctly because the following items are missing: ${missingItems.join(", ")}`);
            }

            this._stateMachine = new RS.ObservableStateMachine(settings.gameStateObservable);

            // Uninitialised -> not ready, we've received our first server data and can update the platform with the balance, win etc.
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.Uninitialised, RS.PlatformGameState.NotReady, () =>
            {
                this._winUpdateRequired = 0;
                this._balanceUpdateRequired = false;
                this.platform.handleBalanceDisplay(this.cashBalance, this.freeBetsBalance, this.totalBalance);
                this.platform.handleWinDisplay(this._settings.winObservable.value);
                this.partnerAdapter.updateBet(this._settings.stakeObservable.value);
            });

            // Not ready -> ready, standard startup routine
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.NotReady, RS.PlatformGameState.Ready, () =>
            {
                this.handleGameInitialised(false);
                this.partnerAdapter.gameReady();
                this._didUpdateWinThisPlay = false;
            });

            // Not ready -> in play, usually when resuming
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.NotReady, RS.PlatformGameState.InPlay, () =>
            {
                RS.Log.warn(`[RS.Slots.IPlatform] Legacy resuming transition: NotReady -> InPlay, Free Rounds will not work; ensure platformGameState set Resuming in custom Resuming states`);
                this.handleGameInitialised(true);
                this.partnerAdapter.gameReady();
                this.partnerAdapter.startedPlay();
                // OB PartnerAdapter clears win on startedPlay regardless of resuming state
                this.updateWin();
                this._didUpdateWinThisPlay = false;
            });

            // Not ready -> resuming
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.NotReady, RS.PlatformGameState.Resuming, () =>
            {
                this.handleGameInitialised(true);
                this.partnerAdapter.gameReady();
                this._didUpdateWinThisPlay = false;
            });

            // Resuming -> in play, called when dialog is accepted
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.Resuming, RS.PlatformGameState.InPlay, () =>
            {
                this.partnerAdapter.startedPlay();
                // OB PartnerAdapter clears win on startedPlay regardless of resuming state
                this.updateWin();
            });

            // Ready -> in play, usually when user clicks spin
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.Ready, RS.PlatformGameState.InPlay, () =>
            {
                this.partnerAdapter.startedPlay();
                this._didUpdateWinThisPlay = false;
            });

            // In play post win rendering -> ready, usually when win rendering is done and game switches back to idle
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.InPlayPostWinRendering, RS.PlatformGameState.Ready, this.onPlayFinished);

            // In play -> ready, usually when win rendering was skipped for some reason (maybe max win) and game switches back to idle
            this._stateMachine.setTransitionHandler(RS.PlatformGameState.InPlay, RS.PlatformGameState.Ready, this.onPlayFinished);

            this._autoplayEnabled = false;
            if (this.partnerAdapter instanceof DummyPartnerAdapter)
            {
                this._autoplayManager = new DummyAutoplayManager({
                    uiContainer: this._settings.uiContainer,
                    enableInteractionArbiter: this._settings.interactionEnabledArbiter
                });
            }
            else
            {
                this._autoplayManager = this.partnerAdapter.partnerAutoplayManager;
            }
            if (this._autoplayManager != null)
            {
                this._autoplayManager.initPartnerAutoplay(this._settings.stakeObservable.value, this._settings.balanceObservable.value, (autoplayEnabled) =>
                {
                    this._autoplayEnabled = autoplayEnabled;
                    if (autoplayEnabled)
                    {
                        this.initAutoplay();
                    }
                });
            }

            RS.Ticker.registerTickers(this);

            const { platform } = this;
            if (platform.freeRoundsInfo.value) { this.handleFreeRoundsUpdated(platform.freeRoundsInfo.value); }
            this._freeRoundsUpdateHandler = platform.freeRoundsInfo.onChanged(this.handleFreeRoundsUpdated);
        }

        public networkResponseReceived(request: XMLHttpRequest): void
        {
            if (request.status !== 200)
            {
                let callback: () => void;
                const lobbyNavService = this.partnerAdapter.getLobbyNavigationService();
                if (lobbyNavService && lobbyNavService.canGoToLobby())
                {
                    callback = () => this.partnerAdapter.goHome();
                }
                else
                {
                    callback = () => this.partnerAdapter.reload();
                }

                const locale = RS.IPlatform.get().locale;
                this.partnerAdapter.showError(Translations.Error.Title.get(locale), Translations.Error.Generic.Message.get(locale), Translations.Error.Button.get(locale), callback);
                return;
            }

            // for whatever reason the PA decides to throw on a server error
            // let's not let it screw up the rest of our stack frame
            try
            {
                this.partnerAdapter.receivedGameLogicResponse(request);
            }
            catch (err)
            {
                RS.Log.error(`Partner adapter error: ${err}`);
            }
        }

        public requestAutoplay(): void
        {
            if (this._freeRoundsStakeLock) { throw new Error("Cannot start autoplay whilst free rounds is active"); }
            this._autoplayManager.launchPartnerAutoplay(this._settings.stakeObservable.value, this._settings.balanceObservable.value, (isEnabled) => RS.Log.debug(`launchPartnerAutoplay: ${isEnabled}`));
        }

        public requestAutoplayStop(): void
        {
            RS.Log.debug(`requestAutoplayStop`);
            this._autoplayController.stop();
        }

        @RS.Callback
        protected onPlayFinished()
        {
            // Don't re-order this method unless explicitly told to

            if (!this._didUpdateWinThisPlay)
            {
                this.updateWin();
                this._didUpdateWinThisPlay = true;
            }

            // Shubham Halle: finishedPostGameAnimations() should be called after finishedPlay()
            this.partnerAdapter.finishedPlay();
            this.partnerAdapter.finishedPostGameAnimations();

            // Order copied from rainbowriches_prt
            this.updateAutoplayStakeAndWin();
        }

        protected initAutoplay(): void
        {
            const sgPlatform = this.platform;
            sgPlatform.onAutoplayStarted(this.handleAutoplayStarted);
            sgPlatform.onAutoplayStopped(this.handleAutoplayStopped);

            this._settings.stakeObservable.onChanged((newStake) => this._autoplayManager.updateOnBetChange(newStake));
            // this._settings.winObservable.onChanged(newWin => this._autoplayManager.updateWinAndStake(newWin, this._settings.stakeObservable.value));
            this._settings.autoplaysRemainingObservable.onChanged((newAutoplaysRemaining) =>
            {
                if (!this._autoplayStatusStarted) { return; }
                this._autoplayManager.updateAutoPlayStatus("progress", newAutoplaysRemaining);
            });
        }

        @RS.Callback
        protected handleAutoplayStarted(autoplayCount: number): void
        {
            if (!this._autoplayController)
            {
                RS.Log.warn(`handleAutoplayStarted: no autoplay controller`);
                return;
            }
            this._autoplayController.start(autoplayCount);
        }

        @RS.Callback
        protected handleAutoplayStopped(): void
        {
            if (!this._autoplayController)
            {
                RS.Log.warn(`handleAutoplayStopped: no autoplay controller`);
                return;
            }
            this._autoplayController.stop();
        }

        protected handleGameInitialised(recovering: boolean)
        {
            this._winUpdateRequired = 0;
            this._balanceUpdateRequired = false;
            this.platform.handleGameInitialised(recovering, this.cashBalance, this.freeBetsBalance, this.totalBalance);
            this.platform.handleBalanceDisplay(this.cashBalance, this.freeBetsBalance, this.totalBalance);
            this.platform.handleWinDisplay(this._settings.winObservable.value);
            this.partnerAdapter.updateBet(this._settings.stakeObservable.value);
        }

        protected updateWin(): void
        {
            this.platform.handleWinDisplay(this._settings.winObservable.value);
        }

        protected updateAutoplayStakeAndWin(): void
        {
            if (this._autoplayEnabled)
            {
                this._autoplayManager.updateWinAndStake(this._settings.winObservable.value, this._settings.stakeObservable.value);
            }
        }

        @RS.Callback
        protected async handleFreeRoundsUpdated(info: FreeRoundsInfo | null): Promise<void>
        {
            if (!info || !Environment.FreeRoundsInfo.isOpen(info))
            {
                this.scheduleStakeReset();
                return;
            }

            switch (info.status)
            {
                case FreeRoundsInfo.Status.Completed:
                case FreeRoundsInfo.Status.NotStarted:
                case FreeRoundsInfo.Status.InProgress:
                {
                    if (info.totalBet != null)
                    {
                        try
                        {
                            await this._settings.wagerUpdateCallback({ type: RS.Slots.PlatformWagerUpdate.Type.TotalBet, totalBet: info.totalBet });
                        }
                        catch (e)
                        {
                            if (e instanceof RS.Slots.PlatformWagerUpdate.InvalidStakeError)
                            {
                                const locale = RS.IPlatform.get().locale;
                                const callback = () => this.partnerAdapter.reload();
                                this.partnerAdapter.showError(Translations.Error.Title.get(locale), Slots.Translations.Error.FreeRounds.InvalidStake.Message.get(locale), Translations.Error.Button.get(locale), callback);
                                return;
                            }

                            throw e;
                        }
                    }

                    if (this._settings.stakeChangeEnabledArbiter) { this._freeRoundsStakeLock = this._settings.stakeChangeEnabledArbiter.declare(false); }
                    if (this._settings.allowAutoplayArbiter) { this._freeRoundsAutoplayLock = this._settings.allowAutoplayArbiter.declare(false); }
                    if (this._settings.paytableEnabledArbiter) { this._freeRoundsPaytableLock = this._settings.paytableEnabledArbiter.declare(false); }
                    break;
                }
            }
        }

        protected async scheduleStakeReset()
        {
            if (this._freeRoundsResetPending) { return; }
            this._freeRoundsResetPending = true;
            await this._settings.gameStateObservable.for(RS.PlatformGameState.Ready);
            await this._settings.wagerUpdateCallback({ type: RS.Slots.PlatformWagerUpdate.Type.Reset, resetType: RS.Slots.PlatformWagerReset.Type.Default });
            this._freeRoundsStakeLock = null;
            this._freeRoundsAutoplayLock = null;
            this._freeRoundsPaytableLock = null;
            this._freeRoundsResetPending = false;

        }

        @RS.Callback
        protected handleBalanceChanged(newBalance: number): void
        {
            this._balanceUpdateRequired = true;
        }

        @RS.Callback
        protected handleNamedBalanceChanged(data: RS.ObservableMap.OnChangedData<number>): void
        {
            this._balanceUpdateRequired = true;
        }

        @RS.Callback
        protected handleStakeChanged(newStake: number): void
        {
            if (this._freeRoundsStakeLock)
            {
                // Validate free rounds stake
                const freeRoundsInfo = this.platform.freeRoundsInfo.value;
                if (freeRoundsInfo && Environment.FreeRoundsInfo.isOpen(freeRoundsInfo) && freeRoundsInfo.totalBet != null)
                {
                    const stake = freeRoundsInfo.totalBet;
                    if (newStake !== stake) { throw new Error(`[SlotsPlatform] Received wrong stake for Free Rounds`); }
                }
            }

            this.partnerAdapter.updateBet(newStake);
        }

        @RS.Tick({ kind: RS.Ticker.Kind.FixedStep, data: 100 })
        protected tick(): void
        {
            if (this._balanceUpdateRequired)
            {
                this._balanceUpdateRequired = false;
                this.platform.handleBalanceDisplay(this.cashBalance, this.freeBetsBalance, this.totalBalance);
            }
            if (this._winUpdateRequired === WinUpdateState.NextTick)
            {
                this._winUpdateRequired = WinUpdateState.Now;
            }
            else if (this._winUpdateRequired === WinUpdateState.Now)
            {
                this._winUpdateRequired = WinUpdateState.None;
                this.updateWin();
                this._didUpdateWinThisPlay = true;
            }
        }

        @RS.Callback
        protected handleWinAmountChanged(newWinAmount: number): void
        {
            this._winUpdateRequired = WinUpdateState.NextTick;
        }
    }

    RS.Slots.IPlatform.register(SlotsPlatform);
}
