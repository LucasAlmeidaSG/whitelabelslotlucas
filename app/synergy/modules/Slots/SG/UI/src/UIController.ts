namespace SG.UI
{
    interface SpinnerOptionState
    {
        properOptions: number[];
        temporaryOptions: number[] | null;
        temporaryOptionIndex: number | null;
    }

    @RS.HasCallbacks
    export class UIController implements RS.Slots.UI.IUIController
    {
        public settings: RS.Slots.UI.IUIController.Settings;

        /** Published when a spin has been requested by the user. */
        public get onSpinRequested() { return this._onSpinRequested.public; }
        protected readonly _onSpinRequested = RS.createSimpleEvent();

        /** Published when a skip has been requested by the user. */
        public get onSkipRequested() { return this._onSkipRequested.public; }
        protected readonly _onSkipRequested = RS.createSimpleEvent();

        /** Published when the user has changed an input option. In this case, value is the index into options rather than the value itself. */
        public get onInputOptionChanged() { return this._onInputOptionChanged.public; }
        protected readonly _onInputOptionChanged = RS.createEvent<RS.Slots.UI.IUIController.InputOptionChangedData>();

        /** Published when the paytable has been requested by the user. */
        public get onPaytableRequested() { return this._onPaytableRequested.public; }
        protected readonly _onPaytableRequested = RS.createSimpleEvent();

        /** Published when the help page has been requested by the user. */
        public get onHelpRequested() { return this._onHelpRequested.public; }
        protected readonly _onHelpRequested = RS.createSimpleEvent();

        /** Published when the settings control has been requested by the user. */
        public get onSettingsRequested() { return this._onSettingsRequested.public; }
        protected readonly _onSettingsRequested = RS.createSimpleEvent();

        /** Published when the big bet feature has been requested by the user. */
        public get onBigBetRequested() { return this._onBigBetRequested.public; }
        protected readonly _onBigBetRequested = RS.createSimpleEvent();

        /** Published when the autoplay feature has been requested by the user. */
        public get onAutoplayRequested() { return this._onAutoplayRequested.public; }
        protected readonly _onAutoplayRequested = RS.createSimpleEvent();

        /** Published when the autoplay feature has been requested by the user. */
        public get onAutoplayStopRequested() { return this._onAutoplayStopRequested.public; }
        protected readonly _onAutoplayStopRequested = RS.createSimpleEvent();

        /** Published when the Turbo feature has been requested by the user. */
        public get onTurboRequested() { return this._onTurboRequested.public; }
        protected readonly _onTurboRequested = RS.createSimpleEvent();

        /** Published when the Turbo feature has been requested to stop by the user. */
        public get onTurboStopRequested() { return this._onTurboStopRequested.public; }
        protected readonly _onTurboStopRequested = RS.createSimpleEvent();

        protected _selectedInputOptions: number[] = [];

        protected _initSettings: RS.Slots.UI.IUIController.InitSettings;

        // Handles/listeners
        @RS.AutoDisposeOnSet protected _orientationChangedListener: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _canQuickSpinChangedListener: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _allowSpinHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _allowSkipHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _spinEnableHandle: RS.IArbiterHandle<boolean>;
        @RS.AutoDisposeOnSet protected _enabledStateHandler: RS.IDisposable;

        // UI elements
        @RS.AutoDisposeOnSet protected _bottomBar: CommonUI.BottomBar;
        @RS.AutoDisposeOnSet protected _desktopButtonPanel: CommonUI.DesktopButtonPanel;
        @RS.AutoDisposeOnSet protected _mobileButtonPanel: CommonUI.MobileButtonPanel;
        @RS.AutoDisposeOnSet protected _mobileMenu: CommonUI.MobileMenuHolder;
        @RS.AutoDisposeOnSet protected _turboToggle: CommonUI.TurboToggle;

        protected _desktopSpinners: CommonUI.DesktopButtonPanel.SpinnerSettings[];
        protected _mobileSpinners: CommonUI.MobileBetMenu.SpinnerSettings[];

        protected _spinQueued: boolean = false;

        /** Gets a rectangle representing how much of the attached container is fully occluded by the UI on all sides. */
        public get enclosedSpace(): RS.Flow.Spacing
        {
            if (this._bottomBar == null) { return RS.Flow.Spacing.none; }
            return {
                left: 0,
                top: 0,
                right: 0,
                bottom: Math.round(this._bottomBar.bottomPanelHeight * this._bottomBar.scaleY)
            };
        }

        /** Number of ignore calls. */
        protected _ignoreRequestCount: number = 0;

        private __linesSpinnerIndex: number | null = null;
        private __totalBetSpinnerIndex: number | null = null;
        private __betPerLineSpinnerIndex: number | null = null;
        private __betMultiplierSpinnerIndex: number | null = null;

        private __spinnerOptions: SpinnerOptionState[];

        /** Gets if this controller has been disposed. */
        public get isDisposed() { return this.__isDisposed; }
        private __isDisposed: boolean = false;

        public initialise(settings: RS.Slots.UI.IUIController.Settings): void
        {
            this.settings = settings;

            this._orientationChangedListener = settings.orientationObservable
                ? settings.orientationObservable.onChanged(this.handleOrientationChanged)
                : RS.Orientation.onChanged(this.handleOrientationChanged);

            this._canQuickSpinChangedListener = settings.allowQuickSpinArbiter.onChanged(this.handleCanQuickSpinChanged);
        }

        /** Creates the UI with the specified settings. */
        public create(settings: RS.Slots.UI.IUIController.InitSettings): void
        {
            this._initSettings = settings;

            const platform = RS.IPlatform.get();
            this._bottomBar = CommonUI.bottomBar.create(CommonUI.BottomBar.defaultSettings,
            {
                enableInteractionArbiter: this.settings.enableInteractionArbiter,
                messageTextArbiter: this.settings.messageTextArbiter,
                showBackButton: platform.canNavigateToLobby,
                showHelpButton: platform.canNavigateToExternalHelp
            });

            this._bottomBar.locale = settings.locale;
            this._bottomBar.currencyFormatter = settings.currencyFormatter;
            this._bottomBar.onBackClicked(this.handleBackClicked);
            this._bottomBar.onSettingsClicked(this.handleSettingsClicked);
            this._bottomBar.onHelpClicked(this.handleHelpClicked);
            this._bottomBar.bindToObservables({ cornerListExpanded: this.settings.enableInteractionArbiter });
            this.handleOrientationChanged(RS.Orientation.currentOrientation);

            this.__spinnerOptions = [];

            if (RS.Device.isDesktopDevice)
            {
                const spinners: CommonUI.DesktopButtonPanel.SpinnerSettings[] = [];
                for (const inputOption of settings.inputOptions)
                {
                    switch (inputOption.type)
                    {
                        case RS.Slots.UI.InputOptionType.Lines:
                            this.__linesSpinnerIndex = spinners.length;
                            spinners.push(CommonUI.DesktopButtonPanel.getSpinnerSettings(CommonUI.Translations.UI.Lines, "IncreaseLinesButton", "DecreaseLinesButton"));
                            this.__spinnerOptions[this.__linesSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                            break;
                        case RS.Slots.UI.InputOptionType.TotalBet:
                            this.__totalBetSpinnerIndex = spinners.length;
                            spinners.push(CommonUI.DesktopButtonPanel.getSpinnerSettings(CommonUI.Translations.UI.TotalBet, "IncreaseBetButton", "DecreaseBetButton"));
                            this.__spinnerOptions[this.__totalBetSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                            break;
                        case RS.Slots.UI.InputOptionType.BetPerLine:
                            this.__betPerLineSpinnerIndex = spinners.length;
                            spinners.push(CommonUI.DesktopButtonPanel.getSpinnerSettings(CommonUI.Translations.UI.BetLineTitle, "IncreaseBetPerLineButton", "DecreaseBetPerLineButton"));
                            this.__spinnerOptions[this.__betPerLineSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                            break;
                        case RS.Slots.UI.InputOptionType.BetMultiplier:
                            this.__betMultiplierSpinnerIndex = spinners.length;
                            spinners.push(CommonUI.DesktopButtonPanel.getSpinnerSettings(CommonUI.Translations.UI.TotalBet, "IncreaseBetMultiplierButton", "DecreaseBetMultiplierButton"));
                            this.__spinnerOptions[this.__betMultiplierSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                            break;
                    }
                }
                this._desktopSpinners = spinners;

                this.createDesktopButtonPanel(
                    (RS.Orientation.currentOrientation === RS.DeviceOrientation.Landscape ? SG.CommonUI.DesktopButtonPanel.defaultLandscapeSettings : null) ||
                    (RS.Orientation.currentOrientation === RS.DeviceOrientation.Portrait ? SG.CommonUI.DesktopButtonPanel.defaultPortraitSettings : null) ||
                    SG.CommonUI.DesktopButtonPanel.defaultSettings
                );
            }
            else
            {
                this._mobileSpinners = this.getMobileSpinners();

                const mobileButtonSettings = (RS.Orientation.currentOrientation === RS.DeviceOrientation.Landscape ? SG.CommonUI.MobileButtonPanel.defaultLandscapeSettings : null) ||
                    (RS.Orientation.currentOrientation === RS.DeviceOrientation.Portrait ? SG.CommonUI.MobileButtonPanel.defaultPortraitSettings : null) ||
                    SG.CommonUI.MobileButtonPanel.defaultSettings;

                const mobileMenuSettings = (RS.Orientation.currentOrientation === RS.DeviceOrientation.Landscape ? CommonUI.MobileMenuHolder.defaultLandscapeSettings : null) ||
                    (RS.Orientation.currentOrientation === RS.DeviceOrientation.Portrait ? CommonUI.MobileMenuHolder.defaultPortraitSettings : null) ||
                    CommonUI.MobileMenuHolder.defaultSettings;

                this.createMobileButtonPanel(mobileButtonSettings, mobileMenuSettings);
            }
            const turboSettings = this.findTurboToggleSettings(this.settings.orientationObservable.value);
            if (turboSettings)
            {
                this._turboToggle = SG.CommonUI.turboToggle.create(turboSettings);
                this._turboToggle.locale = this._initSettings.locale;
                this._turboToggle.onToggledOn(this.handleTurboToggledOn);
                this._turboToggle.onToggledOff(this.handleTurboToggledOff);
                this._turboToggle.bindToObservables({ visible: this.settings.showTurboArbiter });
                this._turboToggle.bindToObservables({ isTurboOn: this.settings.turboModeArbiter });
            }

            for (let i = 0, l = settings.inputOptions.length; i < l; ++i)
            {
                this.setInputOptions(settings.inputOptions[i].type, settings.inputOptions[i].options);
            }

            this._enabledStateHandler = this.settings.enableInteractionArbiter.onChanged(this.handleInteractionEnabledChanged);
            this._allowSpinHandle = this.settings.allowSpinArbiter.onChanged(this.handleAllowSpinArbiterValueChanged);
        }

        /** Updates the balance to be displayed on the UI. */
        public setBalance(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.balance = value;
        }

        /** Updates the win amount to be displayed on the UI. */
        public setWin(value: number, final: boolean)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.winAmount = value;
        }

        /** Updates the total bet to be displayed on the UI. */
        public setTotalBet(value: number)
        {
            if (this._bottomBar) { this._bottomBar.totalBet = value; }
            if (this._mobileMenu) { this._mobileMenu.innerBetMenu.totalBetAmount = value; }
        }

        /** Updates the line count to be displayed on the UI. */
        public setLines(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.lines = value;
        }

        public setWays(value: number)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.ways = value;
        }

        /** Sets replay mode. */
        public setReplayMode(value: boolean)
        {
            if (!this._bottomBar) { return; }
            this._bottomBar.replayMode = value;
        }

        /** Overrides the selected input value. */
        public setSelectedInputValue(optionType: RS.Slots.UI.InputOptionType, value: number)
        {
            const spinner = this.getSpinner(optionType);
            if (spinner)
            {
                if (spinner.value === value) { return; }
                // If the value doesn't exist in options, temporarily insert it at the current index
                // When the user changes option, it will ensure correct order and reset the options back so that this value remains unselectable
                const storedOpts = this.getSpinnerOptionState(optionType);
                const idx = storedOpts.properOptions.indexOf(value);
                if (idx === -1)
                {
                    ++this._ignoreRequestCount;

                    storedOpts.temporaryOptionIndex = spinner.optionIndex;
                    storedOpts.temporaryOptions = [...storedOpts.properOptions];
                    storedOpts.temporaryOptions.splice(storedOpts.temporaryOptionIndex, 0, value);
                    const c = this._initSettings.currencyFormatter;
                    spinner.options = this.isCurrency(optionType) ? storedOpts.temporaryOptions.map((o) => c.format(o)) : storedOpts.temporaryOptions;

                    spinner.value = this.isCurrency(optionType) ? this._initSettings.currencyFormatter.format(value) : value;
                    this._onInputOptionChanged.publish({ optionType, optionIndex: -1, userChanged: false });

                    --this._ignoreRequestCount;
                }
                else
                {
                    this.setSelectedInputOption(optionType, idx);
                }
            }
        }

        /** Updates the user's input option selection. */
        public setSelectedInputOption(optionType: RS.Slots.UI.InputOptionType, optionIndex: number)
        {
            const spinner = this.getSpinner(optionType);
            if (spinner && spinner.optionIndex !== optionIndex)
            {
                ++this._ignoreRequestCount;
                this.resetTemporarySpinnerOptions(optionType);

                spinner.optionIndex = optionIndex;
                this._onInputOptionChanged.publish({ optionType, optionIndex, userChanged: false });

                --this._ignoreRequestCount;
            }

            this._selectedInputOptions[optionType] = optionIndex;
        }

        /** Updates the user's available input options. */
        public setInputOptions(optionType: RS.Slots.UI.InputOptionType, options: number[])
        {
            const spinner = this.getSpinner(optionType);
            if (spinner)
            {
                ++this._ignoreRequestCount;
                const c = this._initSettings.currencyFormatter;
                const optionState = this.getSpinnerOptionState(optionType);
                optionState.properOptions.length = options.length;
                for (let i = 0, l = options.length; i < l; ++i)
                {
                    optionState.properOptions[i] = options[i];
                }
                spinner.options = this.isCurrency(optionType) ? options.map((o) => c.format(o)) : options;
                this._onInputOptionChanged.publish({ optionType, optionIndex: spinner.optionIndex, userChanged: false });
                optionState.temporaryOptions = null;
                optionState.temporaryOptionIndex = null;
                --this._ignoreRequestCount;
            }
        }

        /** Attaches the UI to the specified container. */
        public attach(view: RS.Rendering.IContainer): void
        {
            if (this._bottomBar != null) { view.addChild(this._bottomBar); }
            if (this._turboToggle != null) { view.addChild(this._turboToggle); }
            if (this._desktopButtonPanel != null) { view.addChild(this._desktopButtonPanel); }
            if (this._mobileButtonPanel != null) { view.addChild(this._mobileButtonPanel); }
            if (this._mobileMenu != null) { view.addChild(this._mobileMenu); }

            if (RS.Is.flowElement(view))
            {
                view.invalidateLayout();
            }
        }

        /** Detaches the UI from the specified container, if it is attached to that. */
        public detach(view: RS.Rendering.IContainer): void
        {
            for (const element of [this._bottomBar, this._desktopButtonPanel, this._mobileButtonPanel, this._mobileMenu, this._turboToggle])
            {
                if (element != null && element.parent === view)
                {
                    view.removeChild(element);
                }
            }
        }

        /**
         * Disposes this controller.
         */
        public dispose(): void
        {
            if (this.__isDisposed) { return; }

            this.__isDisposed = true;
        }

        @RS.Callback
        protected handleInteractionEnabledChanged(interactionEnabled: boolean)
        {
            if (!interactionEnabled && this._mobileButtonPanel)
            {
                this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            }
        }

        protected isCurrency(optionType: RS.Slots.UI.InputOptionType): boolean
        {
            switch (optionType)
            {
                case RS.Slots.UI.InputOptionType.Lines: return false;
                case RS.Slots.UI.InputOptionType.TotalBet: return true;
                case RS.Slots.UI.InputOptionType.BetPerLine: return true;
                case RS.Slots.UI.InputOptionType.BetMultiplier: return false;
            }
        }

        /** Gets the current options for the given option type. */
        protected getSpinnerOptionState(optionType: RS.Slots.UI.InputOptionType): SpinnerOptionState
        {
            switch (optionType)
            {
                case RS.Slots.UI.InputOptionType.Lines:
                    if (this.__linesSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__linesSpinnerIndex];
                    }
                    break;
                case RS.Slots.UI.InputOptionType.TotalBet:
                    if (this.__totalBetSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__totalBetSpinnerIndex];
                    }
                    break;
                case RS.Slots.UI.InputOptionType.BetPerLine:
                    if (this.__betPerLineSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__betPerLineSpinnerIndex];
                    }
                    break;
                case RS.Slots.UI.InputOptionType.BetMultiplier:
                    if (this.__betMultiplierSpinnerIndex != null)
                    {
                        return this.__spinnerOptions[this.__betMultiplierSpinnerIndex];
                    }
                    break;
            }
            return null;
        }

        protected resetTemporarySpinnerOptions(optionType: RS.Slots.UI.InputOptionType): void
        {
            const optionState = this.getSpinnerOptionState(optionType);
            if (optionState && (optionState.temporaryOptions != null || optionState.temporaryOptionIndex != null))
            {
                const spinner = this.getSpinner(optionType);
                if (spinner)
                {
                    ++this._ignoreRequestCount;
                    const c = this._initSettings.currencyFormatter;
                    spinner.options = this.isCurrency(optionType) ? optionState.properOptions.map((o) => c.format(o)) : optionState.properOptions;
                    spinner.optionIndex = this._selectedInputOptions[optionType];
                    --this._ignoreRequestCount;
                }
                optionState.temporaryOptionIndex = null;
                optionState.temporaryOptions = null;
            }
        }

        /** Gets the spinner for the given option type. Returns undefined when not found. */
        protected getSpinner(optionType: RS.Slots.UI.InputOptionType): RS.Flow.Spinner | RS.Flow.ListSpinner | void
        {
            if (this._desktopButtonPanel)
            {
                switch (optionType)
                {
                    case RS.Slots.UI.InputOptionType.Lines:
                        if (this.__linesSpinnerIndex != null)
                        {
                            return this._desktopButtonPanel.getSpinner(this.__linesSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.TotalBet:
                        if (this.__totalBetSpinnerIndex != null)
                        {
                            return this._desktopButtonPanel.getSpinner(this.__totalBetSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetPerLine:
                        if (this.__betPerLineSpinnerIndex != null)
                        {
                            return this._desktopButtonPanel.getSpinner(this.__betPerLineSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetMultiplier:
                        if (this.__betMultiplierSpinnerIndex != null)
                        {
                            return this._desktopButtonPanel.getSpinner(this.__betMultiplierSpinnerIndex);
                        }
                        break;
                }
            }
            else if (this._mobileMenu)
            {
                switch (optionType)
                {
                    case RS.Slots.UI.InputOptionType.Lines:
                        if (this.__linesSpinnerIndex != null)
                        {
                            return this._mobileMenu.innerBetMenu.getSpinner(this.__linesSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.TotalBet:
                        if (this.__totalBetSpinnerIndex != null)
                        {
                            return this._mobileMenu.innerBetMenu.getSpinner(this.__totalBetSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetPerLine:
                        if (this.__betPerLineSpinnerIndex != null)
                        {
                            return this._mobileMenu.innerBetMenu.getSpinner(this.__betPerLineSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetMultiplier:
                        if (this.__betMultiplierSpinnerIndex != null)
                        {
                            return this._mobileMenu.innerBetMenu.getSpinner(this.__betMultiplierSpinnerIndex);
                        }
                        break;
                }
            }
        }

        @RS.Callback
        protected handleInputOptionChanged(ev: { inputOption: number; optionIndex: number; })
        {
            if (this._ignoreRequestCount > 0) { return; }
            const optionState = this.getSpinnerOptionState(ev.inputOption);
            const spinner = this.getSpinner(ev.inputOption);
            if (spinner && optionState && optionState.temporaryOptionIndex != null)
            {
                // We're moving off a temporarily inserted value
                // We need to correct the option index to compensate
                if (ev.optionIndex > optionState.temporaryOptionIndex)
                {
                    --ev.optionIndex;
                }
                else
                {
                    ++ev.optionIndex;
                }
            }
            this._selectedInputOptions[ev.inputOption] = ev.optionIndex;
            this.resetTemporarySpinnerOptions(ev.inputOption);
            if (ev.inputOption === this.__linesSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.Lines, optionIndex: ev.optionIndex, userChanged: true });
            }
            else if (ev.inputOption === this.__totalBetSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.TotalBet, optionIndex: ev.optionIndex, userChanged: true });
            }
            else if (ev.inputOption === this.__betPerLineSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.BetPerLine, optionIndex: ev.optionIndex, userChanged: true });
            }
            else if (ev.inputOption === this.__betMultiplierSpinnerIndex)
            {
                this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.BetMultiplier, optionIndex: ev.optionIndex, userChanged: true });
            }
        }

        @RS.Callback
        protected handleSpinButtonClicked()
        {
            if (this.settings.allowQuickSpinArbiter.value)
            {
                this._onSkipRequested.publish();
                this._allowSkipHandle = this.settings.allowSkipArbiter.declare(false);
                this._spinQueued = true;
                RS.Log.debug("quickSpin: Spin now queued");
            }
            else
            {
                this._onSpinRequested.publish();
            }
        }

        @RS.Callback
        protected handleCanQuickSpinChanged(canQuickSpin: boolean)
        {
            if (!canQuickSpin && this._spinQueued)
            {
                RS.Log.debug("quickSpin: can no longer quick spin; spin unqueued");
                this._spinQueued = false;

                if (this._allowSkipHandle)
                {
                    this._allowSkipHandle.dispose();
                    this._allowSkipHandle = null;
                }
            }
        }

        @RS.Callback
        protected handleStopButtonClicked()
        {
            this._onSkipRequested.publish();
        }

        @RS.Callback
        protected handleBackClicked()
        {
            RS.IPlatform.get().navigateToLobby();
        }

        @RS.Callback
        protected handleSettingsClicked()
        {
            this._onSettingsRequested.publish();
            if (this._mobileMenu)
            {
                this.closeMobileMenu();
                this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
                this._spinEnableHandle = null;
            }
        }

        @RS.Callback
        protected handleHelpClicked()
        {
            this._onHelpRequested.publish();
            if (this._mobileMenu)
            {
                this.closeMobileMenu();
                this._spinEnableHandle = null;
            }
        }

        @RS.Callback
        protected handleBigBetClicked()
        {
            this._onBigBetRequested.publish();
            if (this._mobileMenu)
            {
                this.closeMobileMenu();
                this._spinEnableHandle = null;
            }
        }

        @RS.Callback
        protected handlePaytableClicked()
        {
            if (this._mobileMenu && !this._mobileMenu.isOpen) { return; }

            if (this._mobileMenu)
            {
                this.closeMobileMenu();
                this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
                this._spinEnableHandle = null;
            }
            this._onPaytableRequested.publish();
        }

        @RS.Callback
        protected handleAutoplayClicked()
        {
            if (this._mobileMenu && !this._mobileMenu.isOpen) { return; }

            if (this._mobileMenu)
            {
                this.closeMobileMenu();
                this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
                this._spinEnableHandle = null;
            }

            this._onAutoplayRequested.publish();
        }
        @RS.Callback
        protected handleAutoplayStopClicked()
        {
            this._onAutoplayStopRequested.publish();
            if (this._mobileMenu) { this.closeMobileMenu(); }
        }

        @RS.Callback
        protected handleTurboToggledOn()
        {
            this._onTurboRequested.publish();
        }

        @RS.Callback
        protected handleTurboToggledOff()
        {
            this._onTurboStopRequested.publish();
        }

        @RS.Callback
        protected handleBetClicked()
        {
            if (!this._mobileMenu) { return; }
            this._mobileMenu.mode = CommonUI.MobileMenuHolder.Mode.Bet;
        }

        @RS.Callback
        protected handleAcceptMobileBet()
        {
            for (const inputOption of this._initSettings.inputOptions)
            {
                switch (inputOption.type)
                {
                    case RS.Slots.UI.InputOptionType.Lines:
                        if (this.__linesSpinnerIndex != null)
                        {
                            const spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__linesSpinnerIndex);
                            this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.Lines, optionIndex: spinner.optionIndex, userChanged: true });
                            this._selectedInputOptions[this.__linesSpinnerIndex] = spinner.optionIndex;
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.TotalBet:
                        if (this.__totalBetSpinnerIndex != null)
                        {
                            const spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__totalBetSpinnerIndex);
                            this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.TotalBet, optionIndex: spinner.optionIndex, userChanged: true });
                            this._selectedInputOptions[this.__totalBetSpinnerIndex] = spinner.optionIndex;
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetPerLine:
                        if (this.__betPerLineSpinnerIndex != null)
                        {
                            const spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__betPerLineSpinnerIndex);
                            this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.BetPerLine, optionIndex: spinner.optionIndex, userChanged: true });
                            this._selectedInputOptions[this.__betPerLineSpinnerIndex] = spinner.optionIndex;
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetMultiplier:
                        if (this.__betMultiplierSpinnerIndex != null)
                        {
                            const spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__betMultiplierSpinnerIndex);
                            this._onInputOptionChanged.publish({ optionType: RS.Slots.UI.InputOptionType.BetMultiplier, optionIndex: spinner.optionIndex, userChanged: true });
                            this._selectedInputOptions[this.__betMultiplierSpinnerIndex] = spinner.optionIndex;
                        }
                        break;
                }
            }
            this.closeMobileMenu();
            this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            this._spinEnableHandle = null;

        }

        @RS.Callback
        protected handleCancelMobileBet()
        {
            for (const inputOption of this._initSettings.inputOptions)
            {
                let spinner: RS.Flow.ListSpinner | null = null;
                switch (inputOption.type)
                {
                    case RS.Slots.UI.InputOptionType.Lines:
                        if (this.__linesSpinnerIndex != null)
                        {
                            spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__linesSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.TotalBet:
                        if (this.__totalBetSpinnerIndex != null)
                        {
                            spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__totalBetSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetPerLine:
                        if (this.__betPerLineSpinnerIndex != null)
                        {
                            spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__betPerLineSpinnerIndex);
                        }
                        break;
                    case RS.Slots.UI.InputOptionType.BetMultiplier:
                        if (this.__betMultiplierSpinnerIndex != null)
                        {
                            spinner = this._mobileMenu.innerBetMenu.getSpinner(this.__betMultiplierSpinnerIndex);
                        }
                        break;
                }
                if (spinner != null)
                {
                    spinner.optionIndex = this._selectedInputOptions[inputOption.type];
                }
            }
            this.closeMobileMenu();
            this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            this._spinEnableHandle = null;

        }

        @RS.Callback
        protected async handleMobileChevronClicked()
        {
            if (this._mobileMenu.isOpen)
            {
                this._spinEnableHandle = null;

                this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
                this.handleCancelMobileBet();
            }
            else
            {
                if (this._bottomBar.cornerListExpanded)
                {
                    this._bottomBar.cornerListExpanded = false;
                }

                this._spinEnableHandle = this.settings.allowSpinArbiter.declare(false);

                this._mobileMenu.mode = CommonUI.MobileMenuHolder.Mode.Menu;
                // Unbind temporarily so we can hide it.
                this._mobileButtonPanel.bindToObservables({ visible: null });
                this._mobileButtonPanel.visible = false;
                this._mobileMenu.open();
            }
        }

        @RS.Callback
        protected handleAllowSpinArbiterValueChanged(): void
        {
            if (this._spinQueued && this.settings.allowSpinArbiter.value)
            {
                RS.Log.debug("quickSpin: onSpinRequested published, spin no longer queued");
                this._onSpinRequested.publish();
                this._spinQueued = false;

                if (this._allowSkipHandle)
                {
                    this._allowSkipHandle.dispose();
                    this._allowSkipHandle = null;
                }
            }
        }

        @RS.Callback
        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation)
        {
            if (this._bottomBar != null)
            {
                if (newOrientation === RS.DeviceOrientation.Landscape)
                {
                    this._bottomBar.mode = RS.Device.isMobileDevice ? CommonUI.BottomBar.Mode.MobileLandscape : CommonUI.BottomBar.Mode.Landscape;
                }
                else
                {
                    this._bottomBar.mode = RS.Device.isMobileDevice ? CommonUI.BottomBar.Mode.MobilePortrait : CommonUI.BottomBar.Mode.Portrait;
                }
            }
            if (this._desktopButtonPanel)
            {
                let remakeWithSettings: SG.CommonUI.DesktopButtonPanel.Settings | null = null;
                if (newOrientation === RS.DeviceOrientation.Landscape && SG.CommonUI.DesktopButtonPanel.defaultLandscapeSettings)
                {
                    remakeWithSettings = SG.CommonUI.DesktopButtonPanel.defaultLandscapeSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Landscape && SG.CommonUI.DesktopButtonPanel.defaultPortraitSettings)
                {
                    remakeWithSettings = SG.CommonUI.DesktopButtonPanel.defaultSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Portrait && SG.CommonUI.DesktopButtonPanel.defaultPortraitSettings)
                {
                    remakeWithSettings = SG.CommonUI.DesktopButtonPanel.defaultPortraitSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Portrait && SG.CommonUI.DesktopButtonPanel.defaultLandscapeSettings)
                {
                    remakeWithSettings = SG.CommonUI.DesktopButtonPanel.defaultSettings;
                }
                if (remakeWithSettings)
                {
                    //Preserve spinner indices
                    const {spinnerValues, spinnerIndices} = this.findCurrentSpinnerIndexes(this._desktopButtonPanel);

                    //Dispose, recreate desktop button panel
                    const oldParent = this._desktopButtonPanel.parent;
                    if (oldParent)
                    {
                        const oldIndex = oldParent.getChildIndex(this._desktopButtonPanel);
                        oldParent.removeChild(this._desktopButtonPanel);
                        this._desktopButtonPanel.dispose();
                        this.createDesktopButtonPanel(remakeWithSettings, spinnerIndices, spinnerValues);
                        oldParent.addChild(this._desktopButtonPanel, oldIndex);

                        if (RS.Is.flowElement(oldParent))
                        {
                            oldParent.invalidateLayout();
                        }
                    }
                    else
                    {
                        this._desktopButtonPanel.dispose();
                        this.createDesktopButtonPanel(remakeWithSettings, spinnerIndices, spinnerValues);
                    }
                }
            }
            else if (this._mobileButtonPanel)
            {
                let remakeWithSettings: SG.CommonUI.MobileButtonPanel.Settings | null = null;
                if (newOrientation === RS.DeviceOrientation.Landscape && SG.CommonUI.MobileButtonPanel.defaultLandscapeSettings)
                {
                    remakeWithSettings = SG.CommonUI.MobileButtonPanel.defaultLandscapeSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Landscape && SG.CommonUI.MobileButtonPanel.defaultPortraitSettings)
                {
                    remakeWithSettings = SG.CommonUI.MobileButtonPanel.defaultSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Portrait && SG.CommonUI.MobileButtonPanel.defaultPortraitSettings)
                {
                    remakeWithSettings = SG.CommonUI.MobileButtonPanel.defaultPortraitSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Portrait && SG.CommonUI.MobileButtonPanel.defaultLandscapeSettings)
                {
                    remakeWithSettings = SG.CommonUI.MobileButtonPanel.defaultSettings;
                }

                let remakeMenuWithSettings: SG.CommonUI.MobileMenuHolder.Settings | null = null;
                if (newOrientation === RS.DeviceOrientation.Landscape && SG.CommonUI.MobileMenuHolder.defaultLandscapeSettings)
                {
                    remakeMenuWithSettings = SG.CommonUI.MobileMenuHolder.defaultLandscapeSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Landscape && SG.CommonUI.MobileMenuHolder.defaultPortraitSettings)
                {
                    remakeMenuWithSettings = SG.CommonUI.MobileMenuHolder.defaultSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Portrait && SG.CommonUI.MobileMenuHolder.defaultPortraitSettings)
                {
                    remakeMenuWithSettings = SG.CommonUI.MobileMenuHolder.defaultPortraitSettings;
                }
                else if (newOrientation === RS.DeviceOrientation.Portrait && SG.CommonUI.MobileMenuHolder.defaultLandscapeSettings)
                {
                    remakeMenuWithSettings = SG.CommonUI.MobileMenuHolder.defaultSettings;
                }

                //Preserve spinner indices
                const {spinnerValues, spinnerIndices} = this.findCurrentSpinnerIndexes(this._mobileMenu.innerBetMenu);

                if (remakeWithSettings)
                {
                    //Dispose, recreate mobile button panel
                    const oldParent = this._mobileButtonPanel.parent;
                    const oldMenuParent = this._mobileMenu.parent;
                    if (oldParent)
                    {
                        const oldIndex = oldParent.getChildIndex(this._mobileButtonPanel);
                        const oldMenuIndex = oldMenuParent.getChildIndex(this._mobileMenu);
                        const menuOpen = this._mobileMenu.isOpen;
                        const menuMode = this._mobileMenu.mode;
                        const totalBet = this._mobileMenu.innerBetMenu.totalBetAmount;
                        oldParent.removeChild(this._mobileButtonPanel);
                        this._mobileButtonPanel.dispose();
                        if (remakeMenuWithSettings)
                        {
                            oldMenuParent.removeChild(this._mobileMenu);
                            this._mobileMenu.dispose();
                        }
                        this.createMobileButtonPanel(remakeWithSettings, remakeMenuWithSettings);
                        oldParent.addChild(this._mobileButtonPanel, oldIndex);
                        if (remakeMenuWithSettings)
                        {
                            oldMenuParent.addChild(this._mobileMenu, oldMenuIndex);
                            if (menuOpen)
                            {
                                this._mobileMenu.open();
                            }
                            this._mobileMenu.mode = menuMode;
                            for (let i = 0, l = this._initSettings.inputOptions.length; i < l; ++i)
                            {
                                this.setInputOptions(this._initSettings.inputOptions[i].type, this._initSettings.inputOptions[i].options);
                            }
                            // reset the bet menu
                            this._mobileMenu.innerBetMenu.totalBetAmount = totalBet;
                            for (let i = 0; i < spinnerValues.length; i++)
                            {
                                const spinner = this._mobileMenu.innerBetMenu.getSpinner(spinnerIndices[i]);
                                spinner.optionIndex = spinnerValues[i];
                            }
                        }

                        if (RS.Is.flowElement(oldParent))
                        {
                            oldParent.invalidateLayout();
                        }
                        if (RS.Is.flowElement(oldMenuParent))
                        {
                            oldMenuParent.invalidateLayout();
                        }
                    }
                    else
                    {
                        this._mobileButtonPanel.dispose();
                        if (remakeMenuWithSettings) { this._mobileMenu.dispose(); }
                        this.createMobileButtonPanel(remakeWithSettings, remakeMenuWithSettings);
                    }
                }
            }
            if (this._turboToggle)
            {
                let remakeWithSettings = this.findTurboToggleSettings(newOrientation);

                if (remakeWithSettings != null)
                {
                    const oldParent = this._turboToggle.parent;
                    let oldIndex: number;
                    if (oldParent)
                    {
                        oldIndex = oldParent.getChildIndex(this._turboToggle);
                        oldParent.removeChild(this._turboToggle);
                    }
                    this._turboToggle = CommonUI.turboToggle.create(remakeWithSettings);
                    this._turboToggle.locale = this._initSettings.locale;
                    this._turboToggle.onToggledOn(this.handleTurboToggledOn);
                    this._turboToggle.onToggledOff(this.handleTurboToggledOff);
                    this._turboToggle.bindToObservables({ visible: this.settings.showTurboArbiter });
                    this._turboToggle.bindToObservables({ isTurboOn: this.settings.turboModeArbiter });
                    if (oldParent)
                    {
                        oldParent.addChild(this._turboToggle, oldIndex);

                        if (RS.Is.flowElement(oldParent))
                        {
                            oldParent.invalidateLayout();
                        }
                    }
                }
            }
        }

        protected findCurrentSpinnerIndexes(betMenu: SG.CommonUI.MobileBetMenu | CommonUI.DesktopButtonPanel): {spinnerValues: number[], spinnerIndices: number[]}
        {
            const spinnerValues: number[] = [];
            const spinnerIndices: number[] = [];

            if (this.__totalBetSpinnerIndex != null)
            {
                spinnerIndices.push(this.__totalBetSpinnerIndex);
                spinnerValues.push(betMenu.getSpinner(this.__totalBetSpinnerIndex).optionIndex);
            }
            if (this.__linesSpinnerIndex != null)
            {
                spinnerIndices.push(this.__linesSpinnerIndex);
                spinnerValues.push(betMenu.getSpinner(this.__linesSpinnerIndex).optionIndex);
            }
            if (this.__betPerLineSpinnerIndex != null)
            {
                spinnerIndices.push(this.__betPerLineSpinnerIndex);
                spinnerValues.push(betMenu.getSpinner(this.__betPerLineSpinnerIndex).optionIndex);
            }
            if (this.__betMultiplierSpinnerIndex != null)
            {
                spinnerIndices.push(this.__betMultiplierSpinnerIndex);
                spinnerValues.push(betMenu.getSpinner(this.__betMultiplierSpinnerIndex).optionIndex);
            }
            return {spinnerValues: spinnerValues, spinnerIndices: spinnerIndices};
        }

        protected findTurboToggleSettings(orientation: RS.DeviceOrientation): SG.CommonUI.TurboToggle.Settings
        {
            let settings: SG.CommonUI.TurboToggle.Settings = null;
            if (orientation === RS.DeviceOrientation.Landscape)
            {
                if (RS.Device.isMobileDevice)
                {
                    settings = SG.CommonUI.TurboToggle.defaultMobileLandscapeSettings || SG.CommonUI.TurboToggle.defaultMobileSettings;
                }
                else
                {
                    settings = SG.CommonUI.TurboToggle.defaultDesktopLandscapeSettings || SG.CommonUI.TurboToggle.defaultDesktopSettings;
                }
            }
            else
            {
                if (RS.Device.isMobileDevice)
                {
                    settings = SG.CommonUI.TurboToggle.defaultMobilePortraitSettings || SG.CommonUI.TurboToggle.defaultMobileSettings;
                }
                else
                {
                    settings = SG.CommonUI.TurboToggle.defaultDesktopPortraitSettings || SG.CommonUI.TurboToggle.defaultDesktopSettings;
                }
            }

            if (SG.CommonUI.TurboToggle.defaultSettings)
            {
                settings = settings || { ...SG.CommonUI.TurboToggle.defaultSettings };
                settings.locale = this._initSettings.locale;
            }

            return settings;
        }

        protected closeMobileMenu()
        {
            if (!this._bottomBar.cornerListExpanded && this.settings.enableInteractionArbiter.value)
            {
                this._bottomBar.cornerListExpanded = true;
            }

            this._mobileMenu.close();
        }

        protected createDesktopButtonPanel(settings: CommonUI.DesktopButtonPanel.Settings, spinnerIndices?: number[], spinnerValues?: number[]): void
        {
            this._desktopButtonPanel = CommonUI.desktopButtonPanel.create(settings,
            {
                canSpinArbiter: this.settings.allowSpinArbiter,
                canSkipArbiter: this.settings.allowSkipArbiter,
                spinOrSkipArbiter: this.settings.spinOrSkipArbiter,
                canQuickSpinArbiter: this.settings.allowQuickSpinArbiter,
                enableInteractionArbiter: this.settings.enableInteractionArbiter,
                canChangeBetArbiter: this.settings.allowChangeBetArbiter,
                paytableEnabledArbiter: this.settings.paytableEnabledArbiter,
                spinners: this._desktopSpinners,
                showBigBetButton: this._initSettings.showBigBetButton,
                showAutoplayButton: RS.Slots.IPlatform.get().supportsAutoplay !== RS.Slots.PlatformAutoplaySupport.None,
                autoplaysRemainingObservable: this.settings.autoplaysRemainingObservable,
                canAutoplayObservable: this.settings.allowAutoplayArbiter
            });

            for (let i = 0, l = this._initSettings.inputOptions.length; i < l; ++i)
            {
                this.setInputOptions(this._initSettings.inputOptions[i].type, this._initSettings.inputOptions[i].options);
            }

            if (spinnerValues != null && spinnerIndices != null)
            {
                for (let i = 0; i < spinnerValues.length; i++)
                {
                    const spinner = this._desktopButtonPanel.getSpinner(spinnerIndices[i]);
                    spinner.optionIndex = spinnerValues[i];
                }
            }

            this._desktopButtonPanel.locale = this._initSettings.locale;
            this._desktopButtonPanel.currencyFormatter = settings.currencyFormatter;
            this._desktopButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            this._desktopButtonPanel.onInputOptionChanged(this.handleInputOptionChanged);
            this._desktopButtonPanel.onSpinButtonClicked(this.handleSpinButtonClicked);
            this._desktopButtonPanel.onSkipButtonClicked(this.handleStopButtonClicked);
            this._desktopButtonPanel.onBigBetClicked(this.handleBigBetClicked);
            this._desktopButtonPanel.onHelpClicked(this.handlePaytableClicked);
            this._desktopButtonPanel.onAutoplayClicked(this.handleAutoplayClicked);
            this._desktopButtonPanel.onStopAutoplayClicked(this.handleAutoplayStopClicked);
        }

        protected createMobileButtonPanel(settings: CommonUI.MobileButtonPanel.Settings, menuSettings?: CommonUI.MobileMenuHolder.Settings): void
        {
            this._mobileButtonPanel = CommonUI.mobileButtonPanel.create(settings,
            {
                canSpinArbiter: this.settings.allowSpinArbiter,
                canSkipArbiter: this.settings.allowSkipArbiter,
                spinOrSkipArbiter: this.settings.spinOrSkipArbiter,
                enableInteractionArbiter: this.settings.enableInteractionArbiter,
                canChangeBetArbiter: this.settings.allowChangeBetArbiter,

                autoplaysRemainingObservable: this.settings.autoplaysRemainingObservable,

                showBigBetButton: this._initSettings.showBigBetButton
            });

            this._mobileButtonPanel.locale = this._initSettings.locale;
            this._mobileButtonPanel.currencyFormatter = settings.currencyFormatter;
            this._mobileButtonPanel.bindToObservables({ visible: this.settings.showButtonAreaArbiter });
            if (this._initSettings.showBigBetButton) { this._mobileButtonPanel.bindToObservables({ bigBetButtonVisible: this.settings.enableInteractionArbiter }); }

            this._mobileButtonPanel.onInputOptionChanged(this.handleInputOptionChanged);
            this._mobileButtonPanel.onSpinButtonClicked(this.handleSpinButtonClicked);
            this._mobileButtonPanel.onSkipButtonClicked(this.handleStopButtonClicked);
            this._mobileButtonPanel.onBigBetButtonClicked(this.handleBigBetClicked);
            this._mobileButtonPanel.onStopAutoplayButtonClicked(this.handleAutoplayStopClicked);

            if (menuSettings)
            {
                this._mobileMenu = CommonUI.mobileMenuHolder.create(menuSettings,
                    {
                        menuData:
                        {
                            showAutoplayButton: RS.Slots.IPlatform.get().supportsAutoplay !== RS.Slots.PlatformAutoplaySupport.None,
                            paytableEnabledArbiter: this.settings.paytableEnabledArbiter
                        },
                        betMenuData: { spinners: this._mobileSpinners }
                    });
                this._mobileMenu.innerMenu.bindToObservables({ autoplayButtonEnabled: this.settings.allowAutoplayArbiter });

                this._mobileMenu.locale = this._initSettings.locale;
                this._mobileMenu.currencyFormatter = this._initSettings.currencyFormatter;
                this._mobileMenu.bindToObservables({ chevronButtonVisible: this.settings.enableInteractionArbiter });
                this._mobileMenu.innerMenu.bindToObservables({ betButtonEnabled: this.settings.allowChangeBetArbiter });

                this._mobileMenu.onAutoplayButtonClicked(this.handleAutoplayClicked);
                this._mobileMenu.onHelpButtonClicked(this.handlePaytableClicked);
                this._mobileMenu.onChevronButtonClicked(this.handleMobileChevronClicked);
                this._mobileMenu.onBetButtonClicked(this.handleBetClicked);
                this._mobileMenu.innerBetMenu.onAcceptClicked(this.handleAcceptMobileBet);
                this._mobileMenu.innerBetMenu.onCancelClicked(this.handleCancelMobileBet);
            }
        }

        protected getMobileSpinners()
        {
            const spinners: CommonUI.MobileBetMenu.SpinnerSettings[] = [];
            for (const inputOption of this._initSettings.inputOptions)
            {
                switch (inputOption.type)
                {
                    case RS.Slots.UI.InputOptionType.Lines:
                        this.__linesSpinnerIndex = spinners.length;
                        spinners.push(CommonUI.MobileBetMenu.getSpinnerSettings(CommonUI.Translations.UI.Lines));
                        this.__spinnerOptions[this.__linesSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                    case RS.Slots.UI.InputOptionType.TotalBet:
                        this.__totalBetSpinnerIndex = spinners.length;
                        spinners.push(CommonUI.MobileBetMenu.getSpinnerSettings(CommonUI.Translations.UI.TotalBet));
                        this.__spinnerOptions[this.__totalBetSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                    case RS.Slots.UI.InputOptionType.BetPerLine:
                        this.__betPerLineSpinnerIndex = spinners.length;
                        spinners.push(CommonUI.MobileBetMenu.getSpinnerSettings(CommonUI.Translations.UI.BetLineTitle));
                        this.__spinnerOptions[this.__betPerLineSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                    case RS.Slots.UI.InputOptionType.BetMultiplier:
                        this.__betMultiplierSpinnerIndex = spinners.length;
                        spinners.push(CommonUI.MobileBetMenu.getSpinnerSettings(CommonUI.Translations.UI.TotalBet));
                        this.__spinnerOptions[this.__betMultiplierSpinnerIndex] = { properOptions: [], temporaryOptions: null, temporaryOptionIndex: null };
                        break;
                }
            }
            return spinners;
        }
    }

    RS.Slots.UI.IUIController.register(UIController);
}
