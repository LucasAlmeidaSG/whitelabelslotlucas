namespace Slots.SG.Paytable.Pages
{
    /**
     * TODO: remove this
     * @deprecated
     */
    export import LegalPagesFactory = Core.SG.Paytable.Pages.LegalPagesFactory;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import LegalTextPage = Core.SG.Paytable.Pages.LegalTextPage;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import NTLegalTextPage = Core.SG.Paytable.Pages.NTLegalTextPage;

    /**
     * TODO: remove this
     * @deprecated
     */
    export import NTTextPage = Core.SG.Paytable.Pages.NTTextPage;
}
