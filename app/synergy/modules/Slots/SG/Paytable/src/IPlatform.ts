namespace Slots.SG.Paytable
{
    export interface IPlatform extends RS.IPlatform
    {
        isNT?: boolean;
    }
}