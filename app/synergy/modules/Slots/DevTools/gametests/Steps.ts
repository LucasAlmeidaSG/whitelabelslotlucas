defineStep("Demo", function (name: string)
{
    if (!RS.Is.string(name)) { throw new TypeError(`${name} is not a string`); }

    return {
        message: `Force the '${name}' demo`,
        run: async ({ driver }) =>
        {
            //
            await Selenium.executeRequirements(driver, ["DevButton", "DemoButton"])
            await driver.executeScript(`document.querySelector('[data-selenium-text="${name}"]').onclick()`);
            await Selenium.clickElement(driver, "DevButton");
        }
    };
});

defineStep("Force", function (...indices: string[])
{
    return {
        message: `Force '${indices}' on the reels`,
        run: async ({ driver }) =>
        {
            //
            await Selenium.executeRequirements(driver, ["DevButton", "RigButton"]);

            const rigTools = await driver.findElements(selenium.By.css(`[name*="RigTool"]`));
            const rigToolMap = await Promise.all(rigTools.map(async (el, id) =>
            {
                const x = RS.Read.number(await el.getAttribute("data-selenium-x"));
                const y = RS.Read.number(await el.getAttribute("data-selenium-y"));
                return { x, y, id };
            }));

            // Sort by x and y from top-left to bottom-right
            rigToolMap.sort((a, b) => (a.x - b.x) || (a.y - b.y));

            for (let i = 0; i < indices.length; i++)
            {
                const index = indices[i];
                // Do the forcey force!

                const mapping = rigToolMap[i];
                const rigTool = rigTools[mapping.id];

                const valueElement = await rigTool.findElement(selenium.By.name("RigValue"));
                const initialValue = RS.Read.integer(await valueElement.getAttribute("data-selenium-text"));
                const finalValue = RS.Read.integer(index);

                const distance = finalValue - initialValue;
                const buttonElement = await rigTool.findElement(selenium.By.name(distance < 0 ? "RigUp" : "RigDown"));
                const buttonId = await buttonElement.getAttribute("data-selenium-id");

                const count = Math.abs(distance);
                for (let p = 0; p < count; p++)
                {
                    await Selenium.clickElementUsingSelector(driver, `[data-selenium-id="${buttonId}"]`);
                }
            }

            await Selenium.clickElement(driver, "RigButton");
            await Selenium.clickElement(driver, "DevButton");
        }
    };
});