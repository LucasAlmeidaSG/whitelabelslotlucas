namespace RS.Slots.DevTools
{
    /**
     * Responsible for the dev tools UI and functionality.
     */
    export interface IController<TSettings extends IController.Settings = IController.Settings> extends RS.DevTools.IController<TSettings>
    {
        /** Gets or sets the reels instance to target. */
        reels: Reels.IGenericComponent | null;
    }

    export const IController = Controllers.declare<IController<IController.Settings>, IController.Settings>(Strategy.Type.Instance);

    export namespace IController
    {
        export type ForceData = Map<RS.DevTools.Helpers.ItemSelector.Entry<OneOrMany<Engine.ForceResult>>>;

        export interface Settings<TModels extends Models = Models, TReplayData extends {} = any> extends RS.DevTools.IController.Settings<TModels, TReplayData>
        {
            force?: RS.DevTools.UI.Button.Settings;
            applyText?: string;

            /** Set to null if you want to hide the spinner for your game */
            reelsetSpinner?: Flow.Spinner.Settings;
            reelsetSpinnerText?: Flow.Button.Settings;

            engine?: Engine.IEngine<TModels, object, TReplayData>;
            forceData?: ForceData;
            reels?: RS.Reels.IGenericComponent;
            canSpinArbiter?: IArbiter<boolean>;
        }
    }

    export const defaultSettings: IController.Settings =
    {
        ...RS.DevTools.defaultSettings,
        force:
        {
            ...RS.DevTools.UI.Button.defaultSettings,
            innerButton: { ...RS.DevTools.UI.Button.defaultSettings.innerButton, seleniumId: "RigButton" },
            color: new RS.Util.Color(213, 94, 0), // vermillion
            text: "FORCE"
        },
        applyText: "APPLY",
        reelsetSpinner:
        {
            label: { ...Flow.Spinner.defaultSettings.label },
            size: { w: 150, h: 40 },
            incrementButton: { ...Flow.Spinner.defaultSettings.incrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            decrementButton: { ...Flow.Spinner.defaultSettings.decrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            dock: Flow.Dock.None,
            sizeToContents: false,
            options:
            {
                type: Flow.Spinner.Type.Numeric,
                minValue: 0,
                maxValue: 0,
                initialValue: 0
            }
        },
        reelsetSpinnerText:
        {
            background: { kind: Flow.Background.Kind.SolidColor, borderColor: RS.Util.Colors.black, borderSize: 2, color: RS.Util.Colors.gray },
            hoverbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: RS.Util.Colors.black, borderSize: 2, color: RS.Util.Colors.gray },
            pressbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: RS.Util.Colors.black, borderSize: 2, color: RS.Util.Colors.gray },
            disabledbackground: { kind: Flow.Background.Kind.SolidColor, borderColor: RS.Util.Colors.black, borderSize: 2, color: RS.Util.Colors.gray },
            textLabel: { dock: Flow.Dock.Fill, sizeToContents: true, font: Fonts.FrutigerWorld, fontSize: 22, text: "Reelset" },
            sizeToContents: true,
            spacing: Flow.Spacing.all(5),
            expand: Flow.Expand.Disallowed,
            dock: Flow.Dock.Float,
            floatPosition: { x: 0.5, y: -0.45 }
        },
        hideSpinner:
        {
            label: { ...Flow.Spinner.defaultSettings.label },
            size: { w: 150, h: 40 },
            incrementButton: { ...Flow.Spinner.defaultSettings.incrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            decrementButton: { ...Flow.Spinner.defaultSettings.decrementButton, size: { w: 40, h: 40 }, sizeToContents: false, expand: Flow.Expand.Allowed },
            dock: Flow.Dock.None,
            sizeToContents: false,
            options:
            {
                type: Flow.Spinner.Type.Options,
                options: [2.5, 5.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0],
                initialIndex: 2
            }
        },
        replayData: {},
        forceData: null,
        engine: null
    };
}
