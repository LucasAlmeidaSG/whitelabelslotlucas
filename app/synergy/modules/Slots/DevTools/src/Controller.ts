/// <reference path="IController.ts" />

namespace RS.Slots.DevTools
{
    /**
     * TODO: remove this
     * @deprecated
     */
    export import Helpers = RS.DevTools.Helpers;

    @HasCallbacks
    export class Controller<TSettings extends IController.Settings = IController.Settings> extends RS.DevTools.Controller<TSettings> implements IController<TSettings>
    {
        @AutoDisposeOnSet protected _reelSetSpinner: Flow.Spinner | null;
        @AutoDisposeOnSet protected _reelSetSpinnerText: Flow.Button | null;

        @AutoDisposeOnSet protected _canSpinHandle: IArbiterHandle<boolean>;

        protected _reels: Reels.IGenericComponent | null = null;

        /** Gets or sets the reels instance to target. */
        public get reels() { return this._reels; }
        public set reels(value)
        {
            if (value === this._reels) { return; }
            if (this._reels)
            {
                value.rigToolVisible = this._reels.rigToolVisible;
            }
            this._reels = value;
        }

        protected get showForceButton() { return this.settings.engine != null && this.settings.engine.hasReelSets && this._reels != null; }

        public initialise(settings: TSettings)
        {
            super.initialise(settings);
            this._reels = settings.reels;
        }

        protected createUI(gameVersion: Version): void
        {
            let titleText: string;
            if (this.settings.engine && this.settings.engine.version != null)
            {
                titleText = `C: ${Version.toString(gameVersion)}\nE: ${Version.toString(this.settings.engine.version)}`;
            }
            else
            {
                titleText = Version.toString(gameVersion);
            }

            let ntButton = this.settings.toggleNTOn;
            if (this.showToggleNTButton)
            {
                this._ntOverride = RS.Storage.load("isNT", false, true);
                if (this._ntOverride)
                {
                    ntButton = this.settings.toggleNTOff;
                }
            }

            this._ui = RS.DevTools.UI.buttonList.create({
                ...this.settings.buttonList,
                titleLabel:
                {
                    ...Flow.Label.defaultSettings,
                    text: titleText,
                    spacing: Flow.Spacing.all(3),
                    dock: Flow.Dock.Fill,
                    sizeToContents: true,
                    expand: Flow.Expand.Disallowed
                },
                buttons:
                [
                    this.showDemoButton ? this.settings.demo : null,
                    this.showTestButton ? this.settings.tests : null,
                    this.showForceButton ? this.settings.force : null,
                    this.showPauseButton ? this.settings.pause : null,
                    this.showReplayButton ? this.settings.replay : null,
                    this.showGREGButton ? this.settings.GREG : null,
                    this.showTimeButton ? this.settings.time : null,
                    this.showHideButton ? this.settings.hide : null,
                    this.showScreenButton ? this.settings.screen : null,
                    this.showMusicMuteButton ? this.settings.muteMusic : null,
                    this.showDumpButton ? this.settings.dump : null,
                    this.showToggleNTButton ? ntButton : null
                ].filter((b) => b != null),
                dock: Flow.Dock.Float
            });
            this._ui.onButtonClicked(this.handleButtonClicked);
            this._ui.onCollapsed(this.handleUICollapsed);
            this._ui.onOpened(this.handleUIOpened);

            const ticker = RS.ITicker.get();
            ticker.timeScale = RS.URL.getParameterAsNumber("timescale", ticker.timeScale);

            // Restore hidden state
            if (this._isHidden)
            {
                this._ui.alpha = 0.0;
                this._ui.visible = false;
                this._ui.collapsed = true;
            }

            if (this._musicVolumeHandle && this.showMusicMuteButton)
            {
                const index = this._ui.settings.buttons.indexOf(this.settings.muteMusic);
                this._ui.getButton(index).text = "MUSIC OFF";
            }
        }

        @Callback
        protected async handleButtonClicked(index: number)
        {
            const settings = this._ui.settings.buttons[index];

            if (settings === this.settings.force)
            {
                if (this._reels.rigToolVisible)
                {
                    this._reels.rigToolVisible = false;
                    this._ui.getButton(index).text = this.settings.force.text;

                    if (this.settings.reelsetSpinner != null && this.settings.engine.models.config.reelSets.length > 1)
                    {
                        this.settings.engine.force({ stopIndices: this._reels.stopIndices, reelSetIndex: this._reelSetSpinner.value as number });

                        this._reelSetSpinner = null;
                        this._reelSetSpinnerText = null;
                    }
                    else
                    {
                        this.settings.engine.force({ stopIndices: this._reels.stopIndices });
                    }

                    if (this._canSpinHandle)
                    {
                        this._canSpinHandle.dispose();
                        this._canSpinHandle = null;
                    }
                }
                else
                {
                    this._reels.rigToolVisible = true;
                    const forceBtn = this._ui.getButton(index);
                    forceBtn.text = this.settings.applyText;

                    if (this.settings.reelsetSpinner != null && this.settings.engine.models.config.reelSets.length > 1)
                    {
                        const lastResultIndex = this.settings.engine.models.spinResults.length - 1;
                        const reelSetIndex = lastResultIndex >= 0 ? this.settings.engine.models.spinResults[lastResultIndex].reels.currentReelSetIndex : 0;
                        this._reelSetSpinner = Flow.spinner.create(
                        {
                            ...this.settings.reelsetSpinner,
                            options:
                            {
                                type: Flow.Spinner.Type.Numeric,
                                minValue: 0,
                                maxValue: this.settings.engine.models.config.reelSets.length - 1,
                                wrap: true,
                                initialValue: reelSetIndex
                            }
                        }, forceBtn);
                        this._reelSetSpinner.x = forceBtn.layoutSize.w;
                        this._reelSetSpinner.onValueChanged(this.handleReelsetChange);

                        this._reelSetSpinnerText = Flow.button.create(this.settings.reelsetSpinnerText, this._reelSetSpinner)
                        this._reelSetSpinnerText.cursor = RS.Rendering.Cursor.Default;

                        this._reelSetSpinnerText.invalidateLayout();
                    }

                    if (this.settings.canSpinArbiter)
                    {
                        this._canSpinHandle = this.settings.canSpinArbiter.declare(false);
                    }
                }
            }
            else
            {
                await super.handleButtonClicked(index);
            }
        }

        @Callback
        protected async handleUICollapsed()
        {
            await super.handleUICollapsed();

            if (this._reels.rigToolVisible)
            {
                this._reels.rigToolVisible = false;
                for (let i = 0; i < this._ui.settings.buttons.length; i++)
                {
                    if (this._ui.settings.buttons[i] === this.settings.force)
                    {
                        this._ui.getButton(i).text = this.settings.force.text;
                        break;
                    }
                }
                this._canSpinHandle = null;
            }
        }

        protected hideUI(): void
        {
            if (this._isHidden) { return; }
            // Hide the rig tool
            if (this._reels)
            {
                this._reels.rigToolVisible = false;
                let index: number = 0;
                let button: RS.DevTools.UI.Button = this._ui.getButton(index);
                while (button != null)
                {
                    if (button.settings === this.settings.force)
                    {
                        button.text = this.settings.force.text;
                        break;
                    }
                    button = this._ui.getButton(++index);
                }
            }
            this._canSpinHandle = null;
            super.hideUI();
        }

        @Callback
        protected handleReelsetChange(newValue: number)
        {
            const blankStopIndicies = [...this.reels.stopIndices];
            for (let i = 0; i < blankStopIndicies.length; i++)
            {
                blankStopIndicies[i] = 0;
            }
            this.reels.setReelSet(this.settings.engine.models.config.reelSets[newValue]);
            this.reels.setReels(blankStopIndicies);
        }
    }

    IController.register(Controller);
}
