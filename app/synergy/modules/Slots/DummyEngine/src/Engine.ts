namespace RS.Slots
{
    /**
     * Encapsulates a local engine with no actual server communication.
     * This can be configured to match the requirements of a game (e.g. specific symbols, bandsets and winlines) via settings.
     */
    export class DummyEngine<TModels extends Slots.Models = Slots.Models, TSettings extends DummyEngine.Settings = DummyEngine.Settings> extends RS.DummyEngine<TModels, TSettings>
    {
        /** Published when a spin response has been received and fully processed. */
        public readonly onSpinResponseReceived = createEvent<Engine.IResponse>();

        /** Gets if this engine has reel sets. */
        public readonly hasReelSets: boolean = true;

        protected _bandSet: Slots.ReelSet;
        protected _force: OneOrMany<Engine.ForceResult>;

        public constructor(public readonly settings: TSettings, public readonly models: TModels)
        {
            super(settings, models);
            this._bandSet = new Slots.ReelSet(settings.bandSet);
        }

        public async spin(payload: Engine.ISpinRequest.Payload): Promise<RS.Engine.IResponse>
        {
            const error = this.getSpinError();
            if (error) { return error; }

            // Validate everything
            if (!this.validatePaylineCount(payload.paylineCount)) { return this.error(`Invalid payline count ${payload.paylineCount}`, false); }
            if (!this.validateStake(payload.betAmount)) { return this.error(`Invalid bet amount ${payload.betAmount}`, false); }

            // Ensure sufficient funds
            const totalBet = Models.Stake.toTotal(this.models.stake, payload.betAmount);
            if (totalBet.value > this._balance) { return this.error("Insufficient funds to place this bet", false); }

            // Consume stake
            this._balance -= totalBet.value;

            let accumulatedWin = 0;
            if (this._gameState === RS.Models.State.Type.Open && this.models.spinResults.length > 0)
            {
               // Track accumulated win from last spin.
                const lastSpin = this.models.spinResults[this.models.spinResults.length - 1];
                accumulatedWin = lastSpin.win.accumulatedWin;
            }

            // Clear error and advance state
            this.models.error.type = RS.Models.Error.Type.None;
            this.models.state.state = this._gameState = RS.Models.State.Type.Open;

            const response = await this.logic(payload, accumulatedWin);

            // Update balance
            RS.Models.Balances.clear(this.models.customer.finalBalance);
            this.models.customer.finalBalance.primary = this._balance;

            // Store recovery data
            RS.Storage.save("DummyEngine/RecoveryData", this.captureDataForRecovery());

            // Done, remove first force result
            if (Is.array(this._force) && this._force.length > 0)
            {
                this._force.shift();
            }
            else
            {
                this._force = null;
            }

            this.onSpinResponseReceived.publish(response);
            return response;
        }

        /**
         * Sends a close request.
         * Async.
         */
        public async close(): Promise<RS.Engine.IResponse>
        {
            const error = this.getCloseError();
            if (error) { return error; }

            // Close game
            this.models.error.type = RS.Models.Error.Type.None;
            this.models.state.state = this._gameState = Models.State.Type.Closed;
            Models.FreeSpins.clear(this.models.freeSpins);

            // Store recovery data
            RS.Storage.save("DummyEngine/RecoveryData", this.captureDataForRecovery());

            // Done
            const dummyResponse = { isError: false, request: null };
            this.onCloseResponseReceived.publish(dummyResponse);
            return dummyResponse;
        }

        /**
         * Gets an array of all triggerable replays.
         */
        public async getReplayData() { return {}; }

        /**
         * Gets an array of all GREG replays.
         */
        public async getGREGData() { return {}; }

        /** Returns whether or not the given paylineCount is a valid payline count. */
        protected validatePaylineCount(paylineCount: number)
        {
            return paylineCount === this.settings.winlines.length;
        }

        /** Returns whether or not the given betAmount is a valid stake. */
        protected validateStake(betAmount: Models.StakeAmount)
        {
            const model = this.models.stake;
            const totalBet = Models.Stake.toTotal(model, betAmount);
            return model.override != null || this.settings.betOptions.indexOf(totalBet.value) !== -1;
        }

        protected configureCustomerModel(model: RS.Models.Customer): void
        {
            model.finalBalance.primary = this._balance;
            model.currencyCode = "GBP";
            model.currencyMultiplier = 1.0;
        }

        protected configureConfigModel(model: Models.Config): void
        {
            super.configureConfigModel(model);

            const initialStops = this.getStops(false);

            model.reelSets = [ this._bandSet ];
            model.symbolNames = this.settings.symbols.map((s) => s.name);
            model.symbolPayouts = this.settings.symbols.map((s) => s.payouts);
            model.symbolPayoutTables = [model.symbolPayouts];
            model.winlines = this.settings.winlines;
            model.extraWinlines = null;
            model.defaultReels =
            {
                currentReelSetIndex: 0,
                stopIndices: initialStops,
                symbolsInView: this.sampleStops(initialStops)
            };
            model.bigBetEnabled = false;
        }

        /** Configures the stake model. */
        protected configureStakeModel(model: Models.Stake): void
        {
            model.betOptions = this.settings.betOptions.map((value) =>
            {
                return {
                    type: RS.Slots.Models.Stake.Type.Total,
                    value
                };
            });

            model.defaultBetIndex = this.settings.defaultBetOption;
            model.currentBetIndex = this.settings.defaultBetOption;
            model.paylineOptions = [ this.settings.winlines.length ];
            model.defaultPaylineIndex = 0;
            model.currentPaylineIndex = 0;

            this.applyBetConfiguration(model);

            RS.assert(this.models.stake.betOptions.length > 0, "models.stake must have one or more bet options");
        }

        /** Applies custom bet configuration. Simulates the effects of BetConfig. */
        protected applyBetConfiguration(model: Models.Stake)
        {
            const minBet = URL.getParameterAsNumber("minbet", null);
            const maxBet = URL.getParameterAsNumber("maxbet", null);
            Find.remove(model.betOptions, (betOption) =>
            {
                const value = Models.Stake.toTotal(model, betOption).value;
                return (minBet != null && value < minBet) || (maxBet != null && value > maxBet);
            });

            model.defaultBetIndex = Math.clamp(model.defaultBetIndex, 0, model.betOptions.length - 1);
            model.currentBetIndex = Math.clamp(model.currentBetIndex, 0, model.betOptions.length - 1);
        }

        /**
         * Gets an error to be returned when attempting to spin.
         */
        protected getSpinError(): RS.Engine.IResponse | null
        {
            if (this.models.freeSpins.used > 0 || this.models.freeSpins.remaining > 0)
            {
                if (this.models.freeSpins.remaining == null || this.models.freeSpins.remaining <= 0)
                {
                    return this.error("Can't spin when there are no free spins remaining.", true);
                }

                if (this.models.state.isMaxWin)
                {
                    return this.error("Can't spin when max win has been achieved.", true);
                }
            }
            else if (this._gameState !== RS.Models.State.Type.Closed)
            {
                return this.error("Can't start a new wager when the previous wager has not been closed.", true);
            }
        }

        /**
         * Gets an error to be returned when attempting to close.
         */
        protected getCloseError(): RS.Engine.IResponse | null
        {
            if (this.models.freeSpins.remaining > 0 && !this.models.state.isMaxWin)
            {
                return this.error("Can't close when there are free spins remaining.", true);
            }
        }

        /**
         * Gets a set of stop indices.
         * @param random
         */
        protected getStops(random: boolean): number[]
        {
            const result = new Array<number>(this.settings.reelCount);
            for (let i = 0; i < this.settings.reelCount; ++i)
            {
                const band = this.settings.bandSet[i];
                result[i] = random ? this._rng.randomIntRange(0, band.length) : 0;
            }
            return result;
        }

        /**
         * Samples a set of stop indices into a reel set.
         * @param stops
         */
        protected sampleStops(stops: ReadonlyArray<number>): Slots.ReelSet
        {
            return this._bandSet.extractSubset(stops, this.settings.rowCount);
        }

        /**
         * Captures the current state of play into an arbitrary object for recovery.
         */
        protected captureDataForRecovery(): DummyEngine.RecoveryData
        {
            // If we're closed, there is no recovery data
            if (this._gameState === RS.Models.State.Type.Closed)
            {
                return {
                    isOpen: false,
                    balance: this._balance
                };
            }

            // Encode data we need to reproduce the game result
            const totalBet = Models.Stake.toTotal(this.models.stake, this.models.stake.betOptions[this.models.stake.currentBetIndex]);
            const spinResult = this.models.spinResults[0];
            const accumulatedWin = spinResult.win.accumulatedWin - spinResult.win.totalWin;
            return {
                isOpen: true,
                bandSetIndex: spinResult.reels.currentReelSetIndex,
                reelStops: spinResult.reels.stopIndices,
                totalStake: totalBet.value,
                balance: this._balance,
                initialBalance: spinResult.balanceBefore.primary,
                freeSpins: { remaining: this.models.freeSpins.remaining || 0, used: this.models.freeSpins.used || 0 },
                accumulatedWin: accumulatedWin
            };
        }

        /**
         * Restores the state of play from a previously captured recovery object.
         * @param data
         */
        protected recover(data: DummyEngine.RecoveryData): void
        {
            Models.SpinResults.clear(this.models.spinResults);
            if (data.isOpen === true)
            {
                this.recoverOpenGame(data);
            }
            else if (data.isOpen === false)
            {
                this.recoverClosedGame(data);
            }
            this.models.state.state = this._gameState;
            RS.Models.Balances.clear(this.models.customer.finalBalance);
            this.models.customer.finalBalance.primary = this._balance;
        }

        protected recoverOpenGame(data: DummyEngine.RecoveryData.Open)
        {
            // Free-spins to be re-awarded by doLogic if we aren't past the awarding spin.
            if (data.freeSpins.used > 0)
            {
                // Recover to previous free-spin.
                this.models.freeSpins.used = data.freeSpins.used - 1;
                this.models.freeSpins.remaining = data.freeSpins.remaining + 1;
                this.models.freeSpins.isFreeSpins = true;
            }

            // Recover balance and state
            this._balance = data.initialBalance;
            this._gameState = RS.Models.State.Type.Open;

            // Recover stake
            const totalStake: RS.Slots.Models.StakeAmount = { type: Models.Stake.Type.Total, value: data.totalStake };
            const betPerLine = RS.Slots.Models.Stake.toPerLine(this.models.stake, totalStake);
            let found = false;
            this.models.stake.override = null;
            for (let i = 0, l = this.models.stake.betOptions.length; i < l; ++i)
            {
                const betOption = RS.Slots.Models.Stake.toTotal(this.models.stake, this.models.stake.betOptions[i]);
                if (betOption.value === totalStake.value)
                {
                    this.models.stake.currentBetIndex = i;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                // Set override stake since it's not in our stake list
                this.models.stake.override = { totalBet: totalStake.value, betPerLine: betPerLine.value };

                // Set the stake index to the nearest one in our stake list
                const validIndices = Object.keys(this.models.stake.betOptions);
                const nearestIndex = Find.closestToZero(validIndices, (index) => RS.Slots.Models.Stake.toTotal(this.models.stake, this.models.stake.betOptions[index]).value - totalStake.value);
                this.models.stake.currentBetIndex = parseInt(nearestIndex);
            }

            // Force the stored result
            this._force = {
                reelSetIndex: data.bandSetIndex,
                stopIndices: data.reelStops
            };

            // Replay the game
            this.doLogic(betPerLine.value, data.accumulatedWin);
        }

        protected recoverClosedGame(data: DummyEngine.RecoveryData.Closed)
        {
            this._balance = data.balance;
            this._gameState = RS.Models.State.Type.Closed;
        }

        /**
         * Sends an spin request.
         * Async.
         */
        protected async logic(payload: Engine.ISpinRequest.Payload, accumulatedWin: number = 0): Promise<RS.Engine.IResponse>
        {
            const stakePerLine = Models.Stake.toPerLine(this.models.stake, payload.betAmount);
            // Do logic
            try
            {
                this.doLogic(stakePerLine.value, accumulatedWin);
            }
            catch (err)
            {
                Log.error(err);
                this.models.state.state = this._gameState = RS.Models.State.Type.Closed;
                return this.error(`${err}`, false);
            }

            const dummyResponse = { isError: false, request: null };
            return dummyResponse;
        }

        /**
         * Performs logic for a single play.
         * @param stakePerLine
         * @param accumulatedWin total win up to (but NOT including) this spin
         */
        protected doLogic(stakePerLine: number, accumulatedWin?: number): void
        {
            // Spin reels, generate spin result
            this.models.spinResults.length = 1;
            this.models.spinResults[0] = this.generateResults(stakePerLine, accumulatedWin);

            // Award winnings
            this._balance += this.models.spinResults[0].win.totalWin;
            this.models.spinResults[0].balanceAfter.primary = this._balance;

            // Check for max win state.
            this.models.state.isMaxWin = this.checkMaxWin();

            // Continues any free-spins set previously.
            if (this.models.freeSpins.remaining > 0 && !this.models.state.isMaxWin)
            {
                this.continueFreeSpins();
            }
            else
            {
                this.models.freeSpins.isFreeSpins = this.models.freeSpins.used > 0;
            }
        }

        /**
         * Continues free-spins, if there are free-spins remaining.
         */
        protected continueFreeSpins(): void
        {
            // Progress on to next free-spin.
            this.models.freeSpins.isFreeSpins = true;
            this.models.freeSpins.remaining--;
            this.models.freeSpins.used = (this.models.freeSpins.used || 0) + 1;
        }

        protected checkMaxWin(): boolean
        {
            const win = this.models.spinResults[this.models.spinResults.length - 1].win.accumulatedWin;
            return win >= this.models.config.maxWinAmount;
        }

        /**
         * Spins the reels and returns the result.
         * @param stakePerLine
         * @param accumulatedWin total win up to (but NOT including) this spin
         */
        protected generateResults(stakePerLine: number, accumulatedWin?: number): Slots.Models.SpinResult
        {
            const force = Is.array(this._force) && this._force.length > 0 ? this._force[0] : this._force as Engine.ForceResult;

            // Determine stops
            const stops = force ? [...force.stopIndices] : this.getStops(true);
            const symbolsInView = this.sampleStops(stops);

            // Compute wins
            const paylineWins = this.computePaylineWins(stakePerLine, symbolsInView);

            // Assemble spin result
            const totalWin = paylineWins.map((win) => win.totalWin).reduce((a, b) => a + b, 0);
            const spinResult: Slots.Models.SpinResult =
            {
                index: 0,
                reels:
                {
                    stopIndices: stops,
                    symbolsInView: symbolsInView,
                    currentReelSetIndex: 0
                },
                win:
                {
                    totalWin: totalWin,
                    accumulatedWin: Math.min(accumulatedWin + totalWin, this.models.config.maxWinAmount),
                    paylineWins: paylineWins
                },
                balanceBefore: { primary: this._balance, named: {} },
                balanceAfter: { primary: this._balance, named: {} }
            };
            return spinResult;
        }

        /**
         * Computes all payline wins from the specified reel set.
         * @param reelSet
         */
        protected computePaylineWins(stakePerLine: number, reelSet: ReelSet): Models.PaylineWin[]
        {
            const result: Models.PaylineWin[] = [];
            for (let lineIndex = 0; lineIndex < this.settings.winlines.length; ++lineIndex)
            {
                const winline = this.settings.winlines[lineIndex];
                let matchSymbolID: number|null = null;
                let matchCount = 0;
                let matchWild = false;
                for (const pos of winline)
                {
                    const symbolID = reelSet.get(pos.reelIndex, pos.rowIndex);
                    const sym = this.settings.symbols[symbolID];
                    if (matchSymbolID == null)
                    {
                        matchSymbolID = symbolID;
                        if (sym.isWild) { matchWild = true; }
                        matchCount++;
                    }
                    else if (matchSymbolID !== symbolID && !sym.isWild && !matchWild)
                    {
                        break;
                    }
                    else
                    {
                        if (!sym.isWild)
                        {
                            matchWild = false;
                            matchSymbolID = symbolID;
                        }
                        matchCount++;
                    }
                }
                if (matchSymbolID != null)
                {
                    const sym = this.settings.symbols[matchSymbolID];
                    if (sym.payouts)
                    {
                        const multiplier = sym.payouts[matchCount - 1];
                        if (multiplier > 0)
                        {
                            result.push({
                                lineIndex,
                                multiplier,
                                totalWin: multiplier * stakePerLine,
                                positions: winline.slice(0, matchCount),
                                symbolID: matchSymbolID,
                                reverse: false
                            });
                        }
                    }
                    else if (sym.isWild)
                    {
                        // Found a winline of wilds and wilds don't payout
                        // TODO: Find largest paying symbol that wilds substitute for
                    }
                }
            }
            return result;
        }

        /**
         * Gets the number of scatter symbols on the reel set.
         * @param reelSet
         */
        protected getScatterCount(reelSet: RS.Slots.ReelSet): number
        {
            let count = 0;
            reelSet.iterate((reelIndex, rowIndex, symbolID) =>
            {
                if (this.settings.symbols[symbolID].isScatter)
                {
                    ++count;
                }
            });
            return count;
        }

        /**
         * Generates a dummy error response.
         * @param message
         * @param fatal
         */
        protected error(message: string, fatal: boolean): RS.Engine.IResponse
        {
            this.models.error.type = RS.Models.Error.Type.Server;
            this.models.error.title = "DUMMY ENGINE ERROR";
            this.models.error.message = message;
            this.models.error.options = fatal
                ? [ { type: RS.Models.Error.OptionType.Close, text: "OK" } ]
                : [ { type: RS.Models.Error.OptionType.Continue, text: "OK" } ];
            return { isError: true, request: null };
        }

        protected getResultsModel(models: TModels): Models.SpinResults
        {
            return models.spinResults;
        }

        protected clearResultsModel(models: TModels): void
        {
            Models.SpinResults.clear(this.getResultsModel(models));
        }
    }

    export namespace DummyEngine
    {
        export namespace RecoveryData
        {
            export interface Base
            {
                isOpen: boolean;
                balance: number;
            }

            export interface Open extends Base
            {
                isOpen: true;
                totalStake: number;
                bandSetIndex: number;
                reelStops: number[];
                initialBalance: number;
                freeSpins: { remaining: number; used: number; };
                accumulatedWin: number;
            }

            export interface Closed extends Base
            {
                isOpen: false;
            }
        }

        export type RecoveryData = RecoveryData.Open | RecoveryData.Closed;

        export interface DummySymbol
        {
            name: string;
            payouts?: number[];
            isWild?: boolean;
            isScatter?: boolean;
        }

        export interface Settings extends RS.DummyEngine.Settings
        {
            reelCount: number;
            rowCount: number;
            symbols: DummySymbol[];
            winlines: Models.Winline[];
            betOptions: number[];
            defaultBetOption: number;
            bandSet: number[][];
        }

        export namespace Util
        {
            export namespace WinlineShapes
            {
                export const Flat = [0, 0, 0, 0, 0];

                export const VeeUp = [0, 1, 2, 1, 0];
                export const VeeDown = [0, -1, -2, -1, 0];

                export const ZigZagUp = [0, 1, 0, 1, 0];
                export const ZigZagDown = [0, -1, 0, -1, 0];

                export const Hill = [0, 1, 1, 0, -1];
                export const Valley = [0, -1, -1, 0, 1];

                export const Drop = [0, 0, 0, -1, -2];
                export const Climb = [0, 0, 0, 1, 2];

                export const Default =
                [
                    Flat,
                    VeeUp, VeeDown,
                    ZigZagUp, ZigZagDown,
                    Hill, Valley,
                    Drop, Climb
                ];
            }

            /**
             * Generates a random winline shape.
             * @param reelCount Number of reels
             * @param verticalRange Maximum vertical step between reels
             */
            export function generateWinlineShape(reelCount: number, verticalRange: number = 1): number[]
            {
                const shape = [0];
                for (let i = 1; i < reelCount; i++)
                {
                    const step = verticalRange > 0 ? Math.generateRandomInt(-verticalRange, verticalRange + 1) : 0;
                    shape[i] = shape[i - 1] + step;
                }
                return shape;
            }
            /**
             * Generates a random array of winlines.
             * @param reelCount Number of reels
             * @param rowCount Number of rows
             * @param shapes Array of different possible winline shapes, provided as a reelCount-length array of positions relative to the first
             */
            export function generateWinlines(reelCount: number, rowCount: number,
                                             shapes: number[][] = WinlineShapes.Default): Models.Winline[]
            {
                const winlines: Models.Winline[] = [];
                for (let rowID = 0; rowID < rowCount; rowID++)
                {
                    const theseWinLines: Models.Winline[] = [];
                    for (const shape of shapes)
                    {
                        const winline: Models.Winline = [];
                        const baseOffset = shape[0] || 0;

                        let lowest: number;
                        let highest: number;
                        for (let reelID = 0; reelID < shape.length; reelID++)
                        {
                            const offset = shape[reelID] - baseOffset;
                            const offsetRowID = rowID + offset;
                            if (lowest == null || offsetRowID < lowest) { lowest = offsetRowID; }
                            if (highest == null || offsetRowID > highest) { highest = offsetRowID; }

                            winline.push({ reelIndex: reelID, rowIndex: offsetRowID });
                        }

                        if (lowest >= 0 && highest < rowCount && RS.Util.indexOf(winline, theseWinLines, (a, b) => Models.Winline.equals(a, b)) === -1)
                        {
                            theseWinLines.push(winline);
                            winlines.push(winline);
                        }
                    }
                }

                return winlines;
            }
        }

        export const defaultSettings: Settings =
        {
            ...RS.DummyEngine.defaultSettings,
            reelCount: 5,
            rowCount: 3,
            symbols:
            [
                /*  0 */ { name: "Jack", payouts: [ 0, 0, 3, 15, 40 ] },
                /*  1 */ { name: "Queen", payouts: [ 0, 0, 5, 20, 50 ] },
                /*  2 */ { name: "King", payouts: [ 0, 0, 8, 25, 80 ] },
                /*  3 */ { name: "Ace", payouts: [ 0, 0, 10, 30, 100 ] },
                /*  4 */ { name: "Wild", isWild: true },
                /*  5 */ { name: "Scatter", isScatter: true }
            ],
            winlines:
            [
                [ { rowIndex: 0, reelIndex: 0 }, { rowIndex: 0, reelIndex: 1 }, { rowIndex: 0, reelIndex: 2 }, { rowIndex: 0, reelIndex: 3 }, { rowIndex: 0, reelIndex: 4 } ],
                [ { rowIndex: 1, reelIndex: 0 }, { rowIndex: 1, reelIndex: 1 }, { rowIndex: 1, reelIndex: 2 }, { rowIndex: 1, reelIndex: 3 }, { rowIndex: 0, reelIndex: 4 } ],
                [ { rowIndex: 2, reelIndex: 0 }, { rowIndex: 2, reelIndex: 1 }, { rowIndex: 2, reelIndex: 2 }, { rowIndex: 2, reelIndex: 3 }, { rowIndex: 2, reelIndex: 4 } ],
                [ { rowIndex: 0, reelIndex: 0 }, { rowIndex: 1, reelIndex: 1 }, { rowIndex: 2, reelIndex: 2 }, { rowIndex: 1, reelIndex: 3 }, { rowIndex: 0, reelIndex: 4 } ],
                [ { rowIndex: 2, reelIndex: 0 }, { rowIndex: 1, reelIndex: 1 }, { rowIndex: 0, reelIndex: 2 }, { rowIndex: 1, reelIndex: 3 }, { rowIndex: 2, reelIndex: 4 } ]
            ],
            bandSet:
            [
                [ 0, 0, 1, 5, 3, 2, 4, 3, 1, 2, 1, 0, 4 ],
                [ 1, 1, 1, 3, 2, 2, 4, 2, 1, 0, 0, 4, 4 ],
                [ 3, 2, 3, 5, 2, 1, 4, 0, 0, 0, 0, 0, 4, 4 ],
                [ 0, 0, 0, 1, 2, 3, 4, 2, 1, 0, 0, 4, 4, 4 ],
                [ 5, 3, 1, 2, 2, 2, 4, 2, 1, 3, 1, 0, 4, 4 ]
            ]
        };
    }
}
