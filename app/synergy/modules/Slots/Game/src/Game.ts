namespace RS.Slots
{
    /**
     * The core game class that brings everything together.
     * Is it intended that the developer extends this class and then instantiates that in an @RS.Init() handler.
     */
    @HasCallbacks
    export class Game<TSettings extends Game.Settings = Game.Settings, TContext extends Game.Context = Game.Context> extends RS.Game
    {
        public readonly settings: TSettings;

        protected _uiController: UI.IUIController;
        protected _bigWinController: BigWin.IController | null;
        protected _autoplayController: Autoplay.IController;

        protected _context: Game.Context;

        protected _models: Models;
        protected _arbiters: Game.Arbiters;
        protected _observables: Game.Observables;
        protected _engine: Engine.IEngine<Models, object>;

        @AutoDispose protected _handleFreeSpinsRemainingChanged: IDisposable;
        @AutoDisposeOnSet protected _handleInputOptionChanged: IDisposable;

        /** Gets the context. */
        public get context() { return this._context; }

        /** Gets the big win controller for this game. */
        public get bigWinController() { return this._bigWinController; }

        /** Gets the autoplay controller for this game. */
        public get autoplayController() { return this._autoplayController; }

        /** Gets the UI controller for this game. */
        public get uiController() { return this._uiController; }

        /** Gets the models for this game. */
        public get models() { return this._models; }

        /** Gets the arbiters for this game. */
        public get arbiters() { return this._arbiters; }

        /** Gets the observables for this game. */
        public get observables() { return this._observables; }

        /** Gets the engine interface for this game. */
        public get engine() { return this._engine; }

        protected createUIController()
        {
            this._uiController = RS.Slots.UI.IUIController.get();
            this._uiController.initialise({
                allowSpinArbiter: this._arbiters.canSpin,
                allowSkipArbiter: this._arbiters.canSkip,
                allowAutoplayArbiter: this._arbiters.canAutoplay,
                allowChangeBetArbiter: this._arbiters.canChangeBet,
                paytableEnabledArbiter: this._arbiters.canOpenPaytable,
                spinOrSkipArbiter: this._arbiters.spinOrSkip,
                allowQuickSpinArbiter: this._arbiters.canQuickSpin,
                enableInteractionArbiter: this._arbiters.uiEnabled,
                messageTextArbiter: this._arbiters.messageText,
                showButtonAreaArbiter: this._arbiters.showButtonArea,
                autoplaysRemainingObservable: this._observables.autoplaysRemaining,
                orientationObservable: this.orientation,
                showTurboArbiter: this._arbiters.showTurboUI,
                turboModeArbiter: this._arbiters.turboMode,
                muteArbiter: this._arbiters.mute,
                showMeterPanelArbiter: this._arbiters.showMeterPanel,
                showMessageBarArbiter: this._arbiters.showMessageBar
            });
            this._handleInputOptionChanged = this._uiController.onInputOptionChanged(this.handleInputOptionChanged);
            this._uiController.onHelpRequested(this.handleHelpRequested);
            this._uiController.onPaytableRequested(this.handlePaytableRequested);
            this._uiController.onAutoplayRequested(this.handleAutoplayRequested);
            this._uiController.onBigBetRequested(this.handleBigBetRequested);
            this._uiController.onTurboRequested(this.handleTurboRequested);
            this._uiController.onTurboStopRequested(this.handleTurboStopRequested);
            this._uiController.onSettingsRequested(this.handleSettingsRequested);
        }

        protected createControllers()
        {
            super.createControllers();

            if (this.settings.bigWin)
            {
                this._bigWinController = new this.settings.bigWin.controller(this.settings.bigWin.settings);
            }
            else
            {
                this._bigWinController = null;
            }

            this._autoplayController = Autoplay.IController.get();
            this._autoplayController.initialise({
                messageTextArbiter: this._arbiters.messageText,
                uiEnabledArbiter: this._arbiters.uiEnabled,
                autospinArbiter: this._arbiters.shouldAutospin,
                autoplaysRemainingObservable: this._observables.autoplaysRemaining,
                balanceObservable: this._observables.balance,
                totalBetObservable: this._observables.totalBet
            });

            this._minWagerDurationController = IMinWagerDurationController.get();
            this._minWagerDurationController.initialise({
                allowSpinArbiter: this._arbiters.canSpin,
                allowQuickSpinArbiter: this._arbiters.canQuickSpin
            });

            this._models.config.bigBetEnabled = RS.IPlatform.get().bigBetEnabled;
        }

        protected getMaxBet(): number
        {
            return Models.Stake.toTotal(this.models.stake, Find.highest(this.models.stake.betOptions, (v) => v.value)).value;
        }

        @Callback
        protected handleBackRequested(): void
        {
            return;
        }

        @Callback
        protected handleAutoplayRequested(): void
        {
            return;
        }

        @Callback
        protected handleBigBetRequested(): void
        {
            return;
        }

        @Callback
        protected handleViewOnlyModeStateChanged(): void
        {
            super.handleViewOnlyModeStateChanged();
            if (this._canSkipHandler)
            {
                this._canSkipHandler.dispose();
                this._canSkipHandler = null;
            }
            if (this.arbiters.viewOnlyMode.value)
            {
                this._canSkipHandler = this.arbiters.canSkip.declare(false);
            }
        }

        protected initialiseAssetPriorities()
        {
            super.initialiseAssetPriorities();
            this._assetController.setGroupPriority("rsslots", Asset.LoadPriority.Immediate);
        }

        @Callback
        protected handleInputOptionChanged(ev: UI.IUIController.InputOptionChangedData)
        {
            if (ev.userChanged === false) { return; }

            switch (ev.optionType)
            {
                case UI.InputOptionType.TotalBet:
                case UI.InputOptionType.BetPerLine:
                case UI.InputOptionType.BetMultiplier:
                    this.models.stake.currentBetIndex = ev.optionIndex;
                    break;
                case UI.InputOptionType.Lines:
                    this.models.stake.currentPaylineIndex = ev.optionIndex;
                    break;
            }

            this.models.stake.override = null;
            const lines = this.models.stake.paylineOptions[this.models.stake.currentPaylineIndex];
            const totalBet = Models.Stake.getCurrentBetAmount(this.models.stake, Models.Stake.Type.Total);
            this._uiController.setTotalBet(totalBet);
            this._uiController.setLines(lines);
            this._uiController.setInputOptions(UI.InputOptionType.TotalBet, this.models.stake.betOptions.map((o) => Models.Stake.toTotal(this.models.stake, o).value));
            this._observables.totalBet.value = totalBet;
            this._observables.win.value = 0;
        }

        protected createArbiters()
        {
            super.createArbiters();
            this._arbiters =
            {
                ...this._arbiters,
                canOpenPaytable: new Arbiter<boolean>(Arbiter.AndResolver, true),
                canAutoplay: new Arbiter<boolean>(Arbiter.AndResolver, true),
                canQuickSpin: new Arbiter<boolean>(Arbiter.AndResolver, false),
                bigWinVolume: new Arbiter<number>(Arbiter.MinResolver, 1.0),
                shouldAutospin: new Arbiter<boolean>(Arbiter.OrResolver, false)
            };

            this._arbiters.messageText.valueToString = (str) => Localisation.resolveLocalisableString(this.locale, str);
        }

        protected createObservables()
        {
            super.createObservables();
            this._observables =
            {
                ...this._observables,
                freeSpinsRemaining: new Observable<number>(0),
                autoplaysRemaining: new Observable<number>(0),
                jackpots: new ObservableMap({})
            };

            this._handleFreeSpinsRemainingChanged = this._observables.freeSpinsRemaining.onChanged(this.handleFreeSpinsRemaining);
        }

        @Callback
        protected handlePlatformGameStateChanged(newState: PlatformGameState)
        {
            if (this.settings.playerIdleTime == null) { return; }

            this._observables.playerIdleState.value = false;
            if (newState === PlatformGameState.Ready)
            {
                this._idleTimer = RS.Tween
                    .wait<Game>(this.settings.playerIdleTime, this)
                    .call(() => this._observables.playerIdleState.value = true);
            }
            else
            {
                this._idleTimer = null;
            }
        }

        @Callback
        protected handleTotalBetChanged(totalBet: number)
        {
            this.uiController.setTotalBet(totalBet);
            this.uiController.setSelectedInputValue(RS.Slots.UI.InputOptionType.TotalBet, totalBet);
        }

        @Callback
        protected handleFreeSpinsRemaining(newAmount: number)
        {
            // TODO: this
        }

        protected initialiseModels(): void
        {
            this._models = {} as Models;
            Models.clear(this._models);
        }

        protected initialisePlatform(uiContainer: RS.Rendering.IContainer = null)
        {
            super.initialisePlatform(uiContainer);
            const platform = IPlatform.get();
            platform.init({
                balanceObservable: this._observables.balance,
                balancesObservable: this._observables.balances,
                winObservable: this._observables.win,
                stakeObservable: this._observables.totalBet,
                gameStateObservable: this._observables.platformGameState,

                interactionEnabledArbiter: this._arbiters.uiEnabled,
                uiContainer: this._uiContainer,

                autoplaysRemainingObservable: this._observables.autoplaysRemaining,
                stakeChangeEnabledArbiter: this._arbiters.canChangeBet,
                paytableEnabledArbiter: this._arbiters.canOpenPaytable,
                wagerUpdateCallback: this.handlePlatformWagerRequest,
                allowAutoplayArbiter: this._arbiters.canAutoplay,
                paytableVisible: this._arbiters.paytableVisible
            });
            platform.autoplayController = this._autoplayController;
        }

        protected resetStake(): void
        {
            const model = this.models.stake;
            model.currentBetIndex = model.defaultBetIndex;
            model.currentPaylineIndex = model.defaultPaylineIndex;
            this.observables.totalBet.value = Models.Stake.toTotal(model, model.betOptions[model.defaultBetIndex]).value;
        }

        protected setStake(value: number, paylineCount?: number): void
        {
            const model = this.models.stake;

            if (paylineCount != null)
            {
                const paylineIndex = model.paylineOptions.indexOf(paylineCount);
                if (paylineIndex === -1) { throw new Game.InvalidStakeError(`Invalid payline count: ${paylineCount}`); }
                model.currentPaylineIndex = paylineIndex;
            }

            const option = Find.matching(model.betOptions, (opt) => opt.value === value);
            if (!option) { throw new Game.InvalidStakeError(`Invalid stake: ${this.currencyFormatter.format(value)} (${value})`); }
            model.currentBetIndex = model.betOptions.indexOf(option);

            const totalBet = Models.Stake.toTotal(model, option);
            this.observables.totalBet.value = totalBet.value;
        }

        protected setTotalBet(totalBet: number): void
        {
            const model = this.models.stake;

            const option = Find.matching(model.betOptions, (opt) => Models.Stake.toTotal(model, opt).value === totalBet);
            if (!option) { throw new Game.InvalidStakeError(`Invalid total bet: ${this.currencyFormatter.format(totalBet)} (${totalBet})`); }
            model.currentBetIndex = model.betOptions.indexOf(option);

            this.observables.totalBet.value = totalBet;
        }

        protected dump(target: RS.ISubDump)
        {
            super.dump(target);
            target.add(
                {
                    models: this.models,
                    currentState: this.stateMachine.currentState.name,
                    observables: this.observables,
                    arbiters: this.arbiters
                },
                (key, value) => value instanceof Object
            );
        }

        @Callback
        private async handlePlatformWagerRequest(updateRequest: PlatformWagerUpdate)
        {
            // Wait for a valid stake update point.
            await this.observables.platformGameState.for(PlatformGameState.Ready);

            // Update our stake accordingly.
            switch (updateRequest.type)
            {
                case PlatformWagerUpdate.Type.Reset:
                {
                    switch (updateRequest.resetType)
                    {
                        case PlatformWagerReset.Type.Default:
                        {
                            this.resetStake();
                            break;
                        }
                    }
                    break;
                }

                case PlatformWagerUpdate.Type.Stake:
                {
                    try
                    {
                        this.setStake(updateRequest.stake, updateRequest.paylineCount);
                    }
                    catch (e)
                    {
                        if (e instanceof Game.InvalidStakeError)
                        {
                            throw new PlatformWagerUpdate.InvalidStakeError("Invalid stake");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                    break;
                }

                case PlatformWagerUpdate.Type.TotalBet:
                {
                    try
                    {
                        this.setTotalBet(updateRequest.totalBet);
                    }
                    catch (e)
                    {
                        if (e instanceof Game.InvalidStakeError)
                        {
                            throw new PlatformWagerUpdate.InvalidStakeError("Invalid total bet");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                    break;
                }
            }
        }
    }

    export namespace Game
    {
        export class InvalidStakeError extends BaseError
        {
            constructor(message: string)
            {
                super(message);
            }
        }

        /**
         * Contains state arbiters for the game.
         */
        export interface Arbiters extends RS.Game.Arbiters
        {
            /** Indicates whether the user can change their bet amount or not. */
            canOpenPaytable: Arbiter<boolean>;

            /** Indicates whether the skip button should display as "spin" instead - when clicked this will action a skip immediately and then queue up a spin. */
            canQuickSpin: Arbiter<boolean>;

            /** The volume the big win audio should be at. */
            bigWinVolume: Arbiter<number>;

            /** Indicates whether or not the autoplay button should be enabled. */
            canAutoplay: Arbiter<boolean>;

            /** Indicates whether the game should spin on behalf of the user (e.g. autospins or free spins). */
            shouldAutospin: Arbiter<boolean>;

            /** Any status text that should be displayed. */
            messageText: Arbiter<Localisation.LocalisableString>;
        }

        /**
         * Contains common observables for the game.
         * These are NOT models but instead values to display on the UI, or be passed to the platform.
         */
        export interface Observables extends RS.Game.Observables
        {
            /** Current number of free spins remaining. */
            freeSpinsRemaining: Observable<number>;

            /** Current number of autoplays remaining. */
            autoplaysRemaining: Observable<number>;

            /** Map of named jackpots. */
            jackpots: ObservableMap<number>;
        }

        export interface Context extends RS.Game.Context
        {
            game: Game;
            primaryReels: Reels.IGenericComponent;
            winRenderer?: WinRenderers.Base | WinRenderers.Base[];
            cycledWinRenderer?: WinRenderers.Base;
            scatterAnimator?: WinRenderers.IScatterAnimator;
            stickySymbols?: WinRenderers.IStickySymbols;
            features?: IFeature[];
        }

        export interface BigWin
        {
            controller: RS.BigWin.ControllerConstructor;
            settings: RS.BigWin.IController.Settings;
        }

        export interface Settings extends RS.Game.Settings
        {
            /** Constructor and settings for the big win controller to use. */
            bigWin?: BigWin;

            engine: Engine.EngineType<Models, object>;

            /** Losing force for local balance tweaking */
            loseForce?: Engine.ForceResult;
        }
    }
}
