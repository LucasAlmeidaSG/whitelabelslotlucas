namespace RS.Slots
{
    /**
     * Represents the logic behind a specific feature.
     */
    export interface IFeature
    {
        /** Whether or not autoplay should ignore this feature and not stop. */
        isNotStoppingAutoplay?: boolean;

        /** Whether or not this feature should disable turbo mode. */
        stopsTurboMode?: boolean;

        /** Called before anything else when this feature is created from a restored game. Optionally returns state to move to. */
        onResume(): PromiseLike<string|null>;

        /** Called when the resume dialog is closed after the main view has been opened. Optionally returns state to move to. */
        onResumeDialogClosed(): PromiseLike<string|null>;

        /** Called to adjust the stop symbols of the reels before the reels have spun down. Useful for rigging certain results. */
        adjustReels(reels: RS.Reels.IGenericComponent): void;

        /** Called when the reels have reached maximum speed. Reels won't spin down until this is complete. Optionally returns state to move to. */
        onMidSpin(): PromiseLike<string|null>;

        /** Called when the reels have finished spinning down, but before big win or win rendering. Optionally returns state to move to. */
        onEndSpin(): PromiseLike<string|null>;

        /** Called just before win rendering has started. */
        onPreWinRendering(): PromiseLike<void>;

        /** Called after win rendering has completed. Optionally returns state to move to. */
        onEndWinRendering(): PromiseLike<string|null>;

        /** Called right at the end of the entire play, before moving to the next spin. Optionally returns state to move to. */
        onEndPlay(): PromiseLike<string|null>;
    }
}