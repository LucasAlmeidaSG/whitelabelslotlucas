defineStep("WaitForState", function (name: string)
{
    if (!RS.Is.string(name)) { throw new TypeError(`${name} is not a string`); }
    return {
        message: `Wait for ${name}`,
        run: async function ({ driver })
        {
            const element = await driver.findElement(selenium.By.name("Test Name"));
            await driver.wait(selenium.until.elementTextIs(element, name));
        }
    }
});

defineStep("Load", "Load the game", async function ({ driver, url })
{
    // Open the page
    await driver.get(url);

    // Wait for game to create Selenium element
    await RS.withTimeout(driver.wait(selenium.until.elementLocated(selenium.By.name("Test Name"))), 500)
});

defineStep("Spin", "Spin the reels", async function ({ driver })
{
    await Selenium.clickElement(driver, "SpinButton");
});