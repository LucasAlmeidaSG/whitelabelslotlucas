defineStep("OpenPaytable", "Open the paytable", async function ({ driver })
{
    await Selenium.clickElement(driver, "HelpButton");
});

defineStep("ScreenshotPages", "Capture a screenshot of all paytable pages", async function ({ driver })
{
    const element = await driver.findElement(selenium.By.name("Test Name"));

    const io: PromiseLike<any>[] = [];
    do
    {
        const screenshot = await driver.takeScreenshot();
        const base64 = Buffer.from(screenshot.replace(/^data:image\/png;base64,/, ""), "base64");
        const destPath2 = RS.Path.combine(".", await element.getText() + ".png");
        io.push(RS.FileSystem.writeFile(destPath2, base64));

        await Selenium.clickElement(driver, "NextHelpButton");
    }
    while (await element.getText() !== "0");

    await Promise.all(io);
});

defineStep("OpenExternalHelp", "Open external help", async function ({ driver })
{
    const targetRect = await driver.manage().window().getRect();

    await driver.wait(selenium.until.elementLocated(selenium.By.name("ExternalHelpButton")));
    await Selenium.clickElement(driver, "ExternalHelpButton");

    do { /**/ } while ((await driver.getAllWindowHandles()).length === 1);

    const currentHandle = await driver.getWindowHandle();
    const windowHandles = await driver.getAllWindowHandles();
    for (const handle of windowHandles)
    {
        if (handle === currentHandle) { continue; }
        await driver.switchTo().window(handle);
        break;
    }

    await driver.manage().window().setRect(targetRect);
});

defineStep("CloseExternalHelp", "Open external help", async function ({ driver })
{
    const currentHandle = await driver.getWindowHandle();
    const windowHandles = await driver.getAllWindowHandles();
    await driver.executeScript("close();");
    for (const handle of windowHandles)
    {
        if (handle === currentHandle) { continue; }
        await driver.switchTo().window(handle);
        return;
    }
});

defineStep("ScreenshotHelp", "Screenshot help", async function ({ driver })
{
    const scrollStep= await driver.executeScript<[number, number]>(
        `var w = document.getElementById("wrapper");
         return w.clientHeight;`);
    
    let page = 0;
    do
    {
        const screenshot = await driver.takeScreenshot();
        const base64 = Buffer.from(screenshot.replace(/^data:image\/png;base64,/, ""), "base64");
        const destPath2 = RS.Path.combine(".", `test${++page}.png`);
        await RS.FileSystem.writeFile(destPath2, base64);

        await driver.executeScript(`document.getElementById('wrapper').scrollBy(0, ${scrollStep})`);
    }
    while (await driver.executeScript(
        `var w = document.getElementById("wrapper");
         return w.scrollTop !== w.scrollHeight - w.clientHeight;`));

    {
        const screenshot = await driver.takeScreenshot();
        const base64 = Buffer.from(screenshot.replace(/^data:image\/png;base64,/, ""), "base64");
        const destPath2 = RS.Path.combine(".", `test${++page}.png`);
        await RS.FileSystem.writeFile(destPath2, base64);
    }
});

defineCase("ScreenshotAll")

    .addStep("Load")
    .addStep("WaitForState", "idle")

    .addStep("OpenExternalHelp")
    .addStep("ScreenshotHelp")
    .addStep("CloseExternalHelp")

    .addStep("Screenshot", "./landscape.png")
    .addStep("Rotate")
    .addStep("Screenshot", "./portrait.png")

    .addStep("OpenPaytable")
    .addStep("ScreenshotPages")