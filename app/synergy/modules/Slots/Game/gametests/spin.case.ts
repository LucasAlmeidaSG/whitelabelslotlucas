defineCase("Spin")

    // Open game
    .addStep("Load")
    .addStep("WaitForState", "idle")

    .addStep("Note", "BalanceText", "InitialBalance")
    .addStep("Note", "TotalBetText", "InitialStake")

    .addStep("Force", "1", "2", "3", "4", "5")

    .addStep("Spin")
    .addStep("WaitForState", "spinning")

    .addStep("Expect", "BalanceText", "Equals", "InitialBalance", "Minus", "TotalBetText")
    .addStep("Expect", "TotalBetText", "Equals", "InitialStake")
    .addStep("Expect", "WinText", "Equals", "0")

    .addStep("WaitForState", "idle");
