defineCase("Recovery")
    
    // Open game
    .addStep("Load")
    .addStep("WaitForState", "idle")

    .addStep("Force", "1", "2", "3", "4", "5")

    // Spin
    .addStep("Spin")
    .addStep("WaitForState", "spinning")

    // Note win, balance, stake
    .addStep("Note", "BalanceText", "InitialBalance")
    .addStep("Note", "TotalBetText", "InitialStake")
    .addStep("Note", "WinText", "InitialWin")
    .addStep("Screenshot", "./pre-recovery.png")

    // Refresh
    .addStep("Load")
    .addStep("WaitForState", "resuming")
    .addStep("Click", "button-ok")

    // Verify win, balance, stake unchanged
    .addStep("Expect", "BalanceText", "Equals", "InitialBalance")
    .addStep("Expect", "TotalBetText", "Equals", "InitialStake")
    .addStep("Expect", "WinText", "Equals", "InitialWin")
    .addStep("Screenshot", "./post-recovery.png")

    // Verify wager completes successfully
    .addStep("WaitForState", "idle");