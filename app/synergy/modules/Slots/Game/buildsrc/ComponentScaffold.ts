namespace RS.Build.Scaffolds
{
    const componentTemplate = `namespace {{namespace}}.Components
{
    /**
     * The {{className}} component.
     */
    export class {{className:CamelCase}} extends RS.Rendering.DisplayObjectContainer
    {
        constructor(settings: {{className:CamelCase}}.Settings)
        {
            super("{{className}} component");

            this.setupComponent();
        }

        /**
         * Setup the relevant items for the component
         */
        protected setupComponent(): void
        {
            return;
        }
    }

    export namespace {{className:CamelCase}}
    {
        /**
         * Settings for the {{className}} component.
         */
        export interface Settings
        {
            //Example setting
            setting: any;
        }

        /**
         * Default settings for the {{className}} component.
         */
        export const defaultSettings: Settings =
        {
            setting: null
        }
    }
}`;

    export const component = new Scaffold();
    component.addTemplate(componentTemplate, "src/components/{{className}}.ts");
    component.addParam("className", "Class Name", "MyClass");
    registerScaffold("component", component);
}