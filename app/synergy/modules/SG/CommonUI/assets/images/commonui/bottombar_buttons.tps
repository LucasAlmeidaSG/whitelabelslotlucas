<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.2.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>desktop</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>mobile</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>red7</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>bottombar_buttons_{v}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>0.75</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">buttonpanel_raw/btn_autoplay.png</key>
            <key type="filename">buttonpanel_raw/btn_autoplay_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_autoplay_down.png</key>
            <key type="filename">buttonpanel_raw/btn_autoplay_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,20,46,41</rect>
                <key>scale9Paddings</key>
                <rect>23,20,46,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttonpanel_raw/btn_bigbet.png</key>
            <key type="filename">buttonpanel_raw/btn_bigbet_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_bigbet_down.png</key>
            <key type="filename">buttonpanel_raw/btn_bigbet_hover.png</key>
            <key type="filename">buttonpanel_raw/btn_help.png</key>
            <key type="filename">buttonpanel_raw/btn_help_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_help_down.png</key>
            <key type="filename">buttonpanel_raw/btn_help_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,39,40</rect>
                <key>scale9Paddings</key>
                <rect>20,20,39,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttonpanel_raw/btn_decrease.png</key>
            <key type="filename">buttonpanel_raw/btn_decrease_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_decrease_down.png</key>
            <key type="filename">buttonpanel_raw/btn_decrease_hover.png</key>
            <key type="filename">buttonpanel_raw/btn_increase.png</key>
            <key type="filename">buttonpanel_raw/btn_increase_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_increase_down.png</key>
            <key type="filename">buttonpanel_raw/btn_increase_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,21,21</rect>
                <key>scale9Paddings</key>
                <rect>11,11,21,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttonpanel_raw/btn_spin.png</key>
            <key type="filename">buttonpanel_raw/btn_spin_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_spin_hover.png</key>
            <key type="filename">buttonpanel_raw/btn_stop.png</key>
            <key type="filename">buttonpanel_raw/btn_stop_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_stop_down.png</key>
            <key type="filename">buttonpanel_raw/btn_stop_hover.png</key>
            <key type="filename">buttonpanel_raw/btn_stopautoplay.png</key>
            <key type="filename">buttonpanel_raw/btn_stopautoplay_dim.png</key>
            <key type="filename">buttonpanel_raw/btn_stopautoplay_down.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,76,76</rect>
                <key>scale9Paddings</key>
                <rect>38,38,76,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttonpanel_raw/btn_spin_down.png</key>
            <key type="filename">buttonpanel_raw/btn_stopautoplay_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,75,76</rect>
                <key>scale9Paddings</key>
                <rect>38,38,75,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttonpanel_raw/btn_turbo_backer.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,20,75,39</rect>
                <key>scale9Paddings</key>
                <rect>38,20,75,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttonpanel_raw/btn_turbo_hover.png</key>
            <key type="filename">buttonpanel_raw/btn_turbo_off.png</key>
            <key type="filename">buttonpanel_raw/btn_turbo_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,36,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,36,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mobilebuttonpanel_raw/mbtn_accept.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_accept_dim.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_cancel.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_cancel_dim.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,35,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mobilebuttonpanel_raw/mbtn_autoplay.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_autoplay_dim.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_help.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_help_dim.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_totalbet.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_totalbet_dim.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,25,54,49</rect>
                <key>scale9Paddings</key>
                <rect>27,25,54,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mobilebuttonpanel_raw/mbtn_bigbet.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_bigbet_dim.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_chevron.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_chevron_dim.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,79,79</rect>
                <key>scale9Paddings</key>
                <rect>40,40,79,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mobilebuttonpanel_raw/mbtn_spin.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_spin_dim.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,88,167,175</rect>
                <key>scale9Paddings</key>
                <rect>84,88,167,175</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mobilebuttonpanel_raw/mbtn_stop.png</key>
            <key type="filename">mobilebuttonpanel_raw/mbtn_stop_dim.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,47,98,95</rect>
                <key>scale9Paddings</key>
                <rect>49,47,98,95</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mobilebuttonpanel_raw/pointer.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,101,17,201</rect>
                <key>scale9Paddings</key>
                <rect>9,101,17,201</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">paytable_raw/nav_page_off.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,1,18,2</rect>
                <key>scale9Paddings</key>
                <rect>9,1,18,2</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">paytable_raw/nav_page_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,3,22,6</rect>
                <key>scale9Paddings</key>
                <rect>11,3,22,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">paytable_raw/nav_section_off.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,11,11</rect>
                <key>scale9Paddings</key>
                <rect>6,6,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">paytable_raw/nav_section_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">paytable_raw/pbtn_close.png</key>
            <key type="filename">paytable_raw/pbtn_close_down.png</key>
            <key type="filename">paytable_raw/pbtn_close_hover.png</key>
            <key type="filename">paytable_raw/pbtn_left.png</key>
            <key type="filename">paytable_raw/pbtn_left_down.png</key>
            <key type="filename">paytable_raw/pbtn_left_hover.png</key>
            <key type="filename">paytable_raw/pbtn_right.png</key>
            <key type="filename">paytable_raw/pbtn_right_down.png</key>
            <key type="filename">paytable_raw/pbtn_right_hover.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,28,57,57</rect>
                <key>scale9Paddings</key>
                <rect>28,28,57,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>buttonpanel_raw</filename>
            <filename>mobilebuttonpanel_raw</filename>
            <filename>paytable_raw</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
