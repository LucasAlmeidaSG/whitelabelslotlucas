namespace SG.CommonUI
{
    @RS.HasCallbacks
    export class MobileButtonPanel extends RS.Flow.BaseElement<MobileButtonPanel.Settings, MobileButtonPanel.RuntimeData>
    {
        public readonly onInputOptionChanged = RS.createEvent<{ inputOption: number; optionIndex: number; }>();

        protected _stopAutoplayButtonLabel: RS.Flow.Label;
        protected _spinButton: RS.Flow.Button;
        protected _stopButton: RS.Flow.Button;
        protected _bigBetButton: RS.Flow.Button;
        protected _bigBetButtonVisible: boolean;
        protected _stopAutoplayButton: RS.Flow.Button;
        @RS.AutoDisposeOnSet protected _buttonTween: RS.Tween<RS.Flow.Button> | null = null;

        @RS.AutoDispose protected readonly _spinButtonTween: RS.FlipFlopTween<RS.Flow.Button>;

        public get onSpinButtonClicked() { return this._spinButton.onClicked; }
        public get onSkipButtonClicked() { return this._stopButton.onClicked; }
        public get onBigBetButtonClicked() { return this._bigBetButton.onClicked; }
        public get onStopAutoplayButtonClicked() { return this._stopAutoplayButton.onClicked; }

        @RS.AutoDispose protected readonly _spinOrSkipHandler: RS.IDisposable;
        @RS.AutoDispose protected readonly _autoplaysActiveObservable: RS.IReadonlyObservable<boolean>;

        public constructor(settings: MobileButtonPanel.Settings, runtimeData: MobileButtonPanel.RuntimeData)
        {
            super(settings, runtimeData);

            this._bigBetButton = RS.Flow.button.create(this.settings.bigBetButton, this);
            this._bigBetButtonVisible = settings.showBigBetButton && runtimeData.showBigBetButton;
            this._bigBetButton.visible = this._bigBetButtonVisible;

            this._spinButton = RS.Flow.button.create(this.settings.spinButton, this);
            this._stopButton = RS.Flow.button.create(this.settings.stopButton, this);

            this._spinButtonTween = new RS.FlipFlopTween(this._spinButton, {
                left:
                {
                    scaleFactor: settings.spinButtonHighScaleFactor,
                    floatOffset: settings.spinButton.floatOffset
                },
                right:
                {
                    scaleFactor: settings.spinButtonLowScaleFactor,
                    floatOffset: settings.stopButton.floatOffset
                },
                duration: settings.flipFlopTweenTime != null ? settings.flipFlopTweenTime : MobileButtonPanel.defaultSettings.flipFlopTweenTime
            });

            this._spinButtonTween.onLeft(this.handleFlipFlopLeftOrMiddle);
            this._spinButtonTween.onMiddle(this.handleFlipFlopLeftOrMiddle);
            this._spinButtonTween.onRight(this.handleFlipFlopRight);
            this._spinButtonTween.state = RS.FlipFlopTween.State.Left;
            this.handleFlipFlopLeftOrMiddle();

            if (runtimeData.autoplaysRemainingObservable)
            {
                this._stopAutoplayButton = RS.Flow.button.create(this.settings.stopAutoplayButton, this);
                this._stopAutoplayButton.visible = false;
                this._stopAutoplayButton.onClicked(this.handleStopAutoplayClicked);

                const textObservable = runtimeData.autoplaysRemainingObservable.map((o) => `${o}`);
                this._stopAutoplayButtonLabel = RS.Flow.label.create(this.settings.stopAutoplayButtonLabel, this._stopAutoplayButton);
                this._stopAutoplayButtonLabel.bindToObservables({ text: textObservable });

                this._autoplaysActiveObservable = runtimeData.autoplaysRemainingObservable.map((autoplaysRemaining) => autoplaysRemaining > 0);
            }

            if (runtimeData.canSpinArbiter)
            {
                let enabled: RS.IReadonlyObservable<boolean>;
                if (runtimeData.enableInteractionArbiter)
                {
                    enabled = RS.Observable.reduce([ runtimeData.canSpinArbiter, runtimeData.enableInteractionArbiter ], RS.Arbiter.AndResolver);
                    RS.Disposable.bind(enabled, this);
                }
                else
                {
                    enabled = runtimeData.canSpinArbiter;
                }

                this._spinButton.bindToObservables({ enabled });
            }

            if (runtimeData.canSkipArbiter)
            {
                //if (runtimeData.enableInteractionArbiter)
                //{
                // this._canSkipArbiter = new RS.CompositeArbiter([ runtimeData.canSkipArbiter, runtimeData.enableInteractionArbiter ], RS.Arbiter.AndResolver);
                //}
                this._stopButton.bindToObservables({ enabled: runtimeData.canSkipArbiter });
            }

            if (runtimeData.spinOrSkipArbiter)
            {
                this._spinOrSkipHandler = runtimeData.spinOrSkipArbiter.onChanged(this.handleSpinOrSkipChanged);
                this.handleSpinOrSkipChanged();
            }
        }

        public get bigBetButtonVisible() { return this._bigBetButtonVisible; }
        public set bigBetButtonVisible(value)
        {
            if (this._bigBetButtonVisible === value) { return; }
            this._bigBetButtonVisible = value;
            this.skipButtonAnim();
            if (!value)
            {
                this._bigBetButton.enabled = false;
                this._buttonTween = RS.Tween.get(this._bigBetButton)
                    .to({
                        floatPosition: { x: 1.0, y: this.settings.bigBetButton.floatPosition.y },
                        dockAlignment: { x: 0.0, y: this.settings.bigBetButton.dockAlignment.y }
                    }, 500);
            }
            else
            {
                this._buttonTween = RS.Tween.get(this._bigBetButton)
                    .to({
                        floatPosition: this.settings.bigBetButton.floatPosition,
                        dockAlignment: this.settings.bigBetButton.dockAlignment
                    }, 500)
                    .set({ enabled: true });
            }
        }

        protected skipButtonAnim(): void
        {
            if (this._buttonTween)
            {
                if (!this._buttonTween.finished)
                {
                    this._buttonTween.setPosition(this._buttonTween.duration, RS.StepBehaviour.ForwardsOnly);
                    this._buttonTween.finish();
                }
                this._buttonTween = null;
            }
        }

        @RS.Callback
        protected handleFlipFlopLeftOrMiddle(): void
        {
            this._stopButton.visible = false;

            // hide the auto play stop button if we can spin and there are no autoplays remaining
            if (this._stopAutoplayButton && !this._autoplaysActiveObservable.value)
            {
                this._stopAutoplayButton.visible = false;
                this._spinButton.visible = true;
            }
        }

        @RS.Callback
        protected handleFlipFlopRight(): void
        {
            this._spinButton.visible = false;
            this._stopButton.visible = true;

            // show auto spins stop if we are supposed to
            if (this._stopAutoplayButton)
            {
                this._stopAutoplayButton.visible = this._autoplaysActiveObservable.value;
                this._stopButton.visible = !this._stopAutoplayButton.visible;
            }
        }

        @RS.Callback
        protected handleStopAutoplayClicked(): void
        {
            // hide when clicked
            this._stopAutoplayButton.visible = false;
            const showSpinButton = this.runtimeData.spinOrSkipArbiter.value === "spin";
            this._spinButton.visible = showSpinButton;
            this._stopButton.visible = !showSpinButton;
        }

        @RS.Callback
        protected handleSpinOrSkipChanged(): void
        {
            if (this.runtimeData.spinOrSkipArbiter.value === "spin")
            {
                this._spinButtonTween.state = RS.FlipFlopTween.State.Left;
            }
            else
            {
                this._spinButtonTween.state = RS.FlipFlopTween.State.Right;
            }
        }
    }

    export const mobileButtonPanel = RS.Flow.declareElement(MobileButtonPanel, true);

    export namespace MobileButtonPanel
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            spinButton: RS.Flow.Button.Settings;
            stopButton: RS.Flow.Button.Settings;
            bigBetButton: RS.Flow.Button.Settings;
            showBigBetButton: boolean;
            stopAutoplayButton: RS.Flow.Button.Settings;
            stopAutoplayButtonLabel: RS.Flow.Label.Settings;

            spinButtonHighScaleFactor: number;
            spinButtonLowScaleFactor: number;

            flipFlopTweenTime?: number;
        }

        export interface RuntimeData
        {
            spinOrSkipArbiter: RS.IArbiter<"spin"|"skip">;
            canSpinArbiter: RS.IArbiter<boolean>;
            canSkipArbiter: RS.IArbiter<boolean>;
            canChangeBetArbiter: RS.IArbiter<boolean>;
            enableInteractionArbiter: RS.IArbiter<boolean>;
            autoplaysRemainingObservable: RS.IReadonlyObservable<number>;

            showBigBetButton: boolean;
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            asset: Assets.CommonUI.BottomBar.Buttons,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export let defaultSettings: Settings =
        {
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 1.0, y: 0.5 },
            dockAlignment: { x: 1.0, y: 0.5 },
            size: { w: 260, h: 600 },
            sizeToContents: false,
            expand: RS.Flow.Expand.HorizontalOnly,
            spinButtonHighScaleFactor: 1.0,
            spinButtonLowScaleFactor: 0.54,
            spinButton:
            {
                name: "Spin",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_spin },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_spin },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_spin_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_spin_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 1.0, y: 0.5 },
                dockAlignment: { x: 1.0, y: 0.5 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "SpinButton"
            },
            stopButton:
            {
                name: "Stop",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 1.0, y: 0.5 },
                dockAlignment: { x: 1.0, y: 0.5 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "StopSpinButton"
            },
            stopAutoplayButton:
            {
                name: "Stop Autoplay",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_stop_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 1.0, y: 0.5 },
                dockAlignment: { x: 1.0, y: 0.5 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "StopAutoSpinButton"
            },
            stopAutoplayButtonLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                dock: RS.Flow.Dock.Float,
                floatPosition: {x: 0.5, y: 0.5},
                fontSize: 35,
                sizeToContents: true,
                seleniumId: "StopAutoSpinButtonLabel"
            },
            bigBetButton:
            {
                name: "Big Bet",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_bigbet },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_bigbet },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_bigbet_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_bigbet_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 1.0, y: 0.0 },
                dockAlignment: { x: 1.0, y: 0.0 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "BigBetButton"
            },
            showBigBetButton: false,
            flipFlopTweenTime: 500
        };

        export let defaultPortraitSettings: Settings | null = null;
        export let defaultLandscapeSettings: Settings | null = null;
    }
}
