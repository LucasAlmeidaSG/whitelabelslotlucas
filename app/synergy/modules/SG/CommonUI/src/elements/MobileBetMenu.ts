/// <reference path="../generated/Assets.ts" />

namespace SG.CommonUI
{
    /**
     * The MobileBetMenu element.
     */
    export class MobileBetMenu extends RS.Flow.BaseElement<MobileBetMenu.Settings, MobileBetMenu.RuntimeData>
    {
        protected _image: RS.Flow.Image;
        protected _topSection: RS.Flow.Container;
        protected _headerList: RS.Flow.List;
        protected _betAmountLabel: RS.Flow.Label;
        protected _spinners: RS.Flow.ListSpinner[];
        protected _bottomSection: RS.Flow.Container;
        protected _bottomList: RS.Flow.List;
        protected _acceptButton: RS.Flow.Button;
        protected _cancelButton: RS.Flow.Button;

        protected _betAmount: number;

        public get totalBetAmount() { return this._betAmount; }
        public set totalBetAmount(value)
        {
            this._betAmount = value;
            if (this.currencyFormatter)
            {
                this._betAmountLabel.text = this.currencyFormatter.format(value);
            }
            else
            {
                this._betAmountLabel.text = "NO CURRENCY FORMATTER";
            }
        }

        public get onAcceptClicked() { return this._acceptButton.onClicked; }
        public get onCancelClicked() { return this._cancelButton.onClicked; }

        public constructor(settings: MobileBetMenu.Settings, runtimeData: MobileBetMenu.RuntimeData)
        {
            super(settings, runtimeData);

            // Header

            this._topSection = RS.Flow.container.create(this.settings.topSection, this);

            this._image = RS.Flow.image.create(settings.iconImage, this._topSection);

            this._headerList = RS.Flow.list.create(this.settings.headerList, this._topSection);

            RS.Flow.label.create(settings.totalBetLabel, this._headerList);

            if (settings.ribbon) { RS.Flow.background.create(settings.ribbon, this._headerList); }

            this._betAmountLabel = RS.Flow.label.create(settings.betAmountLabel, this._headerList);
            this.totalBetAmount = 0.0;

            if (settings.ribbon) { RS.Flow.background.create(settings.ribbon, this._headerList); }

            // Footer

            this._bottomSection = RS.Flow.container.create(this.settings.bottomSection, this);

            this._bottomList = RS.Flow.list.create(this.settings.bottomList, this._bottomSection);

            this._acceptButton = RS.Flow.button.create(settings.acceptButton, this._bottomList);
            this._cancelButton = RS.Flow.button.create(settings.cancelButton, this._bottomList);

            // Body

            this._spinners = new Array<RS.Flow.ListSpinner>(runtimeData.spinners.length);
            for (let i = runtimeData.spinners.length - 1; i >= 0; --i)
            {
                const spinnerSettings = runtimeData.spinners[i];

                // Make container to hold poiner and spinner
                const container = RS.Flow.container.create({
                    name: "Spinner",
                    sizeToContents: true,
                    dock: RS.Flow.Dock.Fill
                });
                RS.Flow.image.create(settings.pointerImage, container);
                const spinner = RS.Flow.listSpinner.create(spinnerSettings.spinner);
                container.addChild(spinner);

                // Make titled house the container
                const spinnerTitled = new RS.Flow.Titled(spinnerSettings.title, container);
                this.addChild(spinnerTitled);

                this._spinners[i] = spinner;
            }
        }

        public getSpinner(index: number)
        {
            return this._spinners[index];
        }
    }

    export namespace MobileBetMenu
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            topSection: RS.Flow.Container.Settings;
            iconImage: RS.Flow.Image.Settings;
            headerList: RS.Flow.List.Settings;
            totalBetLabel: RS.Flow.Label.Settings;
            betAmountLabel: RS.Flow.Label.Settings;
            bottomSection: RS.Flow.Container.Settings;
            bottomList: RS.Flow.List.Settings;

            ribbon?: RS.Flow.Background.Settings;

            pointerImage: RS.Flow.Image.Settings;

            acceptButton: RS.Flow.Button.Settings;
            cancelButton: RS.Flow.Button.Settings;
        }

        export interface SpinnerSettings
        {
            title: RS.Flow.Titled.Settings;
            spinner: RS.Flow.ListSpinner.Settings;
        }

        export let defaultSpinnerSettings: RS.Flow.ListSpinner.Settings =
        {
            dock: RS.Flow.Dock.Fill,
            spacing: RS.Flow.Spacing.horizontal(5),
            background: null,
            itemLabel:
            {
                ...RS.Flow.Spinner.defaultSettings.label,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 42
            },
            direction: RS.Flow.ListSpinner.Direction.Vertical,
            expand: RS.Flow.Expand.Allowed,
            options:
            {
                type: RS.Flow.Spinner.Type.Options,
                options: [0]
            },
            fadeEdges: true,
            sizeToContents: true
        };

        export function getSpinnerSettings(title: RS.Localisation.LocalisableString): SpinnerSettings
        {
            return {
                title:
                {
                    ...RS.Flow.Titled.defaultSettings,
                    title:
                    {
                        ...RS.Flow.Titled.defaultSettings.title,
                        font: RS.Fonts.Frutiger.CondensedMedium,
                        fontSize: 24,
                        text: Translations.UI.TotalBet
                    },
                    dock: RS.Flow.Dock.Right,
                    sizeToContents: true,
                    expand: RS.Flow.Expand.VerticalOnly
                },
                spinner: defaultSpinnerSettings
            };
        }

        export interface RuntimeData
        {
            spinners: SpinnerSettings[];
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            asset: Assets.CommonUI.BottomBar.Buttons,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export let defaultSettings: Settings =
        {
            dock: RS.Flow.Dock.Fill,
            topSection:
            {
                name: "Top Section",
                dock: RS.Flow.Dock.Top,
                sizeToContents: true,
                spacing: RS.Flow.Spacing.all(4)
            },
            iconImage:
            {
                dock: RS.Flow.Dock.Left,
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.mbtn_totalbet
            },
            headerList:
            {
                dock: RS.Flow.Dock.Fill,
                direction: RS.Flow.List.Direction.TopToBottom,
                sizeToContents: false,
                evenlySpaceItems: true,
                expand: RS.Flow.Expand.Allowed
            },
            totalBetLabel:
            {
                font: RS.Fonts.Frutiger.CondensedMedium,
                text: Translations.UI.TotalBet,
                fontSize: 24,
                sizeToContents: true
            },
            betAmountLabel:
            {
                font: RS.Fonts.Frutiger.CondensedMedium,
                text: "",
                fontSize: 36,
                sizeToContents: true
            },
            bottomSection:
            {
                name: "Bottom Section",
                dock: RS.Flow.Dock.Bottom,
                sizeToContents: true,
                spacing: { left: 50, top: 0, right: 0, bottom: 0 }
            },
            bottomList:
            {
                dock: RS.Flow.Dock.Fill,
                sizeToContents: true,
                direction: RS.Flow.List.Direction.LeftToRight,
                evenlySpaceItems: true,
                expand: RS.Flow.Expand.HorizontalOnly
            },
            ribbon:
            {
                kind: RS.Flow.Background.Kind.Gradient,
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                    [
                        { color: RS.Util.Colors.black, offset: 0.0 },
                        { color: RS.Util.Colors.white, offset: 0.25 },
                        { color: RS.Util.Colors.white, offset: 0.75 },
                        { color: RS.Util.Colors.black, offset: 1.0 }
                    ],
                    posA: { x: 0.5, y: 0 },
                    posB: { x: 3.5, y: 0 }
                },
                textureSize: { w: 4, h: 1 },
                size: { w: 0, h: 2 },
                expand: RS.Flow.Expand.HorizontalOnly,
                sizeToContents: false,
                dock: RS.Flow.Dock.None
            },
            pointerImage:
            {
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.BottomBar.Buttons,
                animationName: Assets.CommonUI.BottomBar.Buttons.pointer,
                sizeToContents: true,
                dock: RS.Flow.Dock.Right,
                expand: RS.Flow.Expand.Disallowed
            },
            acceptButton:
            {
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_accept },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_accept_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_accept_dim },
                textLabel: null,
                seleniumId: "TotalBetAcceptButton"
            },
            cancelButton:
            {
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_cancel },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_cancel_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_cancel_dim },
                textLabel: null,
                seleniumId: "TotalBetCancelButton"
            },

            spacing: RS.Flow.Spacing.all(10)
        };
    }

    /**
     * The MobileBetMenu element.
     */
    export const mobileBetMenu = RS.Flow.declareElement(MobileBetMenu, true);
}