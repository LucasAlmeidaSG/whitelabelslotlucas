/// <reference path="../generated/Assets.ts" />

namespace SG.CommonUI
{
    /**
     * The MobileMenu element.
     */
    export class MobileMenu extends RS.Flow.BaseElement<MobileMenu.Settings, MobileMenu.RuntimeData>
    {
        @RS.AutoDispose protected _buttons: RS.Flow.List;
        @RS.AutoDispose protected _helpButton: RS.Flow.Button;
        @RS.AutoDispose protected _betButton: RS.Flow.Button;
        @RS.AutoDispose protected _autoplayButton: RS.Flow.Button;

        public get onHelpButtonClicked() { return this._helpButton.onClicked; }
        public get onBetButtonClicked() { return this._betButton.onClicked; }
        public get onAutoplayButtonClicked() { return this._autoplayButton.onClicked; }

        public get betButtonEnabled() { return this._betButton.enabled; }
        public set betButtonEnabled(v) { this._betButton.enabled = v; }

        public get autoplayButtonEnabled() { return this._autoplayButton.enabled; }
        public set autoplayButtonEnabled(v) { this._autoplayButton.enabled = v; }

        public constructor(settings: MobileMenu.Settings, runtimeData: MobileMenu.RuntimeData)
        {
            super(settings, runtimeData);

            this._buttons = RS.Flow.list.create(this.settings.buttonList, this);
            this._helpButton = RS.Flow.button.create(this.settings.helpButton, this._buttons);
            this._helpButton.bindToObservables({ enabled: this.runtimeData.paytableEnabledArbiter });
            this._betButton = RS.Flow.button.create(this.settings.betButton, this._buttons);
            this._autoplayButton = RS.Flow.button.create(this.settings.autoplayButton, this._buttons);

            this._autoplayButton.visible = this.runtimeData.showAutoplayButton;
        }
    }

    export namespace MobileMenu
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            buttonList: RS.Flow.List.Settings;
            helpButton: RS.Flow.Button.Settings;
            betButton: RS.Flow.Button.Settings;
            autoplayButton: RS.Flow.Button.Settings;
        }

        export interface RuntimeData
        {
            showAutoplayButton: boolean;
            paytableEnabledArbiter: RS.IArbiter<boolean>;
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            asset: Assets.CommonUI.BottomBar.Buttons,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export let defaultSettings: Settings =
        {
            buttonList:
            {
                direction: RS.Flow.List.Direction.TopToBottom,
                dock: RS.Flow.Dock.Fill,
                evenlySpaceItems: true,
                sizeToContents: false,
                expand: RS.Flow.Expand.VerticalOnly,
                dockAlignment: { x: 0.6, y: 0.5 }
            },
            helpButton:
            {
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_help },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_help_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_help_dim },
                textLabel: null,
                seleniumId: "MobileHelpButton"
            },
            betButton:
            {
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_totalbet },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_totalbet_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_totalbet_dim },
                textLabel: null,
                seleniumId: "TotalBetButton"
            },
            autoplayButton:
            {
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_autoplay },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_autoplay_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_autoplay_dim },
                textLabel: null,
                seleniumId: "AutoplayButton"
            },
            dock: RS.Flow.Dock.Fill
        };
    }

    /**
     * The MobileMenu element.
     */
    export const mobileMenu = RS.Flow.declareElement(MobileMenu, true);
}