namespace SG.CommonUI
{
    export class MessageLabel extends RS.Flow.ScrollingElement<RS.Flow.Label.Settings, void, RS.Flow.Label, MessageLabel.Settings>
    {
        public get text() { return this._element.text; }
        public set text(value) { this._element.text = value; this.updateTweenState(true); }

        protected createElement()
        {
            return RS.Flow.label.create(this.settings.label, this.innerContainer);
        }
    }

    export namespace MessageLabel
    {
        export interface Settings extends RS.Flow.ScrollingElement.Settings
        {
            label: RS.Flow.Label.Settings;
        }
    }

    export const messageLabel = RS.Flow.declareElement(MessageLabel);
}