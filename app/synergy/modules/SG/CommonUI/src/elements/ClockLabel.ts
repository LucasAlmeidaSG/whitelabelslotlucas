namespace SG.CommonUI
{
    const timePrefix = [ "00", "0", "" ];

    @RS.HasCallbacks
    export class ClockLabel extends RS.Flow.BaseElement<ClockLabel.Settings>
    {
        protected _label: RS.Flow.Label;
        protected _timer: number;

        public constructor(settings: ClockLabel.Settings)
        {
            super(settings, undefined);

            // Setup label
            this._label = RS.Flow.label.create(settings.label, this);

            // Setup timer
            this._timer = setInterval(this.update, 1000) as any;
            this.update();
        }

        /**
         * Disposes this element.
         */
        public dispose()
        {
            if (this.isDisposed) { return; }
            clearInterval(this._timer);
            super.dispose();
        }

        @RS.Callback
        protected update()
        {
            const date = new Date(Date.now());
            const hours = date.getHours().toString();
            const minutes = date.getMinutes().toString();
            const time = `${timePrefix[hours.length]}${hours}:${timePrefix[minutes.length]}${minutes}`;
            this._label.text = time;
        }
    }

    export const clockLabel = RS.Flow.declareElement(ClockLabel);

    export namespace ClockLabel
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            label: RS.Flow.Label.Settings;
        }

        export let defaultSettings: Settings =
        {
            label:
            {
                ...RS.Flow.Label.defaultSettings,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                text: "",
                textColor: new RS.Util.Color(0xc4c4c4),
                dock: RS.Flow.Dock.Fill
            },
            sizeToContents: true
        };
    }
}