namespace SG.CommonUI
{

    @RS.HasCallbacks
    export class TurboToggle extends RS.Flow.BaseElement<TurboToggle.Settings>
    {
        /** Published when the turbo mode button has been clicked. */
        @RS.AutoDispose protected readonly _onTurboTurnOnClicked = RS.createSimpleEvent();

        /** Published when the stop turbo mode button has been clicked. */
        @RS.AutoDispose protected readonly _onTurboTurnOffClicked = RS.createSimpleEvent();

        protected _turboOn: boolean = null;

        /** Published when the turbo mode button has been clicked. */
        public get onToggledOn() { return this._onTurboTurnOnClicked.public; }
        /** Published when the stop turbo mode button has been clicked. */
        public get onToggledOff() { return this._onTurboTurnOffClicked.public; }

        public get isTurboOn() { return this._turboOn; }
        public set isTurboOn(turboOn: boolean)
        {
            if (turboOn === this._turboOn) { return; }
            const firstTime = this._turboOn === null;
            if (this._tween && !this._tween.finished) { this._tween.finish(); }
            this._turboOn = turboOn;
            if (turboOn)
            {
                this._turboTurnOnButton.enabled = false;
                this._turboTurnOffButton.enabled = true;
                this.moveToTop(this._turboBackgroundOn);
                if (firstTime)
                {
                    this._turboBackgroundOff.alpha = 0;
                    this._turboBackgroundOn.alpha = 1;
                }
                else
                {
                    this._tween = RS.Tween.get(this._turboBackgroundOn)
                            .to({ alpha: 1.0 }, this.settings.tweenDuration || 300, this.settings.tweenEase || RS.Ease.linear);
                    this._tween.then(() => this._turboBackgroundOff.alpha = 0.0);
                }
            }
            else
            {
                this._turboTurnOnButton.enabled = true;
                this._turboTurnOffButton.enabled = false;
                this.moveToTop(this._turboBackgroundOff);
                if (firstTime)
                {
                    this._turboBackgroundOn.alpha = 0;
                    this._turboBackgroundOff.alpha = 1;
                }
                else
                {
                    this._tween = RS.Tween.get(this._turboBackgroundOff)
                            .to({ alpha: 1.0 }, this.settings.tweenDuration || 300, this.settings.tweenEase || RS.Ease.linear);
                    this._tween.then(() => this._turboBackgroundOn.alpha = 0.0);
                }
            }
        }

        @RS.AutoDisposeOnSet protected _background: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _turboBackgroundOn: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _turboBackgroundOff: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _turboTurnOnButton: RS.Flow.Button;
        @RS.AutoDisposeOnSet protected _turboTurnOffButton: RS.Flow.Button;
        @RS.AutoDisposeOnSet protected _onImage: RS.Flow.Image;
        @RS.AutoDisposeOnSet protected _offImage: RS.Flow.Image;
        @RS.AutoDisposeOnSet protected _tween: RS.SingleTween<RS.Flow.Background>;

        public constructor(settings: TurboToggle.Settings)
        {
            super(settings, undefined);

            if (this.settings.background)
            {
                this._background = RS.Flow.background.create(this.settings.background, this);
            }
            this._turboBackgroundOn = RS.Flow.background.create(this.settings.turboBackgroundOn, this);
            this._turboBackgroundOff = RS.Flow.background.create(this.settings.turboBackgroundOff, this);
            if (this.settings.onImage && this.settings.offImage && (!this.settings.turboOnButton.textLabel ||
                RS.Localisation.resolveLocalisableString(settings.locale, this.settings.turboOnButton.textLabel.text).length > this.settings.labelMaxLength ||
                RS.Localisation.resolveLocalisableString(settings.locale, this.settings.turboOffButton.textLabel.text).length > this.settings.labelMaxLength))
            {
                this._onImage = RS.Flow.image.create(this.settings.onImage, this._turboBackgroundOn);
                this._offImage = RS.Flow.image.create(this.settings.offImage, this._turboBackgroundOff);

                this.settings.turboOnButton.textLabel = null;
                this.settings.turboOffButton.textLabel = null;
            }
            this._turboTurnOnButton = RS.Flow.button.create(this.settings.turboOnButton, this._turboBackgroundOff);
            this._turboTurnOffButton = RS.Flow.button.create(this.settings.turboOffButton, this._turboBackgroundOn);
            this._turboTurnOffButton.enabled = true;
            this._turboTurnOnButton.enabled = false;
            this._turboBackgroundOff.alpha = 0;
            this._turboTurnOnButton.onClicked(this.turnOnClicked);
            this._turboTurnOffButton.onClicked(this.turnOffClicked);
            if (this.settings.turboLabel)
            {
                RS.Flow.label.create(this.settings.turboLabel, this);
            }
        }

        @RS.Callback
        protected turnOnClicked()
        {
            this.isTurboOn = true;
            this._onTurboTurnOnClicked.publish();
        }

        @RS.Callback
        protected turnOffClicked()
        {
            this.isTurboOn = false;
            this._onTurboTurnOffClicked.publish();
        }
    }

    export namespace TurboToggle
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            /** Optional background */
            background?: RS.Flow.Background.Settings;
            /** Turbo Mode! */
            turboLabel?: RS.Flow.Label.Settings;
            /** Optional Image to indicate ON */
            onImage?: RS.Flow.Image.Settings;
            /** Optional Image to indicate OFF */
            offImage?: RS.Flow.Image.Settings;
            /** Number of characters on/off text needs to be to use images to replace it*/
            labelMaxLength?: number;
            /** indicates turbo is ON */
            turboBackgroundOn: RS.Flow.Background.Settings;
            /** indicates turbo is OFF */
            turboBackgroundOff: RS.Flow.Background.Settings;
            /** clicking this will turn turbo ON */
            turboOnButton: RS.Flow.Button.Settings;
            /** clicking this will turn turbo OFF */
            turboOffButton: RS.Flow.Button.Settings;
            /** duration of tween when toggling between on and off */
            tweenDuration?: number;
            /** ease of tween when toggling between on and off */
            tweenEase?: RS.EaseFunction;
        }

        export let Settings: Settings =
        {
            name: "TurboToggle",
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 0.5, y: 1.0 },
            dockAlignment: { x: 0.5, y: 0.5 },
            floatOffset: {x: 500, y: -150},
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed,
            turboLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.5 },
                dockAlignment: { x: 0.5, y: 0.5 },
                floatOffset: { x: 0, y: -60 },
                font: RS.Fonts.FrutigerWorld,
                fontSize: 24,
                text: Translations.UI.Turbo
            },
            turboBackgroundOn:
            {
                kind: RS.Flow.Background.Kind.ImageFrame,
                ignoreParentSpacing: true,
                asset: Assets.CommonUI.BottomBar.Buttons,
                frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_backer,
                sizeToContents: true,
                expand: RS.Flow.Expand.Allowed,
                dock: RS.Flow.Dock.Fill,
            },
            turboBackgroundOff:
            {
                kind: RS.Flow.Background.Kind.ImageFrame,
                ignoreParentSpacing: true,
                asset: Assets.CommonUI.BottomBar.Buttons,
                frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_backer,
                sizeToContents: true,
                expand: RS.Flow.Expand.Allowed,
                dock: RS.Flow.Dock.Fill,
            },
            turboOnButton:
            {
                background:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_off,
                    border: RS.Flow.Spacing.all(5)
                },
                hoverbackground:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_hover,
                    border: RS.Flow.Spacing.all(5)
                },
                pressbackground:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_on,
                    border: RS.Flow.Spacing.all(5)
                },
                disabledbackground:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_off,
                    alpha: 0,
                    border: RS.Flow.Spacing.all(5)
                },
                textLabel:
                {
                    ...RS.Flow.Label.defaultSettings,
                    dock: RS.Flow.Dock.Fill,
                    font: RS.Fonts.FrutigerWorld,
                    fontSize: 24,
                    text: Translations.UI.On,
                },

                dock: RS.Flow.Dock.Left,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
            },
            turboOffButton:
            {
                background:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_on,
                    border: RS.Flow.Spacing.all(5)
                },
                hoverbackground:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_hover,
                    border: RS.Flow.Spacing.all(5)
                },
                pressbackground:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_on,
                    border: RS.Flow.Spacing.all(5)
                },
                disabledbackground:
                {
                    kind: RS.Flow.Background.Kind.ImageFrame,
                    asset: Assets.CommonUI.BottomBar.Buttons,
                    frame: Assets.CommonUI.BottomBar.Buttons.btn_turbo_off,
                    alpha: 0,
                    border: RS.Flow.Spacing.all(5)
                },
                textLabel:
                {
                    ...RS.Flow.Label.defaultSettings,
                    dock: RS.Flow.Dock.Fill,
                    font: RS.Fonts.FrutigerWorld,
                    fontSize: 24,
                    text: Translations.UI.Off,
                },

                dock: RS.Flow.Dock.Right,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
            },
            onImage:
            {
                kind: RS.Flow.Image.Kind.Bitmap,
                asset: Assets.CommonUI.Turbo.CheckMark,
                floatPosition: { x: 0.3, y: 0.5 },
                dock: RS.Flow.Dock.Float,
                size: {w: 40, h: 40}
            },
            offImage:
            {
                kind: RS.Flow.Image.Kind.Bitmap,
                asset: Assets.CommonUI.Turbo.Cross,
                floatPosition: { x: 0.7, y: 0.5 },
                dock: RS.Flow.Dock.Float,
                size: {w: 40, h: 40}
            },
            labelMaxLength: 5
        }
        export let DesktopSettings: Settings | null = {...Settings};
        export let MobileSettings: Settings | null =
        {
            ...Settings,
            floatPosition: { x: 0.95, y: 1.0 },
            floatOffset: { x: 0, y: -200 },
        };
        export let DesktopPortraitSettings: Settings | null =
        {
            ...DesktopSettings,
            floatOffset: {x: 500, y: -240},
        };
        export let DesktopLandscapeSettings: Settings | null =
        {
            ...DesktopSettings,
            floatOffset: { x: 500, y: -150}
        };
        export let MobilePortraitSettings: Settings | null =
        {
            ...MobileSettings,
            floatPosition: { x: 0.95, y: 1.0 },
            floatOffset: { x: 0, y: -240 },
        };
        export let MobileLandscapeSettings: Settings | null = { ...MobileSettings};

        export let defaultSettings = null;
        export let defaultDesktopSettings = null;
        export let defaultMobileSettings = null;

        export let defaultDesktopPortraitSettings = null
        export let defaultDesktopLandscapeSettings = null;
        export let defaultMobilePortraitSettings = null;
        export let defaultMobileLandscapeSettings = null;
    }

    export const turboToggle = RS.Flow.declareElement(TurboToggle);
}