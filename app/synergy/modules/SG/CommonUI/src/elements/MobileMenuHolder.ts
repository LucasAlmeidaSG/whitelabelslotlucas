/// <reference path="../generated/Assets.ts" />
/// <reference path="MobileMenu.ts" />
/// <reference path="MobileBetMenu.ts" />

namespace SG.CommonUI
{
    /**
     * The MobileMenuHolder element.
     */
    export class MobileMenuHolder extends RS.Flow.BaseElement<MobileMenuHolder.Settings, MobileMenuHolder.RuntimeData>
    {
        @RS.AutoDispose protected _container: RS.Flow.Container;
        @RS.AutoDispose protected _chevronButton: RS.Flow.Button;
        @RS.AutoDispose protected _background: RS.Flow.Background;

        @RS.AutoDispose protected _innerMenu: MobileMenu;
        @RS.AutoDispose protected _innerBetMenu: MobileBetMenu;

        @RS.AutoDisposeOnSet protected _slideTween: RS.FlipFlopTween<RS.Flow.Container>;
        @RS.AutoDisposeOnSet protected _buttonTween: RS.Tween<RS.Flow.Button> | null = null;
        protected _isOpen: boolean = false;

        protected _mode: MobileMenuHolder.Mode;
        protected _chevronButtonVisible: boolean = true;

        public get onHelpButtonClicked() { return this._innerMenu.onHelpButtonClicked; }
        public get onBetButtonClicked() { return this._innerMenu.onBetButtonClicked; }
        public get onAutoplayButtonClicked() { return this._innerMenu.onAutoplayButtonClicked; }
        public get onChevronButtonClicked() { return this._chevronButton.onClicked; }

        public get isOpen() { return this._isOpen; }

        public get chevronButtonVisible() { return this._chevronButtonVisible; }
        public set chevronButtonVisible(value)
        {
            if (this._chevronButtonVisible === value) { return; }
            this._chevronButtonVisible = value;
            this.skipButtonAnim();
            if (!value)
            {
                // Close menu
                this._slideTween.state = 0;
                this._isOpen = false;
                
                this._chevronButton.enabled = false;
                this._buttonTween = RS.Tween.get(this._chevronButton)
                    .to({
                        floatPosition: { x: 1.0, y: this.settings.chevronButton.floatPosition.y },
                        dockAlignment: { x: 0.0, y: this.settings.chevronButton.dockAlignment.y },
                    }, this.settings.buttonTween.moveTime)
                    .set({ rotation: 0 });
            }
            else
            {
                this._buttonTween = RS.Tween.get(this._chevronButton)
                    .to({
                        floatPosition: this.settings.chevronButton.floatPosition,
                        dockAlignment: this.settings.chevronButton.dockAlignment
                    }, this.settings.buttonTween.moveTime)
                    .set({ enabled: true });
            }
        }

        public get mode() { return this._mode; }
        public set mode(value)
        {
            if (value === this._mode) { return; }
            this._mode = value;
            this.suppressLayouting();
            this._innerMenu.visible = value === MobileMenuHolder.Mode.Menu;
            this._innerBetMenu.visible = value === MobileMenuHolder.Mode.Bet;
            this.restoreLayouting(true);
        }

        public get innerMenu() { return this._innerMenu; }

        public get innerBetMenu() { return this._innerBetMenu; }

        public constructor(settings: MobileMenuHolder.Settings, runtimeData: MobileMenuHolder.RuntimeData)
        {
            super(settings, runtimeData);

            this._container = RS.Flow.container.create(settings.container, this);
            this._container.interactive = true;

            this._background = RS.Flow.background.create(settings.background, this._container);

            this._innerMenu = mobileMenu.create(MobileMenu.defaultSettings, runtimeData.menuData, this._container);
            this._innerBetMenu = mobileBetMenu.create(MobileBetMenu.defaultSettings, runtimeData.betMenuData, this._container);

            this._slideTween = new RS.FlipFlopTween(this._container,
            {
                left: { floatPosition: { x: 1.0, y: 0.0 } },
                right: { floatPosition: { x: 0.0, y: 0.0 } },
                duration: this.settings.buttonTween.moveTime
            });
            this._slideTween.refresh();

            this._chevronButton = RS.Flow.button.create(settings.chevronButton, this);

            this.mode = MobileMenuHolder.Mode.Menu;
        }

        /**
         * Opens the menu.
         */
        public open(): void
        {
            if (this._isOpen) { return; }

            this.skipButtonAnim();
            this._slideTween.state = 1.0;
            this._isOpen = true;
            const tweenSettings = 
            {
                ...MobileMenuHolder.defaultButtonTween,
                ...this.settings.buttonTween
            };
            this._buttonTween = RS.Tween.get(this._chevronButton)
                .set({ enabled: false })
                .wait(tweenSettings.moveDelay)
                .to({
                    floatPosition: { x: 0.0, y: this.settings.chevronButton.floatPosition.y },
                    dockAlignment: { x: 0.5, y: this.settings.chevronButton.dockAlignment.y }
                }, tweenSettings.moveTime, tweenSettings.easeOpen)
                .to({ rotation: RS.Math.Angles.halfCircle }, tweenSettings.rotateTime, tweenSettings.easeRotate)
                .set({ enabled: true });
        }

        /**
         * Closes the menu.
         */
        public close(): void
        {
            if (!this._isOpen) { return; }

            this.skipButtonAnim();
            this._chevronButton.enabled = false;
            this._slideTween.state = 0.0;
            this._isOpen = false;
            const tweenSettings = 
            {
                ...MobileMenuHolder.defaultButtonTween,
                ...this.settings.buttonTween
            };
            this._buttonTween = RS.Tween.get(this._chevronButton)
                .to({
                    floatPosition: this.settings.chevronButton.floatPosition,
                    dockAlignment: this.settings.chevronButton.dockAlignment
                }, tweenSettings.moveTime, tweenSettings.easeClosed)
                .to({ rotation: 0.0 }, tweenSettings.rotateTime, tweenSettings.easeRotate)
                .wait(tweenSettings.moveDelay)
                .set({ enabled: true });
        }

        protected skipButtonAnim(): void
        {
            if (this._buttonTween)
            {
                if (!this._buttonTween.finished)
                {
                    this._buttonTween.setPosition(this._buttonTween.duration, RS.StepBehaviour.ForwardsOnly);
                    this._buttonTween.finish();
                }
                this._buttonTween = null;
            }
            // if (!this._chevronButton) { return; }
            // if (this._isOpen)
            // {
            //     this._chevronButton.floatPosition = { x: 0.0, y: this.settings.chevronButton.floatPosition.y };
            //     this._chevronButton.dockAlignment = { x: 0.5, y: this.settings.chevronButton.dockAlignment.y };
            // }
            // else
            // {
            //     this._chevronButton.floatPosition = this.settings.chevronButton.floatPosition;
            //     this._chevronButton.dockAlignment = this.settings.chevronButton.dockAlignment;
            // }
        }

        protected performLayout(): void
        {
            if (this._container)
            {
                this._container.suppressLayouting();
                this._container.size = this._layoutSize;
                this._container.restoreLayouting(false);
            }
            // if (this._slideTween)
            // {
            //     this._slideTween.settings.left.x = this._layoutSize.w;
            //     this._slideTween.position = this._slideTween.state;
            // }
            // this.skipButtonAnim();
            super.performLayout();
            // if (this._slideTween)
            // {
            //     this._slideTween.refresh();
            // }
        }

    }

    export namespace MobileMenuHolder
    {
        export enum Mode
        {
            Menu,
            Bet
        }

        export interface TweenSettings
        {
            /** a delay in ms to wait before starting the open tween and after finishing the close tween */
            moveDelay: number;
            /** time taken to move the chevron */
            moveTime: number;
            /** time taken for the chevron's rotation */
            rotateTime: number;
            /** ease function to be used moving the chevron on opening the menu */
            easeOpen?: RS.EaseFunction;
            /** ease function to be used moving the chevron on closing the menu */
            easeClosed?: RS.EaseFunction;
            /** ease function to be used rotating the chevron */
            easeRotate?: RS.EaseFunction;
        }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            container: RS.Flow.Container.Settings;

            background: RS.Flow.Background.Settings;

            chevronButton: RS.Flow.Button.Settings;

            buttonTween?: TweenSettings;
        }

        export interface RuntimeData
        {
            menuData: MobileMenu.RuntimeData;
            betMenuData: MobileBetMenu.RuntimeData;
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            asset: Assets.CommonUI.BottomBar.Buttons,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export const defaultButtonTween: TweenSettings =
        {
            moveTime: 500,
            moveDelay: 0,
            rotateTime: 250,
            easeOpen: RS.Ease.linear,
            easeClosed: RS.Ease.linear,
            easeRotate: RS.Ease.linear
        };

        export let defaultSettings: Settings =
        {
            container:
            {
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.0, y: 0.0 },
                dockAlignment: { x: 0.0, y: 0.0 }
            },
            background:
            {
                kind: RS.Flow.Background.Kind.SolidColor,
                borderSize: 0,
                borderColor: RS.Util.Colors.black,
                color: RS.Util.Colors.black,
                cornerRadius: 20,
                corners: RS.Flow.Background.Corners.leftOnly
            },
            chevronButton:
            {
                name: "Chevron",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_chevron },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_chevron },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_chevron_dim },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.mbtn_chevron_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 1.0, y: 0.9 },
                dockAlignment: { x: 1.0, y: 0.9 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "ChevronButton"
            },
            size: { w: 270, h: 685 },
            dock: RS.Flow.Dock.Float,
            floatPosition: { x: 1.0, y: 0.475 },
            dockAlignment: { x: 1.0, y: 0.5 },
            buttonTween: defaultButtonTween
        };

        export let defaultPortraitSettings: Settings | null = null;
        export let defaultLandscapeSettings: Settings | null = null;
    }

    /**
     * The MobileMenuHolder element.
     */
    export const mobileMenuHolder = RS.Flow.declareElement(MobileMenuHolder, true);
}