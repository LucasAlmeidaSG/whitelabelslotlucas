namespace SG.CommonUI
{
    @RS.HasCallbacks
    export class DesktopButtonPanel extends RS.Flow.BaseElement<DesktopButtonPanel.Settings, DesktopButtonPanel.RuntimeData>
    {
        public readonly onInputOptionChanged = RS.createEvent<{ inputOption: number; optionIndex: number; }>();

        /** Published when the big bet button has been clicked. */
        public readonly onBigBetClicked = RS.createSimpleEvent();

        /** Published when the help button has been clicked. */
        public readonly onHelpClicked = RS.createSimpleEvent();

        /** Published when the autoplay button has been clicked. */
        public readonly onAutoplayClicked = RS.createSimpleEvent();

        /** Published when the stop autoplay button has been clicked. */
        public readonly onStopAutoplayClicked = RS.createSimpleEvent();

        /** Published when the spin button has been clicked. */
        public get onSpinButtonClicked() { return this._spinButton.onClicked; }

        /** Published when the stop button has been clicked. */
        public get onSkipButtonClicked() { return this._stopButton.onClicked; }

        protected _innerPanel: RS.Flow.Container;
        protected _background: RS.Flow.Background;
        protected _buttonList: RS.Flow.List;
        protected _spinnerList: RS.Flow.List;
        protected _spinners: RS.Flow.Titled<RS.Flow.Spinner>[];
        protected _gameName: RS.Flow.Label;

        protected _spinButton: RS.Flow.Button;
        protected _stopButton: RS.Flow.Button;
        protected _buttons: RS.Flow.Button[];

        private __autoplayStopping: boolean = false;

        public constructor(settings: DesktopButtonPanel.Settings, runtimeData: DesktopButtonPanel.RuntimeData)
        {
            super(settings, runtimeData);

            if (!this.settings.legacyDisableInnerPanel)
            {
                this._innerPanel = RS.Flow.container.create({
                    dock: RS.Flow.Dock.Bottom,
                    size: { w: 0, h: this.settings.innerPanelHeight }
                }, this);
            }

            this._background = RS.Flow.background.create(this.settings.background, this._innerPanel || this);

            this._spinButton = RS.Flow.button.create(this.settings.spinButton, this);
            this._stopButton = RS.Flow.button.create(this.settings.stopButton, this);

            const canSpin = RS.Observable.reduce([runtimeData.canSpinArbiter, runtimeData.enableInteractionArbiter], RS.Arbiter.AndResolver);
            RS.Disposable.bind(canSpin, this);

            const canSkipAndAllowQuickSpin = RS.Observable.reduce([runtimeData.canSkipArbiter, runtimeData.canQuickSpinArbiter], RS.Arbiter.AndResolver);
            const canSkipAndAllowQuickSpinOrCanSpin = RS.Observable.reduce([canSpin, canSkipAndAllowQuickSpin], RS.Arbiter.OrResolver);
            RS.Disposable.bind(canSkipAndAllowQuickSpin, this);
            RS.Disposable.bind(canSkipAndAllowQuickSpinOrCanSpin, this);

            this._spinButton.bindToObservables({ enabled: canSkipAndAllowQuickSpinOrCanSpin });
            this._stopButton.bindToObservables({ enabled: runtimeData.canSkipArbiter });

            const canChangeBet = RS.Observable.reduce([runtimeData.canChangeBetArbiter, runtimeData.enableInteractionArbiter], RS.Arbiter.AndResolver);
            RS.Disposable.bind(canChangeBet, this);

            const canAutoplay = RS.Observable.reduce([runtimeData.canAutoplayObservable, runtimeData.enableInteractionArbiter], RS.Arbiter.AndResolver);
            RS.Disposable.bind(canAutoplay, this);

            const spinOrSkipHandler = runtimeData.spinOrSkipArbiter.onChanged(this.handleSpinOrSkipChanged);
            const quickSpinHandler = runtimeData.canQuickSpinArbiter.onChanged(this.handleSpinOrSkipChanged);
            RS.Disposable.bind(spinOrSkipHandler, this);
            RS.Disposable.bind(quickSpinHandler, this);

            this.handleSpinOrSkipChanged();

            this._buttonList = RS.Flow.list.create(settings.buttonList, this._background);

            this._buttons = [];
            if (this.settings.showBigBetButton && runtimeData.showBigBetButton)
            {
                const buttonSettings = settings.bigBetButton;
                const btn = RS.Flow.button.create(buttonSettings, (this.settings.useListForButtons) ? this._buttonList : this._background);
                if (runtimeData.enableInteractionArbiter)
                {
                    btn.bindToObservables({ enabled: runtimeData.enableInteractionArbiter });
                }
                btn.onClicked(this.handleBigBetClicked);
                this._buttons.push(btn);
            }

            {
                const buttonSettings = settings.helpButton;
                const btn = RS.Flow.button.create(buttonSettings, (this.settings.useListForButtons) ? this._buttonList : this._background);
                if (runtimeData.enableInteractionArbiter)
                {
                    const enabledArbiter = RS.Observable.reduce([ runtimeData.enableInteractionArbiter, runtimeData.paytableEnabledArbiter ], RS.Arbiter.AndResolver);
                    btn.bindToObservables({ enabled: enabledArbiter });
                    RS.Disposable.bind(enabledArbiter, this);
                }
                btn.onClicked(this.handleHelpClicked);
                this._buttons.push(btn);
            }

            if (runtimeData.showAutoplayButton)
            {
                const startbtn = RS.Flow.button.create(settings.autoplayButton, (this.settings.useListForButtons) ? this._buttonList : this._background);
                startbtn.bindToObservables({ enabled: canAutoplay });
                startbtn.onClicked(this.handleAutoplayClicked);
                this._buttons.push(startbtn);

                if (runtimeData.autoplaysRemainingObservable)
                {
                    const autoplaysActive = runtimeData.autoplaysRemainingObservable.map((autoplaysRemaining) => autoplaysRemaining > 0);
                    const autoplaysNotActive = runtimeData.autoplaysRemainingObservable.map((autoplaysRemaining) => autoplaysRemaining === 0);
                    RS.Disposable.bind(autoplaysActive, this);
                    RS.Disposable.bind(autoplaysNotActive, this);

                    // Only hide autoplay button if set when autoplay entered
                    if (!settings.showAutoplayButtonInAutoplay) { startbtn.bindToObservables({ visible: autoplaysNotActive }); }

                    const stopbtn = RS.Flow.button.create(settings.stopAutoplayButton, (this.settings.useListForButtons) ? this._buttonList : this._background);
                    stopbtn.bindToObservables({ visible: autoplaysActive });
                    stopbtn.onClicked(this.handleStopAutoplayClicked);
                    this._buttons.push(stopbtn);
                    if (this.settings.stopAutoplayButtonLabel)
                    {
                        const label = RS.Flow.label.create(this.settings.stopAutoplayButtonLabel, stopbtn);
                        const textObservable = runtimeData.autoplaysRemainingObservable.map((o) => `${o}`);
                        RS.Disposable.bind(textObservable, this);
                        label.bindToObservables( { text: textObservable });
                    }
                }
            }

            this._spinnerList = RS.Flow.list.create(settings.spinnerList, this._background);

            this._spinners = [];
            for (let i = 0, l = runtimeData.spinners.length; i < l; ++i)
            {
                const spinnerSettings = runtimeData.spinners[i];
                const spinner = RS.Flow.spinner.create(spinnerSettings.spinner);
                if (runtimeData.enableInteractionArbiter)
                {
                    spinner.bindToObservables({ enabled: canChangeBet || runtimeData.enableInteractionArbiter });
                }
                spinner.onValueChanged((newValue) =>
                {
                    const idx = spinner.options.indexOf(newValue);
                    this.onInputOptionChanged.publish({ inputOption: i, optionIndex: idx });
                });
                const titled = new RS.Flow.Titled(spinnerSettings.title, spinner);
                this._spinnerList.addChild(titled);
                this._spinners.push(titled);
            }

            // Game name label
            if (this.settings.showGameName)
            {
                this._gameName = RS.Flow.label.create(this.settings.gameNameLabel, this);
            }
        }

        public getSpinner(index: number)
        {
            return this._spinners[index].innerElement;
        }

        @RS.Callback
        protected handleBigBetClicked(): void
        {
            this.onBigBetClicked.publish();
        }

        @RS.Callback
        protected handleHelpClicked(): void
        {
            this.onHelpClicked.publish();
        }

        @RS.Callback
        protected handleAutoplayClicked(): void
        {
            this.onAutoplayClicked.publish();
        }

        @RS.Callback
        protected handleStopAutoplayClicked(): void
        {
            this.onStopAutoplayClicked.publish();
            if (this.settings.hideSpinButtonsOnAutoplay)
            {
                this.__autoplayStopping = true;
                this.handleSpinOrSkipChanged();
            }
        }

        @RS.Callback
        protected handleSpinOrSkipChanged(): void
        {
            // If hiding buttons in autoplay and we're in autoplay, hide buttons
            const autoplaysRemaining = this.runtimeData.autoplaysRemainingObservable && this.runtimeData.autoplaysRemainingObservable.value > 0;
            if (this.settings.hideSpinButtonsOnAutoplay)
            {
                if (autoplaysRemaining && !this.__autoplayStopping)
                {
                    this._spinButton.visible = false;
                    this._stopButton.visible = false;
                    return;
                }
                else if (!autoplaysRemaining && this.__autoplayStopping)
                {
                    this.__autoplayStopping = false;
                }
            }

            if (this.runtimeData.spinOrSkipArbiter.value === "spin" || this.runtimeData.canQuickSpinArbiter.value)
            {
                this._spinButton.visible = true;
                this._stopButton.visible = false;
            }
            else
            {
                this._spinButton.visible = false;
                this._stopButton.visible = true;
            }
        }
    }

    export const desktopButtonPanel = RS.Flow.declareElement(DesktopButtonPanel, true);

    export namespace DesktopButtonPanel
    {
        export interface SpinnerSettings
        {
            title: RS.Flow.Titled.Settings;
            spinner: RS.Flow.Spinner.Settings;
        }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            useListForButtons: boolean;
            innerPanelHeight: number;
            legacyDisableInnerPanel?: boolean;
            buttonList: RS.Flow.List.Settings;
            spinnerList: RS.Flow.List.Settings;
            background: RS.Flow.Background.Settings;
            spinButton: RS.Flow.Button.Settings;
            stopButton: RS.Flow.Button.Settings;
            bigBetButton: RS.Flow.Button.Settings;
            helpButton: RS.Flow.Button.Settings;
            autoplayButton: RS.Flow.Button.Settings;
            stopAutoplayButton: RS.Flow.Button.Settings;
            stopAutoplayButtonLabel?: RS.Flow.Label.Settings;
            gameNameLabel: RS.Flow.Label.Settings;
            showGameName: boolean;
            showBigBetButton: boolean;
            hideSpinButtonsOnAutoplay?: boolean;
            showAutoplayButtonInAutoplay?: boolean;
        }

        export interface RuntimeData
        {
            spinOrSkipArbiter: RS.IArbiter<"spin"|"skip">;
            canSpinArbiter: RS.IArbiter<boolean>;
            canSkipArbiter: RS.IArbiter<boolean>;
            canChangeBetArbiter: RS.IArbiter<boolean>;
            paytableEnabledArbiter: RS.IArbiter<boolean>;
            canQuickSpinArbiter: RS.IArbiter<boolean>;
            enableInteractionArbiter: RS.IArbiter<boolean>;
            autoplaysRemainingObservable: RS.IReadonlyObservable<number>;
            canAutoplayObservable?: RS.IObservable<boolean>;

            showBigBetButton: boolean;
            showAutoplayButton: boolean;

            spinners: SpinnerSettings[];
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.CommonUI.BottomBar.Buttons,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export let defaultSpinnerSettings: RS.Flow.Spinner.Settings =
        {
            dock: RS.Flow.Dock.None,
            spacing: RS.Flow.Spacing.horizontal(5),
            decrementButton:
            {
                ...RS.Flow.Spinner.defaultSettings.decrementButton,
                name: "Decrement",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_decrease },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_decrease_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_decrease_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_decrease_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null
            },
            incrementButton:
            {
                ...RS.Flow.Spinner.defaultSettings.incrementButton,
                name: "Increment",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_increase },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_increase_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_increase_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_increase_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null
            },
            background:
            {
                kind: RS.Flow.Background.Kind.SolidColor,
                dock: RS.Flow.Dock.Fill,
                ignoreParentSpacing: true,
                cornerRadius: 0,
                color: new RS.Util.Color(0x191919),
                borderColor: new RS.Util.Color(0x4f5357),
                borderSize: 2
            },
            label:
            {
                ...RS.Flow.Spinner.defaultSettings.label,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28
            },
            size: { w: 250, h: 42 },
            expand: RS.Flow.Expand.Disallowed,
            options:
            {
                type: RS.Flow.Spinner.Type.Options,
                options: [0]
            }
        };

        export function getSpinnerSettings(title: RS.Localisation.LocalisableString = Translations.UI.TotalBet, incSelID?: string, decSelID?: string): SpinnerSettings
        {
            return {
                title:
                {
                    ...RS.Flow.Titled.defaultSettings,
                    title:
                    {
                        ...RS.Flow.Titled.defaultSettings.title,
                        font: RS.Fonts.Frutiger.CondensedMedium,
                        fontSize: 16,
                        text: title
                    },
                    dock: RS.Flow.Dock.None,
                    sizeToContents: true,
                    expand: RS.Flow.Expand.Disallowed,
                    spacing: RS.Flow.Spacing.vertical(3)
                },
                spinner:
                {
                    ...defaultSpinnerSettings,
                    incrementButton:
                    {
                        ...defaultSpinnerSettings.incrementButton,
                        seleniumId: incSelID
                    },
                    decrementButton:
                    {
                        ...defaultSpinnerSettings.decrementButton,
                        seleniumId: decSelID
                    }
                }
            };
        }

        export let defaultSettings: Settings =
        {
            useListForButtons: true,
            background:
            {
                kind: RS.Flow.Background.Kind.Gradient,
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                    [
                        { offset: 0.0, color: new RS.Util.Color(0x191e27) },
                        { offset: 1.0, color: new RS.Util.Color(0x313a4c) }
                    ],
                    posA: { x: 0, y: 0 },
                    posB: { x: 0, y: 4 }
                },
                textureSize: { w: 4, h: 4 }
            },
            innerPanelHeight: 94,
            buttonList:
            {
                ...RS.Flow.List.defaultSettings,
                dock: RS.Flow.Dock.Left,
                direction: RS.Flow.List.Direction.LeftToRight,
                expand: RS.Flow.Expand.Allowed,
                size: { w: 260, h: 0 },
                sizeToContents: false,
                evenlySpaceItems: true,
                spacing: RS.Flow.Spacing.all(10)
            },
            spinnerList:
            {
                dock: RS.Flow.Dock.Right,
                size: { w: 260, h: 0 },
                direction: RS.Flow.List.Direction.TopToBottom,
                contentAlignment: { x: 0.5, y: 0.5 },
                spacing: RS.Flow.Spacing.vertical(10)
            },
            dock: RS.Flow.Dock.Bottom,
            size: { w: 650, h: 132 },
            expand: RS.Flow.Expand.Disallowed,
            spinButton:
            {
                name: "Spin",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_spin },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_spin_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_spin_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_spin_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.0 },
                dockAlignment: { x: 0.5, y: 0.0 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "SpinButton"
            },
            stopButton:
            {
                name: "Stop",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stop },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stop_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stop_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stop_dim },
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.0 },
                dockAlignment: { x: 0.5, y: 0.0 },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "StopSpinButton"
            },
            bigBetButton:
            {
                name: "Big Bet",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_bigbet },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_bigbet_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_bigbet_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_bigbet_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "BigBetButton"
            },
            helpButton:
            {
                name: "Help",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_help },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_help_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_help_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_help_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "HelpButton"
            },
            autoplayButton:
            {
                name: "Autoplay",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_autoplay },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_autoplay_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_autoplay_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_autoplay_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "AutoplayButton"
            },
            stopAutoplayButton:
            {
                name: "Stop Autoplay",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stopautoplay },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stopautoplay_hover },
                pressbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stopautoplay_down },
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Buttons.btn_stopautoplay_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "StopAutoSpinButton"
            },
            gameNameLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                name: "Game Name",
                text: "GAME NAME HERE",
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                textColor: RS.Util.Colors.white,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 1.0 },
                dockAlignment: { x: 0.5, y: 1.0 },
                seleniumId: "GameTitle"
            },
            showGameName: false,
            showBigBetButton: false
        };

        export let defaultPortraitSettings: Settings | null = null;
        export let defaultLandscapeSettings: Settings | null = null;
    }
}
