namespace SG.CommonUI
{
    export class Meter extends RS.Flow.BaseElement<Meter.Settings> implements SGI.Selenium.ISeleniumComponent, SGI.Selenium.ISeleniumDescriber
    {
        protected _list: RS.Flow.List;
        protected _titleLabel: RS.Flow.Label;
        protected _delimiterLabel: RS.Flow.Label;
        protected _valueLabel: RS.Flow.Label;

        protected _value: number;
        protected _textOverride: string;

        /**
         * Gets or sets the numeric value of this meter.
         */
        @RS.CheckDisposed
        public get value() { return this._value; }
        public set value(value)
        {
            if (this._value === value) { return; }
            if (this._textOverride == null)
            {
                this.displayValue(value);
            }
            this._value = value;
        }

        /**
         * Gets or sets the text string to display instead of the value.
         */
        @RS.CheckDisposed
        public get textOverride() { return this._textOverride; }
        public set textOverride(value)
        {
            if (this._textOverride === value) { return; }
            if (value != null)
            {
                this._valueLabel.text = value;
            }
            else
            {
                this.displayValue(this._value);
            }
            this._textOverride = value;
        }

        /** Gets the CSS ID for the selenium div element for this meter. */
        public get seleniumId() { return this.settings.seleniumId || null; }

        public constructor(settings: Meter.Settings)
        {
            super(settings, undefined);

            // Setup list
            this._list = RS.Flow.list.create({
                direction: RS.Flow.List.Direction.LeftToRight,
                dock: RS.Flow.Dock.Fill,
                sizeToContents: true
            }, this);

            // Setup labels
            this._titleLabel = RS.Flow.label.create(settings.titleLabel, this._list);
            this._delimiterLabel = RS.Flow.label.create(settings.delimiterLabel, this._list);
            this._valueLabel = RS.Flow.label.create(settings.valueLabel, this._list);
        }

        /**
         * Gets specific attributes for the selenium component for this meter.
         */
        public seleniumDescribe(): [string, string | number][]
        {
            const attrs: [string, string | number][] =
            [
                [ "text", this._valueLabel.innerText.text ]
            ];
            if (this.settings.isCurrency)
            {
                attrs.push([ "currency", this.value ]);
            }
            else
            {
                attrs.push([ "value", this._value ]);
            }
            return attrs;
        }

        protected displayValue(value: number): void
        {
            if (this.settings.blankValueIfZero && value <= 0)
            {
                this._valueLabel.text = "";
                return;
            }

            const cf = this.currencyFormatter;
            if (this.settings.isCurrency && cf)
            {
                this._valueLabel.text = cf.format(Math.round(value));
            }
            else
            {
                this._valueLabel.text = value.toString();
            }
        }
    }

    export const meter = RS.Flow.declareElement(Meter);

    export namespace Meter
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            titleLabel: RS.Flow.Label.Settings;
            delimiterLabel: RS.Flow.Label.Settings;
            valueLabel: RS.Flow.Label.Settings;
            isCurrency: boolean;
            hiddenValueText: RS.Localisation.LocalisableString;
            seleniumId?: string;
            blankValueIfZero?: boolean;
        }

        export let defaultSettings: Settings =
        {
            titleLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                text: "TITLE",
                textColor: RS.Util.Colors.white,
                sizeToContents: true
            },
            delimiterLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                text: " : ",
                textColor: RS.Util.Colors.white,
                sizeToContents: true
            },
            valueLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                text: "VALUE",
                textColor: RS.Util.Colors.white,
                sizeToContents: true
            },
            sizeToContents: true,
            isCurrency: false,
            hiddenValueText: "",
            blankValueIfZero: false
        };
    }
}