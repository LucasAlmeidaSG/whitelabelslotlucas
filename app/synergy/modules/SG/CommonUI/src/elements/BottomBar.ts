/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/Translations.ts" />

/// <reference path="MessageLabel.ts" />
/// <reference path="ClockLabel.ts" />
/// <reference path="Meter.ts" />

namespace SG.CommonUI
{
    export class BottomBar extends RS.Flow.BaseElement<BottomBar.Settings, BottomBar.RuntimeData>
    {
        protected _mode: BottomBar.Mode;

        @RS.AutoDispose protected _topPanel: RS.Flow.Container;
        @RS.AutoDispose protected _topBG: RS.Flow.Background;
        @RS.AutoDispose protected _topRibbon: RS.Flow.Background;

        @RS.AutoDispose protected _bottomPanel: RS.Flow.Container;
        @RS.AutoDispose protected _bottomBG: RS.Flow.Background;
        @RS.AutoDispose protected _bottomRibbon: RS.Flow.Background;

        @RS.AutoDispose protected _cornerPanel: RS.Flow.Container;
        @RS.AutoDispose protected _cornerTopSection: RS.Flow.Container;
        @RS.AutoDispose protected _cornerRibbonTop: RS.Flow.Background;
        @RS.AutoDispose protected _cornerRibbonRight: RS.Flow.Background;
        @RS.AutoDispose protected _cornerRibbonCorner: RS.Flow.Background;
        @RS.AutoDispose protected _cornerInner: RS.Flow.Container;
        @RS.AutoDispose protected _cornerInnerBG: RS.Flow.Background;
        @RS.AutoDispose protected _cornerList: RS.Flow.List;
        @RS.AutoDispose protected _cornerRetractableList: RS.Flow.List;

        @RS.AutoDispose protected _leftList: RS.Flow.List;
        @RS.AutoDispose protected _rightList: RS.Flow.List;
        @RS.AutoDispose protected _balancePanel: RS.Flow.Container;
        @RS.AutoDispose protected _winPanel: RS.Flow.Container;

        @RS.AutoDispose protected _logo: RS.Flow.Image;
        @RS.AutoDispose protected _clock: ClockLabel;
        @RS.AutoDispose protected _backButton: RS.Flow.Button;
        @RS.AutoDispose protected _settingsButton: RS.Flow.Button;
        @RS.AutoDispose protected _helpButton: RS.Flow.Button;

        @RS.AutoDispose protected _balanceMeter: Meter;
        @RS.AutoDispose protected _winMeter: Meter;
        @RS.AutoDispose protected _betMeter: Meter;
        @RS.AutoDispose protected _linesMeter: Meter;
        @RS.AutoDispose protected _waysMeter: Meter;

        @RS.AutoDispose protected _message: MessageLabel;
        @RS.AutoDispose protected _gameName: RS.Flow.Label;

        /** Published when the back button has been clicked. */
        public get onBackClicked() { return this._backButton.onClicked; }

        /** Published when the settings button has been clicked. */
        public get onSettingsClicked() { return this._settingsButton.onClicked; }

        /** Published when the help button has been clicked. */
        public get onHelpClicked() { return this._helpButton.onClicked; }

        protected _balance: number;
        protected _hideBalance: boolean;
        protected _winAmount: number;
        protected _totalBet: number;
        protected _lines: number;
        protected _ways: number;
        protected _replayMode: boolean = false;

        /** Gets or sets the balance to display. */
        public get balance() { return this._balance; }
        public set balance(value)
        {
            if (value === this._balance) { return; }
            this._balance = value;
            this.updateBalanceTextOverride();
            this._balanceMeter.value = value;
        }

        /** Whether or not to show the current balance */
        public get replayMode() { return this._replayMode; }
        public set replayMode(value)
        {
            if (value === this._replayMode) { return; }
            this._replayMode = value;
            this.updateBalanceTextOverride();
        }

        /** Gets or sets the win amount to display. */
        public get winAmount() { return this._winAmount; }
        public set winAmount(value)
        {
            if (value === this._winAmount) { return; }
            this._winAmount = value;
            this._winMeter.textOverride = this.settings.winMeterBlank && value === 0 ? "" : null;
            this._winMeter.value = value;
        }

        /** Gets or sets the total bet amount to display. */
        public get totalBet() { return this._totalBet; }
        public set totalBet(value)
        {
            if (value === this._totalBet) { return; }
            this._totalBet = value;
            this._betMeter.value = value;
        }

        /** Gets or sets the lines amount to display. */
        public get lines() { return this._lines; }
        public set lines(value)
        {
            if (value === this._lines) { return; }
            this._lines = value;
            this._linesMeter.value = value;
        }

        /** Gets or sets the ways amount to display */
        public get ways() { return this._ways; }
        public set ways(value)
        {
            if (value === this._ways) { return; }
            this._ways = value;
            this._waysMeter.value = value;
        }

        /** Gets or sets the current mode of this bar. */
        public get mode() { return this._mode; }
        public set mode(value)
        {
            if (value === this._mode) { return; }
            this._mode = value;
            switch (value)
            {
                case BottomBar.Mode.Landscape:
                case BottomBar.Mode.MobileLandscape:
                    this.layoutLandscape(value === BottomBar.Mode.MobileLandscape);
                    break;
                case BottomBar.Mode.Portrait:
                case BottomBar.Mode.MobilePortrait:
                    this.layoutPortrait(value === BottomBar.Mode.MobilePortrait);
                    break;
            }
        }

        /** Gets or sets if the corner list is expanded (mobile mode only). */
        public get cornerListExpanded() { return this._cornerRetractableList.visible; }
        public set cornerListExpanded(value) { this._cornerRetractableList.visible = value; }

        /** Gets the height of the horizontal panel (without the left list). */
        public get bottomPanelHeight() { return this._bottomPanel.layoutSize.h + (this._topPanel.visible ? this._topPanel.layoutSize.h : 0); }

        public constructor(settings: BottomBar.Settings, runtimeData: BottomBar.RuntimeData)
        {
            super(settings, runtimeData);

            // Setup panels
            this._cornerPanel = RS.Flow.container.create({ dock: RS.Flow.Dock.Left, size: settings.cornerSize, sizeToContents: true, expand: RS.Flow.Expand.Disallowed, name: "Corner Panel" }, this);
            this._bottomPanel = RS.Flow.container.create({ dock: RS.Flow.Dock.Bottom, size: { w: 0, h: settings.landscapeSize.h }, name: "Bottom Panel" }, this);
            this._topPanel = RS.Flow.container.create({ dock: RS.Flow.Dock.Bottom, size: { w: 0, h: settings.landscapeSize.h }, name: "Top Panel" }, this);
            this._balancePanel = RS.Flow.container.create({ dock: RS.Flow.Dock.Left, name: "Balance Panel" });
            this._winPanel = RS.Flow.container.create({ dock: RS.Flow.Dock.Right, name: "Win Panel" });
            this._cornerTopSection = RS.Flow.container.create({ dock: RS.Flow.Dock.Top, sizeToContents: true, name: "Corner Top Section" });

            // Setup backgrounds
            this._topBG = RS.Flow.background.create(settings.background);
            this._topRibbon = RS.Flow.background.create(settings.ribbonHorizontal);
            this._bottomBG = RS.Flow.background.create(settings.background);
            this._bottomRibbon = RS.Flow.background.create(settings.ribbonHorizontal);
            this._cornerRibbonTop = RS.Flow.background.create({
                dock: RS.Flow.Dock.Top,
                kind: RS.Flow.Background.Kind.SolidColor,
                color: settings.ribbonColorB,
                size: { w: 0, h: 4 },
                sizeToContents: false,
                borderSize: 0,
                cornerRadius: 0,
                expand: RS.Flow.Expand.HorizontalOnly
            });
            this._cornerInner = RS.Flow.container.create({
                name: "Corner Inner",
                dock: RS.Flow.Dock.Fill,
                spacing: { left: 0, top: 2, right: 2, bottom: 0 }
            });
            this._cornerRibbonRight = RS.Flow.background.create(settings.ribbonVertical);
            this._cornerRibbonCorner = RS.Flow.background.create({
                name: "Corner Ribbon Corner",
                kind: RS.Flow.Background.Kind.Image,
                asset: settings.ribbonCornerAsset,
                tint: settings.ribbonColorB,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                dock: RS.Flow.Dock.Right
            });
            this._cornerInnerBG = RS.Flow.background.create({ ...settings.background, ignoreParentSpacing: false });

            // Setup lists
            this._leftList = RS.Flow.list.create({
                name: "Left List",
                dock: RS.Flow.Dock.Left,
                sizeToContents: true,
                expand: RS.Flow.Expand.Allowed,
                expandItems: true,
                direction: RS.Flow.List.Direction.LeftToRight,
                dockAlignment: { x: 0.0, y: 0.5 },
                spacing: RS.Flow.Spacing.horizontal(settings.listSpacing),
                legacy: false
            });
            this._rightList = RS.Flow.list.create({
                name: "Right List",
                dock: RS.Flow.Dock.Right,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                direction: RS.Flow.List.Direction.LeftToRight,
                dockAlignment: { x: 1.0, y: 0.5 },
                spacing: RS.Flow.Spacing.horizontal(settings.listSpacing)
            });
            this._cornerList = RS.Flow.list.create({
                name: "Corner List",
                dock: RS.Flow.Dock.Bottom,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                direction: RS.Flow.List.Direction.BottomToTop,
                dockAlignment: { x: 0.5, y: 0.0 },
                spacing: RS.Flow.Spacing.vertical(settings.cornerListSpacing)
            });
            this._cornerRetractableList = RS.Flow.list.create({
                name: "Corner Retractable List",
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                direction: RS.Flow.List.Direction.BottomToTop,
                spacing: RS.Flow.Spacing.vertical(settings.listSpacing)
            });

            // Logo
            this._logo = RS.Flow.image.create({
                name: "Logo",
                dock: RS.Flow.Dock.None,
                sizeToContents: true,
                ignoreParentSpacing: false,
                expand: RS.Flow.Expand.Disallowed,
                kind: RS.Flow.Image.Kind.Bitmap,
                asset: settings.logoAsset
            });

            // Clock
            this._clock = clockLabel.create(settings.clockLabel);

            // Buttons
            this._backButton = RS.Flow.button.create(settings.backButton);
            this._settingsButton = RS.Flow.button.create(settings.settingsButton);
            this._helpButton = RS.Flow.button.create(settings.helpButton);
            if (runtimeData.enableInteractionArbiter)
            {
                this._backButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._settingsButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
                this._helpButton.bindToObservables({ enabled: this.runtimeData.enableInteractionArbiter });
            }

            // Meters
            this._balanceMeter = meter.create(this.settings.balanceMeter);
            this._winMeter = meter.create(this.settings.winMeter);
            this._betMeter = meter.create(this.settings.totalBetMeter);
            this._linesMeter = meter.create(this.settings.linesMeter);
            this._waysMeter = meter.create(this.settings.waysMeter);

            // Message label
            this._message = messageLabel.create(this.settings.messageLabel);
            if (runtimeData.messageTextArbiter)
            {
                this._message.bindToObservables({
                    text: runtimeData.messageTextArbiter
                });
            }

            // Game name label
            if (this.settings.showGameName)
            {
                this._gameName = RS.Flow.label.create(this.settings.gameNameLabel);
            }
        }

        protected updateBalanceTextOverride(): void
        {
            if (this._replayMode)
            {
                this._balanceMeter.textOverride = RS.Localisation.resolveLocalisableString(this.locale, this.settings.balanceReplayText);
            }
            else if (this.settings.balanceMeterBlank && this._balance === 0)
            {
                this._balanceMeter.textOverride = "";
            }
            else
            {
                this._balanceMeter.textOverride = null;
            }
        }

        protected layoutLandscape(mobile: boolean): void
        {
            this.size = mobile ? this.settings.mobileLandscapeSize : this.settings.landscapeSize;
            this._topPanel.visible = false;
            this._bottomPanel.visible = true;
            this._cornerPanel.visible = mobile;

            this._topPanel.removeAllChildren();
            this._bottomPanel.removeAllChildren();

            this._bottomPanel.addChild(this._bottomBG);
            this._bottomPanel.addChild(this._bottomRibbon);
            if (this._gameName) { this._bottomPanel.addChild(this._gameName); }

            this._leftList.removeAllChildren();
            this._rightList.removeAllChildren();

            this._rightList.dock = RS.Flow.Dock.Right;
            this._leftList.dock = RS.Flow.Dock.Left;

            if (mobile)
            {
                this.layoutCornerPanel();
            }
            else
            {
                this._leftList.addChild(this._logo);
                this._leftList.addChild(this._clock);
                this.createVDivider(this._leftList);

                this._backButton.scaleFactor = 1.0;
                this._settingsButton.scaleFactor = 1.0;
                this._helpButton.scaleFactor = 1.0;
                if (this.runtimeData.showBackButton) { this._leftList.addChild(this._backButton); }
                this._leftList.addChild(this._settingsButton);
                if (this.runtimeData.showHelpButton) { this._leftList.addChild(this._helpButton); }
                this.createVDivider(this._leftList);
            }

            this._leftList.addChild(this._balanceMeter);
            this._balanceMeter.dock = RS.Flow.Dock.None;
            //this.createSpacer(this._leftList, this.settings.meterSpacing);
            this._winMeter.dock = RS.Flow.Dock.None;
            this._leftList.addChild(this._winMeter);
            this.createVDivider(this._leftList);

            this.createVDivider(this._rightList);
            this._betMeter.dock = RS.Flow.Dock.None;
            this._rightList.addChild(this._betMeter);
            //this.createSpacer(this._rightList, this.settings.meterSpacing);
            if (this.settings.showLinesMeter)
            {
                this._linesMeter.dock = RS.Flow.Dock.None;
                this._rightList.addChild(this._linesMeter);
            }
            if (this.settings.showWaysMeter)
            {
                this._waysMeter.dock = RS.Flow.Dock.None;
                this._rightList.addChild(this._waysMeter);
            }

            this._bottomPanel.addChild(this._leftList);
            this._bottomPanel.addChild(this._rightList);
            this._bottomPanel.addChild(this._message);

            this.invalidateLayout();
        }

        protected layoutPortrait(mobile: boolean): void
        {
            this.size = mobile ? this.settings.mobilePortraitSize : this.settings.portraitSize;
            this._topPanel.visible = true;
            this._bottomPanel.visible = true;
            this._cornerPanel.visible = mobile;

            this._balancePanel.removeAllChildren();
            this._winPanel.removeAllChildren();

            this._topPanel.removeAllChildren();
            this._bottomPanel.removeAllChildren();

            this._topPanel.addChild(this._topBG);
            this._topPanel.addChild(this._topRibbon);
            if (this._gameName) { this._topPanel.addChild(this._gameName); }

            this._bottomPanel.addChild(this._bottomBG);
            this._bottomPanel.addChild(this._bottomRibbon);

            this._leftList.removeAllChildren();
            this._rightList.removeAllChildren();

            if (mobile)
            {
                this.layoutCornerPanel();

                this.createHSpacer(this._topPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Left;

                if (this.settings.showLinesMeter)
                {
                    this._linesMeter.dock = RS.Flow.Dock.Left;
                    this._topPanel.addChild(this._linesMeter);
                }
                if (this.settings.showWaysMeter)
                {
                    this._waysMeter.dock = RS.Flow.Dock.Left;
                    this._topPanel.addChild(this._waysMeter);
                }

                this.createHSpacer(this._topPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Right;
                this._winMeter.dock = RS.Flow.Dock.Right;
                this._topPanel.addChild(this._winMeter);

                this._leftList.dock = RS.Flow.Dock.Fill;
                this.createVDivider(this._leftList);
                this._leftList.addChild(this._message);
                this.createVDivider(this._leftList);
                this._topPanel.addChild(this._leftList);

                this.createHSpacer(this._bottomPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Left;
                this._betMeter.dock = RS.Flow.Dock.Left;
                this._bottomPanel.addChild(this._betMeter);

                this.createHSpacer(this._bottomPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Right;
                this._balanceMeter.dock = RS.Flow.Dock.Right;
                this._bottomPanel.addChild(this._balanceMeter);
            }
            else
            {
                this._leftList.dock = RS.Flow.Dock.Left;
                this._leftList.addChild(this._logo);
                this._leftList.addChild(this._clock);
                this.createVDivider(this._leftList);

                this._backButton.scaleFactor = 1.0;
                this._settingsButton.scaleFactor = 1.0;
                this._helpButton.scaleFactor = 1.0;
                if (this.runtimeData.showBackButton) { this._leftList.addChild(this._backButton); }
                this._leftList.addChild(this._settingsButton);
                if (this.runtimeData.showHelpButton) { this._leftList.addChild(this._helpButton); }
                this.createVDivider(this._leftList);

                this.createVDivider(this._balancePanel).dock = RS.Flow.Dock.Right;
                this.createHSpacer(this._balancePanel, this.settings.listSpacing).dock = RS.Flow.Dock.Left;
                this._balancePanel.addChild(this._balanceMeter);
                this._balancePanel.size = { w: this._leftList.desiredSize.w - this.settings.listSpacing * 0.5, h: 0 };
                this._balanceMeter.dock = RS.Flow.Dock.Left;
                this._topPanel.addChild(this._balancePanel);

                this.createVDivider(this._winPanel).dock = RS.Flow.Dock.Left;
                this.createHSpacer(this._winPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Right;
                this._winPanel.addChild(this._winMeter);
                this._winPanel.size = this._balancePanel.size;
                this._winMeter.dock = RS.Flow.Dock.Right;
                this._topPanel.addChild(this._winPanel);

                this._topPanel.addChild(this._message);

                this.createHSpacer(this._bottomPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Left;
                this._bottomPanel.addChild(this._leftList);

                this._betMeter.dock = RS.Flow.Dock.Left;
                this._bottomPanel.addChild(this._betMeter);
                this.createHSpacer(this._bottomPanel, this.settings.listSpacing).dock = RS.Flow.Dock.Right;
                if (this.settings.showLinesMeter)
                {
                    this._linesMeter.dock = RS.Flow.Dock.Right;
                    this._bottomPanel.addChild(this._linesMeter);
                }
                if (this.settings.showWaysMeter)
                {
                    this._waysMeter.dock = RS.Flow.Dock.Right;
                    this._bottomPanel.addChild(this._waysMeter);
                }
            }

            this.invalidateLayout();
        }

        protected layoutCornerPanel(): void
        {
            this._cornerPanel.removeAllChildren();
            this._cornerTopSection.removeAllChildren();
            this._cornerInner.removeAllChildren();
            this._cornerList.removeAllChildren();
            this._cornerRetractableList.removeAllChildren();

            this._cornerPanel.addChild(this._cornerInner);
            this._cornerInner.addChild(this._cornerInnerBG);
            this._cornerPanel.addChild(this._cornerTopSection);
            this._cornerTopSection.addChild(this._cornerRibbonCorner);
            this._cornerTopSection.addChild(this._cornerRibbonTop);
            this._cornerPanel.addChild(this._cornerRibbonRight);
            this._cornerPanel.addChild(this._cornerList);

            this._cornerList.addChild(this._logo);
            this.createHDivider(this._cornerList);
            this._cornerList.addChild(this._clock);

            this._cornerList.addChild(this._cornerRetractableList);

            this.createHDivider(this._cornerRetractableList);

            this._backButton.scaleFactor = 1.5;
            if (this.runtimeData.showBackButton) { this._cornerRetractableList.addChild(this._backButton); }
            this.createVSpacer(this._cornerRetractableList, this.settings.listSpacing);
            this._settingsButton.scaleFactor = 1.5;
            this._cornerRetractableList.addChild(this._settingsButton);
            if (this.settings.showMobileHelpButton)
            {
                this._helpButton.scaleFactor = 1.5;
                this._cornerRetractableList.addChild(this._helpButton);
            }
        }

        protected createVDivider(parent: RS.Rendering.IContainer)
        {
            return RS.Flow.background.create({
                name: "Divider",
                dock: RS.Flow.Dock.None,
                sizeToContents: false,
                expand: RS.Flow.Expand.Disallowed,
                size: this.settings.verticalDivider.size,
                kind: RS.Flow.Background.Kind.Gradient,
                gradient: this.settings.verticalDivider.gradient,
                textureSize: this.settings.verticalDivider.textureSize,
                ignoreParentSpacing: false
            }, parent);
        }

        protected createHDivider(parent: RS.Rendering.IContainer)
        {
            return RS.Flow.background.create({
                name: "Divider",
                dock: RS.Flow.Dock.None,
                sizeToContents: false,
                expand: RS.Flow.Expand.Disallowed,
                size: this.settings.horizontalDivider.size,
                kind: RS.Flow.Background.Kind.Gradient,
                gradient: this.settings.horizontalDivider.gradient,
                textureSize: this.settings.horizontalDivider.textureSize,
                ignoreParentSpacing: false
            }, parent);
        }

        protected createHSpacer(parent: RS.Rendering.IContainer, width: number)
        {
            return RS.Flow.container.create({
                name: "Spacer",
                dock: RS.Flow.Dock.None,
                sizeToContents: false,
                expand: RS.Flow.Expand.Disallowed,
                size: { w: width, h: 0 },
                ignoreParentSpacing: true
            }, parent);
        }

        protected createVSpacer(parent: RS.Rendering.IContainer, height: number)
        {
            return RS.Flow.container.create({
                name: "Spacer",
                dock: RS.Flow.Dock.None,
                sizeToContents: false,
                expand: RS.Flow.Expand.Disallowed,
                size: { w: 0, h: height },
                ignoreParentSpacing: true
            }, parent);
        }
    }

    export const bottomBar = RS.Flow.declareElement(BottomBar, true);

    export namespace BottomBar
    {
        export enum Mode { Landscape, Portrait, MobileLandscape, MobilePortrait }

        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            background: RS.Flow.Background.Settings;
            ribbonVertical: RS.Flow.Background.Settings;
            ribbonHorizontal: RS.Flow.Background.Settings;
            ribbonCornerAsset: RS.Asset.ImageReference;
            logoAsset: RS.Asset.ImageReference;
            clockLabel: ClockLabel.Settings;
            listSpacing: number;
            cornerListSpacing: number;
            verticalDivider:
            {
                gradient: RS.Rendering.Gradient;
                textureSize: RS.Dimensions;
                size: RS.Dimensions;
            };
            horizontalDivider:
            {
                gradient: RS.Rendering.Gradient;
                textureSize: RS.Dimensions;
                size: RS.Dimensions;
            };
            backButton: RS.Flow.Button.Settings;
            settingsButton: RS.Flow.Button.Settings;
            helpButton: RS.Flow.Button.Settings;
            balanceMeter: Meter.Settings;
            winMeter: Meter.Settings;
            totalBetMeter: Meter.Settings;
            linesMeter: Meter.Settings;
            waysMeter: Meter.Settings;
            messageLabel: MessageLabel.Settings;
            gameNameLabel: RS.Flow.Label.Settings;
            meterSpacing: number;
            landscapeSize: RS.Dimensions;
            portraitSize: RS.Dimensions;
            mobileLandscapeSize: RS.Dimensions;
            mobilePortraitSize: RS.Dimensions;
            cornerSize: RS.Dimensions;

            ribbonColorA: RS.Util.Color;
            ribbonColorB: RS.Util.Color;

            /** Whether or not to show the payline count meter */
            showLinesMeter: boolean;
            /** Whether or not to show the ways meter (e.g. for all-ways pays) */
            showWaysMeter: boolean;
            /** Whether or not to show the game name above the bottom bar */
            showGameName: boolean;
            /** Whether or not to show blank space instead of 0.00 for the balance meter */
            balanceMeterBlank?: boolean;
            /** Whether or not to show blank space instead of 0.00 for the win meter */
            winMeterBlank?: boolean;

            balanceReplayText: RS.Localisation.LocalisableString;
            showMobileHelpButton?: boolean;
        }

        export interface RuntimeData
        {
            enableInteractionArbiter: RS.IReadonlyObservable<boolean>;
            messageTextArbiter: RS.IReadonlyObservable<RS.Localisation.LocalisableString>;

            showBackButton: boolean;
            showHelpButton: boolean;
        }

        const defaultButtonBackground: RS.Flow.Background.Settings =
        {
            kind: RS.Flow.Background.Kind.ImageFrame,
            dock: RS.Flow.Dock.Fill,
            ignoreParentSpacing: true,
            asset: Assets.CommonUI.BottomBar.Icons,
            frame: 0,
            sizeToContents: true,
            expand: RS.Flow.Expand.Disallowed
        };

        export let defaultSettings: Settings =
        {
            name: "Bottom Bar",
            background:
            {
                name: "Background",
                kind: RS.Flow.Background.Kind.SolidColor,
                color: new RS.Util.Color(0x1d1d1d),
                borderSize: 0,
                cornerRadius: 0
            },
            ribbonColorA: new RS.Util.Color(0xf04957),
            ribbonColorB: new RS.Util.Color(0x337dad),
            ribbonHorizontal:
            {
                name: "Ribbon H",
                kind: RS.Flow.Background.Kind.Gradient,
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                    [
                        { offset: 0.0, color: new RS.Util.Color(0xf04957) },
                        { offset: 1.0, color: new RS.Util.Color(0x337dad) }
                    ],
                    posA: { x: 0.5, y: 0 },
                    posB: { x: 1.5, y: 0 }
                },
                textureSize: { w: 2, h: 1 },
                dock: RS.Flow.Dock.Top,
                size: { w: 0, h: 4 },
                ignoreParentSpacing: true,
                sizeToContents: false
            },
            ribbonVertical:
            {
                name: "Ribbon V",
                kind: RS.Flow.Background.Kind.Gradient,
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                    [
                        { offset: 0.0, color: new RS.Util.Color(0xf04957) },
                        { offset: 1.0, color: new RS.Util.Color(0x337dad) }
                    ],
                    posA: { x: 0, y: 1.5 },
                    posB: { x: 0, y: 0.5 }
                },
                textureSize: { w: 1, h: 2 },
                dock: RS.Flow.Dock.Right,
                size: { w: 4, h: 0 },
                ignoreParentSpacing: true,
                sizeToContents: false
            },
            ribbonCornerAsset: Assets.CommonUI.BottomBar.Corner,
            logoAsset: Assets.CommonUI.Logo,
            clockLabel: ClockLabel.defaultSettings,
            landscapeSize: { w: 1400, h: 68 },
            portraitSize: { w: 1024, h: 136 },
            mobileLandscapeSize: { w: 1400, h: 328 },
            mobilePortraitSize: { w: 1024, h: 328 },
            expand: RS.Flow.Expand.HorizontalOnly,
            cornerSize: { w: 104, h: 0 },
            enforceMinimumSize: true,
            dock: RS.Flow.Dock.Bottom,
            contentAlignment: { x: 0.5, y: 1.0 },
            listSpacing: 10,
            cornerListSpacing: 12,
            verticalDivider:
            {
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                    [
                        { offset: 0.0, color: new RS.Util.Color(0x1d1d1d) },
                        { offset: 0.5, color: new RS.Util.Color(0xb7b7b7) },
                        { offset: 1.0, color: new RS.Util.Color(0x1d1d1d) }
                    ],
                    posA: { x: 0, y: 0.5 },
                    posB: { x: 0, y: 2.5 }
                },
                textureSize: { w: 1, h: 3 },
                size: { w: 3, h: 64 }
            },
            horizontalDivider:
            {
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                    [
                        { offset: 0.0, color: new RS.Util.Color(0x1d1d1d) },
                        { offset: 0.5, color: new RS.Util.Color(0xb7b7b7) },
                        { offset: 1.0, color: new RS.Util.Color(0x1d1d1d) }
                    ],
                    posA: { x: 0.5, y: 0 },
                    posB: { x: 2.5, y: 0 }
                },
                textureSize: { w: 3, h: 1 },
                size: { w: 100, h: 3 }
            },
            backButton:
            {
                name: "Back",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_back },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_back_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_back_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "BackButton"
            },
            settingsButton:
            {
                name: "Settings",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_settings },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_settings_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_settings_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "SettingsButton"
            },
            helpButton:
            {
                name: "Help",
                background: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_externalhelp },
                hoverbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_externalhelp_hover },
                pressbackground: null,
                disabledbackground: { ...defaultButtonBackground, frame: Assets.CommonUI.BottomBar.Icons.btn_externalhelp_dim },
                spacing: RS.Flow.Spacing.none,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                textLabel: null,
                seleniumId: "ExternalHelpButton"
            },
            balanceMeter:
            {
                ...Meter.defaultSettings,
                name: "Balance",
                titleLabel: { ...Meter.defaultSettings.titleLabel, text: Translations.UI.Balance },
                delimiterLabel: { ...Meter.defaultSettings.delimiterLabel, textColor: new RS.Util.Color(0xf04957), text: " : " },
                isCurrency: true,
                seleniumId: "BalanceText"
            },
            balanceReplayText: Translations.UI.Replay,
            winMeter:
            {
                ...Meter.defaultSettings,
                name: "Win",
                titleLabel: { ...Meter.defaultSettings.titleLabel, text: Translations.UI.Win },
                delimiterLabel: { ...Meter.defaultSettings.delimiterLabel, textColor: new RS.Util.Color(0xf04957), text: " : " },
                isCurrency: true,
                seleniumId: "WinText",
                blankValueIfZero: true
            },
            totalBetMeter:
            {
                ...Meter.defaultSettings,
                name: "Total Bet",
                titleLabel: { ...Meter.defaultSettings.titleLabel, text: Translations.UI.TotalBet },
                delimiterLabel: { ...Meter.defaultSettings.delimiterLabel, textColor: new RS.Util.Color(0x337dad), text: " : " },
                isCurrency: true,
                seleniumId: "TotalBetText"
            },
            linesMeter:
            {
                ...Meter.defaultSettings,
                name: "Lines",
                titleLabel: { ...Meter.defaultSettings.titleLabel, text: Translations.UI.Lines },
                delimiterLabel: { ...Meter.defaultSettings.delimiterLabel, textColor: new RS.Util.Color(0x337dad), text: " : " },
                isCurrency: false,
                seleniumId: "LinesText"
            },
            waysMeter:
            {
                ...Meter.defaultSettings,
                name: "Ways",
                titleLabel: { ...Meter.defaultSettings.titleLabel, text: Translations.UI.Ways },
                delimiterLabel: { ...Meter.defaultSettings.delimiterLabel, textColor: new RS.Util.Color(0x337dad), text: " : " },
                isCurrency: false,
                seleniumId: "WaysText"
            },
            meterSpacing: 10,
            messageLabel:
            {
                ...MessageLabel.defaultSettings,
                scrollDirection: RS.Flow.ScrollContainer.ScrollDirection.HorizontalOnly,
                dock: RS.Flow.Dock.Fill,
                expand: RS.Flow.Expand.Allowed,
                label:
                {
                    ...RS.Flow.Label.defaultSettings,
                    name: "Message",
                    text: "MESSAGE",
                    font: RS.Fonts.Frutiger.CondensedMedium,
                    fontSize: 28,
                    textColor: RS.Util.Colors.white,
                    sizeToContents: true,
                    expand: RS.Flow.Expand.Disallowed,
                    dock: RS.Flow.Dock.Fill,
                    seleniumId: "MessageText"
                },
                fadeTime: 250,
                interval: 500,
                speed: 0.25
            },
            gameNameLabel:
            {
                ...RS.Flow.Label.defaultSettings,
                name: "Game Name",
                text: "GAME NAME HERE",
                font: RS.Fonts.Frutiger.CondensedMedium,
                fontSize: 28,
                textColor: RS.Util.Colors.white,
                layers:
                [
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Border,
                        size: 1,

                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.black
                    },
                    {
                        layerType: RS.Rendering.TextOptions.LayerType.Fill,
                        fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                        color: RS.Util.Colors.white
                    }
                ],
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed,
                dock: RS.Flow.Dock.Float,
                floatPosition: { x: 0.5, y: 0.0 },
                dockAlignment: { x: 0.5, y: 1.0 },
                seleniumId: "TitleText"
            },
            showLinesMeter: false,
            showWaysMeter: false,
            showGameName: false,
            winMeterBlank: false
        };

        export let defaultRuntimeData: RuntimeData =
        {
            messageTextArbiter: null,
            enableInteractionArbiter: null,
            showBackButton: true,
            showHelpButton: true
        };
    }
}
