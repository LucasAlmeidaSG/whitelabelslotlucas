/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/Translations.ts" />

namespace SG.CommonUI
{
    export class ClickToContinue extends RS.Flow.BaseElement<ClickToContinue.Settings, ClickToContinue.RuntimeData>
    {
        // Disposable default elements. May be null if runtimeData elements are set
        @RS.AutoDisposeOnSet protected _defaultLabel: RS.Flow.Label;
        @RS.AutoDisposeOnSet protected _defaultLabelTween: RS.SingleTween<RS.Flow.Label<RS.Flow.Label.Settings>>;

        /**
         * Build a click to continue element
         * @param settings Settings for default label element
         * @param runtimeData Elements to use instead
         */
        public constructor(settings: ClickToContinue.Settings, public readonly runtimeData: ClickToContinue.RuntimeData)
        {
            super(settings, runtimeData);

            // Build elements in runtime data
            this.buildElements();

            // Add default label
            this.buildDefaultLabel();

            // Handle click to capture only if viewController is present
            this.handleClickCapture();
        }

        /**
         * Disposes this element.
         */
        public dispose(): void
        {
            if (this.isDisposed) { return; }

            // We may not have used this tween
            if (this._defaultLabelTween)
            {
                // Make sure to stop any looping
                this._defaultLabelTween.loop = false;
                this._defaultLabelTween.dispose();
            }

            super.dispose();
        }

        /**
         * Build Elements,add elements from runtimeData
         */
        protected buildElements()
        {
            // Elements from constructor
            const elementSettings = this.runtimeData.clickElements;

            if (!elementSettings)
            {
                return;
            }

            // Simply add each element here
            this.addChildren(elementSettings);
        }

        /**
         * Build Default Label.
         * Using defaultLabelSettings, Create
         * a label when runtimeData.clickElements is empty
         */
        protected buildDefaultLabel(): void
        {
            // If we want to use our own elements, don't build a defualt label
            if (this.runtimeData.clickElements && this.runtimeData.clickElements.length)
            {
                return;
            }

            // Create a label by default
            this._defaultLabel = RS.Flow.label.create(this.settings.labelSettings, this);

            // Get label animator
            const labelAnimator = this.runtimeData.labelAnimator;

            // Use label animator callback instead of default tween.
            if (labelAnimator != null)
            {
                // Run the label animation callback
                return labelAnimator(this._defaultLabel);
            }

            // Use a default tween instead of callback
            this.buildDefaultLabelTween();
        }

        /**
         * Build a tween animation for the default click label text.
         */
        protected buildDefaultLabelTween()
        {
            const tweenDuration = this.settings.labelTweenSettings.tweenDuration;
            const scaleToA = this.settings.labelTweenSettings.scaleToA;
            const scaleToB = this.settings.labelTweenSettings.scaleToB;
            const ease = this.settings.labelTweenSettings.ease;
            // Pulsating text
            this._defaultLabelTween = RS.Tween.get(this._defaultLabel,
                {
                    loop: true,
                    disposeBehaviour: RS.Tween.DisposeBehaviour.Skip
                })
                .to({ scaleX: scaleToA, scaleY: scaleToA }, tweenDuration, ease || RS.Ease.linear)
                .to({ scaleX: scaleToB, scaleY: scaleToB }, tweenDuration, ease || RS.Ease.linear);
        }

        /**
         * Handle click capture if a view controller is present
         */
        protected async handleClickCapture(): Promise<void>
        {
            // This can be null, in case a button or something was used
            if (!this.runtimeData.viewController)
            {
                return;
            }

            // Wait for clicked
            const clickCapturer = this.runtimeData.viewController.captureFullScreenClicks();
            await clickCapturer.onClicked;
            clickCapturer.dispose();

            // Set the event to allow state to continue
            if (this.runtimeData.clickToContinueEvent)
            {
                this.runtimeData.clickToContinueEvent.publish();
            }
            else
            {
                RS.Log.warn("Click captured, but click to continue event not present");
            }
        }
    }

    /**
     * Settings and RuntimeData
     */
    export namespace ClickToContinue
    {
        /**
         * RuntimeData Settings ClickToContinue
         */
        export interface RuntimeData extends RS.Flow.BaseRuntimeData
        {
            /**  The view controller to use for click capture */
            viewController?: RS.View.IController;
            /**
             * The event that handles click to continue in loading state
             * Must be set, but only used if viewController is present
             */
            clickToContinueEvent?: RS.ISimplePrivateEvent;
            /** Tween animator callback which can be applied to the default label */
            labelAnimator?: (label: RS.Flow.Label) => void;
            /** Allow the setting of any elements desired */
            clickElements?: RS.Flow.BaseElement[];
        }

        /**
         * Default label tween settings to apply
         */
        export interface LabelTweenSettings
        {
            /** Duration bwteen tween scaleToA - scaleToB */
            tweenDuration: number;
            /** Scale to scale up first */
            scaleToA: number;
            /** Scale to scale up next */
            scaleToB: number;
            /** Tween ease to use */
            ease?: RS.EaseFunction
        }

        /**
         * Settings ClickToContinue
         */
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            labelTweenSettings: LabelTweenSettings;
            /** Default label settings */
            labelSettings: RS.Flow.Label.Settings;
        }

        /**
         * Settings for default label.
         */
        export const defaultLabelTweenSettings: LabelTweenSettings =
        {
            /** Duration bwteen tween scaleToA - scaleToB */
            tweenDuration: 500,
            /** Scale to scale up first */
            scaleToA: 1.1,
            /** Scale to scale up next */
            scaleToB: 1.0,
            /** Tween ease to use */
            ease: RS.Ease.linear
        };

        /**
         * Settings for default label.
         */
        const defaultLabelSettings: RS.Flow.Label.Settings =
        {
            font: RS.Fonts.FrutigerWorld.Bold,
            fontSize: 38,
            sizeToContents: true,
            canWrap: false,
            align: RS.Rendering.TextOptions.Align.Middle,
            dockAlignment: { x: 0.5, y: 0.5 },
            floatPosition: { x: 0.5, y: 0.5 },
            dock: RS.Flow.Dock.Float,
            layers:
            [
                {
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    color: RS.Util.Colors.black,
                    layerType: RS.Rendering.TextOptions.LayerType.Border,
                    offset: { x: 0.00, y: 0.00 },
                    size: 1.5
                },
                {
                    layerType: RS.Rendering.TextOptions.LayerType.Fill,
                    fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                    offset: { x: 0.00, y: 0.00 },
                    color: RS.Util.Colors.lightblue
                }
            ],
            text: RS.Device.isDesktopDevice ? RS.Translations.Loading.ClickToContinue : RS.Translations.Loading.TapToContinue
        };

        /**
         * Default Settings
         */
        export let defaultSettings: Settings =
        {
            name: "ClickToContinue",
            size: { w: 0, h: 150 },
            // Default label tween settings
            labelTweenSettings: defaultLabelTweenSettings,
            // Default label
            labelSettings: defaultLabelSettings,
            sizeToContents: false,
            dock: RS.Flow.Dock.Bottom
        };
    }

    export const clickToContinue = RS.Flow.declareElement(ClickToContinue, true, ClickToContinue.defaultSettings);
}