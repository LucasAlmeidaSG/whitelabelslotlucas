/// <reference path="../generated/Assets.ts" />
/// <reference path="../generated/Translations.ts" />

/// <reference path="ClockLabel.ts" />
/// <reference path="Meter.ts" />

namespace SG.CommonUI
{
    export class LoadingBar extends RS.Flow.BaseElement<LoadingBar.Settings> implements SGI.Selenium.ISeleniumDescriber
    {
        @RS.AutoDispose protected _background: RS.Flow.Background;
        @RS.AutoDispose protected _ribbon: RS.Flow.Background;

        @RS.AutoDispose protected _list: RS.Flow.List;

        @RS.AutoDispose protected _leftLogo: RS.Flow.Image;
        @RS.AutoDispose protected _barContainer: RS.Flow.Container;
        @RS.AutoDispose protected _rightLogo: RS.Flow.Image;

        @RS.AutoDispose protected _barBorder: RS.Flow.Background;
        @RS.AutoDispose protected _bar: RS.Flow.Background;

        protected _barProgress: number;
        protected _barMask: RS.Rendering.IGraphics;

        /** Gets or sets the loading progress (0-1). */
        public get actualProgress() { return this._barProgress; }
        public set actualProgress(value)
        {
            this._barProgress = value;
            this.invalidateLayout({ reason: "actualProgress" });
        }

        /** Gets or sets the loading progress (0-1). */
        public get progress() { return this._barProgress; }
        public set progress(value)
        {
            RS.Tween.get<LoadingBar>(this, { override: true })
                .to({ actualProgress: value }, 500, RS.Ease.quadInOut);
        }
        /**
         * Gets or Sets the visibility of bar border and bar elements
         * @param visibility boolean visibility of bar border and bar
         */
        public set progressBarvisible(visibility: boolean)
        {
            this._barBorder.visible = visibility;
            this._bar.visible = visibility;
        }
        public get progressBarvisible(): boolean
        {
            return (this._barBorder.visible && this._bar.visible)
        }

        public get seleniumId() { return this.settings.seleniumId || null; }

        public constructor(settings: LoadingBar.Settings)
        {
            super(settings, undefined);

            this._background = RS.Flow.background.create(settings.background, this);
            this._ribbon = RS.Flow.background.create(settings.ribbon, this._background);

            this._list = RS.Flow.list.create(
            {
                dock: RS.Flow.Dock.Fill,
                direction: RS.Flow.List.Direction.LeftToRight,
                expand: RS.Flow.Expand.Allowed,
                evenlySpaceItems: true,
                sizeToContents: false,
                ...settings.list
            }, this);

            this._leftLogo = RS.Flow.image.create(settings.leftLogo, this._list);
            this._barContainer = RS.Flow.container.create(settings.barContainer, this._list);
            this._rightLogo = RS.Flow.image.create(settings.rightLogo, this._list);

            this._barMask = new RS.Rendering.Graphics();

            this._bar = RS.Flow.background.create(settings.bar, this._barContainer);
            this._bar.addChild(this._barMask);
            this._bar.mask = this._barMask;
            this._barBorder = RS.Flow.background.create(settings.barBorder, this._barContainer);

            this.progress = 0.0;
        }

        /**
         * Disposes this element.
         */
        public dispose(): void
        {
            if (this.isDisposed) { return; }
            super.dispose();
        }

        public seleniumDescribe(): [string, string | number][]
        {
            return [[ "progress", (this._barProgress * 100).toFixed(0) ]];
        }

        protected performLayout(): void
        {
            // note: we set the _layouting flag here so that changing the size on the barContainer doesn't invalidate the layout and cause an infinite loop
            this._layouting = true;
            if (this._barContainer) { this._barContainer.size = { w: (this._layoutSize.w * this.settings.barWidth) | 0, h: this.settings.barContainer.size.h }; }
            this._layouting = false;
            super.performLayout();
        }

        protected onLayout()
        {
            if (!this._barMask) { return; }
            this._barMask.clear();
            this._barMask.drawRect(0, 0, (this._barContainer.layoutSize.w * this._barProgress) | 0, this._barContainer.layoutSize.h);
        }
    }

    export namespace LoadingBar
    {
        export interface Settings extends Partial<RS.Flow.ElementProperties>
        {
            seleniumId?: string;

            background: RS.Flow.Background.Settings;
            ribbon: RS.Flow.Background.Settings;

            list?: RS.Flow.List.Settings;

            leftLogo: RS.Flow.Image.Settings;
            rightLogo: RS.Flow.Image.Settings;

            barContainer: RS.Flow.Container.Settings;
            bar: RS.Flow.Background.Settings;
            barBorder: RS.Flow.Background.Settings;

            barWidth: number;
        }

        export let defaultSettings: Settings =
        {
            name: "Loading Bar",
            seleniumId: "progress_bar",
            background:
            {
                name: "Background",
                kind: RS.Flow.Background.Kind.SolidColor,
                color: new RS.Util.Color(0x1d1d1d),
                borderSize: 0,
                cornerRadius: 0
            },
            ribbon:
            {
                name: "Ribbon",
                kind: RS.Flow.Background.Kind.Gradient,
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                        [
                            { offset: 0.0, color: new RS.Util.Color(0xf04957) },
                            { offset: 1.0, color: new RS.Util.Color(0x337dad) }
                        ],
                    posA: { x: 0.5, y: 0 },
                    posB: { x: 1.5, y: 0 }
                },
                textureSize: { w: 2, h: 1 },
                dock: RS.Flow.Dock.Top,
                size: { w: 0, h: 4 },
                expand: RS.Flow.Expand.HorizontalOnly,
                sizeToContents: false
            },
            list:
            {
                name: "List",
                dock: RS.Flow.Dock.Fill,
                direction: RS.Flow.List.Direction.LeftToRight,
                expand: RS.Flow.Expand.Allowed,
                evenlySpaceItems: true,
                sizeToContents: false
            },
            leftLogo:
            {
                name: "SGD Logo",
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.Logos,
                animationName: Assets.CommonUI.Logos.sgd,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed
            },
            rightLogo:
            {
                name: "Red7 Logo",
                kind: RS.Flow.Image.Kind.SpriteFrame,
                asset: Assets.CommonUI.Logos,
                animationName: Assets.CommonUI.Logos.red,
                sizeToContents: true,
                expand: RS.Flow.Expand.Disallowed
            },
            barContainer:
            {
                name: "Bar Container",
                size: { w: 0, h: 24 },
                expand: RS.Flow.Expand.HorizontalOnly,
                sizeToContents: false
            },
            bar:
            {
                name: "Bar",
                kind: RS.Flow.Background.Kind.Gradient,
                gradient:
                {
                    type: RS.Rendering.Gradient.Type.Linear,
                    stops:
                        [
                            { offset: 0.0, color: new RS.Util.Color(0xf04957) },
                            { offset: 1.0, color: new RS.Util.Color(0x337dad) }
                        ],
                    posA: { x: 0.5, y: 0 },
                    posB: { x: 1.5, y: 0 }
                },
                textureSize: { w: 2, h: 1 },
                dock: RS.Flow.Dock.Fill
            },
            barBorder:
            {
                name: "Bar Border",
                kind: RS.Flow.Background.Kind.NinePatch,
                asset: Assets.CommonUI.ProgressBarBorder,
                borderSize: RS.Flow.Spacing.all(8),
                dock: RS.Flow.Dock.Fill,
                sizeToContents: false
            },
            size: { w: 0, h: 150 },
            barWidth: 0.25,
            spacing: RS.Flow.Spacing.all(20),
            dock: RS.Flow.Dock.Bottom
        };
    }

    export const loadingBar = RS.Flow.declareElement(LoadingBar, LoadingBar.defaultSettings);
}