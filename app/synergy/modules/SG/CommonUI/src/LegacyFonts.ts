namespace SG.CommonUI.Assets
{
    interface IMyriad extends RS.Asset.FontReference
    {
        /** @deprecated Use RS.Rendering.Assets.Fonts.MyriadPro.Black instead. */
        Black: RS.Asset.FontReference;
        Light:
        {
            /** @deprecated Use RS.Rendering.Assets.Fonts.MyriadPro.LightCond instead. */
            Condensed: RS.Asset.FontReference;
        };
    }

    export namespace Groups
    {
        /** @deprecated Use RS.Rendering.Assets.Groups.fonts instead. */
        export const fonts = RS.Rendering.Assets.Groups.fonts;
    }

    /** @deprecated Use RS.Rendering.Assets.Fonts.MyriadPro.SemiboldCond instead. */
    export const Myriad: IMyriad =
    {
        ...RS.Rendering.Assets.Fonts.MyriadPro.SemiboldCond,
        Black: RS.Rendering.Assets.Fonts.MyriadPro.Black,
        Light: { Condensed: RS.Rendering.Assets.Fonts.MyriadPro.LightCond }
    };
}