namespace SG.CommonUI.Views
{
    @RS.View.RequiresAssets([SG.CommonUI.Assets.Groups.commonUIload, SG.CommonUI.Assets.Groups.fonts])
    @RS.View.ScreenName("Loading")
    @RS.HasCallbacks
    export class Loading extends RS.View.Base<SG.CommonUI.Views.Loading.Settings, RS.Game.Context> implements RS.GameState.LoadingState.IClickToContinueView
    {
        @RS.AutoDisposeOnSet protected _background: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _loadingBar: LoadingBar;
        @RS.AutoDisposeOnSet protected _clickToContinue: ClickToContinue;
        @RS.AutoDisposeOnSet protected _clickToContinueContainer: RS.Flow.Container;
        @RS.AutoDisposeOnSet protected _loadingBarContainer: RS.Flow.Container;

        @RS.AutoDisposeOnSet protected _onLoadOpChangedHandle: RS.IDisposable;
        @RS.AutoDisposeOnSet protected _onLoadProgressChangedHandle: RS.IDisposable;

        @RS.AutoDisposeOnSet protected _newLoadOp: RS.Scheduler.IOperation;

        @RS.AutoDispose protected _clickToContinueEvent: RS.ISimplePrivateEvent;

        public get onClickToContinue() { return this._clickToContinueEvent.public; }

        public async onOpened()
        {
            this.locale = this.context.game.locale;

            super.onOpened();

            this._onLoadOpChangedHandle = this.context.loadOp.onChanged(this.handleLoadOpChanged);

            this.buildLayout(RS.Orientation.currentOrientation);
            this.handleOrientationChanged(RS.Orientation.currentOrientation);
            this._clickToContinueEvent = RS.createSimpleEvent();
        }

        @RS.Callback
        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation): void
        {
            super.handleOrientationChanged(newOrientation);
            this.applyLayout(newOrientation);
        }

        /** Called once by onOpened to build the layouts in the current orientation of the device */
        protected buildLayout(newOrientation: RS.DeviceOrientation)
        {
            const settings = newOrientation === RS.Orientation.Landscape ? this.settings.landscape : this.settings.portrait;
            this.buildBackground(settings);
            this.buildLoadingBar(settings);
        }

        /** Changes the settings applied to the assets in the loading screen */
        protected applyLayout(newOrientation: RS.DeviceOrientation)
        {
            const settings = newOrientation === RS.Orientation.Landscape ? this.settings.landscape : this.settings.portrait;

            this._background.apply(settings.background);
            this._loadingBar.apply(settings.sgLoadingBar);

            if (this._clickToContinue)
            {
                this._clickToContinueContainer.apply(settings.clickToContinueContainer);
                this._clickToContinue.apply(settings.clickToContinue);
            }
        }

        protected buildBackground(settings: Loading.OrientationSettings)
        {
            // Create background
            this._background = RS.Flow.background.create(settings.background, this);
        }

        protected buildLoadingBar(settings: Loading.OrientationSettings)
        {
            if (this._loadingBar != null)
            {
                this._loadingBar = null;
            }

            this._loadingBarContainer = RS.Flow.container.create(settings.loadingBarContainer, this);

            this._loadingBar = loadingBar.create(settings.sgLoadingBar, this._loadingBarContainer);
        }

        @RS.Callback
        protected handleLoadOpChanged(newLoadOp: RS.Scheduler.IOperation | null)
        {
            if (newLoadOp == null)
            {
                return;
            }
            this.context.loadOp.value.complete.for(true).then(this.handleLoadComplete);

            this._newLoadOp = newLoadOp;
            this._onLoadProgressChangedHandle = this._newLoadOp.progress.onChanged(this.handleLoadProgressChanged);

        }

        @RS.Callback
        protected handleLoadProgressChanged(newProgress: number)
        {
            if (!this._loadingBar || this._loadingBar.isDisposed)
            {
                return;
            }

            this._loadingBar.progress = newProgress;
        }

        protected createClickToContinueElement()
        {
            // Create click to continue label
            const currentOrientation = RS.Orientation.currentOrientation;
            const settings = currentOrientation === RS.Orientation.Landscape ? this.settings.landscape : this.settings.portrait;

            this._clickToContinueContainer = RS.Flow.container.create(settings.clickToContinueContainer, this);

            this._clickToContinue = clickToContinue.create(settings.clickToContinue, {
                clickToContinueEvent: this._clickToContinueEvent,
                viewController: this.context.game.viewController
            }, this._clickToContinueContainer);
        }

        @RS.Callback
        protected handleLoadComplete()
        {
            // Hide loading bar
            this._loadingBar.progressBarvisible = false;

            // Create element just for click to continue
            this.createClickToContinueElement();
        }
    }

    export namespace Loading
    {
        export interface OrientationSettings
        {
            sgLoadingBar: LoadingBar.Settings;
            background: RS.Flow.Background.Settings;
            clickToContinue: ClickToContinue.Settings;
            clickToContinueContainer: RS.Flow.Container.Settings;
            loadingBarContainer: RS.Flow.Container.Settings;
        }

        export interface Settings extends RS.Views.Loading.Settings
        {
            landscape: OrientationSettings;
            portrait: OrientationSettings;
        }

        const defaultBackgroundSettings: RS.Flow.Background.Settings =
        {
            dock: RS.Flow.Dock.Fill,
            kind: RS.Flow.Background.Kind.SolidColor,
            color: RS.Util.Colors.gray,
            borderSize: 0,
            cornerRadius: 0
        };

        const defaultClickToContinueContainerSettings: RS.Flow.Container.Settings =
        {
            dock: RS.Flow.Dock.Fill
        };

        const defaultLoadingBarContainerSettings: RS.Flow.Container.Settings =
        {
            dock: RS.Flow.Dock.Fill
        };

        export const defaultSettings: Settings =
        {
            landscape:
            {
                background: defaultBackgroundSettings,
                sgLoadingBar: LoadingBar.defaultSettings,
                clickToContinue: ClickToContinue.defaultSettings,
                clickToContinueContainer: defaultClickToContinueContainerSettings,
                loadingBarContainer: defaultLoadingBarContainerSettings
            },
            portrait:
            {
                background: defaultBackgroundSettings,
                sgLoadingBar: LoadingBar.defaultSettings,
                clickToContinue: ClickToContinue.defaultSettings,
                clickToContinueContainer: defaultClickToContinueContainerSettings,
                loadingBarContainer: defaultLoadingBarContainerSettings
            }
        };
    }
}
