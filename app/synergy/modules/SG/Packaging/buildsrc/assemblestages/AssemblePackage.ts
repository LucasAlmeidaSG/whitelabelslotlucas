/// <reference path="../EnvArgs.ts" />
/// <reference path="CreatePackageFiles.ts" />

namespace RS.AssembleStages
{
    import Path = RS.Path;

    /* tslint:disable:no-string-literal */

    /**
     * Responsible for outputting the zipped package.
     */
    export class AssemblePackage extends RS.Packaging.AssembleStages.AssemblePackage
    {
        protected get moduleConfig() { return RS.Config.activeConfig.moduleConfigs["SG.Packaging"] || {}; }
        protected get packageName() { return `${this.getRequiredConfigProperty("gameCode")}.${EnvArgs.version.value}`; }

        protected get tagWithGameCode(): boolean { return RS.as(this.moduleConfig["tagWithGameCode"], RS.Is.boolean, false); }

        protected get tag() 
        { 
            const gameCode = this.tagWithGameCode ? `${this.getRequiredConfigProperty("gameCode")}-` : "";
            return `sg-${gameCode}${EnvArgs.version.value}`; 
        }
        protected get tagMessage() { return `Package ${EnvArgs.version.value} assembled for SG`; }

        constructor()
        {
            super();

            this.includeSourceMaps = EnvArgs.withSourceMaps.value;
        }

        protected async addPackageFilesToZip(zip: JSZip, settings: RS.AssembleStage.AssembleSettings)
        {
            const scriptFiles = ["thirdparty", "app"];
            for (const file of scriptFiles)
            {
                const baseName = Path.replaceExtension(file, ".js");
                await this.addScriptFileToZip(zip, Path.combine(settings.outputDir, "js", baseName), Path.combine("js", baseName));
            }
            await this.addScriptFileToZip(zip, Path.combine(settings.outputDir, "js", "bootstrapper.js"), Path.combine("js", "metadatabundle.js"));

            const directories = ["assets", "css", "translations", "help"];
            for (const directory of directories)
            {
                await this.addDirectoryToZip(zip, Path.combine(settings.outputDir, directory), directory);
            }

            const files = ["manifest.json", "server.json"];
            for (const file of files)
            {
                await this.addFileToZip(zip, Path.combine(settings.outputDir, file), file);
            }
        }

        protected getRequiredConfigProperty(name: string): any
        {
            if (!(name in this.moduleConfig))
            {
                throw new Error(`Module config is missing '${name}' property`);
            }

            return this.moduleConfig[name];
        }
    }

    export const assemblePackage = new AssemblePackage();
    assemblePackage.includeSourceMaps = EnvArgs.withSourceMaps.value;
    RS.AssembleStage.register({ after: [ assembleBootstrapper, createPackageFiles, code ], last: true }, assemblePackage);
}