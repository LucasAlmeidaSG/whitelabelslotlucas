/// <reference path="../EnvArgs.ts" />

namespace RS.AssembleStages
{
    const validOrientations: Util.Map<boolean> =
    {
        "LANDSCAPE": true,
        "PORTRAIT": true,
        "BOTH": true,
        "PORTRAIT_MOBILE": true
    };

    /**
     * Responsible for outputting the required SG package JSON files.
     */
    export class CreatePackageFiles extends AssembleStage.Base
    {
        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            await FileSystem.createPath(settings.outputDir);

            // Fetch and validate config
            const moduleConfig = Config.activeConfig.moduleConfigs["SG.Packaging"];
            if (!moduleConfig)
            {
                Log.error("No module config present, package will be incomplete", "SG.Packaging");
                return;
            }
            const gameCode = (moduleConfig as any).gameCode;
            if (!gameCode)
            {
                Log.error("Module config is missing 'gameCode' property", "SG.Packaging");
                return;
            }
            const displayName = (moduleConfig as any).displayName;
            if (!gameCode)
            {
                Log.error("Module config is missing 'displayName' property", "SG.Packaging");
                return;
            }
            const orientation = (moduleConfig as any).orientation;
            if (!orientation)
            {
                Log.error("Module config is missing 'orientation' property", "SG.Packaging");
                return;
            }
            if (!validOrientations[orientation])
            {
                Log.error(`Module config 'orientation' property is invalid (expected one of '${Object.keys(validOrientations).join(", ")}')`, "SG.Packaging");
                return;
            }

            // Emit server.json
            const serverObj =
            {
                gameCode,
                displayName,
                version: EnvArgs.version.value,
                level: "LIVE",
                gameConfig:
                {
                    kind: "GameConfig",
                    code: gameCode,
                    platform: "GLS",
                    isLean: true
                }
            };
            await FileSystem.writeFile(Path.combine(settings.outputDir, "server.json"), JSON.stringify(serverObj, null, 4));

            // Emit manifest.json
            const manifestObj =
            {
                orientation,
                default_locale: "${defaultlocale}",
                inject:
                [
                    {
                        link: { rel: "stylesheet", href: `content/${gameCode}/css/app.css` }
                    }
                ],
                javascripts:
                [
                    // `content/${gameCode}/app/js/bootstrapper.js\${metadatajs_params}`
                    /*
                     * "There is a case-insensitive pattern /MetaDataBundle.js match that decides to add cache_burst to URL which is done to resolve caching related issues with AutoPlay configuration."
                     * - Nitin
                     */
                    `content/${gameCode}/js/metadatabundle.js\${metadatajs_params}`
                ],
                publish_events: { platform: "1" },
                main_class: "SG.EntryPoint",
                supported_locales: [ "en" ]
            };
            await FileSystem.writeFile(Path.combine(settings.outputDir, "manifest.json"), JSON.stringify(manifestObj, null, 4));
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // No per-module work to do here
        }
    }

    export const createPackageFiles = new CreatePackageFiles();
    AssembleStage.register({ after: [ assembleBootstrapper ], last: true }, createPackageFiles);
}