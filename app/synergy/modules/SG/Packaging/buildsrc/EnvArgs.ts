namespace RS.EnvArgs
{
    export const version = new EnvArg("VERSION", "dev");
    export const withSourceMaps = new BooleanEnvArg("WITHSOURCEMAPS", false);
}