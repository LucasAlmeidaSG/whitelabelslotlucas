namespace RS.AssembleStages
{

    /**
     * Responsible for outputting the required SG package JSON files.
     */
    export class CreateUberDirectory extends AssembleStage.Base
    {
        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {

            //console.log("Creating uber directory");
            //await FileSystem.createPath(Path.combine(settings.outputDir, "games"));
            const moduleConfig = Config.activeConfig.moduleConfigs["SG.Packaging"];
            if (!moduleConfig)
            {
                Log.error("No module config present, package will be incomplete", "SG.Packaging");
                return;
            }
            const gameCode = (moduleConfig as any).gameCode;
            if (!gameCode)
            {
                Log.error("Module config is missing 'gameCode' property", "SG.Packaging");
                return;
            }
            const root: string = Path.combine(process.cwd(), "games");//Path.combine(settings.outputDir, "uber", "games");
            await FileSystem.deletePath(root);
            await FileSystem.createPath(Path.combine(root, gameCode, EnvArgs.version.value));
            const serverObj =
                [
                    {
                        gameCode,
                        version: EnvArgs.version.value
                    }
                ];
            await FileSystem.writeFile(Path.combine(root, "catalog.json"), JSON.stringify(serverObj, null, 4));
            await this.recusiveCopy(settings.outputDir, Path.combine(root, gameCode, EnvArgs.version.value));
        }

        protected async recusiveCopy(input: string, output: string)
        {
            await FileSystem.createPath(output);
            const files = await FileSystem.readDirectory(input, true);
            for (let i: number = 0; i < files.length; i++)
            {
                if (files[i].type == "file")
                {
                    //console.log("copy", Path.combine(input, files[i].name), "to", Path.combine(output, files[i].name))
                    await FileSystem.copyFile(Path.combine(input, files[i].name), Path.combine(output, files[i].name));
                }
                else
                {
                    await this.recusiveCopy(Path.combine(input, files[i].name), Path.combine(output, files[i].name));
                }
            }
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // No per-module work to do here
        }
    }

    export const createUberDirectory = new CreateUberDirectory();
    AssembleStage.register({ after: [createPackageFiles], last: true }, createUberDirectory);
}