namespace RS.AssembleStages
{
    @DeploymentAgent("ssh")
    export class SSHDeployer implements IDeploymentAgent
    {
        public constructor(protected readonly settings: SSHDeployer.Settings) {}

        public connect() { /* */ }

        public async deploy(packagePath: string, gameCode: string, version: string)
        {
            const target: SSH.TargetInfo = { user: this.settings.user, host: this.settings.host };
            const destPath = this.resolvePath(this.settings.path, gameCode, version, packagePath);

            const sshOpts: SSH.Options =
            {
                logOutput: false,
                batchMode: true
            };

            if (this.settings.createPath)
            {
                const dir = Path.directoryName(destPath);
                Log.info(`Preparing staging area at ${dir}...`);
                await SSH.run(target, `mkdir -p ${dir};`, [], sshOpts);
            }

            const timer = new Timer();
            Log.info(`Uploading to ${target.user}@${target.host}:${destPath}...`);
            await SSH.upload(target, packagePath, destPath, sshOpts);
            Log.info(`Uploaded in ${timer}`);

            if (this.settings.unzip)
            {
                Log.info(`Unzipping package...`);
                await SSH.run(target,
                    [
                        `unzip ${destPath};`,
                        `rm ${destPath};`
                    ].join(""), [], sshOpts);
            }
        }

        public disconnect() { /* */ }

        protected resolvePath(template: string, gameCode: string, version: string, packagePath: string): string
        {
            const interpolatedPath = template.replace("${gamecode}", gameCode).replace("${version}", version);
            if (Path.extension(interpolatedPath))
            {
                return Path.replaceExtension(interpolatedPath, Path.extension(packagePath));
            }
            else if (interpolatedPath[interpolatedPath.length - 1] === "/")
            {
                return `${interpolatedPath}${packagePath}`;
            }
            else
            {
                return `${interpolatedPath}/${packagePath}`;
            }
        }
    }

    export namespace SSHDeployer
    {
        export interface Settings
        {
            host: string;
            user: string;
            path: string;

            createPath?: boolean;
            unzip?: boolean;
        }
    }
}