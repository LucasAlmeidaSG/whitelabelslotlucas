namespace RS.AssembleStages
{
    const fs = require("fs");
    function chmod(path: string, mode: number): PromiseLike<void>
    {
        return new Promise((resolve, reject) =>
        {
            fs.chmod(path, mode, (err) =>
            {
                if (err)
                {
                    reject(err);
                }
                else
                {
                    resolve();
                }
            });
        });
    }

    const defaultSettings: Red7Deployer.Settings =
    {
        user: "staging",
        group: "staging",
        host: "34.250.102.252",
        keyName: "remoteuber_rsa",
        stagingDir: "/home/staging/packages/"
    };

    @DeploymentAgent("red7")
    export class Red7Deployer implements IDeploymentAgent
    {
        protected readonly settings: Red7Deployer.Settings;

        private readonly _srcKeyPath: string;
        private readonly _tmpKeyPath: string;
        private readonly _cryptoPath: string;

        constructor(settings: Red7Deployer.Settings)
        {
            this.settings = { ...defaultSettings, ...settings };
            const keysPath = Path.combine(Module.getModule("SG.Deployment").path, "keys");
            this._srcKeyPath = Path.combine(keysPath, this.settings.keyName);
            this._tmpKeyPath = Path.combine(keysPath, "tmp_rsa");
            this._cryptoPath = Path.combine(keysPath, "crypto.json");
        }

        public async connect()
        {
            await this.decryptKey(this._srcKeyPath, this._tmpKeyPath);
        }

        public async deploy(packagePath: string, gameCode: string)
        {
            const sshOpts: SSH.Options =
            {
                strictHostKeyChecking: false,
                logOutput: false,
                keyPath: this._tmpKeyPath,
                batchMode: true
            };

            const target: SSH.TargetInfo = { user: this.settings.user, host: this.settings.host };
            const { stagingDir, group } = this.settings;

            const zipName = Path.baseName(packagePath);
            const destPath = `${stagingDir}${gameCode}/${zipName}`;

            Log.info(`Preparing staging area for ${gameCode}...`);
            await SSH.run(target,
                [
                    `cd ${stagingDir};`,
                    `if [ ! -d ${gameCode} ];`,
                        `then mkdir ${gameCode};`,
                        `chown ${target.user} ./${gameCode};`,
                        `chgrp ${group} ./${gameCode};`,
                    `fi;`
                ].join(""), [], sshOpts);

            const timer = new Timer();
            Log.info(`Uploading to ${target.host}...`);
            await RSync.sync(target, packagePath, destPath, sshOpts);
            Log.info(`Complete in ${timer}`);
        }

        public async disconnect()
        {
            await FileSystem.deletePath(this._tmpKeyPath);
        }

        protected async decryptKey(srcPath: string, destPath: string): Promise<void>
        {
            // Decrypt key temporarily
            if (!await FileSystem.fileExists(srcPath))
            {
                throw new Error(`Key not found at '${srcPath}'`);
            }

            await FileSystem.copyFile(srcPath, destPath);

            try
            {
                const passphrase = await this.findPassphrase();
                await chmod(destPath, 0o600);
                await Command.run("ssh-keygen", [
                    "-p",
                    "-P", passphrase,
                    "-N", "",
                    "-f", destPath
                ], null, false);
            }
            catch (err)
            {
                await FileSystem.deletePath(destPath);
                throw err;
            }
        }

        protected async findPassphrase(): Promise<string>
        {
            const cryptoData: Util.Map<CryptoData> = await JSON.parseFile(this._cryptoPath);
            const cryptoEntry = cryptoData[this.settings.keyName];
            return RS.Crypto.decrypt(cryptoEntry.passphrase, cryptoEntry.cipher, cryptoEntry.iv);
        }
    }

    export namespace Red7Deployer
    {
        export interface Settings
        {
            user: string;
            host: string;
            group: string;
            keyName: string;
            stagingDir: string;
        }
    }

    interface CryptoData
    {
        passphrase: string;
        cipher: string;
        iv: string;
    }
}