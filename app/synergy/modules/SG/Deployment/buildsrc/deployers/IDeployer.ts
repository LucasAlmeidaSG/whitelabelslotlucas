namespace RS.AssembleStages
{
    interface Constructor extends Function
    {
        prototype: IDeploymentAgent;
        new(settings: DeploymentSettings): IDeploymentAgent;
    }

    export interface IDeploymentAgent
    {
        connect(): PromiseLike<void> | void;
        deploy(packagePath: string, gameCode: string, version: string): PromiseLike<void>;
        disconnect(): PromiseLike<void> | void;
    }

    const deploymentAgents: Util.Map<Constructor> = {};
    export function DeploymentAgent(name: string): ClassDecorator
    {
        return function(target: any)
        {
            deploymentAgents[name] = target as Constructor;
        };
    }

    export interface DeploymentSettings
    {
        strategy: string;
    }

    /**
     * Creates a deployment agent for the specified name.
     * Throws if agent implementation not found.
     * @param name
     */
    export function create(settings: DeploymentSettings): IDeploymentAgent
    {
        const name = settings.strategy;
        if (!(name in deploymentAgents)) { throw new Error(`Deployment strategy '${name}' not implemented`); }
        return new deploymentAgents[name](settings);
    }
}