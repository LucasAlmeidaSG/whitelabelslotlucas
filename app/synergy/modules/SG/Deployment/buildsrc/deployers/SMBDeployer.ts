namespace RS.AssembleStages
{
    const unzipper = require("unzipper");
    const fs = require("fs");

    @DeploymentAgent("SMB")
    export class SMBDeployer implements IDeploymentAgent
    {
        private settings: SMBDeployer.Settings;

        constructor (deploymentSettings: SMBDeployer.Settings)
        {
            this.settings = deploymentSettings;
        }

        public connect(): void | PromiseLike<void>
        {
            /* */
        }

        public async deploy(packagePath: string, gameCode: string, version: string): Promise<void>
        {
            Log.info(`Uploading package ${packagePath}.`);

            let branchName = (await this.getBranchName()).replace("\n", "");

            const targetPath = `${this.settings.rootPath}\\${gameCode}\\${branchName}`;

            Log.info(`Creating target directory on root ${this.settings.rootPath}.`);
            fs.mkdir(targetPath, { recursive: true }, () => { Log.info(`Target directory created.`); });

            fs.createReadStream(packagePath)
                .pipe(unzipper.Extract({ path: targetPath }));

            return Promise.resolve();
        }

        public disconnect(): void | PromiseLike<void>
        {
            /* */
        }

        private async getBranchName(): Promise<string>
        {
            return Command.run("git",
                [
                    "branch",
                    "--show-current"
                ]);
        }
    }

    export namespace SMBDeployer
    {
        export interface Settings
        {
            rootPath: string;
        }
    }
}