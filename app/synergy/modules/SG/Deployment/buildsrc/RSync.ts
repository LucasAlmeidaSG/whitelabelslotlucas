/** Contains utils for synchronisation-based deployment strategy. */
namespace RS.AssembleStages.RSync
{
    const debugMode = new RS.BooleanEnvArg("DEBUG", false);

    function readVersion(path: string): Version | null
    {
        const versionStr = path.match(/[0-9]+\.[0-9]+\.[0-9]+/);
        if (!versionStr) { return null; }
        return Version.fromString(versionStr[0]);
    }

    function readPackageName(path: string): string | null
    {
        const match = path.match(/([^.\/\\]+)(?:\..*).zip/);
        if (!match) { return null; }
        return match[1] || null;
    }

    function normalisePathSeparators(path: string): string
    {
        if (Path.seperator === "\/") { return path; }
        return Util.replaceAll(path, Path.seperator, "\/");
    }

    /** Returns a list of files from the remote directory specified by path, relative to path. */
    export async function readDirectory(target: SSH.TargetInfo, path: string, opts?: SSH.Options): Promise<string[]>
    {
        path = normalisePathSeparators(path);

        const ls = await SSH.run(target, "ls", ["-1", path], opts);
        return ls.split("\n").filter((f) => f != "");
    }

    /** Given a target version and a list of files, returns the file with the closest version. */
    export function closestVersion(target: RS.Version, files: ReadonlyArray<string>): string | null
    {
        let closestVer: Version, closestFile: string = null;
        for (const file of files)
        {
            const version = readVersion(file);
            if (!version) { continue; }

            // No closest file yet
            if (!closestFile)
            {
                closestFile = file;
                closestVer = version;
                continue;
            }

            // Is our major version closer?
            const majorDist = dist(target.major, version.major);
            const closestMajorDist = dist(target.major, closestVer.major);
            if (majorDist < closestMajorDist)
            {
                closestFile = file;
                closestVer = version;
                continue;
            }

            // If our major is not the same as target, don't bother with further checks
            if (version.major !== target.major) { continue; }

            // Is our minor version closer?
            const minorDist = dist(target.minor, version.minor);
            const closestMinorDist = dist(target.minor, closestVer.minor);
            if (minorDist < closestMinorDist)
            {
                closestFile = file;
                closestVer = version;
                continue;
            }

            // If our minor is not the same as target, don't bother with further checks
            if (version.minor !== target.minor) { continue; }

            // Is our revision version closer?
            const revDist = dist(target.revision, version.revision);
            const closestRevDist = dist(target.revision, closestVer.revision);
            if (revDist < closestRevDist)
            {
                closestFile = file;
                closestVer = version;
                continue;
            }
        }

        return closestFile;
    }

    /** Returns the distance from a to b. */
    function dist(a: number, b: number): number
    {
        return Math.abs(a - b);
    }

    /** Copies a file from one remote path to another. */
    export async function copyFile(target: SSH.TargetInfo, from: string, to: string, opts?: SSH.Options): Promise<void>
    {
        from = normalisePathSeparators(from);
        to = normalisePathSeparators(to);

        if (from === to)
        {
            // Debug pointless copy early-out
            if (debugMode.value)
            {
                Log.debug(`copyFile passed identical 'from' and 'to' path, skipping remote copy`);
            }

            return;
        }

        await SSH.run(target, "cp", [from, to], opts);
    }

    /** Copies a file from one remote path to another. */
    export async function moveFile(target: SSH.TargetInfo, from: string, to: string, opts?: SSH.Options): Promise<void>
    {
        from = normalisePathSeparators(from);
        to = normalisePathSeparators(to);

        if (from === to)
        {
            // Debug pointless copy early-out
            if (debugMode.value)
            {
                Log.debug(`moveTo passed identical 'from' and 'to' path, skipping remote move`);
            }

            return;
        }

        await SSH.run(target, "mv", [from, to], opts);
    }

    /** Uploads a file to a remote server via RSync. */
    export async function upload(target: SSH.TargetInfo, srcPath: string, destPath: string, opts?: SSH.Options)
    {
        srcPath = normalisePathSeparators(srcPath);
        destPath = normalisePathSeparators(destPath);

        const destString = `${target.user}@${target.host}:${destPath}`;
        if (debugMode.value) { Log.debug(`rsync ${srcPath} ${destString}`); }
        if (!opts) { return Command.run("rsync", [srcPath, destString], null, false); }

        let sshStr = "ssh";
        if (opts.batchMode)
        {
            sshStr += ` -o "BatchMode yes"`;
        }

        if (opts.strictHostKeyChecking === false)
        {
            sshStr += ` -o "StrictHostKeyChecking no"`;
        }

        if (opts.keyPath)
        {
            sshStr += ` -i "${normalisePathSeparators(opts.keyPath)}"`;
        }

        return SSH.runCommandWithOpts("rsync",
        [
            "-e", sshStr,
            srcPath, destString
        ], opts);
    }

    /** Uploads a versioned file quickly by rsyncing against an existing package (if available). */
    export async function sync(target: SSH.TargetInfo, srcPath: string, destPath: string, opts?: SSH.Options)
    {
        try
        {
            if (!await Command.exists("rsync"))
            {
                throw new Error("rsync not found");
            }

            // 1. Get all relevant files

            // Debug files found
            if (debugMode.value)
            {
                Log.debug("Finding existing packages...");
            }

            // Get all files
            const dirPath = Path.directoryName(destPath);
            const dirFiles = await readDirectory(target, dirPath, opts);

            // Debug files found
            if (debugMode.value)
            {
                Log.debug("Remote directory listing:");
                for (const file of dirFiles)
                {
                    Log.debug(file);
                }
            }

            // Filter out irrelevant files
            const packageName = readPackageName(srcPath);
            if (!packageName) { throw new Error(`Failed to ascertain package name from '${srcPath}'`); }
            const packageFiles = dirFiles.filter((f) =>
            {
                const thisPackageName = readPackageName(f);
                return thisPackageName && thisPackageName === packageName;
            });

            // Debug matching files found
            if (debugMode.value)
            {
                Log.debug(`Found ${packageFiles.length} matching package files`);
            }

            // No relevant files found -> full upload
            if (packageFiles.length === 0)
            {
                throw new Error("Nothing to sync with");
            }

            // 2. Pick file likely to be closest to our source file

            // Get package version to compare against
            const targetVer = readVersion(srcPath);
            if (!targetVer) { throw new Error(`Unversioned files are not supported (got '${srcPath}')`); }

            // Sync with the package with version closest to package version
            const closestFile = closestVersion(targetVer, packageFiles);

            // Debug file to be sync'd with
            if (debugMode.value)
            {
                Log.debug(`Syncing using '${closestFile}'`);
            }

            // 3. Create a temporary intermediate folder/path that the automation will ignore
            const tempFolder = Path.combine(dirPath, ".rsynctemp");
            const tempPath = Path.combine(tempFolder, `.${Path.baseName(srcPath)}`);
            await SSH.run(target, "mkdir", [normalisePathSeparators(tempFolder)], opts);

            if (debugMode.value)
            {
                Log.debug(`Intermediate path: ${tempPath}`);
            }

            // 4. Copy closest version to the intermediate path for sync
            const copyFrom = Path.combine(dirPath, closestFile);
            await copyFile(target, copyFrom, tempPath, opts);

            // 5. RSync with it
            await upload(target, srcPath, tempPath, opts);

            // 6. Copy it to destPath
            await copyFile(target, tempPath, destPath, opts);

            // 7. Delete temporary intermediate folder
            await SSH.run(target, "rm", ["-r", normalisePathSeparators(tempFolder)], opts);
        }
        catch (err)
        {
            Log.warn(`RSync error: ${err}, falling back to scp`);
            return await SSH.upload(target, srcPath, destPath, opts);
        }
    }
}