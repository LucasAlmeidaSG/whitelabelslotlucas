/** SSH utils. */
namespace RS.AssembleStages.SSH
{
    /** Runs a command on a remote server via SSH. */
    export function run(target: TargetInfo, command: string, args: ReadonlyArray<string> = [], opts?: Options): PromiseLike<string>
    {
        const targetString = `${target.user}@${target.host}`;
        if (!opts) { return Command.run("ssh", [targetString, command, ...args], null, false); }

        const sshArgs: string[] = [];

        if (opts.batchMode)
        {
            sshArgs.push("-o", "BatchMode yes");
        }

        if (opts.strictHostKeyChecking === false)
        {
            sshArgs.push("-o", "StrictHostKeyChecking no");
        }

        if (opts.keyPath)
        {
            sshArgs.push("-i", opts.keyPath);
        }

        sshArgs.push(targetString, command, ...args);

        return runCommandWithOpts("ssh", sshArgs, opts);
    }

    /** Uploads a file to a remote server via SCP. */
    export function upload(target: TargetInfo, srcPath: string, destPath: string, opts?: Options): PromiseLike<string>
    {
        const destString = `${target.user}@${target.host}:${destPath}`;
        if (!opts) { return Command.run("scp", [srcPath, destString], null, false); }

        const args: string[] = [];

        if (opts.batchMode)
        {
            args.push("-B");
        }

        if (opts.strictHostKeyChecking === false)
        {
            args.push("-o", "StrictHostKeyChecking no");
        }

        if (opts.keyPath)
        {
            args.push("-i", opts.keyPath);
        }

        args.push(srcPath, destString);

        return runCommandWithOpts("scp", args, opts);
    }

    export function runCommandWithOpts(cmd: string, args: string[], opts: Options): PromiseLike<string>
    {
        return Command.run(cmd, args, opts.cwd || null, opts.logOutput || false);
    }

    export interface TargetInfo
    {
        host: string;
        user: string;
    }

    export interface Options
    {
        strictHostKeyChecking?: boolean;
        keyPath?: string;
        cwd?: string;
        logOutput?: boolean;
        batchMode?: boolean;
    }
}