namespace RS.AssembleStages
{
    /**
     * Responsible for deploying the zipped package to a remote server.
     */
    export class DeployPackage extends AssembleStage.Base
    {
        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            // Bypass version validation if DEVDEPLOY is true
            if (!EnvArgs.devdeploy.value && !/^([0-9]+)\.([0-9]+)\.([0-9]+)$/.test(EnvArgs.version.value))
            {
                Log.error(`Version is invalid or missing, must follow the format 'x.y.z'`);
                throw new Error("Deployment failed");
            }

            // Fetch and validate config
            const moduleConfig = Config.activeConfig.moduleConfigs["SG.Packaging"];
            if (!moduleConfig)
            {
                Log.error(`No module config present (SG.Packaging)`);
                throw new Error("Deployment failed");
            }

            const gameCode = (moduleConfig as any).gameCode;
            if (!gameCode)
            {
                Log.error("Module config is missing 'gameCode' property", "SG.Packaging");
                return;
            }

            if ((moduleConfig as any).makeZip !== undefined && (moduleConfig as any).makeZip == false) {
                // Done
                Log.info("No zip created. 'makeZip' in the SG.Packaing config");
                return;
            }

            let zipName = `${gameCode}.${EnvArgs.version.value}.zip`;
            if ((moduleConfig as any).zipName !== undefined && (moduleConfig as any).zipName.toString().trim().length > 0) {
                zipName = `${(moduleConfig as any).zipName.toString().trim()}.zip`;
            }

            // Get path to package
            const packagePath = Path.combine(Config.activeConfig.assemblePath, zipName);
            if (!await FileSystem.fileExists(packagePath))
            {
                Log.error(`File '${packagePath}' not found`);
                throw new Error("Deployment failed");
            }

            // Deploy

            const deployerSettings = (Config.activeConfig.moduleConfigs["SG.Deployment"] as DeploymentSettings) || { strategy: "red7" };
            const deployer = create(deployerSettings);

            await deployer.connect();

            try
            {
                await deployer.deploy(packagePath, gameCode, EnvArgs.version.value);
            }
            catch (err)
            {
                Log.error(err);
                throw new Error("Deployment failed");
            }
            finally
            {
                await deployer.disconnect();
            }
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // No per-module work to do here
        }
    }

    export const deployPackage = new DeployPackage();
    AssembleStage.register({ after: [ assemblePackage ], last: true }, deployPackage);
}