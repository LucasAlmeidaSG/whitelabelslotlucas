namespace RS.EnvArgs
{
    // Set to true to bypass version checking before deploying
    export const devdeploy = new BooleanEnvArg("DEVDEPLOY", false);
}