namespace RS.SG.Nexus.AssembleStages
{
    /**
     * Responsible for deploying the zipped package to a remote server.
     */
    export class NexusDeploy extends AssembleStage.Base
    {
        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            const moduleConfig = Config.activeConfig.moduleConfigs["SG.Packaging"] as Util.Map<any>;
            if (!moduleConfig) { throw new Error("No module config present (SG.Packaging)"); }

            const gameCode = as(moduleConfig.gameCode, Is.string);
            if (!gameCode) { throw new Error(`moduleConfig.gameCode invalid: ${gameCode}`); }

            const zipName = `${gameCode}.${RS.EnvArgs.version.value}.zip`;
            const packagePath = Path.combine(Config.activeConfig.assemblePath, zipName);
            if (!await FileSystem.fileExists(packagePath)) { throw new Error(`Game package not found at ${packagePath}`); }

            const auth: Auth =
            {
                username: EnvArgs.username.isSet ? EnvArgs.username.value : as(moduleConfig.nexusUser, Is.string),
                password: EnvArgs.password.isSet ? EnvArgs.password.value : null
            };

            const url = (EnvArgs.url.isSet ? EnvArgs.url.value : as(moduleConfig.nexusURL, Is.string)) || Defaults.url;
            const repo = (EnvArgs.repository.isSet ? EnvArgs.repository.value : as(moduleConfig.nexusRepo, Is.string)) || Defaults.repository;
            const groupID = as(moduleConfig.nexusGroup, Is.string) || Defaults.groupID;

            if (!url) { throw new Error("Missing Nexus root URL"); }
            if (!repo) { throw new Error("Missing Nexus repository path"); }
            if (!groupID) { throw new Error("Missing Nexus group"); }

            if (!auth.username)
            {
                Log.info("Enter Nexus username:", "SG.Packaging");
                auth.username = await Input.readLine();
            }

            if (!auth.password)
            {
                // todo: secure password input
                throw new Error("Secure terminal password input not yet supported");
                Log.info("Enter Nexus password:", "SG.Packaging");
                auth.password = await Input.readLine();
            }

            const release: NexusRelease =
            {
                groupId: groupID,
                artifactId: gameCode,
                version: RS.EnvArgs.version.value,
                packaging: "zip",
                auth,
                pomDir: "build/pom",
                url: `${url}/nexus/content/repositories/${repo}`,
                artifact: packagePath,
                quiet: true
            };

            await Nexus.deploy(release);
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            // No per-module work to do here
        }
    }

    export const nexusDeploy = new NexusDeploy();
    AssembleStage.register({ after: [ RS.AssembleStages.assemblePackage ], last: true }, nexusDeploy);
}