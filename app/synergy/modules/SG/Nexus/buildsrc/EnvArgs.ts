namespace RS.SG.Nexus.EnvArgs
{
    export const username = new EnvArg("O2_NEXUS_USERNAME", "");
    export const password = new EnvArg("O2_NEXUS_PASSWORD", "");
    export const url = new EnvArg("O2_NEXUS_URL", "");
    export const repository = new EnvArg("O2_NEXUS_REPO", "");
}