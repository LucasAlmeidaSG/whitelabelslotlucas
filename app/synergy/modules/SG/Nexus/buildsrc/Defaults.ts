namespace RS.SG.Nexus.Defaults
{
    export const url = "https://nexus.wi-gameserver.com";
    export const repository = "releases";
    export const groupID = "com.williamsinteractive.mobile";
}