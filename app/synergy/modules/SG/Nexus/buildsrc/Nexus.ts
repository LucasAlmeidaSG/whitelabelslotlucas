namespace RS.SG.Nexus
{
    const deployer = require("nexus-deployer");
    
    export interface Auth
    {
        username: string;
        password: string;
    }

    export interface NexusRelease
    {
        /**
         * Maven group ID.
         * Determines the portion of the URL preceding the artifact folder.
         * E.g. com.williamsinteractive.mobile
         */
        groupId: string;
        /**
         * Maven artifact ID.
         * Determines the artifact portion of the URL.
         * For games, this should be the gamecode.
         * E.g. eightyeightfortunesmegaways
         */
        artifactId: string;
        /**
         * Maven aritfact version.
         * Should follow the format X.Y.Z.
         * For games, this should be the game version.
         */
        version: string;
        /**
         * Packaging format.
         * E.g. zip
         */
        packaging: "zip";
        /**
         * Nexus repository URL.
         * E.g. https://nexus.wi-gameserver.com
         */
        url: string;
        /** Path to the artifact to be deployed. */
        artifact: string;
        
        /** Nexus authentication details. */
        auth?: Auth;
        /** Whether to not validate SSL certificates. Corresponds to cURL --insecure. */
        insecure?: boolean;
        /** Maven manifest directory. */
        pomDir?: string;
        noproxy?: string;
        cwd?: string;
        /** Whether or not to log to the console. */
        quiet?: boolean;
        /** Whether or not to upload in parallel. */
        parallel?: boolean;
        classifier?: string;
    }

    export function deploy(release: NexusRelease)
    {
        return new Promise<void>((resolve, reject) =>
        {
            deployer.deploy(release, function (error: any)
            {
                if (error) { reject(error); }
                resolve();
            });
        });
    }
}