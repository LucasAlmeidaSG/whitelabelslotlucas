namespace SG.Replay
{
    export class Replay implements IReplay
    {
        public readonly gameID: number;

        public readonly stake: number;

        public readonly win: number;

        protected states: string[];
        protected currentStateIndex: number;
        public get currentState(): string { return this.states[this.currentStateIndex] || null; }

        public get isComplete(): boolean { return this.currentStateIndex == this.states.length; }

        constructor(gameID: number, stake: number, win: number, states: string[])
        {
            this.gameID = gameID;
            this.stake = stake;
            this.win = win;

            this.states = states;
            this.currentStateIndex = 0;
        }

        public advance(): void
        {
            if (this.currentStateIndex < this.states.length)
            {
                this.currentStateIndex++;
            }
            else
            {
                RS.Log.warn("[SG.Replay.Replay] advance() called when no more replay states available!");
            }
        }

        public reset(): void
        {
            this.currentStateIndex = 0;
        }
    }
}