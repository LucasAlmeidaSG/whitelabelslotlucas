namespace SG.Replay
{
    /**
     * Encapsulates parsed replay data.
     */
    export interface IReplay
    {
        /**
         * The ID of the game this replay is for.
         */
        readonly gameID: number;

        /**
         * The player's stake for this wager.
         */
        readonly stake: number;

        /**
         * The total win for this wager.
         */
        readonly win: number;

        /**
         * The current replay state. Null if the replay is complete.
         */
        readonly currentState: string;
        
        /**
         * True iff this object has been advanced to the final replay request.
         */
        readonly isComplete: boolean;

        /**
         * Advances to the next replay state (i.e. next spin/pick/etc. request).
         */
        advance(): void;

        /**
         * Resets to the first history state. Also resets isComplete to false, if it is
         * true.
         */
        reset(): void;
    }
}