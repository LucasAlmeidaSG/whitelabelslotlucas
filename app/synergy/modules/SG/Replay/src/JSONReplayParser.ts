namespace SG.Replay
{
    /**
     * Parses a JSON replay object, whose structure follows the interface ReplayData.
     */
    export class JSONReplayParser implements IReplayParser
    {
        public readonly dataType: RS.Request.AJAXResponseType = RS.Request.AJAXResponseType.JSON;

        public parseReplay(dataArray: object[]): IReplay
        {
            const data: object = dataArray[0];
            if (!isReplayData(data))
            {
                throw new ParseError("Invalid replay data", data);
            }

            const states: string[] = [];
            for (const stateData of data["Data"])
            {
                if (!RS.Is.string(stateData["Comment"]))
                {
                    throw new ParseError("Invalid replay state data", data);
                }

                states.push(stateData["Comment"]);
            }

            return new Replay(data["GameId"], data["Stake"], data["Win"], states);
        }
    }

    export class ParseError extends Error
    {
        constructor(message: string, public readonly data: any)
        {
            super(message);
        }
    }

    IReplayParser.register(JSONReplayParser);
}