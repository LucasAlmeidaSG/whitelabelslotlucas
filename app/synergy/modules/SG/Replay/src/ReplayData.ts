namespace SG.Replay
{
    export interface ReplayData
    {
        Closed: number;
        Data: ReplayStateData[];
        GameId: number;
        Stake: number;
        Win: number;
    }

    export interface ReplayStateData
    {
        Comment: string;
        SequenceId: number;
    }

    /**
     * Returns whether or not a given object is a valid replay data object.
     */
    export function isReplayData(obj: any): obj is ReplayData
    {
        return RS.Is.object(obj) && RS.Is.number(obj["Closed"]) && RS.Is.array(obj["Data"]) && RS.Is.number(obj["GameId"]) && RS.Is.number(obj["Stake"]) && RS.Is.number(obj["Win"]);
    }
}