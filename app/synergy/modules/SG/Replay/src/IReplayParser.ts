namespace SG.Replay
{
    /**
     * Parses replay data into a replay object.
     *
     * @interface
     */
    export interface IReplayParser
    {
        /**
         * The data type parsed by this parser.
         */
        readonly dataType: RS.Request.AJAXResponseType;

        /**
         * Parses a replay object and returns an IReplay.
         *
         * @param data the unparsed replay object in a format corresponding to the specified dataType
         * @returns an object which can be used to generate replay request data
         */
        parseReplay(data: any): IReplay;
    }

    export const IReplayParser = RS.Strategy.declare<IReplayParser>();
}