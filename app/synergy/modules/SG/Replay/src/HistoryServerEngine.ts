/// <reference path="IEngine.ts" />

namespace SG.Replay
{
    /**
     * Retrieves replay data from a remote history server, as configured by an
     * IHistoryServerConfigurator.
     */
    export class HistoryServerEngine implements IEngine
    {
        protected _error: boolean = false;
        public get error(): boolean { return this._error; }

        public get isReplay(): boolean
        {
            return this.configurator.isReplay;
        }

        protected _replay: IReplay;
        public get replay(): IReplay { return this._replay; }

        protected configurator: IHistoryServerConfigurator = IHistoryServerConfigurator.get();
        protected parser: IReplayParser = IReplayParser.get();

        public async init()
        {
            if (this.configurator.isReplay)
            {
                RS.Log.debug("[SG.Replay.Engine] Replay configuration found, retrieving replay data");
                const ajaxFunc: (settings: RS.Request.AJAXSettings, responseType: RS.Request.AJAXResponseType) => PromiseLike<any> = RS.Request.ajax;

                try
                {
                    const data = await ajaxFunc(
                    {
                        url: this.configurator.getReplayURL(),
                        method: "GET"
                    },
                    this.parser.dataType);
                
                    RS.Log.debug("[SG.Replay.Engine] Replay data received: " + JSON.stringify(data));
                    this._replay = this.parser.parseReplay(data);
                }
                catch (err)
                {
                    this._error = true;
                    throw err;
                }
            }
        }
    }

    IEngine.register(HistoryServerEngine);
}