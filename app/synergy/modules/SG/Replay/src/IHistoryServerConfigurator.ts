namespace SG.Replay
{
    /** 
     * Gets configuration information for the history server engine, such as whether to get
     * replay data and where to get it from.
     */
    export interface IHistoryServerConfigurator
    {
        /**
         * Whether or not a replay can be played.
         */
        readonly isReplay: boolean;

        /**
         * Returns the replay data URL.
         *
         * @returns the URL of the replay data, e.g. https://pa01-ggms.wi-gameserver.com/gls-game-handler/game-history/partner/4/game-play/4_1000110638_2
         */
        getReplayURL(): string;

        /**
         * Returns the bet ID which uniquely identifies the wager for the replay.
         *
         * @returns the bet ID, e.g 4_1000110638_2
         */
        getBetID(): string;
    }

    export const IHistoryServerConfigurator = RS.Strategy.declare<IHistoryServerConfigurator>();
}