/// <reference path="IHistoryServerConfigurator.ts" />

namespace SG.Replay
{
    /**
     * Configures the history server engine using URL arguments.
     */
    export class URLParamConfigurator implements IHistoryServerConfigurator
    {
        public get isReplay(): boolean
        {
            return this.getHistoryServerURL() != null && this.getBetID() != null;
        }

        public getReplayURL(): string
        {
            return this.isReplay ? this.getHistoryServerURL() + this.getBetID() : null;
        }

        public getBetID(): string
        {
            const urlParam: string = RS.URL.getParameter("betID");
            return urlParam != "null" ? urlParam : null;
        }

        /**
         * The URL of the history server, without the betID, e.g. https://pa01-ggms.wi-gameserver.com/gls-game-handler/game-history/partner/4/game-play/
         */
        protected getHistoryServerURL(): string
        {
            const urlParam: string = RS.URL.getParameter("HistoryServerURL");
            return urlParam != "null" ? urlParam : null;
        }
    }

    IHistoryServerConfigurator.register(URLParamConfigurator);
}