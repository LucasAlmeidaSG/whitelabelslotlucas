namespace SG.Replay
{
    /**
     * Manages all replay information.
     */
    export interface IEngine
    {
        /**
         * Whether or not there is a valid replay for this session.
         */
        readonly isReplay: boolean;

        /**
         * Whether or not an error was encountered whilst initialising replay data.
         */
        readonly error: boolean;
        
        /**
         * Object encapsulating replay data, such as playback states. Null if isReplay is false.
         */
        readonly replay: IReplay;

        /**
         * Initialises the replay engine, making any necessary replay requests.
         */
        init(): void;
    }

    export const IEngine = RS.Strategy.declare<IEngine>();
}