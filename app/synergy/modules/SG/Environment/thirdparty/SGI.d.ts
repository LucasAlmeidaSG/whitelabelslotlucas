declare namespace SGI
{

    /**
     * Entry point for the game.
     */
    export interface IGame
    {
        launchGame(): void;
        setPartnerAdapter(partnerAdapter: IPartnerAdapter): void;
        resetHard(): void;
        startAutoplay(playcount: number): void;
        stopAutoplay(): void;
        pauseGame(): void;
        resumeGame(): void;
        setOrientationLandscape(): void;
        setOrientationPortrait(): void;

        /**
         * If a game needs to track the balances of any OGS jackpot, providers are also expected to implement this method on the game instance specified to GCM.
         * After invoking pollJackpots method on GCM (as described previously), the game will receive updates regarding the balances of any specified jackpot(s) through this method.
         * @param jackpots
         */
        jackpotBalancesUpdate?(jackpots: JackpotData[]): void;

        addWinDisplayListener(listener: Events.IWinDisplayEventListener): void;
        addPlatformBalanceListener(listener: Events.IBalanceDisplayEventListener): void;
        addGamePlayEventListener(listener: Events.IGameInitializedEventListener): void;
        setBalanceService(balanceService: IBalanceService): void;

        /**
         * Called by the platform to pass promotional info, to be later passed in a logic request.
         */
        handlePromoInfoUpdate(info: PromotionInfo): void;

        /**
         * Called by the platform to pass free rounds info, to be later passed in a logic request.
         */
        handleFreeRoundsUpdate(info: FreeRoundsInfo): void;

        /**
         * Called by the platform when the UI can be enabled after loading, or potentially after a platform-controlled dialog (TBC).
         */
        gameRevealed(): void;
    }

    // #region Services

    /**
     * Partner adapter lobby navigation service
     */
    export interface ILobbyNavigationService
    {
        /**
         * Returns true if the partner adapter supports returning to the lobby.
         */
        canGoToLobby(): boolean;

        /**
         * Returns to the lobby.
         */
        goToLobby(): void;
    }

    /**
     * Partner adapter external help navigation service
     */
    export interface IExternalHelpNavigationService
    {
        /**
         * Returns true if the partner adapter supports navigating to the external help page.
         */
        canGoToExternalHelp(): boolean;

        /**
         * Navigates to the external help page.
         */
        goToExternalHelp(): void;
    }

    /**
     * Partner adapter balance service
     */
    export interface IBalanceService
    {
        fetchBalance(success: IBalanceService.FetchSuccessFunc, failure: IBalanceService.FetchFailureFunc): void;
    }

    export namespace IBalanceService
    {
        export interface Response
        {
            realBalance: number;
            statusCode: string;
            message: string;
        }
        export type FetchSuccessFunc = (playerBalanceRespone: Response) => void;
        export type FetchFailureFunc = (message: string) => void;
    }

    /** Information about an awarded free rounds campaign. */
    export interface FreeRoundsInfo
    {
        readonly CAMPAIGNID: string;
        readonly ACTIVATIONID: string;
        readonly STATUS: FreeRoundsInfo.Status;
        /** 
         * Options available to the player.
         * After player decision, this is reduced to a 1-length array with the chosen bet option.
         */
        readonly OPTIONS: ReadonlyArray<FreeRoundsInfo.Option>;
    }

    export namespace FreeRoundsInfo
    {
        export type Status = "notstarted" | "inprogress" | "completed";

        export interface Option
        {
            /** Bet level for this option (£X.YY) */
            BETLEVEL: number;
            /** Number of free rounds this option awards */
            REMAININGROUNDS: number;
        }
    }

    export interface PromotionInfo
    {
        readonly PROMOTIONS:
        {
            /**
             * An array of Free Rounds campaigns.
             * The OGS Integration guidelines where there are multiple campaigns awarded to that player and for that game that was launched:
             * - Resume any ongoing campaign (and disregard the REJECTABLE parameter)
             * - If none of the campaings has started (all are new campaigns), then choose the first.
             */
            readonly FREEROUNDS: ReadonlyArray<PromotionInfo.FreeRoundsInfo>;
            /**
             * Unknown what exactly this represents.
             * As of 07/08/2019, is ALWAYS equal to 'complete' (see leanpartneradapter source code).
             * The correct Free Rounds status for a completed campaign is 'completed'.
             * However, this seems to be sent in its stead anyway.
             */
            readonly GAMEPLAY: PromotionInfo.GameplayStatus;
        };
    }

    export namespace PromotionInfo
    {
        export interface FreeRoundsInfo
        {
            readonly CAMPAIGNID: string;
            readonly ACTIVATIONID: string;
            /** Total value of the campaign (£X.YY) */
            readonly CAMPAIGNVALUE: number;
            /** Whether or not the player may reject the campaign and play as normal */
            readonly REJECTABLE: boolean;
        }
        
        export type GameplayStatus = "complete" | "pending";
    }

    // #endregion

    // #region Jackpots

    export interface Syndication
    {
        id: string;
        name: string;
        playerCount: number;
        winEligibilityPeriod: number;
    }

    export interface JackpotData
    {
        id: string;
        instance: string | number;
        balanceAmountInRequestedCurrency: number;
        balanceAmountInJackpotCurrency: number;
        requestedCurrency: string;
        jackpotCurrency: string;
        syndication?: Syndication;
    }
    // #endregion

    export type MenuHandlerDelegate = (data: any) => void;
    export type MenuHandlerMuteDelegate = (muted: boolean) => void;
    export type MenuHandlerSettingsDelegate = (data: { visible?: boolean; open: boolean; }) => void;

    export interface ILeanPartnerAdapter extends IPartnerAdapter
    {
        logics:
        {
            platform:
            {
                getPromoInfo(): PromotionInfo;
            }
        }
    }

    /**
     * The SG partner adapter is a JavaScript object that is provided to games by the SG server environment during loading.
     * It acts as the bridge between the game and any external software that is not the GLM.
     *
     * Its responsibilities include:
     * - Handling game errors
     * - Exposing internal game information for use by other parties such as partners
     * - Interfacing with the SG menu and autoplay functionality
     *
     * In order to correctly integrate with SG partners games must ensure they call all required functions during gameplay to expose the internal game information.
     * For more information on the functions the partner adapter provides and their usage please read the following sections.
     * https://docs.google.com/document/d/1iat0CwdN9f0WIepeBKEJMzbMbaTmuCohMY2jgMA_KQc/view#
     */
    export interface IPartnerAdapter
    {
        readonly partnerAutoplayManager: IAutoplayManager;

        /**
         * Adds a balance update listener to the partner adapter.
         */
        addBalanceUpdateListener(listener: Events.IBalanceUpdateEventListener): void;
        /**
         * Set the game in the partner adapter. When this is done the game should be ready to receive requests
         * @param game the game to communicate with
         */
        setGame(game: IGame): void;
        /**
         * Set the platform
         */
        setPlatform(platform: object): void;
        /**
         * Get the session data
         */
        getSessionData(): ISessionData;
        /**
         * Get the root element that the game should create its view inside. Parents or siblings of
         * this element MUST NOT be manipulated or accessed by the game.
         *
         * The method returns a reference to the page DIV element where the game can
         * load it’s DOM elements that make up the game UI.
         */
        getGameDomElement(): HTMLDivElement;
        /**
         * Shows the progress bar
         *
         * This method will show a game loading screen with a horizontal progress bar.
         * The parameters are not used, and calling this method will always show a full
         * screen loading page. It may or may not show the textual representation of
         * the percentage. Use this loading screen to indicate to the player that the
         * game and game assets are being loaded by the game code.
         *
         * @param  isFullScreen  show in full screen mode
         * @param  displayPercentage  true to have percentage shown
         */
        showProgressBar(isFullScreen: boolean, displayPercentage: boolean): void;
        /**
         * Hides the progress bar
         *
         * Call this method when all game assets are finished loading and the game can
         * be displayed fully to the player.
         */
        hideProgressBar(): void;
        /**
         * Update the progression of the loading bar
         *
         * Call this method to advance the progress of loading the game assets.
         * The value parameter should be a decimal in the range of 0 and 1. The message
         * parameter is not used.
         *
         * @param  value  progress from 0.0 to 1.0 inclusive.
         * @param  message  loading message to display
         */
        updatedLoadingProgress(value: number, message: string): void;
        /**
         * Call this method after a game has been initiated, and before a
         * GameRequest[type=”Logic”] request is made.
         * The Partner Adapter uses this call to set the status bar Win to
         * an empty value
         */
        startedPlay(): void;
        /**
         * Call this method when the GameResponse response from a
         * GameRequest[type=”EndGame”] request to the server has been received.
         */
        finishedPlay(): void;
        /**
         * Call this method after all feature games (Free Spins, etc) and win animations
         * are complete.
         */
        finishedPostGameAnimations(): void;
        /**
         * When the game has received a response from the game server it should send
         * this to the partner adapter
         *
         * Call this method for each response returned by the GLS server. Send the entire
         * XMLHttpRequest object as the response parameter.
         * The partner adapter requires this to handle any incoming errors and platform
         * specific data returned by the server. The game should not handle any errors
         * returned from the server.
         *
         * In the event of an error returned from the logic server, this method will throw
         * an exception. The game is not required or even expected to handle this exception.
         * The format and type of the exception is not specified. The game may catch this
         * exception, but shouldn't process the response any further.
         *
         * It is expected that the game will call this method before attempting any processing
         * of the response.
         *
         * @param response The XMLHttpRequest object
         */
        receivedGameLogicResponse(response: XMLHttpRequest): void;
        /**
         * Update the bet. Should be called on startup and when the user changes the bet.
         *
         * Call this method when the game wager/stake is initialized or changed. The
         * totalBet parameter should be a number in the same units used by the GLS server.
         *
         * @param bet in cents
         */
        updateBet(bet: number): void;
        /**
         * Show error. Errors that is received from the game server will be handled by
         * the partner adapter so the game should not call this function when such error
         * occurs.
         *
         * Call this to display an error message in a dialog to the player.
         *
         * @param heading The text to display in the dialog title bar
         * @param message The text to display as the dialog message
         * @param button The text displayed in the dialog dismiss button
         * @param callback A callback function that is called when the player dismisses
         * the dialog
         *
         * Note: The game should not handle any error messages returned by the server.
         * Server errors will be handled by the Partner Adapter through
         * receivedGameLogicResponse.
         */
        showError(heading: string, message: string, button: string, callback: () => void): void;
        /**
         * Reload the game page
         *
         * Call this method to reload the game
         */
        reload(): void;
        /**
         * Redirect the browser to the lobby URL
         *
         * Call this method to return the player to the partner lobby
         */
        goHome(): void;
        setMenuHandler(type: "MUTE", handler: MenuHandlerMuteDelegate): MenuHandlerMuteDelegate;
        setMenuHandler(type: "SETTINGS", handler: MenuHandlerSettingsDelegate): MenuHandlerSettingsDelegate;
        /**
         * Call this method to register a callback handler for type of menu item.
         *
         * @param type Type of handler to register. Only supported types are 'MUTE', 'SETTINGS'
         * @param handler The callback when the state of @param.type changes.
         *
         * @return delegate(data: any): void returns a method/delegate the game can use to update the menu state.
         *      the data parameter is of the same type and semantic as the handler parameter
         *
         * The handler argument (data) is any data relevant to the menu item.
         * For 'MUTE', this will be a boolean value indicating whether the audio should be MUTEd or not.
         * For 'SETTINGS', this will be a structure:
         *  {
         *    visible: boolean // whether the settings button is/set visible,
         *    open:    boolean // whether the settings menu is/set visible
         *  }
         *
         * ex)
         *	var updateMenu = partnerAdapter.setMenuHandler(‘MUTE’, function(data) {
         *		//data will be a boolean value for MUTE.
         *		// true - Audio Off, false - Audio On
         *		var mute = data == true;
         *		if(mute) {
         *			// mute the game audio
         *		} else {
         *			// unmute the game audio
         *		}
         *	});
         *
         *  updateMenu(true); //Update the partner adapter ‘MUTE’ menu to show sound enabled
         *  updateMenu(false); //Update the partner adapter ‘MUTE’ menu to show sound disabled
         *
         * ex)
         *  var updateSettings = partnerAdapter.setMenuHandler('SETTINGS', function(data) {
         *      //I am notified of when the partnerAdapter settings button and menu state changes
         *      //data - {visible: boolean, open: boolean}
         *      //   visible - whether the settings button is visible or not
         *      //   open - whether the settings menu is open or not
         *      if(data.visible) console.log('The settings button is visible');
         *      else console.log('The setting button is hidden');
         *
         *      if(data.open) console.log('The settings menu is open/showing');
         *      else console.log('The settings menu is closed/hidden');
         *  });
         *
         *  updateSettings({visible: true}); //Show the settings button
         *  updateSettings({open: true}); //Show the settings menu
         *  updateSettings({visible: false, open: true}); //Show the settings menu and hide the button
         */
        setMenuHandler(type: string, handler: MenuHandlerDelegate): MenuHandlerDelegate;
        /**
         * Format a number to a string displayed as a money value
         */
        formatCurrency(cents: number): string;
        /**
         * Returns the lobby navigation service.
         */
        getLobbyNavigationService(): ILobbyNavigationService;
        /**
         * Returns the external help navigation service.
         */
        getExternalHelpNavigationService(): IExternalHelpNavigationService;
        /**
         * Informs the partner adapter that the game is ready.
         */
        gameReady(): void;

        /**
         * Game clients should invoke this method on GCM if they need to be notified for any changes in the balances of any OGS jackpot(s).
         * Note that pollJackpots method must be invoked after GCM initialization completes (i.e. after callback configReady(ogsParams) has been called by GCM), and every time the game receives new jackpot instance ids from the game server (dependent on the game server’s integration protocol).
         * @param currency The currency to which the balances will be denominated (typically the player’s currency) in ISO-4217 three-letter code.
         * @param jackpots An array with the list of jackpot instance identifiers for which the balances will be retrieved. Game clients are expected to receive the active identifiers of the jackpots applicable to the game at any given time, from the game server.
         */
        pollJackpots?(currency: string, jackpots: (string | number)[]): void;

        /**
         * Triggers GCM dialog asking for player to wait for their win share to be calculated
         * @param payload 
         */
        showJackpotMessageDialog?(): void;
    }

    /**
     * The game session data is available through the Partner Adapter method,
     * getSessionData(). It contains data about the player, session, and context.
     */
    export interface ISessionData
    {
        /**
         * This method returns the session context.
         * Used to set the GLS GameRequest.Header[affiliate]
         *
         * ex)
         * var header = ‘<Header affiliate=”’ + partnerAdapter.getSessionData().getContextID() + ‘“ />’;
         */
        getContextID(): string;
        /**
         * This method returns the player ID. Used to set the GLS GameRequest.Header[userID]
         *
         * ex)
         * var header = ‘<Header userID=”’ + partnerAdapter.getSessionData().getPlayerID() + ‘“ />’;
         */
        getPlayerID(): string;
        /**
         * This method returns the initial sessionID to be used specifically for
         * setting the GameRequest[type=”Init”].Header[sessionID]
         *
         * ex)
         * var request = ‘<GameRequest type=”Init”><Header sessionID=”’ + partnerAdapter.getSessionData().getSessionID() + ‘“ /></GameRequest>”;
         *
         * Note: The Game Response contains a new sessionID.
         * The sessionID returned from each response should be used in the Header
         * when making the next GameRequest.
         */
        getSessionID(): string;
        /**
         * This method returns an identifier for the player.
         * It is generally not used by games
         */
        getPlayerHash(): string;
    }

    /**
     *
     */
    export interface IAutoplayManager
    {
        /**
         *
         * @param currentTotalSTake
         * @param currentBalance
         * @param callback
         */
        initPartnerAutoplay(currentTotalStake: number, currentBalance: number, callback: (isEnabled: boolean) => void): void;

        /**
         *
         * @param currentTotalSTake
         * @param currentBalance
         * @param callback
         */
        launchPartnerAutoplay(currentTotalStake: number, currentBalance: number, callback: (isEnabled: boolean) => void): void;

        /**
         *
         * @param win
         * @param stake
         */
        updateWinAndStake(win: number, stake: number): void;

        /**
         *
         * @param status
         * @param remainingSpins
         */
        updateAutoPlayStatus(status: string, remainingSpins: number): void;

        /**
         *
         * @param status
         * @param remainingSpins
         */
        updateOnBetChange(totalbet: number): void;
    }

    export namespace Events
    {
        export interface GameInitializedEventData
        {
            isResumeGame: boolean;
            CASH: number;
            FREEBET: number;
            balanceAmount: number;
        }

        export interface IGameInitializedEventListener
        {
            handleGameInitializedEvent(eventData: GameInitializedEventData): void;
        }

        export interface BalanceDisplayEventData
        {
            balanceAmount: number;
            CASH: number;
            FREEBET: number;
        }

        export interface IBalanceDisplayEventListener
        {
            handlePlatformDisplayEvent(eventData: BalanceDisplayEventData): void;
        }

        export interface IBalanceUpdateEventListener
        {
            balanceNotificationFromPartnerAdapter(): void;
            handleSetBalance(setBalanceEvent: { getBalanceAmount: () => number }): void;
        }

        export interface WinDisplayEventData
        {
            winAmount: number;
        }

        export interface IWinDisplayEventListener
        {
            handleWinDisplayEvent(eventData: WinDisplayEventData);
        }
    }

    export interface IMetaDataBundle
    {
        isDebugEnabled(): boolean;
        isGaffingEnabled(): boolean;
        getResourceVersion(): string;
        getLocale(): string;
        isDemoEnabled(): boolean;
        getWebaudio(): boolean;
        isRealMoney(): boolean;
        getGameCode(): string;
        getCdnRoot(): string;
        getLogicUrl(): string;
        getCountryCode(): string;
        getMaxWinStatus(): boolean;
        getExtraBetConfig(): { [key: string]: any };
    }

    export interface IPlatformMetaDataBundle
    {
        getAutomaticBetPerUnitIncrease(): number;
        getAutomaticBetUnitsIncrease(): number;
        getExternalAuthUrl(): string;
        getExternalBalanceTimeOut(): number;
        getExternalBalanceUrl(): string;
        getGame(): string;
        getLocale(): string;
        getLogicInitUrl(): string;
        getLogicPlayEndUrl(): string;
        getLogicPlayUrl(): string;
        getMinimumSpinTimeMillis(): number;
        getMobileConfigUrl(): string;
        getPartnerCode(): string;
        getResourceVersion(): string;
        getResponsibleGamingInterval(): number;
        isDebugEnabled(): boolean;
        isRealmoney(): boolean;
        isTouchDevice(): boolean;
    }

    export interface IPartnerAdapterMetaDataBundle
    {
        getAutoplayLossLimit(): "ON"|"OFF";
        getAutoplayStatus(): "ON"|"OFF";
        isDebugEnabled(): boolean;
        isTouchDevice(): boolean;
        getMaxAutoSpin(): string;
        getAutoplayStopOnWin(): "ON"|"OFF";
        isRealmoney(): boolean;
        getLocale(): string;
        getResourceVersion(): string;
        getResponsibleGamingUrl(): string;
        getResponsibleGamingInterval(): number;
        getHistoryUrl?(): string;
        getPlayerChoiceUrl?(): string;
    }
}

declare namespace glsplatform
{
    export const MetaDataBundle:
    {
        new(): SGI.IPlatformMetaDataBundle;
        prototype: SGI.IPlatformMetaDataBundle;
    };
}

declare namespace leanpartneradapter
{
    export const MetaDataBundle:
    {
        new(): SGI.IPartnerAdapterMetaDataBundle;
        prototype: SGI.IPartnerAdapterMetaDataBundle;
    };
}
