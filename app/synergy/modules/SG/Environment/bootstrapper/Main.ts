/// <reference path="MetaDataBundle.ts" />

namespace SG
{
    interface ScriptSettings
    {
        url: string;
        integrity: string;
    }

    const shouldEnforceHash = ("{!ENFORCE_HASH!}" as string) !== "false";

    const scripts: ScriptSettings[] =
    [
        { url: "{!THIRDPARTY_FILENAME!}", integrity: "sha256-{!THIRDPARTY_HASH!}" },
        { url: "{!APP_FILENAME!}", integrity: "sha256-{!APP_HASH!}" }
    ];

    function loadScript(settings: ScriptSettings): HTMLScriptElement
    {
        const tag = document.createElement("script");
        tag.type = "text/javascript";
        tag.src = `${metaDataBundle.getCdnRoot()}/content/${metaDataBundle.getGameCode()}/${settings.url}`;
        if (shouldEnforceHash) { tag.integrity = settings.integrity; }
        tag.crossOrigin = "anonymous";
        tag.async = true;
        tag.defer = true;
        document.head.appendChild(tag);
        return tag;
    }

    function loadNextScript()
    {
        if (scripts.length === 0) { return; }
        const tag = loadScript(scripts.shift());
        tag.onload = loadNextScript;
    }

    loadNextScript();
}