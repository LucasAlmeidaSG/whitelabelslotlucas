namespace SG
{
    // Note that the strings in this class are replaced at runtime by the environment provided by the uber server.

    export class MetaDataBundle implements SGI.IMetaDataBundle
    {
        private _extraBetConfig: any;
        private _extraBetConfigResolved = false;

        public isDebugEnabled(): boolean
        {
            return "${debugenabled}".toLowerCase() === "true";
        }

        public isGaffingEnabled(): boolean
        {
            return "${gaffingenabled}".toLowerCase() === "true";
        }

        public getResourceVersion(): string
        {
            return "${resourceversion}";
        }

        public getLocale(): string
        {
            return "${locale}";
        }

        public isDemoEnabled(): boolean
        {
            return "${demoenabled}".toLowerCase() === "true";
        }

        public getWebaudio(): boolean
        {
            return "${webaudio}".toLowerCase() === "true";
        }

        public isRealMoney(): boolean
        {
            return "${realmoney}".toLowerCase() === "true";
        }

        public getGameCode(): string
        {
            return "${game}";
        }

        public getCdnRoot(): string
        {
            return "${cdnroot}";
        }

        public getLogicUrl(): string
        {
            return "${logicInitURL}";
        }

        public getCountryCode(): string
        {
            return "${countrycode}";
        }

        public getMaxWinStatus(): boolean
        {
            return "${maxwin}".toLowerCase() === "on";
        }

        public getExtraBetConfig(): { [key: string]: any }
        {
            if (this._extraBetConfigResolved)
            {
                return this._extraBetConfig;
            }
            else
            {
                const cfg = this._extraBetConfig = this.tryParseExtraBetConfig("${extraBetConfig}");
                this._extraBetConfigResolved = true;
                return cfg;
            }
        }

        /**
         * Tries to parse the given ExtraBetConfig string as an ExtraBetConfig object.
         *
         * Upon failure, logs the error to console and returns an empty ExtraBetConfig object.
         */
        protected tryParseExtraBetConfig(str: string): { [key: string]: any }
        {
            try
            {
                return this.parseExtraBetConfig(str);
            }
            catch (err)
            {
                console.error(`[SG.MetaDataBundle] Failed to parse "${str}" as JSON object: ${err}`);
                return {};
            }
        }

        /**
         * Parses the given ExtraBetConfig string as an ExtraBetConfig object.
         *
         * Throws an error if str does not represent a valid ExtraBetConfig string.
        */
        protected parseExtraBetConfig(str: string): { [key: string]: any }
        {
            const obj = JSON.parse(str);
            if (typeof obj !== "object")
            {
                throw new Error(`${obj} is not an object`);
            }
            return obj;
        }
    }

    export const metaDataBundle = new MetaDataBundle();
}