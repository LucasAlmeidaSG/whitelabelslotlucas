namespace SG
{
    export interface IEntryPoint
    {
        /**
         * Gets the SGI partner adapter, or null if one does not exist.
         */
        readonly partnerAdapter: SGI.IPartnerAdapter | null;

        /**
         * Sets or sets an object to redirect all SGI calls to.
         */
        proxy: SGI.IGame | null;
    }

    export interface IStoredListeners 
    {
        winDisplayListener?: SGI.Events.IWinDisplayEventListener;
        balanceDisplayListener?: SGI.Events.IBalanceDisplayEventListener;
        gameInitialiseListener?: SGI.Events.IGameInitializedEventListener;
    }

    export type EntryPointReady = () => void;

    export let entryPoint: IEntryPoint | null = null;
    export let entryPointReadyCallback: EntryPointReady | null = null;

    function validatePath(obj: object, ...keys: string[]): boolean
    {
        let value: any = obj;
        let key = keys.shift();
        while (key)
        {
            if (typeof value !== "object" || !(key in value)) { return false; }
            value = value[key];
            key = keys.shift();
        }
        return true;
    }

    export class EntryPoint implements SGI.IGame, IEntryPoint
    {
        private _balanceService: SGI.IBalanceService | null = null;
        private _partnerAdapter: SGI.IPartnerAdapter | null = null;
        private _proxy: SGI.IGame | null = null;
        private _autoplay: number | null = null;
        private _paused: boolean | null = null;
        private _orientation: "landscape" | "portrait" | null = null;
        private _storedListeners: IStoredListeners = {};

        public get partnerAdapter() { return this._partnerAdapter; }

        public get proxy() { return this._proxy; }
        public set proxy(value)
        {
            if (value === this._proxy) { return; }
            this._proxy = value;
            if (!value) { return; }
            this.log("got new proxy object");
            if (this._partnerAdapter != null) { this.setPartnerAdapterOnProxy(value, this._partnerAdapter); }
            if (this._autoplay != null) { value.startAutoplay(this._autoplay); }
            if (this._paused === true) { value.pauseGame(); }
            if (this._paused === false) { value.resumeGame(); }
            if (this._orientation === "landscape") { value.setOrientationLandscape(); }
            if (this._orientation === "portrait") { value.setOrientationPortrait(); }
            if (this._balanceService != null) { value.setBalanceService(this._balanceService); }
            if (this._storedListeners != null && this._storedListeners.winDisplayListener) 
            {
                value.addWinDisplayListener(this._storedListeners.winDisplayListener);
            }
            if (this._storedListeners != null && this._storedListeners.balanceDisplayListener) 
            {
                value.addPlatformBalanceListener(this._storedListeners.balanceDisplayListener);
            }
            if (this._storedListeners != null && this._storedListeners.gameInitialiseListener) 
            {
                value.addGamePlayEventListener(this._storedListeners.gameInitialiseListener);
            }
        }

        public constructor()
        {
            entryPoint = this;
            if (entryPointReadyCallback != null) { entryPointReadyCallback(); }
        }

        public launchGame(): void
        {
            this.log(`launchGame()`);
            if (this._proxy) { this._proxy.launchGame(); }
        }

        public setPartnerAdapter(partnerAdapter: SGI.IPartnerAdapter): void
        {
            this.log(`setPartnerAdapter(${partnerAdapter})`);
            this._partnerAdapter = partnerAdapter;
            if (this._proxy) { this.setPartnerAdapterOnProxy(this._proxy, partnerAdapter); }
        }

        public resetHard(): void
        {
            this.log(`resetHard()`);
            if (this._proxy) { this._proxy.resetHard(); }
        }

        public startAutoplay(playcount: number): void
        {
            this.log(`startAutoplay(${playcount})`);
            this._autoplay = playcount;
            if (this.proxy) { this._proxy.startAutoplay(playcount); }
        }

        public stopAutoplay(): void
        {
            this.log(`stopAutoplay()`);
            this._autoplay = null;
            if (this.proxy) { this._proxy.stopAutoplay(); }
        }

        public pauseGame(): void
        {
            this.log(`pauseGame()`);
            this._paused = true;
            if (this.proxy) { this._proxy.pauseGame(); }
        }

        public resumeGame(): void
        {
            this.log(`resumeGame()`);
            this._paused = false;
            if (this.proxy) { this._proxy.resumeGame(); }
        }

        public setOrientationLandscape(): void
        {
            this.log(`setOrientationLandscape()`);
            this._orientation = "landscape";
            if (this._proxy) { this._proxy.setOrientationLandscape(); }
        }

        public setOrientationPortrait(): void
        {
            this.log(`setOrientationPortrait()`);
            this._orientation = "portrait";
            if (this._proxy) { this._proxy.setOrientationPortrait(); }
        }

        public addWinDisplayListener(listener: SGI.Events.IWinDisplayEventListener): void
        {
            this.log(`addWinDisplayListener(${listener})`);
            if (this._proxy) { this._proxy.addWinDisplayListener(listener); }
            this._storedListeners.winDisplayListener = listener;
        }

        public addPlatformBalanceListener(listener: SGI.Events.IBalanceDisplayEventListener): void
        {
            this.log(`addPlatformBalanceListener(${listener})`);
            if (this._proxy) { this._proxy.addPlatformBalanceListener(listener); }
            this._storedListeners.balanceDisplayListener = listener;
        }

        public addGamePlayEventListener(listener: SGI.Events.IGameInitializedEventListener): void
        {
            this.log(`addGamePlayEventListener(${listener})`);
            if (this._proxy) { this._proxy.addGamePlayEventListener(listener); }
            this._storedListeners.gameInitialiseListener = listener;
        }

        public setBalanceService(balanceService: SGI.IBalanceService): void
        {
            this.log(`setBalanceService(${balanceService})`);
            this._balanceService = balanceService;
            if (this._proxy) { this._proxy.setBalanceService(balanceService); }
        }

        public jackpotBalancesUpdate(jackpots: SGI.JackpotData[]): void
        {
            this.log(`jackpotBalancesUpdate(${jackpots.length})`);
            if (this._proxy) { this._proxy.jackpotBalancesUpdate(jackpots); }
        }

        public handleFreeRoundsUpdate(info: SGI.FreeRoundsInfo): void
        {
            this.log(`handleFreeRoundsUpdate(${JSON.stringify(info)})`);
            if (this._proxy) { this._proxy.handleFreeRoundsUpdate(info); }
        }

        public gameRevealed(): void
        {
            this.log(`gameRevealed()`);
            if (this._proxy) { this._proxy.gameRevealed(); }
        }

        public handlePromoInfoUpdate(info: SGI.PromotionInfo): void
        {
            this.log(`handlePromoInfoUpdate(${JSON.stringify(info)})`);
            if (this._proxy) { this._proxy.handlePromoInfoUpdate(info); }
        }

        private setPartnerAdapterOnProxy(proxy: SGI.IGame, partnerAdapter: SGI.IPartnerAdapter): void
        {
            proxy.setPartnerAdapter(partnerAdapter);
            const leanPartnerAdapter = partnerAdapter as SGI.ILeanPartnerAdapter;
            if (validatePath(leanPartnerAdapter, "logics", "platform", "getPromoInfo"))
            {
                const promoInfo = leanPartnerAdapter.logics.platform.getPromoInfo();
                if (promoInfo)
                {
                    this.log("got promotional info");
                    proxy.handlePromoInfoUpdate(promoInfo);
                }
            }
        }

        private log(message: string): void
        {
            console.log(`[SG.EntryPoint] ${message}`);
        }
    }
}