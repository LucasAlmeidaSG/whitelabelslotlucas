namespace RS.Tests
{
    const { expect } = chai;

    describe("Platform.ts", function ()
    {
        describe("fixLocaleCode", function ()
        {
            it("should not change ca_ES", function ()
            {
                expect(SG.Environment.fixLocaleCode("ca_ES")).to.equal("ca_ES");
            });

            it("should change ca_es to ca_ES", function ()
            {
                expect(SG.Environment.fixLocaleCode("ca_es")).to.equal("ca_ES");
            });

            it("should change ca-es to ca_ES", function ()
            {
                expect(SG.Environment.fixLocaleCode("ca-es")).to.equal("ca_ES");
            });

            it("should change ca to ca_ES", function ()
            {
                expect(SG.Environment.fixLocaleCode("ca")).to.equal("ca_ES");
            });

            it("should change CA to ca_ES", function ()
            {
                expect(SG.Environment.fixLocaleCode("CA")).to.equal("ca_ES");
            });
        });

    });
}