namespace RS.AssembleStages
{
    const bootstrapperChecksumKey = "assemble_bootstrapper";
    const self = "SG.Environment";

    /**
     * Responsible for copying the JS code of modules.
     */
    export class AssembleBootstrapper extends AssembleStage.Base
    {
        private _moduleCode: Util.Map<Assembler.CodeSource>;
        private _changedModules: Module.Base[];

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            this._moduleCode = {};
            this._changedModules = [];

            const jsDir = Path.combine(settings.outputDir, "js");
            await super.execute(settings, moduleList);
            await FileSystem.createPath(jsDir);

            // Load in app.js and hash it
            const appJS = await FileSystem.readFile(Path.combine(jsDir, "app.js"));
            const appJShash = Checksum.sha256(appJS);

            // Load in thirdparty.js and hash it
            const thirdPartyJS = await FileSystem.readFile(Path.combine(jsDir, "thirdparty.js"));
            const thirdPartyJShash = Checksum.sha256(thirdPartyJS);

            const me = Module.getModule(self);
            const cacheDir = Path.combine(me.path, "build");
            const cacheID = "bootstrapper_assembled";

            if (this._changedModules.length > 0)
            {
                // Assemble bootstrapper.js
                const timer = new Timer();
                await Assembler.assembleJS(cacheDir, cacheID, Object.keys(this._moduleCode).map((k) => this._moduleCode[k]),
                {
                    withHelpers: true,
                    withSourcemaps: true,
                    minify: true
                });

                await Promise.all(this._changedModules.map((m) => m.saveChecksums()));
                Log.debug(`Assembled bootstrapper.js in ${timer}`);
            }

            const cachePath = Path.combine(cacheDir, cacheID);
            const outPath = Path.combine(jsDir, "bootstrapper");

            const outSourcePath = Path.replaceExtension(outPath, ".js");
            const outMapPath = Path.replaceExtension(outPath, ".js.map");

            await FileSystem.copyFile(Path.replaceExtension(cachePath, ".js"), outSourcePath);

            const cacheMapPath = Path.replaceExtension(cachePath, ".js.map");
            if (await FileSystem.fileExists(cacheMapPath))
            {
                await FileSystem.copyFile(cacheMapPath, outMapPath);
            }

            await JSReplace.replace(outSourcePath, outMapPath,
            {
                "{!APP_HASH!}": appJShash,
                "{!APP_FILENAME!}": Path.combine("js", "app.js"),
                "{!THIRDPARTY_HASH!}": thirdPartyJShash,
                "{!THIRDPARTY_FILENAME!}": Path.combine("js", "thirdparty.js"),
                "{!ENFORCE_HASH!}": `${EnvArgs.withIntegrityChecks.value}`
            });
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            const unit = module.getCompilationUnit(SG.BootstrapperCompilationUnit);
            if (unit == null)
            {
                if (module.checksumDB.set(bootstrapperChecksumKey, null))
                {
                    this._changedModules.push(module);
                }

                return;
            }

            // Gather module src
            let content: string, rawSourceMap: string;
            try
            {
                // Load the module's JS
                content = await FileSystem.readFile(unit.outJSFile);
                rawSourceMap = await FileSystem.readFile(unit.outMapFile);
            }
            catch (err)
            {
                // Module doesn't have any src
            }

            if (content == null || rawSourceMap == null) { return; }

            if (!module.checksumDB.set(bootstrapperChecksumKey, unit.srcChecksum))
            {
                return;
            }

            this._changedModules.push(module);

            // Parse the sourcemap and edit the paths
            const sourceMap = JSON.parse(rawSourceMap);
            sourceMap.sources = (sourceMap.sources as string[])
                .map((src) => src.replace("../bootstrapper", module.name));

            // Store
            this._moduleCode[module.name] =
            {
                name: module.name,
                content,
                sourceMap: JSON.stringify(sourceMap), sourceMapDir: Path.combine(module.path, "build")
            };
        }
    }

    export const assembleBootstrapper = new AssembleBootstrapper();
    AssembleStage.register({ after: [ code ], last: true }, assembleBootstrapper);
}