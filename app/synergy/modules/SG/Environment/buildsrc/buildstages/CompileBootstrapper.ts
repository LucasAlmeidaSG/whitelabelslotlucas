namespace RS.BuildStages
{
    /**
     * Responsible for compiling the TypeScript source code of modules.
     */
    export class CompileBootstrapper extends BuildStage.Base
    {
        /**
         * Gets if errors encountered when building a module are fatal and should end the whole process.
         * If this is false, other modules may continue to be compiled after an error has been encountered.
         */
        public get errorsAreFatal() { return true; }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            // Don't do anything unless we have source
            let unit = module.getCompilationUnit(SG.BootstrapperCompilationUnit);
            if (unit == null)
            {
                if (!await module.hasCode("bootstrapper")) { return { workDone: false, errorCount: 0 }; }
                unit = module.addCompilationUnit(SG.BootstrapperCompilationUnit, "bootstrapper");
                Module.CompilationDependencies.addModules(unit, module, SG.BootstrapperCompilationUnit);
                Module.CompilationDependencies.addThirdParty(unit, module);
            }

            // If the module has normal source, expose the bootstrapper to it
            const srcUnit = module.getCompilationUnit(Module.CompilationUnits.Source);
            if (srcUnit != null)
            {
                Module.CompilationDependencies.addModule(srcUnit, module, SG.BootstrapperCompilationUnit);
            }

            // Compile it
            const result = await Build.compileUnit(unit);
            if (result.errorCount > 0) { return result; }

            // Extern it so that the closure compiler doesn't obfuscate references to it
            const externDir = Path.combine(module.path, "externs", "generated");
            await FileSystem.createPath(externDir);
            await FileSystem.writeFile(Path.combine(externDir, `${module.name}.bootstrapper.d.ts`), await FileSystem.readFile(unit.outDTSFile));

            await module.saveChecksums();

            return { workDone: true, errorCount: 0 };
        }
    }

    export const compileBootstrapper = new CompileBootstrapper();
    BuildStage.register({ before: [ compile ] }, compileBootstrapper);
}