namespace SG.Environment
{
    interface JackpotState
    {
        seedValue: number;
        currentValue: number;
        dropValue: number;
        increment: number;
        instanceID: string;
    }

    const initialJackpotStates: JackpotState[] =
    [
        {
            seedValue: 100000 * 100,
            currentValue: 100000 * 100,
            dropValue: 500000 * 100,
            increment: 0.6 * 100,
            instanceID: ""
        },
        {
            seedValue: 25000 * 100,
            currentValue: 25000 * 100,
            dropValue: 100000 * 100,
            increment: 0.8 * 100,
            instanceID: ""
        },
        {
            seedValue: 5000 * 100,
            currentValue: 5000 * 100,
            dropValue: 25000 * 100,
            increment: 1 * 100,
            instanceID: ""
        }
    ];

    const logger = RS.Logging
        .getContext("RGS")
        .getContext("DummyPartnerAdapter");

    @RS.HasCallbacks
    export class DummyPartnerAdapter implements SGI.IPartnerAdapter, SGI.Events.IBalanceDisplayEventListener, SGI.Events.IWinDisplayEventListener
    {
        public readonly partnerAutoplayManager: SGI.IAutoplayManager = null;
        protected _game: SGI.IGame | null = null;
        protected _session: SGI.ISessionData = new DummySessionData();
        protected _lobby: SGI.ILobbyNavigationService = new DummyLobbyService();

        private _jackpotsInterval: number | null = null;
        private _jackpotsMap: RS.Map<JackpotState> | null = null;

        private get freeRoundsSpins(): number { return RS.URL.getParameterAsNumber("frspins", 4); }
        private get freeRoundsBetLevel(): number
        {
            const betLevelInt = RS.URL.getParameterAsNumber("frbet", null);
            return betLevelInt == null ? null : betLevelInt / 100;
        }

        private _freeRoundsCampaign: DummyFreeRoundsCampaign;
        private _promotionInfo: SGI.PromotionInfo;

        public constructor(game: SGI.IGame)
        {
            if (this.freeRoundsBetLevel != null)
            {
                this._freeRoundsCampaign = new DummyFreeRoundsCampaign("MyCampaign", "MyActivation", this.freeRoundsBetLevel * this.freeRoundsSpins);
                this._promotionInfo = { PROMOTIONS: { FREEROUNDS: [this._freeRoundsCampaign.promotionInfo], GAMEPLAY: "complete" } };
            }

            this.setGameInternal(game);
        }

        public setGame(game: SGI.IGame): void
        {
            logger.debug(`DummyPartnerAdapter: setGame`);
            this.setGameInternal(game);
        }

        public handleWinDisplayEvent(ev: SGI.Events.WinDisplayEventData): void
        {
            logger.debug(`DummyPartnerAdapter: handleWinDisplayEvent(${JSON.stringify(ev)})`);
        }

        public handlePlatformDisplayEvent(ev: SGI.Events.BalanceDisplayEventData): void
        {
            logger.debug(`DummyPartnerAdapter: handlePlatformDisplayEvent(${JSON.stringify(ev)})`);
        }

        public setPlatform(platform: object): void
        {
            logger.debug(`DummyPartnerAdapter: setPlatform`);
        }
        public getGameDomElement(): HTMLDivElement
        {
            logger.debug(`DummyPartnerAdapter: getGameDomElement`);
            return null;
        }
        public showError(heading: string, message: string, button: string, callback: () => void): void
        {
            logger.debug(`DummyPartnerAdapter: showError(${heading}, ${message}, ${button})`);
            callback();
        }
        public reload(): void
        {
            logger.debug(`DummyPartnerAdapter: reload`);
        }
        public goHome(): void
        {
            logger.debug(`DummyPartnerAdapter: goHome`);
        }
        public addBalanceUpdateListener(listener: SGI.Events.IBalanceUpdateEventListener): void
        {
            logger.debug(`DummyPartnerAdapter: addBalanceUpdateListener`);
        }
        public getSessionData(): SGI.ISessionData | null
        {
            logger.debug(`DummyPartnerAdapter: getSessionData`);
            return this._session;
        }
        public hideProgressBar(): void
        {
            logger.debug(`DummyPartnerAdapter: hideProgressBar`);
        }
        public showProgressBar(): void
        {
            logger.debug(`DummyPartnerAdapter: showProgressBar`);
        }
        public updatedLoadingProgress(amount: number): void
        {
            logger.debug(`DummyPartnerAdapter: updatedLoadingProgress(${amount})`);
        }

        public setMenuHandler(menuType: "SETTINGS", handler: SGI.MenuHandlerSettingsDelegate): SGI.MenuHandlerSettingsDelegate;
        public setMenuHandler(menuType: "MUTE", handler: SGI.MenuHandlerMuteDelegate): SGI.MenuHandlerMuteDelegate;

        public setMenuHandler(menuType: string, handler: SGI.MenuHandlerDelegate): SGI.MenuHandlerDelegate
        {
            logger.debug(`DummyPartnerAdapter: setMenuHandler(${menuType})`);
            return handler;
        }
        public receivedGameLogicResponse(response: XMLHttpRequest): void
        {
            logger.debug(`DummyPartnerAdapter: receivedGameLogicResponse`);
            
            if (this._freeRoundsCampaign && !this._freeRoundsCampaign.isFinished)
            {
                if (response.status === 200)
                {
                    const responseDOM = new DOMParser().parseFromString(response.responseText, "text/xml");
                    const type = responseDOM.documentElement.getAttribute("type");
                    if (type === "Logic")
                    {
                        this._freeRoundsCampaign.advance();
                        this._game.handleFreeRoundsUpdate(this._freeRoundsCampaign.freeRoundsInfo);
                    }
                }
            }
        }
        public startedPlay(): void
        {
            logger.debug(`DummyPartnerAdapter: startedPlay`);
        }
        public finishedPlay(): void
        {
            logger.debug(`DummyPartnerAdapter: finishedPlay`);
        }
        public finishedPostGameAnimations(): void
        {
            logger.debug(`DummyPartnerAdapter: finishedPostGameAnimations`);
        }
        public updateBet(bet: number): void
        {
            logger.debug(`DummyPartnerAdapter: updateBet(${bet})`);
        }
        public getLobbyNavigationService(): SGI.ILobbyNavigationService
        {
            logger.debug(`DummyPartnerAdapter: getLobbyNavigationService`);
            return this._lobby;
        }
        public getExternalHelpNavigationService(): SGI.IExternalHelpNavigationService
        {
            logger.debug(`DummyPartnerAdapter: getExternalHelpNavigationService`);
            return null;
        }
        public formatCurrency(cents: number): string
        {
            logger.debug(`DummyPartnerAdapter: formatCurrency(${cents})`);
            return cents.toFixed(2);
        }
        public gameReady(): void
        {
            logger.debug(`DummyPartnerAdapter: gameReady`);
            if (this._freeRoundsCampaign)
            {
                this._freeRoundsCampaign.start(this.freeRoundsBetLevel, this.freeRoundsSpins);
                this._game.handleFreeRoundsUpdate(this._freeRoundsCampaign.freeRoundsInfo);
            }
            this._game.gameRevealed();
        }

        public pollJackpots(currency: string, jackpotInstanceIDs: string[]): void
        {
            logger.debug(`DummyPartnerAdapter: pollJackpots("${currency}", ${jackpotInstanceIDs.length})`);
            this._jackpotsMap = {};
            for (let i = 0, l = jackpotInstanceIDs.length; i < l; ++i)
            {
                const jackpotID = jackpotInstanceIDs[i];
                const state = this._jackpotsMap[jackpotID] = { ...initialJackpotStates[i % initialJackpotStates.length] };
                state.currentValue = RS.Math.linearInterpolate(state.seedValue, state.dropValue, Math.random()) | 0;
            }
            if (this._jackpotsInterval != null)
            {
                clearInterval(this._jackpotsInterval);
                this._jackpotsInterval = null;
            }
            this._jackpotsInterval = setInterval(this.simulateJackpots, 3000, currency, jackpotInstanceIDs);
        }

        @RS.Callback
        private simulateJackpots(currency: string, jackpotInstanceIDs: string[]): void
        {
            const data: SGI.JackpotData[] = [];
            for (const jackpotID in this._jackpotsMap)
            {
                const state = this._jackpotsMap[jackpotID];
                state.currentValue += (Math.random() * state.increment)|0;
                if (state.currentValue >= state.dropValue)
                {
                    state.currentValue = state.seedValue;
                    //Give instanceID dropValue so that it actually has something to work with
                    state.instanceID = state.dropValue.toString();
                }
                data.push({
                    id: jackpotID,
                    instance: state.instanceID,
                    requestedCurrency: currency,
                    jackpotCurrency: currency,
                    balanceAmountInRequestedCurrency: state.currentValue / 100,
                    balanceAmountInJackpotCurrency: state.currentValue / 100
                });
            }
            if (this._game && this._game.jackpotBalancesUpdate) { this._game.jackpotBalancesUpdate(data); }
        }

        private setGameInternal(game: SGI.IGame): void
        {
            this._game = game;
            if (this._promotionInfo) { this._game.handlePromoInfoUpdate(this._promotionInfo); }
            this._game.addPlatformBalanceListener(this);
            this._game.addWinDisplayListener(this);
        }
    }
}