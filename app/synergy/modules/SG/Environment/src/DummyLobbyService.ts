namespace SG.Environment
{
    export class DummyLobbyService implements SGI.ILobbyNavigationService
    {
        public canGoToLobby(): boolean {
            return true;
        }

        /**
         * Returns to the lobby.
         */
        public goToLobby(): void {
            return;
        }
    }
}