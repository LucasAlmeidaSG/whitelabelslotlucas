namespace SG.Environment
{
    export class DummySessionData
    {
        /**
         * This method returns the session context.
         * Used to set the GLS GameRequest.Header[affiliate]
         *
         * ex)
         * var header = ‘<Header affiliate=”’ + partnerAdapter.getSessionData().getContextID() + ‘“ />’;
         */
        public getContextID(): string {
            return "";
        }
        /**
         * This method returns the player ID. Used to set the GLS GameRequest.Header[userID]
         *
         * ex)
         * var header = ‘<Header userID=”’ + partnerAdapter.getSessionData().getPlayerID() + ‘“ />’;
         */
        public getPlayerID(): string {
            return "";
        }
        /**
         * This method returns the initial sessionID to be used specifically for
         * setting the GameRequest[type=”Init”].Header[sessionID]
         *
         * ex)
         * var request = ‘<GameRequest type=”Init”><Header sessionID=”’ + partnerAdapter.getSessionData().getSessionID() + ‘“ /></GameRequest>”;
         *
         * Note: The Game Response contains a new sessionID.
         * The sessionID returned from each response should be used in the Header
         * when making the next GameRequest.
         */
        public getSessionID(): string {
            return "";
        }
        /**
         * This method returns an identifier for the player.
         * It is generally not used by games
         */
        public getPlayerHash(): string {
            return "";
        }
    }
}