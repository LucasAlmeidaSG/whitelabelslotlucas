/// <reference path="DummyPartnerAdapter.ts" />
/// <reference path="generated/Translations.ts" />
/// <reference path="generated/Assets.ts" />

namespace SG.Environment
{
    RS.DOM.Document.readyState.for(true).then(() => RS.IPlatform.get());

    const noBlockUI = RS.URL.getParameterAsBool("noblockui", false);

    const ntPartnerCodes =
    [
        "mocknorsktipping",     // internal test
        "em_norsktipping",      // real play production
        "em_norsktipping_fun"   // fun play production
    ];

    const localeMap: RS.Map<string> =
    {
        "bg": "bg_BG",
        "ca": "ca_ES",
        "cs": "cs_CZ",
        "da": "da_DK",
        "de": "de_DE",
        "el": "el_GR",
        "en": "en_GB",
        "es": "es_ES",
        "et": "et_EE",
        "fi": "fi_FI",
        "fr": "fr_FR",
        "hr": "hr_HR",
        "hu": "hu_HU",
        "it": "it_IT",
        "lt": "lt_LT",
        "lv": "lv_LV",
        "nl": "nl_NL",
        "no": "no_NO",
        "pl": "pl_PL",
        "pt": "pt_PT",
        "ro": "ro_RO",
        "ru": "ru_RU",
        "sk": "sk_SK",
        "sl": "sl_SI",
        "sv": "sv_SE",
        "tr": "tr_TR"
    };

    /**
     * Reformats the specified locale code to match "en_GB" format.
     * @param code
     */
    export function fixLocaleCode(code: string): string
    {
        code = code.toLowerCase();
        code = localeMap[code] || code;
        const match = /([a-z]{2})[_\-]([a-z]{2})/.exec(code);
        if (match && match.length === 3)
        {
            code = `${match[1]}_${match[2].toUpperCase()}`;
        }
        return code;
    }

    export interface FreeRoundsInfo
    {
        readonly campaignID: string;
        readonly activationID: string;
        readonly status: FreeRoundsInfo.Status;

        readonly totalBet?: number;
        readonly roundsRemaining?: number;
    }

    export namespace FreeRoundsInfo
    {
        export enum Status
        {
            /** A campaign is available, but the user has not yet activated it. */
            Pending,
            /** A campaign is available, but the user has opted to reject it. */
            Rejected,
            /** Free rounds has been chosen, but the first spin has not completed yet. */
            NotStarted,
            /** Free rounds has been chosen and the first spin has completed. */
            InProgress,
            /** There are no more free rounds remaining. */
            Completed
        }

        /** Returns whether or not Free Rounds is currently open, based on the given free rounds info. */
        export function isOpen(info: FreeRoundsInfo): boolean
        {
            return (info.status !== Status.Completed && info.status !== Status.Rejected) || info.roundsRemaining > 0;
        }

        export namespace Status
        {
            export function fromRaw(value: SGI.FreeRoundsInfo.Status): Status
            {
                switch (value)
                {
                    case "notstarted": return Status.NotStarted;
                    case "inprogress": return Status.InProgress;
                    case "completed": return Status.Completed;
                    default: throw new Error(`Unrecognised Free Rounds status "${value}"`);
                }
            }

            export function toRaw(value: Status): SGI.FreeRoundsInfo.Status
            {
                switch (value)
                {
                    case Status.Pending:
                    case Status.NotStarted:
                        return "notstarted";

                    case Status.InProgress: return "inprogress";
                    case Status.Completed: return "completed";

                    case Status.Rejected: throw new Error(`Status.Rejected has no raw equivalent and should not be passed to anything.`);
                    default: throw new Error(`${value} is not a valid FreeRoundsInfo.Status`);
                }
            }
        }
    }

    @RS.HasCallbacks
    export class Platform implements RS.IPlatform, SGI.IGame
    {
        /** Sound to be played when a partner dialog, such as the recovery dialog, is closed. */
        public static dialogCloseSound: RS.Asset.SoundReference = Assets.Sounds.Null;

        /**
         * Published when the user's balance has been updated externally.
         */
        public readonly onBalanceUpdatedExternally = RS.createEvent<number>();

        public readonly onAutoplayStarted = RS.createEvent<number>();
        public readonly onAutoplayStopped = RS.createSimpleEvent();
        public readonly onResetHard = RS.createSimpleEvent();

        /**
         * Published when new jackpot data has been received.
         */
        public readonly onJackpotsUpdated = RS.createEvent<ReadonlyArray<SGI.JackpotData>>();

        /**
         * Gets how translations are handled.
         */
        public readonly localeMode: RS.PlatformLocaleMode = RS.PlatformLocaleMode.ProvidedByGame;

        /**
         * If syndicate jackpot has been awarded trigger show jackpot message dialog on transition
         */
        public syndicatedJackpotAwarded: boolean;

        @RS.AutoDispose
        private readonly _isGameVisible = new RS.Observable(false);
        public get isGameVisible(): RS.IReadonlyObservable<boolean> { return this._isGameVisible; }

        @RS.AutoDisposeOnSet
        private _gameVisibleUILock: RS.IDisposable;

        private _settings: RS.IPlatform.Settings;
        private _partnerAdapter: SGI.IPartnerAdapter | null = null;

        private _platformMetaData: SGI.IPlatformMetaDataBundle | null = null;
        private _partnerAdapterMetaData: SGI.IPartnerAdapterMetaDataBundle | null = null;

        @RS.AutoDisposeOnSet
        private _pauseArbiterHandle: RS.IDisposable | null = null;
        @RS.AutoDisposeOnSet
        private _muteArbiterHandle: RS.IDisposable | null = null;
        private _uiEnabledArbiterHandle: RS.IDisposable | null = null;

        private _loaderProgress: number = 0.0;
        private _preloaderProgress: number = 0.0;
        private _preloaderVisible: boolean = true;

        private _winDisplayListener: SGI.Events.IWinDisplayEventListener | null = null;
        private _balanceDisplayListener: SGI.Events.IBalanceDisplayEventListener | null = null;
        private _gameInitialisedListener: SGI.Events.IGameInitializedEventListener | null = null;
        private _balanceService: SGI.IBalanceService | null = null;

        private _locale: RS.Localisation.Locale;

        private _settingsMenuDelegate: SGI.MenuHandlerSettingsDelegate;
        private _muteDelegate: SGI.MenuHandlerMuteDelegate;

        private _sessionInfo: RS.IPlatform.SessionInfo;
        public get sessionInfo() { return this._sessionInfo; }

        private _logging: boolean = false;

        private __ntOverride: boolean = false;

        @RS.AutoDispose
        private readonly _freeRoundsInfo = new RS.Observable<FreeRoundsInfo | null>(null);
        public get freeRoundsInfo(): RS.IReadonlyObservable<FreeRoundsInfo | null> { return this._freeRoundsInfo; }

        /**
         * Gets the root URL to load from.
         */
        public get baseCDNRoot()
        {
            if (metaDataBundle == null)
            {
                return "";
            }
            else
            {
                return RS.Path.combine(metaDataBundle.getCdnRoot(), "content", metaDataBundle.getGameCode());
            }
        }

        /**
         * Gets the root URL to load assets from.
         */
        public get assetsRoot() { return RS.Path.combine(this.baseCDNRoot, "assets"); }

        /**
         * Gets the root URL to load translations from.
         */
        public get translationsRoot() { return RS.Path.combine(this.baseCDNRoot, "translations"); }

        /**
         * Gets the code of the locale to use (e.g. "en_GB").
         */
        public get localeCode()
        {
            if (metaDataBundle == null)
            {
                return fixLocaleCode(RS.URL.getParameter("locale", "en_GB", this.decodeLocale));
            }
            else
            {
                return fixLocaleCode(metaDataBundle.getLocale());
            }
        }

        /**
         * Gets the code of the country the player is in.
         * May not match the country code in the locale code.
         */
        public get countryCode()
        {
            if (metaDataBundle == null)
            {
                return RS.URL.getParameter("countrycode", "GB");
            }
            else
            {
                return metaDataBundle.getCountryCode();
            }
        }

        /**
         * Gets how max win will be handled.
         */
        public get maxWinMode()
        {
            if (metaDataBundle)
            {
                return metaDataBundle.getMaxWinStatus() ? RS.PlatformMaxWinMode.GreaterThan : RS.PlatformMaxWinMode.None;
            }
            else
            {
                return RS.PlatformMaxWinMode.GreaterThan;
            }
        }

        /**
         * Gets the minimum duration of a wager (i.e. the minimum time between wagers).
         */
        public get minWagerDuration()
        {
            // Try to use betconfig SpinT
            if (metaDataBundle)
            {
                const cfg = metaDataBundle.getExtraBetConfig();
                if ("SpinT" in cfg)
                {
                    return Math.ceil(RS.as(cfg["SpinT"], RS.Is.number, 3) * 1000);
                }
            }

            // In free-play, allow URL param override
            if (this.isFreePlay)
            {
                return Math.ceil(RS.URL.getParameterAsNumber("spintime", 3) * 1000);
            }

            // Real money + no bet config - default to 3 sec
            return 3000;
        }

        public get turboModeEnabled()
        {
            if (metaDataBundle)
            {
                const cfg = metaDataBundle.getExtraBetConfig();
                if (cfg["Turbo"] === 1)
                {
                    return true;
                }
                else if (cfg["Turbo"] === 0)
                {
                    return false;
                }
                else if (this.isFreePlay)
                {
                    return RS.URL.getParameterAsBool("turbo", false);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return RS.URL.getParameterAsBool("turbo", true);
            }
        }

        public get bigBetEnabled()
        {
            if (metaDataBundle)
            {
                const cfg = metaDataBundle.getExtraBetConfig();
                return cfg["bBet"] !== 0;
            }
            else
            {
                return true;
            }
        }

        /** Gets whether or not we are currently in free-play i.e. demo mode/not playing for real money. */
        public get isFreePlay(): boolean
        {
            if (metaDataBundle)
            {
                return !metaDataBundle.isRealMoney();
            }
            else
            {
                return !RS.URL.getParameterAsBool("realmoney", false);
            }
        }

        /**
         * Gets or sets the locale object to use (the game should either read or write this, depending on localeMode).
         */
        public get locale() { return this._locale; }
        public set locale(value) { this._locale = value; }

        /**
         * Gets or sets the progress of the preload phase (0-1).
         */
        public get preloaderProgress() { return this._preloaderProgress; }
        public set preloaderProgress(value)
        {
            if (this._preloaderProgress === value) { return; }
            this._preloaderProgress = value;
            if (!this._partnerAdapter) { return; }
            this._partnerAdapter.updatedLoadingProgress(RS.Math.clamp(value, 0.0, 1.0), "Loading");
        }

        /**
         * Gets or sets the visibility of the preload bar.
         */
        public get preloaderVisible() { return this._preloaderVisible; }
        public set preloaderVisible(value)
        {
            if (this._preloaderVisible === value) { return; }
            this._preloaderVisible = value;
            if (!this._partnerAdapter) { return; }
            if (value)
            {
                this._partnerAdapter.showProgressBar(false, false);
            }
            else
            {
                this._partnerAdapter.hideProgressBar();
            }
        }

        public get loaderProgress() { return this._loaderProgress; }
        public set loaderProgress(value) { this._loaderProgress = value; }

        /**
         * Gets the URL of the engine endpoint, or null to use the default.
         */
        public get engineEndpointOverride()
        {
            if (metaDataBundle == null)
            {
                return null;
            }
            else
            {
                return metaDataBundle.getLogicUrl();
            }
        }

        /**
         * Gets a generic adapter object for the platform.
         * The exact type and value of this will depend on the concrete platform implementation and should not be relied upon by user code.
         */
        public get adapter() { return this._partnerAdapter; }

        /**
         * Gets how network errors should be handled.
         */
        public get networkErrorHandlingMode()
        {
            if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return RS.PlatformMessageHandlingMode.Display;
            }
            else
            {
                return RS.PlatformMessageHandlingMode.Ignore;
            }
        }

        /**
         * Gets how general errors should be handled.
         */
        public get generalErrorHandlingMode()
        {
            if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return RS.PlatformMessageHandlingMode.Display;
            }
            else
            {
                return RS.PlatformMessageHandlingMode.PassToPlatform;
            }
        }

        /**
         * Gets how recovery messages should be handled.
         */
        public get recoveryMessageHandlingMode()
        {
            if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return RS.PlatformMessageHandlingMode.Display;
            }
            else
            {
                return RS.PlatformMessageHandlingMode.PassToPlatform;
            }
        }

        /**
         * Gets how internet connection lost should be handled.
         */
        public get connectionLostHandlingMode()
        {
            if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return RS.PlatformConnectionLostMode.PauseWithMessage;
            }
            else
            {
                return RS.PlatformConnectionLostMode.Ignore;
            }
        }

        /**
         * Gets how settings menus should be handled.
         */
        public get settingsHandlingMode()
        {
            if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return RS.PlatformSettingsHandlingMode.ProvidedByGame;
            }
            else
            {
                return RS.PlatformSettingsHandlingMode.ProvidedByPlatform;
            }
        }

        /**
         * Gets if the platform supports the "navigate to lobby" function.
         */
        public get canNavigateToLobby()
        {
            const navService = this._partnerAdapter.getLobbyNavigationService();
            return navService && navService.canGoToLobby();
        }

        /**
         * Gets if the platform supports opening a settings menu.
         */
        public get canAccessSettings()
        {
            // TODO: This
            return false;
        }

        /**
         * Gets if the platform supports the "navigate to external help" function.
         */
        public get canNavigateToExternalHelp()
        {
            // Partner adapter implementation of this is apparently incorrect, so we always enable external help
            // const navService = this._partnerAdapter.getExternalHelpNavigationService();
            // return navService && navService.canGoToExternalHelp();
            return true;
        }

        /**
         * Gets if the game should display generic dev tools or not.
         */
        public get shouldDisplayDevTools()
        {
            if (metaDataBundle)
            {
                return metaDataBundle.isGaffingEnabled() || metaDataBundle.isDemoEnabled() || metaDataBundle.isDebugEnabled();
            }
            else if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
         * Gets if the game should display the demo tool or not.
         */
        public get shouldDisplayDemoTool()
        {
            if (metaDataBundle)
            {
                return metaDataBundle.isDemoEnabled();
            }
            else if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
         * Gets if the game should display the gaffing tool or not.
         */
        public get shouldDisplayGaffingTool()
        {
            if (metaDataBundle)
            {
                return metaDataBundle.isGaffingEnabled();
            }
            else if (this._partnerAdapter instanceof DummyPartnerAdapter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
         * Gets the partner code
         */
        public get partnerCode()
        {
            if (this._platformMetaData)
            {
                return this._platformMetaData.getPartnerCode();
            }
            else
            {
                return RS.URL.getParameter("partnercode");
            }
        }

        /**
         * Gets the external authorisation URL
         */
        public get externalAuthUrl()
        {
            if (this._platformMetaData)
            {
                return this._platformMetaData.getExternalAuthUrl();
            }
            else
            {
                return null;
            }
        }

        /**
         * Gets if the game needs to load its NT variant
         */
        public get isNT()
        {
            return this.__ntOverride ? this.__ntOverride : ntPartnerCodes.indexOf(this.partnerCode) !== -1;
        }

        public constructor()
        {
            this._logging = RS.URL.getParameterAsBool("dev");
            this.log("Using SG platform");

            if (SG && SG.EntryPoint)
            {
                if (SG.entryPoint)
                {
                    SG.entryPoint.proxy = this;
                }
                else
                {
                    SG.entryPointReadyCallback = () => SG.entryPoint.proxy = this;
                }
            }
            else
            {
                this.log("SG environment not detected, using dummy partner adapter");
                RS.DOM.Document.readyState.for(true).then(() =>
                {
                    this.setPartnerAdapter(new DummyPartnerAdapter(this));
                });
            }
        }

        /**
         * Initialises the platform.
         * This is called when the game class has been initialised.
         */
        public init(settings: RS.IPlatform.Settings): void
        {
            this._settings = settings;
            this.__ntOverride = RS.Storage.load("isNT", false, true);

            this.hideHamburger();
            this.hideClockAndSessionTimer();

            this.handleGameVisibleChanged(this._isGameVisible.value);
            this._isGameVisible.onChanged(this.handleGameVisibleChanged);

            // Lift rs-w to the game container
            const gameContainer = document.getElementById("game-container");
            if (gameContainer)
            {
                gameContainer.appendChild(RS.DOM.Component.wrapper.element);
            }
        }

        /**
         * Handles a network error.
         */
        public async handleNetworkError(error: Error | string) { return; }

        /**
         * Handles a general error.
         */
        public async handleGeneralError(error: Error | string)
        {
            this.log(`handleGeneralError(${error})`);
            await this.showError(Translations.Error.Title.get(this._locale), error.toString());
        }

        /**
         * Handles a recovery message.
         */
        public async handleRecoveryMessage()
        {
            await this.showError(
                Translations.Recovery.Title.get(this._locale),
                Translations.Recovery.Message.get(this._locale),
                Translations.Recovery.Button.get(this._locale)
            );
        }

        /**
         * Handles a general message.
         */
        public async handleGeneralMessage(message: string, title?: string)
        {
            await this.showError(
                title || Translations.GeneralMessage.Title.get(this._locale),
                message,
                Translations.GeneralMessage.Button.get(this._locale)
            );
        }

        public navigateToLobby(): void
        {
            // const navService = this._partnerAdapter.getLobbyNavigationService();
            // if (navService) { navService.goToLobby(); }
            this._partnerAdapter.goHome();
        }

        public close()
        {
            window.close();
        }

        public reload()
        {
            location.reload();
        }

        public openSettingsMenu(): void
        {
            if (this._settingsMenuDelegate)
            {
                this._settingsMenuDelegate({ open: true });
            }
        }
        public async navigateToExternalHelp(externalHelpData?: RS.Map<any>): Promise<void>
        {
            const analytics = RS.Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Game",
                    action: "OpenExternalHelp"
                });
            }

            // Create the localeCode from the platform as this is updated if a fallback locale is used.
            // force no_NO if NT
            const localeCode: string = this.isNT ? "no_NO" : this.locale.languageCode + "_" + this.locale.countryCode;

            const standardPath = RS.Path.combine(this.baseCDNRoot, "help/help.html");
            const templatePath = RS.Path.combine(this.baseCDNRoot, `help/${localeCode}.html`);

            let useTemplate: boolean;
            try
            {
                useTemplate = !await RS.URL.exists(standardPath);
            }
            catch (error)
            {
                // Abort but leave game intact
                RS.Log.error(error);
                return;
            }

            let helpPageURL = useTemplate ? templatePath : RS.URL.setParameters({ "language": `${localeCode}` }, standardPath);

            // Set NT param
            helpPageURL = RS.URL.setParameters({ "isNT": this.isNT }, helpPageURL);

            const countryCode = this.countryCode;
            if (countryCode != null) { helpPageURL = RS.URL.setParameters({ "countrycode": countryCode }, helpPageURL); }

            if (this.isNT) { helpPageURL = RS.URL.setParameters({ "isNT": this.isNT }, helpPageURL); }

            helpPageURL = RS.URL.setParameters(externalHelpData, helpPageURL);

            window.open(helpPageURL, "", "resizable=1, scrollbars, width=750, height=500");
        }

        public handleWinDisplay(winAmount: number)
        {
            if (this._winDisplayListener)
            {
                this._winDisplayListener.handleWinDisplayEvent({ winAmount });
            }
        }

        public handleBalanceDisplay(cash: number, freebet: number, balance: number)
        {
            if (this._balanceDisplayListener)
            {
                this._balanceDisplayListener.handlePlatformDisplayEvent({ CASH: cash, FREEBET: freebet, balanceAmount: balance });
            }
        }

        public handleGameInitialised(resume: boolean, cash: number, freebet: number, balance: number)
        {
            if (this._gameInitialisedListener)
            {
                this._gameInitialisedListener.handleGameInitializedEvent({ isResumeGame: resume, CASH: cash, FREEBET: freebet, balanceAmount: balance });
            }
        }

        public initialiseJackpots(currencyCode: string, jackpotInstanceIDs: (string | number)[]): void
        {
            if (this._partnerAdapter.pollJackpots)
            {
                this._partnerAdapter.pollJackpots(currencyCode, jackpotInstanceIDs);
            }
            else
            {
                RS.Log.warn(`Game requested jackpots but partner adapter does not have pollJackpots`);
            }
        }

        public showJackpotMessageDialog()
        {
            if (this._partnerAdapter.showJackpotMessageDialog)
            {
                this._partnerAdapter.showJackpotMessageDialog();
            }
            else
            {
                RS.Log.warn(`Game requested jackpots but partner adapter does not have showJackpotMessageDialog`);
            }
        }

        //#region SGI.IGame
        public setPartnerAdapter(partnerAdapter: SGI.IPartnerAdapter): void
        {
            if (this._partnerAdapter === partnerAdapter) { return; }

            if (window["glsplatform"] && glsplatform.MetaDataBundle)
            {
                this._platformMetaData = new glsplatform.MetaDataBundle();
            }
            if (window["leanpartneradapter"] && leanpartneradapter.MetaDataBundle)
            {
                this._partnerAdapterMetaData = new leanpartneradapter.MetaDataBundle();
            }

            this.log(`setPartnerAdapter(${partnerAdapter})`);
            this._partnerAdapter = partnerAdapter;
            this._muteDelegate = partnerAdapter.setMenuHandler("MUTE", this.handleMuteMenuCallback);
            this._settingsMenuDelegate = partnerAdapter.setMenuHandler("SETTINGS", this.handleSettingsMenuCallback);
            partnerAdapter.addBalanceUpdateListener({
                balanceNotificationFromPartnerAdapter: this.balanceNotificationFromPartnerAdapter,
                handleSetBalance: this.handleSetBalance
            });

            // this is a bit hacky but basically it's possible for us to be in the constructor of Platform when this is called, and we don't want to trigger inits here, so delay it a bit
            // we can't use ticker because ticker probably hasn't been started yet
            setTimeout(() => RS.triggerInits(), 0);
        }
        public resetHard(): void
        {
            this.log(`resetHard`);
            this.onResetHard.publish();
        }
        public startAutoplay(playCount: number): void
        {
            this.log(`startAutoplay(${playCount})`);
            this.onAutoplayStarted.publish(playCount);
            const analytics = RS.Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Game",
                    action: "StartAutoplay",
                    value: playCount
                });
            }
        }
        public stopAutoplay(): void
        {
            this.log(`stopAutoplay`);
            this.onAutoplayStopped.publish();
            const analytics = RS.Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Game",
                    action: "StopAutoplay"
                });
            }
        }
        public pauseGame(): void
        {
            this.log(`pauseGame`);
            this._pauseArbiterHandle = this._settings.pauseArbiter.declare(true);
        }
        public resumeGame(): void
        {
            this.log(`resumeGame`);
            this._pauseArbiterHandle = null;
        }
        public setOrientationLandscape(): void
        {
            this.log(`setOrientationLandscape`);
            const analytics = RS.Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Game",
                    action: "ChangeToLandscape"
                });
            }
        }
        public setOrientationPortrait(): void
        {
            this.log(`setOrientationPortrait`);
            const analytics = RS.Analytics.IAnalytics.get();
            if (analytics != null)
            {
                analytics.getTracker().event({
                    category: "Game",
                    action: "ChangeToPortrait"
                });
            }
        }
        public launchGame(): void
        {
            this.log(`launchGame`);
        }
        public addWinDisplayListener(listener: SGI.Events.IWinDisplayEventListener): void
        {
            this._winDisplayListener = listener;
            this.log(`addWinDisplayListener`);
        }
        public addPlatformBalanceListener(listener: SGI.Events.IBalanceDisplayEventListener): void
        {
            this._balanceDisplayListener = listener;
            this.log(`addPlatformBalanceListener`);
        }
        public addGamePlayEventListener(listener: SGI.Events.IGameInitializedEventListener): void
        {
            this._gameInitialisedListener = listener;
            this.log(`addGamePlayEventListener`);
        }
        public setBalanceService(balanceService: SGI.IBalanceService): void
        {
            this._balanceService = balanceService;
            this.log(`setBalanceService`);
        }
        /**
         * Signals that a response has been received to a network request.
         * This should be used for network requests sent to the engine.
         */
        public networkResponseReceived(request: XMLHttpRequest): void { return; }

        public jackpotBalancesUpdate(jackpots: SGI.JackpotData[]): void
        {
            this.log(`jackpotBalancesUpdate(${jackpots.length})`);
            this.onJackpotsUpdated.publish(jackpots);
        }

        public handlePromoInfoUpdate(info: SGI.PromotionInfo): void
        {
            this.log(`handlePromoInfoUpdate(${JSON.stringify(info)})`);
            if (info == null) { return; }

            const campaigns = this.extractFreeRoundsInfo(info);
            // Don't nullify FS info if nothing in promo info
            if (campaigns.length === 0) { return; }
            this._freeRoundsInfo.value = campaigns[0];
        }

        public handleFreeRoundsUpdate(info: SGI.FreeRoundsInfo): void
        {
            this.log(`handleFreeRoundsUpdate(${JSON.stringify(info)})`);
            this._freeRoundsInfo.value = this.processFreeRoundsInfo(info);
        }

        public gameRevealed(): void
        {
            this.log(`gameRevealed()`);
            this._isGameVisible.value = true;

            if (this._freeRoundsInfo.value && this._freeRoundsInfo.value.status === FreeRoundsInfo.Status.Pending)
            {
                this._freeRoundsInfo.value =
                {
                    ...this._freeRoundsInfo.value,
                    status: FreeRoundsInfo.Status.Rejected
                };
            }
        }

        public internalMute(mute: boolean): void
        {
            this.log(`internalMute(${mute})`);
            this._muteArbiterHandle = mute ? this._settings.muteArbiter.declare(true) : null;
        }

        //#endregion

        /**
         * Displays an error. Returns a promise that resolves when the button is clicked on the error.
         * @param heading
         * @param message
         * @param button
         */
        protected showError(heading: string, message: string, button: string = Translations.Error.Button.get(this._locale)): Promise<void>
        {
            return new Promise((resolve, reject) =>
            {
                this._partnerAdapter.showError(heading, message, button, () =>
                {
                    RS.Audio.play(Platform.dialogCloseSound);
                    resolve();
                });
            });
        }

        protected fetchBalance(): PromiseLike<number>
        {
            this.log(`fetchBalance started`);
            return new Promise((resolve, reject) =>
            {
                this._balanceService.fetchBalance((response) =>
                {
                    this.log(`fetchBalance succeeded (${response.statusCode}, ${response.message}, ${response.realBalance})`);
                    resolve(response.realBalance);
                }, (reason) =>
                {
                    this.log(`fetchBalance failed (${reason})`);
                    reject(reason);
                });
            });
        }

        @RS.Callback
        protected handleGameVisibleChanged(value: boolean): void
        {
            if (value)
            {
                this._gameVisibleUILock = null;
            }
            else if (!noBlockUI)
            {
                this._gameVisibleUILock = this._settings.uiEnabledArbiter.declare(false);
            }
        }

        @RS.Callback
        protected balanceNotificationFromPartnerAdapter(): void
        {
            this.fetchBalance()
                .then((newBalance) => this.onBalanceUpdatedExternally.publish(newBalance));
        }

        @RS.Callback
        protected handleSetBalance(setBalanceEvent: { getBalanceAmount: () => number; }): void
        {
            const newBalance = setBalanceEvent.getBalanceAmount();
            this.onBalanceUpdatedExternally.publish(newBalance);
        }

        private log(message: string)
        {
            if (!this._logging) { return; }
            RS.Log.debug(`[SG.Environment.Platform] ${message}`);
        }

        /**
         * Extracts free rounds campaign info from SGI.PromotionInfo.
         * Returns an array as it is somehow possible to have multiple campaigns allocated for a single player and game.
         * But Rainbow Riches only ever uses the first campaign (index 0).
         * @throws {RS.AssertionError} Input SGI info must be valid. If it is not, then we have the wrong definitions or the partner adapter is wrong. Either way, needs investigation before it causes a bigger issue.
         */
        private extractFreeRoundsInfo(promoInfo: SGI.PromotionInfo): FreeRoundsInfo[]
        {
            Validation.validatePromotionInfo(promoInfo);

            const promotions = promoInfo.PROMOTIONS;
            const results: FreeRoundsInfo[] = [];
            for (const campaign of promotions.FREEROUNDS)
            {
                const result: FreeRoundsInfo =
                {
                    activationID: campaign.ACTIVATIONID,
                    campaignID: campaign.CAMPAIGNID,
                    status: FreeRoundsInfo.Status.Pending
                };
                results.push(result);
            }

            return results;
        }

        /**
         * Converts SGI.FreeRoundsInfo into FreeRoundsInfo.
         * @throws {RS.AssertionError} Input SGI info must be valid. If it is not, then we have the wrong definitions or the partner adapter is wrong. Either way, needs investigation before it causes a bigger issue.
         */
        private processFreeRoundsInfo(info: SGI.FreeRoundsInfo): FreeRoundsInfo | null
        {
            if (info == null) { return null; }
            Validation.validateFreeRoundsInfo(info);

            // Basic FR info
            const campaignID = info.CAMPAIGNID;
            const activationID = info.ACTIVATIONID;
            const status = FreeRoundsInfo.Status.fromRaw(info.STATUS);

            // Bet restriction info
            const option = info.OPTIONS[0];

            const totalBet = RS.Math.shiftDecimal(option.BETLEVEL, 2);
            const roundsRemaining = option.REMAININGROUNDS;

            return { campaignID, activationID, status, totalBet, roundsRemaining };
        }

        /**
         * Hides the hamburger menu button.
         */
        private hideHamburger(): void
        {
            const element = document.getElementById("top-bar-menu-button");
            if (!element) { return; }
            element.style.left = "auto";
            element.style.right = "-10%";
            element.style.display = "none";
            element.style.visibility = "hidden";
        }

        /**
         * Hides the DOM for the clock and session timer.
         */
        private hideClockAndSessionTimer(): void
        {
            const elements = document.getElementsByClassName("top-bar-info");
            for (let i = 0, l = elements.length; i < l; ++i)
            {
                const element = new RS.DOM.Component(elements.item(i) as HTMLElement);
                element.css("display", "none");
                element.css("visibility", "hidden");
            }
        }

        @RS.Callback
        private handleMuteMenuCallback(checked: boolean): void
        {
            // Partner adapter notified US of mute change
            this.log(`handleMuteMenuCallback(${checked})`);
            if (checked)
            {
                if (!this._muteArbiterHandle)
                {
                    this._muteArbiterHandle = this._settings.muteArbiter.declare(true);
                    const analytics = RS.Analytics.IAnalytics.get();
                    if (analytics != null)
                    {
                        analytics.getTracker().event({
                            category: "Game",
                            action: "MuteSound"
                        });
                    }
                }
            }
            else
            {
                if (this._muteArbiterHandle)
                {
                    this._muteArbiterHandle.dispose();
                    this._muteArbiterHandle = null;
                    const analytics = RS.Analytics.IAnalytics.get();
                    if (analytics != null)
                    {
                        analytics.getTracker().event({
                            category: "Game",
                            action: "UnmuteSound"
                        });
                    }
                }
            }
        }

        @RS.Callback
        private handleSettingsMenuCallback(data: { visible?: boolean; open: boolean; }): void
        {
            this.log(`handleSettingsMenuCallback(${data.visible}, ${data.open})`);
            if (data.open)
            {
                if (!this._uiEnabledArbiterHandle)
                {
                    this._uiEnabledArbiterHandle = this._settings.uiEnabledArbiter.declare(false);
                }
            }
            else
            {
                if (this._uiEnabledArbiterHandle)
                {
                    this._uiEnabledArbiterHandle.dispose();
                    this._uiEnabledArbiterHandle = null;
                }
            }
        }

        @RS.Callback
        private decodeLocale(locale: string): string
        {
            if (locale === "ra_ND")
            {
                let locales: string[] = [];

                for (const l in localeMap)
                {
                    locales.push(l);
                }

                const index: number = RS.Math.generateRandomInt(0, locales.length);
                const randomLocale: string = localeMap[locales[index]];

                RS.Log.info(`Random locale - ${randomLocale}`);

                return randomLocale;
            }

            return locale;
        }
    }

    RS.IPlatform.register(Platform);
}
