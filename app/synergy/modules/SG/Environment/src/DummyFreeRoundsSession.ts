namespace SG.Environment
{
    export class DummyFreeRoundsCampaign
    {
        public readonly promotionInfo: SGI.PromotionInfo.FreeRoundsInfo;

        public get isFinished() { return this._isFinished; }
        private _isFinished = false;

        public get freeRoundsInfo(): SGI.FreeRoundsInfo { return this._freeRoundsInfo; }
        private _freeRoundsInfo: { -readonly [P in keyof SGI.FreeRoundsInfo]: SGI.FreeRoundsInfo[P] };

        constructor(id: string, activationID: string, value: number)
        {
            this.promotionInfo =
            {
                CAMPAIGNID: id,
                ACTIVATIONID: activationID,
                CAMPAIGNVALUE: value,
                REJECTABLE: true
            }
        }

        public start(betLevel: number, numRounds: number)
        {
            if (this._freeRoundsInfo) { throw new Error("Free Rounds campaign already started; cannot start"); }
            this._freeRoundsInfo =
            {
                CAMPAIGNID: this.promotionInfo.CAMPAIGNID,
                ACTIVATIONID: this.promotionInfo.ACTIVATIONID,
                STATUS: "notstarted",
                OPTIONS:
                [
                    {
                        BETLEVEL: betLevel,
                        REMAININGROUNDS: numRounds
                    }
                ]
            };
        }

        public advance()
        {
            if (!this._freeRoundsInfo) { throw new Error("Free Rounds campaign not started; cannot advance"); }
            const roundsLeft = --this._freeRoundsInfo.OPTIONS[0].REMAININGROUNDS;
            this._freeRoundsInfo.STATUS = roundsLeft > 1 ? "inprogress" : "completed";
            if (roundsLeft === 0) { this._isFinished = true; }
        }
    }
}