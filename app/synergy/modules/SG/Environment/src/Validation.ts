/** Validate everything at the interface with the partner adapter so that if there is some API discrepancy we know immediately. */
namespace SG.Environment.Validation
{
    export function validatePromotionInfo(info: SGI.PromotionInfo): void
    {
        // Validate container
        RS.assert(RS.Is.object(info), "promoInfo must be an object");
        RS.assert(RS.Is.object(info.PROMOTIONS), "PROMOTIONS must be an object");

        // Validate gameplay status
        RS.hope(info.PROMOTIONS.GAMEPLAY === "complete", "GAMEPLAY should be a known status value");

        // Validate campagin info
        RS.assert(RS.Is.array(info.PROMOTIONS.FREEROUNDS), "FREEROUNDS must be an array");
        info.PROMOTIONS.FREEROUNDS.forEach((campaign) => validateFreeRoundsCampaign(campaign));
    }

    function validateFreeRoundsCampaign(campaign: SGI.PromotionInfo.FreeRoundsInfo)
    {
        RS.assert(RS.Is.object(campaign), "CAMPAIGN must be an object");

        // Validate metadata
        RS.assert(RS.Is.string(campaign.ACTIVATIONID), "ACTIVATIONID must be a string");
        RS.assert(RS.Is.string(campaign.CAMPAIGNID), "CAMPAIGNID must be a string");
    }

    export function validateFreeRoundsInfo(info: SGI.FreeRoundsInfo): void
    {
        RS.assert(RS.Is.object(info), "info must be an object");

        // Validate metadata
        RS.assert(RS.Is.string(info.ACTIVATIONID), "ACTIVATIONID must be a string");
        RS.assert(RS.Is.string(info.CAMPAIGNID), "CAMPAIGNID must be a string");
        RS.assert(RS.Is.string(info.STATUS), "STATUS must be a string");

        // Validate bet options
        RS.assert(RS.Is.array(info.OPTIONS), "OPTIONS must be an array");
        RS.assert(info.OPTIONS.length > 0, "OPTIONS must be provided");
        info.OPTIONS.forEach((opt) => validateFreeRoundsOption(opt));
    }

    function validateFreeRoundsOption(option: SGI.FreeRoundsInfo.Option): void
    {
        RS.assert(RS.Is.object(option), "OPTION must be an object");

        RS.assert(RS.Is.number(option.BETLEVEL), "BETLEVEL must be a number");
        RS.assert(option.BETLEVEL > 0, "REMAININGROUNDS must be positive and non-zero");
        RS.hope(isWellFormattedNumber(option.BETLEVEL, 2), "BETLEVEL should not have more than 2 decimal places");

        RS.assert(RS.Is.integer(option.REMAININGROUNDS), "REMAININGROUNDS must be an integer");
        RS.assert(option.REMAININGROUNDS >= 0, "REMAININGROUNDS must be positive");
    }

    function isWellFormattedNumber(value: number, dp: number): boolean
    {
        const shifted = RS.Math.shiftDecimal(value, dp);
        return RS.Is.integer(shifted);
    }

    const freeRoundsExample: SGI.FreeRoundsInfo =
    {
        CAMPAIGNID: "MyCampaign",
        ACTIVATIONID: "qwertyuiop",
        STATUS: "inprogress",
        OPTIONS: [ { BETLEVEL: 200, REMAININGROUNDS: 5 } ]
    };

    const promoExample: SGI.PromotionInfo =
    {
        PROMOTIONS:
        {
            FREEROUNDS:
            [
                {
                    CAMPAIGNID: "MyCampaign",
                    ACTIVATIONID: "qwertyuiop",
                    CAMPAIGNVALUE: 50.00,
                    REJECTABLE: true
                }
            ],
            GAMEPLAY: "complete"
        }
    };
}