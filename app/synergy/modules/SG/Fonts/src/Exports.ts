namespace SG.Fonts
{
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Arial instead
     */
    export import Arial = RS.Fonts.Arial;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.AvenirNextWorld instead
     */
    export import AvenirNextWorld = RS.Fonts.AvenirNextWorld;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Bodoni instead
     */
    export import Bodoni = RS.Fonts.Bodoni;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Bookman instead
     */
    export import Bookman = RS.Fonts.Bookman;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.DIN instead
     */
    export import DIN = RS.Fonts.DIN;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Frutiger instead
     */
    export import Frutiger = RS.Fonts.Frutiger;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.FrutigerWorld instead
     */
    export import FrutigerWorld = RS.Fonts.FrutigerWorld;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Futura instead
     */
    export import Futura = RS.Fonts.Futura;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Honda instead
     */
    export import Honda = RS.Fonts.Honda;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Impact instead
     */
    export import Impact = RS.Fonts.Impact;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Kabel instead
     */
    export import Kabel = RS.Fonts.Kabel;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Really2 instead
     */
    export import Really2 = RS.Fonts.Really2;
}

namespace SG.Fonts.Fonts
{
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Arial instead
     */
    export import Arial = SG.Fonts.Arial;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.AvenirNextWorld instead
     */
    export import AvenirNextWorld = SG.Fonts.AvenirNextWorld;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Bodoni instead
     */
    export import Bodoni = SG.Fonts.Bodoni;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Bookman instead
     */
    export import Bookman = SG.Fonts.Bookman;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.DIN instead
     */
    export import DIN = SG.Fonts.DIN;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Frutiger instead
     */
    export import Frutiger = SG.Fonts.Frutiger;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.FrutigerWorld instead
     */
    export import FrutigerWorld = SG.Fonts.FrutigerWorld;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Futura instead
     */
    export import Futura = SG.Fonts.Futura;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Honda instead
     */
    export import Honda = SG.Fonts.Honda;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Impact instead
     */
    export import Impact = SG.Fonts.Impact;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Kabel instead
     */
    export import Kabel = SG.Fonts.Kabel;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Really2 instead
     */
    export import Really2 = SG.Fonts.Really2;
}

namespace SG.Fonts.Groups
{
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_arial instead
     */
    export import fonts_arial = RS.Fonts.Groups.fonts_arial;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_avenir instead
     */
    export import fonts_avenir = RS.Fonts.Groups.fonts_avenir;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_bodoni instead
     */
    export import fonts_bodoni = RS.Fonts.Groups.fonts_bodoni;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_bookman instead
     */
    export import fonts_bookman = RS.Fonts.Groups.fonts_bookman;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_din instead
     */
    export import fonts_din = RS.Fonts.Groups.fonts_din;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_frutiger instead
     */
    export import fonts_frutiger = RS.Fonts.Groups.fonts_frutiger;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_frutigerWorld instead
     */
    export import fonts_frutigerWorld = RS.Fonts.Groups.fonts_frutigerWorld;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_futura instead
     */
    export import fonts_futura = RS.Fonts.Groups.fonts_futura;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_honda instead
     */
    export import fonts_honda = RS.Fonts.Groups.fonts_honda;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_impact instead
     */
    export import fonts_impact = RS.Fonts.Groups.fonts_impact;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_kabel instead
     */
    export import fonts_kabel = RS.Fonts.Groups.fonts_kabel;
    /**
     * TODO remove in synergy 1.2
     * @deprecated use RS.Fonts.Groups.fonts_really2 instead
     */
    export import fonts_really2 = RS.Fonts.Groups.fonts_really2;
}