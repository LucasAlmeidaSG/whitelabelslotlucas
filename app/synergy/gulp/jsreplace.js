var RS = require("./gulplib.js").RS;
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var syntax = "jsreplace <*.js> <*.js.map> [a=A b=B c=C ...]";
        function doReplace(args) {
            return __awaiter(this, void 0, void 0, function () {
                var jsPath, rawJS, err_1, mapPath, rawMap, err_2, subMap, i, arg, keyVal, map, compiledFile;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            RS.Log.debug("doReplace(" + args.join(" ") + ")");
                            return [4 /*yield*/, RS.FileSystem.rebuildCache()];
                        case 1:
                            _a.sent();
                            jsPath = args[0];
                            if (!RS.Is.string(jsPath)) {
                                console.error(syntax);
                                return [2 /*return*/];
                            }
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, RS.FileSystem.readFile(jsPath)];
                        case 3:
                            rawJS = _a.sent();
                            return [3 /*break*/, 5];
                        case 4:
                            err_1 = _a.sent();
                            console.error(err_1);
                            return [2 /*return*/];
                        case 5:
                            mapPath = args[1] === "null" ? null : args[1];
                            if (mapPath && !RS.Is.string(mapPath)) {
                                console.error(syntax);
                                return [2 /*return*/];
                            }
                            rawMap = null;
                            if (!(mapPath != null)) return [3 /*break*/, 9];
                            _a.label = 6;
                        case 6:
                            _a.trys.push([6, 8, , 9]);
                            return [4 /*yield*/, RS.FileSystem.readFile(mapPath)];
                        case 7:
                            rawMap = _a.sent();
                            return [3 /*break*/, 9];
                        case 8:
                            err_2 = _a.sent();
                            console.error(err_2);
                            return [2 /*return*/];
                        case 9:
                            subMap = {};
                            for (i = 2; i < args.length; ++i) {
                                arg = args[i];
                                keyVal = arg.split("=");
                                if (keyVal.length < 2) {
                                    console.error("Invalid substitution '" + arg + "'");
                                    console.error(syntax);
                                    return [2 /*return*/];
                                }
                                subMap[keyVal[0]] = keyVal.slice(1).join("=");
                            }
                            map = rawMap == null ? null : RS.JSON.parse(rawMap);
                            compiledFile = new Build.CompiledFile(rawJS, map);
                            compiledFile.transform(function (ast) {
                                Build.AST.visit(ast, mapNode.bind(_this, subMap));
                            }, jsPath, mapPath);
                            return [4 /*yield*/, compiledFile.write(jsPath, mapPath)];
                        case 10:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        doReplace(process.argv.slice(2));
        function mapStringLiteral(map, node) {
            if (RS.Is.string(node.value)) {
                for (var key in map) {
                    if (node.value.indexOf(key) !== -1) {
                        RS.Log.debug("Replaced " + key + " with " + map[key]);
                        node.value = node.value.replace(key, map[key]);
                    }
                }
            }
        }
        function mapNode(map, node) {
            switch (node.type) {
                case "Literal":
                    mapStringLiteral(map, node);
                    break;
                case "CallExpression":
                    for (var _i = 0, _a = node.arguments; _i < _a.length; _i++) {
                        var arg = _a[_i];
                        mapNode(map, arg);
                    }
                    break;
                case "ObjectExpression":
                    for (var _b = 0, _c = node.properties; _b < _c.length; _b++) {
                        var p = _c[_b];
                        if (p.kind === "init") {
                            mapNode(map, p.value);
                        }
                    }
                    break;
            }
        }
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
