var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var RS;
(function (RS) {
    var Config;
    (function (Config) {
        var loadedConfig = null;
        Config.activeConfig = null;
        var defaultConfig = {
            rootModule: "Game",
            envArgs: {},
            extraModules: [],
            moduleConfigs: {},
            assemblePath: "build",
            defines: {},
            sourceMap: true
        };
        var configPath = "config.json";
        var userConfigPath = "userConfig.json";
        function mergeUserConfig(config) {
            return __awaiter(this, void 0, void 0, function () {
                var userConfig;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.FileSystem.fileExists(userConfigPath)];
                        case 1:
                            if (!_a.sent()) return [3 /*break*/, 3];
                            RS.Log.info("userConfig.json found, will merge with config");
                            return [4 /*yield*/, RS.JSON.parseFile(userConfigPath)];
                        case 2:
                            userConfig = _a.sent();
                            return [2 /*return*/, RS.assign(userConfig, config, true)];
                        case 3: return [2 /*return*/, config];
                    }
                });
            });
        }
        /**
         * Retrieves or loads the config.
         */
        function get() {
            return __awaiter(this, void 0, void 0, function () {
                var fileConfig;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (loadedConfig) {
                                return [2 /*return*/, loadedConfig];
                            }
                            return [4 /*yield*/, RS.FileSystem.fileExists(configPath)];
                        case 1:
                            if (!_a.sent()) return [3 /*break*/, 4];
                            return [4 /*yield*/, RS.JSON.parseFile(configPath)];
                        case 2:
                            fileConfig = _a.sent();
                            return [4 /*yield*/, mergeUserConfig(__assign(__assign({}, defaultConfig), fileConfig))];
                        case 3:
                            loadedConfig = _a.sent();
                            return [3 /*break*/, 6];
                        case 4:
                            RS.Log.warn("No config.json present, using default build configuration");
                            return [4 /*yield*/, mergeUserConfig(__assign({}, defaultConfig))];
                        case 5:
                            loadedConfig = _a.sent();
                            _a.label = 6;
                        case 6: return [2 /*return*/, loadedConfig];
                    }
                });
            });
        }
        Config.get = get;
    })(Config = RS.Config || (RS.Config = {}));
})(RS || (RS = {}));
/** Contains functions for reading strings as various types of value. */
var RS;
(function (RS) {
    var Read;
    (function (Read) {
        var truthyStrings = ["true", "1", "y", "yes", "pls"];
        var falsyStrings = ["false", "0", "n", "no", "noty"];
        /**
         * Parses the given string as an boolean.
         * @return a boolean (or null if defaultValue = null and value is not a boolean)
         */
        function boolean(value, defaultVal) {
            if (defaultVal === void 0) { defaultVal = null; }
            var lower = value.toLowerCase();
            if (truthyStrings.indexOf(lower) !== -1) {
                return true;
            }
            if (falsyStrings.indexOf(lower) !== -1) {
                return false;
            }
            return defaultVal;
        }
        Read.boolean = boolean;
        /**
         * Parses the given string as an integer.
         * @return a number (or null if defaultValue = null and value is not an integer)
         */
        function integer(value, defaultVal) {
            if (defaultVal === void 0) { defaultVal = null; }
            if (value == "") {
                return defaultVal;
            }
            var parsed = Number(value);
            return RS.as(parsed, RS.Is.integer, defaultVal);
        }
        Read.integer = integer;
        /**
         * Parses the given string as a number.
         * @return a number (or null if defaultValue = null and value is not a number)
         */
        function number(value, defaultVal) {
            if (defaultVal === void 0) { defaultVal = null; }
            if (value == "") {
                return defaultVal;
            }
            var parsed = Number(value);
            return RS.as(parsed, RS.Is.number, defaultVal);
        }
        Read.number = number;
        /**
         * Parses the given string as an object.
         * @return an object or null
         */
        function object(value) {
            var parsed = null;
            try {
                parsed = RS.JSON.parse(value);
            }
            catch ( /* Do nothing, we'll return null. */_a) { /* Do nothing, we'll return null. */ }
            return RS.as(parsed, RS.Is.object);
        }
        Read.object = object;
        /**
         * Parses the given string as an array of a specific type.
         * @return an array of T and null values
         */
        function arrayOf(value, innerReader, delimiter) {
            if (delimiter === void 0) { delimiter = ","; }
            var strings = value.split(delimiter);
            var values = strings.map(function (str) { return innerReader(str); });
            return values;
        }
        Read.arrayOf = arrayOf;
    })(Read = RS.Read || (RS.Read = {}));
})(RS || (RS = {}));
/// <reference path="Read.ts" />
var RS;
(function (RS) {
    /**
     * Represents an argument passed via an environment variable, e.g. "VAR=1 gulp build".
     */
    var EnvArg = /** @class */ (function () {
        /**
         * @param trim Whether or not to trim the value string.
         */
        function EnvArg(varName, defaultValue, trim) {
            if (trim === void 0) { trim = true; }
            this.varName = varName;
            this.defaultValue = defaultValue;
            this.trim = trim;
        }
        Object.defineProperty(EnvArg.prototype, "value", {
            get: function () {
                var raw = process.env[this.varName] || this.defaultValue;
                return this.trim ? raw.trim() : raw;
            },
            set: function (value) { process.env[this.varName] = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EnvArg.prototype, "isSet", {
            get: function () { return this.varName in process.env; },
            enumerable: true,
            configurable: true
        });
        return EnvArg;
    }());
    RS.EnvArg = EnvArg;
    /**
     * Represents a typed argument passed via an environment variable.
     */
    var TypedEnvArg = /** @class */ (function () {
        function TypedEnvArg(varName, defaultValue) {
            this._envArg = new EnvArg(varName, this.typeToString(defaultValue));
        }
        Object.defineProperty(TypedEnvArg.prototype, "varName", {
            get: function () { return this._envArg.varName; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TypedEnvArg.prototype, "defaultValue", {
            get: function () { return this.stringToType(this._envArg.defaultValue); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TypedEnvArg.prototype, "isSet", {
            get: function () { return this._envArg.isSet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TypedEnvArg.prototype, "value", {
            get: function () { return this.parseString(this._envArg.value); },
            set: function (value) { this._envArg.value = this.typeToString(value); },
            enumerable: true,
            configurable: true
        });
        TypedEnvArg.prototype.parseString = function (value) {
            var parsed = this.stringToType(value);
            if (parsed == null) {
                throw new TypeError("Could not read " + value + " as " + this.typeName + ".");
            }
            return parsed;
        };
        return TypedEnvArg;
    }());
    RS.TypedEnvArg = TypedEnvArg;
    /**
     * Represents a boolean argument passed via an environment variable, e.g. "BOOL=1 gulp build".
     * Truthy values are: "true", "1", "y", "yes"
     * Falsy values are: "false", "0", "n", "no"
     * (Case insensitive.)
     */
    var BooleanEnvArg = /** @class */ (function (_super) {
        __extends(BooleanEnvArg, _super);
        function BooleanEnvArg() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Object.defineProperty(BooleanEnvArg.prototype, "typeName", {
            get: function () { return "boolean"; },
            enumerable: true,
            configurable: true
        });
        BooleanEnvArg.prototype.typeToString = function (value) { return value.toString(); };
        BooleanEnvArg.prototype.stringToType = function (value) { return RS.Read.boolean(value); };
        return BooleanEnvArg;
    }(TypedEnvArg));
    RS.BooleanEnvArg = BooleanEnvArg;
    /**
     * Represents an integer argument passed via an environment variable, e.g. "INT=12 gulp build".
     */
    var IntegerEnvArg = /** @class */ (function (_super) {
        __extends(IntegerEnvArg, _super);
        function IntegerEnvArg() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Object.defineProperty(IntegerEnvArg.prototype, "typeName", {
            get: function () { return "number"; },
            enumerable: true,
            configurable: true
        });
        IntegerEnvArg.prototype.typeToString = function (value) { return value.toFixed(0); };
        IntegerEnvArg.prototype.stringToType = function (value) { return RS.Read.integer(value); };
        return IntegerEnvArg;
    }(TypedEnvArg));
    RS.IntegerEnvArg = IntegerEnvArg;
})(RS || (RS = {}));
/// <reference path="EnvArg.ts"/>
var RS;
(function (RS) {
    var EnvArgs;
    (function (EnvArgs) {
        EnvArgs.debugMode = new RS.BooleanEnvArg("DEBUG", false);
    })(EnvArgs = RS.EnvArgs || (RS.EnvArgs = {}));
})(RS || (RS = {}));
(function (RS) {
    var Log;
    (function (Log) {
        var gutil = require("gulp-util");
        var logContextStack = [];
        var currentContext = "Build";
        function log(contextColor, messageColor, message, contextOverride) {
            gutil.log("[" + gutil.colors[contextColor](contextOverride || currentContext) + "] " + gutil.colors[messageColor](message));
        }
        /**
         * Pushes the current context to the stack and sets the specified context as active.
         * @param context
         */
        function pushContext(context) {
            logContextStack.push(currentContext);
            currentContext = context;
        }
        Log.pushContext = pushContext;
        /**
         * Sets the specified context as inactive and restores the previous context from the stack.
         * @param context
        **/
        function popContext(context) {
            if (context !== currentContext) {
                throw new Error("Tried to pop context '" + context + "' when current context is '" + currentContext + "'");
            }
            currentContext = logContextStack.pop();
        }
        Log.popContext = popContext;
        /**
         * Logs an informative message to the console.
         * @param message
         */
        function info(message, contextOverride) {
            log("cyan", "white", message, contextOverride);
        }
        Log.info = info;
        /**
         * Logs an debug message to the console.
         * @param message
         */
        function debug(message, contextOverride) {
            log("cyan", "magenta", message, contextOverride);
        }
        Log.debug = debug;
        /**
         * Logs a warning message to the console.
         * @param message
         */
        function warn(message, contextOverride) {
            log("cyan", "yellow", message, contextOverride);
        }
        Log.warn = warn;
        /**
         * Logs an error message to the console.
         * @param message
         */
        function error(message, contextOverride) {
            log("red", "yellow", message, contextOverride);
        }
        Log.error = error;
    })(Log = RS.Log || (RS.Log = {}));
})(RS || (RS = {}));
/** Contains common type guards. */
var RS;
(function (RS) {
    var Is;
    (function (Is) {
        /**
         * Gets if the specified value is a number.
         * @param value The value to test
         */
        function number(value) {
            return value != null && !isNaN(value) && (typeof value === "number" || value instanceof Number);
        }
        Is.number = number;
        /**
         * Gets if the specified value is an integer.
         * Does not type-guard, as integer type currently impossible.
         * @param value The value to test
         */
        function integer(value) {
            return Is.number(value) && (value % 1) === 0;
        }
        Is.integer = integer;
        /**
         * Gets if the specified value is a string.
         * @param value The value to test
         */
        function string(value) {
            return value != null && (typeof value === "string" || value instanceof String);
        }
        Is.string = string;
        /**
         * Gets if the specified value is an array.
         * @param value The value to test
         */
        function array(value) {
            return value != null && Array.isArray(value);
        }
        Is.array = array;
        /**
         * Gets if the specified value is an array of a specific type.
         * @param value The value to test
         */
        function arrayOf(value, innerTester) {
            return value != null && Is.array(value) && value.every(function (el) { return innerTester(el); });
        }
        Is.arrayOf = arrayOf;
        /**
         * Gets if the specified value is an object but NOT an array.
         * @param value The value to test
         */
        function object(value) {
            return value != null && typeof value === "object" && !Array.isArray(value);
        }
        Is.object = object;
        /**
         * Gets if the specified value is a function.
         * @param value The value to test
         */
        function func(value) {
            return value != null && (typeof value === "function" || value instanceof Function);
        }
        Is.func = func;
        /**
         * Gets if the specified value is a boolean.
         * @param value The value to test
         */
        function boolean(value) {
            return value === true || value === false;
        }
        Is.boolean = boolean;
        /**
         * Gets if the specified value is a primitive.
         * @param value
         */
        function primitive(value) {
            return Is.number(value) || Is.boolean(value) || Is.string(value);
        }
        Is.primitive = primitive;
        /**
         * Gets if the specified value is a non-primitive.
         * @param value
         */
        function nonPrimitive(value) {
            return value != null && !Is.primitive(value);
        }
        Is.nonPrimitive = nonPrimitive;
    })(Is = RS.Is || (RS.Is = {}));
})(RS || (RS = {}));
(function (RS) {
    function as(value, guard, defaultValue) {
        if (defaultValue === void 0) { defaultValue = null; }
        if (guard(value)) {
            return value;
        }
        return defaultValue;
    }
    RS.as = as;
})(RS || (RS = {}));
/** Contains file path manipulation functionality. */
var RS;
(function (RS) {
    var Path;
    (function (Path) {
        var _path = require("path");
        var extEx = [".d.ts"];
        /** Returns the file part of a path. E.g. "my/test/file.txt" => "file.txt". */
        function baseName(path) { return _path.basename(path); }
        Path.baseName = baseName;
        /** Returns the directory part of a path. E.g. "my/test/file.txt" => "my/test". */
        function directoryName(path) { return _path.dirname(path); }
        Path.directoryName = directoryName;
        /** Returns the sub-extension of a path (including the dot and extension). E.g. "script.d.ts" => ".d.ts" */
        function subExtension(path) {
            var fileName = baseName(path);
            var match = fileName.match(/^(?:.*)(\..*\..*)$/);
            return match ? match[1] : "";
        }
        Path.subExtension = subExtension;
        /** Returns the extension of a path (including the dot). E.g. "my/test/file.txt" => ".txt" */
        function extension(path) {
            for (var _i = 0, extEx_1 = extEx; _i < extEx_1.length; _i++) {
                var ex = extEx_1[_i];
                if (path.substr(-ex.length) === ex) {
                    return ex;
                }
            }
            return _path.extname(path);
        }
        Path.extension = extension;
        /** Combines one or more parts of a path. */
        function combine() {
            var paths = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                paths[_i] = arguments[_i];
            }
            return _path.join.apply(_path, paths);
        }
        Path.combine = combine;
        /** Resolves any ".." and "." parts of a path. */
        function normalise(path) { return _path.normalize(path); }
        Path.normalise = normalise;
        /** Finds the relative path from "from" to "to", based on the current working directory. */
        function relative(from, to) { return _path.relative(from, to); }
        Path.relative = relative;
        /** Resolves a sequence of paths or path segments into an absolute path. */
        function resolve() {
            var paths = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                paths[_i] = arguments[_i];
            }
            return _path.resolve.apply(_path, paths);
        }
        Path.resolve = resolve;
        /** Splits a path into segments. */
        function split(path) { return path.split(_path.sep); }
        Path.split = split;
        /** Changes the extension of a path. E.g. "my/test/file.txt" => "my/test/file.json". New extension should include the dot. */
        function replaceExtension(path, newExt) {
            var ext = extension(path);
            return "" + path.substr(0, path.length - ext.length) + newExt;
        }
        Path.replaceExtension = replaceExtension;
        /** Converts the given path to be suitable for use on any main platform - strips it of whitespace, etc. */
        function sanitise(path) {
            return path.toLowerCase().replace(/\s/g, "_");
        }
        Path.sanitise = sanitise;
        /** The path seperator (e.g. "\\" on windows, "/" on osx/linux) */
        Path.seperator = process.platform === "win32" ? "\\" : "/";
    })(Path = RS.Path || (RS.Path = {}));
})(RS || (RS = {}));
/// <reference path="Log.ts" />
/// <reference path="Is.ts" />
/// <reference path="Path.ts" />
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var gulpPath = RS.Path.combine(process.cwd(), "node_modules", "gulp");
        var debugMode = false;
        var gulp = require(gulpPath);
        var baseGulpStart = gulp.start;
        var defaultTaskSettings = {
            name: "default"
        };
        var taskList = [];
        function task(p1, p2) {
            var settings = RS.Is.func(p1) ? defaultTaskSettings : p1;
            var callback = RS.Is.func(p1) ? p1 : p2;
            if (getTask(settings.name)) {
                RS.Log.warn("Duplicate task registered with the name '" + settings.name + "'");
                return;
            }
            if (debugMode) {
                RS.Log.debug("Registered task '" + settings.name + "'");
            }
            var task = { settings: settings, callback: callback, required: false, dependencies: __spreadArrays((settings.requires || [])) };
            taskList.push(task);
            return task;
        }
        Build.task = task;
        /** Declares a task to be run before any others. Can only be declared once. */
        function initTask(callback) {
            return task({ name: "init" }, callback);
        }
        Build.initTask = initTask;
        /** Identify requested gulp tasks. */
        function identifyTasks() {
            return process.argv
                .slice(2)
                .filter(function (str) { return /[a-zA-Z].*/g.test(str); });
        }
        Build.identifyTasks = identifyTasks;
        /** Exports the specified task to gulp. */
        function exportTask(task) {
            var _this = this;
            if (!task.settings.name) {
                return;
            }
            var depends = __spreadArrays((task.settings.requires || []), (task.settings.after || [])).filter(function (t) { return t.required; }).map(function (t) { return t.settings.name; });
            gulp.task(task.settings.name, depends, function () { return __awaiter(_this, void 0, void 0, function () {
                var err_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            RS.Log.pushContext(task.settings.name);
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 4, 5, 6]);
                            if (!task.callback) return [3 /*break*/, 3];
                            return [4 /*yield*/, task.callback()];
                        case 2:
                            _a.sent();
                            _a.label = 3;
                        case 3: return [3 /*break*/, 6];
                        case 4:
                            err_1 = _a.sent();
                            throw err_1;
                        case 5:
                            RS.Log.popContext(task.settings.name);
                            return [7 /*endfinally*/];
                        case 6: return [2 /*return*/];
                    }
                });
            }); });
            if (debugMode) {
                if (depends.length > 0) {
                    RS.Log.debug("Exported task '" + task.settings.name + "' (" + depends.join(", ") + ")");
                }
                else {
                    RS.Log.debug("Exported task '" + task.settings.name + "'");
                }
            }
        }
        Build.exportTask = exportTask;
        function getTask(name) {
            return taskList.filter(function (t) { return t.settings.name == name; })[0];
        }
        Build.getTask = getTask;
        function getTasks() {
            return taskList;
        }
        Build.getTasks = getTasks;
        /** Specifies that the task is required and must run. */
        function requireTask(task) {
            if (task.required) {
                return;
            }
            task.required = true;
            if (task.settings.requires) {
                for (var i = 0; i < task.settings.requires.length; i++) {
                    requireTask(task.settings.requires[i]);
                }
            }
        }
        Build.requireTask = requireTask;
        /** Exports all required tasks. */
        function exportAllTasks() {
            for (var i = 0; i < taskList.length; i++) {
                if (taskList[i].required) {
                    exportTask(taskList[i]);
                }
            }
        }
        Build.exportAllTasks = exportAllTasks;
        // Hijack gulp start method, use it to run our own initialise logic before moving on to task logic
        gulp.start = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var initTask = getTask("init");
            if (initTask != null) {
                RS.Log.pushContext("init");
                initTask.callback()
                    .then(function () {
                    RS.Log.popContext("init");
                    baseGulpStart.apply(gulp, args);
                }, function (reason) {
                    RS.Log.error(reason.stack);
                });
            }
            else {
                baseGulpStart.apply(gulp, args);
            }
        };
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Util;
    (function (Util) {
        /**
         * Gets the chain of classes that the specified class derives.
        **/
        function getInheritanceChain(obj, includeSelf) {
            if (includeSelf === void 0) { includeSelf = false; }
            var result = [];
            var proto = obj.prototype;
            if (includeSelf) {
                result.push(obj);
            }
            while ((proto = Object.getPrototypeOf(proto)) != null && proto !== Function.prototype) {
                result.push(proto.constructor);
            }
            return result;
        }
        Util.getInheritanceChain = getInheritanceChain;
        /**
         * Escapes all special regular expression characters in the string so that it may be matched in a RegExp.
         *
         * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters.
         */
        function escapeRegExpChars(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }
        Util.escapeRegExpChars = escapeRegExpChars;
        function replaceAll(haystack, p2, replacement) {
            if (RS.Is.string(p2)) {
                var re = new RegExp(escapeRegExpChars(p2), "g");
                return haystack.replace(re, replacement);
            }
            else {
                var reStr = "";
                for (var needle in p2) {
                    if (reStr.length) {
                        reStr += "|";
                    }
                    reStr += escapeRegExpChars(needle);
                }
                var re = new RegExp(reStr, "g");
                return haystack.replace(re, function (key) { return p2[key]; });
            }
        }
        Util.replaceAll = replaceAll;
        /**
         * Splits the specified string into an array of lines.
         * @param str
         */
        function splitLines(str) {
            return this._src.split(/\r?\n/g);
        }
        Util.splitLines = splitLines;
        /** Copes one error's stack trace into another. */
        function copyErrorStack(source, dest) {
            var sourceStack = source.stack;
            if (sourceStack) {
                var firstLevel = sourceStack.search(/\n\s*at/g);
                if (firstLevel !== -1) {
                    var trace = sourceStack.slice(firstLevel);
                    dest.stack = dest.name + ": " + dest.message + trace;
                }
            }
        }
        Util.copyErrorStack = copyErrorStack;
        /**
         * Copies a block of bytes from one buffer to another.
         * @param from
         * @param fromOffset
         * @param to
         * @param toOffset
         * @param length
         */
        function copyBytes(from, fromOffset, to, toOffset, length) {
            if (fromOffset + length > from.byteLength) {
                throw new Error("Length exceeds size of 'from' buffer");
            }
            if (toOffset + length > to.byteLength) {
                throw new Error("Length exceeds size of 'to' buffer");
            }
            // Copy 32 bits at a time
            var i32cnt = length >> 2;
            if (i32cnt > 0) {
                var fi32 = new Uint32Array(from, fromOffset, i32cnt);
                var ti32 = new Uint32Array(to, toOffset, i32cnt);
                var iOffset = 0;
                while (--i32cnt >= 0) {
                    ti32[iOffset] = fi32[iOffset];
                    ++iOffset;
                }
                length &= 3;
                fromOffset += fi32.byteLength;
                toOffset += ti32.byteLength;
            }
            // Copy 8 bits at a time
            var fi8 = new Uint8Array(from, fromOffset, length);
            var ti8 = new Uint8Array(to, toOffset, length);
            while (--length >= 0) {
                ti8[toOffset++] = fi8[fromOffset++];
            }
        }
        Util.copyBytes = copyBytes;
        var sizeFormats = [
            { base: Math.pow(2, 0), postfix: "B" },
            { base: Math.pow(2, 10), postfix: "KiB" },
            { base: Math.pow(2, 20), postfix: "MiB" },
            { base: Math.pow(2, 30), postfix: "GiB" }
        ];
        /**
         * Formats a size in bytes into a human friendly string (e.g. 827567 => 828 KiB)
         * @param byteSize
         */
        function formatSize(byteSize) {
            var formatIndex;
            for (formatIndex = 0; formatIndex < sizeFormats.length; ++formatIndex) {
                var sz_1 = byteSize / sizeFormats[formatIndex].base;
                if (sz_1 < 1.0) {
                    break;
                }
            }
            formatIndex--;
            var format = sizeFormats[formatIndex];
            var sz = byteSize / format.base;
            return sz.toFixed(1) + " " + format.postfix;
        }
        Util.formatSize = formatSize;
    })(Util = RS.Util || (RS.Util = {}));
})(RS || (RS = {}));
/// <reference path="../util/Is.ts" />
/// <reference path="../util/Misc.ts" />
/** RSCore Next module system. */
var RS;
(function (RS) {
    var Module;
    (function (Module) {
        /**
         * Validates the specified module info. Returns null for success, or a string containing the error.
         * @param info
         */
        function validateInfo(info) {
            if (info == null) {
                return "Module info is null";
            }
            if (!RS.Is.string(info.title)) {
                return "title is missing or invalid";
            }
            if (info.name != null && !RS.Is.string(info.name)) {
                return "name is invalid";
            }
            if (!RS.Is.string(info.description)) {
                return "description is missing or invalid";
            }
            if (!RS.Is.arrayOf(info.dependencies, RS.Is.string)) {
                return "dependencies are missing or invalid";
            }
            if (info.testdependencies != null) {
                if (!RS.Is.arrayOf(info.testdependencies, RS.Is.string)) {
                    return "test dependencies are invalid";
                }
            }
            if (info.nodedependencies != null) {
                if (!RS.Is.object(info.nodedependencies)) {
                    return "node dependencies are invalid";
                }
                for (var nodeModuleName in info.nodedependencies) {
                    if (nodeModuleName === "build") {
                        var builddependencies = info.nodedependencies.build;
                        if (!RS.Is.object(builddependencies)) {
                            return "build node dependencies are invalid";
                        }
                        for (var nodeModuleName_1 in builddependencies) {
                            if (!RS.Is.string(builddependencies[nodeModuleName_1])) {
                                return "build node dependency '" + nodeModuleName_1 + "' is invalid";
                            }
                        }
                    }
                    else {
                        if (!RS.Is.string(info.nodedependencies[nodeModuleName])) {
                            return "node dependency '" + nodeModuleName + "' is invalid";
                        }
                    }
                }
            }
            if (info.bowerdependencies != null) {
                if (!RS.Is.object(info.bowerdependencies)) {
                    return "bower dependencies are invalid";
                }
                for (var nodeModuleName in info.bowerdependencies) {
                    if (!RS.Is.string(info.bowerdependencies[nodeModuleName])) {
                        return "bower dependency '" + nodeModuleName + "' is invalid";
                    }
                }
            }
            if (info.definitions != null) {
                if (!RS.Is.object(info.definitions)) {
                    return "definitions are invalid";
                }
                for (var name_1 in info.definitions) {
                    if (!RS.Is.string(info.definitions[name_1])) {
                        return "definition '" + name_1 + "' is invalid";
                    }
                }
            }
            if (info.thirdparty != null) {
                if (!RS.Is.object(info.thirdparty)) {
                    return "third-party dependencies are invalid";
                }
                for (var name_2 in info.thirdparty) {
                    var val = info.thirdparty[name_2];
                    if (!RS.Is.object(val)) {
                        return "third-party dependency '" + name_2 + "' is invalid";
                    }
                    if (!RS.Is.string(val.src)) {
                        return "third-party dependency '" + name_2 + "' src is invalid";
                    }
                    if (val.srcmap != null && !RS.Is.string(val.srcmap)) {
                        return "third-party dependency '" + name_2 + "' srcmap is invalid";
                    }
                }
            }
            if (info.important != null && !RS.Is.boolean(info.important)) {
                return "important is invalid";
            }
            if (info.implements != null && !RS.Is.string(info.implements)) {
                return "implements is invalid";
            }
            return null;
        }
        Module.validateInfo = validateInfo;
    })(Module = RS.Module || (RS.Module = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    /** A list of items backed by a standard array. */
    var List = /** @class */ (function () {
        function List(existing) {
            if (existing != null) {
                this._data = new Array(existing.length);
                for (var i = 0, l = existing.length; i < l; ++i) {
                    this._data[i] = existing[i];
                }
            }
            else {
                this._data = [];
            }
        }
        Object.defineProperty(List.prototype, "data", {
            /** Gets the backing array for this list. */
            get: function () { return this._data; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(List.prototype, "dataCopy", {
            /** Gets a copy of the backing array for this list. */
            get: function () { return this._data.slice(); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(List.prototype, "length", {
            /** Gets the number of items in this list. */
            get: function () { return this._data.length; },
            enumerable: true,
            configurable: true
        });
        /** Returns a new list containing unique items from both lists. */
        List.union = function (a, b) {
            var newList = new List();
            newList.addRange(a, true);
            newList.addRange(b, true);
            return newList;
        };
        /** Returns a new list containing only items found in both lists. */
        List.intersect = function (a, b) {
            var newList = new List();
            newList.addRange(a.filter(function (item) { return b.contains(item); }), true);
            newList.addRange(b.filter(function (item) { return a.contains(item); }), true);
            return newList;
        };
        /** Returns a new list containing only items unique to one of the lists. */
        List.difference = function (a, b) {
            var newList = new List();
            newList.addRange(a.filter(function (item) { return !b.contains(item); }), true);
            newList.addRange(b.filter(function (item) { return !a.contains(item); }), true);
            return newList;
        };
        /** Adds an item to the end of this list. Returns the index of the added item. */
        List.prototype.add = function (item) {
            this._data.push(item);
            return this._data.length - 1;
        };
        List.prototype.addRange = function (from, avoidDuplicates) {
            if (avoidDuplicates === void 0) { avoidDuplicates = false; }
            for (var i = 0, l = from.length; i < l; ++i) {
                var item = from instanceof List ? from.get(i) : from[i];
                if (!avoidDuplicates || !this.contains(item)) {
                    this.add(item);
                }
            }
        };
        /** Inserts an item at the specified index. */
        List.prototype.insert = function (item, index) {
            this._data.splice(index, 0, item);
        };
        /** Finds the index of the given item in this list. Returns -1 if item is not found. */
        List.prototype.indexOf = function (item) {
            return this._data.indexOf(item);
        };
        /** Gets if the specified item can be found in this list. */
        List.prototype.contains = function (item) {
            return this.indexOf(item) !== -1;
        };
        /** Removes the specified item from this list. Returns false if item wasn't found. */
        List.prototype.remove = function (item) {
            var idx = this.indexOf(item);
            if (idx === -1) {
                return false;
            }
            this.removeAt(idx);
            return true;
        };
        List.prototype.removeRange = function (from) {
            for (var i = 0, l = from.length; i < l; ++i) {
                var item = from instanceof List ? from.get(i) : from[i];
                this.remove(item);
            }
        };
        /** Removes any duplicate items from this list. */
        List.prototype.removeDuplicates = function () {
            for (var i = this.length - 1; i > 0; --i) {
                var itemA = this._data[i];
                for (var j = i - 1; j >= 0; --j) {
                    var itemB = this._data[j];
                    if (itemA === itemB) {
                        this.removeAt(i);
                        break;
                    }
                }
            }
        };
        /** Removes the item at the specified index from this list. */
        List.prototype.removeAt = function (index) {
            this._data.splice(index, 1);
        };
        /** Removes ALL items from this list. */
        List.prototype.clear = function () {
            this._data.length = 0;
        };
        /** Transforms each item in this list into a new form and returns a new list of the transformed items. */
        List.prototype.map = function (functor) {
            return new List(this._data.map(functor));
        };
        List.prototype.reduce = function (functor, initialValue) {
            return arguments.length === 1 ? this._data.reduce(functor) : this._data.reduce(functor, initialValue);
        };
        /** Gets a new list containing only items within this list that passes the specified predicate. */
        List.prototype.filter = function (predicate) {
            return new List(this._data.filter(predicate));
        };
        /** Gets if all items in this list pass the specified predicate. */
        List.prototype.all = function (predicate) {
            for (var _i = 0, _a = this._data; _i < _a.length; _i++) {
                var item = _a[_i];
                if (!predicate(item)) {
                    return false;
                }
            }
            return true;
        };
        /** Finds a single item matching the specified predicate, or null if none found. */
        List.prototype.find = function (predicate) {
            for (var _i = 0, _a = this._data; _i < _a.length; _i++) {
                var item = _a[_i];
                if (predicate(item)) {
                    return item;
                }
            }
            return null;
        };
        /** Gets an item from this list. */
        List.prototype.get = function (index) { return this._data[index]; };
        /** Sets an item in this list. */
        List.prototype.set = function (index, item) { this._data[index] = item; };
        /** Moves the item at fromIndex to toIndex. */
        List.prototype.move = function (fromIndex, toIndex) {
            var item = this.get(fromIndex);
            this.removeAt(fromIndex);
            this.insert(item, toIndex);
        };
        /** Moves the item to newIndex. Returns false if the item wasn't found in this list. */
        List.prototype.setItemIndex = function (item, newIndex) {
            var idx = this.indexOf(item);
            if (idx === -1) {
                return false;
            }
            this.move(idx, newIndex);
        };
        /** Sorts this list. */
        List.prototype.sort = function (comparer) {
            this._data.sort(comparer);
        };
        /**
         * Sorts this list, ensuring that all items come before or after the set of items returned back from their respective predicates.
         * @param beforePredicate
         * @param afterPredicate
         */
        List.prototype.resolveOrder = function (beforePredicate, afterPredicate) {
            function contains(arr, item) {
                for (var i = 0, l = arr.length; i < l; ++i) {
                    if (arr[i] === item) {
                        return true;
                    }
                }
                return false;
            }
            var tmpArr = __spreadArrays(this._data);
            this._data.length = 0;
            var prevLen;
            while (this._data.length < tmpArr.length) {
                prevLen = this._data.length;
                for (var _i = 0, tmpArr_1 = tmpArr; _i < tmpArr_1.length; _i++) {
                    var item = tmpArr_1[_i];
                    if (this.contains(item)) {
                        continue;
                    }
                    var before = beforePredicate(item);
                    var after = afterPredicate(item);
                    var arrHasEverything = true;
                    var maxIdx = tmpArr.length, minIdx = 0;
                    for (var i = 0, l = before.length; i < l; ++i) {
                        var idx = void 0;
                        if ((idx = this.indexOf(before[i])) === -1) {
                            arrHasEverything = false;
                            break;
                        }
                        maxIdx = Math.min(maxIdx, idx);
                    }
                    for (var i = 0, l = after.length; i < l; ++i) {
                        var idx = void 0;
                        if ((idx = this.indexOf(after[i])) === -1) {
                            arrHasEverything = false;
                            break;
                        }
                        minIdx = Math.max(minIdx, idx + 1);
                    }
                    if (!arrHasEverything) {
                        continue;
                    }
                    if (maxIdx < minIdx) {
                        RS.Log.error("Failed to resolveOrder on list (impossible situation?)");
                        return;
                    }
                    this.insert(item, minIdx);
                }
                if (prevLen === this._data.length) {
                    RS.Log.error("Failed to resolveOrder on list (circular reference?)");
                    return;
                }
            }
            // this._data.sort((a, b) =>
            // {
            //     const aBefore = beforePredicate(a); // items that must come before a
            //     const aAfter = afterPredicate(a); // items that must come after a
            //     const bBefore = beforePredicate(b); // items that must come before b
            //     const bAfter = afterPredicate(b); // items that must come after b
            //     // if a falls in bBefore OR b falls in aAfter
            //     if (contains(bBefore, a) || contains(aAfter, b))
            //     {
            //         // a < b
            //         return 1;
            //     }
            //     // if b falls in aBefore OR a falls in bAfter
            //     else if (contains(aBefore, b) || contains(bAfter, a))
            //     {
            //         // a > b
            //         return -1;
            //     }
            //     else
            //     {
            //         // a == b
            //         return 0;
            //     }
            // });
        };
        List.prototype.toString = function () {
            return this._data.toString();
        };
        return List;
    }());
    RS.List = List;
})(RS || (RS = {}));
var RS;
(function (RS) {
    /** Timer used for profiling. */
    var Timer = /** @class */ (function () {
        function Timer() {
            this._elapsed = 0;
            this._running = true;
            this._last = Timer.now;
        }
        Object.defineProperty(Timer, "now", {
            /** Gets the current timestamp. */
            get: function () {
                var hrtime = process.hrtime();
                return (hrtime[0] + hrtime[1] / 1e9) * 1000.0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Timer.prototype, "running", {
            /** Gets if this timer is running. */
            get: function () { return this._running; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Timer.prototype, "elapsed", {
            /** Gets how long this timer has been running for (ms). */
            get: function () {
                this.update();
                return this._elapsed;
            },
            enumerable: true,
            configurable: true
        });
        /** Starts this timer. */
        Timer.prototype.start = function () {
            if (this._running) {
                return;
            }
            this._last = Timer.now;
            this._running = true;
        };
        /** Stops this timer and resets it. */
        Timer.prototype.stop = function () {
            this._running = false;
            this._elapsed = 0;
        };
        /** Pauses this timer. */
        Timer.prototype.pause = function () {
            this.update();
            this._running = false;
        };
        /** Consumes any pending time into elapsed. */
        Timer.prototype.update = function () {
            if (!this._running) {
                return;
            }
            var now = Timer.now;
            var delta = now - this._last;
            this._last = now;
            this._elapsed += delta;
        };
        /** Gets a human-friendly representaton of this timer's total elapsed time in seconds. */
        Timer.prototype.toString = function () {
            return Math.round(this.elapsed / 10.0) / 100.0 + " s";
        };
        return Timer;
    }());
    RS.Timer = Timer;
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Module;
    (function (Module) {
        var CompilationDependencies;
        (function (CompilationDependencies) {
            /**
             * Adds dependencies on the gulp file to the specified compilation unit.
             * @param unit
             */
            function addGulp(unit, module) {
                var moduleFilename = RS.Path.relative(module.path, __filename);
                unit.dependencies.add({
                    kind: RS.Build.CompilationUnit.DependencyKind.ThirdParty,
                    name: "gulplib",
                    sourceFile: moduleFilename,
                    sourceMap: RS.Path.replaceExtension(moduleFilename, ".js.map"),
                    definitionsFile: RS.Path.replaceExtension(moduleFilename, ".d.ts"),
                    definitionsMapFile: RS.Path.replaceExtension(moduleFilename, ".d.ts.map")
                });
                unit.dependencies.add({
                    kind: RS.Build.CompilationUnit.DependencyKind.DefinitionsFolder,
                    name: "gulpdefs",
                    path: RS.Path.combine(__dirname, "definitions")
                });
            }
            CompilationDependencies.addGulp = addGulp;
            function addUnit(unit, depUnit) {
                unit.dependencies.add({
                    kind: RS.Build.CompilationUnit.DependencyKind.CompilationUnit,
                    name: depUnit.name,
                    compilationUnit: depUnit
                });
                unit.dependencies.add({
                    kind: RS.Build.CompilationUnit.DependencyKind.DefinitionsFolder,
                    name: depUnit.name + ".Dependencies",
                    path: RS.Path.combine(depUnit.path, "dependencies")
                });
            }
            CompilationDependencies.addUnit = addUnit;
            /**
             * Adds dependencies on the compilation unit of "unitName" on a single module to the specified compilation unit.
             * @param unit
             * @param module
             */
            function addModule(unit, module, unitName) {
                var depUnit = module.getCompilationUnit(unitName);
                if (!depUnit) {
                    return;
                }
                addUnit(unit, depUnit);
            }
            CompilationDependencies.addModule = addModule;
            /**
             * Adds dependencies on all compilation units of "unitName" on all dependencies of a module to the specified compilation unit.
             * @param unit
             * @param module
             */
            function addModules(unit, module, unitName) {
                for (var _i = 0, _a = module.dependencies; _i < _a.length; _i++) {
                    var dep = _a[_i];
                    addModule(unit, dep, unitName);
                }
            }
            CompilationDependencies.addModules = addModules;
            /**
             * Adds dependencies on all node module dependencies of a module.
             * Optionally supports sub-maps within the node module dependencies, e.g. "build".
             * @param unit
             * @param module
             * @param subName
             */
            function addNPM(unit, module, subName) {
                if (!module.info.nodedependencies) {
                    return;
                }
                var depsMap = (subName ? module.info.nodedependencies[subName] : module.info.nodedependencies);
                if (!depsMap) {
                    return;
                }
                for (var name_3 in depsMap) {
                    var val = depsMap[name_3];
                    if (RS.Is.string(val)) {
                        unit.dependencies.add({
                            kind: RS.Build.CompilationUnit.DependencyKind.NodeModule,
                            name: "npm:" + name_3,
                            moduleName: name_3,
                            moduleVersion: depsMap[name_3]
                        });
                    }
                }
            }
            CompilationDependencies.addNPM = addNPM;
            /**
             * Adds dependencies on all bower package dependencies of a module.
             * @param unit
             * @param module
             */
            function addBower(unit, module) {
                if (module.info.bowerdependencies) {
                    for (var name_4 in module.info.bowerdependencies) {
                        unit.dependencies.add({
                            kind: RS.Build.CompilationUnit.DependencyKind.BowerPackage,
                            name: "bower:" + name_4,
                            packageName: name_4,
                            packageVersion: module.info.bowerdependencies[name_4]
                        });
                    }
                }
            }
            CompilationDependencies.addBower = addBower;
            /**
             * Adds dependencies on all third party dependencies of a module.
             * @param unit
             * @param module
             */
            function addThirdParty(unit, module) {
                var thirdPartyMap = {};
                if (module.info.thirdparty) {
                    for (var name_5 in module.info.thirdparty) {
                        var thirdParty = module.info.thirdparty[name_5];
                        if (thirdPartyMap[name_5]) {
                            thirdPartyMap[name_5].sourceFile = thirdParty.src;
                            thirdPartyMap[name_5].sourceMap = thirdParty.srcmap;
                        }
                        else {
                            thirdPartyMap[name_5] =
                                {
                                    kind: RS.Build.CompilationUnit.DependencyKind.ThirdParty,
                                    name: name_5,
                                    sourceFile: thirdParty.src,
                                    sourceMap: thirdParty.srcmap
                                };
                        }
                    }
                }
                if (module.info.definitions) {
                    for (var name_6 in module.info.definitions) {
                        var definition = module.info.definitions[name_6];
                        if (thirdPartyMap[name_6]) {
                            thirdPartyMap[name_6].definitionsFile = definition;
                        }
                        else {
                            thirdPartyMap[name_6] =
                                {
                                    kind: RS.Build.CompilationUnit.DependencyKind.ThirdParty,
                                    name: name_6,
                                    definitionsFile: definition
                                };
                        }
                    }
                }
                for (var name_7 in thirdPartyMap) {
                    unit.dependencies.add(thirdPartyMap[name_7]);
                }
            }
            CompilationDependencies.addThirdParty = addThirdParty;
        })(CompilationDependencies = Module.CompilationDependencies || (Module.CompilationDependencies = {}));
    })(Module = RS.Module || (RS.Module = {}));
})(RS || (RS = {}));
/// <reference path="Info.ts" />
/// <reference path="../util/List.ts" />
/// <reference path="../util/Timer.ts" />
/// <reference path="CompilationDependencies.ts" />
var RS;
(function (RS) {
    var Module;
    (function (Module) {
        function Task(taskSettings) {
            return function (target, propertyKey, descriptor) {
                target._tasks = target._tasks || [];
                target._tasks.push({ function: target[propertyKey], taskSettings: taskSettings });
            };
        }
        /** Names of standard compilation units. */
        var CompilationUnits;
        (function (CompilationUnits) {
            CompilationUnits["Source"] = "src";
            CompilationUnits["BuildSource"] = "buildsrc";
        })(CompilationUnits = Module.CompilationUnits || (Module.CompilationUnits = {}));
        /**
         * Represents a module.
         */
        var Base = /** @class */ (function () {
            function Base(name, info, path) {
                this._important = 0;
                this._name = name;
                this._info = info;
                this._path = path;
                this._inited = false;
                this._checksumDB = new RS.Checksum.DB();
                this._compilationUnits = {};
                this.exportTasks();
            }
            Object.defineProperty(Base, "exportedTasks", {
                /** Gets all tasks exported by the base module. */
                get: function () { return this.prototype._tasks; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "info", {
                /** Gets the moduleinfo for this module. */
                get: function () { return this._info; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "name", {
                /** Gets the name of this module. */
                get: function () { return this._name; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "path", {
                /** Gets the path of folder that holds this module. */
                get: function () { return this._path; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "initialised", {
                /** Gets if this module has been initialised. */
                get: function () { return this._inited; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "checksumDB", {
                /** Gets the checksum database for this module. */
                get: function () { return this._checksumDB; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "dependencies", {
                /** Gets all dependencies of this module. */
                get: function () { return this._dependencies; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "buildLoaded", {
                /** Gets if the build component of this module has been loaded. */
                get: function () { return this._buildLoaded; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "important", {
                /**
                 * Gets or sets the importance of this module.
                 * This determines how to sort it within any dependency sets that include it.
                 * Higher values means the module will be "floated" as high as possible within the set.
                 */
                get: function () { return this._important; },
                set: function (value) {
                    this._important = value;
                    for (var _i = 0, _a = this._dependencies; _i < _a.length; _i++) {
                        var module_1 = _a[_i];
                        module_1.important = Math.max(module_1.important, value);
                    }
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Gets a named compilation unit for this module.
             * @param name
             */
            Base.prototype.getCompilationUnit = function (name) {
                return this._compilationUnits[name] || null;
            };
            /**
             * Returns whether or not this module has code at the given path.
             * Used to check whether or not this module should have a particular compilation unit.
             * @param srcPath Path to the folder that holds tsconfig.json, relative to the module's root path.
             */
            Base.prototype.hasCode = function (srcPath) {
                return __awaiter(this, void 0, void 0, function () {
                    var fullPath, files;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                fullPath = RS.Path.combine(this._path, srcPath);
                                return [4 /*yield*/, RS.FileSystem.readFiles(fullPath)];
                            case 1:
                                files = _a.sent();
                                if (files.some(function (f) { return RS.Path.extension(f) === ".ts"; })) {
                                    return [2 /*return*/, true];
                                }
                                return [2 /*return*/, false];
                        }
                    });
                });
            };
            /**
             * Adds a new named compilation unit to this module.
             * Should only be called if the module actually has code for the given compilation unit.
             * Use Module.hasCode(srcPath) to check if this is the case.
             * @param name
             * @param srcPath Path to the folder that holds tsconfig.json, relative to the module's root path.
             */
            Base.prototype.addCompilationUnit = function (name, srcPath, defaultTSConfig) {
                if (this._compilationUnits[name]) {
                    RS.Log.warn("Tried to add the compilation unit '" + name + "' to a module when it's already been added");
                    return this._compilationUnits[name];
                }
                var unit = new RS.Build.CompilationUnit();
                unit.name = this._name + "." + name;
                unit.module = this;
                unit.path = RS.Path.combine(this._path, srcPath);
                if (defaultTSConfig) {
                    unit.defaultTSConfig = defaultTSConfig;
                }
                this._compilationUnits[name] = unit;
                return unit;
            };
            /**
             * Initialises this module. Async.
             */
            Base.prototype.init = function (dependencies) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: 
                            // Load checksum database
                            return [4 /*yield*/, this._checksumDB.loadFromFile(RS.Path.combine(this._path, "build", "checksums.json"))];
                            case 1:
                                // Load checksum database
                                _a.sent();
                                // Initialise
                                this._dependencies = dependencies;
                                this._inited = true;
                                return [4 /*yield*/, this.hasCode("src")];
                            case 2:
                                // Setup standard compilation units
                                if (_a.sent()) {
                                    this.setupSourceCompilationUnit();
                                }
                                return [4 /*yield*/, this.hasCode("buildsrc")];
                            case 3:
                                if (_a.sent()) {
                                    this.setupBuildSourceCompilationUnit();
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Gets the checksum for the specified sub-directory.
             * @param subPath
             */
            Base.prototype.getSourceChecksum = function (subPath) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, _b;
                    var _this = this;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _b = (_a = RS.Checksum).getComposite;
                                return [4 /*yield*/, RS.FileSystem.readFiles(RS.Path.combine(this._path, subPath))];
                            case 1: return [4 /*yield*/, _b.apply(_a, [(_c.sent())
                                        .filter(function (str) { return !_this.shouldExcludeFromSourceChecksum(str); })])];
                            case 2: return [2 /*return*/, _c.sent()];
                        }
                    });
                });
            };
            /**
             * Gathers a sorted list of all dependent modules of this module.
             * Travels up the dependency hierarchy.
             */
            Base.prototype.gatherDependencySet = function () {
                var list = new RS.List();
                this.gatherDependencySetInternal(list);
                return list;
            };
            /**
             * Exports all tasks for this module to the task system.
             */
            Base.prototype.exportTasks = function () {
                var _this = this;
                var tasks = Base.exportedTasks;
                if (tasks != null) {
                    var _loop_1 = function (task) {
                        RS.Build.task(__assign(__assign({}, task.taskSettings), { name: this_1.name + "." + task.taskSettings.name }), function () { return __awaiter(_this, void 0, void 0, function () {
                            var workDone, deps, _i, _a, dep, promiseMaybe, workDoneMaybe;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        workDone = false;
                                        deps = this.gatherDependencySet();
                                        _i = 0, _a = deps.data;
                                        _b.label = 1;
                                    case 1:
                                        if (!(_i < _a.length)) return [3 /*break*/, 5];
                                        dep = _a[_i];
                                        RS.Log.pushContext(dep.name);
                                        promiseMaybe = task.function.call(dep);
                                        if (!(promiseMaybe instanceof Promise)) return [3 /*break*/, 3];
                                        return [4 /*yield*/, promiseMaybe];
                                    case 2:
                                        workDoneMaybe = _b.sent();
                                        if (RS.Is.boolean(workDoneMaybe)) {
                                            workDone = workDone || workDoneMaybe;
                                        }
                                        _b.label = 3;
                                    case 3:
                                        RS.Log.popContext(dep.name);
                                        _b.label = 4;
                                    case 4:
                                        _i++;
                                        return [3 /*break*/, 1];
                                    case 5:
                                        if (!workDone) {
                                            RS.Log.info("No changes detected, no work to do!");
                                        }
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                    };
                    var this_1 = this;
                    for (var _i = 0, tasks_1 = tasks; _i < tasks_1.length; _i++) {
                        var task = tasks_1[_i];
                        _loop_1(task);
                    }
                }
            };
            /**
             * Compiles the build component of this module, if it has one.
             */
            Base.prototype.compileBuild = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var unit, result;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                unit = this.getCompilationUnit(CompilationUnits.BuildSource);
                                if (unit == null) {
                                    return [2 /*return*/, { workDone: true, errorCount: 0 }];
                                }
                                return [4 /*yield*/, RS.Build.compileUnit(unit)];
                            case 1:
                                result = _a.sent();
                                if (!(result.errorCount === 0)) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.saveChecksums()];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3: return [2 /*return*/, result];
                        }
                    });
                });
            };
            /**
             * Loads the build component of this module, if it has one.
             */
            Base.prototype.loadBuild = function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this._buildLoaded) {
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, RS.FileSystem.fileExists(RS.Path.combine(this._path, "buildsrc", "tsconfig.json"))];
                            case 1:
                                // Don't do anything unless we have build source
                                if (!(_a.sent())) {
                                    return [2 /*return*/, false];
                                }
                                // Load it
                                require(RS.Path.relative(__dirname, RS.Path.combine(this._path, "build", "buildsrc.js")));
                                // Log.debug(`Loaded build component of '${this.name}'`);
                                this._buildLoaded = true;
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Saves the checksum database to file.
             */
            Base.prototype.saveChecksums = function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this._checksumDB.saveToFile(RS.Path.combine(this._path, "build", "checksums.json"))];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            Base.prototype.setupSourceCompilationUnit = function () {
                var srcUnit = this.addCompilationUnit(CompilationUnits.Source, "src", Module.DefaultTSConfigs.source);
                Module.CompilationDependencies.addModules(srcUnit, this, CompilationUnits.Source);
                Module.CompilationDependencies.addBower(srcUnit, this);
                Module.CompilationDependencies.addNPM(srcUnit, this);
                Module.CompilationDependencies.addThirdParty(srcUnit, this);
            };
            Base.prototype.setupBuildSourceCompilationUnit = function () {
                var buildSrcUnit = this.addCompilationUnit(CompilationUnits.BuildSource, "buildsrc", Module.DefaultTSConfigs.buildSource);
                Module.CompilationDependencies.addGulp(buildSrcUnit, this);
                Module.CompilationDependencies.addModules(buildSrcUnit, this, CompilationUnits.BuildSource);
                Module.CompilationDependencies.addNPM(buildSrcUnit, this, "build");
                // Add the post processor
                buildSrcUnit.addPostProcessor(function (artifact) {
                    return __assign(__assign({}, artifact), { source: "var RS = module.parent.exports.RS;\n" + artifact.source });
                });
            };
            /**
             * Gets if the specified path should be excluded from the checksum produced by getSourceChecksum.
             * Useful for excluding generated code files from change detection.
             * @param filename
             */
            Base.prototype.shouldExcludeFromSourceChecksum = function (filename) {
                return /[\/\\](dependencies)[\/\\]/g.test(filename);
            };
            Base.prototype.gatherDependencySetInternal = function (set) {
                // Iterate all dependencies
                for (var _i = 0, _a = this._dependencies; _i < _a.length; _i++) {
                    var depModule = _a[_i];
                    // Gather
                    depModule.gatherDependencySetInternal(set);
                }
                // Add us to the list if we're not already there
                if (!set.contains(this)) {
                    set.add(this);
                }
            };
            return Base;
        }());
        Module.Base = Base;
    })(Module = RS.Module || (RS.Module = {}));
})(RS || (RS = {}));
/// <reference path="Log.ts" />
/** Contains console/terminal/bash command functionality. */
var RS;
(function (RS) {
    var Command;
    (function (Command) {
        var debugMode = false; // Log all commands to console?
        var spawn = require("cross-spawn");
        var shell = require("gulp-shell");
        var cmdExists = require("command-exists");
        var cmdPresence = {};
        /**
         * Runs a single command. Async.
         * @param cmd
         * @param args
         * @param cwd
         * @param logOutput
         */
        function run(cmd, args, cwd, logOutput) {
            if (logOutput === void 0) { logOutput = true; }
            return new Promise(function (resolve, reject) {
                RS.Profile.enter("Command.run [" + cmd + "]");
                if (debugMode) {
                    RS.Log.debug("$$ " + cmd + " " + args.filter(function (a) { return "\"" + a + "\""; }).join(" "));
                }
                var opts = (cwd != null) ? { cwd: cwd } : undefined;
                var child = spawn(cmd, args, opts);
                var curOutput = "";
                var curErrOutput = "";
                child.stdout.on("data", function (buffer) {
                    if (debugMode) {
                        RS.Log.debug("$$ " + cmd + ": stdout >> " + buffer.length + " bytes");
                    }
                    var str = buffer.toString();
                    if (logOutput && str && str.length > 0) {
                        if (str.charAt(str.length - 1)) {
                            RS.Log.info(str.substr(0, str.length - 1));
                        }
                        else {
                            RS.Log.info(str);
                        }
                    }
                    curOutput += str;
                });
                child.stderr.on("data", function (buffer) {
                    if (debugMode) {
                        RS.Log.debug("$$ " + cmd + ": stderr >> " + buffer.length + " bytes");
                    }
                    var str = buffer.toString();
                    if (logOutput && str && str.length > 0) {
                        if (str.charAt(str.length - 1)) {
                            RS.Log.error(str.substr(0, str.length - 1));
                        }
                        else {
                            RS.Log.error(str);
                        }
                    }
                    curErrOutput += str;
                });
                child.on("close", function () {
                    if (debugMode) {
                        RS.Log.debug("$$ " + cmd + ": close");
                    }
                    RS.Profile.exit("Command.run [" + cmd + "]");
                    if (curErrOutput.length > 0) {
                        reject(curErrOutput);
                    }
                    else {
                        resolve(curOutput);
                    }
                });
                child.on("error", function (err) {
                    if (debugMode) {
                        RS.Log.debug("$$ " + cmd + ": error");
                    }
                    RS.Profile.exit("Command.run [" + cmd + "]");
                    reject(cmd + " - " + err);
                });
            });
        }
        Command.run = run;
        /**
         * Runs a batch of commands in sequence. Async.
         * @param cmds
         * @param cwd
         */
        function runMultiple(cmds, cwd) {
            return new Promise(function (resolve, reject) {
                var collapsed = cmds.join("; ");
                if (cwd) {
                    collapsed = "pushd \"" + cwd + "\"; " + collapsed + "; popd;";
                }
                return shell.task([collapsed])(resolve);
            });
        }
        Command.runMultiple = runMultiple;
        /**
         * Tests if the specified command is present. Async.
         * @param path
         */
        function exists(cmd) {
            return __awaiter(this, void 0, void 0, function () {
                var _a, _b, _c;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            if (cmdPresence[cmd] != null) {
                                return [2 /*return*/, cmdPresence[cmd]];
                            }
                            _d.label = 1;
                        case 1:
                            _d.trys.push([1, 3, , 4]);
                            _a = cmdPresence;
                            _b = cmd;
                            return [4 /*yield*/, cmdExists(cmd)];
                        case 2: return [2 /*return*/, _a[_b] = (_d.sent()) === cmd];
                        case 3:
                            _c = _d.sent();
                            return [2 /*return*/, cmdPresence[cmd] = false];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        }
        Command.exists = exists;
    })(Command = RS.Command || (RS.Command = {}));
})(RS || (RS = {}));
/// <reference path="Timer.ts" />
/// <reference path="EnvArg.ts" />
var RS;
(function (RS) {
    function Profile(name) {
        return function (target, propertyKey, descriptor) {
            var oldFunc = descriptor.value;
            function newFunc() {
                Profile.enter(name);
                var ret = oldFunc.apply(this, arguments);
                if (ret instanceof Promise) {
                    ret.then(function () { return Profile.exit(name); }, function () { return Profile.exit(name); });
                }
                else {
                    Profile.exit(name);
                }
                return ret;
            }
            descriptor.value = newFunc;
            Object.defineProperty(target, propertyKey, descriptor);
            target[propertyKey] = newFunc;
        };
    }
    RS.Profile = Profile;
    (function (Profile) {
        var profileMap = {};
        function enter(name) {
            var p = profileMap[name] || (profileMap[name] = { refCount: 0, timer: new RS.Timer() });
            p.refCount++;
            if (p.refCount === 1) {
                p.timer.start();
            }
        }
        Profile.enter = enter;
        function exit(name) {
            var p = profileMap[name] || (profileMap[name] = { refCount: 0, timer: new RS.Timer() });
            p.refCount--;
            if (p.refCount === 0) {
                p.timer.pause();
            }
        }
        Profile.exit = exit;
        function dump() {
            var keys = Object.keys(profileMap).sort();
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                var p = profileMap[key];
                var elapsed = p.timer.elapsed / 1000;
                RS.Log.debug("Total time: " + elapsed.toFixed(2) + "s", key);
            }
        }
        Profile.dump = dump;
        function reset() {
            profileMap = {};
        }
        Profile.reset = reset;
    })(Profile = RS.Profile || (RS.Profile = {}));
    var EnvArgs;
    (function (EnvArgs) {
        /** Whether or not performance profiling information should be dumped to the console. */
        EnvArgs.profile = new RS.BooleanEnvArg("PROFILE", false);
    })(EnvArgs = RS.EnvArgs || (RS.EnvArgs = {}));
})(RS || (RS = {}));
/// <reference path="../Command.ts" />
/// <reference path="../Path.ts" />
/// <reference path="../Profile.ts" />
/// <reference path="IFileSystem.ts" />
/** Contains file system manipulation functionality. */
var RS;
(function (RS) {
    var FileIO;
    (function (FileIO) {
        var del = require("del");
        var fs = require("fs");
        var mkdirp = require("mkdirp");
        var RawFileSystem = /** @class */ (function () {
            function RawFileSystem(settings) {
                this.settings = settings;
            }
            /** Returns the true path of the given file, accounting for casing. */
            RawFileSystem.prototype.findPath = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var parts, dir, outPath, _loop_2, this_2, i, state_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.findPath: '" + path + "'");
                                }
                                parts = RS.Path.split(path);
                                dir = [];
                                outPath = [];
                                _loop_2 = function (i) {
                                    var dirPath, entries, part, flattenedName, found, _i, entries_1, entry, remainingParts;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                dirPath = RS.Path.combine.apply(RS.Path, dir);
                                                return [4 /*yield*/, this_2.readDirectory(dirPath, false)];
                                            case 1:
                                                entries = _a.sent();
                                                part = parts[i];
                                                if (entries.some(function (entry) { return entry === part; })) {
                                                    outPath.push(part);
                                                    dir.push(part);
                                                    return [2 /*return*/, "continue"];
                                                }
                                                flattenedName = part.toLowerCase();
                                                found = false;
                                                for (_i = 0, entries_1 = entries; _i < entries_1.length; _i++) {
                                                    entry = entries_1[_i];
                                                    if (entry.toLowerCase() === flattenedName) {
                                                        found = true;
                                                        outPath.push(entry);
                                                        break;
                                                    }
                                                }
                                                if (!found) {
                                                    remainingParts = parts.slice(i);
                                                    outPath.push.apply(outPath, remainingParts);
                                                    return [2 /*return*/, "break"];
                                                }
                                                // Traverse down
                                                dir.push(part);
                                                return [2 /*return*/];
                                        }
                                    });
                                };
                                this_2 = this;
                                i = 0;
                                _a.label = 1;
                            case 1:
                                if (!(i < parts.length)) return [3 /*break*/, 4];
                                return [5 /*yield**/, _loop_2(i)];
                            case 2:
                                state_1 = _a.sent();
                                if (state_1 === "break")
                                    return [3 /*break*/, 4];
                                _a.label = 3;
                            case 3:
                                i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, RS.Path.combine.apply(RS.Path, outPath)];
                        }
                    });
                });
            };
            RawFileSystem.prototype.isFolder = function (path) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    if (_this.settings.debugMode) {
                        RS.Log.debug("FileSystem.isFolder: '" + path + "'");
                    }
                    fs.stat(path, function (err, stats) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(!stats.isFile());
                        }
                    });
                });
            };
            /** Deletes the specified path. Async. */
            RawFileSystem.prototype.deletePath = function (path) {
                return del([path], { force: true });
            };
            /** Creates the specified path. Async. */
            RawFileSystem.prototype.createPath = function (path) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    if (_this.settings.debugMode) {
                        RS.Log.debug("FileSystem.createPath: '" + path + "'");
                    }
                    mkdirp(path, function (err) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(path);
                        }
                    });
                });
            };
            /**
             * Copies a file from one location to another.
             */
            RawFileSystem.prototype.copyFile = function (srcPath, dstPath) {
                var _this = this;
                return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                    function done(err) {
                        if (!cbCalled) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve();
                            }
                            cbCalled = true;
                        }
                    }
                    var cbCalled, rd, wr;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.copyFile: '" + srcPath + "' -> '" + dstPath + "'");
                                }
                                return [4 /*yield*/, this.removeCaseConflicts(dstPath)];
                            case 1:
                                _a.sent();
                                cbCalled = false;
                                rd = fs.createReadStream(srcPath);
                                rd.on("error", function (err) { return done(err); });
                                wr = fs.createWriteStream(dstPath);
                                wr.on("error", function (err) { return done(err); });
                                wr.on("close", function (err) { return done(err); });
                                rd.pipe(wr);
                                return [2 /*return*/];
                        }
                    });
                }); });
            };
            /**
             * Copies multiple files from one location to another.
             * @param srcPaths
             * @param dstPaths
             */
            RawFileSystem.prototype.copyFiles = function (srcPaths, dstPaths) {
                return __awaiter(this, void 0, void 0, function () {
                    var promises, i;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                promises = [];
                                for (i = 0; i < srcPaths.length; i++) {
                                    promises.push(this.copyFile(srcPaths[i], dstPaths[i]));
                                }
                                return [4 /*yield*/, Promise.all(promises)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            RawFileSystem.prototype.readFile = function (path, binary) {
                var _this = this;
                if (binary === void 0) { binary = false; }
                return new Promise(function (resolve, reject) {
                    if (_this.settings.debugMode) {
                        RS.Log.debug("FileSystem.readFile: '" + path + "'");
                    }
                    fs.readFile(path, function (err, data) {
                        if (err) {
                            reject(err);
                            return;
                        }
                        if (binary) {
                            resolve(data);
                            return;
                        }
                        // Scan the buffer for \r characters
                        var strArr = [];
                        var readPos = 0;
                        for (var i = 0; i < data.length; i++) {
                            var val = data[i];
                            var nextVal = i >= data.length ? "" : data[i + 1];
                            //carriage return AND NOT line feed
                            if (val == 0x0D && nextVal != 0x0A) {
                                if (i > readPos) {
                                    strArr.push(data.toString("utf8", readPos, i));
                                    strArr.push("\n");
                                }
                                readPos = i + 1;
                            }
                        }
                        if (readPos < data.length - 1) {
                            strArr.push(data.toString("utf8", readPos, data.length));
                        }
                        resolve(strArr.join(""));
                    });
                });
            };
            /**
             * Streams a file into a function that accepts buffers.
             */
            RawFileSystem.prototype.streamFile = function (path, targetFunc) {
                return new Promise(function (resolve, reject) {
                    var strm = fs.createReadStream(path);
                    var resolved = false;
                    strm.on("end", function () {
                        if (resolved) {
                            return;
                        }
                        resolve();
                        resolved = true;
                    });
                    strm.on("error", function (err) {
                        if (resolved) {
                            return;
                        }
                        reject(err);
                        resolved = true;
                    });
                    strm.on("data", function (chunk) {
                        if (resolved) {
                            return;
                        }
                        targetFunc(chunk);
                    });
                });
            };
            /**
             * Writes multiple text or binary files.
             */
            RawFileSystem.prototype.writeFiles = function (paths, contents, options) {
                return __awaiter(this, void 0, void 0, function () {
                    var promises, i;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                promises = [];
                                for (i = 0; i < paths.length; i++) {
                                    promises.push(this.writeFile(paths[i], contents[i], options));
                                }
                                return [4 /*yield*/, Promise.all(promises)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Writes a text or binary file.
             */
            RawFileSystem.prototype.writeFile = function (path, content, options) {
                var _this = this;
                return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                    var fsOptions;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.writeFile: '" + path + "'");
                                }
                                fsOptions = options ?
                                    {
                                        encoding: options.encoding,
                                        mode: RS.Permissions.toString(options.mode),
                                        flag: options.flag
                                    } : null;
                                return [4 /*yield*/, this.removeCaseConflicts(path)];
                            case 1:
                                _a.sent();
                                fs.writeFile(path, content, fsOptions, function (err) {
                                    if (err) {
                                        reject(err);
                                        return;
                                    }
                                    resolve();
                                });
                                return [2 /*return*/];
                        }
                    });
                }); });
            };
            /**
             * Tests if the specified file exists.
             * @param path
             */
            RawFileSystem.prototype.fileExists = function (path) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    if (_this.settings.debugMode) {
                        RS.Log.debug("FileSystem.fileExists: '" + path + "'");
                    }
                    fs.access(path, fs.constants.F_OK | fs.constants.R_OK, function (err) {
                        if (err) {
                            resolve(false);
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
            };
            RawFileSystem.prototype.readDirectory = function (path, fileOrFolder) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    if (_this.settings.debugMode) {
                        RS.Log.debug("FileSystem.readDirectory: '" + path + "'");
                    }
                    fs.readdir(path, function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            result = result.filter(function (file) { return _this.settings.excluded.indexOf(file) === -1; });
                            if (fileOrFolder) {
                                var fileOrFolderResult_1 = [];
                                var remainCnt_1 = result.length;
                                var done = function (name, err, stat) {
                                    if (!err) {
                                        fileOrFolderResult_1.push({ name: name, type: stat.isFile() ? "file" : "folder" });
                                    }
                                    remainCnt_1--;
                                    if (remainCnt_1 === 0) {
                                        resolve(fileOrFolderResult_1);
                                    }
                                };
                                for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                                    var name_8 = result_1[_i];
                                    fs.stat(path + "/" + name_8, done.bind(_this, name_8));
                                }
                                if (result.length === 0) {
                                    resolve(fileOrFolderResult_1);
                                }
                            }
                            else {
                                resolve(result);
                            }
                        }
                    });
                });
            };
            /**
             * Finds all files within the specified directory and sub-directories. Async.
             * @param path
             */
            RawFileSystem.prototype.readFiles = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var fileList, subFiles, _i, subFiles_1, subFile, subPath, _a, _b, subFile2, _c;
                    return __generator(this, function (_d) {
                        switch (_d.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.readFiles: '" + path + "'");
                                }
                                fileList = [];
                                _d.label = 1;
                            case 1:
                                _d.trys.push([1, 10, , 11]);
                                return [4 /*yield*/, this.readDirectory(path, true)];
                            case 2:
                                subFiles = _d.sent();
                                _i = 0, subFiles_1 = subFiles;
                                _d.label = 3;
                            case 3:
                                if (!(_i < subFiles_1.length)) return [3 /*break*/, 9];
                                subFile = subFiles_1[_i];
                                subPath = RS.Path.combine(path, subFile.name);
                                if (!(subFile.type === "file")) return [3 /*break*/, 4];
                                fileList.push(subPath);
                                return [3 /*break*/, 8];
                            case 4:
                                _a = 0;
                                return [4 /*yield*/, this.readFiles(subPath)];
                            case 5:
                                _b = _d.sent();
                                _d.label = 6;
                            case 6:
                                if (!(_a < _b.length)) return [3 /*break*/, 8];
                                subFile2 = _b[_a];
                                fileList.push(subFile2);
                                _d.label = 7;
                            case 7:
                                _a++;
                                return [3 /*break*/, 6];
                            case 8:
                                _i++;
                                return [3 /*break*/, 3];
                            case 9: return [3 /*break*/, 11];
                            case 10:
                                _c = _d.sent();
                                return [3 /*break*/, 11];
                            case 11: return [2 /*return*/, fileList];
                        }
                    });
                });
            };
            /**
             * Gets statistics for the specified file.
             * @param path
             */
            RawFileSystem.prototype.getFileStats = function (path) {
                return new Promise(function (resolve, reject) {
                    fs.stat(path, function (err, stats) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(stats);
                        }
                    });
                });
            };
            RawFileSystem.prototype.removeCaseConflicts = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var casedPath, casedParts, pathParts_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.findPath(path)];
                            case 1:
                                casedPath = _a.sent();
                                if (!(casedPath !== path)) return [3 /*break*/, 4];
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.removeCaseConflicts: '" + casedPath + "' -> '" + path + "'");
                                }
                                casedParts = RS.Path.split(casedPath);
                                pathParts_1 = RS.Path.split(path);
                                if (!casedParts.slice(0, casedParts.length - 1).some(function (casedPart, index) { return casedPart !== pathParts_1[index]; })) return [3 /*break*/, 2];
                                RS.Log.warn("FileSystem.removeCaseConflicts: cannot resolve directory case mismatch: '" + casedPath + "' -> '" + path + "'");
                                return [3 /*break*/, 4];
                            case 2: return [4 /*yield*/, this.deletePath(casedPath)];
                            case 3:
                                _a.sent();
                                _a.label = 4;
                            case 4: return [2 /*return*/];
                        }
                    });
                });
            };
            __decorate([
                RS.Profile("FileIO.deletePath")
            ], RawFileSystem.prototype, "deletePath", null);
            __decorate([
                RS.Profile("FileIO.createPath")
            ], RawFileSystem.prototype, "createPath", null);
            __decorate([
                RS.Profile("FileIO.copyFile")
            ], RawFileSystem.prototype, "copyFile", null);
            __decorate([
                RS.Profile("FileIO.readFile")
            ], RawFileSystem.prototype, "readFile", null);
            __decorate([
                RS.Profile("FileIO.streamFile")
            ], RawFileSystem.prototype, "streamFile", null);
            __decorate([
                RS.Profile("FileIO.writeFile")
            ], RawFileSystem.prototype, "writeFile", null);
            __decorate([
                RS.Profile("FileIO.fileExists")
            ], RawFileSystem.prototype, "fileExists", null);
            __decorate([
                RS.Profile("FileIO.readDirectory")
            ], RawFileSystem.prototype, "readDirectory", null);
            return RawFileSystem;
        }());
        FileIO.RawFileSystem = RawFileSystem;
    })(FileIO = RS.FileIO || (RS.FileIO = {}));
})(RS || (RS = {}));
/// <reference path="RawFileSystem.ts" />
var RS;
(function (RS) {
    var FileIO;
    (function (FileIO) {
        var FileSystemCache = /** @class */ (function () {
            function FileSystemCache(rootPath, baseSys) {
                this._rootPath = rootPath;
                this._baseFileSys = baseSys;
            }
            Object.defineProperty(FileSystemCache.prototype, "paths", {
                /**
                 * Gets the paths of all cached nodes.
                 */
                get: function () {
                    var _this = this;
                    return Object.keys(this._nodes).filter(function (k) { return _this._nodes[k] != null; });
                },
                enumerable: true,
                configurable: true
            });
            FileSystemCache.prototype.rebuild = function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this._nodes = {};
                                this.set(".", { type: FileSystemCache.NodeType.Directory });
                                return [4 /*yield*/, this.buildDirectory(this._rootPath)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            FileSystemCache.prototype.buildDirectory = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var children, _i, children_1, child, fullPath;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this._baseFileSys.readDirectory(path, true)];
                            case 1:
                                children = _a.sent();
                                _i = 0, children_1 = children;
                                _a.label = 2;
                            case 2:
                                if (!(_i < children_1.length)) return [3 /*break*/, 6];
                                child = children_1[_i];
                                fullPath = RS.Path.combine(path, child.name);
                                if (!(child.type === "file")) return [3 /*break*/, 3];
                                this.set(fullPath, { type: FileSystemCache.NodeType.File, content: null });
                                return [3 /*break*/, 5];
                            case 3:
                                if (!(child.type === "folder")) return [3 /*break*/, 5];
                                this.set(fullPath, { type: FileSystemCache.NodeType.Directory });
                                return [4 /*yield*/, this.buildDirectory(fullPath)];
                            case 4:
                                _a.sent();
                                _a.label = 5;
                            case 5:
                                _i++;
                                return [3 /*break*/, 2];
                            case 6: return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Gets if the specified path is contained within this cache.
             * Does not check if the path actually exists, only that it sits in or below the root path.
             * @param path
             */
            FileSystemCache.prototype.contains = function (path) {
                var relPath = RS.Path.relative(this._rootPath, path);
                return relPath.indexOf("..") === -1;
            };
            /**
             * Retrieves a cached node.
             * @param path
             */
            FileSystemCache.prototype.get = function (path) {
                var relPath = RS.Path.relative(this._rootPath, path);
                if (relPath.indexOf("..") !== -1) {
                    return null;
                }
                return this._nodes[relPath] || null;
            };
            /**
             * Sets a cached node.
             * Returns if successful or not.
             * @param path
             * @param node
             */
            FileSystemCache.prototype.set = function (path, node) {
                var relPath = RS.Path.relative(this._rootPath, path);
                if (relPath.indexOf("..") !== -1) {
                    return false;
                }
                this._nodes[relPath] = node;
                return true;
            };
            /**
             * Finds all child nodes under the specified path.
             * @param rootPath
             * @param recurse
             */
            FileSystemCache.prototype.search = function (path, recurse) {
                path = RS.Path.relative(this._rootPath, path);
                if (path.indexOf("..") !== -1) {
                    return [];
                }
                if (path === "." || path === "") {
                    path = "";
                }
                else {
                    path = "" + path + RS.Path.seperator;
                }
                return this.paths.filter(function (p) {
                    if (p === ".") {
                        return false;
                    }
                    if (p.length < path.length) {
                        return false;
                    }
                    if (p === path) {
                        return false;
                    }
                    if (p.substr(0, path.length) !== path) {
                        return false;
                    }
                    var nestLevel = RS.Path.split(p.substr(path.length)).length;
                    if (nestLevel > 1 && !recurse) {
                        return false;
                    }
                    return true;
                });
            };
            __decorate([
                RS.Profile("FileSystemCache.rebuild")
            ], FileSystemCache.prototype, "rebuild", null);
            return FileSystemCache;
        }());
        FileIO.FileSystemCache = FileSystemCache;
        (function (FileSystemCache) {
            var NodeType;
            (function (NodeType) {
                NodeType[NodeType["File"] = 0] = "File";
                NodeType[NodeType["Directory"] = 1] = "Directory";
            })(NodeType = FileSystemCache.NodeType || (FileSystemCache.NodeType = {}));
        })(FileSystemCache = FileIO.FileSystemCache || (FileIO.FileSystemCache = {}));
    })(FileIO = RS.FileIO || (RS.FileIO = {}));
})(RS || (RS = {}));
/// <reference path="IFileSystem.ts" />
/// <reference path="RawFileSystem.ts" />
/// <reference path="FileSystemCache.ts" />
var RS;
(function (RS) {
    var FileIO;
    (function (FileIO) {
        var CachedFileSystem = /** @class */ (function () {
            function CachedFileSystem(settings) {
                this.settings = settings;
                var fileSys = new FileIO.RawFileSystem(__assign(__assign({}, settings), { debugMode: false }));
                this._cache = new FileIO.FileSystemCache(settings.cacheRoot, fileSys);
                this._baseSys = fileSys;
            }
            Object.defineProperty(CachedFileSystem.prototype, "cache", {
                get: function () { return this._cache; },
                enumerable: true,
                configurable: true
            });
            /** Returns the true path of the given file, accounting for casing. */
            CachedFileSystem.prototype.findPath = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var flattenedPath, _i, _a, cachedPath;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.findPath: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.findPath(path)];
                            case 1: return [2 /*return*/, _b.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.findPath(path)];
                            case 3: return [2 /*return*/, _b.sent()];
                            case 4:
                                // Find exact match.
                                if (this._cache.get(path)) {
                                    return [2 /*return*/, path];
                                }
                                flattenedPath = path.toLowerCase();
                                for (_i = 0, _a = this._cache.paths; _i < _a.length; _i++) {
                                    cachedPath = _a[_i];
                                    if (cachedPath.toLowerCase() === flattenedPath) {
                                        return [2 /*return*/, cachedPath];
                                    }
                                }
                                // Path did not exist at all.
                                return [2 /*return*/, path];
                        }
                    });
                });
            };
            CachedFileSystem.prototype.isFolder = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var node;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.isFolder: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.isFolder(path)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.isFolder(path)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                node = this._cache.get(path);
                                return [2 /*return*/, node.type === FileIO.FileSystemCache.NodeType.Directory];
                        }
                    });
                });
            };
            CachedFileSystem.prototype.cacheMiss = function () {
                RS.Log.debug("Cache miss");
            };
            /**
             * Rebuilds the cache. Async.
             */
            CachedFileSystem.prototype.rebuildCache = function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.rebuildCache");
                                }
                                return [4 /*yield*/, this._cache.rebuild()];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Gets if the specified path falls within our cache.
             * @param path
             */
            CachedFileSystem.prototype.pathIsWithinCache = function (path) {
                var relToRoot = RS.Path.relative(this.settings.cacheRoot, path);
                var segs = RS.Path.split(relToRoot).map(function (seg) { return seg.toLowerCase(); });
                if (segs.indexOf("..") !== -1) {
                    return false;
                }
                for (var _i = 0, _a = this.settings.excluded; _i < _a.length; _i++) {
                    var excl = _a[_i];
                    if (segs.indexOf(excl.toLowerCase()) !== -1) {
                        return false;
                    }
                }
                return true;
            };
            /**
             * Deletes the specified path. Async.
             */
            CachedFileSystem.prototype.deletePath = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var node, _i, _a, p;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.deletePath: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.deletePath(path)];
                            case 1: return [2 /*return*/, _b.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.deletePath(path)];
                            case 3: return [2 /*return*/, _b.sent()];
                            case 4:
                                node = this.getNode(path);
                                if (node == null) {
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, this._baseSys.deletePath(path)];
                            case 5:
                                _b.sent();
                                this._cache.set(path, null);
                                for (_i = 0, _a = this._cache.search(path, true); _i < _a.length; _i++) {
                                    p = _a[_i];
                                    this._cache.set(p, null);
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Creates the specified path. Async.
             */
            CachedFileSystem.prototype.createPath = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var node, spl, curPath, _i, spl_1, seg, node_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.createPath: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.createPath(path)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.createPath(path)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                node = this.getNode(path);
                                if (node != null) {
                                    return [2 /*return*/, path];
                                }
                                return [4 /*yield*/, this._baseSys.createPath(path)];
                            case 5:
                                _a.sent();
                                spl = RS.Path.split(RS.Path.relative(this.settings.cacheRoot, path));
                                curPath = null;
                                for (_i = 0, spl_1 = spl; _i < spl_1.length; _i++) {
                                    seg = spl_1[_i];
                                    if (curPath == null) {
                                        curPath = seg;
                                    }
                                    else {
                                        curPath = RS.Path.combine(curPath, seg);
                                    }
                                    node_1 = this.getNode(curPath);
                                    if (node_1 == null) {
                                        this._cache.set(curPath, { type: FileIO.FileSystemCache.NodeType.Directory });
                                    }
                                }
                                return [2 /*return*/, path];
                        }
                    });
                });
            };
            /**
             * Copies a file from one location to another.
             */
            CachedFileSystem.prototype.copyFile = function (srcPath, dstPath) {
                return __awaiter(this, void 0, void 0, function () {
                    var srcNode;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.copyFile: '" + srcPath + "' -> '" + dstPath + "'");
                                }
                                if (!(!this.pathIsWithinCache(srcPath) || !this.pathIsWithinCache(dstPath))) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.copyFile(srcPath, dstPath)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!(!this._cache.contains(srcPath) || !this._cache.contains(dstPath))) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.copyFile(srcPath, dstPath)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                srcNode = this.getNode(srcPath);
                                if (srcNode == null) {
                                    throw new Error("File not found '" + srcPath + "'");
                                }
                                return [4 /*yield*/, this._baseSys.copyFile(srcPath, dstPath)];
                            case 5:
                                _a.sent();
                                this._cache.set(dstPath, __assign({}, srcNode));
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Copies multiple files from one location to another.
             * @param srcPaths
             * @param dstPaths
             */
            CachedFileSystem.prototype.copyFiles = function (srcPaths, dstPaths) {
                return __awaiter(this, void 0, void 0, function () {
                    var promises, i;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                promises = [];
                                for (i = 0; i < srcPaths.length; i++) {
                                    promises.push(this.copyFile(srcPaths[i], dstPaths[i]));
                                }
                                return [4 /*yield*/, Promise.all(promises)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Reads a text file.
             */
            CachedFileSystem.prototype.readFile = function (path, binary) {
                if (binary === void 0) { binary = false; }
                return __awaiter(this, void 0, void 0, function () {
                    var node, content;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.readFile: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.readFile(path, binary)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.readFile(path, binary)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                node = this.getNode(path);
                                if (node == null) {
                                    throw new Error("File not found '" + path + "'");
                                }
                                if (node.type !== FileIO.FileSystemCache.NodeType.File) {
                                    throw new Error("Tried to read directory as a file");
                                }
                                if (!binary && RS.Is.string(node.content)) {
                                    return [2 /*return*/, node.content];
                                }
                                if (binary && node.content instanceof Buffer) {
                                    return [2 /*return*/, node.content];
                                }
                                return [4 /*yield*/, this._baseSys.readFile(path, binary)];
                            case 5:
                                content = _a.sent();
                                node.content = content;
                                return [2 /*return*/, content];
                        }
                    });
                });
            };
            /**
             * Streams a file into a function that accepts buffers.
             */
            CachedFileSystem.prototype.streamFile = function (path, targetFunc) {
                return __awaiter(this, void 0, void 0, function () {
                    var node;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.streamFile: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.streamFile(path, targetFunc)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.streamFile(path, targetFunc)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                node = this.getNode(path);
                                if (node == null) {
                                    throw new Error("File not found '" + path + "'");
                                }
                                if (node.type !== FileIO.FileSystemCache.NodeType.File) {
                                    throw new Error("Tried to stream directory as a file");
                                }
                                if (node.content instanceof Buffer) {
                                    targetFunc(node.content);
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, this._baseSys.streamFile(path, targetFunc)];
                            case 5:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Writes a text or binary file.
             */
            CachedFileSystem.prototype.writeFile = function (path, content, options) {
                return __awaiter(this, void 0, void 0, function () {
                    var node;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.writeFile: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.writeFile(path, content, options)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.writeFile(path, content, options)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                node = this.getNode(path);
                                if (node != null) {
                                    if (node.type === FileIO.FileSystemCache.NodeType.Directory) {
                                        throw new Error("Tried to write directory as a file");
                                    }
                                    if (RS.Is.string(node.content) && node.content === content) {
                                        return [2 /*return*/];
                                    }
                                }
                                return [4 /*yield*/, this._baseSys.writeFile(path, content, options)];
                            case 5:
                                _a.sent();
                                if (node != null) {
                                    node.content = content;
                                }
                                else {
                                    this._cache.set(path, { type: FileIO.FileSystemCache.NodeType.File, content: content });
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Writes multiple text or binary files.
             */
            CachedFileSystem.prototype.writeFiles = function (paths, contents, options) {
                return __awaiter(this, void 0, void 0, function () {
                    var promises, i;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                promises = [];
                                for (i = 0; i < paths.length; i++) {
                                    promises.push(this.writeFile(paths[i], contents[i], options));
                                }
                                return [4 /*yield*/, Promise.all(promises)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Tests if the specified file exists.
             * @param path
             */
            CachedFileSystem.prototype.fileExists = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var node;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.fileExists: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.fileExists(path)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.fileExists(path)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4:
                                node = this.getNode(path);
                                return [2 /*return*/, node != null && node.type === FileIO.FileSystemCache.NodeType.File];
                        }
                    });
                });
            };
            CachedFileSystem.prototype.readDirectory = function (path, fileOrFolder) {
                return __awaiter(this, void 0, void 0, function () {
                    var node, result;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.readDirectory: '" + path + "'");
                                }
                                if (!!this._cache.contains(path)) return [3 /*break*/, 2];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.readDirectory(path, fileOrFolder)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                node = this.getNode(path);
                                if (node == null) {
                                    return [2 /*return*/, []];
                                }
                                if (node.type !== FileIO.FileSystemCache.NodeType.Directory) {
                                    throw new Error("Tried to read file as a directory");
                                }
                                result = this._cache.search(path, false).map(function (p) { return RS.Path.relative(path, p); });
                                if (fileOrFolder) {
                                    return [2 /*return*/, result.map(function (p) {
                                            var node = _this._cache.get(RS.Path.combine(path, p));
                                            if (node.type === FileIO.FileSystemCache.NodeType.File) {
                                                return { type: "file", name: p };
                                            }
                                            else {
                                                return { type: "folder", name: p };
                                            }
                                        })];
                                }
                                else {
                                    return [2 /*return*/, result];
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Finds all files within the specified directory and sub-directories.
             * Returned paths are relative to the current working directory.
             * Async.
             * @param path
             */
            CachedFileSystem.prototype.readFiles = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.readDirectory: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.readFiles(path)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.readFiles(path)];
                            case 3: return [2 /*return*/, _a.sent()];
                            case 4: return [2 /*return*/, this._cache.search(path, true).filter(function (p) {
                                    var node = _this._cache.get(p);
                                    if (node == null) {
                                        return false;
                                    } // Node could be null if the file existed previously but was deleted
                                    return node.type === FileIO.FileSystemCache.NodeType.File;
                                })];
                        }
                    });
                });
            };
            /**
             * Gets statistics for the specified file.
             * @param path
             */
            CachedFileSystem.prototype.getFileStats = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var node, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (this.settings.debugMode) {
                                    RS.Log.debug("FileSystem.getFileStats: '" + path + "'");
                                }
                                if (!!this.pathIsWithinCache(path)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this._baseSys.getFileStats(path)];
                            case 1: return [2 /*return*/, _b.sent()];
                            case 2:
                                if (!!this._cache.contains(path)) return [3 /*break*/, 4];
                                this.cacheMiss();
                                return [4 /*yield*/, this._baseSys.getFileStats(path)];
                            case 3: return [2 /*return*/, _b.sent()];
                            case 4:
                                node = this.getNode(path);
                                if (node == null) {
                                    throw new Error("File not found '" + path + "'");
                                }
                                if (node.type !== FileIO.FileSystemCache.NodeType.File) {
                                    throw new Error("Path is not a file '" + path + "'");
                                }
                                if (node.stats != null) {
                                    return [2 /*return*/, node.stats];
                                }
                                _a = node;
                                return [4 /*yield*/, this._baseSys.getFileStats(path)];
                            case 5: return [2 /*return*/, _a.stats = _b.sent()];
                        }
                    });
                });
            };
            CachedFileSystem.prototype.getNode = function (path) {
                var node = this._cache.get(path);
                if (node) {
                    return node;
                }
                if (!this.settings.caseSensitive) {
                    var flatPath = path.toLowerCase();
                    for (var _i = 0, _a = this._cache.paths; _i < _a.length; _i++) {
                        var cachePath = _a[_i];
                        if (cachePath.toLowerCase() === flatPath) {
                            return this._cache.get(cachePath);
                        }
                    }
                }
                return null;
            };
            return CachedFileSystem;
        }());
        FileIO.CachedFileSystem = CachedFileSystem;
    })(FileIO = RS.FileIO || (RS.FileIO = {}));
})(RS || (RS = {}));
/// <reference path="CachedFileSystem.ts" />
var RS;
(function (RS) {
    RS.FileSystem = new RS.FileIO.CachedFileSystem({
        excluded: [".DS_Store", ".git", "node_modules", "bower_components"],
        debugMode: false,
        cacheRoot: ".",
        caseSensitive: false
    });
})(RS || (RS = {}));
/// <reference path="Info.ts" />
/// <reference path="Base.ts" />
/// <reference path="../util/fileio/FileSystem.ts" />
/// <reference path="../util/Profile.ts" />
var RS;
(function (RS) {
    var Module;
    (function (Module) {
        var blacklistDirectories = {
            "node_modules": true,
            "bower_components": true,
            "schemas": true
        };
        var modules = {};
        /**
         * Finds all modules underneath the specified path. Recurses down sub-directories. Async.
         * @param path
         */
        function findModules(path) {
            return __awaiter(this, void 0, void 0, function () {
                var list, result, _i, list_1, item, _a, _b, subItem;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            RS.Profile.enter("findModules");
                            return [4 /*yield*/, RS.FileSystem.readDirectory(path, true)];
                        case 1:
                            list = _c.sent();
                            result = [];
                            _i = 0, list_1 = list;
                            _c.label = 2;
                        case 2:
                            if (!(_i < list_1.length)) return [3 /*break*/, 8];
                            item = list_1[_i];
                            if (!(item.type === "file" && item.name === "moduleinfo.json")) return [3 /*break*/, 3];
                            result.push(path + "/" + item.name);
                            return [3 /*break*/, 7];
                        case 3:
                            if (!(item.type === "folder" && !blacklistDirectories[item.name])) return [3 /*break*/, 7];
                            _a = 0;
                            return [4 /*yield*/, findModules(path + "/" + item.name)];
                        case 4:
                            _b = _c.sent();
                            _c.label = 5;
                        case 5:
                            if (!(_a < _b.length)) return [3 /*break*/, 7];
                            subItem = _b[_a];
                            result.push(subItem);
                            _c.label = 6;
                        case 6:
                            _a++;
                            return [3 /*break*/, 5];
                        case 7:
                            _i++;
                            return [3 /*break*/, 2];
                        case 8:
                            RS.Profile.exit("findModules");
                            return [2 /*return*/, result];
                    }
                });
            });
        }
        Module.findModules = findModules;
        /**
         * Determines the name of a module from the location of it's module info.
         * @param path
         */
        function determineNameFromPath(path) {
            var splitBySlash = path.split(/\\|\//g);
            // Identify "modules" folder
            var modulesIdx = splitBySlash.indexOf("modules");
            if (modulesIdx !== -1) {
                // Remove everything up to and including it
                splitBySlash.splice(0, modulesIdx + 1);
            }
            // Remove module info
            if (splitBySlash[splitBySlash.length - 1] === "moduleinfo.json") {
                splitBySlash.pop();
            }
            // Fix casing, join
            return formatName(splitBySlash.join("."));
        }
        Module.determineNameFromPath = determineNameFromPath;
        /**
         * Formats a module name.
         * @param name
         */
        function formatName(name) {
            return name
                .split(".")
                .map(function (item) { return "" + item.substr(0, 1).toUpperCase() + item.substr(1).toLowerCase(); })
                .join(".");
        }
        Module.formatName = formatName;
        /**
         * Gets a module by name.
         * @param name
         */
        function getModule(name) {
            return modules[name] || null;
        }
        Module.getModule = getModule;
        /**
         * Gets all loaded modules.
         * @param sorted If true, sorts the modules by dependency (so module B depending on module A will always come afterwards in the array)
         */
        function getAll(sorted) {
            if (sorted === void 0) { sorted = false; }
            var names = Object.keys(modules);
            if (sorted) {
                var result = [];
                var hasMap = {};
                while (names.length > 0) {
                    var oldNameCount = names.length;
                    for (var i = names.length - 1; i >= 0; --i) {
                        var name_9 = names[i];
                        var module_2 = modules[name_9];
                        var satisfied = true;
                        for (var _i = 0, _a = module_2.info.dependencies; _i < _a.length; _i++) {
                            var dep = _a[_i];
                            if (!hasMap[dep]) {
                                satisfied = false;
                                break;
                            }
                        }
                        if (satisfied) {
                            result.push(module_2);
                            hasMap[name_9] = true;
                            names.splice(i, 1);
                        }
                    }
                    if (names.length === oldNameCount) {
                        RS.Log.warn("getAll: possible circular dependency");
                        break;
                    }
                }
                return result;
            }
            else {
                return names.map(function (name) { return modules[name]; });
            }
        }
        Module.getAll = getAll;
        /**
         * Finds and loads all modules.
         */
        function loadModules() {
            return __awaiter(this, void 0, void 0, function () {
                var rawModules, _i, rawModules_1, path, info, validateErr, moduleName, module_3, didError, moduleName, module_4, _a, _b, depName, modulesToInit, _c, modulesToInit_1, module_5, deps, _d, _e, depName, _f, modulesToInit_2, module_6;
                return __generator(this, function (_g) {
                    switch (_g.label) {
                        case 0:
                            _g.trys.push([0, , 10, 11]);
                            RS.Profile.enter("loadModules");
                            RS.Log.pushContext("LoadModules");
                            return [4 /*yield*/, findModules(".")];
                        case 1:
                            rawModules = _g.sent();
                            _i = 0, rawModules_1 = rawModules;
                            _g.label = 2;
                        case 2:
                            if (!(_i < rawModules_1.length)) return [3 /*break*/, 5];
                            path = rawModules_1[_i];
                            return [4 /*yield*/, RS.JSON.parseFile(path)];
                        case 3:
                            info = _g.sent();
                            validateErr = Module.validateInfo(info);
                            if (validateErr) {
                                RS.Log.warn("Module at '" + path + "': " + validateErr);
                                return [3 /*break*/, 4];
                            }
                            moduleName = info.name || determineNameFromPath(path);
                            if (modules[moduleName]) {
                                RS.Log.warn("Duplicate module detected (by name '" + moduleName + "')");
                                return [3 /*break*/, 4];
                            }
                            module_3 = new Module.Base(moduleName, info, RS.Path.directoryName(path));
                            // Store it
                            modules[moduleName] = module_3;
                            _g.label = 4;
                        case 4:
                            _i++;
                            return [3 /*break*/, 2];
                        case 5:
                            didError = false;
                            for (moduleName in modules) {
                                module_4 = modules[moduleName];
                                for (_a = 0, _b = module_4.info.dependencies; _a < _b.length; _a++) {
                                    depName = _b[_a];
                                    if (!modules[depName]) {
                                        RS.Log.error("Failed to resolve module dependencies for '" + module_4.name + "' - '" + depName + "' was not found");
                                        didError = true;
                                    }
                                }
                            }
                            if (didError) {
                                return [2 /*return*/];
                            }
                            modulesToInit = getAll(true);
                            _c = 0, modulesToInit_1 = modulesToInit;
                            _g.label = 6;
                        case 6:
                            if (!(_c < modulesToInit_1.length)) return [3 /*break*/, 9];
                            module_5 = modulesToInit_1[_c];
                            if (!!module_5.initialised) return [3 /*break*/, 8];
                            deps = [];
                            for (_d = 0, _e = module_5.info.dependencies; _d < _e.length; _d++) {
                                depName = _e[_d];
                                if (!modules[depName]) {
                                    RS.Log.error("Failed to resolve module dependencies for '" + module_5.name + "' - '" + depName + "' was not found");
                                    return [2 /*return*/];
                                }
                                deps.push(modules[depName]);
                            }
                            return [4 /*yield*/, module_5.init(deps)];
                        case 7:
                            _g.sent();
                            _g.label = 8;
                        case 8:
                            _c++;
                            return [3 /*break*/, 6];
                        case 9:
                            // Set importance values
                            for (_f = 0, modulesToInit_2 = modulesToInit; _f < modulesToInit_2.length; _f++) {
                                module_6 = modulesToInit_2[_f];
                                if (module_6.info.important) {
                                    module_6.important = 1;
                                }
                            }
                            return [3 /*break*/, 11];
                        case 10:
                            RS.Log.popContext("LoadModules");
                            RS.Profile.exit("loadModules");
                            return [7 /*endfinally*/];
                        case 11: return [2 /*return*/];
                    }
                });
            });
        }
        Module.loadModules = loadModules;
    })(Module = RS.Module || (RS.Module = {}));
})(RS || (RS = {}));
/// <reference path="../module/Base.ts" />
/// <reference path="Settings.ts" />
var RS;
(function (RS) {
    var AssembleStage;
    (function (AssembleStage) {
        /**
         * Encapsulates a generic assemble stage.
         */
        var Base = /** @class */ (function () {
            function Base() {
            }
            /**
             * Executes this assemble stage.
             */
            Base.prototype.execute = function (settings, moduleList) {
                return __awaiter(this, void 0, void 0, function () {
                    var _i, _a, dep, err_2;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                RS.Log.pushContext("" + this);
                                _i = 0, _a = moduleList.data;
                                _b.label = 1;
                            case 1:
                                if (!(_i < _a.length)) return [3 /*break*/, 7];
                                dep = _a[_i];
                                RS.Log.pushContext(dep.name);
                                _b.label = 2;
                            case 2:
                                _b.trys.push([2, 4, , 5]);
                                return [4 /*yield*/, this.executeModule(settings, dep)];
                            case 3:
                                _b.sent();
                                return [3 /*break*/, 5];
                            case 4:
                                err_2 = _b.sent();
                                RS.Log.error(err_2.stack);
                                return [3 /*break*/, 5];
                            case 5:
                                RS.Log.popContext(dep.name);
                                _b.label = 6;
                            case 6:
                                _i++;
                                return [3 /*break*/, 1];
                            case 7:
                                RS.Log.popContext("" + this);
                                return [2 /*return*/];
                        }
                    });
                });
            };
            Base.prototype.toString = function () {
                var className = Object.getPrototypeOf(this).constructor.name;
                return this.name ? className + ":" + this.name : className;
            };
            return Base;
        }());
        AssembleStage.Base = Base;
    })(AssembleStage = RS.AssembleStage || (RS.AssembleStage = {}));
})(RS || (RS = {}));
/// <reference path="Base.ts" />
/// <reference path="../util/List.ts" />
var RS;
(function (RS) {
    var AssembleStage;
    (function (AssembleStage) {
        var assembleStages = new RS.List();
        /**
         * Registers an assemble stage with the system.
         * @param settings
         * @param stage
         */
        function register(settings, stage) {
            assembleStages.add({ settings: settings, stage: stage });
        }
        AssembleStage.register = register;
        /**
         * Gets all assemble stages in order of execution.
         */
        function getStages() {
            var result = assembleStages.map(function (s) { return s.stage; });
            result.resolveOrder(function (stage) { return assembleStages.find(function (item) { return item.stage === stage; }).settings.before || []; }, function (stage) {
                var settings = assembleStages.find(function (item) { return item.stage === stage; }).settings;
                return __spreadArrays((settings.after || []), (settings.last ? assembleStages.filter(function (s) { return !s.settings.last; }).map(function (s) { return s.stage; }).data : []));
            });
            // for (const entry of result.data)
            // {
            //     const name = Object.getPrototypeOf(entry).constructor.name;
            //     Log.debug(name);
            // }
            return result.dataCopy;
        }
        AssembleStage.getStages = getStages;
    })(AssembleStage = RS.AssembleStage || (RS.AssembleStage = {}));
})(RS || (RS = {}));
/// <reference path="../module/Base.ts" />
var RS;
(function (RS) {
    var BuildStage;
    (function (BuildStage) {
        /**
         * Encapsulates a generic build stage.
         */
        var Base = /** @class */ (function () {
            function Base() {
            }
            Object.defineProperty(Base.prototype, "errorsAreFatal", {
                /**
                 * Gets if errors encountered when building a module are fatal and should end the whole process.
                 * If this is false, other modules may continue to be compiled after an error has been encountered.
                 */
                get: function () { return false; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Base.prototype, "runInParallel", {
                /**
                 * Gets if the executeModule function should be called in parallel for all modules.
                 * Ddependencies are still considered and processed serially.
                 */
                get: function () { return false; },
                enumerable: true,
                configurable: true
            });
            /**
             * Executes this build stage safely, catching and reporting any errors.
             */
            Base.prototype.executeSafe = function (moduleList) {
                return __awaiter(this, void 0, void 0, function () {
                    var err_3;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                return [4 /*yield*/, this.execute(moduleList)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2:
                                err_3 = _a.sent();
                                RS.Log.error(err_3.stack || err_3);
                                return [2 /*return*/, { workDone: true, errorCount: 1 }];
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Executes this build stage.
             */
            Base.prototype.execute = function (moduleList) {
                return __awaiter(this, void 0, void 0, function () {
                    var workDone, errorCount, batches, _i, _a, batch, promises, _loop_3, this_3, _b, _c, module_7, _d, _e, dep, moduleResult, err_4;
                    return __generator(this, function (_f) {
                        switch (_f.label) {
                            case 0:
                                RS.Log.pushContext("" + this);
                                workDone = false;
                                errorCount = 0;
                                if (!this.runInParallel) return [3 /*break*/, 5];
                                batches = RS.Module.getBatchSet(moduleList.data);
                                _i = 0, _a = batches.data;
                                _f.label = 1;
                            case 1:
                                if (!(_i < _a.length)) return [3 /*break*/, 4];
                                batch = _a[_i];
                                promises = [];
                                _loop_3 = function (module_7) {
                                    var promise = this_3.executeModule(module_7)
                                        .then(function (result) {
                                        workDone = result.workDone || workDone;
                                        errorCount += result.errorCount;
                                        return result;
                                    }, function (err) {
                                        RS.Log.error(err.stack || err, module_7.name);
                                        ++errorCount;
                                        return { workDone: false, errorCount: 1 };
                                    });
                                    promises.push(promise);
                                };
                                this_3 = this;
                                // Process each module in the batch in parallel
                                for (_b = 0, _c = batch.data; _b < _c.length; _b++) {
                                    module_7 = _c[_b];
                                    _loop_3(module_7);
                                }
                                return [4 /*yield*/, Promise.all(promises)];
                            case 2:
                                _f.sent();
                                if (errorCount > 0 && this.errorsAreFatal) {
                                    return [3 /*break*/, 4];
                                }
                                _f.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [3 /*break*/, 12];
                            case 5:
                                _d = 0, _e = moduleList.data;
                                _f.label = 6;
                            case 6:
                                if (!(_d < _e.length)) return [3 /*break*/, 12];
                                dep = _e[_d];
                                RS.Log.pushContext(dep.name);
                                _f.label = 7;
                            case 7:
                                _f.trys.push([7, 9, , 10]);
                                return [4 /*yield*/, this.executeModule(dep)];
                            case 8:
                                moduleResult = _f.sent();
                                workDone = moduleResult.workDone || workDone;
                                errorCount += moduleResult.errorCount;
                                return [3 /*break*/, 10];
                            case 9:
                                err_4 = _f.sent();
                                RS.Log.error(err_4.stack || err_4);
                                ++errorCount;
                                return [3 /*break*/, 10];
                            case 10:
                                RS.Log.popContext(dep.name);
                                if (errorCount > 0 && this.errorsAreFatal) {
                                    return [3 /*break*/, 12];
                                }
                                _f.label = 11;
                            case 11:
                                _d++;
                                return [3 /*break*/, 6];
                            case 12:
                                if (!workDone && errorCount === 0) {
                                    RS.Log.info("No changes found, no work to do");
                                }
                                RS.Log.popContext("" + this);
                                return [2 /*return*/, { workDone: workDone, errorCount: errorCount }];
                        }
                    });
                });
            };
            Base.prototype.toString = function () {
                var className = Object.getPrototypeOf(this).constructor.name;
                return this.name ? className + ":" + this.name : className;
            };
            return Base;
        }());
        BuildStage.Base = Base;
    })(BuildStage = RS.BuildStage || (RS.BuildStage = {}));
})(RS || (RS = {}));
/// <reference path="Base.ts" />
/// <reference path="../util/List.ts" />
var RS;
(function (RS) {
    var BuildStage;
    (function (BuildStage) {
        var buildStages = new RS.List();
        /**
         * Registers a build stage with the system.
         * @param settings
         * @param stage
         */
        function register(settings, stage) {
            buildStages.add({ settings: settings, stage: stage });
        }
        BuildStage.register = register;
        /**
         * Gets all build stages in order of execution.
         */
        function getStages() {
            var result = buildStages.map(function (s) { return s.stage; });
            result.resolveOrder(function (stage) { return buildStages.find(function (item) { return item.stage === stage; }).settings.before || []; }, function (stage) { return buildStages.find(function (item) { return item.stage === stage; }).settings.after || []; });
            return result.dataCopy;
        }
        BuildStage.getStages = getStages;
    })(BuildStage = RS.BuildStage || (RS.BuildStage = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.clean = RS.Build.task({ name: "clean", description: "Deletes all built files" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var allModules, _i, allModules_1, module_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Log.info("Cleaning all build directories...");
                        allModules = RS.Module.getAll();
                        _i = 0, allModules_1 = allModules;
                        _a.label = 1;
                    case 1:
                        if (!(_i < allModules_1.length)) return [3 /*break*/, 9];
                        module_8 = allModules_1[_i];
                        return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(module_8.path, "build"))];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(module_8.path, "src", "dependencies"))];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(module_8.path, "src", "generated"))];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(module_8.path, "buildsrc", "dependencies"))];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(module_8.path, "buildsrc", "generated"))];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(module_8.path, "externs", "generated"))];
                    case 7:
                        _a.sent();
                        module_8.checksumDB.clear();
                        _a.label = 8;
                    case 8:
                        _i++;
                        return [3 /*break*/, 1];
                    case 9: return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="../buildstage/BuildStage.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Clean.ts" />
/// <reference path="../util/EnvArg.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        var onlyEnvArg = new RS.EnvArg("ONLY", "");
        Tasks.build = RS.Build.task({ name: "build", after: [Tasks.clean], description: "Builds root module and its dependencies" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var config, rootModule, allDeps, buildStages, _i, buildStages_1, stage, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        return [4 /*yield*/, RS.Config.get()];
                    case 1:
                        config = _a.sent();
                        rootModule = RS.Module.getModule(RS.Config.activeConfig.rootModule);
                        if (rootModule == null) {
                            RS.Log.error("No root module found");
                            return [2 /*return*/];
                        }
                        allDeps = RS.Module.getDependencySet(__spreadArrays([RS.Config.activeConfig.rootModule], RS.Config.activeConfig.extraModules));
                        buildStages = RS.BuildStage.getStages();
                        _i = 0, buildStages_1 = buildStages;
                        _a.label = 2;
                    case 2:
                        if (!(_i < buildStages_1.length)) return [3 /*break*/, 5];
                        stage = buildStages_1[_i];
                        return [4 /*yield*/, stage.executeSafe(allDeps)];
                    case 3:
                        result = _a.sent();
                        if (result.errorCount > 0) {
                            throw new Error(result.errorCount + " errors when executing build stage " + stage);
                        }
                        _a.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5:
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        Tasks.buildOnly = RS.Build.task({ name: "build-only", after: [Tasks.clean], description: "Builds only the specified modules. Use: 'ONLY=MOD,MOD2 gulp build-only'" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var inputModulesRaw, inputModules, allDeps, buildStages, _i, buildStages_2, stage, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        if (!(onlyEnvArg.value !== onlyEnvArg.defaultValue)) return [3 /*break*/, 1];
                        inputModulesRaw = onlyEnvArg.value;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, RS.Input.readLine()];
                    case 2:
                        inputModulesRaw = _a.sent();
                        _a.label = 3;
                    case 3:
                        inputModules = inputModulesRaw.split(",").map(function (name) { return name.trim(); });
                        if (inputModules.length < 0 || inputModules[0].length === 0) {
                            return [2 /*return*/];
                        }
                        allDeps = RS.Module.getDependencySet(inputModules);
                        buildStages = RS.BuildStage.getStages();
                        _i = 0, buildStages_2 = buildStages;
                        _a.label = 4;
                    case 4:
                        if (!(_i < buildStages_2.length)) return [3 /*break*/, 7];
                        stage = buildStages_2[_i];
                        return [4 /*yield*/, stage.executeSafe(allDeps)];
                    case 5:
                        result = _a.sent();
                        if (result.errorCount > 0) {
                            throw new Error(result.errorCount + " errors when executing build stage " + stage);
                        }
                        _a.label = 6;
                    case 6:
                        _i++;
                        return [3 /*break*/, 4];
                    case 7:
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        Tasks.buildAll = RS.Build.task({ name: "build-all", after: [Tasks.clean], description: "Builds all modules" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var allModules, _i, _a, module_9, err_5, buildStages, _b, buildStages_3, stage, result, err_6;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        RS.Profile.reset();
                        allModules = RS.Module.getDependencySet(RS.Module.getAll().map(function (m) { return m.name; }));
                        // Compile build components
                        return [4 /*yield*/, RS.Module.compileBuildComponents(allModules)];
                    case 1:
                        // Compile build components
                        _c.sent();
                        _i = 0, _a = allModules.data;
                        _c.label = 2;
                    case 2:
                        if (!(_i < _a.length)) return [3 /*break*/, 8];
                        module_9 = _a[_i];
                        if (!!module_9.buildLoaded) return [3 /*break*/, 7];
                        RS.Log.pushContext(module_9.name);
                        _c.label = 3;
                    case 3:
                        _c.trys.push([3, 5, 6, 7]);
                        return [4 /*yield*/, module_9.loadBuild()];
                    case 4:
                        _c.sent();
                        return [3 /*break*/, 7];
                    case 5:
                        err_5 = _c.sent();
                        throw err_5;
                    case 6:
                        RS.Log.popContext(module_9.name);
                        return [7 /*endfinally*/];
                    case 7:
                        _i++;
                        return [3 /*break*/, 2];
                    case 8:
                        buildStages = RS.BuildStage.getStages();
                        _b = 0, buildStages_3 = buildStages;
                        _c.label = 9;
                    case 9:
                        if (!(_b < buildStages_3.length)) return [3 /*break*/, 14];
                        stage = buildStages_3[_b];
                        _c.label = 10;
                    case 10:
                        _c.trys.push([10, 12, , 13]);
                        return [4 /*yield*/, stage.executeSafe(allModules)];
                    case 11:
                        result = _c.sent();
                        if (result.errorCount > 0) {
                            throw new Error(result.errorCount + " errors when executing build stage " + stage);
                        }
                        return [3 /*break*/, 13];
                    case 12:
                        err_6 = _c.sent();
                        RS.Log.error(err_6.stack || err_6);
                        throw new Error("build-all failed");
                    case 13:
                        _b++;
                        return [3 /*break*/, 9];
                    case 14:
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="../assemblestage/AssembleStage.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Build.ts" />
/// <reference path="Clean.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.assemble = RS.Build.task({ name: "assemble", requires: [Tasks.build], after: [Tasks.clean] }, function () { return __awaiter(_this, void 0, void 0, function () {
            var config, rootModule, allDeps, settings, assembleStages, _i, assembleStages_1, stage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        return [4 /*yield*/, RS.Config.get()];
                    case 1:
                        config = _a.sent();
                        rootModule = RS.Module.getModule(RS.Config.activeConfig.rootModule);
                        if (rootModule == null) {
                            RS.Log.error("No root module found");
                            return [2 /*return*/];
                        }
                        allDeps = RS.Module.getDependencySet(__spreadArrays([RS.Config.activeConfig.rootModule], RS.Config.activeConfig.extraModules));
                        settings = {
                            outputDir: RS.Config.activeConfig.assemblePath
                        };
                        // Delete existing path
                        return [4 /*yield*/, RS.FileSystem.deletePath(settings.outputDir)];
                    case 2:
                        // Delete existing path
                        _a.sent();
                        assembleStages = RS.AssembleStage.getStages();
                        _i = 0, assembleStages_1 = assembleStages;
                        _a.label = 3;
                    case 3:
                        if (!(_i < assembleStages_1.length)) return [3 /*break*/, 6];
                        stage = assembleStages_1[_i];
                        return [4 /*yield*/, stage.execute(settings, allDeps)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        _i++;
                        return [3 /*break*/, 3];
                    case 6:
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="util/Task.ts" />
/// <reference path="util/Log.ts" />
/// <reference path="module/Loader.ts" />
/// <reference path="tasks/Assemble.ts" />
/// <reference path="Config.ts" />
module.exports.RS = RS;
var RS;
(function (RS) {
    /** Path to gulplib.js. */
    RS.__gulplib = __filename;
})(RS || (RS = {}));
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        // Set up the init task
        var Init = RS.Build.initTask(function () { return __awaiter(_this, void 0, void 0, function () {
            var compatibleNodes, nodeVersion, highestVer, i, ver, config, foundNum, i, l, arg, task, envArg, val, rootModule, allDeps_1, _i, _a, dep, err_7, moduleTasks, _loop_4, _b, moduleTasks_1, task, requiredTasks, _c, requiredTasks_1, taskName, task;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        compatibleNodes = [RS.Version(10), RS.Version(11)];
                        nodeVersion = RS.Version.fromString(process.version);
                        if (!compatibleNodes.some(function (compatibleVersion) { return RS.Version.compatible(nodeVersion, compatibleVersion); })) {
                            highestVer = compatibleNodes[0];
                            for (i = 1; i < compatibleNodes.length; i++) {
                                ver = compatibleNodes[i];
                                if (RS.Version.greaterThan(ver, highestVer)) {
                                    highestVer = ver;
                                }
                            }
                            RS.Log.warn("Synergy does not support Node.js " + process.version + "; please install Node.js ^v" + RS.Version.toString(highestVer) + ".");
                            RS.Log.warn("Some things may work incorrectly, or not at all.");
                        }
                        RS.Profile.reset();
                        return [4 /*yield*/, RS.FileSystem.rebuildCache()];
                    case 1:
                        _d.sent();
                        return [4 /*yield*/, RS.Config.get()];
                    case 2:
                        config = _d.sent();
                        // Load modules
                        return [4 /*yield*/, RS.Module.loadModules()];
                    case 3:
                        // Load modules
                        _d.sent();
                        // Identify config tasks
                        if (config.tasks) {
                            foundNum = 0;
                            for (i = 2, l = process.argv.length; i < l; ++i) {
                                arg = process.argv[i];
                                task = void 0;
                                if (task = config.tasks[arg]) {
                                    if (foundNum === 1) {
                                        RS.Log.error("Attempting to run more than one task from the tasks section of the config. Only the first one will execute!");
                                        break;
                                    }
                                    RS.Config.activeConfig = {
                                        rootModule: task.rootModule || config.rootModule,
                                        extraModules: __spreadArrays((config.extraModules || []), (task.extraModules || [])),
                                        moduleConfigs: __assign(__assign({}, (config.moduleConfigs || {})), (task.moduleConfigs || {})),
                                        envArgs: __assign(__assign({}, (config.envArgs || {})), (task.envArgs || {})),
                                        assemblePath: task.assemblePath || config.assemblePath,
                                        sourceMap: task.sourceMap != null ? task.sourceMap : config.sourceMap,
                                        defines: __assign(__assign({}, (config.defines || {})), (task.defines || {}))
                                    };
                                    RS.Build.task({
                                        name: arg,
                                        requires: (task.requires || []).map(function (str) { return RS.Build.getTask(str); }),
                                        after: (task.after || []).map(function (str) { return RS.Build.getTask(str); })
                                    });
                                    foundNum++;
                                }
                            }
                        }
                        if (RS.Config.activeConfig == null) {
                            RS.Config.activeConfig = config;
                        }
                        // Apply env args
                        for (envArg in RS.Config.activeConfig.envArgs) {
                            val = RS.Config.activeConfig.envArgs[envArg];
                            process.env[envArg] = val;
                        }
                        rootModule = RS.Module.getModule(RS.Config.activeConfig.rootModule);
                        if (!(rootModule != null)) return [3 /*break*/, 12];
                        RS.Log.info("Using '" + rootModule.name + "' as root module");
                        allDeps_1 = RS.Module.getDependencySet(__spreadArrays([RS.Config.activeConfig.rootModule], RS.Config.activeConfig.extraModules));
                        // Compile build components
                        return [4 /*yield*/, RS.Module.compileBuildComponents(allDeps_1)];
                    case 4:
                        // Compile build components
                        _d.sent();
                        _i = 0, _a = allDeps_1.data;
                        _d.label = 5;
                    case 5:
                        if (!(_i < _a.length)) return [3 /*break*/, 11];
                        dep = _a[_i];
                        if (!!dep.buildLoaded) return [3 /*break*/, 10];
                        RS.Log.pushContext(dep.name);
                        _d.label = 6;
                    case 6:
                        _d.trys.push([6, 8, 9, 10]);
                        return [4 /*yield*/, dep.loadBuild()];
                    case 7:
                        _d.sent();
                        return [3 /*break*/, 10];
                    case 8:
                        err_7 = _d.sent();
                        throw err_7;
                    case 9:
                        RS.Log.popContext(dep.name);
                        return [7 /*endfinally*/];
                    case 10:
                        _i++;
                        return [3 /*break*/, 5];
                    case 11:
                        moduleTasks = RS.Module.Base.exportedTasks;
                        if (moduleTasks) {
                            _loop_4 = function (task) {
                                RS.Build.task(task.taskSettings, function () { return __awaiter(_this, void 0, void 0, function () {
                                    var workDone, _i, _a, dep, promiseMaybe, workDoneMaybe;
                                    return __generator(this, function (_b) {
                                        switch (_b.label) {
                                            case 0:
                                                workDone = false;
                                                _i = 0, _a = allDeps_1.data;
                                                _b.label = 1;
                                            case 1:
                                                if (!(_i < _a.length)) return [3 /*break*/, 4];
                                                dep = _a[_i];
                                                promiseMaybe = task.function.call(dep);
                                                if (!(promiseMaybe instanceof Promise)) return [3 /*break*/, 3];
                                                return [4 /*yield*/, promiseMaybe];
                                            case 2:
                                                workDoneMaybe = _b.sent();
                                                if (RS.Is.boolean(workDoneMaybe)) {
                                                    workDone = workDone || workDoneMaybe;
                                                }
                                                _b.label = 3;
                                            case 3:
                                                _i++;
                                                return [3 /*break*/, 1];
                                            case 4:
                                                if (!workDone) {
                                                    RS.Log.info("No changes detected, no work to do!");
                                                }
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                            };
                            for (_b = 0, moduleTasks_1 = moduleTasks; _b < moduleTasks_1.length; _b++) {
                                task = moduleTasks_1[_b];
                                _loop_4(task);
                            }
                        }
                        _d.label = 12;
                    case 12:
                        requiredTasks = RS.Build.identifyTasks();
                        if (requiredTasks.length == 0) {
                            requiredTasks.push("default");
                        }
                        for (_c = 0, requiredTasks_1 = requiredTasks; _c < requiredTasks_1.length; _c++) {
                            taskName = requiredTasks_1[_c];
                            task = RS.Build.getTask(taskName);
                            if (task) {
                                RS.Build.requireTask(task);
                            }
                            else {
                                RS.Log.warn("Unknown task '" + taskName + "'!");
                            }
                        }
                        RS.Build.exportAllTasks();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        // Default task
        Tasks.init = RS.Build.task({ name: "default", requires: [Tasks.assemble] });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var TypeScript;
    (function (TypeScript) {
        // Accurate as of TypeScript 3.7.4
        // This should be updated if subsequent TS compiler updates change the helpers.
        TypeScript.helpers = "var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\nvar __generator = (this && this.__generator) || function (thisArg, body) {\n    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;\n    return g = { next: verb(0), \"throw\": verb(1), \"return\": verb(2) }, typeof Symbol === \"function\" && (g[Symbol.iterator] = function() { return this; }), g;\n    function verb(n) { return function (v) { return step([n, v]); }; }\n    function step(op) {\n        if (f) throw new TypeError(\"Generator is already executing.\");\n        while (_) try {\n            if (f = 1, y && (t = op[0] & 2 ? y[\"return\"] : op[0] ? y[\"throw\"] || ((t = y[\"return\"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;\n            if (y = 0, t) op = [op[0] & 2, t.value];\n            switch (op[0]) {\n                case 0: case 1: t = op; break;\n                case 4: _.label++; return { value: op[1], done: false };\n                case 5: _.label++; y = op[1]; op = [0]; continue;\n                case 7: op = _.ops.pop(); _.trys.pop(); continue;\n                default:\n                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }\n                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }\n                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }\n                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }\n                    if (t[2]) _.ops.pop();\n                    _.trys.pop(); continue;\n            }\n            op = body.call(thisArg, _);\n        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }\n        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };\n    }\n};\nvar __assign = (this && this.__assign) || Object.assign || function(t) {\n    for (var s, i = 1, n = arguments.length; i < n; i++) {\n        s = arguments[i];\n        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))\n            t[p] = s[p];\n    }\n    return t;\n};\nvar __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {\n    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;\n    if (typeof Reflect === \"object\" && typeof Reflect.decorate === \"function\") r = Reflect.decorate(decorators, target, key, desc);\n    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;\n    return c > 3 && r && Object.defineProperty(target, key, r), r;\n};\nvar __extends = (this && this.__extends) || (function () {\n    var extendStatics = Object.setPrototypeOf ||\n        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\nvar __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {\n    if (Object.defineProperty) { Object.defineProperty(cooked, \"raw\", { value: raw }); } else { cooked.raw = raw; }\n    return cooked;\n};\nvar __multiDecorate = (this && this.__multiDecorate) || function(map) {\n    var keys = Object.keys(map);\n    for (var i = 0; i < keys.length; i++) {\n        var key = keys[i];\n        var obj = map[key];\n        __decorate(obj.decorators, obj.target, key, obj.desc);\n    }\n};\nvar __spreadArrays = (this && this.__spreadArrays) || function() {\n    for (var s = 0, i = 0, il = arguments.length; i < il; i++)\n        s += arguments[i].length;\n    for (var r = Array(s), k = 0, i = 0; i < il; i++)\n        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++,\n        k++)\n            r[k] = a[j];\n    return r;\n};\nvar __param = (this && this.__param) || function (paramIndex, decorator) {\n    return function (target, key) { decorator(target, key, paramIndex); }\n};";
    })(TypeScript = RS.TypeScript || (RS.TypeScript = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Assembler;
    (function (Assembler) {
        var generatesourcemap = require("generate-source-map");
        var SourcemapMode;
        (function (SourcemapMode) {
            SourcemapMode["None"] = "none";
            SourcemapMode["WhenSet"] = "whenset";
            SourcemapMode["Always"] = "always";
            SourcemapMode["Default"] = "whenset";
        })(SourcemapMode || (SourcemapMode = {}));
        Assembler.sourcemapMode = new RS.EnvArg("SOURCEMAPS", SourcemapMode.Default);
        /**
         * Generates an identity sourcemap from the specified source.
         * @param sourceText source code
         * @param sourcePath source file name
         */
        function generateIdentitySourcemap(sourceText, sourcePath) {
            RS.Profile.enter("generateIdentitySourcemap");
            var gen = generatesourcemap({ source: sourceText, sourceFile: sourcePath });
            gen.setSourceContent(sourcePath, sourceText);
            RS.Profile.exit("generateIdentitySourcemap");
            return gen.toString();
        }
        Assembler.generateIdentitySourcemap = generateIdentitySourcemap;
        /**
         * Attempts to resolve an identity sourcemap from the specified source and path, using the given module for cache.
         * @param sourceText source code
         * @param sourcePath source file name
         */
        function resolveIdentitySourceMap(module, sourceText, sourcePath) {
            return __awaiter(this, void 0, void 0, function () {
                var checksum, cachedMapName, cachedMapPath, sourceMap;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            checksum = RS.Checksum.getSimple(sourceText);
                            cachedMapName = RS.Path.replaceExtension(RS.Path.baseName(sourcePath), "." + checksum + ".js.map");
                            cachedMapPath = RS.Path.combine(module.path, "build", cachedMapName);
                            return [4 /*yield*/, RS.FileSystem.fileExists(cachedMapPath)];
                        case 1:
                            if (!_a.sent()) return [3 /*break*/, 3];
                            return [4 /*yield*/, RS.FileSystem.readFile(cachedMapPath)];
                        case 2: return [2 /*return*/, _a.sent()];
                        case 3:
                            RS.Log.debug("Generating identity sourcemap for '" + sourcePath + "'", module.name);
                            sourceMap = Assembler.generateIdentitySourcemap(sourceText, sourcePath);
                            return [4 /*yield*/, RS.FileSystem.writeFile(cachedMapPath, sourceMap)];
                        case 4:
                            _a.sent();
                            return [2 /*return*/, sourceMap];
                    }
                });
            });
        }
        Assembler.resolveIdentitySourceMap = resolveIdentitySourceMap;
        /**
         * Assembles a single JavaScript file from the specified set of code sources.
         * Optionally produces a sourcemap.
         * Optionally substitutes strings within string literals.
         * Optionally minifies the output code.
         * @param dir
         * @param fileID
         * @param sources
         * @param settings
         */
        function assembleJS(dir, fileID, sources, settings) {
            return __awaiter(this, void 0, void 0, function () {
                var compiledJSPath, compiledMapPath, tmpDir_1, assembledSource, mappings, concat, helpersSourceMap, _i, sources_1, _a, name_10, content, sourceMap, content_1, compiledFile;
                var _this = this;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            RS.Profile.enter("assembleJS");
                            if (!RS.Config.activeConfig.sourceMap) {
                                settings.withSourcemaps = false;
                            }
                            // Allow environment overriding of config
                            switch (Assembler.sourcemapMode.value) {
                                case SourcemapMode.None:
                                    settings.withSourcemaps = false;
                                    break;
                                case SourcemapMode.Always:
                                    settings.withSourcemaps = true;
                                    break;
                                case SourcemapMode.WhenSet:
                                    break;
                                default:
                                    RS.Log.warn("Unknown sourcemap mode \"" + Assembler.sourcemapMode.value + "\"");
                                    break;
                            }
                            compiledJSPath = RS.Path.combine(dir, fileID + ".js");
                            compiledMapPath = settings.withSourcemaps ? RS.Path.combine(dir, fileID + ".js.map") : null;
                            if (!(settings.replacements != null && Object.keys(settings.replacements).length > 0)) return [3 /*break*/, 6];
                            RS.Profile.enter("assembleJS.replace");
                            tmpDir_1 = RS.Path.combine(dir, fileID + ".tmp");
                            return [4 /*yield*/, RS.FileSystem.createPath(tmpDir_1)];
                        case 1:
                            _b.sent();
                            _b.label = 2;
                        case 2:
                            _b.trys.push([2, , 4, 6]);
                            // Do replacements per-source in parallel
                            // This lets us:
                            //  A) skip the majority of files, which won't need expensive replacements done
                            //  B) do the remaining files in parallel
                            return [4 /*yield*/, Promise.all(sources.map(function (source) { return __awaiter(_this, void 0, void 0, function () {
                                    var containsKey, key, sourcePath, sourceMapPath, _a, _b;
                                    return __generator(this, function (_c) {
                                        switch (_c.label) {
                                            case 0:
                                                containsKey = false;
                                                for (key in settings.replacements) {
                                                    if (source.content.indexOf(key) !== -1) {
                                                        containsKey = true;
                                                        break;
                                                    }
                                                }
                                                if (!containsKey) {
                                                    return [2 /*return*/];
                                                }
                                                sourcePath = RS.Path.combine(tmpDir_1, source.name + ".js");
                                                return [4 /*yield*/, RS.FileSystem.writeFile(sourcePath, source.content)];
                                            case 1:
                                                _c.sent();
                                                sourceMapPath = null;
                                                if (!source.sourceMap) return [3 /*break*/, 3];
                                                sourceMapPath = sourcePath + ".map";
                                                return [4 /*yield*/, RS.FileSystem.writeFile(sourceMapPath, source.sourceMap)];
                                            case 2:
                                                _c.sent();
                                                _c.label = 3;
                                            case 3: return [4 /*yield*/, RS.Command.run("node", __spreadArrays([
                                                    "--max-old-space-size=4096",
                                                    RS.Path.combine(__dirname, "jsreplace.js"),
                                                    sourcePath,
                                                    sourceMapPath
                                                ], Object.keys(settings.replacements).map(function (key) { return key + "=" + settings.replacements[key]; })), undefined, false)];
                                            case 4:
                                                _c.sent();
                                                RS.FileSystem.cache.set(sourcePath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                                                _a = source;
                                                return [4 /*yield*/, RS.FileSystem.readFile(sourcePath)];
                                            case 5:
                                                _a.content = _c.sent();
                                                if (!(sourceMapPath != null)) return [3 /*break*/, 7];
                                                RS.FileSystem.cache.set(sourceMapPath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                                                _b = source;
                                                return [4 /*yield*/, RS.FileSystem.readFile(sourceMapPath)];
                                            case 6:
                                                _b.sourceMap = _c.sent();
                                                _c.label = 7;
                                            case 7: return [2 /*return*/];
                                        }
                                    });
                                }); }))];
                        case 3:
                            // Do replacements per-source in parallel
                            // This lets us:
                            //  A) skip the majority of files, which won't need expensive replacements done
                            //  B) do the remaining files in parallel
                            _b.sent();
                            return [3 /*break*/, 6];
                        case 4:
                            RS.Profile.exit("assembleJS.replace");
                            return [4 /*yield*/, RS.FileSystem.deletePath(tmpDir_1)];
                        case 5:
                            _b.sent();
                            return [7 /*endfinally*/];
                        case 6:
                            if (settings.withSourcemaps) {
                                RS.Profile.enter("assembleJS.concat");
                                concat = new Assembler.SourceMapBuilder();
                                if (settings.withHelpers) {
                                    RS.Profile.enter("assembleJS.concat.helpers");
                                    helpersSourceMap = generateIdentitySourcemap(RS.TypeScript.helpers, "helpers.js");
                                    concat.addSource(RS.TypeScript.helpers, RS.JSON.parse(helpersSourceMap));
                                    RS.Profile.exit("assembleJS.concat.helpers");
                                }
                                for (_i = 0, sources_1 = sources; _i < sources_1.length; _i++) {
                                    _a = sources_1[_i], name_10 = _a.name, content = _a.content, sourceMap = _a.sourceMap;
                                    RS.Profile.enter("assembleJS.concat.source");
                                    concat.addSource(content, RS.JSON.parse(sourceMap));
                                    RS.Profile.exit("assembleJS.concat.source");
                                }
                                concat.addLine("//# sourceMappingURL=" + fileID + ".js.map");
                                assembledSource = concat.toContent().toString();
                                mappings = concat.toSourceMap();
                                RS.Profile.exit("assembleJS.concat");
                            }
                            else {
                                RS.Profile.enter("assembleJS.concat");
                                content_1 = [];
                                if (settings.withHelpers) {
                                    RS.Profile.enter("assembleJS.concat.helpers");
                                    content_1.push(RS.TypeScript.helpers);
                                    RS.Profile.exit("assembleJS.concat.helpers");
                                }
                                sources.forEach(function (thisSource) { return content_1.push(thisSource.content); });
                                assembledSource = content_1.join("\n");
                                mappings = null;
                                RS.Profile.exit("assembleJS.concat");
                            }
                            compiledFile = new RS.Build.CompiledFile(assembledSource, mappings);
                            return [4 /*yield*/, compiledFile.write(compiledJSPath, compiledMapPath)];
                        case 7:
                            _b.sent();
                            if (!(settings.replacements != null)) return [3 /*break*/, 9];
                            return [4 /*yield*/, RS.JSReplace.replace(compiledJSPath, compiledMapPath, settings.replacements)];
                        case 8:
                            _b.sent();
                            _b.label = 9;
                        case 9:
                            if (!settings.minify) return [3 /*break*/, 11];
                            return [4 /*yield*/, RS.JSMinify.minify(compiledJSPath, compiledMapPath)];
                        case 10:
                            _b.sent();
                            _b.label = 11;
                        case 11:
                            RS.Profile.exit("assembleJS");
                            return [2 /*return*/];
                    }
                });
            });
        }
        Assembler.assembleJS = assembleJS;
        /**
         * Assembles a single JavaScript file from the specified set of code sources.
         * Optionally produces a sourcemap.
         * Optionally substitutes strings within string literals.
         * Optionally minifies the output code.
         * @param dir
         * @param fileID
         * @param source
         * @param settings
         */
        function assembleDts(dir, fileID, source, settings) {
            return __awaiter(this, void 0, void 0, function () {
                var concat, _i, source_1, _a, name_11, dtsContent, content, sourceMap, compiledFile, compiledJSPath;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            RS.Profile.enter("assembleDts.concat");
                            concat = new Assembler.SourceMapBuilder();
                            for (_i = 0, source_1 = source; _i < source_1.length; _i++) {
                                _a = source_1[_i], name_11 = _a.name, dtsContent = _a.dtsContent, content = _a.content, sourceMap = _a.sourceMap;
                                concat.addSource(Buffer.from(dtsContent));
                            }
                            RS.Profile.exit("assembleDts.concat");
                            compiledFile = new RS.Build.CompiledFile(concat.toContent().toString(), null);
                            compiledJSPath = RS.Path.combine(dir, fileID + ".d.ts");
                            return [4 /*yield*/, compiledFile.write(compiledJSPath, null)];
                        case 1:
                            _b.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Assembler.assembleDts = assembleDts;
        /**
         * Gathers all third party dependencies for a module and inserts the source for them into the specified code source map.
         * @param module
         * @param source
         */
        function gatherThirdPartyDependencies(module, source) {
            return __awaiter(this, void 0, void 0, function () {
                var _a, _b, _i, depName, bowerInfoPath, bowerInfo, mainPackagePath, mainPath, mainText, mapPath, map, err_8, _c, _d, _e, depName, depInfo, sourceText, map, err_9, _f, _g, _h, depName, nodePath, _j, inPath, outPath, srcmapPath, name_12, sourceText, map;
                return __generator(this, function (_k) {
                    switch (_k.label) {
                        case 0:
                            if (!module.info.bowerdependencies) return [3 /*break*/, 12];
                            _a = [];
                            for (_b in module.info.bowerdependencies)
                                _a.push(_b);
                            _i = 0;
                            _k.label = 1;
                        case 1:
                            if (!(_i < _a.length)) return [3 /*break*/, 12];
                            depName = _a[_i];
                            if (!!source[depName]) return [3 /*break*/, 11];
                            _k.label = 2;
                        case 2:
                            _k.trys.push([2, 10, , 11]);
                            bowerInfoPath = RS.Path.combine("bower_components", depName, "bower.json");
                            return [4 /*yield*/, RS.JSON.parseFile(bowerInfoPath)];
                        case 3:
                            bowerInfo = _k.sent();
                            mainPackagePath = bowerInfo["main"];
                            mainPath = RS.Path.combine("bower_components", depName, mainPackagePath);
                            return [4 /*yield*/, RS.FileSystem.readFile(mainPath)];
                        case 4:
                            mainText = _k.sent();
                            mapPath = RS.Path.replaceExtension(mainPath, ".js.map");
                            map = void 0;
                            return [4 /*yield*/, RS.FileSystem.fileExists(mapPath)];
                        case 5:
                            if (!_k.sent()) return [3 /*break*/, 7];
                            return [4 /*yield*/, RS.FileSystem.readFile(mapPath)];
                        case 6:
                            map = _k.sent();
                            return [3 /*break*/, 9];
                        case 7: return [4 /*yield*/, resolveIdentitySourceMap(module, mainText, mainPackagePath)];
                        case 8:
                            map = _k.sent();
                            _k.label = 9;
                        case 9:
                            source[depName] = { name: depName, content: mainText, sourceMap: map, sourceMapDir: null };
                            return [3 /*break*/, 11];
                        case 10:
                            err_8 = _k.sent();
                            throw new Error("Couldn't locate bower.json file for bower package '" + depName + "': " + err_8);
                        case 11:
                            _i++;
                            return [3 /*break*/, 1];
                        case 12:
                            if (!module.info.thirdparty) return [3 /*break*/, 22];
                            _c = [];
                            for (_d in module.info.thirdparty)
                                _c.push(_d);
                            _e = 0;
                            _k.label = 13;
                        case 13:
                            if (!(_e < _c.length)) return [3 /*break*/, 22];
                            depName = _c[_e];
                            depInfo = module.info.thirdparty[depName];
                            if (!!source[depName]) return [3 /*break*/, 21];
                            _k.label = 14;
                        case 14:
                            _k.trys.push([14, 20, , 21]);
                            return [4 /*yield*/, RS.FileSystem.readFile(RS.Path.combine(module.path, "thirdparty", depInfo.src))];
                        case 15:
                            sourceText = _k.sent();
                            map = void 0;
                            if (!depInfo.srcmap) return [3 /*break*/, 17];
                            return [4 /*yield*/, RS.FileSystem.readFile(RS.Path.combine(module.path, "thirdparty", depInfo.srcmap))];
                        case 16:
                            map = _k.sent();
                            return [3 /*break*/, 19];
                        case 17: return [4 /*yield*/, resolveIdentitySourceMap(module, sourceText, depInfo.src)];
                        case 18:
                            map = _k.sent();
                            _k.label = 19;
                        case 19:
                            source[depName] = { name: depName, content: sourceText, sourceMap: map, sourceMapDir: null };
                            return [3 /*break*/, 21];
                        case 20:
                            err_9 = _k.sent();
                            throw new Error("Couldn't load third party lib '" + depName + "': " + err_9);
                        case 21:
                            _e++;
                            return [3 /*break*/, 13];
                        case 22:
                            if (!module.info.nodedependencies) return [3 /*break*/, 37];
                            _f = [];
                            for (_g in module.info.nodedependencies)
                                _f.push(_g);
                            _h = 0;
                            _k.label = 23;
                        case 23:
                            if (!(_h < _f.length)) return [3 /*break*/, 37];
                            depName = _f[_h];
                            nodePath = RS.Path.combine("node_modules", depName);
                            _j = !source[depName];
                            if (!_j) return [3 /*break*/, 25];
                            return [4 /*yield*/, RS.FileSystem.fileExists(nodePath)];
                        case 24:
                            _j = (_k.sent());
                            _k.label = 25;
                        case 25:
                            if (!_j) return [3 /*break*/, 36];
                            inPath = RS.Path.combine(module.path, "build", "in.js");
                            outPath = RS.Path.combine(module.path, "build", "out.js");
                            srcmapPath = RS.Path.replaceExtension(outPath, ".js.map");
                            // delete existing if exists
                            return [4 /*yield*/, RS.FileSystem.deletePath(inPath)];
                        case 26:
                            // delete existing if exists
                            _k.sent();
                            return [4 /*yield*/, RS.FileSystem.deletePath(outPath)];
                        case 27:
                            _k.sent();
                            return [4 /*yield*/, RS.FileSystem.deletePath(srcmapPath)];
                        case 28:
                            _k.sent();
                            name_12 = depName.replace(/[^a-zA-Z0-9_]+/g, "");
                            // remove numbers from start if any
                            name_12 = name_12.replace(/^[0-9]+/g, "");
                            return [4 /*yield*/, RS.FileSystem.writeFile(inPath, "export var " + name_12 + " = require(\"" + depName + "\");")];
                        case 29:
                            _k.sent();
                            return [4 /*yield*/, RS.Command.run("npx", [
                                    "webpack",
                                    "--mode", "production",
                                    "--devtool", "source-map",
                                    "--entry", RS.Path.resolve(inPath),
                                    "--output", outPath,
                                    "--output-library-target", "global",
                                    "--output-source-map-filename", RS.Path.baseName(srcmapPath) //relative to output path
                                ], null, false)];
                        case 30:
                            _k.sent();
                            // set filesystem cache so readfile can read the generated files
                            RS.FileSystem.cache.set(outPath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                            RS.FileSystem.cache.set(srcmapPath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                            return [4 /*yield*/, RS.FileSystem.readFile(outPath)];
                        case 31:
                            sourceText = _k.sent();
                            return [4 /*yield*/, RS.FileSystem.readFile(srcmapPath)];
                        case 32:
                            map = _k.sent();
                            source[depName] = { name: depName, content: sourceText, sourceMap: map, sourceMapDir: null };
                            // clean up generated files
                            return [4 /*yield*/, RS.FileSystem.deletePath(inPath)];
                        case 33:
                            // clean up generated files
                            _k.sent();
                            return [4 /*yield*/, RS.FileSystem.deletePath(outPath)];
                        case 34:
                            _k.sent();
                            return [4 /*yield*/, RS.FileSystem.deletePath(srcmapPath)];
                        case 35:
                            _k.sent();
                            _k.label = 36;
                        case 36:
                            _h++;
                            return [3 /*break*/, 23];
                        case 37: return [2 /*return*/];
                    }
                });
            });
        }
        Assembler.gatherThirdPartyDependencies = gatherThirdPartyDependencies;
    })(Assembler = RS.Assembler || (RS.Assembler = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Assembler;
    (function (Assembler) {
        var CodeSources;
        (function (CodeSources) {
            function addGulp(codeSources) {
                return __awaiter(this, void 0, void 0, function () {
                    var file, map;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                file = __filename;
                                map = file + ".map";
                                return [4 /*yield*/, addFile(codeSources, file, map)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            }
            CodeSources.addGulp = addGulp;
            function addUnit(codeSources, unit) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, _b, _c;
                    return __generator(this, function (_d) {
                        switch (_d.label) {
                            case 0:
                                _b = (_a = codeSources).push;
                                _c = {
                                    name: unit.name
                                };
                                return [4 /*yield*/, RS.FileSystem.readFile(unit.outJSFile)];
                            case 1:
                                _c.content = _d.sent();
                                return [4 /*yield*/, RS.FileSystem.readFile(unit.outMapFile)];
                            case 2:
                                _b.apply(_a, [(_c.sourceMap = _d.sent(),
                                        _c.sourceMapDir = unit.path,
                                        _c)]);
                                return [2 /*return*/];
                        }
                    });
                });
            }
            CodeSources.addUnit = addUnit;
            function addModule(codeSources, module, unitName) {
                return __awaiter(this, void 0, void 0, function () {
                    var unit;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                unit = module.getCompilationUnit(unitName);
                                if (!unit) {
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, addUnit(codeSources, unit)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            }
            CodeSources.addModule = addModule;
            function addModules(codeSources, p2, unitName) {
                return __awaiter(this, void 0, void 0, function () {
                    var modules, _i, modules_1, module_10;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                modules = RS.Is.array(p2) ? p2 : p2.dependencies;
                                _i = 0, modules_1 = modules;
                                _a.label = 1;
                            case 1:
                                if (!(_i < modules_1.length)) return [3 /*break*/, 4];
                                module_10 = modules_1[_i];
                                return [4 /*yield*/, addModule(codeSources, module_10, unitName)];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/];
                        }
                    });
                });
            }
            CodeSources.addModules = addModules;
            function addFile(codeSources, file, sourceMapFile) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, _b, _c, _d;
                    return __generator(this, function (_e) {
                        switch (_e.label) {
                            case 0:
                                _b = (_a = codeSources).push;
                                _c = {
                                    name: RS.Path.baseName(file)
                                };
                                return [4 /*yield*/, RS.FileSystem.readFile(file)];
                            case 1:
                                _c.content = _e.sent();
                                if (!sourceMapFile) return [3 /*break*/, 3];
                                return [4 /*yield*/, RS.FileSystem.readFile(sourceMapFile)];
                            case 2:
                                _d = _e.sent();
                                return [3 /*break*/, 4];
                            case 3:
                                _d = null;
                                _e.label = 4;
                            case 4:
                                _b.apply(_a, [(_c.sourceMap = _d,
                                        _c.sourceMapDir = sourceMapFile ? RS.Path.directoryName(sourceMapFile) : null,
                                        _c)]);
                                return [2 /*return*/];
                        }
                    });
                });
            }
            CodeSources.addFile = addFile;
        })(CodeSources = Assembler.CodeSources || (Assembler.CodeSources = {}));
    })(Assembler = RS.Assembler || (RS.Assembler = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Assembler;
    (function (Assembler) {
        // Adapted with love from https://stackoverflow.com/questions/29905373/how-to-create-sourcemaps-for-concatenated-files
        /** Source-map spec version. */
        var specVersion = 3;
        var emptySourceMap = { version: specVersion, sources: [], mappings: "" };
        /** Maps a char byte to its integer offset in charSet. */
        var charToInteger = Buffer.alloc(256);
        /** Maps integer offsets to their char byte in charSet. */
        var integerToChar = Buffer.alloc(64);
        // Chars
        /** Buffer filler byte */
        var emptyByte = 255;
        /** Char , */
        var commaByte = 44;
        /** Char ; */
        var semiColonByte = 59;
        /** Char \n */
        var newLineByte = 10;
        /** New-line character */
        var newLineStr = "\n";
        // Populate map buffers
        charToInteger.fill(emptyByte);
        var charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        for (var i = 0; i < charSet.length; i++) {
            var code = charSet.charCodeAt(i);
            charToInteger[code] = i;
            integerToChar[i] = code;
        }
        ;
        /** A buffer which expands in size as necessary. */
        var DynamicBuffer = /** @class */ (function () {
            function DynamicBuffer() {
                this.__buffer = Buffer.alloc(512);
                this.__size = 0;
            }
            /** Decodes this DynamicBuffer into a UTF8 string. */
            DynamicBuffer.prototype.toString = function (encoding, start, end) {
                if (encoding === void 0) { encoding = "utf8"; }
                if (start === void 0) { start = 0; }
                if (end == null) {
                    // Use apparent size
                    end = this.__size;
                }
                else {
                    // Clamp 'end' to apparent size
                    end = Math.min(end, this.__size);
                }
                return this.__buffer.toString(encoding, start, end);
            };
            /** Ensures that this DynamicBuffer is at least 'capacity' in size. */
            DynamicBuffer.prototype.ensureCapacity = function (capacity) {
                if (this.__buffer.length >= capacity) {
                    return;
                }
                var oldBuffer = this.__buffer;
                var newSize = Math.max(oldBuffer.length * 2, capacity);
                this.__buffer = Buffer.alloc(newSize);
                oldBuffer.copy(this.__buffer);
            };
            /** Adds the single byte 'b' to the end of this DynamicBuffer, expanding as necessary. */
            DynamicBuffer.prototype.addByte = function (b) {
                this.ensureCapacity(this.__size + 1);
                this.__buffer[this.__size++] = b;
            };
            /** Adds the variable-length base-64 quantity 'num' to the end of this DynamicBuffer, expanding as necessary. */
            DynamicBuffer.prototype.addVLQ = function (num) {
                var clamped;
                if (num < 0) {
                    num = (-num << 1) | 1;
                }
                else {
                    num <<= 1;
                }
                do {
                    clamped = num & 31;
                    num >>= 5;
                    if (num > 0) {
                        clamped |= 32;
                    }
                    this.addByte(integerToChar[clamped]);
                } while (num > 0);
            };
            /** Appends the string 's' to the end of this DynamicBuffer, expanding as necessary. */
            DynamicBuffer.prototype.addString = function (s) {
                var l = Buffer.byteLength(s);
                this.ensureCapacity(this.__size + l);
                this.__buffer.write(s, this.__size);
                this.__size += l;
            };
            /** Appends the buffer 'b' to the end of this DynamicBuffer, expanding as necessary. */
            DynamicBuffer.prototype.addBuffer = function (b) {
                this.ensureCapacity(this.__size + b.length);
                b.copy(this.__buffer, this.__size);
                this.__size += b.length;
            };
            /** Returns a standard static Buffer with the contents of this DynamicBuffer. */
            DynamicBuffer.prototype.toBuffer = function () {
                return this.__buffer.slice(0, this.__size);
            };
            return DynamicBuffer;
        }());
        /** Counts the number of new-lines in the given Buffer. */
        function countBufferNL(b) {
            var res = 0;
            for (var i = 0; i < b.length; i++) {
                if (b[i] === newLineByte) {
                    res++;
                }
            }
            return res;
        }
        /** Counts the number of new-lines in the given string. */
        function countStringNL(s) {
            var res = 0;
            for (var i = 0; i < s.length; i++) {
                if (s[i] === newLineStr) {
                    res++;
                }
            }
            return res;
        }
        var SourceMapBuilder = /** @class */ (function () {
            function SourceMapBuilder() {
                this.__outputBuffer = new DynamicBuffer();
                this.__sources = new Array();
                this.__sourcesContent = new Array();
                this.__mappings = new DynamicBuffer();
                this.__lastSourceIndex = 0;
                this.__lastSourceLine = 0;
                this.__lastSourceCol = 0;
            }
            /** Adds a single line of text. */
            SourceMapBuilder.prototype.addLine = function (text) {
                this.__outputBuffer.addString(text);
                this.__outputBuffer.addByte(newLineByte);
                this.__mappings.addByte(semiColonByte);
            };
            /** Adds source content, optionally using the given sourceMap for the content. */
            SourceMapBuilder.prototype.addSource = function (content, sourceMap) {
                var _this = this;
                if (sourceMap === void 0) { sourceMap = emptySourceMap; }
                var sourceLines;
                if (RS.Is.string(content)) {
                    this.__outputBuffer.addString(content);
                    sourceLines = countStringNL(content);
                    // Ensure last char is new-line
                    if (content.length > 0 && content[content.length - 1] !== newLineStr) {
                        sourceLines++;
                        this.__outputBuffer.addByte(newLineByte);
                    }
                }
                else {
                    this.__outputBuffer.addBuffer(content);
                    sourceLines = countBufferNL(content);
                    // Ensure last char is new-line
                    if (content.length > 0 && content[content.length - 1] !== newLineByte) {
                        sourceLines++;
                        this.__outputBuffer.addByte(newLineByte);
                    }
                }
                // Work out remap positions & update sources/sourcesContent arrays
                var sourceRemap = [];
                sourceMap.sources.forEach(function (v, i) {
                    var pos = _this.__sources.indexOf(v);
                    if (pos < 0) {
                        pos = _this.__sources.length;
                        _this.__sources.push(v);
                        if (sourceMap.sourcesContent) {
                            _this.__sourcesContent.push(sourceMap.sourcesContent[i]);
                        }
                        else {
                            _this.__sourcesContent.push(null);
                        }
                    }
                    sourceRemap.push(pos);
                });
                // Prepare to update mapping buffer
                var lastOutputCol = 0;
                var inputMappings = Buffer.from(sourceMap.mappings);
                // Current output line index pointer.
                var outputLine = 0;
                // Source-map properties decoded from binary data.
                var inOutputCol = 0;
                var inSourceIndex = 0;
                var inSourceLine = 0;
                var inSourceCol = 0;
                /** Current bit-shift value. */
                var shift = 0;
                /** Binary source-map data from VLQ. */
                var value = 0;
                /** Current index pointer into binary source-map data. */
                var valuePos = 0;
                var commit = function () {
                    if (valuePos === 0)
                        return;
                    _this.__mappings.addVLQ(inOutputCol - lastOutputCol);
                    lastOutputCol = inOutputCol;
                    if (valuePos === 1) {
                        valuePos = 0;
                        return;
                    }
                    var outSourceIndex = sourceRemap[inSourceIndex];
                    _this.__mappings.addVLQ(outSourceIndex - _this.__lastSourceIndex);
                    _this.__lastSourceIndex = outSourceIndex;
                    _this.__mappings.addVLQ(inSourceLine - _this.__lastSourceLine);
                    _this.__lastSourceLine = inSourceLine;
                    _this.__mappings.addVLQ(inSourceCol - _this.__lastSourceCol);
                    _this.__lastSourceCol = inSourceCol;
                    valuePos = 0;
                };
                for (var i = 0; i < inputMappings.length; i++) {
                    var byte = inputMappings[i];
                    if (byte === semiColonByte) {
                        // Add to buffer
                        commit();
                        this.__mappings.addByte(semiColonByte);
                        // Reset to line start
                        inOutputCol = 0;
                        lastOutputCol = 0;
                        outputLine++;
                    }
                    else if (byte === commaByte) {
                        // Add to buffer
                        commit();
                        this.__mappings.addByte(commaByte);
                    }
                    else {
                        byte = charToInteger[byte];
                        if (byte === emptyByte) {
                            // Failed to map byte to char int
                            throw new Error("Invalid sourceMap");
                        }
                        /*
                         * Essentially, this is dealing with the 'VLQ' that is
                         * the foundation of spec. 3 source-maps.
                         *
                         * The VLQ is a compact encoding of binary data, itself
                         * encoding the source-map data you'd expect i.e. lines,
                         * columns etc.
                         */
                        value += (byte & 31) << shift;
                        if (byte & 32) {
                            shift += 5;
                        }
                        else {
                            var shouldNegate = value & 1;
                            value >>= 1;
                            if (shouldNegate) {
                                value = -value;
                            }
                            switch (valuePos) {
                                case 0:
                                    inOutputCol += value;
                                    break;
                                case 1:
                                    inSourceIndex += value;
                                    break;
                                case 2:
                                    inSourceLine += value;
                                    break;
                                case 3:
                                    inSourceCol += value;
                                    break;
                            }
                            valuePos++;
                            value = 0;
                            shift = 0;
                        }
                    }
                }
                commit();
                // Pad output lines to match source lines
                while (outputLine < sourceLines) {
                    this.__mappings.addByte(semiColonByte);
                    outputLine++;
                }
            };
            /** Returns a buffer containing the current source content. */
            SourceMapBuilder.prototype.toContent = function () {
                return this.__outputBuffer.toBuffer();
            };
            /** Returns a source map object representing the current source map state. */
            SourceMapBuilder.prototype.toSourceMap = function () {
                return {
                    version: specVersion,
                    sources: this.__sources,
                    mappings: this.__mappings.toString(),
                    sourcesContent: this.__sourcesContent
                };
            };
            return SourceMapBuilder;
        }());
        Assembler.SourceMapBuilder = SourceMapBuilder;
    })(Assembler = RS.Assembler || (RS.Assembler = {}));
})(RS || (RS = {}));
/// <reference path="../Base.ts" />
/// <reference path="../Assembler.ts" />
/// <reference path="../../util/EnvArg.ts" />
var RS;
(function (RS) {
    var AssembleStages;
    (function (AssembleStages) {
        var versionArg = new RS.EnvArg("VERSION", "dev");
        /**
         * Responsible for copying the JS code of modules.
         */
        var Code = /** @class */ (function (_super) {
            __extends(Code, _super);
            function Code() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * Executes this assemble stage.
             */
            Code.prototype.execute = function (settings, moduleList) {
                return __awaiter(this, void 0, void 0, function () {
                    var jsDir, _a, _b, _c, _d, _e, _f, _g, _h, _j;
                    var _this = this;
                    return __generator(this, function (_k) {
                        switch (_k.label) {
                            case 0:
                                this._moduleCode = [];
                                this._thirdPartyCode = {};
                                jsDir = RS.Path.combine(settings.outputDir, "js");
                                return [4 /*yield*/, _super.prototype.execute.call(this, settings, moduleList)];
                            case 1:
                                _k.sent();
                                return [4 /*yield*/, RS.FileSystem.createPath(jsDir)];
                            case 2:
                                _k.sent();
                                // Log.debug(`Assemble order = ${this._moduleCode.map(m => m.name).join(", ")}`);
                                // Assemble app.js and thirdparty.js in parallel
                                RS.Profile.enter("Assemble app+thirdparty");
                                _k.label = 3;
                            case 3:
                                _k.trys.push([3, , 8, 9]);
                                _b = (_a = Promise).all;
                                _d = (_c = RS.Assembler).assembleJS;
                                _e = [jsDir, "app", this._moduleCode];
                                _f = {
                                    withHelpers: true,
                                    withSourcemaps: true,
                                    minify: false
                                };
                                if (!(versionArg.value !== "dev")) return [3 /*break*/, 5];
                                _h = {
                                    "{!GAME_VERSION!}": versionArg.value
                                };
                                _j = "{!SYNERGY_HASH!}";
                                return [4 /*yield*/, this.getSynergyHash()];
                            case 4:
                                _g = (_h[_j] = _k.sent(),
                                    _h);
                                return [3 /*break*/, 6];
                            case 5:
                                _g = undefined;
                                _k.label = 6;
                            case 6: return [4 /*yield*/, _b.apply(_a, [[
                                        _d.apply(_c, _e.concat([(_f.replacements = _g,
                                                _f)])),
                                        RS.Assembler.assembleJS(jsDir, "thirdparty", Object.keys(this._thirdPartyCode).map(function (k) { return _this._thirdPartyCode[k]; }), {
                                            withHelpers: false,
                                            withSourcemaps: true,
                                            minify: versionArg.value !== "dev"
                                        })
                                    ]])];
                            case 7:
                                _k.sent();
                                return [3 /*break*/, 9];
                            case 8:
                                RS.Profile.exit("Assemble app+thirdparty");
                                return [7 /*endfinally*/];
                            case 9: return [2 /*return*/];
                        }
                    });
                });
            };
            Code.prototype.getSynergyHash = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var gitModules, name_13, _i, gitModules_1, str, relativePath, trimmedPath;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, RS.FileSystem.fileExists(".gitmodules")];
                            case 1:
                                if (!_a.sent()) return [3 /*break*/, 5];
                                return [4 /*yield*/, RS.Command.run("git", [
                                        "config", "-f", ".gitmodules", "-l"
                                    ], undefined, false)];
                            case 2:
                                gitModules = (_a.sent()).split("\n");
                                for (_i = 0, gitModules_1 = gitModules; _i < gitModules_1.length; _i++) {
                                    str = gitModules_1[_i];
                                    //covers both ssh and https
                                    if (str.indexOf("red7mobile/synergy-framework") > -1) {
                                        name_13 = str.split("=")[0].split(".")[1];
                                        break;
                                    }
                                }
                                if (!(name_13 != null)) return [3 /*break*/, 5];
                                return [4 /*yield*/, RS.Command.run("git", [
                                        "config", "-f", ".gitmodules", "--get", "submodule." + name_13 + ".path"
                                    ], undefined, false)];
                            case 3:
                                relativePath = _a.sent();
                                trimmedPath = relativePath.trim();
                                return [4 /*yield*/, RS.Command.run("git", [
                                        "rev-parse", "HEAD"
                                    ], trimmedPath, false)];
                            case 4: 
                            // finally get the commit hash
                            return [2 /*return*/, (_a.sent()).trim()];
                            case 5:
                                RS.Log.warn("No synergy submodule found, defaulting to \"none\"");
                                return [2 /*return*/, "none"];
                        }
                    });
                });
            };
            /**
             * Executes this assemble stage for the given module only.
             * @param module
             */
            Code.prototype.executeModule = function (settings, module) {
                return __awaiter(this, void 0, void 0, function () {
                    var unit, content, rawSourceMap, _a, sourceMap;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                unit = module.getCompilationUnit(RS.Module.CompilationUnits.Source);
                                if (unit == null) {
                                    return [2 /*return*/];
                                }
                                _b.label = 1;
                            case 1:
                                _b.trys.push([1, 4, , 5]);
                                return [4 /*yield*/, RS.FileSystem.readFile(unit.outJSFile)];
                            case 2:
                                // Load the module's JS
                                content = _b.sent();
                                return [4 /*yield*/, RS.FileSystem.readFile(unit.outMapFile)];
                            case 3:
                                rawSourceMap = _b.sent();
                                return [3 /*break*/, 5];
                            case 4:
                                _a = _b.sent();
                                return [3 /*break*/, 5];
                            case 5:
                                // Did it emit any js?
                                if (content == null) {
                                    return [2 /*return*/];
                                }
                                if (!(rawSourceMap == null)) return [3 /*break*/, 7];
                                return [4 /*yield*/, RS.Assembler.resolveIdentitySourceMap(module, content, module.name + ".js")];
                            case 6:
                                // Generate an identity source map
                                rawSourceMap = _b.sent();
                                return [3 /*break*/, 8];
                            case 7:
                                try {
                                    sourceMap = RS.JSON.parse(rawSourceMap);
                                    sourceMap.sources = sourceMap.sources
                                        .map(function (src) { return src.replace("../src", module.name); });
                                    rawSourceMap = RS.JSON.stringify(sourceMap);
                                }
                                catch (err) {
                                    throw new Error("Error whilst parsing source map for module '" + module.name + "': " + err);
                                }
                                _b.label = 8;
                            case 8:
                                // Store
                                this._moduleCode.push({ name: module.name, content: content, sourceMap: rawSourceMap, sourceMapDir: RS.Path.combine(module.path, "build") });
                                // Gather third party dependencies
                                return [4 /*yield*/, RS.Assembler.gatherThirdPartyDependencies(module, this._thirdPartyCode)];
                            case 9:
                                // Gather third party dependencies
                                _b.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            __decorate([
                RS.Profile("AssembleStage.code.execute")
            ], Code.prototype, "execute", null);
            __decorate([
                RS.Profile("AssembleStage.code.executeModule")
            ], Code.prototype, "executeModule", null);
            return Code;
        }(RS.AssembleStage.Base));
        AssembleStages.Code = Code;
        AssembleStages.code = new Code();
        RS.AssembleStage.register({}, AssembleStages.code);
    })(AssembleStages = RS.AssembleStages || (RS.AssembleStages = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var Directives;
        (function (Directives) {
            /**
             *
             * Get checksum, which encodes:
             * - which defines are used by the unit: if a define usage is added or removed, the object key-value mapping changes
             * - which defines are configured: if a define is added or removed, the object key-value mapping changes
             * - what defines are configured to: if a define value changes, the object changes
             *
             * @param usesDefines
             * @param defineValues
             */
            function getDefineChecksum(usesDefines, defineValues) {
                // Sort them so that order of appearance doesn't affect result
                var sortedDefines = __spreadArrays(usesDefines).sort();
                var checksumObj = {};
                for (var _i = 0, sortedDefines_1 = sortedDefines; _i < sortedDefines_1.length; _i++) {
                    var define = sortedDefines_1[_i];
                    checksumObj[define] = defineValues[define];
                }
                return RS.Checksum.getSimple(checksumObj);
            }
            function validateMetadata(data) {
                if (!RS.Is.object(data)) {
                    return false;
                }
                if (!RS.Is.array(data.usesDefines)) {
                    return false;
                }
                if (data.usesDefines.some(function (v) { return !RS.Is.string(v) && !RS.Is.boolean(v); })) {
                    return false;
                }
                return true;
            }
            function getMetadataPath(unit) {
                return RS.Path.replaceExtension(unit.outJSFile, "_directiveData.json");
            }
            /** Returns whether or not the given unit needs to be recompiled due to directives. */
            function checkRecompileNeeded(unit) {
                return __awaiter(this, void 0, void 0, function () {
                    var metadataPath, metadata, err_10, checksum;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (unit.recompileNeeded) {
                                    return [2 /*return*/, true];
                                }
                                metadataPath = getMetadataPath(unit);
                                return [4 /*yield*/, RS.FileSystem.fileExists(metadataPath)];
                            case 1:
                                if (!(_a.sent())) {
                                    return [2 /*return*/, true];
                                }
                                _a.label = 2;
                            case 2:
                                _a.trys.push([2, 4, , 5]);
                                return [4 /*yield*/, RS.JSON.parseFile(metadataPath)];
                            case 3:
                                metadata = _a.sent();
                                return [3 /*break*/, 5];
                            case 4:
                                err_10 = _a.sent();
                                RS.Log.warn(err_10 + "; recompilation required", unit.name);
                                return [2 /*return*/, true];
                            case 5:
                                if (!validateMetadata(metadata)) {
                                    RS.Log.warn("Directive metadata invalid; recompilation required", unit.name);
                                    return [2 /*return*/, true];
                                }
                                checksum = getDefineChecksum(metadata.usesDefines, RS.Config.activeConfig.defines);
                                return [2 /*return*/, unit.module.checksumDB.diff("defines", checksum)];
                        }
                    });
                });
            }
            Directives.checkRecompileNeeded = checkRecompileNeeded;
            /** Adds the directive pre-processor to the given unit. */
            function addPreProcessor(unit) {
                var metadata = { usesDefines: [] };
                unit.addPreProcessor(function (src) {
                    var result = RS.Directives.parse(src, RS.Config.activeConfig.defines);
                    // Store metadata
                    for (var _i = 0, _a = result.usesDefines; _i < _a.length; _i++) {
                        var define = _a[_i];
                        if (metadata.usesDefines.indexOf(define) === -1) {
                            metadata.usesDefines.push(define);
                        }
                    }
                    // Set checksum
                    var checksum = getDefineChecksum(metadata.usesDefines, RS.Config.activeConfig.defines);
                    this.module.checksumDB.set("defines", checksum);
                    return { output: result.content, workDone: result.usesDefines.length > 0 };
                });
                return metadata;
            }
            Directives.addPreProcessor = addPreProcessor;
            /**
             * Persists the given metadata for the given unit.
             * @returns A Promise which resolves once the data has been persisted.
             */
            function saveMetadata(unit, metadata) {
                var path = getMetadataPath(unit);
                return RS.FileSystem.writeFile(path, RS.JSON.stringify(metadata));
            }
            Directives.saveMetadata = saveMetadata;
        })(Directives = Build.Directives || (Build.Directives = {}));
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        /** Compiles the given compilation unit. Returns whether or not work was done. */
        function compileUnit(unit) {
            return __awaiter(this, void 0, void 0, function () {
                var directiveData, recompileNeeded, _a, result, _i, _b, err, err_11;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!(unit.validityState === Build.CompilationUnit.ValidState.Uninitialised)) return [3 /*break*/, 2];
                            return [4 /*yield*/, unit.init()];
                        case 1:
                            _c.sent();
                            _c.label = 2;
                        case 2:
                            directiveData = Build.Directives.addPreProcessor(unit);
                            switch (unit.validityState) {
                                case Build.CompilationUnit.ValidState.MissingDependency:
                                case Build.CompilationUnit.ValidState.NoTSConfig:
                                case Build.CompilationUnit.ValidState.Uninitialised:
                                    RS.Log.error("Bad unit validity state: " + Build.CompilationUnit.ValidState[unit.validityState], unit.name);
                                    return [2 /*return*/, { workDone: true, errorCount: 1 }];
                            }
                            _a = unit.recompileNeeded;
                            if (_a) return [3 /*break*/, 4];
                            return [4 /*yield*/, Build.Directives.checkRecompileNeeded(unit)];
                        case 3:
                            _a = (_c.sent());
                            _c.label = 4;
                        case 4:
                            recompileNeeded = _a;
                            if (!recompileNeeded) return [3 /*break*/, 9];
                            return [4 /*yield*/, unit.execute()];
                        case 5:
                            result = _c.sent();
                            if (!result.success) return [3 /*break*/, 7];
                            return [4 /*yield*/, Build.Directives.saveMetadata(unit, directiveData)];
                        case 6:
                            _c.sent();
                            return [3 /*break*/, 8];
                        case 7:
                            for (_i = 0, _b = result.errors; _i < _b.length; _i++) {
                                err = _b[_i];
                                RS.Log.error(err, unit.name);
                            }
                            return [2 /*return*/, { workDone: true, errorCount: result.errors.length }];
                        case 8:
                            RS.Log.info("Compiled in " + (result.time / 1000).toFixed(2) + " s", unit.name);
                            return [2 /*return*/, { workDone: true, errorCount: 0 }];
                        case 9:
                            if (!unit.lintNeeded) return [3 /*break*/, 13];
                            _c.label = 10;
                        case 10:
                            _c.trys.push([10, 12, , 13]);
                            return [4 /*yield*/, unit.lintFiles()];
                        case 11:
                            _c.sent();
                            return [2 /*return*/, { workDone: true, errorCount: 0 }];
                        case 12:
                            err_11 = _c.sent();
                            if (!(err_11 instanceof RS.TSLint.LintFailure)) {
                                RS.Log.error(err_11);
                            }
                            return [2 /*return*/, { workDone: true, errorCount: 1 }];
                        case 13: return [2 /*return*/, { workDone: false, errorCount: 0 }];
                    }
                });
            });
        }
        Build.compileUnit = compileUnit;
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
/// <reference path="../BuildStage.ts" />
var RS;
(function (RS) {
    var BuildStages;
    (function (BuildStages) {
        /**
         * Responsible for compiling the TypeScript source code of modules.
         */
        var Compile = /** @class */ (function (_super) {
            __extends(Compile, _super);
            function Compile(unitName) {
                var _this = _super.call(this) || this;
                _this.unitName = unitName;
                return _this;
            }
            Object.defineProperty(Compile.prototype, "errorsAreFatal", {
                /**
                 * Gets if errors encountered when building a module are fatal and should end the whole process.
                 * If this is false, other modules may continue to be compiled after an error has been encountered.
                 */
                get: function () { return true; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Compile.prototype, "runInParallel", {
                /**
                 * Gets if the executeModule function should be called in parallel for all modules.
                 * Ddependencies are still considered and processed serially.
                 */
                get: function () { return true; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Compile.prototype, "name", {
                get: function () { return this.unitName; },
                enumerable: true,
                configurable: true
            });
            /**
             * Executes this build stage for the given module only.
             * @param module
             */
            Compile.prototype.executeModule = function (module) {
                return __awaiter(this, void 0, void 0, function () {
                    var unit, result;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                unit = module.getCompilationUnit(this.unitName);
                                if (unit == null) {
                                    return [2 /*return*/, { workDone: false, errorCount: 0 }];
                                }
                                return [4 /*yield*/, RS.Build.compileUnit(unit)];
                            case 1:
                                result = _a.sent();
                                if (!(result.errorCount === 0)) return [3 /*break*/, 3];
                                return [4 /*yield*/, module.saveChecksums()];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3: return [2 /*return*/, result];
                        }
                    });
                });
            };
            return Compile;
        }(RS.BuildStage.Base));
        BuildStages.Compile = Compile;
        BuildStages.compile = new Compile(RS.Module.CompilationUnits.Source);
        RS.BuildStage.register({}, BuildStages.compile);
    })(BuildStages = RS.BuildStages || (RS.BuildStages = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var CodeGeneration;
        (function (CodeGeneration) {
            var EmitLocation;
            (function (EmitLocation) {
                EmitLocation[EmitLocation["Start"] = 0] = "Start";
                EmitLocation[EmitLocation["MidLine"] = 1] = "MidLine";
                EmitLocation[EmitLocation["StartOfLine"] = 2] = "StartOfLine";
                EmitLocation[EmitLocation["EndOfLine"] = 3] = "EndOfLine";
                EmitLocation[EmitLocation["EndOfScope"] = 4] = "EndOfScope";
            })(EmitLocation || (EmitLocation = {}));
            var Formatter = /** @class */ (function () {
                function Formatter() {
                    this._chunks = [];
                    this._identLevel = 0;
                    this._emitLoc = EmitLocation.Start;
                }
                /** Emit a chunk, do not consider emit location. */
                Formatter.prototype.emit = function (chunk) {
                    this._chunks.push(chunk);
                };
                Formatter.prototype.emitIndent = function () {
                    for (var i = 0; i < this._identLevel; i++) {
                        this.emit("\t");
                    }
                };
                /** Prepares the emitter to emit a new line of code. */
                Formatter.prototype.bringToNewLine = function () {
                    switch (this._emitLoc) {
                        case EmitLocation.MidLine:
                        case EmitLocation.EndOfLine:
                        case EmitLocation.EndOfScope:
                            this.emit("\n");
                            this.emitIndent();
                            break;
                    }
                    this._emitLoc = EmitLocation.StartOfLine;
                };
                /** Emits a line of code. */
                Formatter.prototype.emitLine = function (line) {
                    this.bringToNewLine();
                    this.emit(line);
                    this._emitLoc = EmitLocation.EndOfLine;
                };
                /** Emits a new scope. */
                Formatter.prototype.enterScope = function (char) {
                    if (char === void 0) { char = "{"; }
                    this.bringToNewLine();
                    this.emit(char);
                    this._identLevel++;
                    this._emitLoc = EmitLocation.EndOfScope;
                };
                /** Emits the end of a scope. */
                Formatter.prototype.exitScope = function (char) {
                    if (char === void 0) { char = "}"; }
                    this._identLevel--;
                    this.bringToNewLine();
                    this.emit(char);
                    this._emitLoc = EmitLocation.EndOfScope;
                };
                /** Emits the specified named object. */
                Formatter.prototype.emitNamed = function (obj) {
                    switch (obj.kind) {
                        case "interface":
                            this.emitInterface(obj);
                            break;
                        case "propertydeclr":
                            this.emitPropertyDeclaration(obj);
                            break;
                        case "namespace":
                            this.emitNamespace(obj);
                            break;
                        case "var":
                            this.emitVariable(obj);
                            break;
                        case "arrval":
                            this.emitArrayValue(obj);
                            break;
                        case "objval":
                            this.emitObjectValue(obj);
                            break;
                        case "keyval":
                            this.emitKeyValue(obj);
                            break;
                        case "codeval":
                            this.emitCodeValue(obj);
                            break;
                        case "type":
                            this.emitTypeDef(obj);
                            break;
                    }
                };
                /** Emits the specified interface. */
                Formatter.prototype.emitInterface = function (obj) {
                    if (obj.comment != null) {
                        this.emitLine("/** " + obj.comment + " */");
                    }
                    this.bringToNewLine();
                    this.emit("export interface " + obj.name);
                    if (obj.extends && obj.extends.length > 0) {
                        this.emit(" extends ");
                        for (var i = 0; i < obj.extends.length; i++) {
                            if (i > 0) {
                                this.emit(", ");
                            }
                            this.emitType(obj.extends[i]);
                        }
                    }
                    this._emitLoc = EmitLocation.EndOfLine;
                    this.enterScope();
                    if (obj.properties != null) {
                        for (var i = 0; i < obj.properties.length; i++) {
                            var property = obj.properties[i];
                            this.emitPropertyDeclaration(property);
                        }
                    }
                    this.exitScope();
                };
                /** Emits the specified property declaration. */
                Formatter.prototype.emitPropertyDeclaration = function (obj) {
                    if (obj.comment != null) {
                        this.emitLine("/** " + obj.comment + " */");
                    }
                    this.bringToNewLine();
                    this.emit("" + obj.name + (obj.optional ? '?' : '') + ": ");
                    this.emitType(obj.type);
                    this.emit(";");
                    this._emitLoc = EmitLocation.EndOfLine;
                };
                /** Emits the specified namespace. */
                Formatter.prototype.emitNamespace = function (obj) {
                    // const isStart = this._emitLoc === EmitLocation.Start;
                    var isStart = this._identLevel === 0;
                    if (obj.comment != null) {
                        this.emitLine("/** " + obj.comment + " */");
                    }
                    this.emitLine((!isStart ? "export " : "") + "namespace " + obj.name);
                    this.enterScope();
                    if (obj.children != null) {
                        for (var i = 0; i < obj.children.length; i++) {
                            var child = obj.children[i];
                            this.emitNamed(child);
                        }
                    }
                    this.exitScope();
                };
                /** Emits the specified variable. */
                Formatter.prototype.emitVariable = function (obj) {
                    if (obj.comment != null) {
                        this.emitLine("/** " + obj.comment + " */");
                    }
                    this.bringToNewLine();
                    this.emit("export " + (obj.isConst ? "const" : "let") + " " + obj.name);
                    if (obj.type != null) {
                        this.emit(": ");
                        this.emitType(obj.type);
                    }
                    if (obj.value != null) {
                        this.emit(" = ");
                        this.emitValue(obj.value);
                    }
                    this.emit(";");
                    this._emitLoc = EmitLocation.EndOfLine;
                };
                /** Emits the specified value. */
                Formatter.prototype.emitValue = function (obj) {
                    if (RS.Is.string(obj)) {
                        this.emit(obj);
                    }
                    else {
                        this.emitNamed(obj);
                    }
                };
                /** Emits the specified array value. */
                Formatter.prototype.emitArrayValue = function (obj) {
                    this.enterScope("[");
                    if (obj.values) {
                        for (var i = 0; i < obj.values.length; i++) {
                            if (i > 0) {
                                this.emit(",");
                            }
                            this.bringToNewLine();
                            this.emitValue(obj.values[i]);
                        }
                    }
                    this.exitScope("]");
                };
                /** Emits the specified object value. */
                Formatter.prototype.emitObjectValue = function (obj) {
                    this.enterScope("{");
                    if (obj.values) {
                        for (var i = 0; i < obj.values.length; i++) {
                            if (i > 0) {
                                this.emit(",");
                            }
                            this.bringToNewLine();
                            this.emitValue(obj.values[i]);
                            this._emitLoc = EmitLocation.EndOfLine;
                        }
                    }
                    this.exitScope("}");
                };
                /** Emits the specified key-value pair. */
                Formatter.prototype.emitKeyValue = function (obj) {
                    var numKey = parseInt(obj.name);
                    if (RS.Is.number(numKey)) {
                        this.emit("[" + numKey + "]");
                    }
                    else {
                        this.emit(obj.name);
                    }
                    this.emit(": ");
                    this.emitValue(obj.value);
                };
                /** Emits the specified code value. */
                Formatter.prototype.emitCodeValue = function (obj) {
                    for (var i = 0; i < obj.pieces.length; i++) {
                        var piece = obj.pieces[i];
                        if (RS.Is.string(piece)) {
                            this.emit(piece);
                        }
                        else if (CodeGeneration.isNamed(piece)) {
                            this.emitNamed(piece);
                        }
                        else if (CodeGeneration.isTypeRef(piece)) {
                            this.emitType(piece);
                        }
                    }
                    this._emitLoc = EmitLocation.MidLine;
                };
                /** Emits the specified typedef. */
                Formatter.prototype.emitTypeDef = function (obj) {
                    this.bringToNewLine();
                    if (obj.comment != null) {
                        this.emitLine("/** " + obj.comment + " */");
                    }
                    this.emit("export type ");
                    this.emit(obj.name);
                    this.emit(" = ");
                    this.emitType(obj.type);
                    this.emit(";");
                    this._emitLoc = EmitLocation.EndOfLine;
                };
                /** Emits the specified type. */
                Formatter.prototype.emitType = function (type) {
                    this.emit(CodeGeneration.stringifyType(type));
                    this._emitLoc = EmitLocation.MidLine;
                };
                /** Serialises all emmitted code to a string. */
                Formatter.prototype.toString = function () {
                    return this._chunks.join("");
                };
                return Formatter;
            }());
            CodeGeneration.Formatter = Formatter;
        })(CodeGeneration = Build.CodeGeneration || (Build.CodeGeneration = {}));
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var CodeGeneration;
        (function (CodeGeneration) {
            function isNamed(obj) {
                return obj != null && RS.Is.string(obj.kind) && (obj.kind === "propertydeclr" ||
                    obj.kind === "interface" ||
                    obj.kind === "class" ||
                    obj.kind === "enum" ||
                    obj.kind === "type" ||
                    obj.kind === "namespace" ||
                    obj.kind === "var" ||
                    obj.kind === "func" ||
                    obj.kind === "arrval" ||
                    obj.kind === "objval" ||
                    obj.kind === "keyval" ||
                    obj.kind === "codeval");
            }
            CodeGeneration.isNamed = isNamed;
            function isTypeRef(obj) {
                return obj != null && RS.Is.string(obj.kind) && (obj.kind === "basictyperef" ||
                    obj.kind === "inlinetyperef" ||
                    obj.kind === "litnumtyperef" ||
                    obj.kind === "litstrtyperef" ||
                    obj.kind === "litbooltyperef" ||
                    obj.kind === "generictyperef" ||
                    obj.kind === "arraytyperef" ||
                    obj.kind === "fixedtyperef" ||
                    obj.kind === "intersecttyperef" ||
                    obj.kind === "uniontyperef" ||
                    obj.kind === "functiontyperef" ||
                    obj.kind === "istyperef" ||
                    obj.kind === "indexedtyperef" ||
                    obj.kind === "typeoftyperef");
            }
            CodeGeneration.isTypeRef = isTypeRef;
        })(CodeGeneration = Build.CodeGeneration || (Build.CodeGeneration = {}));
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var CodeGeneration;
        (function (CodeGeneration) {
            var debugMode = false;
            CodeGeneration.anyType = { kind: "basictyperef" /* Raw */, name: "any" };
            /**
             * Gets if both type references are EQUAL.
             */
            function typesAreEqual(a, b) {
                // Note: We check that the kinds match later down the line rather than early-out up here
                // This is so TS can narrow BOTH a and b to that kind
                switch (a.kind) {
                    case "basictyperef" /* Raw */:
                        {
                            // TODO: Handle case where b inherits/extends a
                            if (b.kind !== "basictyperef" /* Raw */) {
                                return false;
                            }
                            return a.name === b.name;
                        }
                    case "generictyperef" /* WithTypeArgs */:
                        {
                            if (b.kind !== "generictyperef" /* WithTypeArgs */) {
                                return false;
                            }
                            return typesAreEqual(a.inner, b.inner) && typeArraysAreEqual(a.genericTypeArgs, b.genericTypeArgs, true);
                        }
                    case "arraytyperef" /* Array */:
                        {
                            if (b.kind !== "arraytyperef" /* Array */) {
                                return false;
                            }
                            return typesAreEqual(a.inner, b.inner);
                        }
                    case "intersecttyperef" /* Intersect */:
                        {
                            if (b.kind !== "intersecttyperef" /* Intersect */) {
                                return false;
                            }
                            return typeArraysAreEqual(a.inners, b.inners);
                        }
                    case "uniontyperef" /* Union */:
                        {
                            if (b.kind !== "uniontyperef" /* Union */) {
                                return false;
                            }
                            return typeArraysAreEqual(a.inners, b.inners);
                        }
                    case "inlinetyperef" /* Inline */:
                        {
                            if (b.kind !== "inlinetyperef" /* Inline */) {
                                return false;
                            }
                            if (a.properties) {
                                if (!b.properties) {
                                    return false;
                                }
                                if (!propertyArraysAreEqual(a.properties, b.properties)) {
                                    return false;
                                }
                            }
                            else {
                                if (b.properties) {
                                    return false;
                                }
                            }
                            if (a.methods) {
                                if (!b.methods) {
                                    return false;
                                }
                                if (!methodArraysAreEqual(a.methods, b.methods)) {
                                    return false;
                                }
                            }
                            else {
                                if (b.methods) {
                                    return false;
                                }
                            }
                            return true;
                        }
                }
                // Unknown kind, return false
                return false;
            }
            CodeGeneration.typesAreEqual = typesAreEqual;
            /**
             * Gets if both property declarations are EQUAL.
             * @param a
             * @param b
             */
            function propertiesAreEqual(a, b) {
                return a.name === b.name && a.accessQualifier === b.accessQualifier && a.isAbstract === b.isAbstract && a.optional === b.optional && a.storageQualifier === b.storageQualifier && typesAreEqual(a.type, b.type);
            }
            CodeGeneration.propertiesAreEqual = propertiesAreEqual;
            /**
             * Gets if both arrays of property declarations are EQUAL.
             * @param a
             * @param b
             * @param orderMatters
             */
            function propertyArraysAreEqual(a, b, orderMatters) {
                if (orderMatters === void 0) { orderMatters = false; }
                // Check they have the same number of items
                if (a.length !== b.length) {
                    return false;
                }
                if (orderMatters) {
                    // Check that every item matches
                    for (var i = 0; i < a.length; i++) {
                        if (!propertiesAreEqual(a[i], b[i])) {
                            return false;
                        }
                    }
                }
                else {
                    var _loop_5 = function (i) {
                        if (!b.some(function (item) { return propertiesAreEqual(a[i], item); })) {
                            return { value: false };
                        }
                    };
                    // Check that each item in A is found in B
                    for (var i = 0; i < a.length; i++) {
                        var state_2 = _loop_5(i);
                        if (typeof state_2 === "object")
                            return state_2.value;
                    }
                }
                // Done
                return true;
            }
            CodeGeneration.propertyArraysAreEqual = propertyArraysAreEqual;
            /**
             * Gets if both function signatures are EQUAL.
             * @param a
             * @param b
             */
            function functionSignaturesAreEqual(a, b) {
                if (a.isConstructor !== b.isConstructor) {
                    return false;
                }
                if (a.returnType) {
                    if (!b.returnType) {
                        return false;
                    }
                    if (!typesAreEqual(a.returnType, b.returnType)) {
                        return false;
                    }
                }
                else {
                    if (b.returnType) {
                        return false;
                    }
                }
                // TODO
                throw new Error("Not implemented yet!");
                //return true;
            }
            CodeGeneration.functionSignaturesAreEqual = functionSignaturesAreEqual;
            /**
             * Gets if both method declarations are EQUAL.
             * @param a
             * @param b
             */
            function methodsAreEqual(a, b) {
                return a.name === b.name && a.accessQualifier === b.accessQualifier && a.isAbstract === b.isAbstract && a.optional === b.optional && a.storageQualifier === b.storageQualifier && functionSignaturesAreEqual(a.signature, b.signature);
            }
            CodeGeneration.methodsAreEqual = methodsAreEqual;
            /**
             * Gets if both arrays of method declarations are EQUAL.
             * @param a
             * @param b
             * @param orderMatters
             */
            function methodArraysAreEqual(a, b, orderMatters) {
                if (orderMatters === void 0) { orderMatters = false; }
                // Check they have the same number of items
                if (a.length !== b.length) {
                    return false;
                }
                if (orderMatters) {
                    // Check that every item matches
                    for (var i = 0; i < a.length; i++) {
                        if (!methodsAreEqual(a[i], b[i])) {
                            return false;
                        }
                    }
                }
                else {
                    var _loop_6 = function (i) {
                        if (!b.some(function (item) { return methodsAreEqual(a[i], item); })) {
                            return { value: false };
                        }
                    };
                    // Check that each item in A is found in B
                    for (var i = 0; i < a.length; i++) {
                        var state_3 = _loop_6(i);
                        if (typeof state_3 === "object")
                            return state_3.value;
                    }
                }
                // Done
                return true;
            }
            CodeGeneration.methodArraysAreEqual = methodArraysAreEqual;
            /**
             * Gets if both arrays of type references are EQUAL.
             */
            function typeArraysAreEqual(a, b, orderMatters) {
                if (orderMatters === void 0) { orderMatters = false; }
                // Check they have the same number of items
                if (a.length !== b.length) {
                    return false;
                }
                if (orderMatters) {
                    // Check that every item matches
                    for (var i = 0; i < a.length; i++) {
                        if (!typesAreEqual(a[i], b[i])) {
                            return false;
                        }
                    }
                }
                else {
                    var _loop_7 = function (i) {
                        if (!b.some(function (item) { return typesAreEqual(a[i], item); })) {
                            return { value: false };
                        }
                    };
                    // Check that each item in A is found in B
                    for (var i = 0; i < a.length; i++) {
                        var state_4 = _loop_7(i);
                        if (typeof state_4 === "object")
                            return state_4.value;
                    }
                }
                // Done
                return true;
            }
            CodeGeneration.typeArraysAreEqual = typeArraysAreEqual;
            /**
             * Returns a new type reference that represents a narrowed by b.
             * Returns null if narrowing failed.
             */
            function narrowType(a, b) {
                var result = narrowTypeInternal(a, b);
                if (debugMode) {
                    var formatter1 = new CodeGeneration.Formatter();
                    formatter1.emitType(a);
                    var formatter2 = new CodeGeneration.Formatter();
                    formatter2.emitType(b);
                    if (result == null) {
                        console.log("Failed to narrow '" + formatter1.toString() + "' by '" + formatter2.toString() + "'");
                    }
                    else {
                        var formatter3 = new CodeGeneration.Formatter();
                        formatter3.emitType(result);
                        console.log("Narrowed '" + formatter1.toString() + "' by '" + formatter2.toString() + "' into '" + formatter3.toString() + "'");
                    }
                }
                return result;
            }
            CodeGeneration.narrowType = narrowType;
            /**
             * Returns a new type reference that represents a narrowed by b.
             * Returns null if narrowing failed.
             */
            function narrowTypeInternal(a, b) {
                // We want to find a type that satisfies both a and b.
                // We start with a, and then reduce it to match b.
                // Edge case: If b is a union type and any of it's inners match a exactly, return a
                if (b.kind === "uniontyperef" /* Union */ && a.kind !== "uniontyperef" /* Union */) {
                    // e.g. a = "string", b: "number | string | boolean", expected output = "string"
                    for (var i = 0; i < b.inners.length; i++) {
                        if (typesAreEqual(a, b.inners[i])) {
                            return __assign({}, a); // TODO: Deep clone?
                        }
                    }
                    // Now check if a narrows to anything in b
                    // e.g. a = "DerivedInterface", b: "number | BaseInterface | string", expected output = "BaseInterface"
                    for (var i = 0; i < b.inners.length; i++) {
                        var narrowedInner = narrowType(a, b.inners[i]);
                        if (narrowedInner != null) {
                            return narrowedInner;
                        }
                    }
                    // Never mind, carry on with standard logic
                }
                // Edge case: If b is a intersect type, a has to satisfy or reduce to EVERY inner of b
                if (b.kind === "intersecttyperef" /* Intersect */) {
                    var currentNarrowed = a;
                    for (var i = 0; i < b.inners.length; i++) {
                        var bInner = b.inners[i];
                        // If they match exactly, we're good on this inner
                        if (typesAreEqual(currentNarrowed, bInner)) {
                            continue;
                        }
                        // Attempt to narrow it, if it fails, fail everything
                        currentNarrowed = narrowType(currentNarrowed, bInner);
                        if (currentNarrowed == null) {
                            return null;
                        }
                    }
                    // Return the new narrowed type
                    if (currentNarrowed === a) {
                        return __assign({}, a); // TODO: Deep clone?
                    }
                    else {
                        return currentNarrowed;
                    }
                }
                switch (a.kind) {
                    case "basictyperef" /* Raw */:
                        {
                            // We can't narrow this any further so only return if it matches b exactly
                            if (b.kind === "basictyperef" /* Raw */ && a.name === b.name) {
                                return __assign({}, b);
                            }
                            else {
                                // TODO: Handle case where b inherits/extends a
                                return null;
                            }
                        }
                    case "generictyperef" /* WithTypeArgs */:
                        {
                            // We can only narrow this if b is a generic also, refers to the same base type, and has the same amount of generic args
                            if (b.kind !== "generictyperef" /* WithTypeArgs */ || !typesAreEqual(a.inner, b.inner) || a.genericTypeArgs.length !== b.genericTypeArgs.length) {
                                return null;
                            }
                            // Narrow each generic type arg
                            var newGenericTypeArgs = [];
                            for (var i = 0; i < a.genericTypeArgs.length; i++) {
                                var narrowedTypeArg = narrowType(a.genericTypeArgs[i], b.genericTypeArgs[i]);
                                if (narrowedTypeArg == null) {
                                    // Failed to narrow, so fail the whole thing
                                    return null;
                                }
                                newGenericTypeArgs.push(narrowedTypeArg);
                            }
                            // Success
                            return {
                                kind: "generictyperef" /* WithTypeArgs */,
                                inner: a,
                                genericTypeArgs: newGenericTypeArgs
                            };
                        }
                    case "arraytyperef" /* Array */:
                        {
                            // TODO: Handle narrowing to ArrayLike<T>?
                            // We can only narrow this if b is an array also
                            if (b.kind !== "arraytyperef" /* Array */) {
                                return null;
                            }
                            // Attempt to narrow the inners
                            var narrowedInner = narrowType(a.inner, b.inner);
                            if (narrowedInner == null) {
                                return null;
                            }
                            // Success
                            return {
                                kind: "arraytyperef" /* Array */,
                                inner: narrowedInner
                            };
                        }
                    case "uniontyperef" /* Union */:
                    case "intersecttyperef" /* Intersect */:
                        {
                            var newInners = [];
                            // Is b an union type?
                            if (b.kind === "uniontyperef" /* Union */) {
                                // Both types are union types - find all inners that satisfy BOTH
                                // e.g. a = "string | number", b = "number | boolean | string[]", expected output = "number"
                                for (var i = 0; i < a.inners.length; i++) {
                                    var aInner = a.inners[i];
                                    var narrowedInner = null;
                                    // Find an equivalent b inner
                                    for (var j = 0; j < b.inners.length; j++) {
                                        var bInner = b.inners[j];
                                        if (typesAreEqual(aInner, bInner)) {
                                            // Success
                                            narrowedInner = __assign({}, bInner);
                                            break;
                                        }
                                    }
                                    // If not found, now try to narrow against each b inner
                                    if (narrowedInner == null) {
                                        for (var j = 0; j < b.inners.length; j++) {
                                            var bInner = b.inners[j];
                                            narrowedInner = narrowType(aInner, bInner);
                                            if (narrowedInner != null) {
                                                break;
                                            }
                                        }
                                    }
                                    // Found?
                                    if (narrowedInner != null) {
                                        newInners.push(narrowedInner);
                                    }
                                }
                                // 
                            }
                            else {
                                // Iterate each inner type of a, attempt to narrow it
                                for (var i = 0; i < a.inners.length; i++) {
                                    var narrowedInner = narrowType(a.inners[i], b);
                                    if (narrowedInner != null) {
                                        newInners.push(narrowedInner);
                                    }
                                }
                            }
                            // Return based on how many new inners we found
                            removeDuplicateTypes(newInners);
                            if (newInners.length === 0) {
                                return null;
                            }
                            if (newInners.length === 1) {
                                return __assign({}, b); // TODO: Deep clone?
                            }
                            else {
                                return {
                                    kind: a.kind,
                                    inners: newInners
                                };
                            }
                        }
                }
            }
            /**
             * Removes any duplicate types from the specified type array.
             */
            function removeDuplicateTypes(typeArr) {
                for (var i = typeArr.length - 1; i >= 0; i--) {
                    for (var j = i - 1; j >= 0; j--) {
                        if (typesAreEqual(typeArr[i], typeArr[j])) {
                            typeArr.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            CodeGeneration.removeDuplicateTypes = removeDuplicateTypes;
            /**
             * Creates a new type that can represent ANY of the specified types.
             * Returns null if no such type can be found.
             */
            function getUnion(typeArr) {
                // Attempt to narrow all types to the same type
                var currentType = typeArr[0];
                for (var i = 1; i < typeArr.length; i++) {
                    currentType = narrowType(currentType, typeArr[1]);
                    if (currentType == null) {
                        break;
                    }
                }
                if (currentType) {
                    return currentType;
                }
                // Failing that, return a union type
                var newInners = __spreadArrays(typeArr);
                removeDuplicateTypes(newInners);
                if (newInners.length === 0) {
                    return null;
                }
                if (newInners.length === 1) {
                    return __assign({}, newInners[0]);
                } // TODO: Deep clone?
                return {
                    kind: "uniontyperef" /* Union */,
                    inners: newInners
                };
            }
            CodeGeneration.getUnion = getUnion;
            /**
             * Creates a new type that can represent BOTH of the specified types.
             * @param a
             * @param b
             */
            function intersect(a, b) {
                if (a == null) {
                    return b;
                }
                if (b == null) {
                    return a;
                }
                if (a.kind === "intersecttyperef" /* Intersect */) {
                    if (b.kind === "intersecttyperef" /* Intersect */) {
                        return { kind: "intersecttyperef" /* Intersect */, inners: __spreadArrays(a.inners, b.inners) };
                    }
                    else {
                        return { kind: "intersecttyperef" /* Intersect */, inners: __spreadArrays(a.inners, [b]) };
                    }
                }
                else {
                    if (b.kind === "intersecttyperef" /* Intersect */) {
                        return { kind: "intersecttyperef" /* Intersect */, inners: __spreadArrays([a], b.inners) };
                    }
                    else {
                        return { kind: "intersecttyperef" /* Intersect */, inners: [a, b] };
                    }
                }
            }
            CodeGeneration.intersect = intersect;
            /**
             * Makes a name safe for emitting as an identifier.
             */
            function sanitiseIdentifier(id) {
                return id
                    .replace(/[-\.\s]/g, "_")
                    .replace(/_+/g, "_");
            }
            CodeGeneration.sanitiseIdentifier = sanitiseIdentifier;
            /**
             * Gets a string representation of the specified type.
             * @param type
             */
            function stringifyType(type, useBrackets) {
                if (type === void 0) { type = null; }
                if (useBrackets === void 0) { useBrackets = false; }
                var value;
                var brackets;
                if (type == null) {
                    value = "any";
                    brackets = false;
                }
                else {
                    switch (type.kind) {
                        case "basictyperef" /* Raw */:
                            value = type.name;
                            brackets = false;
                            break;
                        case "litnumtyperef" /* LiteralNumber */:
                            value = type.value.toString();
                            brackets = false;
                            break;
                        case "litstrtyperef" /* LiteralString */:
                            value = type.value;
                            brackets = false;
                            break;
                        case "litbooltyperef" /* LiteralBool */:
                            value = type.value ? "true" : "false";
                            brackets = false;
                            break;
                        case "generictyperef" /* WithTypeArgs */:
                            value = stringifyType(type.inner, true) + "<" + type.genericTypeArgs.map(function (t) { return stringifyType(t); }).join(", ") + ">";
                            brackets = false;
                            break;
                        case "arraytyperef" /* Array */:
                            {
                                value = stringifyType(type.inner, true) + "[]";
                                brackets = false;
                                break;
                            }
                        case "fixedarraytyperef" /* FixedArray */:
                            value = "[" + type.inners.map(function (t) { return stringifyType(t); }).join(", ") + "]";
                            brackets = false;
                            break;
                        case "intersecttyperef" /* Intersect */:
                            value = type.inners.map(function (t) { return stringifyType(t, true); }).join(" & ");
                            brackets = true;
                            break;
                        case "uniontyperef" /* Union */:
                            value = type.inners.map(function (t) { return stringifyType(t, true); }).join(" | ");
                            brackets = true;
                            break;
                        case "functiontyperef" /* Function */:
                            value = "(" + type.signature.parameters.map(function (p) { return p.name + ": " + stringifyType(p.type); }).join(", ") + ") => " + stringifyType(type.signature.returnType);
                            brackets = true;
                            break;
                        case "istyperef" /* Is */:
                            value = type.parameterName + " is " + stringifyType(type.inner, true);
                            brackets = true;
                            break;
                        case "inlinetyperef" /* Inline */:
                            var segs = [];
                            segs.push("{ ");
                            if (type.properties) {
                                for (var _i = 0, _a = type.properties; _i < _a.length; _i++) {
                                    var prop = _a[_i];
                                    segs.push(prop.name);
                                    segs.push(": ");
                                    segs.push(stringifyType(prop.type, false));
                                    segs.push("; ");
                                }
                            }
                            if (type.methods) {
                                for (var _b = 0, _c = type.methods; _b < _c.length; _b++) {
                                    var method = _c[_b];
                                    segs.push(method.name);
                                    segs.push(": ");
                                    segs.push("Function"); // TODO: This!
                                    segs.push("; ");
                                }
                            }
                            segs.push("}");
                            value = segs.join("");
                            brackets = false;
                            break;
                        default:
                            value = "any";
                            brackets = false;
                            break;
                    }
                }
                if (brackets && useBrackets) {
                    return "(" + value + ")";
                }
                else {
                    return value;
                }
            }
            CodeGeneration.stringifyType = stringifyType;
            /**
             * Attempts to normalise the specified TypeRef, turning any "relative" types into "absolute" types.
             * For instance, the simple type "My.Class" would normalise to "RS.My.Class" if referenced from within the "RS" namespace AND if "RS.My.Class" exists.
             * The normalised type should be resolvable given the global namespace.
             * Unidentifiable types are left alone.
             * @param type The type to resolve
             * @param namespaceHierarchy Path of namespaces leading to the location of the type. e.g. for the above example, [RS]
             */
            function normaliseTypeRef(type, namespaceHierarchy) {
                switch (type.kind) {
                    case "basictyperef" /* Raw */:
                        // Search each namespace in turn for it
                        for (var i = namespaceHierarchy.length - 1; i >= 0; ++i) {
                            var ns = namespaceHierarchy[i];
                            var result = searchForNamed(ns, type.name);
                            if (result) {
                                // We found it, grab a full name
                                var nsPath = namespaceHierarchy.slice(0, i + 1).map(function (n) { return n.name; }).join(".");
                                RS.Log.debug("normalised " + type.name + " to " + nsPath + "." + type.name);
                                type.name = nsPath + "." + type.name;
                                break;
                            }
                        }
                        break;
                    case "typeoftyperef" /* TypeOf */:
                        normaliseTypeRef(type.inner, namespaceHierarchy);
                        break;
                }
            }
            CodeGeneration.normaliseTypeRef = normaliseTypeRef;
            /**
             * Searches a hierarchy for the specified Named.
             * @param ns
             * @param typeName
             */
            function searchForNamed(rootNode, typeName) {
                if (!typeName) {
                    return { value: rootNode, prefix: "" };
                }
                var spl = typeName.split(".");
                switch (rootNode.kind) {
                    case "namespace":
                        var candidates = rootNode.children.filter(function (c) { return c.name === spl[0]; });
                        for (var _i = 0, candidates_1 = candidates; _i < candidates_1.length; _i++) {
                            var candidate = candidates_1[_i];
                            var result = searchForNamed(candidate, spl.slice(1).join("."));
                            if (result) {
                                return { value: result.value, prefix: "" + result.prefix + rootNode.name + "." };
                            }
                        }
                        return null;
                    // TODO: Handle other types
                    default:
                        return null;
                }
            }
            CodeGeneration.searchForNamed = searchForNamed;
        })(CodeGeneration = Build.CodeGeneration || (Build.CodeGeneration = {}));
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Module;
    (function (Module) {
        var DefaultTSConfigs;
        (function (DefaultTSConfigs) {
            var compilerOpts = {
                // Basic requirements
                noEmitHelpers: true,
                rootDir: ".",
                declaration: true,
                declarationMap: true,
                // Source maps, optional
                sourceMap: true,
                inlineSources: true,
                inlineSourceMap: false,
                // Optional compiler features
                experimentalDecorators: true,
                stripInternal: true,
                typeRoots: []
            };
            DefaultTSConfigs.source = {
                compilerOptions: __assign(__assign({}, compilerOpts), { outFile: "../build/src.js", target: "ES5", lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"] })
            };
            DefaultTSConfigs.buildSource = {
                compilerOptions: __assign(__assign({}, compilerOpts), { outFile: "../build/buildsrc.js", target: "ES5", lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"] })
            };
        })(DefaultTSConfigs = Module.DefaultTSConfigs || (Module.DefaultTSConfigs = {}));
    })(Module = RS.Module || (RS.Module = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Module;
    (function (Module) {
        /**
         * Gathers a complete ordered list of modules that satisfies all dependencies of the given list of module names, plus those modules themselves.
         * @param modules
         */
        function getDependencySet(modules) {
            // Gather all dependencies in a list
            // Log.debug(`Gathering dependency set for '${modules.join(", ")}'`);
            var list = new RS.List();
            var erroredFor = {};
            for (var _i = 0, modules_2 = modules; _i < modules_2.length; _i++) {
                var name_14 = modules_2[_i];
                var module_11 = Module.getModule(name_14);
                if (module_11 == null) {
                    if (!erroredFor[name_14]) {
                        RS.Log.warn("Module '" + name_14 + "' not found");
                        erroredFor[name_14] = true;
                    }
                    continue;
                }
                var depSet = module_11.gatherDependencySet();
                for (var _a = 0, _b = depSet.data; _a < _b.length; _a++) {
                    var depModule = _b[_a];
                    if (!list.contains(depModule)) {
                        list.add(depModule);
                    }
                }
                if (!list.contains(module_11)) {
                    list.add(module_11);
                }
            }
            // Sort by importance
            var curImportance = 0, matchedModules = -1;
            while (matchedModules !== 0) {
                matchedModules = 0;
                ++curImportance;
                for (var i = 0; i < list.length; ++i) {
                    var module_12 = list.get(i);
                    if (module_12.important === curImportance) {
                        ++matchedModules;
                        // Log.debug(`Floating '${module.name}'...`);
                        // "Float" the module as high as it can go in the list without violating dependencies
                        var idx = i - 1;
                        // Log.debug(`Test vs '${list.get(idx).name}'...`);
                        while (idx > 0 && module_12.dependencies.indexOf(list.get(idx)) === -1) {
                            // Log.debug(`Test vs '${list.get(idx).name}', no dependency found, moving up`);
                            --idx;
                        }
                        ++idx;
                        if (idx !== i) {
                            // Log.debug(`Moving '${module.name}' up by ${i - idx}`);
                            list.move(i, idx);
                        }
                        else {
                            // Log.debug(`Not moving '${module.name}' up`);
                        }
                    }
                }
            }
            // Place implementing modules immediately after abstract ones
            // NOTE: if module A implements module B, and depends on module C, which also depends on B, C will still appear before A
            // This is to handle the various abstract definitions provided by Core
            var implementingModules = list.filter(function (m) { return m.info.implements != null; }).data;
            var _loop_8 = function (concreteModule) {
                var abstractModule = Module.getModule(concreteModule.info.implements);
                var concreteDependencies = concreteModule.gatherDependencySet();
                var directDependents = list.filter(function (m) {
                    return m !== concreteModule && m !== abstractModule
                        && !concreteDependencies.contains(m)
                        && m.dependencies.indexOf(abstractModule) !== -1;
                });
                for (var i = directDependents.length - 1; i >= 0; --i) {
                    var dependent = directDependents.get(i);
                    var listIndex = list.indexOf(dependent);
                    var concreteIndex = list.indexOf(concreteModule);
                    // Does it already appear after?
                    if (concreteIndex < listIndex) {
                        continue;
                    }
                    // Relocate it to after here
                    list.move(listIndex, concreteIndex + 1);
                    correctDependents(dependent, list);
                }
            };
            for (var _c = 0, implementingModules_1 = implementingModules; _c < implementingModules_1.length; _c++) {
                var concreteModule = implementingModules_1[_c];
                _loop_8(concreteModule);
            }
            // Log.debug(`Result =\n${list.data.map((module) => module.name).join("\n")}`);
            return list;
        }
        Module.getDependencySet = getDependencySet;
        function correctDependents(module, list) {
            var index = list.indexOf(module);
            var dependents = list.filter(function (m) {
                return m.dependencies.indexOf(module) !== -1 &&
                    list.indexOf(m) < index;
            });
            for (var i = dependents.length - 1; i >= 0; --i) {
                var dependent = dependents.get(i);
                var dependentIndex = list.indexOf(dependent);
                list.move(dependentIndex, index + 1);
                correctDependents(dependent, list);
            }
        }
        /**
         * Sorts an array of modules into several batches, in which no module depends on another.
         * Modules in each batch can be processed in parallel, so long as the set of batches is processed serially.
         * This should be executed a complete dependency set.
         * @param modules
         * @param maxBatchSize
         */
        function getBatchSet(modules, maxBatchSize) {
            if (maxBatchSize === void 0) { maxBatchSize = 4; }
            var resolved = new RS.List();
            var remaining = new RS.List(modules);
            var batchList = new RS.List();
            while (remaining.length > 0) {
                var batch = new RS.List();
                for (var i = remaining.length - 1; i >= 0; --i) {
                    var module_13 = remaining.get(i);
                    // In order to add module to batch, all dependencies of it must be in resolved
                    var allDepsResolved = true;
                    for (var _i = 0, _a = module_13.dependencies; _i < _a.length; _i++) {
                        var depModule = _a[_i];
                        if (!resolved.contains(depModule)) {
                            allDepsResolved = false;
                            break;
                        }
                    }
                    if (allDepsResolved) {
                        // Accept for this batch
                        batch.add(module_13);
                        if (batch.length >= maxBatchSize) {
                            break;
                        }
                    }
                }
                if (batch.length === 0) {
                    throw new Error("getBatchSet: Infinite loop detected (circular dependency in module list?)");
                }
                resolved.addRange(batch);
                remaining.removeRange(batch);
                batchList.add(batch);
            }
            return batchList;
        }
        Module.getBatchSet = getBatchSet;
        /**
         * Compiles build components for all specified modules.
         * @param moduleList
         */
        function compileBuildComponents(moduleList) {
            return __awaiter(this, void 0, void 0, function () {
                var batches, _loop_9, _i, _a, batch;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            batches = getBatchSet(moduleList.data);
                            _loop_9 = function (batch) {
                                var errorCount, promises, _loop_10, _i, _a, module_14;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            errorCount = 0;
                                            promises = [];
                                            _loop_10 = function (module_14) {
                                                if (!module_14.buildLoaded) {
                                                    var promise = module_14.compileBuild()
                                                        .then(function (result) {
                                                        errorCount += result.errorCount;
                                                        return result;
                                                    }, function (err) {
                                                        RS.Log.error(err.stack || err, module_14.name);
                                                        ++errorCount;
                                                        return { workDone: false, errorCount: 1 };
                                                    });
                                                    promises.push(promise);
                                                }
                                            };
                                            for (_i = 0, _a = batch.data; _i < _a.length; _i++) {
                                                module_14 = _a[_i];
                                                _loop_10(module_14);
                                            }
                                            if (!(promises.length > 0)) return [3 /*break*/, 2];
                                            return [4 /*yield*/, Promise.all(promises)];
                                        case 1:
                                            _b.sent();
                                            if (errorCount > 0) {
                                                throw new Error(errorCount + " errors when compiling build components");
                                            }
                                            _b.label = 2;
                                        case 2: return [2 /*return*/];
                                    }
                                });
                            };
                            _i = 0, _a = batches.data;
                            _b.label = 1;
                        case 1:
                            if (!(_i < _a.length)) return [3 /*break*/, 4];
                            batch = _a[_i];
                            return [5 /*yield**/, _loop_9(batch)];
                        case 2:
                            _b.sent();
                            _b.label = 3;
                        case 3:
                            _i++;
                            return [3 /*break*/, 1];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        }
        Module.compileBuildComponents = compileBuildComponents;
    })(Module = RS.Module || (RS.Module = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var Scaffold = /** @class */ (function () {
            function Scaffold() {
                this._templates = [];
                this._params = {};
            }
            Scaffold.prototype.addTemplate = function (source, outputPath) {
                this._templates.push({ source: source, outputPath: outputPath });
            };
            Scaffold.prototype.addParam = function (key, displayName, example, defaultValue) {
                this._params[key] = { displayName: displayName, example: example, defaultValue: defaultValue };
            };
            Scaffold.prototype.promptForParams = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var result, _a, _b, _i, key, paramInfo, prompt_1, value;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                result = {};
                                _a = [];
                                for (_b in this._params)
                                    _a.push(_b);
                                _i = 0;
                                _c.label = 1;
                            case 1:
                                if (!(_i < _a.length)) return [3 /*break*/, 6];
                                key = _a[_i];
                                paramInfo = this._params[key];
                                prompt_1 = ["Enter " + (paramInfo.displayName || key)];
                                if (paramInfo.defaultValue) {
                                    prompt_1.push("(or blank for '" + paramInfo.defaultValue + "')");
                                }
                                if (paramInfo.example) {
                                    prompt_1.push("(e.g. '" + paramInfo.example + "')");
                                }
                                prompt_1.push(":");
                                value = null;
                                _c.label = 2;
                            case 2:
                                if (!!value) return [3 /*break*/, 4];
                                RS.Log.info(prompt_1.join(" "));
                                return [4 /*yield*/, RS.Input.readLine()];
                            case 3:
                                value = _c.sent();
                                if (!value) {
                                    value = paramInfo.defaultValue;
                                }
                                if (!value) {
                                    RS.Log.warn((paramInfo.displayName || key) + " is required.");
                                }
                                return [3 /*break*/, 2];
                            case 4:
                                result[key] = value;
                                _c.label = 5;
                            case 5:
                                _i++;
                                return [3 /*break*/, 1];
                            case 6: return [2 /*return*/, result];
                        }
                    });
                });
            };
            Scaffold.prototype.generate = function (targetModule) {
                return __awaiter(this, void 0, void 0, function () {
                    var args, _i, _a, template, src, path;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!targetModule.info.primarynamespace) {
                                    RS.Log.error("Module '" + targetModule.name + "' does not have a \"primarynamespace\" entry in module info. Scaffolding failed.");
                                    return [2 /*return*/, false];
                                }
                                return [4 /*yield*/, this.promptForParams()];
                            case 1:
                                args = _b.sent();
                                args["namespace"] = targetModule.info.primarynamespace;
                                _i = 0, _a = this._templates;
                                _b.label = 2;
                            case 2:
                                if (!(_i < _a.length)) return [3 /*break*/, 8];
                                template = _a[_i];
                                src = this.applyArgs(template.source, args);
                                path = this.applyArgs(RS.Path.combine(targetModule.path, template.outputPath), args);
                                return [4 /*yield*/, RS.FileSystem.createPath(RS.Path.directoryName(path))];
                            case 3:
                                _b.sent();
                                return [4 /*yield*/, RS.FileSystem.fileExists(path)];
                            case 4:
                                if (!_b.sent()) return [3 /*break*/, 5];
                                RS.Log.error("Didn't create '" + path + "' (already exists)!");
                                return [3 /*break*/, 7];
                            case 5: return [4 /*yield*/, RS.FileSystem.writeFile(path, src)];
                            case 6:
                                _b.sent();
                                RS.Log.info("Created '" + path + "'");
                                _b.label = 7;
                            case 7:
                                _i++;
                                return [3 /*break*/, 2];
                            case 8: return [2 /*return*/];
                        }
                    });
                });
            };
            Scaffold.prototype.applyArgs = function (str, args) {
                var _this = this;
                return str.replace(Scaffold._regexp, function (whole, argName, argFlag) {
                    if (argFlag) {
                        argFlag = argFlag.substr(1);
                    }
                    return _this.serialiseArg(args[argName], argFlag);
                });
            };
            Scaffold.prototype.serialiseArg = function (argValue, argFlag) {
                if (argValue == null) {
                    argValue = "missing";
                }
                if (argFlag && argFlag.toLowerCase() === "camelcase") {
                    // camelcase
                    // Camelcase
                    // camelCase
                    // CamelCase
                    var frontUpper_1 = argFlag[0] === "C";
                    var backUpper_1 = argFlag[5] === "C";
                    for (var i = 1; i < 9; ++i) {
                        if (i !== 5 && argFlag[i].toLowerCase() !== argFlag[i]) {
                            RS.Log.warn("Unsupported flag '" + argFlag + "' in scaffold template");
                            return argValue;
                        }
                    }
                    var segments = argValue.split(".");
                    return segments.map(function (s) {
                        var isFront = true;
                        var words = argValue.split(/(?=[A-Z])/g);
                        return words.map(function (w) {
                            var isCaps = isFront ? frontUpper_1 : backUpper_1;
                            isFront = false;
                            return "" + (isCaps ? w[0].toUpperCase() : w[0].toLowerCase()) + w.substr(1);
                        }).join("");
                    }).join(".");
                }
                return argValue;
            };
            Scaffold._regexp = /\{\{([a-zA-Z0-9]+)(:[a-zA-Z0-9]+)?\}\}/g;
            return Scaffold;
        }());
        Build.Scaffold = Scaffold;
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
/// <reference path="Base.ts" />
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var scaffolds = {};
        function registerScaffold(type, scaffold, description) {
            type = type.toLowerCase();
            scaffolds[type] = { scaffold: scaffold, description: description };
        }
        Build.registerScaffold = registerScaffold;
        function findScaffold(type) {
            type = type.toLowerCase();
            return scaffolds[type];
        }
        Build.findScaffold = findScaffold;
        function getAllScaffolds() {
            return scaffolds;
        }
        Build.getAllScaffolds = getAllScaffolds;
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="Build.ts" />
//https://www.fileformat.info/info/unicode/block/box_drawing/list.htm
//⊢
//⌞
//║
//│
//╠
//╚
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var displayDepthArg = new RS.IntegerEnvArg("DEPTH", 0).value;
        var displayDepth = 0;
        var showCount = new RS.BooleanEnvArg("COUNT", false);
        var targetModuleArg = new RS.EnvArg("MODULE", "");
        var rootModuleArg = new RS.EnvArg("ROOT", "");
        Tasks.dependencyTree = RS.Build.task({ name: "tree", description: "Shows Module dependency tree. E.g. \"DEPTH=2 COUNT=TRUE MODULE=\"Core\" ROOT=\"MyGame\" gulp tree\"" }, function () { return buildTree(); });
        /** Print out module dependency tree from the root module. */
        function buildTree() {
            return __awaiter(this, void 0, void 0, function () {
                var rootModuleName, rootModule, deps;
                return __generator(this, function (_a) {
                    try {
                        validateEnvArgs();
                    }
                    catch (e) {
                        RS.Log.error("Failed to validate Environment Arguments:");
                        RS.Log.error(e);
                        RS.Log.error("Aborting");
                        return [2 /*return*/];
                    }
                    rootModuleName = (rootModuleArg.value == "") ? RS.Config.activeConfig.rootModule : rootModuleArg.value;
                    rootModule = RS.Module.getModule(rootModuleName);
                    deps = printDependencies(rootModule, 0);
                    if (deps.deps.length == 0) {
                        RS.Log.error("No module dependencies found for root module '" + rootModule.name + "'.");
                        if (targetModuleArg.value !== "") {
                            RS.Log.error("Target module '" + targetModuleArg.value + "' was specified, check it was spelt correctly.");
                        }
                        return [2 /*return*/];
                    }
                    RS.Log.info("\n" + __spreadArrays([getModule(rootModule, "")], deps.deps).join('\n'));
                    return [2 /*return*/];
                });
            });
        }
        Tasks.buildTree = buildTree;
        ;
        /** Return the printout of a module's dependencies. */
        function printDependencies(module, currentDepth, prefix) {
            if (prefix === void 0) { prefix = "  "; }
            var depStrings = [];
            var hasModule = false;
            for (var _i = 0, _a = module.dependencies; _i < _a.length; _i++) {
                var dep = _a[_i];
                var isTarget = (targetModuleArg.value == "" || targetModuleArg.value == dep.name);
                if (isTarget) {
                    hasModule = true;
                }
                var lastDependency = (dep == module.dependencies[module.dependencies.length - 1]);
                var newPrefix = lastDependency ? "╚═" : "╠═";
                if (displayDepth == 0 || currentDepth < displayDepth) {
                    var deps = printDependencies(dep, (currentDepth + 1), "" + prefix + (lastDependency ? "" : "║") + "    ");
                    hasModule = hasModule || deps.hasModule;
                    if (deps.hasModule || isTarget) {
                        depStrings.push(getModule(dep, "" + prefix + newPrefix + " "));
                        depStrings.push.apply(depStrings, deps.deps);
                    }
                }
            }
            return { deps: depStrings, hasModule: hasModule };
        }
        /** Get the total number of dependency nodes of a module. */
        /** TODO: Seperate the tree navigation and the formatting, to avoid re-counting nodes */
        function getDependencyCount(module) {
            if (module.dependencies.length == 0) {
                return 0;
            }
            return module.dependencies.map(function (d) { return getDependencyCount(d); }).reduceRight(function (a, b) { return a + b; }) + module.dependencies.length;
        }
        /** Get a formatted string of a specific module entry. */
        function getModule(module, prefix) {
            var depCount = showCount.value ? getDependencyCount(module) : 0;
            return "" + prefix + module.name + " " + (depCount > 0 ? "[" + depCount + "]" : "");
        }
        /** Check that the environment args are valid, throws an error if not. */
        function validateEnvArgs() {
            displayDepth = Math.max(0, displayDepthArg);
            validateModuleExists(targetModuleArg.value, "Failed to validate env arg \"MODULE\"");
            validateModuleExists(rootModuleArg.value, "Failed to validate env arg \"ROOT\"");
        }
        /** Validates that a module exists */
        function validateModuleExists(moduleName, errorPrefix) {
            if (moduleName == "") {
                return;
            }
            if (!RS.Module.getModule(moduleName)) {
                throw Error(errorPrefix + " - module '" + moduleName + "' could not be found.");
            }
        }
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Build.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        var version = new RS.EnvArg("VERSION", "", true);
        function doDocs(moduleList) {
            return __awaiter(this, void 0, void 0, function () {
                var compileCnt, _i, _a, module_15, srcUnit, timer, codeSources, thirdPartyCodeSources, _b, _c, module_16, srcUnit, _d, _e, _f, docsDirectory, template, readme, title, err_12;
                return __generator(this, function (_g) {
                    switch (_g.label) {
                        case 0:
                            compileCnt = 0;
                            _i = 0, _a = moduleList.data;
                            _g.label = 1;
                        case 1:
                            if (!(_i < _a.length)) return [3 /*break*/, 4];
                            module_15 = _a[_i];
                            srcUnit = module_15.getCompilationUnit(RS.Module.CompilationUnits.Source);
                            if (!srcUnit) return [3 /*break*/, 3];
                            return [4 /*yield*/, srcUnit.init()];
                        case 2:
                            _g.sent();
                            _g.label = 3;
                        case 3:
                            _i++;
                            return [3 /*break*/, 1];
                        case 4:
                            RS.Log.info("Assembling code...");
                            timer = new RS.Timer();
                            timer.start();
                            codeSources = [];
                            thirdPartyCodeSources = {};
                            _b = 0, _c = moduleList.data;
                            _g.label = 5;
                        case 5:
                            if (!(_b < _c.length)) return [3 /*break*/, 12];
                            module_16 = _c[_b];
                            srcUnit = module_16.getCompilationUnit(RS.Module.CompilationUnits.Source);
                            if (!srcUnit) return [3 /*break*/, 9];
                            _e = (_d = codeSources).push;
                            _f = {
                                name: srcUnit.name
                            };
                            return [4 /*yield*/, RS.FileSystem.readFile(srcUnit.outDTSFile)];
                        case 6:
                            _f.dtsContent = _g.sent();
                            return [4 /*yield*/, RS.FileSystem.readFile(srcUnit.outJSFile)];
                        case 7:
                            _f.content = _g.sent();
                            return [4 /*yield*/, RS.FileSystem.readFile(srcUnit.outMapFile)];
                        case 8:
                            _e.apply(_d, [(_f.sourceMap = _g.sent(),
                                    _f.sourceMapDir = srcUnit.path,
                                    _f)]);
                            _g.label = 9;
                        case 9: return [4 /*yield*/, RS.Assembler.gatherThirdPartyDependencies(module_16, thirdPartyCodeSources)];
                        case 10:
                            _g.sent();
                            _g.label = 11;
                        case 11:
                            _b++;
                            return [3 /*break*/, 5];
                        case 12:
                            docsDirectory = "docs";
                            return [4 /*yield*/, RS.FileSystem.createPath(docsDirectory)];
                        case 13:
                            _g.sent();
                            // Assemble dts files
                            return [4 /*yield*/, Promise.all([
                                    RS.Assembler.assembleDts(docsDirectory, "docs", codeSources, {
                                        withHelpers: false,
                                        withSourcemaps: false,
                                        minify: false
                                    })
                                ])];
                        case 14:
                            // Assemble dts files
                            _g.sent();
                            RS.Log.info("Assembled in " + (timer.elapsed / 1000).toFixed(2) + " s");
                            timer.stop();
                            RS.Log.info("Generating docs...");
                            timer = new RS.Timer();
                            timer.start();
                            //Rename .d.ts file to .ts for TypeDoc consumption
                            return [4 /*yield*/, RS.FileSystem.copyFile(RS.Path.combine(docsDirectory, "docs.d.ts"), RS.Path.combine(docsDirectory, "Synergy.ts"))];
                        case 15:
                            //Rename .d.ts file to .ts for TypeDoc consumption
                            _g.sent();
                            return [4 /*yield*/, RS.FileSystem.deletePath(RS.Path.combine(docsDirectory, "docs.d.ts"))];
                        case 16:
                            _g.sent();
                            _g.label = 17;
                        case 17:
                            _g.trys.push([17, 19, , 20]);
                            template = RS.Path.combine(__dirname, "docs", "template");
                            readme = RS.Path.combine(__dirname, "docs", "README.md");
                            title = "Synergy API Documentation";
                            if (version.isSet) {
                                title += " | v" + version.value;
                            }
                            return [4 /*yield*/, RS.Command.run("node", [
                                    "node_modules/typedoc/bin/typedoc",
                                    "--out", RS.Path.combine(docsDirectory, "output"),
                                    "--ignoreCompilerErrors",
                                    "--target", "ES5",
                                    "--theme", template,
                                    "--readme", readme,
                                    "--name", title,
                                    docsDirectory
                                ], undefined, false)];
                        case 18:
                            _g.sent();
                            return [3 /*break*/, 20];
                        case 19:
                            err_12 = _g.sent();
                            return [3 /*break*/, 20];
                        case 20:
                            RS.Log.info("Docs generated in in " + (timer.elapsed / 1000).toFixed(2) + " s");
                            timer.stop();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Tasks.documentAll = RS.Build.task({ name: "docs", requires: [Tasks.buildAll], description: "Builds synergy documentation" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var allModules;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        allModules = RS.Module.getDependencySet(RS.Module.getAll().map(function (m) { return m.name; }));
                        // Create docs
                        return [4 /*yield*/, doDocs(allModules)];
                    case 1:
                        // Create docs
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var _this = this;
        var tsFormatter = require("typescript-formatter");
        var formatOptions = {
            tsfmt: true,
            tsfmtFile: RS.Path.combine(__dirname, "linting", "tsfmt.json")
        };
        function doFormat(modules) {
            return __awaiter(this, void 0, void 0, function () {
                var _i, _a, module_17, srcFiles, buildSrcFiles, allFiles, resultMap, formatCount, _b, _c, _d, fileName, result, err_13;
                return __generator(this, function (_e) {
                    switch (_e.label) {
                        case 0:
                            _i = 0, _a = modules.data;
                            _e.label = 1;
                        case 1:
                            if (!(_i < _a.length)) return [3 /*break*/, 14];
                            module_17 = _a[_i];
                            return [4 /*yield*/, RS.FileSystem.readFiles(RS.Path.combine(module_17.path, "src"))];
                        case 2:
                            srcFiles = _e.sent();
                            return [4 /*yield*/, RS.FileSystem.readFiles(RS.Path.combine(module_17.path, "buildsrc"))];
                        case 3:
                            buildSrcFiles = _e.sent();
                            allFiles = __spreadArrays(srcFiles, buildSrcFiles).filter(function (file) { return RS.Path.extension(file) === ".ts"; });
                            if (!(allFiles.length > 0)) return [3 /*break*/, 13];
                            RS.Log.pushContext(module_17.name);
                            RS.Log.info("Formatting " + allFiles.length + " ts files...");
                            _e.label = 4;
                        case 4:
                            _e.trys.push([4, 11, , 12]);
                            return [4 /*yield*/, tsFormatter.processFiles(allFiles, formatOptions)];
                        case 5:
                            resultMap = _e.sent();
                            formatCount = 0;
                            _b = [];
                            for (_c in resultMap)
                                _b.push(_c);
                            _d = 0;
                            _e.label = 6;
                        case 6:
                            if (!(_d < _b.length)) return [3 /*break*/, 10];
                            fileName = _b[_d];
                            result = resultMap[fileName];
                            if (!result.error) return [3 /*break*/, 7];
                            RS.Log.warn(result.fileName + ": " + result.message);
                            return [3 /*break*/, 9];
                        case 7:
                            if (!RS.Is.string(result.dest)) return [3 /*break*/, 9];
                            ++formatCount;
                            if (!(result.dest !== result.src)) return [3 /*break*/, 9];
                            return [4 /*yield*/, RS.FileSystem.writeFile(fileName, result.dest)];
                        case 8:
                            _e.sent();
                            _e.label = 9;
                        case 9:
                            _d++;
                            return [3 /*break*/, 6];
                        case 10:
                            if (formatCount !== allFiles.length) {
                                RS.Log.warn("Only formatted " + formatCount + " of " + allFiles.length + " files");
                            }
                            return [3 /*break*/, 12];
                        case 11:
                            err_13 = _e.sent();
                            RS.Log.error(err_13.stack || err_13);
                            return [3 /*break*/, 12];
                        case 12:
                            RS.Log.popContext(module_17.name);
                            _e.label = 13;
                        case 13:
                            _i++;
                            return [3 /*break*/, 1];
                        case 14: return [2 /*return*/];
                    }
                });
            });
        }
        /**
         * Executes code formatter for the root module only.
         */
        Build.format = Build.task({ name: "format", description: "Formats the root module" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var config, rootModule, allModules;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        return [4 /*yield*/, RS.Config.get()];
                    case 1:
                        config = _a.sent();
                        rootModule = RS.Module.getModule(config.rootModule);
                        if (rootModule == null) {
                            RS.Log.error("No root module found");
                            return [2 /*return*/];
                        }
                        allModules = new RS.List([rootModule]);
                        // Do code formatting
                        return [4 /*yield*/, doFormat(allModules)];
                    case 2:
                        // Do code formatting
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        /**
         * Executes code formatter for a specific set of modules only.
         */
        Build.formatOnly = Build.task({ name: "format-only", description: "Formats the specified modules. Use: 'ONLY=MOD,MOD2 gulp format-only'" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var inputModulesRaw, inputModules, allModules, _i, inputModules_1, moduleName, module_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        RS.Log.info("Please enter the names of the modules to formatted, separated by comma:");
                        return [4 /*yield*/, RS.Input.readLine()];
                    case 1:
                        inputModulesRaw = _a.sent();
                        inputModules = inputModulesRaw.split(",").map(function (name) { return name.trim(); });
                        if (inputModules.length < 0 || inputModules[0].length === 0) {
                            return [2 /*return*/];
                        }
                        allModules = new RS.List();
                        for (_i = 0, inputModules_1 = inputModules; _i < inputModules_1.length; _i++) {
                            moduleName = inputModules_1[_i];
                            module_18 = RS.Module.getModule(moduleName);
                            if (module_18 == null) {
                                RS.Log.warn("Module '" + moduleName + "' not found");
                            }
                            else {
                                allModules.add(module_18);
                            }
                        }
                        if (allModules.length === 0) {
                            return [2 /*return*/];
                        }
                        // Do code formatting
                        return [4 /*yield*/, doFormat(allModules)];
                    case 2:
                        // Do code formatting
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        /**
         * Executes code formatter for all loaded modules.
         */
        Build.formatAll = Build.task({ name: "format-all", description: "Formats all modules" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var config, rootModule, allModules;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        return [4 /*yield*/, RS.Config.get()];
                    case 1:
                        config = _a.sent();
                        rootModule = RS.Module.getModule(config.rootModule);
                        if (rootModule == null) {
                            RS.Log.error("No root module found");
                            return [2 /*return*/];
                        }
                        allModules = RS.Module.getDependencySet(RS.Module.getAll().map(function (m) { return m.name; }));
                        // Do code formatting
                        return [4 /*yield*/, doFormat(allModules)];
                    case 2:
                        // Do code formatting
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="Build.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.manual = RS.Build.task({ name: "help", description: "Displays task descriptions" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var tasks, _i, tasks_2, task, rootConfig, rootTasks, _a, rootTasks_1, rootTask;
            return __generator(this, function (_b) {
                tasks = RS.Build.getTasks();
                RS.Log.info("Synergy gulp tasks");
                RS.Log.info("************************");
                for (_i = 0, tasks_2 = tasks; _i < tasks_2.length; _i++) {
                    task = tasks_2[_i];
                    RS.Log.info(task.settings.name + ": " + (task.settings.description || "unknown"));
                }
                rootConfig = RS.Config.activeConfig;
                if (rootConfig.tasks) {
                    rootTasks = Object.keys(rootConfig.tasks);
                    for (_a = 0, rootTasks_1 = rootTasks; _a < rootTasks_1.length; _a++) {
                        rootTask = rootTasks_1[_a];
                        RS.Log.info(rootTask + ": " + (rootConfig.tasks[rootTask].description || "custom task"));
                    }
                }
                return [2 /*return*/];
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.list = RS.Build.task({ name: "list", description: "Lists all modules" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var allModules, _i, allModules_2, module_19;
            return __generator(this, function (_a) {
                allModules = RS.Module.getAll();
                for (_i = 0, allModules_2 = allModules; _i < allModules_2.length; _i++) {
                    module_19 = allModules_2[_i];
                    RS.Log.info(module_19.name);
                }
                return [2 /*return*/];
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Input;
    (function (Input) {
        var readline = require("readline");
        var Writable = require("stream").Writable;
        /**
         * Waits for a line of user input.
         */
        function readLine(completer, muted) {
            return new Promise(function (resolve, reject) {
                var outStream;
                var shouldMute = false;
                if (muted) {
                    outStream = new Writable({
                        write: function (chunk, encoding, callback) {
                            if (!shouldMute) {
                                process.stdout.write(chunk, encoding);
                            }
                            callback();
                        }
                    });
                }
                else {
                    outStream = process.stdout;
                }
                var rl = readline.createInterface({
                    input: process.stdin,
                    output: outStream,
                    terminal: true,
                    completer: completer
                });
                rl.prompt();
                shouldMute = true;
                rl.on("line", function (line) {
                    rl.close();
                    if (muted) {
                        console.log("");
                    }
                    resolve(line);
                });
            });
        }
        Input.readLine = readLine;
    })(Input = RS.Input || (RS.Input = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="../util/Input.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.NewModule = RS.Build.task({ name: "new", description: "Utility for building a new module" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var name, nameSplit, bestMatch, bestMatchSegs, _i, _a, module_20, moduleName, matchSegs, i, l, rootPath, i, l, path, formattedName, moduleInfo, tsconfig;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        RS.Log.pushContext("NewModule");
                        RS.Log.info("Enter the name of the module to use: ");
                        return [4 /*yield*/, RS.Input.readLine()];
                    case 1:
                        name = _b.sent();
                        nameSplit = name.split(".");
                        bestMatch = null;
                        bestMatchSegs = 0;
                        for (_i = 0, _a = RS.Module.getAll(); _i < _a.length; _i++) {
                            module_20 = _a[_i];
                            moduleName = module_20.name.split(".");
                            matchSegs = Math.min(moduleName.length, nameSplit.length);
                            for (i = 0, l = matchSegs; i < l; ++i) {
                                if (moduleName[i] !== nameSplit[i]) {
                                    matchSegs = i;
                                    break;
                                }
                            }
                            if (matchSegs > bestMatchSegs) {
                                bestMatch = module_20;
                                bestMatchSegs = matchSegs;
                            }
                        }
                        if (bestMatch != null) {
                            rootPath = bestMatch.path;
                            for (i = 0, l = bestMatch.name.match(/\./g).length; i <= l; ++i) {
                                rootPath = RS.Path.combine(rootPath, "..");
                            }
                            rootPath = RS.Path.normalise(rootPath);
                        }
                        else {
                            rootPath = "modules";
                        }
                        path = RS.Path.combine(rootPath, name.replace(/\./g, "/"));
                        return [4 /*yield*/, RS.FileSystem.createPath(path)];
                    case 2:
                        _b.sent();
                        formattedName = RS.Module.formatName(name);
                        moduleInfo = {
                            title: name,
                            description: "Description goes here",
                            dependencies: ["Core"]
                        };
                        moduleInfo["$schema"] = RS.Path.relative(path, "schemas/moduleinfo.json");
                        if (name !== formattedName) {
                            moduleInfo.name = name;
                        }
                        return [4 /*yield*/, RS.FileSystem.writeFile(RS.Path.combine(path, "moduleinfo.json"), RS.JSON.stringify(moduleInfo, null, 2))];
                    case 3:
                        _b.sent();
                        return [4 /*yield*/, RS.FileSystem.createPath(RS.Path.combine(path, "src"))];
                    case 4:
                        _b.sent();
                        tsconfig = {
                            compilerOptions: {
                                target: "es5",
                                lib: [
                                    "dom",
                                    "es2015.iterable",
                                    "es2015.promise",
                                    "es5"
                                ],
                                rootDir: ".",
                                outFile: "../build/src.js",
                                sourceMap: true,
                                inlineSourceMap: false,
                                inlineSources: true,
                                experimentalDecorators: true,
                                declaration: true,
                                noEmitHelpers: true
                            }
                        };
                        return [4 /*yield*/, RS.FileSystem.writeFile(RS.Path.combine(path, "src", "tsconfig.json"), RS.JSON.stringify(tsconfig, null, 2))];
                    case 5:
                        _b.sent();
                        RS.Log.info("Created module by name '" + name + "' at '" + path + "'.");
                        RS.Log.popContext("NewModule");
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.noop = RS.Build.task({ name: "noop", description: "Debug Task" }, function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                RS.Log.debug("no-op");
                return [2 /*return*/];
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../util/Task.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Build.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        var onlyEnvArg = new RS.EnvArg("ONLY", "");
        var defaultTSConfig = {
            compilerOptions: {
                // Basic requirements
                noEmitHelpers: true,
                rootDir: ".",
                declaration: true,
                declarationMap: true,
                // Source maps, optional
                sourceMap: true,
                inlineSources: true,
                inlineSourceMap: false,
                // Optional compiler features
                experimentalDecorators: true,
                stripInternal: true,
                outFile: "../build/testsrc.js",
                target: "ES5",
                lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"],
                typeRoots: []
            }
        };
        function doTests(moduleList) {
            return __awaiter(this, void 0, void 0, function () {
                var moduleTestUnits, testDependencies, testDefinitionsPath, compileCnt, _i, _a, module_21, saveChecksums, srcUnit, unit, testDeps, _b, testDeps_1, testDep, testModule, dependencySet, requiredTestDependencies, testDependencySet, buildStages, _c, buildStages_4, stage, result;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            moduleTestUnits = new RS.List();
                            testDependencies = new RS.List();
                            testDefinitionsPath = RS.Path.combine(__dirname, "testing", "definitions");
                            compileCnt = 0;
                            _i = 0, _a = moduleList.data;
                            _d.label = 1;
                        case 1:
                            if (!(_i < _a.length)) return [3 /*break*/, 7];
                            module_21 = _a[_i];
                            saveChecksums = false;
                            srcUnit = module_21.getCompilationUnit(RS.Module.CompilationUnits.Source);
                            if (!(srcUnit && srcUnit.validityState === RS.Build.CompilationUnit.ValidState.Uninitialised)) return [3 /*break*/, 3];
                            return [4 /*yield*/, srcUnit.init()];
                        case 2:
                            _d.sent();
                            _d.label = 3;
                        case 3:
                            unit = module_21.addCompilationUnit("tests", "tests", defaultTSConfig);
                            if (srcUnit) {
                                unit.dependencies.add({
                                    kind: RS.Build.CompilationUnit.DependencyKind.CompilationUnit,
                                    name: srcUnit.name,
                                    compilationUnit: srcUnit
                                });
                                unit.dependencies.add({
                                    kind: RS.Build.CompilationUnit.DependencyKind.DefinitionsFolder,
                                    name: srcUnit.name + ".Dependencies",
                                    path: RS.Path.combine(srcUnit.path, "dependencies")
                                });
                                unit.dependencies.add({
                                    kind: RS.Build.CompilationUnit.DependencyKind.DefinitionsFolder,
                                    name: "testing",
                                    path: testDefinitionsPath
                                });
                                testDeps = srcUnit.module.info.testdependencies || [];
                                for (_b = 0, testDeps_1 = testDeps; _b < testDeps_1.length; _b++) {
                                    testDep = testDeps_1[_b];
                                    testModule = RS.Module.getModule(testDep);
                                    dependencySet = testModule.gatherDependencySet();
                                    testDependencies.addRange(dependencySet);
                                }
                            }
                            RS.Module.CompilationDependencies.addModule(unit, module_21, RS.Module.CompilationUnits.Source);
                            return [4 /*yield*/, Tasks.Testing.initialiseUnit(unit)];
                        case 4:
                            if (_d.sent()) {
                                ++compileCnt;
                                saveChecksums = true;
                            }
                            if (Tasks.Testing.isUnitValid(unit)) {
                                moduleTestUnits.add(unit);
                            }
                            if (!saveChecksums) return [3 /*break*/, 6];
                            return [4 /*yield*/, module_21.saveChecksums()];
                        case 5:
                            _d.sent();
                            _d.label = 6;
                        case 6:
                            _i++;
                            return [3 /*break*/, 1];
                        case 7:
                            testDependencies.removeDuplicates();
                            requiredTestDependencies = testDependencies.data
                                .filter(function (module) {
                                var depUnit = module.getCompilationUnit(RS.Module.CompilationUnits.Source);
                                if (!depUnit) {
                                    return false;
                                }
                                return depUnit.validityState === RS.Build.CompilationUnit.ValidState.Uninitialised;
                            })
                                .map(function (module) { return module.name; });
                            if (!(requiredTestDependencies.length > 0)) return [3 /*break*/, 11];
                            RS.Log.info("Building test dependencies...");
                            testDependencySet = RS.Module.getDependencySet(requiredTestDependencies);
                            buildStages = RS.BuildStage.getStages();
                            _c = 0, buildStages_4 = buildStages;
                            _d.label = 8;
                        case 8:
                            if (!(_c < buildStages_4.length)) return [3 /*break*/, 11];
                            stage = buildStages_4[_c];
                            return [4 /*yield*/, stage.executeSafe(testDependencySet)];
                        case 9:
                            result = _d.sent();
                            if (result.errorCount > 0) {
                                throw new Error(result.errorCount + " errors when executing build stage");
                            }
                            _d.label = 10;
                        case 10:
                            _c++;
                            return [3 /*break*/, 8];
                        case 11:
                            // If we have no tests, exit
                            if (moduleTestUnits.length === 0) {
                                RS.Log.warn("No tests to run!");
                                return [2 /*return*/];
                            }
                            if (compileCnt === 0) {
                                RS.Log.info("No changes to module tests detected");
                            }
                            if (!(moduleTestUnits.length > 0)) return [3 /*break*/, 13];
                            return [4 /*yield*/, doUnitTests(moduleTestUnits)];
                        case 12:
                            _d.sent();
                            _d.label = 13;
                        case 13: return [2 /*return*/];
                    }
                });
            });
        }
        function doUnitTests(units) {
            return __awaiter(this, void 0, void 0, function () {
                var testBuildDir, timer, promises, _i, _a, unit, codeSources, depSet, results;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            RS.Log.info("Assembling tests...");
                            testBuildDir = "tests";
                            timer = new RS.Timer();
                            timer.start();
                            return [4 /*yield*/, RS.FileSystem.deletePath(testBuildDir)];
                        case 1:
                            _b.sent();
                            return [4 /*yield*/, RS.FileSystem.createPath(testBuildDir)];
                        case 2:
                            _b.sent();
                            promises = [];
                            // Gather all third party dependencies
                            promises.push(Tasks.Testing.assembleThirdPartyJS(units, testBuildDir));
                            _i = 0, _a = units.data;
                            _b.label = 3;
                        case 3:
                            if (!(_i < _a.length)) return [3 /*break*/, 7];
                            unit = _a[_i];
                            codeSources = [];
                            depSet = RS.Module.getDependencySet(__spreadArrays([unit.module.name], (unit.module.info.testdependencies || [])));
                            return [4 /*yield*/, RS.Assembler.CodeSources.addModules(codeSources, depSet.data, RS.Module.CompilationUnits.Source)];
                        case 4:
                            _b.sent();
                            return [4 /*yield*/, RS.Assembler.CodeSources.addUnit(codeSources, unit)];
                        case 5:
                            _b.sent();
                            // Assemble js
                            promises.push(RS.Assembler.assembleJS(testBuildDir, unit.module.name + ".spec", codeSources, {
                                withHelpers: true,
                                withSourcemaps: false,
                                minify: false
                            }));
                            _b.label = 6;
                        case 6:
                            _i++;
                            return [3 /*break*/, 3];
                        case 7: return [4 /*yield*/, Promise.all(promises)];
                        case 8:
                            _b.sent();
                            RS.Log.info("Assembled in " + (timer.elapsed / 1000).toFixed(2) + " s");
                            timer.stop();
                            return [4 /*yield*/, Tasks.Testing.runKarma(RS.Path.combine(__dirname, "testing", "karma.conf.js"))];
                        case 9:
                            results = _b.sent();
                            if (results.failed > 0) {
                                throw new Error(results.failed + " tests failed");
                            }
                            if (results.error || results.exitCode !== 0) {
                                throw new Error("Error while running tests");
                            }
                            return [2 /*return*/];
                    }
                });
            });
        }
        Tasks.test = RS.Build.task({ name: "test", requires: [Tasks.build], description: "Runs unit tests for root module and its dependencies" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var config, rootModule, allDeps;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        return [4 /*yield*/, RS.Config.get()];
                    case 1:
                        config = _a.sent();
                        rootModule = RS.Module.getModule(config.rootModule);
                        if (rootModule == null) {
                            RS.Log.error("No root module found");
                            return [2 /*return*/];
                        }
                        allDeps = RS.Module.getDependencySet(__spreadArrays([RS.Config.activeConfig.rootModule], RS.Config.activeConfig.extraModules));
                        // Run tests
                        return [4 /*yield*/, doTests(allDeps)];
                    case 2:
                        // Run tests
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        Tasks.testOnly = RS.Build.task({ name: "test-only", requires: [Tasks.buildOnly], description: "Runs unit tests for specified modules. Use: \"ONLY=MOD,MOD2 gulp test-only\"" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var inputModulesRaw, inputModules, allModules, _i, inputModules_2, moduleName, module_22;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        if (!(onlyEnvArg.value !== onlyEnvArg.defaultValue)) return [3 /*break*/, 1];
                        inputModulesRaw = onlyEnvArg.value;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, RS.Input.readLine()];
                    case 2:
                        inputModulesRaw = _a.sent();
                        _a.label = 3;
                    case 3:
                        inputModules = inputModulesRaw.split(",").map(function (name) { return name.trim(); });
                        if (inputModules.length < 0 || inputModules[0].length === 0) {
                            return [2 /*return*/];
                        }
                        allModules = new RS.List();
                        for (_i = 0, inputModules_2 = inputModules; _i < inputModules_2.length; _i++) {
                            moduleName = inputModules_2[_i];
                            module_22 = RS.Module.getModule(moduleName);
                            if (module_22 == null) {
                                RS.Log.warn("Module '" + moduleName + "' not found");
                            }
                            else {
                                allModules.add(module_22);
                            }
                        }
                        if (allModules.length === 0) {
                            return [2 /*return*/];
                        }
                        // Run tests
                        return [4 /*yield*/, doTests(allModules)];
                    case 4:
                        // Run tests
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        Tasks.testAll = RS.Build.task({ name: "test-all", requires: [Tasks.buildAll], description: "Runs unit tests for all modules" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var allModules;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RS.Profile.reset();
                        allModules = RS.Module.getDependencySet(RS.Module.getAll().map(function (m) { return m.name; }));
                        // Run tests
                        return [4 /*yield*/, doTests(allModules)];
                    case 1:
                        // Run tests
                        _a.sent();
                        if (RS.EnvArgs.profile.value) {
                            RS.Profile.dump();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="../scaffold/API.ts" />
/// <reference path="../util/Task.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        Tasks.scaffold = RS.Build.task({ name: "scaffold", description: "Outputs templates for various types of file" }, function () { return __awaiter(_this, void 0, void 0, function () {
            function completer(line) {
                var hits = keys.filter(function (k) { return k.length >= line.length && k.substr(0, line.length) === line; });
                // show all completions if none found
                return [hits.length ? hits : keys, line];
            }
            var rootModule, all, keys, _i, keys_2, type, info, scaffold, type, scaffoldInfo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        rootModule = RS.Module.getModule(RS.Config.activeConfig.rootModule);
                        if (!rootModule) {
                            RS.Log.error("No root module found. Scaffolding failed.");
                            return [2 /*return*/];
                        }
                        all = RS.Build.getAllScaffolds();
                        keys = Object.keys(all);
                        if (keys.length === 0) {
                            RS.Log.error("No scaffolds found.");
                            return [2 /*return*/];
                        }
                        RS.Log.info("Enter a scaffold from the list below:");
                        for (_i = 0, keys_2 = keys; _i < keys_2.length; _i++) {
                            type = keys_2[_i];
                            info = all[type];
                            if (info.description) {
                                RS.Log.info(" " + type + ": " + info.description);
                            }
                            else {
                                RS.Log.info(" " + type);
                            }
                        }
                        scaffold = null;
                        _a.label = 1;
                    case 1:
                        if (!!scaffold) return [3 /*break*/, 3];
                        return [4 /*yield*/, RS.Input.readLine(completer)];
                    case 2:
                        type = _a.sent();
                        if (!type) {
                            return [2 /*return*/];
                        }
                        scaffoldInfo = RS.Build.findScaffold(type);
                        if (!scaffoldInfo) {
                            RS.Log.error("Unknown scaffold type '" + type + "'");
                        }
                        else {
                            scaffold = scaffoldInfo.scaffold;
                        }
                        return [3 /*break*/, 1];
                    case 3: return [4 /*yield*/, scaffold.generate(rootModule)];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
/// <reference path="Assemble.ts" />
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var _this = this;
        var port = new RS.IntegerEnvArg("PORT", 8080);
        var cache = new RS.BooleanEnvArg("CACHE", false);
        var debug = new RS.BooleanEnvArg("DEBUG", false);
        Tasks.serve = RS.Build.task({ name: "serve", requires: [Tasks.assemble], description: "Assembles and serves the project using a minimal server" }, function () { return __awaiter(_this, void 0, void 0, function () {
            var modulePath, binPath, config, rootDir, args;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        modulePath = RS.Path.combine(process.cwd(), "node_modules", "http-server");
                        binPath = RS.Path.combine(modulePath, "bin", "http-server");
                        return [4 /*yield*/, RS.Config.get()];
                    case 1:
                        config = _a.sent();
                        rootDir = config.assemblePath || "";
                        args = [rootDir, "-p" + port.value];
                        if (!cache.value) {
                            // Disable client caching
                            args.push("-c-1");
                        }
                        RS.Log.info("You can also install http-server globally to serve any arbitary folder.");
                        RS.Log.info("It's as simple as:");
                        RS.Log.info("npm install -g http-server");
                        RS.Log.info("http-server <path>");
                        RS.Log.info("Listening on http://localhost:" + port.value);
                        return [4 /*yield*/, RS.Command.run(binPath, args, null, debug.value)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Tasks;
    (function (Tasks) {
        var Testing;
        (function (Testing) {
            function assembleThirdPartyJS(testUnits, dir) {
                return __awaiter(this, void 0, void 0, function () {
                    var thirdPartyCodeSources, _i, _a, unit, depSet, _b, _c, module_23;
                    return __generator(this, function (_d) {
                        switch (_d.label) {
                            case 0:
                                thirdPartyCodeSources = {};
                                _i = 0, _a = testUnits.data;
                                _d.label = 1;
                            case 1:
                                if (!(_i < _a.length)) return [3 /*break*/, 6];
                                unit = _a[_i];
                                depSet = RS.Module.getDependencySet(__spreadArrays([unit.module.name], (unit.module.info.testdependencies || [])));
                                _b = 0, _c = depSet.data;
                                _d.label = 2;
                            case 2:
                                if (!(_b < _c.length)) return [3 /*break*/, 5];
                                module_23 = _c[_b];
                                return [4 /*yield*/, RS.Assembler.gatherThirdPartyDependencies(module_23, thirdPartyCodeSources)];
                            case 3:
                                _d.sent();
                                _d.label = 4;
                            case 4:
                                _b++;
                                return [3 /*break*/, 2];
                            case 5:
                                _i++;
                                return [3 /*break*/, 1];
                            case 6: return [4 /*yield*/, RS.Assembler.assembleJS(dir, "thirdparty", Object.keys(thirdPartyCodeSources).map(function (k) { return thirdPartyCodeSources[k]; }), {
                                    withHelpers: false,
                                    withSourcemaps: false,
                                    minify: true
                                })];
                            case 7:
                                _d.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            }
            Testing.assembleThirdPartyJS = assembleThirdPartyJS;
            function executeUnit(unit) {
                return __awaiter(this, void 0, void 0, function () {
                    var result, _i, _a, err;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, unit.execute()];
                            case 1:
                                result = _b.sent();
                                if (!result.success) {
                                    for (_i = 0, _a = result.errors; _i < _a.length; _i++) {
                                        err = _a[_i];
                                        RS.Log.error(err, unit.module.name);
                                    }
                                    throw new Error("Game tests failed to build.");
                                }
                                RS.Log.info("Compiled in " + (result.time / 1000).toFixed(2) + " s", "" + unit.name);
                                return [2 /*return*/];
                        }
                    });
                });
            }
            Testing.executeUnit = executeUnit;
            function initialiseUnit(unit) {
                return __awaiter(this, void 0, void 0, function () {
                    var compiled;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                compiled = false;
                                return [4 /*yield*/, unit.init()];
                            case 1:
                                _a.sent();
                                if (!(unit.validityState === RS.Build.CompilationUnit.ValidState.Initialised)) return [3 /*break*/, 3];
                                if (!unit.recompileNeeded) return [3 /*break*/, 3];
                                return [4 /*yield*/, executeUnit(unit)];
                            case 2:
                                _a.sent();
                                compiled = true;
                                _a.label = 3;
                            case 3: return [2 /*return*/, compiled];
                        }
                    });
                });
            }
            Testing.initialiseUnit = initialiseUnit;
            function isUnitValid(unit) {
                return unit.validityState === RS.Build.CompilationUnit.ValidState.Initialised || unit.validityState === RS.Build.CompilationUnit.ValidState.Compiled;
            }
            Testing.isUnitValid = isUnitValid;
            var karma = require("karma");
            var colors = require("colors/safe");
            function runKarma(configPath, cliOptions) {
                if (cliOptions === void 0) { cliOptions = {}; }
                return __awaiter(this, void 0, void 0, function () {
                    var config, results;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                config = karma.config.parseConfig(configPath, cliOptions);
                                return [4 /*yield*/, runKarmaWithConfig(config)];
                            case 1:
                                results = _a.sent();
                                console.log(colors.reset(" "));
                                return [2 /*return*/, results];
                        }
                    });
                });
            }
            Testing.runKarma = runKarma;
            function runKarmaWithConfig(config) {
                return new Promise(function (resolve, reject) {
                    var testResults = null;
                    var server = new karma.Server(config, function (exitCode) {
                        if (exitCode === 0) {
                            if (testResults) {
                                resolve(testResults);
                            }
                            else {
                                reject("no test results");
                            }
                        }
                        else {
                            if (testResults) {
                                resolve(testResults);
                            }
                            else {
                                reject("got exit code " + exitCode);
                            }
                        }
                    });
                    server.on("run_complete", function (browsers, results) {
                        testResults = results;
                    });
                    server.start();
                });
            }
            Testing.runKarmaWithConfig = runKarmaWithConfig;
        })(Testing = Tasks.Testing || (Tasks.Testing = {}));
    })(Tasks = RS.Tasks || (RS.Tasks = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var AST;
        (function (AST) {
            function visit(ast, visitor) {
                visitProgram(ast.program, visitor);
            }
            AST.visit = visit;
            function isStatement(value) {
                return RS.Is.object(value) && RS.Is.string(value.type) && value.type.indexOf("Statement") !== -1;
            }
            function isExpression(value) {
                return RS.Is.object(value) && RS.Is.string(value.type) && value.type.indexOf("Expression") !== -1;
            }
            function visitProgram(program, visitor) {
                if (visitor(program)) {
                    return;
                }
                for (var _i = 0, _a = program.body; _i < _a.length; _i++) {
                    var childNode = _a[_i];
                    if (isStatement(childNode)) {
                        visitStatement(childNode, visitor);
                    }
                }
            }
            function visitStatement(statement, visitor) {
                if (visitor(statement)) {
                    return;
                }
                switch (statement.type) {
                    case "BlockStatement":
                        for (var _i = 0, _a = statement.body; _i < _a.length; _i++) {
                            var inner = _a[_i];
                            visitStatement(inner, visitor);
                        }
                        break;
                    case "ExpressionStatement":
                        visitExpression(statement.expression, visitor);
                        break;
                    case "IfStatement":
                        visitExpression(statement.test, visitor);
                        visitStatement(statement.consequent, visitor);
                        if (statement.consequent) {
                            visitStatement(statement.consequent, visitor);
                        }
                        break;
                    case "LabeledStatement":
                        visitStatement(statement.body, visitor);
                        break;
                    case "WithStatement":
                        visitExpression(statement.object, visitor);
                        visitStatement(statement.body, visitor);
                        break;
                    case "SwitchStatement":
                        visitExpression(statement.discriminant, visitor);
                        for (var _b = 0, _c = statement.cases; _b < _c.length; _b++) {
                            var subCase = _c[_b];
                            if (visitor(subCase)) {
                                continue;
                            }
                            if (subCase.test) {
                                visitExpression(subCase.test, visitor);
                            }
                            for (var _d = 0, _e = subCase.consequent; _d < _e.length; _d++) {
                                var subStatement = _e[_d];
                                visitStatement(subStatement, visitor);
                            }
                        }
                        break;
                    case "ReturnStatement":
                        if (statement.argument) {
                            visitExpression(statement.argument, visitor);
                        }
                        break;
                    case "ThrowStatement":
                        visitExpression(statement.argument, visitor);
                        break;
                    case "TryStatement":
                        visitStatement(statement.block, visitor);
                        if (statement.handler) {
                            visitStatement(statement.handler.body, visitor);
                        }
                        if (statement.finalizer) {
                            visitStatement(statement.finalizer, visitor);
                        }
                        break;
                    case "WhileStatement":
                        visitExpression(statement.test, visitor);
                        visitStatement(statement.body, visitor);
                        break;
                    case "DoWhileStatement":
                        visitStatement(statement.body, visitor);
                        visitExpression(statement.test, visitor);
                        break;
                    case "ForStatement":
                        if (statement.init) {
                            if (isExpression(statement.init)) {
                                visitExpression(statement.init, visitor);
                            }
                            else {
                                visitVariableDeclr(statement.init, visitor);
                            }
                        }
                        if (statement.test) {
                            visitExpression(statement.test, visitor);
                        }
                        if (statement.update) {
                            visitExpression(statement.update, visitor);
                        }
                        visitStatement(statement.body, visitor);
                        break;
                    case "ForInStatement":
                    case "ForOfStatement":
                        if (statement.left.type === "VariableDeclaration") {
                            visitVariableDeclr(statement.left, visitor);
                        }
                        visitExpression(statement.right, visitor);
                        visitStatement(statement.body, visitor);
                        break;
                    case "FunctionDeclaration":
                        visitor(statement.id);
                        visitStatement(statement.body, visitor);
                        break;
                    case "VariableDeclaration":
                        visitVariableDeclr(statement, visitor, true);
                        break;
                    case "ClassDeclaration":
                        visitor(statement.id);
                        if (statement.superClass) {
                            visitExpression(statement.superClass, visitor);
                        }
                        visitor(statement.body);
                        for (var _f = 0, _g = statement.body.body; _f < _g.length; _f++) {
                            var method = _g[_f];
                            if (visitor(method)) {
                                continue;
                            }
                            visitExpression(method.key, visitor);
                            visitExpression(method.value, visitor);
                        }
                        break;
                }
            }
            function visitExpression(expr, visitor) {
                if (visitor(expr)) {
                    return;
                }
                switch (expr.type) {
                    case "ArrayExpression":
                        for (var _i = 0, _a = expr.elements; _i < _a.length; _i++) {
                            var subElement = _a[_i];
                            if (subElement) {
                                if (isExpression(subElement)) {
                                    visitExpression(subElement, visitor);
                                }
                                else if (subElement.argument) {
                                    visitExpression(subElement.argument, visitor);
                                }
                            }
                        }
                        break;
                    case "ObjectExpression":
                        for (var _b = 0, _c = expr.properties; _b < _c.length; _b++) {
                            var property = _c[_b];
                            visitExpression(property.key, visitor);
                            if (isExpression(property.value)) {
                                visitExpression(property.value, visitor);
                            }
                        }
                        break;
                    case "FunctionExpression":
                        visitStatement(expr.body, visitor);
                        break;
                    case "SequenceExpression":
                        for (var _d = 0, _e = expr.expressions; _d < _e.length; _d++) {
                            var subExpr = _e[_d];
                            visitExpression(subExpr, visitor);
                        }
                        break;
                    case "UnaryExpression":
                        visitExpression(expr.argument, visitor);
                        break;
                    case "BinaryExpression":
                        visitExpression(expr.left, visitor);
                        visitExpression(expr.right, visitor);
                        break;
                    case "AssignmentExpression":
                        if (isExpression(expr.left)) {
                            visitExpression(expr.left, visitor);
                        }
                        visitExpression(expr.right, visitor);
                        break;
                    case "UpdateExpression":
                        visitExpression(expr.argument, visitor);
                        break;
                    case "LogicalExpression":
                        visitExpression(expr.left, visitor);
                        visitExpression(expr.right, visitor);
                        break;
                    case "ConditionalExpression":
                        visitExpression(expr.test, visitor);
                        visitExpression(expr.alternate, visitor);
                        visitExpression(expr.consequent, visitor);
                        break;
                    case "CallExpression":
                    case "NewExpression":
                        if (isExpression(expr.callee)) {
                            visitExpression(expr.callee, visitor);
                        }
                        for (var _f = 0, _g = expr.arguments; _f < _g.length; _f++) {
                            var arg = _g[_f];
                            if (isExpression(arg)) {
                                visitExpression(arg, visitor);
                            }
                        }
                        break;
                    case "MemberExpression":
                        if (isExpression(expr.object)) {
                            visitExpression(expr.object, visitor);
                        }
                        visitExpression(expr.property, visitor);
                        break;
                    case "ArrowFunctionExpression":
                        if (isExpression(expr.body)) {
                            visitExpression(expr.body, visitor);
                        }
                        else {
                            visitStatement(expr.body, visitor);
                        }
                        break;
                }
            }
            function visitVariableDeclr(declr, visitor, noOuterVisit) {
                if (noOuterVisit === void 0) { noOuterVisit = false; }
                if (!noOuterVisit) {
                    if (visitor(declr)) {
                        return;
                    }
                }
                if (declr.declarations) {
                    for (var _i = 0, _a = declr.declarations; _i < _a.length; _i++) {
                        var childDeclr = _a[_i];
                        visitor(childDeclr);
                        visitor(childDeclr.id);
                        if (childDeclr.init) {
                            visitExpression(childDeclr.init, visitor);
                        }
                    }
                }
            }
            function expressionsEqual(a, b) {
                if (a.type === "SpreadElement" || b.type === "SpreadElement") {
                    return a.type === "SpreadElement" && b.type === "SpreadElement" && expressionsEqual(a.argument, b.argument);
                }
                if (a.type !== b.type) {
                    return false;
                }
                switch (a.type) {
                    case "ThisExpression":
                        return true;
                    case "ArrayExpression":
                        {
                            var other_1 = b;
                            return a.elements.length === other_1.elements.length && a.elements.every(function (e, i) { return expressionsEqual(e, other_1.elements[i]); });
                        }
                    case "ObjectExpression":
                        {
                            var other_2 = b;
                            return a.properties.length === other_2.properties.length && a.properties.every(function (p, i) { return propertiesEqual(p, other_2.properties[i]); });
                        }
                    case "Identifier":
                        {
                            var other = b;
                            return a.name === other.name;
                        }
                    case "MemberExpression":
                        {
                            var other = b;
                            if (a.object.type === "Super" || other.object.type === "Super") {
                                return a.object.type === other.object.type;
                            }
                            return expressionsEqual(a.object, other.object) && expressionsEqual(a.property, other.property);
                        }
                    case "UnaryExpression":
                        {
                            var other = b;
                            return a.operator === other.operator && expressionsEqual(a.argument, other.argument);
                        }
                    case "BinaryExpression":
                        {
                            var other = b;
                            return a.operator === other.operator && expressionsEqual(a.left, other.left) && expressionsEqual(a.right, other.right);
                        }
                    case "LogicalExpression":
                        {
                            var other = b;
                            return a.operator === other.operator && expressionsEqual(a.left, other.left) && expressionsEqual(a.right, other.right);
                        }
                    case "Literal":
                        {
                            var other = b;
                            return a.value === other.value && a.raw === other.raw;
                        }
                    default:
                        RS.Log.warn("expressionsEqual: type '" + a.type + "' not yet supported");
                        // Not yet supported
                        return false;
                }
            }
            AST.expressionsEqual = expressionsEqual;
            function propertiesEqual(a, b) {
                if (a.computed !== b.computed) {
                    return false;
                }
                if (a.kind !== b.kind) {
                    return false;
                }
                if (!expressionsEqual(a.key, b.key)) {
                    return false;
                }
                if (isExpression(a.value)) {
                    return isExpression(b.value) && expressionsEqual(a.value, b.value);
                }
                return false; // non-expressions not yet supported
            }
            AST.propertiesEqual = propertiesEqual;
        })(AST = Build.AST || (Build.AST = {}));
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    function assign(from, to, deep) {
        for (var key in from) {
            var val = from[key];
            if (deep && RS.Is.nonPrimitive(val) && to[key] != null) {
                assign(val, to[key], true);
            }
            else {
                to[key] = val;
            }
        }
        return to;
    }
    RS.assign = assign;
})(RS || (RS = {}));
/// <reference path="Command.ts" />
/** Contains utility functions for the bower package system. */
var RS;
(function (RS) {
    var Bower;
    (function (Bower) {
        /**
         * Installs a bower package locally.
         * @param name
         * @param version
         */
        function installPackage(name, version) {
            return __awaiter(this, void 0, void 0, function () {
                var formattedName, logOutput, err_14;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.Command.exists("bower")];
                        case 1:
                            // Check that the command exists
                            if (!(_a.sent())) {
                                throw new Error("Could not find bower - check that it's installed! (npm install -g bower)");
                            }
                            RS.Log.info("Installing bower package '" + name + "' (" + version + ")...");
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            formattedName = /:\/\//g.test(version) ? version : name + "#" + version;
                            return [4 /*yield*/, RS.Command.run("bower", ["--allow-root", "install", formattedName, "--force"], undefined, false)];
                        case 3:
                            logOutput = _a.sent();
                            return [3 /*break*/, 5];
                        case 4:
                            err_14 = _a.sent();
                            if (/not found/g.test(err_14.toString())) {
                                RS.Log.error("Package '" + name + "' not found", "Bower");
                            }
                            else {
                                RS.Log.error(err_14);
                            }
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        }
        Bower.installPackage = installPackage;
    })(Bower = RS.Bower || (RS.Bower = {}));
})(RS || (RS = {}));
/// <reference path="fileio/FileSystem.ts" />
/// <reference path="Profile.ts" />
/** Contains utility functions for calculating checksums. */
var RS;
(function (RS) {
    var Checksum;
    (function (Checksum) {
        var crypto = require("crypto");
        /**
         * Gets a checksum for the contents of an object.
         * @param data
         */
        function getSimple(data) {
            RS.Profile.enter("checksum");
            var hash = crypto.createHash("md5");
            hash.update((RS.Is.string(data) ? data : RS.JSON.stringify(data)) || "", "utf8");
            var result = hash.digest("hex");
            RS.Profile.exit("checksum");
            return result;
        }
        Checksum.getSimple = getSimple;
        /**
         * Gets a checksum for the file at the specified path. Async.
         */
        function get(path) {
            return __awaiter(this, void 0, void 0, function () {
                var hash, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            RS.Profile.enter("checksum");
                            hash = crypto.createHash("md5");
                            return [4 /*yield*/, RS.FileSystem.streamFile(path, hash.update.bind(hash))];
                        case 1:
                            _a.sent();
                            result = hash.digest("hex");
                            RS.Profile.exit("checksum");
                            return [2 /*return*/, result];
                    }
                });
            });
        }
        Checksum.get = get;
        function getComposite(paths, extraData) {
            return __awaiter(this, void 0, void 0, function () {
                var hash, i, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            RS.Profile.enter("checksum");
                            paths.sort();
                            hash = crypto.createHash("md5");
                            i = 0;
                            _a.label = 1;
                        case 1:
                            if (!(i < paths.length)) return [3 /*break*/, 4];
                            return [4 /*yield*/, RS.FileSystem.streamFile(paths[i], hash.update.bind(hash))];
                        case 2:
                            _a.sent();
                            _a.label = 3;
                        case 3:
                            i++;
                            return [3 /*break*/, 1];
                        case 4:
                            if (extraData) {
                                hash.update(RS.JSON.stringify(extraData), "utf8");
                            }
                            result = hash.digest("hex");
                            RS.Profile.exit("checksum");
                            return [2 /*return*/, result];
                    }
                });
            });
        }
        Checksum.getComposite = getComposite;
        /**
         * Gets a base64 encoded SHA256 hash of the specified string.
         * @param content
         */
        function sha256(str) {
            RS.Profile.enter("checksum");
            var hash = crypto.createHash("sha256");
            hash.update(str, "utf8");
            var result = hash.digest("base64");
            RS.Profile.exit("checksum");
            return result;
        }
        Checksum.sha256 = sha256;
        /**
         * Encapsulates a database of checksums.
         */
        var DB = /** @class */ (function () {
            function DB() {
                this._db = {};
                this._dirty = false;
            }
            Object.defineProperty(DB.prototype, "dirty", {
                /** Gets if this DB is dirty (has been changed but not saved). */
                get: function () { return this._dirty; },
                enumerable: true,
                configurable: true
            });
            /**
             * Clears the database.
             */
            DB.prototype.clear = function () {
                this._db = {};
            };
            /**
             * Loads the database from the specified path. Fails silently if the file doesn't exist. Async.
             * @param path
             */
            DB.prototype.loadFromFile = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, RS.FileSystem.fileExists(path)];
                            case 1:
                                if (!(_b.sent())) {
                                    this.clear();
                                    return [2 /*return*/];
                                }
                                _a = this;
                                return [4 /*yield*/, RS.JSON.parseFile(path)];
                            case 2:
                                _a._db = (_b.sent()) || {};
                                this._dirty = false;
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Writes the database to the specified path. Async.
             * @param path
             */
            DB.prototype.saveToFile = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (RS.EnvArgs.dryRun.value) {
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, RS.FileSystem.createPath(RS.Path.directoryName(path))];
                            case 1:
                                _a.sent();
                                return [4 /*yield*/, RS.FileSystem.writeFile(path, RS.JSON.stringify(this._db))];
                            case 2:
                                _a.sent();
                                this._dirty = false;
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Gets the checksum for the specified key. Returns null if not found.
             * @param key
             */
            DB.prototype.get = function (key) {
                return this._db[key] || null;
            };
            /**
             * Sets the checksum for the specified key.
             * @param key
             */
            DB.prototype.set = function (key, checksum) {
                if (checksum == null) {
                    var changed = false;
                    if (this._db[key] != null) {
                        this._dirty = true;
                        changed = true;
                    }
                    delete this._db[key];
                    return changed;
                }
                if (this._db[key] === checksum) {
                    return false;
                }
                this._db[key] = checksum;
                this._dirty = true;
                return true;
            };
            /**
             * Gets if the checksum by the specified key is DIFFERENT to that provided.
             * @param key
             * @param checksum
             */
            DB.prototype.diff = function (key, checksum) {
                return this.get(key) !== checksum;
            };
            return DB;
        }());
        Checksum.DB = DB;
    })(Checksum = RS.Checksum || (RS.Checksum = {}));
})(RS || (RS = {}));
(function (RS) {
    var EnvArgs;
    (function (EnvArgs) {
        EnvArgs.dryRun = new RS.BooleanEnvArg("DRYRUN", false);
    })(EnvArgs = RS.EnvArgs || (RS.EnvArgs = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var strict = new RS.BooleanEnvArg("STRICT", false);
        /**
         * Encapsulates a set of TypeScript source files to compile.
         */
        var CompilationUnit = /** @class */ (function () {
            function CompilationUnit() {
                this._name = null;
                this._module = null;
                this._path = null;
                this._dependencies = new RS.List();
                this._rawDependencies = new RS.List();
                this._postProcessors = new RS.List();
                this._preProcessors = new RS.List();
                this._validState = CompilationUnit.ValidState.Uninitialised;
            }
            Object.defineProperty(CompilationUnit.prototype, "defaultTSConfig", {
                /** Gets or sets the default tsconfig.json. */
                get: function () { return this._defaultTSConfig; },
                set: function (value) { this._defaultTSConfig = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "name", {
                /** Gets or sets the name of this compilation unit. */
                get: function () { return this._name; },
                set: function (value) { this._name = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "module", {
                /** Gets or sets the module to which this compilation unit belongs. */
                get: function () { return this._module; },
                set: function (value) { this._module = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "path", {
                /** Gets or sets the path to the source code of this compilation unit, relative to the cwd. */
                get: function () { return this._path; },
                set: function (value) { this._path = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "dependencies", {
                /** Gets the dependencies of this compilation unit. */
                get: function () { return this._dependencies; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "rawDependencies", {
                /** Gets the raw dependencies of this compilation unit. */
                get: function () { return this._rawDependencies; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "validityState", {
                /** Gets the validity state of this compilation unit. */
                get: function () { return this._validState; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "rootDir", {
                /** Gets the root directory of this compilation unit, relative to the cwd. */
                get: function () { return this._rootDir; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "outJSFile", {
                /** Gets the output js file of this compilation unit, relative to the cwd. */
                get: function () { return this._outFile; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "outMapFile", {
                /** Gets the output js sourcemap file of this compilation unit, relative to the cwd. */
                get: function () { return RS.Path.replaceExtension(this._outFile, ".js.map"); },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "outDTSFile", {
                /** Gets the output d.ts file of this compilation unit, relative to the cwd. */
                get: function () { return RS.Path.replaceExtension(this._outFile, ".d.ts"); },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "outDTSMapFile", {
                /** Gets the output d.ts sourcemap file of this compilation unit, relative to the cwd. */
                get: function () { return RS.Path.replaceExtension(this._outFile, ".d.ts.map"); },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "requiresHelpers", {
                /** Gets if the output js code requires the ts helpers to be externally present. */
                get: function () { return this._requiresHelpers; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "srcChecksum", {
                /** Gets the checksum for the source of this compilation unit. */
                get: function () { return this._srcChecksum; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "recompileNeeded", {
                /** Gets if compiliation is needed for this compilation unit. */
                get: function () { return this._recompileNeeded; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompilationUnit.prototype, "lintNeeded", {
                /** Gets if lint is needed for this compilation unit. */
                get: function () { return this._lintNeeded; },
                enumerable: true,
                configurable: true
            });
            /**
             * Adds a post processor to this compilation unit.
             * @param postProcessor
             */
            CompilationUnit.prototype.addPostProcessor = function (postProcessor) {
                this._postProcessors.add(postProcessor);
            };
            /**
             * Adds a pre-processor to this compilation unit.
             * @param preProcessor
             */
            CompilationUnit.prototype.addPreProcessor = function (preProcessor) {
                this._preProcessors.add(preProcessor);
            };
            /**
             * Initialises this compilation unit.
             */
            CompilationUnit.prototype.init = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var tsConfig, compilerOpts, _a, _b, _c;
                    return __generator(this, function (_d) {
                        switch (_d.label) {
                            case 0:
                                if (this._validState !== CompilationUnit.ValidState.Uninitialised) {
                                    RS.Log.warn("Attempted to re-initialise already-initialised compilation unit", this._name);
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, this.resolveTSConfig()];
                            case 1:
                                tsConfig = _d.sent();
                                if (!tsConfig) {
                                    this._validState = CompilationUnit.ValidState.NoTSConfig;
                                    return [2 /*return*/];
                                }
                                compilerOpts = tsConfig.compilerOptions;
                                this._rootDir = RS.Path.normalise(RS.Path.combine(this._path, compilerOpts && compilerOpts.rootDir || "."));
                                this._outFile = RS.Path.normalise(RS.Path.combine(this._path, compilerOpts && compilerOpts.outFile || "../build/src.js"));
                                this._emitsSourceMap = compilerOpts && compilerOpts.sourceMap || false;
                                this._emitsDefinitions = compilerOpts && compilerOpts.declaration || false;
                                this._emitsDefinitionMap = compilerOpts && compilerOpts.declarationMap || false;
                                this._requiresHelpers = compilerOpts && compilerOpts.noEmitHelpers || false;
                                return [4 /*yield*/, this.resolveDependencies()];
                            case 2:
                                // Resolve dependencies
                                if (!(_d.sent())) {
                                    this._validState = CompilationUnit.ValidState.MissingDependency;
                                    return [2 /*return*/];
                                }
                                // Compile checksum
                                _a = this;
                                _c = (_b = RS.Checksum).getComposite;
                                return [4 /*yield*/, RS.FileSystem.readFiles(this._rootDir)];
                            case 3: return [4 /*yield*/, _c.apply(_b, [_d.sent()])];
                            case 4:
                                // Compile checksum
                                _a._srcChecksum = _d.sent();
                                // Done
                                this._validState = CompilationUnit.ValidState.Initialised;
                                return [4 /*yield*/, RS.FileSystem.fileExists(this._outFile)];
                            case 5:
                                // Check if we need to recompile
                                if (!(_d.sent())) {
                                    this._recompileNeeded = true;
                                }
                                else if (this._module.checksumDB.diff(this._name + ".ts", this._srcChecksum)) {
                                    this._recompileNeeded = true;
                                }
                                else {
                                    this._recompileNeeded = false;
                                }
                                // Check if we need to relint
                                if (this._recompileNeeded) {
                                    // If we need to recompile, linting should happen as part of that.
                                    this._lintNeeded = false;
                                }
                                else if (this._module.checksumDB.diff(this._name + ".ts.lint", this._srcChecksum)) {
                                    this._lintNeeded = true;
                                }
                                else {
                                    this._lintNeeded = false;
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Validates this compilation unit's code.
             */
            CompilationUnit.prototype.lintFiles = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var lintConfigPath, defaultConfigPath, useCustom, success, srcOb, tmpConfigPath;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                lintConfigPath = RS.Path.combine(this._rootDir, "tslint.json");
                                defaultConfigPath = RS.Path.combine(__dirname, "linting", strict.value ? "tslint-strict.json" : "tslint.json");
                                return [4 /*yield*/, RS.FileSystem.fileExists(lintConfigPath)];
                            case 1:
                                useCustom = _a.sent();
                                if (!useCustom) return [3 /*break*/, 6];
                                return [4 /*yield*/, RS.JSON.parseFile(lintConfigPath)];
                            case 2:
                                srcOb = _a.sent();
                                srcOb.extends = [defaultConfigPath];
                                tmpConfigPath = RS.Path.combine(this._rootDir, "tslint_tmp.json");
                                return [4 /*yield*/, RS.FileSystem.writeFile(tmpConfigPath, RS.JSON.stringify(srcOb))];
                            case 3:
                                _a.sent();
                                return [4 /*yield*/, this.lintPath(tmpConfigPath)];
                            case 4:
                                success = _a.sent();
                                return [4 /*yield*/, RS.FileSystem.deletePath(tmpConfigPath)];
                            case 5:
                                _a.sent();
                                return [3 /*break*/, 8];
                            case 6: return [4 /*yield*/, this.lintPath(defaultConfigPath)];
                            case 7:
                                success = _a.sent();
                                _a.label = 8;
                            case 8:
                                if (success) {
                                    // Set lint checksum
                                    this._module.checksumDB.set(this._name + ".ts.lint", this._srcChecksum);
                                }
                                return [2 /*return*/, success];
                        }
                    });
                });
            };
            /**
             * Executes this compilation unit.
             */
            CompilationUnit.prototype.execute = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var result, timer, pathParts, tmpDirPath, workDone, ppChangesMade, compileArtifact, _a, _b, _c, _d, _i, _e, pp, newCompileArtifact, err_15, lines, tmp, _f, lines_1, line;
                    return __generator(this, function (_g) {
                        switch (_g.label) {
                            case 0:
                                result = { success: false, time: 0, errors: null };
                                timer = new RS.Timer();
                                _g.label = 1;
                            case 1:
                                _g.trys.push([1, 33, 34, 35]);
                                if (!(this._preProcessors.length > 0)) return [3 /*break*/, 12];
                                pathParts = RS.Path.split(this._path);
                                tmpDirPath = RS.Path.combine.apply(RS.Path, __spreadArrays(pathParts.slice(0, -1), ["." + pathParts[pathParts.length - 1] + "-tmp"]));
                                return [4 /*yield*/, RS.FileSystem.deletePath(tmpDirPath)];
                            case 2:
                                _g.sent();
                                _g.label = 3;
                            case 3:
                                _g.trys.push([3, , 9, 11]);
                                // cache to temp dir
                                return [4 /*yield*/, this.copyFolder(this._path, tmpDirPath)];
                            case 4:
                                // cache to temp dir
                                _g.sent();
                                return [4 /*yield*/, this.preProcess(this._path)];
                            case 5:
                                workDone = _g.sent();
                                return [4 /*yield*/, RS.TypeScript.compile(this._path, false)];
                            case 6:
                                _g.sent();
                                if (!workDone) return [3 /*break*/, 8];
                                // restore original cached files
                                return [4 /*yield*/, this.copyFolder(tmpDirPath, this._path)];
                            case 7:
                                // restore original cached files
                                _g.sent();
                                _g.label = 8;
                            case 8: return [3 /*break*/, 11];
                            case 9: return [4 /*yield*/, RS.FileSystem.deletePath(tmpDirPath)];
                            case 10:
                                _g.sent();
                                return [7 /*endfinally*/];
                            case 11: return [3 /*break*/, 14];
                            case 12: return [4 /*yield*/, RS.TypeScript.compile(this._rootDir, false)];
                            case 13:
                                _g.sent();
                                _g.label = 14;
                            case 14: return [4 /*yield*/, this.lintFiles()];
                            case 15:
                                _g.sent();
                                // Update file cache
                                RS.FileSystem.cache.set(this.outJSFile, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                                if (this._emitsSourceMap) {
                                    RS.FileSystem.cache.set(this.outMapFile, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                                }
                                if (this._emitsDefinitions) {
                                    RS.FileSystem.cache.set(this.outDTSFile, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                                }
                                if (this._emitsDefinitionMap) {
                                    RS.FileSystem.cache.set(this.outDTSMapFile, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                                }
                                ppChangesMade = false;
                                _a = {};
                                return [4 /*yield*/, RS.FileSystem.readFile(this.outJSFile)];
                            case 16:
                                _a.source = _g.sent();
                                if (!this._emitsDefinitions) return [3 /*break*/, 18];
                                return [4 /*yield*/, RS.FileSystem.readFile(this.outDTSFile)];
                            case 17:
                                _b = _g.sent();
                                return [3 /*break*/, 19];
                            case 18:
                                _b = null;
                                _g.label = 19;
                            case 19:
                                _a.definitions = _b;
                                if (!this._emitsSourceMap) return [3 /*break*/, 21];
                                return [4 /*yield*/, RS.JSON.parseFile(this.outMapFile)];
                            case 20:
                                _c = _g.sent();
                                return [3 /*break*/, 22];
                            case 21:
                                _c = null;
                                _g.label = 22;
                            case 22:
                                _a.sourceMap = _c;
                                if (!this._emitsDefinitionMap) return [3 /*break*/, 24];
                                return [4 /*yield*/, RS.JSON.parseFile(this.outDTSMapFile)];
                            case 23:
                                _d = _g.sent();
                                return [3 /*break*/, 25];
                            case 24:
                                _d = null;
                                _g.label = 25;
                            case 25:
                                compileArtifact = (_a.definitionsSourceMap = _d,
                                    _a);
                                for (_i = 0, _e = this._postProcessors.data; _i < _e.length; _i++) {
                                    pp = _e[_i];
                                    newCompileArtifact = pp.call(this, compileArtifact);
                                    if (newCompileArtifact !== compileArtifact) {
                                        ppChangesMade = true;
                                        compileArtifact = newCompileArtifact;
                                    }
                                }
                                if (!ppChangesMade) return [3 /*break*/, 32];
                                return [4 /*yield*/, RS.FileSystem.writeFile(this.outJSFile, compileArtifact.source)];
                            case 26:
                                _g.sent();
                                if (!(compileArtifact.sourceMap != null)) return [3 /*break*/, 28];
                                return [4 /*yield*/, RS.FileSystem.writeFile(this.outMapFile, RS.JSON.stringify(compileArtifact.sourceMap))];
                            case 27:
                                _g.sent();
                                _g.label = 28;
                            case 28:
                                if (!(compileArtifact.definitions != null)) return [3 /*break*/, 30];
                                return [4 /*yield*/, RS.FileSystem.writeFile(this.outDTSFile, compileArtifact.definitions)];
                            case 29:
                                _g.sent();
                                _g.label = 30;
                            case 30:
                                if (!(compileArtifact.definitionsSourceMap != null)) return [3 /*break*/, 32];
                                return [4 /*yield*/, RS.FileSystem.writeFile(this.outDTSMapFile, RS.JSON.stringify(compileArtifact.definitionsSourceMap))];
                            case 31:
                                _g.sent();
                                _g.label = 32;
                            case 32:
                                // Success
                                result.success = true;
                                this._module.checksumDB.set(this._name + ".ts", this._srcChecksum);
                                this._validState = CompilationUnit.ValidState.Compiled;
                                return [3 /*break*/, 35];
                            case 33:
                                err_15 = _g.sent();
                                result.errors = [];
                                if (RS.Is.string(err_15)) {
                                    lines = err_15.split("\n");
                                    tmp = [];
                                    for (_f = 0, lines_1 = lines; _f < lines_1.length; _f++) {
                                        line = lines_1[_f];
                                        if (line[0] !== " ") {
                                            if (tmp.length > 0) {
                                                result.errors.push(tmp.join("\n"));
                                                tmp.length = 0;
                                            }
                                        }
                                        tmp.push(line);
                                    }
                                    if (tmp.length > 0) {
                                        result.errors.push(tmp.join("\n"));
                                        tmp.length = 0;
                                    }
                                }
                                else {
                                    result.errors.push(err_15.stack || err_15);
                                }
                                this._validState = CompilationUnit.ValidState.CompileError;
                                return [3 /*break*/, 35];
                            case 34:
                                result.time = timer.elapsed;
                                return [7 /*endfinally*/];
                            case 35: return [2 /*return*/, result];
                        }
                    });
                });
            };
            CompilationUnit.prototype.copyFolder = function (sourcePath, destPath) {
                return __awaiter(this, void 0, void 0, function () {
                    var srcPaths, promises;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, RS.FileSystem.readFiles(sourcePath)];
                            case 1:
                                srcPaths = _a.sent();
                                promises = srcPaths.map(function (path) { return __awaiter(_this, void 0, void 0, function () {
                                    var relPath, targetPath;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                relPath = RS.Path.relative(sourcePath, path);
                                                targetPath = RS.Path.combine(destPath, relPath);
                                                return [4 /*yield*/, RS.FileSystem.deletePath(targetPath)];
                                            case 1:
                                                _a.sent();
                                                return [4 /*yield*/, RS.FileSystem.createPath(RS.Path.directoryName(targetPath))];
                                            case 2:
                                                _a.sent();
                                                return [4 /*yield*/, RS.FileSystem.copyFile(path, targetPath)];
                                            case 3:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                                return [4 /*yield*/, Promise.all(promises)];
                            case 2:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            CompilationUnit.prototype.preProcess = function (outPath) {
                return __awaiter(this, void 0, void 0, function () {
                    var srcPaths, workDone, promises;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, RS.FileSystem.readFiles(outPath)];
                            case 1:
                                srcPaths = _a.sent();
                                workDone = false;
                                promises = srcPaths.map(function (path) { return __awaiter(_this, void 0, void 0, function () {
                                    var srcIn, result;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                if (!(RS.Path.extension(path) === ".ts")) return [3 /*break*/, 3];
                                                return [4 /*yield*/, RS.FileSystem.readFile(path)];
                                            case 1:
                                                srcIn = _a.sent();
                                                result = this._preProcessors.reduce(function (src, proc) { return proc.call(_this, src); }, srcIn);
                                                if (result.workDone) {
                                                    workDone = true;
                                                }
                                                // write to original location (overwrites original file)
                                                return [4 /*yield*/, RS.FileSystem.writeFile(path, result.output)];
                                            case 2:
                                                // write to original location (overwrites original file)
                                                _a.sent();
                                                _a.label = 3;
                                            case 3: return [2 /*return*/];
                                        }
                                    });
                                }); });
                                return [4 /*yield*/, Promise.all(promises)];
                            case 2:
                                _a.sent();
                                return [2 /*return*/, workDone];
                        }
                    });
                });
            };
            CompilationUnit.prototype.lintPath = function (path) {
                return __awaiter(this, void 0, void 0, function () {
                    var err_16, lines, _i, lines_2, line;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                return [4 /*yield*/, RS.TSLint.lint(this._rootDir, path, false)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/, true];
                            case 2:
                                err_16 = _a.sent();
                                if (!strict.value && err_16 instanceof RS.TSLint.LintFailure && !err_16.isError) {
                                    lines = ("" + err_16).split("\n");
                                    for (_i = 0, lines_2 = lines; _i < lines_2.length; _i++) {
                                        line = lines_2[_i];
                                        RS.Log.warn(line, this._name);
                                    }
                                    return [2 /*return*/, false];
                                }
                                throw err_16;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            CompilationUnit.prototype.resolveTSConfig = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var tsConfigPath, srcFiles, tsConfigRaw;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                tsConfigPath = RS.Path.combine(this._path, "tsconfig.json");
                                return [4 /*yield*/, RS.FileSystem.fileExists(tsConfigPath)];
                            case 1:
                                if (!!(_a.sent())) return [3 /*break*/, 5];
                                if (!this._defaultTSConfig) return [3 /*break*/, 4];
                                return [4 /*yield*/, RS.FileSystem.readFiles(this._path)];
                            case 2:
                                srcFiles = _a.sent();
                                if (!srcFiles.some(function (file) { return RS.Path.extension(file) === ".ts"; })) return [3 /*break*/, 4];
                                tsConfigRaw = RS.JSON.stringify(this._defaultTSConfig, null, 2);
                                return [4 /*yield*/, RS.FileSystem.writeFile(tsConfigPath, tsConfigRaw)];
                            case 3:
                                _a.sent();
                                return [2 /*return*/, this._defaultTSConfig];
                            case 4: return [2 /*return*/, null];
                            case 5: return [4 /*yield*/, RS.JSON.parseFile(tsConfigPath)];
                            case 6: return [2 /*return*/, _a.sent()];
                        }
                    });
                });
            };
            CompilationUnit.prototype.resolveDependencies = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var depMap, depSrcPath, existingDepSrcFiles, fileWriteList, fileCopyList, _i, _a, dep, depChecksumKey, depChecksum, depDidChange, definitionsFile, definitionsMapFile, _b, err_17, err_18, defs, _c, defs_1, def, relDef, outPath, existingIdx, definitionsMapObj, rootPath, i, l, depName, outputPath, existingIdx, _d, depMapName, mapOutputPath, mapExistingIdx, _e, definitionsMapObj, localPath, rootPath, i, l, copyKeys, writeKeys, copySrcs, copyDests, paths, files, i, i, _f, existingDepSrcFiles_1, existing, path;
                    return __generator(this, function (_g) {
                        switch (_g.label) {
                            case 0:
                                depMap = {};
                                this._rawDependencies.clear();
                                depSrcPath = RS.Path.combine(this._rootDir, "dependencies");
                                return [4 /*yield*/, RS.FileSystem.createPath(depSrcPath)];
                            case 1:
                                _g.sent();
                                return [4 /*yield*/, RS.FileSystem.readFiles(depSrcPath)];
                            case 2:
                                existingDepSrcFiles = (_g.sent()).map(function (p) { return RS.Path.relative(depSrcPath, p); });
                                fileWriteList = {};
                                fileCopyList = {};
                                _i = 0, _a = this._dependencies.data;
                                _g.label = 3;
                            case 3:
                                if (!(_i < _a.length)) return [3 /*break*/, 36];
                                dep = _a[_i];
                                if (depMap[dep.name]) {
                                    RS.Log.warn("Duplicate dependency name '" + dep.name + "' in compilation unit", this._name);
                                }
                                else {
                                    depMap[dep.name] = true;
                                }
                                depChecksumKey = this._name + ".deps." + dep.name;
                                depChecksum = CompilationUnit.Dependency.getChecksum(dep);
                                depDidChange = this._module.checksumDB.diff(depChecksumKey, depChecksum);
                                definitionsFile = null;
                                definitionsMapFile = null;
                                _b = dep.kind;
                                switch (_b) {
                                    case CompilationUnit.DependencyKind.ThirdParty: return [3 /*break*/, 4];
                                    case CompilationUnit.DependencyKind.CompilationUnit: return [3 /*break*/, 5];
                                    case CompilationUnit.DependencyKind.NodeModule: return [3 /*break*/, 6];
                                    case CompilationUnit.DependencyKind.BowerPackage: return [3 /*break*/, 11];
                                    case CompilationUnit.DependencyKind.DefinitionsFolder: return [3 /*break*/, 16];
                                }
                                return [3 /*break*/, 23];
                            case 4:
                                {
                                    if (dep.definitionsFile != null) {
                                        definitionsFile = dep.definitionsFile;
                                    }
                                    if (dep.definitionsMapFile != null) {
                                        definitionsMapFile = dep.definitionsMapFile;
                                    }
                                    return [3 /*break*/, 23];
                                }
                                _g.label = 5;
                            case 5:
                                {
                                    if (dep.compilationUnit === this) {
                                        RS.Log.warn("Dependency '" + dep.name + "' is me", this._name);
                                        return [2 /*return*/, false];
                                    }
                                    if (dep.compilationUnit.validityState === CompilationUnit.ValidState.NoTSConfig) {
                                        // Doing nothing is the same as resolving this compilation unit, because there is nothing to resolve.
                                        RS.Log.warn("Dependency '" + dep.name + "' is missing tsconfig.json", this._name);
                                        return [3 /*break*/, 23];
                                    }
                                    if (dep.compilationUnit.validityState === CompilationUnit.ValidState.Initialised) {
                                        if (dep.compilationUnit.recompileNeeded) {
                                            RS.Log.warn("Dependency '" + dep.name + "' needs to be recompiled before this compilation unit can be initialised", this._name);
                                            return [2 /*return*/, false];
                                        }
                                    }
                                    else if (dep.compilationUnit.validityState !== CompilationUnit.ValidState.Compiled) {
                                        RS.Log.warn("Dependency '" + dep.name + "' needs to be compiled with no errors before this compilation unit can be initialised", this._name);
                                        return [2 /*return*/, false];
                                    }
                                    if (dep.compilationUnit._emitsDefinitions) {
                                        definitionsFile = dep.compilationUnit.outDTSFile;
                                    }
                                    if (dep.compilationUnit._emitsDefinitionMap) {
                                        definitionsMapFile = dep.compilationUnit.outDTSMapFile;
                                    }
                                    return [3 /*break*/, 23];
                                }
                                _g.label = 6;
                            case 6:
                                if (!depDidChange) return [3 /*break*/, 10];
                                _g.label = 7;
                            case 7:
                                _g.trys.push([7, 9, , 10]);
                                return [4 /*yield*/, RS.NPM.installPackage(dep.moduleName, dep.moduleVersion || "latest")];
                            case 8:
                                _g.sent();
                                return [3 /*break*/, 10];
                            case 9:
                                err_17 = _g.sent();
                                RS.Log.error(err_17, this._name);
                                return [2 /*return*/, false];
                            case 10: 
                            // TODO: Fetch TS definitions from the node module
                            return [3 /*break*/, 23];
                            case 11:
                                if (!depDidChange) return [3 /*break*/, 15];
                                _g.label = 12;
                            case 12:
                                _g.trys.push([12, 14, , 15]);
                                return [4 /*yield*/, RS.Bower.installPackage(dep.packageName, dep.packageVersion || "latest")];
                            case 13:
                                _g.sent();
                                return [3 /*break*/, 15];
                            case 14:
                                err_18 = _g.sent();
                                RS.Log.error(err_18, this._name);
                                return [2 /*return*/, false];
                            case 15: 
                            // TODO: Fetch TS definitions from the bower package
                            return [3 /*break*/, 23];
                            case 16: return [4 /*yield*/, RS.FileSystem.readFiles(dep.path)];
                            case 17:
                                defs = _g.sent();
                                _c = 0, defs_1 = defs;
                                _g.label = 18;
                            case 18:
                                if (!(_c < defs_1.length)) return [3 /*break*/, 22];
                                def = defs_1[_c];
                                relDef = RS.Path.relative(dep.path, def);
                                outPath = RS.Path.combine(depSrcPath, relDef);
                                existingIdx = existingDepSrcFiles.indexOf(relDef);
                                if (existingIdx !== -1) {
                                    existingDepSrcFiles.splice(existingIdx, 1);
                                }
                                if (!(RS.Path.extension(def) === ".map")) return [3 /*break*/, 20];
                                return [4 /*yield*/, RS.JSON.parseFile(def)];
                            case 19:
                                definitionsMapObj = _g.sent();
                                rootPath = RS.Path.relative(RS.Path.directoryName(outPath), RS.Path.directoryName(def));
                                for (i = 0, l = definitionsMapObj.sources.length; i < l; ++i) {
                                    definitionsMapObj.sources[i] = RS.Path.normalise(RS.Path.combine(rootPath, definitionsMapObj.sources[i]));
                                }
                                fileWriteList[outPath] = RS.JSON.stringify(definitionsMapObj);
                                return [3 /*break*/, 21];
                            case 20:
                                fileCopyList[outPath] = def;
                                _g.label = 21;
                            case 21:
                                _c++;
                                return [3 /*break*/, 18];
                            case 22: return [3 /*break*/, 23];
                            case 23:
                                if (!(definitionsFile != null)) return [3 /*break*/, 34];
                                depName = dep.name + ".d.ts";
                                outputPath = RS.Path.combine(depSrcPath, depName);
                                existingIdx = existingDepSrcFiles.indexOf(depName);
                                if (existingIdx !== -1) {
                                    existingDepSrcFiles.splice(existingIdx, 1);
                                }
                                _d = depDidChange || !RS.HTTP.isURL(definitionsFile);
                                if (_d) return [3 /*break*/, 25];
                                return [4 /*yield*/, RS.FileSystem.fileExists(outputPath)];
                            case 24:
                                _d = !(_g.sent());
                                _g.label = 25;
                            case 25:
                                if (!_d) return [3 /*break*/, 27];
                                return [4 /*yield*/, this.acquireDefinition(definitionsFile, outputPath)];
                            case 26:
                                if (!(_g.sent())) {
                                    return [2 /*return*/, false];
                                }
                                _g.label = 27;
                            case 27:
                                if (!(definitionsMapFile != null)) return [3 /*break*/, 34];
                                depMapName = dep.name + ".d.ts.map";
                                mapOutputPath = RS.Path.combine(depSrcPath, depMapName);
                                mapExistingIdx = existingDepSrcFiles.indexOf(depMapName);
                                if (mapExistingIdx !== -1) {
                                    existingDepSrcFiles.splice(mapExistingIdx, 1);
                                }
                                _e = depDidChange || !RS.HTTP.isURL(definitionsMapFile);
                                if (_e) return [3 /*break*/, 29];
                                return [4 /*yield*/, RS.FileSystem.fileExists(mapOutputPath)];
                            case 28:
                                _e = !(_g.sent());
                                _g.label = 29;
                            case 29:
                                if (!_e) return [3 /*break*/, 34];
                                return [4 /*yield*/, this.acquireDefinition(definitionsMapFile, mapOutputPath)];
                            case 30:
                                if (!(_g.sent())) {
                                    return [2 /*return*/, false];
                                }
                                return [4 /*yield*/, RS.JSON.parseFile(mapOutputPath)];
                            case 31:
                                definitionsMapObj = _g.sent();
                                if (!!RS.HTTP.isURL(definitionsMapFile)) return [3 /*break*/, 33];
                                localPath = RS.Path.combine(this._module.path, definitionsMapFile);
                                rootPath = void 0;
                                return [4 /*yield*/, RS.FileSystem.fileExists(localPath)];
                            case 32:
                                if (_g.sent()) {
                                    rootPath = RS.Path.relative(depSrcPath, RS.Path.directoryName(localPath));
                                }
                                else {
                                    rootPath = RS.Path.relative(depSrcPath, RS.Path.directoryName(definitionsMapFile));
                                }
                                for (i = 0, l = definitionsMapObj.sources.length; i < l; ++i) {
                                    definitionsMapObj.sources[i] = RS.Path.normalise(RS.Path.combine(rootPath, definitionsMapObj.sources[i]));
                                }
                                _g.label = 33;
                            case 33:
                                // Change the file
                                definitionsMapObj.file = depName;
                                fileWriteList[mapOutputPath] = RS.JSON.stringify(definitionsMapObj);
                                _g.label = 34;
                            case 34:
                                this._module.checksumDB.set(depChecksumKey, depChecksum);
                                _g.label = 35;
                            case 35:
                                _i++;
                                return [3 /*break*/, 3];
                            case 36:
                                copyKeys = Object.keys(fileCopyList);
                                writeKeys = Object.keys(fileWriteList);
                                copySrcs = [];
                                copyDests = [];
                                paths = [];
                                files = [];
                                for (i = 0; i < copyKeys.length; i++) {
                                    copySrcs.push(fileCopyList[copyKeys[i]]);
                                    copyDests.push(copyKeys[i]);
                                }
                                for (i = 0; i < writeKeys.length; i++) {
                                    paths.push(writeKeys[i]);
                                    files.push(fileWriteList[writeKeys[i]]);
                                }
                                return [4 /*yield*/, RS.FileSystem.writeFiles(paths, files)];
                            case 37:
                                _g.sent();
                                return [4 /*yield*/, RS.FileSystem.copyFiles(copySrcs, copyDests)];
                            case 38:
                                _g.sent();
                                _f = 0, existingDepSrcFiles_1 = existingDepSrcFiles;
                                _g.label = 39;
                            case 39:
                                if (!(_f < existingDepSrcFiles_1.length)) return [3 /*break*/, 42];
                                existing = existingDepSrcFiles_1[_f];
                                path = RS.Path.combine(depSrcPath, existing);
                                RS.Log.debug("Cleaning up old file '" + path + "'");
                                return [4 /*yield*/, RS.FileSystem.deletePath(path)];
                            case 40:
                                _g.sent();
                                _g.label = 41;
                            case 41:
                                _f++;
                                return [3 /*break*/, 39];
                            case 42: return [2 /*return*/, true];
                        }
                    });
                });
            };
            /**
             * Acquires the specified definition.
             * @param name
             * @param path
             */
            CompilationUnit.prototype.acquireDefinition = function (path, output) {
                return __awaiter(this, void 0, void 0, function () {
                    var result, localPath;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!RS.HTTP.isURL(path)) return [3 /*break*/, 4];
                                return [4 /*yield*/, RS.HTTP.fetch(path)];
                            case 1:
                                result = _a.sent();
                                if (!(result.responseCode === 200)) return [3 /*break*/, 3];
                                return [4 /*yield*/, RS.FileSystem.writeFile(output, result.data)];
                            case 2:
                                _a.sent();
                                RS.Log.info("Downloaded definition from '" + path + "'", this._name);
                                return [2 /*return*/, true];
                            case 3:
                                RS.Log.error("Failed to retrieve definition from '" + path + "' (got HTTP code " + result.responseCode + ")", this._name);
                                return [2 /*return*/, false];
                            case 4:
                                localPath = RS.Path.combine(this._module.path, path);
                                return [4 /*yield*/, RS.FileSystem.fileExists(localPath)];
                            case 5:
                                if (!_a.sent()) return [3 /*break*/, 7];
                                return [4 /*yield*/, RS.FileSystem.copyFile(localPath, output)];
                            case 6:
                                _a.sent();
                                return [2 /*return*/, true];
                            case 7: return [4 /*yield*/, RS.FileSystem.fileExists(path)];
                            case 8:
                                if (!_a.sent()) return [3 /*break*/, 10];
                                return [4 /*yield*/, RS.FileSystem.copyFile(path, output)];
                            case 9:
                                _a.sent();
                                return [2 /*return*/, true];
                            case 10:
                                RS.Log.error("Failed to locate definition (unknown path '" + path + "')", this._name);
                                return [2 /*return*/, false];
                        }
                    });
                });
            };
            __decorate([
                RS.Profile("CompilationUnit.init")
            ], CompilationUnit.prototype, "init", null);
            __decorate([
                RS.Profile("CompilationUnit.execute")
            ], CompilationUnit.prototype, "execute", null);
            __decorate([
                RS.Profile("CompilationUnit.execute.preProcess")
            ], CompilationUnit.prototype, "preProcess", null);
            __decorate([
                RS.Profile("CompilationUnit.resolveDependencies")
            ], CompilationUnit.prototype, "resolveDependencies", null);
            return CompilationUnit;
        }());
        Build.CompilationUnit = CompilationUnit;
        (function (CompilationUnit) {
            function executeUnit(unit) {
                return __awaiter(this, void 0, void 0, function () {
                    var compiled, result, _i, _a, err;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                compiled = false;
                                if (!(unit.validityState === ValidState.Uninitialised)) return [3 /*break*/, 2];
                                return [4 /*yield*/, unit.init()];
                            case 1:
                                _b.sent();
                                _b.label = 2;
                            case 2:
                                if (!(unit.validityState === ValidState.Initialised)) return [3 /*break*/, 4];
                                if (!unit.recompileNeeded) return [3 /*break*/, 4];
                                return [4 /*yield*/, unit.execute()];
                            case 3:
                                result = _b.sent();
                                if (!result.success) {
                                    for (_i = 0, _a = result.errors; _i < _a.length; _i++) {
                                        err = _a[_i];
                                        RS.Log.error(err, unit.name);
                                    }
                                    throw new Error(unit.name + " failed to build.");
                                }
                                RS.Log.info("Compiled in " + (result.time / 1000).toFixed(2) + " s", "" + unit.name);
                                compiled = true;
                                _b.label = 4;
                            case 4: return [2 /*return*/, compiled];
                        }
                    });
                });
            }
            CompilationUnit.executeUnit = executeUnit;
            var DependencyKind;
            (function (DependencyKind) {
                DependencyKind[DependencyKind["ThirdParty"] = 0] = "ThirdParty";
                DependencyKind[DependencyKind["CompilationUnit"] = 1] = "CompilationUnit";
                DependencyKind[DependencyKind["NodeModule"] = 2] = "NodeModule";
                DependencyKind[DependencyKind["BowerPackage"] = 3] = "BowerPackage";
                DependencyKind[DependencyKind["DefinitionsFolder"] = 4] = "DefinitionsFolder";
            })(DependencyKind = CompilationUnit.DependencyKind || (CompilationUnit.DependencyKind = {}));
            var Dependency;
            (function (Dependency) {
                function getChecksum(dep) {
                    switch (dep.kind) {
                        case DependencyKind.CompilationUnit:
                            return RS.Checksum.getSimple("{\"kind\":" + dep.kind + ",\"compilationUnit\":\"" + dep.compilationUnit.name + "\"}");
                        default:
                            return RS.Checksum.getSimple(RS.JSON.stringify(dep));
                    }
                }
                Dependency.getChecksum = getChecksum;
            })(Dependency = CompilationUnit.Dependency || (CompilationUnit.Dependency = {}));
            var ValidState;
            (function (ValidState) {
                ValidState[ValidState["Uninitialised"] = 0] = "Uninitialised";
                ValidState[ValidState["NoTSConfig"] = 1] = "NoTSConfig";
                ValidState[ValidState["MissingDependency"] = 2] = "MissingDependency";
                ValidState[ValidState["Initialised"] = 3] = "Initialised";
                ValidState[ValidState["Compiled"] = 4] = "Compiled";
                ValidState[ValidState["CompileError"] = 5] = "CompileError";
            })(ValidState = CompilationUnit.ValidState || (CompilationUnit.ValidState = {}));
        })(CompilationUnit = Build.CompilationUnit || (Build.CompilationUnit = {}));
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
/// <reference path="AST.ts" />
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        var recast = require("recast");
        var UglifyJS = require("uglify-js");
        /**
         * A compiled code file with an optional associated source map.
         */
        var CompiledFile = /** @class */ (function () {
            function CompiledFile(source, mappings) {
                this._src = source;
                this._map = mappings;
            }
            Object.defineProperty(CompiledFile.prototype, "source", {
                /** Gets the code for this file. */
                get: function () { return this._src; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CompiledFile.prototype, "mappings", {
                /** Gets the mappings for this file. */
                get: function () { return this._map; },
                enumerable: true,
                configurable: true
            });
            CompiledFile.prototype.transform = function (fn, sourceFileName, sourceMapName) {
                var ast = recast.parse(this._src, { sourceFileName: sourceFileName });
                try {
                    fn(ast);
                }
                catch (err) {
                    RS.Log.error(err.stack);
                }
                var result = recast.print(ast, { sourceMapName: sourceMapName, inputSourceMap: this._map });
                this._src = result.code;
                this._map = result.map;
            };
            CompiledFile.prototype.minify = function (sourceFileName) {
                var result = UglifyJS.minify(this._src, {
                    sourceMap: {
                        content: this._map,
                        filename: sourceFileName,
                        url: RS.Path.replaceExtension(sourceFileName, ".js.map")
                    },
                    output: {
                        webkit: true,
                        wrap_iife: true
                    }
                });
                if (result.error) {
                    throw new Error(result.error.message + " (" + result.error.filename + ":" + result.error.line + " " + result.error.col + ")");
                }
                this._src = result.code;
                try {
                    this._map = RS.JSON.parse(result.map);
                }
                catch (err) {
                    throw new Error("Error whilst parsing source map: " + err);
                }
            };
            /**
             * Writes this compiled file to disk.
             * @param sourceFile
             * @param mapFile
             */
            CompiledFile.prototype.write = function (sourceFile, mapFile) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, RS.FileSystem.createPath(RS.Path.directoryName(sourceFile))];
                            case 1:
                                _a.sent();
                                return [4 /*yield*/, RS.FileSystem.writeFile(sourceFile, this._src)];
                            case 2:
                                _a.sent();
                                if (!(this._map && mapFile != null)) return [3 /*break*/, 4];
                                return [4 /*yield*/, RS.FileSystem.writeFile(mapFile, RS.JSON.stringify(this._map))];
                            case 3:
                                _a.sent();
                                _a.label = 4;
                            case 4: return [2 /*return*/];
                        }
                    });
                });
            };
            /**
             * Loads a compiled file from disk.
             * @param sourceFile
             * @param mapFile
             */
            CompiledFile.load = function (sourceFile, mapFile) {
                return __awaiter(this, void 0, void 0, function () {
                    var src, map, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, RS.FileSystem.readFile(sourceFile)];
                            case 1:
                                src = _b.sent();
                                if (!mapFile) return [3 /*break*/, 3];
                                return [4 /*yield*/, RS.JSON.parseFile(mapFile)];
                            case 2:
                                _a = _b.sent();
                                return [3 /*break*/, 4];
                            case 3:
                                _a = null;
                                _b.label = 4;
                            case 4:
                                map = _a;
                                return [2 /*return*/, new CompiledFile(src, map)];
                        }
                    });
                });
            };
            __decorate([
                RS.Profile("CompiledFile.transform")
            ], CompiledFile.prototype, "transform", null);
            __decorate([
                RS.Profile("CompiledFile.minify")
            ], CompiledFile.prototype, "minify", null);
            __decorate([
                RS.Profile("CompiledFile.write")
            ], CompiledFile.prototype, "write", null);
            __decorate([
                RS.Profile("CompiledFile.load")
            ], CompiledFile, "load", null);
            return CompiledFile;
        }());
        Build.CompiledFile = CompiledFile;
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Crypto;
    (function (Crypto) {
        var crypto = require("crypto");
        var algorithm = "aes-256-ctr";
        /**
         * Generates a random IV.
         */
        function generateIV() {
            return crypto.randomBytes(16);
        }
        Crypto.generateIV = generateIV;
        /**
         * Generates a random key.
         */
        function generateKey() {
            return crypto.randomBytes(32);
        }
        Crypto.generateKey = generateKey;
        /**
         * Encrypts the specified string using the specified password.
         * @param text
         * @param password
         */
        function encrypt(text, password, iv) {
            var cipher = crypto.createCipheriv(algorithm, password, iv);
            var crypted = cipher.update(text, "utf8", "hex");
            crypted += cipher.final("hex");
            return crypted;
        }
        Crypto.encrypt = encrypt;
        /**
         * Decrypts the specified string using the specified password.
         * @param text
         * @param password
         */
        function decrypt(text, password, iv) {
            var decipher = crypto.createDecipheriv(algorithm, password, iv);
            var dec = decipher.update(text, "hex", "utf8");
            dec += decipher.final("utf8");
            return dec;
        }
        Crypto.decrypt = decrypt;
    })(Crypto = RS.Crypto || (RS.Crypto = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Directives;
    (function (Directives) {
        function parse(str, defines) {
            /*
             * Match /* with an optional // prefix
             * Followed by any amount of whitespace
             * Followed by 1 or more letters
             * Followed optionally by : and
             * one or more non-* characters
             * Followed by any amount of whitespace
             * Followed by *​/
             */
            // const parser = new Parser(/\/\*\s*([a-z]+)(:[^\*]+)?\s*\*\//gi, str);
            var parser = new Parser(/(?:\/\/\s*)?\/\*\s*([a-z]+)(:[^\*]+)?\s*\*\//gi, str);
            var usesDefines = [];
            var hasCondition, index;
            do {
                index = parser.lastIndex;
                hasCondition = readCondition(parser, defines, usesDefines);
            } while (hasCondition);
            parser.parsedString += str.slice(index);
            return { content: parser.parsedString, usesDefines: usesDefines };
        }
        Directives.parse = parse;
        var Keywords;
        (function (Keywords) {
            Keywords["IfDefined"] = "ifdef";
            Keywords["IfNotDefined"] = "ifndef";
            Keywords["Else"] = "else";
            Keywords["EndIf"] = "endif";
        })(Keywords || (Keywords = {}));
        var Parser = /** @class */ (function () {
            function Parser(regExp, string) {
                this.regExp = regExp;
                this.string = string;
                this.parsedString = "";
                this._lastMatch = null;
            }
            Object.defineProperty(Parser.prototype, "lastMatch", {
                get: function () { return this._lastMatch; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Parser.prototype, "lastIndex", {
                get: function () { return this.regExp.lastIndex; },
                set: function (index) { this.regExp.lastIndex = index; },
                enumerable: true,
                configurable: true
            });
            Parser.prototype.next = function () {
                var match;
                do {
                    match = this.regExp.exec(this.string);
                } while (match != null && !this.isKeyword(match[1]));
                this._lastMatch = match;
                return this._lastMatch;
            };
            Parser.prototype.isKeyword = function (str) {
                for (var key in Keywords) {
                    if (Keywords[key] === str) {
                        return true;
                    }
                }
                return false;
            };
            return Parser;
        }());
        function pushUnique(arr, item) {
            if (arr.indexOf(item) === -1) {
                arr.push(item);
            }
        }
        /** Reads a condition e.g. ifdef. */
        function readCondition(parser, defines, usesDefines, nested) {
            if (nested === void 0) { nested = false; }
            var startIndex = parser.lastIndex;
            for (var match = parser.next(); match != null; match = parser.next()) {
                var matchStr = match[0], keyword = match[1], args = match[2];
                //commented out, pretend it doesn't exist
                if (matchStr.slice(0, 2) === "//") {
                    continue;
                }
                var invert = void 0;
                switch (keyword) {
                    case Keywords.IfDefined:
                        invert = false;
                        break;
                    case Keywords.IfNotDefined:
                        invert = true;
                        break;
                    default:
                        if (nested) {
                            return false;
                        }
                        throw new Error("Unexpected " + keyword);
                }
                // Split to current index
                parser.parsedString += parser.string.slice(startIndex, parser.lastIndex - matchStr.length);
                if (!args) {
                    throw new Error("Argument string missing");
                }
                var argv = args.split(":");
                var define = argv[1].trim();
                pushUnique(usesDefines, define);
                var hasDefine = !!defines[define];
                var truthy = invert ? !hasDefine : hasDefine;
                if (truthy) {
                    // Parse inner conditions recursively
                    readConditionBlock(parser, defines, usesDefines);
                    var _a = parser.lastMatch, nextKeyword = _a[1];
                    switch (nextKeyword) {
                        case Keywords.Else:
                            {
                                // Skip to EndIf and return
                                skipConditionBlock(parser);
                                if (parser.lastMatch == null) {
                                    throw new Error("Missing " + Keywords.EndIf);
                                }
                                var _b = parser.lastMatch, lastKeyword = _b[1];
                                if (lastKeyword !== Keywords.EndIf) {
                                    throw new Error("Unexpected " + lastKeyword);
                                }
                                return true;
                            }
                        case Keywords.EndIf: return true;
                        default: throw new Error("Unexpected " + nextKeyword);
                    }
                }
                else {
                    // Skip this block
                    skipConditionBlock(parser);
                    // Expect EndIf or else
                    if (parser.lastMatch == null) {
                        throw new Error("Missing " + Keywords.EndIf);
                    }
                    var _c = parser.lastMatch, nextKeyword = _c[1];
                    switch (nextKeyword) {
                        case Keywords.Else:
                            {
                                readConditionBlock(parser, defines, usesDefines);
                                var _d = parser.lastMatch, lastKeyword = _d[1];
                                if (lastKeyword !== Keywords.EndIf) {
                                    throw new Error("Unexpected " + lastKeyword);
                                }
                                return true;
                            }
                        case Keywords.EndIf: return true;
                        default: throw new Error("Unexpected " + nextKeyword);
                    }
                }
            }
            return false;
        }
        /** Parses the inside of a conditional block. */
        function readConditionBlock(parser, defines, usesDefines) {
            // Parse inner conditions recursively
            var hasCondition, endIndex;
            do {
                endIndex = parser.lastIndex;
                hasCondition = readCondition(parser, defines, usesDefines, true);
            } while (hasCondition);
            // Expect EndIf
            if (parser.lastMatch == null) {
                throw new Error("Missing " + Keywords.EndIf);
            }
            var lastFull = parser.lastMatch[0];
            parser.parsedString += parser.string.slice(endIndex, parser.lastIndex - lastFull.length);
        }
        /** Moves parser to the next directive at the current depth, ignoring any nested directives. */
        function skipConditionBlock(parser) {
            var depth = 0;
            for (var match = parser.next(); match != null; match = parser.next()) {
                var keyword = match[1];
                if (depth === 0) {
                    if (keyword !== Keywords.IfNotDefined && keyword !== Keywords.IfDefined) {
                        return;
                    }
                    ++depth;
                }
                else {
                    if (keyword === Keywords.EndIf) {
                        --depth;
                    }
                }
            }
        }
    })(Directives = RS.Directives || (RS.Directives = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var BaseError = /** @class */ (function () {
        function BaseError(message) {
            this.inner = new Error(message);
        }
        Object.defineProperty(BaseError.prototype, "name", {
            get: function () { return this.inner.name; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseError.prototype, "message", {
            get: function () { return this.inner.message; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseError.prototype, "stack", {
            get: function () { return this.inner.stack; },
            enumerable: true,
            configurable: true
        });
        BaseError.prototype.toString = function () {
            return this.inner.toString();
        };
        return BaseError;
    }());
    RS.BaseError = BaseError;
})(RS || (RS = {}));
var RS;
(function (RS) {
    var FFMPEG;
    (function (FFMPEG) {
        var FFProbe;
        (function (FFProbe) {
            /**
             * Runs FFProbe on a file and returns the result.
             * @param fileName
             */
            function run(fileName) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, _b, err_19;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _c.trys.push([0, 2, , 3]);
                                _b = (_a = RS.JSON).parse;
                                return [4 /*yield*/, RS.Command.run("ffprobe", ["-v", "error", "-show_entries", "format:stream", "-of", "json", fileName], null, false)];
                            case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                            case 2:
                                err_19 = _c.sent();
                                throw new Error("Error whilst running FFProbe on " + fileName + ": " + err_19);
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            }
            FFProbe.run = run;
            function filterStreams(streams, codecType) {
                return streams.filter(function (s) { return s.codec_type === codecType; });
            }
            FFProbe.filterStreams = filterStreams;
        })(FFProbe = FFMPEG.FFProbe || (FFMPEG.FFProbe = {}));
    })(FFMPEG = RS.FFMPEG || (RS.FFMPEG = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Git;
    (function (Git) {
        /**
         * Removes a tag from the current commit.
         * @param name
         * @param message
         * @param repo
         */
        function untag(name, repo) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.Command.run("git", [
                                "tag",
                                "-d", name
                            ], repo, false)];
                        case 1:
                            _a.sent();
                            return [4 /*yield*/, RS.Command.run("git", [
                                    "push", "origin",
                                    ":" + name,
                                    "-q"
                                ], repo, false)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Git.untag = untag;
        /**
         * Adds an annotated tag to the current commit.
         * @param name
         * @param message
         * @param repo
         */
        function tag(name, message, repo) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.Command.run("git", [
                                "tag",
                                "-a", name,
                                "-m", message
                            ], repo, false)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Git.tag = tag;
        /**
         * Pushes the specified local tag to remote.
         * @param name
         * @param repo
         */
        function pushTag(name, repo) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.Command.run("git", [
                                "push", "origin",
                                name,
                                "-q"
                            ], repo, false)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Git.pushTag = pushTag;
        /**
         * Pushes all local tags to remote.
         * @param repo
         */
        function pushTags(repo) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.Command.run("git", [
                                "push", "origin",
                                "--tags",
                                "-q"
                            ], repo, false)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Git.pushTags = pushTags;
        /**
         * Clones a specified repo to a path, if the path is valid and repo doesn't already exist
         * @param repo
         * @param clonePath
         * @param args
         */
        function clone(repo, clonePath, args) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (RS.FileSystem.fileExists(clonePath)) {
                                return [2 /*return*/];
                            }
                            return [4 /*yield*/, RS.Command.run("git", [
                                    "clone",
                                    // ...args,
                                    repo,
                                    clonePath
                                ], null, false)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Git.clone = clone;
        /**
         * Commits all changes in specified repo with a message
         * @param repoPath
         * @param message
         */
        function commitAll(repoPath, message) {
            return __awaiter(this, void 0, void 0, function () {
                var error_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, RS.Command.run("git", [
                                    "commit",
                                    "-am",
                                    message
                                ], repoPath, false)];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            error_1 = _a.sent();
                            RS.Log.error("Error committing changes to repo '" + repoPath + "': " + error_1);
                            return [3 /*break*/, 3];
                        case 3: return [2 /*return*/];
                    }
                });
            });
        }
        Git.commitAll = commitAll;
        /**
         * Pushes up changes in repo path
         * @param repoPath
         */
        function push(repoPath) {
            return __awaiter(this, void 0, void 0, function () {
                var error_2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, RS.Command.run("git", ["push"], repoPath, false)];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            error_2 = _a.sent();
                            RS.Log.error("Error pushing changes to repo '" + repoPath + "': " + error_2);
                            return [3 /*break*/, 3];
                        case 3: return [2 /*return*/];
                    }
                });
            });
        }
        Git.push = push;
        /**
         * Checks out the specified branch and pulls the latest changes, optionally by force
         * @param repoPath
         * @param branch
         * @param force
         */
        function update(repoPath, branch, force) {
            if (force === void 0) { force = true; }
            return __awaiter(this, void 0, void 0, function () {
                var error_3;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, RS.Command.run("git", [
                                    "checkout",
                                    (force ? "-f" : null),
                                    branch
                                ], repoPath, false)];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            error_3 = _a.sent();
                            // If the error isn't that we're already on the branch (Network, invalid name etc) then re-throw
                            if (error_3 != "Already on '" + branch + "'\n" && error_3 != "Error: Switched to branch '" + branch + "'") {
                                throw new Error(error_3);
                            }
                            return [3 /*break*/, 3];
                        case 3: return [4 /*yield*/, RS.Command.run("git", ["pull"], repoPath, false)];
                        case 4:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        Git.update = update;
    })(Git = RS.Git || (RS.Git = {}));
})(RS || (RS = {}));
/**
 * Contains HTTP utilities.
 */
var RS;
(function (RS) {
    var HTTP;
    (function (HTTP) {
        var debugMode = false;
        var http = require("http");
        var https = require("https");
        /**
         * Fetches the content at the specified URL.
         * @param url
         */
        function fetch(url) {
            var protocol = url.match(/[a-z]+(?=:\/\/)/g)[0];
            var lib = protocol === "https" ? https : http;
            return new Promise(function (resolve, reject) {
                var req = lib.request(url, function (result) {
                    var str = "";
                    result.setEncoding("utf8");
                    result.on("data", function (buffer) {
                        if (debugMode) {
                            RS.Log.debug("HTTP request got some data");
                        }
                        str += buffer.toString();
                    });
                    result.on("end", function () {
                        if (debugMode) {
                            RS.Log.debug("HTTP request ended");
                        }
                        resolve({ data: str, responseCode: result.statusCode });
                    });
                });
                req.on("error", function (err) {
                    reject(err);
                });
                req.end();
                if (debugMode) {
                    RS.Log.debug("Sending HTTP request to '" + url + "'");
                }
            });
        }
        HTTP.fetch = fetch;
        /**
         * https://gist.github.com/dperini/729294
         */
        var re_weburl = new RegExp("^" +
            // protocol identifier
            "(?:(?:https?|ftp)://)" +
            // user:pass authentication
            "(?:\\S+(?::\\S*)?@)?" +
            "(?:" +
            // IP address exclusion
            // private & local networks
            "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
            "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
            "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
            // IP address dotted notation octets
            // excludes loopback network 0.0.0.0
            // excludes reserved space >= 224.0.0.0
            // excludes network & broacast addresses
            // (first & last IP address of each class)
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
            "|" +
            // host name
            "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            // domain name
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
            // TLD identifier
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            // TLD may end with dot
            "\\.?" +
            ")" +
            // port number
            "(?::\\d{2,5})?" +
            // resource path
            "(?:[/?#]\\S*)?" +
            "$", "i");
        /**
         * Gets if the specified string is a valid URL.
         * @param val
         */
        function isURL(val) {
            return re_weburl.test(val);
        }
        HTTP.isURL = isURL;
    })(HTTP = RS.HTTP || (RS.HTTP = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var MessageHandler = /** @class */ (function () {
        function MessageHandler() {
            this._messageListeners = [];
            this._isDisposed = false;
            this.onMessageReceived = this.onMessageReceived.bind(this);
        }
        Object.defineProperty(MessageHandler.prototype, "isDisposed", {
            get: function () { return this._isDisposed; },
            enumerable: true,
            configurable: true
        });
        MessageHandler.listenerDoesMatch = function (listener, message) {
            if (listener.predicate == null) {
                return true;
            }
            if (RS.Is.func(listener.predicate)) {
                return listener.predicate(message);
            }
            return listener.predicate === message;
        };
        MessageHandler.prototype.on = function (p1, p2) {
            this._messageListeners.push(RS.Is.string(p1) ? { callback: p2, predicate: p1 } : { callback: p1 });
        };
        MessageHandler.prototype.off = function (p1, p2) {
            if (RS.Is.string(p1)) {
                for (var i = 0; i < this._messageListeners.length; ++i) {
                    var listener = this._messageListeners[i];
                    if (listener.predicate === p1 && listener.callback === p2) {
                        this._messageListeners.splice(i, 1);
                        return;
                    }
                }
            }
            else {
                for (var i = 0; i < this._messageListeners.length; ++i) {
                    var listener = this._messageListeners[i];
                    if (listener.callback === p1) {
                        this._messageListeners.splice(i, 1);
                        return;
                    }
                }
            }
        };
        /** Waits for a message of the given name. */
        MessageHandler.prototype.event = function (name) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this._messageListeners.push({ predicate: name, resolve: resolve, reject: reject });
            });
        };
        /** Waits for a message, optionally matching the given predicate. */
        MessageHandler.prototype.message = function (predicate) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this._messageListeners.push({ predicate: predicate, resolve: resolve, reject: reject });
            });
        };
        MessageHandler.prototype.dispose = function () {
            if (this._isDisposed) {
                return;
            }
            this.rejectAllMessageListeners();
            this._isDisposed = true;
        };
        MessageHandler.prototype.onMessageReceived = function (message) {
            RS.Log.debug("Received message: " + message);
            var messageListeners = this._messageListeners.filter(function (listener) { return MessageHandler.listenerDoesMatch(listener, message); });
            for (var _i = 0, messageListeners_1 = messageListeners; _i < messageListeners_1.length; _i++) {
                var messageListener = messageListeners_1[_i];
                if (messageListener.callback) {
                    messageListener.callback(message);
                }
                else if (messageListener.resolve) {
                    messageListener.resolve(message);
                    var index = this._messageListeners.indexOf(messageListener);
                    this._messageListeners.splice(index, 1);
                }
            }
            if (messageListeners.length === 0) {
                RS.Log.warn("Unhandled message: " + message);
            }
        };
        MessageHandler.prototype.rejectAllMessageListeners = function (reason) {
            for (var _i = 0, _a = this._messageListeners; _i < _a.length; _i++) {
                var messageListener = _a[_i];
                if (messageListener.reject) {
                    messageListener.reject(reason);
                }
            }
            this._messageListeners.length = 0;
        };
        return MessageHandler;
    }());
    RS.MessageHandler = MessageHandler;
})(RS || (RS = {}));
/// <reference path="MessageHandler.ts"/>
var RS;
(function (RS) {
    var _ipc;
    function getIPC() {
        if (_ipc) {
            return _ipc;
        }
        _ipc = require("node-ipc");
        _ipc.config.logger = function (msg) { return RS.Log.debug(msg, "IPC"); };
        _ipc.config.maxRetries = 0;
        _ipc.config.silent = true;
        return _ipc;
    }
    var IPC = /** @class */ (function () {
        function IPC() {
        }
        Object.defineProperty(IPC, "id", {
            get: function () { return getIPC().config.id; },
            set: function (value) { getIPC().config.id = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IPC, "timeout", {
            get: function () { return getIPC().config.retry; },
            set: function (value) { getIPC().config.retry = value; },
            enumerable: true,
            configurable: true
        });
        return IPC;
    }());
    RS.IPC = IPC;
    (function (IPC) {
        var ReplyTarget;
        (function (ReplyTarget) {
            ReplyTarget[ReplyTarget["None"] = 0] = "None";
            ReplyTarget[ReplyTarget["Client"] = 1] = "Client";
            ReplyTarget[ReplyTarget["Parent"] = 2] = "Parent";
        })(ReplyTarget || (ReplyTarget = {}));
        var Server = /** @class */ (function (_super) {
            __extends(Server, _super);
            function Server() {
                var _this = _super.call(this) || this;
                _this._running = false;
                _this._starting = false;
                _this._replyTarget = ReplyTarget.None;
                _this.onParentMessageReceived = _this.onParentMessageReceived.bind(_this);
                _this.onClientMessageReceived = _this.onClientMessageReceived.bind(_this);
                if (RS.Process.isChild) {
                    process.on("message", _this.onParentMessageReceived);
                }
                return _this;
            }
            Server.get = function () {
                if (this._instance) {
                    return this._instance;
                }
                return this._instance = new Server();
            };
            Server.prototype.start = function () {
                var _this = this;
                if (this._starting) {
                    throw new Error("Cannot start as server is already starting");
                }
                if (this._running) {
                    throw new Error("Cannot start as server is already running");
                }
                this._starting = true;
                return new Promise(function (resolve, reject) {
                    function resolveAndUnlisten() {
                        getIPC().server.off("error", rejectAndUnlisten);
                        resolve();
                    }
                    function rejectAndUnlisten(reason) {
                        getIPC().server.off("error", rejectAndUnlisten);
                        reject(reason);
                    }
                    getIPC().serve(function () {
                        _this._starting = false;
                        _this._running = true;
                        _this.setListeners(true);
                        resolveAndUnlisten();
                    });
                    getIPC().server.on("error", rejectAndUnlisten);
                    getIPC().server.start();
                });
            };
            Server.prototype.broadcast = function (message) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        // For some reason node-ipc borks on an empty message
                        if (RS.Process.isChild) {
                            process.send(message);
                        }
                        getIPC().server.broadcast("message", this.prepareMessageForNodeIPC(message));
                        RS.Log.debug("Broadcasted message: " + message);
                        return [2 /*return*/];
                    });
                });
            };
            /** Replies to the previous client message. */
            Server.prototype.reply = function (message) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (this._replyTarget) {
                            case ReplyTarget.Client:
                                {
                                    getIPC().server.emit(this._lastClient, "message", this.prepareMessageForNodeIPC(message));
                                    RS.Log.debug("Replied to " + this._lastClient + " with message: " + message);
                                    this._lastClient = null;
                                    break;
                                }
                            case ReplyTarget.Parent:
                                {
                                    RS.Log.debug("Replied to parent process with message: " + message);
                                    process.send(message);
                                    break;
                                }
                            case ReplyTarget.None:
                            default:
                                throw new Error("No target for reply");
                        }
                        this._replyTarget = ReplyTarget.None;
                        return [2 /*return*/];
                    });
                });
            };
            /** Opens a two-way communications channel with the last client. */
            Server.prototype.open = function (name) {
                throw new Error("Unimplemented");
            };
            Server.prototype.dispose = function () {
                if (this.isDisposed) {
                    return;
                }
                this.setListeners(false);
                _super.prototype.dispose.call(this);
            };
            Server.prototype.onParentMessageReceived = function (message) {
                this._replyTarget = ReplyTarget.Parent;
                this.onMessageReceived(message);
            };
            Server.prototype.onClientMessageReceived = function (message, socket) {
                this._lastClient = socket;
                this._replyTarget = ReplyTarget.Client;
                this.onMessageReceived(this.readPreparedMessage(message));
            };
            Server.prototype.prepareMessageForNodeIPC = function (message) {
                return message || "\0";
            };
            Server.prototype.readPreparedMessage = function (message) {
                return message === "\0" ? "" : message;
            };
            Server.prototype.setListeners = function (attach) {
                var serverFunc = attach ? getIPC().server.on.bind(getIPC().server) : getIPC().server.off.bind(getIPC().server);
                serverFunc("message", this.onClientMessageReceived);
            };
            return Server;
        }(RS.MessageHandler));
        IPC.Server = Server;
        var Client = /** @class */ (function (_super) {
            __extends(Client, _super);
            function Client(target) {
                var _this = _super.call(this) || this;
                _this.target = target;
                _this._connected = false;
                _this.onConnected = _this.onConnected.bind(_this);
                _this.onDisconnected = _this.onDisconnected.bind(_this);
                _this.onServerMessageReceived = _this.onServerMessageReceived.bind(_this);
                return _this;
            }
            Object.defineProperty(Client.prototype, "server", {
                get: function () { return getIPC().of[this.target]; },
                enumerable: true,
                configurable: true
            });
            Client.prototype.connect = function () {
                var _this = this;
                if (this._connected) {
                    throw new Error("Attempted to connect a connected client");
                }
                return new Promise(function (resolve, reject) {
                    getIPC().connectTo(_this.target, function () {
                        function resolveAndUnlisten() {
                            if (this.server) {
                                this.server.off("error", rejectAndUnlisten);
                                this.server.off("connect", resolveAndUnlisten);
                            }
                            resolve();
                        }
                        function rejectAndUnlisten(reason) {
                            if (this.server) {
                                this.server.off("error", rejectAndUnlisten);
                                this.server.off("connect", resolveAndUnlisten);
                            }
                            reject(reason);
                        }
                        _this.setListeners(true);
                        _this.server.on("error", rejectAndUnlisten);
                        _this.server.on("connect", resolveAndUnlisten);
                    });
                });
            };
            Client.prototype.send = function (message) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        if (!this._connected) {
                            throw new Error("Attempted to send to an unconnected client");
                        }
                        this.server.emit("message", message);
                        return [2 /*return*/];
                    });
                });
            };
            Client.prototype.dispose = function () {
                if (this.isDisposed) {
                    return;
                }
                this.setListeners(false);
                if (this._connected) {
                    getIPC().disconnect(this.target);
                }
                _super.prototype.dispose.call(this);
            };
            Client.prototype.onConnected = function () {
                this._connected = true;
            };
            Client.prototype.onDisconnected = function () {
                this._connected = false;
                this.rejectAllMessageListeners();
            };
            Client.prototype.onServerMessageReceived = function (message) {
                if (message === "\0") {
                    message = "";
                }
                this.onMessageReceived(message);
            };
            Client.prototype.setListeners = function (attach) {
                var func = attach ? this.server.on.bind(this.server) : this.server.off.bind(this.server);
                func("connect", this.onConnected);
                func("disconnect", this.onDisconnected);
                func("message", this.onServerMessageReceived);
            };
            return Client;
        }(RS.MessageHandler));
        IPC.Client = Client;
    })(IPC = RS.IPC || (RS.IPC = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var JSMinify;
    (function (JSMinify) {
        function minify(sourcePath, mapPath) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            RS.Profile.enter("jsminify");
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, , 3, 4]);
                            return [4 /*yield*/, RS.Command.run("node", [
                                    "--max-old-space-size=4096",
                                    RS.Path.combine(__dirname, "jsminify.js"),
                                    sourcePath,
                                    mapPath
                                ], undefined, false)];
                        case 2:
                            _a.sent();
                            return [3 /*break*/, 4];
                        case 3:
                            RS.Profile.exit("jsminify");
                            return [7 /*endfinally*/];
                        case 4:
                            RS.FileSystem.cache.set(sourcePath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                            if (mapPath != null) {
                                RS.FileSystem.cache.set(mapPath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                            }
                            return [2 /*return*/];
                    }
                });
            });
        }
        JSMinify.minify = minify;
    })(JSMinify = RS.JSMinify || (RS.JSMinify = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var JSON;
    (function (JSON) {
        var stripjsoncomments = require("strip-json-comments");
        /** Tries to parse a file as JSON. */
        function parseFile(path) {
            return __awaiter(this, void 0, void 0, function () {
                var data, err_20;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, RS.FileSystem.readFile(path)];
                        case 1:
                            data = _a.sent();
                            return [2 /*return*/, parse(data)];
                        case 2:
                            err_20 = _a.sent();
                            throw new Error("Error whilst reading " + path + ": " + err_20);
                        case 3: return [2 /*return*/];
                    }
                });
            });
        }
        JSON.parseFile = parseFile;
        /**
         * Converts a JavaScript Object Notation (JSON) string into an object.
         * @param json
         */
        function parse(json) {
            var commentless = stripjsoncomments(json);
            var trimmed = commentless.trim();
            return global.JSON.parse(trimmed);
        }
        JSON.parse = parse;
        /**
         * Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
         */
        JSON.stringify = global.JSON.stringify;
    })(JSON = RS.JSON || (RS.JSON = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var JSReplace;
    (function (JSReplace) {
        function replace(sourcePath, mapPath, replacements) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            RS.Profile.enter("jsreplace");
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, , 3, 4]);
                            return [4 /*yield*/, RS.Command.run("node", __spreadArrays([
                                    "--max-old-space-size=4096",
                                    RS.Path.combine(__dirname, "jsreplace.js"),
                                    sourcePath,
                                    mapPath
                                ], Object.keys(replacements).map(function (key) { return key + "=" + replacements[key]; })), undefined, false)];
                        case 2:
                            _a.sent();
                            return [3 /*break*/, 4];
                        case 3:
                            RS.Profile.exit("jsreplace");
                            return [7 /*endfinally*/];
                        case 4:
                            RS.FileSystem.cache.set(sourcePath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                            if (mapPath != null) {
                                RS.FileSystem.cache.set(mapPath, { type: RS.FileIO.FileSystemCache.NodeType.File, content: null });
                            }
                            return [2 /*return*/];
                    }
                });
            });
        }
        JSReplace.replace = replace;
    })(JSReplace = RS.JSReplace || (RS.JSReplace = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        // Ported from: https://github.com/io-monad/line-column
        /**
         * Finder for index and line-column from given string.
         */
        var LineColumnFinder = /** @class */ (function () {
            function LineColumnFinder(str, options) {
                if (options === void 0) { options = {}; }
                this.str = str || "";
                this.lineToIndex = this.buildLineToIndex(this.str);
                this.origin = options.origin != null ? options.origin : 1;
            }
            /**
             * Find line and column from index in the string.
             *
             * @param  {number} index - Index in the string. (0-origin)
             * @return {Object|null}
             *     Found line number and column number in object `{ line: X, col: Y }`.
             *     If the given index is out of range, it returns `null`.
             */
            LineColumnFinder.prototype.fromIndex = function (index) {
                if (index < 0 || index >= this.str.length || isNaN(index)) {
                    return null;
                }
                var line = this.findLowerIndexInRangeArray(index, this.lineToIndex);
                return {
                    line: line + this.origin,
                    col: index - this.lineToIndex[line] + this.origin
                };
            };
            LineColumnFinder.prototype.toIndex = function (p1, p2) {
                if (p2 == null) {
                    if (RS.Is.array(p1)) {
                        if (p1.length >= 2) {
                            return this.toIndex(p1[0], p1[1]);
                        }
                        else {
                            return -1;
                        }
                    }
                    else if (RS.Is.number(p1)) {
                        return -1;
                    }
                    else {
                        return this.toIndex(p1.line, p1.col);
                    }
                }
                var line = p1;
                var column = p2;
                if (!RS.Is.number(line)) {
                    return -1;
                }
                line -= this.origin;
                column -= this.origin;
                if (line >= 0 && column >= 0 && line < this.lineToIndex.length) {
                    var lineIndex = this.lineToIndex[line];
                    var nextIndex = (line === this.lineToIndex.length - 1
                        ? this.str.length
                        : this.lineToIndex[line + 1]);
                    if (column < nextIndex - lineIndex) {
                        return lineIndex + column;
                    }
                }
                return -1;
            };
            /**
             * Build an array of indexes of each line from a string.
             */
            LineColumnFinder.prototype.buildLineToIndex = function (str) {
                var lines = RS.Util.splitLines(str);
                var lineToIndex = new Array(lines.length);
                var index = 0;
                for (var i = 0, l = lines.length; i < l; ++i) {
                    lineToIndex[i] = index;
                    index += lines[i].length + /* "\n".length */ 1;
                }
                return lineToIndex;
            };
            /**
             * Find a lower-bound index of a value in a sorted array of ranges.
             *
             * Assume `arr = [0, 5, 10, 15, 20]` and
             * this returns `1` for `value = 7` (5 <= value < 10),
             * and returns `3` for `value = 18` (15 <= value < 20).
             */
            LineColumnFinder.prototype.findLowerIndexInRangeArray = function (value, arr) {
                if (value >= arr[arr.length - 1]) {
                    return arr.length - 1;
                }
                var min = 0, max = arr.length - 2, mid;
                while (min < max) {
                    mid = min + ((max - min) >> 1);
                    if (value < arr[mid]) {
                        max = mid - 1;
                    }
                    else if (value >= arr[mid + 1]) {
                        min = mid + 1;
                    }
                    else { // value >= arr[mid] && value < arr[mid + 1]
                        min = mid;
                        break;
                    }
                }
                return min;
            };
            return LineColumnFinder;
        }());
        Build.LineColumnFinder = LineColumnFinder;
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
/// <reference path="Command.ts" />
/** Contains utility functions for the node module system. */
var RS;
(function (RS) {
    var NPM;
    (function (NPM) {
        var pendingInstall = null;
        /**
         * Installs a node module locally.
         * @param name
         * @param version
         */
        function installPackage(name, version) {
            return __awaiter(this, void 0, void 0, function () {
                var myPromise;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(pendingInstall != null)) return [3 /*break*/, 2];
                            return [4 /*yield*/, pendingInstall];
                        case 1:
                            _a.sent();
                            pendingInstall = null;
                            _a.label = 2;
                        case 2:
                            myPromise = pendingInstall = installPackageInner(name, version);
                            return [4 /*yield*/, pendingInstall];
                        case 3:
                            _a.sent();
                            if (pendingInstall === myPromise) {
                                pendingInstall = null;
                            }
                            return [2 /*return*/];
                    }
                });
            });
        }
        NPM.installPackage = installPackage;
        /**
         * Installs a node module locally without parallel checks.
         * @param name
         * @param version
         */
        function installPackageInner(name, version) {
            return __awaiter(this, void 0, void 0, function () {
                var formattedName, logOutput, err_21;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, RS.Command.exists("npm")];
                        case 1:
                            // Check that the command exists
                            if (!(_a.sent())) {
                                throw new Error("Could not find npm - check that it's installed! (which it will be because it's part of node, so how you managed to get this error is beyond me...)");
                            }
                            RS.Log.info("Installing node module '" + name + "' (" + version + ") via npm...");
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            formattedName = /:\/\//g.test(version) ? version : name + "@" + version;
                            return [4 /*yield*/, RS.Command.run("npm", ["install", formattedName], undefined, false)];
                        case 3:
                            logOutput = _a.sent();
                            return [3 /*break*/, 5];
                        case 4:
                            err_21 = _a.sent();
                            if (/npm ERR/g.test(err_21.toString())) {
                                RS.Log.error(err_21);
                            }
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        }
    })(NPM = RS.NPM || (RS.NPM = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Build;
    (function (Build) {
        /**
         * Runs several async processing chains in parallel.
        **/
        var ParallelProcessor = /** @class */ (function () {
            function ParallelProcessor(items, threadCount, processor) {
                this._items = items;
                this._threadCount = threadCount;
                this._processor = processor;
            }
            /** Launches all threads and waits until completion, returning the results. */
            ParallelProcessor.prototype.process = function () {
                return __awaiter(this, void 0, void 0, function () {
                    var result, threads, i, i;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                result = this._result = {
                                    succeedItems: [],
                                    failedItems: []
                                };
                                this._curItemIndex = 0;
                                threads = [];
                                for (i = 0; i < this._threadCount; i++) {
                                    threads.push(this.thread(i));
                                }
                                i = 0;
                                _a.label = 1;
                            case 1:
                                if (!(i < this._threadCount)) return [3 /*break*/, 4];
                                return [4 /*yield*/, threads[i]];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                i++;
                                return [3 /*break*/, 1];
                            case 4:
                                // Done
                                this._result = null;
                                return [2 /*return*/, result];
                        }
                    });
                });
            };
            /** Runs a single thread that will process all items until exhaustion. */
            ParallelProcessor.prototype.thread = function (id) {
                return __awaiter(this, void 0, void 0, function () {
                    var success, item, err_22;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!(this._curItemIndex < this._items.length)) return [3 /*break*/, 5];
                                success = true;
                                item = this._items[this._curItemIndex++];
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, this._processor(item, id)];
                            case 2:
                                _a.sent();
                                return [3 /*break*/, 4];
                            case 3:
                                err_22 = _a.sent();
                                success = false;
                                return [3 /*break*/, 4];
                            case 4:
                                success && this._result.succeedItems.push(item) || this._result.failedItems.push(item);
                                return [3 /*break*/, 0];
                            case 5: return [2 /*return*/];
                        }
                    });
                });
            };
            return ParallelProcessor;
        }());
        Build.ParallelProcessor = ParallelProcessor;
    })(Build = RS.Build || (RS.Build = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Process;
    (function (Process) {
        Process.isChild = process.send != null;
    })(Process = RS.Process || (RS.Process = {}));
})(RS || (RS = {}));
/// <reference path="MessageHandler.ts"/>
var RS;
(function (RS) {
    var cp = require("child_process");
    var Sandbox = /** @class */ (function (_super) {
        __extends(Sandbox, _super);
        function Sandbox(scriptPath) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var _this = _super.call(this) || this;
            _this._scriptPath = scriptPath;
            _this._args = args;
            _this.onExit = _this.onExit.bind(_this);
            _this.onError = _this.onError.bind(_this);
            return _this;
        }
        Sandbox.prototype.start = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (this._process) {
                        throw new Error("Attempted to start sandbox that had already been started");
                    }
                    this._process = cp.fork(this._scriptPath, this._args);
                    this._process.on("message", this.onMessageReceived);
                    this._process.on("error", this.onError);
                    this._process.on("close", this.onExit);
                    this._process.on("exit", this.onExit);
                    return [2 /*return*/];
                });
            });
        };
        Sandbox.prototype.send = function (message) {
            var _this = this;
            if (!this._process) {
                throw new Error("Cannot send a message to an inactive sandbox");
            }
            return new Promise(function (resolve, reject) {
                _this._process.send(message, function (err) {
                    RS.Log.debug("Sent message: " + message);
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve();
                });
            });
        };
        Sandbox.prototype.dispose = function () {
            if (this.isDisposed) {
                return;
            }
            this.detachProcessListeners();
            this._process.kill();
            this._process = null;
            _super.prototype.dispose.call(this);
        };
        Sandbox.prototype.onError = function (err) {
            this.detachProcessListeners();
            this.rejectAllMessageListeners(err);
        };
        Sandbox.prototype.onExit = function (code, signal) {
            this.detachProcessListeners();
            this.rejectAllMessageListeners(new Error("Process exited with code " + code + (signal != null ? " (signal " + signal + ")" : "")));
        };
        Sandbox.prototype.detachProcessListeners = function () {
            RS.Log.debug("Detaching process listeners", "Sandbox");
            this._process.off("message", this.onMessageReceived);
            this._process.off("error", this.onError);
            this._process.off("close", this.onExit);
            this._process.off("exit", this.onExit);
        };
        return Sandbox;
    }(RS.MessageHandler));
    RS.Sandbox = Sandbox;
})(RS || (RS = {}));
/// <reference path="Command.ts" />
/** Contains TypeScript compiler functionality. */
var RS;
(function (RS) {
    var TSLint;
    (function (TSLint) {
        var tslintPath = "./node_modules/tslint/bin/tslint";
        /**
         * Compiles the specified TypeScript project.
         * @param projectPath
         * @param logOutput
         */
        function lint(projectPath, lintPath, logOutput) {
            if (logOutput === void 0) { logOutput = false; }
            return new Promise(function (resolve, reject) {
                RS.Command.run(tslintPath, ["-c", lintPath, "--project", RS.Path.combine(projectPath, "tsconfig.json")], undefined, logOutput)
                    .then(function (data) {
                    if (data.length > 0) {
                        reject(new LintFailure(data.trim()));
                    }
                    else {
                        resolve();
                    }
                }, function (reason) {
                    var reasonAsStr = RS.Is.string(reason) ? reason : reason.toString();
                    if (/ENOENT/g.test(reasonAsStr)) {
                        reject("TSLint not found. Check that node modules are installed.");
                    }
                    else {
                        reject(reason);
                    }
                });
            });
        }
        TSLint.lint = lint;
        var LintFailure = /** @class */ (function () {
            function LintFailure(data) {
                this.data = data;
            }
            Object.defineProperty(LintFailure.prototype, "isError", {
                get: function () { return /ERROR: /.test(this.data); },
                enumerable: true,
                configurable: true
            });
            LintFailure.prototype.toString = function () { return "" + this.data; };
            return LintFailure;
        }());
        TSLint.LintFailure = LintFailure;
    })(TSLint = RS.TSLint || (RS.TSLint = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Tag;
    (function (Tag) {
        var nextTagID = 0;
        function hasTags(value) {
            return RS.Is.object(value) && RS.Is.array(value._tags);
        }
        /**
         * Creates a new tag.
         */
        function create() {
            var tagID = nextTagID++;
            var decoratorAsTag = decorator;
            function decorator(value) {
                return function (target) {
                    // target is the constructor of the function
                    var targetEx = target.prototype;
                    if (targetEx.__tags == null || targetEx.__tags === Object.getPrototypeOf(targetEx).__tags) {
                        targetEx.__tags = [];
                    }
                    targetEx.__tags[tagID] = value;
                    decoratorAsTag.classes.push(target);
                };
            }
            decoratorAsTag.get = function (obj) {
                if (RS.Is.func(obj)) {
                    var protoChain = RS.Util.getInheritanceChain(obj, true);
                    for (var _i = 0, protoChain_1 = protoChain; _i < protoChain_1.length; _i++) {
                        var ctor = protoChain_1[_i];
                        var proto = ctor.prototype;
                        if (proto.__tags) {
                            return proto.__tags[tagID];
                        }
                    }
                }
                else if (hasTags(obj)) {
                    return obj.__tags[tagID];
                }
                else {
                    var proto = Object.getPrototypeOf(obj);
                    return this.get(proto.constructor);
                }
                return null;
            };
            decoratorAsTag.classes = [];
            decoratorAsTag.getClassesWithTag = function (value) {
                return this.classes.filter(function (c) { return decoratorAsTag.get(c) === value; });
            };
            return decoratorAsTag;
        }
        Tag.create = create;
    })(Tag = RS.Tag || (RS.Tag = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    /** Returns a promise that resolves with the given promise and rejects if a set amount of time passes first. */
    function withTimeout(promise, timeoutMillis) {
        return new Promise(function (resolve, reject) {
            var timeoutHandle;
            promise.then(function (result) {
                // Clear timeout on timely resolution
                clearTimeout(timeoutHandle);
                resolve(result);
            });
            // Schedule rejection on timeout
            setTimeout(function () { reject(new TimeoutError(timeoutMillis)); }, timeoutMillis);
        });
    }
    RS.withTimeout = withTimeout;
    var TimeoutError = /** @class */ (function (_super) {
        __extends(TimeoutError, _super);
        function TimeoutError(millis) {
            return _super.call(this, "Timed out after " + millis + " ms") || this;
        }
        return TimeoutError;
    }(RS.BaseError));
    RS.TimeoutError = TimeoutError;
})(RS || (RS = {}));
/// <reference path="Command.ts" />
/** Contains TypeScript compiler functionality. */
var RS;
(function (RS) {
    var TypeScript;
    (function (TypeScript) {
        var tscPath = "./node_modules/typescript/bin/tsc";
        /**
         * Compiles the specified TypeScript project.
         * @param projectPath
         * @param logOutput
         */
        function compile(projectPath, logOutput) {
            if (logOutput === void 0) { logOutput = false; }
            return new Promise(function (resolve, reject) {
                RS.Command.run(tscPath, ["-p", projectPath], undefined, logOutput)
                    .then(function (data) {
                    if (data.length > 0) {
                        reject(data);
                    }
                    else {
                        resolve();
                    }
                }, function (reason) {
                    var reasonAsStr = RS.Is.string(reason) ? reason : reason.toString();
                    if (/ENOENT/g.test(reasonAsStr)) {
                        reject("TypeScript compiler not found. Check that node modules are installed.");
                    }
                    else {
                        reject(reason);
                    }
                });
            });
        }
        TypeScript.compile = compile;
    })(TypeScript = RS.TypeScript || (RS.TypeScript = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var _url = require("url");
    var URL = /** @class */ (function () {
        function URL(host) {
            this._nativeURL = new _url.URL(host);
        }
        Object.defineProperty(URL.prototype, "path", {
            get: function () { return this._nativeURL.pathname; },
            set: function (value) { this._nativeURL.pathname = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(URL.prototype, "port", {
            get: function () { return this._nativeURL.port ? parseInt(this._nativeURL.port) : undefined; },
            set: function (value) { this._nativeURL.port = "" + value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(URL.prototype, "params", {
            get: function () { return this._nativeURL.searchParams; },
            enumerable: true,
            configurable: true
        });
        URL.prototype.toString = function () { return "" + this._nativeURL; };
        return URL;
    }());
    RS.URL = URL;
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Is;
    (function (Is) {
        /** Gets if the specified value is a Version. */
        function version(value) {
            return Is.object(value) && Is.number(value.major) && Is.number(value.minor) && Is.number(value.revision);
        }
        Is.version = version;
    })(Is = RS.Is || (RS.Is = {}));
})(RS || (RS = {}));
(function (RS) {
    function Version(major, minor, revision) {
        if (major === void 0) { major = 0; }
        if (minor === void 0) { minor = 0; }
        if (revision === void 0) { revision = 0; }
        return { major: major, minor: minor, revision: revision };
    }
    RS.Version = Version;
    var regExp = /([0-9]+)(?:\.([0-9]+))?(?:\.([0-9]+))?/;
    (function (Version) {
        /** Copies a to b. */
        function copy(a, b) {
            b.major = a.major;
            b.minor = a.minor;
            b.revision = a.revision;
        }
        Version.copy = copy;
        /** Clones a. */
        function clone(a, out) {
            out = out || Version();
            copy(a, out);
            return out;
        }
        Version.clone = clone;
        /** Gets if a == b. */
        function equals(a, b) {
            return a.major === b.major && a.minor === b.minor && a.revision === b.revision;
        }
        Version.equals = equals;
        /** Gets if a > b. */
        function greaterThan(a, b) {
            if (a.major === b.major) {
                if (a.minor === b.minor) {
                    return a.revision > b.revision;
                }
                else {
                    return a.minor > b.minor;
                }
            }
            else {
                return a.major > b.major;
            }
        }
        Version.greaterThan = greaterThan;
        /** Gets if a < b. */
        function lessThan(a, b) {
            if (a.major === b.major) {
                if (a.minor === b.minor) {
                    return a.revision < b.revision;
                }
                else {
                    return a.minor < b.minor;
                }
            }
            else {
                return a.major < b.major;
            }
        }
        Version.lessThan = lessThan;
        /**
         * Gets if a version is compatible with a given required version.
         * It is assumed that minor and revision versions are not breaking, whilst major version are.
         * Therefore:
         * - 1.2.3 is not compatible with 2.2.3
         * - 1.3.3 is compatible with 1.2.3
         * - 1.2.4 is compatible with 1.2.3
         * - 1.2.2 is not compatible with 1.2.3
         * - 1.1.4 is not compatible with 1.2.3
         * - 0.0.4 is not compatible with 1.2.3
         */
        function compatible(version, required) {
            // Different major -> incompatible
            if (version.major !== required.major) {
                return false;
            }
            // Lower minor -> incompatible
            if (version.minor < required.minor) {
                return false;
            }
            // Same minor -> revision must be at least required revision
            if (version.minor === required.minor && version.revision < required.revision) {
                return false;
            }
            return true;
        }
        Version.compatible = compatible;
        /**
         * Gets if a matches the specified version values.
         * Non-present version values are assumed to be wildcards.
         * e.g. match(Version(1, 2, 3), 1) = match 1.2.3 with 1.*.* = true
         * e.g. match(Version(1, 2, 3), 1, 3, 3) = match 1.2.3 with 1.3.3 = false
         * e.g. match(Version(1, 2, 3), 1, 2) = match 1.2.3 with 1.2.* = true
         */
        function match(a, major, minor, revision) {
            if (major != null && a.major !== major) {
                return false;
            }
            if (minor != null && a.minor !== minor) {
                return false;
            }
            if (revision != null && a.revision !== revision) {
                return false;
            }
            return true;
        }
        Version.match = match;
        /**
         * Gets the next revision of a.
         * @param a
         * @param out
         */
        function nextRevision(a, out) {
            out = out || Version();
            copy(a, out);
            out.revision++;
            return out;
        }
        Version.nextRevision = nextRevision;
        /**
         * Gets the next minor version of a.
         * @param a
         * @param out
         */
        function nextMinor(a, out) {
            out = out || Version();
            copy(a, out);
            out.minor++;
            out.revision = 0;
            return out;
        }
        Version.nextMinor = nextMinor;
        /**
         * Gets the next major version of a.
         * @param a
         * @param out
         */
        function nextMajor(a, out) {
            out = out || Version();
            copy(a, out);
            out.major++;
            out.minor = 0;
            out.revision = 0;
            return out;
        }
        Version.nextMajor = nextMajor;
        /**
         * Converts a to a string.
         * @param a
         */
        function toString(a) {
            return a.major + "." + a.minor + "." + a.revision;
        }
        Version.toString = toString;
        /**
         * Parses a string into a version.
         * @param a
         */
        function fromString(str, out) {
            out = out || Version();
            var result = str.match(regExp);
            if (result) {
                out.major = parseInt(result[1]) || 0;
                out.minor = parseInt(result[2]) || 0;
                out.revision = parseInt(result[3]) || 0;
            }
            return out;
        }
        Version.fromString = fromString;
    })(Version = RS.Version || (RS.Version = {}));
})(RS || (RS = {}));
var RS;
(function (RS) {
    var _http = require("http");
    var _mimeTypes = require("mime-types");
    var debugMode = false;
    /** An extremely lightweight server that just serves a directory. */
    var WebServer = /** @class */ (function () {
        function WebServer(ip, port, rootPath) {
            this.ip = ip;
            this.port = port;
            this.rootPath = rootPath;
            this.serve = this.serve.bind(this);
            this._server = _http.createServer(this.serve);
        }
        Object.defineProperty(WebServer.prototype, "isRunning", {
            get: function () { return this._server.listening; },
            enumerable: true,
            configurable: true
        });
        WebServer.prototype.open = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this._server.listen(_this.port, _this.ip, function () {
                    RS.Log.info("Listening on " + _this.ip + ":" + _this.port + " at " + _this.rootPath);
                    // Asynchronously start pre-cache
                    _this.preCache();
                    resolve();
                });
            });
        };
        WebServer.prototype.close = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this._server.close(function (err) {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve();
                });
            });
        };
        WebServer.prototype.debug = function (message) {
            if (!debugMode) {
                return;
            }
            RS.Log.debug(message);
        };
        WebServer.prototype.preCache = function () {
            return __awaiter(this, void 0, void 0, function () {
                var path;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.debug("Pre-caching index page.");
                            path = RS.Path.combine(this.rootPath, "index.html");
                            return [4 /*yield*/, RS.FileSystem.readFile(path, true)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        WebServer.prototype.serve = function (request, response) {
            return __awaiter(this, void 0, void 0, function () {
                var queryIndex, path, baseName, directoryName, fullPath, mimeType, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            queryIndex = request.url.indexOf("?");
                            path = queryIndex === -1 ? request.url : request.url.substr(0, queryIndex);
                            this.debug("Received request for " + path);
                            baseName = RS.Path.baseName(path) || "index.html";
                            directoryName = RS.Path.directoryName(path);
                            fullPath = RS.Path.combine(this.rootPath, directoryName, baseName);
                            this.debug("Reading file at " + fullPath);
                            return [4 /*yield*/, RS.FileSystem.fileExists(fullPath)];
                        case 1:
                            if (!(_a.sent())) {
                                this.debug(fullPath + " not found");
                                response.writeHead(404, { "Content-Type": "text/plain" });
                                response.write("404 File Not Found");
                                response.end();
                                return [2 /*return*/];
                            }
                            mimeType = _mimeTypes.lookup(fullPath);
                            this.debug("Using MIME type " + mimeType);
                            return [4 /*yield*/, RS.FileSystem.readFile(fullPath, true)];
                        case 2:
                            responseData = _a.sent();
                            response.writeHead(200, { "Content-Type": mimeType });
                            response.write(responseData, "binary");
                            response.end(null, "binary");
                            return [2 /*return*/];
                    }
                });
            });
        };
        return WebServer;
    }());
    RS.WebServer = WebServer;
})(RS || (RS = {}));
var RS;
(function (RS) {
    var Permission;
    (function (Permission) {
        Permission[Permission["None"] = 0] = "None";
        Permission[Permission["Execute"] = 1] = "Execute";
        Permission[Permission["Write"] = 2] = "Write";
        Permission[Permission["WriteExecute"] = 3] = "WriteExecute";
        Permission[Permission["Read"] = 4] = "Read";
        Permission[Permission["ReadExecute"] = 5] = "ReadExecute";
        Permission[Permission["ReadWrite"] = 6] = "ReadWrite";
        Permission[Permission["ReadWriteExecute"] = 7] = "ReadWriteExecute";
    })(Permission = RS.Permission || (RS.Permission = {}));
    function validateArray(arr) {
        if (!arr) {
            throw Error("[Permissions] Input empty");
        }
        if (arr.length !== 9) {
            throw Error("[Permissions] Invalid input length");
        }
    }
    function convertToBoolArray(arr, comparator) {
        validateArray(arr);
        return arr.map(comparator || (function (a) { return a ? true : false; }));
    }
    function Permissions(p1, p2, p3) {
        if (RS.Is.number(p1)) {
            if (p2 == undefined || p3 == undefined) {
                // decimal number
                // stringify and read as octal string
                return Permissions(p1.toString());
            }
            else {
                return {
                    owner: p1,
                    group: p2,
                    other: p3
                };
            }
        }
        else if (RS.Is.arrayOf(p1, RS.Is.boolean)) {
            //binary array
            validateArray(p1);
            return {
                owner: (p1[0] ? Permission.Read : Permission.None) | (p1[1] ? Permission.Write : Permission.None) | (p1[2] ? Permission.Execute : Permission.None),
                group: (p1[3] ? Permission.Read : Permission.None) | (p1[4] ? Permission.Write : Permission.None) | (p1[5] ? Permission.Execute : Permission.None),
                other: (p1[6] ? Permission.Read : Permission.None) | (p1[7] ? Permission.Write : Permission.None) | (p1[8] ? Permission.Execute : Permission.None)
            };
        }
        else {
            if (p1.length === 3) {
                // octal string
                if (isNaN(parseInt(p1, 8))) {
                    throw Error("[Permissions] Invalid octal input: " + p1);
                }
                return {
                    owner: parseInt(p1[0]),
                    group: parseInt(p1[1]),
                    other: parseInt(p1[2])
                };
            }
            else if (p1.length === 9) {
                //binary string
                return Permissions(convertToBoolArray(p1.split("")));
            }
            else if (p1.length === 10 && p1.indexOf("-") === 0) {
                // string with leading "-"
                return Permissions(convertToBoolArray(p1.slice(1).split(""), function (s) { return s === "-" ? false : true; }));
            }
            else {
                throw Error("[Permissions] Unsupported input: " + p1);
            }
        }
    }
    RS.Permissions = Permissions;
    (function (Permissions) {
        /** "724" */
        function toString(permissions) {
            return "" + permissions.owner + permissions.group + permissions.other;
        }
        Permissions.toString = toString;
    })(Permissions = RS.Permissions || (RS.Permissions = {}));
})(RS || (RS = {}));
//# sourceMappingURL=gulplib.js.map