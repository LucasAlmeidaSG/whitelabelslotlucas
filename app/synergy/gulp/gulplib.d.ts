/// <reference types="node" />
declare namespace RS {
    /**
     * Build config, as specified in root "config.json".
     */
    interface Config extends Config.Base {
        tasks?: Util.Map<Config.Task>;
    }
    namespace Config {
        interface Base {
            rootModule: string;
            extraModules: string[];
            /** Environment args associated with this config. */
            envArgs: Util.Map<string>;
            moduleConfigs: Util.Map<{
                [key: string]: any;
            }>;
            assemblePath: string;
            /** Whether or not source maps should be generated. */
            sourceMap: boolean;
            /** Build-time constants. */
            defines: Util.Map<string | boolean>;
        }
        interface Task extends Base {
            requires?: string[];
            after?: string[];
        }
        let activeConfig: Base | null;
        /**
         * Retrieves or loads the config.
         */
        function get(): Promise<Config>;
    }
}
/** Contains functions for reading strings as various types of value. */
declare namespace RS.Read {
    /**
     * Parses the given string as an boolean.
     * @return a boolean (or null if defaultValue = null and value is not a boolean)
     */
    function boolean(value: string, defaultVal?: boolean | null): boolean | null;
    /**
     * Parses the given string as an integer.
     * @return a number (or null if defaultValue = null and value is not an integer)
     */
    function integer(value: string, defaultVal?: number | null): number | null;
    /**
     * Parses the given string as a number.
     * @return a number (or null if defaultValue = null and value is not a number)
     */
    function number(value: string, defaultVal?: number | null): number | null;
    /**
     * Parses the given string as an object.
     * @return an object or null
     */
    function object(value: string): object | null;
    /**
     * Parses the given string as an array of a specific type.
     * @return an array of T and null values
     */
    function arrayOf<T>(value: string, innerReader: (value: string) => (T | null), delimiter?: string): T[];
}
declare namespace RS {
    /**
     * Represents an argument passed via an environment variable, e.g. "VAR=1 gulp build".
     */
    class EnvArg {
        readonly varName: string;
        readonly defaultValue: string;
        readonly trim: boolean;
        /**
         * @param trim Whether or not to trim the value string.
         */
        constructor(varName: string, defaultValue: string, trim?: boolean);
        set value(value: string);
        get value(): string;
        get isSet(): boolean;
    }
    /**
     * Represents a typed argument passed via an environment variable.
     */
    abstract class TypedEnvArg<T> {
        protected abstract readonly typeName: string;
        private readonly _envArg;
        constructor(varName: string, defaultValue: T);
        get varName(): string;
        get defaultValue(): T;
        get isSet(): boolean;
        get value(): T;
        set value(value: T);
        protected abstract stringToType(value: string): T | null;
        protected abstract typeToString(value: T): string;
        private parseString;
    }
    /**
     * Represents a boolean argument passed via an environment variable, e.g. "BOOL=1 gulp build".
     * Truthy values are: "true", "1", "y", "yes"
     * Falsy values are: "false", "0", "n", "no"
     * (Case insensitive.)
     */
    class BooleanEnvArg extends TypedEnvArg<boolean> {
        protected get typeName(): string;
        protected typeToString(value: boolean): string;
        protected stringToType(value: string): boolean;
    }
    /**
     * Represents an integer argument passed via an environment variable, e.g. "INT=12 gulp build".
     */
    class IntegerEnvArg extends TypedEnvArg<number> {
        protected get typeName(): string;
        protected typeToString(value: number): string;
        protected stringToType(value: string): number;
    }
}
declare namespace RS.EnvArgs {
    const debugMode: BooleanEnvArg;
}
declare namespace RS.Log {
    /**
     * Pushes the current context to the stack and sets the specified context as active.
     * @param context
     */
    function pushContext(context: string): void;
    /**
     * Sets the specified context as inactive and restores the previous context from the stack.
     * @param context
    **/
    function popContext(context: string): void;
    /**
     * Logs an informative message to the console.
     * @param message
     */
    function info(message: string, contextOverride?: string): void;
    /**
     * Logs an debug message to the console.
     * @param message
     */
    function debug(message: string, contextOverride?: string): void;
    /**
     * Logs a warning message to the console.
     * @param message
     */
    function warn(message: string, contextOverride?: string): void;
    /**
     * Logs an error message to the console.
     * @param message
     */
    function error(message: string, contextOverride?: string): void;
}
/** Contains common type guards. */
declare namespace RS.Is {
    /**
     * Gets if the specified value is a number.
     * @param value The value to test
     */
    function number(value: any): value is number;
    /**
     * Gets if the specified value is an integer.
     * Does not type-guard, as integer type currently impossible.
     * @param value The value to test
     */
    function integer(value: any): boolean;
    /**
     * Gets if the specified value is a string.
     * @param value The value to test
     */
    function string(value: any): value is string;
    /**
     * Gets if the specified value is an array.
     * @param value The value to test
     */
    function array(value: any): value is any[] | ReadonlyArray<any>;
    /**
     * Gets if the specified value is an array of a specific type.
     * @param value The value to test
     */
    function arrayOf<T>(value: any, innerTester: (value: any) => value is T): value is T[] | ReadonlyArray<T>;
    /**
     * Gets if the specified value is an object but NOT an array.
     * @param value The value to test
     */
    function object(value: any): value is {
        [key: string]: any;
    };
    /**
     * Gets if the specified value is a function.
     * @param value The value to test
     */
    function func(value: any): value is Function;
    /**
     * Gets if the specified value is a boolean.
     * @param value The value to test
     */
    function boolean(value: any): value is boolean;
    /**
     * Gets if the specified value is a primitive.
     * @param value
     */
    function primitive(value: any): value is (boolean | number | string);
    /**
     * Gets if the specified value is a non-primitive.
     * @param value
     */
    function nonPrimitive(value: any): value is object;
}
declare namespace RS {
    /**
     * Returns the given value if the type-guard matches, otherwise returns null.
     */
    function as<T>(value: any, guard: (value: any) => value is T, defaultValue?: T | null): T | null;
    function as<T>(value: any, guard: (value: any) => boolean, defaultValue?: T | null): T | null;
}
/** Contains file path manipulation functionality. */
declare namespace RS.Path {
    /** Returns the file part of a path. E.g. "my/test/file.txt" => "file.txt". */
    function baseName(path: string): string;
    /** Returns the directory part of a path. E.g. "my/test/file.txt" => "my/test". */
    function directoryName(path: string): string;
    /** Returns the sub-extension of a path (including the dot and extension). E.g. "script.d.ts" => ".d.ts" */
    function subExtension(path: string): string;
    /** Returns the extension of a path (including the dot). E.g. "my/test/file.txt" => ".txt" */
    function extension(path: string): string;
    /** Combines one or more parts of a path. */
    function combine(...paths: string[]): string;
    /** Resolves any ".." and "." parts of a path. */
    function normalise(path: string): string;
    /** Finds the relative path from "from" to "to", based on the current working directory. */
    function relative(from: string, to: string): string;
    /** Resolves a sequence of paths or path segments into an absolute path. */
    function resolve(...paths: string[]): string;
    /** Splits a path into segments. */
    function split(path: string): string[];
    /** Changes the extension of a path. E.g. "my/test/file.txt" => "my/test/file.json". New extension should include the dot. */
    function replaceExtension(path: string, newExt: string): string;
    /** Converts the given path to be suitable for use on any main platform - strips it of whitespace, etc. */
    function sanitise(path: string): string;
    /** The path seperator (e.g. "\\" on windows, "/" on osx/linux) */
    const seperator: string;
}
declare namespace RS.Build {
    /** Executor function for a task. */
    type TaskCallback = () => PromiseLike<void>;
    /** Settings for a task. */
    interface TaskSettings {
        /** Gulp name of the task. */
        name: string;
        /** Any tasks that MUST be executed before this one. */
        requires?: Task[];
        /** Any tasks that SHOULD be executed before this one but ONLY if required by another. */
        after?: Task[];
        /** Description of task functionality and usage */
        description?: string;
    }
    /** Represents a task. */
    interface Task {
        settings: TaskSettings;
        callback?: TaskCallback;
        required: boolean;
        dependencies: Task[];
    }
    /** Declares a task with the specified settings and callback. */
    function task(settings: TaskSettings, callback: TaskCallback): Task;
    /** Declares a task with default settings and specified callback. */
    function task(callback: TaskCallback): Task;
    /** Declares a task with the specified settings and no callback. */
    function task(settings: TaskSettings): Task;
    /** Declares a task to be run before any others. Can only be declared once. */
    function initTask(callback: TaskCallback): Task;
    /** Identify requested gulp tasks. */
    function identifyTasks(): string[];
    /** Exports the specified task to gulp. */
    function exportTask(task: Task): void;
    function getTask(name: string): Task;
    function getTasks(): ReadonlyArray<Task>;
    /** Specifies that the task is required and must run. */
    function requireTask(task: Task): void;
    /** Exports all required tasks. */
    function exportAllTasks(): void;
}
declare namespace RS.Util {
    /**
     * Gets the chain of classes that the specified class derives.
    **/
    function getInheritanceChain(obj: Function, includeSelf?: boolean): Function[];
    type Map<T> = {
        [key: string]: T;
    };
    /**
     * Escapes all special regular expression characters in the string so that it may be matched in a RegExp.
     *
     * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters.
     */
    function escapeRegExpChars(str: string): string;
    /** Replaces all occurrences of needle in haystack with replacement. */
    function replaceAll(haystack: string, needle: string, replacement: string): string;
    function replaceAll(haystack: string, needles: Map<string>): string;
    /**
     * Splits the specified string into an array of lines.
     * @param str
     */
    function splitLines(str: string): string[];
    /** Copes one error's stack trace into another. */
    function copyErrorStack(source: Error, dest: Error): void;
    /**
     * Copies a block of bytes from one buffer to another.
     * @param from
     * @param fromOffset
     * @param to
     * @param toOffset
     * @param length
     */
    function copyBytes(from: ArrayBuffer, fromOffset: number, to: ArrayBuffer, toOffset: number, length: number): void;
    /**
     * Formats a size in bytes into a human friendly string (e.g. 827567 => 828 KiB)
     * @param byteSize
     */
    function formatSize(byteSize: number): string;
}
/** RSCore Next module system. */
declare namespace RS.Module {
    /** Describes a single module. */
    interface Info {
        title: string;
        primarynamespace?: string;
        name?: string;
        description: string;
        dependencies: string[];
        testdependencies?: string[];
        nodedependencies?: Util.Map<string> & {
            build: Util.Map<string>;
        };
        bowerdependencies?: Util.Map<string>;
        definitions?: Util.Map<string>;
        thirdparty?: Util.Map<{
            src: string;
            srcmap?: string;
        }>;
        implements?: string;
        important?: boolean;
    }
    /**
     * Validates the specified module info. Returns null for success, or a string containing the error.
     * @param info
     */
    function validateInfo(info: Info): string | null;
}
declare namespace RS {
    /** A list of items backed by a standard array. */
    class List<T> {
        protected _data: T[];
        /** Gets the backing array for this list. */
        get data(): ReadonlyArray<T>;
        /** Gets a copy of the backing array for this list. */
        get dataCopy(): T[];
        /** Gets the number of items in this list. */
        get length(): number;
        constructor(existing?: ArrayLike<T>);
        /** Returns a new list containing unique items from both lists. */
        static union<T>(a: List<T>, b: List<T>): List<T>;
        /** Returns a new list containing only items found in both lists. */
        static intersect<T>(a: List<T>, b: List<T>): List<T>;
        /** Returns a new list containing only items unique to one of the lists. */
        static difference<T>(a: List<T>, b: List<T>): List<T>;
        /** Adds an item to the end of this list. Returns the index of the added item. */
        add(item: T): number;
        /** Adds all items from the specified array-like to this list. */
        addRange(from: ArrayLike<T>, avoidDuplicates?: boolean): void;
        /** Adds all items from the specified list to this list. */
        addRange(from: List<T>, avoidDuplicates?: boolean): void;
        /** Inserts an item at the specified index. */
        insert(item: T, index: number): void;
        /** Finds the index of the given item in this list. Returns -1 if item is not found. */
        indexOf(item: T): number;
        /** Gets if the specified item can be found in this list. */
        contains(item: T): boolean;
        /** Removes the specified item from this list. Returns false if item wasn't found. */
        remove(item: T): boolean;
        /** Removes all items from the specified array-like from this list. */
        removeRange(from: ArrayLike<T>): void;
        /** Removes all items from the specified list from this list. */
        removeRange(from: List<T>): void;
        /** Removes any duplicate items from this list. */
        removeDuplicates(): void;
        /** Removes the item at the specified index from this list. */
        removeAt(index: number): void;
        /** Removes ALL items from this list. */
        clear(): void;
        /** Transforms each item in this list into a new form and returns a new list of the transformed items. */
        map<T2>(functor: (this: void, item: T, index: number) => T2): List<T2>;
        reduce(functor: (this: void, previousValue: T, currentValue: T, index: number) => T): T;
        reduce<T2>(functor: (this: void, previousValue: T2, currentValue: T, index: number) => T2, initialValue: T2): T2;
        /** Gets a new list containing only items within this list that passes the specified predicate. */
        filter(predicate: (this: void, item: T) => boolean): List<T>;
        /** Gets if all items in this list pass the specified predicate. */
        all(predicate: (this: void, item: T) => boolean): boolean;
        /** Finds a single item matching the specified predicate, or null if none found. */
        find(predicate: (item: T) => boolean): T | null;
        /** Gets an item from this list. */
        get(index: number): T;
        /** Sets an item in this list. */
        set(index: number, item: T): void;
        /** Moves the item at fromIndex to toIndex. */
        move(fromIndex: number, toIndex: number): void;
        /** Moves the item to newIndex. Returns false if the item wasn't found in this list. */
        setItemIndex(item: T, newIndex: number): boolean;
        /** Sorts this list. */
        sort(comparer: (a: T, b: T) => number): void;
        /**
         * Sorts this list, ensuring that all items come before or after the set of items returned back from their respective predicates.
         * @param beforePredicate
         * @param afterPredicate
         */
        resolveOrder(beforePredicate: (item: T) => ArrayLike<T>, afterPredicate: (item: T) => ArrayLike<T>): void;
        toString(): string;
    }
}
declare namespace RS {
    /** Timer used for profiling. */
    class Timer {
        /** Gets the current timestamp. */
        static get now(): number;
        protected _elapsed: number;
        protected _running: boolean;
        protected _last: number;
        /** Gets if this timer is running. */
        get running(): boolean;
        /** Gets how long this timer has been running for (ms). */
        get elapsed(): number;
        /** Starts this timer. */
        start(): void;
        /** Stops this timer and resets it. */
        stop(): void;
        /** Pauses this timer. */
        pause(): void;
        /** Consumes any pending time into elapsed. */
        protected update(): void;
        /** Gets a human-friendly representaton of this timer's total elapsed time in seconds. */
        toString(): string;
    }
}
declare namespace RS.Module.CompilationDependencies {
    /**
     * Adds dependencies on the gulp file to the specified compilation unit.
     * @param unit
     */
    function addGulp(unit: Build.CompilationUnit, module: Base): void;
    function addUnit(unit: Build.CompilationUnit, depUnit: Build.CompilationUnit): void;
    /**
     * Adds dependencies on the compilation unit of "unitName" on a single module to the specified compilation unit.
     * @param unit
     * @param module
     */
    function addModule(unit: Build.CompilationUnit, module: Base, unitName: string): void;
    /**
     * Adds dependencies on all compilation units of "unitName" on all dependencies of a module to the specified compilation unit.
     * @param unit
     * @param module
     */
    function addModules(unit: Build.CompilationUnit, module: Base, unitName: string): void;
    /**
     * Adds dependencies on all node module dependencies of a module.
     * Optionally supports sub-maps within the node module dependencies, e.g. "build".
     * @param unit
     * @param module
     * @param subName
     */
    function addNPM(unit: Build.CompilationUnit, module: Base, subName?: string): void;
    /**
     * Adds dependencies on all bower package dependencies of a module.
     * @param unit
     * @param module
     */
    function addBower(unit: Build.CompilationUnit, module: Base): void;
    /**
     * Adds dependencies on all third party dependencies of a module.
     * @param unit
     * @param module
     */
    function addThirdParty(unit: Build.CompilationUnit, module: Base): void;
}
declare namespace RS.Module {
    interface TaskInfo {
        function: Function;
        taskSettings: Build.TaskSettings;
    }
    /** Names of standard compilation units. */
    enum CompilationUnits {
        Source = "src",
        BuildSource = "buildsrc"
    }
    /**
     * Represents a module.
     */
    class Base {
        /** Gets all tasks exported by the base module. */
        static get exportedTasks(): TaskInfo[];
        protected _info: Info;
        protected _name: string;
        protected _path: string;
        protected _inited: boolean;
        protected _dependencies: Base[];
        protected _checksumDB: Checksum.DB;
        protected _buildLoaded: boolean;
        protected _important: number;
        protected _compilationUnits: Util.Map<Build.CompilationUnit>;
        /** Gets the moduleinfo for this module. */
        get info(): Info;
        /** Gets the name of this module. */
        get name(): string;
        /** Gets the path of folder that holds this module. */
        get path(): string;
        /** Gets if this module has been initialised. */
        get initialised(): boolean;
        /** Gets the checksum database for this module. */
        get checksumDB(): Checksum.DB;
        /** Gets all dependencies of this module. */
        get dependencies(): Base[];
        /** Gets if the build component of this module has been loaded. */
        get buildLoaded(): boolean;
        /**
         * Gets or sets the importance of this module.
         * This determines how to sort it within any dependency sets that include it.
         * Higher values means the module will be "floated" as high as possible within the set.
         */
        get important(): number;
        set important(value: number);
        constructor(name: string, info: Info, path: string);
        /**
         * Gets a named compilation unit for this module.
         * @param name
         */
        getCompilationUnit(name: string): Build.CompilationUnit | null;
        /**
         * Returns whether or not this module has code at the given path.
         * Used to check whether or not this module should have a particular compilation unit.
         * @param srcPath Path to the folder that holds tsconfig.json, relative to the module's root path.
         */
        hasCode(srcPath: string): Promise<boolean>;
        /**
         * Adds a new named compilation unit to this module.
         * Should only be called if the module actually has code for the given compilation unit.
         * Use Module.hasCode(srcPath) to check if this is the case.
         * @param name
         * @param srcPath Path to the folder that holds tsconfig.json, relative to the module's root path.
         */
        addCompilationUnit(name: string, srcPath: string, defaultTSConfig?: Readonly<TSConfig>): Build.CompilationUnit;
        /**
         * Initialises this module. Async.
         */
        init(dependencies: Base[]): Promise<void>;
        /**
         * Gets the checksum for the specified sub-directory.
         * @param subPath
         */
        getSourceChecksum(subPath: string): Promise<string>;
        /**
         * Gathers a sorted list of all dependent modules of this module.
         * Travels up the dependency hierarchy.
         */
        gatherDependencySet(): List<Base>;
        /**
         * Exports all tasks for this module to the task system.
         */
        exportTasks(): void;
        /**
         * Compiles the build component of this module, if it has one.
         */
        compileBuild(): Promise<BuildStage.Result>;
        /**
         * Loads the build component of this module, if it has one.
         */
        loadBuild(): Promise<boolean>;
        /**
         * Saves the checksum database to file.
         */
        saveChecksums(): Promise<void>;
        protected setupSourceCompilationUnit(): void;
        protected setupBuildSourceCompilationUnit(): void;
        /**
         * Gets if the specified path should be excluded from the checksum produced by getSourceChecksum.
         * Useful for excluding generated code files from change detection.
         * @param filename
         */
        protected shouldExcludeFromSourceChecksum(filename: string): boolean;
        protected gatherDependencySetInternal(set: List<Base>): void;
    }
}
declare namespace RS.FileIO {
    interface FileOrFolder {
        type: "file" | "folder";
        name: string;
    }
    interface FileStats {
        dev: number;
        mode: number;
        nlink: number;
        uid: number;
        gid: number;
        rdev: number;
        blksize: number;
        ino: number;
        size: number;
        blocks: number;
        atimeMs: number;
        mtimeMs: number;
        ctimeMs: number;
        birthtimeMs: number;
        atime: Date;
        mtime: Date;
        ctime: Date;
        birthtime: Date;
    }
    /** https://nodejs.org/api/fs.html#fs_fs_writefile_file_data_options_callback */
    interface WriteOptions {
        /** File Encoding - default 'utf8' */
        encoding?: string;
        /** Permissions - default 0o666 / -rw-rw-rw- / read/write for all users */
        mode?: Permissions;
        /** Flag - default 'w' https://nodejs.org/api/fs.html#fs_file_system_flags */
        flag?: string;
    }
    interface IFileSystem {
        /**
         * Returns the true path of the given file, accounting for casing.
         */
        findPath(path: string): Promise<string>;
        /**
         * Deletes the specified path.
         */
        deletePath(path: string): PromiseLike<void>;
        /**
         * Creates the specified path.
         */
        createPath(path: string): PromiseLike<string>;
        /**
         * Copies a file from one location to another.
         */
        copyFile(srcPath: string, dstPath: string): PromiseLike<void>;
        /**
         * Copies multiple files from one location to another.
         */
        copyFiles(srcPaths: string[], dstPaths: string[]): PromiseLike<void>;
        /**
         * Reads a text file.
         */
        readFile(path: string): PromiseLike<string>;
        /**
         * Reads a text file.
         */
        readFile(path: string, binary: false): PromiseLike<string>;
        /**
         * Reads a binary file.
         */
        readFile(path: string, binary: true): PromiseLike<Buffer>;
        /**
         * Reads a text or binary file.
         */
        readFile(path: string, binary: boolean): PromiseLike<string | Buffer>;
        /**
         * Streams a file into a function that accepts buffers.
         */
        streamFile(path: string, targetFunc: (buffer: Buffer) => void): PromiseLike<void>;
        /**
         * Writes a text or binary file.
         */
        writeFile(path: string, content: string | Buffer | Uint8Array, options?: WriteOptions): PromiseLike<void>;
        /**
         * Writes multiple text or binary files.
         */
        writeFiles(paths: string[], contents: (string | Buffer | Uint8Array)[], options?: WriteOptions): PromiseLike<void>;
        /**
         * Tests if the specified path exists.
         * NOTE: returns true if the given path exists and points to a folder.
         * @param path
         */
        fileExists(path: string): PromiseLike<boolean>;
        /**
         * Tests if the specified path is a folder.
         * NOTE: may or may not throw if the path does not exist.
         * @param path
         */
        isFolder(path: string): PromiseLike<boolean>;
        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: false): PromiseLike<string[]>;
        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: true): PromiseLike<FileOrFolder[]>;
        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: boolean): PromiseLike<string[] | FileOrFolder[]>;
        /**
         * Finds all files within the specified directory and sub-directories.
         * @param path
         */
        readFiles(path: string): PromiseLike<string[]>;
        /**
         * Gets statistics for the specified file.
         * @param path
         */
        getFileStats(path: string): PromiseLike<FileStats>;
    }
}
/** Contains console/terminal/bash command functionality. */
declare namespace RS.Command {
    /**
     * Runs a single command. Async.
     * @param cmd
     * @param args
     * @param cwd
     * @param logOutput
     */
    function run(cmd: string, args: string[], cwd?: string, logOutput?: boolean): PromiseLike<string>;
    /**
     * Runs a batch of commands in sequence. Async.
     * @param cmds
     * @param cwd
     */
    function runMultiple(cmds: string[], cwd?: string): PromiseLike<void>;
    /**
     * Tests if the specified command is present. Async.
     * @param path
     */
    function exists(cmd: string): Promise<boolean>;
}
declare namespace RS {
    function Profile(name: string): MethodDecorator;
    namespace Profile {
        function enter(name: string): void;
        function exit(name: string): void;
        function dump(): void;
        function reset(): void;
    }
    namespace EnvArgs {
        /** Whether or not performance profiling information should be dumped to the console. */
        const profile: BooleanEnvArg;
    }
}
/** Contains file system manipulation functionality. */
declare namespace RS.FileIO {
    class RawFileSystem implements IFileSystem {
        readonly settings: RawFileSystem.Settings;
        constructor(settings: RawFileSystem.Settings);
        /** Returns the true path of the given file, accounting for casing. */
        findPath(path: string): Promise<string>;
        isFolder(path: string): PromiseLike<boolean>;
        /** Deletes the specified path. Async. */
        deletePath(path: string): PromiseLike<void>;
        /** Creates the specified path. Async. */
        createPath(path: string): PromiseLike<string>;
        /**
         * Copies a file from one location to another.
         */
        copyFile(srcPath: string, dstPath: string): PromiseLike<void>;
        /**
         * Copies multiple files from one location to another.
         * @param srcPaths
         * @param dstPaths
         */
        copyFiles(srcPaths: string[], dstPaths: string[]): Promise<void>;
        /**
         * Reads a text file.
         */
        readFile(path: string): PromiseLike<string>;
        /**
         * Reads a text file.
         */
        readFile(path: string, binary: false): PromiseLike<string>;
        /**
         * Reads a binary file.
         */
        readFile(path: string, binary: true): PromiseLike<Buffer>;
        /**
         * Reads a text or binary file.
         */
        readFile(path: string, binary: boolean): Promise<string | Buffer>;
        /**
         * Streams a file into a function that accepts buffers.
         */
        streamFile(path: string, targetFunc: (buffer: Buffer) => void): PromiseLike<void>;
        /**
         * Writes multiple text or binary files.
         */
        writeFiles(paths: string[], contents: (string | Buffer | Uint8Array)[], options?: WriteOptions): Promise<void>;
        /**
         * Writes a text or binary file.
         */
        writeFile(path: string, content: string | Buffer | Uint8Array, options?: WriteOptions): PromiseLike<void>;
        /**
         * Tests if the specified file exists.
         * @param path
         */
        fileExists(path: string): PromiseLike<boolean>;
        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: false): PromiseLike<string[]>;
        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: true): PromiseLike<FileOrFolder[]>;
        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: boolean): Promise<string[] | FileOrFolder[]>;
        /**
         * Finds all files within the specified directory and sub-directories. Async.
         * @param path
         */
        readFiles(path: string): Promise<string[]>;
        /**
         * Gets statistics for the specified file.
         * @param path
         */
        getFileStats(path: string): Promise<FileStats>;
        protected removeCaseConflicts(path: string): Promise<void>;
    }
    namespace RawFileSystem {
        interface Settings {
            excluded: string[];
            debugMode: boolean;
        }
    }
}
declare namespace RS.FileIO {
    class FileSystemCache {
        protected _nodes: Util.Map<FileSystemCache.Node>;
        protected _rootPath: string;
        protected _baseFileSys: IFileSystem;
        /**
         * Gets the paths of all cached nodes.
         */
        get paths(): string[];
        constructor(rootPath: string, baseSys: IFileSystem);
        rebuild(): Promise<void>;
        protected buildDirectory(path: string): Promise<void>;
        /**
         * Gets if the specified path is contained within this cache.
         * Does not check if the path actually exists, only that it sits in or below the root path.
         * @param path
         */
        contains(path: string): boolean;
        /**
         * Retrieves a cached node.
         * @param path
         */
        get(path: string): FileSystemCache.Node | null;
        /**
         * Sets a cached node.
         * Returns if successful or not.
         * @param path
         * @param node
         */
        set(path: string, node: FileSystemCache.Node | null): boolean;
        /**
         * Finds all child nodes under the specified path.
         * @param rootPath
         * @param recurse
         */
        search(path: string, recurse: boolean): string[];
    }
    namespace FileSystemCache {
        enum NodeType {
            File = 0,
            Directory = 1
        }
        interface BaseNode<T extends NodeType> {
            type: T;
        }
        interface DirectoryNode extends BaseNode<NodeType.Directory> {
        }
        interface FileNode extends BaseNode<NodeType.File> {
            content: string | Buffer | Uint8Array | null;
            stats?: FileStats;
        }
        type Node = DirectoryNode | FileNode;
    }
}
declare namespace RS.FileIO {
    class CachedFileSystem implements IFileSystem {
        readonly settings: CachedFileSystem.Settings;
        protected _baseSys: IFileSystem;
        protected _cache: FileSystemCache;
        get cache(): FileSystemCache;
        constructor(settings: CachedFileSystem.Settings);
        /** Returns the true path of the given file, accounting for casing. */
        findPath(path: string): Promise<string>;
        isFolder(path: string): Promise<boolean>;
        protected cacheMiss(): void;
        /**
         * Rebuilds the cache. Async.
         */
        rebuildCache(): Promise<void>;
        /**
         * Gets if the specified path falls within our cache.
         * @param path
         */
        protected pathIsWithinCache(path: string): boolean;
        /**
         * Deletes the specified path. Async.
         */
        deletePath(path: string): Promise<void>;
        /**
         * Creates the specified path. Async.
         */
        createPath(path: string): Promise<string>;
        /**
         * Copies a file from one location to another.
         */
        copyFile(srcPath: string, dstPath: string): Promise<void>;
        /**
         * Copies multiple files from one location to another.
         * @param srcPaths
         * @param dstPaths
         */
        copyFiles(srcPaths: string[], dstPaths: string[]): Promise<void>;
        /**
         * Reads a text file.
         */
        readFile(path: string): Promise<string>;
        /**
         * Reads a text file.
         */
        readFile(path: string, binary: false): Promise<string>;
        /**
         * Reads a binary file.
         */
        readFile(path: string, binary: true): Promise<Buffer>;
        /**
         * Reads a text or binary file.
         */
        readFile(path: string, binary: boolean): Promise<string | Buffer>;
        /**
         * Streams a file into a function that accepts buffers.
         */
        streamFile(path: string, targetFunc: (buffer: Buffer) => void): Promise<void>;
        /**
         * Writes a text or binary file.
         */
        writeFile(path: string, content: string | Buffer | Uint8Array, options?: WriteOptions): Promise<void>;
        /**
         * Writes multiple text or binary files.
         */
        writeFiles(paths: string[], contents: (string | Buffer | Uint8Array)[], options?: WriteOptions): Promise<void>;
        /**
         * Tests if the specified file exists.
         * @param path
         */
        fileExists(path: string): Promise<boolean>;
        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: false): Promise<string[]>;
        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: true): Promise<FileOrFolder[]>;
        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: boolean): Promise<string[] | FileOrFolder[]>;
        /**
         * Finds all files within the specified directory and sub-directories.
         * Returned paths are relative to the current working directory.
         * Async.
         * @param path
         */
        readFiles(path: string): Promise<string[]>;
        /**
         * Gets statistics for the specified file.
         * @param path
         */
        getFileStats(path: string): Promise<FileStats>;
        protected getNode(path: string): FileSystemCache.Node | null;
    }
    namespace CachedFileSystem {
        interface Settings extends RawFileSystem.Settings {
            cacheRoot: string;
            caseSensitive: boolean;
        }
    }
}
declare namespace RS {
    const FileSystem: FileIO.CachedFileSystem;
}
declare namespace RS.Module {
    /**
     * Finds all modules underneath the specified path. Recurses down sub-directories. Async.
     * @param path
     */
    function findModules(path: string): Promise<string[]>;
    /**
     * Determines the name of a module from the location of it's module info.
     * @param path
     */
    function determineNameFromPath(path: string): string;
    /**
     * Formats a module name.
     * @param name
     */
    function formatName(name: string): string;
    /**
     * Gets a module by name.
     * @param name
     */
    function getModule(name: string): Base | null;
    /**
     * Gets all loaded modules.
     * @param sorted If true, sorts the modules by dependency (so module B depending on module A will always come afterwards in the array)
     */
    function getAll(sorted?: boolean): Base[];
    /**
     * Finds and loads all modules.
     */
    function loadModules(): Promise<void>;
}
declare namespace RS.AssembleStage {
    /**
     * Settings for assembly.
     */
    interface AssembleSettings {
        outputDir: string;
    }
}
declare namespace RS.AssembleStage {
    /**
     * Encapsulates a generic assemble stage.
     */
    abstract class Base {
        readonly name?: string | null;
        /**
         * Executes this assemble stage.
         */
        execute(settings: AssembleSettings, moduleList: List<Module.Base>): Promise<void>;
        toString(): any;
        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected abstract executeModule(settings: AssembleSettings, module: Module.Base): PromiseLike<void>;
    }
}
declare namespace RS.AssembleStage {
    /** Settings for an assemble stage. */
    interface Settings {
        /** Any stages that should come before this one. */
        before?: Base[];
        /** Any stages that should come after this one. */
        after?: Base[];
        /** Only use this stage if explicitly requested. */
        explicit?: boolean;
        /** This should be the last stage to execute. Does NOT take priority over "before" and "after". */
        last?: boolean;
    }
    /**
     * Registers an assemble stage with the system.
     * @param settings
     * @param stage
     */
    function register(settings: Settings, stage: Base): void;
    /**
     * Gets all assemble stages in order of execution.
     */
    function getStages(): Base[];
}
declare namespace RS.BuildStage {
    /**
     * Build stage execution result.
     */
    interface Result {
        workDone: boolean;
        errorCount: number;
    }
    /**
     * Encapsulates a generic build stage.
     */
    abstract class Base {
        /**
         * Gets if errors encountered when building a module are fatal and should end the whole process.
         * If this is false, other modules may continue to be compiled after an error has been encountered.
         */
        get errorsAreFatal(): boolean;
        /**
         * Gets if the executeModule function should be called in parallel for all modules.
         * Ddependencies are still considered and processed serially.
         */
        get runInParallel(): boolean;
        readonly name?: string | null;
        /**
         * Executes this build stage safely, catching and reporting any errors.
         */
        executeSafe(moduleList: List<Module.Base>): Promise<Result>;
        /**
         * Executes this build stage.
         */
        execute(moduleList: List<Module.Base>): Promise<Result>;
        toString(): any;
        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected abstract executeModule(module: Module.Base): PromiseLike<Result>;
    }
}
declare namespace RS.BuildStage {
    /** Settings for a build stage. */
    interface Settings {
        /** Any stages that should come before this one. */
        before?: Base[];
        /** Any stages that should come after this one. */
        after?: Base[];
        /** Only use this stage if explicitly requested. */
        explicit?: boolean;
    }
    /**
     * Registers a build stage with the system.
     * @param settings
     * @param stage
     */
    function register(settings: Settings, stage: Base): void;
    /**
     * Gets all build stages in order of execution.
     */
    function getStages(): Base[];
}
declare namespace RS.Tasks {
    const clean: Build.Task;
}
declare namespace RS.Tasks {
    const build: Build.Task;
    const buildOnly: Build.Task;
    const buildAll: Build.Task;
}
declare namespace RS.Tasks {
    const assemble: Build.Task;
}
declare namespace RS {
    /** Path to gulplib.js. */
    const __gulplib: string;
}
declare namespace RS.Tasks {
    const init: Build.Task;
}
declare namespace RS.TypeScript {
    const helpers = "var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\nvar __generator = (this && this.__generator) || function (thisArg, body) {\n    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;\n    return g = { next: verb(0), \"throw\": verb(1), \"return\": verb(2) }, typeof Symbol === \"function\" && (g[Symbol.iterator] = function() { return this; }), g;\n    function verb(n) { return function (v) { return step([n, v]); }; }\n    function step(op) {\n        if (f) throw new TypeError(\"Generator is already executing.\");\n        while (_) try {\n            if (f = 1, y && (t = op[0] & 2 ? y[\"return\"] : op[0] ? y[\"throw\"] || ((t = y[\"return\"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;\n            if (y = 0, t) op = [op[0] & 2, t.value];\n            switch (op[0]) {\n                case 0: case 1: t = op; break;\n                case 4: _.label++; return { value: op[1], done: false };\n                case 5: _.label++; y = op[1]; op = [0]; continue;\n                case 7: op = _.ops.pop(); _.trys.pop(); continue;\n                default:\n                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }\n                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }\n                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }\n                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }\n                    if (t[2]) _.ops.pop();\n                    _.trys.pop(); continue;\n            }\n            op = body.call(thisArg, _);\n        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }\n        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };\n    }\n};\nvar __assign = (this && this.__assign) || Object.assign || function(t) {\n    for (var s, i = 1, n = arguments.length; i < n; i++) {\n        s = arguments[i];\n        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))\n            t[p] = s[p];\n    }\n    return t;\n};\nvar __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {\n    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;\n    if (typeof Reflect === \"object\" && typeof Reflect.decorate === \"function\") r = Reflect.decorate(decorators, target, key, desc);\n    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;\n    return c > 3 && r && Object.defineProperty(target, key, r), r;\n};\nvar __extends = (this && this.__extends) || (function () {\n    var extendStatics = Object.setPrototypeOf ||\n        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\nvar __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {\n    if (Object.defineProperty) { Object.defineProperty(cooked, \"raw\", { value: raw }); } else { cooked.raw = raw; }\n    return cooked;\n};\nvar __multiDecorate = (this && this.__multiDecorate) || function(map) {\n    var keys = Object.keys(map);\n    for (var i = 0; i < keys.length; i++) {\n        var key = keys[i];\n        var obj = map[key];\n        __decorate(obj.decorators, obj.target, key, obj.desc);\n    }\n};\nvar __spreadArrays = (this && this.__spreadArrays) || function() {\n    for (var s = 0, i = 0, il = arguments.length; i < il; i++)\n        s += arguments[i].length;\n    for (var r = Array(s), k = 0, i = 0; i < il; i++)\n        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++,\n        k++)\n            r[k] = a[j];\n    return r;\n};\nvar __param = (this && this.__param) || function (paramIndex, decorator) {\n    return function (target, key) { decorator(target, key, paramIndex); }\n};";
}
declare namespace RS.Assembler {
    const sourcemapMode: EnvArg;
    interface CodeSource {
        name: string;
        dtsContent?: string;
        content: string;
        sourceMap: string | null;
        sourceMapDir: string | null;
    }
    interface Settings {
        withHelpers: boolean;
        withSourcemaps: boolean;
        replacements?: RS.Util.Map<string>;
        minify: boolean;
    }
    /**
     * Generates an identity sourcemap from the specified source.
     * @param sourceText source code
     * @param sourcePath source file name
     */
    function generateIdentitySourcemap(sourceText: string, sourcePath: string): string;
    /**
     * Attempts to resolve an identity sourcemap from the specified source and path, using the given module for cache.
     * @param sourceText source code
     * @param sourcePath source file name
     */
    function resolveIdentitySourceMap(module: Module.Base, sourceText: string, sourcePath: string): Promise<string>;
    /**
     * Assembles a single JavaScript file from the specified set of code sources.
     * Optionally produces a sourcemap.
     * Optionally substitutes strings within string literals.
     * Optionally minifies the output code.
     * @param dir
     * @param fileID
     * @param sources
     * @param settings
     */
    function assembleJS(dir: string, fileID: string, sources: CodeSource[], settings: Settings): Promise<void>;
    /**
     * Assembles a single JavaScript file from the specified set of code sources.
     * Optionally produces a sourcemap.
     * Optionally substitutes strings within string literals.
     * Optionally minifies the output code.
     * @param dir
     * @param fileID
     * @param source
     * @param settings
     */
    function assembleDts(dir: string, fileID: string, source: CodeSource[], settings: Settings): Promise<void>;
    /**
     * Gathers all third party dependencies for a module and inserts the source for them into the specified code source map.
     * @param module
     * @param source
     */
    function gatherThirdPartyDependencies(module: Module.Base, source: Util.Map<CodeSource>): Promise<void>;
}
declare namespace RS.Assembler.CodeSources {
    function addGulp(codeSources: CodeSource[]): Promise<void>;
    function addUnit(codeSources: CodeSource[], unit: Build.CompilationUnit): Promise<void>;
    function addModule(codeSources: CodeSource[], module: Module.Base, unitName: string): Promise<void>;
    function addModules(codeSources: CodeSource[], p2: Module.Base | ReadonlyArray<Module.Base>, unitName: string): Promise<void>;
    function addFile(codeSources: CodeSource[], file: string, sourceMapFile?: string): Promise<void>;
}
declare namespace RS.Assembler {
    interface SourceMap {
        /** SourceMap spec version. */
        version: number;
        /** SourceMap spec version. */
        sources: string[];
        mappings: string;
        file?: string;
        sourceRoot?: string;
        /** Array of textual source content. Aligned with 'sources'. */
        sourcesContent?: string[];
        names?: string[];
    }
    class SourceMapBuilder {
        private readonly __outputBuffer;
        private readonly __sources;
        private readonly __sourcesContent;
        private readonly __mappings;
        private __lastSourceIndex;
        private __lastSourceLine;
        private __lastSourceCol;
        /** Adds a single line of text. */
        addLine(text: string): void;
        /** Adds source content, optionally using the given sourceMap for the content. */
        addSource(content: Buffer | string, sourceMap?: SourceMap): void;
        /** Returns a buffer containing the current source content. */
        toContent(): Buffer;
        /** Returns a source map object representing the current source map state. */
        toSourceMap(): SourceMap;
    }
}
declare namespace RS.AssembleStages {
    /**
     * Responsible for copying the JS code of modules.
     */
    class Code extends AssembleStage.Base {
        private _moduleCode;
        private _thirdPartyCode;
        /**
         * Executes this assemble stage.
         */
        execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>): Promise<void>;
        protected getSynergyHash(): Promise<string>;
        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base): Promise<void>;
    }
    const code: Code;
}
declare namespace RS.Build.Directives {
    /** Contains directive metadata for a module. */
    interface Metadata {
        usesDefines: string[];
    }
    /** Returns whether or not the given unit needs to be recompiled due to directives. */
    function checkRecompileNeeded(unit: CompilationUnit): Promise<boolean>;
    /** Adds the directive pre-processor to the given unit. */
    function addPreProcessor(unit: CompilationUnit): Metadata;
    /**
     * Persists the given metadata for the given unit.
     * @returns A Promise which resolves once the data has been persisted.
     */
    function saveMetadata(unit: CompilationUnit, metadata: Metadata): PromiseLike<void>;
}
declare namespace RS.Build {
    /** Compiles the given compilation unit. Returns whether or not work was done. */
    function compileUnit(unit: CompilationUnit): Promise<BuildStage.Result>;
}
declare namespace RS.BuildStages {
    /**
     * Responsible for compiling the TypeScript source code of modules.
     */
    class Compile extends BuildStage.Base {
        readonly unitName: string;
        /**
         * Gets if errors encountered when building a module are fatal and should end the whole process.
         * If this is false, other modules may continue to be compiled after an error has been encountered.
         */
        get errorsAreFatal(): boolean;
        /**
         * Gets if the executeModule function should be called in parallel for all modules.
         * Ddependencies are still considered and processed serially.
         */
        get runInParallel(): boolean;
        get name(): string;
        constructor(unitName: string);
        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected executeModule(module: Module.Base): Promise<BuildStage.Result>;
    }
    const compile: Compile;
}
declare namespace RS.Build.CodeGeneration {
    import TypeRef = CodeMeta.TypeRef;
    class Formatter {
        private _chunks;
        private _identLevel;
        private _emitLoc;
        /** Emit a chunk, do not consider emit location. */
        private emit;
        private emitIndent;
        /** Prepares the emitter to emit a new line of code. */
        private bringToNewLine;
        /** Emits a line of code. */
        emitLine(line: string): void;
        /** Emits a new scope. */
        enterScope(char?: string): void;
        /** Emits the end of a scope. */
        exitScope(char?: string): void;
        /** Emits the specified named object. */
        emitNamed(obj: Named): void;
        /** Emits the specified interface. */
        private emitInterface;
        /** Emits the specified property declaration. */
        private emitPropertyDeclaration;
        /** Emits the specified namespace. */
        private emitNamespace;
        /** Emits the specified variable. */
        private emitVariable;
        /** Emits the specified value. */
        private emitValue;
        /** Emits the specified array value. */
        private emitArrayValue;
        /** Emits the specified object value. */
        private emitObjectValue;
        /** Emits the specified key-value pair. */
        private emitKeyValue;
        /** Emits the specified code value. */
        private emitCodeValue;
        /** Emits the specified typedef. */
        private emitTypeDef;
        /** Emits the specified type. */
        emitType(type: TypeRef): void;
        /** Serialises all emmitted code to a string. */
        toString(): string;
    }
}
declare namespace RS.Build.CodeGeneration {
    import TypeRef = CodeMeta.TypeRef;
    import AccessQualifier = CodeMeta.AccessQualifier;
    import StorageQualifier = CodeMeta.StorageQualifier;
    import GenericTypeArg = CodeMeta.GenericTypeArg;
    import FunctionSignature = CodeMeta.FunctionSignature;
    /**
     * Represents some kind of named TypeScript construct.
     */
    interface NamedBase {
        /** Kind of object. */
        kind: string;
        /** Name of the object. */
        name: string;
        /** Documentation comment for the property. */
        comment?: string;
    }
    /**
     * Represents a TypeScript property to export.
     */
    interface PropertyDeclaration extends NamedBase {
        kind: "propertydeclr";
        /** Is the property abstract? */
        isAbstract?: boolean;
        /** Is the property readonly? */
        isReadonly?: boolean;
        /** Access qualifier of the property. If not set, assume public. */
        accessQualifier?: AccessQualifier;
        /** Storage qualifier of the property. If not set, assume instance. */
        storageQualifier?: StorageQualifier;
        /** Is the property optional? */
        optional?: boolean;
        /** Type of the property. */
        type: TypeRef;
    }
    /**
     * Represents a TypeScript interface to export.
     */
    interface Interface extends NamedBase {
        kind: "interface";
        /** Generic type arguments to this interface. */
        genericTypeArgs?: GenericTypeArg[];
        /** Base interfaces to extend. */
        extends?: TypeRef[];
        /** Properties contained within this interface. */
        properties?: PropertyDeclaration[];
        /** Methods contained within this interface. */
        methods?: MethodDeclaration[];
    }
    /**
     * Represents a TypeScript class to export.
     */
    interface Class extends NamedBase {
        kind: "class";
        /** Is the class abstract? */
        isAbstract?: boolean;
        /** Generic type arguments to this class. */
        genericTypeArgs?: GenericTypeArg[];
        /** Base classes to extend. */
        extends?: TypeRef[];
        /** Interfaces to extend. */
        implements?: TypeRef[];
        /** Properties contained within this class. */
        properties?: PropertyDeclaration[];
        /** Methods contained within this class. */
        methods?: MethodDeclaration[];
    }
    /**
     * Represents a TypeScript enum to export.
     */
    interface Enum extends NamedBase {
        kind: "enum";
        /** Is it a const enum? */
        isConst?: boolean;
        /** Enumerators. */
        enumerators?: {
            [name: string]: number | string;
        };
    }
    /**
     * Represents a TypeScript type to export (e.g. "export type X = number").
     */
    interface Type extends NamedBase {
        kind: "type";
        /** Generic type arguments to this type. */
        genericTypeArgs?: GenericTypeArg[];
        /** The actual type that this type refers to. */
        type?: TypeRef;
    }
    /**
     * Represents a TypeScript namespace to export.
     */
    interface Namespace extends NamedBase {
        kind: "namespace";
        /** Children of this namespace. */
        children?: Named[];
    }
    /**
     * Represents a TypeScript const object to export.
     */
    interface Variable extends NamedBase {
        kind: "var";
        /** Type of the variable. */
        type?: TypeRef;
        /** Is it const? */
        isConst?: boolean;
        /** Value of the variable. */
        value?: Value;
    }
    /**
     * Represents a TypeScript function to export.
     */
    interface FunctionDeclaration extends NamedBase {
        kind: "func";
        /** Signature of the function. */
        signature: FunctionSignature;
    }
    /**
     * Represents a TypeScript method (member function) to export.
     */
    interface MethodDeclaration extends NamedBase {
        kind: "method";
        /** Is the method abstract? Only relevant inside a class. */
        isAbstract?: boolean;
        /** Access qualifier of the method. If not set, assume public. */
        accessQualifier?: AccessQualifier;
        /** Storage qualifier of the method. If not set, assume instance. */
        storageQualifier?: StorageQualifier;
        /** Is the method optional? */
        optional?: boolean;
        /** Signature of the function. */
        signature: FunctionSignature;
    }
    /**
     * Represents a TypeScript array value to export.
     */
    interface ArrayValue extends NamedBase {
        kind: "arrval";
        /** Contents of the array. */
        values?: Value[];
    }
    /**
     * Represents a TypeScript object value to export.
     */
    interface ObjectValue extends NamedBase {
        kind: "objval";
        /** Contents of the object. */
        values?: KeyValue[];
    }
    /**
     * Represents a TypeScript key-value pair to export.
     */
    interface KeyValue extends NamedBase {
        kind: "keyval";
        value: Value;
    }
    /**
     * Represents an arbitrary piece of code to export.
     */
    interface CodeValue extends NamedBase {
        kind: "codeval";
        /** Pieces to emit in sequence. */
        pieces: (string | TypeRef | Named)[];
    }
    type Value = string | Named;
    type Named = PropertyDeclaration | Interface | Class | Enum | Type | Namespace | Variable | FunctionDeclaration | ArrayValue | ObjectValue | KeyValue | CodeValue;
    function isNamed(obj: any): obj is Named;
    function isTypeRef(obj: any): obj is TypeRef;
}
declare namespace RS.Build.CodeGeneration {
    import TypeRef = CodeMeta.TypeRef;
    const anyType: TypeRef;
    /**
     * Gets if both type references are EQUAL.
     */
    function typesAreEqual(a: TypeRef, b: TypeRef): boolean;
    /**
     * Gets if both property declarations are EQUAL.
     * @param a
     * @param b
     */
    function propertiesAreEqual(a: CodeMeta.PropertyDeclaration, b: CodeMeta.PropertyDeclaration): boolean;
    /**
     * Gets if both arrays of property declarations are EQUAL.
     * @param a
     * @param b
     * @param orderMatters
     */
    function propertyArraysAreEqual(a: CodeMeta.PropertyDeclaration[], b: CodeMeta.PropertyDeclaration[], orderMatters?: boolean): boolean;
    /**
     * Gets if both function signatures are EQUAL.
     * @param a
     * @param b
     */
    function functionSignaturesAreEqual(a: CodeMeta.FunctionSignature, b: CodeMeta.FunctionSignature): boolean;
    /**
     * Gets if both method declarations are EQUAL.
     * @param a
     * @param b
     */
    function methodsAreEqual(a: CodeMeta.MethodDeclaration, b: CodeMeta.MethodDeclaration): boolean;
    /**
     * Gets if both arrays of method declarations are EQUAL.
     * @param a
     * @param b
     * @param orderMatters
     */
    function methodArraysAreEqual(a: CodeMeta.MethodDeclaration[], b: CodeMeta.MethodDeclaration[], orderMatters?: boolean): boolean;
    /**
     * Gets if both arrays of type references are EQUAL.
     */
    function typeArraysAreEqual(a: TypeRef[], b: TypeRef[], orderMatters?: boolean): boolean;
    /**
     * Returns a new type reference that represents a narrowed by b.
     * Returns null if narrowing failed.
     */
    function narrowType(a: TypeRef, b: TypeRef): TypeRef | null;
    /**
     * Removes any duplicate types from the specified type array.
     */
    function removeDuplicateTypes(typeArr: TypeRef[]): void;
    /**
     * Creates a new type that can represent ANY of the specified types.
     * Returns null if no such type can be found.
     */
    function getUnion(typeArr: TypeRef[]): TypeRef;
    /**
     * Creates a new type that can represent BOTH of the specified types.
     * @param a
     * @param b
     */
    function intersect(a: TypeRef | null, b: TypeRef | null): TypeRef | null;
    /**
     * Makes a name safe for emitting as an identifier.
     */
    function sanitiseIdentifier(id: string): string;
    /**
     * Gets a string representation of the specified type.
     * @param type
     */
    function stringifyType(type?: TypeRef, useBrackets?: boolean): string;
    /**
     * Attempts to normalise the specified TypeRef, turning any "relative" types into "absolute" types.
     * For instance, the simple type "My.Class" would normalise to "RS.My.Class" if referenced from within the "RS" namespace AND if "RS.My.Class" exists.
     * The normalised type should be resolvable given the global namespace.
     * Unidentifiable types are left alone.
     * @param type The type to resolve
     * @param namespaceHierarchy Path of namespaces leading to the location of the type. e.g. for the above example, [RS]
     */
    function normaliseTypeRef(type: TypeRef, namespaceHierarchy: Namespace[]): void;
    /**
     * Searches a hierarchy for the specified Named.
     * @param ns
     * @param typeName
     */
    function searchForNamed(rootNode: Named, typeName: string): {
        value: Named;
        prefix: string;
    } | null;
}
declare namespace RS.Module.DefaultTSConfigs {
    const source: TSConfig;
    const buildSource: TSConfig;
}
declare namespace RS.Module {
    /**
     * Gathers a complete ordered list of modules that satisfies all dependencies of the given list of module names, plus those modules themselves.
     * @param modules
     */
    function getDependencySet(modules: string[]): List<Module.Base>;
    /**
     * Sorts an array of modules into several batches, in which no module depends on another.
     * Modules in each batch can be processed in parallel, so long as the set of batches is processed serially.
     * This should be executed a complete dependency set.
     * @param modules
     * @param maxBatchSize
     */
    function getBatchSet(modules: ArrayLike<Module.Base>, maxBatchSize?: number): List<List<Module.Base>>;
    /**
     * Compiles build components for all specified modules.
     * @param moduleList
     */
    function compileBuildComponents(moduleList: List<Module.Base>): Promise<void>;
}
declare namespace RS.Build {
    interface TemplateParam {
        displayName?: string;
        example?: string;
        defaultValue?: string;
    }
    interface TemplateInfo {
        source: string;
        outputPath: string;
    }
    class Scaffold {
        protected _templates: TemplateInfo[];
        protected _params: Util.Map<TemplateParam>;
        constructor();
        addTemplate(source: string, outputPath: string): void;
        addParam(key: string, displayName?: string, example?: string, defaultValue?: string): void;
        protected promptForParams(): Promise<Util.Map<string>>;
        generate(targetModule: Module.Base): Promise<boolean>;
        private static _regexp;
        protected applyArgs(str: string, args: Util.Map<string>): string;
        protected serialiseArg(argValue?: string, argFlag?: string): string;
    }
}
declare namespace RS.Build {
    interface ScaffoldInfo {
        scaffold: Scaffold;
        description?: string;
    }
    function registerScaffold(type: string, scaffold: Scaffold, description?: string): void;
    function findScaffold(type: string): ScaffoldInfo | null;
    function getAllScaffolds(): Readonly<Util.Map<ScaffoldInfo>>;
}
declare namespace RS.Tasks {
    const dependencyTree: Build.Task;
    /** Print out module dependency tree from the root module. */
    function buildTree(): Promise<void>;
}
declare namespace RS.Tasks {
    const documentAll: Build.Task;
}
declare namespace RS.Build {
    /**
     * Executes code formatter for the root module only.
     */
    const format: Task;
    /**
     * Executes code formatter for a specific set of modules only.
     */
    const formatOnly: Task;
    /**
     * Executes code formatter for all loaded modules.
     */
    const formatAll: Task;
}
declare namespace RS.Tasks {
    const manual: Build.Task;
}
declare namespace RS.Tasks {
    const list: Build.Task;
}
declare namespace RS.Input {
    type CompleterFunction = (line: string) => [string[], string];
    /**
     * Waits for a line of user input.
     */
    function readLine(completer?: CompleterFunction, muted?: boolean): PromiseLike<string>;
}
declare namespace RS.Tasks {
    const NewModule: Build.Task;
}
declare namespace RS.Tasks {
    const noop: Build.Task;
}
declare namespace RS.Tasks {
    const test: Build.Task;
    const testOnly: Build.Task;
    const testAll: Build.Task;
}
declare namespace RS.Tasks {
    const scaffold: Build.Task;
}
declare namespace RS.Tasks {
    const serve: Build.Task;
}
declare namespace RS.Tasks.Testing {
    function assembleThirdPartyJS(testUnits: List<Build.CompilationUnit>, dir: string): Promise<void>;
    function executeUnit(unit: Build.CompilationUnit): Promise<void>;
    function initialiseUnit(unit: Build.CompilationUnit): Promise<boolean>;
    function isUnitValid(unit: Build.CompilationUnit): boolean;
    function runKarma(configPath: string, cliOptions?: Partial<karma.ConfigOptions>): Promise<karma.TestResults>;
    function runKarmaWithConfig(config: karma.ConfigOptions): Promise<karma.TestResults>;
}
declare namespace RS.Build {
    interface AST {
        program: ESTree.Program;
        name: string | null;
        loc: ESTree.SourceLocation | null;
        type: string;
        comments: ESTree.Comment[] | null;
    }
    namespace AST {
        type Visitor = (node: ESTree.Node) => boolean | void;
        function visit(ast: AST, visitor: Visitor): void;
        function expressionsEqual(a: ESTree.Expression | ESTree.SpreadElement, b: ESTree.Expression | ESTree.SpreadElement): boolean;
        function propertiesEqual(a: ESTree.Property, b: ESTree.Property): boolean;
    }
}
declare namespace RS {
    type PartialRecursive<T> = {
        [P in keyof T]?: PartialRecursive<T[P]>;
    };
    /**
     * Assign all properties of "from" to "to".
     * Performs deep assignment.
     * Returns back "to".
     * @param from
     * @param to
     * @param deep
     */
    function assign<T extends object>(from: PartialRecursive<T>, to: T, deep: true): T;
    /**
     * Assign all properties of "from" to "to".
     * Performs shallow assignment.
     * Returns back "to".
     * @param from
     * @param to
     * @param deep
     */
    function assign<T extends object>(from: Partial<T>, to: T, deep: false): T;
}
/** Contains utility functions for the bower package system. */
declare namespace RS.Bower {
    /**
     * Installs a bower package locally.
     * @param name
     * @param version
     */
    function installPackage(name: string, version: string): Promise<void>;
}
/** Contains utility functions for calculating checksums. */
declare namespace RS.Checksum {
    /**
     * Gets a checksum for the contents of an object.
     * @param data
     */
    function getSimple(data: object): string;
    /**
     * Gets a checksum for the specified string.
     * @param data
     */
    function getSimple(data: string): string;
    /**
     * Gets a checksum for the file at the specified path. Async.
     */
    function get(path: string): Promise<any>;
    /**
     * Finds a checksum for a group of files.
     */
    function getComposite(paths: string[]): PromiseLike<string>;
    /**
     * Finds a checksum for a group of files and an object.
     */
    function getComposite(paths: string[], extraData: object): PromiseLike<string>;
    /**
     * Gets a base64 encoded SHA256 hash of the specified string.
     * @param content
     */
    function sha256(str: string): string;
    /**
     * Encapsulates a database of checksums.
     */
    class DB {
        protected _db: {
            [key: string]: string;
        };
        protected _dirty: boolean;
        /** Gets if this DB is dirty (has been changed but not saved). */
        get dirty(): boolean;
        /**
         * Clears the database.
         */
        clear(): void;
        /**
         * Loads the database from the specified path. Fails silently if the file doesn't exist. Async.
         * @param path
         */
        loadFromFile(path: string): Promise<void>;
        /**
         * Writes the database to the specified path. Async.
         * @param path
         */
        saveToFile(path: string): Promise<void>;
        /**
         * Gets the checksum for the specified key. Returns null if not found.
         * @param key
         */
        get(key: string): string | null;
        /**
         * Sets the checksum for the specified key.
         * @param key
         */
        set(key: string, checksum: string): boolean;
        /**
         * Gets if the checksum by the specified key is DIFFERENT to that provided.
         * @param key
         * @param checksum
         */
        diff(key: string, checksum: string): boolean;
    }
}
declare namespace RS.EnvArgs {
    const dryRun: BooleanEnvArg;
}
declare namespace RS.Build {
    /**
     * Encapsulates a set of TypeScript source files to compile.
     */
    class CompilationUnit {
        protected _name: string | null;
        protected _module: Module.Base | null;
        protected _path: string | null;
        protected _dependencies: List<CompilationUnit.Dependency>;
        protected _rawDependencies: List<CompilationUnit.RawDependency>;
        protected _validState: CompilationUnit.ValidState;
        protected _postProcessors: List<CompilationUnit.PostProcessor>;
        protected _preProcessors: List<CompilationUnit.PreProcessor>;
        protected _rootDir: string;
        protected _outFile: string;
        protected _emitsSourceMap: boolean;
        protected _emitsDefinitions: boolean;
        protected _emitsDefinitionMap: boolean;
        protected _requiresHelpers: boolean;
        protected _srcChecksum: string;
        protected _recompileNeeded: boolean;
        protected _lintNeeded: boolean;
        protected _defaultTSConfig: Readonly<TSConfig>;
        /** Gets or sets the default tsconfig.json. */
        get defaultTSConfig(): Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.FilesDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }> | Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.ExcludeDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }> | Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.IncludeDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }> | Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.ReferencesDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }>;
        set defaultTSConfig(value: Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.FilesDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }> | Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.ExcludeDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }> | Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.IncludeDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }> | Readonly<TSConfig.CompileOnSaveDefinition & TSConfig.TypeAcquisitionDefinition & TSConfig.ExtendsDefinition & TSConfig.ReferencesDefinition & {
            compilerOptions?: TSConfig.CompilerOptions;
        }>);
        /** Gets or sets the name of this compilation unit. */
        get name(): string;
        set name(value: string);
        /** Gets or sets the module to which this compilation unit belongs. */
        get module(): Module.Base;
        set module(value: Module.Base);
        /** Gets or sets the path to the source code of this compilation unit, relative to the cwd. */
        get path(): string;
        set path(value: string);
        /** Gets the dependencies of this compilation unit. */
        get dependencies(): List<CompilationUnit.Dependency>;
        /** Gets the raw dependencies of this compilation unit. */
        get rawDependencies(): List<CompilationUnit.RawDependency>;
        /** Gets the validity state of this compilation unit. */
        get validityState(): CompilationUnit.ValidState;
        /** Gets the root directory of this compilation unit, relative to the cwd. */
        get rootDir(): string;
        /** Gets the output js file of this compilation unit, relative to the cwd. */
        get outJSFile(): string;
        /** Gets the output js sourcemap file of this compilation unit, relative to the cwd. */
        get outMapFile(): string;
        /** Gets the output d.ts file of this compilation unit, relative to the cwd. */
        get outDTSFile(): string;
        /** Gets the output d.ts sourcemap file of this compilation unit, relative to the cwd. */
        get outDTSMapFile(): string;
        /** Gets if the output js code requires the ts helpers to be externally present. */
        get requiresHelpers(): boolean;
        /** Gets the checksum for the source of this compilation unit. */
        get srcChecksum(): string;
        /** Gets if compiliation is needed for this compilation unit. */
        get recompileNeeded(): boolean;
        /** Gets if lint is needed for this compilation unit. */
        get lintNeeded(): boolean;
        constructor();
        /**
         * Adds a post processor to this compilation unit.
         * @param postProcessor
         */
        addPostProcessor(postProcessor: CompilationUnit.PostProcessor): void;
        /**
         * Adds a pre-processor to this compilation unit.
         * @param preProcessor
         */
        addPreProcessor(preProcessor: CompilationUnit.PreProcessor): void;
        /**
         * Initialises this compilation unit.
         */
        init(): Promise<void>;
        /**
         * Validates this compilation unit's code.
         */
        lintFiles(): Promise<boolean>;
        /**
         * Executes this compilation unit.
         */
        execute(): Promise<CompilationUnit.CompileResult>;
        protected copyFolder(sourcePath: string, destPath: string): Promise<void>;
        protected preProcess(outPath: string): Promise<boolean>;
        protected lintPath(path: string): Promise<boolean>;
        /** Returns a promise for a TSConfig object. */
        protected resolveTSConfig(): PromiseLike<TSConfig | null>;
        protected resolveDependencies(): Promise<boolean>;
        /**
         * Acquires the specified definition.
         * @param name
         * @param path
         */
        protected acquireDefinition(path: string, output: string): Promise<boolean>;
    }
    namespace CompilationUnit {
        function executeUnit(unit: CompilationUnit): Promise<boolean>;
        enum DependencyKind {
            ThirdParty = 0,
            CompilationUnit = 1,
            NodeModule = 2,
            BowerPackage = 3,
            DefinitionsFolder = 4
        }
        interface BaseDependency<TKind extends DependencyKind> {
            kind: TKind;
            name: string;
        }
        interface ThirdPartyDependency extends BaseDependency<DependencyKind.ThirdParty> {
            /** Path of the js file, relative to the module. */
            sourceFile?: string;
            /** Path of the js sourcemap file, relative to the module. */
            sourceMap?: string;
            /** Path of the ts definitions file, relative to the module. */
            definitionsFile?: string;
            /** Path of the ts definitions map file, relative to the module. */
            definitionsMapFile?: string;
        }
        interface CompilationUnitDependency extends BaseDependency<DependencyKind.CompilationUnit> {
            /** Compilation unit to depend on. */
            compilationUnit: CompilationUnit;
        }
        interface NodeModuleDependency extends BaseDependency<DependencyKind.NodeModule> {
            /** Name of the node module. */
            moduleName: string;
            /** Version of the node module. */
            moduleVersion?: string;
        }
        interface BowerPackageDependency extends BaseDependency<DependencyKind.BowerPackage> {
            /** Name of the bower package. */
            packageName: string;
            /** Version of the bower package. */
            packageVersion?: string;
        }
        interface DefinitionsFolderDependency extends BaseDependency<DependencyKind.DefinitionsFolder> {
            /** Path of the folder to read, relative to the cwd. */
            path: string;
        }
        type Dependency = ThirdPartyDependency | CompilationUnitDependency | NodeModuleDependency | BowerPackageDependency | DefinitionsFolderDependency;
        namespace Dependency {
            function getChecksum(dep: Dependency): string;
        }
        interface RawDependency {
            sourceFile: string;
            sourceMap?: string;
        }
        interface CompileArtifact {
            source: string;
            sourceMap: SourceMap | null;
            definitions: string | null;
            definitionsSourceMap: SourceMap | null;
        }
        type PostProcessor = (this: CompilationUnit, artifact: CompileArtifact) => CompileArtifact;
        interface PreProcessorOut {
            output: string;
            workDone: boolean;
        }
        type PreProcessor = (this: CompilationUnit, source: string) => PreProcessorOut;
        enum ValidState {
            Uninitialised = 0,
            NoTSConfig = 1,
            MissingDependency = 2,
            Initialised = 3,
            Compiled = 4,
            CompileError = 5
        }
        interface CompileResult {
            success: boolean;
            time: number;
            errors: string[] | null;
        }
    }
}
declare namespace RS.Build {
    /**
     * A compiled code file with an optional associated source map.
     */
    class CompiledFile {
        protected _src: string;
        protected _map: object | null;
        /** Gets the code for this file. */
        get source(): string;
        /** Gets the mappings for this file. */
        get mappings(): object;
        constructor(source: string, mappings?: object);
        transform(fn: (ast: AST) => void, sourceFileName: string, sourceMapName: string): void;
        minify(sourceFileName: string): void;
        /**
         * Writes this compiled file to disk.
         * @param sourceFile
         * @param mapFile
         */
        write(sourceFile: string, mapFile?: string): Promise<void>;
        /**
         * Loads a compiled file from disk.
         * @param sourceFile
         * @param mapFile
         */
        static load(sourceFile: string, mapFile?: string): Promise<CompiledFile>;
    }
}
declare namespace RS.Crypto {
    /**
     * Generates a random IV.
     */
    function generateIV(): Buffer;
    /**
     * Generates a random key.
     */
    function generateKey(): Buffer;
    /**
     * Encrypts the specified string using the specified password.
     * @param text
     * @param password
     */
    function encrypt(text: string, password: string | Buffer, iv: string | Buffer): string;
    /**
     * Decrypts the specified string using the specified password.
     * @param text
     * @param password
     */
    function decrypt(text: string, password: string | Buffer, iv: string | Buffer): string;
}
declare namespace RS.Directives {
    type Defines = {
        [name: string]: boolean | string;
    };
    function parse(str: string, defines: Defines): ParseResult;
    interface ParseResult {
        usesDefines: string[];
        content: string;
    }
}
declare namespace RS {
    class BaseError implements Error {
        get name(): string;
        get message(): string;
        get stack(): string;
        protected readonly inner: Error;
        constructor(message?: string);
        toString(): string;
    }
}
declare namespace RS.FFMPEG {
    interface FFProbe {
        programs: FFProbe.Program[];
        streams: FFProbe.Stream[];
        format: FFProbe.Format;
    }
    namespace FFProbe {
        interface Program {
        }
        interface BaseStream<TCodecType extends "video" | "audio"> {
            index: number;
            codec_name: string;
            codec_long_name: string;
            codec_type: TCodecType;
            codec_time_base: string;
            codec_tag_string: string;
            codec_tag: string;
            r_frame_rate: string;
            avg_frame_rate: string;
            time_base: string;
            start_pts: number;
            start_time: string;
            duration_ts: number;
            duration: string;
            bit_rate: string;
            disposition: Util.Map<number>;
            tags: Util.Map<string>;
        }
        interface VideoStream extends BaseStream<"video"> {
            profile: string;
            width: number;
            height: number;
            coded_width: number;
            coded_height: number;
            has_b_frames: number;
            sample_aspect_ratio: string;
            display_aspect_ratio: string;
            pix_fmt: string;
            level: number;
            chrome_location: string;
            refs: number;
            is_avc: string;
            nal_length_size: string;
            bits_per_raw_sample: string;
            nb_frames: string;
        }
        interface AudioStream extends BaseStream<"audio"> {
            sample_fmt: string;
            sample_rate: string;
            channels: number;
            channel_layout: string;
            bits_per_sample: number;
        }
        type Stream = VideoStream | AudioStream;
        interface Format {
            filename: string;
            nb_streams: number;
            nb_programs: number;
            format_name: string;
            format_long_name: string;
            start_time: string;
            duration: string;
            size: string;
            bit_rate: string;
            probe_score: number;
            tags: Util.Map<string>;
        }
        /**
         * Runs FFProbe on a file and returns the result.
         * @param fileName
         */
        function run(fileName: string): Promise<FFProbe>;
        /**
         * Equivalent to streams.filter(s => s.codec_type === "audio").
         * @param streams
         * @param codecType
         */
        function filterStreams(streams: Stream[], codecType: "audio"): AudioStream[];
        /**
         * Equivalent to streams.filter(s => s.codec_type === "video").
         * @param streams
         * @param codecType
         */
        function filterStreams(streams: Stream[], codecType: "video"): VideoStream[];
    }
}
declare namespace RS {
    type Func<TIn, TOut = TIn> = (obj: TIn) => TOut;
    type Action<T = void> = T extends void ? () => void : (obj: T) => void;
}
declare namespace RS.Git {
    /**
     * Removes a tag from the current commit.
     * @param name
     * @param message
     * @param repo
     */
    function untag(name: string, repo?: string): Promise<void>;
    /**
     * Adds an annotated tag to the current commit.
     * @param name
     * @param message
     * @param repo
     */
    function tag(name: string, message: string, repo?: string): Promise<void>;
    /**
     * Pushes the specified local tag to remote.
     * @param name
     * @param repo
     */
    function pushTag(name: string, repo?: string): Promise<void>;
    /**
     * Pushes all local tags to remote.
     * @param repo
     */
    function pushTags(repo?: string): Promise<void>;
    /**
     * Clones a specified repo to a path, if the path is valid and repo doesn't already exist
     * @param repo
     * @param clonePath
     * @param args
     */
    function clone(repo: string, clonePath: string, args?: ReadonlyArray<string>): Promise<void>;
    /**
     * Commits all changes in specified repo with a message
     * @param repoPath
     * @param message
     */
    function commitAll(repoPath: string, message: string): Promise<void>;
    /**
     * Pushes up changes in repo path
     * @param repoPath
     */
    function push(repoPath: string): Promise<void>;
    /**
     * Checks out the specified branch and pulls the latest changes, optionally by force
     * @param repoPath
     * @param branch
     * @param force
     */
    function update(repoPath: string, branch: string, force?: boolean): Promise<void>;
}
/**
 * Contains HTTP utilities.
 */
declare namespace RS.HTTP {
    interface FetchResult {
        data?: string;
        responseCode: number;
    }
    /**
     * Fetches the content at the specified URL.
     * @param url
     */
    function fetch(url: string): PromiseLike<FetchResult>;
    /**
     * Gets if the specified string is a valid URL.
     * @param val
     */
    function isURL(val: string): boolean;
}
declare namespace RS {
    interface IDisposable {
        readonly isDisposed: boolean;
        dispose(): void;
    }
}
declare namespace RS {
    class MessageHandler implements IDisposable {
        private readonly _messageListeners;
        private _isDisposed;
        get isDisposed(): boolean;
        constructor();
        private static listenerDoesMatch;
        on(callback: Action<string>): void;
        on(event: string, callback: Action<string>): void;
        off(callback: Action<string>): void;
        off(event: string, callback: Action<string>): void;
        /** Waits for a message of the given name. */
        event(name: string): Promise<string>;
        /** Waits for a message, optionally matching the given predicate. */
        message(predicate?: (message: string) => boolean): Promise<string>;
        dispose(): void;
        protected onMessageReceived(message: string): void;
        protected rejectAllMessageListeners(reason?: any): void;
    }
}
declare namespace RS {
    class IPC {
        static get id(): string;
        static set id(value: string);
        static get timeout(): number;
        static set timeout(value: number);
        private constructor();
    }
    namespace IPC {
        interface IChannel extends MessageHandler {
            readonly name: string;
            /** Sends a message on this communications channel. */
            send(message: string): Promise<void>;
            /** Closes this communications channel. */
            close(): void;
        }
        class Server extends MessageHandler {
            private static _instance;
            private _running;
            private _starting;
            private _lastClient;
            private _replyTarget;
            private constructor();
            static get(): Server;
            start(): Promise<void>;
            broadcast(message: string): Promise<void>;
            /** Replies to the previous client message. */
            reply(message: string): Promise<void>;
            /** Opens a two-way communications channel with the last client. */
            open(name: string): IChannel;
            dispose(): void;
            private onParentMessageReceived;
            private onClientMessageReceived;
            private prepareMessageForNodeIPC;
            private readPreparedMessage;
            private setListeners;
        }
        class Client extends MessageHandler {
            readonly target: string;
            private _connected;
            private get server();
            constructor(target: string);
            connect(): Promise<void>;
            send(message: string): Promise<void>;
            dispose(): void;
            private onConnected;
            private onDisconnected;
            private onServerMessageReceived;
            private setListeners;
        }
    }
}
declare namespace RS.JSMinify {
    function minify(sourcePath: string, mapPath: string): PromiseLike<void>;
}
declare namespace RS.JSON {
    /** Tries to parse a file as JSON. */
    function parseFile(path: string): Promise<any>;
    /**
     * Converts a JavaScript Object Notation (JSON) string into an object.
     * @param json
     */
    function parse(json: string): any;
    /**
     * Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
     */
    const stringify: {
        (value: any, replacer?: (this: any, key: string, value: any) => any, space?: string | number): string;
        (value: any, replacer?: (string | number)[], space?: string | number): string;
    };
}
declare namespace RS.JSReplace {
    function replace(sourcePath: string, mapPath: string, replacements: {
        [name: string]: string;
    }): PromiseLike<void>;
}
declare namespace RS.Build {
    interface LineColumnFinderOptions {
        origin?: number;
    }
    interface LineColumn {
        line: number;
        col: number;
    }
    /**
     * Finder for index and line-column from given string.
     */
    class LineColumnFinder {
        private str;
        private lineToIndex;
        private origin;
        constructor(str: string, options?: LineColumnFinderOptions);
        /**
         * Find line and column from index in the string.
         *
         * @param  {number} index - Index in the string. (0-origin)
         * @return {Object|null}
         *     Found line number and column number in object `{ line: X, col: Y }`.
         *     If the given index is out of range, it returns `null`.
         */
        fromIndex(index: number): LineColumn;
        /**
         * Find index from line and column in the string.
         */
        toIndex(line: number, column: number): number;
        /**
         * Find index from line and column in the string.
         */
        toIndex(lineColumn: number[]): number;
        /**
         * Find index from line and column in the string.
         */
        toIndex(lineColumn: LineColumn): number;
        /**
         * Build an array of indexes of each line from a string.
         */
        private buildLineToIndex;
        /**
         * Find a lower-bound index of a value in a sorted array of ranges.
         *
         * Assume `arr = [0, 5, 10, 15, 20]` and
         * this returns `1` for `value = 7` (5 <= value < 10),
         * and returns `3` for `value = 18` (15 <= value < 20).
         */
        private findLowerIndexInRangeArray;
    }
}
/** Contains utility functions for the node module system. */
declare namespace RS.NPM {
    /**
     * Installs a node module locally.
     * @param name
     * @param version
     */
    function installPackage(name: string, version: string): Promise<void>;
}
declare namespace RS.Build {
    type ItemProcessor<T> = (item: T, threadID: number) => PromiseLike<void>;
    interface ProcessResult<T> {
        succeedItems: T[];
        failedItems: T[];
    }
    /**
     * Runs several async processing chains in parallel.
    **/
    class ParallelProcessor<T> {
        protected _items: T[];
        protected _threadCount: number;
        protected _processor: ItemProcessor<T>;
        protected _result: ProcessResult<T>;
        protected _curItemIndex: number;
        constructor(items: T[], threadCount: number, processor: ItemProcessor<T>);
        /** Launches all threads and waits until completion, returning the results. */
        process(): Promise<ProcessResult<T>>;
        /** Runs a single thread that will process all items until exhaustion. */
        protected thread(id: number): Promise<void>;
    }
}
declare namespace RS.Process {
    const isChild: boolean;
}
declare namespace RS {
    class Sandbox extends MessageHandler {
        private readonly _scriptPath;
        private readonly _args;
        private _process;
        constructor(scriptPath: string, ...args: string[]);
        start(): Promise<void>;
        send(message: string): Promise<void>;
        dispose(): void;
        private onError;
        private onExit;
        private detachProcessListeners;
    }
}
/** Contains TypeScript compiler functionality. */
declare namespace RS.TSLint {
    /**
     * Compiles the specified TypeScript project.
     * @param projectPath
     * @param logOutput
     */
    function lint(projectPath: string, lintPath: string, logOutput?: boolean): PromiseLike<void>;
    class LintFailure {
        readonly data: string;
        get isError(): boolean;
        constructor(data: string);
        toString(): string;
    }
}
declare namespace RS {
    /** A type of decorator that tags a class with a some kind of metadata. */
    interface Tag<T> {
        (value: T): ClassDecorator;
        get(obj: object): T;
        classes: ({
            new (): object;
        } & Function)[];
        getClassesWithTag(value: T): ({
            new (): object;
        } & Function)[];
    }
    namespace Tag {
        /**
         * Creates a new tag.
         */
        function create<T>(): Tag<T>;
    }
}
declare namespace RS {
    /** Returns a promise that resolves with the given promise and rejects if a set amount of time passes first. */
    function withTimeout<T>(promise: PromiseLike<T>, timeoutMillis: number): PromiseLike<T>;
    class TimeoutError extends BaseError {
        constructor(millis: number);
    }
}
/** Contains TypeScript compiler functionality. */
declare namespace RS.TypeScript {
    /**
     * Compiles the specified TypeScript project.
     * @param projectPath
     * @param logOutput
     */
    function compile(projectPath: string, logOutput?: boolean): PromiseLike<void>;
}
declare namespace RS {
    class URL {
        private readonly _nativeURL;
        constructor(host: string);
        get path(): string;
        set path(value: string);
        get port(): number | void;
        set port(value: number | void);
        get params(): url.URLSearchParams;
        toString(): string;
    }
}
declare namespace RS.Is {
    /** Gets if the specified value is a Version. */
    function version(value: any): value is Version;
}
declare namespace RS {
    /** Encapsulates a version in "major.minor.revision" form. */
    interface Version {
        major: number;
        minor: number;
        revision: number;
    }
    /** Creates an empty version (0.0.0). */
    function Version(): Version;
    /** Creates a version. */
    function Version(major: number, minor?: number, revision?: number): Version;
    namespace Version {
        /** Copies a to b. */
        function copy(a: Version, b: Version): void;
        /** Clones a. */
        function clone(a: Version, out?: Version): Version;
        /** Gets if a == b. */
        function equals(a: Version, b: Version): boolean;
        /** Gets if a > b. */
        function greaterThan(a: Version, b: Version): boolean;
        /** Gets if a < b. */
        function lessThan(a: Version, b: Version): boolean;
        /**
         * Gets if a version is compatible with a given required version.
         * It is assumed that minor and revision versions are not breaking, whilst major version are.
         * Therefore:
         * - 1.2.3 is not compatible with 2.2.3
         * - 1.3.3 is compatible with 1.2.3
         * - 1.2.4 is compatible with 1.2.3
         * - 1.2.2 is not compatible with 1.2.3
         * - 1.1.4 is not compatible with 1.2.3
         * - 0.0.4 is not compatible with 1.2.3
         */
        function compatible(version: Version, required: Version): boolean;
        /**
         * Gets if a matches the specified version values.
         * Non-present version values are assumed to be wildcards.
         * e.g. match(Version(1, 2, 3), 1) = match 1.2.3 with 1.*.* = true
         * e.g. match(Version(1, 2, 3), 1, 3, 3) = match 1.2.3 with 1.3.3 = false
         * e.g. match(Version(1, 2, 3), 1, 2) = match 1.2.3 with 1.2.* = true
         */
        function match(a: Version, major?: number, minor?: number, revision?: number): boolean;
        /**
         * Gets the next revision of a.
         * @param a
         * @param out
         */
        function nextRevision(a: Version, out?: Version): Version;
        /**
         * Gets the next minor version of a.
         * @param a
         * @param out
         */
        function nextMinor(a: Version, out?: Version): Version;
        /**
         * Gets the next major version of a.
         * @param a
         * @param out
         */
        function nextMajor(a: Version, out?: Version): Version;
        /**
         * Converts a to a string.
         * @param a
         */
        function toString(a: Version): string;
        /**
         * Parses a string into a version.
         * @param a
         */
        function fromString(str: string, out?: Version): Version;
    }
}
declare namespace RS {
    /** An extremely lightweight server that just serves a directory. */
    class WebServer {
        readonly ip: string;
        readonly port: number;
        readonly rootPath: string;
        private readonly _server;
        get isRunning(): boolean;
        constructor(ip: string, port: number, rootPath: string);
        open(): Promise<void>;
        close(): Promise<void>;
        private debug;
        private preCache;
        private serve;
    }
}
declare namespace RS {
    enum Permission {
        None = 0,
        Execute = 1,
        Write = 2,
        WriteExecute = 3,
        Read = 4,
        ReadExecute = 5,
        ReadWrite = 6,
        ReadWriteExecute = 7
    }
    interface Permissions {
        owner: Permission;
        group: Permission;
        other: Permission;
    }
    /** e.g. 724 for "-rwx-w-r--" */
    function Permissions(decimal: number): Permissions;
    /** e.g. "724" for "-rwx-w-r--" */
    function Permissions(octal: string): Permissions;
    /** e.g. "111010100" for "-rwx-w-r--" */
    function Permissions(binary: string): Permissions;
    /** e.g. [ true, true, true, false, true, false, true, false, false ] for "-rwx-w-r--" */
    function Permissions(binaryArray: boolean[]): Permissions;
    /** e.g. "-rwx-w-r--" for 724 */
    function Permissions(str: string): Permissions;
    /** e.g. (Permission.Read | Permission.Write | Permission.Execute, Permission.Write, Permission.Read) for 724 */
    function Permissions(owner: Permission, group: Permission, other: Permission): Permissions;
    namespace Permissions {
        /** "724" */
        function toString(permissions: Permissions): string;
    }
}
//# sourceMappingURL=gulplib.d.ts.map