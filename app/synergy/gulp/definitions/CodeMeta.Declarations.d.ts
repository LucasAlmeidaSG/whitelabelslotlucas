declare namespace CodeMeta
{
    /**
     * Property access qualifier.
     */
    export const enum AccessQualifier { Private, Protected, Public }

    /**
     * Property storage qualifier.
     */
    export const enum StorageQualifier { Instance, Static }

    /**
     * Represents a TypeScript property to export.
     */
    export interface PropertyDeclaration
    {
        /** Name of the property. */
        name: string;

        /** Is the property abstract? */
        isAbstract?: boolean;

        /** Access qualifier of the property. If not set, assume public. */
        accessQualifier?: AccessQualifier;

        /** Storage qualifier of the property. If not set, assume instance. */
        storageQualifier?: StorageQualifier;

        /** Is the property optional? */
        optional?: boolean;

        /** Type of the property. */
        type: TypeRef;
    }

    /**
     * Represents a generic type argument.
     */
    export interface GenericTypeArg
    {
        /** Name of the generic type argument. */
        name: string;

        /** Type restriction. */
        extends?: TypeRef;

        /** Default type. */
        default?: TypeRef;
    }

    /**
     * Represents a parameter to a function.
     */
    export interface FunctionParameter
    {
        /** Name of the parameter. */
        name: string;

        /** Is it optional? */
        optional?: boolean;

        /** Is it spread? (e.g. "...x: number[]") */
        isSpread?: boolean;

        /** Type of the parameter. */
        type?: TypeRef;
    }

    /**
     * Represents the signature of a callable function.
     */
    export interface FunctionSignature
    {
        /** Is the function a contructor? (MUST be invoked using new) */
        isConstructor?: boolean;

        /** Any generic type arguments for this function. */
        genericTypeArgs?: GenericTypeArg[];

        /** Parameters to the function. */
        parameters?: FunctionParameter[];

        /** Return type. If null, assume "void". */
        returnType?: TypeRef;
    }

    /**
     * Represents a TypeScript method (member function) to export.
     */
    export interface MethodDeclaration
    {
        /** Name of the method. */
        name: string;

        /** Is the method abstract? Only relevant inside a class. */
        isAbstract?: boolean;

        /** Access qualifier of the method. If not set, assume public. */
        accessQualifier?: AccessQualifier;

        /** Storage qualifier of the method. If not set, assume instance. */
        storageQualifier?: StorageQualifier;

        /** Is the method optional? */
        optional?: boolean;

        /** Signature of the function. */
        signature: FunctionSignature;
    }
}