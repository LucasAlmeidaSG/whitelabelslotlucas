declare interface SourceMap
{
    version: number;
    file: string;
    sources: string[];
    sourceRoot?: string;
    names: string[];
    mappings: string;
    sourcesContent?: string[];
}