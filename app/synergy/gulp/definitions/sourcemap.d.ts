declare module SourceMap
{
    interface LineColumn
    {
        line: number;
        column: number;
    }

    interface Mapping
    {
        source: string;
        generatedLine: number;
        generatedColumn: number;
        originalLine: number;
        originalColumn: number;
    }

    type BiasType = number;
    type OrderType = number;

    /**
     * A SourceMapConsumer instance represents a parsed source map which we can query for information about the original file positions by giving it a file position in the generated source.
     */
    class SourceMapConsumer
    {
        public constructor(rawSourceMap: object);

        /**
         * Compute the last column for each generated mapping. The last column is inclusive.
         */
        computeColumnSpans(): void;

        /**
         * Returns the original source, line, and column information for the generated source's line and column positions provided.
         * @param generatedPosition 
         */
        originalPositionFor(generatedPosition: { line: number; column: number; bias: BiasType; }): {
            line: number | null;
            column: number | null;
            source: string | null;
            name: string | null;
        };
        
        /**
         * Returns the generated line and column information for the original source, line, and column positions provided.
         * @param originalPosition 
         */
        generatedPositionFor(originalPosition: { line: number; column: number; source: string; }): {
            line: number | null;
            column: number | null;
            lastColumn?: number;
        };
        
        /**
         * Returns all generated line and column information for the original source, line, and column provided.
         * If no column is provided, returns all mappings corresponding to a either the line we are searching for or the next closest line that has any mappings. Otherwise, returns all mappings corresponding to the given line and either the column we are searching for or the next closest column that has any offsets.
         * @param originalPosition 
         */
        allGeneratedPositionsFor(originalPosition: { line: number; column?: number; source: string; }): {
            line: number | null;
            column: number | null;
            lastColumn?: number;
        }[];

        /**
         * Return true if we have the embedded source content for every source listed in the source map, false otherwise.
         */
        hasContentsOfAllSources(): boolean;

        /**
         * Returns the original source content for the source provided.
         * If the source content for the given source is not found, then an error is thrown.
         * Optionally, pass true as the second param to have null returned instead.
         * @param source 
         * @param returnNullOnMissing 
         */
        sourceContentFor(source: string, returnNullOnMissing?: boolean): string;

        /**
         * Iterate over each mapping between an original source/line/column and a generated line/column in this source map.
         * @param callback 
         * @param context 
         * @param order 
         */
        eachMapping(callback: (m: Mapping) => void, context?: object, order?: OrderType): void;
    }

    namespace SourceMapConsumer
    {
        const GREATEST_LOWER_BOUND: BiasType;
        const LEAST_UPPER_BOUND: BiasType;

        const GENERATED_ORDER: OrderType;
        const ORIGINAL_ORDER: OrderType;
    }

    /**
     * An instance of the SourceMapGenerator represents a source map which is being built incrementally.
     */
    class SourceMapGenerator
    {
        public constructor(startOfSourceMap?: { file: string; sourceRoot: string; skipValidation?: boolean; });

        /**
         * Creates a new SourceMapGenerator from an existing SourceMapConsumer instance.
         * @param sourceMapConsumer 
         */
        static fromSourceMap(sourceMapConsumer: SourceMapConsumer): SourceMapGenerator;

        /**
         * Add a single mapping from original source line and column to the generated source's line and column for this source map being created.
         * @param mapping 
         */
        addMapping(mapping: { source: string; name?: string; original: LineColumn; generated: LineColumn; }): void;

        /**
         * Set the source content for an original source file.
         * @param sourceFile 
         * @param sourceContent 
         */
        setSourceContent(sourceFile: string, sourceContent: string): void;

        /**
         * Applies a SourceMap for a source file to the SourceMap.
         * Each mapping to the supplied source file is rewritten using the supplied SourceMap.
         * Note: The resolution for the resulting mappings is the minimum of this map and the supplied map.
         * @param sourceMapConsumer 
         * @param sourceFile 
         * @param sourceMapPath 
         */
        applySourceMap(sourceMapConsumer: SourceMapConsumer, sourceFile?: string, sourceMapPath?: string): void;

        /**
         * Renders the source map being generated to a string.
         */
        toString(): string;
    }

    type Chunk = string | SourceNode | (string | SourceNode)[];

    /**
     * SourceNodes provide a way to abstract over interpolating and/or concatenating snippets of generated JavaScript source code, while maintaining the line and column information associated between those snippets and the original source code.
     * This is useful as the final intermediate representation a compiler might use before outputting the generated JS and source map.
     */
    class SourceNode
    {
        public constructor();

        public constructor(line: number | null, column: number | null, source: string | null, chunk?: Chunk, name?: string);

        /**
         * Creates a SourceNode from generated code and a SourceMapConsumer.
         * @param code 
         * @param sourceMapConsumer 
         * @param relativePath 
         */
        static fromStringWithSourceMap(code: string, sourceMapConsumer: SourceMapConsumer, relativePath?: string): SourceNode;

        /**
         * Add a chunk of generated JS to this source node.
         * @param chunk 
         */
        add(chunk: Chunk): void;

        /**
         * Prepend a chunk of generated JS to this source node.
         * @param chunk 
         */
        prepend(chunk: Chunk): void;

        /**
         * Set the source content for a source file.
         * This will be added to the SourceMap in the sourcesContent field.
         * @param sourceFile 
         * @param sourceContent 
         */
        setSourceContent(sourceFile: string, sourceContent: string): void;

        /**
         * Walk over the tree of JS snippets in this node and its children.
         * The walking function is called once for each snippet of JS and is passed that snippet and the its original associated source's line/column location.
         * @param fn 
         */
        walk(fn: (code: string, loc: { source: string | null; line: number | null; column: number | null; name: string | null; }) => void): void;

        /**
         * Walk over the tree of SourceNodes.
         * The walking function is called for each source file content and is passed the filename and source content.
         * @param fn 
         */
        walkSourceContents(fn: (source: string, contents: string) => void): void;

        /**
         * Like Array.prototype.join except for SourceNodes.
         * Inserts the separator between each of this source node's children.
         * @param sep 
         */
        join(sep: string): SourceNode;

        /**
         * Call String.prototype.replace on the very right-most source snippet.
         * Useful for trimming white space from the end of a source node, etc.
         * @param pattern 
         * @param replacement 
         */
        replaceRight(pattern: RegExp, replacement: string): void;

        /**
         * Return the string representation of this source node.
         * Walks over the tree and concatenates all the various snippets together to one string.
         */
        toString(): string;

        /**
         * Returns the string representation of this tree of source nodes, plus a SourceMapGenerator which contains all the mappings between the generated and original sources.
         * @param startOfSourceMap 
         */
        toStringWithSourceMap(startOfSourceMap?: { file: string; sourceRoot: string; skipValidation?: boolean; }): string;
    }

}

declare module "source-map"
{
    export = SourceMap;
}