declare namespace CodeMeta
{
    export namespace TypeRef
    {
        export const enum Kind
        {
            Raw = "basictyperef",
            Inline = "inlinetyperef",
            LiteralNumber = "litnumtyperef",
            LiteralString = "litstrtyperef",
            LiteralBool = "litbooltyperef",
            WithTypeArgs = "generictyperef",
            Array = "arraytyperef",
            FixedArray = "fixedarraytyperef",
            Intersect = "intersecttyperef",
            Union = "uniontyperef",
            Function = "functiontyperef",
            Is = "istyperef",
            Indexed = "indexedtyperef",
            TypeOf = "typeoftyperef",
            KeyOf = "keyoftyperef",
            Conditional = "conditionaltyperef"
        }

        /**
         * Represents a reference to a TypeScript type.
         */
        export interface Base<T extends Kind>
        {
            kind: T;
        }

        /**
         * Represents a reference to a raw TypeScript type.
         */
        export interface Raw<T extends string = string> extends Base<Kind.Raw>
        {
            name: T;
        }

        /**
         * Represents a reference to an inlined type.
         */
        export interface Inline extends Base<Kind.Inline>
        {
            /** Properties contained within the inlined type to which this reference refers. */
            properties?: PropertyDeclaration[];

            /** Methods contained within the inlined type to which this reference refers. */
            methods?: MethodDeclaration[];
        }

        /**
         * Represents a reference to an literal number type.
         */
        export interface LiteralNumber extends Base<Kind.LiteralNumber>
        {
            /** Literal number for the type. */
            value: number;
        }

        /**
         * Represents a reference to an literal string type.
         */
        export interface LiteralString extends Base<Kind.LiteralString>
        {
            /** Literal string for the type. */
            value: string;
        }

        /**
         * Represents a reference to an literal boolean type.
         */
        export interface LiteralBool extends Base<Kind.LiteralBool>
        {
            /** Literal bool for the type. */
            value: boolean;
        }

        /**
         * Represents a reference to a generic TypeScript type.
         */
        export interface WithTypeArgs extends Base<Kind.WithTypeArgs>
        {
            inner: TypeRef;
            genericTypeArgs: TypeRef[];
        }

        /**
         * Represents a reference to a array TypeScript type.
         */
        export interface Array extends Base<Kind.Array>
        {
            inner: TypeRef;
        }

        /**
         * Represents a reference to a fixed array TypeScript type (e.g. [number, number, boolean]).
         */
        export interface FixedArray extends Base<Kind.FixedArray>
        {
            inners: TypeRef[];
        }

        /**
         * Represents a reference to a intersect TypeScript type (e.g. x & y)
         */
        export interface Intersect extends Base<Kind.Intersect>
        {
            inners: TypeRef[];
        }

        /**
         * Represents a reference to a union TypeScript type (e.g. x | y)
         */
        export interface Union extends Base<Kind.Union>
        {
            inners: TypeRef[];
        }

        /**
         * Represents a reference to a function TypeScript type (e.g. () => void)
         */
        export interface Function extends Base<Kind.Function>
        {
            signature: FunctionSignature;
        }

        /**
         * Represents a reference to a TypeScript "is" type (e.g. "X is number").
         */
        export interface Is extends Base<Kind.Is>
        {
            parameterName: string;
            inner: TypeRef;
        }

        /**
         * Represents a reference to a TypeScript "indexed" type (e.g. "X[T]").
         */
        export interface Indexed extends Base<Kind.Indexed>
        {
            inner: TypeRef;
            index: TypeRef;
        }

        /**
         * Represents a reference to a TypeScript "typeof" type (e.g. "typeof Myclass").
         */
        export interface TypeOf extends Base<Kind.TypeOf>
        {
            inner: TypeRef;
        }

        /**
         * Represents a reference to a TypeScript "keyof" type (e.g. "keyof Mytype").
         */
        export interface KeyOf extends Base<Kind.KeyOf>
        {
            inner: TypeRef;
        }

        /**
         * Represents a conditional TypeScript type (e.g. T extends U ? V : W)
         */
        export interface Conditional extends Base<Kind.Conditional>
        {
            /** "T" in (T extends U ? V : W) */
            subject: TypeRef;

            /** "U" in (T extends U ? V : W) */
            condition: TypeRef;

            /** "V" in (T extends U ? V : W) */
            pass: TypeRef;

            /** "W" in (T extends U ? V : W) */
            fail: TypeRef;
        }
    }
    
    export type TypeRef =
        TypeRef.Raw | TypeRef.Inline |
        TypeRef.LiteralNumber | TypeRef.LiteralString | TypeRef.LiteralBool |
        TypeRef.WithTypeArgs |
        TypeRef.Array | TypeRef.FixedArray |
        TypeRef.Intersect | TypeRef.Union |
        TypeRef.Function |
        TypeRef.Is |
        TypeRef.Indexed |
        TypeRef.TypeOf | TypeRef.KeyOf |
        TypeRef.Conditional;
}