declare namespace XML2JSParser
{
    export interface BaseXMLNode
    {
        [key: string]: undefined | (XMLNode | string)[];
    }

    export type XMLNode = { $?: { [attr: string]: string; }; _?: string; } & BaseXMLNode;

    export interface RootNode
    {
        [key: string]: XMLNode;
    }

    export type Processor = (value: string) => string;

    export interface Options
    {
        attrkey: string;
        charkey: string;
        explicitCharkey: boolean;
        trim: boolean;
        normalizeTags: boolean;
        normalize: boolean;
        explicitRoot: boolean;
        emptyTag: string;
        explicitArray: boolean;
        ignoreAttrs: boolean;
        mergeAttrs: boolean;
        validator: Function;
        xmlns: boolean;
        explicitChildren: boolean;
        childkey: string;
        preserveChildrenOrder: boolean;
        charsAsChildren: boolean;
        includeWhiteChars: boolean;
        strict: boolean;
        attrNameProcessors: Processor[] | null;
        attrValueProcessors: Processor[] | null;
        tagNameProcessors: Processor[] | null;
        valueProcessors: Processor[] | null;
    }

    export interface Parser
    {
        parseString(xml: string, cb: (err: string, result: XML2JSParser.RootNode) => void): void;
        parseString(xml: string): Promise<XML2JSParser.RootNode>;
        parseStringSync(xml: string): XML2JSParser.RootNode;
    }
}

declare interface XML2JSParser
{
    parseString(xml: string, cb: (err: string, result: XML2JSParser.RootNode) => void): void;
    parseString(xml: string, opts: Partial<XML2JSParser.Options>, cb: (err: string, result: XML2JSParser.RootNode) => void): void;
    parseString(xml: string): Promise<XML2JSParser.RootNode>;
    parseString(xml: string, opts: Partial<XML2JSParser.Options>): Promise<XML2JSParser.RootNode>;
    parseStringSync(xml: string): XML2JSParser.RootNode;
    parseStringSync(xml: string, opts: Partial<XML2JSParser.Options>): XML2JSParser.RootNode;

    new(): XML2JSParser.Parser;
    new(opts: Partial<XML2JSParser.Options>): XML2JSParser.Parser;
}