// Karma configuration
// Generated on Fri Oct 20 2017 11:12:25 GMT+0100 (GMT Daylight Time)
const cwd = process.cwd();

const isPipeline = process.env["CI"] === "true";

const debug = process.env["TESTDEBUG"] === "1";
const localDebug = debug && !isPipeline;

const useSourcemaps = localDebug;
const useCoverage = !isPipeline && !localDebug;
const useIframes = true;

function cleaned(arr)
{
    return arr.filter((item) => item != null);
}

module.exports = function (config)
{
    "use strict";
    config.set({
        basePath: cwd,
        plugins:
        cleaned([
            // frameworks
            "karma-mocha",
            "karma-mocha-reporter",
            "karma-sinon",
            useIframes ? "karma-iframes" : null,

            // reporters
            useCoverage ? "karma-coverage" : null,
            "karma-junit-reporter",

            // preprocessors
            useSourcemaps ? "karma-sourcemap-loader" : null,

            // browsers
            "karma-chrome-launcher",

            // misc
            "karma-chai"
        ]),
        frameworks: cleaned(["mocha", "sinon", useIframes ? "iframes" : null]),
        reporters: cleaned(["mocha", useCoverage ? "coverage" : null, "junit"]),
        client: { mocha: { timeout: 20000 } },
        browsers: [localDebug ? "Chrome" : "ChromeHeadless"],
        port: 9876,
        colors: true,
        logLevel: debug ? config.LOG_DEBUG : config.LOG_INFO,
        autoWatch: false,
        runInParent: !localDebug,
        singleRun: !localDebug,
        concurrency: 1,
        browserNoActivityTimeout: localDebug ? Infinity : 60000,
        failOnEmptyTestSuite: true,
        // reporter options
        mochaReporter: {
            output: "full"
        },
        preprocessors: {
            "tests/*.spec.js": cleaned([useSourcemaps ? "sourcemap" : null, useCoverage ? "coverage" : null, useIframes ? "iframes" : null]),
        },
        coverageReporter: useCoverage ? {
            reporters: [
                { type: "html", subdir: "report-html" }
            ],
            dir: "coverage",
            instrumenter: {
                "tests/*.spec.js": ["istanbul"]
            },
        } : null,
        junitReporter:
        {
            outputDir: "./test-reports/"
        },
        files: [
            "node_modules/chai/chai.js",
            "tests/thirdparty.js",
            "tests/*.spec.js"
        ]
    })
}
