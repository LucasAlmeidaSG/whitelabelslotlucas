namespace RS.Build
{
    const syntax = "jsminify <*.js> <*.js.map> [a=A b=B c=C ...]";
    
    async function doMinify(args: string[])
    {
        Log.debug(args.join(" "));

        await FileSystem.rebuildCache();

        // JS
        const jsPath = args[0];
        if (!Is.string(jsPath))
        {
            console.error(syntax);
            return;
        }
        let rawJS: string;
        try
        {
            rawJS = await FileSystem.readFile(jsPath);
        }
        catch (err)
        {
            console.error(err);
            return;
        }

        // Map
        const mapPath = args[1] === "null" ? null : args[1];
        if (mapPath && !Is.string(mapPath))
        {
            console.error(syntax);
            return;
        }
        let rawMap: string = null;
        if (mapPath != null)
        {
            try
            {
                rawMap = await FileSystem.readFile(mapPath);
            }
            catch (err)
            {
                console.error(err);
                return;
            }
        }

        const map = rawMap == null ? null : JSON.parse(rawMap);
        const compiledFile = new Build.CompiledFile(rawJS, map);
        compiledFile.minify(Path.baseName(jsPath));

        await compiledFile.write(jsPath, mapPath);
    }

    doMinify(process.argv.slice(2));
}