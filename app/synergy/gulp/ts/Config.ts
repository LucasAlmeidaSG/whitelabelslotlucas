namespace RS
{
    /**
     * Build config, as specified in root "config.json".
     */
    export interface Config extends Config.Base
    {
        tasks?: Util.Map<Config.Task>;
    }

    export namespace Config
    {
        export interface Base
        {
            rootModule: string;
            extraModules: string[];
            /** Environment args associated with this config. */
            envArgs: Util.Map<string>;
            moduleConfigs: Util.Map<{ [key: string]: any }>;
            assemblePath: string;
            /** Whether or not source maps should be generated. */
            sourceMap: boolean;
            /** Build-time constants. */
            defines: Util.Map<string | boolean>;
        }

        export interface Task extends Base
        {
            requires?: string[];
            after?: string[];
        }

        let loadedConfig: Config | null = null;
        export let activeConfig: Base | null = null;

        const defaultConfig: Config =
        {
            rootModule: "Game",
            envArgs: {},
            extraModules: [],
            moduleConfigs: {},
            assemblePath: "build",
            defines: {},
            sourceMap: true
        };

        const configPath = "config.json";
        const userConfigPath = "userConfig.json";

        async function mergeUserConfig(config: Config): Promise<Config>
        {
            if (await FileSystem.fileExists(userConfigPath))
            {
                Log.info("userConfig.json found, will merge with config");
                const userConfig = await JSON.parseFile(userConfigPath);
                return assign(userConfig, config, true);
            }
            return config;
        }

        /**
         * Retrieves or loads the config.
         */
        export async function get(): Promise<Config>
        {
            if (loadedConfig) { return loadedConfig; }

            if (await FileSystem.fileExists(configPath))
            {
                const fileConfig = await JSON.parseFile(configPath);
                loadedConfig = await mergeUserConfig({ ...defaultConfig, ...fileConfig});
            }
            else
            {
                Log.warn("No config.json present, using default build configuration");
                loadedConfig = await mergeUserConfig({ ...defaultConfig });
            }

            return loadedConfig;
        }
    }
}