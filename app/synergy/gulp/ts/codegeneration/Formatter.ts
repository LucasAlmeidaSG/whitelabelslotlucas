namespace RS.Build.CodeGeneration
{
    import TypeRef = CodeMeta.TypeRef;
    
    enum EmitLocation { Start, MidLine, StartOfLine, EndOfLine, EndOfScope }

    export class Formatter
    {
        private _chunks: string[] = [];
        private _identLevel: number = 0;
        private _emitLoc: EmitLocation = EmitLocation.Start;

        /** Emit a chunk, do not consider emit location. */
        private emit(chunk: string)
        {
            this._chunks.push(chunk);
        }

        private emitIndent()
        {
            for (let i = 0; i < this._identLevel; i++)
            {
                this.emit("\t");
            }
        }

        /** Prepares the emitter to emit a new line of code. */
        private bringToNewLine()
        {
            switch (this._emitLoc)
            {
                case EmitLocation.MidLine:
                case EmitLocation.EndOfLine:
                case EmitLocation.EndOfScope:
                    this.emit("\n");
                    this.emitIndent();
                    break;
            }
            this._emitLoc = EmitLocation.StartOfLine;
        }

        /** Emits a line of code. */
        public emitLine(line: string)
        {
            this.bringToNewLine();
            this.emit(line);
            this._emitLoc = EmitLocation.EndOfLine;
        }

        /** Emits a new scope. */
        public enterScope(char: string = "{")
        {
            this.bringToNewLine();
            this.emit(char);
            this._identLevel++;
            this._emitLoc = EmitLocation.EndOfScope;
        }

        /** Emits the end of a scope. */
        public exitScope(char: string = "}")
        {
            this._identLevel--;
            this.bringToNewLine();
            this.emit(char);
            this._emitLoc = EmitLocation.EndOfScope;
        }

        /** Emits the specified named object. */
        public emitNamed(obj: Named)
        {
            switch (obj.kind)
            {
                case "interface":
                    this.emitInterface(obj);
                    break;
                case "propertydeclr":
                    this.emitPropertyDeclaration(obj);
                    break;
                case "namespace":
                    this.emitNamespace(obj);
                    break;
                case "var":
                    this.emitVariable(obj);
                    break;
                case "arrval":
                    this.emitArrayValue(obj);
                    break;
                case "objval":
                    this.emitObjectValue(obj);
                    break;
                case "keyval":
                    this.emitKeyValue(obj);
                    break;
                case "codeval":
                    this.emitCodeValue(obj);
                    break;
                case "type":
                    this.emitTypeDef(obj);
                    break;
            }
        }

        /** Emits the specified interface. */
        private emitInterface(obj: Interface)
        {
            if (obj.comment != null) { this.emitLine(`/** ${obj.comment} */`); }
            this.bringToNewLine();
            this.emit(`export interface ${obj.name}`);
            if (obj.extends && obj.extends.length > 0)
            {
                this.emit(` extends `);
                for (let i = 0; i < obj.extends.length; i++)
                {
                    if (i > 0) { this.emit(", "); }
                    this.emitType(obj.extends[i]);
                }
            }
            this._emitLoc = EmitLocation.EndOfLine;
            this.enterScope();
            if (obj.properties != null)
            {
                for (let i = 0; i < obj.properties.length; i++)
                {
                    const property = obj.properties[i];
                    this.emitPropertyDeclaration(property);
                }
            }
            this.exitScope();
        }

        /** Emits the specified property declaration. */
        private emitPropertyDeclaration(obj: PropertyDeclaration)
        {
            if (obj.comment != null) { this.emitLine(`/** ${obj.comment} */`); }
            this.bringToNewLine();
            this.emit(`${obj.name}${obj.optional ? '?' : ''}: `);
            this.emitType(obj.type);
            this.emit(`;`);
            this._emitLoc = EmitLocation.EndOfLine;
        }

        /** Emits the specified namespace. */
        private emitNamespace(obj: Namespace)
        {
            // const isStart = this._emitLoc === EmitLocation.Start;
            const isStart = this._identLevel === 0;
            if (obj.comment != null) { this.emitLine(`/** ${obj.comment} */`); }
            this.emitLine(`${!isStart ? "export " : ""}namespace ${obj.name}`);
            this.enterScope();
            if (obj.children != null)
            {
                for (let i = 0; i < obj.children.length; i++)
                {
                    const child = obj.children[i];
                    this.emitNamed(child);
                }
            }
            this.exitScope();
        }

        /** Emits the specified variable. */
        private emitVariable(obj: Variable)
        {
            if (obj.comment != null) { this.emitLine(`/** ${obj.comment} */`); }
            this.bringToNewLine();
            this.emit(`export ${obj.isConst ? "const" : "let"} ${obj.name}`);
            if (obj.type != null)
            {
                this.emit(`: `);
                this.emitType(obj.type);
            }
            if (obj.value != null)
            {
                this.emit(` = `);
                this.emitValue(obj.value);
            }
            this.emit(";");
            this._emitLoc = EmitLocation.EndOfLine;
        }

        /** Emits the specified value. */
        private emitValue(obj: Value)
        {
            if (Is.string(obj))
            {
                this.emit(obj);
            }
            else
            {
                this.emitNamed(obj);
            }
        }

        /** Emits the specified array value. */
        private emitArrayValue(obj: ArrayValue)
        {
            this.enterScope("[");
            if (obj.values)
            {
                for (let i = 0; i < obj.values.length; i++)
                {
                    if (i > 0) { this.emit(","); }
                    this.bringToNewLine();
                    this.emitValue(obj.values[i]);
                }
            }
            this.exitScope("]");
        }

        /** Emits the specified object value. */
        private emitObjectValue(obj: ObjectValue)
        {
            this.enterScope("{");
            if (obj.values)
            {
                for (let i = 0; i < obj.values.length; i++)
                {
                    if (i > 0) { this.emit(","); }
                    this.bringToNewLine();
                    this.emitValue(obj.values[i]);
                    this._emitLoc = EmitLocation.EndOfLine;
                }
            }
            this.exitScope("}");
        }

        /** Emits the specified key-value pair. */
        private emitKeyValue(obj: KeyValue)
        {
            const numKey = parseInt(obj.name);
            if (Is.number(numKey))
            {
                this.emit(`[${numKey}]`);
            }
            else
            {
                this.emit(obj.name);
            }
            this.emit(": ");
            this.emitValue(obj.value);
        }

        /** Emits the specified code value. */
        private emitCodeValue(obj: CodeValue)
        {
            for (let i = 0; i < obj.pieces.length; i++)
            {
                const piece = obj.pieces[i];
                if (Is.string(piece))
                {
                    this.emit(piece);
                }
                else if (isNamed(piece))
                {
                    this.emitNamed(piece);
                }
                else if (isTypeRef(piece))
                {
                    this.emitType(piece);
                }
            }
            this._emitLoc = EmitLocation.MidLine;
        }

        /** Emits the specified typedef. */
        private emitTypeDef(obj: Type)
        {
            this.bringToNewLine();
            if (obj.comment != null) { this.emitLine(`/** ${obj.comment} */`); }
            this.emit("export type ");
            this.emit(obj.name);
            this.emit(" = ");
            this.emitType(obj.type);
            this.emit(";");
            this._emitLoc = EmitLocation.EndOfLine;
        }

        /** Emits the specified type. */
        public emitType(type: TypeRef)
        {
            this.emit(stringifyType(type));
            this._emitLoc = EmitLocation.MidLine;
        }

        /** Serialises all emmitted code to a string. */
        public toString()
        {
            return this._chunks.join("");
        }
    }
}