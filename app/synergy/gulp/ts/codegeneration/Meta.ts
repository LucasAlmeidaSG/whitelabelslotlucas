namespace RS.Build.CodeGeneration
{
    import TypeRef = CodeMeta.TypeRef;
    import AccessQualifier = CodeMeta.AccessQualifier;
    import StorageQualifier = CodeMeta.StorageQualifier;
    import GenericTypeArg = CodeMeta.GenericTypeArg;
    import FunctionParameter = CodeMeta.FunctionParameter;
    import FunctionSignature = CodeMeta.FunctionSignature;

    /**
     * Represents some kind of named TypeScript construct.
     */
    export interface NamedBase
    {
        /** Kind of object. */
        kind: string;

        /** Name of the object. */
        name: string;

        /** Documentation comment for the property. */
        comment?: string;
    }

    /**
     * Represents a TypeScript property to export.
     */
    export interface PropertyDeclaration extends NamedBase
    {
        kind: "propertydeclr";

        /** Is the property abstract? */
        isAbstract?: boolean;

        /** Is the property readonly? */
        isReadonly?: boolean;

        /** Access qualifier of the property. If not set, assume public. */
        accessQualifier?: AccessQualifier;

        /** Storage qualifier of the property. If not set, assume instance. */
        storageQualifier?: StorageQualifier;

        /** Is the property optional? */
        optional?: boolean;

        /** Type of the property. */
        type: TypeRef;
    }

    /**
     * Represents a TypeScript interface to export.
     */
    export interface Interface extends NamedBase
    {
        kind: "interface";

        /** Generic type arguments to this interface. */
        genericTypeArgs?: GenericTypeArg[];

        /** Base interfaces to extend. */
        extends?: TypeRef[];

        /** Properties contained within this interface. */
        properties?: PropertyDeclaration[];

        /** Methods contained within this interface. */
        methods?: MethodDeclaration[];
    }

    /**
     * Represents a TypeScript class to export.
     */
    export interface Class extends NamedBase
    {
        kind: "class";

        /** Is the class abstract? */
        isAbstract?: boolean;

        /** Generic type arguments to this class. */
        genericTypeArgs?: GenericTypeArg[];

        /** Base classes to extend. */
        extends?: TypeRef[];

        /** Interfaces to extend. */
        implements?: TypeRef[];

        /** Properties contained within this class. */
        properties?: PropertyDeclaration[];

        /** Methods contained within this class. */
        methods?: MethodDeclaration[];
    }

    /**
     * Represents a TypeScript enum to export.
     */
    export interface Enum extends NamedBase
    {
        kind: "enum";

        /** Is it a const enum? */
        isConst?: boolean;

        /** Enumerators. */
        enumerators?: { [name: string]: number|string };
    }

    /**
     * Represents a TypeScript type to export (e.g. "export type X = number").
     */
    export interface Type extends NamedBase
    {
        kind: "type";

        /** Generic type arguments to this type. */
        genericTypeArgs?: GenericTypeArg[];

        /** The actual type that this type refers to. */
        type?: TypeRef;
    }

    /**
     * Represents a TypeScript namespace to export.
     */
    export interface Namespace extends NamedBase
    {
        kind: "namespace";

        /** Children of this namespace. */
        children?: Named[];
    }

    /**
     * Represents a TypeScript const object to export.
     */
    export interface Variable extends NamedBase
    {
        kind: "var";

        /** Type of the variable. */
        type?: TypeRef;

        /** Is it const? */
        isConst?: boolean;

        /** Value of the variable. */
        value?: Value;
    }

    /**
     * Represents a TypeScript function to export.
     */
    export interface FunctionDeclaration extends NamedBase
    {
        kind: "func";

        /** Signature of the function. */
        signature: FunctionSignature;
    }

    /**
     * Represents a TypeScript method (member function) to export.
     */
    export interface MethodDeclaration extends NamedBase
    {
        kind: "method";

        /** Is the method abstract? Only relevant inside a class. */
        isAbstract?: boolean;

        /** Access qualifier of the method. If not set, assume public. */
        accessQualifier?: AccessQualifier;

        /** Storage qualifier of the method. If not set, assume instance. */
        storageQualifier?: StorageQualifier;

        /** Is the method optional? */
        optional?: boolean;

        /** Signature of the function. */
        signature: FunctionSignature;
    }


    /**
     * Represents a TypeScript array value to export.
     */
    export interface ArrayValue extends NamedBase
    {
        kind: "arrval";

        /** Contents of the array. */
        values?: Value[];
    }

    /**
     * Represents a TypeScript object value to export.
     */
    export interface ObjectValue extends NamedBase
    {
        kind: "objval";

        /** Contents of the object. */
        values?: KeyValue[];
    }

    /**
     * Represents a TypeScript key-value pair to export.
     */
    export interface KeyValue extends NamedBase
    {
        kind: "keyval";

        value: Value;
    }

    /**
     * Represents an arbitrary piece of code to export.
     */
    export interface CodeValue extends NamedBase
    {
        kind: "codeval";

        /** Pieces to emit in sequence. */
        pieces: (string | TypeRef | Named)[];
    }

    export type Value = string | Named;

    export type Named = PropertyDeclaration | Interface | Class | Enum | Type | Namespace | Variable | FunctionDeclaration | ArrayValue | ObjectValue | KeyValue | CodeValue;

    export function isNamed(obj: any): obj is Named
    {
        return obj != null && Is.string(obj.kind) && (
            obj.kind === "propertydeclr" ||
            obj.kind === "interface" ||
            obj.kind === "class" ||
            obj.kind === "enum" ||
            obj.kind === "type" ||
            obj.kind === "namespace" ||
            obj.kind === "var" ||
            obj.kind === "func" ||
            obj.kind === "arrval" ||
            obj.kind === "objval" ||
            obj.kind === "keyval" ||
            obj.kind === "codeval"
        );
    }

    export function isTypeRef(obj: any): obj is TypeRef
    {
        return obj != null && Is.string(obj.kind) && (
            obj.kind === "basictyperef" ||
            obj.kind === "inlinetyperef" ||
            obj.kind === "litnumtyperef" ||
            obj.kind === "litstrtyperef" ||
            obj.kind === "litbooltyperef" ||
            obj.kind === "generictyperef" ||
            obj.kind === "arraytyperef" ||
            obj.kind === "fixedtyperef" ||
            obj.kind === "intersecttyperef" ||
            obj.kind === "uniontyperef" ||
            obj.kind === "functiontyperef" ||
            obj.kind === "istyperef" ||
            obj.kind === "indexedtyperef" ||
            obj.kind === "typeoftyperef"
        );
    }
}