namespace RS.Build.CodeGeneration
{
    import TypeRef = CodeMeta.TypeRef;

    const debugMode = false;

    export const anyType: TypeRef = { kind: TypeRef.Kind.Raw, name: "any" };

    /**
     * Gets if both type references are EQUAL.
     */
    export function typesAreEqual(a: TypeRef, b: TypeRef): boolean
    {
        // Note: We check that the kinds match later down the line rather than early-out up here
        // This is so TS can narrow BOTH a and b to that kind
        switch (a.kind)
        {
            case TypeRef.Kind.Raw:
            {
                // TODO: Handle case where b inherits/extends a
                if (b.kind !== TypeRef.Kind.Raw) { return false; }
                return a.name === b.name;
            }

            case TypeRef.Kind.WithTypeArgs:
            {
                if (b.kind !== TypeRef.Kind.WithTypeArgs) { return false; }
                return typesAreEqual(a.inner, b.inner) && typeArraysAreEqual(a.genericTypeArgs, b.genericTypeArgs, true);
            }

            case TypeRef.Kind.Array:
            {
                if (b.kind !== TypeRef.Kind.Array) { return false; }
                return typesAreEqual(a.inner, b.inner);
            }

            case TypeRef.Kind.Intersect:
            {
                if (b.kind !== TypeRef.Kind.Intersect) { return false; }
                return typeArraysAreEqual(a.inners, b.inners);
            }

            case TypeRef.Kind.Union:
            {
                if (b.kind !== TypeRef.Kind.Union) { return false; }
                return typeArraysAreEqual(a.inners, b.inners);
            }

            case TypeRef.Kind.Inline:
            {
                if (b.kind !== TypeRef.Kind.Inline) { return false; }
                if (a.properties)
                {
                    if (!b.properties) { return false; }
                    if (!propertyArraysAreEqual(a.properties, b.properties)) { return false; }
                }
                else
                {
                    if (b.properties) { return false; }
                }
                if (a.methods)
                {
                    if (!b.methods) { return false; }
                    if (!methodArraysAreEqual(a.methods, b.methods)) { return false; }
                }
                else
                {
                    if (b.methods) { return false; }
                }
                return true;
            }
        }

        // Unknown kind, return false
        return false;
    }

    /**
     * Gets if both property declarations are EQUAL.
     * @param a 
     * @param b 
     */
    export function propertiesAreEqual(a: CodeMeta.PropertyDeclaration, b: CodeMeta.PropertyDeclaration): boolean
    {
        return a.name === b.name && a.accessQualifier === b.accessQualifier && a.isAbstract === b.isAbstract && a.optional === b.optional && a.storageQualifier === b.storageQualifier && typesAreEqual(a.type, b.type);
    }

    /**
     * Gets if both arrays of property declarations are EQUAL.
     * @param a 
     * @param b 
     * @param orderMatters 
     */
    export function propertyArraysAreEqual(a: CodeMeta.PropertyDeclaration[], b: CodeMeta.PropertyDeclaration[], orderMatters: boolean = false): boolean
    {
        // Check they have the same number of items
        if (a.length !== b.length) { return false; }

        if (orderMatters)
        {
            // Check that every item matches
            for (let i = 0; i < a.length; i++)
            {
                if (!propertiesAreEqual(a[i], b[i])) { return false; }
            }
        }
        else
        {
            // Check that each item in A is found in B
            for (let i = 0; i < a.length; i++)
            {
                if (!b.some(item => propertiesAreEqual(a[i], item)))
                {
                    return false;
                }
            }
        }

        // Done
        return true;
    }

    /**
     * Gets if both function signatures are EQUAL.
     * @param a 
     * @param b 
     */
    export function functionSignaturesAreEqual(a: CodeMeta.FunctionSignature, b: CodeMeta.FunctionSignature): boolean
    {
        if (a.isConstructor !== b.isConstructor) { return false; }
        if (a.returnType)
        {
            if (!b.returnType) { return false; }
            if (!typesAreEqual(a.returnType, b.returnType)) { return false; }
        }
        else
        {
            if (b.returnType) { return false; }
        }
        // TODO
        throw new Error("Not implemented yet!");
        //return true;
    }

    /**
     * Gets if both method declarations are EQUAL.
     * @param a 
     * @param b 
     */
    export function methodsAreEqual(a: CodeMeta.MethodDeclaration, b: CodeMeta.MethodDeclaration): boolean
    {
        return a.name === b.name && a.accessQualifier === b.accessQualifier && a.isAbstract === b.isAbstract && a.optional === b.optional && a.storageQualifier === b.storageQualifier && functionSignaturesAreEqual(a.signature, b.signature);
    }

    /**
     * Gets if both arrays of method declarations are EQUAL.
     * @param a 
     * @param b 
     * @param orderMatters 
     */
    export function methodArraysAreEqual(a: CodeMeta.MethodDeclaration[], b: CodeMeta.MethodDeclaration[], orderMatters: boolean = false): boolean
    {
        // Check they have the same number of items
        if (a.length !== b.length) { return false; }

        if (orderMatters)
        {
            // Check that every item matches
            for (let i = 0; i < a.length; i++)
            {
                if (!methodsAreEqual(a[i], b[i])) { return false; }
            }
        }
        else
        {
            // Check that each item in A is found in B
            for (let i = 0; i < a.length; i++)
            {
                if (!b.some(item => methodsAreEqual(a[i], item)))
                {
                    return false;
                }
            }
        }

        // Done
        return true;
    }

    /**
     * Gets if both arrays of type references are EQUAL.
     */
    export function typeArraysAreEqual(a: TypeRef[], b: TypeRef[], orderMatters: boolean = false): boolean
    {
        // Check they have the same number of items
        if (a.length !== b.length) { return false; }

        if (orderMatters)
        {
            // Check that every item matches
            for (let i = 0; i < a.length; i++)
            {
                if (!typesAreEqual(a[i], b[i])) { return false; }
            }
        }
        else
        {
            // Check that each item in A is found in B
            for (let i = 0; i < a.length; i++)
            {
                if (!b.some(item => typesAreEqual(a[i], item)))
                {
                    return false;
                }
            }
        }

        // Done
        return true;
    }

    /**
     * Returns a new type reference that represents a narrowed by b.
     * Returns null if narrowing failed.
     */
    export function narrowType(a: TypeRef, b: TypeRef): TypeRef|null
    {
        const result = narrowTypeInternal(a, b);
        if (debugMode)
        {
            
            const formatter1 = new Formatter();
            formatter1.emitType(a);
            const formatter2 = new Formatter();
            formatter2.emitType(b);
            if (result == null)
            {
                console.log(`Failed to narrow '${formatter1.toString()}' by '${formatter2.toString()}'`);
            }
            else
            {
                const formatter3 = new Formatter();
                formatter3.emitType(result);
                console.log(`Narrowed '${formatter1.toString()}' by '${formatter2.toString()}' into '${formatter3.toString()}'`);
            }
        }
        return result;
    }

    /**
     * Returns a new type reference that represents a narrowed by b.
     * Returns null if narrowing failed.
     */
    function narrowTypeInternal(a: TypeRef, b: TypeRef): TypeRef|null
    {
        // We want to find a type that satisfies both a and b.
        // We start with a, and then reduce it to match b.

        // Edge case: If b is a union type and any of it's inners match a exactly, return a
        if (b.kind === TypeRef.Kind.Union && a.kind !== TypeRef.Kind.Union)
        {
            // e.g. a = "string", b: "number | string | boolean", expected output = "string"
            for (let i = 0; i < b.inners.length; i++)
            {
                if (typesAreEqual(a, b.inners[i]))
                {
                    return {...a}; // TODO: Deep clone?
                }
            }

            // Now check if a narrows to anything in b
            // e.g. a = "DerivedInterface", b: "number | BaseInterface | string", expected output = "BaseInterface"
            for (let i = 0; i < b.inners.length; i++)
            {
                const narrowedInner = narrowType(a, b.inners[i]);
                if (narrowedInner != null)
                {
                    return narrowedInner;
                }
            }

            // Never mind, carry on with standard logic
        }

        // Edge case: If b is a intersect type, a has to satisfy or reduce to EVERY inner of b
        if (b.kind === TypeRef.Kind.Intersect)
        {
            let currentNarrowed: TypeRef = a;
            for (let i = 0; i < b.inners.length; i++)
            {
                const bInner = b.inners[i];

                // If they match exactly, we're good on this inner
                if (typesAreEqual(currentNarrowed, bInner)) { continue; }

                // Attempt to narrow it, if it fails, fail everything
                currentNarrowed = narrowType(currentNarrowed, bInner);
                if (currentNarrowed == null) { return null; }
            }

            // Return the new narrowed type
            if (currentNarrowed === a)
            {
                return {...a};  // TODO: Deep clone?
            }
            else
            {
                return currentNarrowed;
            }
        }

        switch (a.kind)
        {
            case TypeRef.Kind.Raw:
            {
                // We can't narrow this any further so only return if it matches b exactly
                if (b.kind === TypeRef.Kind.Raw && a.name === b.name)
                {
                    return {...b};
                }
                else
                {
                    // TODO: Handle case where b inherits/extends a
                    return null;
                }                
            }

            case TypeRef.Kind.WithTypeArgs:
            {
                // We can only narrow this if b is a generic also, refers to the same base type, and has the same amount of generic args
                if (b.kind !== TypeRef.Kind.WithTypeArgs || !typesAreEqual(a.inner, b.inner) || a.genericTypeArgs.length !== b.genericTypeArgs.length) { return null; }

                // Narrow each generic type arg
                const newGenericTypeArgs: TypeRef[] = [];
                for (let i = 0; i < a.genericTypeArgs.length; i++)
                {
                    const narrowedTypeArg = narrowType(a.genericTypeArgs[i], b.genericTypeArgs[i]);
                    if (narrowedTypeArg == null)
                    {
                        // Failed to narrow, so fail the whole thing
                        return null;
                    }
                    newGenericTypeArgs.push(narrowedTypeArg);
                }

                // Success
                return {
                    kind: TypeRef.Kind.WithTypeArgs,
                    inner: a,
                    genericTypeArgs: newGenericTypeArgs
                };
            }

            case TypeRef.Kind.Array:
            {
                // TODO: Handle narrowing to ArrayLike<T>?

                // We can only narrow this if b is an array also
                if (b.kind !== TypeRef.Kind.Array) { return null; }

                // Attempt to narrow the inners
                const narrowedInner = narrowType(a.inner, b.inner);
                if (narrowedInner == null) { return null; }
                
                // Success
                return {
                    kind: TypeRef.Kind.Array,
                    inner: narrowedInner
                };
            }

            case TypeRef.Kind.Union:
            case TypeRef.Kind.Intersect:
            {
                const newInners: TypeRef[] = [];

                // Is b an union type?
                if (b.kind === TypeRef.Kind.Union)
                {
                    // Both types are union types - find all inners that satisfy BOTH
                    // e.g. a = "string | number", b = "number | boolean | string[]", expected output = "number"

                    for (let i = 0; i < a.inners.length; i++)
                    {
                        const aInner = a.inners[i];
                        let narrowedInner: TypeRef|null = null;

                        // Find an equivalent b inner
                        for (let j = 0; j < b.inners.length; j++)
                        {
                            const bInner = b.inners[j];
                            if (typesAreEqual(aInner, bInner))
                            {
                                // Success
                                narrowedInner = {...bInner};
                                break;
                            }
                        }

                        // If not found, now try to narrow against each b inner
                        if (narrowedInner == null)
                        {
                            for (let j = 0; j < b.inners.length; j++)
                            {
                                const bInner = b.inners[j];
                                narrowedInner = narrowType(aInner, bInner);
                                if (narrowedInner != null) { break; }
                            }
                        }

                        // Found?
                        if (narrowedInner != null)
                        {
                            newInners.push(narrowedInner);
                        }
                    }

                    // 
                }
                else
                {
                    // Iterate each inner type of a, attempt to narrow it
                    for (let i = 0; i < a.inners.length; i++)
                    {
                        const narrowedInner = narrowType(a.inners[i], b);
                        if (narrowedInner != null)
                        {
                            newInners.push(narrowedInner);
                        }
                    }
                }

                // Return based on how many new inners we found
                removeDuplicateTypes(newInners);
                if (newInners.length === 0) { return null; }
                if (newInners.length === 1)
                {
                    return {...b}; // TODO: Deep clone?
                }
                else
                {
                    return <TypeRef.Intersect | TypeRef.Union> {
                        kind: a.kind,
                        inners: newInners
                    };
                }
            }
        }


    }

    /**
     * Removes any duplicate types from the specified type array.
     */
    export function removeDuplicateTypes(typeArr: TypeRef[])
    {
        for (let i = typeArr.length - 1; i >= 0; i--)
        {
            for (let j = i - 1; j >= 0; j--)
            {
                if (typesAreEqual(typeArr[i], typeArr[j]))
                {
                    typeArr.splice(i, 1);
                    break;
                }
            }
        }
    }

    /**
     * Creates a new type that can represent ANY of the specified types.
     * Returns null if no such type can be found.
     */
    export function getUnion(typeArr: TypeRef[]): TypeRef
    {
        // Attempt to narrow all types to the same type
        let currentType: TypeRef|null = typeArr[0];
        for (let i = 1; i < typeArr.length; i++)
        {
            currentType = narrowType(currentType, typeArr[1]);
            if (currentType == null) { break; }
        }
        if (currentType) { return currentType; }

        // Failing that, return a union type
        const newInners = [...typeArr];
        removeDuplicateTypes(newInners);
        if (newInners.length === 0) { return null; }
        if (newInners.length === 1) { return {...newInners[0]}; } // TODO: Deep clone?
        return {
            kind: TypeRef.Kind.Union,
            inners: newInners
        };
    }

    /**
     * Creates a new type that can represent BOTH of the specified types.
     * @param a 
     * @param b 
     */
    export function intersect(a: TypeRef | null, b: TypeRef | null): TypeRef | null
    {
        if (a == null) { return b; }
        if (b == null) { return a; }
        if (a.kind === TypeRef.Kind.Intersect)
        {
            if (b.kind === TypeRef.Kind.Intersect)
            {
                return { kind: TypeRef.Kind.Intersect, inners: [ ...a.inners, ...b.inners ] };
            }
            else
            {
                return { kind: TypeRef.Kind.Intersect, inners: [ ...a.inners, b ] };
            }
        }
        else
        {
            if (b.kind === TypeRef.Kind.Intersect)
            {
                return { kind: TypeRef.Kind.Intersect, inners: [ a, ...b.inners ] };
            }
            else
            {
                return { kind: TypeRef.Kind.Intersect, inners: [ a, b ] };
            }
        }
    }

    /**
     * Makes a name safe for emitting as an identifier.
     */
    export function sanitiseIdentifier(id: string)
    {
        return id
            .replace(/[-\.\s]/g, "_")
            .replace(/_+/g, "_");
    }
    
    /**
     * Gets a string representation of the specified type.
     * @param type 
     */
    export function stringifyType(type: TypeRef = null, useBrackets: boolean = false): string
    {
        let value: string;
        let brackets: boolean;
        if (type == null)
        {
            value = "any";
            brackets = false;
        }
        else
        {
            switch (type.kind)
            {
                case TypeRef.Kind.Raw:
                    value = type.name;
                    brackets = false;
                    break;
                case TypeRef.Kind.LiteralNumber:
                    value = type.value.toString();
                    brackets = false;
                    break;
                case TypeRef.Kind.LiteralString:
                    value = type.value;
                    brackets = false;
                    break;
                case TypeRef.Kind.LiteralBool:
                    value = type.value ? "true" : "false";
                    brackets = false;
                    break;
                case TypeRef.Kind.WithTypeArgs:
                    value = `${stringifyType(type.inner, true)}<${type.genericTypeArgs.map(t => stringifyType(t)).join(", ")}>`;
                    brackets = false;
                    break;
                case TypeRef.Kind.Array:
                {
                    value = `${stringifyType(type.inner, true)}[]`;
                    brackets = false;
                    break;
                }
                case TypeRef.Kind.FixedArray:
                    value = `[${type.inners.map(t => stringifyType(t)).join(", ")}]`;
                    brackets = false;
                    break;
                case TypeRef.Kind.Intersect:
                    value = type.inners.map(t => stringifyType(t, true)).join(" & ");
                    brackets = true;
                    break;
                case TypeRef.Kind.Union:
                    value = type.inners.map(t => stringifyType(t, true)).join(" | ");
                    brackets = true;
                    break;
                case TypeRef.Kind.Function:
                    value = `(${type.signature.parameters.map(p => `${p.name}: ${stringifyType(p.type)}`).join(", ")}) => ${stringifyType(type.signature.returnType)}`;
                    brackets = true;
                    break;
                case TypeRef.Kind.Is:
                    value = `${type.parameterName} is ${stringifyType(type.inner, true)}`;
                    brackets = true;
                    break;
                case TypeRef.Kind.Inline:
                    const segs: string[] = [];
                    segs.push("{ ");
                    if (type.properties)
                    {
                        for (const prop of type.properties)
                        {
                            segs.push(prop.name);
                            segs.push(": ");
                            segs.push(stringifyType(prop.type, false));
                            segs.push("; ");
                        }
                    }
                    if (type.methods)
                    {
                        for (const method of type.methods)
                        {
                            segs.push(method.name);
                            segs.push(": ");
                            segs.push("Function"); // TODO: This!
                            segs.push("; ");
                        }
                    }
                    segs.push("}");
                    value = segs.join("");
                    brackets = false;
                    break;
                default:
                    value = "any";
                    brackets = false;
                    break;
            }
        }
        if (brackets && useBrackets)
        {
            return `(${value})`;
        }
        else
        {
            return value;
        }
    }

    /**
     * Attempts to normalise the specified TypeRef, turning any "relative" types into "absolute" types.
     * For instance, the simple type "My.Class" would normalise to "RS.My.Class" if referenced from within the "RS" namespace AND if "RS.My.Class" exists.
     * The normalised type should be resolvable given the global namespace.
     * Unidentifiable types are left alone.
     * @param type The type to resolve
     * @param namespaceHierarchy Path of namespaces leading to the location of the type. e.g. for the above example, [RS]
     */
    export function normaliseTypeRef(type: TypeRef, namespaceHierarchy: Namespace[]): void
    {
        switch (type.kind)
        {
            case TypeRef.Kind.Raw:
                // Search each namespace in turn for it
                for (let i = namespaceHierarchy.length - 1; i >= 0; ++i)
                {
                    const ns = namespaceHierarchy[i];
                    const result = searchForNamed(ns, type.name);
                    if (result)
                    {
                        // We found it, grab a full name
                        const nsPath = namespaceHierarchy.slice(0, i + 1).map(n => n.name).join(".");
                        Log.debug(`normalised ${type.name} to ${nsPath}.${type.name}`);
                        type.name = `${nsPath}.${type.name}`;
                        break;
                    }
                }
                break;
            case TypeRef.Kind.TypeOf:
                normaliseTypeRef(type.inner, namespaceHierarchy);
                break;
        }
    }

    /**
     * Searches a hierarchy for the specified Named.
     * @param ns 
     * @param typeName 
     */
    export function searchForNamed(rootNode: Named, typeName: string): { value: Named; prefix: string; }|null
    {
        if (!typeName) { return { value: rootNode, prefix: "" }; }
        const spl = typeName.split(".");
        switch (rootNode.kind)
        {
            case "namespace":
                const candidates = rootNode.children.filter(c => c.name === spl[0]);
                for (const candidate of candidates)
                {
                    const result = searchForNamed(candidate, spl.slice(1).join("."));
                    if (result) { return { value: result.value, prefix: `${result.prefix}${rootNode.name}.` }; }
                }
                return null;
            // TODO: Handle other types
            default:
                return null;
        }
    }
}