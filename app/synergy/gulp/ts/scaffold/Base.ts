namespace RS.Build
{
    export interface TemplateParam
    {
        displayName?: string;
        example?: string;
        defaultValue?: string;
    }

    export interface TemplateInfo
    {
        source: string;
        outputPath: string;
    }

    export class Scaffold
    {
        protected _templates: TemplateInfo[] = [];
        protected _params: Util.Map<TemplateParam> = {};

        public constructor() {}

        public addTemplate(source: string, outputPath: string): void
        {
            this._templates.push({ source, outputPath });
        }

        public addParam(key: string, displayName?: string, example?: string, defaultValue?: string): void
        {
            this._params[key] = { displayName, example, defaultValue };
        }

        protected async promptForParams()
        {
            const result: Util.Map<string> = {};
            for (const key in this._params)
            {
                const paramInfo = this._params[key];
                const prompt: string[] = [ `Enter ${paramInfo.displayName || key}` ];
                if (paramInfo.defaultValue)
                {
                    prompt.push(`(or blank for '${paramInfo.defaultValue}')`);
                }
                if (paramInfo.example)
                {
                    prompt.push(`(e.g. '${paramInfo.example}')`);
                }
                prompt.push(":");
                
                
                let value: string | null = null;
                while (!value)
                {
                    Log.info(prompt.join(" "));
                    value = await Input.readLine();
                    if (!value)
                    {
                        value = paramInfo.defaultValue;
                    }
                    if (!value)
                    {
                        Log.warn(`${paramInfo.displayName || key} is required.`);
                    }
                }
                result[key] = value;
            }
            return result;
        }

        public async generate(targetModule: Module.Base)
        {
            if (!targetModule.info.primarynamespace)
            {
                Log.error(`Module '${targetModule.name}' does not have a "primarynamespace" entry in module info. Scaffolding failed.`);
                return false;
            }
            const args = await this.promptForParams();
            args["namespace"] = targetModule.info.primarynamespace;
            for (const template of this._templates)
            {
                const src = this.applyArgs(template.source, args);
                const path = this.applyArgs(Path.combine(targetModule.path, template.outputPath), args);
                await FileSystem.createPath(Path.directoryName(path));
                if (await FileSystem.fileExists(path))
                {
                    Log.error(`Didn't create '${path}' (already exists)!`);
                }
                else
                {
                    await FileSystem.writeFile(path, src);
                    Log.info(`Created '${path}'`);
                }
            }
        }

        private static _regexp = /\{\{([a-zA-Z0-9]+)(:[a-zA-Z0-9]+)?\}\}/g;

        protected applyArgs(str: string, args: Util.Map<string>): string
        {
            return str.replace(Scaffold._regexp, (whole: string, argName: string, argFlag?: string) =>
            {
                if (argFlag) { argFlag = argFlag.substr(1); }
                return this.serialiseArg(args[argName], argFlag);
            });
        }

        protected serialiseArg(argValue?: string, argFlag?: string): string
        {
            if (argValue == null)
            {
                argValue = "missing";
            }
            if (argFlag && argFlag.toLowerCase() === "camelcase")
            {
                // camelcase
                // Camelcase
                // camelCase
                // CamelCase

                const frontUpper = argFlag[0] === "C";
                const backUpper = argFlag[5] === "C";

                for (let i = 1; i < 9; ++i)
                {
                    if (i !== 5 && argFlag[i].toLowerCase() !== argFlag[i])
                    {
                        Log.warn(`Unsupported flag '${argFlag}' in scaffold template`);
                        return argValue;
                    }
                }

                const segments = argValue.split(".");
                return segments.map(s =>
                {
                    let isFront = true;
                    const words = argValue.split(/(?=[A-Z])/g);
                    return words.map(w =>
                    {
                        const isCaps = isFront ? frontUpper : backUpper;
                        isFront = false;
                        return `${isCaps ? w[0].toUpperCase() : w[0].toLowerCase()}${w.substr(1)}`;
                    }).join("");
                }).join(".");
            }
            return argValue;
        }
    }
}