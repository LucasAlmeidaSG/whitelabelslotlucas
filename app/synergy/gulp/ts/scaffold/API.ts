/// <reference path="Base.ts" />

namespace RS.Build
{
    export interface ScaffoldInfo
    {
        scaffold: Scaffold;
        description?: string;
    }

    const scaffolds: Util.Map<ScaffoldInfo> = {};

    export function registerScaffold(type: string, scaffold: Scaffold, description?: string)
    {
        type = type.toLowerCase();
        scaffolds[type] = { scaffold, description };
    }

    export function findScaffold(type: string): ScaffoldInfo|null
    {
        type = type.toLowerCase();
        return scaffolds[type];
    }

    export function getAllScaffolds(): Readonly<Util.Map<ScaffoldInfo>>
    {
        return scaffolds;
    }
}