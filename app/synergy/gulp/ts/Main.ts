/// <reference path="util/Task.ts" />
/// <reference path="util/Log.ts" />
/// <reference path="module/Loader.ts" />
/// <reference path="tasks/Assemble.ts" />
/// <reference path="Config.ts" />

module.exports.RS = RS;

namespace RS
{
    /** Path to gulplib.js. */
    export const __gulplib = __filename;
}

namespace RS.Tasks
{
    // Set up the init task
    const Init = Build.initTask(
        async () =>
        {
            const compatibleNodes = [Version(10), Version(11)];
            const nodeVersion = Version.fromString(process.version);
            if (!compatibleNodes.some((compatibleVersion) => Version.compatible(nodeVersion, compatibleVersion)))
            {
                let highestVer = compatibleNodes[0];
                for (let i = 1; i < compatibleNodes.length; i++)
                {
                    const ver = compatibleNodes[i];
                    if (Version.greaterThan(ver, highestVer)) { highestVer = ver; }
                }
                Log.warn(`Synergy does not support Node.js ${process.version}; please install Node.js ^v${Version.toString(highestVer)}.`);
                Log.warn(`Some things may work incorrectly, or not at all.`);
            }

            Profile.reset();
            await FileSystem.rebuildCache();

            // Load config
            const config = await Config.get();

            // Load modules
            await Module.loadModules();

            // Identify config tasks
            if (config.tasks)
            {
                let foundNum = 0;
                for (let i = 2, l = process.argv.length; i < l; ++i)
                {
                    const arg = process.argv[i];
                    let task: Config.Task;
                    if (task = config.tasks[arg])
                    {
                        if (foundNum === 1)
                        {
                            Log.error(`Attempting to run more than one task from the tasks section of the config. Only the first one will execute!`);
                            break;
                        }

                        Config.activeConfig = {
                            rootModule: task.rootModule || config.rootModule,
                            extraModules: [ ...(config.extraModules || []), ...(task.extraModules || []) ],
                            moduleConfigs: { ...(config.moduleConfigs || {}), ...(task.moduleConfigs || {}) },
                            envArgs: { ...(config.envArgs || {}), ...(task.envArgs || {}) },
                            assemblePath: task.assemblePath || config.assemblePath,
                            sourceMap: task.sourceMap != null ? task.sourceMap : config.sourceMap,
                            defines: { ...(config.defines || {}), ...(task.defines || {}) }
                        };
                        Build.task({
                            name: arg,
                            requires: (task.requires || []).map(str => Build.getTask(str)),
                            after: (task.after || []).map(str => Build.getTask(str))
                        });

                        foundNum++;
                    }
                }
            }
            if (Config.activeConfig == null)
            {
                Config.activeConfig = config;
            }

            // Apply env args
            for (const envArg in RS.Config.activeConfig.envArgs)
            {
                const val = RS.Config.activeConfig.envArgs[envArg];
                process.env[envArg] = val;
            }

            // Look for root module
            const rootModule = Module.getModule(Config.activeConfig.rootModule);
            if (rootModule != null)
            {
                Log.info(`Using '${rootModule.name}' as root module`);

                // Gather all dependent modules
                const allDeps = Module.getDependencySet([ Config.activeConfig.rootModule, ...Config.activeConfig.extraModules ]);

                // Compile build components
                await Module.compileBuildComponents(allDeps);

                // Load build components
                for (const dep of allDeps.data)
                {
                    if (!dep.buildLoaded)
                    {
                        Log.pushContext(dep.name);
                        try
                        {
                            await dep.loadBuild();
                        }
                        catch (err)
                        {
                            throw err;
                        }
                        finally
                        {
                            Log.popContext(dep.name);
                        }
                    }
                }

                // Gather all module tasks
                const moduleTasks = Module.Base.exportedTasks;
                if (moduleTasks)
                {
                    for (const task of moduleTasks)
                    {
                        Build.task(task.taskSettings, async () =>
                        {
                            let workDone = false;
                            for (const dep of allDeps.data)
                            {
                                const promiseMaybe: PromiseLike<boolean|void>|void = task.function.call(dep);
                                if (promiseMaybe instanceof Promise)
                                {
                                    const workDoneMaybe = await promiseMaybe;
                                    if (Is.boolean(workDoneMaybe))
                                    {
                                        workDone = workDone || workDoneMaybe;
                                    }
                                }
                            }
                            if (!workDone)
                            {
                                Log.info("No changes detected, no work to do!");
                            }
                        });
                    }
                }
            }

            // Gather all required tasks
            const requiredTasks = Build.identifyTasks();
            if (requiredTasks.length == 0) { requiredTasks.push("default"); }
            for (const taskName of requiredTasks)
            {
                const task = Build.getTask(taskName);
                if (task)
                {
                    Build.requireTask(task);
                }
                else
                {
                    Log.warn(`Unknown task '${taskName}'!`);
                }
            }
            Build.exportAllTasks();

            if (EnvArgs.profile.value) { Profile.dump(); }
        }
    );

    // Default task
    export const init = Build.task({ name: "default", requires: [ Tasks.assemble ] });
}