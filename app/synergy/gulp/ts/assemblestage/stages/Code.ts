/// <reference path="../Base.ts" />
/// <reference path="../Assembler.ts" />
/// <reference path="../../util/EnvArg.ts" />

namespace RS.AssembleStages
{
    const versionArg = new EnvArg("VERSION", "dev");

    /**
     * Responsible for copying the JS code of modules.
     */
    export class Code extends AssembleStage.Base
    {
        private _moduleCode: Assembler.CodeSource[];
        private _thirdPartyCode: Util.Map<Assembler.CodeSource>;

        /**
         * Executes this assemble stage.
         */
        @Profile("AssembleStage.code.execute")
        public async execute(settings: AssembleStage.AssembleSettings, moduleList: List<Module.Base>)
        {
            this._moduleCode = [];
            this._thirdPartyCode = {};

            const jsDir = Path.combine(settings.outputDir, "js");
            await super.execute(settings, moduleList);
            await FileSystem.createPath(jsDir);

            // Log.debug(`Assemble order = ${this._moduleCode.map(m => m.name).join(", ")}`);

            // Assemble app.js and thirdparty.js in parallel
            Profile.enter("Assemble app+thirdparty");
            try
            {
                await Promise.all([
                    Assembler.assembleJS(jsDir, "app", this._moduleCode,
                    {
                        withHelpers: true,
                        withSourcemaps: true,
                        minify: false,
                        replacements: versionArg.value !== "dev" ?
                        {
                            "{!GAME_VERSION!}": versionArg.value,
                            "{!SYNERGY_HASH!}": await this.getSynergyHash()
                        } : undefined
                    }),
                    Assembler.assembleJS(jsDir, "thirdparty", Object.keys(this._thirdPartyCode).map(k => this._thirdPartyCode[k]),
                    {
                        withHelpers: false,
                        withSourcemaps: true,
                        minify: versionArg.value !== "dev"
                    })
                ]);
            }
            finally
            {
                Profile.exit("Assemble app+thirdparty");
            }
        }

        protected async getSynergyHash(): Promise<string>
        {
            // Find path via submodules
            // Can't get the synergy hash if the instance of synergy is just a snapshot (no git repo)
            if (await FileSystem.fileExists(".gitmodules"))
            {
                // use git config here to read the file
                // gives an easier output to parse than if we read the file directly with FileSystem.readFile
                const gitModules = (await Command.run("git",
                [
                    "config", "-f", ".gitmodules", "-l"
                ], undefined, false)).split("\n");

                // find submodule name by comparing url
                //submodule.<name>.url=git@bitbucket.org:red7mobile/synergy-framework.git
                //submodule.<name>.url=https://bitbucket.org/red7mobile/synergy-framework/
                let name: string;
                for (const str of gitModules)
                {
                    //covers both ssh and https
                    if (str.indexOf("red7mobile/synergy-framework") > -1)
                    {
                        name = str.split("=")[0].split(".")[1];
                        break;
                    }
                }

                /**
                 * Once again we use git config to simplify things.
                 * Reading from the existing gitModules array will prevent re-reading the file,
                 * but it means we will need to loop through and compare strings again and split =
                 * `git config` might be slower, hard to say.
                 */
                if (name != null)
                {
                    const relativePath = await Command.run("git",
                    [
                        "config", "-f", ".gitmodules", "--get", `submodule.${name}.path`
                    ], undefined, false);

                    // trim path cause git returns a trailing line break which breaks the cwd param
                    const trimmedPath = relativePath.trim();

                    // finally get the commit hash
                    return (await Command.run("git",
                    [
                        "rev-parse", "HEAD"
                    ], trimmedPath, false)).trim();
                }
            }

            Log.warn("No synergy submodule found, defaulting to \"none\"");
            return "none";
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        @Profile("AssembleStage.code.executeModule")
        protected async executeModule(settings: AssembleStage.AssembleSettings, module: Module.Base)
        {
            const unit = module.getCompilationUnit(Module.CompilationUnits.Source);
            if (unit == null) { return; }

            // Gather module src
            let content: string, rawSourceMap: string;
            try
            {
                // Load the module's JS
                content = await FileSystem.readFile(unit.outJSFile);
                rawSourceMap = await FileSystem.readFile(unit.outMapFile);
            }
            catch
            {
                // One of the files didn't load, probably doesn't exist
            }

            // Did it emit any js?
            if (content == null) { return; }

            // Did it emit any map?
            if (rawSourceMap == null)
            {
                // Generate an identity source map
                rawSourceMap = await Assembler.resolveIdentitySourceMap(module, content, `${module.name}.js`);
            }
            else
            {
                try
                {
                    // Parse the sourcemap and edit the paths
                    const sourceMap = JSON.parse(rawSourceMap);
                    sourceMap.sources = (sourceMap.sources as string[])
                        .map((src) => src.replace("../src", module.name));
                    rawSourceMap = JSON.stringify(sourceMap);
                }
                catch (err)
                {
                    throw new Error(`Error whilst parsing source map for module '${module.name}': ${err}`);
                }
            }

            // Store
            this._moduleCode.push({ name: module.name, content, sourceMap: rawSourceMap, sourceMapDir: Path.combine(module.path, "build") });

            // Gather third party dependencies
            await Assembler.gatherThirdPartyDependencies(module, this._thirdPartyCode);
        }
    }

    export const code = new Code();
    AssembleStage.register({}, code);
}