namespace RS.Assembler
{
    const generatesourcemap = require("generate-source-map");

    enum SourcemapMode
    {
        None = "none",
        WhenSet = "whenset",
        Always = "always",

        Default = WhenSet
    }

    export const sourcemapMode = new EnvArg("SOURCEMAPS", SourcemapMode.Default);

    export interface CodeSource
    {
        name: string;
        dtsContent?: string;
        content: string;
        sourceMap: string|null;
        sourceMapDir: string|null;
    }

    export interface Settings
    {
        withHelpers: boolean;
        withSourcemaps: boolean;
        replacements?: RS.Util.Map<string>;
        minify: boolean;
    }

    /**
     * Generates an identity sourcemap from the specified source.
     * @param sourceText source code
     * @param sourcePath source file name
     */
    export function generateIdentitySourcemap(sourceText: string, sourcePath: string): string
    {
        Profile.enter("generateIdentitySourcemap");
        const gen: SourceMap.SourceMapGenerator = generatesourcemap({ source: sourceText, sourceFile: sourcePath });
        gen.setSourceContent(sourcePath, sourceText);
        Profile.exit("generateIdentitySourcemap");
        return gen.toString();
    }

    /**
     * Attempts to resolve an identity sourcemap from the specified source and path, using the given module for cache.
     * @param sourceText source code
     * @param sourcePath source file name
     */
    export async function resolveIdentitySourceMap(module: Module.Base, sourceText: string, sourcePath: string)
    {
        const checksum = Checksum.getSimple(sourceText);
        const cachedMapName = Path.replaceExtension(Path.baseName(sourcePath), `.${checksum}.js.map`);
        const cachedMapPath = Path.combine(module.path, "build", cachedMapName);

        if (await FileSystem.fileExists(cachedMapPath))
        {
            return await FileSystem.readFile(cachedMapPath);
        }
        else
        {
            Log.debug(`Generating identity sourcemap for '${sourcePath}'`, module.name);
            const sourceMap = Assembler.generateIdentitySourcemap(sourceText, sourcePath);
            await FileSystem.writeFile(cachedMapPath, sourceMap);
            return sourceMap;
        }
    }

    /**
     * Assembles a single JavaScript file from the specified set of code sources.
     * Optionally produces a sourcemap.
     * Optionally substitutes strings within string literals.
     * Optionally minifies the output code.
     * @param dir
     * @param fileID
     * @param sources
     * @param settings
     */
    export async function assembleJS(dir: string, fileID: string, sources: CodeSource[], settings: Settings)
    {
        Profile.enter("assembleJS");

        if (!Config.activeConfig.sourceMap) { settings.withSourcemaps = false; }

        // Allow environment overriding of config
        switch (sourcemapMode.value)
        {
            case SourcemapMode.None:
                settings.withSourcemaps = false;
                break;

            case SourcemapMode.Always:
                settings.withSourcemaps = true;
                break;

            case SourcemapMode.WhenSet:
                break;

            default:
                Log.warn(`Unknown sourcemap mode "${sourcemapMode.value}"`);
                break;
        }

        const compiledJSPath = Path.combine(dir, `${fileID}.js`);
        const compiledMapPath = settings.withSourcemaps ? Path.combine(dir, `${fileID}.js.map`) : null;

        if (settings.replacements != null && Object.keys(settings.replacements).length > 0)
        {
            Profile.enter("assembleJS.replace");

            const tmpDir = Path.combine(dir, `${fileID}.tmp`);
            await FileSystem.createPath(tmpDir);

            try
            {
                // Do replacements per-source in parallel
                // This lets us:
                //  A) skip the majority of files, which won't need expensive replacements done
                //  B) do the remaining files in parallel
                await Promise.all(sources.map(async (source) =>
                {
                    // Do a fast search to see if we actually need to do this one
                    let containsKey = false;
                    for (const key in settings.replacements)
                    {
                        if (source.content.indexOf(key) !== -1)
                        {
                            containsKey = true;
                            break;
                        }
                    }
                    if (!containsKey) { return; }

                    const sourcePath = Path.combine(tmpDir, `${source.name}.js`);
                    await FileSystem.writeFile(sourcePath, source.content);

                    let sourceMapPath: string | null = null;
                    if (source.sourceMap)
                    {
                        sourceMapPath = `${sourcePath}.map`;
                        await FileSystem.writeFile(sourceMapPath, source.sourceMap);
                    }

                    await Command.run("node",
                    [
                        "--max-old-space-size=4096",
                        Path.combine(__dirname, "jsreplace.js"),
                        sourcePath,
                        sourceMapPath,
                        ...Object.keys(settings.replacements).map((key) => `${key}=${settings.replacements[key]}`)
                    ], undefined, false);

                    FileSystem.cache.set(sourcePath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                    source.content = await FileSystem.readFile(sourcePath);

                    if (sourceMapPath != null)
                    {
                        FileSystem.cache.set(sourceMapPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                        source.sourceMap = await FileSystem.readFile(sourceMapPath);
                    }
                }));
            }
            finally
            {
                Profile.exit("assembleJS.replace");
                await FileSystem.deletePath(tmpDir);
            }
        }

        let assembledSource: string;
        let mappings: SourceMap;
        if (settings.withSourcemaps)
        {
            Profile.enter("assembleJS.concat");
            const concat = new SourceMapBuilder();
            if (settings.withHelpers)
            {
                Profile.enter("assembleJS.concat.helpers");
                const helpersSourceMap = generateIdentitySourcemap(TypeScript.helpers, "helpers.js");
                concat.addSource(TypeScript.helpers, JSON.parse(helpersSourceMap));
                Profile.exit("assembleJS.concat.helpers");
            }
            for (const { name, content, sourceMap } of sources)
            {
                Profile.enter("assembleJS.concat.source");
                concat.addSource(content, JSON.parse(sourceMap));
                Profile.exit("assembleJS.concat.source");
            }
            concat.addLine(`//# sourceMappingURL=${fileID}.js.map`);

            assembledSource = concat.toContent().toString();
            mappings = concat.toSourceMap();

            Profile.exit("assembleJS.concat");
        }
        else
        {
            Profile.enter("assembleJS.concat");

            const content: string[] = [];

            if (settings.withHelpers)
            {
                Profile.enter("assembleJS.concat.helpers");
                content.push(TypeScript.helpers);
                Profile.exit("assembleJS.concat.helpers");
            }

            sources.forEach((thisSource) => content.push(thisSource.content));

            assembledSource = content.join("\n");
            mappings = null;

            Profile.exit("assembleJS.concat");
        }

        const compiledFile = new Build.CompiledFile(assembledSource, mappings);
        await compiledFile.write(compiledJSPath, compiledMapPath);

        if (settings.replacements != null)
        {
            await JSReplace.replace(compiledJSPath, compiledMapPath, settings.replacements);
        }

        if (settings.minify)
        {
            await JSMinify.minify(compiledJSPath, compiledMapPath);
        }

        Profile.exit("assembleJS");
    }

    /**
     * Assembles a single JavaScript file from the specified set of code sources.
     * Optionally produces a sourcemap.
     * Optionally substitutes strings within string literals.
     * Optionally minifies the output code.
     * @param dir
     * @param fileID
     * @param source
     * @param settings
     */
    export async function assembleDts(dir: string, fileID: string, source: CodeSource[], settings: Settings)
    {
        Profile.enter("assembleDts.concat");
        const concat = new SourceMapBuilder();
        for (const { name, dtsContent, content, sourceMap } of source)
        {
            concat.addSource(Buffer.from(dtsContent));
        }
        Profile.exit("assembleDts.concat");

        const compiledFile = new Build.CompiledFile(concat.toContent().toString(), null);
        const compiledJSPath = Path.combine(dir, `${fileID}.d.ts`);

        await compiledFile.write(compiledJSPath, null);
    }

    /**
     * Gathers all third party dependencies for a module and inserts the source for them into the specified code source map.
     * @param module
     * @param source
     */
    export async function gatherThirdPartyDependencies(module: Module.Base, source: Util.Map<CodeSource>)
    {
        // Gather module bower packages
        if (module.info.bowerdependencies)
        {
            for (const depName in module.info.bowerdependencies)
            {
                if (!source[depName])
                {
                    try
                    {
                        const bowerInfoPath = Path.combine("bower_components", depName, "bower.json");
                        const bowerInfo = await JSON.parseFile(bowerInfoPath);

                        const mainPackagePath = bowerInfo["main"];
                        const mainPath = Path.combine("bower_components", depName, mainPackagePath);
                        const mainText = await FileSystem.readFile(mainPath);
                        const mapPath = Path.replaceExtension(mainPath, ".js.map");

                        let map: string;
                        if (await FileSystem.fileExists(mapPath))
                        {
                            map = await FileSystem.readFile(mapPath);
                        }
                        else
                        {
                            map = await resolveIdentitySourceMap(module, mainText, mainPackagePath);
                        }

                        source[depName] = { name: depName, content: mainText, sourceMap: map, sourceMapDir: null };
                    }
                    catch (err)
                    {
                        throw new Error(`Couldn't locate bower.json file for bower package '${depName}': ${err}`);
                    }
                }
            }
        }

        // Gather module third party libs
        if (module.info.thirdparty)
        {
            for (const depName in module.info.thirdparty)
            {
                const depInfo = module.info.thirdparty[depName];
                if (!source[depName])
                {
                    try
                    {
                        const sourceText = await FileSystem.readFile(Path.combine(module.path, "thirdparty", depInfo.src));

                        let map: string;
                        if (depInfo.srcmap)
                        {
                            map = await FileSystem.readFile(Path.combine(module.path, "thirdparty", depInfo.srcmap));
                        }
                        else
                        {
                            map = await resolveIdentitySourceMap(module, sourceText, depInfo.src);
                        }

                        source[depName] = { name: depName, content: sourceText, sourceMap: map, sourceMapDir: null };
                    }
                    catch (err)
                    {
                        throw new Error(`Couldn't load third party lib '${depName}': ${err}`);
                    }
                }
            }
        }

        // Gather module nodedependencies
        if (module.info.nodedependencies)
        {
            for (const depName in module.info.nodedependencies)
            {
                const nodePath = Path.combine("node_modules", depName);
                if (!source[depName] && await FileSystem.fileExists(nodePath))
                {
                    const inPath = Path.combine(module.path, "build", "in.js");
                    const outPath = Path.combine(module.path, "build", "out.js");
                    const srcmapPath = Path.replaceExtension(outPath, ".js.map");

                    // delete existing if exists
                    await FileSystem.deletePath(inPath);
                    await FileSystem.deletePath(outPath);
                    await FileSystem.deletePath(srcmapPath);

                    // sanitise name for export (remove all unknown characters)
                    let name = depName.replace(/[^a-zA-Z0-9_]+/g, "");
                    // remove numbers from start if any
                    name = name.replace(/^[0-9]+/g, "");

                    await FileSystem.writeFile(inPath,
                        `export var ${name} = require("${depName}");`
                    );
                    await Command.run("npx",
                    [
                        "webpack",
                        "--mode", "production",
                        "--devtool", "source-map",
                        "--entry", Path.resolve(inPath), //webpack entry needs absolute path
                        "--output", outPath,
                        "--output-library-target", "global", // export to window namespace
                        "--output-source-map-filename", Path.baseName(srcmapPath) //relative to output path
                    ], null, false);

                    // set filesystem cache so readfile can read the generated files
                    FileSystem.cache.set(outPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                    FileSystem.cache.set(srcmapPath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                    const sourceText = await FileSystem.readFile(outPath);
                    const map = await FileSystem.readFile(srcmapPath);
                    source[depName] = { name: depName, content: sourceText, sourceMap: map, sourceMapDir: null };

                    // clean up generated files
                    await FileSystem.deletePath(inPath);
                    await FileSystem.deletePath(outPath);
                    await FileSystem.deletePath(srcmapPath);
                }
            }
        }
    }
}