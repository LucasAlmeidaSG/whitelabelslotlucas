namespace RS.Assembler.CodeSources
{
    export async function addGulp(codeSources: CodeSource[])
    {
        const file = __filename;
        const map = `${file}.map`;
        await addFile(codeSources, file, map);
    }

    export async function addUnit(codeSources: CodeSource[], unit: Build.CompilationUnit)
    {
        codeSources.push({
            name: unit.name,
            content: await FileSystem.readFile(unit.outJSFile),
            sourceMap: await FileSystem.readFile(unit.outMapFile),
            sourceMapDir: unit.path
        });
    }

    export async function addModule(codeSources: CodeSource[], module: Module.Base, unitName: string)
    {
        const unit = module.getCompilationUnit(unitName);
        if (!unit) { return; }
        await addUnit(codeSources, unit);
    }

    export async function addModules(codeSources: CodeSource[], p2: Module.Base | ReadonlyArray<Module.Base>, unitName: string)
    {
        const modules = Is.array(p2) ? p2 : p2.dependencies;
        for (const module of modules)
        {
            await addModule(codeSources, module, unitName);
        }
    }

    export async function addFile(codeSources: CodeSource[], file: string, sourceMapFile?: string)
    {
        codeSources.push({
            name: Path.baseName(file),
            content: await FileSystem.readFile(file),
            sourceMap: sourceMapFile ? await FileSystem.readFile(sourceMapFile) : null,
            sourceMapDir: sourceMapFile ? Path.directoryName(sourceMapFile) : null
        });
    }
}