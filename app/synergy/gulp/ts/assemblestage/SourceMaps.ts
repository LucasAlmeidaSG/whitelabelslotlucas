namespace RS.Assembler
{
    // Adapted with love from https://stackoverflow.com/questions/29905373/how-to-create-sourcemaps-for-concatenated-files

    /** Source-map spec version. */
    const specVersion = 3;

    export interface SourceMap
    {
        /** SourceMap spec version. */
        version: number;
        /** SourceMap spec version. */
        sources: string[];
        mappings: string;
        file?: string;
        sourceRoot?: string;
        /** Array of textual source content. Aligned with 'sources'. */
        sourcesContent?: string[];
        names?: string[];
    }

    const emptySourceMap: SourceMap = { version: specVersion, sources: [], mappings: "" };

    /** Maps a char byte to its integer offset in charSet. */
    const charToInteger = Buffer.alloc(256);
    /** Maps integer offsets to their char byte in charSet. */
    const integerToChar = Buffer.alloc(64);

    // Chars

    /** Buffer filler byte */
    const emptyByte = 255;
    /** Char , */
    const commaByte = 44;
    /** Char ; */
    const semiColonByte = 59;
    /** Char \n */
    const newLineByte = 10;
    /** New-line character */
    const newLineStr = "\n";

    // Populate map buffers
    charToInteger.fill(emptyByte);
    const charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    for (let i = 0; i < charSet.length; i++)
    {
        const code = charSet.charCodeAt(i);
        charToInteger[code] = i;
        integerToChar[i] = code;
    };

    /** A buffer which expands in size as necessary. */
    class DynamicBuffer
    {
        private __buffer = Buffer.alloc(512);
        private __size = 0;

        /** Decodes this DynamicBuffer into a UTF8 string. */
        public toString(encoding: string = "utf8", start: number = 0, end?: number)
        {
            if (end == null)
            {
                // Use apparent size
                end = this.__size;
            }
            else
            {
                // Clamp 'end' to apparent size
                end = Math.min(end, this.__size);
            }

            return this.__buffer.toString(encoding, start, end);

        }

        /** Ensures that this DynamicBuffer is at least 'capacity' in size. */
        public ensureCapacity(capacity: number)
        {
            if (this.__buffer.length >= capacity) { return; }
            const oldBuffer = this.__buffer;
            const newSize = Math.max(oldBuffer.length * 2, capacity);
            this.__buffer = Buffer.alloc(newSize);
            oldBuffer.copy(this.__buffer);
        }

        /** Adds the single byte 'b' to the end of this DynamicBuffer, expanding as necessary. */
        public addByte(b: number)
        {
            this.ensureCapacity(this.__size + 1);
            this.__buffer[this.__size++] = b;
        }

        /** Adds the variable-length base-64 quantity 'num' to the end of this DynamicBuffer, expanding as necessary. */
        public addVLQ(num: number)
        {
            let clamped: number;

            if (num < 0)
            {
                num = (-num << 1) | 1;
            }
            else
            {
                num <<= 1;
            }

            do
            {
                clamped = num & 31;
                num >>= 5;

                if (num > 0)
                {
                    clamped |= 32;
                }

                this.addByte(integerToChar[clamped]);
            } while (num > 0);
        }

        /** Appends the string 's' to the end of this DynamicBuffer, expanding as necessary. */
        public addString(s: string)
        {
            const l = Buffer.byteLength(s);
            this.ensureCapacity(this.__size + l);
            this.__buffer.write(s, this.__size);
            this.__size += l;
        }

        /** Appends the buffer 'b' to the end of this DynamicBuffer, expanding as necessary. */
        public addBuffer(b: Buffer)
        {
            this.ensureCapacity(this.__size + b.length);
            b.copy(this.__buffer, this.__size);
            this.__size += b.length;
        }

        /** Returns a standard static Buffer with the contents of this DynamicBuffer. */
        public toBuffer(): Buffer
        {
            return this.__buffer.slice(0, this.__size);
        }
    }

    /** Counts the number of new-lines in the given Buffer. */
    function countBufferNL(b: Buffer): number
    {
        let res = 0;
        for (let i = 0; i < b.length; i++)
        {
            if (b[i] === newLineByte) { res++; }
        }
        return res;
    }

    /** Counts the number of new-lines in the given string. */
    function countStringNL(s: string): number
    {
        let res = 0;
        for (let i = 0; i < s.length; i++)
        {
            if (s[i] === newLineStr) { res++; }
        }
        return res;
    }

    export class SourceMapBuilder
    {
        private readonly __outputBuffer = new DynamicBuffer();
        private readonly __sources = new Array<string>();
        private readonly __sourcesContent = new Array<string>();
        private readonly __mappings = new DynamicBuffer();

        private __lastSourceIndex = 0;
        private __lastSourceLine = 0;
        private __lastSourceCol = 0;

        /** Adds a single line of text. */
        public addLine(text: string)
        {
            this.__outputBuffer.addString(text);
            this.__outputBuffer.addByte(newLineByte);
            this.__mappings.addByte(semiColonByte);
        }

        /** Adds source content, optionally using the given sourceMap for the content. */
        public addSource(content: Buffer | string, sourceMap: SourceMap = emptySourceMap)
        {
            let sourceLines: number;
            if (Is.string(content))
            {
                this.__outputBuffer.addString(content);
                sourceLines = countStringNL(content);
                // Ensure last char is new-line
                if (content.length > 0 && content[content.length - 1] !== newLineStr)
                {
                    sourceLines++;
                    this.__outputBuffer.addByte(newLineByte);
                }
            }
            else
            {
                this.__outputBuffer.addBuffer(content);
                sourceLines = countBufferNL(content);
                // Ensure last char is new-line
                if (content.length > 0 && content[content.length - 1] !== newLineByte)
                {
                    sourceLines++;
                    this.__outputBuffer.addByte(newLineByte);
                }
            }

            // Work out remap positions & update sources/sourcesContent arrays
            const sourceRemap: number[] = [];
            sourceMap.sources.forEach((v, i) =>
            {
                let pos = this.__sources.indexOf(v);
                if (pos < 0)
                {
                    pos = this.__sources.length;
                    this.__sources.push(v);

                    if (sourceMap.sourcesContent)
                    {
                        this.__sourcesContent.push(sourceMap.sourcesContent[i]);
                    }
                    else
                    {
                        this.__sourcesContent.push(null);
                    }
                }
                sourceRemap.push(pos);
            });

            // Prepare to update mapping buffer
            let lastOutputCol = 0;
            const inputMappings = Buffer.from(sourceMap.mappings);
            // Current output line index pointer.
            let outputLine = 0;
            // Source-map properties decoded from binary data.
            let inOutputCol = 0;
            let inSourceIndex = 0;
            let inSourceLine = 0;
            let inSourceCol = 0;
            /** Current bit-shift value. */
            let shift = 0;
            /** Binary source-map data from VLQ. */
            let value = 0;
            /** Current index pointer into binary source-map data. */
            let valuePos = 0;
            const commit = () =>
            {
                if (valuePos === 0) return;
                this.__mappings.addVLQ(inOutputCol - lastOutputCol);
                lastOutputCol = inOutputCol;
                if (valuePos === 1)
                {
                    valuePos = 0;
                    return;
                }
                let outSourceIndex = sourceRemap[inSourceIndex];
                this.__mappings.addVLQ(outSourceIndex - this.__lastSourceIndex);
                this.__lastSourceIndex = outSourceIndex;
                this.__mappings.addVLQ(inSourceLine - this.__lastSourceLine);
                this.__lastSourceLine = inSourceLine;
                this.__mappings.addVLQ(inSourceCol - this.__lastSourceCol);
                this.__lastSourceCol = inSourceCol;
                valuePos = 0;
            }

            for (let i = 0; i < inputMappings.length; i++)
            {
                let byte = inputMappings[i];
                if (byte === semiColonByte)
                {
                    // Add to buffer
                    commit();
                    this.__mappings.addByte(semiColonByte);

                    // Reset to line start
                    inOutputCol = 0;
                    lastOutputCol = 0;
                    outputLine++;
                }
                else if (byte === commaByte)
                {
                    // Add to buffer
                    commit();
                    this.__mappings.addByte(commaByte);
                }
                else
                {
                    byte = charToInteger[byte];
                    if (byte === emptyByte)
                    {
                        // Failed to map byte to char int
                        throw new Error("Invalid sourceMap");
                    }

                    /*
                     * Essentially, this is dealing with the 'VLQ' that is
                     * the foundation of spec. 3 source-maps.
                     *
                     * The VLQ is a compact encoding of binary data, itself
                     * encoding the source-map data you'd expect i.e. lines,
                     * columns etc.
                     */

                    value += (byte & 31) << shift;
                    if (byte & 32)
                    {
                        shift += 5;
                    }
                    else
                    {
                        const shouldNegate = value & 1;
                        value >>= 1;
                        if (shouldNegate) { value = -value; }
                        switch (valuePos)
                        {
                            case 0: inOutputCol += value; break;
                            case 1: inSourceIndex += value; break;
                            case 2: inSourceLine += value; break;
                            case 3: inSourceCol += value; break;
                        }

                        valuePos++;
                        value = 0;
                        shift = 0;
                    }
                }
            }
            commit();

            // Pad output lines to match source lines
            while (outputLine < sourceLines)
            {
                this.__mappings.addByte(semiColonByte);
                outputLine++;
            }
        }

        /** Returns a buffer containing the current source content. */
        public toContent(): Buffer
        {
            return this.__outputBuffer.toBuffer();
        }

        /** Returns a source map object representing the current source map state. */
        public toSourceMap(): SourceMap
        {
            return {
                version: specVersion,
                sources: this.__sources,
                mappings: this.__mappings.toString(),
                sourcesContent: this.__sourcesContent
            };
        }
    }
}