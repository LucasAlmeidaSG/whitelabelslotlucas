/// <reference path="../module/Base.ts" />
/// <reference path="Settings.ts" />

namespace RS.AssembleStage
{
    /**
     * Encapsulates a generic assemble stage.
     */
    export abstract class Base
    {
        public readonly name?: string | null;

        /**
         * Executes this assemble stage.
         */
        public async execute(settings: AssembleSettings, moduleList: List<Module.Base>)
        {
            Log.pushContext(`${this}`);

            // Iterate them
            for (const dep of moduleList.data)
            {
                Log.pushContext(dep.name);
                try
                {
                    await this.executeModule(settings, dep);
                }
                catch (err)
                {
                    Log.error(err.stack);
                }
                Log.popContext(dep.name);
            }

            Log.popContext(`${this}`);
        }

        public toString()
        {
            const className = Object.getPrototypeOf(this).constructor.name;
            return this.name ? `${className}:${this.name}` : className;
        }

        /**
         * Executes this assemble stage for the given module only.
         * @param module
         */
        protected abstract executeModule(settings: AssembleSettings, module: Module.Base): PromiseLike<void>;
    }
}