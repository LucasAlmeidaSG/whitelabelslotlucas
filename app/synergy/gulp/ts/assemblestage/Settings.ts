namespace RS.AssembleStage
{
    /**
     * Settings for assembly.
     */
    export interface AssembleSettings
    {
        outputDir: string;
    }
}