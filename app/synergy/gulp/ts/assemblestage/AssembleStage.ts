/// <reference path="Base.ts" />
/// <reference path="../util/List.ts" />

namespace RS.AssembleStage
{
    /** Settings for an assemble stage. */
    export interface Settings
    {
        /** Any stages that should come before this one. */
        before?: Base[];

        /** Any stages that should come after this one. */
        after?: Base[];

        /** Only use this stage if explicitly requested. */
        explicit?: boolean;

        /** This should be the last stage to execute. Does NOT take priority over "before" and "after". */
        last?: boolean;
    }

    const assembleStages = new List<{ settings: Settings; stage: Base; }>();

    /**
     * Registers an assemble stage with the system.
     * @param settings
     * @param stage 
     */
    export function register(settings: Settings, stage: Base)
    {
        assembleStages.add({ settings, stage });
    }

    /**
     * Gets all assemble stages in order of execution.
     */
    export function getStages(): Base[]
    {
        const result = assembleStages.map(s => s.stage);
        result.resolveOrder(
            stage => assembleStages.find(item => item.stage === stage).settings.before || [],
            stage =>
            {
                const settings = assembleStages.find(item => item.stage === stage).settings;
                return [
                    ...(settings.after || []),
                    ...(settings.last ? assembleStages.filter(s => !s.settings.last).map(s => s.stage).data : [])
                ];
            }
        );
        // for (const entry of result.data)
        // {
        //     const name = Object.getPrototypeOf(entry).constructor.name;
        //     Log.debug(name);
        // }
        return result.dataCopy;
    }
}