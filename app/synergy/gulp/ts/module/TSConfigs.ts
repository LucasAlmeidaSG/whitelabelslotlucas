namespace RS.Module.DefaultTSConfigs
{
    const compilerOpts: TSConfig.CompilerOptions =
    {
        // Basic requirements
        noEmitHelpers: true,
        rootDir: ".",

        declaration: true,
        declarationMap: true,

        // Source maps, optional
        sourceMap: true,
        inlineSources: true,
        inlineSourceMap: false,

        // Optional compiler features
        experimentalDecorators: true,
        stripInternal: true,

        typeRoots: []
    };

    export const source: TSConfig =
    {
        compilerOptions:
        {
            ...compilerOpts,
            outFile: "../build/src.js",

            target: "ES5",
            lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"]
        }
    };

    export const buildSource: TSConfig =
    {
        compilerOptions:
        {
            ...compilerOpts,
            outFile: "../build/buildsrc.js",

            target: "ES5",
            lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"]
        }
    };
}
