namespace RS.Module
{
    /**
     * Gathers a complete ordered list of modules that satisfies all dependencies of the given list of module names, plus those modules themselves.
     * @param modules 
     */
    export function getDependencySet(modules: string[]): List<Module.Base>
    {
        // Gather all dependencies in a list
        // Log.debug(`Gathering dependency set for '${modules.join(", ")}'`);
        const list = new List<Module.Base>();
        const erroredFor: Util.Map<boolean> = {};
        for (const name of modules)
        {
            const module = getModule(name);
            if (module == null)
            {
                if (!erroredFor[name])
                {
                    Log.warn(`Module '${name}' not found`);
                    erroredFor[name] = true;
                }
                continue;
            }
            const depSet = module.gatherDependencySet();
            for (const depModule of depSet.data)
            {
                if (!list.contains(depModule))
                {
                    list.add(depModule);
                }
            }
            if (!list.contains(module))
            {
                list.add(module);
            }
        }
        // Sort by importance
        let curImportance = 0, matchedModules = -1;
        while (matchedModules !== 0)
        {
            matchedModules = 0;
            ++curImportance;
            for (let i = 0; i < list.length; ++i)
            {
                const module = list.get(i);
                if (module.important === curImportance)
                {
                    ++matchedModules;
                    // Log.debug(`Floating '${module.name}'...`);
                    // "Float" the module as high as it can go in the list without violating dependencies
                    let idx = i - 1;
                    // Log.debug(`Test vs '${list.get(idx).name}'...`);
                    while (idx > 0 && module.dependencies.indexOf(list.get(idx)) === -1)
                    {
                        // Log.debug(`Test vs '${list.get(idx).name}', no dependency found, moving up`);
                        --idx;
                    }
                    ++idx;
                    if (idx !== i)
                    {
                        // Log.debug(`Moving '${module.name}' up by ${i - idx}`);
                        list.move(i, idx);
                    }
                    else
                    {
                        // Log.debug(`Not moving '${module.name}' up`);
                    }
                }
            }
        }
        // Place implementing modules immediately after abstract ones
        // NOTE: if module A implements module B, and depends on module C, which also depends on B, C will still appear before A
        // This is to handle the various abstract definitions provided by Core
        const implementingModules = list.filter((m) => m.info.implements != null).data;
        for (const concreteModule of implementingModules)
        {
            const abstractModule = Module.getModule(concreteModule.info.implements);
            const concreteDependencies = concreteModule.gatherDependencySet();
            const directDependents = list.filter((m) =>
                m !== concreteModule && m !== abstractModule
                && !concreteDependencies.contains(m)
                && m.dependencies.indexOf(abstractModule) !== -1);

            for (let i = directDependents.length - 1; i >= 0; --i)
            {
                const dependent = directDependents.get(i);
                const listIndex = list.indexOf(dependent);
                const concreteIndex = list.indexOf(concreteModule);

                // Does it already appear after?
                if (concreteIndex < listIndex) { continue; }
                // Relocate it to after here
                list.move(listIndex, concreteIndex + 1);
                correctDependents(dependent, list);
            }
        }
        // Log.debug(`Result =\n${list.data.map((module) => module.name).join("\n")}`);
        return list;
    }

    function correctDependents(module: Base, list: List<Base>)
    {
        const index = list.indexOf(module);
        const dependents = list.filter((m) =>
            m.dependencies.indexOf(module) !== -1 &&
            list.indexOf(m) < index);

        for (let i = dependents.length - 1; i >= 0; --i)
        {
            const dependent = dependents.get(i);
            const dependentIndex = list.indexOf(dependent);
            list.move(dependentIndex, index + 1);
            correctDependents(dependent, list);
        }
    }

    /**
     * Sorts an array of modules into several batches, in which no module depends on another.
     * Modules in each batch can be processed in parallel, so long as the set of batches is processed serially.
     * This should be executed a complete dependency set.
     * @param modules 
     * @param maxBatchSize
     */
    export function getBatchSet(modules: ArrayLike<Module.Base>, maxBatchSize: number = 4): List<List<Module.Base>>
    {
        const resolved = new List<Module.Base>();
        const remaining = new List<Module.Base>(modules);

        const batchList = new List<List<Module.Base>>();

        while (remaining.length > 0)
        {
            const batch = new List<Module.Base>();
            for (let i = remaining.length - 1; i >= 0; --i)
            {
                const module = remaining.get(i);
                
                // In order to add module to batch, all dependencies of it must be in resolved
                let allDepsResolved = true;
                for (const depModule of module.dependencies)
                {
                    if (!resolved.contains(depModule))
                    {
                        allDepsResolved = false;
                        break;
                    }
                }

                if (allDepsResolved)
                {
                    // Accept for this batch
                    batch.add(module);
                    if (batch.length >= maxBatchSize) { break; }
                }
            }

            if (batch.length === 0) { throw new Error("getBatchSet: Infinite loop detected (circular dependency in module list?)"); }

            resolved.addRange(batch);
            remaining.removeRange(batch);

            batchList.add(batch);
        }

        return batchList;
    }

    /**
     * Compiles build components for all specified modules.
     * @param moduleList 
     */
    export async function compileBuildComponents(moduleList: List<Module.Base>)
    {
        // == NEW PARALLEL WAY ==
        const batches = getBatchSet(moduleList.data);
        for (const batch of batches.data)
        {
            let errorCount = 0;
            const promises: PromiseLike<BuildStage.Result>[] = [];
            for (const module of batch.data)
            {
                if (!module.buildLoaded)
                {
                    const promise = module.compileBuild()
                        .then(result =>
                        {
                            errorCount += result.errorCount;
                            return result;
                        }, err =>
                        {
                            Log.error(err.stack || err, module.name);
                            ++errorCount;
                            return { workDone: false, errorCount: 1 };
                        });
                    promises.push(promise);
                }
            }
            if (promises.length > 0)
            {
                await Promise.all(promises);
                if (errorCount > 0)
                {
                    throw new Error(`${errorCount} errors when compiling build components`);
                }
            }
        }

        // == OLD SERIAL WAY ==
        // for (const dep of moduleList.data)
        // {
        //     if (!dep.buildLoaded)
        //     {
        //         RS.Log.pushContext(dep.name);
        //         try
        //         {
        //             const result = await dep.compileBuild();
        //             if (result.errorCount > 0)
        //             {
        //                 throw new Error(`${result.errorCount} errors when compiling build source for '${dep.name}'`);
        //             }
        //         }
        //         catch (err)
        //         {
        //             throw err;
        //         }
        //         finally
        //         {
        //             RS.Log.popContext(dep.name);
        //         }
                
        //     }
        // }
    }
}