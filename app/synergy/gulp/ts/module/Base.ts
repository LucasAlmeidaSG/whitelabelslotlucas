/// <reference path="Info.ts" />
/// <reference path="../util/List.ts" />
/// <reference path="../util/Timer.ts" />
/// <reference path="CompilationDependencies.ts" />

namespace RS.Module
{
    export interface TaskInfo
    {
        function: Function;
        taskSettings: Build.TaskSettings;
    }

    function Task(taskSettings: Build.TaskSettings): MethodDecorator
    {
        interface TargetEx extends Object
        {
            _tasks?: TaskInfo[];
        }
        return (target: TargetEx, propertyKey: string, descriptor: PropertyDescriptor) =>
        {
            target._tasks = target._tasks || [];
            target._tasks.push({ function: target[propertyKey], taskSettings });
        };
    }

    /** Names of standard compilation units. */
    export enum CompilationUnits
    {
        Source = "src",
        BuildSource = "buildsrc"
    }

    /**
     * Represents a module.
     */
    export class Base
    {
        /** Gets all tasks exported by the base module. */
        public static get exportedTasks() { return (this.prototype as { _tasks?: TaskInfo[] })._tasks; }

        protected _info: Info;
        protected _name: string;
        protected _path: string;
        protected _inited: boolean;
        protected _dependencies: Base[];
        protected _checksumDB: Checksum.DB;
        protected _buildLoaded: boolean;
        protected _important: number = 0;
        protected _compilationUnits: Util.Map<Build.CompilationUnit>;

        /** Gets the moduleinfo for this module. */
        public get info() { return this._info; }

        /** Gets the name of this module. */
        public get name() { return this._name; }

        /** Gets the path of folder that holds this module. */
        public get path() { return this._path; }

        /** Gets if this module has been initialised. */
        public get initialised() { return this._inited; }

        /** Gets the checksum database for this module. */
        public get checksumDB() { return this._checksumDB; }

        /** Gets all dependencies of this module. */
        public get dependencies() { return this._dependencies; }

        /** Gets if the build component of this module has been loaded. */
        public get buildLoaded() { return this._buildLoaded; }

        /**
         * Gets or sets the importance of this module.
         * This determines how to sort it within any dependency sets that include it.
         * Higher values means the module will be "floated" as high as possible within the set.
         */
        public get important() { return this._important; }
        public set important(value)
        {
            this._important = value;
            for (const module of this._dependencies)
            {
                module.important = Math.max(module.important, value);
            }
        }

        public constructor(name: string, info: Info, path: string)
        {
            this._name = name;
            this._info = info;
            this._path = path;
            this._inited = false;
            this._checksumDB = new Checksum.DB();
            this._compilationUnits = {};
            this.exportTasks();
        }

        /**
         * Gets a named compilation unit for this module.
         * @param name
         */
        public getCompilationUnit(name: string): Build.CompilationUnit | null
        {
            return this._compilationUnits[name] || null;
        }

        /**
         * Returns whether or not this module has code at the given path.
         * Used to check whether or not this module should have a particular compilation unit.
         * @param srcPath Path to the folder that holds tsconfig.json, relative to the module's root path.
         */
        public async hasCode(srcPath: string): Promise<boolean>
        {
            const fullPath = Path.combine(this._path, srcPath);
            const files = await FileSystem.readFiles(fullPath);
            if (files.some((f) => Path.extension(f) === ".ts")) { return true; }
            return false;
        }

        /**
         * Adds a new named compilation unit to this module.
         * Should only be called if the module actually has code for the given compilation unit.
         * Use Module.hasCode(srcPath) to check if this is the case.
         * @param name
         * @param srcPath Path to the folder that holds tsconfig.json, relative to the module's root path.
         */
        public addCompilationUnit(name: string, srcPath: string, defaultTSConfig?: Readonly<TSConfig>): Build.CompilationUnit
        {
            if (this._compilationUnits[name])
            {
                Log.warn(`Tried to add the compilation unit '${name}' to a module when it's already been added`);
                return this._compilationUnits[name];
            }
            const unit = new Build.CompilationUnit();
            unit.name = `${this._name}.${name}`;
            unit.module = this;
            unit.path = Path.combine(this._path, srcPath);
            if (defaultTSConfig) { unit.defaultTSConfig = defaultTSConfig; }
            this._compilationUnits[name] = unit;
            return unit;
        }

        /**
         * Initialises this module. Async.
         */
        public async init(dependencies: Base[])
        {
            // Load checksum database
            await this._checksumDB.loadFromFile(Path.combine(this._path, "build", "checksums.json"));

            // Initialise
            this._dependencies = dependencies;
            this._inited = true;

            // Setup standard compilation units
            if (await this.hasCode("src"))
            {
                this.setupSourceCompilationUnit();
            }
            if (await this.hasCode("buildsrc"))
            {
                this.setupBuildSourceCompilationUnit();
            }
        }

        /**
         * Gets the checksum for the specified sub-directory.
         * @param subPath
         */
        public async getSourceChecksum(subPath: string)
        {
            return await Checksum.getComposite(
                (await FileSystem.readFiles(Path.combine(this._path, subPath)))
                    .filter((str) => !this.shouldExcludeFromSourceChecksum(str))
            );
        }

        /**
         * Gathers a sorted list of all dependent modules of this module.
         * Travels up the dependency hierarchy.
         */
        public gatherDependencySet(): List<Base>
        {
            const list = new List<Base>();
            this.gatherDependencySetInternal(list);
            return list;
        }

        /**
         * Exports all tasks for this module to the task system.
         */
        public exportTasks()
        {
            const tasks = Base.exportedTasks;
            if (tasks != null)
            {
                for (const task of tasks)
                {
                    RS.Build.task({
                        ...task.taskSettings,
                        name: `${this.name}.${task.taskSettings.name}`,
                    }, async () =>
                    {
                        let workDone = false;
                        const deps = this.gatherDependencySet();
                        for (const dep of deps.data)
                        {
                            Log.pushContext(dep.name);
                            const promiseMaybe: Promise<void|boolean>|void = task.function.call(dep);
                            if (promiseMaybe instanceof Promise)
                            {
                                const workDoneMaybe = await promiseMaybe;
                                if (Is.boolean(workDoneMaybe))
                                {
                                    workDone = workDone || workDoneMaybe;
                                }
                            }
                            Log.popContext(dep.name);
                        }
                        if (!workDone)
                        {
                            RS.Log.info("No changes detected, no work to do!");
                        }
                    });
                }
            }
        }

        /**
         * Compiles the build component of this module, if it has one.
         */
        public async compileBuild(): Promise<BuildStage.Result>
        {
            // Don't do anything unless we have build source
            const unit = this.getCompilationUnit(CompilationUnits.BuildSource);
            if (unit == null) { return { workDone: true, errorCount: 0 }; }

            // Initialise the unit
            const result = await Build.compileUnit(unit);
            if (result.errorCount === 0) { await this.saveChecksums(); }
            return result;
        }

        /**
         * Loads the build component of this module, if it has one.
         */
        public async loadBuild()
        {
            if (this._buildLoaded) { return; }

            // Don't do anything unless we have build source
            if (!await FileSystem.fileExists(Path.combine(this._path, "buildsrc", "tsconfig.json"))) { return false; }

            // Load it
            require(Path.relative(__dirname, Path.combine(this._path, "build", "buildsrc.js")));
            // Log.debug(`Loaded build component of '${this.name}'`);

            this._buildLoaded = true;
        }

        /**
         * Saves the checksum database to file.
         */
        public async saveChecksums()
        {
            await this._checksumDB.saveToFile(Path.combine(this._path, "build", "checksums.json"));
        }

        protected setupSourceCompilationUnit()
        {
            const srcUnit = this.addCompilationUnit(CompilationUnits.Source, "src", DefaultTSConfigs.source);

            CompilationDependencies.addModules(srcUnit, this, CompilationUnits.Source);
            CompilationDependencies.addBower(srcUnit, this);
            CompilationDependencies.addNPM(srcUnit, this);
            CompilationDependencies.addThirdParty(srcUnit, this);
        }

        protected setupBuildSourceCompilationUnit()
        {
            const buildSrcUnit = this.addCompilationUnit(CompilationUnits.BuildSource, "buildsrc", DefaultTSConfigs.buildSource);

            CompilationDependencies.addGulp(buildSrcUnit, this);
            CompilationDependencies.addModules(buildSrcUnit, this, CompilationUnits.BuildSource);
            CompilationDependencies.addNPM(buildSrcUnit, this, "build");

            // Add the post processor
            buildSrcUnit.addPostProcessor((artifact) =>
            {
                return {
                    ...artifact,
                    source: `var RS = module.parent.exports.RS;\n${artifact.source}`
                };
            });
        }

        /**
         * Gets if the specified path should be excluded from the checksum produced by getSourceChecksum.
         * Useful for excluding generated code files from change detection.
         * @param filename
         */
        protected shouldExcludeFromSourceChecksum(filename: string): boolean
        {
            return /[\/\\](dependencies)[\/\\]/g.test(filename);
        }

        protected gatherDependencySetInternal(set: List<Base>)
        {
            // Iterate all dependencies
            for (const depModule of this._dependencies)
            {
                // Gather
                depModule.gatherDependencySetInternal(set);
            }

            // Add us to the list if we're not already there
            if (!set.contains(this))
            {
                set.add(this);
            }
        }
    }
}