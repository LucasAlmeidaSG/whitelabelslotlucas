/// <reference path="../util/Is.ts" />
/// <reference path="../util/Misc.ts" />

/** RSCore Next module system. */
namespace RS.Module
{
    /** Describes a single module. */
    export interface Info
    {
        title: string;
        primarynamespace?: string;
        name?: string;
        description: string;
        dependencies: string[];
        testdependencies?: string[];
        nodedependencies?: Util.Map<string> & { build: Util.Map<string>; };
        bowerdependencies?: Util.Map<string>;
        definitions?: Util.Map<string>;
        thirdparty?: Util.Map<{ src: string; srcmap?: string; }>;
        implements?: string;
        important?: boolean;
    }

    /**
     * Validates the specified module info. Returns null for success, or a string containing the error.
     * @param info
     */
    export function validateInfo(info: Info): string|null
    {
        if (info == null) { return "Module info is null"; }
        if (!Is.string(info.title)) { return "title is missing or invalid"; }
        if (info.name != null && !Is.string(info.name)) { return "name is invalid"; }
        if (!Is.string(info.description)) { return "description is missing or invalid"; }
        if (!Is.arrayOf(info.dependencies, Is.string)) { return "dependencies are missing or invalid"; }
        if (info.testdependencies != null)
        {
            if (!Is.arrayOf(info.testdependencies, Is.string)) { return "test dependencies are invalid"; }
        }
        if (info.nodedependencies != null)
        {
            if (!Is.object(info.nodedependencies)) { return "node dependencies are invalid"; }
            for (const nodeModuleName in info.nodedependencies)
            {
                if (nodeModuleName === "build")
                {
                    const builddependencies = info.nodedependencies.build;
                    if (!Is.object(builddependencies)) { return `build node dependencies are invalid`; }
                    for (const nodeModuleName in builddependencies)
                    {
                        if (!Is.string(builddependencies[nodeModuleName])) { return `build node dependency '${nodeModuleName}' is invalid`; }
                    }
                }
                else
                {
                    if (!Is.string(info.nodedependencies[nodeModuleName])) { return `node dependency '${nodeModuleName}' is invalid`; }
                }
            }
        }
        if (info.bowerdependencies != null)
        {
            if (!Is.object(info.bowerdependencies)) { return "bower dependencies are invalid"; }
            for (const nodeModuleName in info.bowerdependencies)
            {
                if (!Is.string(info.bowerdependencies[nodeModuleName])) { return `bower dependency '${nodeModuleName}' is invalid`; }
            }
        }
        if (info.definitions != null)
        {
            if (!Is.object(info.definitions)) { return "definitions are invalid"; }
            for (const name in info.definitions)
            {
                if (!Is.string(info.definitions[name])) { return `definition '${name}' is invalid`; }
            }
        }
        if (info.thirdparty != null)
        {
            if (!Is.object(info.thirdparty)) { return "third-party dependencies are invalid"; }
            for (const name in info.thirdparty)
            {
                const val = info.thirdparty[name];
                if (!Is.object(val)) { return `third-party dependency '${name}' is invalid`; }
                if (!Is.string(val.src)) { return `third-party dependency '${name}' src is invalid`; }
                if (val.srcmap != null && !Is.string(val.srcmap)) { return `third-party dependency '${name}' srcmap is invalid`; }
            }
        }
        if (info.important != null && !Is.boolean(info.important)) { return "important is invalid"; }
        if (info.implements != null && !Is.string(info.implements)) { return "implements is invalid"; }
        return null;
    }
}