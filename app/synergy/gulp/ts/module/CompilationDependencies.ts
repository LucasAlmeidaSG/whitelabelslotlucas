namespace RS.Module.CompilationDependencies
{
    /**
     * Adds dependencies on the gulp file to the specified compilation unit.
     * @param unit 
     */
    export function addGulp(unit: Build.CompilationUnit, module: Base): void
    {
        const moduleFilename = Path.relative(module.path, __filename);
        unit.dependencies.add({
            kind: Build.CompilationUnit.DependencyKind.ThirdParty,
            name: "gulplib",
            sourceFile: moduleFilename,
            sourceMap: Path.replaceExtension(moduleFilename, ".js.map"),
            definitionsFile: Path.replaceExtension(moduleFilename, ".d.ts"),
            definitionsMapFile: Path.replaceExtension(moduleFilename, ".d.ts.map")
        });
        unit.dependencies.add({
            kind: Build.CompilationUnit.DependencyKind.DefinitionsFolder,
            name: "gulpdefs",
            path: Path.combine(__dirname, "definitions")
        });
    }

    export function addUnit(unit: Build.CompilationUnit, depUnit: Build.CompilationUnit): void
    {
        unit.dependencies.add({
            kind: Build.CompilationUnit.DependencyKind.CompilationUnit,
            name: depUnit.name,
            compilationUnit: depUnit
        });
        unit.dependencies.add({
            kind: Build.CompilationUnit.DependencyKind.DefinitionsFolder,
            name: `${depUnit.name}.Dependencies`,
            path: Path.combine(depUnit.path, "dependencies")
        });
    }

    /**
     * Adds dependencies on the compilation unit of "unitName" on a single module to the specified compilation unit.
     * @param unit 
     * @param module 
     */
    export function addModule(unit: Build.CompilationUnit, module: Base, unitName: string): void
    {
        const depUnit = module.getCompilationUnit(unitName);
        if (!depUnit) { return; }
        addUnit(unit, depUnit);
    }

    /**
     * Adds dependencies on all compilation units of "unitName" on all dependencies of a module to the specified compilation unit.
     * @param unit 
     * @param module 
     */
    export function addModules(unit: Build.CompilationUnit, module: Base, unitName: string): void
    {
        for (const dep of module.dependencies)
        {
            addModule(unit, dep, unitName);
        }
    }

    /**
     * Adds dependencies on all node module dependencies of a module.
     * Optionally supports sub-maps within the node module dependencies, e.g. "build".
     * @param unit 
     * @param module 
     * @param subName 
     */
    export function addNPM(unit: Build.CompilationUnit, module: Base, subName?: string): void
    {
        if (!module.info.nodedependencies) { return; }
        const depsMap = (subName ? module.info.nodedependencies[subName] : module.info.nodedependencies) as Util.Map<string>;
        if (!depsMap) { return; }

        for (const name in depsMap)
        {
            const val = depsMap[name];
            if (Is.string(val))
            {
                unit.dependencies.add({
                    kind: Build.CompilationUnit.DependencyKind.NodeModule,
                    name: `npm:${name}`,
                    moduleName: name,
                    moduleVersion: depsMap[name]
                });
            }
        }
    }

    /**
     * Adds dependencies on all bower package dependencies of a module.
     * @param unit 
     * @param module 
     */
    export function addBower(unit: Build.CompilationUnit, module: Base): void
    {
        if (module.info.bowerdependencies)
        {
            for (const name in module.info.bowerdependencies)
            {
                unit.dependencies.add({
                    kind: Build.CompilationUnit.DependencyKind.BowerPackage,
                    name: `bower:${name}`,
                    packageName: name,
                    packageVersion: module.info.bowerdependencies[name]
                });
            }
        }
    }

    /**
     * Adds dependencies on all third party dependencies of a module.
     * @param unit 
     * @param module 
     */
    export function addThirdParty(unit: Build.CompilationUnit, module: Base): void
    {
        const thirdPartyMap: Util.Map<Build.CompilationUnit.ThirdPartyDependency> = {};
        if (module.info.thirdparty)
        {
            for (const name in module.info.thirdparty)
            {
                const thirdParty = module.info.thirdparty[name];
                if (thirdPartyMap[name])
                {
                    thirdPartyMap[name].sourceFile = thirdParty.src;
                    thirdPartyMap[name].sourceMap = thirdParty.srcmap;
                }
                else
                {
                    thirdPartyMap[name] =
                    {
                        kind: Build.CompilationUnit.DependencyKind.ThirdParty,
                        name,
                        sourceFile: thirdParty.src,
                        sourceMap: thirdParty.srcmap
                    };
                }
            }
        }
        if (module.info.definitions)
        {
            for (const name in module.info.definitions)
            {
                const definition = module.info.definitions[name];
                if (thirdPartyMap[name])
                {
                    thirdPartyMap[name].definitionsFile = definition
                }
                else
                {
                    thirdPartyMap[name] =
                    {
                        kind: Build.CompilationUnit.DependencyKind.ThirdParty,
                        name,
                        definitionsFile: definition
                    };
                }
            }
        }
        for (const name in thirdPartyMap)
        {
            unit.dependencies.add(thirdPartyMap[name]);
        }
    }
}