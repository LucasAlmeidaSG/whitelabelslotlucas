/// <reference path="Info.ts" />
/// <reference path="Base.ts" />

/// <reference path="../util/fileio/FileSystem.ts" />
/// <reference path="../util/Profile.ts" />

namespace RS.Module
{
    const blacklistDirectories =
    {
        "node_modules": true,
        "bower_components": true,
        "schemas": true
    };

    const modules: { [moduleName: string]: Base } = {};

    /**
     * Finds all modules underneath the specified path. Recurses down sub-directories. Async.
     * @param path 
     */
    export async function findModules(path: string)
    {
        Profile.enter("findModules");
        const list = await FileSystem.readDirectory(path, true);
        const result: string[] = [];
        for (const item of list)
        {
            if (item.type === "file" && item.name === "moduleinfo.json")
            {
                result.push(`${path}/${item.name}`);
            }
            else if (item.type === "folder" && !blacklistDirectories[item.name])
            {
                for (const subItem of await findModules(`${path}/${item.name}`))
                {
                    result.push(subItem);
                }
            }
        }
        Profile.exit("findModules");
        return result;
    }

    /**
     * Determines the name of a module from the location of it's module info.
     * @param path 
     */
    export function determineNameFromPath(path: string)
    {
        const splitBySlash = path.split(/\\|\//g);

        // Identify "modules" folder
        const modulesIdx = splitBySlash.indexOf("modules");
        if (modulesIdx !== -1)
        {
            // Remove everything up to and including it
            splitBySlash.splice(0, modulesIdx + 1);
        }

        // Remove module info
        if (splitBySlash[splitBySlash.length - 1] === "moduleinfo.json")
        {
            splitBySlash.pop();
        }

        // Fix casing, join
        return formatName(splitBySlash.join("."));
    }

    /**
     * Formats a module name.
     * @param name 
     */
    export function formatName(name: string): string
    {
        return name
            .split(".")
            .map(item => `${item.substr(0, 1).toUpperCase()}${item.substr(1).toLowerCase()}`)
            .join(".");
    }

    /**
     * Gets a module by name.
     * @param name 
     */
    export function getModule(name: string): Base|null
    {
        return modules[name] || null;
    }

    /**
     * Gets all loaded modules.
     * @param sorted If true, sorts the modules by dependency (so module B depending on module A will always come afterwards in the array)
     */
    export function getAll(sorted = false): Base[]
    {
        const names = Object.keys(modules);
        if (sorted)
        {
            const result: Base[] = [];
            const hasMap: Util.Map<boolean> = {};
            while (names.length > 0)
            {
                const oldNameCount = names.length;
                for (let i = names.length - 1; i >= 0; --i)
                {
                    const name = names[i];
                    const module = modules[name];
                    let satisfied = true;
                    for (const dep of module.info.dependencies)
                    {
                        if (!hasMap[dep])
                        {
                            satisfied = false;
                            break;
                        }
                    }
                    if (satisfied)
                    {
                        result.push(module);
                        hasMap[name] = true;
                        names.splice(i, 1);
                    }
                }
                if (names.length === oldNameCount)
                {
                    Log.warn("getAll: possible circular dependency");
                    break;
                }
            }
            return result;
        }
        else
        {
            return names.map(name => modules[name]);
        }
    }

    /**
     * Finds and loads all modules.
     */
    export async function loadModules()
    {
        try
        {
            Profile.enter("loadModules");
            Log.pushContext("LoadModules");

            const rawModules = await findModules(".");
            for (const path of rawModules)
            {
                // Load module info
                const info = await JSON.parseFile(path) as Info;
                
                // Validate it
                const validateErr = validateInfo(info);
                if (validateErr)
                {
                    Log.warn(`Module at '${path}': ${validateErr}`);
                    continue;
                }

                // Check for duplicate
                const moduleName = info.name || determineNameFromPath(path);
                if (modules[moduleName])
                {
                    Log.warn(`Duplicate module detected (by name '${moduleName}')`);
                    continue;
                }

                // Spawn it
                const module = new Base(moduleName, info, Path.directoryName(path));

                // Store it
                modules[moduleName] = module;
            }

            // Check that all dependencies can be satisifed
            let didError = false;
            for (const moduleName in modules)
            {
                const module = modules[moduleName];
                for (const depName of module.info.dependencies)
                {
                    if (!modules[depName])
                    {
                        Log.error(`Failed to resolve module dependencies for '${module.name}' - '${depName}' was not found`);
                        didError = true;
                    }
                }
            }
            if (didError) { return; }

            // Initialise all modules, resolve dependencies here also
            const modulesToInit = getAll(true);
            for (const module of modulesToInit)
            {
                if (!module.initialised)
                {
                    const deps: Base[] = [];
                    for (const depName of module.info.dependencies)
                    {
                        if (!modules[depName])
                        {
                            Log.error(`Failed to resolve module dependencies for '${module.name}' - '${depName}' was not found`);
                            return;
                        }
                        deps.push(modules[depName]);
                    }
                    await module.init(deps);
                }
            }

            // Set importance values
            for (const module of modulesToInit)
            {
                if (module.info.important)
                {
                    module.important = 1;
                }
            }
        }
        finally
        {
            Log.popContext("LoadModules");
            Profile.exit("loadModules");
        }
    }
}