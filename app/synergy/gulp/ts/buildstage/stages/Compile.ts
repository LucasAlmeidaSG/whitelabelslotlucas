/// <reference path="../BuildStage.ts" />

namespace RS.BuildStages
{
    /**
     * Responsible for compiling the TypeScript source code of modules.
     */
    export class Compile extends BuildStage.Base
    {
        /**
         * Gets if errors encountered when building a module are fatal and should end the whole process.
         * If this is false, other modules may continue to be compiled after an error has been encountered.
         */
        public get errorsAreFatal() { return true; }

        /**
         * Gets if the executeModule function should be called in parallel for all modules.
         * Ddependencies are still considered and processed serially.
         */
        public get runInParallel() { return true; }

        public get name() { return this.unitName; }

        constructor(public readonly unitName: string)
        {
            super();
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected async executeModule(module: Module.Base): Promise<BuildStage.Result>
        {
            // Don't do anything unless we have source
            const unit = module.getCompilationUnit(this.unitName);
            if (unit == null) { return { workDone: false, errorCount: 0 }; }

            const result = await Build.compileUnit(unit);
            if (result.errorCount === 0) { await module.saveChecksums(); }
            return result;
        }
    }

    export const compile = new Compile(Module.CompilationUnits.Source);
    BuildStage.register({}, compile);
}