namespace RS.Build.Directives
{
    /** Contains directive metadata for a module. */
    export interface Metadata
    {
        usesDefines: string[];
    }

    /**
     *
     * Get checksum, which encodes:
     * - which defines are used by the unit: if a define usage is added or removed, the object key-value mapping changes
     * - which defines are configured: if a define is added or removed, the object key-value mapping changes
     * - what defines are configured to: if a define value changes, the object changes
     *
     * @param usesDefines
     * @param defineValues
     */
    function getDefineChecksum(usesDefines: ReadonlyArray<string>, defineValues: Util.Map<boolean | string>): string
    {
        // Sort them so that order of appearance doesn't affect result
        const sortedDefines = [...usesDefines].sort();
        const checksumObj: RS.Util.Map<string | boolean> = {};
        for (const define of sortedDefines)
        {
            checksumObj[define] = defineValues[define];
        }
        return Checksum.getSimple(checksumObj);
    }

    function validateMetadata(data: any): data is Metadata
    {
        if (!Is.object(data)) { return false; }
        if (!Is.array(data.usesDefines)) { return false; }
        if ((data.usesDefines as ReadonlyArray<any>).some((v) => !Is.string(v) && !Is.boolean(v))) { return false; }
        return true;
    }

    function getMetadataPath(unit: CompilationUnit): string
    {
        return Path.replaceExtension(unit.outJSFile, "_directiveData.json");
    }

    /** Returns whether or not the given unit needs to be recompiled due to directives. */
    export async function checkRecompileNeeded(unit: CompilationUnit): Promise<boolean>
    {
        if (unit.recompileNeeded) { return true; }

        // Find out if we need a recompilation due to directive change
        const metadataPath = getMetadataPath(unit);
        if (!await FileSystem.fileExists(metadataPath)) { return true; }

        // Find defines in common with allDefines
        let metadata: any;
        try
        {
            metadata = await JSON.parseFile(metadataPath);
        }
        catch (err)
        {
            Log.warn(`${err}; recompilation required`, unit.name);
            return true;
        }

        if (!validateMetadata(metadata))
        {
            Log.warn(`Directive metadata invalid; recompilation required`, unit.name);
            return true;
        }

        const checksum = getDefineChecksum(metadata.usesDefines, Config.activeConfig.defines);
        return unit.module.checksumDB.diff("defines", checksum);
    }

    /** Adds the directive pre-processor to the given unit. */
    export function addPreProcessor(unit: CompilationUnit): Metadata
    {
        const metadata: Metadata = { usesDefines: [] };
        unit.addPreProcessor(function (src)
        {
            const result = RS.Directives.parse(src, Config.activeConfig.defines);

            // Store metadata
            for (const define of result.usesDefines)
            {
                if (metadata.usesDefines.indexOf(define) === -1)
                {
                    metadata.usesDefines.push(define);
                }
            }

            // Set checksum
            const checksum = getDefineChecksum(metadata.usesDefines, Config.activeConfig.defines);
            this.module.checksumDB.set("defines", checksum);

            return { output: result.content, workDone: result.usesDefines.length > 0 };
        });
        return metadata;
    }

    /**
     * Persists the given metadata for the given unit.
     * @returns A Promise which resolves once the data has been persisted.
     */
    export function saveMetadata(unit: CompilationUnit, metadata: Metadata): PromiseLike<void>
    {
        const path = getMetadataPath(unit);
        return FileSystem.writeFile(path, JSON.stringify(metadata));
    }
}