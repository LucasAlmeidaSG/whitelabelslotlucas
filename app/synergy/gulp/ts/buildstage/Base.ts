/// <reference path="../module/Base.ts" />

namespace RS.BuildStage
{
    /**
     * Build stage execution result.
     */
    export interface Result
    {
        workDone: boolean;
        errorCount: number;
    }

    /**
     * Encapsulates a generic build stage.
     */
    export abstract class Base
    {
        /**
         * Gets if errors encountered when building a module are fatal and should end the whole process.
         * If this is false, other modules may continue to be compiled after an error has been encountered.
         */
        public get errorsAreFatal() { return false; }

        /**
         * Gets if the executeModule function should be called in parallel for all modules.
         * Ddependencies are still considered and processed serially.
         */
        public get runInParallel() { return false; }

        public readonly name?: string | null;

        /**
         * Executes this build stage safely, catching and reporting any errors.
         */
        public async executeSafe(moduleList: List<Module.Base>): Promise<Result>
        {
            try
            {
                return await this.execute(moduleList);
            }
            catch (err)
            {
                Log.error(err.stack || err);
                return { workDone: true, errorCount: 1 };
            }
        }

        /**
         * Executes this build stage.
         */
        public async execute(moduleList: List<Module.Base>): Promise<Result>
        {
            Log.pushContext(`${this}`);

            let workDone = false;
            let errorCount = 0;

            if (this.runInParallel)
            {
                // Derive batches
                const batches = Module.getBatchSet(moduleList.data);
                for (const batch of batches.data)
                {
                    const promises: PromiseLike<Result>[] = [];

                    // Process each module in the batch in parallel
                    for (const module of batch.data)
                    {
                        const promise = this.executeModule(module)
                            .then(result =>
                            {
                                workDone = result.workDone || workDone;
                                errorCount += result.errorCount;
                                return result;
                            }, err =>
                            {
                                Log.error(err.stack || err, module.name);
                                ++errorCount;
                                return { workDone: false, errorCount: 1 };
                            });
                        promises.push(promise);
                    }

                    await Promise.all(promises);
                    if (errorCount > 0 && this.errorsAreFatal) { break; }
                }
            }
            else
            {
                // Iterate them
                for (const dep of moduleList.data)
                {
                    Log.pushContext(dep.name);
                    try
                    {
                        const moduleResult = await this.executeModule(dep);
                        workDone = moduleResult.workDone || workDone;
                        errorCount += moduleResult.errorCount;
                    }
                    catch (err)
                    {
                        Log.error(err.stack || err);
                        ++errorCount;
                    }
                    Log.popContext(dep.name);
                    if (errorCount > 0 && this.errorsAreFatal) { break; }
                }
            }

            if (!workDone && errorCount === 0)
            {
                Log.info("No changes found, no work to do");
            }

            Log.popContext(`${this}`);

            return { workDone, errorCount };
        }

        public toString()
        {
            const className = Object.getPrototypeOf(this).constructor.name;
            return this.name ? `${className}:${this.name}` : className;
        }

        /**
         * Executes this build stage for the given module only.
         * @param module
         */
        protected abstract executeModule(module: Module.Base): PromiseLike<Result>;
    }
}