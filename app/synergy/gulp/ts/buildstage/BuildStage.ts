/// <reference path="Base.ts" />
/// <reference path="../util/List.ts" />

namespace RS.BuildStage
{
    /** Settings for a build stage. */
    export interface Settings
    {
        /** Any stages that should come before this one. */
        before?: Base[];

        /** Any stages that should come after this one. */
        after?: Base[];

        /** Only use this stage if explicitly requested. */
        explicit?: boolean;
    }

    const buildStages = new List<{ settings: Settings; stage: Base; }>();

    /**
     * Registers a build stage with the system.
     * @param settings
     * @param stage 
     */
    export function register(settings: Settings, stage: Base)
    {
        buildStages.add({ settings, stage });
    }

    /**
     * Gets all build stages in order of execution.
     */
    export function getStages(): Base[]
    {
        const result = buildStages.map(s => s.stage);
        result.resolveOrder(
            stage => buildStages.find(item => item.stage === stage).settings.before || [],
            stage => buildStages.find(item => item.stage === stage).settings.after || []
        );
        return result.dataCopy;
    }
}