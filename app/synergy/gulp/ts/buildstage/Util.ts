namespace RS.Build
{
    /** Compiles the given compilation unit. Returns whether or not work was done. */
    export async function compileUnit(unit: CompilationUnit): Promise<BuildStage.Result>
    {
        if (unit.validityState === CompilationUnit.ValidState.Uninitialised) { await unit.init(); }
        const directiveData = Directives.addPreProcessor(unit);

        switch (unit.validityState)
        {
            case CompilationUnit.ValidState.MissingDependency:
            case CompilationUnit.ValidState.NoTSConfig:
            case CompilationUnit.ValidState.Uninitialised:
                Log.error(`Bad unit validity state: ${CompilationUnit.ValidState[unit.validityState]}`, unit.name);
                return { workDone: true, errorCount: 1 };
        }

        const recompileNeeded = unit.recompileNeeded || await Directives.checkRecompileNeeded(unit);
        if (recompileNeeded)
        {
            const result = await unit.execute();
            if (result.success)
            {
                await Directives.saveMetadata(unit, directiveData);
            }
            else
            {
                for (const err of result.errors)
                {
                    Log.error(err, unit.name);
                }

                return { workDone: true, errorCount: result.errors.length };
            }

            Log.info(`Compiled in ${(result.time / 1000).toFixed(2)} s`, unit.name);
            return { workDone: true, errorCount: 0 };
        }
        else if (unit.lintNeeded)
        {
            try
            {
                await unit.lintFiles();
                return { workDone: true, errorCount: 0 };
            }
            catch (err)
            {
                if (!(err instanceof TSLint.LintFailure)) { Log.error(err); }
                return { workDone: true, errorCount: 1 };
            }
        }

        return { workDone: false, errorCount: 0 };
    }
}