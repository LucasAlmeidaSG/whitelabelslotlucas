/// <reference path="../util/Task.ts" />
/// <reference path="Build.ts" />

namespace RS.Tasks
{
    export const manual = Build.task({ name: "help", description: "Displays task descriptions" }, async () =>
    {
        const tasks: ReadonlyArray<Build.Task> = Build.getTasks();
        Log.info(`Synergy gulp tasks`);
        Log.info(`************************`);
        for (const task of tasks)
        {
            Log.info(`${task.settings.name}: ${task.settings.description || "unknown"}`);
        }
        const rootConfig = (RS.Config.activeConfig as (Config.Base & {tasks: {[name: string]: {description: string}}}));
        if (rootConfig.tasks)
        {
            const rootTasks = Object.keys(rootConfig.tasks);
            for (const rootTask of rootTasks)
            {
                Log.info(`${rootTask}: ${rootConfig.tasks[rootTask].description || "custom task"}`);
            }
        }
    });
}