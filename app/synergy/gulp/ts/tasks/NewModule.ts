/// <reference path="../util/Task.ts" />
/// <reference path="../util/Input.ts" />

namespace RS.Tasks
{
    export const NewModule = Build.task({ name: "new", description: "Utility for building a new module" }, async () =>
    {
        Log.pushContext("NewModule");
        Log.info("Enter the name of the module to use: ");
        const name = await Input.readLine();

        const nameSplit = name.split(".");

        // Find an existing module with the most matching name segments
        let bestMatch: Module.Base|null = null;
        let bestMatchSegs = 0;
        for (const module of Module.getAll())
        {
            const moduleName = module.name.split(".");
            let matchSegs = Math.min(moduleName.length, nameSplit.length);
            for (let i = 0, l = matchSegs; i < l; ++i)
            {
                if (moduleName[i] !== nameSplit[i])
                {
                    matchSegs = i;
                    break;
                }
            }
            if (matchSegs > bestMatchSegs)
            {
                bestMatch = module;
                bestMatchSegs = matchSegs;
            }
        }

        let rootPath: string;
        if (bestMatch != null)
        {
            rootPath = bestMatch.path;
            for (let i = 0, l = bestMatch.name.match(/\./g).length; i <= l; ++i)
            {
                rootPath = Path.combine(rootPath, "..");
            }
            rootPath = Path.normalise(rootPath);
        }
        else
        {
            rootPath = "modules";
        }


        const path = Path.combine(rootPath, name.replace(/\./g, "/"));

        await FileSystem.createPath(path);

        const formattedName = Module.formatName(name);

        const moduleInfo: Module.Info =
        {
            title: name,
            description: "Description goes here",
            dependencies: ["Core"]
        };
        moduleInfo["$schema"] = Path.relative(path, "schemas/moduleinfo.json");
        if (name !== formattedName) { moduleInfo.name = name; }

        await FileSystem.writeFile(Path.combine(path, "moduleinfo.json"), JSON.stringify(moduleInfo, null, 2));

        await FileSystem.createPath(Path.combine(path, "src"));

        const tsconfig =
        {
            compilerOptions:
            {
                target: "es5",
                lib:
                [
                    "dom",
                    "es2015.iterable",
                    "es2015.promise",
                    "es5"
                ],
                rootDir: ".",
                outFile: "../build/src.js",
                sourceMap: true,
                inlineSourceMap: false,
                inlineSources: true,
                experimentalDecorators: true,
                declaration: true,
                noEmitHelpers: true
            }
        };

        await FileSystem.writeFile(Path.combine(path, "src", "tsconfig.json"), JSON.stringify(tsconfig, null, 2));

        Log.info(`Created module by name '${name}' at '${path}'.`);
        Log.popContext("NewModule");
    });
}
