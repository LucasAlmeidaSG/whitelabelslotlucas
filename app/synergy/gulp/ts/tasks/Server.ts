/// <reference path="Assemble.ts" />

namespace RS.Tasks
{
    const port = new IntegerEnvArg("PORT", 8080);
    const cache = new BooleanEnvArg("CACHE", false);
    const debug = new BooleanEnvArg("DEBUG", false);

    export const serve = Build.task({ name: "serve", requires: [ Tasks.assemble ], description: "Assembles and serves the project using a minimal server" }, async () =>
    {
        const modulePath = Path.combine(process.cwd(), "node_modules", "http-server");
        const binPath = Path.combine(modulePath, "bin", "http-server");

        const config = await Config.get();
        const rootDir = config.assemblePath || "";

        const args: string[] = [rootDir, `-p${port.value}`];

        if (!cache.value)
        {
            // Disable client caching
            args.push("-c-1");
        }

        Log.info("You can also install http-server globally to serve any arbitary folder.");
        Log.info("It's as simple as:");
        Log.info("npm install -g http-server");
        Log.info("http-server <path>");

        Log.info(`Listening on http://localhost:${port.value}`);
        
        await Command.run(binPath, args, null, debug.value);
    });
}