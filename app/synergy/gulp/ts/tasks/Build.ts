/// <reference path="../util/Task.ts" />
/// <reference path="../buildstage/BuildStage.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Clean.ts" />
/// <reference path="../util/EnvArg.ts" />

namespace RS.Tasks
{
    const onlyEnvArg = new EnvArg("ONLY", "");

    export const build = Build.task({ name: "build", after: [ Tasks.clean ], description: "Builds root module and its dependencies" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(RS.Config.activeConfig.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Gather all dependent modules
        const allDeps = RS.Module.getDependencySet([ RS.Config.activeConfig.rootModule, ...RS.Config.activeConfig.extraModules ]);

        // Get all build stages
        const buildStages = BuildStage.getStages();

        // Execute in order
        for (const stage of buildStages)
        {
            const result = await stage.executeSafe(allDeps);
            if (result.errorCount > 0)
            {
                throw new Error(`${result.errorCount} errors when executing build stage ${stage}`);
            }
        }

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    export const buildOnly = Build.task({ name: "build-only", after: [ Tasks.clean ], description: "Builds only the specified modules. Use: 'ONLY=MOD,MOD2 gulp build-only'" }, async () =>
    {
        Profile.reset();

        let inputModulesRaw: string;
        if (onlyEnvArg.value !== onlyEnvArg.defaultValue)
        {
            inputModulesRaw = onlyEnvArg.value;
        }
        else
        {
            inputModulesRaw = await Input.readLine();
        }
        const inputModules = inputModulesRaw.split(",").map((name) => name.trim());
        if (inputModules.length < 0 || inputModules[0].length === 0) { return; }

        const allDeps = Module.getDependencySet(inputModules);
        const buildStages = BuildStage.getStages();
        for (const stage of buildStages)
        {
            const result = await stage.executeSafe(allDeps);
            if (result.errorCount > 0)
            {
                throw new Error(`${result.errorCount} errors when executing build stage ${stage}`);
            }
        }

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    export const buildAll = Build.task({ name: "build-all", after: [ Tasks.clean ], description: "Builds all modules" }, async () =>
    {
        Profile.reset();

        // Get all modules
        const allModules = RS.Module.getDependencySet(RS.Module.getAll().map((m) => m.name));

        // Compile build components
        await RS.Module.compileBuildComponents(allModules);

        // Load build components
        for (const module of allModules.data)
        {
            if (!module.buildLoaded)
            {
                RS.Log.pushContext(module.name);
                try
                {
                    await module.loadBuild();
                }
                catch (err)
                {
                    throw err;
                }
                finally
                {
                    RS.Log.popContext(module.name);
                }
            }
        }

        // Get all build stages
        const buildStages = BuildStage.getStages();

        // Execute in order
        for (const stage of buildStages)
        {
            try
            {
                const result = await stage.executeSafe(allModules);
                if (result.errorCount > 0)
                {
                    throw new Error(`${result.errorCount} errors when executing build stage ${stage}`);
                }
            }
            catch (err)
            {
                Log.error(err.stack || err);
                throw new Error("build-all failed");
            }
        }

        if (EnvArgs.profile.value) { Profile.dump(); }
    });
}