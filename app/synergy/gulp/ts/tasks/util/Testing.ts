namespace RS.Tasks.Testing
{
    export async function assembleThirdPartyJS(testUnits: List<Build.CompilationUnit>, dir: string)
    {
        const thirdPartyCodeSources: Util.Map<Assembler.CodeSource> = {};
        for (const unit of testUnits.data)
        {
            const depSet = Module.getDependencySet([ unit.module.name, ...(unit.module.info.testdependencies || []) ]);
            for (const module of depSet.data)
            {
                await Assembler.gatherThirdPartyDependencies(module, thirdPartyCodeSources);
            }
        }
        await Assembler.assembleJS(dir, "thirdparty", Object.keys(thirdPartyCodeSources).map(k => thirdPartyCodeSources[k]), {
            withHelpers: false,
            withSourcemaps: false,
            minify: true
        });
    }

    export async function executeUnit(unit: Build.CompilationUnit)
    {
        const result = await unit.execute();
        if (!result.success)
        {
            for (const err of result.errors)
            {
                Log.error(err, unit.module.name);
            }
            throw new Error("Game tests failed to build.");
        }
        Log.info(`Compiled in ${(result.time / 1000).toFixed(2)} s`, `${unit.name}`);
    }

    export async function initialiseUnit(unit: Build.CompilationUnit)
    {
        let compiled = false;
        await unit.init();
        if (unit.validityState === Build.CompilationUnit.ValidState.Initialised)
        {
            if (unit.recompileNeeded)
            {
                await executeUnit(unit);
                compiled = true;
            }
        }
        return compiled;
    }

    export function isUnitValid(unit: Build.CompilationUnit)
    {
        return unit.validityState === Build.CompilationUnit.ValidState.Initialised || unit.validityState === Build.CompilationUnit.ValidState.Compiled;
    }

    const karma: karma.Karma = require("karma");
    const colors = require("colors/safe");
    export async function runKarma(configPath: string, cliOptions: Partial<karma.ConfigOptions> = {}): Promise<karma.TestResults>
    {
        // Run tests
        const config = karma.config.parseConfig(configPath, cliOptions);
        const results = await runKarmaWithConfig(config);
        console.log(colors.reset(" "));
        return results;
    }

    export function runKarmaWithConfig(config: karma.ConfigOptions): Promise<karma.TestResults>
    {
        return new Promise((resolve, reject) =>
        {
            let testResults: karma.TestResults | null = null;
            const server = new karma.Server(config, (exitCode) =>
            {
                if (exitCode === 0)
                {
                    if (testResults)
                    {
                        resolve(testResults);
                    }
                    else
                    {
                        reject(`no test results`);
                    }
                }
                else
                {
                    if (testResults)
                    {
                        resolve(testResults);
                    }
                    else
                    {
                        reject(`got exit code ${exitCode}`);
                    }
                }
            });
            server.on("run_complete", (browsers, results: karma.TestResults) =>
            {
                testResults = results;
            });
            server.start();
        });
    }
}