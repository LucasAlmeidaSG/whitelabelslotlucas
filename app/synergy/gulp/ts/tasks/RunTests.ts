/// <reference path="../util/Task.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Build.ts" />

namespace RS.Tasks
{
    const onlyEnvArg = new EnvArg("ONLY", "");

    const defaultTSConfig: TSConfig =
    {
        compilerOptions:
        {
            // Basic requirements
            noEmitHelpers: true,
            rootDir: ".",

            declaration: true,
            declarationMap: true,

            // Source maps, optional
            sourceMap: true,
            inlineSources: true,
            inlineSourceMap: false,

            // Optional compiler features
            experimentalDecorators: true,
            stripInternal: true,

            outFile: "../build/testsrc.js",

            target: "ES5",
            lib: ["DOM", "ES2015.Promise", "ES2015.Iterable", "ES5"],
            typeRoots: []
        }
    };

    async function doTests(moduleList: List<Module.Base>)
    {
        const moduleTestUnits = new List<Build.CompilationUnit>();
        const testDependencies = new List<Module.Base>();

        // Get path to gulp
        const testDefinitionsPath = Path.combine(__dirname, "testing", "definitions");

        // Compile all tests
        let compileCnt = 0;
        for (const module of moduleList.data)
        {
            let saveChecksums = false;

            // Get source unit
            const srcUnit = module.getCompilationUnit(Module.CompilationUnits.Source);
            if (srcUnit && srcUnit.validityState === Build.CompilationUnit.ValidState.Uninitialised)
            {
                await srcUnit.init();
            }

            // Create test unit
            const unit = module.addCompilationUnit("tests", "tests", defaultTSConfig);
            if (srcUnit)
            {
                unit.dependencies.add({
                    kind: Build.CompilationUnit.DependencyKind.CompilationUnit,
                    name: srcUnit.name,
                    compilationUnit: srcUnit
                });
                unit.dependencies.add({
                    kind: Build.CompilationUnit.DependencyKind.DefinitionsFolder,
                    name: `${srcUnit.name}.Dependencies`,
                    path: Path.combine(srcUnit.path, "dependencies")
                });
                unit.dependencies.add({
                    kind: Build.CompilationUnit.DependencyKind.DefinitionsFolder,
                    name: `testing`,
                    path: testDefinitionsPath
                });

                const testDeps = srcUnit.module.info.testdependencies || [];
                for (const testDep of testDeps)
                {
                    const testModule = Module.getModule(testDep);
                    const dependencySet = testModule.gatherDependencySet();
                    testDependencies.addRange(dependencySet);
                }
            }

            Module.CompilationDependencies.addModule(unit, module, Module.CompilationUnits.Source);

            if (await Testing.initialiseUnit(unit)) { ++compileCnt; saveChecksums = true; }
            if (Testing.isUnitValid(unit)) { moduleTestUnits.add(unit); }

            if (saveChecksums) { await module.saveChecksums(); }
        }

        testDependencies.removeDuplicates();
        const requiredTestDependencies = testDependencies.data
            .filter((module) =>
            {
                const depUnit = module.getCompilationUnit(Module.CompilationUnits.Source);
                if (!depUnit) { return false; }
                return depUnit.validityState === Build.CompilationUnit.ValidState.Uninitialised;
            })
            .map((module) => module.name);

        if (requiredTestDependencies.length > 0)
        {
            Log.info("Building test dependencies...");
            const testDependencySet = Module.getDependencySet(requiredTestDependencies);
            const buildStages = BuildStage.getStages();
            for (const stage of buildStages)
            {
                const result = await stage.executeSafe(testDependencySet);
                if (result.errorCount > 0)
                {
                    throw new Error(`${result.errorCount} errors when executing build stage`);
                }
            }
        }

        // If we have no tests, exit
        if (moduleTestUnits.length === 0)
        {
            Log.warn("No tests to run!");
            return;
        }

        if (compileCnt === 0)
        {
            Log.info("No changes to module tests detected");
        }

        if (moduleTestUnits.length > 0) { await doUnitTests(moduleTestUnits); }
    }

    async function doUnitTests(units: List<Build.CompilationUnit>)
    {
        Log.info("Assembling tests...");
        const testBuildDir = "tests";

        const timer = new RS.Timer();
        timer.start();
        await FileSystem.deletePath(testBuildDir);
        await FileSystem.createPath(testBuildDir);

        const promises: PromiseLike<void>[] = [];
        // Gather all third party dependencies
        promises.push(Testing.assembleThirdPartyJS(units, testBuildDir));

        // Iterate each unit and assemble it into an individual js
        for (const unit of units.data)
        {
            // Convert each unit to a code source
            const codeSources: Assembler.CodeSource[] = [];
            const depSet = Module.getDependencySet([ unit.module.name, ...(unit.module.info.testdependencies || []) ]);
            await Assembler.CodeSources.addModules(codeSources, depSet.data, Module.CompilationUnits.Source);
            await Assembler.CodeSources.addUnit(codeSources, unit);

            // Assemble js
            promises.push(Assembler.assembleJS(testBuildDir, `${unit.module.name}.spec`, codeSources, {
                withHelpers: true,
                withSourcemaps: false,
                minify: false
            }));
        }

        await Promise.all(promises);
        Log.info(`Assembled in ${(timer.elapsed / 1000).toFixed(2)} s`);
        timer.stop();

        // Run tests
        const results = await Testing.runKarma(Path.combine(__dirname, "testing", "karma.conf.js"));
        if (results.failed > 0)
        {
            throw new Error(`${results.failed} tests failed`);
        }
        if (results.error || results.exitCode !== 0)
        {
            throw new Error(`Error while running tests`);
        }
    }

    export const test = Build.task({ name: "test", requires: [ Tasks.build ], description: "Runs unit tests for root module and its dependencies" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Gather all dependent modules
        const allDeps = RS.Module.getDependencySet([ RS.Config.activeConfig.rootModule, ...RS.Config.activeConfig.extraModules ]);

        // Run tests
        await doTests(allDeps);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    export const testOnly = Build.task({ name: "test-only", requires: [ Tasks.buildOnly ], description: "Runs unit tests for specified modules. Use: \"ONLY=MOD,MOD2 gulp test-only\"" }, async () =>
    {
        Profile.reset();

        let inputModulesRaw: string;
        if (onlyEnvArg.value !== onlyEnvArg.defaultValue)
        {
            inputModulesRaw = onlyEnvArg.value;
        }
        else
        {
            inputModulesRaw = await Input.readLine();
        }
        const inputModules = inputModulesRaw.split(",").map((name) => name.trim());
        if (inputModules.length < 0 || inputModules[0].length === 0) { return; }

        const allModules = new List<Module.Base>();
        for (const moduleName of inputModules)
        {
            const module = Module.getModule(moduleName);
            if (module == null)
            {
                Log.warn(`Module '${moduleName}' not found`);
            }
            else
            {
                allModules.add(module);
            }
        }
        if (allModules.length === 0) { return; }

        // Run tests
        await doTests(allModules);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    export const testAll = Build.task({ name: "test-all", requires: [ Tasks.buildAll ], description: "Runs unit tests for all modules" }, async () =>
    {
        Profile.reset();

        // Get all modules
        const allModules = RS.Module.getDependencySet(RS.Module.getAll().map(m => m.name));

        // Run tests
        await doTests(allModules);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });
}
