/// <reference path="../util/Task.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Build.ts" />

namespace RS.Tasks
{
    const version = new EnvArg("VERSION", "", true);

    async function doDocs(moduleList: List<Module.Base>)
    {
        // Compile all code
        let compileCnt = 0;
        for (const module of moduleList.data)
        {
            // Get source unit
            const srcUnit = module.getCompilationUnit(Module.CompilationUnits.Source);
            if (srcUnit)
            {
                await srcUnit.init();
            }
        }

        Log.info("Assembling code...");
        let timer = new RS.Timer();
        timer.start();

        // Convert each unit to a code source
        const codeSources: Assembler.CodeSource[] = [];
        const thirdPartyCodeSources: Util.Map<Assembler.CodeSource> = {};
        for (const module of moduleList.data)
        {
            const srcUnit = module.getCompilationUnit(Module.CompilationUnits.Source);
            if (srcUnit)
            {
                codeSources.push({
                    name: srcUnit.name,
                    dtsContent:  await FileSystem.readFile(srcUnit.outDTSFile),
                    content: await FileSystem.readFile(srcUnit.outJSFile),
                    sourceMap: await FileSystem.readFile(srcUnit.outMapFile),
                    sourceMapDir: srcUnit.path
                });
            }
            await Assembler.gatherThirdPartyDependencies(module, thirdPartyCodeSources);
        }

        // Output directory
        const docsDirectory = "docs";
        await FileSystem.createPath(docsDirectory);

        // Assemble dts files
        await Promise.all([
            Assembler.assembleDts(docsDirectory, "docs", codeSources, {
                withHelpers: false,
                withSourcemaps: false,
                minify: false
            })
        ]);

        Log.info(`Assembled in ${(timer.elapsed / 1000).toFixed(2)} s`);
        timer.stop();

        Log.info("Generating docs...");
        timer = new RS.Timer();
        timer.start();
        //Rename .d.ts file to .ts for TypeDoc consumption
        await FileSystem.copyFile(Path.combine(docsDirectory, "docs.d.ts"), Path.combine(docsDirectory, "Synergy.ts"));
        await FileSystem.deletePath(Path.combine(docsDirectory, "docs.d.ts"));
        // Run TypeDoc
        try
        {
            const template = Path.combine(__dirname, "docs", "template");
            const readme = Path.combine(__dirname, "docs", "README.md");
            let title = "Synergy API Documentation";
            if (version.isSet)
            {
                title += ` | v${version.value}`;
            }
            await Command.run("node", [
                "node_modules/typedoc/bin/typedoc",
                "--out", Path.combine(docsDirectory, "output"),
                "--ignoreCompilerErrors",
                "--target", "ES5",
                "--theme", template,
                "--readme", readme,
                "--name", title,
                docsDirectory
            ], undefined, false);
        }
        catch (err)
        {
            //We expect it to "fail" as we aren't providing dependencies, will build docs anyway
        }

        Log.info(`Docs generated in in ${(timer.elapsed / 1000).toFixed(2)} s`);
        timer.stop();
    }

    export const documentAll = Build.task({ name: "docs", requires: [ Tasks.buildAll ], description: "Builds synergy documentation" }, async () =>
    {
        Profile.reset();

        // Get all modules
        const allModules = RS.Module.getDependencySet(RS.Module.getAll().map(m => m.name));

        // Create docs
        await doDocs(allModules);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });
}