namespace RS.Build
{
    const tsFormatter = require("typescript-formatter");

    const formatOptions =
    {
        tsfmt: true,
        tsfmtFile: Path.combine(__dirname, "linting", "tsfmt.json")
    };

    async function doFormat(modules: List<Module.Base>)
    {
        for (const module of modules.data)
        {
            const srcFiles = await FileSystem.readFiles(Path.combine(module.path, "src"));
            const buildSrcFiles = await FileSystem.readFiles(Path.combine(module.path, "buildsrc"));
            const allFiles = [ ...srcFiles, ...buildSrcFiles ].filter((file) => Path.extension(file) === ".ts");
            // Log.debug(`${module.name}: ${allFiles.join(", ")}`);
            if (allFiles.length > 0)
            {
                Log.pushContext(module.name);
                Log.info(`Formatting ${allFiles.length} ts files...`);
                try
                {
                    const resultMap = await tsFormatter.processFiles(allFiles, formatOptions);
                    let formatCount = 0;
                    for (const fileName in resultMap)
                    {
                        const result = resultMap[fileName];
                        if (result.error)
                        {
                            Log.warn(`${result.fileName}: ${result.message}`);
                        }
                        else if (Is.string(result.dest))
                        {
                            ++formatCount;
                            if (result.dest !== result.src)
                            {
                                await FileSystem.writeFile(fileName, result.dest);
                            }
                        }
                    }
                    if (formatCount !== allFiles.length)
                    {
                        Log.warn(`Only formatted ${formatCount} of ${allFiles.length} files`);
                    }
                }
                catch (err)
                {
                    Log.error(err.stack || err);
                }
                Log.popContext(module.name);
            }
        }
    }

    /**
     * Executes code formatter for the root module only.
     */
    export const format = Build.task({ name: "format", description: "Formats the root module" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Gather all dependent modules
        const allModules = new List([ rootModule ]);

        // Do code formatting
        await doFormat(allModules);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Executes code formatter for a specific set of modules only.
     */
    export const formatOnly = Build.task({ name: "format-only", description: "Formats the specified modules. Use: 'ONLY=MOD,MOD2 gulp format-only'" }, async () =>
    {
        Profile.reset();

        Log.info(`Please enter the names of the modules to formatted, separated by comma:`);

        const inputModulesRaw = await Input.readLine();
        const inputModules = inputModulesRaw.split(",").map((name) => name.trim());
        if (inputModules.length < 0 || inputModules[0].length === 0) { return; }

        const allModules = new List<Module.Base>();
        for (const moduleName of inputModules)
        {
            const module = Module.getModule(moduleName);
            if (module == null)
            {
                Log.warn(`Module '${moduleName}' not found`);
            }
            else
            {
                allModules.add(module);
            }
        }
        if (allModules.length === 0) { return; }

        // Do code formatting
        await doFormat(allModules);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });

    /**
     * Executes code formatter for all loaded modules.
     */
    export const formatAll = Build.task({ name: "format-all", description: "Formats all modules" }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(config.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Get all modules
        const allModules = RS.Module.getDependencySet(RS.Module.getAll().map((m) => m.name));

        // Do code formatting
        await doFormat(allModules);

        if (EnvArgs.profile.value) { Profile.dump(); }
    });
}
