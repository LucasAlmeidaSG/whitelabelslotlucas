/// <reference path="../util/Task.ts" />
/// <reference path="Build.ts" />

//https://www.fileformat.info/info/unicode/block/box_drawing/list.htm
//⊢
//⌞
//║
//│
//╠
//╚
namespace RS.Tasks
{
    const displayDepthArg = new IntegerEnvArg("DEPTH", 0).value;
    let displayDepth: number = 0;
    const showCount = new BooleanEnvArg("COUNT", false);
    const targetModuleArg = new EnvArg("MODULE", "");
    const rootModuleArg = new EnvArg("ROOT", "");

    export const dependencyTree = Build.task({ name: "tree", description: "Shows Module dependency tree. E.g. \"DEPTH=2 COUNT=TRUE MODULE=\"Core\" ROOT=\"MyGame\" gulp tree\"" }, () => buildTree());
    
    /** Print out module dependency tree from the root module. */
    export async function buildTree(): Promise<void>
    {
        try
        {
            validateEnvArgs();
        }
        catch (e)
        {
            Log.error(`Failed to validate Environment Arguments:`);
            Log.error(e);
            Log.error(`Aborting`);
            return;
        }

        const rootModuleName = (rootModuleArg.value == "") ? RS.Config.activeConfig.rootModule : rootModuleArg.value;
        const rootModule = Module.getModule(rootModuleName);
        const deps = printDependencies(rootModule, 0);
        if (deps.deps.length == 0)
        {
            Log.error(`No module dependencies found for root module '${rootModule.name}'.`);
            if (targetModuleArg.value !== "")
            {
                Log.error(`Target module '${targetModuleArg.value}' was specified, check it was spelt correctly.`);
            }
            return;
        }
        Log.info(`\n${[ getModule(rootModule, ""), ...deps.deps ].join('\n')}`);
    };

    /** Return the printout of a module's dependencies. */
    function printDependencies(module: Module.Base, currentDepth: number, prefix: string = "  "): { deps: string[], hasModule: boolean }
    {
        const depStrings: string[] = [];
        let hasModule: boolean = false;
        for (const dep of module.dependencies)
        {
            const isTarget = (targetModuleArg.value == "" || targetModuleArg.value == dep.name);
            if (isTarget)
            {
                hasModule = true;
            }
            const lastDependency = (dep == module.dependencies[module.dependencies.length - 1]);
            const newPrefix = lastDependency ? "╚═" : "╠═";
            if (displayDepth == 0 || currentDepth < displayDepth)
            {
                const deps = printDependencies(dep, (currentDepth + 1), `${prefix}${lastDependency ? "" : "║"}    `);
                hasModule = hasModule || deps.hasModule;
                if (deps.hasModule || isTarget)
                {
                    depStrings.push(getModule(dep, `${prefix}${newPrefix} `));
                    depStrings.push(...deps.deps);
                }
            }
        }
        return { deps: depStrings, hasModule };
    }

    /** Get the total number of dependency nodes of a module. */
    /** TODO: Seperate the tree navigation and the formatting, to avoid re-counting nodes */
    function getDependencyCount(module: Module.Base): number
    {
        if (module.dependencies.length == 0) { return 0; }
        return module.dependencies.map((d) => getDependencyCount(d)).reduceRight((a, b) => a + b) + module.dependencies.length;
    }

    /** Get a formatted string of a specific module entry. */
    function getModule(module: Module.Base, prefix: string): string
    {
        const depCount = showCount.value ? getDependencyCount(module) : 0;
        return `${prefix}${module.name} ${depCount > 0 ? `[${depCount}]` : ""}`;
    }

    /** Check that the environment args are valid, throws an error if not. */
    function validateEnvArgs()
    {
        displayDepth = Math.max(0, displayDepthArg);
        validateModuleExists(targetModuleArg.value, `Failed to validate env arg "MODULE"`);
        validateModuleExists(rootModuleArg.value, `Failed to validate env arg "ROOT"`);
    }

    /** Validates that a module exists */
    function validateModuleExists(moduleName: string, errorPrefix: string)
    {
        if (moduleName == "") { return; }
        if (!Module.getModule(moduleName))
        {
            throw Error(`${errorPrefix} - module '${moduleName}' could not be found.`);
        }
    }
}