/// <reference path="../util/Task.ts" />

namespace RS.Tasks
{
    export const list = Build.task({ name: "list", description: "Lists all modules" }, async () =>
    {
        const allModules = Module.getAll();
        for (const module of allModules)
        {
            Log.info(module.name);
        }
    });
}