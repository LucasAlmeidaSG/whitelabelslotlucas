/// <reference path="../util/Task.ts" />

namespace RS.Tasks
{
    export const noop = Build.task({ name: "noop", description: "Debug Task" }, async () =>
    {
        Log.debug("no-op");
    });
}