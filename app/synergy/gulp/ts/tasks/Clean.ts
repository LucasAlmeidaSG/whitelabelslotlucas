/// <reference path="../util/Task.ts" />

namespace RS.Tasks
{
    export const clean = Build.task({ name: "clean", description: "Deletes all built files" }, async () =>
    {
        Log.info("Cleaning all build directories...");
        const allModules = Module.getAll();
        for (const module of allModules)
        {
            await FileSystem.deletePath(Path.combine(module.path, "build"));
            await FileSystem.deletePath(Path.combine(module.path, "src", "dependencies"));
            await FileSystem.deletePath(Path.combine(module.path, "src", "generated"));
            await FileSystem.deletePath(Path.combine(module.path, "buildsrc", "dependencies"));
            await FileSystem.deletePath(Path.combine(module.path, "buildsrc", "generated"));
            await FileSystem.deletePath(Path.combine(module.path, "externs", "generated"));
            module.checksumDB.clear();
        }
    });
}