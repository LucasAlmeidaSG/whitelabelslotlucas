/// <reference path="../util/Task.ts" />
/// <reference path="../assemblestage/AssembleStage.ts" />
/// <reference path="../module/Loader.ts" />
/// <reference path="Build.ts" />
/// <reference path="Clean.ts" />

namespace RS.Tasks
{
    export const assemble = Build.task({ name: "assemble", requires: [ Tasks.build ], after: [ Tasks.clean ] }, async () =>
    {
        Profile.reset();

        const config = await Config.get();

        // Get root module
        const rootModule = Module.getModule(RS.Config.activeConfig.rootModule);
        if (rootModule == null)
        {
            Log.error("No root module found");
            return;
        }

        // Gather all dependent modules
        const allDeps = RS.Module.getDependencySet([ RS.Config.activeConfig.rootModule, ...RS.Config.activeConfig.extraModules ]);

        // Assemble settings
        const settings: AssembleStage.AssembleSettings =
        {
            outputDir: Config.activeConfig.assemblePath
        };

        // Delete existing path
        await FileSystem.deletePath(settings.outputDir);

        // Get all assemble stages
        const assembleStages = AssembleStage.getStages();

        // Execute in order
        for (const stage of assembleStages)
        {
            await stage.execute(settings, allDeps);
        }

        if (EnvArgs.profile.value) { Profile.dump(); }
    });
}