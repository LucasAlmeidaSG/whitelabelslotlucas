/// <reference path="../scaffold/API.ts" />
/// <reference path="../util/Task.ts" />

namespace RS.Tasks
{
    export const scaffold = Build.task({ name: "scaffold", description: "Outputs templates for various types of file" }, async () =>
    {
        const rootModule = Module.getModule(Config.activeConfig.rootModule);
        if (!rootModule)
        {
            Log.error("No root module found. Scaffolding failed.");
            return;
        }

        const all = Build.getAllScaffolds();
        const keys = Object.keys(all);
        if (keys.length === 0)
        {
            Log.error("No scaffolds found.");
            return;
        }

        Log.info("Enter a scaffold from the list below:");
        for (const type of keys)
        {
            const info = all[type];
            if (info.description)
            {
                Log.info(` ${type}: ${info.description}`);
            }
            else
            {
                Log.info(` ${type}`);
            }
        }

        function completer(line: string): [ string[], string ]
        {
            const hits = keys.filter(k => k.length >= line.length && k.substr(0, line.length) === line);
            // show all completions if none found
            return [hits.length ? hits : keys, line];
        }

        let scaffold: Build.Scaffold | null = null;
        while (!scaffold)
        {
            const type = await Input.readLine(completer);
            if (!type) { return; }

            const scaffoldInfo = Build.findScaffold(type);
            if (!scaffoldInfo)
            {
                Log.error(`Unknown scaffold type '${type}'`);
            }
            else
            {
                scaffold = scaffoldInfo.scaffold;
            }
        }

        await scaffold.generate(rootModule);
    });
}