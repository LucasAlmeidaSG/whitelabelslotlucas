/// <reference path="AST.ts" />

namespace RS.Build
{
    const recast = require("recast");
    const UglifyJS = require("uglify-js");

    /**
     * A compiled code file with an optional associated source map.
     */
    export class CompiledFile
    {
        protected _src: string;
        protected _map: object | null;

        /** Gets the code for this file. */
        public get source() { return this._src; }

        /** Gets the mappings for this file. */
        public get mappings() { return this._map; }

        public constructor(source: string, mappings?: object)
        {
            this._src = source;
            this._map = mappings;
        }

        @Profile("CompiledFile.transform")
        public transform(fn: (ast: AST) => void, sourceFileName: string, sourceMapName: string): void
        {
            const ast = recast.parse(this._src, { sourceFileName }) as AST;
            try
            {
                fn(ast);
            }
            catch (err)
            {
                Log.error(err.stack);
            }
            const result = recast.print(ast, { sourceMapName, inputSourceMap: this._map });
            this._src = result.code;
            this._map = result.map;
        }

        @Profile("CompiledFile.minify")
        public minify(sourceFileName: string): void
        {
            const result = UglifyJS.minify(this._src, {
                sourceMap:
                {
                    content: this._map,
                    filename: sourceFileName,
                    url: Path.replaceExtension(sourceFileName, ".js.map")
                },
                output:
                {
                    webkit: true,
                    wrap_iife: true
                }
            });
            if (result.error)
            {
                throw new Error(`${result.error.message} (${result.error.filename}:${result.error.line} ${result.error.col})`);
            }
            this._src = result.code;
            try
            {
                this._map = JSON.parse(result.map);
            }
            catch (err)
            {
                throw new Error(`Error whilst parsing source map: ${err}`);
            }
        }

        /**
         * Writes this compiled file to disk.
         * @param sourceFile
         * @param mapFile
         */
        @Profile("CompiledFile.write")
        public async write(sourceFile: string, mapFile?: string)
        {
            await FileSystem.createPath(Path.directoryName(sourceFile));
            await FileSystem.writeFile(sourceFile, this._src);
            if (this._map && mapFile != null) { await FileSystem.writeFile(mapFile, JSON.stringify(this._map)); }
        }

        /**
         * Loads a compiled file from disk.
         * @param sourceFile
         * @param mapFile
         */
        @Profile("CompiledFile.load")
        public static async load(sourceFile: string, mapFile?: string)
        {
            const src = await FileSystem.readFile(sourceFile);
            const map = mapFile ? await JSON.parseFile(mapFile) : null;
            return new CompiledFile(src, map);
        }
    }
}