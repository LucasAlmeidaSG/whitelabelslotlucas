namespace RS
{
    const _url = require("url") as typeof url;

    export class URL
    {
        private readonly _nativeURL: url.URL;
        
        constructor(host: string)
        {
            this._nativeURL = new _url.URL(host)
        }

        public get path(): string { return this._nativeURL.pathname; }
        public set path(value: string) { this._nativeURL.pathname = value; }

        public get port(): number | void { return this._nativeURL.port ? parseInt(this._nativeURL.port) : undefined; }
        public set port(value) { this._nativeURL.port = `${value}`; }

        public get params() { return this._nativeURL.searchParams;  }

        public toString() { return `${this._nativeURL}`; }
    }
}