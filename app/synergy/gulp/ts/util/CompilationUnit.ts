namespace RS.Build
{
    const strict = new BooleanEnvArg("STRICT", false);

    /**
     * Encapsulates a set of TypeScript source files to compile.
     */
    export class CompilationUnit
    {
        protected _name: string | null = null;
        protected _module: Module.Base | null = null;
        protected _path: string | null = null;
        protected _dependencies: List<CompilationUnit.Dependency>;
        protected _rawDependencies: List<CompilationUnit.RawDependency>;
        protected _validState: CompilationUnit.ValidState;
        protected _postProcessors: List<CompilationUnit.PostProcessor>;
        protected _preProcessors: List<CompilationUnit.PreProcessor>;

        protected _rootDir: string;
        protected _outFile: string;
        protected _emitsSourceMap: boolean;
        protected _emitsDefinitions: boolean;
        protected _emitsDefinitionMap: boolean;
        protected _requiresHelpers: boolean;

        protected _srcChecksum: string;
        protected _recompileNeeded: boolean;
        protected _lintNeeded: boolean;

        protected _defaultTSConfig: Readonly<TSConfig>;

        /** Gets or sets the default tsconfig.json. */
        public get defaultTSConfig() { return this._defaultTSConfig; }
        public set defaultTSConfig(value) { this._defaultTSConfig = value; }

        /** Gets or sets the name of this compilation unit. */
        public get name() { return this._name; }
        public set name(value) { this._name = value; }

        /** Gets or sets the module to which this compilation unit belongs. */
        public get module() { return this._module; }
        public set module(value) { this._module = value; }

        /** Gets or sets the path to the source code of this compilation unit, relative to the cwd. */
        public get path() { return this._path; }
        public set path(value) { this._path = value; }

        /** Gets the dependencies of this compilation unit. */
        public get dependencies() { return this._dependencies; }

        /** Gets the raw dependencies of this compilation unit. */
        public get rawDependencies() { return this._rawDependencies; }

        /** Gets the validity state of this compilation unit. */
        public get validityState() { return this._validState; }

        /** Gets the root directory of this compilation unit, relative to the cwd. */
        public get rootDir() { return this._rootDir; }

        /** Gets the output js file of this compilation unit, relative to the cwd. */
        public get outJSFile() { return this._outFile; }

        /** Gets the output js sourcemap file of this compilation unit, relative to the cwd. */
        public get outMapFile() { return Path.replaceExtension(this._outFile, ".js.map"); }

        /** Gets the output d.ts file of this compilation unit, relative to the cwd. */
        public get outDTSFile() { return Path.replaceExtension(this._outFile, ".d.ts"); }

        /** Gets the output d.ts sourcemap file of this compilation unit, relative to the cwd. */
        public get outDTSMapFile() { return Path.replaceExtension(this._outFile, ".d.ts.map"); }

        /** Gets if the output js code requires the ts helpers to be externally present. */
        public get requiresHelpers() { return this._requiresHelpers; }

        /** Gets the checksum for the source of this compilation unit. */
        public get srcChecksum() { return this._srcChecksum; }

        /** Gets if compiliation is needed for this compilation unit. */
        public get recompileNeeded() { return this._recompileNeeded; }

        /** Gets if lint is needed for this compilation unit. */
        public get lintNeeded() { return this._lintNeeded; }

        public constructor()
        {
            this._dependencies = new List();
            this._rawDependencies = new List();
            this._postProcessors = new List();
            this._preProcessors = new List();
            this._validState = CompilationUnit.ValidState.Uninitialised;
        }

        /**
         * Adds a post processor to this compilation unit.
         * @param postProcessor
         */
        public addPostProcessor(postProcessor: CompilationUnit.PostProcessor): void
        {
            this._postProcessors.add(postProcessor);
        }

        /**
         * Adds a pre-processor to this compilation unit.
         * @param preProcessor
         */
        public addPreProcessor(preProcessor: CompilationUnit.PreProcessor): void
        {
            this._preProcessors.add(preProcessor);
        }

        /**
         * Initialises this compilation unit.
         */
        @Profile("CompilationUnit.init")
        public async init()
        {
            if (this._validState !== CompilationUnit.ValidState.Uninitialised)
            {
                Log.warn("Attempted to re-initialise already-initialised compilation unit", this._name);
                return;
            }
            // Log.debug(`Initialising compilation unit '${this._name}'`);

            const tsConfig = await this.resolveTSConfig();
            if (!tsConfig)
            {
                this._validState = CompilationUnit.ValidState.NoTSConfig;
                return;
            }

            const compilerOpts = tsConfig.compilerOptions;
            this._rootDir = Path.normalise(Path.combine(this._path, compilerOpts && compilerOpts.rootDir || "."));
            this._outFile = Path.normalise(Path.combine(this._path, compilerOpts && compilerOpts.outFile || "../build/src.js"));
            this._emitsSourceMap = compilerOpts && compilerOpts.sourceMap || false;
            this._emitsDefinitions = compilerOpts && compilerOpts.declaration || false;
            this._emitsDefinitionMap = compilerOpts && compilerOpts.declarationMap || false;
            this._requiresHelpers = compilerOpts && compilerOpts.noEmitHelpers || false;

            // Resolve dependencies
            if (!await this.resolveDependencies())
            {
                this._validState = CompilationUnit.ValidState.MissingDependency;
                return;
            }

            // Compile checksum
            this._srcChecksum = await Checksum.getComposite(await FileSystem.readFiles(this._rootDir));

            // Done
            this._validState = CompilationUnit.ValidState.Initialised;

            // Check if we need to recompile
            if (!await FileSystem.fileExists(this._outFile))
            {
                this._recompileNeeded = true;
            }
            else if (this._module.checksumDB.diff(`${this._name}.ts`, this._srcChecksum))
            {
                this._recompileNeeded = true;
            }
            else
            {
                this._recompileNeeded = false;
            }

            // Check if we need to relint
            if (this._recompileNeeded)
            {
                // If we need to recompile, linting should happen as part of that.
                this._lintNeeded = false;
            }
            else if (this._module.checksumDB.diff(`${this._name}.ts.lint`, this._srcChecksum))
            {
                this._lintNeeded = true;
            }
            else
            {
                this._lintNeeded = false;
            }
        }

        /**
         * Validates this compilation unit's code.
         */
        public async lintFiles(): Promise<boolean>
        {
            const lintConfigPath = Path.combine(this._rootDir, "tslint.json");
            const defaultConfigPath = Path.combine(__dirname, "linting", strict.value ? "tslint-strict.json" : "tslint.json");
            const useCustom = await FileSystem.fileExists(lintConfigPath);
            let success: boolean;
            if (useCustom)
            {
                const srcOb = await JSON.parseFile(lintConfigPath);
                srcOb.extends = [defaultConfigPath];
                const tmpConfigPath = Path.combine(this._rootDir, "tslint_tmp.json");
                await FileSystem.writeFile(tmpConfigPath, JSON.stringify(srcOb));
                success = await this.lintPath(tmpConfigPath);
                await FileSystem.deletePath(tmpConfigPath);
            }
            else
            {
                success = await this.lintPath(defaultConfigPath);
            }

            if (success)
            {
                // Set lint checksum
                this._module.checksumDB.set(`${this._name}.ts.lint`, this._srcChecksum);
            }

            return success;
        }

        /**
         * Executes this compilation unit.
         */
        @Profile("CompilationUnit.execute")
        public async execute()
        {
            // Log.debug(`Executing compilation unit '${this._name}'`);

            const result: CompilationUnit.CompileResult = { success: false, time: 0, errors: null };
            const timer = new RS.Timer();
            try
            {
                // Compile TS and lint
                if (this._preProcessors.length > 0)
                {
                    const pathParts = Path.split(this._path);
                    const tmpDirPath = Path.combine(...pathParts.slice(0, -1), `.${pathParts[pathParts.length - 1]}-tmp`);
                    await FileSystem.deletePath(tmpDirPath);

                    try
                    {
                        // cache to temp dir
                        await this.copyFolder(this._path, tmpDirPath);
                        // preprocess files
                        const workDone = await this.preProcess(this._path);
                        await TypeScript.compile(this._path, false);

                        if (workDone)
                        {
                            // restore original cached files
                            await this.copyFolder(tmpDirPath, this._path);
                        }
                    }
                    finally
                    {
                        await FileSystem.deletePath(tmpDirPath);
                    }
                }
                else
                {
                    await TypeScript.compile(this._rootDir, false);
                }

                await this.lintFiles();

                // Update file cache
                FileSystem.cache.set(this.outJSFile, { type: FileIO.FileSystemCache.NodeType.File, content: null });
                if (this._emitsSourceMap) { FileSystem.cache.set(this.outMapFile, { type: FileIO.FileSystemCache.NodeType.File, content: null }); }
                if (this._emitsDefinitions) { FileSystem.cache.set(this.outDTSFile, { type: FileIO.FileSystemCache.NodeType.File, content: null }); }
                if (this._emitsDefinitionMap) { FileSystem.cache.set(this.outDTSMapFile, { type: FileIO.FileSystemCache.NodeType.File, content: null }); }

                // Run postprocessors
                let ppChangesMade = false;
                let compileArtifact: CompilationUnit.CompileArtifact =
                {
                    source: await FileSystem.readFile(this.outJSFile),
                    definitions: this._emitsDefinitions ? await FileSystem.readFile(this.outDTSFile) : null,
                    sourceMap: this._emitsSourceMap ? await JSON.parseFile(this.outMapFile) : null,
                    definitionsSourceMap: this._emitsDefinitionMap ? await JSON.parseFile(this.outDTSMapFile) : null
                };
                for (const pp of this._postProcessors.data)
                {
                    const newCompileArtifact = pp.call(this, compileArtifact);
                    if (newCompileArtifact !== compileArtifact)
                    {
                        ppChangesMade = true;
                        compileArtifact = newCompileArtifact;
                    }
                }
                if (ppChangesMade)
                {
                    await FileSystem.writeFile(this.outJSFile, compileArtifact.source);
                    if (compileArtifact.sourceMap != null)
                    {
                        await FileSystem.writeFile(this.outMapFile, JSON.stringify(compileArtifact.sourceMap));
                    }
                    if (compileArtifact.definitions != null)
                    {
                        await FileSystem.writeFile(this.outDTSFile, compileArtifact.definitions);
                    }
                    if (compileArtifact.definitionsSourceMap != null)
                    {
                        await FileSystem.writeFile(this.outDTSMapFile, JSON.stringify(compileArtifact.definitionsSourceMap));
                    }
                }

                // Success
                result.success = true;
                this._module.checksumDB.set(`${this._name}.ts`, this._srcChecksum);
                this._validState = CompilationUnit.ValidState.Compiled;
            }
            catch (err)
            {
                result.errors = [];
                if (Is.string(err))
                {
                    const lines = err.split("\n");
                    const tmp: string[] = [];
                    for (const line of lines)
                    {
                        if (line[0] !== " ")
                        {
                            if (tmp.length > 0)
                            {
                                result.errors.push(tmp.join("\n"));
                                tmp.length = 0;
                            }
                        }
                        tmp.push(line);
                    }
                    if (tmp.length > 0)
                    {
                        result.errors.push(tmp.join("\n"));
                        tmp.length = 0;
                    }
                }
                else
                {
                    result.errors.push(err.stack || err);
                }
                this._validState = CompilationUnit.ValidState.CompileError;
            }
            finally
            {
                result.time = timer.elapsed;
            }
            return result;
        }

        protected async copyFolder(sourcePath: string, destPath: string): Promise<void>
        {
            const srcPaths = await FileSystem.readFiles(sourcePath);
            const promises = srcPaths.map(async (path) =>
            {
                const relPath = Path.relative(sourcePath, path);
                const targetPath = Path.combine(destPath, relPath);
                await FileSystem.deletePath(targetPath);
                await FileSystem.createPath(Path.directoryName(targetPath));
                await FileSystem.copyFile(path, targetPath);
            });
            await Promise.all(promises);
        }

        @Profile("CompilationUnit.execute.preProcess")
        protected async preProcess(outPath: string): Promise<boolean>
        {
            const srcPaths = await FileSystem.readFiles(outPath);
            let workDone = false;
            const promises = srcPaths.map(async (path) =>
            {
                if (Path.extension(path) === ".ts")
                {
                    // read
                    const srcIn = await FileSystem.readFile(path);
                    // run preprocessors
                    const result: CompilationUnit.PreProcessorOut = this._preProcessors.reduce((src, proc) => proc.call(this, src), srcIn);
                    if (result.workDone)
                    {
                        workDone = true;
                    }
                    // write to original location (overwrites original file)
                    await FileSystem.writeFile(path, result.output);
                }
            });

            await Promise.all(promises);
            return workDone;
        }

        protected async lintPath(path: string): Promise<boolean>
        {
            try
            {
                await TSLint.lint(this._rootDir, path, false);
                return true;
            }
            catch (err)
            {
                if (!strict.value && err instanceof TSLint.LintFailure && !err.isError)
                {
                    const lines = `${err}`.split("\n");
                    for (const line of lines)
                    {
                        Log.warn(line, this._name);
                    }
                    return false;
                }
                throw err;
            }
        }

        /** Returns a promise for a TSConfig object. */
        protected resolveTSConfig(): PromiseLike<TSConfig | null>;
        protected async resolveTSConfig(): Promise<TSConfig | null>
        {
            const tsConfigPath = Path.combine(this._path, "tsconfig.json");
            if (!await FileSystem.fileExists(tsConfigPath))
            {
                // File doesn't exist
                if (this._defaultTSConfig)
                {
                    const srcFiles = await FileSystem.readFiles(this._path);
                    if (srcFiles.some((file) => Path.extension(file) === ".ts"))
                    {
                        // Write the default TSConfig and return that
                        const tsConfigRaw = JSON.stringify(this._defaultTSConfig, null, 2);
                        await FileSystem.writeFile(tsConfigPath, tsConfigRaw);
                        return this._defaultTSConfig;
                    }
                }

                return null;
            }
            return await JSON.parseFile(tsConfigPath);
        }

        @Profile("CompilationUnit.resolveDependencies")
        protected async resolveDependencies()
        {
            // Log.debug(`Resolving dependencies`);

            const depMap: Util.Map<boolean> = {};

            this._rawDependencies.clear();
            const depSrcPath = Path.combine(this._rootDir, "dependencies");
            await FileSystem.createPath(depSrcPath);
            const existingDepSrcFiles = (await FileSystem.readFiles(depSrcPath)).map((p) => Path.relative(depSrcPath, p));
            const fileWriteList: Util.Map<string> = {};
            const fileCopyList: Util.Map<string> = {};
            for (const dep of this._dependencies.data)
            {
                if (depMap[dep.name])
                {
                    Log.warn(`Duplicate dependency name '${dep.name}' in compilation unit`, this._name);
                }
                else
                {
                    depMap[dep.name] = true;
                }
                const depChecksumKey = `${this._name}.deps.${dep.name}`;
                const depChecksum = CompilationUnit.Dependency.getChecksum(dep);
                const depDidChange = this._module.checksumDB.diff(depChecksumKey, depChecksum);
                // Log.debug(`${depChecksumKey} (depDidChange=${depDidChange})`);
                let definitionsFile: string | null = null;
                let definitionsMapFile: string | null = null;
                switch (dep.kind)
                {
                    case CompilationUnit.DependencyKind.ThirdParty:
                        {
                            if (dep.definitionsFile != null)
                            {
                                definitionsFile = dep.definitionsFile;
                            }
                            if (dep.definitionsMapFile != null)
                            {
                                definitionsMapFile = dep.definitionsMapFile;
                            }
                            break;
                        }
                    case CompilationUnit.DependencyKind.CompilationUnit:
                        {
                            if (dep.compilationUnit === this)
                            {
                                Log.warn(`Dependency '${dep.name}' is me`, this._name);
                                return false;
                            }

                            if (dep.compilationUnit.validityState === CompilationUnit.ValidState.NoTSConfig)
                            {
                                // Doing nothing is the same as resolving this compilation unit, because there is nothing to resolve.
                                Log.warn(`Dependency '${dep.name}' is missing tsconfig.json`, this._name);
                                break;
                            }

                            if (dep.compilationUnit.validityState === CompilationUnit.ValidState.Initialised)
                            {
                                if (dep.compilationUnit.recompileNeeded)
                                {
                                    Log.warn(`Dependency '${dep.name}' needs to be recompiled before this compilation unit can be initialised`, this._name);
                                    return false;
                                }
                            }
                            else if (dep.compilationUnit.validityState !== CompilationUnit.ValidState.Compiled)
                            {
                                Log.warn(`Dependency '${dep.name}' needs to be compiled with no errors before this compilation unit can be initialised`, this._name);
                                return false;
                            }

                            if (dep.compilationUnit._emitsDefinitions)
                            {
                                definitionsFile = dep.compilationUnit.outDTSFile;
                            }
                            if (dep.compilationUnit._emitsDefinitionMap)
                            {
                                definitionsMapFile = dep.compilationUnit.outDTSMapFile;
                            }
                            break;
                        }
                    case CompilationUnit.DependencyKind.NodeModule:
                        {
                            // Did it change?
                            if (depDidChange)
                            {
                                // Install node module
                                try
                                {
                                    await NPM.installPackage(dep.moduleName, dep.moduleVersion || "latest");
                                }
                                catch (err)
                                {
                                    Log.error(err, this._name);
                                    return false;
                                }
                            }
                            // TODO: Fetch TS definitions from the node module
                            break;
                        }
                    case CompilationUnit.DependencyKind.BowerPackage:
                        {
                            // Did it change?
                            if (depDidChange)
                            {
                                // Install bower package
                                try
                                {
                                    await Bower.installPackage(dep.packageName, dep.packageVersion || "latest");
                                }
                                catch (err)
                                {
                                    Log.error(err, this._name);
                                    return false;
                                }
                            }
                            // TODO: Fetch TS definitions from the bower package
                            break;
                        }
                    case CompilationUnit.DependencyKind.DefinitionsFolder:
                        {
                            // Log.debug(dep.path);
                            const defs = await FileSystem.readFiles(dep.path);
                            for (const def of defs)
                            {
                                const relDef = Path.relative(dep.path, def);
                                const outPath = Path.combine(depSrcPath, relDef);
                                const existingIdx = existingDepSrcFiles.indexOf(relDef);
                                if (existingIdx !== -1) { existingDepSrcFiles.splice(existingIdx, 1); }
                                if (Path.extension(def) === ".map")
                                {
                                    const definitionsMapObj: SourceMap = await JSON.parseFile(def);
                                    const rootPath = Path.relative(Path.directoryName(outPath), Path.directoryName(def));
                                    for (let i = 0, l = definitionsMapObj.sources.length; i < l; ++i)
                                    {
                                        definitionsMapObj.sources[i] = Path.normalise(Path.combine(rootPath, definitionsMapObj.sources[i]));
                                    }
                                    fileWriteList[outPath] = JSON.stringify(definitionsMapObj);
                                }
                                else
                                {
                                    fileCopyList[outPath] = def;
                                }
                            }
                            break;
                        }
                }
                if (definitionsFile != null)
                {
                    const depName = `${dep.name}.d.ts`;
                    const outputPath = Path.combine(depSrcPath, depName);
                    // Log.debug(`${definitionsFile} -> ${outputPath}`);
                    const existingIdx = existingDepSrcFiles.indexOf(depName);
                    if (existingIdx !== -1) { existingDepSrcFiles.splice(existingIdx, 1); }

                    // If it's a URL, only acquire it if depDidChange, or the output file is missing
                    if (depDidChange || !HTTP.isURL(definitionsFile) || !await FileSystem.fileExists(outputPath))
                    {
                        if (!await this.acquireDefinition(definitionsFile, outputPath)) { return false; }
                    }

                    // Check for definitions map file
                    if (definitionsMapFile != null)
                    {
                        const depMapName = `${dep.name}.d.ts.map`;
                        const mapOutputPath = Path.combine(depSrcPath, depMapName);
                        const mapExistingIdx = existingDepSrcFiles.indexOf(depMapName);
                        if (mapExistingIdx !== -1) { existingDepSrcFiles.splice(mapExistingIdx, 1); }

                        // If it's a URL, only acquire it if depDidChange, or the output file is missing
                        if (depDidChange || !HTTP.isURL(definitionsMapFile) || !await FileSystem.fileExists(mapOutputPath))
                        {
                            if (!await this.acquireDefinition(definitionsMapFile, mapOutputPath)) { return false; }

                            const definitionsMapObj: SourceMap = await JSON.parseFile(mapOutputPath);

                            // Rewire map to have correct paths, if it came from somewhere local
                            if (!HTTP.isURL(definitionsMapFile))
                            {
                                const localPath = Path.combine(this._module.path, definitionsMapFile);
                                let rootPath: string;
                                if (await FileSystem.fileExists(localPath))
                                {
                                    rootPath = Path.relative(depSrcPath, Path.directoryName(localPath));
                                }
                                else
                                {
                                    rootPath = Path.relative(depSrcPath, Path.directoryName(definitionsMapFile));
                                }
                                for (let i = 0, l = definitionsMapObj.sources.length; i < l; ++i)
                                {
                                    definitionsMapObj.sources[i] = Path.normalise(Path.combine(rootPath, definitionsMapObj.sources[i]));
                                }
                            }

                            // Change the file
                            definitionsMapObj.file = depName;

                            fileWriteList[mapOutputPath] = JSON.stringify(definitionsMapObj);
                        }
                    }
                }
                this._module.checksumDB.set(depChecksumKey, depChecksum);
            }
            const copyKeys = Object.keys(fileCopyList);
            const writeKeys = Object.keys(fileWriteList);
            const copySrcs: string[] = [];
            const copyDests: string[] = [];
            const paths: string[] = [];
            const files: string[] = [];
            for (let i = 0; i < copyKeys.length; i++)
            {
                copySrcs.push(fileCopyList[copyKeys[i]]);
                copyDests.push(copyKeys[i]);
            }
            for (let i = 0; i < writeKeys.length; i++)
            {
                paths.push(writeKeys[i]);
                files.push(fileWriteList[writeKeys[i]]);
            }
            await FileSystem.writeFiles(paths, files);
            await FileSystem.copyFiles(copySrcs, copyDests);
            for (const existing of existingDepSrcFiles)
            {
                const path = Path.combine(depSrcPath, existing);
                Log.debug(`Cleaning up old file '${path}'`);
                await FileSystem.deletePath(path);
            }
            return true;
        }

        /**
         * Acquires the specified definition.
         * @param name
         * @param path
         */
        protected async acquireDefinition(path: string, output: string)
        {
            if (HTTP.isURL(path))
            {
                const result = await HTTP.fetch(path);
                if (result.responseCode === 200)
                {
                    await FileSystem.writeFile(output, result.data);
                    Log.info(`Downloaded definition from '${path}'`, this._name);
                    return true;
                }
                else
                {
                    Log.error(`Failed to retrieve definition from '${path}' (got HTTP code ${result.responseCode})`, this._name);
                    return false;
                }
            }

            const localPath = Path.combine(this._module.path, path);
            if (await FileSystem.fileExists(localPath))
            {
                await FileSystem.copyFile(localPath, output);
                return true;
            }
            if (await FileSystem.fileExists(path))
            {
                await FileSystem.copyFile(path, output);
                return true;
            }

            Log.error(`Failed to locate definition (unknown path '${path}')`, this._name);
            return false;
        }
    }

    export namespace CompilationUnit
    {
        export async function executeUnit(unit: CompilationUnit)
        {
            let compiled = false;
            if (unit.validityState === ValidState.Uninitialised) { await unit.init(); }
            if (unit.validityState === ValidState.Initialised)
            {
                if (unit.recompileNeeded)
                {
                    const result = await unit.execute();
                    if (!result.success)
                    {
                        for (const err of result.errors)
                        {
                            Log.error(err, unit.name);
                        }
                        throw new Error(`${unit.name} failed to build.`);
                    }
                    Log.info(`Compiled in ${(result.time / 1000).toFixed(2)} s`, `${unit.name}`);
                    compiled = true;
                }
            }
            return compiled;
        }

        export enum DependencyKind { ThirdParty, CompilationUnit, NodeModule, BowerPackage, DefinitionsFolder }
        export interface BaseDependency<TKind extends DependencyKind>
        {
            kind: TKind;
            name: string;
        }
        export interface ThirdPartyDependency extends BaseDependency<DependencyKind.ThirdParty>
        {
            /** Path of the js file, relative to the module. */
            sourceFile?: string;

            /** Path of the js sourcemap file, relative to the module. */
            sourceMap?: string;

            /** Path of the ts definitions file, relative to the module. */
            definitionsFile?: string;

            /** Path of the ts definitions map file, relative to the module. */
            definitionsMapFile?: string;
        }
        export interface CompilationUnitDependency extends BaseDependency<DependencyKind.CompilationUnit>
        {
            /** Compilation unit to depend on. */
            compilationUnit: CompilationUnit;
        }
        export interface NodeModuleDependency extends BaseDependency<DependencyKind.NodeModule>
        {
            /** Name of the node module. */
            moduleName: string;

            /** Version of the node module. */
            moduleVersion?: string;
        }
        export interface BowerPackageDependency extends BaseDependency<DependencyKind.BowerPackage>
        {
            /** Name of the bower package. */
            packageName: string;

            /** Version of the bower package. */
            packageVersion?: string;
        }
        export interface DefinitionsFolderDependency extends BaseDependency<DependencyKind.DefinitionsFolder>
        {
            /** Path of the folder to read, relative to the cwd. */
            path: string;
        }

        export type Dependency = ThirdPartyDependency | CompilationUnitDependency | NodeModuleDependency | BowerPackageDependency | DefinitionsFolderDependency;

        export namespace Dependency
        {
            export function getChecksum(dep: Dependency)
            {
                switch (dep.kind)
                {
                    case DependencyKind.CompilationUnit:
                        return Checksum.getSimple(`{"kind":${dep.kind},"compilationUnit":"${dep.compilationUnit.name}"}`);
                    default:
                        return Checksum.getSimple(JSON.stringify(dep));
                }
            }
        }

        export interface RawDependency
        {
            sourceFile: string;
            sourceMap?: string;
        }

        export interface CompileArtifact
        {
            source: string;
            sourceMap: SourceMap | null;
            definitions: string | null;
            definitionsSourceMap: SourceMap | null;
        }
        export type PostProcessor = (this: CompilationUnit, artifact: CompileArtifact) => CompileArtifact;

        export interface PreProcessorOut
        {
            output: string;
            workDone: boolean;
        }
        export type PreProcessor = (this: CompilationUnit, source: string) => PreProcessorOut;

        export enum ValidState
        {
            Uninitialised,
            NoTSConfig,
            MissingDependency,
            Initialised,
            Compiled,
            CompileError
        }

        export interface CompileResult
        {
            success: boolean;
            time: number;
            errors: string[] | null;
        }
    }
}
