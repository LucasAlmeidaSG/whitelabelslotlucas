namespace RS.Build
{
    // Ported from: https://github.com/io-monad/line-column

    export interface LineColumnFinderOptions
    {
        origin?: number;
    }

    export interface LineColumn
    {
        line: number;
        col: number;
    }

    /**
     * Finder for index and line-column from given string.
     */
    export class LineColumnFinder
    {
        private str: string;
        private lineToIndex: number[];
        private origin: number;

        public constructor(str: string, options: LineColumnFinderOptions = {})
        {
            this.str = str || "";
            this.lineToIndex = this.buildLineToIndex(this.str);
            this.origin = options.origin != null ? options.origin : 1;
        }

        /**
         * Find line and column from index in the string.
         *
         * @param  {number} index - Index in the string. (0-origin)
         * @return {Object|null}
         *     Found line number and column number in object `{ line: X, col: Y }`.
         *     If the given index is out of range, it returns `null`.
         */
        public fromIndex(index: number): LineColumn
        {
            if (index < 0 || index >= this.str.length || isNaN(index)) { return null; }
            const line = this.findLowerIndexInRangeArray(index, this.lineToIndex);
            return {
                line: line + this.origin,
                col: index - this.lineToIndex[line] + this.origin
            };
        }

        /**
         * Find index from line and column in the string.
         */
        public toIndex(line: number, column: number): number;

        /**
         * Find index from line and column in the string.
         */
        public toIndex(lineColumn: number[]): number;

        /**
         * Find index from line and column in the string.
         */
        public toIndex(lineColumn: LineColumn): number;

        public toIndex(p1: number | LineColumn | number[], p2?: number)
        {
            if (p2 == null)
            {
                if (Is.array(p1))
                {
                    if (p1.length >= 2)
                    {
                        return this.toIndex(p1[0], p1[1]);
                    }
                    else
                    {
                        return -1;
                    }
                }
                else if (Is.number(p1))
                {
                    return -1;
                }
                else
                {
                    return this.toIndex(p1.line, p1.col);
                }
            }
            let line = p1;
            let column = p2;
            if (!Is.number(line)) { return -1; }

            line -= this.origin;
            column -= this.origin;

            if (line >= 0 && column >= 0 && line < this.lineToIndex.length)
            {
                var lineIndex = this.lineToIndex[line];
                var nextIndex = (
                    line === this.lineToIndex.length - 1
                        ? this.str.length
                        : this.lineToIndex[line + 1]
                );

                if (column < nextIndex - lineIndex)
                {
                    return lineIndex + column;
                }
            }
            return -1;
        }

        /**
         * Build an array of indexes of each line from a string.
         */
        private buildLineToIndex(str): number[]
        {
            const lines = Util.splitLines(str);
            const lineToIndex = new Array<number>(lines.length);
            let index = 0;
            for (let i = 0, l = lines.length; i < l; ++i)
            {
                lineToIndex[i] = index;
                index += lines[i].length + /* "\n".length */ 1;
            }
            return lineToIndex;
        }

        /**
         * Find a lower-bound index of a value in a sorted array of ranges.
         *
         * Assume `arr = [0, 5, 10, 15, 20]` and
         * this returns `1` for `value = 7` (5 <= value < 10),
         * and returns `3` for `value = 18` (15 <= value < 20).
         */
        private findLowerIndexInRangeArray(value: number, arr: number[]): number
        {
            if (value >= arr[arr.length - 1])
            {
                return arr.length - 1;
            }

            var min = 0, max = arr.length - 2, mid;
            while (min < max)
            {
                mid = min + ((max - min) >> 1);

                if (value < arr[mid])
                {
                    max = mid - 1;
                } else if (value >= arr[mid + 1])
                {
                    min = mid + 1;
                } else
                { // value >= arr[mid] && value < arr[mid + 1]
                    min = mid;
                    break;
                }
            }
            return min;
        }
    }
}