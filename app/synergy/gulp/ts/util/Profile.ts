/// <reference path="Timer.ts" />
/// <reference path="EnvArg.ts" />

namespace RS
{
    export function Profile(name: string): MethodDecorator
    {
        return function(target: Object, propertyKey: string, descriptor: PropertyDescriptor)
        {
            const oldFunc: Function = descriptor.value;
            function newFunc()
            {
                Profile.enter(name);
                const ret = oldFunc.apply(this, arguments);
                if (ret instanceof Promise)
                {
                    ret.then(() => Profile.exit(name), () => Profile.exit(name));
                }
                else
                {
                    Profile.exit(name);
                }
                return ret;
            }
            descriptor.value = newFunc;
            Object.defineProperty(target, propertyKey, descriptor);
            target[propertyKey] = newFunc;
        };
    }

    export namespace Profile
    {
        interface ProfileDetails
        {
            refCount: number;
            timer: Timer;
        }

        let profileMap: Util.Map<ProfileDetails> = {};

        export function enter(name: string): void
        {
            const p = profileMap[name] || (profileMap[name] = { refCount: 0, timer: new Timer() });
            p.refCount++;
            if (p.refCount === 1)
            {
                p.timer.start();
            }
        }

        export function exit(name: string): void
        {
            const p = profileMap[name] || (profileMap[name] = { refCount: 0, timer: new Timer() });
            p.refCount--;
            if (p.refCount === 0)
            {
                p.timer.pause();
            }
        }

        export function dump(): void
        {
            const keys = Object.keys(profileMap).sort();
            for (const key of keys)
            {
                const p = profileMap[key];
                const elapsed = p.timer.elapsed / 1000;
                Log.debug(`Total time: ${elapsed.toFixed(2)}s`, key);
            }
        }

        export function reset(): void
        {
            profileMap = {};
        }
    }

    export namespace EnvArgs
    {
        /** Whether or not performance profiling information should be dumped to the console. */
        export const profile = new BooleanEnvArg("PROFILE", false);
    }
}