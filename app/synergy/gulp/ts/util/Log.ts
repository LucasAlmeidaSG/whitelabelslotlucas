/// <reference path="EnvArg.ts"/>
namespace RS.EnvArgs
{
    export const debugMode = new BooleanEnvArg("DEBUG", false);
}
namespace RS.Log
{
    const gutil = require("gulp-util");

    const logContextStack: string[] = [];
    let currentContext = "Build";

    function log(contextColor: string, messageColor: string, message: string, contextOverride?: string): void
    {
        gutil.log(`[${gutil.colors[contextColor](contextOverride || currentContext)}] ${gutil.colors[messageColor](message)}`);
    }

    /**
     * Pushes the current context to the stack and sets the specified context as active.
     * @param context
     */
    export function pushContext(context: string)
    {
        logContextStack.push(currentContext);
        currentContext = context;
    }

    /**
     * Sets the specified context as inactive and restores the previous context from the stack.
     * @param context
    **/
    export function popContext(context: string)
    {
        if (context !== currentContext)
        {
            throw new Error(`Tried to pop context '${context}' when current context is '${currentContext}'`);
        }
        currentContext = logContextStack.pop();
    }

    /**
     * Logs an informative message to the console.
     * @param message
     */
    export function info(message: string, contextOverride?: string)
    {
        log("cyan", "white", message, contextOverride);
    }

    /**
     * Logs an debug message to the console.
     * @param message
     */
    export function debug(message: string, contextOverride?: string)
    {
        log("cyan", "magenta", message, contextOverride);
    }

    /**
     * Logs a warning message to the console.
     * @param message
     */
    export function warn(message: string, contextOverride?: string)
    {
        log("cyan", "yellow", message, contextOverride);
    }

    /**
     * Logs an error message to the console.
     * @param message
     */
    export function error(message: string, contextOverride?: string)
    {
        log("red", "yellow", message, contextOverride);
    }
}