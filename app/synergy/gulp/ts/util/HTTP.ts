/**
 * Contains HTTP utilities.
 */
namespace RS.HTTP
{
    const debugMode = false;
    const http = require("http");
    const https = require("https");

    export interface FetchResult
    {
        data?: string;
        responseCode: number;
    }

    /**
     * Fetches the content at the specified URL.
     * @param url 
     */
    export function fetch(url: string): PromiseLike<FetchResult>
    {
        const protocol = url.match(/[a-z]+(?=:\/\/)/g)[0];
        const lib = protocol === "https" ? https : http;
        return new Promise<FetchResult>((resolve, reject) =>
        {
            const req = lib.request(url, result =>
            {
                let str = "";
                result.setEncoding("utf8");
                result.on("data", buffer =>
                {
                    if (debugMode) { Log.debug(`HTTP request got some data`); }
                    str += buffer.toString();
                });
                result.on("end", () =>
                {
                    if (debugMode) { Log.debug(`HTTP request ended`); }
                    resolve({ data: str, responseCode: result.statusCode });
                });
            });
            req.on("error", err =>
            {
                reject(err);
            });
            req.end();
            if (debugMode) { Log.debug(`Sending HTTP request to '${url}'`); }
        });
    }

    /**
     * https://gist.github.com/dperini/729294
     */
    const re_weburl = new RegExp(
        "^" +
          // protocol identifier
          "(?:(?:https?|ftp)://)" +
          // user:pass authentication
          "(?:\\S+(?::\\S*)?@)?" +
          "(?:" +
            // IP address exclusion
            // private & local networks
            "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
            "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
            "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
            // IP address dotted notation octets
            // excludes loopback network 0.0.0.0
            // excludes reserved space >= 224.0.0.0
            // excludes network & broacast addresses
            // (first & last IP address of each class)
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
          "|" +
            // host name
            "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            // domain name
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
            // TLD identifier
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            // TLD may end with dot
            "\\.?" +
          ")" +
          // port number
          "(?::\\d{2,5})?" +
          // resource path
          "(?:[/?#]\\S*)?" +
        "$", "i"
      );

    /**
     * Gets if the specified string is a valid URL.
     * @param val 
     */
    export function isURL(val: string): boolean
    {
        return re_weburl.test(val);
    }
}