namespace RS.Directives
{
    export type Defines = { [name: string]: boolean | string };

    export function parse(str: string, defines: Defines): ParseResult
    {
        /*
         * Match /* with an optional // prefix
         * Followed by any amount of whitespace
         * Followed by 1 or more letters
         * Followed optionally by : and
         * one or more non-* characters
         * Followed by any amount of whitespace
         * Followed by *​/
         */
        // const parser = new Parser(/\/\*\s*([a-z]+)(:[^\*]+)?\s*\*\//gi, str);
        const parser = new Parser(/(?:\/\/\s*)?\/\*\s*([a-z]+)(:[^\*]+)?\s*\*\//gi, str);
        const usesDefines: string[] = [];

        let hasCondition: boolean, index: number;
        do
        {
            index = parser.lastIndex;
            hasCondition = readCondition(parser, defines, usesDefines);
        }
        while (hasCondition);

        parser.parsedString += str.slice(index);
        return { content: parser.parsedString, usesDefines };
    }

    export interface ParseResult
    {
        usesDefines: string[];
        content: string;
    }

    enum Keywords
    {
        IfDefined = "ifdef",
        IfNotDefined = "ifndef",
        Else = "else",
        EndIf = "endif"
    }

    class Parser
    {
        public parsedString: string = "";

        private _lastMatch: [string, Keywords, string] | null = null;
        public get lastMatch(): Readonly<[string, Keywords, string]> | null { return this._lastMatch; }

        public get lastIndex() { return this.regExp.lastIndex; }
        public set lastIndex(index) { this.regExp.lastIndex = index; }

        constructor(private readonly regExp: RegExp, public readonly string: string) {}

        public next(): [string, Keywords, string] | null
        {
            let match: RegExpMatchArray;
            do
            {
                match = this.regExp.exec(this.string);
            }
            while (match != null && !this.isKeyword(match[1]));

            this._lastMatch = match as [string, Keywords, string] | null;
            return this._lastMatch;
        }

        private isKeyword(str: string): str is Keywords
        {
            for (const key in Keywords)
            {
                if (Keywords[key] === str) { return true; }
            }
            return false;
        }
    }

    function pushUnique<T>(arr: T[], item: T): void
    {
        if (arr.indexOf(item) === -1)
        {
            arr.push(item);
        }
    }

    /** Reads a condition e.g. ifdef. */
    function readCondition(parser: Parser, defines: Defines, usesDefines: string[], nested = false): boolean
    {
        const startIndex = parser.lastIndex;
        for (let match = parser.next(); match != null; match = parser.next())
        {
            const [matchStr, keyword, args] = match;

            //commented out, pretend it doesn't exist
            if (matchStr.slice(0, 2) === "//")
            {
                continue;
            }

            let invert: boolean;
            switch (keyword)
            {
                case Keywords.IfDefined:
                    invert = false;
                    break;

                case Keywords.IfNotDefined:
                    invert = true;
                    break;

                default:
                    if (nested) { return false; }
                    throw new Error(`Unexpected ${keyword}`);
            }

            // Split to current index
            parser.parsedString += parser.string.slice(startIndex, parser.lastIndex - matchStr.length);

            if (!args) { throw new Error(`Argument string missing`); }
            const argv = args.split(":");

            const define = argv[1].trim();
            pushUnique(usesDefines, define);

            const hasDefine = !!defines[define];
            const truthy = invert ? !hasDefine : hasDefine;

            if (truthy)
            {
                // Parse inner conditions recursively
                readConditionBlock(parser, defines, usesDefines);
                const [, nextKeyword] = parser.lastMatch;
                switch (nextKeyword)
                {
                    case Keywords.Else:
                    {
                        // Skip to EndIf and return
                        skipConditionBlock(parser);
                        if (parser.lastMatch == null) { throw new Error(`Missing ${Keywords.EndIf}`); }
                        const [, lastKeyword] = parser.lastMatch;
                        if (lastKeyword !== Keywords.EndIf) { throw new Error(`Unexpected ${lastKeyword}`); }
                        return true;
                    }

                    case Keywords.EndIf: return true;
                    default: throw new Error(`Unexpected ${nextKeyword}`);
                }
            }
            else
            {
                // Skip this block
                skipConditionBlock(parser);

                // Expect EndIf or else
                if (parser.lastMatch == null) { throw new Error(`Missing ${Keywords.EndIf}`); }
                const [, nextKeyword] = parser.lastMatch;

                switch (nextKeyword)
                {
                    case Keywords.Else:
                    {
                        readConditionBlock(parser, defines, usesDefines);
                        const [, lastKeyword] = parser.lastMatch;
                        if (lastKeyword !== Keywords.EndIf) { throw new Error(`Unexpected ${lastKeyword}`); }
                        return true;
                    }

                    case Keywords.EndIf: return true;
                    default: throw new Error(`Unexpected ${nextKeyword}`);
                }
            }
        }

        return false;
    }

    /** Parses the inside of a conditional block. */
    function readConditionBlock(parser: Parser, defines: Defines, usesDefines: string[]): void
    {
        // Parse inner conditions recursively
        let hasCondition: boolean, endIndex: number;
        do
        {
            endIndex = parser.lastIndex;
            hasCondition = readCondition(parser, defines, usesDefines, true);
        }
        while (hasCondition);

        // Expect EndIf
        if (parser.lastMatch == null) { throw new Error(`Missing ${Keywords.EndIf}`); }
        const [lastFull] = parser.lastMatch;
        parser.parsedString += parser.string.slice(endIndex, parser.lastIndex - lastFull.length);
    }

    /** Moves parser to the next directive at the current depth, ignoring any nested directives. */
    function skipConditionBlock(parser: Parser): void
    {
        let depth = 0;
        for (let match = parser.next(); match != null; match = parser.next())
        {
            const [, keyword] = match;
            if (depth === 0)
            {
                if (keyword !== Keywords.IfNotDefined && keyword !== Keywords.IfDefined) { return; }
                ++depth;
            }
            else
            {
                if (keyword === Keywords.EndIf) { --depth; }
            }
        }
    }
}