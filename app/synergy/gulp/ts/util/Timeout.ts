namespace RS
{
    /** Returns a promise that resolves with the given promise and rejects if a set amount of time passes first. */
    export function withTimeout<T>(promise: PromiseLike<T>, timeoutMillis: number): PromiseLike<T>
    {
        return new Promise<T>((resolve, reject) =>
        {
            let timeoutHandle: number | NodeJS.Timer;

            promise.then(function (result)
            {
                // Clear timeout on timely resolution
                clearTimeout(timeoutHandle as any);
                resolve(result);
            });

            // Schedule rejection on timeout
            setTimeout(function () { reject(new TimeoutError(timeoutMillis)); }, timeoutMillis);
        });
    }

    export class TimeoutError extends BaseError
    {
        constructor(millis: number)
        {
            super(`Timed out after ${millis} ms`);
        }
    }
}