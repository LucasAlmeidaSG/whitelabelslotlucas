namespace RS
{
    export type Func<TIn, TOut = TIn> = (obj: TIn) => TOut;
    export type Action<T = void> = T extends void ? () => void : (obj: T) => void;
}