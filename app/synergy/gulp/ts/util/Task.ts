/// <reference path="Log.ts" />
/// <reference path="Is.ts" />
/// <reference path="Path.ts" />

namespace RS.Build
{
    const gulpPath = Path.combine(process.cwd(), "node_modules", "gulp");
    const debugMode = false;
    const gulp = require(gulpPath);

    const baseGulpStart: Function = gulp.start;
    
    /** Executor function for a task. */
    export type TaskCallback = () => PromiseLike<void>;

    /** Settings for a task. */
    export interface TaskSettings
    {
        /** Gulp name of the task. */
        name: string;

        /** Any tasks that MUST be executed before this one. */
        requires?: Task[];

        /** Any tasks that SHOULD be executed before this one but ONLY if required by another. */
        after?: Task[];

        /** Description of task functionality and usage */
        description?: string;
    }

    /** Represents a task. */
    export interface Task
    {
        settings: TaskSettings;
        callback?: TaskCallback;
        required: boolean;
        dependencies: Task[];
    }

    const defaultTaskSettings: TaskSettings =
    {
        name: "default"
    };

    const taskList: Task[] = [];

    /** Declares a task with the specified settings and callback. */
    export function task(settings: TaskSettings, callback: TaskCallback): Task;

    /** Declares a task with default settings and specified callback. */
    export function task(callback: TaskCallback): Task;

    /** Declares a task with the specified settings and no callback. */
    export function task(settings: TaskSettings): Task;

    export function task(p1: TaskSettings|TaskCallback, p2?: TaskCallback): Task
    {
        const settings = Is.func(p1) ? defaultTaskSettings : p1;
        const callback = Is.func(p1) ? p1 : p2;

        if (getTask(settings.name))
        {
            Log.warn(`Duplicate task registered with the name '${settings.name}'`);
            return;
        }

        if (debugMode)
        {
            Log.debug(`Registered task '${settings.name}'`);
        }

        const task = { settings, callback, required: false, dependencies: [...(settings.requires||[])] };
        taskList.push(task);
        return task;
    }

    /** Declares a task to be run before any others. Can only be declared once. */
    export function initTask(callback: TaskCallback): Task
    {
        return task({ name: "init" }, callback);
    }

    /** Identify requested gulp tasks. */
    export function identifyTasks(): string[]
    {
        return process.argv
            .slice(2)
            .filter(str => /[a-zA-Z].*/g.test(str));
    }

    /** Exports the specified task to gulp. */
    export function exportTask(task: Task): void
    {
        if (!task.settings.name) { return; }
        const depends = [...(task.settings.requires||[]), ...(task.settings.after||[])].filter(t => t.required).map(t => t.settings.name);
        gulp.task(task.settings.name, depends, async () =>
        {
            Log.pushContext(task.settings.name);
            try
            {
                if (task.callback) { await task.callback(); }
            }
            catch (err)
            {
                throw err;
            }
            finally
            {
                Log.popContext(task.settings.name);
            }
        });
        if (debugMode)
        {
            if (depends.length > 0)
            {
                Log.debug(`Exported task '${task.settings.name}' (${depends.join(", ")})`);
            }
            else
            {
                Log.debug(`Exported task '${task.settings.name}'`);
            }
        }
    }

    export function getTask(name: string): Task
    {
        return taskList.filter(t => t.settings.name == name)[0];
    }
    
    export function getTasks(): ReadonlyArray<Task>
    {
        return taskList;
    }

    /** Specifies that the task is required and must run. */
    export function requireTask(task: Task): void
    {
        if (task.required) { return; }
        task.required = true;
        if (task.settings.requires)
        {
            for (let i = 0; i < task.settings.requires.length; i++)
            {
                requireTask(task.settings.requires[i]);
            }
        }
    }

    /** Exports all required tasks. */
    export function exportAllTasks()
    {
        for (let i = 0; i < taskList.length; i++)
        {
            if (taskList[i].required)
            {
                exportTask(taskList[i]);
            }
        }
    }

    // Hijack gulp start method, use it to run our own initialise logic before moving on to task logic
    gulp.start = (...args: string[]) =>
    {
        const initTask = getTask("init");
        if (initTask != null)
        {
            Log.pushContext("init");
            initTask.callback()
                .then(() =>
                {
                    Log.popContext("init");
                    baseGulpStart.apply(gulp, args);
                }, (reason) =>
                {
                    Log.error(reason.stack);
                });
        }
        else
        {
            baseGulpStart.apply(gulp, args);
        }
    };
}