namespace RS.FFMPEG
{
    export interface FFProbe
    {
        programs: FFProbe.Program[];
        streams: FFProbe.Stream[];
        format: FFProbe.Format;
    }

    export namespace FFProbe
    {
        export interface Program
        {

        }

        export interface BaseStream<TCodecType extends "video"|"audio">
        {
            index: number;
            codec_name: string;
            codec_long_name: string;
            codec_type: TCodecType;
            codec_time_base: string;
            codec_tag_string: string;
            codec_tag: string;

            r_frame_rate: string;
            avg_frame_rate: string;
            time_base: string;
            start_pts: number;
            start_time: string;
            duration_ts: number;
            duration: string;
            bit_rate: string;

            disposition: Util.Map<number>;
            tags: Util.Map<string>;
        }

        export interface VideoStream extends BaseStream<"video">
        {
            profile: string;
            width: number;
            height: number;
            coded_width: number;
            coded_height: number;
            has_b_frames: number;
            sample_aspect_ratio: string;
            display_aspect_ratio: string;
            pix_fmt: string;
            level: number;
            chrome_location: string;
            refs: number;
            is_avc: string;
            nal_length_size: string;
            bits_per_raw_sample: string;
            nb_frames: string;
        }

        export interface AudioStream extends BaseStream<"audio">
        {
            sample_fmt: string;
            sample_rate: string;
            channels: number;
            channel_layout: string;
            bits_per_sample: number;
        }

        export type Stream = VideoStream | AudioStream;

        export interface Format
        {
            filename: string;
            nb_streams: number;
            nb_programs: number;
            format_name: string;
            format_long_name: string;
            start_time: string;
            duration: string;
            size: string;
            bit_rate: string;
            probe_score: number;
            tags: Util.Map<string>;
        }

        /**
         * Runs FFProbe on a file and returns the result.
         * @param fileName 
         */
        export async function run(fileName: string): Promise<FFProbe>
        {
            try
            {
                return JSON.parse(await Command.run("ffprobe", [ "-v", "error", "-show_entries", "format:stream", "-of", "json", fileName ], null, false));
            }
            catch (err)
            {
                throw new Error(`Error whilst running FFProbe on ${fileName}: ${err}`);
            }
        }

        /**
         * Equivalent to streams.filter(s => s.codec_type === "audio").
         * @param streams 
         * @param codecType 
         */
        export function filterStreams(streams: Stream[], codecType: "audio"): AudioStream[];

        /**
         * Equivalent to streams.filter(s => s.codec_type === "video").
         * @param streams 
         * @param codecType 
         */
        export function filterStreams(streams: Stream[], codecType: "video"): VideoStream[];

        export function filterStreams(streams: Stream[], codecType: string): Stream[]
        {
            return streams.filter(s => s.codec_type === codecType);
        }
    }
}