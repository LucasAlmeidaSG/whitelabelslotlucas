/// <reference path="Read.ts" />
namespace RS
{
    /**
     * Represents an argument passed via an environment variable, e.g. "VAR=1 gulp build".
     */
    export class EnvArg
    {
        /**
         * @param trim Whether or not to trim the value string.
         */
        public constructor(public readonly varName: string, public readonly defaultValue: string, public readonly trim: boolean = true) { }

        public set value(value) { process.env[this.varName] = value; }
        public get value(): string
        {
            const raw = process.env[this.varName] || this.defaultValue;
            return this.trim ? raw.trim() : raw;
        }

        public get isSet(): boolean { return this.varName in process.env; }
    }

    /**
     * Represents a typed argument passed via an environment variable.
     */
    export abstract class TypedEnvArg<T>
    {
        protected abstract readonly typeName: string;
        private readonly _envArg: EnvArg;
        public constructor(varName: string, defaultValue: T) { this._envArg = new EnvArg(varName, this.typeToString(defaultValue)); }

        public get varName(): string { return this._envArg.varName; }
        public get defaultValue(): T { return this.stringToType(this._envArg.defaultValue); }
        public get isSet() { return this._envArg.isSet; }

        public get value(): T { return this.parseString(this._envArg.value); }
        public set value(value: T) { this._envArg.value = this.typeToString(value); }

        protected abstract stringToType(value: string): T | null;
        protected abstract typeToString(value: T): string;

        private parseString(value: string): T
        {
            const parsed = this.stringToType(value);
            if (parsed == null) { throw new TypeError(`Could not read ${value} as ${this.typeName}.`); }
            return parsed;
        }
    }

    /**
     * Represents a boolean argument passed via an environment variable, e.g. "BOOL=1 gulp build".
     * Truthy values are: "true", "1", "y", "yes"
     * Falsy values are: "false", "0", "n", "no"
     * (Case insensitive.)
     */
    export class BooleanEnvArg extends TypedEnvArg<boolean>
    {
        protected get typeName() { return "boolean"; }
        protected typeToString(value: boolean) { return value.toString(); }
        protected stringToType(value: string) { return Read.boolean(value); }
    }

    /**
     * Represents an integer argument passed via an environment variable, e.g. "INT=12 gulp build".
     */
    export class IntegerEnvArg extends TypedEnvArg<number>
    {
        protected get typeName() { return "number"; }
        protected typeToString(value: number) { return value.toFixed(0); }
        protected stringToType(value: string) { return Read.integer(value); }
    }
}