namespace RS.JSON
{
    const stripjsoncomments: ((data: string) => string) =  require("strip-json-comments");

    /** Tries to parse a file as JSON. */
    export async function parseFile(path: string): Promise<any>
    {
        try
        {
            const data = await RS.FileSystem.readFile(path);
            return parse(data);
        }
        catch (err)
        {
            throw new Error(`Error whilst reading ${path}: ${err}`);
        }
    }

    /**
     * Converts a JavaScript Object Notation (JSON) string into an object.
     * @param json
     */
    export function parse(json: string): any
    {
        const commentless = stripjsoncomments(json);
        const trimmed = commentless.trim();
        return global.JSON.parse(trimmed);
    }

    /**
     * Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
     */
    export const stringify = global.JSON.stringify;
}