namespace RS
{
    export class BaseError implements Error
    {
        public get name() { return this.inner.name; }
        public get message() { return this.inner.message; }
        public get stack() { return this.inner.stack; }

        protected readonly inner: Error;

        constructor(message?: string)
        {
            this.inner = new Error(message);
        }

        public toString(): string
        {
            return this.inner.toString();
        }
    }
}