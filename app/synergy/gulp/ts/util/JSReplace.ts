namespace RS.JSReplace
{
    export function replace(sourcePath: string, mapPath: string, replacements: { [name: string]: string }): PromiseLike<void>;
    export async function replace(sourcePath: string, mapPath: string, replacements: { [name: string]: string }): Promise<void>
    {
        Profile.enter("jsreplace");

        try
        {
            await Command.run("node",
            [
                "--max-old-space-size=4096",
                Path.combine(__dirname, "jsreplace.js"),
                sourcePath,
                mapPath,
                ...Object.keys(replacements).map((key) => `${key}=${replacements[key]}`)
            ], undefined, false);
        }
        finally
        {
            Profile.exit("jsreplace");
        }

        FileSystem.cache.set(sourcePath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
        if (mapPath != null) { FileSystem.cache.set(mapPath, { type: FileIO.FileSystemCache.NodeType.File, content: null }); }

    }
}