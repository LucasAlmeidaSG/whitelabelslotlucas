namespace RS
{
    /** A list of items backed by a standard array. */
    export class List<T>
    {
        protected _data: T[];

        /** Gets the backing array for this list. */
        public get data(): ReadonlyArray<T> { return this._data; }

        /** Gets a copy of the backing array for this list. */
        public get dataCopy(): T[] { return this._data.slice(); }

        /** Gets the number of items in this list. */
        public get length() { return this._data.length; }

        public constructor(existing?: ArrayLike<T>)
        {
            if (existing != null)
            {
                this._data = new Array<T>(existing.length);
                for (let i = 0, l = existing.length; i < l; ++i)
                {
                    this._data[i] = existing[i];
                }
            }
            else
            {
                this._data = [];
            }
        }

        /** Returns a new list containing unique items from both lists. */
        public static union<T>(a: List<T>, b: List<T>): List<T>
        {
            const newList = new List<T>();
            newList.addRange(a, true);
            newList.addRange(b, true);
            return newList;
        }

        /** Returns a new list containing only items found in both lists. */
        public static intersect<T>(a: List<T>, b: List<T>): List<T>
        {
            const newList = new List<T>();
            newList.addRange(a.filter((item) => b.contains(item)), true);
            newList.addRange(b.filter((item) => a.contains(item)), true);
            return newList;
        }

        /** Returns a new list containing only items unique to one of the lists. */
        public static difference<T>(a: List<T>, b: List<T>): List<T>
        {
            const newList = new List<T>();
            newList.addRange(a.filter((item) => !b.contains(item)), true);
            newList.addRange(b.filter((item) => !a.contains(item)), true);
            return newList;
        }

        /** Adds an item to the end of this list. Returns the index of the added item. */
        public add(item: T): number
        {
            this._data.push(item);
            return this._data.length - 1;
        }

        /** Adds all items from the specified array-like to this list. */
        public addRange(from: ArrayLike<T>, avoidDuplicates?: boolean): void;

        /** Adds all items from the specified list to this list. */
        public addRange(from: List<T>, avoidDuplicates?: boolean): void;

        public addRange(from: ArrayLike<T> | List<T>, avoidDuplicates: boolean = false): void
        {
            for (let i = 0, l = from.length; i < l; ++i)
            {
                const item = from instanceof List ? from.get(i) : from[i];
                if (!avoidDuplicates || !this.contains(item))
                {
                    this.add(item);
                }
            }
        }

        /** Inserts an item at the specified index. */
        public insert(item: T, index: number): void
        {
            this._data.splice(index, 0, item);
        }

        /** Finds the index of the given item in this list. Returns -1 if item is not found. */
        public indexOf(item: T): number
        {
            return this._data.indexOf(item);
        }

        /** Gets if the specified item can be found in this list. */
        public contains(item: T): boolean
        {
            return this.indexOf(item) !== -1;
        }

        /** Removes the specified item from this list. Returns false if item wasn't found. */
        public remove(item: T): boolean
        {
            const idx = this.indexOf(item);
            if (idx === -1) { return false; }
            this.removeAt(idx);
            return true;
        }

        /** Removes all items from the specified array-like from this list. */
        public removeRange(from: ArrayLike<T>): void;

        /** Removes all items from the specified list from this list. */
        public removeRange(from: List<T>): void;

        public removeRange(from: ArrayLike<T> | List<T>): void
        {
            for (let i = 0, l = from.length; i < l; ++i)
            {
                const item = from instanceof List ? from.get(i) : from[i];
                this.remove(item);
            }
        }

        /** Removes any duplicate items from this list. */
        public removeDuplicates(): void
        {
            for (let i = this.length - 1; i > 0; --i)
            {
                const itemA = this._data[i];
                for (let j = i - 1; j >= 0; --j)
                {
                    const itemB = this._data[j];
                    if (itemA === itemB)
                    {
                        this.removeAt(i);
                        break;
                    }
                }
            }
        }

        /** Removes the item at the specified index from this list. */
        public removeAt(index: number): void
        {
            this._data.splice(index, 1);
        }

        /** Removes ALL items from this list. */
        public clear(): void
        {
            this._data.length = 0;
        }

        /** Transforms each item in this list into a new form and returns a new list of the transformed items. */
        public map<T2>(functor: (this: void, item: T, index: number) => T2): List<T2>
        {
            return new List<T2>(this._data.map(functor));
        }

        public reduce(functor: (this: void, previousValue: T, currentValue: T, index: number) => T): T;
        public reduce<T2>(functor: (this: void, previousValue: T2, currentValue: T, index: number) => T2, initialValue: T2): T2;
        public reduce(functor: (this: void, previousValue: any, currentValue: T, index: number) => any, initialValue?: any): any
        {
            return arguments.length === 1 ? this._data.reduce(functor) : this._data.reduce(functor, initialValue);
        }

        /** Gets a new list containing only items within this list that passes the specified predicate. */
        public filter(predicate: (this: void, item: T) => boolean): List<T>
        {
            return new List<T>(this._data.filter(predicate));
        }

        /** Gets if all items in this list pass the specified predicate. */
        public all(predicate: (this: void, item: T) => boolean): boolean
        {
            for (const item of this._data)
            {
                if (!predicate(item))
                {
                    return false;
                }
            }
            return true;
        }

        /** Finds a single item matching the specified predicate, or null if none found. */
        public find(predicate: (item: T) => boolean): T|null
        {
            for (const item of this._data)
            {
                if (predicate(item))
                {
                    return item;
                }
            }
            return null;
        }

        /** Gets an item from this list. */
        public get(index: number): T { return this._data[index]; }

        /** Sets an item in this list. */
        public set(index: number, item: T) { this._data[index] = item; }

        /** Moves the item at fromIndex to toIndex. */
        public move(fromIndex: number, toIndex: number)
        {
            const item = this.get(fromIndex);
            this.removeAt(fromIndex);
            this.insert(item, toIndex);
        }

        /** Moves the item to newIndex. Returns false if the item wasn't found in this list. */
        public setItemIndex(item: T, newIndex: number): boolean
        {
            const idx = this.indexOf(item);
            if (idx === -1) { return false; }
            this.move(idx, newIndex);
        }

        /** Sorts this list. */
        public sort(comparer: (a: T, b: T) => number): void
        {
            this._data.sort(comparer);
        }

        /**
         * Sorts this list, ensuring that all items come before or after the set of items returned back from their respective predicates.
         * @param beforePredicate
         * @param afterPredicate
         */
        public resolveOrder(beforePredicate: (item: T) => ArrayLike<T>, afterPredicate: (item: T) => ArrayLike<T>)
        {
            function contains(arr: ArrayLike<T>, item: T)
            {
                for (let i = 0, l = arr.length; i < l; ++i)
                {
                    if (arr[i] === item)
                    {
                        return true;
                    }
                }
                return false;
            }
            const tmpArr = [...this._data];
            this._data.length = 0;

            let prevLen: number;
            while (this._data.length < tmpArr.length)
            {
                prevLen = this._data.length;
                for (const item of tmpArr)
                {
                    if (this.contains(item)) { continue; }

                    const before = beforePredicate(item);
                    const after = afterPredicate(item);

                    let arrHasEverything = true;
                    let maxIdx = tmpArr.length, minIdx = 0;
                    for (let i = 0, l = before.length; i < l; ++i)
                    {
                        let idx: number;
                        if ((idx = this.indexOf(before[i])) === -1)
                        {
                            arrHasEverything = false;
                            break;
                        }
                        maxIdx = Math.min(maxIdx, idx);
                    }
                    for (let i = 0, l = after.length; i < l; ++i)
                    {
                        let idx: number;
                        if ((idx = this.indexOf(after[i])) === -1)
                        {
                            arrHasEverything = false;
                            break;
                        }
                        minIdx = Math.max(minIdx, idx + 1);
                    }
                    if (!arrHasEverything) { continue; }

                    if (maxIdx < minIdx)
                    {
                        Log.error("Failed to resolveOrder on list (impossible situation?)");
                        return;
                    }

                    this.insert(item, minIdx);
                }
                if (prevLen === this._data.length)
                {
                    Log.error("Failed to resolveOrder on list (circular reference?)");
                    return;
                }
            }

            // this._data.sort((a, b) =>
            // {
            //     const aBefore = beforePredicate(a); // items that must come before a
            //     const aAfter = afterPredicate(a); // items that must come after a
            //     const bBefore = beforePredicate(b); // items that must come before b
            //     const bAfter = afterPredicate(b); // items that must come after b

            //     // if a falls in bBefore OR b falls in aAfter
            //     if (contains(bBefore, a) || contains(aAfter, b))
            //     {
            //         // a < b
            //         return 1;
            //     }
            //     // if b falls in aBefore OR a falls in bAfter
            //     else if (contains(aBefore, b) || contains(bAfter, a))
            //     {
            //         // a > b
            //         return -1;
            //     }
            //     else
            //     {
            //         // a == b
            //         return 0;
            //     }
            // });
        }

        public toString()
        {
            return this._data.toString();
        }
    }
}