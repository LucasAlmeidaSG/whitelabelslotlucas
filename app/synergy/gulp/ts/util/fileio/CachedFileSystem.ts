/// <reference path="IFileSystem.ts" />
/// <reference path="RawFileSystem.ts" />
/// <reference path="FileSystemCache.ts" />

namespace RS.FileIO
{
    export class CachedFileSystem implements IFileSystem
    {
        protected _baseSys: IFileSystem;
        protected _cache: FileSystemCache;

        public get cache() { return this._cache; }

        public constructor(public readonly settings: CachedFileSystem.Settings)
        {
            const fileSys = new RawFileSystem({
                ...settings,
                debugMode: false
            } as RawFileSystem.Settings);
            this._cache = new FileSystemCache(settings.cacheRoot, fileSys);
            this._baseSys = fileSys;
        }

        /** Returns the true path of the given file, accounting for casing. */
        public async findPath(path: string): Promise<string>
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.findPath: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.findPath(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.findPath(path); }

            // Find exact match.
            if (this._cache.get(path)) { return path; }

            // Find case-insensitive match.
            const flattenedPath = path.toLowerCase();
            for (const cachedPath of this._cache.paths)
            {
                if (cachedPath.toLowerCase() === flattenedPath)
                {
                    return cachedPath;
                }
            }

            // Path did not exist at all.
            return path;
        }

        public async isFolder(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.isFolder: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.isFolder(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.isFolder(path); }

            const node = this._cache.get(path);
            return node.type === FileSystemCache.NodeType.Directory;
        }

        protected cacheMiss(): void
        {
            Log.debug("Cache miss");
        }

        /**
         * Rebuilds the cache. Async.
         */
        public async rebuildCache()
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.rebuildCache`); }
            await this._cache.rebuild();
        }

        /**
         * Gets if the specified path falls within our cache.
         * @param path
         */
        protected pathIsWithinCache(path: string): boolean
        {
            const relToRoot = Path.relative(this.settings.cacheRoot, path);
            const segs = Path.split(relToRoot).map(seg => seg.toLowerCase());

            if (segs.indexOf("..") !== -1) { return false; }
            for (const excl of this.settings.excluded)
            {
                if (segs.indexOf(excl.toLowerCase()) !== -1) { return false; }
            }

            return true;
        }

        /**
         * Deletes the specified path. Async.
         */
        public async deletePath(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.deletePath: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.deletePath(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.deletePath(path); }
            const node = this.getNode(path);
            if (node == null) { return; }
            await this._baseSys.deletePath(path);
            this._cache.set(path, null);
            for (const p of this._cache.search(path, true))
            {
                this._cache.set(p, null);
            }
        }

        /**
         * Creates the specified path. Async.
         */
        public async createPath(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.createPath: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.createPath(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.createPath(path); }
            const node = this.getNode(path);
            if (node != null) { return path; }
            await this._baseSys.createPath(path);

            const spl = Path.split(Path.relative(this.settings.cacheRoot, path));
            let curPath: string | null = null;
            for (const seg of spl)
            {
                if (curPath == null)
                {
                    curPath = seg;
                }
                else
                {
                    curPath = Path.combine(curPath, seg);
                }
                const node = this.getNode(curPath);
                if (node == null) { this._cache.set(curPath, { type: FileSystemCache.NodeType.Directory }); }
            }
            return path;
        }

        /**
         * Copies a file from one location to another.
         */
        public async copyFile(srcPath: string, dstPath: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.copyFile: '${srcPath}' -> '${dstPath}'`); }
            if (!this.pathIsWithinCache(srcPath) || !this.pathIsWithinCache(dstPath)) { return await this._baseSys.copyFile(srcPath, dstPath); }
            if (!this._cache.contains(srcPath) || !this._cache.contains(dstPath)) { this.cacheMiss(); return await this._baseSys.copyFile(srcPath, dstPath); }
            const srcNode = this.getNode(srcPath);
            if (srcNode == null) { throw new Error(`File not found '${srcPath}'`); }
            await this._baseSys.copyFile(srcPath, dstPath);
            this._cache.set(dstPath, { ...srcNode });
        }

        /**
         * Copies multiple files from one location to another.
         * @param srcPaths
         * @param dstPaths
         */
        public async copyFiles(srcPaths: string[], dstPaths: string[])
        {
            const promises: PromiseLike<any>[] = [];
            for (let i = 0; i < srcPaths.length; i++)
            {
                promises.push(this.copyFile(srcPaths[i], dstPaths[i]));
            }
            await Promise.all(promises);
        }

        /**
         * Reads a text file.
         */
        public async readFile(path: string): Promise<string>;

        /**
         * Reads a text file.
         */
        public async readFile(path: string, binary: false): Promise<string>;

        /**
         * Reads a binary file.
         */
        public async readFile(path: string, binary: true): Promise<Buffer>;

        /**
         * Reads a text or binary file.
         */
        public async readFile(path: string, binary: boolean): Promise<string | Buffer>;

        /**
         * Reads a text file.
         */
        public async readFile(path: string, binary: boolean = false): Promise<string | Buffer>
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.readFile: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.readFile(path, binary); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.readFile(path, binary); }
            const node = this.getNode(path);
            if (node == null) { throw new Error(`File not found '${path}'`); }
            if (node.type !== FileSystemCache.NodeType.File) { throw new Error("Tried to read directory as a file"); }
            if (!binary && Is.string(node.content)) { return node.content; }
            if (binary && node.content instanceof Buffer) { return node.content; }
            const content = await this._baseSys.readFile(path, binary);
            node.content = content;
            return content;
        }

        /**
         * Streams a file into a function that accepts buffers.
         */
        public async streamFile(path: string, targetFunc: (buffer: Buffer) => void)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.streamFile: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.streamFile(path, targetFunc); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.streamFile(path, targetFunc); }
            const node = this.getNode(path);
            if (node == null) { throw new Error(`File not found '${path}'`); }
            if (node.type !== FileSystemCache.NodeType.File) { throw new Error("Tried to stream directory as a file"); }
            if (node.content instanceof Buffer)
            {
                targetFunc(node.content);
                return;
            }
            await this._baseSys.streamFile(path, targetFunc);
        }

        /**
         * Writes a text or binary file.
         */
        public async writeFile(path: string, content: string | Buffer | Uint8Array, options?: WriteOptions)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.writeFile: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.writeFile(path, content, options); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.writeFile(path, content, options); }
            const node = this.getNode(path);
            if (node != null)
            {
                if (node.type === FileSystemCache.NodeType.Directory)
                {
                    throw new Error("Tried to write directory as a file");
                }
                if (Is.string(node.content) && node.content === content) { return; }
            }
            await this._baseSys.writeFile(path, content, options);
            if (node != null)
            {
                (node as FileSystemCache.FileNode).content = content;
            }
            else
            {
                this._cache.set(path, { type: FileSystemCache.NodeType.File, content });
            }
        }

        /**
         * Writes multiple text or binary files.
         */
        public async writeFiles(paths: string[], contents: (string | Buffer | Uint8Array)[], options?: WriteOptions)
        {
            const promises: PromiseLike<any>[] = [];
            for (let i = 0; i < paths.length; i++)
            {
                promises.push(this.writeFile(paths[i], contents[i], options));
            }
            await Promise.all(promises);
        }

        /**
         * Tests if the specified file exists.
         * @param path
         */
        public async fileExists(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.fileExists: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.fileExists(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.fileExists(path); }
            const node = this.getNode(path);
            return node != null && node.type === FileSystemCache.NodeType.File;
        }

        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        public async readDirectory(path: string, fileOrFolder: false): Promise<string[]>;

        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        public async readDirectory(path: string, fileOrFolder: true): Promise<FileOrFolder[]>;

        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        public async readDirectory(path: string, fileOrFolder: boolean): Promise<string[] | FileOrFolder[]>;

        public async readDirectory(path: string, fileOrFolder: boolean): Promise<string[] | FileOrFolder[]>
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.readDirectory: '${path}'`); }
            if (!this._cache.contains(path))
            {
                this.cacheMiss();
                return await this._baseSys.readDirectory(path, fileOrFolder);
            }
            const node = this.getNode(path);
            if (node == null) { return []; }
            if (node.type !== FileSystemCache.NodeType.Directory) { throw new Error("Tried to read file as a directory"); }
            const result = this._cache.search(path, false).map(p => Path.relative(path, p));
            if (fileOrFolder)
            {
                return result.map<FileOrFolder>(p =>
                {
                    const node = this._cache.get(Path.combine(path, p));
                    if (node.type === FileSystemCache.NodeType.File)
                    {
                        return { type: "file", name: p };
                    }
                    else
                    {
                        return { type: "folder", name: p };
                    }
                });
            }
            else
            {
                return result;
            }
        }

        /**
         * Finds all files within the specified directory and sub-directories.
         * Returned paths are relative to the current working directory.
         * Async.
         * @param path
         */
        public async readFiles(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.readDirectory: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.readFiles(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.readFiles(path); }
            return this._cache.search(path, true).filter(p =>
            {
                const node = this._cache.get(p);
                if (node == null) { return false; } // Node could be null if the file existed previously but was deleted
                return node.type === FileSystemCache.NodeType.File;
            });
        }

        /**
         * Gets statistics for the specified file.
         * @param path
         */
        public async getFileStats(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.getFileStats: '${path}'`); }
            if (!this.pathIsWithinCache(path)) { return await this._baseSys.getFileStats(path); }
            if (!this._cache.contains(path)) { this.cacheMiss(); return await this._baseSys.getFileStats(path); }
            const node = this.getNode(path);
            if (node == null) { throw new Error(`File not found '${path}'`); }
            if (node.type !== FileSystemCache.NodeType.File) { throw new Error(`Path is not a file '${path}'`); }
            if (node.stats != null) { return node.stats; }
            return node.stats = await this._baseSys.getFileStats(path);
        }

        protected getNode(path: string): FileSystemCache.Node | null
        {
            const node = this._cache.get(path);
            if (node) { return node; }

            if (!this.settings.caseSensitive)
            {
                const flatPath = path.toLowerCase();
                for (const cachePath of this._cache.paths)
                {
                    if (cachePath.toLowerCase() === flatPath)
                    {
                        return this._cache.get(cachePath);
                    }
                }
            }

            return null;
        }
    }

    export namespace CachedFileSystem
    {
        export interface Settings extends RawFileSystem.Settings
        {
            cacheRoot: string;
            caseSensitive: boolean;
        }
    }
}
