/// <reference path="CachedFileSystem.ts" />

namespace RS
{
    export const FileSystem = new FileIO.CachedFileSystem({
        excluded: [ ".DS_Store", ".git", "node_modules", "bower_components" ],
        debugMode: false,
        cacheRoot: ".",
        caseSensitive: false
    });
}