namespace RS.FileIO
{
    export interface FileOrFolder
    {
        type: "file"|"folder";
        name: string;
    }

    export interface FileStats
    {
        dev: number;
        mode: number;
        nlink: number;
        uid: number;
        gid: number;
        rdev: number;
        blksize: number;
        ino: number;
        size: number;
        blocks: number;
        atimeMs: number;
        mtimeMs: number;
        ctimeMs: number;
        birthtimeMs: number;
        atime: Date;
        mtime: Date;
        ctime: Date;
        birthtime: Date;
    }

    /** https://nodejs.org/api/fs.html#fs_fs_writefile_file_data_options_callback */
    export interface WriteOptions
    {
        /** File Encoding - default 'utf8' */
        encoding?: string;
        /** Permissions - default 0o666 / -rw-rw-rw- / read/write for all users */
        mode?: Permissions;
        /** Flag - default 'w' https://nodejs.org/api/fs.html#fs_file_system_flags */
        flag?: string;
    }

    export interface IFileSystem
    {
        /**
         * Returns the true path of the given file, accounting for casing.
         */
        findPath(path: string): Promise<string>

        /**
         * Deletes the specified path.
         */
        deletePath(path: string): PromiseLike<void>;

        /**
         * Creates the specified path.
         */
        createPath(path: string): PromiseLike<string>;

        /**
         * Copies a file from one location to another.
         */
        copyFile(srcPath: string, dstPath: string): PromiseLike<void>;

        /**
         * Copies multiple files from one location to another.
         */
        copyFiles(srcPaths: string[], dstPaths: string[]): PromiseLike<void>;

        /**
         * Reads a text file.
         */
        readFile(path: string): PromiseLike<string>;

        /**
         * Reads a text file.
         */
        readFile(path: string, binary: false): PromiseLike<string>;

        /**
         * Reads a binary file.
         */
        readFile(path: string, binary: true): PromiseLike<Buffer>;

        /**
         * Reads a text or binary file.
         */
        readFile(path: string, binary: boolean): PromiseLike<string | Buffer>;

        /**
         * Streams a file into a function that accepts buffers.
         */
        streamFile(path: string, targetFunc: (buffer: Buffer) => void): PromiseLike<void>;

        /**
         * Writes a text or binary file.
         */
        writeFile(path: string, content: string | Buffer | Uint8Array, options?: WriteOptions): PromiseLike<void>;

        /**
         * Writes multiple text or binary files.
         */
        writeFiles(paths: string[], contents: (string | Buffer | Uint8Array)[], options?: WriteOptions): PromiseLike<void>;

        /**
         * Tests if the specified path exists.
         * NOTE: returns true if the given path exists and points to a folder.
         * @param path
         */
        fileExists(path: string): PromiseLike<boolean>;

        /**
         * Tests if the specified path is a folder.
         * NOTE: may or may not throw if the path does not exist.
         * @param path
         */
        isFolder(path: string): PromiseLike<boolean>;

        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: false): PromiseLike<string[]>;

        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: true): PromiseLike<FileOrFolder[]>;

        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        readDirectory(path: string, fileOrFolder: boolean): PromiseLike<string[] | FileOrFolder[]>;

        /**
         * Finds all files within the specified directory and sub-directories.
         * @param path
         */
        readFiles(path: string): PromiseLike<string[]>;

        /**
         * Gets statistics for the specified file.
         * @param path
         */
        getFileStats(path: string): PromiseLike<FileStats>;
    }
}
