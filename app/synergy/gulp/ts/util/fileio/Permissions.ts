namespace RS
{
    export enum Permission
    {
        None = 0,
        Execute = 1,
        Write = 2,
        WriteExecute = 3,
        Read = 4,
        ReadExecute = 5,
        ReadWrite = 6,
        ReadWriteExecute = 7
    }

    export interface Permissions
    {
        owner: Permission;
        group: Permission;
        other: Permission;
    }

    function validateArray(arr: any[])
    {
        if (!arr)
        {
            throw Error(`[Permissions] Input empty`);
        }

        if (arr.length !== 9)
        {
            throw Error(`[Permissions] Invalid input length`);
        }
    }

    function convertToBoolArray<T>(arr: T[], comparator?: (a: T) => boolean): boolean[]
    {
        validateArray(arr);
        return arr.map(comparator || ((a) => a ? true : false));
    }

    /** e.g. 724 for "-rwx-w-r--" */
    export function Permissions(decimal: number): Permissions;

    /** e.g. "724" for "-rwx-w-r--" */
    export function Permissions(octal: string): Permissions;

    /** e.g. "111010100" for "-rwx-w-r--" */
    export function Permissions(binary: string): Permissions;

    /** e.g. [ true, true, true, false, true, false, true, false, false ] for "-rwx-w-r--" */
    export function Permissions(binaryArray: boolean[]): Permissions;

    /** e.g. "-rwx-w-r--" for 724 */
    export function Permissions(str: string): Permissions;

    /** e.g. (Permission.Read | Permission.Write | Permission.Execute, Permission.Write, Permission.Read) for 724 */
    export function Permissions(owner: Permission, group: Permission, other: Permission): Permissions;

    export function Permissions(p1: number | string | boolean[] | Permission, p2?: Permission, p3?: Permission): Permissions
    {
        if (Is.number(p1))
        {
            if (p2 == undefined || p3 == undefined)
            {
                // decimal number
                // stringify and read as octal string
                return Permissions(p1.toString());
            }
            else
            {
                return {
                    owner: p1,
                    group: p2,
                    other: p3
                };
            }
        }
        else if (Is.arrayOf(p1, Is.boolean))
        {
            //binary array
            validateArray(p1);
            return {
                owner: (p1[0] ? Permission.Read : Permission.None) | (p1[1] ? Permission.Write : Permission.None) | (p1[2] ? Permission.Execute : Permission.None),
                group: (p1[3] ? Permission.Read : Permission.None) | (p1[4] ? Permission.Write : Permission.None) | (p1[5] ? Permission.Execute : Permission.None),
                other: (p1[6] ? Permission.Read : Permission.None) | (p1[7] ? Permission.Write : Permission.None) | (p1[8] ? Permission.Execute : Permission.None)
            };
        }
        else
        {
            if (p1.length === 3)
            {
                // octal string
                if (isNaN(parseInt(p1, 8)))
                {
                    throw Error(`[Permissions] Invalid octal input: ${p1}`);
                }

                return {
                    owner: parseInt(p1[0]),
                    group: parseInt(p1[1]),
                    other: parseInt(p1[2])
                }
            }
            else if (p1.length === 9)
            {
                //binary string
                return Permissions(convertToBoolArray(p1.split("")));
            }
            else if (p1.length === 10 && p1.indexOf("-") === 0)
            {
                // string with leading "-"
                return Permissions(convertToBoolArray(p1.slice(1).split(""), (s) => s === "-" ? false : true));
            }
            else
            {
                throw Error(`[Permissions] Unsupported input: ${p1}`);
            }
        }
    }

    export namespace Permissions
    {
        /** "724" */
        export function toString(permissions: Permissions): string
        {
            return `${permissions.owner}${permissions.group}${permissions.other}`;
        }
    }
}
