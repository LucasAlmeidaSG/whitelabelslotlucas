/// <reference path="RawFileSystem.ts" />

namespace RS.FileIO
{
    export class FileSystemCache
    {
        protected _nodes: Util.Map<FileSystemCache.Node>;
        protected _rootPath: string;
        protected _baseFileSys: IFileSystem;

        /**
         * Gets the paths of all cached nodes.
         */
        public get paths() { return Object.keys(this._nodes).filter(k => this._nodes[k] != null); }

        public constructor(rootPath: string, baseSys: IFileSystem)
        {
            this._rootPath = rootPath;
            this._baseFileSys = baseSys;
        }

        @Profile("FileSystemCache.rebuild")
        public async rebuild()
        {
            this._nodes = {};
            this.set(".", { type: FileSystemCache.NodeType.Directory });
            await this.buildDirectory(this._rootPath);
        }
        
        protected async buildDirectory(path: string)
        {
            const children = await this._baseFileSys.readDirectory(path, true);
            for (const child of children)
            {
                const fullPath = Path.combine(path, child.name);
                if (child.type === "file")
                {
                    this.set(fullPath, { type: FileSystemCache.NodeType.File, content: null });
                }
                else if (child.type === "folder")
                {
                    this.set(fullPath, { type: FileSystemCache.NodeType.Directory });
                    await this.buildDirectory(fullPath);
                }
            }
        }

        /**
         * Gets if the specified path is contained within this cache.
         * Does not check if the path actually exists, only that it sits in or below the root path.
         * @param path 
         */
        public contains(path: string): boolean
        {
            const relPath = Path.relative(this._rootPath, path);
            return relPath.indexOf("..") === -1;
        }

        /**
         * Retrieves a cached node.
         * @param path 
         */
        public get(path: string): FileSystemCache.Node | null
        {
            const relPath = Path.relative(this._rootPath, path);
            if (relPath.indexOf("..") !== -1) { return null; }
            return this._nodes[relPath] || null;
        }

        /**
         * Sets a cached node.
         * Returns if successful or not.
         * @param path 
         * @param node 
         */
        public set(path: string, node: FileSystemCache.Node | null): boolean
        {
            const relPath = Path.relative(this._rootPath, path);
            if (relPath.indexOf("..") !== -1) { return false; }
            this._nodes[relPath] = node;
            return true;
        }

        /**
         * Finds all child nodes under the specified path.
         * @param rootPath 
         * @param recurse
         */
        public search(path: string, recurse: boolean): string[]
        {
            path = Path.relative(this._rootPath, path);
            if (path.indexOf("..") !== -1) { return []; }
            if (path === "." || path === "")
            {
                path = "";
            }
            else
            {
                path = `${path}${Path.seperator}`;
            }
            return this.paths.filter(p =>
            {
                if (p === ".") { return false; }
                if (p.length < path.length) { return false; }
                if (p === path) { return false; }
                if (p.substr(0, path.length) !== path) { return false; }
                const nestLevel = Path.split(p.substr(path.length)).length;
                if (nestLevel > 1 && !recurse) { return false; }
                return true;
            });
        }
    }

    export namespace FileSystemCache
    {
        export enum NodeType
        {
            File,
            Directory
        }

        export interface BaseNode<T extends NodeType>
        {
            type: T;
        }

        export interface DirectoryNode extends BaseNode<NodeType.Directory>
        {

        }

        export interface FileNode extends BaseNode<NodeType.File>
        {
            content: string | Buffer | Uint8Array | null;
            stats?: FileStats;
        }

        export type Node = DirectoryNode | FileNode;
    }
}