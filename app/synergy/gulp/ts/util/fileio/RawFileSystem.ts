/// <reference path="../Command.ts" />
/// <reference path="../Path.ts" />
/// <reference path="../Profile.ts" />

/// <reference path="IFileSystem.ts" />

/** Contains file system manipulation functionality. */
namespace RS.FileIO
{
    const del = require("del");
    const fs = require("fs");
    const mkdirp = require("mkdirp");

    export class RawFileSystem implements IFileSystem
    {
        public constructor(public readonly settings: RawFileSystem.Settings) { }

        /** Returns the true path of the given file, accounting for casing. */
        public async findPath(path: string): Promise<string>
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.findPath: '${path}'`); }

            const parts = Path.split(path);

            const dir: string[] = [];
            const outPath: string[] = [];
            for (let i = 0; i < parts.length; i++)
            {
                const dirPath = Path.combine(...dir);
                const entries = await this.readDirectory(dirPath, false);

                // Find exact match.
                const part = parts[i];
                if (entries.some((entry) => entry === part))
                {
                    outPath.push(part);
                    dir.push(part);
                    continue;
                }

                // Find case insensitive match.
                const flattenedName = part.toLowerCase();
                let found = false;
                for (const entry of entries)
                {
                    if (entry.toLowerCase() === flattenedName)
                    {
                        found = true;
                        outPath.push(entry);
                        break;
                    }
                }

                if (!found)
                {
                    // Push remainder of input path and break out
                    const remainingParts = parts.slice(i);
                    outPath.push(...remainingParts);
                    break;
                }

                // Traverse down
                dir.push(part);
            }

            return Path.combine(...outPath);
        }

        public isFolder(path: string): PromiseLike<boolean>
        {
            return new Promise((resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.isFolder: '${path}'`); }
                fs.stat(path, (err, stats) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve(!stats.isFile());
                    }
                });
            });
        }

        /** Deletes the specified path. Async. */
        @Profile("FileIO.deletePath")
        public deletePath(path: string): PromiseLike<void>
        {
            return del([path], {force: true});
        }

        /** Creates the specified path. Async. */
        @Profile("FileIO.createPath")
        public createPath(path: string): PromiseLike<string>
        {
            return new Promise((resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.createPath: '${path}'`); }
                mkdirp(path, err =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve(path);
                    }
                });
            });
        }

        /**
         * Copies a file from one location to another.
         */
        @Profile("FileIO.copyFile")
        public copyFile(srcPath: string, dstPath: string): PromiseLike<void>
        {
            return new Promise(async (resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.copyFile: '${srcPath}' -> '${dstPath}'`); }

                await this.removeCaseConflicts(dstPath);

                let cbCalled = false;

                const rd = fs.createReadStream(srcPath);
                rd.on("error", (err) => done(err));
                const wr = fs.createWriteStream(dstPath);
                wr.on("error", (err) => done(err));
                wr.on("close", (err) => done(err));
                rd.pipe(wr);

                function done(err: string)
                {
                    if (!cbCalled)
                    {
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            resolve();
                        }
                        cbCalled = true;
                    }
                }
            });
        }

        /**
         * Copies multiple files from one location to another.
         * @param srcPaths
         * @param dstPaths
         */
        public async copyFiles(srcPaths: string[], dstPaths: string[])
        {
            const promises: PromiseLike<any>[] = [];
            for (let i = 0; i < srcPaths.length; i++)
            {
                promises.push(this.copyFile(srcPaths[i], dstPaths[i]));
            }
            await Promise.all(promises);
        }

        /**
         * Reads a text file.
         */
        public readFile(path: string): PromiseLike<string>;

        /**
         * Reads a text file.
         */
        public readFile(path: string, binary: false): PromiseLike<string>;

        /**
         * Reads a binary file.
         */
        public readFile(path: string, binary: true): PromiseLike<Buffer>;

        /**
         * Reads a text or binary file.
         */
        public async readFile(path: string, binary: boolean): Promise<string | Buffer>;

        @Profile("FileIO.readFile")
        public readFile(path: string, binary: boolean = false): PromiseLike<string | Buffer>
        {
            return new Promise((resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.readFile: '${path}'`); }

                fs.readFile(path, (err: string, data: Buffer) =>
                {
                    if (err)
                    {
                        reject(err);
                        return;
                    }

                    if (binary)
                    {
                        resolve(data);
                        return;
                    }

                    // Scan the buffer for \r characters
                    const strArr: string[] = [];
                    let readPos = 0;
                    for (let i = 0; i < data.length; i++)
                    {
                        const val = data[i];
                        const nextVal = i >= data.length? "":data[i+1];
                        //carriage return AND NOT line feed
                        if (val == 0x0D && nextVal != 0x0A)
                        {
                            if (i > readPos)
                            {
                                strArr.push(data.toString("utf8", readPos, i));
                                strArr.push("\n");
                            }
                            readPos = i + 1;
                        }
                    }
                    if (readPos < data.length - 1)
                    {
                        strArr.push(data.toString("utf8", readPos, data.length));
                    }
                    resolve(strArr.join(""));
                });
            });
        }

        /**
         * Streams a file into a function that accepts buffers.
         */
        @Profile("FileIO.streamFile")
        public streamFile(path: string, targetFunc: (buffer: Buffer) => void): PromiseLike<void>
        {
            return new Promise((resolve, reject) =>
            {
                const strm = fs.createReadStream(path);
                let resolved = false;
                strm.on("end", () =>
                {
                    if (resolved) { return; }
                    resolve();
                    resolved = true;
                });
                strm.on("error", (err: Error) =>
                {
                    if (resolved) { return; }
                    reject(err);
                    resolved = true;
                });
                strm.on("data", (chunk: Buffer) =>
                {
                    if (resolved) { return; }
                    targetFunc(chunk);
                });
            });
        }

        /**
         * Writes multiple text or binary files.
         */
        public async writeFiles(paths: string[], contents: (string | Buffer | Uint8Array)[], options?: WriteOptions)
        {
            const promises: PromiseLike<any>[] = [];
            for (let i = 0; i < paths.length; i++)
            {
                promises.push(this.writeFile(paths[i], contents[i], options));
            }
            await Promise.all(promises);
        }

        /**
         * Writes a text or binary file.
         */
        @Profile("FileIO.writeFile")
        public writeFile(path: string, content: string | Buffer | Uint8Array, options?: WriteOptions): PromiseLike<void>
        {
            return new Promise(async (resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.writeFile: '${path}'`); }

                const fsOptions = options ?
                {
                    encoding: options.encoding,
                    mode: Permissions.toString(options.mode),
                    flag: options.flag
                } : null;

                await this.removeCaseConflicts(path);

                fs.writeFile(path, content, fsOptions, (err) =>
                {
                    if (err)
                    {
                        reject(err);
                        return;
                    }
                    resolve();
                });
            });
        }

        /**
         * Tests if the specified file exists.
         * @param path
         */
        @Profile("FileIO.fileExists")
        public fileExists(path: string): PromiseLike<boolean>
        {
            return new Promise((resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.fileExists: '${path}'`); }

                fs.access(path, fs.constants.F_OK | fs.constants.R_OK, (err) =>
                {
                    if (err)
                    {
                        resolve(false);
                    }
                    else
                    {
                        resolve(true);
                    }
                });
            });
        }

        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        public readDirectory(path: string, fileOrFolder: false): PromiseLike<string[]>;

        /**
         * Finds all files and folders within the specified directory.
         * @param path
         * @param fileOrFolder
         */
        public readDirectory(path: string, fileOrFolder: true): PromiseLike<FileOrFolder[]>;

        /**
         * Finds all files and folders within the specified directory.
         * Returned paths are relative to the passed in path.
         * @param path
         * @param fileOrFolder
         */
        public async readDirectory(path: string, fileOrFolder: boolean): Promise<string[] | FileOrFolder[]>;

        @Profile("FileIO.readDirectory")
        public readDirectory(path: string, fileOrFolder: boolean): PromiseLike<string[] | FileOrFolder[]>
        {
            return new Promise((resolve, reject) =>
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.readDirectory: '${path}'`); }
                fs.readdir(path, (err: string|null, result: string[]) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        result = result.filter(file => this.settings.excluded.indexOf(file) === -1);
                        if (fileOrFolder)
                        {
                            const fileOrFolderResult: FileOrFolder[] = [];
                            let remainCnt = result.length;
                            const done = (name: string, err?: string, stat?: any) =>
                            {
                                if (!err)
                                {
                                    fileOrFolderResult.push({ name, type: stat.isFile() ? "file" : "folder" });
                                }
                                remainCnt--;
                                if (remainCnt === 0)
                                {
                                    resolve(fileOrFolderResult);
                                }
                            };
                            for (const name of result)
                            {
                                fs.stat(`${path}/${name}`, done.bind(this, name));
                            }
                            if (result.length === 0)
                            {
                                resolve(fileOrFolderResult);
                            }
                        }
                        else
                        {
                            resolve(result);
                        }
                    }
                });
            });
        }

        /**
         * Finds all files within the specified directory and sub-directories. Async.
         * @param path
         */
        public async readFiles(path: string)
        {
            if (this.settings.debugMode) { Log.debug(`FileSystem.readFiles: '${path}'`); }
            const fileList: string[] = [];

            // Get a recursive listing of all files
            try
            {
                const subFiles = await this.readDirectory(path, true);
                for (const subFile of subFiles)
                {
                    const subPath = Path.combine(path, subFile.name);
                    if (subFile.type === "file")
                    {
                        fileList.push(subPath);
                    }
                    else
                    {
                        for (const subFile2 of await this.readFiles(subPath))
                        {
                            fileList.push(subFile2);
                        }
                    }
                }
            }
            catch
            {
                // Directory doesn't exist probably, fall through and return empty array
            }
            return fileList;
        }

        /**
         * Gets statistics for the specified file.
         * @param path
         */
        public getFileStats(path: string): Promise<FileStats>
        {
            return new Promise((resolve, reject) =>
            {
                fs.stat(path, (err, stats) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else
                    {
                        resolve(stats);
                    }
                });
            });
        }

        protected async removeCaseConflicts(path: string): Promise<void>
        {
            const casedPath = await this.findPath(path);
            if (casedPath !== path)
            {
                if (this.settings.debugMode) { Log.debug(`FileSystem.removeCaseConflicts: '${casedPath}' -> '${path}'`); }

                // Find out if we've got a folder difference
                const casedParts = Path.split(casedPath);
                const pathParts = Path.split(path);

                if (casedParts.slice(0, casedParts.length - 1).some((casedPart, index) => casedPart !== pathParts[index]))
                {
                    Log.warn(`FileSystem.removeCaseConflicts: cannot resolve directory case mismatch: '${casedPath}' -> '${path}'`);
                }
                else
                {
                    await this.deletePath(casedPath);
                }
            }
        }
    }

    export namespace RawFileSystem
    {
        export interface Settings
        {
            excluded: string[];
            debugMode: boolean;
        }
    }
}
