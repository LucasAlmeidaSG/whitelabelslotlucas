/** Contains common type guards. */
namespace RS.Is
{
    /**
     * Gets if the specified value is a number.
     * @param value The value to test
     */
    export function number(value: any): value is number
    {
        return value != null && !isNaN(value) && (typeof value === "number" || value instanceof Number);
    }

    /**
     * Gets if the specified value is an integer.
     * Does not type-guard, as integer type currently impossible.
     * @param value The value to test
     */
    export function integer(value: any): boolean
    {
        return Is.number(value) && (value % 1) === 0;
    }

    /**
     * Gets if the specified value is a string.
     * @param value The value to test
     */
    export function string(value: any): value is string
    {
        return value != null && (typeof value === "string" || value instanceof String);
    }

    /**
     * Gets if the specified value is an array.
     * @param value The value to test
     */
    export function array(value: any): value is any[] | ReadonlyArray<any>
    {
        return value != null && Array.isArray(value);
    }

    /**
     * Gets if the specified value is an array of a specific type.
     * @param value The value to test
     */
    export function arrayOf<T>(value: any, innerTester: (value: any) => value is T): value is T[] | ReadonlyArray<T>
    {
        return value != null && Is.array(value) && (value as Array<T>).every((el) => innerTester(el));
    }

    /**
     * Gets if the specified value is an object but NOT an array.
     * @param value The value to test
     */
    export function object(value: any): value is { [key: string]: any }
    {
        return value != null && typeof value === "object" && !Array.isArray(value);
    }

    /**
     * Gets if the specified value is a function.
     * @param value The value to test
     */
    export function func(value: any): value is Function
    {
        return value != null && (typeof value === "function" || value instanceof Function);
    }

    /**
     * Gets if the specified value is a boolean.
     * @param value The value to test
     */
    export function boolean(value: any): value is boolean
    {
        return value === true || value === false;
    }

    /**
     * Gets if the specified value is a primitive.
     * @param value
     */
    export function primitive(value: any): value is (boolean|number|string)
    {
        return Is.number(value) || Is.boolean(value) || Is.string(value);
    }

    /**
     * Gets if the specified value is a non-primitive.
     * @param value
     */
    export function nonPrimitive(value: any): value is object
    {
        return value != null && !Is.primitive(value);
    }
}

namespace RS
{
    /**
     * Returns the given value if the type-guard matches, otherwise returns null.
     */
    export function as<T>(value: any, guard: (value: any) => value is T, defaultValue?: T | null): T | null;
    export function as<T>(value: any, guard: (value: any) => boolean, defaultValue?: T | null): T | null;
    export function as<T>(value: any, guard: (value: any) => boolean, defaultValue: T | null = null): T | null
    {
        if (guard(value)) { return value; }
        return defaultValue;
    }
}