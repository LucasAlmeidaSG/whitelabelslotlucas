namespace RS
{
    /** A type of decorator that tags a class with a some kind of metadata. */
    export interface Tag<T>
    {
        (value: T): ClassDecorator;
        get(obj: object): T;
        classes: ({ new(): object; } & Function)[];
        getClassesWithTag(value: T): ({ new(): object; } & Function)[];
    }

    export namespace Tag
    {
        let nextTagID = 0;

        type WithTags = { __tags?: any[] };
        function hasTags<T>(value: any): value is WithTags
        {
            return Is.object(value) && Is.array(value._tags);
        }

        /**
         * Creates a new tag.
         */
        export function create<T>(): Tag<T>
        {
            const tagID = nextTagID++;
            const decoratorAsTag = decorator as Tag<T>;
            function decorator(value: T): ClassDecorator
            {
                return function<T extends Function>(target: T)
                {
                    // target is the constructor of the function
                    const targetEx = target.prototype as (WithTags & Object);
                    if (targetEx.__tags == null || targetEx.__tags === Object.getPrototypeOf(targetEx).__tags) { targetEx.__tags = []; }
                    targetEx.__tags[tagID] = value;
                    decoratorAsTag.classes.push(target as any);
                };
            }
            decoratorAsTag.get = function(this: Tag<T>, obj: object): T|null
            {
                if (Is.func(obj))
                {
                    const protoChain = Util.getInheritanceChain(obj, true);
                    for (const ctor of protoChain)
                    {
                        const proto = ctor.prototype;
                        if (proto.__tags) { return proto.__tags[tagID]; }
                    }
                }
                else if (hasTags(obj))
                {
                    return obj.__tags[tagID];
                }
                else
                {
                    const proto = Object.getPrototypeOf(obj);
                    return this.get(proto.constructor);
                }
                return null;
            };
            decoratorAsTag.classes = [];
            decoratorAsTag.getClassesWithTag = function(this: Tag<T>, value: T)
            {
                return this.classes.filter(c => decoratorAsTag.get(c) === value);
            };
            return decoratorAsTag;
        }
    }
}