/// <reference path="Command.ts" />

/** Contains TypeScript compiler functionality. */
namespace RS.TSLint
{
    const tslintPath = "./node_modules/tslint/bin/tslint";

    /**
     * Compiles the specified TypeScript project.
     * @param projectPath
     * @param logOutput
     */
    export function lint(projectPath: string, lintPath: string, logOutput: boolean = false): PromiseLike<void>
    {
        return new Promise<void>((resolve, reject) =>
        {
            Command.run(tslintPath, ["-c", lintPath, "--project", Path.combine(projectPath, "tsconfig.json")], undefined, logOutput)
                .then((data) =>
                {
                    if (data.length > 0)
                    {
                        reject(new LintFailure(data.trim()));
                    }
                    else
                    {
                        resolve();
                    }
                }, (reason) =>
                {
                    const reasonAsStr = Is.string(reason) ? reason : reason.toString() as string;
                    if (/ENOENT/g.test(reasonAsStr))
                    {
                        reject("TSLint not found. Check that node modules are installed.");
                    }
                    else
                    {
                        reject(reason);
                    }
                });
        });
    }

    export class LintFailure
    {
        public get isError() { return /ERROR: /.test(this.data); }
        constructor(public readonly data: string) {}
        public toString() { return `${this.data}`; }
    }
}