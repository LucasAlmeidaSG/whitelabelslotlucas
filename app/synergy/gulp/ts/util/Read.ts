/** Contains functions for reading strings as various types of value. */
namespace RS.Read
{
    const truthyStrings: ReadonlyArray<string> = ["true", "1", "y", "yes", "pls"];
    const falsyStrings: ReadonlyArray<string> = ["false", "0", "n", "no", "noty"];

    /**
     * Parses the given string as an boolean.
     * @return a boolean (or null if defaultValue = null and value is not a boolean)
     */
    export function boolean(value: string, defaultVal: boolean | null = null): boolean | null
    {
        const lower = value.toLowerCase();
        if (truthyStrings.indexOf(lower) !== -1) { return true; }
        if (falsyStrings.indexOf(lower) !== -1) { return false; }
        return defaultVal;
    }

    /**
     * Parses the given string as an integer.
     * @return a number (or null if defaultValue = null and value is not an integer)
     */
    export function integer(value: string, defaultVal: number | null = null): number | null
    {
        if (value == "") { return defaultVal; }
        const parsed = Number(value);
        return as(parsed, Is.integer, defaultVal);
    }

    /**
     * Parses the given string as a number.
     * @return a number (or null if defaultValue = null and value is not a number)
     */
    export function number(value: string, defaultVal: number | null = null): number | null
    {
        if (value == "") { return defaultVal; }
        const parsed = Number(value);
        return as(parsed, Is.number, defaultVal);
    }

    /**
     * Parses the given string as an object.
     * @return an object or null
     */
    export function object(value: string): object | null
    {
        let parsed: any = null;
        try { parsed = JSON.parse(value); } catch { /* Do nothing, we'll return null. */ }
        return as(parsed, Is.object);
    }

    /**
     * Parses the given string as an array of a specific type.
     * @return an array of T and null values
     */
    export function arrayOf<T>(value: string, innerReader: (value: string) => (T | null), delimiter: string = ","): T[]
    {
        const strings = value.split(delimiter);
        const values = strings.map((str) => innerReader(str));
        return values;
    }
}