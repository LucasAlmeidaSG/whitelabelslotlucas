namespace RS
{
    /** Timer used for profiling. */
    export class Timer
    {
        /** Gets the current timestamp. */
        public static get now()
        {
            const hrtime = process.hrtime();
            return (hrtime[0] + hrtime[1] / 1e9) * 1000.0;
        }

        protected _elapsed: number = 0;
        protected _running: boolean = true;
        protected _last: number = Timer.now;

        /** Gets if this timer is running. */
        public get running() { return this._running; }

        /** Gets how long this timer has been running for (ms). */
        public get elapsed()
        {
            this.update();
            return this._elapsed;
        }

        /** Starts this timer. */
        public start()
        {
            if (this._running) { return; }
            this._last = Timer.now;
            this._running = true;
        }

        /** Stops this timer and resets it. */
        public stop()
        {
            this._running = false;
            this._elapsed = 0;
        }

        /** Pauses this timer. */
        public pause()
        {
            this.update();
            this._running = false;
        }

        /** Consumes any pending time into elapsed. */
        protected update()
        {
            if (!this._running) { return; }
            const now = Timer.now;
            const delta = now - this._last;
            this._last = now;
            this._elapsed += delta;
        }

        /** Gets a human-friendly representaton of this timer's total elapsed time in seconds. */
        public toString(): string
        {
            return `${Math.round(this.elapsed / 10.0) / 100.0} s`;
        }
    }
}