namespace RS.Util
{
    /**
     * Gets the chain of classes that the specified class derives.
    **/
    export function getInheritanceChain(obj: Function, includeSelf = false): Function[]
    {
        const result: Function[] = [];
        let proto = obj.prototype;
        if (includeSelf) { result.push(obj); }
        while ((proto = Object.getPrototypeOf(proto)) != null && proto !== Function.prototype)
        {
            result.push(proto.constructor);
        }
        return result;
    }

    export type Map<T> = { [key: string]: T };

    /**
     * Escapes all special regular expression characters in the string so that it may be matched in a RegExp.
     *
     * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters.
     */
    export function escapeRegExpChars(str: string): string
    {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    }

    /** Replaces all occurrences of needle in haystack with replacement. */
    export function replaceAll(haystack: string, needle: string, replacement: string): string;
    export function replaceAll(haystack: string, needles: Map<string>): string;
    export function replaceAll(haystack: string, p2: string | Map<string>, replacement?: string): string
    {
        if (Is.string(p2))
        {
            const re = new RegExp(escapeRegExpChars(p2), "g");
            return haystack.replace(re, replacement);
        }
        else
        {
            let reStr = "";
            for (const needle in p2)
            {
                if (reStr.length) { reStr += "|"; }
                reStr += escapeRegExpChars(needle);
            }
            const re = new RegExp(reStr, "g");
            return haystack.replace(re, (key) => p2[key]);
        }
    }

    /**
     * Splits the specified string into an array of lines.
     * @param str
     */
    export function splitLines(str: string): string[]
    {
        return this._src.split(/\r?\n/g);
    }

    /** Copes one error's stack trace into another. */
    export function copyErrorStack(source: Error, dest: Error): void
    {
        const sourceStack: string = source.stack;
        if (sourceStack)
        {
            const firstLevel = sourceStack.search(/\n\s*at/g);
            if (firstLevel !== -1)
            {
                const trace = sourceStack.slice(firstLevel);
                dest.stack = `${dest.name}: ${dest.message}${trace}`;
            }
        }
    }

    /**
     * Copies a block of bytes from one buffer to another.
     * @param from
     * @param fromOffset
     * @param to
     * @param toOffset
     * @param length
     */
    export function copyBytes(from: ArrayBuffer, fromOffset: number, to: ArrayBuffer, toOffset: number, length: number): void
    {
        if (fromOffset + length > from.byteLength)
        {
            throw new Error("Length exceeds size of 'from' buffer");
        }
        if (toOffset + length > to.byteLength)
        {
            throw new Error("Length exceeds size of 'to' buffer");
        }

        // Copy 32 bits at a time
        let i32cnt = length >> 2;
        if (i32cnt > 0)
        {
            const fi32 = new Uint32Array(from, fromOffset, i32cnt);
            const ti32 = new Uint32Array(to, toOffset, i32cnt);
            let iOffset = 0;
            while (--i32cnt >= 0)
            {
                ti32[iOffset] = fi32[iOffset];
                ++iOffset;
            }
            length &= 3;
            fromOffset += fi32.byteLength;
            toOffset += ti32.byteLength;
        }

        // Copy 8 bits at a time
        const fi8 = new Uint8Array(from, fromOffset, length);
        const ti8 = new Uint8Array(to, toOffset, length);
        while (--length >= 0)
        {
            ti8[toOffset++] = fi8[fromOffset++];
        }
    }

    const sizeFormats =
    [
        { base: 2 ** 0, postfix: "B" },
        { base: 2 ** 10, postfix: "KiB" },
        { base: 2 ** 20, postfix: "MiB" },
        { base: 2 ** 30, postfix: "GiB" }
    ];

    /**
     * Formats a size in bytes into a human friendly string (e.g. 827567 => 828 KiB)
     * @param byteSize
     */
    export function formatSize(byteSize: number): string
    {
        let formatIndex: number;
        for (formatIndex = 0; formatIndex < sizeFormats.length; ++formatIndex)
        {
            const sz = byteSize / sizeFormats[formatIndex].base;
            if (sz < 1.0) { break; }
        }
        formatIndex--;

        const format = sizeFormats[formatIndex];
        const sz = byteSize / format.base;

        return `${sz.toFixed(1)} ${format.postfix}`;
    }
}