namespace RS
{
    const _http: typeof http = require("http");
    const _mimeTypes = require("mime-types");

    const debugMode = false;
    /** An extremely lightweight server that just serves a directory. */
    export class WebServer
    {
        private readonly _server: http.Server;
        public get isRunning() { return this._server.listening; }

        constructor(public readonly ip: string, public readonly port: number, public readonly rootPath: string)
        {
            this.serve = this.serve.bind(this);
            this._server = _http.createServer(this.serve);
        }

        public open()
        {
            return new Promise<void>((resolve, reject) =>
            {
                this._server.listen(this.port, this.ip, () =>
                {
                    Log.info(`Listening on ${this.ip}:${this.port} at ${this.rootPath}`);
                    // Asynchronously start pre-cache
                    this.preCache();
                    resolve();
                });
            });
        }

        public close()
        {
            return new Promise<void>((resolve, reject) =>
            {
                this._server.close((err) =>
                {
                    if (err) { reject(err); return; }
                    resolve();
                });
            });
        }

        private debug(message: string)
        {
            if (!debugMode) { return; }
            Log.debug(message);
        }

        private async preCache()
        {
            this.debug("Pre-caching index page.");
            const path = Path.combine(this.rootPath, "index.html");
            await FileSystem.readFile(path, true);
        }

        private async serve(request: http.IncomingMessage, response: http.ServerResponse)
        {
            const queryIndex = request.url.indexOf("?");
            const path = queryIndex === -1 ? request.url : request.url.substr(0, queryIndex);

            this.debug(`Received request for ${path}`);
            
            const baseName = Path.baseName(path) || "index.html";
            const directoryName = Path.directoryName(path);

            const fullPath = Path.combine(this.rootPath, directoryName, baseName);
            this.debug(`Reading file at ${fullPath}`);

            if (!(await FileSystem.fileExists(fullPath)))
            {
                this.debug(`${fullPath} not found`);
                response.writeHead(404, { "Content-Type": "text/plain" });
                response.write("404 File Not Found");
                response.end();
                return;
            }

            const mimeType = _mimeTypes.lookup(fullPath);
            this.debug(`Using MIME type ${mimeType}`);

            const responseData = await FileSystem.readFile(fullPath, true);
            response.writeHead(200, { "Content-Type": mimeType });
            response.write(responseData, "binary");
            response.end(null, "binary");
        }
    }
}