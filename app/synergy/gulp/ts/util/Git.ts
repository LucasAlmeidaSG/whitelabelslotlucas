namespace RS.Git
{
    /**
     * Removes a tag from the current commit.
     * @param name 
     * @param message 
     * @param repo 
     */
    export async function untag(name: string, repo?: string)
    {
        await Command.run("git",
        [
            "tag",
            "-d", name
        ], repo, false);
        await Command.run("git",
        [
            "push", "origin",
            `:${name}`,
            "-q"
        ], repo, false);
    }

    /**
     * Adds an annotated tag to the current commit.
     * @param name 
     * @param message 
     * @param repo 
     */
    export async function tag(name: string, message: string, repo?: string)
    {
        await Command.run("git",
        [
            "tag",
            "-a", name,
            "-m", message
        ], repo, false);
    }

    /**
     * Pushes the specified local tag to remote.
     * @param name
     * @param repo
     */
    export async function pushTag(name: string, repo?: string)
    {
        await Command.run("git",
        [
            "push", "origin",
            name,
            "-q"
        ], repo, false);
    }

    /**
     * Pushes all local tags to remote.
     * @param repo
     */
    export async function pushTags(repo?: string)
    {
        await Command.run("git",
        [
            "push", "origin",
            "--tags",
            "-q"
        ], repo, false);
    }

    /**
     * Clones a specified repo to a path, if the path is valid and repo doesn't already exist
     * @param repo
     * @param clonePath
     * @param args
     */
    export async function clone(repo: string, clonePath: string, args?: ReadonlyArray<string>)
    {
        if (RS.FileSystem.fileExists(clonePath))
        {
            return;
        }
        await Command.run("git",
        [
            "clone",
            // ...args,
            repo,
            clonePath
        ], null, false);
    }

    /**
     * Commits all changes in specified repo with a message
     * @param repoPath
     * @param message
     */
    export async function commitAll(repoPath: string, message: string)
    {
        // Check if the destination folder is already present
        try
        {
            await Command.run("git",
            [
                "commit",
                "-am",
                message
            ], repoPath, false);
        }
        // If not present or invalid path
        catch (error)
        {
            Log.error(`Error committing changes to repo '${repoPath}': ${error}`);
        }
    }

    /**
     * Pushes up changes in repo path
     * @param repoPath
     */
    export async function push(repoPath: string)
    {
        try
        {
            await Command.run("git", [ "push" ], repoPath, false);
        }
        // If not present or invalid path
        catch (error)
        {
            Log.error(`Error pushing changes to repo '${repoPath}': ${error}`);
        }
    }

    /**
     * Checks out the specified branch and pulls the latest changes, optionally by force
     * @param repoPath
     * @param branch
     * @param force
     */
    export async function update(repoPath: string, branch: string, force: boolean = true)
    {
        // Try to check out the desired branch
        try
        {
            await Command.run("git",
            [
                "checkout",
                (force ? "-f" : null),
                branch
            ], repoPath, false);
        }
        catch (error)
        {
            // If the error isn't that we're already on the branch (Network, invalid name etc) then re-throw
            if (error != `Already on '${branch}'\n` && error != `Error: Switched to branch '${branch}'`)
            {
                throw new Error(error);
            }
        }

        await Command.run("git", [ "pull" ], repoPath, false);
    }
}