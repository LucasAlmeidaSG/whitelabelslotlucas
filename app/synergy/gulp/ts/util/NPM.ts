/// <reference path="Command.ts" />

/** Contains utility functions for the node module system. */
namespace RS.NPM
{
    let pendingInstall: Promise<void> | null = null;

    /**
     * Installs a node module locally.
     * @param name 
     * @param version 
     */
    export async function installPackage(name: string, version: string)
    {
        // We must be careful to only run one installPackageInner promise at once
        if (pendingInstall != null)
        {
            await pendingInstall;
            pendingInstall = null;
        }
        const myPromise = pendingInstall = installPackageInner(name, version);
        await pendingInstall;
        if (pendingInstall === myPromise)
        {
            pendingInstall = null;
        }
    }

    /**
     * Installs a node module locally without parallel checks.
     * @param name 
     * @param version 
     */
    async function installPackageInner(name: string, version: string)
    {
        // Check that the command exists
        if (!await Command.exists("npm"))
        {
            throw new Error("Could not find npm - check that it's installed! (which it will be because it's part of node, so how you managed to get this error is beyond me...)");
        }

        Log.info(`Installing node module '${name}' (${version}) via npm...`);

        // Execute
        try
        {
            const formattedName = /:\/\//g.test(version) ? version : `${name}@${version}`;
            const logOutput = await Command.run("npm", [ "install", formattedName ], undefined, false);
        }
        catch (err)
        {
            if (/npm ERR/g.test(err.toString()))
            {
                Log.error(err);
            }
        }
    }
}