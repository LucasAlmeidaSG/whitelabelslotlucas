namespace RS.JSMinify
{
    export function minify(sourcePath: string, mapPath: string): PromiseLike<void>;
    export async function minify(sourcePath: string, mapPath: string): Promise<void>
    {
        Profile.enter("jsminify");

        try
        {
            await Command.run("node",
            [
                "--max-old-space-size=4096",
                Path.combine(__dirname, "jsminify.js"),
                sourcePath,
                mapPath
            ], undefined, false);
        }
        finally
        {
            Profile.exit("jsminify");
        }

        FileSystem.cache.set(sourcePath, { type: FileIO.FileSystemCache.NodeType.File, content: null });
        if (mapPath != null) { FileSystem.cache.set(mapPath, { type: FileIO.FileSystemCache.NodeType.File, content: null }); }
    }
}