/// <reference path="MessageHandler.ts"/>
namespace RS
{
    const cp: typeof child_process = require("child_process");

    export class Sandbox extends MessageHandler
    {
        private readonly _scriptPath: string;
        private readonly _args: ReadonlyArray<string>;
        private _process: child_process.ChildProcess;

        constructor(scriptPath: string, ...args: string[])
        {
            super();
            
            this._scriptPath = scriptPath;
            this._args = args;

            this.onExit = this.onExit.bind(this);
            this.onError = this.onError.bind(this);
        }

        public async start()
        {
            if (this._process) { throw new Error(`Attempted to start sandbox that had already been started`); }
            this._process = cp.fork(this._scriptPath, this._args);

            this._process.on("message", this.onMessageReceived);
            this._process.on("error", this.onError);
            this._process.on("close", this.onExit);
            this._process.on("exit", this.onExit);
        }

        public send(message: string): Promise<void>
        {
            if (!this._process) { throw new Error(`Cannot send a message to an inactive sandbox`); }
            return new Promise<void>((resolve, reject) =>
            {
                this._process.send(message, (err) =>
                {
                    Log.debug(`Sent message: ${message}`);
                    if (err) { reject(err); return; }
                    resolve();
                });
            });
        }

        public dispose()
        {
            if (this.isDisposed) { return; }
            this.detachProcessListeners();
            this._process.kill();
            this._process = null;
            super.dispose();
        }

        private onError(err: Error)
        {
            this.detachProcessListeners();
            this.rejectAllMessageListeners(err);
        }

        private onExit(code: number, signal: string)
        {
            this.detachProcessListeners();
            this.rejectAllMessageListeners(new Error(`Process exited with code ${code}${signal != null ? ` (signal ${signal})` : ""}`));
        }

        private detachProcessListeners()
        {
            Log.debug(`Detaching process listeners`, "Sandbox");
            this._process.off("message", this.onMessageReceived);
            this._process.off("error", this.onError);
            this._process.off("close", this.onExit);
            this._process.off("exit", this.onExit);
        }
    }

    interface MessageListener
    {
        predicate?: (message: string) => boolean;
        resolve?: (message: string) => void;
        reject?: (reason?: any) => void;
    }
}