namespace RS
{
    export type PartialRecursive<T> =
    {
        [P in keyof T]?: PartialRecursive<T[P]>;
    };

    /**
     * Assign all properties of "from" to "to".
     * Performs deep assignment.
     * Returns back "to".
     * @param from
     * @param to
     * @param deep
     */
    export function assign<T extends object>(from: PartialRecursive<T>, to: T, deep: true): T;

    /**
     * Assign all properties of "from" to "to".
     * Performs shallow assignment.
     * Returns back "to".
     * @param from
     * @param to
     * @param deep
     */
    export function assign<T extends object>(from: Partial<T>, to: T, deep: false): T;

    export function assign(from: object, to: object, deep: boolean): object
    {
        for (const key in from)
        {
            const val = from[key];
            if (deep && Is.nonPrimitive(val) && to[key] != null)
            {
                assign(val, to[key], true);
            }
            else
            {
                to[key] = val;
            }
        }
        return to;
    }
}