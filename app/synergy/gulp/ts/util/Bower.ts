/// <reference path="Command.ts" />

/** Contains utility functions for the bower package system. */
namespace RS.Bower
{
    /**
     * Installs a bower package locally.
     * @param name
     * @param version
     */
    export async function installPackage(name: string, version: string)
    {
        // Check that the command exists
        if (!await Command.exists("bower"))
        {
            throw new Error("Could not find bower - check that it's installed! (npm install -g bower)");
        }

        Log.info(`Installing bower package '${name}' (${version})...`);

        // Execute
        try
        {
            const formattedName = /:\/\//g.test(version) ? version : `${name}#${version}`;
            const logOutput = await Command.run("bower", [ "--allow-root", "install", formattedName, "--force"], undefined, false);
        }
        catch (err)
        {
            if (/not found/g.test(err.toString()))
            {
                Log.error(`Package '${name}' not found`, "Bower");
            }
            else
            {
                Log.error(err);
            }
        }
    }
}