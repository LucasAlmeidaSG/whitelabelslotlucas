/// <reference path="Command.ts" />

/** Contains TypeScript compiler functionality. */
namespace RS.TypeScript
{
    const tscPath = "./node_modules/typescript/bin/tsc";

    /**
     * Compiles the specified TypeScript project.
     * @param projectPath
     * @param logOutput 
     */
    export function compile(projectPath: string, logOutput: boolean = false): PromiseLike<void>
    {
        return new Promise<void>((resolve, reject) =>
        {
            Command.run(tscPath, ["-p", projectPath], undefined, logOutput)
                .then(data =>
                {
                    if (data.length > 0)
                    {
                        reject(data);
                    }
                    else
                    {
                        resolve();
                    }
                }, reason =>
                {
                    const reasonAsStr = Is.string(reason) ? reason : reason.toString() as string;
                    if (/ENOENT/g.test(reasonAsStr))
                    {
                        reject("TypeScript compiler not found. Check that node modules are installed.");
                    }
                    else
                    {
                        reject(reason);
                    }
                    
                });
        });
    }
}