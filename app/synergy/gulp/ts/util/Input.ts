namespace RS.Input
{
    const readline = require("readline");
    const { Writable } = require("stream");

    export type CompleterFunction = (line: string) => [string[], string];

    /**
     * Waits for a line of user input.
     */
    export function readLine(completer?: CompleterFunction, muted?: boolean): PromiseLike<string>
    {
        return new Promise<string>((resolve, reject) =>
        {
            let outStream: NodeJS.WritableStream;
            let shouldMute = false;
            if (muted)
            {
                outStream = new Writable({
                    write: function (chunk, encoding, callback)
                    {
                        if (!shouldMute) { process.stdout.write(chunk, encoding); }
                        callback();
                    }
                });
            }
            else
            {
                outStream = process.stdout;
            }
            const rl = readline.createInterface({
                input: process.stdin,
                output: outStream,
                terminal: true,
                completer
            });
            rl.prompt();
            shouldMute = true;
            rl.on("line", (line: string) =>
            {
                rl.close();
                if (muted) { console.log(""); }
                resolve(line);
            });
        });
    }
}