namespace RS
{
    type InterfaceOf<T> = { [P in keyof T]: T[P] };

    export class MessageHandler implements IDisposable
    {
        private readonly _messageListeners: MessageListener[] = [];

        private _isDisposed = false;
        public get isDisposed() { return this._isDisposed; }

        constructor()
        {
            this.onMessageReceived = this.onMessageReceived.bind(this);
        }

        private static listenerDoesMatch(listener: MessageListener, message: string): boolean
        {
            if (listener.predicate == null) { return true; }
            if (Is.func(listener.predicate)) { return listener.predicate(message); }
            return listener.predicate === message;
        }

        public on(callback: Action<string>): void;
        public on(event: string, callback: Action<string>): void;
        public on(p1: string | Action<string>, p2?: Action<string>): void
        {
            this._messageListeners.push(Is.string(p1) ? { callback: p2, predicate: p1 } : { callback: p1 });
        }

        public off(callback: Action<string>): void;
        public off(event: string, callback: Action<string>): void;
        public off(p1: string | Action<string>, p2?: Action<string>): void
        {
            if (Is.string(p1))
            {
                for (let i = 0; i < this._messageListeners.length; ++i)
                {
                    const listener = this._messageListeners[i];
                    if (listener.predicate === p1 && listener.callback === p2)
                    {
                        this._messageListeners.splice(i, 1);
                        return;
                    }
                }
            }
            else
            {
                for (let i = 0; i < this._messageListeners.length; ++i)
                {
                    const listener = this._messageListeners[i];
                    if (listener.callback === p1)
                    {
                        this._messageListeners.splice(i, 1);
                        return;
                    }
                }
            }
        }

        /** Waits for a message of the given name. */
        public event(name: string): Promise<string>
        {
            return new Promise<string>((resolve, reject) =>
            {
                this._messageListeners.push({ predicate: name, resolve, reject });
            });
        }

        /** Waits for a message, optionally matching the given predicate. */
        public message(predicate?: (message: string) => boolean): Promise<string>
        {
            return new Promise<string>((resolve, reject) =>
            {
                this._messageListeners.push({ predicate, resolve, reject });
            });
        }

        public dispose()
        {
            if (this._isDisposed) { return; }
            this.rejectAllMessageListeners();
            this._isDisposed = true;
        }

        protected onMessageReceived(message: string)
        {
            Log.debug(`Received message: ${message}`);
            const messageListeners = this._messageListeners.filter((listener) => MessageHandler.listenerDoesMatch(listener, message));
            for (const messageListener of messageListeners)
            {
                if (messageListener.callback)
                {
                    messageListener.callback(message);
                }
                else if (messageListener.resolve)
                {
                    messageListener.resolve(message);
                    const index = this._messageListeners.indexOf(messageListener);
                    this._messageListeners.splice(index, 1);
                }
            }
            if (messageListeners.length === 0) { Log.warn(`Unhandled message: ${message}`); }
        }

        protected rejectAllMessageListeners(reason?: any)
        {
            for (const messageListener of this._messageListeners)
            {
                if (messageListener.reject)
                {
                    messageListener.reject(reason);
                }
            }
            this._messageListeners.length = 0;
        }
    }

    interface MessageListener
    {
        callback?: Action<string>;
        predicate?: Func<string, boolean> | string;
        resolve?: Action<string>;
        reject?: Action<any>;
    }
}