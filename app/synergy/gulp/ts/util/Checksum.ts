/// <reference path="fileio/FileSystem.ts" />
/// <reference path="Profile.ts" />

/** Contains utility functions for calculating checksums. */
namespace RS.Checksum
{
    const crypto = require("crypto");

    /**
     * Gets a checksum for the contents of an object.
     * @param data
     */
    export function getSimple(data: object): string;

    /**
     * Gets a checksum for the specified string.
     * @param data
     */
    export function getSimple(data: string): string;

    /**
     * Gets a checksum for the contents of an object.
     * @param data
     */
    export function getSimple(data: object | string): string
    {
        Profile.enter("checksum");
        const hash = crypto.createHash("md5");
        hash.update((Is.string(data) ? data : JSON.stringify(data)) || "", "utf8");
        const result = hash.digest("hex");
        Profile.exit("checksum");
        return result;
    }

    /**
     * Gets a checksum for the file at the specified path. Async.
     */
    export async function get(path: string)
    {
        Profile.enter("checksum");
        const hash = crypto.createHash("md5");
        await FileSystem.streamFile(path, hash.update.bind(hash));
        const result = hash.digest("hex");
        Profile.exit("checksum");
        return result;
    }

    /**
     * Finds a checksum for a group of files.
     */
    export function getComposite(paths: string[]): PromiseLike<string>;

    /**
     * Finds a checksum for a group of files and an object.
     */
    export function getComposite(paths: string[], extraData: object): PromiseLike<string>;

    export async function getComposite(paths: string[], extraData?: Object)
    {
        Profile.enter("checksum");
        paths.sort();
        const hash = crypto.createHash("md5");
        for (let i = 0; i < paths.length; i++)
        {
            await FileSystem.streamFile(paths[i], hash.update.bind(hash));
        }
        if (extraData)
        {
            hash.update(JSON.stringify(extraData), "utf8");
        }
        const result = hash.digest("hex");
        Profile.exit("checksum");
        return result;
    }

    /**
     * Gets a base64 encoded SHA256 hash of the specified string.
     * @param content
     */
    export function sha256(str: string): string
    {
        Profile.enter("checksum");
        const hash = crypto.createHash("sha256");
        hash.update(str, "utf8");
        const result = hash.digest("base64");
        Profile.exit("checksum");
        return result;
    }

    /**
     * Encapsulates a database of checksums.
     */
    export class DB
    {
        protected _db: { [key: string]: string; } = {};
        protected _dirty: boolean = false;

        /** Gets if this DB is dirty (has been changed but not saved). */
        public get dirty() { return this._dirty; }

        /**
         * Clears the database.
         */
        public clear()
        {
            this._db = {};
        }

        /**
         * Loads the database from the specified path. Fails silently if the file doesn't exist. Async.
         * @param path
         */
        public async loadFromFile(path: string)
        {
            if (!await FileSystem.fileExists(path))
            {
                this.clear();
                return;
            }

            this._db = await JSON.parseFile(path) || {};
            this._dirty = false;
        }

        /**
         * Writes the database to the specified path. Async.
         * @param path
         */
        public async saveToFile(path: string)
        {
            if (EnvArgs.dryRun.value) { return; }
            await FileSystem.createPath(Path.directoryName(path));
            await FileSystem.writeFile(path, JSON.stringify(this._db));
            this._dirty = false;
        }

        /**
         * Gets the checksum for the specified key. Returns null if not found.
         * @param key
         */
        public get(key: string): string | null
        {
            return this._db[key] || null;
        }

        /**
         * Sets the checksum for the specified key.
         * @param key
         */
        public set(key: string, checksum: string): boolean
        {
            if (checksum == null)
            {
                let changed = false;
                if (this._db[key] != null)
                {
                    this._dirty = true;
                    changed = true;
                }

                delete this._db[key];
                return changed;
            }

            if (this._db[key] === checksum) { return false; }
            this._db[key] = checksum;
            this._dirty = true;
            return true;
        }

        /**
         * Gets if the checksum by the specified key is DIFFERENT to that provided.
         * @param key
         * @param checksum
         */
        public diff(key: string, checksum: string): boolean
        {
            return this.get(key) !== checksum;
        }
    }
}

namespace RS.EnvArgs
{
    export const dryRun = new BooleanEnvArg("DRYRUN", false);
}