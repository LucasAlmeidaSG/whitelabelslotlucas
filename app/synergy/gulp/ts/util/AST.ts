namespace RS.Build
{
    export interface AST
    {
        program: ESTree.Program;
        name: string | null;
        loc: ESTree.SourceLocation | null;
        type: string;
        comments: ESTree.Comment[] | null;
    }

    export namespace AST
    {
        export type Visitor = (node: ESTree.Node) => boolean | void;

        export function visit(ast: AST, visitor: Visitor): void
        {
            visitProgram(ast.program, visitor);
        }

        function isStatement(value: any): value is ESTree.Statement
        {
            return Is.object(value) && Is.string(value.type) && value.type.indexOf("Statement") !== -1;
        }

        function isExpression(value: any): value is ESTree.Expression
        {
            return Is.object(value) && Is.string(value.type) && value.type.indexOf("Expression") !== -1;
        }

        function visitProgram(program: ESTree.Program, visitor: Visitor): void
        {
            if (visitor(program)) { return; }
            for (const childNode of program.body)
            {
                if (isStatement(childNode))
                {
                    visitStatement(childNode, visitor);
                }
            }
        }

        function visitStatement(statement: ESTree.Statement, visitor: Visitor): void
        {
            if (visitor(statement)) { return; }
            switch (statement.type)
            {
                case "BlockStatement":
                    for (const inner of statement.body)
                    {
                        visitStatement(inner, visitor);
                    }
                    break;
                case "ExpressionStatement":
                    visitExpression(statement.expression, visitor);
                    break;
                case "IfStatement":
                    visitExpression(statement.test, visitor);
                    visitStatement(statement.consequent, visitor);
                    if (statement.consequent) { visitStatement(statement.consequent, visitor); }
                    break;
                case "LabeledStatement":
                    visitStatement(statement.body, visitor);
                    break;
                case "WithStatement":
                    visitExpression(statement.object, visitor);
                    visitStatement(statement.body, visitor);
                    break;
                case "SwitchStatement":
                    visitExpression(statement.discriminant, visitor);
                    for (const subCase of statement.cases)
                    {
                        if (visitor(subCase)) { continue; }
                        if (subCase.test) { visitExpression(subCase.test, visitor); }
                        for (const subStatement of subCase.consequent)
                        {
                            visitStatement(subStatement, visitor);
                        }
                    }
                    break;
                case "ReturnStatement":
                    if (statement.argument) { visitExpression(statement.argument, visitor); }
                    break;
                case "ThrowStatement":
                    visitExpression(statement.argument, visitor);
                    break;
                case "TryStatement":
                    visitStatement(statement.block, visitor);
                    if (statement.handler) { visitStatement(statement.handler.body, visitor); }
                    if (statement.finalizer) { visitStatement(statement.finalizer, visitor); }
                    break;
                case "WhileStatement":
                    visitExpression(statement.test, visitor);
                    visitStatement(statement.body, visitor);
                    break;
                case "DoWhileStatement":
                    visitStatement(statement.body, visitor);
                    visitExpression(statement.test, visitor);
                    break;
                case "ForStatement":
                    if (statement.init)
                    {
                        if (isExpression(statement.init))
                        {
                            visitExpression(statement.init, visitor);
                        }
                        else
                        {
                            visitVariableDeclr(statement.init, visitor);
                        }
                    }
                    if (statement.test) { visitExpression(statement.test, visitor); }
                    if (statement.update) { visitExpression(statement.update, visitor); }
                    visitStatement(statement.body, visitor);
                    break;
                case "ForInStatement":
                case "ForOfStatement":
                    if (statement.left.type === "VariableDeclaration") { visitVariableDeclr(statement.left, visitor); }
                    visitExpression(statement.right, visitor);
                    visitStatement(statement.body, visitor);
                    break;
                case "FunctionDeclaration":
                    visitor(statement.id);
                    visitStatement(statement.body, visitor);
                    break;
                case "VariableDeclaration":
                    visitVariableDeclr(statement, visitor, true);
                    break;
                case "ClassDeclaration":
                    visitor(statement.id);
                    if (statement.superClass) { visitExpression(statement.superClass, visitor); }
                    visitor(statement.body);
                    for (const method of statement.body.body)
                    {
                        if (visitor(method)) { continue; }
                        visitExpression(method.key, visitor);
                        visitExpression(method.value, visitor);
                    }
                    break;
            }
        }

        function visitExpression(expr: ESTree.Expression, visitor: Visitor): void
        {
            if (visitor(expr)) { return; }
            switch (expr.type)
            {
                case "ArrayExpression":
                    for (const subElement of expr.elements)
                    {
                        if (subElement)
                        {
                            if (isExpression(subElement))
                            {
                                visitExpression(subElement, visitor);
                            }
                            else if (subElement.argument)
                            {
                                visitExpression(subElement.argument, visitor);
                            }
                        }
                    }
                    break;
                case "ObjectExpression":
                    for (const property of expr.properties)
                    {
                        visitExpression(property.key, visitor);
                        if (isExpression(property.value))
                        {
                            visitExpression(property.value, visitor);
                        }
                    }
                    break;
                case "FunctionExpression":
                    visitStatement(expr.body, visitor);
                    break;
                case "SequenceExpression":
                    for (const subExpr of expr.expressions)
                    {
                        visitExpression(subExpr, visitor);
                    }
                    break;
                case "UnaryExpression":
                    visitExpression(expr.argument, visitor);
                    break;
                case "BinaryExpression":
                    visitExpression(expr.left, visitor);
                    visitExpression(expr.right, visitor);
                    break;
                case "AssignmentExpression":
                    if (isExpression(expr.left))
                    {
                        visitExpression(expr.left, visitor);
                    }
                    visitExpression(expr.right, visitor);
                    break;
                case "UpdateExpression":
                    visitExpression(expr.argument, visitor);
                    break;
                case "LogicalExpression":
                    visitExpression(expr.left, visitor);
                    visitExpression(expr.right, visitor);
                    break;
                case "ConditionalExpression":
                    visitExpression(expr.test, visitor);
                    visitExpression(expr.alternate, visitor);
                    visitExpression(expr.consequent, visitor);
                    break;
                case "CallExpression":
                case "NewExpression":
                    if (isExpression(expr.callee)) { visitExpression(expr.callee, visitor); }
                    for (const arg of expr.arguments)
                    {
                        if (isExpression(arg)) { visitExpression(arg, visitor); }
                    }
                    break;
                case "MemberExpression":
                    if (isExpression(expr.object)) { visitExpression(expr.object, visitor); }
                    visitExpression(expr.property, visitor);
                    break;
                case "ArrowFunctionExpression":
                    if (isExpression(expr.body))
                    {
                        visitExpression(expr.body, visitor);
                    }
                    else
                    {
                        visitStatement(expr.body, visitor);
                    }
                    break;
                
            }
        }

        function visitVariableDeclr(declr: ESTree.VariableDeclaration, visitor: Visitor, noOuterVisit = false): void
        {
            if (!noOuterVisit) { if (visitor(declr)) { return; } }
            if (declr.declarations)
            {
                for (const childDeclr of declr.declarations)
                {
                    visitor(childDeclr);
                    visitor(childDeclr.id);
                    if (childDeclr.init) { visitExpression(childDeclr.init, visitor); }
                }
            }
        }

        export function expressionsEqual(a: ESTree.Expression | ESTree.SpreadElement, b: ESTree.Expression | ESTree.SpreadElement): boolean
        {
            if (a.type === "SpreadElement" || b.type === "SpreadElement")
            {
                return a.type === "SpreadElement" && b.type === "SpreadElement" && expressionsEqual(a.argument, b.argument);
            }
            if (a.type !== b.type) { return false; }
            switch (a.type)
            {
                case "ThisExpression":
                    return true;
                case "ArrayExpression":
                {
                    const other = b as ESTree.ArrayExpression;
                    return a.elements.length === other.elements.length && a.elements.every((e, i) => expressionsEqual(e, other.elements[i]));
                }
                case "ObjectExpression":
                {
                    const other = b as ESTree.ObjectExpression;
                    return a.properties.length === other.properties.length && a.properties.every((p, i) => propertiesEqual(p, other.properties[i]));
                }
                case "Identifier":
                {
                    const other = b as ESTree.Identifier;
                    return a.name === other.name;
                }
                case "MemberExpression":
                {
                    const other = b as ESTree.MemberExpression;
                    if (a.object.type === "Super" || other.object.type === "Super") { return a.object.type === other.object.type; }
                    return expressionsEqual(a.object, other.object) && expressionsEqual(a.property, other.property);
                }
                case "UnaryExpression":
                {
                    const other = b as ESTree.UnaryExpression;
                    return a.operator === other.operator && expressionsEqual(a.argument, other.argument);
                }
                case "BinaryExpression":
                {
                    const other = b as ESTree.BinaryExpression;
                    return a.operator === other.operator && expressionsEqual(a.left, other.left) && expressionsEqual(a.right, other.right);
                }
                case "LogicalExpression":
                {
                    const other = b as ESTree.LogicalExpression;
                    return a.operator === other.operator && expressionsEqual(a.left, other.left) && expressionsEqual(a.right, other.right);
                }
                case "Literal":
                {
                    const other = b as ESTree.Literal;
                    return a.value === other.value && a.raw === other.raw;
                }
                default:
                    Log.warn(`expressionsEqual: type '${a.type}' not yet supported`);
                    // Not yet supported
                    return false;
            }
        }

        export function propertiesEqual(a: ESTree.Property, b: ESTree.Property): boolean
        {
            if (a.computed !== b.computed) { return false; }
            if (a.kind !== b.kind) { return false; }
            if (!expressionsEqual(a.key, b.key)) { return false; }
            if (isExpression(a.value))
            {
                return isExpression(b.value) && expressionsEqual(a.value, b.value);
            }
            return false; // non-expressions not yet supported
        }
    }
}