namespace RS.Crypto
{
    const crypto = require("crypto");
    const algorithm = "aes-256-ctr";

    /**
     * Generates a random IV.
     */
    export function generateIV(): Buffer
    {
        return crypto.randomBytes(16);
    }

    /**
     * Generates a random key.
     */
    export function generateKey(): Buffer
    {
        return crypto.randomBytes(32);
    }

    /**
     * Encrypts the specified string using the specified password.
     * @param text 
     * @param password 
     */
    export function encrypt(text: string, password: string | Buffer, iv: string | Buffer): string
    {
        const cipher = crypto.createCipheriv(algorithm, password, iv);
        let crypted = cipher.update(text, "utf8", "hex");
        crypted += cipher.final("hex");
        return crypted;
    }

    /**
     * Decrypts the specified string using the specified password.
     * @param text 
     * @param password 
     */
    export function decrypt(text: string, password: string | Buffer, iv: string | Buffer): string
    {
        const decipher = crypto.createDecipheriv(algorithm, password, iv);
        let dec = decipher.update(text, "hex", "utf8");
        dec += decipher.final("utf8");
        return dec;
    }
}