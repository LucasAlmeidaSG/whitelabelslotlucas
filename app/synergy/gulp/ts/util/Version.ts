namespace RS.Is
{
    /** Gets if the specified value is a Version. */
    export function version(value: any): value is Version
    {
        return Is.object(value) && Is.number(value.major) && Is.number(value.minor) && Is.number(value.revision);
    }
}

namespace RS
{
    /** Encapsulates a version in "major.minor.revision" form. */
    export interface Version
    {
        major: number;
        minor: number;
        revision: number;
    }

    /** Creates an empty version (0.0.0). */
    export function Version(): Version;

    /** Creates a version. */
    export function Version(major: number, minor?: number, revision?: number): Version;

    export function Version(major = 0, minor = 0, revision = 0): Version
    {
        return { major, minor, revision };
    }

    const regExp = /([0-9]+)(?:\.([0-9]+))?(?:\.([0-9]+))?/;

    export namespace Version
    {
        /** Copies a to b. */
        export function copy(a: Version, b: Version): void
        {
            b.major = a.major;
            b.minor = a.minor;
            b.revision = a.revision;
        }

        /** Clones a. */
        export function clone(a: Version, out?: Version): Version
        {
            out = out || Version();
            copy(a, out);
            return out;
        }

        /** Gets if a == b. */
        export function equals(a: Version, b: Version): boolean
        {
            return a.major === b.major && a.minor === b.minor && a.revision === b.revision;
        }

        /** Gets if a > b. */
        export function greaterThan(a: Version, b: Version): boolean
        {
            if (a.major === b.major)
            {
                if (a.minor === b.minor)
                {
                    return a.revision > b.revision;
                }
                else
                {
                    return a.minor > b.minor;
                }
            }
            else
            {
                return a.major > b.major;
            }
        }

        /** Gets if a < b. */
        export function lessThan(a: Version, b: Version): boolean
        {
            if (a.major === b.major)
            {
                if (a.minor === b.minor)
                {
                    return a.revision < b.revision;
                }
                else
                {
                    return a.minor < b.minor;
                }
            }
            else
            {
                return a.major < b.major;
            }
        }

        /**
         * Gets if a version is compatible with a given required version.
         * It is assumed that minor and revision versions are not breaking, whilst major version are.
         * Therefore:
         * - 1.2.3 is not compatible with 2.2.3
         * - 1.3.3 is compatible with 1.2.3
         * - 1.2.4 is compatible with 1.2.3
         * - 1.2.2 is not compatible with 1.2.3
         * - 1.1.4 is not compatible with 1.2.3
         * - 0.0.4 is not compatible with 1.2.3
         */
        export function compatible(version: Version, required: Version): boolean
        {
            // Different major -> incompatible
            if (version.major !== required.major) { return false; }
            // Lower minor -> incompatible
            if (version.minor < required.minor) { return false; }
            // Same minor -> revision must be at least required revision
            if (version.minor === required.minor && version.revision < required.revision) { return false; }
            return true;
        }

        /**
         * Gets if a matches the specified version values.
         * Non-present version values are assumed to be wildcards.
         * e.g. match(Version(1, 2, 3), 1) = match 1.2.3 with 1.*.* = true
         * e.g. match(Version(1, 2, 3), 1, 3, 3) = match 1.2.3 with 1.3.3 = false
         * e.g. match(Version(1, 2, 3), 1, 2) = match 1.2.3 with 1.2.* = true
         */
        export function match(a: Version, major?: number, minor?: number, revision?: number): boolean
        {
            if (major != null && a.major !== major) { return false; }
            if (minor != null && a.minor !== minor) { return false; }
            if (revision != null && a.revision !== revision) { return false; }
            return true;
        }

        /**
         * Gets the next revision of a.
         * @param a
         * @param out
         */
        export function nextRevision(a: Version, out?: Version): Version
        {
            out = out || Version();
            copy(a, out);
            out.revision++;
            return out;
        }

        /**
         * Gets the next minor version of a.
         * @param a
         * @param out
         */
        export function nextMinor(a: Version, out?: Version): Version
        {
            out = out || Version();
            copy(a, out);
            out.minor++;
            out.revision = 0;
            return out;
        }

        /**
         * Gets the next major version of a.
         * @param a
         * @param out
         */
        export function nextMajor(a: Version, out?: Version): Version
        {
            out = out || Version();
            copy(a, out);
            out.major++;
            out.minor = 0;
            out.revision = 0;
            return out;
        }

        /**
         * Converts a to a string.
         * @param a
         */
        export function toString(a: Version): string
        {
            return `${a.major}.${a.minor}.${a.revision}`;
        }

        /**
         * Parses a string into a version.
         * @param a
         */
        export function fromString(str: string, out?: Version): Version
        {
            out = out || Version();
            const result = str.match(regExp);
            if (result)
            {
                out.major = parseInt(result[1]) || 0;
                out.minor = parseInt(result[2]) || 0;
                out.revision = parseInt(result[3]) || 0;
            }
            return out;
        }
    }
}