/// <reference path="MessageHandler.ts"/>
namespace RS
{
    let _ipc;
    function getIPC()
    {
        if (_ipc) { return _ipc; }

        _ipc = require("node-ipc");
        _ipc.config.logger = (msg: string) => Log.debug(msg, "IPC");
        _ipc.config.maxRetries = 0;
        _ipc.config.silent = true;
        return _ipc;
    }

    export class IPC
    {
        public static get id(): string { return getIPC().config.id; }
        public static set id(value: string) { getIPC().config.id = value; }

        public static get timeout(): number { return getIPC().config.retry; }
        public static set timeout(value: number) { getIPC().config.retry = value; }

        private constructor() {}
    }

    export namespace IPC
    {
        enum ReplyTarget
        {
            None,
            Client,
            Parent
        }

        export interface IChannel extends MessageHandler
        {
            readonly name: string;
            /** Sends a message on this communications channel. */
            send(message: string): Promise<void>;
            /** Closes this communications channel. */
            close(): void;
        }

        export class Server extends MessageHandler
        {
            private static _instance: Server;
            private _running = false;
            private _starting = false;

            private _lastClient: net.Socket;
            private _replyTarget: ReplyTarget = ReplyTarget.None;

            private constructor()
            {
                super();

                this.onParentMessageReceived = this.onParentMessageReceived.bind(this);
                this.onClientMessageReceived = this.onClientMessageReceived.bind(this);
                if (Process.isChild) { process.on("message", this.onParentMessageReceived); }
            }

            public static get()
            {
                if (this._instance) { return this._instance; }
                return this._instance = new Server();
            }

            public start(): Promise<void>
            {
                if (this._starting) { throw new Error(`Cannot start as server is already starting`); }
                if (this._running) { throw new Error(`Cannot start as server is already running`); }

                this._starting = true;
                return new Promise<void>((resolve, reject) =>
                {
                    function resolveAndUnlisten()
                    {
                        getIPC().server.off("error", rejectAndUnlisten);
                        resolve();
                    }

                    function rejectAndUnlisten(reason?: any)
                    {
                        getIPC().server.off("error", rejectAndUnlisten);
                        reject(reason);
                    }

                    getIPC().serve(() =>
                    {
                        this._starting = false;
                        this._running = true;
                        this.setListeners(true);
                        resolveAndUnlisten();
                    });
                    
                    getIPC().server.on("error", rejectAndUnlisten);
                    getIPC().server.start();
                });
            }

            public async broadcast(message: string): Promise<void>
            {
                // For some reason node-ipc borks on an empty message
                if (Process.isChild) { process.send(message); }
                getIPC().server.broadcast("message", this.prepareMessageForNodeIPC(message));
                Log.debug(`Broadcasted message: ${message}`);
            }

            /** Replies to the previous client message. */
            public async reply(message: string): Promise<void>
            {
                switch (this._replyTarget)
                {
                    case ReplyTarget.Client:
                    {
                        getIPC().server.emit(this._lastClient, "message", this.prepareMessageForNodeIPC(message));
                        Log.debug(`Replied to ${this._lastClient} with message: ${message}`);
                        this._lastClient = null;
                        break;
                    }

                    case ReplyTarget.Parent:
                    {
                        Log.debug(`Replied to parent process with message: ${message}`);
                        process.send(message);
                        break;
                    }

                    case ReplyTarget.None: 
                    default:
                        throw new Error(`No target for reply`);
                }
                
                this._replyTarget = ReplyTarget.None;
            }

            /** Opens a two-way communications channel with the last client. */
            public open(name: string): IChannel
            {
                throw new Error("Unimplemented");
            }

            public dispose()
            {
                if (this.isDisposed) { return; }
                this.setListeners(false);
                super.dispose();
            }

            private onParentMessageReceived(message: string)
            {
                this._replyTarget = ReplyTarget.Parent;
                this.onMessageReceived(message);
            }

            private onClientMessageReceived(message: string, socket: net.Socket)
            {
                this._lastClient = socket;
                this._replyTarget = ReplyTarget.Client;
                this.onMessageReceived(this.readPreparedMessage(message));
            }

            private prepareMessageForNodeIPC(message: string)
            {
                return message || "\0";
            }

            private readPreparedMessage(message: string): string
            {
                return message === "\0" ? "" : message;
            }

            private setListeners(attach: boolean)
            {
                const serverFunc = attach ? getIPC().server.on.bind(getIPC().server) : getIPC().server.off.bind(getIPC().server);
                serverFunc("message", this.onClientMessageReceived);
            }
        }

        export class Client extends MessageHandler
        {
            private _connected = false;

            private get server() { return getIPC().of[this.target]; }

            constructor(public readonly target: string)
            {
                super();

                this.onConnected = this.onConnected.bind(this);
                this.onDisconnected = this.onDisconnected.bind(this);
                this.onServerMessageReceived = this.onServerMessageReceived.bind(this);
            }

            public connect(): Promise<void>
            {
                if (this._connected) { throw new Error(`Attempted to connect a connected client`); }
                return new Promise<void>((resolve, reject) =>
                {
                    getIPC().connectTo(this.target, () =>
                    {
                        function resolveAndUnlisten()
                        {
                            if (this.server)
                            {
                                this.server.off("error", rejectAndUnlisten);
                                this.server.off("connect", resolveAndUnlisten);
                            }
                            
                            resolve();
                        }

                        function rejectAndUnlisten(reason?: any)
                        {
                            if (this.server)
                            {
                                this.server.off("error", rejectAndUnlisten);
                                this.server.off("connect", resolveAndUnlisten);
                            }

                            reject(reason);
                        }

                        this.setListeners(true);
                        this.server.on("error", rejectAndUnlisten);
                        this.server.on("connect", resolveAndUnlisten);
                    });
                });
            }

            public async send(message: string): Promise<void>
            {
                if (!this._connected) { throw new Error(`Attempted to send to an unconnected client`); }
                this.server.emit("message", message);
            }

            public dispose()
            {
                if (this.isDisposed) { return; }
                this.setListeners(false);
                if (this._connected) { getIPC().disconnect(this.target); }
                super.dispose();
            }

            private onConnected()
            {
                this._connected = true;
            }

            private onDisconnected()
            {
                this._connected = false;
                this.rejectAllMessageListeners();
            }

            private onServerMessageReceived(message: string)
            {
                if (message === "\0") { message = ""; }
                this.onMessageReceived(message);
            }

            private setListeners(attach: boolean)
            {
                const func = attach ? this.server.on.bind(this.server) : this.server.off.bind(this.server);
                func("connect", this.onConnected);
                func("disconnect", this.onDisconnected);
                func("message", this.onServerMessageReceived);
            }
        }
    }

    interface MessageListener
    {
        predicate?: (message: string) => boolean;
        resolve?: (message: string) => void;
        reject?: (reason?: any) => void;
    }
}