/** Contains file path manipulation functionality. */
namespace RS.Path
{
    const _path = require("path");

    const extEx = [ ".d.ts" ];

    /** Returns the file part of a path. E.g. "my/test/file.txt" => "file.txt". */
    export function baseName(path: string): string { return _path.basename(path); }

    /** Returns the directory part of a path. E.g. "my/test/file.txt" => "my/test". */
    export function directoryName(path: string): string { return _path.dirname(path); }

    /** Returns the sub-extension of a path (including the dot and extension). E.g. "script.d.ts" => ".d.ts" */
    export function subExtension(path: string): string
    {
        const fileName = baseName(path);
        const match = fileName.match(/^(?:.*)(\..*\..*)$/);
        return match ? match[1] : "";
    }

    /** Returns the extension of a path (including the dot). E.g. "my/test/file.txt" => ".txt" */
    export function extension(path: string): string
    {
        for (const ex of extEx)
        {
            if (path.substr(-ex.length) === ex)
            {
                return ex;
            }
        }
        return _path.extname(path);
    }

    /** Combines one or more parts of a path. */
    export function combine(...paths: string[]): string { return _path.join.apply(_path, paths); }

    /** Resolves any ".." and "." parts of a path. */
    export function normalise(path: string): string { return _path.normalize(path); }

    /** Finds the relative path from "from" to "to", based on the current working directory. */
    export function relative(from: string, to: string): string { return _path.relative(from, to); }

    /** Resolves a sequence of paths or path segments into an absolute path. */
    export function resolve(...paths: string[]): string { return _path.resolve(...paths); }

    /** Splits a path into segments. */
    export function split(path: string): string[] { return path.split(_path.sep); }

    /** Changes the extension of a path. E.g. "my/test/file.txt" => "my/test/file.json". New extension should include the dot. */
    export function replaceExtension(path: string, newExt: string): string
    {
        const ext = extension(path);
        return `${path.substr(0, path.length - ext.length)}${newExt}`;
    }

    /** Converts the given path to be suitable for use on any main platform - strips it of whitespace, etc. */
    export function sanitise(path: string): string
    {
        return path.toLowerCase().replace(/\s/g, "_");
    }

    /** The path seperator (e.g. "\\" on windows, "/" on osx/linux) */
    export const seperator = process.platform === "win32" ? "\\" : "/";
}