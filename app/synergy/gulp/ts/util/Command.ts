/// <reference path="Log.ts" />

/** Contains console/terminal/bash command functionality. */
namespace RS.Command
{
    const debugMode = false; // Log all commands to console?

    const spawn = require("cross-spawn");
    const shell = require("gulp-shell");
    const cmdExists = require("command-exists");

    const cmdPresence: { [cmdName: string]: boolean } = {};

    /**
     * Runs a single command. Async.
     * @param cmd
     * @param args
     * @param cwd
     * @param logOutput
     */
    export function run(cmd: string, args: string[], cwd?: string, logOutput: boolean = true): PromiseLike<string>
    {
        return new Promise<string>((resolve, reject) =>
        {
            Profile.enter(`Command.run [${cmd}]`);
            if (debugMode) { Log.debug(`$$ ${cmd} ${args.filter(a => `"${a}"`).join(" ")}`); }
            const opts = (cwd != null) ? {cwd} : undefined;
            const child = spawn(cmd, args, opts);
            let curOutput = "";
            let curErrOutput = "";
            child.stdout.on("data", (buffer: Buffer) =>
            {
                if (debugMode) { Log.debug(`$$ ${cmd}: stdout >> ${buffer.length} bytes`); }
                const str: string = buffer.toString();
                if (logOutput && str && str.length > 0)
                {
                    if (str.charAt(str.length - 1))
                    {
                        Log.info(str.substr(0, str.length - 1));
                    }
                    else
                    {
                        Log.info(str);
                    }
                }
                curOutput += str;
            });
            child.stderr.on("data", (buffer: Buffer) =>
            {
                if (debugMode) { Log.debug(`$$ ${cmd}: stderr >> ${buffer.length} bytes`); }
                const str = buffer.toString();
                if (logOutput && str && str.length > 0)
                {
                    if (str.charAt(str.length - 1))
                    {
                        Log.error(str.substr(0, str.length - 1));
                    }
                    else
                    {
                        Log.error(str);
                    }
                }
                curErrOutput += str;
            });
            child.on("close", () =>
            {
                if (debugMode) { Log.debug(`$$ ${cmd}: close`); }
                Profile.exit(`Command.run [${cmd}]`);
                if (curErrOutput.length > 0)
                {
                    reject(curErrOutput);
                }
                else
                {
                    resolve(curOutput);
                }
            });
            child.on("error", err =>
            {
                if (debugMode) { Log.debug(`$$ ${cmd}: error`); }
                Profile.exit(`Command.run [${cmd}]`);
                reject(`${cmd} - ${err}`);
            });
        });
    }

    /**
     * Runs a batch of commands in sequence. Async.
     * @param cmds
     * @param cwd
     */
    export function runMultiple(cmds: string[], cwd?: string): PromiseLike<void>
    {
        return new Promise<void>((resolve, reject) =>
        {
            let collapsed = cmds.join("; ");
            if (cwd)
            {
                collapsed = `pushd "${cwd}"; ${collapsed}; popd;`;
            }
            return shell.task([collapsed])(resolve);
        });
    }

    /**
     * Tests if the specified command is present. Async.
     * @param path
     */
    export async function exists(cmd: string)
    {
        if (cmdPresence[cmd] != null) { return cmdPresence[cmd]; }
        try
        {
            return cmdPresence[cmd] = (await cmdExists(cmd)) === cmd;
        }
        catch
        {
            return cmdPresence[cmd] = false;
        }
    }
}
