namespace RS.Build
{
    const syntax = "jsreplace <*.js> <*.js.map> [a=A b=B c=C ...]";

    async function doReplace(args: string[])
    {
        Log.debug(`doReplace(${args.join(" ")})`);

        await FileSystem.rebuildCache();

        // JS
        const jsPath = args[0];
        if (!Is.string(jsPath))
        {
            console.error(syntax);
            return;
        }
        let rawJS: string;
        try
        {
            rawJS = await FileSystem.readFile(jsPath);
        }
        catch (err)
        {
            console.error(err);
            return;
        }

        // Map
        const mapPath = args[1] === "null" ? null : args[1];
        if (mapPath && !Is.string(mapPath))
        {
            console.error(syntax);
            return;
        }

        let rawMap: string = null;
        if (mapPath != null)
        {
            try
            {
                rawMap = await FileSystem.readFile(mapPath);
            }
            catch (err)
            {
                console.error(err);
                return;
            }
        }

        // Subtitutions
        const subMap: Util.Map<string> = {};
        for (let i = 2; i < args.length; ++i)
        {
            const arg = args[i];
            const keyVal = arg.split("=");
            if (keyVal.length < 2)
            {
                console.error(`Invalid substitution '${arg}'`);
                console.error(syntax);
                return;
            }
            subMap[keyVal[0]] = keyVal.slice(1).join("=");
        }

        const map = rawMap == null ? null : JSON.parse(rawMap);
        const compiledFile = new Build.CompiledFile(rawJS, map);

        compiledFile.transform((ast) =>
        {
            Build.AST.visit(ast, mapNode.bind(this, subMap));
        }, jsPath, mapPath);

        await compiledFile.write(jsPath, mapPath);
    }

    doReplace(process.argv.slice(2));

    function mapStringLiteral(map: Readonly<Util.Map<string>>, node: ESTree.Literal): void
    {
        if (Is.string(node.value))
        {
            for (const key in map)
            {
                if (node.value.indexOf(key) !== -1)
                {
                    Log.debug(`Replaced ${key} with ${map[key]}`);
                    node.value = node.value.replace(key, map[key]);
                }
            }
        }
    }

    function mapNode(map: Readonly<Util.Map<string>>, node: ESTree.Node): void
    {
        switch (node.type)
        {
            case "Literal":
                mapStringLiteral(map, node);
                break;

            case "CallExpression":
                for (const arg of node.arguments)
                {
                    mapNode(map, arg);
                }
                break;

            case "ObjectExpression":
                for (const p of node.properties)
                {
                    if (p.kind === "init")
                    {
                        mapNode(map, p.value);
                    }
                }
                break;
        }
    }
}