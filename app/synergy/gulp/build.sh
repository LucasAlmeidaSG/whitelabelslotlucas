#!/bin/bash

# Build the gulp file
tsc -p ./ts

# Build the jsreplace utility
tsc -p ./jsreplace-ts
{ echo "var RS = require(\"./gulplib.js\").RS;"; cat jsreplace.js; } > jsreplace_new.js
rm jsreplace.js
mv jsreplace_new.js jsreplace.js

# Build the jsminify utility
tsc -p ./jsminify-ts
{ echo "var RS = require(\"./gulplib.js\").RS;"; cat jsminify.js; } > jsminify_new.js
rm jsminify.js
mv jsminify_new.js jsminify.js